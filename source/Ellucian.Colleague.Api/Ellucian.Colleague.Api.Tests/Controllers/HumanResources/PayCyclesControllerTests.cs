﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Api.Controllers.HumanResources;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Coordination.HumanResources.Adapters;
using Ellucian.Colleague.Domain.HumanResources.Tests;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.TestUtil;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Ellucian.Colleague.Coordination.HumanResources.Services;

namespace Ellucian.Colleague.Api.Tests.Controllers.HumanResources
{
    [TestClass]
    public class PayCyclesControllerTests
    {
        public Mock<ILogger> loggerMock;
        public Mock<IPayCycleService> payCycleRepositoryMock;
        public Mock<IAdapterRegistry> adapterRegistryMock;

        public TestPayCycleRepository testPayCycleRepository;
        public PayCyclesController controllerUnderTest;

        public PayCycleEntityToDtoAdapter payCycleEntityToDtoAdapter;

        public FunctionEqualityComparer<PayCycle> payCycleDtoComparer;

        public void PayCyclesControllerTestsInitialize()
        {
            loggerMock = new Mock<ILogger>();
            payCycleRepositoryMock = new Mock<IPayCycleService>();
            adapterRegistryMock = new Mock<IAdapterRegistry>();

            payCycleEntityToDtoAdapter = new PayCycleEntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);
            testPayCycleRepository = new TestPayCycleRepository();

            //payCycleRepositoryMock.Setup(r => r.GetPayCyclesAsync()).Returns(() =>
            //    testPayCycleRepository.GetPayCyclesAsync());

            adapterRegistryMock.Setup(r => r.GetAdapter<Domain.HumanResources.Entities.PayCycle, Dtos.HumanResources.PayCycle>())
                .Returns(() => (ITypeAdapter<Domain.HumanResources.Entities.PayCycle, Dtos.HumanResources.PayCycle>)payCycleEntityToDtoAdapter);

            controllerUnderTest = new PayCyclesController(loggerMock.Object, adapterRegistryMock.Object, payCycleRepositoryMock.Object);

            payCycleDtoComparer = new FunctionEqualityComparer<PayCycle>(
                (pc1, pc2) => pc1.Id == pc2.Id && pc1.Description == pc2.Description,
                (pc) => pc.Id.GetHashCode());
        }

        [TestClass]
        public class GetPayCyclesTests : PayCyclesControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            /// Gets or sets the test context which provides
            /// informatin about and functionality for the current test run.
            /// </summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            public async Task<List<PayCycle>> getExpectedPayCycles()
            {
                return (await testPayCycleRepository.GetPayCyclesAsync()).Select(pc => payCycleEntityToDtoAdapter.MapToType(pc)).ToList();
            }

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                PayCyclesControllerTestsInitialize();
            }

            [TestMethod]
            public async Task ExpectedEqualsActualTest()
            {
                //var expected = await getExpectedPayCycles();
                //var actualPayCycles = await controllerUnderTest.GetPayCyclesAsync();
                //CollectionAssert.AreEqual(expected.ToArray(), actualPayCycles.ToArray(), payCycleDtoComparer);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchGenericExceptionTest()
            {
                payCycleRepositoryMock.Setup(r => r.GetPayCyclesAsync()).Throws(new Exception());
                try
                {
                    await controllerUnderTest.GetPayCyclesAsync();
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw hre;
                }
            }
        }
    }
}
