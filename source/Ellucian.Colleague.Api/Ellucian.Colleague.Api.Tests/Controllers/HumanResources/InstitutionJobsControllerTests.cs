﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Controllers.HumanResources;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.HumanResources.Services;
using Ellucian.Colleague.Domain.Base.Tests;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace Ellucian.Colleague.Api.Tests.Controllers.HumanResources
{
    [TestClass]
    public class InstitutionJobsControllerTests
    {
        [TestClass]
        public class InstitutionJobsControllerGet
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private InstitutionJobsController InstitutionJobsController;
            private Mock<IInstitutionJobsRepository> institutionJobRepositoryMock;
            private IInstitutionJobsRepository institutionJobRepository;
            private IAdapterRegistry AdapterRegistry;
            private IEnumerable<Ellucian.Colleague.Domain.HumanResources.Entities.InstitutionJobs> allInstitutionJobEntities;
            ILogger logger = new Mock<ILogger>().Object;
            private Mock<IInstitutionJobsService> institutionJobServiceMock;
            private IInstitutionJobsService institutionJobService;
            List<Ellucian.Colleague.Dtos.InstitutionJobs> InstitutionJobList;
            private string institutionJobsGuid = "625c69ff-280b-4ed3-9474-662a43616a8a";
 
            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                institutionJobRepositoryMock = new Mock<IInstitutionJobsRepository>();
                institutionJobRepository = institutionJobRepositoryMock.Object;

                HashSet<ITypeAdapter> adapters = new HashSet<ITypeAdapter>();
                AdapterRegistry = new AdapterRegistry(adapters, logger);
                var testAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.HumanResources.Entities.InstitutionJobs, Dtos.InstitutionJobs>(AdapterRegistry, logger);
                AdapterRegistry.AddAdapter(testAdapter);

                institutionJobServiceMock = new Mock<IInstitutionJobsService>();
                institutionJobService = institutionJobServiceMock.Object;

                allInstitutionJobEntities = new TestInstitutionJobsRepository().GetInstitutionJobs();
                InstitutionJobList = new List<Dtos.InstitutionJobs>();

                InstitutionJobsController = new InstitutionJobsController(institutionJobService, logger);
                InstitutionJobsController.Request = new HttpRequestMessage();
                InstitutionJobsController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

                foreach (var institutionJob in allInstitutionJobEntities)
                {
                    Dtos.InstitutionJobs target = ConvertInstitutionJobsEntityToDto(institutionJob);
                    InstitutionJobList.Add(target);
                }
            }

            [TestCleanup]
            public void Cleanup()
            {
                InstitutionJobsController = null;
                institutionJobRepository = null;
            }

            [TestMethod]
            public async Task InstitutionJobsController_GetInstitutionJobsAsync()
            {
                InstitutionJobsController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };

                var tuple = new Tuple<IEnumerable<Dtos.InstitutionJobs>, int>(InstitutionJobList, 5);

                institutionJobServiceMock.Setup(s => s.GetInstitutionJobsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(tuple);
                var institutionJobs = await InstitutionJobsController.GetInstitutionJobsAsync(new Paging(10, 0));

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await institutionJobs.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.InstitutionJobs> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.InstitutionJobs>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.InstitutionJobs>;

                var result = results.FirstOrDefault();

                Assert.IsTrue(institutionJobs is IHttpActionResult);

                foreach (var institutionJobsDto in InstitutionJobList)
                {
                    var emp = results.FirstOrDefault(i => i.Id == institutionJobsDto.Id);

                    Assert.AreEqual(institutionJobsDto.Id, emp.Id);
                    Assert.AreEqual(institutionJobsDto.Person, emp.Person);
                }
            }

            [TestMethod]
            public async Task InstitutionJobsController_GetInstitutionJobsAsyncFilters()
            {
                InstitutionJobsController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };

                var tuple = new Tuple<IEnumerable<Dtos.InstitutionJobs>, int>(InstitutionJobList, 5);

                institutionJobServiceMock.Setup(s => s.GetInstitutionJobsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(tuple);

                var criteria = "{\"person\":\"e9e6837f-2c51-431b-9069-4ac4c0da3041\"";
                criteria += ",\"employer\":\"27447903-1c15-41e2-b997-a3a308a262d8\"";
                criteria += ",\"position\":\"4784012f-97d6-43e7-8dca-80e2362e200c\"";
                criteria += ",\"department\":\"Math\"";
                criteria += ",\"startOn\":\"2000-01-01 00:00:00.000\"";
                criteria += ",\"endOn\":\"2020-12-31 00:00:00.000\"";
                criteria += ",\"classification\":\"0f24ecb6-e3bf-4a76-9582-463a2bced2a7\"";
                criteria += ",\"preference\":\"primary\"";
                criteria += ",\"status\":\"active\"}";

                var institutionJobs = await InstitutionJobsController.GetInstitutionJobsAsync(new Paging(10, 0), criteria);

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await institutionJobs.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.InstitutionJobs> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.InstitutionJobs>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.InstitutionJobs>;

                var result = results.FirstOrDefault();

                Assert.IsTrue(institutionJobs is IHttpActionResult);

                foreach (var institutionJobsDto in InstitutionJobList)
                {
                    var emp = results.FirstOrDefault(i => i.Id == institutionJobsDto.Id);

                    Assert.AreEqual(institutionJobsDto.Id, emp.Id);
                    Assert.AreEqual(institutionJobsDto.Person, emp.Person);
                }
            }


            [TestMethod]
            public async Task GetInstitutionJobsByGuidAsync_Validate()
            {
                var thisInstitutionJob = InstitutionJobList.Where(m => m.Id == institutionJobsGuid).FirstOrDefault();

                institutionJobServiceMock.Setup(x => x.GetInstitutionJobsByGuidAsync(It.IsAny<string>())).ReturnsAsync(thisInstitutionJob);

                var institutionJob = await InstitutionJobsController.GetInstitutionJobsByGuidAsync(institutionJobsGuid);
                Assert.AreEqual(thisInstitutionJob.Id, institutionJob.Id);
                Assert.AreEqual(thisInstitutionJob.Person, institutionJob.Person);
            }

            [TestMethod]
            public async Task InstitutionJobsController_GetHedmAsync_CacheControlNotNull()
            {
                InstitutionJobsController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
                InstitutionJobsController.Request.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue();

                var tuple = new Tuple<IEnumerable<Dtos.InstitutionJobs>, int>(InstitutionJobList, 5);

                institutionJobServiceMock.Setup(s => s.GetInstitutionJobsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(tuple);
                var institutionJobs = await InstitutionJobsController.GetInstitutionJobsAsync(new Paging(10, 0));

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await institutionJobs.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.InstitutionJobs> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.InstitutionJobs>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.InstitutionJobs>;

                var result = results.FirstOrDefault();

                Assert.IsTrue(institutionJobs is IHttpActionResult);

                foreach (var institutionJobsDto in InstitutionJobList)
                {
                    var emp = results.FirstOrDefault(i => i.Id == institutionJobsDto.Id);

                    Assert.AreEqual(institutionJobsDto.Id, emp.Id);
                    Assert.AreEqual(institutionJobsDto.Person, emp.Person);
                }
            }

            [TestMethod]
            public async Task InstitutionJobsController_GetHedmAsync_NoCache()
            {
                InstitutionJobsController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
                InstitutionJobsController.Request.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue();
                InstitutionJobsController.Request.Headers.CacheControl.NoCache = true;

                var tuple = new Tuple<IEnumerable<Dtos.InstitutionJobs>, int>(InstitutionJobList, 5);

                institutionJobServiceMock.Setup(s => s.GetInstitutionJobsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(tuple);
                var institutionJobs = await InstitutionJobsController.GetInstitutionJobsAsync(new Paging(10, 0));

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await institutionJobs.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.InstitutionJobs> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.InstitutionJobs>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.InstitutionJobs>;

                var result = results.FirstOrDefault();

                Assert.IsTrue(institutionJobs is IHttpActionResult);

                foreach (var institutionJobsDto in InstitutionJobList)
                {
                    var emp = results.FirstOrDefault(i => i.Id == institutionJobsDto.Id);

                    Assert.AreEqual(institutionJobsDto.Id, emp.Id);
                    Assert.AreEqual(institutionJobsDto.Person, emp.Person);
                }
            }

            [TestMethod]
            public async Task InstitutionJobsController_GetHedmAsync_Cache()
            {
                InstitutionJobsController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
                InstitutionJobsController.Request.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue();
                InstitutionJobsController.Request.Headers.CacheControl.NoCache = false;

                var tuple = new Tuple<IEnumerable<Dtos.InstitutionJobs>, int>(InstitutionJobList, 5);

                institutionJobServiceMock.Setup(s => s.GetInstitutionJobsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(tuple);
                var institutionJobs = await InstitutionJobsController.GetInstitutionJobsAsync(new Paging(10, 0));

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await institutionJobs.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.InstitutionJobs> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.InstitutionJobs>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.InstitutionJobs>;

                var result = results.FirstOrDefault();

                Assert.IsTrue(institutionJobs is IHttpActionResult);

                foreach (var institutionJobsDto in InstitutionJobList)
                {
                    var emp = results.FirstOrDefault(i => i.Id == institutionJobsDto.Id);

                    Assert.AreEqual(institutionJobsDto.Id, emp.Id);
                    Assert.AreEqual(institutionJobsDto.Person, emp.Person);
                }
            }

            [TestMethod]
            public async Task InstitutionJobsController_GetByIdHedmAsync()
            {
                var thisInstitutionJob = InstitutionJobList.Where(m => m.Id == "625c69ff-280b-4ed3-9474-662a43616a8a").FirstOrDefault();

                institutionJobServiceMock.Setup(x => x.GetInstitutionJobsByGuidAsync(It.IsAny<string>())).ReturnsAsync(thisInstitutionJob);

                var institutionJob = await InstitutionJobsController.GetInstitutionJobsByGuidAsync("625c69ff-280b-4ed3-9474-662a43616a8a");
                Assert.AreEqual(thisInstitutionJob.Id, institutionJob.Id);
                Assert.AreEqual(thisInstitutionJob.Person, institutionJob.Person);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InstitutionJobsController_GetThrowsIntAppiExc()
            {
                institutionJobServiceMock.Setup(s => s.GetInstitutionJobsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).Throws<Exception>();

                await InstitutionJobsController.GetInstitutionJobsAsync(new Paging(100, 0));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InstitutionJobsController_GetThrowsIntAppiKeyNotFoundExc()
            {
                institutionJobServiceMock.Setup(s => s.GetInstitutionJobsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).Throws<KeyNotFoundException>();

                await InstitutionJobsController.GetInstitutionJobsAsync(new Paging(100, 0));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InstitutionJobsController_GetThrowsIntAppiArgumentExc()
            {
                institutionJobServiceMock.Setup(s => s.GetInstitutionJobsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).Throws<ArgumentException>();

                await InstitutionJobsController.GetInstitutionJobsAsync(new Paging(100, 0));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InstitutionJobsController_GetThrowsIntAppiRepositoryExc()
            {
                institutionJobServiceMock.Setup(s => s.GetInstitutionJobsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).Throws<RepositoryException>();

                await InstitutionJobsController.GetInstitutionJobsAsync(new Paging(100, 0));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InstitutionJobsController_GetThrowsIntAppiIntegrationExc()
            {
                institutionJobServiceMock.Setup(s => s.GetInstitutionJobsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).Throws<IntegrationApiException>();

                await InstitutionJobsController.GetInstitutionJobsAsync(new Paging(100, 0));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InstitutionJobsController_GetThrowsIntAppiPermissionExc()
            {
                institutionJobServiceMock.Setup(s => s.GetInstitutionJobsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).Throws<PermissionsException>();

                await InstitutionJobsController.GetInstitutionJobsAsync(new Paging(100, 0));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InstitutionJobsController_GetByIdThrowsExc()
            {
                await InstitutionJobsController.GetInstitutionJobsByGuidAsync(null);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InstitutionJobsController_GetByIdThrowsIntAppiExc()
            {
                institutionJobServiceMock.Setup(gc => gc.GetInstitutionJobsByGuidAsync(It.IsAny<string>())).Throws<Exception>();

                await InstitutionJobsController.GetInstitutionJobsByGuidAsync("sdjfh");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InstitutionJobsController_GetByIdThrowsIntAppiPermissionExc()
            {
                institutionJobServiceMock.Setup(gc => gc.GetInstitutionJobsByGuidAsync(It.IsAny<string>())).Throws<PermissionsException>();

                await InstitutionJobsController.GetInstitutionJobsByGuidAsync("sdjfh");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InstitutionJobsController_GetByIdThrowsIntAppiKeyNotFoundExc()
            {
                institutionJobServiceMock.Setup(gc => gc.GetInstitutionJobsByGuidAsync(It.IsAny<string>())).Throws<KeyNotFoundException>();

                await InstitutionJobsController.GetInstitutionJobsByGuidAsync("sdjfh");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InstitutionJobsController_GetByIdThrowsIntAppiIntegrationExc()
            {
                institutionJobServiceMock.Setup(gc => gc.GetInstitutionJobsByGuidAsync(It.IsAny<string>())).Throws<IntegrationApiException>();

                await InstitutionJobsController.GetInstitutionJobsByGuidAsync("sdjfh");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InstitutionJobsController_GetByIdThrowsIntAppiArgumentExc()
            {
                institutionJobServiceMock.Setup(gc => gc.GetInstitutionJobsByGuidAsync(It.IsAny<string>())).Throws<ArgumentException>();

                await InstitutionJobsController.GetInstitutionJobsByGuidAsync("sdjfh");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InstitutionJobsController_GetByIdThrowsIntAppiRepositoryExc()
            {
                institutionJobServiceMock.Setup(gc => gc.GetInstitutionJobsByGuidAsync(It.IsAny<string>())).Throws<RepositoryException>();

                await InstitutionJobsController.GetInstitutionJobsByGuidAsync("sdjfh");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InstitutionJobsController_PostThrowsIntAppiExc()
            {
                await InstitutionJobsController.PostInstitutionJobsAsync(InstitutionJobList[0]);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InstitutionJobsController_PutThrowsIntAppiExc()
            {
                var result = await InstitutionJobsController.PutInstitutionJobsAsync("9ae3a175-1dfd-4937-b97b-3c9ad596e023", InstitutionJobList[0]);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InstitutionJobsController_DeleteThrowsIntAppiExc()
            {
                await InstitutionJobsController.DeleteInstitutionJobsAsync("9ae3a175-1dfd-4937-b97b-3c9ad596e023");
            }

            /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
            /// <summary>
            /// Converts a InstitutionJob domain entity to its corresponding InstitutionJob DTO
            /// </summary>
            /// <param name="source">InstitutionJob domain entity</param>
            /// <returns>InstitutionJob DTO</returns>
            private Ellucian.Colleague.Dtos.InstitutionJobs ConvertInstitutionJobsEntityToDto(Ellucian.Colleague.Domain.HumanResources.Entities.InstitutionJobs source)
            {
                var institutionJob = new Ellucian.Colleague.Dtos.InstitutionJobs();
                institutionJob.Id = source.Guid;
                institutionJob.Person = new Dtos.GuidObject2(source.PersonId);

                return institutionJob;
            }
        }
    }
}