﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Controllers.HumanResources;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.HumanResources.Services;
using Ellucian.Colleague.Domain.Base.Tests;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace Ellucian.Colleague.Api.Tests.Controllers.HumanResources
{
    [TestClass]
    public class EmployeesControllerTests
    {
        [TestClass]
        public class EmployeesControllerGet
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private EmployeesController EmployeesController;
            private Mock<IEmployeeRepository> employeeRepositoryMock;
            private IEmployeeRepository employeeRepository;
            private IAdapterRegistry AdapterRegistry;
            private IEnumerable<Ellucian.Colleague.Domain.HumanResources.Entities.Employee> allEmployeeEntities;
            ILogger logger = new Mock<ILogger>().Object;
            private Mock<IEmployeeService> employeeServiceMock;
            private IEmployeeService employeeService;
            List<Ellucian.Colleague.Dtos.Employee> EmployeeList;
            private string employeesGuid = "625c69ff-280b-4ed3-9474-662a43616a8a";
 
            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                employeeRepositoryMock = new Mock<IEmployeeRepository>();
                employeeRepository = employeeRepositoryMock.Object;

                HashSet<ITypeAdapter> adapters = new HashSet<ITypeAdapter>();
                AdapterRegistry = new AdapterRegistry(adapters, logger);
                var testAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.HumanResources.Entities.Employee, Dtos.Employee>(AdapterRegistry, logger);
                AdapterRegistry.AddAdapter(testAdapter);

                employeeServiceMock = new Mock<IEmployeeService>();
                employeeService = employeeServiceMock.Object;

                allEmployeeEntities = new TestEmployeeRepository().GetEmployees();
                EmployeeList = new List<Dtos.Employee>();

                EmployeesController = new EmployeesController(logger, employeeService);
                EmployeesController.Request = new HttpRequestMessage();
                EmployeesController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

                foreach (var employee in allEmployeeEntities)
                {
                    Dtos.Employee target = ConvertEmployeeEntityToDto(employee);
                    EmployeeList.Add(target);
                }
            }

            [TestCleanup]
            public void Cleanup()
            {
                EmployeesController = null;
                employeeRepository = null;
            }

            [TestMethod]
            public async Task EmployeesController_GetEmployeesAsync()
            {
                EmployeesController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };

                var tuple = new Tuple<IEnumerable<Dtos.Employee>, int>(EmployeeList, 5);

                employeeServiceMock.Setup(s => s.GetEmployeesAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(tuple);
                var employees = await EmployeesController.GetEmployeesAsync(new Paging(10, 0), "", "");

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await employees.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.Employee> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.Employee>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.Employee>;

                var result = results.FirstOrDefault();

                Assert.IsTrue(employees is IHttpActionResult);

                foreach (var employeesDto in EmployeeList)
                {
                    var emp = results.FirstOrDefault(i => i.Id == employeesDto.Id);

                    Assert.AreEqual(employeesDto.Id, emp.Id);
                    Assert.AreEqual(employeesDto.Person, emp.Person);
                }
            }

            [TestMethod]
            public async Task GetEmployeesByGuidAsync_Validate()
            {
                var thisEmployee = EmployeeList.Where(m => m.Id == employeesGuid).FirstOrDefault();

                employeeServiceMock.Setup(x => x.GetEmployeeByIdAsync(It.IsAny<string>())).ReturnsAsync(thisEmployee);

                var employee = await EmployeesController.GetEmployeeByIdAsync(employeesGuid);
                Assert.AreEqual(thisEmployee.Id, employee.Id);
                Assert.AreEqual(thisEmployee.Person, employee.Person);
            }

            [TestMethod]
            public async Task EmployeesController_GetHedmAsync_CacheControlNotNull()
            {
                EmployeesController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
                EmployeesController.Request.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue();

                var tuple = new Tuple<IEnumerable<Dtos.Employee>, int>(EmployeeList, 5);

                employeeServiceMock.Setup(s => s.GetEmployeesAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(tuple);
                var employees = await EmployeesController.GetEmployeesAsync(new Paging(10, 0), "", "");

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await employees.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.Employee> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.Employee>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.Employee>;

                var result = results.FirstOrDefault();

                Assert.IsTrue(employees is IHttpActionResult);

                foreach (var employeesDto in EmployeeList)
                {
                    var emp = results.FirstOrDefault(i => i.Id == employeesDto.Id);

                    Assert.AreEqual(employeesDto.Id, emp.Id);
                    Assert.AreEqual(employeesDto.Person, emp.Person);
                }
            }

            [TestMethod]
            public async Task EmployeesController_GetHedmAsync_NoCache()
            {
                EmployeesController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
                EmployeesController.Request.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue();
                EmployeesController.Request.Headers.CacheControl.NoCache = true;

                var tuple = new Tuple<IEnumerable<Dtos.Employee>, int>(EmployeeList, 5);

                employeeServiceMock.Setup(s => s.GetEmployeesAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(tuple);
                var employees = await EmployeesController.GetEmployeesAsync(new Paging(10, 0), "", "");

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await employees.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.Employee> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.Employee>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.Employee>;

                var result = results.FirstOrDefault();

                Assert.IsTrue(employees is IHttpActionResult);

                foreach (var employeesDto in EmployeeList)
                {
                    var emp = results.FirstOrDefault(i => i.Id == employeesDto.Id);

                    Assert.AreEqual(employeesDto.Id, emp.Id);
                    Assert.AreEqual(employeesDto.Person, emp.Person);
                }
            }

            [TestMethod]
            public async Task EmployeesController_GetHedmAsync_Cache()
            {
                EmployeesController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
                EmployeesController.Request.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue();
                EmployeesController.Request.Headers.CacheControl.NoCache = false;

                var tuple = new Tuple<IEnumerable<Dtos.Employee>, int>(EmployeeList, 5);

                employeeServiceMock.Setup(s => s.GetEmployeesAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(tuple);
                var employees = await EmployeesController.GetEmployeesAsync(new Paging(10, 0), "", "");

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await employees.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.Employee> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.Employee>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.Employee>;

                var result = results.FirstOrDefault();

                Assert.IsTrue(employees is IHttpActionResult);

                foreach (var employeesDto in EmployeeList)
                {
                    var emp = results.FirstOrDefault(i => i.Id == employeesDto.Id);

                    Assert.AreEqual(employeesDto.Id, emp.Id);
                    Assert.AreEqual(employeesDto.Person, emp.Person);
                }
            }

            [TestMethod]
            public async Task EmployeesController_GetByIdHedmAsync()
            {
                var thisEmployee = EmployeeList.Where(m => m.Id == "625c69ff-280b-4ed3-9474-662a43616a8a").FirstOrDefault();

                employeeServiceMock.Setup(x => x.GetEmployeeByIdAsync(It.IsAny<string>())).ReturnsAsync(thisEmployee);

                var employee = await EmployeesController.GetEmployeeByIdAsync("625c69ff-280b-4ed3-9474-662a43616a8a");
                Assert.AreEqual(thisEmployee.Id, employee.Id);
                Assert.AreEqual(thisEmployee.Person, employee.Person);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task EmployeesController_GetThrowsIntAppiExc()
            {
                employeeServiceMock.Setup(s => s.GetEmployeesAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws<Exception>();

                await EmployeesController.GetEmployeesAsync(new Paging(100, 0));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task EmployeesController_GetThrowsIntAppiArgumentExc()
            {
                employeeServiceMock.Setup(s => s.GetEmployeesAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws<ArgumentException>();

                await EmployeesController.GetEmployeesAsync(new Paging(100, 0));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task EmployeesController_GetThrowsIntAppiRepositoryExc()
            {
                employeeServiceMock.Setup(s => s.GetEmployeesAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws<RepositoryException>();

                await EmployeesController.GetEmployeesAsync(new Paging(100, 0));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task EmployeesController_GetThrowsIntAppiPermissionExc()
            {
                employeeServiceMock.Setup(s => s.GetEmployeesAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Throws<PermissionsException>();

                await EmployeesController.GetEmployeesAsync(new Paging(100, 0));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task EmployeesController_GetByIdThrowsIntAppiExc()
            {
                employeeServiceMock.Setup(gc => gc.GetEmployeeByIdAsync(It.IsAny<string>())).Throws<Exception>();

                await EmployeesController.GetEmployeeByIdAsync("sdjfh");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task EmployeesController_GetByIdThrowsIntAppiPermissionExc()
            {
                employeeServiceMock.Setup(gc => gc.GetEmployeeByIdAsync(It.IsAny<string>())).Throws<PermissionsException>();

                await EmployeesController.GetEmployeeByIdAsync("sdjfh");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task EmployeesController_GetByIdThrowsIntAppiArgumentExc()
            {
                employeeServiceMock.Setup(gc => gc.GetEmployeeByIdAsync(It.IsAny<string>())).Throws<ArgumentException>();

                await EmployeesController.GetEmployeeByIdAsync("sdjfh");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task EmployeesController_GetByIdThrowsIntAppiRepositoryExc()
            {
                employeeServiceMock.Setup(gc => gc.GetEmployeeByIdAsync(It.IsAny<string>())).Throws<RepositoryException>();

                await EmployeesController.GetEmployeeByIdAsync("sdjfh");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task EmployeesController_PostThrowsIntAppiExc()
            {
                await EmployeesController.PostEmployeeAsync(EmployeeList[0]);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task EmployeesController_PutThrowsIntAppiExc()
            {
                var result = await EmployeesController.PutEmployeeAsync("9ae3a175-1dfd-4937-b97b-3c9ad596e023", EmployeeList[0]);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task EmployeesController_DeleteThrowsIntAppiExc()
            {
                await EmployeesController.DeleteEmployeeAsync("9ae3a175-1dfd-4937-b97b-3c9ad596e023");
            }

            /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
            /// <summary>
            /// Converts a Employee domain entity to its corresponding Employee DTO
            /// </summary>
            /// <param name="source">Employee domain entity</param>
            /// <returns>Employee DTO</returns>
            private Ellucian.Colleague.Dtos.Employee ConvertEmployeeEntityToDto(Ellucian.Colleague.Domain.HumanResources.Entities.Employee source)
            {
                var employee = new Ellucian.Colleague.Dtos.Employee();
                employee.Id = source.Guid;
                employee.Person = new Dtos.GuidObject2(source.PersonId);

                return employee;
            }
        }
    }
}