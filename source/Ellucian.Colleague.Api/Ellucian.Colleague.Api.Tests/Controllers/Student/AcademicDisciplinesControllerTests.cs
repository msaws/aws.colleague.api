﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Web.Adapters;
using slf4net;
using Ellucian.Colleague.Dtos;
using System.Threading.Tasks;
using System.Collections.Generic;
using Ellucian.Colleague.Configuration.Licensing;
using System.Reflection;
using Ellucian.Colleague.Domain.Base.Tests;
using System.Net.Http;
using System.Web.Http.Hosting;
using System.Web.Http;
using System.Net;
using Ellucian.Colleague.Dtos.DtoProperties;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Api.Controllers.Student;

namespace Ellucian.Colleague.Api.Tests.Controllers.Student
{
    [TestClass]
    public class AcademicDisciplinesControllerTests
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        private Mock<IAdapterRegistry> adapterRegistryMock;
        private Mock<ILogger> loggerMock;
        private Mock<IAcademicDisciplineService> academicDisciplineServiceMock;

        private string academicDisciplineId;

        private AcademicDiscipline expectedAcademicDiscipline;
        private AcademicDiscipline testAcademicDiscipline;
        private AcademicDiscipline actualAcademicDiscipline;

        private AcademicDiscipline2 expectedAcademicDiscipline2;
        private AcademicDiscipline2 testAcademicDiscipline2;
        private AcademicDiscipline2 actualAcademicDiscipline2;

        private AcademicDisciplinesController academicDisciplinesController;


        public async Task<List<AcademicDiscipline>> getActualAcademicDisciplines()
        {
            IEnumerable<AcademicDiscipline> academicDisciplineList = await academicDisciplinesController.GetAcademicDisciplinesAsync();
            return (await academicDisciplinesController.GetAcademicDisciplinesAsync()).ToList();
        }

        [TestInitialize]
        public async void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.DeploymentDirectory, "App_Data"));

            adapterRegistryMock = new Mock<IAdapterRegistry>();
            loggerMock = new Mock<ILogger>();
            academicDisciplineServiceMock = new Mock<IAcademicDisciplineService>();

            academicDisciplineId = "idc2935b-29e8-675f-907b-15a34da4f433";

            expectedAcademicDiscipline = new AcademicDiscipline()
            {
                Id = "idc2935b-29e8-675f-907b-15a34da4f433", //Id = "idc2935b-29e8-675f-907b-15a34da4f433",
                Abbreviation = "AAAA", //Code = "AAAA",
                Title = "Academic Administration",
                Description = null,
                Type = AcademicDisciplineType.Major,
            };

            expectedAcademicDiscipline2 = new AcademicDiscipline2()
            {
                Id = "idc2935b-29e8-675f-907b-15a34da4f433", //Id = "idc2935b-29e8-675f-907b-15a34da4f433",
                Abbreviation = "AAAA", //Code = "AAAA",
                Title = "Academic Administration",
                Description = null,
                Type = AcademicDisciplineType.Major,
                Reporting = new List<ReportingDtoProperty>() 
                { 
                    new ReportingDtoProperty() { Value = new ReportingCountryDtoProperty() { Code = Dtos.EnumProperties.IsoCode.USA, Value = "99.9999" }}
                }
            };

            testAcademicDiscipline = new AcademicDiscipline();
            testAcademicDiscipline2 = new AcademicDiscipline2();


            foreach (var property in typeof(AcademicDiscipline).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                property.SetValue(testAcademicDiscipline, property.GetValue(expectedAcademicDiscipline, null), null);
            }
            foreach (var property in typeof(AcademicDiscipline2).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                property.SetValue(testAcademicDiscipline2, property.GetValue(expectedAcademicDiscipline2, null), null);
            }



            academicDisciplineServiceMock.Setup<Task<AcademicDiscipline>>(s => s.GetAcademicDisciplineByGuidAsync(academicDisciplineId)).Returns(Task.FromResult(testAcademicDiscipline));
            academicDisciplineServiceMock.Setup<Task<AcademicDiscipline2>>(s => s.GetAcademicDiscipline2ByGuidAsync(academicDisciplineId, It.IsAny<bool>())).Returns(Task.FromResult(testAcademicDiscipline2));

            academicDisciplinesController = new AcademicDisciplinesController(adapterRegistryMock.Object, academicDisciplineServiceMock.Object, loggerMock.Object);
            actualAcademicDiscipline = await academicDisciplinesController.GetAcademicDisciplineByIdAsync(academicDisciplineId);
            actualAcademicDiscipline2 = await academicDisciplinesController.GetAcademicDiscipline2ByIdAsync(academicDisciplineId);
        }

        [TestCleanup]
        public void Cleanup()
        {
            adapterRegistryMock = null;
            loggerMock = null;
            academicDisciplineServiceMock = null;
            academicDisciplineId = null;
            expectedAcademicDiscipline = null;
            expectedAcademicDiscipline2 = null;
            testAcademicDiscipline = null;
            testAcademicDiscipline2 = null;
            actualAcademicDiscipline = null;
            actualAcademicDiscipline2 = null;
            academicDisciplinesController = null;
        }

        [TestMethod]
        public void AcademicDisciplinesTypeTest()
        {
            Assert.AreEqual(typeof(AcademicDiscipline), actualAcademicDiscipline.GetType());
            Assert.AreEqual(expectedAcademicDiscipline.GetType(), actualAcademicDiscipline.GetType());
        }
        [TestMethod]
        public void AcademicDisciplinesTypeTest2()
        {
            Assert.AreEqual(typeof(AcademicDiscipline2), actualAcademicDiscipline2.GetType());
            Assert.AreEqual(expectedAcademicDiscipline2.GetType(), actualAcademicDiscipline2.GetType());
        }
        [TestMethod]
        public void NumberOfKnownPropertiesTest()
        {
            var academicDisciplineProperties = typeof(AcademicDiscipline).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            Assert.AreEqual(6, academicDisciplineProperties.Length);
        }
        [TestMethod]
        public void NumberOfKnownPropertiesTest2()
        {
            var academicDisciplineProperties = typeof(AcademicDiscipline2).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            Assert.AreEqual(7, academicDisciplineProperties.Length);
        }
        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void AcademicDisciplinesController_PostThrowsIntAppiExc()
        {
            var result = academicDisciplinesController.PostAcademicDisciplines(actualAcademicDiscipline);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void AcademicDisciplinesController_PutThrowsIntAppiExc()
        {
            var result = academicDisciplinesController.PutAcademicDisciplines(actualAcademicDiscipline.Id, actualAcademicDiscipline);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public void AcademicDisciplinesController_DeleteThrowsIntAppiExc()
        {
            academicDisciplinesController.DeleteAcademicDisciplines(actualAcademicDiscipline.Id);
        }
    }

    [TestClass]
    public class AcademicDisciplineController_GetAllTests
    {
        private TestContext testContextInstance2;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance2;
            }
            set
            {
                testContextInstance2 = value;
            }
        }

        private AcademicDisciplinesController AcademicDisciplineController;
        private Mock<IAcademicDisciplineService> AcademicDisciplineServiceMock;
        private IAcademicDisciplineService AcademicDisciplineService;
        private IAdapterRegistry AdapterRegistry;
        private List<Ellucian.Colleague.Domain.Base.Entities.OtherMajor> allOtherMajorsEntities;
        private List<Ellucian.Colleague.Domain.Base.Entities.OtherMinor> allOtherMinorsEntities;
        private List<Ellucian.Colleague.Domain.Base.Entities.OtherSpecial> allOtherSpecialsEntities;
        ILogger logger = new Mock<ILogger>().Object;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
            AcademicDisciplineServiceMock = new Mock<IAcademicDisciplineService>();
            AcademicDisciplineService = AcademicDisciplineServiceMock.Object;

            HashSet<ITypeAdapter> adapters = new HashSet<ITypeAdapter>();
            AdapterRegistry = new AdapterRegistry(adapters, logger);
            var testAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Base.Entities.OtherMajor, AcademicDiscipline>(AdapterRegistry, logger);
            AdapterRegistry.AddAdapter(testAdapter);

            allOtherMajorsEntities = new TestAcademicDisciplineRepository().GetOtherMajors() as List<Ellucian.Colleague.Domain.Base.Entities.OtherMajor>;
            var AcademicDisciplinesList = new List<AcademicDiscipline>();
            var AcademicDisciplinesList2 = new List<AcademicDiscipline2>();

            AcademicDisciplineController = new AcademicDisciplinesController(AdapterRegistry, AcademicDisciplineService, logger);
            AcademicDisciplineController.Request = new HttpRequestMessage();
            AcademicDisciplineController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            foreach (var academicDiscipline in allOtherMajorsEntities)
            {
                AcademicDiscipline target = ConvertOtherMajorsEntitytoAcademicDisciplineDto(academicDiscipline);
                AcademicDisciplinesList.Add(target);

                AcademicDiscipline2 target2 = ConvertOtherMajorsEntitytoAcademicDiscipline2Dto(academicDiscipline);
                AcademicDisciplinesList2.Add(target2);

            }

            AcademicDisciplineServiceMock.Setup<Task<IEnumerable<AcademicDiscipline>>>(s => s.GetAcademicDisciplinesAsync(false)).ReturnsAsync(AcademicDisciplinesList);
            AcademicDisciplineServiceMock.Setup<Task<IEnumerable<AcademicDiscipline2>>>(s => s.GetAcademicDisciplines2Async(false)).ReturnsAsync(AcademicDisciplinesList2);
        }

        [TestCleanup]
        public void Cleanup()
        {
            AcademicDisciplineController = null;
            AcademicDisciplineService = null;
        }

        [TestMethod]
        public async Task ReturnsAllAcademicDisciplines()
        {
            List<AcademicDiscipline> AcademicDisciplines = await AcademicDisciplineController.GetAcademicDisciplinesAsync() as List<AcademicDiscipline>;
            Assert.AreEqual(AcademicDisciplines.Count, allOtherMajorsEntities.Count);
        }

        [TestMethod]
        public async Task GetAcademicDisciplines_DisciplineProperties()
        {
            List<AcademicDiscipline> AcademicDisciplines = await AcademicDisciplineController.GetAcademicDisciplinesAsync() as List<AcademicDiscipline>;
            AcademicDiscipline al = AcademicDisciplines.Where(a => a.Abbreviation == "ENGL").FirstOrDefault();
            Ellucian.Colleague.Domain.Base.Entities.OtherMajor alt = allOtherMajorsEntities.Where(a => a.Code == "ENGL").FirstOrDefault();
            Assert.AreEqual(alt.Code, al.Abbreviation);
            Assert.AreEqual(alt.Description, al.Title);
        }

        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Converts a OtherMajor domain entity to its corresponding Academic Discipline DTO
        /// </summary>
        /// <param name="source">Other Major domain entity</param>
        /// <returns>AcademicDiscipline DTO</returns>
        private Dtos.AcademicDiscipline ConvertOtherMajorsEntitytoAcademicDisciplineDto(Domain.Base.Entities.OtherMajor source)
        {
            var academicDiscipline = new Dtos.AcademicDiscipline();
            academicDiscipline.Id = source.Guid;
            academicDiscipline.Abbreviation = source.Code;
            academicDiscipline.Title = source.Description;
            academicDiscipline.Description = null;
            academicDiscipline.Type = Dtos.AcademicDisciplineType.Major;
            return academicDiscipline;
        }
        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Converts a OtherMajor domain entity to its corresponding Academic Discipline2 DTO
        /// </summary>
        /// <param name="source">Other Major domain entity</param>
        /// <returns>AcademicDiscipline DTO</returns>
        private Dtos.AcademicDiscipline2 ConvertOtherMajorsEntitytoAcademicDiscipline2Dto(Domain.Base.Entities.OtherMajor source)
        {
            var academicDiscipline = new Dtos.AcademicDiscipline2();
            academicDiscipline.Id = source.Guid;
            academicDiscipline.Abbreviation = source.Code;
            academicDiscipline.Title = source.Description;
            academicDiscipline.Description = null;
            academicDiscipline.Type = Dtos.AcademicDisciplineType.Major;
            return academicDiscipline;
        }
    }

    [TestClass]
    public class AcademicDisciplineController_GetByGuidTests
    {
        private TestContext testContextInstance2;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance2;
            }
            set
            {
                testContextInstance2 = value;
            }
        }

        private Mock<IAdapterRegistry> adapterRegistryMock;
        private Mock<ILogger> loggerMock;
        private Mock<IAcademicDisciplineService> academicDisciplineServiceMock;

        private string academicDisciplineId;

        private AcademicDiscipline expectedAcademicDiscipline;
        private AcademicDiscipline testAcademicDiscipline;

        private AcademicDiscipline2 expectedAcademicDiscipline2;
        private AcademicDiscipline2 testAcademicDiscipline2;

        private AcademicDisciplinesController academicDisciplinesController;


        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.DeploymentDirectory, "App_Data"));

            adapterRegistryMock = new Mock<IAdapterRegistry>();
            loggerMock = new Mock<ILogger>();
            academicDisciplineServiceMock = new Mock<IAcademicDisciplineService>();

            academicDisciplineId = "idc2935b-29e8-675f-907b-15a34da4f433";

            expectedAcademicDiscipline = new AcademicDiscipline()
            {
                Id = "idc2935b-29e8-675f-907b-15a34da4f433", //Id = "idc2935b-29e8-675f-907b-15a34da4f433",
                Abbreviation = "AAAA", //Code = "AAAA",
                Title = "Academic Administration",
                Description = null,
                Type = AcademicDisciplineType.Major,
            };

            expectedAcademicDiscipline2 = new AcademicDiscipline2()
            {
                Id = "idc2935b-29e8-675f-907b-15a34da4f433", //Id = "idc2935b-29e8-675f-907b-15a34da4f433",
                Abbreviation = "AAAA", //Code = "AAAA",
                Title = "Academic Administration",
                Description = null,
                Type = AcademicDisciplineType.Major,
                Reporting = new List<ReportingDtoProperty>() 
                { 
                    new ReportingDtoProperty() { Value = new ReportingCountryDtoProperty() { Code = Dtos.EnumProperties.IsoCode.USA, Value = "99.9999" }}
                }
            };

            testAcademicDiscipline = new AcademicDiscipline();
            testAcademicDiscipline2 = new AcademicDiscipline2();


            foreach (var property in typeof(AcademicDiscipline).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                property.SetValue(testAcademicDiscipline, property.GetValue(expectedAcademicDiscipline, null), null);
            }
            foreach (var property in typeof(AcademicDiscipline2).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                property.SetValue(testAcademicDiscipline2, property.GetValue(expectedAcademicDiscipline2, null), null);
            }



            academicDisciplineServiceMock.Setup<Task<AcademicDiscipline>>(s => s.GetAcademicDisciplineByGuidAsync(academicDisciplineId)).Returns(Task.FromResult(testAcademicDiscipline));
            academicDisciplineServiceMock.Setup<Task<AcademicDiscipline2>>(s => s.GetAcademicDiscipline2ByGuidAsync(academicDisciplineId, It.IsAny<bool>())).Returns(Task.FromResult(testAcademicDiscipline2));

            academicDisciplinesController = new AcademicDisciplinesController(adapterRegistryMock.Object, academicDisciplineServiceMock.Object, loggerMock.Object);

        }

        [TestCleanup]
        public void Cleanup()
        {
            academicDisciplinesController = null;
            academicDisciplineServiceMock = null;
        }

        [TestMethod]
        public async Task ReturnsAcademicDiscipline()
        {
            var actualAcademicDiscipline = await academicDisciplinesController.GetAcademicDisciplineByIdAsync(academicDisciplineId);
            Assert.AreEqual(actualAcademicDiscipline.Id, testAcademicDiscipline.Id);
        }
        [TestMethod]
        public async Task ReturnsAcademicDiscipline2()
        {
            var actualAcademicDiscipline = await academicDisciplinesController.GetAcademicDiscipline2ByIdAsync(academicDisciplineId);
            Assert.AreEqual(actualAcademicDiscipline.Id, testAcademicDiscipline.Id);
        }
        [TestMethod]
        public async Task GetAcademicDisciplineByIdAsync_BadGuid_ReturnsNotFound()
        {
            try
            {
                var actualAcademicDiscipline = await academicDisciplinesController.GetAcademicDisciplineByIdAsync("Bad ID");
            }
            catch (HttpResponseException ex)
            {
                Assert.IsTrue(ex.Response.StatusCode == HttpStatusCode.NotFound);
                throw;
            }
            
        }
        [TestMethod]
        public async Task GetAcademicDiscipline2ByIdAsync_BadGuid_ReturnsNotFound()
        {
            try
            {
                var actualAcademicDiscipline = await academicDisciplinesController.GetAcademicDiscipline2ByIdAsync("Bad ID");
            }
            catch (HttpResponseException ex)
            {
                Assert.IsTrue(ex.Response.StatusCode == HttpStatusCode.NotFound);
                throw;
            }
        }

    }

}
