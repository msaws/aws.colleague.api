﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Controllers;
using Ellucian.Colleague.Api.Controllers.Student;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Hosting;
using AdmissionApplication = Ellucian.Colleague.Dtos.AdmissionApplication;

namespace Ellucian.Colleague.Api.Tests.Controllers.Student
{
    [TestClass]
    public class AdmissionApplicationsControllerTests
    {
        [TestClass]
        public class GET
        {
            /// <summary>
            ///     Gets or sets the test context which provides
            ///     information about and functionality for the current test run.
            /// </summary>
            public TestContext TestContext { get; set; }

            Mock<IAdmissionApplicationsService> admissionApplicationServiceMock;
            Mock<IAdapterRegistry> adapterRegistryMock;
            Mock<ILogger> loggerMock;

            AdmissionApplicationsController admissionApplicationsController;
            List<Dtos.AdmissionApplication> admissionApplicationDtos;
            int offset = 0;
            int limit = 200;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(Path.Combine(TestContext.DeploymentDirectory, "App_Data"));

                admissionApplicationServiceMock = new Mock<IAdmissionApplicationsService>();
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                loggerMock = new Mock<ILogger>();

                admissionApplicationDtos = BuildData();

                admissionApplicationsController = new AdmissionApplicationsController(admissionApplicationServiceMock.Object, loggerMock.Object) { Request = new HttpRequestMessage() };
                admissionApplicationsController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
                admissionApplicationsController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            }

            private List<Dtos.AdmissionApplication> BuildData()
            {
                List<Dtos.AdmissionApplication> admissionApplications = new List<Dtos.AdmissionApplication>() 
                {
                    new Dtos.AdmissionApplication()
                    {
                        Id = "bbd216fb-0fc5-4f44-ae45-42d3cdd1e89a", 
                        Applicant = new Dtos.GuidObject2("d190d4b5-03b5-41aa-99b8-b8286717c956"),
                        Type = new Dtos.GuidObject2("b90812ee-b573-4acb-88b0-6999a050be4f"), 
                        Owner = new Dtos.GuidObject2("e0c0c94c-53a7-46b7-96c4-76b12512c323")
                    },
                    new Dtos.AdmissionApplication()
                    {
                        Id = "3f67b180-ce1d-4552-8d81-feb96b9fea5b", 
                        Applicant = new Dtos.GuidObject2("0bbb15f2-bb03-4056-bb9b-57a0ddf057ff"),
                        Type = new Dtos.GuidObject2("b90812ee-b573-4acb-88b0-6999a050be4f"),
                        Owner = new Dtos.GuidObject2("0cva17h3-er23-5796-cb9a-32f5tdh065yf")
                    },
                    new Dtos.AdmissionApplication()
                    {
                        Id = "bf67e156-8f5d-402b-8101-81b0a2796873",
                        Applicant = new Dtos.GuidObject2("cecdce5a-54a7-45fb-a975-5392a579e5bf"), 
                        Type = new Dtos.GuidObject2("b83022ee-ufhs-3idd-88b0-3837a050be4f"),
                        Owner = new Dtos.GuidObject2("0ac28907-5a9b-4102-a0d7-5d3d9c585512")
                    },
                    new Dtos.AdmissionApplication()
                    {
                        Id = "0111d6ef-5a86-465f-ac58-4265a997c136", 
                        Applicant = new Dtos.GuidObject2("cecdce5a-54a7-45fb-a975-5392a579e5bf"),
                        Type = new Dtos.GuidObject2("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52"), 
                        Owner = new Dtos.GuidObject2("bb6c261c-3818-4dc3-b693-eb3e64d70d8b")
                    },
                };
                return admissionApplications;
            }

            [TestCleanup]
            public void Cleanup()
            {
                admissionApplicationsController = null;
                admissionApplicationDtos = null;
                admissionApplicationServiceMock = null;
                adapterRegistryMock = null;
                loggerMock = null;
            }

            [TestMethod]
            public async Task AdmissionApplicationsController_GetAll_NoCache_True()
            {
                admissionApplicationsController.Request.Headers.CacheControl = new CacheControlHeaderValue
                {
                    NoCache = true,
                    Public = true
                };
                var tuple = new Tuple<IEnumerable<Dtos.AdmissionApplication>, int>(admissionApplicationDtos, 4);
                admissionApplicationServiceMock.Setup(ci => ci.GetAdmissionApplicationsAsync(offset, limit, true)).ReturnsAsync(tuple);
                var admissionApplications = await admissionApplicationsController.GetAdmissionApplicationsAsync(new Paging(limit, offset));

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await admissionApplications.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.AdmissionApplication> actuals = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.AdmissionApplication>>)httpResponseMessage.Content)
                                                                .Value as IEnumerable<Dtos.AdmissionApplication>;


                Assert.AreEqual(admissionApplicationDtos.Count, actuals.Count());

                foreach (var actual in actuals)
                {
                    var expected = admissionApplicationDtos.FirstOrDefault(i => i.Id.Equals(actual.Id, StringComparison.OrdinalIgnoreCase));

                    Assert.IsNotNull(expected);
                    Assert.AreEqual(expected.Id, actual.Id);
                    Assert.AreEqual(expected.Applicant, actual.Applicant);
                    Assert.AreEqual(expected.Type, actual.Type);
                    Assert.AreEqual(expected.Owner, actual.Owner);
                }
            }

            [TestMethod]
            public async Task AdmissionApplicationsController_GetAll_NoCache_False()
            {
                admissionApplicationsController.Request.Headers.CacheControl = new CacheControlHeaderValue
                {
                    NoCache = false,
                    Public = true
                };
                var tuple = new Tuple<IEnumerable<Dtos.AdmissionApplication>, int>(admissionApplicationDtos, 4);
                admissionApplicationServiceMock.Setup(ci => ci.GetAdmissionApplicationsAsync(offset, limit, false)).ReturnsAsync(tuple);
                var admissionApplications = await admissionApplicationsController.GetAdmissionApplicationsAsync(new Paging(limit, offset));

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await admissionApplications.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.AdmissionApplication> actuals = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.AdmissionApplication>>)httpResponseMessage.Content)
                                                                .Value as IEnumerable<Dtos.AdmissionApplication>;


                Assert.AreEqual(admissionApplicationDtos.Count, actuals.Count());

                foreach (var actual in actuals)
                {
                    var expected = admissionApplicationDtos.FirstOrDefault(i => i.Id.Equals(actual.Id, StringComparison.OrdinalIgnoreCase));

                    Assert.IsNotNull(expected);
                    Assert.AreEqual(expected.Id, actual.Id);
                    Assert.AreEqual(expected.Applicant, actual.Applicant);
                    Assert.AreEqual(expected.Type, actual.Type);
                    Assert.AreEqual(expected.Owner, actual.Owner);
                }
            }

            [TestMethod]
            public async Task AdmissionApplicationsController_GetAll_NullPage()
            {
                admissionApplicationsController.Request.Headers.CacheControl = new CacheControlHeaderValue
                {
                    NoCache = true,
                    Public = true
                };
                var tuple = new Tuple<IEnumerable<Dtos.AdmissionApplication>, int>(admissionApplicationDtos, 4);
                admissionApplicationServiceMock.Setup(ci => ci.GetAdmissionApplicationsAsync(offset, limit, true)).ReturnsAsync(tuple);
                var admissionApplications = await admissionApplicationsController.GetAdmissionApplicationsAsync(null);

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await admissionApplications.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.AdmissionApplication> actuals = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.AdmissionApplication>>)httpResponseMessage.Content)
                                                                .Value as IEnumerable<Dtos.AdmissionApplication>;


                Assert.AreEqual(admissionApplicationDtos.Count, actuals.Count());

                foreach (var actual in actuals)
                {
                    var expected = admissionApplicationDtos.FirstOrDefault(i => i.Id.Equals(actual.Id, StringComparison.OrdinalIgnoreCase));

                    Assert.IsNotNull(expected);
                    Assert.AreEqual(expected.Id, actual.Id);
                    Assert.AreEqual(expected.Applicant, actual.Applicant);
                    Assert.AreEqual(expected.Type, actual.Type);
                    Assert.AreEqual(expected.Owner, actual.Owner);
                }
            }

            [TestMethod]
            public async Task AdmissionApplicationsController_GetById()
            {
                var id = "bbd216fb-0fc5-4f44-ae45-42d3cdd1e89a";
                var admissionApplication = admissionApplicationDtos.FirstOrDefault(i => i.Id.Equals(id, StringComparison.OrdinalIgnoreCase));
                admissionApplicationServiceMock.Setup(ci => ci.GetAdmissionApplicationsByGuidAsync(id)).ReturnsAsync(admissionApplication);

                var actual = await admissionApplicationsController.GetAdmissionApplicationsByGuidAsync(id);

                var expected = admissionApplicationDtos.FirstOrDefault(i => i.Id.Equals(actual.Id, StringComparison.OrdinalIgnoreCase));

                Assert.IsNotNull(expected);
                Assert.AreEqual(expected.Id, actual.Id);
                Assert.AreEqual(expected.Applicant, actual.Applicant);
                Assert.AreEqual(expected.Type, actual.Type);
                Assert.AreEqual(expected.Owner, actual.Owner);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_GetAll_Exception()
            {
                admissionApplicationsController.Request.Headers.CacheControl = new CacheControlHeaderValue
                {
                    NoCache = false,
                    Public = true
                };
                var tuple = new Tuple<IEnumerable<Dtos.AdmissionApplication>, int>(admissionApplicationDtos, 4);
                admissionApplicationServiceMock.Setup(ci => ci.GetAdmissionApplicationsAsync(offset, limit, false)).ThrowsAsync(new Exception());
                var admissionApplications = await admissionApplicationsController.GetAdmissionApplicationsAsync(new Paging(limit, offset));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_GetById_Exception()
            {
                admissionApplicationServiceMock.Setup(ci => ci.GetAdmissionApplicationsByGuidAsync(It.IsAny<string>())).ThrowsAsync(new Exception());

                var actual = await admissionApplicationsController.GetAdmissionApplicationsByGuidAsync(It.IsAny<string>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_GetById_KeyNotFoundException()
            {
                admissionApplicationServiceMock.Setup(ci => ci.GetAdmissionApplicationsByGuidAsync(It.IsAny<string>())).ThrowsAsync(new KeyNotFoundException());

                var actual = await admissionApplicationsController.GetAdmissionApplicationsByGuidAsync(It.IsAny<string>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_PUT_Not_Supported()
            {
                var actual = await admissionApplicationsController.PutAdmissionApplicationsAsync(It.IsAny<string>(), It.IsAny<Dtos.AdmissionApplication>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_POST_Not_Supported()
            {
                var actual = await admissionApplicationsController.PostAdmissionApplicationsAsync(It.IsAny<Dtos.AdmissionApplication>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_DELETE_Not_Supported()
            {
                await admissionApplicationsController.DeleteAdmissionApplicationsAsync(It.IsAny<string>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_GetAdmissionApplications_KeyNotFoundException()
            {
                admissionApplicationServiceMock.Setup(x => x.GetAdmissionApplicationsAsync(offset, limit, It.IsAny<bool>())).Throws<KeyNotFoundException>();
                await admissionApplicationsController.GetAdmissionApplicationsAsync(new Paging(limit, offset));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_GetAdmissionApplications_PermissionsException()
            {
                admissionApplicationServiceMock.Setup(x => x.GetAdmissionApplicationsAsync(offset, limit, It.IsAny<bool>())).Throws<PermissionsException>();
                await admissionApplicationsController.GetAdmissionApplicationsAsync(new Paging(limit, offset));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_GetAdmissionApplications_ArgumentException()
            {
                admissionApplicationServiceMock.Setup(x => x.GetAdmissionApplicationsAsync(offset, limit, It.IsAny<bool>())).Throws<ArgumentException>();
                await admissionApplicationsController.GetAdmissionApplicationsAsync(new Paging(limit, offset));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_GetAdmissionApplications_RepositoryException()
            {
                admissionApplicationServiceMock.Setup(x => x.GetAdmissionApplicationsAsync(offset, limit, It.IsAny<bool>())).Throws<RepositoryException>();
                await admissionApplicationsController.GetAdmissionApplicationsAsync(new Paging(limit, offset));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_GetAdmissionApplications_IntegrationApiException()
            {
                admissionApplicationServiceMock.Setup(x => x.GetAdmissionApplicationsAsync(offset, limit, It.IsAny<bool>())).Throws<IntegrationApiException>();
                await admissionApplicationsController.GetAdmissionApplicationsAsync(new Paging(limit, offset));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_GetAdmissionApplicationsByGuidAsync_KeyNotFoundException()
            {
                admissionApplicationServiceMock.Setup(x => x.GetAdmissionApplicationsByGuidAsync(It.IsAny<string>())).Throws<KeyNotFoundException>();
                await admissionApplicationsController.GetAdmissionApplicationsByGuidAsync("1234");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_GetAdmissionApplicationsByGuidAsync_PermissionsException()
            {
                admissionApplicationServiceMock.Setup(x => x.GetAdmissionApplicationsByGuidAsync(It.IsAny<string>())).Throws<PermissionsException>();
                await admissionApplicationsController.GetAdmissionApplicationsByGuidAsync("1234");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_GetAdmissionApplicationsByGuidAsync_ArgumentException()
            {
                admissionApplicationServiceMock.Setup(x => x.GetAdmissionApplicationsByGuidAsync(It.IsAny<string>())).Throws<ArgumentException>();
                await admissionApplicationsController.GetAdmissionApplicationsByGuidAsync("1234");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_GetAdmissionApplicationsByGuidAsync_RepositoryException()
            {
                admissionApplicationServiceMock.Setup(x => x.GetAdmissionApplicationsByGuidAsync(It.IsAny<string>())).Throws<RepositoryException>();
                await admissionApplicationsController.GetAdmissionApplicationsByGuidAsync("1234");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_GetAdmissionApplicationsByGuidAsync_IntegrationApiException()
            {
                admissionApplicationServiceMock.Setup(x => x.GetAdmissionApplicationsByGuidAsync(It.IsAny<string>())).Throws<IntegrationApiException>();
                await admissionApplicationsController.GetAdmissionApplicationsByGuidAsync("1234");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionApplicationsController_GetAdmissionApplicationsByGuidAsync_NoId_Exception()
            {
                admissionApplicationServiceMock.Setup(x => x.GetAdmissionApplicationsByGuidAsync(It.IsAny<string>())).Throws<Exception>();
                await admissionApplicationsController.GetAdmissionApplicationsByGuidAsync("");
            }
        }
    }
}
