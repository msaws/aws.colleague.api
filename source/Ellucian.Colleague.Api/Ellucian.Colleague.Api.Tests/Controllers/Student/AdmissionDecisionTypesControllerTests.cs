﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Controllers;
using Ellucian.Colleague.Api.Controllers.Student;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Hosting;
using AdmissionDecisionType = Ellucian.Colleague.Dtos.AdmissionDecisionTypes;

namespace Ellucian.Colleague.Api.Tests.Controllers.Student
{
    [TestClass]
    public class AdmissionDecisionTypesControllerTests
    {
        [TestClass]
        public class GET
        {
            /// <summary>
            ///     Gets or sets the test context which provides
            ///     information about and functionality for the current test run.
            /// </summary>
            public TestContext TestContext { get; set; }

            Mock<IAdapterRegistry> adapterRegistryMock;
            Mock<ILogger> loggerMock;

            AdmissionDecisionTypesController admissionDecisionTypesController;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(Path.Combine(TestContext.DeploymentDirectory, "App_Data"));

                adapterRegistryMock = new Mock<IAdapterRegistry>();
                loggerMock = new Mock<ILogger>();

                admissionDecisionTypesController = new AdmissionDecisionTypesController(loggerMock.Object) { Request = new HttpRequestMessage() };
                admissionDecisionTypesController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
                admissionDecisionTypesController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            }

            [TestCleanup]
            public void Cleanup()
            {
                admissionDecisionTypesController = null;
                adapterRegistryMock = null;
                loggerMock = null;
            }

            [TestMethod]
            public async Task AdmissionDecisionTypesController_GetAll_True()
            {
                var admissionDecisionTypes = await admissionDecisionTypesController.GetAdmissionDecisionTypesAsync();

                Assert.IsTrue(admissionDecisionTypes.Count().Equals(0));
             
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionDecisionTypesController_GETBYID_Not_Supported()
            {
                var actual = await admissionDecisionTypesController.GetAdmissionDecisionTypeByGuidAsync(It.IsAny<string>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionDecisionTypesController_PUT_Not_Supported()
            {
                var actual = await admissionDecisionTypesController.PutAdmissionDecisionTypesAsync(It.IsAny<Dtos.AdmissionDecisionTypes>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionDecisionTypesController_POST_Not_Supported()
            {
                var actual = await admissionDecisionTypesController.PostAdmissionDecisionTypesAsync(It.IsAny<Dtos.AdmissionDecisionTypes>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task AdmissionDecisionTypesController_DELETE_Not_Supported()
            {
                await admissionDecisionTypesController.DeleteAdmissionDecisionTypesAsync(It.IsAny<string>());
            }

        }
    }
}
