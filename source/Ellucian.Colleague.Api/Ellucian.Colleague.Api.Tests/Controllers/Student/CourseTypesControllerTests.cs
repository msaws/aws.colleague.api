﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Ellucian.Colleague.Api.Controllers;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Api.Tests.Controllers.Student
{
    [TestClass]
    public class CourseTypesControllerTests
    {
        [TestClass]
        public class CourseTypeControllerGet
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private CourseTypesController CourseTypesController;

            private Mock<IStudentReferenceDataRepository> CourseTypeRepositoryMock;
            private IStudentReferenceDataRepository CourseTypeRepository;

            private IAdapterRegistry AdapterRegistry;

            private IEnumerable<Ellucian.Colleague.Domain.Student.Entities.CourseType> allCourseTypeDtos;

            ILogger logger = new Mock<ILogger>().Object;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                CourseTypeRepositoryMock = new Mock<IStudentReferenceDataRepository>();
                CourseTypeRepository = CourseTypeRepositoryMock.Object;

                HashSet<ITypeAdapter> adapters = new HashSet<ITypeAdapter>();
                AdapterRegistry = new AdapterRegistry(adapters, logger);
                var testAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.CourseType, CourseType>(AdapterRegistry, logger);
                AdapterRegistry.AddAdapter(testAdapter);

                allCourseTypeDtos = new TestCourseTypeRepository().Get();
                var CourseTypesList = new List<CourseType>();

                CourseTypesController = new CourseTypesController(AdapterRegistry, CourseTypeRepository);
                Mapper.CreateMap<Ellucian.Colleague.Domain.Student.Entities.CourseType, CourseType>();
                foreach (var CourseType in allCourseTypeDtos)
                {
                    CourseType target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.CourseType, CourseType>(CourseType);
                    CourseTypesList.Add(target);
                }
                CourseTypeRepositoryMock.Setup(x => x.GetCourseTypesAsync()).Returns(Task.FromResult((allCourseTypeDtos)));
            }

            [TestCleanup]
            public void Cleanup()
            {
                CourseTypesController = null;
                CourseTypeRepository = null;
            }


            [TestMethod]
            public async Task ReturnsAllCourseTypes()
            {
                var CourseTypes = await CourseTypesController.GetAsync();
                Assert.AreEqual(CourseTypes.Count(), allCourseTypeDtos.Count());
            }

        }
    }
}
