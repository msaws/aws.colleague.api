﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Hosting;
using AutoMapper;
using Ellucian.Colleague.Api.Controllers;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Student.Entities.Requirements;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Colleague.Dtos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Exceptions;

namespace Ellucian.Colleague.Api.Tests.Controllers.Student
{
    [TestClass]
    public class AcademicProgramControllerTests
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }


        private Mock<ILogger> _loggerMock;
        private Mock<IAcademicProgramService> _academicProgramServiceMock;
        private Mock<IAdapterRegistry> _adapterRegistryMock;

        private string _academicProgramId;

        private AcademicProgram _expectedAcademicProgram;
        private AcademicProgram _testAcademicProgram;
        private AcademicProgram _actualAcademicProgram;

        private AcademicProgramsController _academicProgramsController;


        [TestInitialize]
        public async void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(Path.Combine(TestContext.DeploymentDirectory, "App_Data"));


            _loggerMock = new Mock<ILogger>();
            _academicProgramServiceMock = new Mock<IAcademicProgramService>();
            _adapterRegistryMock = new Mock<IAdapterRegistry>();

            _academicProgramId = "idc2935b-29e8-675f-907b-15a34da4f433";

            _expectedAcademicProgram = new AcademicProgram
            {
                Id = "idc2935b-29e8-675f-907b-15a34da4f433",
                Code = "BA-HIST",
                StartDate = new DateTime(1999, 02, 01),
                EndDate = DateTime.Today,
                Title = "Bachelor Degree in History"
            };

            _testAcademicProgram = new AcademicProgram();
            foreach (var property in typeof (AcademicProgram).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                if (property.SetMethod != null)
                {
                    property.SetValue(_testAcademicProgram, property.GetValue(_expectedAcademicProgram, null), null);
                }
            }
            _academicProgramServiceMock.Setup(s => s.GetAcademicProgramByGuidAsync(_academicProgramId))
                .Returns(Task.FromResult(_testAcademicProgram));

            _academicProgramsController = new AcademicProgramsController(_adapterRegistryMock.Object, _academicProgramServiceMock.Object,
                _loggerMock.Object);
            _actualAcademicProgram = await _academicProgramsController.GetAcademicProgramByIdAsync(_academicProgramId);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _loggerMock = null;
            _academicProgramServiceMock = null;
            _academicProgramId = null;
            _expectedAcademicProgram = null;
            _testAcademicProgram = null;
            _actualAcademicProgram = null;
            _academicProgramsController = null;
        }

        [TestMethod]
        public void NumberOfKnownPropertiesTest()
        {
            var academicProgramProperties =
                typeof (AcademicProgram).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            Assert.AreEqual(13, academicProgramProperties.Length);
        }
    }

    [TestClass]
    public class AcademicProgramControllerGetAllNonHeDM
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        private AcademicProgramsController _academicProgramController;
        private Mock<IAcademicProgramService> _academicProgramServiceMock;
        private Mock<IAdapterRegistry> _adapterRegistryMock;
        private IAcademicProgramService _academicProgramService;
        private IAdapterRegistry _adapterRegistry;
        private List<Domain.Student.Entities.AcademicProgram> _allAcademicProgramsEntities;
        private List<Dtos.Student.AcademicProgram> _allAcademicProgramsDtos;
        private readonly ILogger _logger = new Mock<ILogger>().Object;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
            _academicProgramServiceMock = new Mock<IAcademicProgramService>();
            _academicProgramService = _academicProgramServiceMock.Object;
            _adapterRegistryMock = new Mock<IAdapterRegistry>();
            _adapterRegistry = _adapterRegistryMock.Object;

            _allAcademicProgramsEntities = new TestAcademicProgramRepository().GetAsync().Result as List<Domain.Student.Entities.AcademicProgram>;

            _academicProgramController = new AcademicProgramsController(_adapterRegistry, _academicProgramService, _logger)
            {
                Request = new HttpRequestMessage()
            };
            _academicProgramController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey,
                new HttpConfiguration());
            _academicProgramController.Request.Headers.CacheControl = new CacheControlHeaderValue
            {
                NoCache = true,
                Public = true
            };

            _allAcademicProgramsDtos = new List<Dtos.Student.AcademicProgram>();
            Mapper.CreateMap<Domain.Student.Entities.AcademicProgram, Dtos.Student.AcademicProgram>();
            foreach (var academicProgram in _allAcademicProgramsEntities)
            {
                var target = Mapper.Map<Domain.Student.Entities.AcademicProgram, Dtos.Student.AcademicProgram>(academicProgram);
                target.Code = academicProgram.Code;
                target.Description = academicProgram.Description;
                _allAcademicProgramsDtos.Add(target);
            }

            _academicProgramServiceMock.Setup(s => s.GetAsync())
                .ReturnsAsync(_allAcademicProgramsDtos);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _academicProgramController = null;
            _academicProgramService = null;
            _allAcademicProgramsDtos = null;
            _allAcademicProgramsEntities = null;
        }

        [TestMethod]
        public async Task AcademicProgramController_ReturnsAllAcademicPrograms()
        {
            var academicPrograms = await _academicProgramController.GetAsync() as List<Dtos.Student.AcademicProgram>;
            Assert.AreEqual(academicPrograms.Count, _allAcademicProgramsEntities.Count);
        }

        [TestMethod]
        [ExpectedException(typeof (HttpResponseException))]
        public async Task AcademicProgramController_ReturnsAllAcademicPrograms_Exception()
        {
            _academicProgramServiceMock.Setup(s => s.GetAsync()).Throws<Exception>();
            await _academicProgramController.GetAsync();
        }
    }

    [TestClass]
    public class AcademicProgramControllerGetAllHeDM
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        private AcademicProgramsController _academicProgramController;
        private Mock<IAcademicProgramService> _academicProgramServiceMock;
        private Mock<IAdapterRegistry> _adapterRegistryMock;
        private IAcademicProgramService _academicProgramService;
        private IAdapterRegistry _adapterRegistry;
        private List<Domain.Student.Entities.AcademicProgram> _allAcademicProgramsEntities;
        private List<AcademicProgram> _allAcademicProgramsDtos;
        private readonly ILogger _logger = new Mock<ILogger>().Object;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
            _academicProgramServiceMock = new Mock<IAcademicProgramService>();
            _academicProgramService = _academicProgramServiceMock.Object;
            _adapterRegistryMock = new Mock<IAdapterRegistry>();
            _adapterRegistry = _adapterRegistryMock.Object;

            _allAcademicProgramsEntities = new TestAcademicProgramRepository().GetAsync().Result as List<Domain.Student.Entities.AcademicProgram>;

            _academicProgramController = new AcademicProgramsController(_adapterRegistry, _academicProgramService, _logger)
            {
                Request = new HttpRequestMessage()
            };
            _academicProgramController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey,
                new HttpConfiguration());
            _academicProgramController.Request.Headers.CacheControl = new CacheControlHeaderValue
            {
                NoCache = true,
                Public = true
            };
            
            _allAcademicProgramsDtos = new List<AcademicProgram>();

            foreach (var academicProgram in _allAcademicProgramsEntities)
            {
                var target = new AcademicProgram();
                target.Code = academicProgram.Code;
                target.Title = academicProgram.Description;
                target.Description = academicProgram.LongDescription;
                target.Credentials = new List<GuidObject2>() { new GuidObject2() { Id = "1df164eb-8178-4321-a9f7-24f27f3991d8" } };
                target.Disciplines = new List<AcademicProgramDisciplines>() { new AcademicProgramDisciplines() { Discipline = new GuidObject2() { Id = "1df164eb-8178-5678-a9f7-24f27f3991d8" }}};
                target.AcademicLevel = new GuidObject2() { Id = "1df589eb-8178-4321-a9f7-24f43f3891d8" };
                _allAcademicProgramsDtos.Add(target);
            }

            _academicProgramServiceMock.Setup(s => s.GetAcademicProgramsAsync(It.IsAny<bool>()))
                .ReturnsAsync(_allAcademicProgramsDtos);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _academicProgramController = null;
            _academicProgramService = null;
            _allAcademicProgramsDtos = null;
            _allAcademicProgramsEntities = null;
        }

        [TestMethod]
        public async Task AcademicProgramController_ReturnsAllAcademicPrograms()
        {
            var academicPrograms = await _academicProgramController.GetAcademicProgramsAsync() as List<AcademicProgram>;
            Assert.AreEqual(academicPrograms.Count, _allAcademicProgramsEntities.Count);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramController_ReturnsAllAcademicPrograms_Exception()
        {
            _academicProgramServiceMock.Setup(s => s.GetAcademicProgramsAsync(It.IsAny<bool>())).Throws<Exception>();
            await _academicProgramController.GetAcademicProgramsAsync();
        }

        [TestMethod]
        [ExpectedException(typeof (HttpResponseException))]
        public async Task AcademicProgramController_GetAcademicProgramsById_Exception()
        {
            _academicProgramServiceMock.Setup(x => x.GetAcademicProgramByGuidAsync(It.IsAny<string>()))
                .Throws<Exception>();
            await _academicProgramController.GetAcademicProgramByIdAsync(string.Empty);
        }

        [TestMethod]
        public async Task AcademicProgramController_GetAcademicPrograms_ProgramProperties()
        {
            var academicPrograms = await _academicProgramController.GetAcademicProgramsAsync() as List<AcademicProgram>;
            var al = academicPrograms.FirstOrDefault(a => a.Code == "BA-MATH");
            var alt = _allAcademicProgramsEntities.FirstOrDefault(a => a.Code == "BA-MATH");

            Assert.AreEqual(alt.Code, al.Code);
            Assert.AreEqual(alt.Description, al.Title);
            Assert.AreEqual(alt.Code, al.Code);
        }

        [TestMethod]
        [ExpectedException(typeof (HttpResponseException))]
        public void AcademicProgramController_PostAcademicPrograms()
        {
            _academicProgramController.PostAcademicProgram(_allAcademicProgramsDtos.FirstOrDefault());
        }

        [TestMethod]
        [ExpectedException(typeof (HttpResponseException))]
        public void AcademicProgramController_PutAcademicPrograms()
        {
            var academicProgram = _allAcademicProgramsDtos.FirstOrDefault();
            _academicProgramController.PutAcademicProgram(academicProgram.Id, academicProgram);
        }

        [TestMethod]
        [ExpectedException(typeof (HttpResponseException))]
        public void AcademicProgramController_DeleteAcademicProgramAsync()
        {
            _academicProgramController.DeleteAcademicProgram(_allAcademicProgramsDtos.FirstOrDefault().Id);
        }
    }

#region HEDM V10 tests

    [TestClass]
    public class AcademicProgramsControllerTests
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        private Mock<IAcademicProgramService> academicProgramsServiceMock;
        private Mock<ILogger> loggerMock;
        private Mock<IAdapterRegistry> adapterRegistryMock;

        private AcademicProgramsController academicProgramsController;
        private IEnumerable<Domain.Student.Entities.AcademicProgram> allAcadPrograms;
        private List<Dtos.AcademicProgram3> academicProgramsCollection;
        private string expectedGuid = "7a2bf6b5-cdcd-4c8f-b5d8-3053bf5b3fbc";

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.DeploymentDirectory, "App_Data"));

            academicProgramsServiceMock = new Mock<IAcademicProgramService>();
            loggerMock = new Mock<ILogger>();
            adapterRegistryMock = new Mock<IAdapterRegistry>();

            academicProgramsCollection = new List<Dtos.AcademicProgram3>();

            allAcadPrograms = new List<Domain.Student.Entities.AcademicProgram>()
                {
                    new Domain.Student.Entities.AcademicProgram("7a2bf6b5-cdcd-4c8f-b5d8-3053bf5b3fbc", "AT", "Athletic"),
                    new Domain.Student.Entities.AcademicProgram("849e6a7c-6cd4-4f98-8a73-ab0aa3627f0d", "AC", "Academic"),
                    new Domain.Student.Entities.AcademicProgram("d2253ac7-9931-4560-b42f-1fccd43c952e", "CU", "Cultural")
                };

            foreach (var source in allAcadPrograms)
            {
                var academicProgram = new Ellucian.Colleague.Dtos.AcademicProgram3
                {
                    Id = source.Guid,
                    Code = source.Code,
                    Title = source.Description,
                    Description = null
                };
                academicProgramsCollection.Add(academicProgram);
            }

            academicProgramsController = new AcademicProgramsController(adapterRegistryMock.Object, academicProgramsServiceMock.Object, loggerMock.Object)
            {
                Request = new HttpRequestMessage()
            };
            academicProgramsController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
        }

        [TestCleanup]
        public void Cleanup()
        {
            academicProgramsController = null;
            allAcadPrograms = null;
            academicProgramsCollection = null;
            loggerMock = null;
            academicProgramsServiceMock = null;
        }

        [TestMethod]
        public async Task AcademicProgramsController_GetAcademicPrograms_ValidateFields_Nocache()
        {
            academicProgramsController.Request.Headers.CacheControl =
                 new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };

            academicProgramsServiceMock.Setup(x => x.GetAcademicPrograms3Async(false)).ReturnsAsync(academicProgramsCollection);

            var sourceContexts = (await academicProgramsController.GetAcademicPrograms3Async()).ToList();
            Assert.AreEqual(academicProgramsCollection.Count, sourceContexts.Count);
            for (var i = 0; i < sourceContexts.Count; i++)
            {
                var expected = academicProgramsCollection[i];
                var actual = sourceContexts[i];
                Assert.AreEqual(expected.Id, actual.Id, "Id, Index=" + i.ToString());
                Assert.AreEqual(expected.Title, actual.Title, "Title, Index=" + i.ToString());
                Assert.AreEqual(expected.Code, actual.Code, "Code, Index=" + i.ToString());
            }
        }

        [TestMethod]
        public async Task AcademicProgramsController_GetAcademicPrograms_ValidateFields_Cache()
        {
            academicProgramsController.Request.Headers.CacheControl =
                new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = true };

            academicProgramsServiceMock.Setup(x => x.GetAcademicPrograms3Async(true)).ReturnsAsync(academicProgramsCollection);

            var sourceContexts = (await academicProgramsController.GetAcademicPrograms3Async()).ToList();
            Assert.AreEqual(academicProgramsCollection.Count, sourceContexts.Count);
            for (var i = 0; i < sourceContexts.Count; i++)
            {
                var expected = academicProgramsCollection[i];
                var actual = sourceContexts[i];
                Assert.AreEqual(expected.Id, actual.Id, "Id, Index=" + i.ToString());
                Assert.AreEqual(expected.Title, actual.Title, "Title, Index=" + i.ToString());
                Assert.AreEqual(expected.Code, actual.Code, "Code, Index=" + i.ToString());
            }
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramsController_GetAcademicPrograms_KeyNotFoundException()
        {
            //
            academicProgramsServiceMock.Setup(x => x.GetAcademicProgramsAsync(false))
                .Throws<KeyNotFoundException>();
            await academicProgramsController.GetAcademicProgramsAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramsController_GetAcademicPrograms_PermissionsException()
        {

            academicProgramsServiceMock.Setup(x => x.GetAcademicProgramsAsync(false))
                .Throws<PermissionsException>();
            await academicProgramsController.GetAcademicProgramsAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramsController_GetAcademicPrograms_ArgumentException()
        {

            academicProgramsServiceMock.Setup(x => x.GetAcademicProgramsAsync(false))
                .Throws<ArgumentException>();
            await academicProgramsController.GetAcademicProgramsAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramsController_GetAcademicPrograms_RepositoryException()
        {

            academicProgramsServiceMock.Setup(x => x.GetAcademicProgramsAsync(false))
                .Throws<RepositoryException>();
            await academicProgramsController.GetAcademicProgramsAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramsController_GetAcademicPrograms_IntegrationApiException()
        {

            academicProgramsServiceMock.Setup(x => x.GetAcademicProgramsAsync(false))
                .Throws<IntegrationApiException>();
            await academicProgramsController.GetAcademicProgramsAsync();
        }

        [TestMethod]
        public async Task AcademicProgramsController_GetAcademicProgramsByGuidAsync_ValidateFields()
        {
            var expected = academicProgramsCollection.FirstOrDefault();
            academicProgramsServiceMock.Setup(x => x.GetAcademicProgramByGuid3Async(expected.Id)).ReturnsAsync(expected);

            var actual = await academicProgramsController.GetAcademicProgramById3Async(expected.Id);

            Assert.AreEqual(expected.Id, actual.Id, "Id");
            Assert.AreEqual(expected.Title, actual.Title, "Title");
            Assert.AreEqual(expected.Code, actual.Code, "Code");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramsController_GetAcademicPrograms_Exception()
        {
            academicProgramsServiceMock.Setup(x => x.GetAcademicProgramsAsync(false)).Throws<Exception>();
            await academicProgramsController.GetAcademicProgramsAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramsController_GetAcademicProgramsByGuidAsync_Exception()
        {
            academicProgramsServiceMock.Setup(x => x.GetAcademicProgramByGuid3Async(It.IsAny<string>())).Throws<Exception>();
            await academicProgramsController.GetAcademicProgramById3Async(string.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramsController_GetAcademicProgramsByGuid_KeyNotFoundException()
        {
            academicProgramsServiceMock.Setup(x => x.GetAcademicProgramByGuidAsync(It.IsAny<string>()))
                .Throws<KeyNotFoundException>();
            await academicProgramsController.GetAcademicProgramByIdAsync(expectedGuid);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramsController_GetAcademicProgramsByGuid_PermissionsException()
        {
            academicProgramsServiceMock.Setup(x => x.GetAcademicProgramByGuidAsync(It.IsAny<string>()))
                .Throws<PermissionsException>();
            await academicProgramsController.GetAcademicProgramByIdAsync(expectedGuid);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramsController_GetAcademicProgramsByGuid_ArgumentException()
        {
            academicProgramsServiceMock.Setup(x => x.GetAcademicProgramByGuid3Async(It.IsAny<string>()))
                .Throws<ArgumentException>();
            await academicProgramsController.GetAcademicProgramById3Async(expectedGuid);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramsController_GetAcademicProgramsByGuid_RepositoryException()
        {
            academicProgramsServiceMock.Setup(x => x.GetAcademicProgramByGuid3Async(It.IsAny<string>()))
                .Throws<RepositoryException>();
            await academicProgramsController.GetAcademicProgramById3Async(expectedGuid);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramsController_GetAcademicProgramsByGuid_IntegrationApiException()
        {
            academicProgramsServiceMock.Setup(x => x.GetAcademicProgramByGuid3Async(It.IsAny<string>()))
                .Throws<IntegrationApiException>();
            await academicProgramsController.GetAcademicProgramById3Async(expectedGuid);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramsController_GetAcademicProgramsByGuid_Exception()
        {
            academicProgramsServiceMock.Setup(x => x.GetAcademicProgramByGuid3Async(It.IsAny<string>()))
                .Throws<Exception>();
            await academicProgramsController.GetAcademicProgramById3Async(expectedGuid);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramsController_PostAcademicProgramsAsync_Exception()
        {
            academicProgramsController.PostAcademicProgram3(academicProgramsCollection.FirstOrDefault());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramsController_PutAcademicProgramsAsync_Exception()
        {
            var sourceContext = academicProgramsCollection.FirstOrDefault();
            academicProgramsController.PutAcademicProgram3(sourceContext.Id, sourceContext);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AcademicProgramsController_DeleteAcademicProgramsAsync_Exception()
        {
            academicProgramsController.DeleteAcademicProgram(academicProgramsCollection.FirstOrDefault().Id);
        }
    }
#endregion



}