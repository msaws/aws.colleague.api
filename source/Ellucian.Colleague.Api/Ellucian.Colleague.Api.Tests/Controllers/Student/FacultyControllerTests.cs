﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using AutoMapper;
using Ellucian.Colleague.Api.Controllers;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Ellucian.Colleague.Api.Tests.Controllers.Student
{
    [TestClass]
    public class FacultyControllerTests
    {
        /// <summary>
        /// Set up class to use for each faculty controller test class
        /// </summary>
        public abstract class FacultyControllerTestSetup
        {
            #region Test Context

            protected TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            public FacultyController facultyController;
            public Mock<IFacultyService> facultyServiceMock;
            public IFacultyService facultyService;
            public Mock<IFacultyRestrictionService> facultyRestrictionServiceMock;
            public IFacultyRestrictionService facultyRestrictionService;
            public Mock<ILogger> loggerMock;
            public IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Faculty> allFaculty;

            public async Task InitializeFacultyController()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                loggerMock = new Mock<ILogger>();
                facultyServiceMock = new Mock<IFacultyService>();
                facultyService = facultyServiceMock.Object;
                facultyRestrictionServiceMock = new Mock<IFacultyRestrictionService>();
                facultyRestrictionService = facultyRestrictionServiceMock.Object;
                allFaculty = await new TestFacultyRepository().GetAsync();

                facultyController = new FacultyController(facultyService, facultyRestrictionService, loggerMock.Object);

                Mapper.CreateMap<Ellucian.Colleague.Domain.Student.Entities.Faculty, Faculty>();
                Mapper.CreateMap<Ellucian.Colleague.Domain.Student.Entities.Corequisite, Corequisite>();

            }

        }

        [TestClass]
        public class FacultyController_GetFaculty: FacultyControllerTestSetup
        {
            string facultyId;
            private Faculty facultyDto;

            [TestInitialize]
            public async void Initialize()
            {
                await InitializeFacultyController();

                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                facultyId = "0000036";
                var facultyEntity = allFaculty.Where(f => f.Id == facultyId ).First();

                facultyController = new FacultyController(facultyService, facultyRestrictionService, loggerMock.Object);
                Mapper.CreateMap<Ellucian.Colleague.Domain.Student.Entities.Faculty, Faculty>();
                Mapper.CreateMap<Ellucian.Colleague.Domain.Student.Entities.Corequisite, Corequisite>();
                facultyDto = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Faculty, Faculty>(facultyEntity);
            }

            [TestCleanup]
            public void Cleanup()
            {
                facultyController = null;
                facultyService = null;
            }

            [TestMethod]
            public async Task ReturnsSingleFacultyDto()
            {
                // arrange
                facultyServiceMock.Setup(x => x.GetAsync(It.IsAny<string>())).Returns(Task.FromResult(facultyDto));

                // act
                var response = await facultyController.GetFacultyAsync(facultyId);

                // assert
                Assert.IsTrue(response is Faculty);
                Assert.AreEqual(facultyId, response.Id);
            }

        }

        [TestClass]
        public class FacultyController_GetFacultyRestrictions : FacultyControllerTestSetup
        {
            string facultyId;
            private Faculty facultyDto;

            [TestInitialize]
            public async void Initialize()
            {
                await InitializeFacultyController();

                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                facultyId = "0000036";
                var facultyEntity = allFaculty.Where(f => f.Id == facultyId).First();

                facultyController = new FacultyController(facultyService, facultyRestrictionService, loggerMock.Object);
                Mapper.CreateMap<Ellucian.Colleague.Domain.Student.Entities.Faculty, Faculty>();
                Mapper.CreateMap<Ellucian.Colleague.Domain.Student.Entities.Corequisite, Corequisite>();
                facultyDto = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Faculty, Faculty>(facultyEntity);
                facultyServiceMock.Setup(x => x.GetAsync(It.IsAny<string>())).Returns(Task.FromResult(facultyDto));
            }

            [TestCleanup]
            public void Cleanup()
            {
                facultyController = null;
                facultyService = null;
            }

            [TestMethod]
            public async Task ReturnsFacultyRestrictions()
            {
                // arrange
                facultyRestrictionServiceMock.Setup(svc => svc.GetFacultyRestrictionsAsync(It.IsAny<string>())).Returns(Task.FromResult<IEnumerable<Dtos.Base.PersonRestriction>>(new List<PersonRestriction>()
                    {
                        new PersonRestriction() {Id = "1", Title = "Restriction1"},
                        new PersonRestriction() {Id = "2", Title = "Restriction2"}
                    }));
                // act
                var restrictions = await facultyController.GetFacultyRestrictionsAsync(facultyId);
                // assert
                Assert.IsTrue(restrictions is IEnumerable<PersonRestriction>);
            }

        }

        [TestClass]
        public class FacultyController_GetFacultySections3 : FacultyControllerTestSetup
        {
            string facultyId;
            private Faculty facultyDto;

            [TestInitialize]
            public async void Initialize()
            {
                await InitializeFacultyController();
                
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                facultyId = "0000036";
                var facultyEntity = allFaculty.Where(f => f.Id == facultyId).First();

                facultyController = new FacultyController(facultyService, facultyRestrictionService, loggerMock.Object);
                Mapper.CreateMap<Ellucian.Colleague.Domain.Student.Entities.Faculty, Faculty>();
                Mapper.CreateMap<Ellucian.Colleague.Domain.Student.Entities.Corequisite, Corequisite>();
                facultyDto = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Faculty, Faculty>(facultyEntity);
                facultyServiceMock.Setup(x => x.GetAsync(It.IsAny<string>())).Returns(Task.FromResult(facultyDto));
            }

            [TestCleanup]
            public void Cleanup()
            {
                facultyController = null;
                facultyService = null;
            }

            [TestMethod]
            public async Task ReturnsFacultySections()
            {
                // Arrange
                DateTime? getDate = DateTime.Now;
                facultyServiceMock.Setup(svc => svc.GetFacultySections3Async(facultyId, getDate, getDate, false)).Returns(Task.FromResult<IEnumerable<Dtos.Student.Section3>>(new List<Section3>()
                    {
                        new Section3() {Id = "1", CourseId = "12" },
                        new Section3() {Id = "2", CourseId = "22" }
                    }));
                // Act
                var sections = await facultyController.GetFacultySections3Async(facultyId, getDate, getDate, false);
                // Assert
                Assert.IsTrue(sections is IEnumerable<Section3>);
                Assert.AreEqual(2, sections.Count());
            }
        }

        [TestClass]
        public class FacultyController_QueryFacultyByPost: FacultyControllerTestSetup
        {
            private List<string> facultyIds;
            private List<Faculty> facultyList;
            FacultyQueryCriteria criteria;

            [TestInitialize]
            public async void Initialize()
            {
                await InitializeFacultyController();
                
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                facultyIds = new List<string>() { "0000036", "0000045", "0000046", "0000048", "9999999" };
                var facultyEntityList = allFaculty.Where(f => facultyIds.Contains(f.Id)).ToList();

                facultyController = new FacultyController(facultyService, facultyRestrictionService, loggerMock.Object);
                Mapper.CreateMap<Ellucian.Colleague.Domain.Student.Entities.Faculty, Faculty>();
                Mapper.CreateMap<Ellucian.Colleague.Domain.Student.Entities.Corequisite, Corequisite>();
                facultyList = new List<Faculty>();
                foreach (var faculty in facultyEntityList)
                {
                    Faculty target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Faculty, Faculty>(faculty);
                    facultyList.Add(target);
                }
                facultyRestrictionServiceMock.Setup(svc => svc.GetFacultyRestrictionsAsync(It.IsAny<string>())).Returns(Task.FromResult<IEnumerable<Dtos.Base.PersonRestriction>>(new List<PersonRestriction>()));

                criteria = new FacultyQueryCriteria() { FacultyIds = facultyIds };
                facultyServiceMock.Setup(x => x.QueryFacultyAsync(criteria)).Returns(Task.FromResult<IEnumerable<Faculty>>(facultyList));
            }

            [TestCleanup]
            public void Cleanup()
            {
                facultyController = null;
                facultyService = null;
            }

            [TestMethod]
            public async Task ReturnsFacultyDtos()
            {
                // act
                var response = await facultyController.QueryFacultyByPostAsync(criteria);

                // assert
                Assert.IsTrue(response is IEnumerable<Faculty>);
                Assert.AreEqual(4, response.Count());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task RethrowsNullArgumentException()
            {
                // arrange
                criteria.FacultyIds = null;
                facultyServiceMock.Setup(x => x.QueryFacultyAsync(criteria)).Throws(new ArgumentNullException());

                // act
                var response =await facultyController.QueryFacultyByPostAsync(criteria);
            }
        }

        [TestClass]
        public class FacultyController_GetFacultySections4 : FacultyControllerTestSetup
        {
            string facultyId;
            private Faculty facultyDto;

            [TestInitialize]
            public async void Initialize()
            {
                await InitializeFacultyController();

                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                facultyId = "0000036";
                var facultyEntity = allFaculty.Where(f => f.Id == facultyId).First();

                facultyController = new FacultyController(facultyService, facultyRestrictionService, loggerMock.Object);
                Mapper.CreateMap<Ellucian.Colleague.Domain.Student.Entities.Faculty, Faculty>();
                Mapper.CreateMap<Ellucian.Colleague.Domain.Student.Entities.Corequisite, Corequisite>();
                facultyDto = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Faculty, Faculty>(facultyEntity);
            }

            [TestCleanup]
            public void Cleanup()
            {
                facultyController = null;
                facultyService = null;
            }

            [TestMethod]
            public async Task ReturnsFacultySections()
            {
                // Arrange
                DateTime? getDate = DateTime.Now;
                facultyServiceMock.Setup(svc => svc.GetFacultySections4Async(facultyId, getDate, getDate, false)).Returns(Task.FromResult<IEnumerable<Dtos.Student.Section3>>(new List<Section3>()
                    {
                        new Section3() {Id = "1", CourseId = "12" },
                        new Section3() {Id = "2", CourseId = "22" }
                    }));
                // Act
                var sections = await facultyController.GetFacultySections4Async(facultyId, getDate, getDate, false);
                // Assert
                Assert.IsTrue(sections is IEnumerable<Section3>);
                Assert.AreEqual(2, sections.Count());
            }
        }
    }
}