﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Ellucian.Colleague.Api.Controllers;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Api.Tests.Controllers.Student
{
    [TestClass]
    public class BooksControllerTests
    {
        #region Test Context

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #endregion

        private BooksController booksController;
        private Mock<IBookRepository> BookRepositoryMock;
        private IBookRepository bookRepository;
        private IAdapterRegistry AdapterRegistry;
        private IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Book> allBooks;
        ILogger logger = new Mock<ILogger>().Object;
        private Ellucian.Colleague.Domain.Student.Entities.Book book = null;

        [TestInitialize]
        public async void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            BookRepositoryMock = new Mock<IBookRepository>();
            bookRepository = BookRepositoryMock.Object;
            logger = new Mock<ILogger>().Object;

            HashSet<ITypeAdapter> adapters = new HashSet<ITypeAdapter>();
            AdapterRegistry = new AdapterRegistry(adapters, logger);
            var testAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Book, Book>(AdapterRegistry, logger);
            AdapterRegistry.AddAdapter(testAdapter);

            allBooks = await new TestBookRepository().GetAsync();
            book = allBooks.Where(b => b.Id == "1000").FirstOrDefault();

            booksController = new BooksController(AdapterRegistry, bookRepository, logger);
            Mapper.CreateMap<Ellucian.Colleague.Domain.Student.Entities.Book, Book>();

            //Book bookDto = Mapper.Map<Ellucian.Web.Student.Domain.Entities.Book, Book>(book);

            BookRepositoryMock.Setup(x => x.GetAsync(It.IsAny<string>())).Returns(Task.FromResult( book));
        }

        [TestCleanup]
        public void Cleanup()
        {
            booksController = null;
            bookRepository = null;
        }

        [TestMethod]
        public async Task GetABook_BookProperties()
        {
            var bookItem = await booksController.GetAsync("1000");
            Assert.IsTrue(bookItem is Book);
            Assert.AreEqual(book.Price, bookItem.Price);
        }

    }
}
