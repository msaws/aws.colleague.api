﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Net;
using Ellucian.Colleague.Api.Controllers;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System.Threading.Tasks;
using System.Web.ModelBinding;
using System.Collections.Specialized;
using System.Web.Mvc;
using System.Globalization;
using AutoMapper;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Exceptions;
using System.Reflection;
using System.Web.Http.Results;
using System.Net.Http;
using Ellucian.Colleague.Api.Controllers.Student;
using Ellucian.Colleague.Dtos.EnumProperties;
using System.Web.Http.Hosting;


namespace Ellucian.Colleague.Api.Tests.Controllers.Student
{
    [TestClass]
    public class SectionsControllerTests
    {
        [TestClass]
        public class SectionsControllerTestsGetSectionById
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;
            private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>(); //;{ "Whatever" };

            private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

            private Ellucian.Colleague.Domain.Student.Entities.Student firstStudent;
            private Ellucian.Colleague.Domain.Student.Entities.Student secondStudent;
            private Ellucian.Colleague.Domain.Student.Entities.Student thirdStudent;

            private List<Dtos.Section> allSectionDtos;
            private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
            private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
            private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
            private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
                logger = new Mock<ILogger>().Object;

                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;

                studentRepoMock = new Mock<IStudentRepository>();
                studentRepository = studentRepoMock.Object;

                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;

                allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
                allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
                if (allCourseLevels != null)
                    levels.Add(allCourseLevels[0].Code);

                firstStudent = new Domain.Student.Entities.Student("STU1", "Gamgee", null, new List<string>(), new List<string>());
                firstStudent.FirstName = "Samwise";
                secondStudent = new Domain.Student.Entities.Student("STU2", "Took", null, new List<string>(), new List<string>());
                secondStudent.FirstName = "Peregrin";
                thirdStudent = new Domain.Student.Entities.Student("STU1", "Baggins", null, new List<string>(), new List<string>());
                thirdStudent.FirstName = "Frodo"; thirdStudent.MiddleName = "Ring-bearer";

                adapterRegistry = adapterRegistryMock.Object;

                List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
                var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

                firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                firstSection.TermId = "2012/FA";
                firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
                sectionEntityList.Add(firstSection);

                secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
                sectionEntityList.Add(secondSection);

                thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
                thirdSection.EndDate = new DateTime(2011, 12, 21);
                sectionEntityList.Add(thirdSection);

                IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
                IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());

                var section1Response = BuildRosterStudentResponse(new List<string>() { "STU1" });
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("1")).Returns(Task.FromResult<IEnumerable<RosterStudent>>(section1Response));
                var section2Response = BuildRosterStudentResponse(new List<string>() { "STU1", "STU2" });
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("2")).Returns(Task.FromResult<IEnumerable<RosterStudent>>(section1Response));
                var section3Response = BuildRosterStudentResponse(new List<string>() { "STU1", "STU2", "STU3" });
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("3")).Returns(Task.FromResult<IEnumerable<RosterStudent>>(section1Response));

                // Mock section repo GetCachedSections
                bool bestFit = false;
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // Mock section repo GetNonCachedSections
                sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // mock DTO adapters
                var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

                academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
                allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

                allSectionDtos = new List<Dtos.Section>();
                foreach (var sectionEntity in sectionEntityList)
                {
                    //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                    var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section>(adapterRegistry, logger);
                    Ellucian.Colleague.Dtos.Section target = sectionDtoAdapter.MapToType(sectionEntity);
                    target.Guid = sectionEntity.Guid;
                    Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                    target.AcademicLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject() { Guid = academicLevel.Guid } };

                    Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                    target.GradeSchemes = new List<Dtos.GuidObject>() { new Dtos.GuidObject() { Guid = gradeScheme.Guid } };

                    var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                    if (course != null) target.Course = new Dtos.GuidObject { Guid = course.Guid };

                    if (sectionEntity.CourseLevelCodes != null)
                    {
                        var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                        target.CourseLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject() { Guid = courseLevel.Guid } };
                    }
                    allSectionDtos.Add(target);
                }


                // mock controller
                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionsController = null;
                sectionRepository = null;
                studentRepository = null;
                sectionCoordinationService = null;
            }

            public IEnumerable<RosterStudent> BuildRosterStudentResponse(IEnumerable<string> studentIds)
            {
                List<RosterStudent> result = new List<RosterStudent>();
                if (studentIds.Contains("STU1"))
                {
                    var rDto = new RosterStudent();
                    rDto.Id = firstStudent.Id;
                    rDto.FirstName = firstStudent.FirstName;
                    rDto.MiddleName = firstStudent.MiddleName;
                    rDto.LastName = firstStudent.LastName;
                    result.Add(rDto);
                }
                if (studentIds.Contains("STU2"))
                {
                    var rDto = new RosterStudent();
                    rDto.Id = secondStudent.Id;
                    rDto.FirstName = secondStudent.FirstName;
                    rDto.MiddleName = secondStudent.MiddleName;
                    rDto.LastName = secondStudent.LastName;
                    result.Add(rDto);
                }
                if (studentIds.Contains("STU3"))
                {
                    var rDto = new RosterStudent();
                    rDto.Id = thirdStudent.Id;
                    rDto.FirstName = thirdStudent.FirstName;
                    rDto.MiddleName = thirdStudent.MiddleName;
                    rDto.LastName = thirdStudent.LastName;
                    result.Add(rDto);
                }
                return result;
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetSectionByGuid_Exception()
            {
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionByGuidAsync(It.IsAny<string>())).Throws<Exception>();
                await sectionsController.GetSectionByGuidAsync(It.IsAny<string>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetSectionByGuid_ConfigurationException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSectionByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ConfigurationException());
                await sectionsController.GetSectionByGuidAsync(It.IsAny<string>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetSectionByGuid_PermissionException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSectionByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new PermissionsException());
                await sectionsController.GetSectionByGuidAsync(It.IsAny<string>());
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetSectionByGuid_ArgNullException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSectionByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ArgumentNullException());
                await sectionsController.GetSectionByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetSectionByGuid_RepoException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSectionByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new RepositoryException());
                await sectionsController.GetSectionByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetSectionByGuid_IntegrationApiException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSectionByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new IntegrationApiException());
                await sectionsController.GetSectionByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetSectionByGuid_ConfigurationExceptionException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSectionByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ConfigurationException());
                await sectionsController.GetSectionByGuidAsync("");
            }


            /*
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionsController_GetSectionByGuid_ArgNullException()
            {
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionByGuidAsync(It.IsAny<string>())).Throws<ArgumentNullException>();
                
                await sectionsController.GetSectionByGuidAsync("");
            }
            */
            [TestMethod]
            public async Task SectionsController_GetSectionByGuid()
            {
                var section = allSectionDtos.FirstOrDefault();

                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(section);
                var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Guid);

                var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
                Ellucian.Colleague.Domain.Student.Entities.Course course = null;
                if (section.Course != null)
                    course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Guid);

                var sectionByGuid = await sectionsController.GetSectionByGuidAsync(section.Guid);
                Assert.AreEqual(section.Guid, sectionByGuid.Guid);
                Assert.AreEqual(section.Title, sectionByGuid.Title);
                Assert.AreEqual(section.Description, sectionByGuid.Description);
                Assert.AreEqual(academicLevel.Guid, sectionByGuid.AcademicLevels.FirstOrDefault().Guid);
                Assert.AreEqual(gradeScheme.Guid, sectionByGuid.GradeSchemes.FirstOrDefault().Guid);
                Assert.AreEqual(section.StartDate, sectionByGuid.StartDate);
                Assert.AreEqual(section.EndDate, sectionByGuid.EndDate);
                if (course != null && sectionByGuid.Course != null)
                    Assert.AreEqual(course.Guid, sectionByGuid.Course.Guid);
                Assert.AreEqual(section.Number, sectionByGuid.Number);
                Assert.AreEqual(section.MaximumEnrollment, sectionByGuid.MaximumEnrollment);

                var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Guid);
                if (courseLevel != null)
                    Assert.AreEqual(courseLevel.Guid, sectionByGuid.CourseLevels[0].Guid);

            }
            /*
            [TestMethod]
            public async Task SectionsController_GetHedmSectionByGuid()
            {


                var section = allSectionDtos.FirstOrDefault(); 
                //sectionCoordinationServiceMock.Setup(svc => svc.GetSection2ByGuidAsync(It.IsAny<string>())).ReturnsAsync(section);
               
                
                var sectionByGuid = await sectionsController.GetHedmSectionByGuidAsync(section.Guid);
                Assert.AreEqual(section.Guid, sectionByGuid.Id);
                Assert.AreEqual(section.Title, sectionByGuid.Title);
                Assert.AreEqual(section.Description, sectionByGuid.Description);
            }
            */

        }

        [TestClass]
        public class SectionsControllerTestsPutGradesExceptions
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;
                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;
                logger = new Mock<ILogger>().Object;

                // controller that will be tested using mock objects
                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                adapterRegistry = null;
                sectionsController = null;
                sectionRepository = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_UpdateGradesPermissionException()
            {
                string sectionId = "100";
                sectionCoordinationServiceMock.Setup(s => s.ImportGradesAsync(It.Is<Ellucian.Colleague.Dtos.Student.SectionGrades>(x => x.SectionId == sectionId)))
                    .Throws(new Ellucian.Web.Security.PermissionsException());
                await sectionsController.PutCollectionOfStudentGradesAsync(sectionId, new SectionGrades() { SectionId = sectionId, StudentGrades = GetStudentGrade() });
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_CoordinationServiceThrowsException()
            {
                string sectionId = "101";
                sectionCoordinationServiceMock.Setup(s => s.ImportGradesAsync(It.Is<Ellucian.Colleague.Dtos.Student.SectionGrades>(x => x.SectionId == sectionId)))
                    .Throws(new Exception());
                await sectionsController.PutCollectionOfStudentGradesAsync(sectionId, new SectionGrades() { SectionId = sectionId, StudentGrades = GetStudentGrade() });
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_MismatchedSectionIds()
            {
                await sectionsController.PutCollectionOfStudentGradesAsync("1", new SectionGrades() { SectionId = "2", StudentGrades = GetStudentGrade() });
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_MissingSectionId()
            {
                await sectionsController.PutCollectionOfStudentGradesAsync("1", new SectionGrades() { SectionId = null, StudentGrades = GetStudentGrade() });
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_MissingGrades()
            {
                await sectionsController.PutCollectionOfStudentGradesAsync("1", new SectionGrades() { SectionId = "1" });
            }

            private List<Dtos.Student.StudentGrade> GetStudentGrade()
            {
                List<Dtos.Student.StudentGrade> listOfOneGrade = new List<StudentGrade>();
                listOfOneGrade.Add(new Dtos.Student.StudentGrade() { StudentId = "1" });
                return listOfOneGrade;
            }

        }

        [TestClass]
        public class SectionsControllerTestsPutGrades
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;
                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;
                logger = new Mock<ILogger>().Object;

                sectionCoordinationServiceMock.Setup(s => s.ImportGradesAsync(It.IsAny<Ellucian.Colleague.Dtos.Student.SectionGrades>()))
                   .Returns(Task.FromResult<IEnumerable<SectionGradeResponse>>(GetSectionGradeResponse()));

                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                adapterRegistry = null;
                sectionsController = null;
                sectionRepository = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            public async Task SectionsController_PutCollectionOfStudentGrades()
            {
                string sectionId = "111";
                var response = await sectionsController.PutCollectionOfStudentGradesAsync(sectionId, new SectionGrades() { SectionId = sectionId, StudentGrades = GetStudentGrade() });
                var expected = GetSectionGradeResponse();

                Assert.AreEqual(expected[0].StudentId, response.First().StudentId);
                Assert.AreEqual(expected[0].Status, response.First().Status);
                Assert.AreEqual(expected[0].Errors.Count(), response.First().Errors.Count());
                Assert.AreEqual(expected[0].Errors[0].Message, response.First().Errors[0].Message);
                Assert.AreEqual(expected[0].Errors[0].Property, response.First().Errors[0].Property);
            }

            private List<Dtos.Student.SectionGradeResponse> GetSectionGradeResponse()
            {
                List<SectionGradeResponse> returnList = new List<SectionGradeResponse>();

                var response = new Dtos.Student.SectionGradeResponse();
                response.StudentId = "101";
                response.Status = "status";

                var error = new Dtos.Student.SectionGradeResponseError();
                error.Message = "message";
                error.Property = "property";
                response.Errors.Add(error);

                returnList.Add(response);
                return returnList;
            }

            private List<Dtos.Student.StudentGrade> GetStudentGrade()
            {
                List<Dtos.Student.StudentGrade> listOfOneGrade = new List<StudentGrade>();
                listOfOneGrade.Add(new Dtos.Student.StudentGrade() { StudentId = "1" });
                return listOfOneGrade;
            }
        }

        [TestClass]
        public class SectionsControllerTestsPutGrades2
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;
                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;
                logger = new Mock<ILogger>().Object;

                sectionCoordinationServiceMock.Setup(s => s.ImportGrades2Async(It.IsAny<Ellucian.Colleague.Dtos.Student.SectionGrades2>()))
                   .Returns(Task.FromResult<IEnumerable<SectionGradeResponse>>(GetSectionGradeResponse()));

                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                adapterRegistry = null;
                sectionsController = null;
                sectionRepository = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades2_EmptySectionGrades()
            {
                var response = await sectionsController.PutCollectionOfStudentGrades2Async("123", null);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades2_ModelState()
            {
                sectionsController.ModelState.AddModelError("key", "error message");
                var response = await sectionsController.PutCollectionOfStudentGrades2Async("123", null);
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades2_EmptySectionID()
            {
                var response = await sectionsController.PutCollectionOfStudentGrades2Async("", new SectionGrades2() { SectionId = "", StudentGrades = GetStudentGrade2() });
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades2_NonmatchingSectionID()
            {
                var response = await sectionsController.PutCollectionOfStudentGrades2Async("123", new SectionGrades2() { SectionId = "321", StudentGrades = GetStudentGrade2() });
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades2_Exception()
            {

                sectionCoordinationServiceMock.Setup(s => s.ImportGrades2Async(It.IsAny<Ellucian.Colleague.Dtos.Student.SectionGrades2>())).Throws<Exception>();

                var response = await sectionsController.PutCollectionOfStudentGrades2Async("123", new SectionGrades2() { SectionId = "321", StudentGrades = GetStudentGrade2() });
            }

            [TestMethod]
            public async Task SectionsController_PutCollectionOfStudentGrades2()
            {
                string sectionId = "111";
                var response = await sectionsController.PutCollectionOfStudentGrades2Async(sectionId, new SectionGrades2() { SectionId = sectionId, StudentGrades = GetStudentGrade2() });
                var expected = GetSectionGradeResponse();

                Assert.AreEqual(expected[0].StudentId, response.First().StudentId);
                Assert.AreEqual(expected[0].Status, response.First().Status);
                Assert.AreEqual(expected[0].Errors.Count(), response.First().Errors.Count());
                Assert.AreEqual(expected[0].Errors[0].Message, response.First().Errors[0].Message);
                Assert.AreEqual(expected[0].Errors[0].Property, response.First().Errors[0].Property);
            }

            private List<Dtos.Student.SectionGradeResponse> GetSectionGradeResponse()
            {
                List<SectionGradeResponse> returnList = new List<SectionGradeResponse>();

                var response = new Dtos.Student.SectionGradeResponse();
                response.StudentId = "101";
                response.Status = "status";

                var error = new Dtos.Student.SectionGradeResponseError();
                error.Message = "message";
                error.Property = "property";
                response.Errors.Add(error);

                returnList.Add(response);
                return returnList;
            }

            private List<Dtos.Student.StudentGrade2> GetStudentGrade2()
            {
                List<Dtos.Student.StudentGrade2> listOfOneGrade = new List<StudentGrade2>();
                listOfOneGrade.Add(new Dtos.Student.StudentGrade2() { StudentId = "1", EffectiveStartDate = DateTime.Now, EffectiveEndDate = DateTime.Now.AddDays(30) });
                return listOfOneGrade;
            }
        }

        [TestClass]
        public class SectionsControllerTestsPutGrades3
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion
            private IAdapterRegistry adapterRegistry;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;
                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;
                logger = new Mock<ILogger>().Object;

                sectionCoordinationServiceMock.Setup(s => s.ImportGrades3Async(It.IsAny<Ellucian.Colleague.Dtos.Student.SectionGrades3>()))
                    .Returns(Task.FromResult<IEnumerable<SectionGradeResponse>>(GetSectionGradeResponse()));

                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                adapterRegistry = null;
                sectionsController = null;
                sectionRepository = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades3_EmptySectionGrades()
            {
                try
                {
                    var response = await sectionsController.PutCollectionOfStudentGrades3Async("123", null);
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.BadRequest);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades3_ModelState()
            {
                try
                {
                    sectionsController.ModelState.AddModelError("key", "error message");
                    var response = await sectionsController.PutCollectionOfStudentGrades3Async("123", null);
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.BadRequest);
                    throw;
                }
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades3_EmptySectionID()
            {
                try
                {
                    var response = await sectionsController.PutCollectionOfStudentGrades3Async("", new SectionGrades3() { SectionId = "", StudentGrades = GetStudentGrade2() });
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.BadRequest);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades3_NonmatchingSectionID()
            {
                try
                {
                    var response = await sectionsController.PutCollectionOfStudentGrades3Async("123", new SectionGrades3() { SectionId = "321", StudentGrades = GetStudentGrade2() });
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.BadRequest);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades3_Exception()
            {

                try
                {
                    sectionCoordinationServiceMock.Setup(s => s.ImportGrades3Async(It.IsAny<Ellucian.Colleague.Dtos.Student.SectionGrades3>())).Throws<Exception>();
                    var response = await sectionsController.PutCollectionOfStudentGrades3Async("123", new SectionGrades3() { SectionId = "321", StudentGrades = GetStudentGrade2() });
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.BadRequest);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades3_PermissionsException()
            {
                string sectionId = "100";
                sectionCoordinationServiceMock.Setup(s => s.ImportGrades3Async(It.Is<Ellucian.Colleague.Dtos.Student.SectionGrades3>(x => x.SectionId == sectionId)))
                    .Throws(new Ellucian.Web.Security.PermissionsException());

                try
                {
                    var response = await sectionsController.PutCollectionOfStudentGrades3Async(sectionId, new SectionGrades3() { SectionId = sectionId, StudentGrades = GetStudentGrade2() });
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.Forbidden);
                    throw;
                }
            }

            [TestMethod]
            public async Task SectionsController_PutCollectionOfStudentGrades3()
            {
                string sectionId = "111";
                var response = await sectionsController.PutCollectionOfStudentGrades3Async(sectionId, new SectionGrades3() { SectionId = sectionId, StudentGrades = GetStudentGrade2() });
                var expected = GetSectionGradeResponse();

                Assert.AreEqual(expected[0].StudentId, response.First().StudentId);
                Assert.AreEqual(expected[0].Status, response.First().Status);
                Assert.AreEqual(expected[0].Errors.Count(), response.First().Errors.Count());
                Assert.AreEqual(expected[0].Errors[0].Message, response.First().Errors[0].Message);
                Assert.AreEqual(expected[0].Errors[0].Property, response.First().Errors[0].Property);
            }


            private List<Dtos.Student.SectionGradeResponse> GetSectionGradeResponse()
            {
                List<SectionGradeResponse> returnList = new List<SectionGradeResponse>();

                var response = new Dtos.Student.SectionGradeResponse();
                response.StudentId = "101";
                response.Status = "status";

                var error = new Dtos.Student.SectionGradeResponseError();
                error.Message = "message";
                error.Property = "property";
                response.Errors.Add(error);

                returnList.Add(response);
                return returnList;
            }

            private List<Dtos.Student.StudentGrade2> GetStudentGrade2()
            {
                List<Dtos.Student.StudentGrade2> listOfOneGrade = new List<StudentGrade2>();
                listOfOneGrade.Add(new Dtos.Student.StudentGrade2() { StudentId = "1", EffectiveStartDate = DateTime.Now, EffectiveEndDate = DateTime.Now.AddDays(30) });
                return listOfOneGrade;
            }
        }

        [TestClass]
        public class SectionsControllerTestsPutGrades4
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion
            private IAdapterRegistry adapterRegistry;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;
                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;
                logger = new Mock<ILogger>().Object;

                sectionCoordinationServiceMock.Setup(s => s.ImportGrades4Async(It.IsAny<Ellucian.Colleague.Dtos.Student.SectionGrades3>()))
                    .Returns(Task.FromResult<IEnumerable<SectionGradeResponse>>(GetSectionGradeResponse()));

                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                adapterRegistry = null;
                sectionsController = null;
                sectionRepository = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades4_EmptySectionGrades()
            {
                try
                {
                    var response = await sectionsController.PutCollectionOfStudentGrades4Async("123", null);
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.BadRequest);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades4_ModelState()
            {
                try
                {
                    sectionsController.ModelState.AddModelError("key", "error message");
                    var response = await sectionsController.PutCollectionOfStudentGrades4Async("123", null);
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.BadRequest);
                    throw;
                }
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades4_EmptySectionID()
            {
                try
                {
                    var response = await sectionsController.PutCollectionOfStudentGrades4Async("", new SectionGrades3() { SectionId = "", StudentGrades = GetStudentGrade2() });
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.BadRequest);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades4_NonmatchingSectionID()
            {
                try
                {
                    var response = await sectionsController.PutCollectionOfStudentGrades4Async("123", new SectionGrades3() { SectionId = "321", StudentGrades = GetStudentGrade2() });
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.BadRequest);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades4_Exception()
            {

                try
                {
                    sectionCoordinationServiceMock.Setup(s => s.ImportGrades4Async(It.IsAny<Ellucian.Colleague.Dtos.Student.SectionGrades3>())).Throws<Exception>();
                    var response = await sectionsController.PutCollectionOfStudentGrades4Async("123", new SectionGrades3() { SectionId = "321", StudentGrades = GetStudentGrade2() });
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.BadRequest);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutCollectionOfStudentGrades4_PermissionsException()
            {
                string sectionId = "100";
                sectionCoordinationServiceMock.Setup(s => s.ImportGrades4Async(It.Is<Ellucian.Colleague.Dtos.Student.SectionGrades3>(x => x.SectionId == sectionId)))
                    .Throws(new Ellucian.Web.Security.PermissionsException());

                try
                {
                    var response = await sectionsController.PutCollectionOfStudentGrades4Async(sectionId, new SectionGrades3() { SectionId = sectionId, StudentGrades = GetStudentGrade2() });
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.Forbidden);
                    throw;
                }
            }

            [TestMethod]
            public async Task SectionsController_PutCollectionOfStudentGrades4()
            {
                string sectionId = "111";
                var response = await sectionsController.PutCollectionOfStudentGrades4Async(sectionId, new SectionGrades3() { SectionId = sectionId, StudentGrades = GetStudentGrade2() });
                var expected = GetSectionGradeResponse();

                Assert.AreEqual(expected[0].StudentId, response.First().StudentId);
                Assert.AreEqual(expected[0].Status, response.First().Status);
                Assert.AreEqual(expected[0].Errors.Count(), response.First().Errors.Count());
                Assert.AreEqual(expected[0].Errors[0].Message, response.First().Errors[0].Message);
                Assert.AreEqual(expected[0].Errors[0].Property, response.First().Errors[0].Property);
            }


            private List<Dtos.Student.SectionGradeResponse> GetSectionGradeResponse()
            {
                List<SectionGradeResponse> returnList = new List<SectionGradeResponse>();

                var response = new Dtos.Student.SectionGradeResponse();
                response.StudentId = "101";
                response.Status = "status";

                var error = new Dtos.Student.SectionGradeResponseError();
                error.Message = "message";
                error.Property = "property";
                response.Errors.Add(error);

                returnList.Add(response);
                return returnList;
            }

            private List<Dtos.Student.StudentGrade2> GetStudentGrade2()
            {
                List<Dtos.Student.StudentGrade2> listOfOneGrade = new List<StudentGrade2>();
                listOfOneGrade.Add(new Dtos.Student.StudentGrade2() { StudentId = "1", EffectiveStartDate = DateTime.Now, EffectiveEndDate = DateTime.Now.AddDays(30) });
                return listOfOneGrade;
            }
        }

        [TestClass]
        public class SectionsControllerTestsPutIlpGrades1
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion
            private IAdapterRegistry adapterRegistry;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;
                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;
                logger = new Mock<ILogger>().Object;

                sectionCoordinationServiceMock.Setup(s => s.ImportIlpGrades1Async(It.IsAny<Ellucian.Colleague.Dtos.Student.SectionGrades3>()))
                    .Returns(Task.FromResult<IEnumerable<SectionGradeResponse>>(GetSectionGradeResponse()));

                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                adapterRegistry = null;
                sectionsController = null;
                sectionRepository = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutIlpCollectionOfStudentGrades1_EmptySectionGrades()
            {
                try
                {
                    var response = await sectionsController.PutIlpCollectionOfStudentGrades1Async("123", null);
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.BadRequest);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutIlpCollectionOfStudentGrades1_ModelState()
            {
                try
                {
                    sectionsController.ModelState.AddModelError("key", "error message");
                    var response = await sectionsController.PutIlpCollectionOfStudentGrades1Async("123", null);
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.BadRequest);
                    throw;
                }
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutIlpCollectionOfStudentGrades1_EmptySectionID()
            {
                try
                {
                    var response = await sectionsController.PutIlpCollectionOfStudentGrades1Async("", new SectionGrades3() { SectionId = "", StudentGrades = GetStudentGrade2() });
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.BadRequest);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutIlpCollectionOfStudentGrades1_NonmatchingSectionID()
            {
                try
                {
                    var response = await sectionsController.PutIlpCollectionOfStudentGrades1Async("123", new SectionGrades3() { SectionId = "321", StudentGrades = GetStudentGrade2() });
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.BadRequest);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutIlpCollectionOfStudentGrades1_Exception()
            {

                try
                {
                    sectionCoordinationServiceMock.Setup(s => s.ImportIlpGrades1Async(It.IsAny<Ellucian.Colleague.Dtos.Student.SectionGrades3>())).Throws<Exception>();
                    var response = await sectionsController.PutIlpCollectionOfStudentGrades1Async("123", new SectionGrades3() { SectionId = "321", StudentGrades = GetStudentGrade2() });
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.BadRequest);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutIlpCollectionOfStudentGrades1_PermissionsException()
            {
                string sectionId = "100";
                sectionCoordinationServiceMock.Setup(s => s.ImportIlpGrades1Async(It.Is<Ellucian.Colleague.Dtos.Student.SectionGrades3>(x => x.SectionId == sectionId)))
                    .Throws(new Ellucian.Web.Security.PermissionsException());

                try
                {
                    var response = await sectionsController.PutIlpCollectionOfStudentGrades1Async(sectionId, new SectionGrades3() { SectionId = sectionId, StudentGrades = GetStudentGrade2() });
                }
                catch (HttpResponseException hte)
                {
                    Assert.AreEqual(hte.Response.StatusCode, HttpStatusCode.Forbidden);
                    throw;
                }
            }

            [TestMethod]
            public async Task SectionsController_PutIlpCollectionOfStudentGrades1()
            {
                string sectionId = "111";
                var response = await sectionsController.PutIlpCollectionOfStudentGrades1Async(sectionId, new SectionGrades3() { SectionId = sectionId, StudentGrades = GetStudentGrade2() });
                var expected = GetSectionGradeResponse();

                Assert.AreEqual(expected[0].StudentId, response.First().StudentId);
                Assert.AreEqual(expected[0].Status, response.First().Status);
                Assert.AreEqual(expected[0].Errors.Count(), response.First().Errors.Count());
                Assert.AreEqual(expected[0].Errors[0].Message, response.First().Errors[0].Message);
                Assert.AreEqual(expected[0].Errors[0].Property, response.First().Errors[0].Property);
            }


            private List<Dtos.Student.SectionGradeResponse> GetSectionGradeResponse()
            {
                List<SectionGradeResponse> returnList = new List<SectionGradeResponse>();

                var response = new Dtos.Student.SectionGradeResponse();
                response.StudentId = "101";
                response.Status = "status";

                var error = new Dtos.Student.SectionGradeResponseError();
                error.Message = "message";
                error.Property = "property";
                response.Errors.Add(error);

                returnList.Add(response);
                return returnList;
            }

            private List<Dtos.Student.StudentGrade2> GetStudentGrade2()
            {
                List<Dtos.Student.StudentGrade2> listOfOneGrade = new List<StudentGrade2>();
                listOfOneGrade.Add(new Dtos.Student.StudentGrade2() { StudentId = "1", EffectiveStartDate = DateTime.Now, EffectiveEndDate = DateTime.Now.AddDays(30) });
                return listOfOneGrade;
            }
        }


        [TestClass]
        public class SectionsControllerTestsQuerySectionRegistrationDates
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private SectionsController sectionsController;
            private Mock<IRegistrationGroupService> registrationGroupServiceMock;
            private IRegistrationGroupService registrationGroupService;
            private ILogger logger;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                registrationGroupServiceMock = new Mock<IRegistrationGroupService>();
                registrationGroupService = registrationGroupServiceMock.Object;
                logger = new Mock<ILogger>().Object;
                sectionsController = new SectionsController(adapterRegistry, null, null, null, registrationGroupService, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                adapterRegistry = null;
                sectionsController = null;
                registrationGroupService = null;
            }

            [TestMethod]
            public async Task SectionsController_SuccessfulQuery()
            {
                registrationGroupServiceMock.Setup(r => r.GetSectionRegistrationDatesAsync(It.IsAny<List<string>>())).Returns(Task.FromResult<IEnumerable<SectionRegistrationDate>>(GetSectionRegistrationDatesResponse()));
                var criteria = new Dtos.Student.SectionDateQueryCriteria();
                criteria.SectionIds = new List<string>() { "Section1", "Section2" };
                var response = await sectionsController.QuerySectionRegistrationDatesAsync(criteria);
                var expected = GetSectionRegistrationDatesResponse();

                Assert.AreEqual(expected.Count(), response.Count());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_AnyException_ReturnsHttpResponseException_BadRequest()
            {
                try
                {
                    registrationGroupServiceMock.Setup(r => r.GetSectionRegistrationDatesAsync(It.IsAny<List<string>>())).Throws(new Exception());
                    var criteria = new Dtos.Student.SectionDateQueryCriteria();
                    criteria.SectionIds = new List<string>() { "Section1", "Section2" };
                    var response = await sectionsController.QuerySectionRegistrationDatesAsync(criteria);
                }
                catch (HttpResponseException ex)
                {
                    Assert.AreEqual(System.Net.HttpStatusCode.BadRequest, ex.Response.StatusCode);
                    throw ex;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_NoSectionIdsProvided_ReturnsHttpResponseException_BadRequest()
            {
                try
                {
                    registrationGroupServiceMock.Setup(r => r.GetSectionRegistrationDatesAsync(It.IsAny<List<string>>())).Returns(Task.FromResult<IEnumerable<SectionRegistrationDate>>(GetSectionRegistrationDatesResponse()));
                    var criteria = new Dtos.Student.SectionDateQueryCriteria();
                    var response = await sectionsController.QuerySectionRegistrationDatesAsync(criteria);
                }
                catch (HttpResponseException ex)
                {
                    Assert.AreEqual(System.Net.HttpStatusCode.BadRequest, ex.Response.StatusCode);
                    throw ex;
                }
            }

            private List<Dtos.Student.SectionRegistrationDate> GetSectionRegistrationDatesResponse()
            {
                List<Dtos.Student.SectionRegistrationDate> returnList = new List<Dtos.Student.SectionRegistrationDate>();
                var item1 = new Dtos.Student.SectionRegistrationDate() { SectionId = "section1", RegistrationStartDate = DateTime.Today, RegistrationEndDate = DateTime.Today.AddDays(10) };
                returnList.Add(item1);
                var item2 = new Dtos.Student.SectionRegistrationDate() { SectionId = "section2", RegistrationStartDate = DateTime.Today, RegistrationEndDate = DateTime.Today.AddDays(10) };
                returnList.Add(item2);
                return returnList;
            }


        }

        [TestClass]
        public class SectionsControllerTests_PostSectionAsync
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;
            private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>();

            private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

            private List<Dtos.Section> allSectionDtos;
            private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
            private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
            private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
            private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
                logger = new Mock<ILogger>().Object;

                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;

                studentRepoMock = new Mock<IStudentRepository>();
                studentRepository = studentRepoMock.Object;

                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;

                allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
                allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
                if (allCourseLevels != null)
                    levels.Add(allCourseLevels[0].Code);

                adapterRegistry = adapterRegistryMock.Object;

                List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
                var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

                firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                firstSection.TermId = "2012/FA";
                firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
                sectionEntityList.Add(firstSection);

                secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
                sectionEntityList.Add(secondSection);

                thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
                thirdSection.EndDate = new DateTime(2011, 12, 21);
                sectionEntityList.Add(thirdSection);

                IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
                IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());

                // Mock section repo GetCachedSections
                bool bestFit = false;
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // Mock section repo GetNonCachedSections
                sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // mock DTO adapters
                var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

                academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
                allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

                allSectionDtos = new List<Dtos.Section>();
                foreach (var sectionEntity in sectionEntityList)
                {
                    //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                    var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section>(adapterRegistry, logger);
                    Ellucian.Colleague.Dtos.Section target = sectionDtoAdapter.MapToType(sectionEntity);
                    target.Guid = sectionEntity.Guid;
                    Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                    target.AcademicLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject() { Guid = academicLevel.Guid } };

                    Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                    target.GradeSchemes = new List<Dtos.GuidObject>() { new Dtos.GuidObject() { Guid = gradeScheme.Guid } };

                    var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                    if (course != null) target.Course = new Dtos.GuidObject { Guid = course.Guid };

                    if (sectionEntity.CourseLevelCodes != null)
                    {
                        var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                        target.CourseLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject() { Guid = courseLevel.Guid } };
                    }
                    allSectionDtos.Add(target);
                }

                // mock controller
                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionsController = null;
                sectionRepository = null;
                studentRepository = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostSectionAsync_Exception()
            {
                sectionCoordinationServiceMock
                    .Setup(svc => svc.PostSectionAsync(It.IsAny<Dtos.Section>()))
                    .Throws<Exception>();
                await sectionsController.PostSectionAsync(It.IsAny<Dtos.Section>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostSectionAsync_ConfigurationException()
            {
                sectionCoordinationServiceMock
                   .Setup(svc => svc.PostSectionAsync(It.IsAny<Dtos.Section>()))
                   .Throws<ConfigurationException>();
                await sectionsController.PostSectionAsync(It.IsAny<Dtos.Section>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostSectionAsync_PermissionException()
            {
                sectionCoordinationServiceMock
                   .Setup(svc => svc.PostSectionAsync(It.IsAny<Dtos.Section>()))
                   .Throws<PermissionsException>();
                await sectionsController.PostSectionAsync(It.IsAny<Dtos.Section>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostSectionAsync_ArgNullException()
            {
                sectionCoordinationServiceMock
                  .Setup(svc => svc.PostSectionAsync(null))
                  .Throws<ArgumentNullException>();
                await sectionsController.PostSectionAsync(null);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostSectionAsync_RepoException()
            {
                sectionCoordinationServiceMock
                   .Setup(svc => svc.PostSectionAsync(It.IsAny<Dtos.Section>()))
                   .Throws<RepositoryException>();
                await sectionsController.PostSectionAsync(It.IsAny<Dtos.Section>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostSectionAsync_IntegrationApiException()
            {
                sectionCoordinationServiceMock
                   .Setup(svc => svc.PostSectionAsync(It.IsAny<Dtos.Section>()))
                   .Throws<IntegrationApiException>();
                await sectionsController.PostSectionAsync(It.IsAny<Dtos.Section>());
            }

            [TestMethod]
            public async Task SectionsController_PostSectionAsync()
            {
                var section = allSectionDtos.FirstOrDefault();

                sectionCoordinationServiceMock.Setup(svc => svc.PostSectionAsync(section)).ReturnsAsync(section);
                var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Guid);

                var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
                Ellucian.Colleague.Domain.Student.Entities.Course course = null;
                if (section.Course != null)
                    course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Guid);

                var sectionByGuid = await sectionsController.PostSectionAsync(section);
                Assert.AreEqual(section.Guid, sectionByGuid.Guid);
                Assert.AreEqual(section.Title, sectionByGuid.Title);
                Assert.AreEqual(section.Description, sectionByGuid.Description);
                Assert.AreEqual(academicLevel.Guid, sectionByGuid.AcademicLevels.FirstOrDefault().Guid);
                Assert.AreEqual(gradeScheme.Guid, sectionByGuid.GradeSchemes.FirstOrDefault().Guid);
                Assert.AreEqual(section.StartDate, sectionByGuid.StartDate);
                Assert.AreEqual(section.EndDate, sectionByGuid.EndDate);
                if (course != null && sectionByGuid.Course != null)
                    Assert.AreEqual(course.Guid, sectionByGuid.Course.Guid);
                Assert.AreEqual(section.Number, sectionByGuid.Number);
                Assert.AreEqual(section.MaximumEnrollment, sectionByGuid.MaximumEnrollment);

                var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Guid);
                if (courseLevel != null)
                    Assert.AreEqual(courseLevel.Guid, sectionByGuid.CourseLevels[0].Guid);

            }
        }

        [TestClass]
        public class SectionsControllerTests_PutSectionAsync
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;
            private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>();

            private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

            private string firstSectionGuid = "eab43b50-ce12-4e7f-8947-2d63661ab7ac";
            private string secondSectionGuid = "51adca90-4839-442b-b22a-12c8009b1186";
            private string thirdSectionGuid = "cd74617e-d304-4c6d-8598-6622ed15192a";

            private List<Dtos.Section> allSectionDtos;
            private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
            private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
            private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
            private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;


            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
                logger = new Mock<ILogger>().Object;

                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;

                studentRepoMock = new Mock<IStudentRepository>();
                studentRepository = studentRepoMock.Object;

                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;

                allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
                allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
                if (allCourseLevels != null)
                    levels.Add(allCourseLevels[0].Code);

                adapterRegistry = adapterRegistryMock.Object;

                List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
                var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

                firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                firstSection.TermId = "2012/FA";
                firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
                firstSection.Guid = firstSectionGuid;
                sectionEntityList.Add(firstSection);

                secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
                secondSection.Guid = secondSectionGuid;
                sectionEntityList.Add(secondSection);

                thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
                thirdSection.EndDate = new DateTime(2011, 12, 21);
                thirdSection.Guid = thirdSectionGuid;
                sectionEntityList.Add(thirdSection);

                IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
                IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());

                // Mock section repo GetCachedSections
                bool bestFit = false;
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // Mock section repo GetNonCachedSections
                sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // mock DTO adapters
                var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

                academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
                allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

                allSectionDtos = new List<Dtos.Section>();
                foreach (var sectionEntity in sectionEntityList)
                {
                    //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                    var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section>(adapterRegistry, logger);
                    Ellucian.Colleague.Dtos.Section target = sectionDtoAdapter.MapToType(sectionEntity);
                    target.Guid = sectionEntity.Guid;
                    Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                    target.AcademicLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject() { Guid = academicLevel.Guid } };

                    Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                    target.GradeSchemes = new List<Dtos.GuidObject>() { new Dtos.GuidObject() { Guid = gradeScheme.Guid } };

                    var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                    if (course != null) target.Course = new Dtos.GuidObject { Guid = course.Guid };

                    if (sectionEntity.CourseLevelCodes != null)
                    {
                        var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                        target.CourseLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject() { Guid = courseLevel.Guid } };
                    }
                    allSectionDtos.Add(target);
                }

                // mock controller
                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionsController = null;
                sectionRepository = null;
                studentRepository = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutSectionAsync_Exception()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Guid == firstSectionGuid);

                sectionCoordinationServiceMock
                    .Setup(svc => svc.PutSectionAsync(It.IsAny<Dtos.Section>()))
                    .Throws<Exception>();
                await sectionsController.PutSectionAsync(section.Guid, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutSectionAsync_ArgumentException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Guid == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSectionAsync(It.IsAny<Dtos.Section>()))
                   .Throws<ArgumentException>();
                await sectionsController.PutSectionAsync(section.Guid, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutSectionAsync_ConfigurationException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Guid == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSectionAsync(It.IsAny<Dtos.Section>()))
                   .Throws<ConfigurationException>();
                await sectionsController.PutSectionAsync(section.Guid, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutSectionAsync_IntegrationApiException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Guid == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSectionAsync(It.IsAny<Dtos.Section>()))
                   .Throws<IntegrationApiException>();
                await sectionsController.PutSectionAsync(section.Guid, section);
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutSectionAsync_RepositoryException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Guid == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSectionAsync(It.IsAny<Dtos.Section>()))
                   .Throws<RepositoryException>();
                await sectionsController.PutSectionAsync(section.Guid, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutSectionAsync_PermissionException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Guid == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSectionAsync(It.IsAny<Dtos.Section>()))
                   .Throws<PermissionsException>();
                await sectionsController.PutSectionAsync(section.Guid, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutSectionAsync_NullGuidException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Guid == firstSectionGuid);

                await sectionsController.PutSectionAsync(null, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutSectionAsync_NullSectionException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Guid == firstSectionGuid);

                await sectionsController.PutSectionAsync(section.Guid, null);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutSectionAsync_InvalidException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Guid == firstSectionGuid);

                await sectionsController.PutSectionAsync(secondSectionGuid, section);
            }


            [TestMethod]
            public async Task SectionsController_PutSectionAsync()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Guid == firstSectionGuid);

                sectionCoordinationServiceMock.Setup(svc => svc.PutSectionAsync(section)).ReturnsAsync(section);
                var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Guid);

                var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
                Ellucian.Colleague.Domain.Student.Entities.Course course = null;
                if (section.Course != null)
                    course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Guid);

                var sectionByGuid = await sectionsController.PutSectionAsync(firstSectionGuid, section);
                Assert.AreEqual(section.Guid, sectionByGuid.Guid);
                Assert.AreEqual(section.Title, sectionByGuid.Title);
                Assert.AreEqual(section.Description, sectionByGuid.Description);
                Assert.AreEqual(academicLevel.Guid, sectionByGuid.AcademicLevels.FirstOrDefault().Guid);
                Assert.AreEqual(gradeScheme.Guid, sectionByGuid.GradeSchemes.FirstOrDefault().Guid);
                Assert.AreEqual(section.StartDate, sectionByGuid.StartDate);
                Assert.AreEqual(section.EndDate, sectionByGuid.EndDate);
                if (course != null && sectionByGuid.Course != null)
                    Assert.AreEqual(course.Guid, sectionByGuid.Course.Guid);
                Assert.AreEqual(section.Number, sectionByGuid.Number);
                Assert.AreEqual(section.MaximumEnrollment, sectionByGuid.MaximumEnrollment);

                var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Guid);
                if (courseLevel != null)
                    Assert.AreEqual(courseLevel.Guid, sectionByGuid.CourseLevels[0].Guid);

            }
        }

        [TestClass]
        public class SectionsControllerTests_PostHedmSectionAsync
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;
            private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>();

            private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

            private string firstSectionGuid = "eab43b50-ce12-4e7f-8947-2d63661ab7ac";
            private string secondSectionGuid = "51adca90-4839-442b-b22a-12c8009b1186";
            private string thirdSectionGuid = "cd74617e-d304-4c6d-8598-6622ed15192a";


            private List<Dtos.Section2> allSectionDtos;
            private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
            private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
            private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
            private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
                logger = new Mock<ILogger>().Object;

                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;

                studentRepoMock = new Mock<IStudentRepository>();
                studentRepository = studentRepoMock.Object;

                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;

                allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
                allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
                if (allCourseLevels != null)
                    levels.Add(allCourseLevels[0].Code);

                adapterRegistry = adapterRegistryMock.Object;

                List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
                var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

                firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                firstSection.TermId = "2012/FA";
                firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
                firstSection.Guid = firstSectionGuid;
                sectionEntityList.Add(firstSection);

                secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
                secondSection.Guid = secondSectionGuid;
                sectionEntityList.Add(secondSection);

                thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
                thirdSection.EndDate = new DateTime(2011, 12, 21);
                thirdSection.Guid = thirdSectionGuid;
                sectionEntityList.Add(thirdSection);

                IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
                IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());

                // Mock section repo GetCachedSections
                bool bestFit = false;
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // Mock section repo GetNonCachedSections
                sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // mock DTO adapters
                var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

                academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
                allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

                allSectionDtos = new List<Dtos.Section2>();
                foreach (var sectionEntity in sectionEntityList)
                {
                    //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                    var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section2>(adapterRegistry, logger);
                    Ellucian.Colleague.Dtos.Section2 target = sectionDtoAdapter.MapToType(sectionEntity);
                    target.Id = sectionEntity.Guid;
                    Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                    target.AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = academicLevel.Guid } };

                    Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                    target.GradeSchemes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = gradeScheme.Guid } };

                    var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                    if (course != null) target.Course = new Dtos.GuidObject2 { Id = course.Guid };

                    if (sectionEntity.CourseLevelCodes != null)
                    {
                        var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                        target.CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = courseLevel.Guid } };
                    }
                    allSectionDtos.Add(target);
                }

                // mock controller
                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionsController = null;
                sectionRepository = null;
                studentRepository = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostHedmSectionAsync_Exception()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
                
                sectionCoordinationServiceMock
                    .Setup(svc => svc.PostSection2Async(It.IsAny<Dtos.Section2>()))
                    .Throws<Exception>();
                await sectionsController.PostHedmSectionAsync(It.IsAny<Dtos.Section2>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostHedmSectionAsync_ConfigurationException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
                
                sectionCoordinationServiceMock
                   .Setup(svc => svc.PostSection2Async(It.IsAny<Dtos.Section2>()))
                   .Throws<ConfigurationException>();
                await sectionsController.PostHedmSectionAsync(It.IsAny<Dtos.Section2>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostHedmSectionAsync_PermissionException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
                
                sectionCoordinationServiceMock
                   .Setup(svc => svc.PostSection2Async(It.IsAny<Dtos.Section2>()))
                   .Throws<PermissionsException>();
                await sectionsController.PostHedmSectionAsync(It.IsAny<Dtos.Section2>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostHedmSectionAsync_ArgNullException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
                
                sectionCoordinationServiceMock
                  .Setup(svc => svc.PostSection2Async(null))
                  .Throws<ArgumentNullException>();
                await sectionsController.PostHedmSectionAsync(null);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostHedmSectionAsync_RepoException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
                
                sectionCoordinationServiceMock
                   .Setup(svc => svc.PostSection2Async(It.IsAny<Dtos.Section2>()))
                   .Throws<RepositoryException>();
                await sectionsController.PostHedmSectionAsync(It.IsAny<Dtos.Section2>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostHedmSectionAsync_IntegrationApiException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
                
                sectionCoordinationServiceMock
                   .Setup(svc => svc.PostSection2Async(It.IsAny<Dtos.Section2>()))
                   .Throws<IntegrationApiException>();
                await sectionsController.PostHedmSectionAsync(It.IsAny<Dtos.Section2>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostHedmSectionAsync_ID_Null()
            {
                sectionCoordinationServiceMock
                   .Setup(svc => svc.PostSection2Async(It.IsAny<Dtos.Section2>()))
                   .Throws<IntegrationApiException>();
                await sectionsController.PostHedmSectionAsync(new Dtos.Section2() { Id = string.Empty });
            }

            [TestMethod]
            public async Task SectionsController_PostHedmSectionAsync()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock.Setup(svc => svc.PostSection2Async(section)).ReturnsAsync(section);
                var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

                var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
                Ellucian.Colleague.Domain.Student.Entities.Course course = null;
                if (section.Course != null)
                    course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

                var sectionByGuid = await sectionsController.PostHedmSectionAsync(section);
                Assert.AreEqual(section.Id, sectionByGuid.Id);
                Assert.AreEqual(section.Title, sectionByGuid.Title);
                Assert.AreEqual(section.Description, sectionByGuid.Description);
                Assert.AreEqual(academicLevel.Guid, sectionByGuid.AcademicLevels.FirstOrDefault().Id);
                Assert.AreEqual(gradeScheme.Guid, sectionByGuid.GradeSchemes.FirstOrDefault().Id);
                Assert.AreEqual(section.StartOn, sectionByGuid.StartOn);
                Assert.AreEqual(section.EndOn, sectionByGuid.EndOn);
                if (course != null && sectionByGuid.Course != null)
                    Assert.AreEqual(course.Guid, sectionByGuid.Course.Id);
                Assert.AreEqual(section.Number, sectionByGuid.Number);
                Assert.AreEqual(section.MaximumEnrollment, sectionByGuid.MaximumEnrollment);

                var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
                if (courseLevel != null)
                    Assert.AreEqual(courseLevel.Guid, sectionByGuid.CourseLevels[0].Id);

            }
        }

        [TestClass]
        public class SectionsControllerTests_PutHedmSectionAsync
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;
            private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>();

            private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

            private string firstSectionGuid = "eab43b50-ce12-4e7f-8947-2d63661ab7ac";
            private string secondSectionGuid = "51adca90-4839-442b-b22a-12c8009b1186";
            private string thirdSectionGuid = "cd74617e-d304-4c6d-8598-6622ed15192a";

            private List<Dtos.Section2> allSectionDtos;
            private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
            private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
            private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
            private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;


            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
                logger = new Mock<ILogger>().Object;

                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;

                studentRepoMock = new Mock<IStudentRepository>();
                studentRepository = studentRepoMock.Object;

                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;

                allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
                allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
                if (allCourseLevels != null)
                    levels.Add(allCourseLevels[0].Code);

                adapterRegistry = adapterRegistryMock.Object;

                List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
                var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

                firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                firstSection.TermId = "2012/FA";
                firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
                firstSection.Guid = firstSectionGuid;
                sectionEntityList.Add(firstSection);

                secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
                secondSection.Guid = secondSectionGuid;
                sectionEntityList.Add(secondSection);

                thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
                thirdSection.EndDate = new DateTime(2011, 12, 21);
                thirdSection.Guid = thirdSectionGuid;
                sectionEntityList.Add(thirdSection);

                IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
                IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());

                // Mock section repo GetCachedSections
                bool bestFit = false;
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // Mock section repo GetNonCachedSections
                sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // mock DTO adapters
                var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

                academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
                allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

                allSectionDtos = new List<Dtos.Section2>();
                foreach (var sectionEntity in sectionEntityList)
                {
                    //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                    var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section2>(adapterRegistry, logger);
                    Ellucian.Colleague.Dtos.Section2 target = sectionDtoAdapter.MapToType(sectionEntity);
                    target.Id = sectionEntity.Guid;
                    Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                    target.AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = academicLevel.Guid } };

                    Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                    target.GradeSchemes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = gradeScheme.Guid } };

                    var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                    if (course != null) target.Course = new Dtos.GuidObject2 { Id = course.Guid };

                    if (sectionEntity.CourseLevelCodes != null)
                    {
                        var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                        target.CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = courseLevel.Guid } };
                    }
                    allSectionDtos.Add(target);
                }

                // mock controller
                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionsController = null;
                sectionRepository = null;
                studentRepository = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_Exception()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
                
                sectionCoordinationServiceMock
                    .Setup(svc => svc.PutSection2Async(It.IsAny<Dtos.Section2>()))
                    .Throws<Exception>();
                await sectionsController.PutHedmSectionAsync(section.Id, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_ConfigurationException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
                
                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSection2Async(It.IsAny<Dtos.Section2>()))
                   .Throws<ConfigurationException>();
                await sectionsController.PutHedmSectionAsync(section.Id, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_PermissionException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
                
                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSection2Async(It.IsAny<Dtos.Section2>()))
                   .Throws<PermissionsException>();
                await sectionsController.PutHedmSectionAsync(section.Id, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_RepoException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSection2Async(It.IsAny<Dtos.Section2>()))
                   .Throws<RepositoryException>();
                await sectionsController.PutHedmSectionAsync(section.Id, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_ArgException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSection2Async(It.IsAny<Dtos.Section2>()))
                   .Throws<ArgumentException>();
                await sectionsController.PutHedmSectionAsync(section.Id, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_NilGUID_ArgException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSection2Async(It.IsAny<Dtos.Section2>()))
                   .Throws<ArgumentException>();
                await sectionsController.PutHedmSectionAsync(Guid.Empty.ToString(), section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_SectionIdNull_ArgException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSection2Async(It.IsAny<Dtos.Section2>()))
                   .Throws<ArgumentException>();
                await sectionsController.PutHedmSectionAsync("1234", new Dtos.Section2() { Id = "" });
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_IntegrationApiException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSection2Async(It.IsAny<Dtos.Section2>()))
                   .Throws<IntegrationApiException>();
                await sectionsController.PutHedmSectionAsync(section.Id, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_NullIDException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                await sectionsController.PutHedmSectionAsync(null, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_NullSectionException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                await sectionsController.PutHedmSectionAsync(firstSectionGuid, null);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_InvalidException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                await sectionsController.PutHedmSectionAsync(secondSectionGuid, section);
            }

            [TestMethod]
            public async Task SectionsController_PutHedmSectionAsync()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock.Setup(svc => svc.PutSection2Async(section)).ReturnsAsync(section);
                var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

                var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
                Ellucian.Colleague.Domain.Student.Entities.Course course = null;
                if (section.Course != null)
                    course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

                var sectionByGuid = await sectionsController.PutHedmSectionAsync(firstSectionGuid, section);
                Assert.AreEqual(section.Id, sectionByGuid.Id);
                Assert.AreEqual(section.Title, sectionByGuid.Title);
                Assert.AreEqual(section.Description, sectionByGuid.Description);
                Assert.AreEqual(academicLevel.Guid, sectionByGuid.AcademicLevels.FirstOrDefault().Id);
                Assert.AreEqual(gradeScheme.Guid, sectionByGuid.GradeSchemes.FirstOrDefault().Id);
                Assert.AreEqual(section.StartOn, sectionByGuid.StartOn);
                Assert.AreEqual(section.EndOn, sectionByGuid.EndOn);
                if (course != null && sectionByGuid.Course != null)
                    Assert.AreEqual(course.Guid, sectionByGuid.Course.Id);
                Assert.AreEqual(section.Number, sectionByGuid.Number);
                Assert.AreEqual(section.MaximumEnrollment, sectionByGuid.MaximumEnrollment);

                var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
                if (courseLevel != null)
                    Assert.AreEqual(courseLevel.Guid, sectionByGuid.CourseLevels[0].Id);
            }
        }

        [TestClass]
        public class SectionsControllerTestsGetHedmSectionById
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;
            private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>(); 

            private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;
            
            private string firstSectionGuid = "eab43b50-ce12-4e7f-8947-2d63661ab7ac";
            private string secondSectionGuid = "51adca90-4839-442b-b22a-12c8009b1186";
            private string thirdSectionGuid = "cd74617e-d304-4c6d-8598-6622ed15192a";

            private Ellucian.Colleague.Domain.Student.Entities.Student firstStudent;
            private Ellucian.Colleague.Domain.Student.Entities.Student secondStudent;
            private Ellucian.Colleague.Domain.Student.Entities.Student thirdStudent;

            private List<Dtos.Section2> allSectionDtos;
            private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
            private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
            private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
            private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
                logger = new Mock<ILogger>().Object;

                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;

                studentRepoMock = new Mock<IStudentRepository>();
                studentRepository = studentRepoMock.Object;

                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;

                allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
                allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
                if (allCourseLevels != null)
                    levels.Add(allCourseLevels[0].Code);

                firstStudent = new Domain.Student.Entities.Student("STU1", "Gamgee", null, new List<string>(), new List<string>());
                firstStudent.FirstName = "Samwise";
                secondStudent = new Domain.Student.Entities.Student("STU2", "Took", null, new List<string>(), new List<string>());
                secondStudent.FirstName = "Peregrin";
                thirdStudent = new Domain.Student.Entities.Student("STU1", "Baggins", null, new List<string>(), new List<string>());
                thirdStudent.FirstName = "Frodo"; thirdStudent.MiddleName = "Ring-bearer";

                adapterRegistry = adapterRegistryMock.Object;

                List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
                var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

                firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                firstSection.TermId = "2012/FA";
                firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
                firstSection.Guid = firstSectionGuid;
                sectionEntityList.Add(firstSection);

                secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
                secondSection.Guid = secondSectionGuid;
                sectionEntityList.Add(secondSection);

                thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
                thirdSection.EndDate = new DateTime(2011, 12, 21);
                thirdSection.Guid = thirdSectionGuid;
                sectionEntityList.Add(thirdSection);

                IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
                IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());

             
                // Mock section repo GetCachedSections
                bool bestFit = false;
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // Mock section repo GetNonCachedSections
                sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // mock DTO adapters
                var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

                academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
                allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

                allSectionDtos = new List<Dtos.Section2>();
                foreach (var sectionEntity in sectionEntityList)
                {
                    //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                    var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section2>(adapterRegistry, logger);
                    Ellucian.Colleague.Dtos.Section2 target = sectionDtoAdapter.MapToType(sectionEntity);
                    target.Id = sectionEntity.Guid;
                    Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                    target.AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = academicLevel.Guid } };

                    Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                    target.GradeSchemes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = gradeScheme.Guid } };

                    var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                    if (course != null) target.Course = new Dtos.GuidObject2 { Id = course.Guid };

                    if (sectionEntity.CourseLevelCodes != null)
                    {
                        var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                        target.CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = courseLevel.Guid } };
                    }
                    allSectionDtos.Add(target);
                }


                // mock controller
                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionsController = null;
                sectionRepository = null;
                studentRepository = null;
                sectionCoordinationService = null;
            }

          
            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSectionByGuid_Exception()
            {
                sectionCoordinationServiceMock.Setup(svc => svc.GetSection2ByGuidAsync(It.IsAny<string>())).Throws<Exception>();
                await sectionsController.GetHedmSectionByGuidAsync(It.IsAny<string>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSectionByGuid_ConfigurationException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection2ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ConfigurationException());
                await sectionsController.GetHedmSectionByGuidAsync(It.IsAny<string>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSectionByGuid_PermissionException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection2ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new PermissionsException());
                await sectionsController.GetHedmSectionByGuidAsync(It.IsAny<string>());
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSectionByGuid_ArgNullException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection2ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ArgumentNullException());
                await sectionsController.GetHedmSectionByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSectionByGuid_RepoException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection2ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new RepositoryException());
                await sectionsController.GetHedmSectionByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSectionByGuid_IntegrationApiException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection2ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new IntegrationApiException());
                await sectionsController.GetHedmSectionByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSectionByGuid_ConfigurationExceptionException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection2ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ConfigurationException());
                await sectionsController.GetHedmSectionByGuidAsync("");
            }

            [TestMethod]
            public async Task SectionsController_GetHedmSectionByGuid()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock.Setup(svc => svc.GetSection2ByGuidAsync(It.IsAny<string>())).ReturnsAsync(section);
                var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

                var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
                Ellucian.Colleague.Domain.Student.Entities.Course course = null;
                if (section.Course != null)
                    course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

                var sectionByGuid = await sectionsController.GetHedmSectionByGuidAsync(firstSectionGuid);
                Assert.AreEqual(section.Id, sectionByGuid.Id);
                Assert.AreEqual(section.Title, sectionByGuid.Title);
                Assert.AreEqual(section.Description, sectionByGuid.Description);
                Assert.AreEqual(academicLevel.Guid, sectionByGuid.AcademicLevels.FirstOrDefault().Id);
                Assert.AreEqual(gradeScheme.Guid, sectionByGuid.GradeSchemes.FirstOrDefault().Id);
                Assert.AreEqual(section.StartOn, sectionByGuid.StartOn);
                Assert.AreEqual(section.EndOn, sectionByGuid.EndOn);
                if (course != null && sectionByGuid.Course != null)
                    Assert.AreEqual(course.Guid, sectionByGuid.Course.Id);
                Assert.AreEqual(section.Number, sectionByGuid.Number);
                Assert.AreEqual(section.MaximumEnrollment, sectionByGuid.MaximumEnrollment);

                var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
                if (courseLevel != null)
                    Assert.AreEqual(courseLevel.Guid, sectionByGuid.CourseLevels[0].Id);

            }
           
        }

        [TestClass]
        public class SectionsControllerTestsDeleteHedmSectionById
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;
            private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>();

            private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

            private string firstSectionGuid = "eab43b50-ce12-4e7f-8947-2d63661ab7ac";
            private string secondSectionGuid = "51adca90-4839-442b-b22a-12c8009b1186";
            private string thirdSectionGuid = "cd74617e-d304-4c6d-8598-6622ed15192a";

            private Ellucian.Colleague.Domain.Student.Entities.Student firstStudent;
            private Ellucian.Colleague.Domain.Student.Entities.Student secondStudent;
            private Ellucian.Colleague.Domain.Student.Entities.Student thirdStudent;

            private List<Dtos.Section2> allSectionDtos;
            private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
            private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
            private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
            private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
                logger = new Mock<ILogger>().Object;

                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;

                studentRepoMock = new Mock<IStudentRepository>();
                studentRepository = studentRepoMock.Object;

                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;

                allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
                allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
                if (allCourseLevels != null)
                    levels.Add(allCourseLevels[0].Code);

                firstStudent = new Domain.Student.Entities.Student("STU1", "Gamgee", null, new List<string>(), new List<string>());
                firstStudent.FirstName = "Samwise";
                secondStudent = new Domain.Student.Entities.Student("STU2", "Took", null, new List<string>(), new List<string>());
                secondStudent.FirstName = "Peregrin";
                thirdStudent = new Domain.Student.Entities.Student("STU1", "Baggins", null, new List<string>(), new List<string>());
                thirdStudent.FirstName = "Frodo"; thirdStudent.MiddleName = "Ring-bearer";

                adapterRegistry = adapterRegistryMock.Object;

                List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
                var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

                firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                firstSection.TermId = "2012/FA";
                firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
                firstSection.Guid = firstSectionGuid;
                sectionEntityList.Add(firstSection);

                secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
                secondSection.Guid = secondSectionGuid;
                sectionEntityList.Add(secondSection);

                thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
                thirdSection.EndDate = new DateTime(2011, 12, 21);
                thirdSection.Guid = thirdSectionGuid;
                sectionEntityList.Add(thirdSection);

                IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
                IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());


                // Mock section repo GetCachedSections
                bool bestFit = false;
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // Mock section repo GetNonCachedSections
                sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // mock DTO adapters
                var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

                academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
                allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

                allSectionDtos = new List<Dtos.Section2>();
                foreach (var sectionEntity in sectionEntityList)
                {
                    //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                    var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section2>(adapterRegistry, logger);
                    Ellucian.Colleague.Dtos.Section2 target = sectionDtoAdapter.MapToType(sectionEntity);
                    target.Id = sectionEntity.Guid;
                    Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                    target.AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = academicLevel.Guid } };

                    Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                    target.GradeSchemes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = gradeScheme.Guid } };

                    var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                    if (course != null) target.Course = new Dtos.GuidObject2 { Id = course.Guid };

                    if (sectionEntity.CourseLevelCodes != null)
                    {
                        var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                        target.CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = courseLevel.Guid } };
                    }
                    allSectionDtos.Add(target);
                }


                // mock controller
                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionsController = null;
                sectionRepository = null;
                studentRepository = null;
                sectionCoordinationService = null;
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_Exception()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
                
                sectionCoordinationServiceMock.Setup(svc => svc.GetSection2ByGuidAsync(It.IsAny<string>())).Throws<Exception>();
                await sectionsController.DeleteHedmSectionByGuidAsync(firstSectionGuid);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_ConfigurationException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection2ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ConfigurationException());
                await sectionsController.DeleteHedmSectionByGuidAsync(firstSectionGuid);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_PermissionException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection2ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new PermissionsException());
                await sectionsController.DeleteHedmSectionByGuidAsync(firstSectionGuid);
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_ArgNullException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection2ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ArgumentNullException());
                await sectionsController.DeleteHedmSectionByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_ArgException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection2ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ArgumentException());
                await sectionsController.DeleteHedmSectionByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_RepoException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection2ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new RepositoryException());
                await sectionsController.DeleteHedmSectionByGuidAsync(firstSectionGuid);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_IntegrationApiException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection2ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new IntegrationApiException());
                await sectionsController.DeleteHedmSectionByGuidAsync(firstSectionGuid);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_ConfigurationExceptionException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection2ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ConfigurationException());
                await sectionsController.DeleteHedmSectionByGuidAsync(firstSectionGuid);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_GuidException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
                section.Id = secondSectionGuid; 
                sectionCoordinationServiceMock.Setup(svc => svc.GetSection2ByGuidAsync(firstSectionGuid)).ReturnsAsync(section);
                await sectionsController.DeleteHedmSectionByGuidAsync(firstSectionGuid);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_NullException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
                
                sectionCoordinationServiceMock.Setup(svc => svc.GetSection2ByGuidAsync(firstSectionGuid)).ReturnsAsync(null);
                await sectionsController.DeleteHedmSectionByGuidAsync(firstSectionGuid);
            }
            
            [TestMethod]
            public async Task SectionsController_DeleteHedmSectionByGuid()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock.Setup(svc => svc.GetSection2ByGuidAsync(firstSectionGuid)).ReturnsAsync(section);
                sectionCoordinationServiceMock.Setup(svc => svc.PutSection2Async(section)).ReturnsAsync(section);
                await sectionsController.DeleteHedmSectionByGuidAsync(firstSectionGuid);
            }

        }

        [TestClass]
        public class SectionsControllerTestsGetHedmSection
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;
            private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>();

            private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

            private string firstSectionGuid = "eab43b50-ce12-4e7f-8947-2d63661ab7ac";
            private string secondSectionGuid = "51adca90-4839-442b-b22a-12c8009b1186";
            private string thirdSectionGuid = "cd74617e-d304-4c6d-8598-6622ed15192a";

            private Ellucian.Colleague.Domain.Student.Entities.Student firstStudent;
            private Ellucian.Colleague.Domain.Student.Entities.Student secondStudent;
            private Ellucian.Colleague.Domain.Student.Entities.Student thirdStudent;

            private List<Dtos.Section2> allSectionDtos;
            private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
            private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
            private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
            private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;
            private Ellucian.Web.Http.Models.Paging paging = new Web.Http.Models.Paging(3, 0);

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
                logger = new Mock<ILogger>().Object;

                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;

                studentRepoMock = new Mock<IStudentRepository>();
                studentRepository = studentRepoMock.Object;

                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;

                allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
                allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
                if (allCourseLevels != null)
                    levels.Add(allCourseLevels[0].Code);

                firstStudent = new Domain.Student.Entities.Student("STU1", "Gamgee", null, new List<string>(), new List<string>());
                firstStudent.FirstName = "Samwise";
                secondStudent = new Domain.Student.Entities.Student("STU2", "Took", null, new List<string>(), new List<string>());
                secondStudent.FirstName = "Peregrin";
                thirdStudent = new Domain.Student.Entities.Student("STU1", "Baggins", null, new List<string>(), new List<string>());
                thirdStudent.FirstName = "Frodo"; thirdStudent.MiddleName = "Ring-bearer";

                adapterRegistry = adapterRegistryMock.Object;

                List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
                var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

                firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                firstSection.TermId = "2012/FA";
                firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
                firstSection.Guid = firstSectionGuid;
                sectionEntityList.Add(firstSection);

                secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
                secondSection.Guid = secondSectionGuid;
                sectionEntityList.Add(secondSection);

                thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
                thirdSection.EndDate = new DateTime(2011, 12, 21);
                thirdSection.Guid = thirdSectionGuid;
                sectionEntityList.Add(thirdSection);

                IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
                IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());


                // Mock section repo GetCachedSections
                bool bestFit = false;
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // Mock section repo GetNonCachedSections
                sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // mock DTO adapters
                var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

                academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
                allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

                allSectionDtos = new List<Dtos.Section2>();
                foreach (var sectionEntity in sectionEntityList)
                {
                    //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                    var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section2>(adapterRegistry, logger);
                    Ellucian.Colleague.Dtos.Section2 target = sectionDtoAdapter.MapToType(sectionEntity);
                    target.Id = sectionEntity.Guid;
                    Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                    target.AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = academicLevel.Guid } };

                    Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                    target.GradeSchemes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = gradeScheme.Guid } };

                    var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                    if (course != null) target.Course = new Dtos.GuidObject2 { Id = course.Guid };

                    if (sectionEntity.CourseLevelCodes != null)
                    {
                        var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                        target.CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = courseLevel.Guid } };
                    }
                    allSectionDtos.Add(target);
                }


                // mock controller
                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionsController = null;
                sectionRepository = null;
                studentRepository = null;
                sectionCoordinationService = null;
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSections_Exception()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                    .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                    .Throws<Exception>();
                await sectionsController.GetHedmSectionsAsync(paging, section.Title);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSections_ConfigurationException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                     .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                    .ThrowsAsync(new ConfigurationException());
                await sectionsController.GetHedmSectionsAsync(paging, section.Title);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSections_PermissionException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                     .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                    .ThrowsAsync(new PermissionsException());
                await sectionsController.GetHedmSectionsAsync(paging, section.Title);
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSections_ArgNullException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                     .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                    .ThrowsAsync(new ArgumentNullException());
                await sectionsController.GetHedmSectionsAsync(paging, section.Title);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSections_RepoException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                     .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                    .ThrowsAsync(new RepositoryException());
                await sectionsController.GetHedmSectionsAsync(paging, section.Title);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSections_IntegrationApiException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                     .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                    .ThrowsAsync(new IntegrationApiException());
                await sectionsController.GetHedmSectionsAsync(paging, section.Title);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSections_ConfigurationExceptionException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                     .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                    .ThrowsAsync(new ConfigurationException());
                await sectionsController.GetHedmSectionsAsync(paging, section.Title);
            }

            [TestMethod]
            public async Task SectionsController_GetHedmSectionByTitle()
            {
                sectionsController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };

                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
                var tuple = new Tuple<IEnumerable<Dtos.Section2>, int>(new List<Dtos.Section2>(){ section} , 5);

                sectionCoordinationServiceMock.Setup(svc => svc.GetSection2ByGuidAsync(It.IsAny<string>())).ReturnsAsync(section);
                sectionCoordinationServiceMock.Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                    .ReturnsAsync(tuple);
                var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

                var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
                Ellucian.Colleague.Domain.Student.Entities.Course course = null;
                if (section.Course != null)
                    course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

                var sectionByTitle = await sectionsController.GetHedmSectionsAsync(paging, title: section.Title);

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await sectionByTitle.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.Section2> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.Section2>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.Section2>;

                var result = results.FirstOrDefault();

                Assert.IsTrue(sectionByTitle is IHttpActionResult);
                
                Assert.AreEqual(section.Id, result.Id);
                Assert.AreEqual(section.Title, result.Title);
                Assert.AreEqual(section.Description, result.Description);
                Assert.AreEqual(academicLevel.Guid, result.AcademicLevels.FirstOrDefault().Id);
                Assert.AreEqual(gradeScheme.Guid, result.GradeSchemes.FirstOrDefault().Id);
                Assert.AreEqual(section.StartOn, result.StartOn);
                Assert.AreEqual(section.EndOn, result.EndOn);
                if (course != null && result.Course != null)
                    Assert.AreEqual(course.Guid, result.Course.Id);
                Assert.AreEqual(section.Number, result.Number);
                Assert.AreEqual(section.MaximumEnrollment, result.MaximumEnrollment);

                var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
                if (courseLevel != null)
                    Assert.AreEqual(courseLevel.Guid, result.CourseLevels[0].Id);

            }
        }

        //V6 Data Model tests

        [TestClass]
        public class SectionsControllerTests_PostHedmSectionAsync_V6
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;
            private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>();

            private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

            private string firstSectionGuid = "eab43b50-ce12-4e7f-8947-2d63661ab7ac";
            private string secondSectionGuid = "51adca90-4839-442b-b22a-12c8009b1186";
            private string thirdSectionGuid = "cd74617e-d304-4c6d-8598-6622ed15192a";


            private List<Dtos.Section3> allSectionDtos;
            private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
            private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
            private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
            private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
                logger = new Mock<ILogger>().Object;

                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;

                studentRepoMock = new Mock<IStudentRepository>();
                studentRepository = studentRepoMock.Object;

                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;

                allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
                allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
                if (allCourseLevels != null)
                    levels.Add(allCourseLevels[0].Code);

                adapterRegistry = adapterRegistryMock.Object;

                List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
                var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

                firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                firstSection.TermId = "2012/FA";
                firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
                firstSection.Guid = firstSectionGuid;
                sectionEntityList.Add(firstSection);

                secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
                secondSection.Guid = secondSectionGuid;
                sectionEntityList.Add(secondSection);

                thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
                thirdSection.EndDate = new DateTime(2011, 12, 21);
                thirdSection.Guid = thirdSectionGuid;
                sectionEntityList.Add(thirdSection);

                IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
                IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());

                // Mock section repo GetCachedSections
                bool bestFit = false;
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // Mock section repo GetNonCachedSections
                sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // mock DTO adapters
                var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

                academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
                allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

                allSectionDtos = new List<Dtos.Section3>();
                foreach (var sectionEntity in sectionEntityList)
                {
                    //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                    var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section3>(adapterRegistry, logger);
                    Ellucian.Colleague.Dtos.Section3 target = sectionDtoAdapter.MapToType(sectionEntity);
                    target.Id = sectionEntity.Guid;
                    Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                    target.AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = academicLevel.Guid } };

                    Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                    target.GradeSchemes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = gradeScheme.Guid } };

                    var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                    if (course != null) target.Course = new Dtos.GuidObject2 { Id = course.Guid };

                    if (sectionEntity.CourseLevelCodes != null)
                    {
                        var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                        target.CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = courseLevel.Guid } };
                    }
                    allSectionDtos.Add(target);
                }

                // mock controller
                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionsController = null;
                sectionRepository = null;
                studentRepository = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostHedmSectionAsync_Exception()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                    .Setup(svc => svc.PostSection3Async(It.IsAny<Dtos.Section3>()))
                    .Throws<Exception>();
                await sectionsController.PostHedmSection2Async(It.IsAny<Dtos.Section3>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostHedmSectionAsync_ConfigurationException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PostSection3Async(It.IsAny<Dtos.Section3>()))
                   .Throws<ConfigurationException>();
                await sectionsController.PostHedmSection2Async(It.IsAny<Dtos.Section3>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostHedmSectionAsync_PermissionException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PostSection3Async(It.IsAny<Dtos.Section3>()))
                   .Throws<PermissionsException>();
                await sectionsController.PostHedmSection2Async(It.IsAny<Dtos.Section3>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostHedmSectionAsync_ArgNullException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                  .Setup(svc => svc.PostSection3Async(null))
                  .Throws<ArgumentNullException>();
                await sectionsController.PostHedmSection2Async(null);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostHedmSectionAsync_RepoException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PostSection3Async(It.IsAny<Dtos.Section3>()))
                   .Throws<RepositoryException>();
                await sectionsController.PostHedmSection2Async(It.IsAny<Dtos.Section3>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostHedmSectionAsync_IntegrationApiException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PostSection3Async(It.IsAny<Dtos.Section3>()))
                   .Throws<IntegrationApiException>();
                await sectionsController.PostHedmSection2Async(It.IsAny<Dtos.Section3>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PostHedmSectionAsync_ID_Null()
            {
                sectionCoordinationServiceMock
                   .Setup(svc => svc.PostSection3Async(It.IsAny<Dtos.Section3>()))
                   .Throws<IntegrationApiException>();
                await sectionsController.PostHedmSection2Async(new Dtos.Section3() { Id = string.Empty });
            }

            [TestMethod]
            public async Task SectionsController_PostHedmSectionAsync()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock.Setup(svc => svc.PostSection3Async(section)).ReturnsAsync(section);
                var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

                var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
                Ellucian.Colleague.Domain.Student.Entities.Course course = null;
                if (section.Course != null)
                    course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

                var sectionByGuid = await sectionsController.PostHedmSection2Async(section);
                Assert.AreEqual(section.Id, sectionByGuid.Id);
                Assert.AreEqual(section.Title, sectionByGuid.Title);
                Assert.AreEqual(section.Description, sectionByGuid.Description);
                Assert.AreEqual(academicLevel.Guid, sectionByGuid.AcademicLevels.FirstOrDefault().Id);
                Assert.AreEqual(gradeScheme.Guid, sectionByGuid.GradeSchemes.FirstOrDefault().Id);
                Assert.AreEqual(section.StartOn, sectionByGuid.StartOn);
                Assert.AreEqual(section.EndOn, sectionByGuid.EndOn);
                if (course != null && sectionByGuid.Course != null)
                    Assert.AreEqual(course.Guid, sectionByGuid.Course.Id);
                Assert.AreEqual(section.Number, sectionByGuid.Number);
                Assert.AreEqual(section.MaximumEnrollment, sectionByGuid.MaximumEnrollment);

                var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
                if (courseLevel != null)
                    Assert.AreEqual(courseLevel.Guid, sectionByGuid.CourseLevels[0].Id);

            }
        }

        [TestClass]
        public class SectionsControllerTests_PutHedmSectionAsync_V6
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;
            private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>();

            private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

            private string firstSectionGuid = "eab43b50-ce12-4e7f-8947-2d63661ab7ac";
            private string secondSectionGuid = "51adca90-4839-442b-b22a-12c8009b1186";
            private string thirdSectionGuid = "cd74617e-d304-4c6d-8598-6622ed15192a";

            private List<Dtos.Section3> allSectionDtos;
            private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
            private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
            private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
            private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;


            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
                logger = new Mock<ILogger>().Object;

                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;

                studentRepoMock = new Mock<IStudentRepository>();
                studentRepository = studentRepoMock.Object;

                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;

                allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
                allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
                if (allCourseLevels != null)
                    levels.Add(allCourseLevels[0].Code);

                adapterRegistry = adapterRegistryMock.Object;

                List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
                var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

                firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                firstSection.TermId = "2012/FA";
                firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
                firstSection.Guid = firstSectionGuid;
                sectionEntityList.Add(firstSection);

                secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
                secondSection.Guid = secondSectionGuid;
                sectionEntityList.Add(secondSection);

                thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
                thirdSection.EndDate = new DateTime(2011, 12, 21);
                thirdSection.Guid = thirdSectionGuid;
                sectionEntityList.Add(thirdSection);

                IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
                IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());

                // Mock section repo GetCachedSections
                bool bestFit = false;
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // Mock section repo GetNonCachedSections
                sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // mock DTO adapters
                var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

                academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
                allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

                allSectionDtos = new List<Dtos.Section3>();
                foreach (var sectionEntity in sectionEntityList)
                {
                    //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                    var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section3>(adapterRegistry, logger);
                    Ellucian.Colleague.Dtos.Section3 target = sectionDtoAdapter.MapToType(sectionEntity);
                    target.Id = sectionEntity.Guid;
                    Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                    target.AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = academicLevel.Guid } };

                    Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                    target.GradeSchemes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = gradeScheme.Guid } };

                    var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                    if (course != null) target.Course = new Dtos.GuidObject2 { Id = course.Guid };

                    if (sectionEntity.CourseLevelCodes != null)
                    {
                        var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                        target.CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = courseLevel.Guid } };
                    }
                    allSectionDtos.Add(target);
                }

                // mock controller
                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionsController = null;
                sectionRepository = null;
                studentRepository = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_Exception()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                    .Setup(svc => svc.PutSection3Async(It.IsAny<Dtos.Section3>()))
                    .Throws<Exception>();
                await sectionsController.PutHedmSection2Async(section.Id, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_ConfigurationException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSection3Async(It.IsAny<Dtos.Section3>()))
                   .Throws<ConfigurationException>();
                await sectionsController.PutHedmSection2Async(section.Id, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_PermissionException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSection3Async(It.IsAny<Dtos.Section3>()))
                   .Throws<PermissionsException>();
                await sectionsController.PutHedmSection2Async(section.Id, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_RepoException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSection3Async(It.IsAny<Dtos.Section3>()))
                   .Throws<RepositoryException>();
                await sectionsController.PutHedmSection2Async(section.Id, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_ArgException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSection3Async(It.IsAny<Dtos.Section3>()))
                   .Throws<ArgumentException>();
                await sectionsController.PutHedmSection2Async(section.Id, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_NilGUID_ArgException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSection3Async(It.IsAny<Dtos.Section3>()))
                   .Throws<ArgumentException>();
                await sectionsController.PutHedmSection2Async(Guid.Empty.ToString(), section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_SectionIdNull_ArgException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSection3Async(It.IsAny<Dtos.Section3>()))
                   .Throws<ArgumentException>();
                await sectionsController.PutHedmSection2Async("1234", new Dtos.Section3() { Id = "" });
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_IntegrationApiException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                   .Setup(svc => svc.PutSection3Async(It.IsAny<Dtos.Section3>()))
                   .Throws<IntegrationApiException>();
                await sectionsController.PutHedmSection2Async(section.Id, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_NullIDException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                await sectionsController.PutHedmSection2Async(null, section);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_NullSectionException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                await sectionsController.PutHedmSection2Async(firstSectionGuid, null);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_PutHedmSectionAsync_InvalidException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                await sectionsController.PutHedmSection2Async(secondSectionGuid, section);
            }

            [TestMethod]
            public async Task SectionsController_PutHedmSectionAsync()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock.Setup(svc => svc.PutSection3Async(section)).ReturnsAsync(section);
                var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

                var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
                Ellucian.Colleague.Domain.Student.Entities.Course course = null;
                if (section.Course != null)
                    course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

                var sectionByGuid = await sectionsController.PutHedmSection2Async(firstSectionGuid, section);
                Assert.AreEqual(section.Id, sectionByGuid.Id);
                Assert.AreEqual(section.Title, sectionByGuid.Title);
                Assert.AreEqual(section.Description, sectionByGuid.Description);
                Assert.AreEqual(academicLevel.Guid, sectionByGuid.AcademicLevels.FirstOrDefault().Id);
                Assert.AreEqual(gradeScheme.Guid, sectionByGuid.GradeSchemes.FirstOrDefault().Id);
                Assert.AreEqual(section.StartOn, sectionByGuid.StartOn);
                Assert.AreEqual(section.EndOn, sectionByGuid.EndOn);
                if (course != null && sectionByGuid.Course != null)
                    Assert.AreEqual(course.Guid, sectionByGuid.Course.Id);
                Assert.AreEqual(section.Number, sectionByGuid.Number);
                Assert.AreEqual(section.MaximumEnrollment, sectionByGuid.MaximumEnrollment);

                var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
                if (courseLevel != null)
                    Assert.AreEqual(courseLevel.Guid, sectionByGuid.CourseLevels[0].Id);
            }
        }

        [TestClass]
        public class SectionsControllerTestsGetHedmSectionById_V6
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;
            private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>();

            private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

            private string firstSectionGuid = "eab43b50-ce12-4e7f-8947-2d63661ab7ac";
            private string secondSectionGuid = "51adca90-4839-442b-b22a-12c8009b1186";
            private string thirdSectionGuid = "cd74617e-d304-4c6d-8598-6622ed15192a";

            private Ellucian.Colleague.Domain.Student.Entities.Student firstStudent;
            private Ellucian.Colleague.Domain.Student.Entities.Student secondStudent;
            private Ellucian.Colleague.Domain.Student.Entities.Student thirdStudent;

            private List<Dtos.Section3> allSectionDtos;
            private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
            private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
            private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
            private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
                logger = new Mock<ILogger>().Object;

                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;

                studentRepoMock = new Mock<IStudentRepository>();
                studentRepository = studentRepoMock.Object;

                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;

                allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
                allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
                if (allCourseLevels != null)
                    levels.Add(allCourseLevels[0].Code);

                firstStudent = new Domain.Student.Entities.Student("STU1", "Gamgee", null, new List<string>(), new List<string>());
                firstStudent.FirstName = "Samwise";
                secondStudent = new Domain.Student.Entities.Student("STU2", "Took", null, new List<string>(), new List<string>());
                secondStudent.FirstName = "Peregrin";
                thirdStudent = new Domain.Student.Entities.Student("STU1", "Baggins", null, new List<string>(), new List<string>());
                thirdStudent.FirstName = "Frodo"; thirdStudent.MiddleName = "Ring-bearer";

                adapterRegistry = adapterRegistryMock.Object;

                List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
                var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

                firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                firstSection.TermId = "2012/FA";
                firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
                firstSection.Guid = firstSectionGuid;
                sectionEntityList.Add(firstSection);

                secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
                secondSection.Guid = secondSectionGuid;
                sectionEntityList.Add(secondSection);

                thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
                thirdSection.EndDate = new DateTime(2011, 12, 21);
                thirdSection.Guid = thirdSectionGuid;
                sectionEntityList.Add(thirdSection);

                IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
                IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());
                

                // Mock section repo GetCachedSections
                bool bestFit = false;
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // Mock section repo GetNonCachedSections
                sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // mock DTO adapters
                var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

                academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
                allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

                allSectionDtos = new List<Dtos.Section3>();
                foreach (var sectionEntity in sectionEntityList)
                {
                    //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                    var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section3>(adapterRegistry, logger);
                    Ellucian.Colleague.Dtos.Section3 target = sectionDtoAdapter.MapToType(sectionEntity);
                    target.Id = sectionEntity.Guid;
                    Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                    target.AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = academicLevel.Guid } };

                    Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                    target.GradeSchemes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = gradeScheme.Guid } };

                    var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                    if (course != null) target.Course = new Dtos.GuidObject2 { Id = course.Guid };

                    if (sectionEntity.CourseLevelCodes != null)
                    {
                        var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                        target.CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = courseLevel.Guid } };
                    }
                    allSectionDtos.Add(target);
                }

                sectionCoordinationServiceMock.Setup(s => s.GetDataPrivacyListByApi(It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(new List<string>());
                // mock controller
                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger)
                {
                    Request = new HttpRequestMessage()
                };
                sectionsController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionsController = null;
                sectionRepository = null;
                studentRepository = null;
                sectionCoordinationService = null;
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSectionByGuid_Exception()
            {
                sectionCoordinationServiceMock.Setup(svc => svc.GetSection3ByGuidAsync(It.IsAny<string>())).Throws<Exception>();
                await sectionsController.GetHedmSectionByGuid2Async(It.IsAny<string>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSectionByGuid_ConfigurationException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection3ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ConfigurationException());
                await sectionsController.GetHedmSectionByGuid2Async(It.IsAny<string>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSectionByGuid_PermissionException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection3ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new PermissionsException());
                await sectionsController.GetHedmSectionByGuid2Async(It.IsAny<string>());
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSectionByGuid_ArgNullException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection3ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ArgumentNullException());
                await sectionsController.GetHedmSectionByGuid2Async("");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSectionByGuid_RepoException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection3ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new RepositoryException());
                await sectionsController.GetHedmSectionByGuid2Async("");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSectionByGuid_IntegrationApiException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection3ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new IntegrationApiException());
                await sectionsController.GetHedmSectionByGuid2Async("");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSectionByGuid_ConfigurationExceptionException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection3ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ConfigurationException());
                await sectionsController.GetHedmSectionByGuid2Async("");
            }

            [TestMethod]
            public async Task SectionsController_GetHedmSectionByGuid()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock.Setup(svc => svc.GetSection3ByGuidAsync(It.IsAny<string>())).ReturnsAsync(section);
                var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

                var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
                Ellucian.Colleague.Domain.Student.Entities.Course course = null;
                if (section.Course != null)
                    course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

                var sectionByGuid = await sectionsController.GetHedmSectionByGuid2Async(firstSectionGuid);
                Assert.AreEqual(section.Id, sectionByGuid.Id);
                Assert.AreEqual(section.Title, sectionByGuid.Title);
                Assert.AreEqual(section.Description, sectionByGuid.Description);
                Assert.AreEqual(academicLevel.Guid, sectionByGuid.AcademicLevels.FirstOrDefault().Id);
                Assert.AreEqual(gradeScheme.Guid, sectionByGuid.GradeSchemes.FirstOrDefault().Id);
                Assert.AreEqual(section.StartOn, sectionByGuid.StartOn);
                Assert.AreEqual(section.EndOn, sectionByGuid.EndOn);
                if (course != null && sectionByGuid.Course != null)
                    Assert.AreEqual(course.Guid, sectionByGuid.Course.Id);
                Assert.AreEqual(section.Number, sectionByGuid.Number);
                Assert.AreEqual(section.MaximumEnrollment, sectionByGuid.MaximumEnrollment);

                var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
                if (courseLevel != null)
                    Assert.AreEqual(courseLevel.Guid, sectionByGuid.CourseLevels[0].Id);

            }

        }

        [TestClass]
        public class SectionsControllerTestsDeleteHedmSectionById_V6
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;
            private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>();

            private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

            private string firstSectionGuid = "eab43b50-ce12-4e7f-8947-2d63661ab7ac";
            private string secondSectionGuid = "51adca90-4839-442b-b22a-12c8009b1186";
            private string thirdSectionGuid = "cd74617e-d304-4c6d-8598-6622ed15192a";

            private Ellucian.Colleague.Domain.Student.Entities.Student firstStudent;
            private Ellucian.Colleague.Domain.Student.Entities.Student secondStudent;
            private Ellucian.Colleague.Domain.Student.Entities.Student thirdStudent;

            private List<Dtos.Section3> allSectionDtos;
            private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
            private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
            private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
            private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
                logger = new Mock<ILogger>().Object;

                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;

                studentRepoMock = new Mock<IStudentRepository>();
                studentRepository = studentRepoMock.Object;

                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;

                allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
                allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
                if (allCourseLevels != null)
                    levels.Add(allCourseLevels[0].Code);

                firstStudent = new Domain.Student.Entities.Student("STU1", "Gamgee", null, new List<string>(), new List<string>());
                firstStudent.FirstName = "Samwise";
                secondStudent = new Domain.Student.Entities.Student("STU2", "Took", null, new List<string>(), new List<string>());
                secondStudent.FirstName = "Peregrin";
                thirdStudent = new Domain.Student.Entities.Student("STU1", "Baggins", null, new List<string>(), new List<string>());
                thirdStudent.FirstName = "Frodo"; thirdStudent.MiddleName = "Ring-bearer";

                adapterRegistry = adapterRegistryMock.Object;

                List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
                var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

                firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                firstSection.TermId = "2012/FA";
                firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
                firstSection.Guid = firstSectionGuid;
                sectionEntityList.Add(firstSection);

                secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
                secondSection.Guid = secondSectionGuid;
                sectionEntityList.Add(secondSection);

                thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
                thirdSection.EndDate = new DateTime(2011, 12, 21);
                thirdSection.Guid = thirdSectionGuid;
                sectionEntityList.Add(thirdSection);

                IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
                IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());


                // Mock section repo GetCachedSections
                bool bestFit = false;
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // Mock section repo GetNonCachedSections
                sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // mock DTO adapters
                var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

                academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
                allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

                allSectionDtos = new List<Dtos.Section3>();
                foreach (var sectionEntity in sectionEntityList)
                {
                    //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                    var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section3>(adapterRegistry, logger);
                    Ellucian.Colleague.Dtos.Section3 target = sectionDtoAdapter.MapToType(sectionEntity);
                    target.Id = sectionEntity.Guid;
                    Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                    target.AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = academicLevel.Guid } };

                    Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                    target.GradeSchemes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = gradeScheme.Guid } };

                    var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                    if (course != null) target.Course = new Dtos.GuidObject2 { Id = course.Guid };

                    if (sectionEntity.CourseLevelCodes != null)
                    {
                        var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                        target.CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = courseLevel.Guid } };
                    }
                    allSectionDtos.Add(target);
                }


                // mock controller
                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionsController = null;
                sectionRepository = null;
                studentRepository = null;
                sectionCoordinationService = null;
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_Exception()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock.Setup(svc => svc.GetSection3ByGuidAsync(It.IsAny<string>())).Throws<Exception>();
                await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_ConfigurationException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection3ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ConfigurationException());
                await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_PermissionException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection3ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new PermissionsException());
                await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_ArgNullException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection3ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ArgumentNullException());
                await sectionsController.DeleteHedmSectionByGuid2Async("");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_ArgException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection3ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ArgumentException());
                await sectionsController.DeleteHedmSectionByGuid2Async("");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_RepoException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection3ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new RepositoryException());
                await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_IntegrationApiException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection3ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new IntegrationApiException());
                await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_ConfigurationExceptionException()
            {
                sectionCoordinationServiceMock
                    .Setup(c => c.GetSection3ByGuidAsync(It.IsAny<string>()))
                    .ThrowsAsync(new ConfigurationException());
                await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_GuidException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
                section.Id = secondSectionGuid;
                sectionCoordinationServiceMock.Setup(svc => svc.GetSection3ByGuidAsync(firstSectionGuid)).ReturnsAsync(section);
                await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_DeleteHedmSectionByGuid_NullException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock.Setup(svc => svc.GetSection3ByGuidAsync(firstSectionGuid)).ReturnsAsync(null);
                await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
            }

            [TestMethod]
            public async Task SectionsController_DeleteHedmSectionByGuid()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock.Setup(svc => svc.GetSection3ByGuidAsync(firstSectionGuid)).ReturnsAsync(section);
                sectionCoordinationServiceMock.Setup(svc => svc.PutSection3Async(section)).ReturnsAsync(section);
                await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
            }

        }

        [TestClass]
        public class SectionsControllerTestsGetHedmSection_V6
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private IAdapterRegistry adapterRegistry;
            private SectionsController sectionsController;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepository;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepository;
            private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
            private ISectionCoordinationService sectionCoordinationService;
            private ILogger logger;
            private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>();

            private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
            private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

            private string firstSectionGuid = "eab43b50-ce12-4e7f-8947-2d63661ab7ac";
            private string secondSectionGuid = "51adca90-4839-442b-b22a-12c8009b1186";
            private string thirdSectionGuid = "cd74617e-d304-4c6d-8598-6622ed15192a";

            private Ellucian.Colleague.Domain.Student.Entities.Student firstStudent;
            private Ellucian.Colleague.Domain.Student.Entities.Student secondStudent;
            private Ellucian.Colleague.Domain.Student.Entities.Student thirdStudent;

            private List<Dtos.Section3> allSectionDtos;
            private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
            private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
            private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
            private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;
            private Ellucian.Web.Http.Models.Paging paging = new Web.Http.Models.Paging(3, 0);

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
                logger = new Mock<ILogger>().Object;

                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepository = sectionRepoMock.Object;

                studentRepoMock = new Mock<IStudentRepository>();
                studentRepository = studentRepoMock.Object;

                sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
                sectionCoordinationService = sectionCoordinationServiceMock.Object;

                allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
                allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
                if (allCourseLevels != null)
                    levels.Add(allCourseLevels[0].Code);

                firstStudent = new Domain.Student.Entities.Student("STU1", "Gamgee", null, new List<string>(), new List<string>());
                firstStudent.FirstName = "Samwise";
                secondStudent = new Domain.Student.Entities.Student("STU2", "Took", null, new List<string>(), new List<string>());
                secondStudent.FirstName = "Peregrin";
                thirdStudent = new Domain.Student.Entities.Student("STU1", "Baggins", null, new List<string>(), new List<string>());
                thirdStudent.FirstName = "Frodo"; thirdStudent.MiddleName = "Ring-bearer";

                adapterRegistry = adapterRegistryMock.Object;

                List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
                var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

                firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                firstSection.TermId = "2012/FA";
                firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
                firstSection.Guid = firstSectionGuid;
                sectionEntityList.Add(firstSection);

                secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
                secondSection.Guid = secondSectionGuid;
                sectionEntityList.Add(secondSection);

                thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
                thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
                thirdSection.EndDate = new DateTime(2011, 12, 21);
                thirdSection.Guid = thirdSectionGuid;
                sectionEntityList.Add(thirdSection);

                IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
                IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
                sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());


                // Mock section repo GetCachedSections
                bool bestFit = false;
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // Mock section repo GetNonCachedSections
                sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
                // mock DTO adapters
                var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

                academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
                allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

                allSectionDtos = new List<Dtos.Section3>();
                foreach (var sectionEntity in sectionEntityList)
                {
                    //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                    var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section3>(adapterRegistry, logger);
                    Ellucian.Colleague.Dtos.Section3 target = sectionDtoAdapter.MapToType(sectionEntity);
                    target.Id = sectionEntity.Guid;
                    Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                    target.AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = academicLevel.Guid } };

                    Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                    target.GradeSchemes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = gradeScheme.Guid } };

                    var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                    if (course != null) target.Course = new Dtos.GuidObject2 { Id = course.Guid };

                    if (sectionEntity.CourseLevelCodes != null)
                    {
                        var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                        target.CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = courseLevel.Guid } };
                    }
                    allSectionDtos.Add(target);
                }

                sectionCoordinationServiceMock.Setup(s => s.GetDataPrivacyListByApi(It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(new List<string>());
                // mock controller
                sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger)
                {
                    Request = new HttpRequestMessage()
                };
                sectionsController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionsController = null;
                sectionRepository = null;
                studentRepository = null;
                sectionCoordinationService = null;
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSections_Exception()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                    .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                    .Throws<Exception>();
                await sectionsController.GetHedmSections2Async(paging, section.Title);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSections_ConfigurationException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                     .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                    .ThrowsAsync(new ConfigurationException());
                await sectionsController.GetHedmSections2Async(paging, section.Title);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSections_PermissionException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                     .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                    .ThrowsAsync(new PermissionsException());
                await sectionsController.GetHedmSections2Async(paging, section.Title);
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSections_ArgNullException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                     .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                    .ThrowsAsync(new ArgumentNullException());
                await sectionsController.GetHedmSections2Async(paging, section.Title);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSections_RepoException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                     .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                    .ThrowsAsync(new RepositoryException());
                await sectionsController.GetHedmSections2Async(paging, section.Title);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSections_IntegrationApiException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                     .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                    .ThrowsAsync(new IntegrationApiException());
                await sectionsController.GetHedmSections2Async(paging, section.Title);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task SectionsController_GetHedmSections_ConfigurationExceptionException()
            {
                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

                sectionCoordinationServiceMock
                     .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                    .ThrowsAsync(new ConfigurationException());
                await sectionsController.GetHedmSections2Async(paging, section.Title);
            }

            [TestMethod]
            public async Task SectionsController_GetHedmSectionByTitle()
            {
                sectionsController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };

                var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
                var tuple = new Tuple<IEnumerable<Dtos.Section3>, int>(new List<Dtos.Section3>() { section }, 5);

                sectionCoordinationServiceMock.Setup(svc => svc.GetSection3ByGuidAsync(It.IsAny<string>())).ReturnsAsync(section);
                sectionCoordinationServiceMock.Setup(svc => svc.GetSections3Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                    .ReturnsAsync(tuple);
                var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

                var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
                Ellucian.Colleague.Domain.Student.Entities.Course course = null;
                if (section.Course != null)
                    course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

                var sectionByTitle = await sectionsController.GetHedmSections2Async(paging, title: section.Title);

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await sectionByTitle.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.Section3> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.Section3>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.Section3>;

                var result = results.FirstOrDefault();

                Assert.IsTrue(sectionByTitle is IHttpActionResult);

                Assert.AreEqual(section.Id, result.Id);
                Assert.AreEqual(section.Title, result.Title);
                Assert.AreEqual(section.Description, result.Description);
                Assert.AreEqual(academicLevel.Guid, result.AcademicLevels.FirstOrDefault().Id);
                Assert.AreEqual(gradeScheme.Guid, result.GradeSchemes.FirstOrDefault().Id);
                Assert.AreEqual(section.StartOn, result.StartOn);
                Assert.AreEqual(section.EndOn, result.EndOn);
                if (course != null && result.Course != null)
                    Assert.AreEqual(course.Guid, result.Course.Id);
                Assert.AreEqual(section.Number, result.Number);
                Assert.AreEqual(section.MaximumEnrollment, result.MaximumEnrollment);

                var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
                if (courseLevel != null)
                    Assert.AreEqual(courseLevel.Guid, result.CourseLevels[0].Id);

            }
        }
    }

    //V8 Data Model tests

    [TestClass]
    public class SectionsControllerTests_PostHedmSectionAsync_V8
    {

        #region Test Context

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #endregion

        private IAdapterRegistry adapterRegistry;
        private SectionsController sectionsController;
        private Mock<ISectionRepository> sectionRepoMock;
        private ISectionRepository sectionRepository;
        private Mock<IStudentRepository> studentRepoMock;
        private IStudentRepository studentRepository;
        private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
        private ISectionCoordinationService sectionCoordinationService;
        private ILogger logger;
        private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
        private List<string> levels = new List<string>();

        private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
        private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
        private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

        private string firstSectionGuid = "eab43b50-ce12-4e7f-8947-2d63661ab7ac";
        private string secondSectionGuid = "51adca90-4839-442b-b22a-12c8009b1186";
        private string thirdSectionGuid = "cd74617e-d304-4c6d-8598-6622ed15192a";


        private List<Dtos.Section4> allSectionDtos;
        private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
        private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
        private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
        private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
            logger = new Mock<ILogger>().Object;

            sectionRepoMock = new Mock<ISectionRepository>();
            sectionRepository = sectionRepoMock.Object;

            studentRepoMock = new Mock<IStudentRepository>();
            studentRepository = studentRepoMock.Object;

            sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
            sectionCoordinationService = sectionCoordinationServiceMock.Object;

            allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
            allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
            if (allCourseLevels != null)
                levels.Add(allCourseLevels[0].Code);

            adapterRegistry = adapterRegistryMock.Object;

            List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
            var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

            firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
            firstSection.TermId = "2012/FA";
            firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
            firstSection.Guid = firstSectionGuid;
            sectionEntityList.Add(firstSection);

            secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
            secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
            secondSection.Guid = secondSectionGuid;
            sectionEntityList.Add(secondSection);

            thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
            thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
            thirdSection.EndDate = new DateTime(2011, 12, 21);
            thirdSection.Guid = thirdSectionGuid;
            sectionEntityList.Add(thirdSection);

            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
            IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

            sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
            sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
            sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());

            // Mock section repo GetCachedSections
            bool bestFit = false;
            sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
            // Mock section repo GetNonCachedSections
            sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
            // mock DTO adapters
            var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
            adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

            academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
            allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

            allSectionDtos = new List<Dtos.Section4>();
            foreach (var sectionEntity in sectionEntityList)
            {
                //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section4>(adapterRegistry, logger);
                Ellucian.Colleague.Dtos.Section4 target = sectionDtoAdapter.MapToType(sectionEntity);
                target.Id = sectionEntity.Guid;
                Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                target.AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = academicLevel.Guid } };

                Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                target.GradeSchemes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = gradeScheme.Guid } };

                var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                if (course != null) target.Course = new Dtos.GuidObject2 { Id = course.Guid };

                if (sectionEntity.CourseLevelCodes != null)
                {
                    var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                    target.CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = courseLevel.Guid } };
                }
                allSectionDtos.Add(target);
            }

            // mock controller
            sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
        }

        [TestCleanup]
        public void Cleanup()
        {
            sectionsController = null;
            sectionRepository = null;
            studentRepository = null;
            sectionCoordinationService = null;
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PostHedmSectionAsync_Exception()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                .Setup(svc => svc.PostSection4Async(It.IsAny<Dtos.Section4>()))
                .Throws<Exception>();
            await sectionsController.PostHedmSection4Async(It.IsAny<Dtos.Section4>());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PostHedmSectionAsync_ConfigurationException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                .Setup(svc => svc.PostSection4Async(It.IsAny<Dtos.Section4>()))
                .Throws<ConfigurationException>();
            await sectionsController.PostHedmSection4Async(It.IsAny<Dtos.Section4>());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PostHedmSectionAsync_PermissionException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                .Setup(svc => svc.PostSection4Async(It.IsAny<Dtos.Section4>()))
                .Throws<PermissionsException>();
            await sectionsController.PostHedmSection4Async(It.IsAny<Dtos.Section4>());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PostHedmSectionAsync_ArgNullException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                .Setup(svc => svc.PostSection4Async(null))
                .Throws<ArgumentNullException>();
            await sectionsController.PostHedmSection2Async(null);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PostHedmSectionAsync_RepoException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                .Setup(svc => svc.PostSection4Async(It.IsAny<Dtos.Section4>()))
                .Throws<RepositoryException>();
            await sectionsController.PostHedmSection4Async(It.IsAny<Dtos.Section4>());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PostHedmSectionAsync_IntegrationApiException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                .Setup(svc => svc.PostSection4Async(It.IsAny<Dtos.Section4>()))
                .Throws<IntegrationApiException>();
            await sectionsController.PostHedmSection4Async(It.IsAny<Dtos.Section4>());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PostHedmSectionAsync_ID_Null()
        {
            sectionCoordinationServiceMock
                .Setup(svc => svc.PostSection4Async(It.IsAny<Dtos.Section4>()))
                .Throws<IntegrationApiException>();
            await sectionsController.PostHedmSection4Async(new Dtos.Section4() { Id = string.Empty });
        }

        [TestMethod]
        public async Task SectionsController_PostHedmSectionAsync()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock.Setup(svc => svc.PostSection4Async(section)).ReturnsAsync(section);
            var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

            var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
            Ellucian.Colleague.Domain.Student.Entities.Course course = null;
            if (section.Course != null)
                course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

            var sectionByGuid = await sectionsController.PostHedmSection4Async(section);
            Assert.AreEqual(section.Id, sectionByGuid.Id);
            Assert.AreEqual(section.Title, sectionByGuid.Title);
            Assert.AreEqual(section.Description, sectionByGuid.Description);
            Assert.AreEqual(academicLevel.Guid, sectionByGuid.AcademicLevels.FirstOrDefault().Id);
            Assert.AreEqual(gradeScheme.Guid, sectionByGuid.GradeSchemes.FirstOrDefault().Id);
            Assert.AreEqual(section.StartOn, sectionByGuid.StartOn);
            Assert.AreEqual(section.EndOn, sectionByGuid.EndOn);
            if (course != null && sectionByGuid.Course != null)
                Assert.AreEqual(course.Guid, sectionByGuid.Course.Id);
            Assert.AreEqual(section.Number, sectionByGuid.Number);
            Assert.AreEqual(section.MaximumEnrollment, sectionByGuid.MaximumEnrollment);

            var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
            if (courseLevel != null)
                Assert.AreEqual(courseLevel.Guid, sectionByGuid.CourseLevels[0].Id);

        }
    }

    [TestClass]
    public class SectionsControllerTests_PutHedmSectionAsync_V8
    {

        #region Test Context

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #endregion

        private IAdapterRegistry adapterRegistry;
        private SectionsController sectionsController;
        private Mock<ISectionRepository> sectionRepoMock;
        private ISectionRepository sectionRepository;
        private Mock<IStudentRepository> studentRepoMock;
        private IStudentRepository studentRepository;
        private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
        private ISectionCoordinationService sectionCoordinationService;
        private ILogger logger;
        private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
        private List<string> levels = new List<string>();

        private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
        private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
        private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

        private string firstSectionGuid = "eab43b50-ce12-4e7f-8947-2d63661ab7ac";
        private string secondSectionGuid = "51adca90-4839-442b-b22a-12c8009b1186";
        private string thirdSectionGuid = "cd74617e-d304-4c6d-8598-6622ed15192a";

        private List<Dtos.Section4> allSectionDtos;
        private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
        private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
        private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
        private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;


        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
            logger = new Mock<ILogger>().Object;

            sectionRepoMock = new Mock<ISectionRepository>();
            sectionRepository = sectionRepoMock.Object;

            studentRepoMock = new Mock<IStudentRepository>();
            studentRepository = studentRepoMock.Object;

            sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
            sectionCoordinationService = sectionCoordinationServiceMock.Object;

            allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
            allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
            if (allCourseLevels != null)
                levels.Add(allCourseLevels[0].Code);

            adapterRegistry = adapterRegistryMock.Object;

            List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
            var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

            firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
            firstSection.TermId = "2012/FA";
            firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
            firstSection.Guid = firstSectionGuid;
            sectionEntityList.Add(firstSection);

            secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
            secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
            secondSection.Guid = secondSectionGuid;
            sectionEntityList.Add(secondSection);

            thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
            thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
            thirdSection.EndDate = new DateTime(2011, 12, 21);
            thirdSection.Guid = thirdSectionGuid;
            sectionEntityList.Add(thirdSection);

            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
            IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

            sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
            sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
            sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());

            // Mock section repo GetCachedSections
            bool bestFit = false;
            sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
            // Mock section repo GetNonCachedSections
            sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
            // mock DTO adapters
            var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
            adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

            academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
            allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

            allSectionDtos = new List<Dtos.Section4>();
            foreach (var sectionEntity in sectionEntityList)
            {
                //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section4>(adapterRegistry, logger);
                Ellucian.Colleague.Dtos.Section4 target = sectionDtoAdapter.MapToType(sectionEntity);
                target.Id = sectionEntity.Guid;
                Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                target.AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = academicLevel.Guid } };

                Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                target.GradeSchemes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = gradeScheme.Guid } };

                var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                if (course != null) target.Course = new Dtos.GuidObject2 { Id = course.Guid };

                if (sectionEntity.CourseLevelCodes != null)
                {
                    var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                    target.CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = courseLevel.Guid } };
                }
                allSectionDtos.Add(target);
            }

            // mock controller
            sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
        }

        [TestCleanup]
        public void Cleanup()
        {
            sectionsController = null;
            sectionRepository = null;
            studentRepository = null;
            sectionCoordinationService = null;
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PutHedmSectionAsync_Exception()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                .Setup(svc => svc.PutSection4Async(It.IsAny<Dtos.Section4>()))
                .Throws<Exception>();
            await sectionsController.PutHedmSection4Async(section.Id, section);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PutHedmSectionAsync_ConfigurationException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                .Setup(svc => svc.PutSection4Async(It.IsAny<Dtos.Section4>()))
                .Throws<ConfigurationException>();
            await sectionsController.PutHedmSection4Async(section.Id, section);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PutHedmSectionAsync_PermissionException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                .Setup(svc => svc.PutSection4Async(It.IsAny<Dtos.Section4>()))
                .Throws<PermissionsException>();
            await sectionsController.PutHedmSection4Async(section.Id, section);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PutHedmSectionAsync_RepoException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                .Setup(svc => svc.PutSection4Async(It.IsAny<Dtos.Section4>()))
                .Throws<RepositoryException>();
            await sectionsController.PutHedmSection4Async(section.Id, section);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PutHedmSectionAsync_ArgException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                .Setup(svc => svc.PutSection4Async(It.IsAny<Dtos.Section4>()))
                .Throws<ArgumentException>();
            await sectionsController.PutHedmSection4Async(section.Id, section);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PutHedmSectionAsync_NilGUID_ArgException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                .Setup(svc => svc.PutSection4Async(It.IsAny<Dtos.Section4>()))
                .Throws<ArgumentException>();
            await sectionsController.PutHedmSection4Async(Guid.Empty.ToString(), section);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PutHedmSectionAsync_SectionIdNull_ArgException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                .Setup(svc => svc.PutSection4Async(It.IsAny<Dtos.Section4>()))
                .Throws<ArgumentException>();
            await sectionsController.PutHedmSection4Async("1234", new Dtos.Section4() { Id = "" });
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PutHedmSectionAsync_IntegrationApiException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                .Setup(svc => svc.PutSection4Async(It.IsAny<Dtos.Section4>()))
                .Throws<IntegrationApiException>();
            await sectionsController.PutHedmSection4Async(section.Id, section);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PutHedmSectionAsync_NullIDException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            await sectionsController.PutHedmSection4Async(null, section);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PutHedmSectionAsync_NullSectionException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            await sectionsController.PutHedmSection4Async(firstSectionGuid, null);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_PutHedmSectionAsync_InvalidException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            await sectionsController.PutHedmSection4Async(secondSectionGuid, section);
        }

        [TestMethod]
        public async Task SectionsController_PutHedmSectionAsync()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock.Setup(svc => svc.PutSection4Async(section)).ReturnsAsync(section);
            var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

            var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
            Ellucian.Colleague.Domain.Student.Entities.Course course = null;
            if (section.Course != null)
                course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

            var sectionByGuid = await sectionsController.PutHedmSection4Async(firstSectionGuid, section);
            Assert.AreEqual(section.Id, sectionByGuid.Id);
            Assert.AreEqual(section.Title, sectionByGuid.Title);
            Assert.AreEqual(section.Description, sectionByGuid.Description);
            Assert.AreEqual(academicLevel.Guid, sectionByGuid.AcademicLevels.FirstOrDefault().Id);
            Assert.AreEqual(gradeScheme.Guid, sectionByGuid.GradeSchemes.FirstOrDefault().Id);
            Assert.AreEqual(section.StartOn, sectionByGuid.StartOn);
            Assert.AreEqual(section.EndOn, sectionByGuid.EndOn);
            if (course != null && sectionByGuid.Course != null)
                Assert.AreEqual(course.Guid, sectionByGuid.Course.Id);
            Assert.AreEqual(section.Number, sectionByGuid.Number);
            Assert.AreEqual(section.MaximumEnrollment, sectionByGuid.MaximumEnrollment);

            var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
            if (courseLevel != null)
                Assert.AreEqual(courseLevel.Guid, sectionByGuid.CourseLevels[0].Id);
        }
    }

    [TestClass]
    public class SectionsControllerTestsGetHedmSectionById_V6
    {

        #region Test Context

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #endregion

        private IAdapterRegistry adapterRegistry;
        private SectionsController sectionsController;
        private Mock<ISectionRepository> sectionRepoMock;
        private ISectionRepository sectionRepository;
        private Mock<IStudentRepository> studentRepoMock;
        private IStudentRepository studentRepository;
        private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
        private ISectionCoordinationService sectionCoordinationService;
        private ILogger logger;
        private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
        private List<string> levels = new List<string>();

        private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
        private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
        private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

        private string firstSectionGuid = "eab43b50-ce12-4e7f-8947-2d63661ab7ac";
        private string secondSectionGuid = "51adca90-4839-442b-b22a-12c8009b1186";
        private string thirdSectionGuid = "cd74617e-d304-4c6d-8598-6622ed15192a";

        private Ellucian.Colleague.Domain.Student.Entities.Student firstStudent;
        private Ellucian.Colleague.Domain.Student.Entities.Student secondStudent;
        private Ellucian.Colleague.Domain.Student.Entities.Student thirdStudent;

        private List<Dtos.Section4> allSectionDtos;
        private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
        private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
        private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
        private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
            logger = new Mock<ILogger>().Object;

            sectionRepoMock = new Mock<ISectionRepository>();
            sectionRepository = sectionRepoMock.Object;

            studentRepoMock = new Mock<IStudentRepository>();
            studentRepository = studentRepoMock.Object;

            sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
            sectionCoordinationService = sectionCoordinationServiceMock.Object;

            allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
            allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
            if (allCourseLevels != null)
                levels.Add(allCourseLevels[0].Code);

            firstStudent = new Domain.Student.Entities.Student("STU1", "Gamgee", null, new List<string>(), new List<string>());
            firstStudent.FirstName = "Samwise";
            secondStudent = new Domain.Student.Entities.Student("STU2", "Took", null, new List<string>(), new List<string>());
            secondStudent.FirstName = "Peregrin";
            thirdStudent = new Domain.Student.Entities.Student("STU1", "Baggins", null, new List<string>(), new List<string>());
            thirdStudent.FirstName = "Frodo"; thirdStudent.MiddleName = "Ring-bearer";

            adapterRegistry = adapterRegistryMock.Object;

            List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
            var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

            firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
            firstSection.TermId = "2012/FA";
            firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
            firstSection.Guid = firstSectionGuid;
            sectionEntityList.Add(firstSection);

            secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
            secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
            secondSection.Guid = secondSectionGuid;
            sectionEntityList.Add(secondSection);

            thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
            thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
            thirdSection.EndDate = new DateTime(2011, 12, 21);
            thirdSection.Guid = thirdSectionGuid;
            sectionEntityList.Add(thirdSection);

            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
            IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

            sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
            sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
            sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());


            // Mock section repo GetCachedSections
            bool bestFit = false;
            sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
            // Mock section repo GetNonCachedSections
            sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
            // mock DTO adapters
            var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
            adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

            academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
            allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

            allSectionDtos = new List<Dtos.Section4>();
            foreach (var sectionEntity in sectionEntityList)
            {
                //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section4>(adapterRegistry, logger);
                Ellucian.Colleague.Dtos.Section4 target = sectionDtoAdapter.MapToType(sectionEntity);
                target.Id = sectionEntity.Guid;
                Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                target.AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = academicLevel.Guid } };

                Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                target.GradeSchemes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = gradeScheme.Guid } };

                var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                if (course != null) target.Course = new Dtos.GuidObject2 { Id = course.Guid };

                if (sectionEntity.CourseLevelCodes != null)
                {
                    var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                    target.CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = courseLevel.Guid } };
                }
                allSectionDtos.Add(target);
            }


            sectionCoordinationServiceMock.Setup(s => s.GetDataPrivacyListByApi(It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(new List<string>());
            // mock controller
            sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger)
            {
                Request = new HttpRequestMessage()
            };
            sectionsController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
        }

        [TestCleanup]
        public void Cleanup()
        {
            sectionsController = null;
            sectionRepository = null;
            studentRepository = null;
            sectionCoordinationService = null;
        }


        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_GetHedmSectionByGuid_Exception()
        {
            sectionCoordinationServiceMock.Setup(svc => svc.GetSection4ByGuidAsync(It.IsAny<string>())).Throws<Exception>();
            await sectionsController.GetHedmSectionByGuid2Async(It.IsAny<string>());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_GetHedmSectionByGuid_ConfigurationException()
        {
            sectionCoordinationServiceMock
                .Setup(c => c.GetSection4ByGuidAsync(It.IsAny<string>()))
                .ThrowsAsync(new ConfigurationException());
            await sectionsController.GetHedmSectionByGuid2Async(It.IsAny<string>());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_GetHedmSectionByGuid_PermissionException()
        {
            sectionCoordinationServiceMock
                .Setup(c => c.GetSection4ByGuidAsync(It.IsAny<string>()))
                .ThrowsAsync(new PermissionsException());
            await sectionsController.GetHedmSectionByGuid2Async(It.IsAny<string>());
        }


        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_GetHedmSectionByGuid_ArgNullException()
        {
            sectionCoordinationServiceMock
                .Setup(c => c.GetSection4ByGuidAsync(It.IsAny<string>()))
                .ThrowsAsync(new ArgumentNullException());
            await sectionsController.GetHedmSectionByGuid2Async("");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_GetHedmSectionByGuid_RepoException()
        {
            sectionCoordinationServiceMock
                .Setup(c => c.GetSection4ByGuidAsync(It.IsAny<string>()))
                .ThrowsAsync(new RepositoryException());
            await sectionsController.GetHedmSectionByGuid2Async("");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_GetHedmSectionByGuid_IntegrationApiException()
        {
            sectionCoordinationServiceMock
                .Setup(c => c.GetSection4ByGuidAsync(It.IsAny<string>()))
                .ThrowsAsync(new IntegrationApiException());
            await sectionsController.GetHedmSectionByGuid2Async("");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_GetHedmSectionByGuid_ConfigurationExceptionException()
        {
            sectionCoordinationServiceMock
                .Setup(c => c.GetSection4ByGuidAsync(It.IsAny<string>()))
                .ThrowsAsync(new ConfigurationException());
            await sectionsController.GetHedmSectionByGuid2Async("");
        }

        [TestMethod]
        public async Task SectionsController_GetHedmSectionByGuid()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock.Setup(svc => svc.GetSection4ByGuidAsync(It.IsAny<string>())).ReturnsAsync(section);
            var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

            var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
            Ellucian.Colleague.Domain.Student.Entities.Course course = null;
            if (section.Course != null)
                course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

            var sectionByGuid = await sectionsController.GetHedmSectionByGuid3Async(firstSectionGuid);
            Assert.AreEqual(section.Id, sectionByGuid.Id);
            Assert.AreEqual(section.Title, sectionByGuid.Title);
            Assert.AreEqual(section.Description, sectionByGuid.Description);
            Assert.AreEqual(academicLevel.Guid, sectionByGuid.AcademicLevels.FirstOrDefault().Id);
            Assert.AreEqual(gradeScheme.Guid, sectionByGuid.GradeSchemes.FirstOrDefault().Id);
            Assert.AreEqual(section.StartOn, sectionByGuid.StartOn);
            Assert.AreEqual(section.EndOn, sectionByGuid.EndOn);
            if (course != null && sectionByGuid.Course != null)
                Assert.AreEqual(course.Guid, sectionByGuid.Course.Id);
            Assert.AreEqual(section.Number, sectionByGuid.Number);
            Assert.AreEqual(section.MaximumEnrollment, sectionByGuid.MaximumEnrollment);

            var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
            if (courseLevel != null)
                Assert.AreEqual(courseLevel.Guid, sectionByGuid.CourseLevels[0].Id);

        }

    }

    [TestClass]
    public class SectionsControllerTestsDeleteHedmSectionById_V6
    {

        #region Test Context

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #endregion

        private IAdapterRegistry adapterRegistry;
        private SectionsController sectionsController;
        private Mock<ISectionRepository> sectionRepoMock;
        private ISectionRepository sectionRepository;
        private Mock<IStudentRepository> studentRepoMock;
        private IStudentRepository studentRepository;
        private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
        private ISectionCoordinationService sectionCoordinationService;
        private ILogger logger;
        private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
        private List<string> levels = new List<string>();

        private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
        private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
        private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

        private string firstSectionGuid = "eab43b50-ce12-4e7f-8947-2d63661ab7ac";
        private string secondSectionGuid = "51adca90-4839-442b-b22a-12c8009b1186";
        private string thirdSectionGuid = "cd74617e-d304-4c6d-8598-6622ed15192a";

        private Ellucian.Colleague.Domain.Student.Entities.Student firstStudent;
        private Ellucian.Colleague.Domain.Student.Entities.Student secondStudent;
        private Ellucian.Colleague.Domain.Student.Entities.Student thirdStudent;

        private List<Dtos.Section4> allSectionDtos;
        private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
        private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
        private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
        private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
            logger = new Mock<ILogger>().Object;

            sectionRepoMock = new Mock<ISectionRepository>();
            sectionRepository = sectionRepoMock.Object;

            studentRepoMock = new Mock<IStudentRepository>();
            studentRepository = studentRepoMock.Object;

            sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
            sectionCoordinationService = sectionCoordinationServiceMock.Object;

            allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
            allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
            if (allCourseLevels != null)
                levels.Add(allCourseLevels[0].Code);

            firstStudent = new Domain.Student.Entities.Student("STU1", "Gamgee", null, new List<string>(), new List<string>());
            firstStudent.FirstName = "Samwise";
            secondStudent = new Domain.Student.Entities.Student("STU2", "Took", null, new List<string>(), new List<string>());
            secondStudent.FirstName = "Peregrin";
            thirdStudent = new Domain.Student.Entities.Student("STU1", "Baggins", null, new List<string>(), new List<string>());
            thirdStudent.FirstName = "Frodo"; thirdStudent.MiddleName = "Ring-bearer";

            adapterRegistry = adapterRegistryMock.Object;

            List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
            var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

            firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
            firstSection.TermId = "2012/FA";
            firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
            firstSection.Guid = firstSectionGuid;
            sectionEntityList.Add(firstSection);

            secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
            secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
            secondSection.Guid = secondSectionGuid;
            sectionEntityList.Add(secondSection);

            thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
            thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
            thirdSection.EndDate = new DateTime(2011, 12, 21);
            thirdSection.Guid = thirdSectionGuid;
            sectionEntityList.Add(thirdSection);

            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
            IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

            sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
            sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
            sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());


            // Mock section repo GetCachedSections
            bool bestFit = false;
            sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
            // Mock section repo GetNonCachedSections
            sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
            // mock DTO adapters
            var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
            adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

            academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
            allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

            allSectionDtos = new List<Dtos.Section4>();
            foreach (var sectionEntity in sectionEntityList)
            {
                //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section4>(adapterRegistry, logger);
                Ellucian.Colleague.Dtos.Section4 target = sectionDtoAdapter.MapToType(sectionEntity);
                target.Id = sectionEntity.Guid;
                Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                target.AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = academicLevel.Guid } };

                Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                target.GradeSchemes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = gradeScheme.Guid } };

                var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                if (course != null) target.Course = new Dtos.GuidObject2 { Id = course.Guid };

                if (sectionEntity.CourseLevelCodes != null)
                {
                    var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                    target.CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = courseLevel.Guid } };
                }
                allSectionDtos.Add(target);
            }


            // mock controller
            sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger);
        }

        [TestCleanup]
        public void Cleanup()
        {
            sectionsController = null;
            sectionRepository = null;
            studentRepository = null;
            sectionCoordinationService = null;
        }


        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_DeleteHedmSectionByGuid_Exception()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock.Setup(svc => svc.GetSection4ByGuidAsync(It.IsAny<string>())).Throws<Exception>();
            await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_DeleteHedmSectionByGuid_ConfigurationException()
        {
            sectionCoordinationServiceMock
                .Setup(c => c.GetSection4ByGuidAsync(It.IsAny<string>()))
                .ThrowsAsync(new ConfigurationException());
            await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_DeleteHedmSectionByGuid_PermissionException()
        {
            sectionCoordinationServiceMock
                .Setup(c => c.GetSection4ByGuidAsync(It.IsAny<string>()))
                .ThrowsAsync(new PermissionsException());
            await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
        }


        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_DeleteHedmSectionByGuid_ArgNullException()
        {
            sectionCoordinationServiceMock
                .Setup(c => c.GetSection4ByGuidAsync(It.IsAny<string>()))
                .ThrowsAsync(new ArgumentNullException());
            await sectionsController.DeleteHedmSectionByGuid2Async("");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_DeleteHedmSectionByGuid_ArgException()
        {
            sectionCoordinationServiceMock
                .Setup(c => c.GetSection4ByGuidAsync(It.IsAny<string>()))
                .ThrowsAsync(new ArgumentException());
            await sectionsController.DeleteHedmSectionByGuid2Async("");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_DeleteHedmSectionByGuid_RepoException()
        {
            sectionCoordinationServiceMock
                .Setup(c => c.GetSection4ByGuidAsync(It.IsAny<string>()))
                .ThrowsAsync(new RepositoryException());
            await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_DeleteHedmSectionByGuid_IntegrationApiException()
        {
            sectionCoordinationServiceMock
                .Setup(c => c.GetSection4ByGuidAsync(It.IsAny<string>()))
                .ThrowsAsync(new IntegrationApiException());
            await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_DeleteHedmSectionByGuid_ConfigurationExceptionException()
        {
            sectionCoordinationServiceMock
                .Setup(c => c.GetSection4ByGuidAsync(It.IsAny<string>()))
                .ThrowsAsync(new ConfigurationException());
            await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_DeleteHedmSectionByGuid_GuidException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
            section.Id = secondSectionGuid;
            sectionCoordinationServiceMock.Setup(svc => svc.GetSection4ByGuidAsync(firstSectionGuid)).ReturnsAsync(section);
            await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_DeleteHedmSectionByGuid_NullException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock.Setup(svc => svc.GetSection4ByGuidAsync(firstSectionGuid)).ReturnsAsync(null);
            await sectionsController.DeleteHedmSectionByGuid2Async(firstSectionGuid);
        }


    }

    [TestClass]
    public class SectionsControllerTestsGetHedmSection_V6
    {

        #region Test Context

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #endregion

        private IAdapterRegistry adapterRegistry;
        private SectionsController sectionsController;
        private Mock<ISectionRepository> sectionRepoMock;
        private ISectionRepository sectionRepository;
        private Mock<IStudentRepository> studentRepoMock;
        private IStudentRepository studentRepository;
        private Mock<ISectionCoordinationService> sectionCoordinationServiceMock;
        private ISectionCoordinationService sectionCoordinationService;
        private ILogger logger;
        private List<Domain.Student.Entities.OfferingDepartment> dpts = new List<Domain.Student.Entities.OfferingDepartment>() { new Domain.Student.Entities.OfferingDepartment("ART", 100m) };
        private List<string> levels = new List<string>();

        private Ellucian.Colleague.Domain.Student.Entities.Section firstSection;
        private Ellucian.Colleague.Domain.Student.Entities.Section secondSection;
        private Ellucian.Colleague.Domain.Student.Entities.Section thirdSection;

        private string firstSectionGuid = "eab43b50-ce12-4e7f-8947-2d63661ab7ac";
        private string secondSectionGuid = "51adca90-4839-442b-b22a-12c8009b1186";
        private string thirdSectionGuid = "cd74617e-d304-4c6d-8598-6622ed15192a";

        private Ellucian.Colleague.Domain.Student.Entities.Student firstStudent;
        private Ellucian.Colleague.Domain.Student.Entities.Student secondStudent;
        private Ellucian.Colleague.Domain.Student.Entities.Student thirdStudent;

        private List<Dtos.Section4> allSectionDtos;
        private List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel> academicLevels;
        private List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme> allGradeSchemes;
        private List<Ellucian.Colleague.Domain.Student.Entities.Course> allCourses;
        private List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel> allCourseLevels;
        private Ellucian.Web.Http.Models.Paging paging = new Web.Http.Models.Paging(3, 0);

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();
            logger = new Mock<ILogger>().Object;

            sectionRepoMock = new Mock<ISectionRepository>();
            sectionRepository = sectionRepoMock.Object;

            studentRepoMock = new Mock<IStudentRepository>();
            studentRepository = studentRepoMock.Object;

            sectionCoordinationServiceMock = new Mock<ISectionCoordinationService>();
            sectionCoordinationService = sectionCoordinationServiceMock.Object;

            allCourses = new TestCourseRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.Course>;
            allCourseLevels = new TestCourseLevelRepository().Get() as List<Ellucian.Colleague.Domain.Student.Entities.CourseLevel>;
            if (allCourseLevels != null)
                levels.Add(allCourseLevels[0].Code);

            firstStudent = new Domain.Student.Entities.Student("STU1", "Gamgee", null, new List<string>(), new List<string>());
            firstStudent.FirstName = "Samwise";
            secondStudent = new Domain.Student.Entities.Student("STU2", "Took", null, new List<string>(), new List<string>());
            secondStudent.FirstName = "Peregrin";
            thirdStudent = new Domain.Student.Entities.Student("STU1", "Baggins", null, new List<string>(), new List<string>());
            thirdStudent.FirstName = "Frodo"; thirdStudent.MiddleName = "Ring-bearer";

            adapterRegistry = adapterRegistryMock.Object;

            List<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntityList = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
            var statuses = new List<Domain.Student.Entities.SectionStatusItem>() { new Domain.Student.Entities.SectionStatusItem(Domain.Student.Entities.SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) };

            firstSection = new Ellucian.Colleague.Domain.Student.Entities.Section("1", allCourses[0].Name, "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
            firstSection.TermId = "2012/FA";
            firstSection.EndDate = new DateTime(2012, 12, 21); firstSection.AddActiveStudent("STU1");
            firstSection.Guid = firstSectionGuid;
            sectionEntityList.Add(firstSection);

            secondSection = new Ellucian.Colleague.Domain.Student.Entities.Section("2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
            secondSection.TermId = "2012/FA"; secondSection.AddActiveStudent("STU1"); secondSection.AddActiveStudent("STU2");
            secondSection.Guid = secondSectionGuid;
            sectionEntityList.Add(secondSection);

            thirdSection = new Ellucian.Colleague.Domain.Student.Entities.Section("3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses);
            thirdSection.TermId = "2011/FA"; thirdSection.AddActiveStudent("STU1"); thirdSection.AddActiveStudent("STU2"); thirdSection.AddActiveStudent("STU3");
            thirdSection.EndDate = new DateTime(2011, 12, 21);
            thirdSection.Guid = thirdSectionGuid;
            sectionEntityList.Add(thirdSection);

            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sections = sectionEntityList;
            IEnumerable<string> listOfIds = new List<string>() { "1", "2", "3" };

            sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("99")).Throws(new ArgumentException());
            sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("98")).Throws(new PermissionsException());
            sectionCoordinationServiceMock.Setup(svc => svc.GetSectionRosterAsync("")).Throws(new ArgumentNullException());


            // Mock section repo GetCachedSections
            bool bestFit = false;
            sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
            // Mock section repo GetNonCachedSections
            sectionRepoMock.Setup(repo => repo.GetNonCachedSectionsAsync(listOfIds, bestFit)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sections));
            // mock DTO adapters
            var sectionAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>(adapterRegistry, logger);
            adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>()).Returns(sectionAdapter);

            academicLevels = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
            allGradeSchemes = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;

            allSectionDtos = new List<Dtos.Section4>();
            foreach (var sectionEntity in sectionEntityList)
            {
                //Ellucian.Colleague.Dtos.Section target = Mapper.Map<Ellucian.Colleague.Domain.Student.Entities.Section, Ellucian.Colleague.Dtos.Section>(entity);
                var sectionDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Dtos.Section4>(adapterRegistry, logger);
                Ellucian.Colleague.Dtos.Section4 target = sectionDtoAdapter.MapToType(sectionEntity);
                target.Id = sectionEntity.Guid;
                Ellucian.Colleague.Domain.Student.Entities.AcademicLevel academicLevel = academicLevels.FirstOrDefault(x => x.Code == sectionEntity.AcademicLevelCode);
                target.AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = academicLevel.Guid } };

                Ellucian.Colleague.Domain.Student.Entities.GradeScheme gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);

                target.GradeSchemes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = gradeScheme.Guid } };

                var course = allCourses.FirstOrDefault(x => x.Name == sectionEntity.CourseId);
                if (course != null) target.Course = new Dtos.GuidObject2 { Id = course.Guid };

                if (sectionEntity.CourseLevelCodes != null)
                {
                    var courseLevel = allCourseLevels.FirstOrDefault(x => x.Code == sectionEntity.CourseLevelCodes[0]);
                    target.CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2() { Id = courseLevel.Guid } };
                }
                allSectionDtos.Add(target);
            }

            sectionCoordinationServiceMock.Setup(s => s.GetDataPrivacyListByApi(It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(new List<string>());
            // mock controller
            sectionsController = new SectionsController(adapterRegistry, sectionRepository, sectionCoordinationService, null, null, logger)
            {
                Request = new HttpRequestMessage()
            };
            sectionsController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
        }

        [TestCleanup]
        public void Cleanup()
        {
            sectionsController = null;
            sectionRepository = null;
            studentRepository = null;
            sectionCoordinationService = null;
        }


        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_GetHedmSections_Exception()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                .Throws<Exception>();
            await sectionsController.GetHedmSections2Async(paging, section.Title);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_GetHedmSections_ConfigurationException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                    .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                .ThrowsAsync(new ConfigurationException());
            await sectionsController.GetHedmSections2Async(paging, section.Title);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_GetHedmSections_PermissionException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                    .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                .ThrowsAsync(new PermissionsException());
            await sectionsController.GetHedmSections2Async(paging, section.Title);
        }


        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_GetHedmSections_ArgNullException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                    .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                .ThrowsAsync(new ArgumentNullException());
            await sectionsController.GetHedmSections2Async(paging, section.Title);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_GetHedmSections_RepoException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                    .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                .ThrowsAsync(new RepositoryException());
            await sectionsController.GetHedmSections2Async(paging, section.Title);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_GetHedmSections_IntegrationApiException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                    .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                .ThrowsAsync(new IntegrationApiException());
            await sectionsController.GetHedmSections2Async(paging, section.Title);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_GetHedmSections_ConfigurationExceptionException()
        {
            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);

            sectionCoordinationServiceMock
                    .Setup(svc => svc.GetSections2Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", ""))
                .ThrowsAsync(new ConfigurationException());
            await sectionsController.GetHedmSections2Async(paging, section.Title);
        }

        [TestMethod]
        public async Task SectionsController_GetHedmSectionByTitle()
        {
            sectionsController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };

            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
            var tuple = new Tuple<IEnumerable<Dtos.Section4>, int>(new List<Dtos.Section4>() { section }, 5);

            sectionCoordinationServiceMock.Setup(svc => svc.GetSection4ByGuidAsync(It.IsAny<string>())).ReturnsAsync(section);
            sectionCoordinationServiceMock.Setup(svc => svc.GetSections4Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", "", "", "", SectionsSearchable.NotSet, It.IsAny<string>()))
                .ReturnsAsync(tuple);
            var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

            var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
            Ellucian.Colleague.Domain.Student.Entities.Course course = null;
            if (section.Course != null)
                course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

            char quote = '"';
            string criteriaString = "["+ quote + "title" + quote + ":" + quote + section.Title + quote + "]";

            Dictionary<String, String> criterion = new Dictionary<string, string>();
            criterion.Add("title", section.Title);
            var criteriaJson = Newtonsoft.Json.JsonConvert.SerializeObject(criterion);





            var sectionByTitle = await sectionsController.GetHedmSections4Async(paging, criteria: criteriaJson);

            var cancelToken = new System.Threading.CancellationToken(false);

            System.Net.Http.HttpResponseMessage httpResponseMessage = await sectionByTitle.ExecuteAsync(cancelToken);

            IEnumerable<Dtos.Section4> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.Section4>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.Section4>;

            var result = results.FirstOrDefault();

            Assert.IsTrue(sectionByTitle is IHttpActionResult);

            Assert.AreEqual(section.Id, result.Id);
            Assert.AreEqual(section.Title, result.Title);
            Assert.AreEqual(section.Description, result.Description);
            Assert.AreEqual(academicLevel.Guid, result.AcademicLevels.FirstOrDefault().Id);
            Assert.AreEqual(gradeScheme.Guid, result.GradeSchemes.FirstOrDefault().Id);
            Assert.AreEqual(section.StartOn, result.StartOn);
            Assert.AreEqual(section.EndOn, result.EndOn);
            if (course != null && result.Course != null)
                Assert.AreEqual(course.Guid, result.Course.Id);
            Assert.AreEqual(section.Number, result.Number);
            Assert.AreEqual(section.MaximumEnrollment, result.MaximumEnrollment);

            var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
            if (courseLevel != null)
                Assert.AreEqual(courseLevel.Guid, result.CourseLevels[0].Id);

        }

        [TestMethod]
        public async Task SectionsController_GetHedmSectionByStatus()
        {
            sectionsController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };

            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
            var tuple = new Tuple<IEnumerable<Dtos.Section4>, int>(new List<Dtos.Section4>() { section }, 5);

            sectionCoordinationServiceMock.Setup(svc => 
                svc.GetSection4ByGuidAsync(It.IsAny<string>())).ReturnsAsync(section);
            sectionCoordinationServiceMock.Setup(svc =>
                svc.GetSections4Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), 
                It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), SectionsSearchable.NotSet, It.IsAny<string>()))
                .ReturnsAsync(tuple);
            var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

            var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
            Ellucian.Colleague.Domain.Student.Entities.Course course = null;
            if (section.Course != null)
                course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

            char quote = '"';
            string criteriaString = "[" + quote + "status" + quote + ":" + quote + "open" + quote + "]";

            Dictionary<String, String> criterion = new Dictionary<string, string>();
            criterion.Add("status", "open");
            var criteriaJson = Newtonsoft.Json.JsonConvert.SerializeObject(criterion);

            var sectionByTitle = await sectionsController.GetHedmSections4Async(paging, criteria: criteriaJson);

            var cancelToken = new System.Threading.CancellationToken(false);

            System.Net.Http.HttpResponseMessage httpResponseMessage = await sectionByTitle.ExecuteAsync(cancelToken);

            IEnumerable<Dtos.Section4> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.Section4>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.Section4>;

            var result = results.FirstOrDefault();

            Assert.IsTrue(sectionByTitle is IHttpActionResult);

            Assert.AreEqual(section.Id, result.Id);
            Assert.AreEqual(section.Title, result.Title);
            Assert.AreEqual(section.Description, result.Description);
            Assert.AreEqual(academicLevel.Guid, result.AcademicLevels.FirstOrDefault().Id);
            Assert.AreEqual(gradeScheme.Guid, result.GradeSchemes.FirstOrDefault().Id);
            Assert.AreEqual(section.StartOn, result.StartOn);
            Assert.AreEqual(section.EndOn, result.EndOn);
            if (course != null && result.Course != null)
                Assert.AreEqual(course.Guid, result.Course.Id);
            Assert.AreEqual(section.Number, result.Number);
            Assert.AreEqual(section.MaximumEnrollment, result.MaximumEnrollment);

            var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
            if (courseLevel != null)
                Assert.AreEqual(courseLevel.Guid, result.CourseLevels[0].Id);

        }

        
        [TestMethod]
        public async Task SectionsController_GetHedmSections4_Searchable_No()
        {
            sectionsController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };

            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
            var tuple = new Tuple<IEnumerable<Dtos.Section4>, int>(new List<Dtos.Section4>() { section }, 5);

            sectionCoordinationServiceMock.Setup(svc => svc.GetSection4ByGuidAsync(It.IsAny<string>())).ReturnsAsync(section);
            sectionCoordinationServiceMock.Setup(svc => svc.GetSections4Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", "", "", "", SectionsSearchable.No, It.IsAny<string>()))
                .ReturnsAsync(tuple);
            var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

            var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
            Ellucian.Colleague.Domain.Student.Entities.Course course = null;
            if (section.Course != null)
                course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

            char quote = '"';
            string criteria = "[\"no\"]";

            //Dictionary<String, String> criterion = new Dictionary<string, string>();
            //criterion.Add("title", section.Title);
            //var criteriaJson = Newtonsoft.Json.JsonConvert.SerializeObject(criterion);

            var sectionByTitle = await sectionsController.GetHedmSections4Async(paging, searchable: criteria);

            var cancelToken = new System.Threading.CancellationToken(false);

            System.Net.Http.HttpResponseMessage httpResponseMessage = await sectionByTitle.ExecuteAsync(cancelToken);

            IEnumerable<Dtos.Section4> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.Section4>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.Section4>;

            var result = results.FirstOrDefault();

            Assert.IsTrue(sectionByTitle is IHttpActionResult);

            Assert.AreEqual(section.Id, result.Id);
            Assert.AreEqual(section.Title, result.Title);
            Assert.AreEqual(section.Description, result.Description);
            Assert.AreEqual(academicLevel.Guid, result.AcademicLevels.FirstOrDefault().Id);
            Assert.AreEqual(gradeScheme.Guid, result.GradeSchemes.FirstOrDefault().Id);
            Assert.AreEqual(section.StartOn, result.StartOn);
            Assert.AreEqual(section.EndOn, result.EndOn);
            if (course != null && result.Course != null)
                Assert.AreEqual(course.Guid, result.Course.Id);
            Assert.AreEqual(section.Number, result.Number);
            Assert.AreEqual(section.MaximumEnrollment, result.MaximumEnrollment);

            var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
            if (courseLevel != null)
                Assert.AreEqual(courseLevel.Guid, result.CourseLevels[0].Id);

        }

        [TestMethod]
        public async Task SectionsController_GetHedmSections4_Searchable_Yes()
        {
            sectionsController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };

            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
            var tuple = new Tuple<IEnumerable<Dtos.Section4>, int>(new List<Dtos.Section4>() { section }, 5);

            sectionCoordinationServiceMock.Setup(svc => svc.GetSection4ByGuidAsync(It.IsAny<string>())).ReturnsAsync(section);
            sectionCoordinationServiceMock.Setup(svc => svc.GetSections4Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", "", "", "", SectionsSearchable.Yes, It.IsAny<string>()))
                .ReturnsAsync(tuple);
            var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

            var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
            Ellucian.Colleague.Domain.Student.Entities.Course course = null;
            if (section.Course != null)
                course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

            char quote = '"';
            string criteria = "[\"yes\"]";

            //Dictionary<String, String> criterion = new Dictionary<string, string>();
            //criterion.Add("title", section.Title);
            //var criteriaJson = Newtonsoft.Json.JsonConvert.SerializeObject(criterion);

            var sectionByTitle = await sectionsController.GetHedmSections4Async(paging, searchable: criteria);

            var cancelToken = new System.Threading.CancellationToken(false);

            System.Net.Http.HttpResponseMessage httpResponseMessage = await sectionByTitle.ExecuteAsync(cancelToken);

            IEnumerable<Dtos.Section4> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.Section4>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.Section4>;

            var result = results.FirstOrDefault();

            Assert.IsTrue(sectionByTitle is IHttpActionResult);

            Assert.AreEqual(section.Id, result.Id);
            Assert.AreEqual(section.Title, result.Title);
            Assert.AreEqual(section.Description, result.Description);
            Assert.AreEqual(academicLevel.Guid, result.AcademicLevels.FirstOrDefault().Id);
            Assert.AreEqual(gradeScheme.Guid, result.GradeSchemes.FirstOrDefault().Id);
            Assert.AreEqual(section.StartOn, result.StartOn);
            Assert.AreEqual(section.EndOn, result.EndOn);
            if (course != null && result.Course != null)
                Assert.AreEqual(course.Guid, result.Course.Id);
            Assert.AreEqual(section.Number, result.Number);
            Assert.AreEqual(section.MaximumEnrollment, result.MaximumEnrollment);

            var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
            if (courseLevel != null)
                Assert.AreEqual(courseLevel.Guid, result.CourseLevels[0].Id);

        }

        [TestMethod]
        public async Task SectionsController_GetHedmSections4_Searchable_Hidden()
        {
            sectionsController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };

            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
            var tuple = new Tuple<IEnumerable<Dtos.Section4>, int>(new List<Dtos.Section4>() { section }, 5);

            sectionCoordinationServiceMock.Setup(svc => svc.GetSection4ByGuidAsync(It.IsAny<string>())).ReturnsAsync(section);
            sectionCoordinationServiceMock.Setup(svc => svc.GetSections4Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", "", "", "", SectionsSearchable.Hidden, It.IsAny<string>()))
                .ReturnsAsync(tuple);
            var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

            var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
            Ellucian.Colleague.Domain.Student.Entities.Course course = null;
            if (section.Course != null)
                course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

            char quote = '"';
            string criteria = "[\"hidden\"]";

            //Dictionary<String, String> criterion = new Dictionary<string, string>();
            //criterion.Add("title", section.Title);
            //var criteriaJson = Newtonsoft.Json.JsonConvert.SerializeObject(criterion);

            var sectionByTitle = await sectionsController.GetHedmSections4Async(paging, searchable: criteria);

            var cancelToken = new System.Threading.CancellationToken(false);

            System.Net.Http.HttpResponseMessage httpResponseMessage = await sectionByTitle.ExecuteAsync(cancelToken);

            IEnumerable<Dtos.Section4> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.Section4>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.Section4>;

            var result = results.FirstOrDefault();

            Assert.IsTrue(sectionByTitle is IHttpActionResult);

            Assert.AreEqual(section.Id, result.Id);
            Assert.AreEqual(section.Title, result.Title);
            Assert.AreEqual(section.Description, result.Description);
            Assert.AreEqual(academicLevel.Guid, result.AcademicLevels.FirstOrDefault().Id);
            Assert.AreEqual(gradeScheme.Guid, result.GradeSchemes.FirstOrDefault().Id);
            Assert.AreEqual(section.StartOn, result.StartOn);
            Assert.AreEqual(section.EndOn, result.EndOn);
            if (course != null && result.Course != null)
                Assert.AreEqual(course.Guid, result.Course.Id);
            Assert.AreEqual(section.Number, result.Number);
            Assert.AreEqual(section.MaximumEnrollment, result.MaximumEnrollment);

            var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
            if (courseLevel != null)
                Assert.AreEqual(courseLevel.Guid, result.CourseLevels[0].Id);

        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task SectionsController_GetHedmSections4_Searchable_Invalid()
        {
            sectionsController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };

            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
            var tuple = new Tuple<IEnumerable<Dtos.Section4>, int>(new List<Dtos.Section4>() { section }, 5);

            sectionCoordinationServiceMock.Setup(svc => svc.GetSection4ByGuidAsync(It.IsAny<string>())).ReturnsAsync(section);
            sectionCoordinationServiceMock.Setup(svc => svc.GetSections4Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", "", "", "", SectionsSearchable.Hidden, It.IsAny<string>()))
                .ReturnsAsync(tuple);
            var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

            var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
            Ellucian.Colleague.Domain.Student.Entities.Course course = null;
            if (section.Course != null)
                course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

            char quote = '"';
            string criteria = "[\"invalid\"]";

            //Dictionary<String, String> criterion = new Dictionary<string, string>();
            //criterion.Add("title", section.Title);
            //var criteriaJson = Newtonsoft.Json.JsonConvert.SerializeObject(criterion);

            var sectionByTitle = await sectionsController.GetHedmSections4Async(paging, searchable: criteria);

            var cancelToken = new System.Threading.CancellationToken(false);

            System.Net.Http.HttpResponseMessage httpResponseMessage = await sectionByTitle.ExecuteAsync(cancelToken);

            IEnumerable<Dtos.Section4> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.Section4>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.Section4>;

            var result = results.FirstOrDefault();

            Assert.IsTrue(sectionByTitle is IHttpActionResult);

            Assert.AreEqual(section.Id, result.Id);
            Assert.AreEqual(section.Title, result.Title);
            Assert.AreEqual(section.Description, result.Description);
            Assert.AreEqual(academicLevel.Guid, result.AcademicLevels.FirstOrDefault().Id);
            Assert.AreEqual(gradeScheme.Guid, result.GradeSchemes.FirstOrDefault().Id);
            Assert.AreEqual(section.StartOn, result.StartOn);
            Assert.AreEqual(section.EndOn, result.EndOn);
            if (course != null && result.Course != null)
                Assert.AreEqual(course.Guid, result.Course.Id);
            Assert.AreEqual(section.Number, result.Number);
            Assert.AreEqual(section.MaximumEnrollment, result.MaximumEnrollment);

            var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
            if (courseLevel != null)
                Assert.AreEqual(courseLevel.Guid, result.CourseLevels[0].Id);

        }

        [TestMethod]
        public async Task SectionsController_GetHedmSections4_Keyword()
        {
            sectionsController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };

            var section = allSectionDtos.FirstOrDefault(x => x.Id == firstSectionGuid);
            var tuple = new Tuple<IEnumerable<Dtos.Section4>, int>(new List<Dtos.Section4>() { section }, 5);

            sectionCoordinationServiceMock.Setup(svc => svc.GetSection4ByGuidAsync(It.IsAny<string>())).ReturnsAsync(section);
            sectionCoordinationServiceMock.Setup(svc => svc.GetSections4Async(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), "", "", "", "", "", "", "", "", "", "", "", "", "", SectionsSearchable.NotSet, It.IsAny<string>()))
                .ReturnsAsync(tuple);
            var academicLevel = academicLevels.FirstOrDefault(x => x.Guid == section.AcademicLevels.FirstOrDefault().Id);

            var gradeScheme = allGradeSchemes.FirstOrDefault(x => x.Code == academicLevel.GradeScheme);
            Ellucian.Colleague.Domain.Student.Entities.Course course = null;
            if (section.Course != null)
                course = allCourses.FirstOrDefault(x => x.Guid == section.Course.Id);

            char quote = '"';
            string criteria = "[\"math\"]";

            //Dictionary<String, String> criterion = new Dictionary<string, string>();
            //criterion.Add("title", section.Title);
            //var criteriaJson = Newtonsoft.Json.JsonConvert.SerializeObject(criterion);

            var sectionByTitle = await sectionsController.GetHedmSections4Async(paging, keywordSearch: criteria);

            var cancelToken = new System.Threading.CancellationToken(false);

            System.Net.Http.HttpResponseMessage httpResponseMessage = await sectionByTitle.ExecuteAsync(cancelToken);

            IEnumerable<Dtos.Section4> results = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.Section4>>)httpResponseMessage.Content).Value as IEnumerable<Dtos.Section4>;

            var result = results.FirstOrDefault();

            Assert.IsTrue(sectionByTitle is IHttpActionResult);

            Assert.AreEqual(section.Id, result.Id);
            Assert.AreEqual(section.Title, result.Title);
            Assert.AreEqual(section.Description, result.Description);
            Assert.AreEqual(academicLevel.Guid, result.AcademicLevels.FirstOrDefault().Id);
            Assert.AreEqual(gradeScheme.Guid, result.GradeSchemes.FirstOrDefault().Id);
            Assert.AreEqual(section.StartOn, result.StartOn);
            Assert.AreEqual(section.EndOn, result.EndOn);
            if (course != null && result.Course != null)
                Assert.AreEqual(course.Guid, result.Course.Id);
            Assert.AreEqual(section.Number, result.Number);
            Assert.AreEqual(section.MaximumEnrollment, result.MaximumEnrollment);

            var courseLevel = allCourseLevels.FirstOrDefault(x => x.Guid == section.CourseLevels[0].Id);
            if (courseLevel != null)
                Assert.AreEqual(courseLevel.Guid, result.CourseLevels[0].Id);

        }
    }
}
