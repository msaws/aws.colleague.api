﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Hosting;
using Ellucian.Colleague.Api.Controllers;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Colleague.Dtos;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using AcademicPeriod = Ellucian.Colleague.Domain.Student.Entities.AcademicPeriod;
using AutoMapper;
using System.Net.Http.Headers;
using System;
using Ellucian.Web.Http.TestUtil;

namespace Ellucian.Colleague.Api.Tests.Controllers.Student
{
    [TestClass]
    public class AcademicPeriodsControllerTests
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        private AcademicPeriodsController _academicPeriodController;
        private Mock<IAcademicPeriodService> _academicPeriodServiceMock;
        private IAcademicPeriodService _academicPeriodService;
        private IAdapterRegistry _adapterRegistry;
        private List<AcademicPeriod> _allAcademicPeriods;
        private List<Dtos.AcademicPeriod> _allAcademicPeriodsDto;
        private readonly ILogger _logger = new Mock<ILogger>().Object;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
            _academicPeriodServiceMock = new Mock<IAcademicPeriodService>();
            _academicPeriodService = _academicPeriodServiceMock.Object;

            _allAcademicPeriodsDto = new List<Dtos.AcademicPeriod>();
            var adapters = new HashSet<ITypeAdapter>();
            _adapterRegistry = new AdapterRegistry(adapters, _logger);
            var testAdapter = new AutoMapperAdapter<AcademicPeriod, Dtos.AcademicPeriod>(_adapterRegistry, _logger);
            _adapterRegistry.AddAdapter(testAdapter);

            _allAcademicPeriods = new  TestAcademicPeriodRepository().Get() as List<AcademicPeriod>;

            _academicPeriodController = new AcademicPeriodsController(_adapterRegistry, _academicPeriodService, _logger)
            {
                Request = new HttpRequestMessage()
            };
            _academicPeriodController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey,
                new HttpConfiguration());
            _academicPeriodController.Request.Headers.CacheControl = new CacheControlHeaderValue
            {
                NoCache = true,
                Public = true
            };
            Mapper.CreateMap<AcademicPeriod, Dtos.AcademicPeriod>();
            Debug.Assert(_allAcademicPeriods != null, "_allAcademicPeriods != null");
            foreach (var academicPeriod in _allAcademicPeriods)
            {
                var target = Mapper.Map<AcademicPeriod, Dtos.AcademicPeriod>(academicPeriod);
                target.Abbreviation = academicPeriod.Code;
                _allAcademicPeriodsDto.Add(target);
            }
            _academicPeriodServiceMock.Setup(s => s.GetAcademicPeriodsAsync())
                .ReturnsAsync(_allAcademicPeriodsDto);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _academicPeriodController = null;
            _academicPeriodService = null;
            _allAcademicPeriods = null;
            _allAcademicPeriodsDto = null;
            _academicPeriodServiceMock = null;
        }

        [TestMethod]
        public async Task AcadePeriodController_ReturnsAllAcademicPeriods()
        {
            var academicPeriods = await _academicPeriodController.GetAcademicPeriodsAsync() as List<Dtos.AcademicPeriod>;
            Debug.Assert(academicPeriods != null, "academicPeriods == null");
            Assert.AreEqual(academicPeriods.Count, _allAcademicPeriods.Count);
        }

        [TestMethod]
        public async Task AcadePeriodController_GetAcademicPeriodByIdAsync()
        {
            var expected = _allAcademicPeriodsDto.FirstOrDefault();
            _academicPeriodServiceMock.Setup(x => x.GetAcademicPeriodByGuidAsync(expected.Guid)).ReturnsAsync(expected);

            Debug.Assert(expected != null, "expected != null");
            var actual = await _academicPeriodController.GetAcademicPeriodByGuidAsync(expected.Guid);

            Assert.AreEqual(expected.Guid, actual.Guid, "Guid");
           Assert.AreEqual(expected.Abbreviation, actual.Abbreviation, "Abbreviation"); ;
        }


        [TestMethod]
        public async Task AcadePeriodController_GetAcademicPeriods_ValidateProperties()
        {
            var academicPeriods = await _academicPeriodController.GetAcademicPeriodsAsync() as List<Dtos.AcademicPeriod>;
            Debug.Assert(academicPeriods != null, "academicPeriods != null");
            var al = academicPeriods.FirstOrDefault(a => a.Abbreviation == "2000RSU");
            var alt = _allAcademicPeriods.FirstOrDefault(a => a.Code == "2000RSU");
            Debug.Assert(alt != null, "alt != null");
            Debug.Assert(al != null, "al != null");
            Assert.AreEqual(alt.Code, al.Abbreviation);
            Assert.AreEqual(alt.Guid, al.Guid);
        }

        [TestMethod]
        public async Task AcadePeriodController_AcademicPeriodsTypeTest()
        {
            var academicPeriods = (await _academicPeriodController.GetAcademicPeriodsAsync()).FirstOrDefault();
            Debug.Assert(academicPeriods != null, "academicPeriods != null");
            Assert.AreEqual(typeof (Dtos.AcademicPeriod), academicPeriods.GetType());
        }

        [TestMethod]
        public void AcadePeriodController_NumberOfKnownPropertiesTest()
        {
            var academicPeriodProperties =
                typeof (Dtos.AcademicPeriod).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            Assert.AreEqual(8, academicPeriodProperties.Length);
        }

        [TestMethod]
        [ExpectedException(typeof (HttpResponseException))]
        public async Task AcadePeriodController_GetAcademicPeriodByGuidAsync_Exception()
        {
            var expected = _allAcademicPeriodsDto.FirstOrDefault();
            _academicPeriodServiceMock.Setup(x => x.GetAcademicPeriodByGuidAsync(expected.Guid)).Throws<Exception>();
            Debug.Assert(expected != null, "expected != null");
            await _academicPeriodController.GetAcademicPeriodByGuidAsync(expected.Guid);
        }

        [TestMethod]
        [ExpectedException(typeof (HttpResponseException))]
        public async Task AcadePeriodController_GetAcademicPeriodsAsync_Exception()
        {
            _academicPeriodServiceMock.Setup(s => s.GetAcademicPeriodsAsync()).Throws<Exception>();
            await _academicPeriodController.GetAcademicPeriodsAsync();
        }    
    }
    
    [TestClass]
    public class AcademicPeriodsControllerTests2
    {
        /// <summary>
        ///     Gets or sets the test context which provides
        ///     information about and functionality for the current test run.
        /// </summary>
        public TestContext TestContext { get; set; }

        private AcademicPeriodsController _academicPeriodController;
        private Mock<IAcademicPeriodService> _academicPeriodServiceMock;
        private IAcademicPeriodService _academicPeriodService;
        private IAdapterRegistry _adapterRegistry;
        private List<AcademicPeriod> _allAcademicPeriods;
        private List<AcademicPeriod2> _allAcademicPeriodsDto;
        private readonly ILogger _logger = new Mock<ILogger>().Object;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
            _academicPeriodServiceMock = new Mock<IAcademicPeriodService>();
            _academicPeriodService = _academicPeriodServiceMock.Object;

            _allAcademicPeriodsDto = new List<AcademicPeriod2>();
            var adapters = new HashSet<ITypeAdapter>();
            _adapterRegistry = new AdapterRegistry(adapters, _logger);
            var testAdapter = new AutoMapperAdapter<AcademicPeriod, AcademicPeriod2>(_adapterRegistry, _logger);
            _adapterRegistry.AddAdapter(testAdapter);

            _allAcademicPeriods = new TestAcademicPeriodRepository().Get() as List<AcademicPeriod>;

            _academicPeriodController = new AcademicPeriodsController(_adapterRegistry, _academicPeriodService, _logger)
            {
                Request = new HttpRequestMessage()
            };
            _academicPeriodController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey,
                new HttpConfiguration());
            _academicPeriodController.Request.Headers.CacheControl = new CacheControlHeaderValue
            {
                NoCache = true,
                Public = true
            };
            Mapper.CreateMap<AcademicPeriod, AcademicPeriod2>();
            Debug.Assert(_allAcademicPeriods != null, "_allAcademicPeriods != null");
            foreach (var academicPeriods in _allAcademicPeriods)
            {
                var target = Mapper.Map<AcademicPeriod, AcademicPeriod2>(academicPeriods);
                target.Id = academicPeriods.Guid;
                target.Title = academicPeriods.Description;
                _allAcademicPeriodsDto.Add(target);
            }
            _academicPeriodServiceMock.Setup(s => s.GetAcademicPeriods2Async(It.IsAny<bool>()))
                .ReturnsAsync(_allAcademicPeriodsDto);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _academicPeriodController = null;
            _academicPeriodService = null;
            _allAcademicPeriods = null;
            _allAcademicPeriodsDto = null;
            _academicPeriodServiceMock = null;
        }

        [TestMethod]
        public async Task AcadePeriodController_ReturnsAllAcademicPeriods()
        {
            var academicPeriods = await _academicPeriodController.GetAcademicPeriods2Async() as List<AcademicPeriod2>;
            Debug.Assert(academicPeriods != null, "academicPeriods != null");
            Assert.AreEqual(academicPeriods.Count, _allAcademicPeriods.Count);
        }

        [TestMethod]
        public async Task AcadePeriodController_GetAcademicPeriodById2Async()
        {
            var expected = _allAcademicPeriodsDto.FirstOrDefault();
            _academicPeriodServiceMock.Setup(x => x.GetAcademicPeriodByGuid2Async(expected.Id)).ReturnsAsync(expected);

            Debug.Assert(expected != null, "expected != null");
            var actual = await _academicPeriodController.GetAcademicPeriodByGuid2Async(expected.Id);

            Assert.AreEqual(expected.Id, actual.Id, "Id");
            Assert.AreEqual(expected.Title, actual.Title, "Title");
            Assert.AreEqual(expected.Code, actual.Code, "Code"); ;
        }


        [TestMethod]
        public async Task AcadePeriodController_GetAcademicPeriods_ValidateProperties()
        {
            var academicPeriods = await _academicPeriodController.GetAcademicPeriods2Async() as List<AcademicPeriod2>;
            Debug.Assert(academicPeriods != null, "academicPeriods != null");
            var al = academicPeriods.FirstOrDefault(a => a.Code == "2000RSU");
            var alt = _allAcademicPeriods.FirstOrDefault(a => a.Code == "2000RSU");
            Debug.Assert(alt != null, "alt != null");
            Debug.Assert(al != null, "al != null");
            Assert.AreEqual(alt.Code, al.Code);
            Assert.AreEqual(alt.Description, al.Title);
        }

        [TestMethod]
        public async Task AcadePeriodController_AcademicPeriodsTypeTest()
        {
            var academicPeriods = (await _academicPeriodController.GetAcademicPeriods2Async()).FirstOrDefault();
            Debug.Assert(academicPeriods != null, "academicPeriods != null");
            Assert.AreEqual(typeof (AcademicPeriod2), academicPeriods.GetType());
        }

        [TestMethod]
        public void AcadePeriodController_NumberOfKnownPropertiesTest()
        {
            var academicPeriodProperties =
                typeof (AcademicPeriod2).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            Assert.AreEqual(8, academicPeriodProperties.Length);
        }

        [TestMethod]
        [ExpectedException(typeof (HttpResponseException))]
        public async Task AcadePeriodController_GetAcademicPeriodById2Async_Exception()
        {
            var expected = _allAcademicPeriodsDto.FirstOrDefault();
            _academicPeriodServiceMock.Setup(x => x.GetAcademicPeriodByGuid2Async(expected.Id)).Throws<Exception>();
            Debug.Assert(expected != null, "expected != null");
            await _academicPeriodController.GetAcademicPeriodByGuid2Async(expected.Id);
        }

        [TestMethod]
        [ExpectedException(typeof (HttpResponseException))]
        public async Task AcadePeriodController_GetAcademicPeriods2Async_Exception()
        {
            _academicPeriodServiceMock.Setup(s => s.GetAcademicPeriods2Async(It.IsAny<bool>())).Throws<Exception>();
            await _academicPeriodController.GetAcademicPeriods2Async();
        }

        [TestMethod]
        [ExpectedException(typeof (HttpResponseException))]
        public async Task AcadPeriodController_DeleteThrowsIntApiExc()
        {
            await _academicPeriodController.DeleteAcademicPeriodByIdAsync("UG");
        }

        [TestMethod]
        [ExpectedException(typeof (HttpResponseException))]
        public async Task AcadPeriodController_PostThrowsIntAppiExc()
        {
            await _academicPeriodController.PostAcademicPeriodAsync(new AcademicPeriod2());
        }

        [TestMethod]
        [ExpectedException(typeof (HttpResponseException))]
        public async Task AcadePeriodController_PutThrowsIntAppiExc()
        {
            await _academicPeriodController.PutAcademicPeriodAsync("UG", new AcademicPeriod2());
        }

    }
}