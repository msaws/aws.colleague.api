﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using Ellucian.Colleague.Coordination.Base.Services;
using System.Threading.Tasks;
using System.Collections.Generic;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Base.Tests;
using System.Net.Http;
using System.Web.Http.Hosting;
using System.Web.Http;
using Ellucian.Colleague.Api.Controllers;

namespace Ellucian.Colleague.Api.Tests.Controllers.Base
{
    [TestClass]
    public class ContactTypesControllerTests
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

       private Mock<ILogger> loggerMock;
        private ContactTypesController contactTypesController;      
       private const string contactTypeGuid = "94e1e454-5920-4352-b542-c348edfe7312";
       private Dtos.ContactType contactType;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.DeploymentDirectory, "App_Data"));
                 
            contactType = new Dtos.ContactType{
            Id = contactTypeGuid, Code = "PR", Description= null, Title = "Primary" };

            loggerMock = new Mock<ILogger>();          
            contactTypesController = new ContactTypesController(loggerMock.Object)
            {
                Request = new HttpRequestMessage()
            };
            contactTypesController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
        }

        [TestCleanup]
        public void Cleanup()
        {
            contactTypesController = null;
            loggerMock = null;
            contactType = null;
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ContactTypesController_GetContactTypes_Exception()
        {
            await contactTypesController.GetContactTypesAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ContactTypesController_GetContactTypesById_Exception()
        {
            await contactTypesController.GetContactTypesByIdAsync(contactType.Id);
        }
       
        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ContactTypesController_PostContactType_Exception()
        {
            await contactTypesController.PostContactTypesAsync(contactType);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ContactTypesController_PutContactType_Exception()
        {
           await contactTypesController.PutContactTypesAsync(contactType.Id, contactType);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ContactTypesController_DeleteContactType_Exception()
        {
            await contactTypesController.DeleteContactTypesAsync(contactType.Id);
        }
    }
}