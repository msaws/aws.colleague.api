﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Ellucian.Colleague.Api.Controllers;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Base.Tests;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System.Net.Http;
using System.Web.Http.Hosting;
using System.Web.Http;
using System.Threading.Tasks;
using System;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Dtos;
using Ellucian.Web.Http.TestUtil;

namespace Ellucian.Colleague.Api.Tests.Controllers.Base
{

    [TestClass]
    public class BuildingControllerTests
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        private Mock<IAdapterRegistry> adapterRegistryMock;
        private IAdapterRegistry adapterRegistry;
        private Mock<IReferenceDataRepository> referenceDataRepositoryMock;
        private IReferenceDataRepository referenceDataRepository;
        private Mock<IFacilitiesService> facilitiesServiceMock;
        private IFacilitiesService facilitiesService;
        private ILogger logger = new Mock<ILogger>().Object;

        private BuildingsController buildingsController;
        private IEnumerable<Ellucian.Colleague.Domain.Base.Entities.Building> allBuildingEntities;
        private List<Dtos.Building> allBuildingDtos = new List<Dtos.Building>();
        private List<Dtos.Building2> allBuilding2Dtos = new List<Dtos.Building2>();
       
        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
           referenceDataRepositoryMock = new Mock<IReferenceDataRepository>();
            referenceDataRepository = referenceDataRepositoryMock.Object;

            adapterRegistryMock = new Mock<IAdapterRegistry>();
            adapterRegistry = adapterRegistryMock.Object;
            facilitiesServiceMock = new Mock<IFacilitiesService>();
            facilitiesService = facilitiesServiceMock.Object;

            allBuildingEntities = new TestBuildingRepository().Get();

            Mapper.CreateMap<Ellucian.Colleague.Domain.Base.Entities.Building, Dtos.Building>();
            foreach (var building in allBuildingEntities)
            {
                Dtos.Building target = Mapper.Map<Ellucian.Colleague.Domain.Base.Entities.Building, Dtos.Building>(building);
                allBuildingDtos.Add(target);
            }


            Mapper.CreateMap<Ellucian.Colleague.Domain.Base.Entities.Building, Dtos.Building2>();
            foreach (var building in allBuildingEntities)
            {
                Dtos.Building2 target = Mapper.Map<Ellucian.Colleague.Domain.Base.Entities.Building, Dtos.Building2>(building);
                target.Id = building.Guid;
                allBuilding2Dtos.Add(target);
            }


            buildingsController = new BuildingsController(adapterRegistry, referenceDataRepository, facilitiesService, logger);
             buildingsController.Request = new HttpRequestMessage();
            buildingsController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
        }

        [TestCleanup]
        public void Cleanup()
        {
            buildingsController = null;
            referenceDataRepository = null;
        }

        [TestMethod]
        public async Task GetHedmBuildingsAsync_ValidateFields()
        {
            facilitiesServiceMock.Setup(x => x.GetBuildingsAsync()).ReturnsAsync(allBuildingDtos);

            var buildings = (await buildingsController.GetHedmBuildingsAsync()).ToList();
            Assert.AreEqual(allBuildingDtos.Count, buildings.Count);
            for (int i = 0; i < buildings.Count; i++)
            {
                var expected = allBuildingDtos[i];
                var actual = buildings[i];
                Assert.AreEqual(expected.Guid, actual.Guid, "Guid, Index=" + i.ToString());
               Assert.AreEqual(expected.Title, actual.Title, "Title, Index=" + i.ToString());
            }
        }

        [TestMethod]
        public async Task GetBuildingByGuidAsync_ValidateFields()
        {
            var expected = allBuildingDtos.FirstOrDefault();
            facilitiesServiceMock.Setup(x => x.GetBuildingAsync(expected.Guid)).ReturnsAsync(expected);

            var actual = (await buildingsController.GetBuildingByGuidAsync(expected.Guid));

            Assert.AreEqual(expected.Guid, actual.Guid, "Guid");
            Assert.AreEqual(expected.Title, actual.Title, "Title");
        }

        [TestMethod]
        public async Task GetHedmBuildings2Async_ValidateFields()
        {
            facilitiesServiceMock.Setup(x => x.GetBuildings2Async(It.IsAny<bool>())).ReturnsAsync(allBuilding2Dtos);

            var buildings = (await buildingsController.GetHedmBuildings2Async()).ToList();
            Assert.AreEqual(allBuilding2Dtos.Count, buildings.Count);
            for (int i = 0; i < buildings.Count; i++)
            {
                var expected = allBuilding2Dtos[i];
                var actual = buildings[i];
                Assert.AreEqual(expected.Id, actual.Id, "Guid, Index=" + i.ToString());
                Assert.AreEqual(expected.Title, actual.Title, "Title, Index=" + i.ToString());
                Assert.AreEqual(expected.Code, actual.Code, "Code, Index=" + i.ToString());
                Assert.AreEqual(expected.Description, actual.Description, "Desc, Index=" + i.ToString());
           }
        }

        [TestMethod]
        public async Task GetHedmBuildingByIdAsync_ValidateFields()
        {
            var expected = allBuilding2Dtos.FirstOrDefault();
            facilitiesServiceMock.Setup(x => x.GetBuilding2Async(expected.Id)).ReturnsAsync(expected);

            var actual = (await buildingsController.GetHedmBuildingByIdAsync(expected.Id));

            Assert.AreEqual(expected.Id, actual.Id, "Guid");
            Assert.AreEqual(expected.Title, actual.Title, "Title");
            Assert.AreEqual(expected.Code, actual.Code, "Code");
            Assert.AreEqual(expected.Title, actual.Title, "Title");
        }

        [TestMethod]
        public async Task BuildingsController_GetHedmAsync_CacheControlNotNull()
        {
            buildingsController.Request.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue();
            facilitiesServiceMock.Setup(gc => gc.GetBuildings2Async(It.IsAny<bool>())).ReturnsAsync(allBuilding2Dtos);

            var building = await buildingsController.GetHedmBuildings2Async();
            Assert.AreEqual(building.Count(), allBuildingEntities.Count());

            int count = allBuildingEntities.Count();
            for (int i = 0; i < count; i++)
            {
                var expected = allBuilding2Dtos[i];
                var actual = allBuildingEntities.ToList()[i];

                Assert.AreEqual(expected.Id, actual.Guid);
                Assert.AreEqual(expected.Code, actual.Code);
                Assert.AreEqual(expected.Description, actual.Description);
            }
        }

        [TestMethod]
        public async Task BuildingsController_GetHedmAsync_NoCache()
        {
            buildingsController.Request.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue();
            buildingsController.Request.Headers.CacheControl.NoCache = true;

            facilitiesServiceMock.Setup(gc => gc.GetBuildings2Async(It.IsAny<bool>())).ReturnsAsync(allBuilding2Dtos);

            var building = await buildingsController.GetHedmBuildings2Async();
            Assert.AreEqual(building.Count(), allBuildingEntities.Count());

            int count = allBuildingEntities.Count();
            for (int i = 0; i < count; i++)
            {
                var expected = allBuilding2Dtos[i];
                var actual = allBuildingEntities.ToList()[i];

                Assert.AreEqual(expected.Id, actual.Guid);
                Assert.AreEqual(expected.Code, actual.Code);
                Assert.AreEqual(expected.Description, actual.Description);
            }
        }

        [TestMethod]
        public async Task BuildingsController_GetHedmAsync_Cache()
        {
            buildingsController.Request.Headers.CacheControl = new System.Net.Http.Headers.CacheControlHeaderValue();
            buildingsController.Request.Headers.CacheControl.NoCache = false;

            facilitiesServiceMock.Setup(gc => gc.GetBuildings2Async(It.IsAny<bool>())).ReturnsAsync(allBuilding2Dtos);

            var building = await buildingsController.GetHedmBuildings2Async();
            Assert.AreEqual(building.Count(), allBuildingEntities.Count());

            int count = allBuildingEntities.Count();
            for (int i = 0; i < count; i++)
            {
                var expected = allBuilding2Dtos[i];
                var actual = allBuildingEntities.ToList()[i];

                Assert.AreEqual(expected.Id, actual.Guid);
                Assert.AreEqual(expected.Code, actual.Code);
                Assert.AreEqual(expected.Description, actual.Description);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task BuildingsController_GetThrowsIntAppiExc()
        {
            facilitiesServiceMock.Setup(gc => gc.GetBuildingsAsync()).Throws<Exception>();

            await buildingsController.GetHedmBuildingsAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task Buildings2Controller_GetThrowsIntAppiExc()
        {
            facilitiesServiceMock.Setup(gc => gc.GetBuildings2Async(It.IsAny<bool>())).Throws<Exception>();

            await buildingsController.GetHedmBuildings2Async();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task BuildingController_GetByIdThrowsIntAppiExc()
        {
            facilitiesServiceMock.Setup(gc => gc.GetBuildingAsync(It.IsAny<string>())).Throws<Exception>();

            await buildingsController.GetBuildingByGuidAsync("sdjfh");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task Building2Controller_GetByIdThrowsIntAppiExc()
        {
            facilitiesServiceMock.Setup(gc => gc.GetBuilding2Async(It.IsAny<string>())).Throws<Exception>();

            await buildingsController.GetHedmBuildingByIdAsync("sdjfh");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task BuildingController_PostThrowsIntAppiExc()
        {
            await buildingsController.PostBuildingAsync(allBuilding2Dtos[0]);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task BuildingController_PutThrowsIntAppiExc()
        {
            var result = await buildingsController.PutBuildingAsync("dhjdsk", allBuilding2Dtos[0]);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task BuildingController_DeleteThrowsIntAppiExc()
        {
            var result = await buildingsController.DeleteBuildingAsync("9ae3a175-1dfd-4937-b97b-3c9ad596e023");
        }
    }
}