﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using Ellucian.Colleague.Coordination.Base.Services;
using System.Threading.Tasks;
using System.Collections.Generic;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Base.Tests;
using System.Net.Http;
using System.Web.Http.Hosting;
using System.Web.Http;
using Ellucian.Colleague.Api.Controllers;
using Ellucian.Web.Adapters;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Domain.Base.Exceptions;

namespace Ellucian.Colleague.Api.Tests.Controllers.Base
{
    [TestClass]
    public class EducationalInstitutionControllerTests
    {
        ///// <summary>
        /////Gets or sets the test context which provides
        /////information about and functionality for the current test run.
        /////</summary>
        //public TestContext TestContext { get; set; }
        //private Mock<IEducationalInstitutionsService> educationalInstitutionsServiceMock;
        //private Mock<ILogger> loggerMock;

        //private EducationalInstitutionsController educationalInstitutionsController;
        //private List<Dtos.EducationalInstitution> educationalInstitutionCollection;

        //[TestInitialize]
        //public void Initialize()
        //{
        //    EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.DeploymentDirectory, "App_Data"));

        //    educationalInstitutionsServiceMock = new Mock<IEducationalInstitutionsService>();
        //    loggerMock = new Mock<ILogger>();

        //    educationalInstitutionCollection = new List<Dtos.EducationalInstitution>();

        //    educationalInstitutionsController = new EducationalInstitutionsController(educationalInstitutionsServiceMock.Object, loggerMock.Object)
        //    {
        //        Request = new HttpRequestMessage()
        //    };
        //    educationalInstitutionsController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
        //}

        //[TestCleanup]
        //public void Cleanup()
        //{
        //    educationalInstitutionsController = null;
        //    educationalInstitutionCollection = null;
        //    loggerMock = null;
        //    educationalInstitutionsServiceMock = null;
        //}

        //#region EducationalInstitutions

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutions_PermissionsException()
        //{
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsAsync(It.IsAny<bool>())).Throws<PermissionsException>();
        //    //await educationalInstitutionsController.GetEducationalInstitutionsAsync();
        //}

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutions_ArgumentException()
        //{
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsAsync(It.IsAny<bool>())).Throws<ArgumentException>();
        //    //await educationalInstitutionsController.GetEducationalInstitutionsAsync();
        //}

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutions_RepositoryException()
        //{
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsAsync(It.IsAny<bool>())).Throws<RepositoryException>();
        //    //await educationalInstitutionsController.GetEducationalInstitutionsAsync();
        //}

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutions_IntegrationApiException()
        //{
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsAsync(It.IsAny<bool>())).Throws<IntegrationApiException>();
        //    //await educationalInstitutionsController.GetEducationalInstitutionsAsync();
        //}

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutions_Exception()
        //{
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsAsync(It.IsAny<bool>())).Throws<Exception>();
        //    //await educationalInstitutionsController.GetEducationalInstitutionsAsync();
        //}

        //#endregion GetEducationalInstitutions

        //#region GetEducationalInstitutionsByGuid

        //[TestMethod]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutionsByGuid()
        //{
        //    //var expected = educationalInstitutionCollection.FirstOrDefault();
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsByGuidAsync(expected.Id)).ReturnsAsync(expected);

        //    //var actual = (await educationalInstitutionsController.GetEducationalInstitutionsByGuidAsync(expected.Id));
        //    //Assert.AreEqual(expected.Id, actual.Id, "Id");
        //}


        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutionsByGuid_NullArgument()
        //{
        //    //await educationalInstitutionsController.GetEducationalInstitutionsByGuidAsync(null);
        //}

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutionsByGuid_EmptyArgument()
        //{
        //    //await educationalInstitutionsController.GetEducationalInstitutionsByGuidAsync("");
        //}

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutionsByGuid_PermissionsException()
        //{
        //    //var expected = educationalInstitutionCollection.FirstOrDefault();
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsByGuidAsync(expected.Id)).Throws<PermissionsException>();
        //    //await educationalInstitutionsController.GetEducationalInstitutionsByGuidAsync(expected.Id);

        //}

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutionsByGuid_ArgumentException()
        //{
        //    //var expected = educationalInstitutionCollection.FirstOrDefault();
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsByGuidAsync(expected.Id)).Throws<ArgumentException>();
        //    //await educationalInstitutionsController.GetEducationalInstitutionsByGuidAsync(expected.Id);
        //}

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutionsByGuid_RepositoryException()
        //{
        //    //var expected = educationalInstitutionCollection.FirstOrDefault();
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsByGuidAsync(expected.Id)).Throws<RepositoryException>();
        //    //await educationalInstitutionsController.GetEducationalInstitutionsByGuidAsync(expected.Id);
        //}

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutionsByGuid_IntegrationApiException()
        //{
        //    //var expected = educationalInstitutionCollection.FirstOrDefault();
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsByGuidAsync(expected.Id)).Throws<IntegrationApiException>();
        //    //await educationalInstitutionsController.GetEducationalInstitutionsByGuidAsync(expected.Id);
        //}

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutionsByGuid_Exception()
        //{
        //    //var expected = educationalInstitutionCollection.FirstOrDefault();
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsByGuidAsync(expected.Id)).Throws<Exception>();
        //    //await educationalInstitutionsController.GetEducationalInstitutionsByGuidAsync(expected.Id);
        //}

        //#endregion GetEducationalInstitutionsByGuid

        //#region GetEducationalInstitutionsByType

        //[TestMethod]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutionsByType()
        //{
        //    //var educationalInstitutions = educationalInstitutionCollection
        //    //    .Where(x => x.EducationalInstitutionType == Dtos.EnumProperties.EducationalInstitutionType.Department).ToList();
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsByTypeAsync("department", It.IsAny<bool>())).ReturnsAsync(educationalInstitutions);

        //    //var educationalInstitutionsByType = (await educationalInstitutionsController.GetEducationalInstitutionsByTypeAsync("department")).ToList();
        //    //for (var i = 0; i < educationalInstitutions.Count; i++)
        //    //{
        //    //    var expected = educationalInstitutionsByType[i];
        //    //    var actual = educationalInstitutions[i];
        //    //    Assert.AreEqual(expected.Id, actual.Id, "Id, Index=" + i.ToString());
        //    //}
        //}


        //[TestMethod]
        //[ExpectedException(typeof(ArgumentNullException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutionsByType_NullArgument()
        //{
        //    //await educationalInstitutionsController.GetEducationalInstitutionsByTypeAsync(null);
        //}

        //[TestMethod]
        //[ExpectedException(typeof(ArgumentNullException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutionsByType_EmptyArgument()
        //{
        //    //await educationalInstitutionsController.GetEducationalInstitutionsByTypeAsync("");
        //}

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutionsByType_PermissionsException()
        //{
        //    //var expected = educationalInstitutionCollection.FirstOrDefault();
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsByTypeAsync(expected.Id, It.IsAny<bool>())).Throws<PermissionsException>();
        //    //await educationalInstitutionsController.GetEducationalInstitutionsByTypeAsync(expected.Id);

        //}

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutionsByType_ArgumentException()
        //{
        //    //var expected = educationalInstitutionCollection.FirstOrDefault();
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsByTypeAsync(expected.Id, It.IsAny<bool>())).Throws<ArgumentException>();
        //    //await educationalInstitutionsController.GetEducationalInstitutionsByTypeAsync(expected.Id);
        //}

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutionsByType_RepositoryException()
        //{
        //    //var expected = educationalInstitutionCollection.FirstOrDefault();
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsByTypeAsync(expected.Id, It.IsAny<bool>())).Throws<RepositoryException>();
        //    //await educationalInstitutionsController.GetEducationalInstitutionsByTypeAsync(expected.Id);
        //}

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutionsByType_IntegrationApiException()
        //{
        //    //var expected = educationalInstitutionCollection.FirstOrDefault();
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsByTypeAsync(expected.Id, It.IsAny<bool>())).Throws<IntegrationApiException>();
        //    //await educationalInstitutionsController.GetEducationalInstitutionsByTypeAsync(expected.Id);
        //}

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_GetEducationalInstitutionsByType_Exception()
        //{
        //    //var expected = educationalInstitutionCollection.FirstOrDefault();
        //    //educationalInstitutionsServiceMock.Setup(x => x.GetEducationalInstitutionsByTypeAsync(expected.Id, It.IsAny<bool>())).Throws<Exception>();
        //    //await educationalInstitutionsController.GetEducationalInstitutionsByTypeAsync(expected.Id);
        //}

        //#endregion GetEducationalInstitutionsByType

        //#region Put

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_PutEducationalInstitutions_NullArgument()
        //{
        //    await educationalInstitutionsController.PutEducationalInstitutionsAsync(null, null);
        //}

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_PutEducationalInstitutions_EmptyArgument()
        //{
        //    await educationalInstitutionsController.PutEducationalInstitutionsAsync("", null);
        //}
      
        //#endregion

        //#region Post

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_PostEducationalInstitutions_NullArgument()
        //{
        //    await educationalInstitutionsController.PostEducationalInstitutionsAsync(null);
        //}
 
        //#endregion

        //#region Delete

        //[TestMethod]
        //[ExpectedException(typeof(HttpResponseException))]
        //public async Task EducationalInstitutionsController_DeleteEducationalInstitutions_EmptyArgument()
        //{
        //    var expected = educationalInstitutionCollection.FirstOrDefault();
        //    await educationalInstitutionsController.DeleteEducationalInstitutionByGuidAsync("");
        //}
 
        //#endregion
    }
}