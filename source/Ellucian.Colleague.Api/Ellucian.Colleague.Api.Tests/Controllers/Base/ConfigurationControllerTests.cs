﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Controllers;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;


namespace Ellucian.Colleague.Api.Tests.Controllers.Base
{
    [TestClass]
    public class ConfigurationControllerTests
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private IConfigurationService configService;
        private Mock<IConfigurationService> configServiceMock;
        private IProxyService proxyService;
        private Mock<IProxyService> proxyServiceMock;
        private ConfigurationController configurationController;
        private Dtos.Base.ProxyConfiguration configuration;
        private Dtos.Base.PrivacyConfiguration privacyConfiguration;
        private IAdapterRegistry adapterRegistry;
        private Mock<IAdapterRegistry> adapterRegistryMock;
        private ILogger logger;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
            proxyServiceMock = new Mock<IProxyService>();
            proxyService = proxyServiceMock.Object;

            configServiceMock = new Mock<IConfigurationService>();
            configService = configServiceMock.Object;

            adapterRegistryMock = new Mock<IAdapterRegistry>();
            adapterRegistry = adapterRegistryMock.Object;

            logger = new Mock<ILogger>().Object;

            configuration = BuildProxyConfiguration();
            configurationController = new ConfigurationController(adapterRegistry, configService, proxyService, logger);
        }

        [TestCleanup]
        public void Cleanup()
        {
            configurationController = null;
            configService = null;
            configuration = null;
        }

        [TestMethod]
        public async Task ConfigurationController_GetProxyConfigurationAsync_Valid()
        {
            proxyServiceMock.Setup(x => x.GetProxyConfigurationAsync()).Returns(Task.FromResult(configuration));
            configuration = BuildProxyConfiguration();
            configurationController = new ConfigurationController(adapterRegistry, configService, proxyService, logger);

            var config = await configurationController.GetProxyConfigurationAsync();

            Assert.IsNotNull(config);
            Assert.AreEqual(configuration.DisclosureReleaseDocumentId, config.DisclosureReleaseDocumentId);
            Assert.AreEqual(configuration.ProxyEmailDocumentId, config.ProxyEmailDocumentId);
            Assert.AreEqual(configuration.ProxyIsEnabled, config.ProxyIsEnabled);
            Assert.AreEqual(configuration.WorkflowGroups.Count(), config.WorkflowGroups.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ConfigurationController_GetProxyConfigurationAsync_BadRequest()
        {
            proxyServiceMock.Setup(x => x.GetProxyConfigurationAsync()).Throws(new ConfigurationException());
            configurationController = new ConfigurationController(adapterRegistry, configService, proxyService, logger);

            var config = await configurationController.GetProxyConfigurationAsync();
        }

        [TestMethod]
        public async Task ConfigurationController_GetPrivacyConfigurationAsync_Valid()
        {
            privacyConfiguration = new PrivacyConfiguration()
            {
                RecordDenialMessage = "Record not accesible due to a privacy request"
            };
            configServiceMock.Setup(x => x.GetPrivacyConfigurationAsync()).ReturnsAsync(privacyConfiguration);
            configurationController = new ConfigurationController(adapterRegistry, configService, proxyService, logger);

            var config = await configurationController.GetPrivacyConfigurationAsync();

            Assert.IsNotNull(config);
            Assert.AreEqual(privacyConfiguration.RecordDenialMessage, config.RecordDenialMessage);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ConfigurationController_GetPrivacyConfigurationAsync_BadRequest()
        {
            configServiceMock.Setup(x => x.GetPrivacyConfigurationAsync()).ThrowsAsync(new ConfigurationException());
            configurationController = new ConfigurationController(adapterRegistry, configService, proxyService, logger);
            var config = await configurationController.GetPrivacyConfigurationAsync();
        }

        [TestMethod]
        public async Task ConfigurationController_GetOrganizationalRelationshipConfigurationAsync_Valid()
        {
            var organizationalRelationshipConfiguration = new OrganizationalRelationshipConfiguration()
            {
                RelationshipTypeCodeMapping = new Dictionary<OrganizationalRelationshipType, List<string>>
                {
                    { OrganizationalRelationshipType.Manager, new List<string> { "MGR" } }
                }
            };
            configServiceMock.Setup(x => x.GetOrganizationalRelationshipConfigurationAsync()).ReturnsAsync(organizationalRelationshipConfiguration);
            configurationController = new ConfigurationController(adapterRegistry, configService, proxyService, logger);

            var config = await configurationController.GetOrganizationalRelationshipConfigurationAsync();

            Assert.IsNotNull(config);
            CollectionAssert.AreEqual(organizationalRelationshipConfiguration.RelationshipTypeCodeMapping[OrganizationalRelationshipType.Manager], config.RelationshipTypeCodeMapping[OrganizationalRelationshipType.Manager]);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ConfigurationController_GetOrganizationalRelationshipConfigurationAsync_BadRequest()
        {
            configServiceMock.Setup(x => x.GetOrganizationalRelationshipConfigurationAsync()).ThrowsAsync(new Exception());
            configurationController = new ConfigurationController(adapterRegistry, configService, proxyService, logger);
            var config = await configurationController.GetOrganizationalRelationshipConfigurationAsync();
        }

        private Dtos.Base.ProxyConfiguration BuildProxyConfiguration()
        {
            var config = new Dtos.Base.ProxyConfiguration()
            {
                DisclosureReleaseDocumentId = "PROX.DISC",
                ProxyEmailDocumentId = "PROX.EMAIL",
                ProxyIsEnabled = true,
                WorkflowGroups = new List<Dtos.Base.ProxyWorkflowGroup>()
                {
                    new Dtos.Base.ProxyWorkflowGroup()
                    {
                        Code = "SF",
                        Description = "Student Finance",
                        Workflows = new List<Dtos.Base.ProxyWorkflow>()
                        {
                            new Dtos.Base.ProxyWorkflow() { Code = "SFAA", Description = "Student Finance Account Activity", WorkflowGroupId = "SF", IsEnabled = true },
                            new Dtos.Base.ProxyWorkflow() { Code = "SFMAP", Description = "Student Finance Make a Payment", WorkflowGroupId = "SF", IsEnabled = true }
                        }
                    }
                }
            };

            return config;
        }

    }
}
