﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Web.Adapters;
using slf4net;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Dtos;
using Ellucian.Colleague.Api.Controllers.Base;
using System.Threading.Tasks;
using System.Collections.Generic;
using Ellucian.Colleague.Configuration.Licensing;
using System.Reflection;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Base.Tests;
using System.Net.Http;
using System.Web.Http.Hosting;
using System.Web.Http;
using Ellucian.Colleague.Domain.Base.Entities;

namespace Ellucian.Colleague.Api.Tests.Controllers.Base
{
    [TestClass]
    public class GeographicAreasControllerTests
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        private Mock<IAdapterRegistry> adapterRegistryMock;
        private Mock<ILogger> loggerMock;
        private Mock<IGeographicAreaService> geographicAreaServiceMock;

        private string geographicAreaId;

        private GeographicArea expectedGeographicArea;
        private GeographicArea testGeographicArea;
        private GeographicArea actualGeographicArea;

        private GeographicAreasController geographicAreasController;


        public async Task<List<GeographicArea>> getActualGeographicAreas()
        {
            //IEnumerable<GeographicArea> geographicAreaList = await geographicAreasController.GetGeographicAreasAsync();
            return (await geographicAreasController.GetGeographicAreasAsync()).ToList();
        }

        [TestInitialize]
        public async void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.DeploymentDirectory, "App_Data"));

            adapterRegistryMock = new Mock<IAdapterRegistry>();
            loggerMock = new Mock<ILogger>();
            geographicAreaServiceMock = new Mock<IGeographicAreaService>();

            geographicAreaId = "idc2935b-29e8-675f-907b-15a34da4f433";

            expectedGeographicArea = new GeographicArea()
            {
                Id = "idc2935b-29e8-675f-907b-15a34da4f433",
                Code = "MIDW", 
                Title = "Midwestern US",
                Description = null,
                Type = new Dtos.GeographicAreaTypeProperty()
                {
                    category = Dtos.GeographicAreaTypeCategory.Governmental,
                    detail = new Dtos.GuidObject2() { Id = Guid.NewGuid().ToString() }
                }
            };

            testGeographicArea = new GeographicArea();
            foreach (var property in typeof(GeographicArea).GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                property.SetValue(testGeographicArea, property.GetValue(expectedGeographicArea, null), null);
            }
            geographicAreaServiceMock.Setup<Task<GeographicArea>>(s => s.GetGeographicAreaByGuidAsync(geographicAreaId)).Returns(Task.FromResult(testGeographicArea));

            geographicAreasController = new GeographicAreasController(geographicAreaServiceMock.Object, loggerMock.Object);
            actualGeographicArea = await geographicAreasController.GetGeographicAreaByIdAsync(geographicAreaId);
        }

        [TestCleanup]
        public void Cleanup()
        {
            adapterRegistryMock = null;
            loggerMock = null;
            geographicAreaServiceMock = null;
            geographicAreaId = null;
            expectedGeographicArea = null;
            testGeographicArea = null;
            actualGeographicArea = null;
            geographicAreasController = null;
        }

        [TestMethod]
        public void GeographicAreasTypeTest()
        {
            Assert.AreEqual(typeof(GeographicArea), actualGeographicArea.GetType());
            Assert.AreEqual(expectedGeographicArea.GetType(), actualGeographicArea.GetType());
        }

        [TestMethod]
        public void NumberOfKnownPropertiesTest()
        {
            var geographicAreaProperties = typeof(GeographicArea).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            Assert.AreEqual(7, geographicAreaProperties.Length);
        }
    }

    [TestClass]
    public class GeographicAreaController_GetAllTests
    {
        private TestContext testContextInstance2;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance2;
            }
            set
            {
                testContextInstance2 = value;
            }
        }

        private GeographicAreasController GeographicAreaController;
        private Mock<IGeographicAreaService> GeographicAreaServiceMock;
        private IGeographicAreaService GeographicAreaService;
        private IAdapterRegistry AdapterRegistry;
        private List<Ellucian.Colleague.Domain.Base.Entities.Chapter> allChaptersEntities;
        private List<Ellucian.Colleague.Domain.Base.Entities.County> allCountiesEntities;
        private List<Ellucian.Colleague.Domain.Base.Entities.ZipcodeXlat> allXipCodeXlatsEntities;
        ILogger logger = new Mock<ILogger>().Object;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
            GeographicAreaServiceMock = new Mock<IGeographicAreaService>();
            GeographicAreaService = GeographicAreaServiceMock.Object;

            HashSet<ITypeAdapter> adapters = new HashSet<ITypeAdapter>();
            AdapterRegistry = new AdapterRegistry(adapters, logger);
            var testAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Base.Entities.Chapter, GeographicArea>(AdapterRegistry, logger);
            AdapterRegistry.AddAdapter(testAdapter);

            allChaptersEntities = new TestGeographicAreaRepository().GetChapters() as List<Ellucian.Colleague.Domain.Base.Entities.Chapter>;
            var GeographicAreasList = new List<GeographicArea>();

            GeographicAreaController = new GeographicAreasController(GeographicAreaService, logger);
            GeographicAreaController.Request = new HttpRequestMessage();
            GeographicAreaController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            foreach (var geographicArea in allChaptersEntities)
            {
                GeographicArea target = ConvertChapterEntityToGeographicAreaDto(geographicArea);
                GeographicAreasList.Add(target);
            }

            GeographicAreaServiceMock.Setup<Task<IEnumerable<GeographicArea>>>(s => s.GetGeographicAreasAsync(false)).ReturnsAsync(GeographicAreasList);
        }

        [TestCleanup]
        public void Cleanup()
        {
            GeographicAreaController = null;
            GeographicAreaService = null;
        }

        [TestMethod]
        public async Task ReturnsAllGeographicAreas()
        {
            List<GeographicArea> GeographicAreas = await GeographicAreaController.GetGeographicAreasAsync() as List<GeographicArea>;
            Assert.AreEqual(GeographicAreas.Count, allChaptersEntities.Count);
        }

        [TestMethod]
        public async Task GetGeographicAreasProperties()
        {
            List<GeographicArea> GeogaphicAreas = await GeographicAreaController.GetGeographicAreasAsync() as List<GeographicArea>;
            GeographicArea ga = GeogaphicAreas.Where(a => a.Code == "BALT").FirstOrDefault();
            Ellucian.Colleague.Domain.Base.Entities.Chapter chp = allChaptersEntities.Where(a => a.Code == "BALT").FirstOrDefault();
            Assert.AreEqual(chp.Code, ga.Code);
            Assert.AreEqual(chp.Description, ga.Title);
        }

        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Converts an Chapter domain entity to its corresponding GeographicArea DTO
        /// </summary>
        /// <param name="source">Chapter domain entity</param>
        /// <returns>GeographicArea DTO</returns>
        private Dtos.GeographicArea ConvertChapterEntityToGeographicAreaDto(Chapter source)
        {
            var geographicArea = new Dtos.GeographicArea();
            geographicArea.Id = source.Guid;
            geographicArea.Code = source.Code;
            geographicArea.Title = source.Description;
            geographicArea.Description = null;
            geographicArea.Type = new Dtos.GeographicAreaTypeProperty()
            {
                category = Dtos.GeographicAreaTypeCategory.Fundraising,
                detail = new Dtos.GuidObject2() { Id = Guid.NewGuid().ToString() }
            };

            return geographicArea;
        }
    }

    [TestClass]
    public class GeographicAreasControllerTests2
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        private Mock<IGeographicAreaService> _geographicAreaServiceMock;
        private IGeographicAreaService _geographicAreaService;
        private readonly ILogger _logger = new Mock<ILogger>().Object;

        private Mock<IReferenceDataRepository> _refRepoMock;
        private IReferenceDataRepository _refRepo;
        private GeographicAreasController _geographicAreasController;

        private IEnumerable<Domain.Base.Entities.Chapter> _allChapters;
        private IEnumerable<Domain.Base.Entities.County> _allCounties;
        private IEnumerable<Domain.Base.Entities.ZipcodeXlat> _allZipCodeXlats;
        private List<Dtos.GeographicArea> _geographicAreasCollection;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            _refRepoMock = new Mock<IReferenceDataRepository>();
            _refRepo = _refRepoMock.Object;

            _geographicAreaServiceMock = new Mock<IGeographicAreaService>();
            _geographicAreaService = _geographicAreaServiceMock.Object;
            _geographicAreasCollection = new List<Dtos.GeographicArea>();

            _allChapters = new TestGeographicAreaRepository().GetChapters();
            _allCounties = new TestGeographicAreaRepository().GetCounties();
            _allZipCodeXlats = new TestGeographicAreaRepository().GetZipCodeXlats();

            foreach (var source in _allChapters)
            {
                var geographicArea = new Ellucian.Colleague.Dtos.GeographicArea
                {
                    Id = source.Guid,
                    Code = source.Code,
                    Title = source.Description,
                    Description = null,
                    Type = new Dtos.GeographicAreaTypeProperty()
                    {
                        category = Dtos.GeographicAreaTypeCategory.Fundraising,
                        detail = new Dtos.GuidObject2() { Id = Guid.NewGuid().ToString() }
                    }
                };

                _geographicAreasCollection.Add(geographicArea);
            }

            foreach (var source in _allCounties)
            {
                var geographicArea = new Ellucian.Colleague.Dtos.GeographicArea
                {
                    Id = source.Guid,
                    Code = source.Code,
                    Title = source.Description,
                    Description = null,
                    Type = new Dtos.GeographicAreaTypeProperty()
                    {
                        category = Dtos.GeographicAreaTypeCategory.Governmental,
                        detail = new Dtos.GuidObject2() { Id = Guid.NewGuid().ToString() }
                    }
                };


                _geographicAreasCollection.Add(geographicArea);
            }

            foreach (var source in _allZipCodeXlats)
            {
                var geographicArea = new Ellucian.Colleague.Dtos.GeographicArea
                {
                    Id = source.Guid,
                    Code = source.Code,
                    Title = source.Description,
                    Description = null,
                    Type = new Dtos.GeographicAreaTypeProperty()
                    {
                        category = Dtos.GeographicAreaTypeCategory.Postal,
                        detail = new Dtos.GuidObject2() { Id = Guid.NewGuid().ToString() }
                    }
                };


                _geographicAreasCollection.Add(geographicArea);
            }

            _geographicAreasController = new GeographicAreasController(_geographicAreaService, _logger)
            {
                Request = new HttpRequestMessage()
            };
            _geographicAreasController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
        }

        [TestCleanup]
        public void Cleanup()
        {
            _geographicAreasController = null;
            _refRepoMock = null;
            _refRepo = null;
            _geographicAreaService = null;
            _allCounties = null;
            _allZipCodeXlats = null;
            _allChapters = null;
            _geographicAreasCollection = null;
        }

        [TestMethod]
        public async Task GeographicAreasController_GetGeographicAreasAsync_ValidateFields_Nocache()
        {
            _geographicAreasController.Request.Headers.CacheControl =
                 new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };

            _geographicAreaServiceMock.Setup(x => x.GetGeographicAreasAsync(false)).ReturnsAsync(_geographicAreasCollection);

            var geographicAreas = (await _geographicAreasController.GetGeographicAreasAsync()).ToList();
            Assert.AreEqual(_geographicAreasCollection.Count, geographicAreas.Count);
            for (var i = 0; i < geographicAreas.Count; i++)
            {
                var expected = _geographicAreasCollection[i];
                var actual = geographicAreas[i];
                Assert.AreEqual(expected.Id, actual.Id, "Id, Index=" + i.ToString());
                Assert.AreEqual(expected.Title, actual.Title, "Title, Index=" + i.ToString());
                Assert.AreEqual(expected.Code, actual.Code, "Code, Index=" + i.ToString());
            }
        }

        [TestMethod]
        public async Task GeographicAreaAcademicCredentialsController_GetGeographicAreasAsync_ValidateFields_Cache()
        {
            _geographicAreasController.Request.Headers.CacheControl =
                 new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = true };

            _geographicAreaServiceMock.Setup(x => x.GetGeographicAreasAsync(true)).ReturnsAsync(_geographicAreasCollection);

            var geographicAreas = (await _geographicAreasController.GetGeographicAreasAsync()).ToList();
            Assert.AreEqual(_geographicAreasCollection.Count, geographicAreas.Count);
            for (var i = 0; i < geographicAreas.Count; i++)
            {
                var expected = _geographicAreasCollection[i];
                var actual = geographicAreas[i];
                Assert.AreEqual(expected.Id, actual.Id, "Id, Index=" + i.ToString());
                Assert.AreEqual(expected.Title, actual.Title, "Title, Index=" + i.ToString());
                Assert.AreEqual(expected.Code, actual.Code, "Code, Index=" + i.ToString());
            }
        }

        [TestMethod]
        public async Task GeographicAreasController_GetGeographicAreaByGuidAsync_ValidateFields()
        {
            var expected = _geographicAreasCollection.FirstOrDefault();
            _geographicAreaServiceMock.Setup(x => x.GetGeographicAreaByGuidAsync(expected.Id)).ReturnsAsync(expected);

            var actual = await _geographicAreasController.GetGeographicAreaByIdAsync(expected.Id);

            Assert.AreEqual(expected.Id, actual.Id, "Id");
            Assert.AreEqual(expected.Title, actual.Title, "Title");
            Assert.AreEqual(expected.Code, actual.Code, "Code");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeographicAreasController_GetGeographicAreasAsync_Exception()
        {
            _geographicAreaServiceMock.Setup(x => x.GetGeographicAreasAsync(false)).Throws<Exception>();
            await _geographicAreasController.GetGeographicAreasAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeographicAreasController_GetGeographicAreaByGuidAsync_Exception()
        {
            _geographicAreaServiceMock.Setup(x => x.GetGeographicAreaByGuidAsync(It.IsAny<string>())).Throws<Exception>();
            await _geographicAreasController.GetGeographicAreaByIdAsync(string.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeographicAreasController_PostGeographicArea()
        {
            await _geographicAreasController.PostGeographicAreaAsync(_geographicAreasCollection.FirstOrDefault());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeographicAreasController_PutGeographicArea()
        {
            var geographicArea = _geographicAreasCollection.FirstOrDefault();
            await _geographicAreasController.PutGeographicAreaAsync(geographicArea);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeographicAreasController_DeleteGeographicArea()
        {
            await _geographicAreasController.DeleteGeographicAreaAsync(_geographicAreasCollection.FirstOrDefault().Id);
        }
    }
}
