﻿/*Copyright 2014-2017 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web.Http;
using Ellucian.Colleague.Api.Controllers.FinancialAid;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.FinancialAid.Services;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System.Threading.Tasks;
using System.IO;
using System.Web.Http.Hosting;
using System.Net.Http;
using System.Net.Http.Headers;
using Ellucian.Web.Http.Models;
using Ellucian.Colleague.Domain.Exceptions;

namespace Ellucian.Colleague.Api.Tests.Controllers.FinancialAid
{
    [TestClass]
    public class FinancialAidApplicationsControllerTests
    {
        [TestClass]
        public class GetFinancialAidApplicationsControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            private Mock<IAdapterRegistry> adapterRegistryMock;
            private Mock<ILogger> loggerMock;
            private Mock<IFinancialAidApplicationService> financialAidApplicationServiceMock;

            private string studentId;

            private IEnumerable<FinancialAidApplication> expectedFinancialAidApplications;
            private List<FinancialAidApplication> testFinancialAidApplications;
            private IEnumerable<FinancialAidApplication> actualFinancialAidApplications;

            private FinancialAidApplicationsController FinancialAidApplicationsController;

            int offset = 0;
            int limit = 200;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                adapterRegistryMock = new Mock<IAdapterRegistry>();
                loggerMock = new Mock<ILogger>();
                financialAidApplicationServiceMock = new Mock<IFinancialAidApplicationService>();

                studentId = "0003915";
                expectedFinancialAidApplications = new List<FinancialAidApplication>()
                {
                    new FinancialAidApplication()
                    {
                        StudentId = "0003915",
                        AwardYear = "2014",
                        IsFafsaComplete = true,
                        IsProfileComplete = true,
                    },
                    new FinancialAidApplication()
                    {
                        StudentId = "0003915",
                        AwardYear = "2015",
                        IsFafsaComplete = true,
                        IsProfileComplete = false,
                    }
                };

                testFinancialAidApplications = new List<FinancialAidApplication>();
                foreach (var application in expectedFinancialAidApplications)
                {
                    var testApplication = new FinancialAidApplication();
                    foreach (var property in typeof(FinancialAidApplication).GetProperties(BindingFlags.Public | BindingFlags.Instance))
                    {
                        property.SetValue(testApplication, property.GetValue(application, null), null);
                    }
                    testFinancialAidApplications.Add(testApplication);
                }

                financialAidApplicationServiceMock.Setup(a => a.GetFinancialAidApplications(studentId)).Returns(testFinancialAidApplications);
                FinancialAidApplicationsController = new FinancialAidApplicationsController(adapterRegistryMock.Object, financialAidApplicationServiceMock.Object, null, loggerMock.Object);
                actualFinancialAidApplications = FinancialAidApplicationsController.GetFinancialAidApplications(studentId);
            }

            [TestCleanup]
            public void Cleanup()
            {
                adapterRegistryMock = null;
                loggerMock = null;
                financialAidApplicationServiceMock = null;
                studentId = null;
                expectedFinancialAidApplications = null;
                testFinancialAidApplications = null;
                actualFinancialAidApplications = null;
                FinancialAidApplicationsController = null;
            }

            [TestMethod]
            public void FinancialAidApplicationTypeTest()
            {
                Assert.AreEqual(expectedFinancialAidApplications.GetType(), actualFinancialAidApplications.GetType());

                foreach (var singleApplication in actualFinancialAidApplications)
                {
                    Assert.AreEqual(typeof(FinancialAidApplication), singleApplication.GetType());
                }
            }

            [TestMethod]
            public void PropertiesAreEqualTest()
            {
                var financialAidApplicationsProperties = typeof(FinancialAidApplication).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                foreach (var singleApplication in expectedFinancialAidApplications)
                {
                    var actualFinancialAidApplication = expectedFinancialAidApplications.First(d => d.AwardYear == singleApplication.AwardYear);
                    foreach (var property in financialAidApplicationsProperties)
                    {
                        var expectedPropertyValue = property.GetValue(singleApplication, null);
                        var actualPropertyValue = property.GetValue(actualFinancialAidApplication, null);
                        Assert.AreEqual(expectedPropertyValue, actualPropertyValue);
                    }
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public void StudentIdRequiredTest()
            {
                FinancialAidApplicationsController.GetFinancialAidApplications(null);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public void CatchPermissionsExceptionTest()
            {
                financialAidApplicationServiceMock.Setup(a => a.GetFinancialAidApplications(studentId)).Throws(new PermissionsException("pex"));
                try
                {
                    FinancialAidApplicationsController.GetFinancialAidApplications(studentId);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.Forbidden, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<PermissionsException>(), It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public void CatchKeyNotFoundExceptionTest()
            {
                financialAidApplicationServiceMock.Setup(a => a.GetFinancialAidApplications(studentId)).Throws(new KeyNotFoundException("knfe"));
                try
                {
                    FinancialAidApplicationsController.GetFinancialAidApplications(studentId);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.NotFound, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<KeyNotFoundException>(), It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public void CatchGenericExceptionTest()
            {
                financialAidApplicationServiceMock.Setup(a => a.GetFinancialAidApplications(studentId)).Throws(new Exception("knfe"));
                try
                {
                    FinancialAidApplicationsController.GetFinancialAidApplications(studentId);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
                    throw;
                }
            }
        }

        [TestClass]
        public class GET
        {
            /// <summary>
            ///     Gets or sets the test context which provides
            ///     information about and functionality for the current test run.
            /// </summary>
            public TestContext TestContext { get; set; }

            Mock<IFinancialAidApplicationService> financialAidApplicationServiceMock;
            Mock<IFinancialAidApplicationService2> financialAidApplicationService2Mock;
            Mock<IAdapterRegistry> adapterRegistryMock;
            Mock<ILogger> loggerMock;

            FinancialAidApplicationsController financialAidApplicationsController;
            List<Dtos.FinancialAidApplication> financialAidApplicationDtos;
            int offset = 0;
            int limit = 200;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(Path.Combine(TestContext.DeploymentDirectory, "App_Data"));

                financialAidApplicationServiceMock = new Mock<IFinancialAidApplicationService>();
                financialAidApplicationService2Mock = new Mock<IFinancialAidApplicationService2>();
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                loggerMock = new Mock<ILogger>();

                financialAidApplicationDtos = BuildData();

                financialAidApplicationsController = new FinancialAidApplicationsController(adapterRegistryMock.Object, financialAidApplicationServiceMock.Object, financialAidApplicationService2Mock.Object, loggerMock.Object) { Request = new HttpRequestMessage() };
                financialAidApplicationsController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
                financialAidApplicationsController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            }

            private List<Dtos.FinancialAidApplication> BuildData()
            {
                List<Dtos.FinancialAidApplication> financialAidApplications = new List<Dtos.FinancialAidApplication>() 
                {
                    new Dtos.FinancialAidApplication()
                    {
                        Id = "bbd216fb-0fc5-4f44-ae45-42d3cdd1e89a", 
                        AidYear = new Dtos.GuidObject2("e0c0c94c-53a7-46b7-96c4-76b12512c323")
                    },
                    new Dtos.FinancialAidApplication()
                    {
                        Id = "3f67b180-ce1d-4552-8d81-feb96b9fea5b", 
                        AidYear = new Dtos.GuidObject2("0bbb15f2-bb03-4056-bb9b-57a0ddf057ff")
                    },
                    new Dtos.FinancialAidApplication()
                    {
                        Id = "bf67e156-8f5d-402b-8101-81b0a2796873",   
                        AidYear = new Dtos.GuidObject2("0ac28907-5a9b-4102-a0d7-5d3d9c585512")
                    },
                    new Dtos.FinancialAidApplication()
                    {
                        Id = "0111d6ef-5a86-465f-ac58-4265a997c136",
                        AidYear = new Dtos.GuidObject2("bb6c261c-3818-4dc3-b693-eb3e64d70d8b")
                    },
                };
                return financialAidApplications;
            }

            [TestCleanup]
            public void Cleanup()
            {
                financialAidApplicationsController = null;
                financialAidApplicationDtos = null;
                financialAidApplicationServiceMock = null;
                adapterRegistryMock = null;
                loggerMock = null;
            }

            [TestMethod]
            public async Task FinancialAidApplicationsController_GetAll_NoCache_True()
            {
                financialAidApplicationsController.Request.Headers.CacheControl = new CacheControlHeaderValue
                {
                    NoCache = true,
                    Public = true
                };
                var tuple = new Tuple<IEnumerable<Dtos.FinancialAidApplication>, int>(financialAidApplicationDtos, 4);
                financialAidApplicationService2Mock.Setup(ci => ci.GetAsync(offset, limit, true)).ReturnsAsync(tuple);
                var financialAidApplications = await financialAidApplicationsController.GetAsync(new Paging(limit, offset));

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await financialAidApplications.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.FinancialAidApplication> actuals = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.FinancialAidApplication>>)httpResponseMessage.Content)
                                                                .Value as IEnumerable<Dtos.FinancialAidApplication>;


                Assert.AreEqual(financialAidApplicationDtos.Count, actuals.Count());

                foreach (var actual in actuals)
                {
                    var expected = financialAidApplicationDtos.FirstOrDefault(i => i.Id.Equals(actual.Id, StringComparison.OrdinalIgnoreCase));

                    Assert.IsNotNull(expected);
                    Assert.AreEqual(expected.Id, actual.Id);
                    Assert.AreEqual(expected.AidYear, actual.AidYear);
                    //Assert.AreEqual(expected.AwardFund, actual.AwardFund);
                    //Assert.AreEqual(expected.Student, actual.Student);
                }
            }

            [TestMethod]
            public async Task FinancialAidApplicationsController_GetAll_NoCache_False()
            {
                financialAidApplicationsController.Request.Headers.CacheControl = new CacheControlHeaderValue
                {
                    NoCache = false,
                    Public = true
                };
                var tuple = new Tuple<IEnumerable<Dtos.FinancialAidApplication>, int>(financialAidApplicationDtos, 4);
                financialAidApplicationService2Mock.Setup(ci => ci.GetAsync(offset, limit, false)).ReturnsAsync(tuple);
                var financialAidApplications = await financialAidApplicationsController.GetAsync(new Paging(limit, offset));

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await financialAidApplications.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.FinancialAidApplication> actuals = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.FinancialAidApplication>>)httpResponseMessage.Content)
                                                                .Value as IEnumerable<Dtos.FinancialAidApplication>;


                Assert.AreEqual(financialAidApplicationDtos.Count, actuals.Count());

                foreach (var actual in actuals)
                {
                    var expected = financialAidApplicationDtos.FirstOrDefault(i => i.Id.Equals(actual.Id, StringComparison.OrdinalIgnoreCase));

                    Assert.IsNotNull(expected);
                    Assert.AreEqual(expected.Id, actual.Id);
                    Assert.AreEqual(expected.AidYear, actual.AidYear);
                    //Assert.AreEqual(expected.AwardFund, actual.AwardFund);
                    //Assert.AreEqual(expected.Student, actual.Student);
                }
            }

            [TestMethod]
            public async Task FinancialAidApplicationsController_GetAll_NullPage()
            {
                financialAidApplicationsController.Request.Headers.CacheControl = new CacheControlHeaderValue
                {
                    NoCache = true,
                    Public = true
                };
                var tuple = new Tuple<IEnumerable<Dtos.FinancialAidApplication>, int>(financialAidApplicationDtos, 4);
                financialAidApplicationService2Mock.Setup(ci => ci.GetAsync(It.IsAny<int>(), It.IsAny<int>(), true)).ReturnsAsync(tuple);
                var financialAidApplications = await financialAidApplicationsController.GetAsync(null);

                var cancelToken = new System.Threading.CancellationToken(false);

                System.Net.Http.HttpResponseMessage httpResponseMessage = await financialAidApplications.ExecuteAsync(cancelToken);

                IEnumerable<Dtos.FinancialAidApplication> actuals = ((ObjectContent<IEnumerable<Ellucian.Colleague.Dtos.FinancialAidApplication>>)httpResponseMessage.Content)
                                                                .Value as IEnumerable<Dtos.FinancialAidApplication>;


                Assert.AreEqual(financialAidApplicationDtos.Count, actuals.Count());

                foreach (var actual in actuals)
                {
                    var expected = financialAidApplicationDtos.FirstOrDefault(i => i.Id.Equals(actual.Id, StringComparison.OrdinalIgnoreCase));

                    Assert.IsNotNull(expected);
                    Assert.AreEqual(expected.Id, actual.Id);
                    Assert.AreEqual(expected.AidYear, actual.AidYear);
                    //Assert.AreEqual(expected.AwardFund, actual.AwardFund);
                    //Assert.AreEqual(expected.Student, actual.Student);
                }
            }

            [TestMethod]
            public async Task FinancialAidApplicationsController_GetById()
            {
                var id = "bbd216fb-0fc5-4f44-ae45-42d3cdd1e89a";
                var financialAidApplication = financialAidApplicationDtos.FirstOrDefault(i => i.Id.Equals(id, StringComparison.OrdinalIgnoreCase));
                financialAidApplicationService2Mock.Setup(ci => ci.GetByIdAsync(id)).ReturnsAsync(financialAidApplication);

                var actual = await financialAidApplicationsController.GetByIdAsync(id);

                var expected = financialAidApplicationDtos.FirstOrDefault(i => i.Id.Equals(actual.Id, StringComparison.OrdinalIgnoreCase));

                Assert.IsNotNull(expected);
                Assert.AreEqual(expected.Id, actual.Id);
                Assert.AreEqual(expected.AidYear, actual.AidYear);
                //Assert.AreEqual(expected.AwardFund, actual.AwardFund);
                //Assert.AreEqual(expected.Student, actual.Student);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidApplicationsController_GetAll_PermissionException()
            {
                financialAidApplicationsController.Request.Headers.CacheControl = new CacheControlHeaderValue
                {
                    NoCache = false,
                    Public = true
                };
                var tuple = new Tuple<IEnumerable<Dtos.FinancialAidApplication>, int>(financialAidApplicationDtos, 4);
                financialAidApplicationService2Mock.Setup(ci => ci.GetAsync(offset, limit, false)).ThrowsAsync(new PermissionsException());
                var financialAidApplications = await financialAidApplicationsController.GetAsync(new Paging(limit, offset));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidApplicationsController_GetAll_ArgumentException()
            {
                financialAidApplicationsController.Request.Headers.CacheControl = new CacheControlHeaderValue
                {
                    NoCache = false,
                    Public = true
                };
                var tuple = new Tuple<IEnumerable<Dtos.FinancialAidApplication>, int>(financialAidApplicationDtos, 4);
                financialAidApplicationService2Mock.Setup(ci => ci.GetAsync(offset, limit, false)).ThrowsAsync(new ArgumentException());
                var financialAidApplications = await financialAidApplicationsController.GetAsync(new Paging(limit, offset));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidApplicationsController_GetAll_RepositoryException()
            {
                financialAidApplicationsController.Request.Headers.CacheControl = new CacheControlHeaderValue
                {
                    NoCache = false,
                    Public = true
                };
                var tuple = new Tuple<IEnumerable<Dtos.FinancialAidApplication>, int>(financialAidApplicationDtos, 4);
                financialAidApplicationService2Mock.Setup(ci => ci.GetAsync(offset, limit, false)).ThrowsAsync(new RepositoryException());
                var financialAidApplications = await financialAidApplicationsController.GetAsync(new Paging(limit, offset));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidApplicationsController_GetAll_Exception()
            {
                financialAidApplicationsController.Request.Headers.CacheControl = new CacheControlHeaderValue
                {
                    NoCache = false,
                    Public = true
                };
                var tuple = new Tuple<IEnumerable<Dtos.FinancialAidApplication>, int>(financialAidApplicationDtos, 4);
                financialAidApplicationService2Mock.Setup(ci => ci.GetAsync(offset, limit, false)).ThrowsAsync(new Exception());
                var financialAidApplications = await financialAidApplicationsController.GetAsync(new Paging(limit, offset));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidApplicationsController_GetById_PermissionsException()
            {
                financialAidApplicationService2Mock.Setup(ci => ci.GetByIdAsync(It.IsAny<string>())).ThrowsAsync(new PermissionsException());

                var actual = await financialAidApplicationsController.GetByIdAsync("ds");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidApplicationsController_GetById_RepositoryException()
            {
                financialAidApplicationService2Mock.Setup(ci => ci.GetByIdAsync(It.IsAny<string>())).ThrowsAsync(new RepositoryException());

                var actual = await financialAidApplicationsController.GetByIdAsync("ds");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidApplicationsController_GetById_ArgumentException()
            {
                financialAidApplicationService2Mock.Setup(ci => ci.GetByIdAsync(It.IsAny<string>())).ThrowsAsync(new ArgumentException());

                var actual = await financialAidApplicationsController.GetByIdAsync(It.IsAny<string>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidApplicationsController_GetById_Exception()
            {
                financialAidApplicationService2Mock.Setup(ci => ci.GetByIdAsync(It.IsAny<string>())).ThrowsAsync(new Exception());

                var actual = await financialAidApplicationsController.GetByIdAsync("ds");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidApplicationsController_GetById_KeyNotFoundException()
            {
                financialAidApplicationService2Mock.Setup(ci => ci.GetByIdAsync(It.IsAny<string>())).ThrowsAsync(new KeyNotFoundException());

                var actual = await financialAidApplicationsController.GetByIdAsync("ds");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidApplicationsController_PUT_Not_Supported()
            {
                var actual = await financialAidApplicationsController.UpdateAsync(It.IsAny<string>(), It.IsAny<Dtos.FinancialAidApplication>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidApplicationsController_POST_Not_Supported()
            {
                var actual = await financialAidApplicationsController.CreateAsync(It.IsAny<Dtos.FinancialAidApplication>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidApplicationsController_DELETE_Not_Supported()
            {
                await financialAidApplicationsController.DeleteAsync(It.IsAny<string>());
            }

            //Restricted
     }

    }
}
