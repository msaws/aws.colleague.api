﻿/*Copyright 2014-2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web.Http;
using Ellucian.Colleague.Api.Controllers.FinancialAid;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.FinancialAid.Services;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System.Threading.Tasks;
using System.Net.Http;
using System.Web.Http.Hosting;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Web.Security;

namespace Ellucian.Colleague.Api.Tests.Controllers.FinancialAid
{
    [TestClass]
    public class FinancialAidOfficesControllerTests
    {
        [TestClass]
        public class GetFinancialAidOfficesTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            private Mock<IAdapterRegistry> adapterRegistryMock;
            private Mock<ILogger> loggerMock;
            private Mock<IFinancialAidOfficeService> financialAidOfficeServiceMock;

            private IEnumerable<FinancialAidOffice> expectedOffices;
            private IEnumerable<FinancialAidOffice> actualOffices;

            private FinancialAidOfficesController financialAidOfficesController;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                adapterRegistryMock = new Mock<IAdapterRegistry>();
                loggerMock = new Mock<ILogger>();
                financialAidOfficeServiceMock = new Mock<IFinancialAidOfficeService>();

                expectedOffices = new List<FinancialAidOffice>()
                {
                    new FinancialAidOffice()
                    {
                        Id = "MAIN",
                        Name = "Main Office",
                        AddressLabel = new List<string>() {"2375 Fair Lakes Court", "Fairfax, VA 22033"},
                        PhoneNumber = "555-555-5555",
                        EmailAddress = "mainfaoffice@ellucian.edu",
                        DirectorName = "Cindy Lou"                                        
                    },
                    new FinancialAidOffice()
                    {
                        Id = "LAW",
                        Name = "Law Office",
                        AddressLabel = new List<string>() {"444 MadeUp Dr.", "Whatever, ST 54321"},
                        PhoneNumber = "666-666-6666",
                        EmailAddress = "lawfaoffice@ellucian.edu",
                        DirectorName = "JD Director"    
                    }
                };

                financialAidOfficeServiceMock.Setup(o => o.GetFinancialAidOffices()).Returns(expectedOffices);
                financialAidOfficesController = new FinancialAidOfficesController(adapterRegistryMock.Object, financialAidOfficeServiceMock.Object, loggerMock.Object);
                actualOffices = financialAidOfficesController.GetFinancialAidOffices();
            }

            [TestCleanup]
            public void Cleanup()
            {
                adapterRegistryMock = null;
                loggerMock = null;
                financialAidOfficeServiceMock = null;
                expectedOffices = null;
                actualOffices = null;
                financialAidOfficesController = null;
            }

            [TestMethod]
            public void PropertiesAreEqualTest()
            {
                foreach (var expectedOffice in expectedOffices)
                {
                    var actualOffice = actualOffices.FirstOrDefault(o => o.Id == expectedOffice.Id);
                    Assert.IsNotNull(actualOffice);

                    Assert.AreEqual(expectedOffice.Name, actualOffice.Name);
                    Assert.AreEqual(expectedOffice.PhoneNumber, actualOffice.PhoneNumber);
                    Assert.AreEqual(expectedOffice.DirectorName, actualOffice.DirectorName);
                    Assert.AreEqual(expectedOffice.EmailAddress, actualOffice.EmailAddress);
                    Assert.AreEqual(expectedOffice.AddressLabel.Count(), actualOffice.AddressLabel.Count());
                    for (int i = 0; i < expectedOffice.AddressLabel.Count(); i++)
                    {
                        Assert.AreEqual(expectedOffice.AddressLabel[i], actualOffice.AddressLabel[i]);
                    }
                }
            }

            [TestMethod]
            public void CatchKeyNotFoundExceptionAndLogMessageTest()
            {
                financialAidOfficeServiceMock.Setup(s => s.GetFinancialAidOffices()).Throws(new KeyNotFoundException("Not FoundException"));

                var exceptionCaught = false;
                try
                {
                    financialAidOfficesController.GetFinancialAidOffices();
                }
                catch (HttpResponseException hre)
                {
                    exceptionCaught = true;
                    Assert.AreEqual(System.Net.HttpStatusCode.NotFound, hre.Response.StatusCode);
                }
                Assert.IsTrue(exceptionCaught);
                loggerMock.Verify(l => l.Error(It.IsAny<KeyNotFoundException>(), It.IsAny<string>()));
            }

            [TestMethod]
            public void CatchUnknownExceptionAndLogMessageTest()
            {
                financialAidOfficeServiceMock.Setup(s => s.GetFinancialAidOffices()).Throws(new Exception("Unknown Exception"));

                var exceptionCaught = false;
                try
                {
                    financialAidOfficesController.GetFinancialAidOffices();
                }
                catch (HttpResponseException hre)
                {
                    exceptionCaught = true;
                    Assert.AreEqual(System.Net.HttpStatusCode.BadRequest, hre.Response.StatusCode);
                }
                Assert.IsTrue(exceptionCaught);
                loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
            }
        }

        [TestClass]
        public class GetFinancialAidOfficesAsyncTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            private Mock<IAdapterRegistry> adapterRegistryMock;
            private Mock<ILogger> loggerMock;
            private Mock<IFinancialAidOfficeService> financialAidOfficeServiceMock;

            private IEnumerable<FinancialAidOffice> expectedOffices;
            private IEnumerable<FinancialAidOffice> actualOffices;

            private FinancialAidOfficesController financialAidOfficesController;

            [TestInitialize]
            public async void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                adapterRegistryMock = new Mock<IAdapterRegistry>();
                loggerMock = new Mock<ILogger>();
                financialAidOfficeServiceMock = new Mock<IFinancialAidOfficeService>();

                expectedOffices = new List<FinancialAidOffice>()
                {
                    new FinancialAidOffice()
                    {
                        Id = "MAIN",
                        Name = "Main Office",
                        AddressLabel = new List<string>() {"2375 Fair Lakes Court", "Fairfax, VA 22033"},
                        PhoneNumber = "555-555-5555",
                        EmailAddress = "mainfaoffice@ellucian.edu",
                        DirectorName = "Cindy Lou"                                        
                    },
                    new FinancialAidOffice()
                    {
                        Id = "LAW",
                        Name = "Law Office",
                        AddressLabel = new List<string>() {"444 MadeUp Dr.", "Whatever, ST 54321"},
                        PhoneNumber = "666-666-6666",
                        EmailAddress = "lawfaoffice@ellucian.edu",
                        DirectorName = "JD Director"    
                    }
                };

                financialAidOfficeServiceMock.Setup(o => o.GetFinancialAidOfficesAsync()).ReturnsAsync(expectedOffices);
                financialAidOfficesController = new FinancialAidOfficesController(adapterRegistryMock.Object, financialAidOfficeServiceMock.Object, loggerMock.Object);
                actualOffices = await financialAidOfficesController.GetFinancialAidOfficesAsync();
            }

            [TestCleanup]
            public void Cleanup()
            {
                adapterRegistryMock = null;
                loggerMock = null;
                financialAidOfficeServiceMock = null;
                expectedOffices = null;
                actualOffices = null;
                financialAidOfficesController = null;
            }

            [TestMethod]
            public void PropertiesAreEqualTest()
            {
                foreach (var expectedOffice in expectedOffices)
                {
                    var actualOffice = actualOffices.FirstOrDefault(o => o.Id == expectedOffice.Id);
                    Assert.IsNotNull(actualOffice);

                    Assert.AreEqual(expectedOffice.Name, actualOffice.Name);
                    Assert.AreEqual(expectedOffice.PhoneNumber, actualOffice.PhoneNumber);
                    Assert.AreEqual(expectedOffice.DirectorName, actualOffice.DirectorName);
                    Assert.AreEqual(expectedOffice.EmailAddress, actualOffice.EmailAddress);
                    Assert.AreEqual(expectedOffice.AddressLabel.Count(), actualOffice.AddressLabel.Count());
                    for (int i = 0; i < expectedOffice.AddressLabel.Count(); i++)
                    {
                        Assert.AreEqual(expectedOffice.AddressLabel[i], actualOffice.AddressLabel[i]);
                    }
                }
            }

            [TestMethod]
            public async Task CatchKeyNotFoundExceptionAndLogMessageTest()
            {
                financialAidOfficeServiceMock.Setup(s => s.GetFinancialAidOfficesAsync()).Throws(new KeyNotFoundException("Not FoundException"));

                var exceptionCaught = false;
                try
                {
                    await financialAidOfficesController.GetFinancialAidOfficesAsync();
                }
                catch (HttpResponseException hre)
                {
                    exceptionCaught = true;
                    Assert.AreEqual(System.Net.HttpStatusCode.NotFound, hre.Response.StatusCode);
                }
                Assert.IsTrue(exceptionCaught);
                loggerMock.Verify(l => l.Error(It.IsAny<KeyNotFoundException>(), It.IsAny<string>()));
            }

            [TestMethod]
            public async Task CatchUnknownExceptionAndLogMessageTest()
            {
                financialAidOfficeServiceMock.Setup(s => s.GetFinancialAidOfficesAsync()).Throws(new Exception("Unknown Exception"));

                var exceptionCaught = false;
                try
                {
                    await financialAidOfficesController.GetFinancialAidOfficesAsync();
                }
                catch (HttpResponseException hre)
                {
                    exceptionCaught = true;
                    Assert.AreEqual(System.Net.HttpStatusCode.BadRequest, hre.Response.StatusCode);
                }
                Assert.IsTrue(exceptionCaught);
                loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
            }
        }

        [TestClass]
        public class GetFinancialAidOffices2Tests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            private Mock<IAdapterRegistry> adapterRegistryMock;
            private Mock<ILogger> loggerMock;
            private Mock<IFinancialAidOfficeService> financialAidOfficeServiceMock;

            private IEnumerable<FinancialAidOffice2> expectedOffices;
            private IEnumerable<FinancialAidOffice2> actualOffices;

            private FinancialAidOfficesController financialAidOfficesController;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                adapterRegistryMock = new Mock<IAdapterRegistry>();
                loggerMock = new Mock<ILogger>();
                financialAidOfficeServiceMock = new Mock<IFinancialAidOfficeService>();

                expectedOffices = new List<FinancialAidOffice2>()
                {
                    new FinancialAidOffice2()
                    {
                        Id = "MAIN",
                        Name = "Main Office",
                        AddressLabel = new List<string>() {"2375 Fair Lakes Court", "Fairfax, VA 22033"},
                        PhoneNumber = "555-555-5555",
                        EmailAddress = "mainfaoffice@ellucian.edu",
                        DirectorName = "Cindy Lou"                                        
                    },
                    new FinancialAidOffice2()
                    {
                        Id = "LAW",
                        Name = "Law Office",
                        AddressLabel = new List<string>() {"444 MadeUp Dr.", "Whatever, ST 54321"},
                        PhoneNumber = "666-666-6666",
                        EmailAddress = "lawfaoffice@ellucian.edu",
                        DirectorName = "JD Director"    
                    }
                };

                financialAidOfficeServiceMock.Setup(o => o.GetFinancialAidOffices2()).Returns(expectedOffices);
                financialAidOfficesController = new FinancialAidOfficesController(adapterRegistryMock.Object, financialAidOfficeServiceMock.Object, loggerMock.Object);
                actualOffices = financialAidOfficesController.GetFinancialAidOffices2();
            }

            [TestCleanup]
            public void Cleanup()
            {
                adapterRegistryMock = null;
                loggerMock = null;
                financialAidOfficeServiceMock = null;
                expectedOffices = null;
                actualOffices = null;
                financialAidOfficesController = null;
            }

            [TestMethod]
            public void PropertiesAreEqualTest2()
            {
                foreach (var expectedOffice in expectedOffices)
                {
                    var actualOffice = actualOffices.FirstOrDefault(o => o.Id == expectedOffice.Id);
                    Assert.IsNotNull(actualOffice);

                    Assert.AreEqual(expectedOffice.Name, actualOffice.Name);
                    Assert.AreEqual(expectedOffice.PhoneNumber, actualOffice.PhoneNumber);
                    Assert.AreEqual(expectedOffice.DirectorName, actualOffice.DirectorName);
                    Assert.AreEqual(expectedOffice.EmailAddress, actualOffice.EmailAddress);
                    Assert.AreEqual(expectedOffice.AddressLabel.Count(), actualOffice.AddressLabel.Count());
                    for (int i = 0; i < expectedOffice.AddressLabel.Count(); i++)
                    {
                        Assert.AreEqual(expectedOffice.AddressLabel[i], actualOffice.AddressLabel[i]);
                    }
                }
            }

            [TestMethod]
            public void CatchKeyNotFoundExceptionAndLogMessageTest2()
            {
                financialAidOfficeServiceMock.Setup(s => s.GetFinancialAidOffices2()).Throws(new KeyNotFoundException("Not FoundException"));

                var exceptionCaught = false;
                try
                {
                    financialAidOfficesController.GetFinancialAidOffices2();
                }
                catch (HttpResponseException hre)
                {
                    exceptionCaught = true;
                    Assert.AreEqual(System.Net.HttpStatusCode.NotFound, hre.Response.StatusCode);
                }
                Assert.IsTrue(exceptionCaught);
                loggerMock.Verify(l => l.Error(It.IsAny<KeyNotFoundException>(), It.IsAny<string>()));
            }

            [TestMethod]
            public void CatchUnknownExceptionAndLogMessageTest2()
            {
                financialAidOfficeServiceMock.Setup(s => s.GetFinancialAidOffices2()).Throws(new Exception("Unknown Exception"));

                var exceptionCaught = false;
                try
                {
                    financialAidOfficesController.GetFinancialAidOffices2();
                }
                catch (HttpResponseException hre)
                {
                    exceptionCaught = true;
                    Assert.AreEqual(System.Net.HttpStatusCode.BadRequest, hre.Response.StatusCode);
                }
                Assert.IsTrue(exceptionCaught);
                loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
            }
        }

        [TestClass]
        public class GetFinancialAidOffices2AsyncTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            private Mock<IAdapterRegistry> adapterRegistryMock;
            private Mock<ILogger> loggerMock;
            private Mock<IFinancialAidOfficeService> financialAidOfficeServiceMock;

            private IEnumerable<FinancialAidOffice2> expectedOffices;
            private IEnumerable<FinancialAidOffice2> actualOffices;

            private FinancialAidOfficesController financialAidOfficesController;

            [TestInitialize]
            public async void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                adapterRegistryMock = new Mock<IAdapterRegistry>();
                loggerMock = new Mock<ILogger>();
                financialAidOfficeServiceMock = new Mock<IFinancialAidOfficeService>();

                expectedOffices = new List<FinancialAidOffice2>()
                {
                    new FinancialAidOffice2()
                    {
                        Id = "MAIN",
                        Name = "Main Office",
                        AddressLabel = new List<string>() {"2375 Fair Lakes Court", "Fairfax, VA 22033"},
                        PhoneNumber = "555-555-5555",
                        EmailAddress = "mainfaoffice@ellucian.edu",
                        DirectorName = "Cindy Lou"                                        
                    },
                    new FinancialAidOffice2()
                    {
                        Id = "LAW",
                        Name = "Law Office",
                        AddressLabel = new List<string>() {"444 MadeUp Dr.", "Whatever, ST 54321"},
                        PhoneNumber = "666-666-6666",
                        EmailAddress = "lawfaoffice@ellucian.edu",
                        DirectorName = "JD Director"    
                    }
                };

                financialAidOfficeServiceMock.Setup(o => o.GetFinancialAidOffices2Async()).ReturnsAsync(expectedOffices);
                financialAidOfficesController = new FinancialAidOfficesController(adapterRegistryMock.Object, financialAidOfficeServiceMock.Object, loggerMock.Object);
                actualOffices = await financialAidOfficesController.GetFinancialAidOffices2Async();
            }

            [TestCleanup]
            public void Cleanup()
            {
                adapterRegistryMock = null;
                loggerMock = null;
                financialAidOfficeServiceMock = null;
                expectedOffices = null;
                actualOffices = null;
                financialAidOfficesController = null;
            }

            [TestMethod]
            public void PropertiesAreEqualTest2()
            {
                foreach (var expectedOffice in expectedOffices)
                {
                    var actualOffice = actualOffices.FirstOrDefault(o => o.Id == expectedOffice.Id);
                    Assert.IsNotNull(actualOffice);

                    Assert.AreEqual(expectedOffice.Name, actualOffice.Name);
                    Assert.AreEqual(expectedOffice.PhoneNumber, actualOffice.PhoneNumber);
                    Assert.AreEqual(expectedOffice.DirectorName, actualOffice.DirectorName);
                    Assert.AreEqual(expectedOffice.EmailAddress, actualOffice.EmailAddress);
                    Assert.AreEqual(expectedOffice.AddressLabel.Count(), actualOffice.AddressLabel.Count());
                    for (int i = 0; i < expectedOffice.AddressLabel.Count(); i++)
                    {
                        Assert.AreEqual(expectedOffice.AddressLabel[i], actualOffice.AddressLabel[i]);
                    }
                }
            }

            [TestMethod]
            public async Task CatchKeyNotFoundExceptionAndLogMessageTest2()
            {
                financialAidOfficeServiceMock.Setup(s => s.GetFinancialAidOffices2Async()).Throws(new KeyNotFoundException("Not FoundException"));

                var exceptionCaught = false;
                try
                {
                    await financialAidOfficesController.GetFinancialAidOffices2Async();
                }
                catch (HttpResponseException hre)
                {
                    exceptionCaught = true;
                    Assert.AreEqual(System.Net.HttpStatusCode.NotFound, hre.Response.StatusCode);
                }
                Assert.IsTrue(exceptionCaught);
                loggerMock.Verify(l => l.Error(It.IsAny<KeyNotFoundException>(), It.IsAny<string>()));
            }

            [TestMethod]
            public async Task CatchUnknownExceptionAndLogMessageTest2()
            {
                financialAidOfficeServiceMock.Setup(s => s.GetFinancialAidOffices2Async()).Throws(new Exception("Unknown Exception"));

                var exceptionCaught = false;
                try
                {
                    await financialAidOfficesController.GetFinancialAidOffices2Async();
                }
                catch (HttpResponseException hre)
                {
                    exceptionCaught = true;
                    Assert.AreEqual(System.Net.HttpStatusCode.BadRequest, hre.Response.StatusCode);
                }
                Assert.IsTrue(exceptionCaught);
                loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
            }
        }

        [TestClass]
        public class FinancialAidOfficesEedmControllerTests
        {
            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext { get; set; }

            private Mock<IFinancialAidOfficeService> financialAidOfficeServiceMock;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private Mock<ILogger> loggerMock;
            private FinancialAidOfficesController financialAidOfficesController;
            private IEnumerable<Dtos.FinancialAidOffice> allFinancialAidOffice;
            private List<Dtos.FinancialAidOffice> financialAidOfficeCollection;

            [TestInitialize]
            public async void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.DeploymentDirectory, "App_Data"));

                financialAidOfficeServiceMock = new Mock<IFinancialAidOfficeService>();
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                loggerMock = new Mock<ILogger>();
                financialAidOfficeCollection = new List<Dtos.FinancialAidOffice>();

                allFinancialAidOffice = new List<Dtos.FinancialAidOffice>()
                {
                    new Dtos.FinancialAidOffice()
                    {
                        Id = "MAIN",
                        Name = "Main Office",
                        AddressLines = new List<string>() {"2375 Fair Lakes Court", "Fairfax, VA 22033"},
                        PhoneNumber = new Dtos.DtoProperties.NumberDtoProperty() { Number = "555-555-5555" },
                        EmailAddress = "mainfaoffice@ellucian.edu",
                        AidAdministrator = "Cindy Lou"                                        
                    },
                    new Dtos.FinancialAidOffice()
                    {
                        Id = "LAW",
                        Name = "Law Office",
                        AddressLines = new List<string>() {"444 MadeUp Dr.", "Whatever, ST 54321"},
                        PhoneNumber = new Dtos.DtoProperties.NumberDtoProperty() { Number = "666-666-6666" },
                        EmailAddress = "lawfaoffice@ellucian.edu",
                        AidAdministrator = "JD Director"    
                    }
                };

                foreach (var source in allFinancialAidOffice)
                {
                    var financialAidOffice = new Ellucian.Colleague.Dtos.FinancialAidOffice
                    {
                        Id = source.Id,
                        Code = source.Code,
                        Description = null,
                        Name = source.Name
                    };
                    financialAidOfficeCollection.Add(financialAidOffice);
                }

                financialAidOfficesController = new FinancialAidOfficesController(adapterRegistryMock.Object, financialAidOfficeServiceMock.Object, loggerMock.Object)
                {
                    Request = new HttpRequestMessage()
                };
                financialAidOfficesController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
            }

            [TestCleanup]
            public void Cleanup()
            {
                financialAidOfficesController = null;
                allFinancialAidOffice = null;
                financialAidOfficeCollection = null;
                loggerMock = null;
                financialAidOfficeServiceMock = null;
            }

            [TestMethod]
            public async Task FinancialAidOfficeController_GetFinancialAidOffice_ValidateFields_Nocache()
            {
                financialAidOfficesController.Request.Headers.CacheControl =
                     new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };

                financialAidOfficeServiceMock.Setup(x => x.GetFinancialAidOfficesAsync(false)).ReturnsAsync(financialAidOfficeCollection);

                var sourceContexts = (await financialAidOfficesController.GetEedmFinancialAidOfficesAsync()).ToList();
                Assert.AreEqual(financialAidOfficeCollection.Count, sourceContexts.Count);
                for (var i = 0; i < sourceContexts.Count; i++)
                {
                    var expected = financialAidOfficeCollection[i];
                    var actual = sourceContexts[i];
                    Assert.AreEqual(expected.Id, actual.Id, "Id, Index=" + i.ToString());
                    Assert.AreEqual(expected.Code, actual.Code, "Code, Index=" + i.ToString());
                }
            }

            [TestMethod]
            public async Task FinancialAidOfficeController_GetFinancialAidOffice_ValidateFields_Cache()
            {
                financialAidOfficesController.Request.Headers.CacheControl =
                    new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = true };

                financialAidOfficeServiceMock.Setup(x => x.GetFinancialAidOfficesAsync(true)).ReturnsAsync(financialAidOfficeCollection);

                var sourceContexts = (await financialAidOfficesController.GetEedmFinancialAidOfficesAsync()).ToList();
                Assert.AreEqual(financialAidOfficeCollection.Count, sourceContexts.Count);
                for (var i = 0; i < sourceContexts.Count; i++)
                {
                    var expected = financialAidOfficeCollection[i];
                    var actual = sourceContexts[i];
                    Assert.AreEqual(expected.Id, actual.Id, "Id, Index=" + i.ToString());
                    Assert.AreEqual(expected.Code, actual.Code, "Code, Index=" + i.ToString());
                }
            }

            [TestMethod]
            public async Task FinancialAidOfficeController_GetFinancialAidOfficesByIdAsync_ValidateFields()
            {
                var expected = financialAidOfficeCollection.FirstOrDefault();
                financialAidOfficeServiceMock.Setup(x => x.GetFinancialAidOfficeByGuidAsync(expected.Id)).ReturnsAsync(expected);

                var actual = await financialAidOfficesController.GetFinancialAidOfficeByGuidAsync(expected.Id);

                Assert.AreEqual(expected.Id, actual.Id, "Id");
                Assert.AreEqual(expected.Code, actual.Code, "Code");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidOfficeController_GetFinancialAidOffice_PermissionsException()
            {
                financialAidOfficeServiceMock.Setup(x => x.GetFinancialAidOfficesAsync(false)).Throws<PermissionsException>();
                await financialAidOfficesController.GetEedmFinancialAidOfficesAsync();
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidOfficeController_GetFinancialAidOffice_KeyNotFoundException()
            {
                financialAidOfficeServiceMock.Setup(x => x.GetFinancialAidOfficesAsync(false)).Throws<KeyNotFoundException>();
                await financialAidOfficesController.GetEedmFinancialAidOfficesAsync();
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidOfficeController_GetFinancialAidOffice_ArgumentNullException()
            {
                financialAidOfficeServiceMock.Setup(x => x.GetFinancialAidOfficesAsync(false)).Throws<ArgumentNullException>();
                await financialAidOfficesController.GetEedmFinancialAidOfficesAsync();
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidOfficeController_GetFinancialAidOffice_RepositoryException()
            {
                financialAidOfficeServiceMock.Setup(x => x.GetFinancialAidOfficesAsync(false)).Throws<RepositoryException>();
                await financialAidOfficesController.GetEedmFinancialAidOfficesAsync();
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidOfficeController_GetFinancialAidOffice_IntgApiException()
            {
                financialAidOfficeServiceMock.Setup(x => x.GetFinancialAidOfficesAsync(false)).Throws<IntegrationApiException>();
                await financialAidOfficesController.GetEedmFinancialAidOfficesAsync();
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidOfficeController_GetFinancialAidOffice_Exception()
            {
                financialAidOfficeServiceMock.Setup(x => x.GetFinancialAidOfficesAsync(false)).Throws<Exception>();
                await financialAidOfficesController.GetEedmFinancialAidOfficesAsync();
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidOfficeController_GetFinancialAidOfficesByIdAsync_PermissionsException()
            {
                financialAidOfficeServiceMock.Setup(x => x.GetFinancialAidOfficeByGuidAsync(It.IsAny<string>())).Throws<PermissionsException>();
                await financialAidOfficesController.GetFinancialAidOfficeByGuidAsync(string.Empty);
        }

        [TestClass]
        public class GetFinancialAidOffices3AsyncTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            private Mock<IAdapterRegistry> adapterRegistryMock;
            private Mock<ILogger> loggerMock;
            private Mock<IFinancialAidOfficeService> financialAidOfficeServiceMock;

            private IEnumerable<FinancialAidOffice3> expectedOffices;
            private IEnumerable<FinancialAidOffice3> actualOffices;

            private FinancialAidOfficesController financialAidOfficesController;

            [TestInitialize]
            public async void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                adapterRegistryMock = new Mock<IAdapterRegistry>();
                loggerMock = new Mock<ILogger>();
                financialAidOfficeServiceMock = new Mock<IFinancialAidOfficeService>();

                expectedOffices = new List<FinancialAidOffice3>()
                {
                    new FinancialAidOffice3()
                    {
                        Id = "MAIN",
                        Name = "Main Office",
                        AddressLabel = new List<string>() {"2375 Fair Lakes Court", "Fairfax, VA 22033"},
                        PhoneNumber = "555-555-5555",
                        EmailAddress = "mainfaoffice@ellucian.edu",
                        DirectorName = "Cindy Lou"                                        
                    },
                    new FinancialAidOffice3()
                    {
                        Id = "LAW",
                        Name = "Law Office",
                        AddressLabel = new List<string>() {"444 MadeUp Dr.", "Whatever, ST 54321"},
                        PhoneNumber = "666-666-6666",
                        EmailAddress = "lawfaoffice@ellucian.edu",
                        DirectorName = "JD Director"    
                    }
                };

                financialAidOfficeServiceMock.Setup(o => o.GetFinancialAidOffices3Async()).ReturnsAsync(expectedOffices);
                financialAidOfficesController = new FinancialAidOfficesController(adapterRegistryMock.Object, financialAidOfficeServiceMock.Object, loggerMock.Object);
                actualOffices = await financialAidOfficesController.GetFinancialAidOffices3Async();
            }

            [TestCleanup]
            public void Cleanup()
            {
                adapterRegistryMock = null;
                loggerMock = null;
                financialAidOfficeServiceMock = null;
                expectedOffices = null;
                actualOffices = null;
                financialAidOfficesController = null;
            }

            [TestMethod]
            public void PropertiesAreEqualTest2()
            {
                foreach (var expectedOffice in expectedOffices)
                {
                    var actualOffice = actualOffices.FirstOrDefault(o => o.Id == expectedOffice.Id);
                    Assert.IsNotNull(actualOffice);

                    Assert.AreEqual(expectedOffice.Name, actualOffice.Name);
                    Assert.AreEqual(expectedOffice.PhoneNumber, actualOffice.PhoneNumber);
                    Assert.AreEqual(expectedOffice.DirectorName, actualOffice.DirectorName);
                    Assert.AreEqual(expectedOffice.EmailAddress, actualOffice.EmailAddress);
                    Assert.AreEqual(expectedOffice.AddressLabel.Count(), actualOffice.AddressLabel.Count());
                    for (int i = 0; i < expectedOffice.AddressLabel.Count(); i++)
                    {
                        Assert.AreEqual(expectedOffice.AddressLabel[i], actualOffice.AddressLabel[i]);
                    }
                }
            }

            [TestMethod]
            public async Task CatchKeyNotFoundExceptionAndLogMessageTest2()
            {
                financialAidOfficeServiceMock.Setup(s => s.GetFinancialAidOffices3Async()).Throws(new KeyNotFoundException("Not FoundException"));

                var exceptionCaught = false;
                try
                {
                    await financialAidOfficesController.GetFinancialAidOffices3Async();
                }
                catch (HttpResponseException hre)
                {
                    exceptionCaught = true;
                    Assert.AreEqual(System.Net.HttpStatusCode.NotFound, hre.Response.StatusCode);
                }
                Assert.IsTrue(exceptionCaught);
                loggerMock.Verify(l => l.Error(It.IsAny<KeyNotFoundException>(), It.IsAny<string>()));
            }

            [TestMethod]
            public async Task CatchUnknownExceptionAndLogMessageTest2()
            {
                financialAidOfficeServiceMock.Setup(s => s.GetFinancialAidOffices3Async()).Throws(new Exception("Unknown Exception"));

                var exceptionCaught = false;
                try
                {
                    await financialAidOfficesController.GetFinancialAidOffices3Async();
                }
                catch (HttpResponseException hre)
                {
                    exceptionCaught = true;
                    Assert.AreEqual(System.Net.HttpStatusCode.BadRequest, hre.Response.StatusCode);
                }
                Assert.IsTrue(exceptionCaught);
                loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
            }
        }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidOfficeController_GetFinancialAidOfficesByIdAsync_KeyNotFoundException()
            {
                financialAidOfficeServiceMock.Setup(x => x.GetFinancialAidOfficeByGuidAsync(It.IsAny<string>())).Throws<KeyNotFoundException>();
                await financialAidOfficesController.GetFinancialAidOfficeByGuidAsync(string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidOfficeController_GetFinancialAidOfficesByIdAsync_ArgumentNullException()
            {
                financialAidOfficeServiceMock.Setup(x => x.GetFinancialAidOfficeByGuidAsync(It.IsAny<string>())).Throws<ArgumentNullException>();
                await financialAidOfficesController.GetFinancialAidOfficeByGuidAsync(string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidOfficeController_GetFinancialAidOfficesByIdAsync_RepositoryException()
            {
                financialAidOfficeServiceMock.Setup(x => x.GetFinancialAidOfficeByGuidAsync(It.IsAny<string>())).Throws<RepositoryException>();
                await financialAidOfficesController.GetFinancialAidOfficeByGuidAsync(string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidOfficeController_GetFinancialAidOfficesByIdAsync_IntgApiException()
            {
                financialAidOfficeServiceMock.Setup(x => x.GetFinancialAidOfficeByGuidAsync(It.IsAny<string>())).Throws<IntegrationApiException>();
                await financialAidOfficesController.GetFinancialAidOfficeByGuidAsync(string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidOfficeController_GetFinancialAidOfficesByIdAsync_Exception()
            {
                financialAidOfficeServiceMock.Setup(x => x.GetFinancialAidOfficeByGuidAsync(It.IsAny<string>())).Throws<Exception>();
                await financialAidOfficesController.GetFinancialAidOfficeByGuidAsync(string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidOfficeController_PostFinancialAidOfficesAsync_Exception()
            {
                await financialAidOfficesController.PostFinancialAidOfficeAsync(financialAidOfficeCollection.FirstOrDefault());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidOfficeController_PutFinancialAidOfficesAsync_Exception()
            {
                var sourceContext = financialAidOfficeCollection.FirstOrDefault();
                await financialAidOfficesController.PutFinancialAidOfficeAsync(sourceContext.Id, sourceContext);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task FinancialAidOfficeController_DeleteFinancialAidOfficesAsync_Exception()
            {
                await financialAidOfficesController.DeleteFinancialAidOfficeAsync(financialAidOfficeCollection.FirstOrDefault().Id);
            }
        }
    }
}
