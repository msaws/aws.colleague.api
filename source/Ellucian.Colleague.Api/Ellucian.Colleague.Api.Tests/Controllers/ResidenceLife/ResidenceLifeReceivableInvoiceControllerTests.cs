﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Web.Http;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Api.Controllers.ResidenceLife;
using Ellucian.Colleague.Coordination.ResidenceLife.Services;
using Ellucian.Colleague.Dtos.ResidenceLife;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Moq;
using slf4net;

namespace Ellucian.Colleague.Api.Tests.Controllers.ResidenceLife
{
    [TestClass]
    public class ResidenceLifeReceivableInvoiceControllerTests
    {

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get {return testContextInstance;}
            set {testContextInstance = value;}
        }
        
        private ResidenceLifeReceivableInvoicesController residenceLifeReceivableInvoicesController;
        private Mock<IResidenceLifeAccountsReceivableService> residenceLifeAccountsReceivableServiceMock;
        private IResidenceLifeAccountsReceivableService residenceLifeAccountsReceivableService;
        private ILogger logger = new Mock<ILogger>().Object;

        private string externalId = "1";
        private string permissionsExceptionExternalId = "2";
        private string exceptionExternalId = "3";

        [TestInitialize]
        public void Initialize()
        {
            // Copied from AdvisorControllerTests. Causes the license check in the controller to pass.
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            // Mock ResidenceLifeAccountsReceivableService to construct the controller
            residenceLifeAccountsReceivableServiceMock = new Mock<IResidenceLifeAccountsReceivableService>();
            residenceLifeAccountsReceivableService = residenceLifeAccountsReceivableServiceMock.Object;
            logger = new Mock<ILogger>().Object;

            // Instantiate controller with mock service
            residenceLifeReceivableInvoicesController = new ResidenceLifeReceivableInvoicesController(residenceLifeAccountsReceivableService, logger);

            // Mock successful CreateReceivableInvoice
            Dtos.ResidenceLife.ReceivableInvoice receivableInvoiceIn = new Dtos.ResidenceLife.ReceivableInvoice();
            Dtos.Finance.ReceivableInvoice receivableInvoiceOut = new Dtos.Finance.ReceivableInvoice();
            receivableInvoiceIn.ExternalIdentifier = externalId;
            receivableInvoiceOut.ExternalIdentifier = externalId;
            residenceLifeAccountsReceivableServiceMock.Setup(
                x => x.CreateReceivableInvoice(It.Is<Dtos.ResidenceLife.ReceivableInvoice>(ri => ri.ExternalIdentifier == externalId)))
                .Returns(receivableInvoiceOut);

            // Mock CreateReceivableInvoice to return permissions exception
            residenceLifeAccountsReceivableServiceMock.Setup(
                x => x.CreateReceivableInvoice(It.Is<Dtos.ResidenceLife.ReceivableInvoice>(ri => ri.ExternalIdentifier == permissionsExceptionExternalId)))
                .Throws(new PermissionsException());

            // Mock CreateReceivableInvoice to return a generic exception
            residenceLifeAccountsReceivableServiceMock.Setup(
                x => x.CreateReceivableInvoice(It.Is<Dtos.ResidenceLife.ReceivableInvoice>(ri => ri.ExternalIdentifier == exceptionExternalId)))
                .Throws(new Exception());
        }

        // Validate that a successful service CreateReceivableInvoice call is handled by the controller
        [TestMethod]
        public void SuccessfulCreateReceivableInvoice()
        {
            Dtos.Finance.ReceivableInvoice receivableInvoiceOut;
            receivableInvoiceOut = residenceLifeReceivableInvoicesController.PostReceivableInvoice(
                new Dtos.ResidenceLife.ReceivableInvoice { ExternalIdentifier = externalId });
            Assert.AreEqual(externalId, receivableInvoiceOut.ExternalIdentifier);
        }

        // Validate that a permissions exception from the CreateReceivableInvoice call is handled by the controller
        [TestMethod]
        public void PermissionsExceptionCreateReceivableInvoice()
        {
            try
            {
                Dtos.Finance.ReceivableInvoice receivableInvoice;
                receivableInvoice = residenceLifeReceivableInvoicesController.PostReceivableInvoice(
                    new Dtos.ResidenceLife.ReceivableInvoice { ExternalIdentifier = permissionsExceptionExternalId });
                Assert.Fail("Exception expected");
            }
            catch (Exception ex)
            {
                Assert.AreEqual(typeof(HttpResponseException), ex.GetType());
                HttpResponseException hex = (HttpResponseException)ex;
                Assert.AreEqual(hex.Response.StatusCode, HttpStatusCode.Forbidden);
            }
        }

        // Validate that a non-permissions exception from the CreateReceivableInvoice call is handled by the controller
        [TestMethod]
        public void GenericExceptionCreateReceivableInvoice()
        {
            try
            {
                Dtos.Finance.ReceivableInvoice receivableInvoice;
                receivableInvoice = residenceLifeReceivableInvoicesController.PostReceivableInvoice(
                    new Dtos.ResidenceLife.ReceivableInvoice { ExternalIdentifier = exceptionExternalId });
                Assert.Fail("Exception expected");
            }
            catch (Exception ex)
            {
                Assert.AreEqual(typeof(HttpResponseException), ex.GetType());
                HttpResponseException hex = (HttpResponseException)ex;
                Assert.AreEqual(hex.Response.StatusCode, HttpStatusCode.BadRequest);
            }

        }
    }
}
