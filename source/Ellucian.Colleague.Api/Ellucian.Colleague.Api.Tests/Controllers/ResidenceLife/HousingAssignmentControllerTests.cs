﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Web.Http;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Api.Controllers.ResidenceLife;
using Ellucian.Colleague.Coordination.ResidenceLife.Services;
using Ellucian.Colleague.Dtos.ResidenceLife;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Moq;
using slf4net;

namespace Ellucian.Colleague.Api.Tests.Controllers.ResidenceLife
{
    [TestClass]
    public class HousingAssignmentControllerTests
    {

        private TestContext testContextInstance;
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get {return testContextInstance;}
            set {testContextInstance = value;}
        }
        
        private HousingAssignmentsController housingAssignmentController;
        private Mock<IHousingAssignmentService> housingAssignmentServiceMock;
        private IHousingAssignmentService housingAssignmentService;
        private ILogger logger = new Mock<ILogger>().Object;
        private string housingAssignmentFakeId1 = "1";
        private string housingAssignmentPermExId = "2";
        private string housingAssignmentExId = "3";

        [TestInitialize]
        public void Initialize()
        {
            // Copied from AdvisorControllerTests. Causes the license check in the controller to pass.
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            // Mock HousingAssignmentService to construct the controller
            housingAssignmentServiceMock = new Mock<IHousingAssignmentService>();
            housingAssignmentService = housingAssignmentServiceMock.Object;
            logger = new Mock<ILogger>().Object;

            // Instantiate controller with mock service
            housingAssignmentController = new HousingAssignmentsController(housingAssignmentService, logger);

            // Mock successful GetHousingAssignment for an ID
            Dtos.ResidenceLife.HousingAssignment housingAssignment;
            housingAssignment = new Dtos.ResidenceLife.HousingAssignment();
            housingAssignment.Id = housingAssignmentFakeId1;
            housingAssignmentServiceMock.Setup(x => x.GetHousingAssignment(housingAssignmentFakeId1)).Returns(housingAssignment);

            // Mock GetHousingAssignment to return permissions exception
            housingAssignmentServiceMock.Setup(x => x.GetHousingAssignment(housingAssignmentPermExId)).Throws(new PermissionsException());

            // Mock GetHousingAssignment to return a generic exception
            housingAssignmentServiceMock.Setup(x => x.GetHousingAssignment(housingAssignmentExId)).Throws(new Exception());

            // Mock successful CreateHousingAssignment for an ID
            housingAssignmentServiceMock.Setup(x =>
                x.CreateHousingAssignment(It.Is<Dtos.ResidenceLife.HousingAssignment>(
                    dto => (dto.Id == housingAssignmentFakeId1)))).Returns(housingAssignment);

            // Mock CreateHousingAssignment to return permissions exception
            housingAssignmentServiceMock.Setup(x =>
                x.CreateHousingAssignment(It.Is<Dtos.ResidenceLife.HousingAssignment>(
                    dto => (dto.Id == housingAssignmentPermExId)))).Throws(new PermissionsException());

            // Mock CreateHousingAssignment to return a generic exception
            housingAssignmentServiceMock.Setup(x =>
                x.CreateHousingAssignment(It.Is<Dtos.ResidenceLife.HousingAssignment>(
                    dto => (dto.Id == housingAssignmentExId)))).Throws(new Exception());

            // Mock successful UpdateHousingAssignment for an ID
            housingAssignmentServiceMock.Setup(x =>
                x.UpdateHousingAssignment(It.Is<Dtos.ResidenceLife.HousingAssignment>(
                    dto => (dto.Id == housingAssignmentFakeId1)))).Returns(housingAssignment);

            // Mock UpdateHousingAssignment to return permissions exception
            housingAssignmentServiceMock.Setup(x =>
                x.UpdateHousingAssignment(It.Is<Dtos.ResidenceLife.HousingAssignment>(
                    dto => (dto.Id == housingAssignmentPermExId)))).Throws(new PermissionsException());

            // Mock UpdateHousingAssignment to return a generic exception
            housingAssignmentServiceMock.Setup(x =>
                x.UpdateHousingAssignment(It.Is<Dtos.ResidenceLife.HousingAssignment>(
                    dto => (dto.Id == housingAssignmentExId)))).Throws(new Exception());

        }

        // Validate that a successful service get call is handled
        [TestMethod]
        public void SuccessfulGetCall()
        {
            Dtos.ResidenceLife.HousingAssignment housingAssignment;
            housingAssignment = housingAssignmentController.GetHousingAssignment(housingAssignmentFakeId1);
            Assert.AreEqual(housingAssignmentFakeId1, housingAssignment.Id);
        }

        // Validate that a permissions exception from the service get call is handled
        [TestMethod]
        public void PermissionsExceptionGetCall()
        {
            Dtos.ResidenceLife.HousingAssignment housingAssignment;
            try
            {
                housingAssignment = housingAssignmentController.GetHousingAssignment(housingAssignmentPermExId);
                Assert.Fail("Exception expected");
            }
            catch (Exception ex)
            {
                Assert.AreEqual(typeof(HttpResponseException), ex.GetType());
                HttpResponseException hex = (HttpResponseException)ex;
                Assert.AreEqual(hex.Response.StatusCode, HttpStatusCode.Forbidden);
            }
        }

        // Validate that a non-permissions exception from the service get call is handled
        [TestMethod]
        public void GenericExceptionGetCall()
        {
            Dtos.ResidenceLife.HousingAssignment housingAssignment;
            try
            {
                housingAssignment = housingAssignmentController.GetHousingAssignment(housingAssignmentExId);
                Assert.Fail("Exception expected");
            }
            catch (Exception ex)
            {
                Assert.AreEqual(typeof(HttpResponseException), ex.GetType());
                HttpResponseException hex = (HttpResponseException)ex;
                Assert.AreEqual(hex.Response.StatusCode, HttpStatusCode.BadRequest);
            }
        }

        // Validate that a successful service Create call is handled
        [TestMethod]
        public void SuccessfulPostCall()
        {
            Dtos.ResidenceLife.HousingAssignment housingAssignmentOut;
            Dtos.ResidenceLife.HousingAssignment housingAssignmentIn = new Dtos.ResidenceLife.HousingAssignment();
            housingAssignmentIn.Id = housingAssignmentFakeId1;

            housingAssignmentOut = housingAssignmentController.PostHousingAssignment(housingAssignmentIn);
            Assert.AreEqual(housingAssignmentFakeId1, housingAssignmentOut.Id);
        }

        // Validate that a permissions exception from the service Create call is handled
        [TestMethod]
        public void PermissionsExceptionPostCall()
        {
            Dtos.ResidenceLife.HousingAssignment housingAssignmentOut;
            Dtos.ResidenceLife.HousingAssignment housingAssignmentIn = new Dtos.ResidenceLife.HousingAssignment();
            housingAssignmentIn.Id = housingAssignmentPermExId;
            try
            {
                housingAssignmentOut = housingAssignmentController.PostHousingAssignment(housingAssignmentIn);
                Assert.Fail("Exception expected");
            }
            catch (Exception ex)
            {
                Assert.AreEqual(typeof(HttpResponseException), ex.GetType());
                HttpResponseException hex = (HttpResponseException)ex;
                Assert.AreEqual(hex.Response.StatusCode, HttpStatusCode.Forbidden);
            }
        }

        // Validate that a non-permissions exception from the service Create call is handled
        [TestMethod]
        public void GenericExceptionPostCall()
        {
            Dtos.ResidenceLife.HousingAssignment housingAssignmentOut;
            Dtos.ResidenceLife.HousingAssignment housingAssignmentIn = new Dtos.ResidenceLife.HousingAssignment();
            housingAssignmentIn.Id = housingAssignmentExId;
            try
            {
                housingAssignmentOut = housingAssignmentController.PostHousingAssignment(housingAssignmentIn);
                Assert.Fail("Exception expected");
            }
            catch (Exception ex)
            {
                Assert.AreEqual(typeof(HttpResponseException), ex.GetType());
                HttpResponseException hex = (HttpResponseException)ex;
                Assert.AreEqual(hex.Response.StatusCode, HttpStatusCode.BadRequest);
            }
        }

        // Validate that a successful service Update call is handled
        [TestMethod]
        public void SuccessfulPutCall()
        {
            Dtos.ResidenceLife.HousingAssignment housingAssignmentOut;
            Dtos.ResidenceLife.HousingAssignment housingAssignmentIn = new Dtos.ResidenceLife.HousingAssignment();
            housingAssignmentIn.Id = housingAssignmentFakeId1;

            housingAssignmentOut = housingAssignmentController.PutHousingAssignment(housingAssignmentIn);
            Assert.AreEqual(housingAssignmentFakeId1, housingAssignmentOut.Id);
        }

        // Validate that a permissions exception from the service Update call is handled
        [TestMethod]
        public void PermissionsExceptionPutCall()
        {
            Dtos.ResidenceLife.HousingAssignment housingAssignmentOut;
            Dtos.ResidenceLife.HousingAssignment housingAssignmentIn = new Dtos.ResidenceLife.HousingAssignment();
            housingAssignmentIn.Id = housingAssignmentPermExId;
            try
            {
                housingAssignmentOut = housingAssignmentController.PutHousingAssignment(housingAssignmentIn);
                Assert.Fail("Exception expected");
            }
            catch (Exception ex)
            {
                Assert.AreEqual(typeof(HttpResponseException), ex.GetType());
                HttpResponseException hex = (HttpResponseException)ex;
                Assert.AreEqual(hex.Response.StatusCode, HttpStatusCode.Forbidden);
            }
        }

        // Validate that a non-permissions exception from the service Update call is handled
        [TestMethod] 
        public void GenericExceptionPutCall()
        {
            Dtos.ResidenceLife.HousingAssignment housingAssignmentOut;
            Dtos.ResidenceLife.HousingAssignment housingAssignmentIn = new Dtos.ResidenceLife.HousingAssignment();
            housingAssignmentIn.Id = housingAssignmentExId;
            try
            {
                housingAssignmentOut = housingAssignmentController.PutHousingAssignment(housingAssignmentIn);
                Assert.Fail("Exception expected");
            }
            catch (Exception ex)
            {
                Assert.AreEqual(typeof(HttpResponseException), ex.GetType());
                HttpResponseException hex = (HttpResponseException)ex;
                Assert.AreEqual(hex.Response.StatusCode, HttpStatusCode.BadRequest);
            }
        }
    }
}
