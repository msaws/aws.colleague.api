﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Web.Http;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Api.Controllers.ResidenceLife;
using Ellucian.Colleague.Coordination.ResidenceLife.Services;
using Ellucian.Colleague.Dtos.ResidenceLife;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Moq;
using slf4net;

namespace Ellucian.Colleague.Api.Tests.Controllers.ResidenceLife
{
    [TestClass]
    public class MealPlanAssignmentControllerTests
    {

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get {return testContextInstance;}
            set {testContextInstance = value;}
        }
        
        private MealPlanAssignmentsController mealPlanAssignmentController;
        private Mock<IMealPlanAssignmentService> mealPlanAssignmentServiceMock;
        private IMealPlanAssignmentService mealPlanAssignmentService;
        private ILogger logger = new Mock<ILogger>().Object;
        private string mealPlanAssignmentFakeId1 = "1";
        private string mealPlanAssignmentPermExId = "2";
        private string mealPlanAssignmentExId = "3";

        [TestInitialize]
        public void Initialize()
        {
            // Copied from AdvisorControllerTests. Causes the license check in the controller to pass.
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            // Mock MealPlanAssignmentService to construct the controller
            mealPlanAssignmentServiceMock = new Mock<IMealPlanAssignmentService>();
            mealPlanAssignmentService = mealPlanAssignmentServiceMock.Object;
            logger = new Mock<ILogger>().Object;

            // Instantiate controller with mock service
            mealPlanAssignmentController = new MealPlanAssignmentsController(mealPlanAssignmentService, logger);

            // Mock successful GetMealPlanAssignment for an ID
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignment;
            mealPlanAssignment = new Dtos.ResidenceLife.MealPlanAssignment();
            mealPlanAssignment.Id = mealPlanAssignmentFakeId1;
            mealPlanAssignmentServiceMock.Setup(x => x.GetMealPlanAssignment(mealPlanAssignmentFakeId1)).Returns(mealPlanAssignment);

            // Mock GetMealPlanAssignment to return permissions exception
            mealPlanAssignmentServiceMock.Setup(x => x.GetMealPlanAssignment(mealPlanAssignmentPermExId)).Throws(new PermissionsException());

            // Mock GetMealPlanAssignment to return a generic exception
            mealPlanAssignmentServiceMock.Setup(x => x.GetMealPlanAssignment(mealPlanAssignmentExId)).Throws(new Exception());

            // Mock successful CreateMealPlanAssignment for an ID
            mealPlanAssignmentServiceMock.Setup(x =>
                x.CreateMealPlanAssignment(It.Is<Dtos.ResidenceLife.MealPlanAssignment>(
                    dto => (dto.Id == mealPlanAssignmentFakeId1)))).Returns(mealPlanAssignment);

            // Mock CreateMealPlanAssignment to return permissions exception
            mealPlanAssignmentServiceMock.Setup(x =>
                x.CreateMealPlanAssignment(It.Is<Dtos.ResidenceLife.MealPlanAssignment>(
                    dto => (dto.Id == mealPlanAssignmentPermExId)))).Throws(new PermissionsException());

            // Mock CreateMealPlanAssignment to return a generic exception
            mealPlanAssignmentServiceMock.Setup(x =>
                x.CreateMealPlanAssignment(It.Is<Dtos.ResidenceLife.MealPlanAssignment>(
                    dto => (dto.Id == mealPlanAssignmentExId)))).Throws(new Exception());

            // Mock successful UpdateMealPlanAssignment for an ID
            mealPlanAssignmentServiceMock.Setup(x =>
                x.UpdateMealPlanAssignment(It.Is<Dtos.ResidenceLife.MealPlanAssignment>(
                    dto => (dto.Id == mealPlanAssignmentFakeId1)))).Returns(mealPlanAssignment);

            // Mock UpdateMealPlanAssignment to return permissions exception
            mealPlanAssignmentServiceMock.Setup(x =>
                x.UpdateMealPlanAssignment(It.Is<Dtos.ResidenceLife.MealPlanAssignment>(
                    dto => (dto.Id == mealPlanAssignmentPermExId)))).Throws(new PermissionsException());

            // Mock UpdateMealPlanAssignment to return a generic exception
            mealPlanAssignmentServiceMock.Setup(x =>
                x.UpdateMealPlanAssignment(It.Is<Dtos.ResidenceLife.MealPlanAssignment>(
                    dto => (dto.Id == mealPlanAssignmentExId)))).Throws(new Exception());

        }

        // Validate that a successful service get call is handled
        [TestMethod]
        public void SuccessfulGetCall()
        {
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignment;
            mealPlanAssignment = mealPlanAssignmentController.GetMealPlanAssignment(mealPlanAssignmentFakeId1);
            Assert.AreEqual(mealPlanAssignmentFakeId1, mealPlanAssignment.Id);
        }

        // Validate that a permissions exception from the service get call is handled
        [TestMethod]
        public void PermissionsExceptionGetCall()
        {
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignment;
            try
            {
                mealPlanAssignment = mealPlanAssignmentController.GetMealPlanAssignment(mealPlanAssignmentPermExId);
                Assert.Fail("Exception expected");
            }
            catch (Exception ex)
            {
                Assert.AreEqual(typeof(HttpResponseException), ex.GetType());
                HttpResponseException hex = (HttpResponseException)ex;
                Assert.AreEqual(hex.Response.StatusCode, HttpStatusCode.Forbidden);
            }
        }

        // Validate that a non-permissions exception from the service get call is handled
        [TestMethod]
        public void GenericExceptionGetCall()
        {
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignment;
            try
            {
                mealPlanAssignment = mealPlanAssignmentController.GetMealPlanAssignment(mealPlanAssignmentExId);
                Assert.Fail("Exception expected");
            }
            catch (Exception ex)
            {
                Assert.AreEqual(typeof(HttpResponseException), ex.GetType());
                HttpResponseException hex = (HttpResponseException)ex;
                Assert.AreEqual(hex.Response.StatusCode, HttpStatusCode.BadRequest);
            }
        }

        // Validate that a successful service Create call is handled
        [TestMethod]
        public void SuccessfulPostCall()
        {
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentOut;
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentIn = new Dtos.ResidenceLife.MealPlanAssignment();
            mealPlanAssignmentIn.Id = mealPlanAssignmentFakeId1;

            mealPlanAssignmentOut = mealPlanAssignmentController.PostMealPlanAssignment(mealPlanAssignmentIn);
            Assert.AreEqual(mealPlanAssignmentFakeId1, mealPlanAssignmentOut.Id);
        }

        // Validate that a permissions exception from the service Create call is handled
        [TestMethod]
        public void PermissionsExceptionPostCall()
        {
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentOut;
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentIn = new Dtos.ResidenceLife.MealPlanAssignment();
            mealPlanAssignmentIn.Id = mealPlanAssignmentPermExId;
            try
            {
                mealPlanAssignmentOut = mealPlanAssignmentController.PostMealPlanAssignment(mealPlanAssignmentIn);
                Assert.Fail("Exception expected");
            }
            catch (Exception ex)
            {
                Assert.AreEqual(typeof(HttpResponseException), ex.GetType());
                HttpResponseException hex = (HttpResponseException)ex;
                Assert.AreEqual(hex.Response.StatusCode, HttpStatusCode.Forbidden);
            }
        }

        // Validate that a non-permissions exception from the service Create call is handled
        [TestMethod]
        public void GenericExceptionPostCall()
        {
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentOut;
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentIn = new Dtos.ResidenceLife.MealPlanAssignment();
            mealPlanAssignmentIn.Id = mealPlanAssignmentExId;
            try
            {
                mealPlanAssignmentOut = mealPlanAssignmentController.PostMealPlanAssignment(mealPlanAssignmentIn);
                Assert.Fail("Exception expected");
            }
            catch (Exception ex)
            {
                Assert.AreEqual(typeof(HttpResponseException), ex.GetType());
                HttpResponseException hex = (HttpResponseException)ex;
                Assert.AreEqual(hex.Response.StatusCode, HttpStatusCode.BadRequest);
            }
        }

        // Validate that a successful service Update call is handled
        [TestMethod]
        public void SuccessfulPutCall()
        {
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentOut;
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentIn = new Dtos.ResidenceLife.MealPlanAssignment();
            mealPlanAssignmentIn.Id = mealPlanAssignmentFakeId1;

            mealPlanAssignmentOut = mealPlanAssignmentController.PutMealPlanAssignment(mealPlanAssignmentIn);
            Assert.AreEqual(mealPlanAssignmentFakeId1, mealPlanAssignmentOut.Id);
        }

        // Validate that a permissions exception from the service Update call is handled
        [TestMethod]
        public void PermissionsExceptionPutCall()
        {
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentOut;
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentIn = new Dtos.ResidenceLife.MealPlanAssignment();
            mealPlanAssignmentIn.Id = mealPlanAssignmentPermExId;
            try
            {
                mealPlanAssignmentOut = mealPlanAssignmentController.PutMealPlanAssignment(mealPlanAssignmentIn);
                Assert.Fail("Exception expected");
            }
            catch (Exception ex)
            {
                Assert.AreEqual(typeof(HttpResponseException), ex.GetType());
                HttpResponseException hex = (HttpResponseException)ex;
                Assert.AreEqual(hex.Response.StatusCode, HttpStatusCode.Forbidden);
            }
        }

        // Validate that a non-permissions exception from the service Update call is handled
        [TestMethod] 
        public void GenericExceptionPutCall()
        {
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentOut;
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentIn = new Dtos.ResidenceLife.MealPlanAssignment();
            mealPlanAssignmentIn.Id = mealPlanAssignmentExId;
            try
            {
                mealPlanAssignmentOut = mealPlanAssignmentController.PutMealPlanAssignment(mealPlanAssignmentIn);
                Assert.Fail("Exception expected");
            }
            catch (Exception ex)
            {
                Assert.AreEqual(typeof(HttpResponseException), ex.GetType());
                HttpResponseException hex = (HttpResponseException)ex;
                Assert.AreEqual(hex.Response.StatusCode, HttpStatusCode.BadRequest);
            }
        }
    }
}
