﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Controllers.TimeManagement;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.TimeManagement.Services;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Http.TestUtil;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Ellucian.Colleague.Api.Tests.Controllers.TimeManagement
{
    [TestClass]
    public class TimecardStatusesControllerTests
    {
        public Mock<ILogger> loggerMock;
        public Mock<ITimecardStatusesService> timecardStatusesServiceMock;

        public TimecardStatusesController controllerUnderTest;

        public List<TimecardStatus> expectedStatuses;

        public FunctionEqualityComparer<TimecardStatus> timecardStatusDtoComparer;

        public void TimecardStatusesControllerTestsInitialize()
        {
            loggerMock = new Mock<ILogger>();
            timecardStatusesServiceMock = new Mock<ITimecardStatusesService>();

            timecardStatusDtoComparer = new FunctionEqualityComparer<TimecardStatus>(
                (t1, t2) => t1.Id == t2.Id && t1.TimecardId == t2.TimecardId,
                t => t.Id.GetHashCode() ^ t.TimecardId.GetHashCode());


            expectedStatuses = new List<TimecardStatus>() {
                new TimecardStatus() {
                    Id = "5",
                    TimecardId = "6",                    
                },
                new TimecardStatus() {
                    Id = "6",
                    TimecardId = "6"
                },
                new TimecardStatus() {
                    Id = "7",
                    TimecardId = "98"
                }
            };

            controllerUnderTest = new TimecardStatusesController(loggerMock.Object, timecardStatusesServiceMock.Object);
        }

        [TestClass]
        public class CreateTimecardStatusAsyncTests : TimecardStatusesControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            public string inputId;
            public TimecardStatus inputStatus;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                TimecardStatusesControllerTestsInitialize();

                timecardStatusesServiceMock.Setup(m => m.CreateTimecardStatusesAsync(It.IsAny<List<TimecardStatus>>()))
                    .Returns<IEnumerable<TimecardStatus>>((stts) => Task.FromResult(stts));


                inputStatus = expectedStatuses[0];
                inputId = inputStatus.TimecardId;
            }
            [TestMethod]
            public async Task StatusIsReturnedTest()
            {
                var actual = await controllerUnderTest.CreateTimecardStatusAsync(inputId, inputStatus);
                Assert.IsTrue(timecardStatusDtoComparer.Equals(inputStatus, actual));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task NullTimecardIdErrorTest()
            {
                try
                {
                    await controllerUnderTest.CreateTimecardStatusAsync(null, inputStatus);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task NullStatusErrorTest()
            {
                try
                {
                    await controllerUnderTest.CreateTimecardStatusAsync(inputId, null);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task IdDiffersFromStatusIdTest()
            {
                inputId = "foobar";
                try
                {
                    await controllerUnderTest.CreateTimecardStatusAsync(inputId, inputStatus);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PermissionsErrorCaughtAndLogged()
            {
                try
                {
                    timecardStatusesServiceMock.Setup(d => d.CreateTimecardStatusesAsync(It.IsAny<List<TimecardStatus>>()))
                        .Throws(new PermissionsException());
                    await controllerUnderTest.CreateTimecardStatusAsync(inputId, inputStatus);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.Forbidden, hre.Response.StatusCode);
                    throw hre;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task ErrorCaughtAndLogged()
            {
                try
                {
                    timecardStatusesServiceMock.Setup(d => d.CreateTimecardStatusesAsync(It.IsAny<List<TimecardStatus>>()))
                        .Throws(new Exception());
                    await controllerUnderTest.CreateTimecardStatusAsync(inputId, inputStatus);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw hre;
                }
            }
        }

        [TestClass]
        public class CreateTimecardStatusesAsyncTests : TimecardStatusesControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            public string inputId;
            public TimecardStatus inputStatus;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                TimecardStatusesControllerTestsInitialize();

                timecardStatusesServiceMock.Setup(m => m.CreateTimecardStatusesAsync(It.IsAny<List<TimecardStatus>>()))
                    .Returns<IEnumerable<TimecardStatus>>((stts) => Task.FromResult(stts));


                inputStatus = expectedStatuses[0];
                inputId = inputStatus.TimecardId;
            }
            [TestMethod]
            public async Task StatusIsReturnedTest()
            {
                var actual = await controllerUnderTest.CreateTimecardStatusesAsync(new List<TimecardStatus>() { inputStatus });
                Assert.IsTrue(timecardStatusDtoComparer.Equals(inputStatus, actual.ElementAt(0)));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task TimecardIdErrorTest()
            {
                try
                {
                    inputStatus.TimecardId = string.Empty;
                    await controllerUnderTest.CreateTimecardStatusesAsync(new List<TimecardStatus>() { inputStatus });
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task NullStatusErrorTest()
            {
                try
                {
                    await controllerUnderTest.CreateTimecardStatusesAsync(null);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PermissionsErrorCaughtAndLogged()
            {
                try
                {
                    timecardStatusesServiceMock.Setup(d => d.CreateTimecardStatusesAsync(It.IsAny<List<TimecardStatus>>()))
                        .Throws(new PermissionsException());
                    await controllerUnderTest.CreateTimecardStatusAsync(inputId, inputStatus);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.Forbidden, hre.Response.StatusCode);
                    throw hre;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task ErrorCaughtAndLogged()
            {
                try
                {
                    timecardStatusesServiceMock.Setup(d => d.CreateTimecardStatusesAsync(It.IsAny<List<TimecardStatus>>()))
                        .Throws(new Exception());
                    await controllerUnderTest.CreateTimecardStatusAsync(inputId, inputStatus);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw hre;
                }
            }
        }

        [TestClass]
        public class GetTimecardStatusesByTimecardIdTests : TimecardStatusesControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion
            public string inputTimecardId;
            public IEnumerable<TimecardStatus> expected 
            {
                get
                {
                    return expectedStatuses.Where(s => s.TimecardId == inputTimecardId);
                }
            }

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                TimecardStatusesControllerTestsInitialize();

                timecardStatusesServiceMock.Setup(s => s.GetTimecardStatusesByTimecardIdAsync(It.IsAny<string>()))
                    .Returns<string>((timecardId) => Task.FromResult(expected));

                inputTimecardId = expectedStatuses[0].TimecardId;
            }

            [TestMethod]
            public async Task GetStatusesByTimecardIdTest()
            {
                var actual = await controllerUnderTest.GetTimecardStatusesByTimecardIdAsync(inputTimecardId);

                CollectionAssert.AreEqual(expected.ToList(), actual.ToList(), timecardStatusDtoComparer);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task KeyNotFoundExceptionTest()
            {
                timecardStatusesServiceMock.Setup(s => s.GetTimecardStatusesByTimecardIdAsync(It.IsAny<string>()))
                    .Throws(new KeyNotFoundException());

                try
                {
                    await controllerUnderTest.GetTimecardStatusesByTimecardIdAsync(inputTimecardId);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.NotFound, hre.Response.StatusCode);
                    throw;
                }
            }


            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PermissionsExceptionTest()
            {
                timecardStatusesServiceMock.Setup(s => s.GetTimecardStatusesByTimecardIdAsync(It.IsAny<string>()))
                    .Throws(new PermissionsException());

                try
                {
                    await controllerUnderTest.GetTimecardStatusesByTimecardIdAsync(inputTimecardId);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.Forbidden, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task GenericExceptionTest()
            {
                timecardStatusesServiceMock.Setup(s => s.GetTimecardStatusesByTimecardIdAsync(It.IsAny<string>()))
                    .Throws(new Exception());

                try
                {
                    await controllerUnderTest.GetTimecardStatusesByTimecardIdAsync(inputTimecardId);
                }
                catch(HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }
        }
        
        [TestClass]
        public class GetLatestTimecardStatuses : TimecardStatusesControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                TimecardStatusesControllerTestsInitialize();

                timecardStatusesServiceMock.Setup(s => s.GetTimecardStatusesAsync(It.IsAny<bool>()))
                    .Returns<bool>(b => Task.FromResult(expectedStatuses.AsEnumerable()));                   
            }

            [TestMethod]
            public async Task GetAllTimecardStatusesTest()
            {
                var actual = await controllerUnderTest.GetLatestTimecardStatusesAsync();

                CollectionAssert.AreEqual(expectedStatuses, actual.ToList(), timecardStatusDtoComparer);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchGenericExceptionTest()
            {
                timecardStatusesServiceMock.Setup(s => s.GetTimecardStatusesAsync(It.IsAny<bool>()))
                    .Throws(new Exception());

                try
                {
                    await controllerUnderTest.GetLatestTimecardStatusesAsync();
                }
                catch(HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }
        }
    }
}
