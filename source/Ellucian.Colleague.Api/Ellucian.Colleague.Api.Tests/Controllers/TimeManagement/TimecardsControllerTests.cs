﻿using Ellucian.Colleague.Api.Controllers.TimeManagement;
/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.TimeManagement.Services;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Hosting;
using System.Web.Http.Routing;

namespace Ellucian.Colleague.Api.Tests.Controllers.TimeManagement
{
    [TestClass]
    public class TimecardsControllerTests
    {
        public Mock<ILogger> loggerMock;
        public Mock<ITimecardsService> timecardServiceMock;

        public TimecardsController controllerUnderTest;

        public string employeeId;
        public Timecard timecardDto;
        public List<Timecard> timecardDtos;
        public Timecard2 timecard2Dto;
        public List<Timecard2> timecard2Dtos;

        public void TimecardsTestsInitialize()
        {
            loggerMock = new Mock<ILogger>();
            timecardServiceMock = new Mock<ITimecardsService>();

            employeeId = "24601";
            timecardDto = new Timecard()
            {
                Id = "001",
                EmployeeId = "24601",
                PayCycleId = "001",
                PeriodEndDate = new DateTime(2016, 03, 31),
                PeriodStartDate = new DateTime(2016, 02, 29),
                PositionId = "001",
                Timestamp = new Timestamp(),
                TimeEntries = new List<TimeEntry>() 
                { 
                    new TimeEntry()
                    {
                        Id = "A",
                        EarningsTypeId = "B",
                        InDateTime = new DateTime(2016, 03, 21, 09,00,00),
                        OutDateTime = new DateTime(2016, 03, 21, 17,00,00),
                        PersonLeaveId = "C",
                        ProjectId = "D",
                        TimecardId = "001",
                        Timestamp = new Timestamp(),
                        WorkedTime = new TimeSpan(08,00,00),                      
                        WorkedDate = new DateTime(2016, 03, 21, 00,00,00)
                    }
                }

            };
            timecardDtos = new List<Timecard>(){timecardDto};
            timecard2Dto = new Timecard2()
            {
                Id = "001",
                EmployeeId = "24601",
                PayCycleId = "001",
                PeriodEndDate = new DateTime(2016, 03, 31),
                PeriodStartDate = new DateTime(2016, 02, 29),
                PositionId = "001",
                Timestamp = new Timestamp(),
                TimeEntries = new List<TimeEntry2>() 
                { 
                    new TimeEntry2()
                    {
                        Id = "A",
                        EarningsTypeId = "B",
                        InDateTime = new DateTime(2016, 03, 21, 09,00,00),
                        OutDateTime = new DateTime(2016, 03, 21, 17,00,00),
                        PersonLeaveId = "C",
                        ProjectId = "D",
                        TimecardId = "001",
                        Timestamp = new Timestamp(),
                        WorkedTime = new TimeSpan(08,00,00),                      
                        WorkedDate = new DateTime(2016, 03, 21, 00,00,00)
                    }
                }

            };
            timecard2Dtos = new List<Timecard2>() { timecard2Dto };
            // v1
            timecardServiceMock.Setup(s => s.GetTimecardsAsync()).ReturnsAsync(timecardDtos);
            timecardServiceMock.Setup(s => s.GetTimecardAsync(It.IsAny<string>())).Returns<string>((id) => Task.FromResult(timecardDtos.First(t => t.Id == id)));
            timecardServiceMock.Setup(s => s.CreateTimecardAsync(It.IsAny<Timecard>()))
                .Returns<Timecard>((inTimecard) =>
                {
                    var idCnt = new Random();
                    inTimecard.Id = "foo";
                    inTimecard.TimeEntries.ForEach(te => te.Id = idCnt.Next(1, 1000).ToString());
                    return Task.FromResult(inTimecard);
                });
            timecardServiceMock.Setup(s => s.UpdateTimecardAsync(It.IsAny<Timecard>()))
                .Returns<Timecard>((inTimecard) => Task.FromResult(inTimecard));
            // v2
            timecardServiceMock.Setup(s => s.GetTimecards2Async()).ReturnsAsync(timecard2Dtos);
            timecardServiceMock.Setup(s => s.GetTimecard2Async(It.IsAny<string>())).Returns<string>((id) => Task.FromResult(timecard2Dtos.First(t => t.Id == id)));
            timecardServiceMock.Setup(s => s.CreateTimecard2Async(It.IsAny<Timecard2>()))
                .Returns<Timecard2>((inTimecard) =>
                {
                    var idCnt = new Random();
                    inTimecard.Id = "foo";
                    inTimecard.TimeEntries.ForEach(te => te.Id = idCnt.Next(1, 1000).ToString());
                    return Task.FromResult(inTimecard);
                });
            timecardServiceMock.Setup(s => s.UpdateTimecard2Async(It.IsAny<Timecard2>()))
                .Returns<Timecard2>((inTimecard) => Task.FromResult(inTimecard));

            controllerUnderTest = new TimecardsController(loggerMock.Object, timecardServiceMock.Object);
        }

        #region v1
        [TestClass]
        public class GetTimecardsAsyncTests : TimecardsControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            public string inputEmployeeId;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                base.TimecardsTestsInitialize();

                inputEmployeeId = employeeId;
            }

            [TestMethod]
            public async Task TimecardsReturnedTest()
            {
                var actual = await controllerUnderTest.GetTimecardsAsync();
                Assert.AreEqual(timecardDtos.Count(), actual.Count());               
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchPermissionsExceptionTest()
            {
                timecardServiceMock.Setup(d => d.GetTimecardsAsync())
                    .Throws(new PermissionsException());

                try
                {
                    await controllerUnderTest.GetTimecardsAsync();
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<PermissionsException>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.Forbidden, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchGenericExceptionTest()
            {
                timecardServiceMock.Setup(d => d.GetTimecardsAsync())
                    .Throws(new Exception());
                try
                {
                    await controllerUnderTest.GetTimecardsAsync();
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }
        }

        [TestClass]
        public class GetTimecardAsyncTests : TimecardsControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            public string inputTimecardId;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                base.TimecardsTestsInitialize();

                inputTimecardId = timecardDto.Id;
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InputTimecardIdIsRequiredTest()
            {
                try
                {
                    await controllerUnderTest.GetTimecardAsync(null);
                }
                catch(HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            public async Task TimecardDtoReturnedTest()
            {
                var actual = await controllerUnderTest.GetTimecardAsync(inputTimecardId);
                Assert.AreEqual(inputTimecardId, actual.Id);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchPermissionExceptionTest()
            {
                timecardServiceMock.Setup(s => s.GetTimecardAsync(It.IsAny<string>())).Throws(new PermissionsException("pex"));
                try
                {
                    await controllerUnderTest.GetTimecardAsync(inputTimecardId);
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<PermissionsException>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.Forbidden, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchKeyNotFoundExceptionTest()
            {
                timecardServiceMock.Setup(s => s.GetTimecardAsync(It.IsAny<string>())).Throws(new KeyNotFoundException("knfe"));
                try
                {
                    await controllerUnderTest.GetTimecardAsync(inputTimecardId);
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<KeyNotFoundException>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.NotFound, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchGenericExceptionTest()
            {
                timecardServiceMock.Setup(s => s.GetTimecardAsync(It.IsAny<string>())).Throws(new ApplicationException("ae"));
                try
                {
                    await controllerUnderTest.GetTimecardAsync(inputTimecardId);
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<ApplicationException>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }
            
        }

        [TestClass]
        public class CreateTimecardAsyncTests : TimecardsControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            public Timecard inputTimecard;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                base.TimecardsTestsInitialize();

                inputTimecard = timecardDto;

                //this sets up the route for location link resolution
                var config = new HttpConfiguration();
                var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost/api/timecards/");
                var getRoute = config.Routes.MapHttpRoute(

                    name: "GetTimecardAsync",
                    routeTemplate: "api/timecards/{id}",
                    defaults: new { controller = "Timecards", action = "GetTimecardAsync" }
                );
                var routeData = new HttpRouteData(getRoute);
                var context = new HttpControllerContext(config, routeData, request);

                var httpResponse = new HttpResponse(new StringWriter());
                HttpContext.Current = new HttpContext(new HttpRequest("", "http://doesntMatter.com", ""), httpResponse);

                controllerUnderTest = new TimecardsController(loggerMock.Object, timecardServiceMock.Object);

                //this sets up the request
                controllerUnderTest.ControllerContext = new HttpControllerContext(config, routeData, request);
                controllerUnderTest.Request = request;
                controllerUnderTest.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;
                controllerUnderTest.Url = new UrlHelper(request);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InputTimecardRequiredTest()
            {
                await controllerUnderTest.CreateTimecardAsync(null);
            }

            [TestMethod]
            public async Task CreateInputTimecardSuccessTest()
            {
                var response = await controllerUnderTest.CreateTimecardAsync(inputTimecard);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);
               

                var actualTimecard = JsonConvert.DeserializeObject<Timecard>(response.Content.ReadAsStringAsync().Result);
                
                Assert.AreEqual(HttpContext.Current.Response.RedirectLocation, string.Format("http://localhost/api/timecards/{0}", actualTimecard.Id));               
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchPermissionsExceptiontest()
            {
                timecardServiceMock.Setup(s => s.CreateTimecardAsync(It.IsAny<Timecard>())).Throws(new PermissionsException());
                try
                {
                    await controllerUnderTest.CreateTimecardAsync(inputTimecard);
                }
                catch(HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.Forbidden, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<PermissionsException>(), It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchExistingResourceExceptiontest()
            {
                timecardServiceMock.Setup(s => s.CreateTimecardAsync(It.IsAny<Timecard>())).Throws(new ExistingResourceException("foo", "bar"));
                try
                {
                    await controllerUnderTest.CreateTimecardAsync(inputTimecard);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.Conflict, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<ExistingResourceException>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpContext.Current.Response.RedirectLocation, string.Format("http://localhost/api/timecards/{0}", "bar"));    
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchGenericExceptiontest()
            {
                timecardServiceMock.Setup(s => s.CreateTimecardAsync(It.IsAny<Timecard>())).Throws(new Exception());
                try
                {
                    await controllerUnderTest.CreateTimecardAsync(inputTimecard);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
                    throw;
                }
            }
        }

        [TestClass]
        public class UpdateTimecardAsyncTests : TimecardsControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            public Timecard inputTimecard;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                base.TimecardsTestsInitialize();

                inputTimecard = timecardDto;
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InputTimecardRequiredTest()
            {
                await controllerUnderTest.UpdateTimecardAsync(null);
            }

            [TestMethod]
            public async Task ReturnsUpdatedTimecardTest()
            {
                var actual = await controllerUnderTest.UpdateTimecardAsync(inputTimecard);
                Assert.AreEqual(inputTimecard.Id, actual.Id);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchPermissionsExceptionTest()
            {
                timecardServiceMock.Setup(s => s.UpdateTimecardAsync(It.IsAny<Timecard>())).Throws(new PermissionsException());
                try
                {
                    await controllerUnderTest.UpdateTimecardAsync(inputTimecard);
                }
                catch(HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.Forbidden, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<PermissionsException>(), It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchKeyNotFoundExceptionTest()
            {
                timecardServiceMock.Setup(s => s.UpdateTimecardAsync(It.IsAny<Timecard>())).Throws(new KeyNotFoundException());
                try
                {
                    await controllerUnderTest.UpdateTimecardAsync(inputTimecard);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.NotFound, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<KeyNotFoundException>(), It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchRecordLockExceptionTest()
            {
                timecardServiceMock.Setup(s => s.UpdateTimecardAsync(It.IsAny<Timecard>())).Throws(new RecordLockException());
                try
                {
                    await controllerUnderTest.UpdateTimecardAsync(inputTimecard);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.Conflict, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<RecordLockException>(), It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchGenericExceptionTest()
            {
                timecardServiceMock.Setup(s => s.UpdateTimecardAsync(It.IsAny<Timecard>())).Throws(new Exception());
                try
                {
                    await controllerUnderTest.UpdateTimecardAsync(inputTimecard);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
                    throw;
                }
            }
        }
        #endregion


        #region v2
        [TestClass]
        public class GetTimecards2AsyncTests : TimecardsControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            public string inputEmployeeId;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                base.TimecardsTestsInitialize();

                inputEmployeeId = employeeId;
            }

            [TestMethod]
            public async Task TimecardsReturnedTest()
            {
                var actual = await controllerUnderTest.GetTimecards2Async();
                Assert.AreEqual(timecard2Dtos.Count(), actual.Count());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchPermissionsExceptionTest()
            {
                timecardServiceMock.Setup(d => d.GetTimecards2Async())
                    .Throws(new PermissionsException());

                try
                {
                    await controllerUnderTest.GetTimecards2Async();
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<PermissionsException>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.Forbidden, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchGenericExceptionTest()
            {
                timecardServiceMock.Setup(d => d.GetTimecards2Async())
                    .Throws(new Exception());
                try
                {
                    await controllerUnderTest.GetTimecards2Async();
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }
        }

        [TestClass]
        public class GetTimecard2AsyncTests : TimecardsControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            public string inputTimecardId;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                base.TimecardsTestsInitialize();

                inputTimecardId = timecardDto.Id;
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InputTimecardIdIsRequiredTest()
            {
                try
                {
                    await controllerUnderTest.GetTimecard2Async(null);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            public async Task TimecardDtoReturnedTest()
            {
                var actual = await controllerUnderTest.GetTimecard2Async(inputTimecardId);
                Assert.AreEqual(inputTimecardId, actual.Id);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchPermissionExceptionTest()
            {
                timecardServiceMock.Setup(s => s.GetTimecard2Async(It.IsAny<string>())).Throws(new PermissionsException("pex"));
                try
                {
                    await controllerUnderTest.GetTimecard2Async(inputTimecardId);
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<PermissionsException>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.Forbidden, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchKeyNotFoundExceptionTest()
            {
                timecardServiceMock.Setup(s => s.GetTimecard2Async(It.IsAny<string>())).Throws(new KeyNotFoundException("knfe"));
                try
                {
                    await controllerUnderTest.GetTimecard2Async(inputTimecardId);
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<KeyNotFoundException>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.NotFound, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchGenericExceptionTest()
            {
                timecardServiceMock.Setup(s => s.GetTimecard2Async(It.IsAny<string>())).Throws(new ApplicationException("ae"));
                try
                {
                    await controllerUnderTest.GetTimecard2Async(inputTimecardId);
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<ApplicationException>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }

        }

        [TestClass]
        public class CreateTimecard2AsyncTests : TimecardsControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            public Timecard2 inputTimecard;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                base.TimecardsTestsInitialize();

                inputTimecard = timecard2Dto;

                //this sets up the route for location link resolution
                var config = new HttpConfiguration();
                var request = new HttpRequestMessage(HttpMethod.Post, "http://localhost/api/timecards/");
                var getRoute = config.Routes.MapHttpRoute(

                    name: "GetTimecard2Async",
                    routeTemplate: "api/timecards/{id}",
                    defaults: new { controller = "Timecards", action = "GetTimecard2Async" }
                );
                var routeData = new HttpRouteData(getRoute);
                var context = new HttpControllerContext(config, routeData, request);

                var httpResponse = new HttpResponse(new StringWriter());
                HttpContext.Current = new HttpContext(new HttpRequest("", "http://doesntMatter.com", ""), httpResponse);

                controllerUnderTest = new TimecardsController(loggerMock.Object, timecardServiceMock.Object);

                //this sets up the request
                controllerUnderTest.ControllerContext = new HttpControllerContext(config, routeData, request);
                controllerUnderTest.Request = request;
                controllerUnderTest.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;
                controllerUnderTest.Url = new UrlHelper(request);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InputTimecardRequiredTest()
            {
                await controllerUnderTest.CreateTimecard2Async(null);
            }

            [TestMethod]
            public async Task CreateInputTimecardSuccessTest()
            {
                var response = await controllerUnderTest.CreateTimecard2Async(inputTimecard);
                Assert.AreEqual(HttpStatusCode.Created, response.StatusCode);


                var actualTimecard = JsonConvert.DeserializeObject<Timecard2>(response.Content.ReadAsStringAsync().Result);

                Assert.AreEqual(HttpContext.Current.Response.RedirectLocation, string.Format("http://localhost/api/timecards/{0}", actualTimecard.Id));
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchPermissionsExceptiontest()
            {
                timecardServiceMock.Setup(s => s.CreateTimecard2Async(It.IsAny<Timecard2>())).Throws(new PermissionsException());
                try
                {
                    await controllerUnderTest.CreateTimecard2Async(inputTimecard);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.Forbidden, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<PermissionsException>(), It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchExistingResourceExceptiontest()
            {
                timecardServiceMock.Setup(s => s.CreateTimecard2Async(It.IsAny<Timecard2>())).Throws(new ExistingResourceException("foo", "bar"));
                try
                {
                    await controllerUnderTest.CreateTimecard2Async(inputTimecard);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.Conflict, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<ExistingResourceException>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpContext.Current.Response.RedirectLocation, string.Format("http://localhost/api/timecards/{0}", "bar"));
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchGenericExceptiontest()
            {
                timecardServiceMock.Setup(s => s.CreateTimecard2Async(It.IsAny<Timecard2>())).Throws(new Exception());
                try
                {
                    await controllerUnderTest.CreateTimecard2Async(inputTimecard);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
                    throw;
                }
            }
        }

        [TestClass]
        public class UpdateTimecard2AsyncTests : TimecardsControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            public Timecard2 inputTimecard;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                base.TimecardsTestsInitialize();

                inputTimecard = timecard2Dto;
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task InputTimecardRequiredTest()
            {
                await controllerUnderTest.UpdateTimecard2Async(null);
            }

            [TestMethod]
            public async Task ReturnsUpdatedTimecardTest()
            {
                var actual = await controllerUnderTest.UpdateTimecard2Async(inputTimecard);
                Assert.AreEqual(inputTimecard.Id, actual.Id);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchPermissionsExceptionTest()
            {
                timecardServiceMock.Setup(s => s.UpdateTimecard2Async(It.IsAny<Timecard2>())).Throws(new PermissionsException());
                try
                {
                    await controllerUnderTest.UpdateTimecard2Async(inputTimecard);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.Forbidden, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<PermissionsException>(), It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchKeyNotFoundExceptionTest()
            {
                timecardServiceMock.Setup(s => s.UpdateTimecard2Async(It.IsAny<Timecard2>())).Throws(new KeyNotFoundException());
                try
                {
                    await controllerUnderTest.UpdateTimecard2Async(inputTimecard);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.NotFound, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<KeyNotFoundException>(), It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchRecordLockExceptionTest()
            {
                timecardServiceMock.Setup(s => s.UpdateTimecard2Async(It.IsAny<Timecard2>())).Throws(new RecordLockException());
                try
                {
                    await controllerUnderTest.UpdateTimecard2Async(inputTimecard);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.Conflict, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<RecordLockException>(), It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchGenericExceptionTest()
            {
                timecardServiceMock.Setup(s => s.UpdateTimecard2Async(It.IsAny<Timecard2>())).Throws(new Exception());
                try
                {
                    await controllerUnderTest.UpdateTimecard2Async(inputTimecard);
                }
                catch (HttpResponseException hre)
                {
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
                    throw;
                }
            }
        }
        #endregion


    }
}
