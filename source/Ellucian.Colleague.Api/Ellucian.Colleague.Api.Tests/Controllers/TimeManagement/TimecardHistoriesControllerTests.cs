﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/

using Ellucian.Colleague.Api.Controllers.TimeManagement;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.TimeManagement.Services;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Ellucian.Colleague.Api.Tests.Controllers.TimeManagement
{
    [TestClass]
    public class TimecardHistoriesControllerTests
    {
        public Mock<ILogger> loggerMock;
        public Mock<ITimecardHistoriesService> timecardHistoriesServiceMock;
        public TimecardHistoriesController timecardHistoriesController;
        public DateTime startDate;
        public DateTime endDate;
        public List<TimecardHistory> timecardHistories;
        public List<TimeEntryHistory> timeEntryHistories;

        public void TimecardHistoriesTestsInitialize()
        {
            loggerMock = new Mock<ILogger>();
            timecardHistoriesServiceMock = new Mock<ITimecardHistoriesService>();

            startDate = new DateTime(2016, 08, 15);
            endDate = new DateTime(2016, 09, 15);

            timecardHistories = new List<TimecardHistory>()
            {
                new TimecardHistory()
                {
                    Id = "001",
                    EmployeeId = "24601",
                    StartDate = new DateTime(2016,09,01),
                    EndDate = new DateTime(2016, 09, 07),
                    PayCycleId = "77",
                    PeriodStartDate = new DateTime(2016, 09, 01),
                    PeriodEndDate = new DateTime(2016, 09, 14),
                    PositionId = "JAJA",
                    Timestamp = new Timestamp()
                    {
                        AddDateTime = new DateTime(2016, 09, 15),
                        ChangeDateTime = new DateTime(2016, 09, 15),
                        AddOperator = "24601",
                        ChangeOperator = "24601"
                    },
                    TimeEntryHistories = new List<TimeEntryHistory>()
                    {
                        new TimeEntryHistory()
                        {
                            Id = "11",
                            EarningsTypeId = ":)LOL",
                            InDateTime = new DateTime(2016, 09, 04),
                            OutDateTime = new DateTime(2016, 09, 04),
                            PersonLeaveId = ":(toobad",
                            ProjectId = "HISTRIONICPERSONALITYDISORDER",
                            TimecardHistoryId = "001",
                            Timestamp = new Timestamp()
                            {
                                AddDateTime = new DateTime(2016, 09, 15),
                                ChangeDateTime = new DateTime(2016, 09, 15),
                                AddOperator = "24601",
                                ChangeOperator = "24601"
                            },
                            WorkedDate = new DateTime(2016,09,04),
                            WorkedTime = new TimeSpan(42)                            
                        }
                    }
                }
            };

            timecardHistoriesController = new TimecardHistoriesController(loggerMock.Object, timecardHistoriesServiceMock.Object);
        }

        #region v1

        [TestClass]
        public class GetTimecardHistoriesAsyncTests : TimecardHistoriesControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            public string inputEmployeeId;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                base.TimecardHistoriesTestsInitialize();

            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchPermissionsExceptionTest()
            {
                timecardHistoriesServiceMock.Setup(d => d.GetTimecardHistoriesAsync(It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                    .Throws(new PermissionsException());

                try
                {
                    await timecardHistoriesController.GetTimecardHistoriesAsync(startDate, endDate);
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<PermissionsException>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.Forbidden, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchGenericExceptionTest()
            {
                timecardHistoriesServiceMock.Setup(d => d.GetTimecardHistoriesAsync(It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                    .Throws(new Exception());
                try
                {
                    await timecardHistoriesController.GetTimecardHistoriesAsync(startDate, endDate);
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }
        }

        #endregion

        #region v2

        [TestClass]
        public class GetTimecardHistories2AsyncTests : TimecardHistoriesControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            public string inputEmployeeId;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                base.TimecardHistoriesTestsInitialize();

            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchPermissionsExceptionTest()
            {
                timecardHistoriesServiceMock.Setup(d => d.GetTimecardHistories2Async(It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                    .Throws(new PermissionsException());

                try
                {
                    await timecardHistoriesController.GetTimecardHistories2Async(startDate, endDate);
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<PermissionsException>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.Forbidden, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchGenericExceptionTest()
            {
                timecardHistoriesServiceMock.Setup(d => d.GetTimecardHistories2Async(It.IsAny<DateTime>(), It.IsAny<DateTime>()))
                    .Throws(new Exception());
                try
                {
                    await timecardHistoriesController.GetTimecardHistories2Async(startDate, endDate);
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }
        }

        #endregion
    }
}
