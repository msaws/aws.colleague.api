﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Api.Controllers.TimeManagement;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.TimeManagement.Services;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Ellucian.Colleague.Api.Tests.Controllers.TimeManagement
{
    [TestClass]
    public class OvertimeControllerTests
    {
        public Mock<ILogger> loggerMock;
        public Mock<IOvertimeCalculationService> overtimeCalculationServiceMock;

        public OvertimeController controllerUnderTest;

        public void OvertimeControllerTestsInitialize()
        {
            loggerMock = new Mock<ILogger>();
            overtimeCalculationServiceMock = new Mock<IOvertimeCalculationService>();            

            controllerUnderTest = new OvertimeController(overtimeCalculationServiceMock.Object, loggerMock.Object);
        }

        [TestClass]
        public class QueryByPostOvertimeTests : OvertimeControllerTests
        {
            #region Test Context
            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }
            #endregion

            public OvertimeQueryCriteria inputCriteria;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                base.OvertimeControllerTestsInitialize();

                overtimeCalculationServiceMock.Setup(s => s.CalculateOvertime(It.IsAny<OvertimeQueryCriteria>()))
                    .Returns<OvertimeQueryCriteria>(oqc => Task.FromResult(new OvertimeCalculationResult()
                    {
                        PersonId = oqc.PersonId,
                        StartDate = oqc.StartDate,
                        EndDate = oqc.EndDate
                    }));

                inputCriteria = new OvertimeQueryCriteria()
                {
                    PersonId = "0003914",
                    StartDate = new DateTime(2016, 6, 4),
                    EndDate = new DateTime(2016, 6, 10)
                };
            }

            [TestMethod]
            public async Task ResultIsBasedOnCriteriaTest()
            {
                var result = await controllerUnderTest.QueryByPostOvertime(inputCriteria);
                Assert.AreEqual(inputCriteria.PersonId, result.PersonId);
                Assert.AreEqual(inputCriteria.StartDate, result.StartDate);
                Assert.AreEqual(inputCriteria.EndDate, result.EndDate);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CritieraRequiredTest()
            {
                await controllerUnderTest.QueryByPostOvertime(null);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchArgumentExceptionTest()
            {
                overtimeCalculationServiceMock.Setup(s => s.CalculateOvertime(It.IsAny<OvertimeQueryCriteria>())).Throws(new ArgumentException());
                
                try
                {
                    await controllerUnderTest.QueryByPostOvertime(inputCriteria);
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<ArgumentException>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchApplicationExceptionTest()
            {
                overtimeCalculationServiceMock.Setup(s => s.CalculateOvertime(It.IsAny<OvertimeQueryCriteria>())).Throws(new ApplicationException());

                try
                {
                    await controllerUnderTest.QueryByPostOvertime(inputCriteria);
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<ApplicationException>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchPermissionsExceptionTest()
            {
                overtimeCalculationServiceMock.Setup(s => s.CalculateOvertime(It.IsAny<OvertimeQueryCriteria>())).Throws(new PermissionsException());

                try
                {
                    await controllerUnderTest.QueryByPostOvertime(inputCriteria);
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<PermissionsException>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.Forbidden, hre.Response.StatusCode);
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task CatchGenericExceptionTest()
            {
                overtimeCalculationServiceMock.Setup(s => s.CalculateOvertime(It.IsAny<OvertimeQueryCriteria>())).Throws(new Exception());

                try
                {
                    await controllerUnderTest.QueryByPostOvertime(inputCriteria);
                }
                catch (HttpResponseException hre)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
                    Assert.AreEqual(HttpStatusCode.BadRequest, hre.Response.StatusCode);
                    throw;
                }
            }
        }
    }
}
