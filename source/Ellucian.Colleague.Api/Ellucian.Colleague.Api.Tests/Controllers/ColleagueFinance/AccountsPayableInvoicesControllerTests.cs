﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System.Threading.Tasks;
using System.Collections.Generic;
using Ellucian.Colleague.Configuration.Licensing;
using System.Net.Http;
using System.Web.Http;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Colleague.Api.Controllers.ColleagueFinance;
using Ellucian.Colleague.Dtos;
using Ellucian.Web.Http.Models;
using Ellucian.Colleague.Dtos.EnumProperties;
using Ellucian.Colleague.Domain.Base.Exceptions;
using System.Web.Http.Hosting;

namespace Ellucian.Colleague.Api.Tests.Controllers.ColleagueFinance
{
    [TestClass]
    public class AccountsPayableInvoicesControllerTests
    {
        public TestContext TestContext { get; set; }
        private Mock<IAccountsPayableInvoicesService> _mockAccountsPayableInvoicesService;
        private Mock<ILogger> _loggerMock;
        private AccountsPayableInvoicesController _accountsPayableInvoicesController;

        private AccountsPayableInvoices _accountsPayableInvoices;

        private string Guid = "02dc2629-e8a7-410e-b4df-572d02822f8b";
        private Paging page;
        private int limit;
        private int offset;
        private Tuple<IEnumerable<AccountsPayableInvoices>, int> accountsPayableInvoicesTuple;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            _loggerMock = new Mock<ILogger>();
            _mockAccountsPayableInvoicesService = new Mock<IAccountsPayableInvoicesService>();

            var lineItems = new List<Dtos.DtoProperties.AccountsPayableInvoicesLineItemDtoProperty>();

            _accountsPayableInvoices = new AccountsPayableInvoices()
            {
                Id = Guid,
                Vendor = new GuidObject2("0123VendorGuid"),
                VendorAddress = new GuidObject2("02344AddressGuid"),
                ReferenceNumber = "refNo012",
                VendorInvoiceNumber = "VIN021",
                TransactionDate = new DateTime(2017, 1, 12),
                VendorInvoiceDate = new DateTime(2017, 1, 12),
                VoidDate = new DateTime(2017, 1, 25),
                ProcessState = Dtos.EnumProperties.AccountsPayableInvoicesProcessState.NotSet,
                VendorBilledAmount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 40m, Currency = Dtos.EnumProperties.CurrencyIsoCode.USD },
                InvoiceDiscountAmount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 5m, Currency = Dtos.EnumProperties.CurrencyIsoCode.USD },
                Taxes = new List<Dtos.DtoProperties.AccountsPayableInvoicesTaxesDtoProperty>()
                {
                    new Dtos.DtoProperties.AccountsPayableInvoicesTaxesDtoProperty()
                    {
                        TaxCode = new GuidObject2("TaxCodeGuid"),
                        VendorAmount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 1m, Currency = Dtos.EnumProperties.CurrencyIsoCode.USD }
                    }
                },
                InvoiceType = Dtos.EnumProperties.AccountsPayableInvoicesInvoiceType.Invoice,
                Payment = new Dtos.DtoProperties.AccountsPayableInvoicesPaymentDtoProperty()
                {
                    Source = new GuidObject2("SourceGuid0123"),
                    PaymentDueOn = new DateTime(2017, 1, 17),
                    PaymentTerms = new GuidObject2("TermsGuid321")
                },
                InvoiceComment = "This is a Comment 321",
                GovernmentReporting = new List<Dtos.DtoProperties.GovernmentReportingDtoProperty>()
                {
                    new Dtos.DtoProperties.GovernmentReportingDtoProperty()
                    {
                        Code = CountryCodeType.USA,
                        TransactionType = Dtos.EnumProperties.AccountsPayableInvoicesTransactionType.NotSet
                    }
                },
                LineItems = new List<Dtos.DtoProperties.AccountsPayableInvoicesLineItemDtoProperty>()
                {
                    new Dtos.DtoProperties.AccountsPayableInvoicesLineItemDtoProperty()
                    {
                         AccountDetails =new List<Dtos.DtoProperties.AccountsPayableInvoicesAccountDetailDtoProperty>()
                         {
                             new Dtos.DtoProperties.AccountsPayableInvoicesAccountDetailDtoProperty()
                             {
                                  AccountingString = "10-10-1000-400",
                                   Allocation = new Dtos.DtoProperties.AccountsPayableInvoicesAllocationDtoProperty()
                                   {
                                        Allocated = new Dtos.DtoProperties.AccountsPayableInvoicesAllocatedDtoProperty()
                                        {
                                             Amount = new Dtos.DtoProperties.Amount2DtoProperty() {
                                                 Value = 10m,
                                                 Currency = Dtos.EnumProperties.CurrencyIsoCode.USD
                                             },
                                              Percentage = 100m,
                                              Quantity = 2m
                                        }
                                   },
                                    BudgetCheck = Dtos.EnumProperties.AccountsPayableInvoicesAccountBudgetCheck.NotRequired,
                                 SequenceNumber = 1,
                                  Source = new GuidObject2( "asbc-321"),
                                   SubmittedBy = new GuidObject2("submit-guid"),
                                   
                             }
                         },
                           Comment = "LineItem comment",
                         Description = "line item Description",
                         CommodityCode = new GuidObject2("Commodity-guid"),
                         Quantity = 2m,
                         UnitofMeasure = new GuidObject2("um-guid"),
                         UnitPrice = new Dtos.DtoProperties.Amount2DtoProperty() {
                            Value = 5m, Currency = Dtos.EnumProperties.CurrencyIsoCode.USD},
                         Taxes = new List<Dtos.DtoProperties.AccountsPayableInvoicesTaxesDtoProperty>()
                         {
                             new Dtos.DtoProperties.AccountsPayableInvoicesTaxesDtoProperty()
                             {
                                  TaxCode = new GuidObject2("taxCode-guid"),
                                  VendorAmount = new Dtos.DtoProperties.Amount2DtoProperty()
                                  {
                                      Value = 1m,
                                      Currency = Dtos.EnumProperties.CurrencyIsoCode.USD
                                  }
                             }
                         },
                         Discount = new Dtos.DtoProperties.AccountsPayableInvoicesDiscountDtoProperty()
                         {
                             Amount = new Dtos.DtoProperties.Amount2DtoProperty()
                             {
                                 Value = 1m,
                                 Currency = Dtos.EnumProperties.CurrencyIsoCode.USD
                             },
                              Percent = 1m
                         }
                        , PaymentStatus = Dtos.EnumProperties.AccountsPayableInvoicesPaymentStatus.Nohold
                        
                    }
                }
            };

            _mockAccountsPayableInvoicesService.Setup(s => s.GetDataPrivacyListByApi(It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(new List<string>());

            _accountsPayableInvoicesController = new AccountsPayableInvoicesController(_mockAccountsPayableInvoicesService.Object, _loggerMock.Object)
            {
                Request = new HttpRequestMessage()
            };
            _accountsPayableInvoicesController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            limit = 100;
            offset = 0;
            page = new Paging(limit, offset);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _loggerMock = null;
            _mockAccountsPayableInvoicesService = null;
            _accountsPayableInvoicesController = null;
        }

        [TestMethod]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesByGuidAsync()
        {
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesByGuidAsync(It.IsAny<string>())).ReturnsAsync(_accountsPayableInvoices);

            var actual = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesByGuidAsync(Guid);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesAsync()
        {
            var DtosAPI = new List<Dtos.AccountsPayableInvoices>();
            DtosAPI.Add(_accountsPayableInvoices);
            _accountsPayableInvoices.Id = "NewGuid2";
            DtosAPI.Add(_accountsPayableInvoices);
            _accountsPayableInvoices.Id = "NewGuid3";
            DtosAPI.Add(_accountsPayableInvoices);

            accountsPayableInvoicesTuple = new Tuple<IEnumerable<AccountsPayableInvoices>, int>(DtosAPI, 3);

            _accountsPayableInvoicesController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _accountsPayableInvoicesController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = true };
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesAsync(offset, limit, It.IsAny<bool>())).ReturnsAsync(accountsPayableInvoicesTuple);
            var actuals = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesAsync(page);
            var cancelToken = new System.Threading.CancellationToken(false);
            System.Net.Http.HttpResponseMessage httpResponseMessage = await actuals.ExecuteAsync(cancelToken);
            List<AccountsPayableInvoices> ActualsAPI = ((ObjectContent<IEnumerable<AccountsPayableInvoices>>)httpResponseMessage.Content).Value as List<AccountsPayableInvoices>;
            for (var i = 0; i < ActualsAPI.Count; i++)
            {
                var expected = DtosAPI.ToList()[i];
                var actual = ActualsAPI[i];
                Assert.AreEqual(expected.Id, actual.Id);
                Assert.AreEqual(expected.GovernmentReporting, actual.GovernmentReporting);
                Assert.AreEqual(expected.InvoiceComment, actual.InvoiceComment);
                Assert.AreEqual(expected.InvoiceDiscountAmount.Currency, actual.InvoiceDiscountAmount.Currency);
                Assert.AreEqual(expected.InvoiceDiscountAmount.Value, actual.InvoiceDiscountAmount.Value);
                Assert.AreEqual(expected.InvoiceType, actual.InvoiceType);
                Assert.AreEqual(expected.LineItems, actual.LineItems);
                Assert.AreEqual(expected.Payment.DirectDepositOverride, actual.Payment.DirectDepositOverride);
                Assert.AreEqual(expected.Payment.PaymentDueOn, actual.Payment.PaymentDueOn);
                Assert.AreEqual(expected.Payment.PaymentTerms, actual.Payment.PaymentTerms);
                Assert.AreEqual(expected.Payment.Source, actual.Payment.Source);
                Assert.AreEqual(expected.PaymentStatus, actual.PaymentStatus);
                Assert.AreEqual(expected.ProcessState, actual.ProcessState);
                Assert.AreEqual(expected.ReferenceNumber, actual.ReferenceNumber);
                Assert.AreEqual(expected.Taxes[0].TaxCode, actual.Taxes[0].TaxCode);
                Assert.AreEqual(expected.Taxes[0].VendorAmount.Currency, actual.Taxes[0].VendorAmount.Currency);
                Assert.AreEqual(expected.Taxes[0].VendorAmount.Value, actual.Taxes[0].VendorAmount.Value);
                Assert.AreEqual(expected.Vendor, actual.Vendor);
                Assert.AreEqual(expected.TransactionDate, actual.TransactionDate);
                Assert.AreEqual(expected.VendorAddress, actual.VendorAddress);
                Assert.AreEqual(expected.VendorBilledAmount.Currency, actual.VendorBilledAmount.Currency);
                Assert.AreEqual(expected.VendorBilledAmount.Value, actual.VendorBilledAmount.Value);
                Assert.AreEqual(expected.VendorInvoiceDate, actual.VendorInvoiceDate);
                Assert.AreEqual(expected.VendorInvoiceNumber, actual.VendorInvoiceNumber);
                Assert.AreEqual(expected.VoidDate, actual.VoidDate);
                Assert.AreEqual(expected.LineItems.Count(), actual.LineItems.Count());
                for (int x = 0; x < expected.LineItems.Count(); x++)
                {
                    var lineItem = actual.LineItems[x];
                    var expectedLi = expected.LineItems[x];

                    Assert.AreEqual(expectedLi.Description, lineItem.Description);
                    Assert.AreEqual(expectedLi.Comment, lineItem.Comment);
                    Assert.AreEqual(expectedLi.CommodityCode.Id, lineItem.CommodityCode.Id);
                    Assert.AreEqual(expectedLi.Quantity, lineItem.Quantity);
                    Assert.AreEqual(expectedLi.UnitofMeasure.Id, lineItem.UnitofMeasure.Id);
                    Assert.AreEqual(expectedLi.UnitPrice.Value, lineItem.UnitPrice.Value);
                    Assert.AreEqual(expectedLi.UnitPrice.Currency, lineItem.UnitPrice.Currency);

                    Assert.AreEqual(expectedLi.Taxes.Count(), lineItem.Taxes.Count());
                    Assert.AreEqual(expectedLi.Taxes[0].TaxCode.Id, lineItem.Taxes[0].TaxCode.Id);
                    Assert.AreEqual(expectedLi.Taxes[0].VendorAmount.Currency, lineItem.Taxes[0].VendorAmount.Currency);
                    Assert.AreEqual(expectedLi.Taxes[0].VendorAmount.Value, lineItem.Taxes[0].VendorAmount.Value);

                    Assert.AreEqual(expectedLi.Discount.Amount.Value, lineItem.Discount.Amount.Value);
                    Assert.AreEqual(expectedLi.Discount.Amount.Currency, lineItem.Discount.Amount.Currency);
                    Assert.AreEqual(expectedLi.Discount.Percent, lineItem.Discount.Percent);

                    Assert.AreEqual(expectedLi.PaymentStatus, lineItem.PaymentStatus);

                    Assert.AreEqual(expectedLi.AccountDetails.Count(), lineItem.AccountDetails.Count());
                    Assert.AreEqual(expectedLi.AccountDetails[0].SequenceNumber, lineItem.AccountDetails[0].SequenceNumber);
                    Assert.AreEqual(expectedLi.AccountDetails[0].AccountingString, lineItem.AccountDetails[0].AccountingString);
                    Assert.AreEqual(expectedLi.AccountDetails[0].Allocation.Allocated.Amount.Value, lineItem.AccountDetails[0].Allocation.Allocated.Amount.Value);
                    Assert.AreEqual(expectedLi.AccountDetails[0].Allocation.Allocated.Amount.Currency, lineItem.AccountDetails[0].Allocation.Allocated.Amount.Currency);
                    Assert.AreEqual(expectedLi.AccountDetails[0].Allocation.Allocated.Percentage, lineItem.AccountDetails[0].Allocation.Allocated.Percentage);
                    Assert.AreEqual(expectedLi.AccountDetails[0].Allocation.Allocated.Quantity, lineItem.AccountDetails[0].Allocation.Allocated.Quantity);
                }
            }
        }

        [TestMethod]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesAsync_cache()
        {
            var DtosAPI = new List<Dtos.AccountsPayableInvoices>();
            DtosAPI.Add(_accountsPayableInvoices);
            _accountsPayableInvoices.Id = "NewGuid2";
            DtosAPI.Add(_accountsPayableInvoices);
            _accountsPayableInvoices.Id = "NewGuid3";
            DtosAPI.Add(_accountsPayableInvoices);

            accountsPayableInvoicesTuple = new Tuple<IEnumerable<AccountsPayableInvoices>, int>(DtosAPI, 3);

            _accountsPayableInvoicesController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _accountsPayableInvoicesController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesAsync(offset, limit, It.IsAny<bool>())).ReturnsAsync(accountsPayableInvoicesTuple);
            var actuals = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesAsync(page);
            var cancelToken = new System.Threading.CancellationToken(false);
            System.Net.Http.HttpResponseMessage httpResponseMessage = await actuals.ExecuteAsync(cancelToken);
            List<AccountsPayableInvoices> ActualsAPI = ((ObjectContent<IEnumerable<AccountsPayableInvoices>>)httpResponseMessage.Content).Value as List<AccountsPayableInvoices>;
            for (var i = 0; i < ActualsAPI.Count; i++)
            {
                var expected = DtosAPI.ToList()[i];
                var actual = ActualsAPI[i];
                Assert.AreEqual(expected.Id, actual.Id);
                Assert.AreEqual(expected.GovernmentReporting, actual.GovernmentReporting);
                Assert.AreEqual(expected.InvoiceComment, actual.InvoiceComment);
                Assert.AreEqual(expected.InvoiceDiscountAmount.Currency, actual.InvoiceDiscountAmount.Currency);
                Assert.AreEqual(expected.InvoiceDiscountAmount.Value, actual.InvoiceDiscountAmount.Value);
                Assert.AreEqual(expected.InvoiceType, actual.InvoiceType);
                Assert.AreEqual(expected.LineItems, actual.LineItems);
                Assert.AreEqual(expected.Payment.DirectDepositOverride, actual.Payment.DirectDepositOverride);
                Assert.AreEqual(expected.Payment.PaymentDueOn, actual.Payment.PaymentDueOn);
                Assert.AreEqual(expected.Payment.PaymentTerms, actual.Payment.PaymentTerms);
                Assert.AreEqual(expected.Payment.Source, actual.Payment.Source);
                Assert.AreEqual(expected.PaymentStatus, actual.PaymentStatus);
                Assert.AreEqual(expected.ProcessState, actual.ProcessState);
                Assert.AreEqual(expected.ReferenceNumber, actual.ReferenceNumber);
                Assert.AreEqual(expected.Taxes[0].TaxCode, actual.Taxes[0].TaxCode);
                Assert.AreEqual(expected.Taxes[0].VendorAmount.Currency, actual.Taxes[0].VendorAmount.Currency);
                Assert.AreEqual(expected.Taxes[0].VendorAmount.Value, actual.Taxes[0].VendorAmount.Value);
                Assert.AreEqual(expected.Vendor, actual.Vendor);
                Assert.AreEqual(expected.TransactionDate, actual.TransactionDate);
                Assert.AreEqual(expected.VendorAddress, actual.VendorAddress);
                Assert.AreEqual(expected.VendorBilledAmount.Currency, actual.VendorBilledAmount.Currency);
                Assert.AreEqual(expected.VendorBilledAmount.Value, actual.VendorBilledAmount.Value);
                Assert.AreEqual(expected.VendorInvoiceDate, actual.VendorInvoiceDate);
                Assert.AreEqual(expected.VendorInvoiceNumber, actual.VendorInvoiceNumber);
                Assert.AreEqual(expected.VoidDate, actual.VoidDate);

                Assert.AreEqual(expected.LineItems.Count(), actual.LineItems.Count());
                for(int x = 0; x < expected.LineItems.Count(); x++)
                {
                    var lineItem = actual.LineItems[x];
                    var expectedLi = expected.LineItems[x];

                    Assert.AreEqual(expectedLi.Description, lineItem.Description);
                    Assert.AreEqual(expectedLi.Comment, lineItem.Comment);
                    Assert.AreEqual(expectedLi.CommodityCode.Id, lineItem.CommodityCode.Id);
                    Assert.AreEqual(expectedLi.Quantity, lineItem.Quantity);
                    Assert.AreEqual(expectedLi.UnitofMeasure.Id, lineItem.UnitofMeasure.Id);
                    Assert.AreEqual(expectedLi.UnitPrice.Value, lineItem.UnitPrice.Value);
                    Assert.AreEqual(expectedLi.UnitPrice.Currency, lineItem.UnitPrice.Currency);

                    Assert.AreEqual(expectedLi.Taxes.Count(), lineItem.Taxes.Count());
                    Assert.AreEqual(expectedLi.Taxes[0].TaxCode.Id, lineItem.Taxes[0].TaxCode.Id);
                    Assert.AreEqual(expectedLi.Taxes[0].VendorAmount.Currency, lineItem.Taxes[0].VendorAmount.Currency);
                    Assert.AreEqual(expectedLi.Taxes[0].VendorAmount.Value, lineItem.Taxes[0].VendorAmount.Value);

                    Assert.AreEqual(expectedLi.Discount.Amount.Value, lineItem.Discount.Amount.Value);
                    Assert.AreEqual(expectedLi.Discount.Amount.Currency, lineItem.Discount.Amount.Currency);
                    Assert.AreEqual(expectedLi.Discount.Percent, lineItem.Discount.Percent);

                    Assert.AreEqual(expectedLi.PaymentStatus, lineItem.PaymentStatus);

                    Assert.AreEqual(expectedLi.AccountDetails.Count(), lineItem.AccountDetails.Count());
                    Assert.AreEqual(expectedLi.AccountDetails[0].SequenceNumber, lineItem.AccountDetails[0].SequenceNumber);
                    Assert.AreEqual(expectedLi.AccountDetails[0].AccountingString, lineItem.AccountDetails[0].AccountingString);
                    Assert.AreEqual(expectedLi.AccountDetails[0].Allocation.Allocated.Amount.Value, lineItem.AccountDetails[0].Allocation.Allocated.Amount.Value);
                    Assert.AreEqual(expectedLi.AccountDetails[0].Allocation.Allocated.Amount.Currency, lineItem.AccountDetails[0].Allocation.Allocated.Amount.Currency);
                    Assert.AreEqual(expectedLi.AccountDetails[0].Allocation.Allocated.Percentage, lineItem.AccountDetails[0].Allocation.Allocated.Percentage);
                    Assert.AreEqual(expectedLi.AccountDetails[0].Allocation.Allocated.Quantity, lineItem.AccountDetails[0].Allocation.Allocated.Quantity);
                }
            }
        }

        [TestMethod]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesAsync_NoCache()
        {
            var DtosAPI = new List<Dtos.AccountsPayableInvoices>();
            DtosAPI.Add(_accountsPayableInvoices);
            _accountsPayableInvoices.Id = "NewGuid2";
            DtosAPI.Add(_accountsPayableInvoices);
            _accountsPayableInvoices.Id = "NewGuid3";
            DtosAPI.Add(_accountsPayableInvoices);

            accountsPayableInvoicesTuple = new Tuple<IEnumerable<AccountsPayableInvoices>, int>(DtosAPI, 3);

            _accountsPayableInvoicesController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _accountsPayableInvoicesController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = true };
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesAsync(offset, limit, It.IsAny<bool>())).ReturnsAsync(accountsPayableInvoicesTuple);
            var actuals = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesAsync(page);
            var cancelToken = new System.Threading.CancellationToken(false);
            System.Net.Http.HttpResponseMessage httpResponseMessage = await actuals.ExecuteAsync(cancelToken);
            List<AccountsPayableInvoices> ActualsAPI = ((ObjectContent<IEnumerable<AccountsPayableInvoices>>)httpResponseMessage.Content).Value as List<AccountsPayableInvoices>;
            for (var i = 0; i < ActualsAPI.Count; i++)
            {
                var expected = DtosAPI.ToList()[i];
                var actual = ActualsAPI[i];
                Assert.AreEqual(expected.Id, actual.Id);
                Assert.AreEqual(expected.GovernmentReporting, actual.GovernmentReporting);
                Assert.AreEqual(expected.InvoiceComment, actual.InvoiceComment);
                Assert.AreEqual(expected.InvoiceDiscountAmount.Currency, actual.InvoiceDiscountAmount.Currency);
                Assert.AreEqual(expected.InvoiceDiscountAmount.Value, actual.InvoiceDiscountAmount.Value);
                Assert.AreEqual(expected.InvoiceType, actual.InvoiceType);
                Assert.AreEqual(expected.LineItems, actual.LineItems);
                Assert.AreEqual(expected.Payment.DirectDepositOverride, actual.Payment.DirectDepositOverride);
                Assert.AreEqual(expected.Payment.PaymentDueOn, actual.Payment.PaymentDueOn);
                Assert.AreEqual(expected.Payment.PaymentTerms, actual.Payment.PaymentTerms);
                Assert.AreEqual(expected.Payment.Source, actual.Payment.Source);
                Assert.AreEqual(expected.PaymentStatus, actual.PaymentStatus);
                Assert.AreEqual(expected.ProcessState, actual.ProcessState);
                Assert.AreEqual(expected.ReferenceNumber, actual.ReferenceNumber);
                Assert.AreEqual(expected.Taxes[0].TaxCode, actual.Taxes[0].TaxCode);
                Assert.AreEqual(expected.Taxes[0].VendorAmount.Currency, actual.Taxes[0].VendorAmount.Currency);
                Assert.AreEqual(expected.Taxes[0].VendorAmount.Value, actual.Taxes[0].VendorAmount.Value);
                Assert.AreEqual(expected.Vendor, actual.Vendor);
                Assert.AreEqual(expected.TransactionDate, actual.TransactionDate);
                Assert.AreEqual(expected.VendorAddress, actual.VendorAddress);
                Assert.AreEqual(expected.VendorBilledAmount.Currency, actual.VendorBilledAmount.Currency);
                Assert.AreEqual(expected.VendorBilledAmount.Value, actual.VendorBilledAmount.Value);
                Assert.AreEqual(expected.VendorInvoiceDate, actual.VendorInvoiceDate);
                Assert.AreEqual(expected.VendorInvoiceNumber, actual.VendorInvoiceNumber);
                Assert.AreEqual(expected.VoidDate, actual.VoidDate);

                Assert.AreEqual(expected.LineItems.Count(), actual.LineItems.Count());
                for (int x = 0; x < expected.LineItems.Count(); x++)
                {
                    var lineItem = actual.LineItems[x];
                    var expectedLi = expected.LineItems[x];

                    Assert.AreEqual(expectedLi.Description, lineItem.Description);
                    Assert.AreEqual(expectedLi.Comment, lineItem.Comment);
                    Assert.AreEqual(expectedLi.CommodityCode.Id, lineItem.CommodityCode.Id);
                    Assert.AreEqual(expectedLi.Quantity, lineItem.Quantity);
                    Assert.AreEqual(expectedLi.UnitofMeasure.Id, lineItem.UnitofMeasure.Id);
                    Assert.AreEqual(expectedLi.UnitPrice.Value, lineItem.UnitPrice.Value);
                    Assert.AreEqual(expectedLi.UnitPrice.Currency, lineItem.UnitPrice.Currency);

                    Assert.AreEqual(expectedLi.Taxes.Count(), lineItem.Taxes.Count());
                    Assert.AreEqual(expectedLi.Taxes[0].TaxCode.Id, lineItem.Taxes[0].TaxCode.Id);
                    Assert.AreEqual(expectedLi.Taxes[0].VendorAmount.Currency, lineItem.Taxes[0].VendorAmount.Currency);
                    Assert.AreEqual(expectedLi.Taxes[0].VendorAmount.Value, lineItem.Taxes[0].VendorAmount.Value);

                    Assert.AreEqual(expectedLi.Discount.Amount.Value, lineItem.Discount.Amount.Value);
                    Assert.AreEqual(expectedLi.Discount.Amount.Currency, lineItem.Discount.Amount.Currency);
                    Assert.AreEqual(expectedLi.Discount.Percent, lineItem.Discount.Percent);

                    Assert.AreEqual(expectedLi.PaymentStatus, lineItem.PaymentStatus);

                    Assert.AreEqual(expectedLi.AccountDetails.Count(), lineItem.AccountDetails.Count());
                    Assert.AreEqual(expectedLi.AccountDetails[0].SequenceNumber, lineItem.AccountDetails[0].SequenceNumber);
                    Assert.AreEqual(expectedLi.AccountDetails[0].AccountingString, lineItem.AccountDetails[0].AccountingString);
                    Assert.AreEqual(expectedLi.AccountDetails[0].Allocation.Allocated.Amount.Value, lineItem.AccountDetails[0].Allocation.Allocated.Amount.Value);
                    Assert.AreEqual(expectedLi.AccountDetails[0].Allocation.Allocated.Amount.Currency, lineItem.AccountDetails[0].Allocation.Allocated.Amount.Currency);
                    Assert.AreEqual(expectedLi.AccountDetails[0].Allocation.Allocated.Percentage, lineItem.AccountDetails[0].Allocation.Allocated.Percentage);
                    Assert.AreEqual(expectedLi.AccountDetails[0].Allocation.Allocated.Quantity, lineItem.AccountDetails[0].Allocation.Allocated.Quantity);
                }
            }
        }

        [TestMethod]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesAsync_Paging()
        {
            var DtosAPI = new List<Dtos.AccountsPayableInvoices>();
            DtosAPI.Add(_accountsPayableInvoices);

            page = new Paging(1, 1);

            accountsPayableInvoicesTuple = new Tuple<IEnumerable<AccountsPayableInvoices>, int>(DtosAPI, 1);

            _accountsPayableInvoicesController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _accountsPayableInvoicesController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesAsync(1, 1, It.IsAny<bool>())).ReturnsAsync(accountsPayableInvoicesTuple);
            var actuals = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesAsync(page);
            var cancelToken = new System.Threading.CancellationToken(false);
            System.Net.Http.HttpResponseMessage httpResponseMessage = await actuals.ExecuteAsync(cancelToken);
            List<AccountsPayableInvoices> ActualsAPI = ((ObjectContent<IEnumerable<AccountsPayableInvoices>>)httpResponseMessage.Content).Value as List<AccountsPayableInvoices>;
            for (var i = 0; i < ActualsAPI.Count; i++)
            {
                var expected = DtosAPI.ToList()[i];
                var actual = ActualsAPI[i];
                Assert.AreEqual(expected.Id, actual.Id);
                Assert.AreEqual(expected.GovernmentReporting, actual.GovernmentReporting);
                Assert.AreEqual(expected.InvoiceComment, actual.InvoiceComment);
                Assert.AreEqual(expected.InvoiceDiscountAmount.Currency, actual.InvoiceDiscountAmount.Currency);
                Assert.AreEqual(expected.InvoiceDiscountAmount.Value, actual.InvoiceDiscountAmount.Value);
                Assert.AreEqual(expected.InvoiceType, actual.InvoiceType);
                Assert.AreEqual(expected.LineItems, actual.LineItems);
                Assert.AreEqual(expected.Payment.DirectDepositOverride, actual.Payment.DirectDepositOverride);
                Assert.AreEqual(expected.Payment.PaymentDueOn, actual.Payment.PaymentDueOn);
                Assert.AreEqual(expected.Payment.PaymentTerms, actual.Payment.PaymentTerms);
                Assert.AreEqual(expected.Payment.Source, actual.Payment.Source);
                Assert.AreEqual(expected.PaymentStatus, actual.PaymentStatus);
                Assert.AreEqual(expected.ProcessState, actual.ProcessState);
                Assert.AreEqual(expected.ReferenceNumber, actual.ReferenceNumber);
                Assert.AreEqual(expected.Taxes[0].TaxCode, actual.Taxes[0].TaxCode);
                Assert.AreEqual(expected.Taxes[0].VendorAmount.Currency, actual.Taxes[0].VendorAmount.Currency);
                Assert.AreEqual(expected.Taxes[0].VendorAmount.Value, actual.Taxes[0].VendorAmount.Value);
                Assert.AreEqual(expected.Vendor, actual.Vendor);
                Assert.AreEqual(expected.TransactionDate, actual.TransactionDate);
                Assert.AreEqual(expected.VendorAddress, actual.VendorAddress);
                Assert.AreEqual(expected.VendorBilledAmount.Currency, actual.VendorBilledAmount.Currency);
                Assert.AreEqual(expected.VendorBilledAmount.Value, actual.VendorBilledAmount.Value);
                Assert.AreEqual(expected.VendorInvoiceDate, actual.VendorInvoiceDate);
                Assert.AreEqual(expected.VendorInvoiceNumber, actual.VendorInvoiceNumber);
                Assert.AreEqual(expected.VoidDate, actual.VoidDate);

                Assert.AreEqual(expected.LineItems.Count(), actual.LineItems.Count());
                for (int x = 0; x < expected.LineItems.Count(); x++)
                {
                    var lineItem = actual.LineItems[x];
                    var expectedLi = expected.LineItems[x];

                    Assert.AreEqual(expectedLi.Description, lineItem.Description);
                    Assert.AreEqual(expectedLi.Comment, lineItem.Comment);
                    Assert.AreEqual(expectedLi.CommodityCode.Id, lineItem.CommodityCode.Id);
                    Assert.AreEqual(expectedLi.Quantity, lineItem.Quantity);
                    Assert.AreEqual(expectedLi.UnitofMeasure.Id, lineItem.UnitofMeasure.Id);
                    Assert.AreEqual(expectedLi.UnitPrice.Value, lineItem.UnitPrice.Value);
                    Assert.AreEqual(expectedLi.UnitPrice.Currency, lineItem.UnitPrice.Currency);

                    Assert.AreEqual(expectedLi.Taxes.Count(), lineItem.Taxes.Count());
                    Assert.AreEqual(expectedLi.Taxes[0].TaxCode.Id, lineItem.Taxes[0].TaxCode.Id);
                    Assert.AreEqual(expectedLi.Taxes[0].VendorAmount.Currency, lineItem.Taxes[0].VendorAmount.Currency);
                    Assert.AreEqual(expectedLi.Taxes[0].VendorAmount.Value, lineItem.Taxes[0].VendorAmount.Value);

                    Assert.AreEqual(expectedLi.Discount.Amount.Value, lineItem.Discount.Amount.Value);
                    Assert.AreEqual(expectedLi.Discount.Amount.Currency, lineItem.Discount.Amount.Currency);
                    Assert.AreEqual(expectedLi.Discount.Percent, lineItem.Discount.Percent);

                    Assert.AreEqual(expectedLi.PaymentStatus, lineItem.PaymentStatus);

                    Assert.AreEqual(expectedLi.AccountDetails.Count(), lineItem.AccountDetails.Count());
                    Assert.AreEqual(expectedLi.AccountDetails[0].SequenceNumber, lineItem.AccountDetails[0].SequenceNumber);
                    Assert.AreEqual(expectedLi.AccountDetails[0].AccountingString, lineItem.AccountDetails[0].AccountingString);
                    Assert.AreEqual(expectedLi.AccountDetails[0].Allocation.Allocated.Amount.Value, lineItem.AccountDetails[0].Allocation.Allocated.Amount.Value);
                    Assert.AreEqual(expectedLi.AccountDetails[0].Allocation.Allocated.Amount.Currency, lineItem.AccountDetails[0].Allocation.Allocated.Amount.Currency);
                    Assert.AreEqual(expectedLi.AccountDetails[0].Allocation.Allocated.Percentage, lineItem.AccountDetails[0].Allocation.Allocated.Percentage);
                    Assert.AreEqual(expectedLi.AccountDetails[0].Allocation.Allocated.Quantity, lineItem.AccountDetails[0].Allocation.Allocated.Quantity);
                }
            }
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesByGuidAsync_KeyNotFoundExecpt()
        {
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesByGuidAsync(It.IsAny<string>())).ThrowsAsync(new KeyNotFoundException());

            var actual = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesByGuidAsync(Guid);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesByGuidAsync_PermissionsException()
        {
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesByGuidAsync(It.IsAny<string>())).ThrowsAsync(new PermissionsException());

            var actual = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesByGuidAsync(Guid);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesByGuidAsync_ArgumentException()
        {
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesByGuidAsync(It.IsAny<string>())).ThrowsAsync(new ArgumentException());

            var actual = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesByGuidAsync(Guid);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesByGuidAsync_RepositoryException()
        {
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesByGuidAsync(It.IsAny<string>())).ThrowsAsync(new RepositoryException());

            var actual = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesByGuidAsync(Guid);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesByGuidAsync_IntegrationApiException()
        {
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesByGuidAsync(It.IsAny<string>())).ThrowsAsync(new IntegrationApiException());

            var actual = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesByGuidAsync(Guid);
            Assert.IsNotNull(actual);
        }
        
        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesByGuidAsync_Exception()
        {
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesByGuidAsync(It.IsAny<string>())).ThrowsAsync(new Exception());

            var actual = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesByGuidAsync(Guid);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesByGuidAsync_nullGuid()
        {
           // _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesByGuidAsync(It.IsAny<string>())).ThrowsAsync(new Exception());

            var actual = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesByGuidAsync(null);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesAsync_KeyNotFoundExecpt()
        {
            _accountsPayableInvoicesController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _accountsPayableInvoicesController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesAsync(offset, limit, It.IsAny<bool>()))
                .ThrowsAsync(new KeyNotFoundException());
            var actuals = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesAsync(page);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesAsync_PermissionsException()
        {
            _accountsPayableInvoicesController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _accountsPayableInvoicesController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesAsync(offset, limit, It.IsAny<bool>())).ThrowsAsync(new PermissionsException());
            var actuals = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesAsync(page);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesAsync_ArgumentException()
        {
            _accountsPayableInvoicesController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _accountsPayableInvoicesController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesAsync(offset, limit, It.IsAny<bool>())).ThrowsAsync(new ArgumentException());
            var actuals = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesAsync(page);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesAsync_RepositoryException()
        {
            _accountsPayableInvoicesController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _accountsPayableInvoicesController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesAsync(offset, limit, It.IsAny<bool>())).ThrowsAsync(new RepositoryException());
            var actuals = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesAsync(page);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesAsync_IntegrationApiException()
        {
            _accountsPayableInvoicesController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _accountsPayableInvoicesController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesAsync(offset, limit, It.IsAny<bool>())).ThrowsAsync(new IntegrationApiException());
            var actuals = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesAsync(page);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesControllerTests_GetAccountsPayableInvoicesAsync_Exception()
        {
            _accountsPayableInvoicesController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _accountsPayableInvoicesController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };
            _mockAccountsPayableInvoicesService.Setup(x => x.GetAccountsPayableInvoicesAsync(offset, limit, It.IsAny<bool>())).ThrowsAsync(new Exception());
            var actuals = await _accountsPayableInvoicesController.GetAccountsPayableInvoicesAsync(page);
        }


    }

    [TestClass]
    public class AccountsPayableInvoicesControllerTests_PUT_POST
    {
        public TestContext TestContext { get; set; }
        private Mock<IAccountsPayableInvoicesService> _mockAccountsPayableInvoicesService;
        private Mock<ILogger> _loggerMock;
        private AccountsPayableInvoicesController _accountsPayableInvoicesController;

        private AccountsPayableInvoices _accountsPayableInvoices;

        private string Guid = "02dc2629-e8a7-410e-b4df-572d02822f8b";

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            _loggerMock = new Mock<ILogger>();
            _mockAccountsPayableInvoicesService = new Mock<IAccountsPayableInvoicesService>();

            var lineItems = new List<Dtos.DtoProperties.AccountsPayableInvoicesLineItemDtoProperty>();

            BuildData();

            _accountsPayableInvoicesController = new AccountsPayableInvoicesController(_mockAccountsPayableInvoicesService.Object, _loggerMock.Object);
        }

        [TestMethod]
        public async Task AccountsPayableInvoicesController_Put()
        {
            _mockAccountsPayableInvoicesService.Setup(i => i.PutAccountsPayableInvoicesAsync(It.IsAny<string>(), It.IsAny<Dtos.AccountsPayableInvoices>())).ReturnsAsync(_accountsPayableInvoices);
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task AccountsPayableInvoicesController_Put_Dto_Id_Null()
        {
            _accountsPayableInvoices.Id = string.Empty;
            _mockAccountsPayableInvoicesService.Setup(i => i.PutAccountsPayableInvoicesAsync(It.IsAny<string>(), It.IsAny<Dtos.AccountsPayableInvoices>())).ReturnsAsync(_accountsPayableInvoices);
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task AccountsPayableInvoicesController_Post()
        {
            _accountsPayableInvoices.VoidDate = null;
            _mockAccountsPayableInvoicesService.Setup(i => i.PostAccountsPayableInvoicesAsync(It.IsAny<Dtos.AccountsPayableInvoices>())).ReturnsAsync(_accountsPayableInvoices);
            var result = await _accountsPayableInvoicesController.PostAccountsPayableInvoicesAsync(_accountsPayableInvoices);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Put_Id_Null()
        {
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(string.Empty, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Put_Dto_Null()
        {
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync( "1234", null);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Put_EmptyGuid()
        {
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(System.Guid.Empty.ToString(), _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Put_Guid_Not_Equal()
        {
            _accountsPayableInvoices.Id = System.Guid.NewGuid().ToString();
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync("1234", _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Put_PermissionsException()
        {
            _mockAccountsPayableInvoicesService.Setup(i => i.PutAccountsPayableInvoicesAsync(It.IsAny<string>(), It.IsAny<Dtos.AccountsPayableInvoices>()))
                .ThrowsAsync(new PermissionsException());
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Put_KeyNotFoundException()
        {
            _mockAccountsPayableInvoicesService.Setup(i => i.PutAccountsPayableInvoicesAsync(It.IsAny<string>(), It.IsAny<Dtos.AccountsPayableInvoices>()))
                .ThrowsAsync(new KeyNotFoundException());
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Put_ArgumentException()
        {
            _mockAccountsPayableInvoicesService.Setup(i => i.PutAccountsPayableInvoicesAsync(It.IsAny<string>(), It.IsAny<Dtos.AccountsPayableInvoices>()))
                .ThrowsAsync(new ArgumentException());
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Put_RepositoryException()
        {
            _mockAccountsPayableInvoicesService.Setup(i => i.PutAccountsPayableInvoicesAsync(It.IsAny<string>(), It.IsAny<Dtos.AccountsPayableInvoices>()))
                .ThrowsAsync(new RepositoryException());
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Put_IntegrationApiException()
        {
            _mockAccountsPayableInvoicesService.Setup(i => i.PutAccountsPayableInvoicesAsync(It.IsAny<string>(), It.IsAny<Dtos.AccountsPayableInvoices>()))
                .ThrowsAsync(new IntegrationApiException());
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Put_ConfigurationException()
        {
            _mockAccountsPayableInvoicesService.Setup(i => i.PutAccountsPayableInvoicesAsync(It.IsAny<string>(), It.IsAny<Dtos.AccountsPayableInvoices>()))
                .ThrowsAsync(new ConfigurationException());
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Put_Exception()
        {
            _mockAccountsPayableInvoicesService.Setup(i => i.PutAccountsPayableInvoicesAsync(It.IsAny<string>(), It.IsAny<Dtos.AccountsPayableInvoices>()))
                .ThrowsAsync(new Exception());
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Post_Dto_Null()
        {
            var result = await _accountsPayableInvoicesController.PostAccountsPayableInvoicesAsync(null);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Post_PermissionsException()
        {
            _mockAccountsPayableInvoicesService.Setup(i => i.PostAccountsPayableInvoicesAsync(It.IsAny<Dtos.AccountsPayableInvoices>()))
                .ThrowsAsync(new PermissionsException());
            var result = await _accountsPayableInvoicesController.PostAccountsPayableInvoicesAsync(_accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Post_KeyNotFoundException()
        {
            _mockAccountsPayableInvoicesService.Setup(i => i.PostAccountsPayableInvoicesAsync(It.IsAny<Dtos.AccountsPayableInvoices>()))
                .ThrowsAsync(new KeyNotFoundException());
            var result = await _accountsPayableInvoicesController.PostAccountsPayableInvoicesAsync(_accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Post_ArgumentException()
        {
            _mockAccountsPayableInvoicesService.Setup(i => i.PostAccountsPayableInvoicesAsync(It.IsAny<Dtos.AccountsPayableInvoices>()))
                .ThrowsAsync(new ArgumentException());
            var result = await _accountsPayableInvoicesController.PostAccountsPayableInvoicesAsync(_accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Post_RepositoryException()
        {
            _mockAccountsPayableInvoicesService.Setup(i => i.PostAccountsPayableInvoicesAsync(It.IsAny<Dtos.AccountsPayableInvoices>()))
                .ThrowsAsync(new RepositoryException());
            var result = await _accountsPayableInvoicesController.PostAccountsPayableInvoicesAsync(_accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Post_IntegrationApiException()
        {
            _mockAccountsPayableInvoicesService.Setup(i => i.PostAccountsPayableInvoicesAsync(It.IsAny<Dtos.AccountsPayableInvoices>()))
                .ThrowsAsync(new IntegrationApiException());
            var result = await _accountsPayableInvoicesController.PostAccountsPayableInvoicesAsync(_accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Post_ConfigurationException()
        {
            _mockAccountsPayableInvoicesService.Setup(i => i.PostAccountsPayableInvoicesAsync(It.IsAny<Dtos.AccountsPayableInvoices>()))
                .ThrowsAsync(new ConfigurationException());
            var result = await _accountsPayableInvoicesController.PostAccountsPayableInvoicesAsync(_accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Post_Exception()
        {
            _mockAccountsPayableInvoicesService.Setup(i => i.PostAccountsPayableInvoicesAsync(It.IsAny<Dtos.AccountsPayableInvoices>()))
                .ThrowsAsync(new Exception());
            var result = await _accountsPayableInvoicesController.PostAccountsPayableInvoicesAsync(_accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_VendorNull()
        {
            _accountsPayableInvoices.Vendor = null;
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_VendorIdNull()
        {
            _accountsPayableInvoices.Vendor = new GuidObject2("");
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }


        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_ProcessState_NotSet()
        {
            _accountsPayableInvoices.ProcessState = AccountsPayableInvoicesProcessState.NotSet;
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_PaymentStatus_NotSet()
        {
            _accountsPayableInvoices.PaymentStatus = AccountsPayableInvoicesPaymentStatus.NotSet;
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_Payment_Null()
        {
            _accountsPayableInvoices.Payment = null;
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_Payment_Source_Null()
        {
            _accountsPayableInvoices.Payment.Source = null;
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_PaymentDueOn_Null()
        {
            _accountsPayableInvoices.Payment.PaymentDueOn = null;
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_SourceId_Null()
        {
            _accountsPayableInvoices.Payment.Source = new GuidObject2("");
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_PaymentTermsId_Null()
        {
            _accountsPayableInvoices.Payment.PaymentTerms = new GuidObject2("");
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_Vendor_Address_Id_Null()
        {
            _accountsPayableInvoices.VendorAddress.Id = "";
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_Vendor_Billed_Amount_Null()
        {
            _accountsPayableInvoices.VendorBilledAmount = new Dtos.DtoProperties.Amount2DtoProperty() { };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_Vendor_Billed_Amount_Currency_Null()
        {
            _accountsPayableInvoices.VendorBilledAmount = new Dtos.DtoProperties.Amount2DtoProperty() {  Value = 10 };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_Invoice_Discount_Value_Null()
        {
            _accountsPayableInvoices.InvoiceDiscountAmount = new Dtos.DtoProperties.Amount2DtoProperty() {  };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_Invoice_Discount_Currency_Null()
        {
            _accountsPayableInvoices.InvoiceDiscountAmount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 10 };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_Tax_Code_Id_Null()
        {
            _accountsPayableInvoices.Taxes.First().TaxCode.Id = "";
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_Tax_VendorAmount_Value_Null()
        {
            _accountsPayableInvoices.Taxes.First().VendorAmount = new Dtos.DtoProperties.Amount2DtoProperty() { };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_Tax_VendorAmount_Currency_Null()
        {
            _accountsPayableInvoices.Taxes.First().VendorAmount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 10 };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItems_CommodityCodeId_Null()
        {
            _accountsPayableInvoices.LineItems.First().CommodityCode.Id = "";
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItems_UnitofMeasureId_Null()
        {
            _accountsPayableInvoices.LineItems.First().UnitofMeasure.Id = "";
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItems_UnitPrice_Value_Null()
        {
            _accountsPayableInvoices.LineItems.First().UnitPrice = new Dtos.DtoProperties.Amount2DtoProperty() { };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItems_UnitPrice_Currency_Null()
        {
            _accountsPayableInvoices.LineItems.First().UnitPrice = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 10 };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItems_VendorBilledUnitPrice_Value_Null()
        {
            _accountsPayableInvoices.LineItems.First().VendorBilledUnitPrice = new Dtos.DtoProperties.Amount2DtoProperty() { };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItems_VendorBilledUnitPrice_Currency_Null()
        {
            _accountsPayableInvoices.LineItems.First().VendorBilledUnitPrice = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 10 };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItems_AdditionalAmount_Value_Null()
        {
            _accountsPayableInvoices.LineItems.First().AdditionalAmount = new Dtos.DtoProperties.Amount2DtoProperty() { };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItems_AdditionalAmount_Currency_Null()
        {
            _accountsPayableInvoices.LineItems.First().AdditionalAmount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 10 };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_TaxCode_Null()
        {
            _accountsPayableInvoices.LineItems.First().Taxes.First().TaxCode = null;
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_TaxCode_Id_Null()
        {
            _accountsPayableInvoices.LineItems.First().Taxes.First().TaxCode = new GuidObject2("");
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_TaxCode_VendorAmount_Value_Null()
        {
            _accountsPayableInvoices.LineItems.First().Taxes.First().VendorAmount = new Dtos.DtoProperties.Amount2DtoProperty() { };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_TaxCode_VendorAmount_Currency_Null()
        {
            _accountsPayableInvoices.LineItems.First().Taxes.First().VendorAmount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 10 };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItem_Discount_Value_Null()
        {
            _accountsPayableInvoices.LineItems.First().Discount.Amount = new Dtos.DtoProperties.Amount2DtoProperty() {  };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItem_Discount_Currency_Null()
        {
            _accountsPayableInvoices.LineItems.First().Discount.Amount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 10 };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItem_AllocatedAmount_Value_Null()
        {
            _accountsPayableInvoices.LineItems.First().AccountDetails.First().Allocation = new Dtos.DtoProperties.AccountsPayableInvoicesAllocationDtoProperty()
            {
                Allocated = new Dtos.DtoProperties.AccountsPayableInvoicesAllocatedDtoProperty()
                {
                    Amount = new Dtos.DtoProperties.Amount2DtoProperty() 
                    {
                       
                    }
                }
            };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItem_AllocatedAmount_Currency_Null()
        {
            _accountsPayableInvoices.LineItems.First().AccountDetails.First().Allocation = new Dtos.DtoProperties.AccountsPayableInvoicesAllocationDtoProperty()
            {
                Allocated = new Dtos.DtoProperties.AccountsPayableInvoicesAllocatedDtoProperty()
                {
                    Amount = new Dtos.DtoProperties.Amount2DtoProperty()
                    {
                        Value = 10
                    }
                }
            };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItem_Allocation_TaxAmount_Null()
        {
            _accountsPayableInvoices.LineItems.First().AccountDetails.First().Allocation.TaxAmount = new Dtos.DtoProperties.Amount2DtoProperty() { };            
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItem_Allocation_TaxAmount_Currency_Null()
        {
            _accountsPayableInvoices.LineItems.First().AccountDetails.First().Allocation.TaxAmount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 10 };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItem_Allocation_AdditionalAmount_Null()
        {
            _accountsPayableInvoices.LineItems.First().AccountDetails.First().Allocation.AdditionalAmount = new Dtos.DtoProperties.Amount2DtoProperty() { };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItem_Allocation_AdditionalAmount_Currency_Null()
        {
            _accountsPayableInvoices.LineItems.First().AccountDetails.First().Allocation.AdditionalAmount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 10 };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItem_Allocation_DiscountAmount_Null()
        {
            _accountsPayableInvoices.LineItems.First().AccountDetails.First().Allocation.DiscountAmount = new Dtos.DtoProperties.Amount2DtoProperty() { };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItem_Allocation_DiscountAmount_Currency_Null()
        {
            _accountsPayableInvoices.LineItems.First().AccountDetails.First().Allocation.DiscountAmount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 10 };
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_AccountDetail_SourceId_Null()
        {
            _accountsPayableInvoices.LineItems.First().AccountDetails.First().Source = new GuidObject2("");
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_AccountDetail_SubmittedById_Null()
        {
            _accountsPayableInvoices.LineItems.First().AccountDetails.First().SubmittedBy = new GuidObject2("");
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_AccountDetail_AccountingString_Null()
        {
            _accountsPayableInvoices.LineItems.First().AccountDetails.First().AccountingString = string.Empty;
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_AccountDetail_Allocation_Null()
        {
            _accountsPayableInvoices.LineItems.First().AccountDetails.First().Allocation = null;
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_AccountDetail_BudgetCheck_NotSet()
        {
            _accountsPayableInvoices.LineItems.First().AccountDetails.First().BudgetCheck = AccountsPayableInvoicesAccountBudgetCheck.NotSet;
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItem_Description_NotSet()
        {
            _accountsPayableInvoices.LineItems.First().Description = string.Empty;
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItem_Quantity_NotSet()
        {
            _accountsPayableInvoices.LineItems.First().Quantity = 0;
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItem_UnitPrice_NotSet()
        {
            _accountsPayableInvoices.LineItems.First().UnitPrice = null;
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_AccountDetail_PaymentStatus_NotSet()
        {
            _accountsPayableInvoices.LineItems.First().PaymentStatus = null;
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task ValidateAccountsPayableInvoices_LineItem_Status_NotSet()
        {
            _accountsPayableInvoices.LineItems.First().Status = null;
            var result = await _accountsPayableInvoicesController.PutAccountsPayableInvoicesAsync(Guid, _accountsPayableInvoices);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountsPayableInvoicesController_Delete()
        {
            await _accountsPayableInvoicesController.DeleteAccountsPayableInvoicesAsync(Guid);
        } 

        private void BuildData()
        {
            _accountsPayableInvoices = new AccountsPayableInvoices()
            {
                Id = Guid,
                Vendor = new GuidObject2("0123VendorGuid"),
                VendorAddress = new GuidObject2("02344AddressGuid"),
                ReferenceNumber = "refNo012",
                VendorInvoiceNumber = "VIN021",
                TransactionDate = DateTime.Now.AddDays(1),
                VendorInvoiceDate = DateTime.Now.AddDays(1),
                VoidDate = DateTime.Now.AddDays(1),
                PaymentStatus = Dtos.EnumProperties.AccountsPayableInvoicesPaymentStatus.Nohold,
                ProcessState = Dtos.EnumProperties.AccountsPayableInvoicesProcessState.Approved,
                VendorBilledAmount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 40m, Currency = Dtos.EnumProperties.CurrencyIsoCode.USD },
                InvoiceDiscountAmount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 5m, Currency = Dtos.EnumProperties.CurrencyIsoCode.USD },
                Taxes = new List<Dtos.DtoProperties.AccountsPayableInvoicesTaxesDtoProperty>()
                {
                    new Dtos.DtoProperties.AccountsPayableInvoicesTaxesDtoProperty()
                    {
                        TaxCode = new GuidObject2("TaxCodeGuid"),
                        VendorAmount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 1m, Currency = Dtos.EnumProperties.CurrencyIsoCode.USD }
                    }
                },
                InvoiceType = Dtos.EnumProperties.AccountsPayableInvoicesInvoiceType.Invoice,
                Payment = new Dtos.DtoProperties.AccountsPayableInvoicesPaymentDtoProperty()
                {
                    Source = new GuidObject2("SourceGuid0123"),
                    PaymentDueOn = DateTime.Now.AddDays(1),
                    PaymentTerms = new GuidObject2("TermsGuid321")
                },
                InvoiceComment = "This is a Comment 321",
                GovernmentReporting = new List<Dtos.DtoProperties.GovernmentReportingDtoProperty>()
                {
                    new Dtos.DtoProperties.GovernmentReportingDtoProperty()
                    {
                        Code = CountryCodeType.USA,
                        TransactionType = Dtos.EnumProperties.AccountsPayableInvoicesTransactionType.Deposit
                    }
                },
                LineItems = new List<Dtos.DtoProperties.AccountsPayableInvoicesLineItemDtoProperty>()
                {
                    new Dtos.DtoProperties.AccountsPayableInvoicesLineItemDtoProperty()
                    {
                         AccountDetails =new List<Dtos.DtoProperties.AccountsPayableInvoicesAccountDetailDtoProperty>()
                         {
                             new Dtos.DtoProperties.AccountsPayableInvoicesAccountDetailDtoProperty()
                             {
                                  AccountingString = "10-10-1000-400",
                                   Allocation = new Dtos.DtoProperties.AccountsPayableInvoicesAllocationDtoProperty()
                                   {
                                        Allocated = new Dtos.DtoProperties.AccountsPayableInvoicesAllocatedDtoProperty()
                                        {
                                             Amount = new Dtos.DtoProperties.Amount2DtoProperty() {
                                                 Value = 10m,
                                                 Currency = Dtos.EnumProperties.CurrencyIsoCode.USD
                                             },
                                              Percentage = 100m,
                                              Quantity = 2m
                                        }
                                   },
                                    BudgetCheck = Dtos.EnumProperties.AccountsPayableInvoicesAccountBudgetCheck.NotRequired,
                                 SequenceNumber = 1,
                                  Source = new GuidObject2( "asbc-321"),
                                   SubmittedBy = new GuidObject2("submit-guid"),
                                   
                             }
                         },
                         Comment = "LineItem comment",
                         Description = "line item Description",
                         CommodityCode = new GuidObject2("Commodity-guid"),
                         Quantity = 2m,
                         UnitofMeasure = new GuidObject2("um-guid"),
                         UnitPrice = new Dtos.DtoProperties.Amount2DtoProperty() 
                         {
                            Value = 5m, 
                            Currency = Dtos.EnumProperties.CurrencyIsoCode.USD
                         },
                         Taxes = new List<Dtos.DtoProperties.AccountsPayableInvoicesTaxesDtoProperty>()
                         {
                             new Dtos.DtoProperties.AccountsPayableInvoicesTaxesDtoProperty()
                             {
                                  TaxCode = new GuidObject2("taxCode-guid"),
                                  VendorAmount = new Dtos.DtoProperties.Amount2DtoProperty()
                                  {
                                      Value = 1m,
                                      Currency = Dtos.EnumProperties.CurrencyIsoCode.USD
                                  }
                             }
                         },
                         Discount = new Dtos.DtoProperties.AccountsPayableInvoicesDiscountDtoProperty()
                         {
                             Amount = new Dtos.DtoProperties.Amount2DtoProperty()
                             {
                                 Value = 1m,
                                 Currency = Dtos.EnumProperties.CurrencyIsoCode.USD
                             },
                             Percent = 1m
                         },
                         PaymentStatus = Dtos.EnumProperties.AccountsPayableInvoicesPaymentStatus.Nohold,
                         Status = AccountsPayableInvoicesStatus.Open
                    }
                }
            };
        }
    }
}