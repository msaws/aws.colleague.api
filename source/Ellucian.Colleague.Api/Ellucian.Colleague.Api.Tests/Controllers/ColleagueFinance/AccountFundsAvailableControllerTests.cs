﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System.Threading.Tasks;
using System.Collections.Generic;
using Ellucian.Colleague.Configuration.Licensing;
using System.Net.Http;
using System.Web.Http.Hosting;
using System.Web.Http;
using Ellucian.Colleague.Api.Controllers;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Colleague.Api.Controllers.ColleagueFinance;
using Ellucian.Colleague.Dtos;
using System.Web;
using System.IO;
using Newtonsoft.Json;
using Ellucian.Colleague.Dtos.EnumProperties;

namespace Ellucian.Colleague.Api.Tests.Controllers.ColleagueFinance
{
    [TestClass]
    public class AccountFundsAvailableControllerTests
    {
        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }
        private Mock<IAccountFundsAvailableService> _accountFundsAvailableServiceMock;
        private Mock<ILogger> _loggerMock;
        private AccountFundsAvailableController _accountFundsAvailableController;

        private AccountFundsAvailable _accountFundsAvailable;
        private Dtos.AccountFundsAvailable_Transactions acctFundsAvailabaleTransDto;
        private HttpResponse _response;

        private string accountingString = "11-00-01-00-00000-10110";
        private DateTime? balanceOn = new DateTime(2016, 12, 31);
        private decimal amount = 2000m;
        private string submittedByGuid = "02dc2629-e8a7-410e-b4df-572d02822f8b";
        private string criteria = "{ 'accountingString':'11_00_01_00_20603_52010', 'amount': '2000', 'balanceOn': '12/31/2016', 'submittedBy': '02dc2629-e8a7-410e-b4df-572d02822f8b' }";

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            _loggerMock = new Mock<ILogger>();
            _accountFundsAvailableServiceMock = new Mock<IAccountFundsAvailableService>();

            _response = new HttpResponse(new StringWriter());
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://doesntMatter.com", ""), _response);

            BuildData();

            _accountFundsAvailableController = new AccountFundsAvailableController(_accountFundsAvailableServiceMock.Object, _loggerMock.Object);
        }

        private void BuildData()
        {
            _accountFundsAvailable = new AccountFundsAvailable()
            {
                AccountingStringValue = accountingString,
                BalanceOn = balanceOn,
                FundsAvailable = Dtos.EnumProperties.FundsAvailable.Available
            };
            acctFundsAvailabaleTransDto = new AccountFundsAvailable_Transactions()
            {
                Transactions = new List<AccountFundsAvailable_Transactionstransactions>() 
                {
                    new AccountFundsAvailable_Transactionstransactions()
                    {
                        TransactionDate = DateTime.Today.AddDays(-10),
                        TransactionDetailLines = new List<AccountFundsAvailable_TransactionstransactionDetailLines>()
                        {
                           new AccountFundsAvailable_TransactionstransactionDetailLines()
                           {
                               AccountingString = "11_00_01_00_00000_10110",
                               Amount = new Dtos.DtoProperties.AmountDtoProperty()
                               {
                                   Currency = CurrencyCodes.USD,
                                   Value = 100
                               },
                               FundsAvailable = Dtos.EnumProperties.FundsAvailable.Available,
                               SubmittedBy = new GuidObject2("2c0ee889-2db9-4625-a1ca-7ad307152d60"),
                               Type = CreditOrDebit.Credit
                           }
                        }
                    },
                     new AccountFundsAvailable_Transactionstransactions()
                    {
                        TransactionDate = DateTime.Today.AddDays(-10),
                        TransactionDetailLines = new List<AccountFundsAvailable_TransactionstransactionDetailLines>()
                        {
                           new AccountFundsAvailable_TransactionstransactionDetailLines()
                           {
                               AccountingString = "11_00_01_00_00000_10220",
                               Amount = new Dtos.DtoProperties.AmountDtoProperty()
                               {
                                   Currency = CurrencyCodes.CAD,
                                   Value = 50
                               },
                               FundsAvailable = Dtos.EnumProperties.FundsAvailable.Available,
                               SubmittedBy = new GuidObject2("a386bf6a-25c2-4d5c-932e-bb4513b25cd9"),
                               Type = CreditOrDebit.Debit
                           }
                        }
                    }
                }
            };
        }

        [TestCleanup]
        public void Cleanup()
        {
            _loggerMock = null;
            _accountFundsAvailableServiceMock = null;
            _accountFundsAvailableController = null;
        }

        [TestMethod]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailableControllerByFilterCriteriaAsync()
        {
            _accountFundsAvailableServiceMock.Setup(x => x.GetAccountFundsAvailableByFilterCriteriaAsync(It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<DateTime?>(), It.IsAny<string>())).ReturnsAsync(_accountFundsAvailable);
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailableByFilterAsync(criteria);
            Assert.IsNotNull(actual);

            Assert.AreEqual(_accountFundsAvailable.AccountingStringValue, actual.AccountingStringValue);
            Assert.AreEqual(_accountFundsAvailable.BalanceOn, actual.BalanceOn);
            Assert.AreEqual(_accountFundsAvailable.FundsAvailable, actual.FundsAvailable);
        }

        [TestMethod]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailableControllerByFilterCriteriaAsync_EmptyBalanceAndSubmittedBy()
        {
            criteria = "{ 'accountingString':'11_00_01_00_20603_52010', 'amount': '2000', 'balanceOn': '', 'submittedBy': '' }";
            _accountFundsAvailableServiceMock.Setup(x => x.GetAccountFundsAvailableByFilterCriteriaAsync(It.IsAny<string>(), It.IsAny<decimal>(), null, "")).ReturnsAsync(_accountFundsAvailable);
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailableByFilterAsync(criteria);
            Assert.IsNotNull(actual);
        }
        /*
        [TestMethod]
        public async Task AccountFundsAvailableController_QueryAccountFundsAvailable_TransactionsAsync()
        {
            _accountFundsAvailableServiceMock.Setup(i => i.CheckAccountFundsAvailable_TransactionsAsync(It.IsAny<AccountFundsAvailable_Transactions>())).ReturnsAsync(acctFundsAvailabaleTransDto);
            var actual = await _accountFundsAvailableController.QueryAccountFundsAvailable_TransactionsAsync(It.IsAny<AccountFundsAvailable_Transactions>());
            Assert.IsNotNull(actual);
            Assert.AreEqual(acctFundsAvailabaleTransDto.Transactions.Count(), actual.Transactions.Count());            
        }
        */

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_QueryAccountFundsAvailable_TransactionsAsync_IntegrationApiException()
        {
            _accountFundsAvailableServiceMock.Setup(i => i.CheckAccountFundsAvailable_Transactions2Async(It.IsAny<AccountFundsAvailable_Transactions>()))
                .ThrowsAsync(new IntegrationApiException());
            var actual = await _accountFundsAvailableController.QueryAccountFundsAvailable_TransactionsAsync(It.IsAny<AccountFundsAvailable_Transactions>());
        }
        
        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_QueryAccountFundsAvailable_TransactionsAsync_ArgumentException()
        {
            _accountFundsAvailableServiceMock.Setup(i => i.CheckAccountFundsAvailable_Transactions2Async(It.IsAny<AccountFundsAvailable_Transactions>()))
                .ThrowsAsync(new ArgumentException());
            var actual = await _accountFundsAvailableController.QueryAccountFundsAvailable_TransactionsAsync(It.IsAny<AccountFundsAvailable_Transactions>());
        }
        
        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_QueryAccountFundsAvailable_TransactionsAsync_Exception()
        {
            _accountFundsAvailableServiceMock.Setup(i => i.CheckAccountFundsAvailable_Transactions2Async(It.IsAny<AccountFundsAvailable_Transactions>()))
                .ThrowsAsync(new Exception());
            var actual = await _accountFundsAvailableController.QueryAccountFundsAvailable_TransactionsAsync(It.IsAny<AccountFundsAvailable_Transactions>());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailableControllerByFilterCriteriaAsync_EmptyCriteria()
        {
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailableByFilterAsync(string.Empty);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailableControllerByFilterCriteriaAsync_EmptyAccountingString()
        {
            criteria = "{ 'accountingString':'', 'amount': '2000', 'balanceOn': '12/31/2016', 'submittedBy': '02dc2629-e8a7-410e-b4df-572d02822f8b' }";
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailableByFilterAsync(criteria);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailableControllerByFilterCriteriaAsync_EmptyAmount()
        {
            criteria = "{ 'accountingString':'11_00_01_00_20603_52010', 'amount': '', 'balanceOn': '12/31/2016', 'submittedBy': '02dc2629-e8a7-410e-b4df-572d02822f8b' }";
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailableByFilterAsync(criteria);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailableControllerByFilterCriteriaAsync_BadBalanceOn()
        {
            criteria = "{ 'accountingString':'11_00_01_00_20603_52010', 'amount': '2000', 'balanceOn': '02/31/2016', 'submittedBy': '02dc2629-e8a7-410e-b4df-572d02822f8b' }";
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailableByFilterAsync(criteria);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailableControllerByFilterCriteriaAsync_ZeroAmount()
        {
            criteria = "{ 'accountingString':'11_00_01_00_20603_52010', 'amount': '0', 'balanceOn': '12/31/2016', 'submittedBy': '02dc2629-e8a7-410e-b4df-572d02822f8b' }";
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailableByFilterAsync(criteria);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailableControllerByFilterCriteriaAsync_BadAmountString()
        {
            criteria = "{ 'accountingString':'11_00_01_00_20603_52010', 'amount': '5abc', 'balanceOn': '12/31/2016', 'submittedBy': '02dc2629-e8a7-410e-b4df-572d02822f8b' }";
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailableByFilterAsync(criteria);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailableControllerByFilterCriteriaAsync_EmptyGuid()
        {
            criteria = "{ 'accountingString':'11_00_01_00_20603_52010', 'amount': '2000', 'balanceOn': '12/31/2016', 'submittedBy': '00000000-0000-0000-0000-000000000000' }";
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailableByFilterAsync(criteria);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailableControllerByFilterCriteriaAsync_BadGuid()
        {
            criteria = "{ 'accountingString':'11_00_01_00_20603_52010', 'amount': '2000', 'balanceOn': '12/31/2016', 'submittedBy': '02dc2629-e8a7-410e-b4df-572d02822f8' }";
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailableByFilterAsync(criteria);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailableControllerByFilterCriteriaAsync_KeyNotFoundException()
        {
            _accountFundsAvailableServiceMock.Setup(x => x.GetAccountFundsAvailableByFilterCriteriaAsync(It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<DateTime?>(), It.IsAny<string>()))
                .ThrowsAsync(new KeyNotFoundException());
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailableByFilterAsync(criteria);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailableControllerByFilterCriteriaAsync_JsonSerializationException()
        {
            criteria = " 'accountingString':'11_00_01_00_20603_52010', 'amount': '2000', 'balanceOn': '12/31/2016', 'submittedBy': '02dc2629-e8a7-410e-b4df-572d02822f8b' }";
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailableByFilterAsync(criteria);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailableControllerByFilterCriteriaAsync_RepositoryException()
        {
            _accountFundsAvailableServiceMock.Setup(x => x.GetAccountFundsAvailableByFilterCriteriaAsync(It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<DateTime?>(), It.IsAny<string>()))
                .ThrowsAsync(new RepositoryException());
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailableByFilterAsync(criteria);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailableControllerByFilterCriteriaAsync_PermissionsException()
        {
            _accountFundsAvailableServiceMock.Setup(x => x.GetAccountFundsAvailableByFilterCriteriaAsync(It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<DateTime?>(), It.IsAny<string>()))
                .ThrowsAsync(new PermissionsException());
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailableByFilterAsync(criteria);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailableAsync_NotImplemented()
        {
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailableAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_PostAccountFundsAvailableAsync_NotImplemented()
        {
            var actual = await _accountFundsAvailableController.PostAccountFundsAvailableAsync(It.IsAny<AccountFundsAvailable>());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_PutAccountFundsAvailableAsync_NotImplemented()
        {
            var actual = await _accountFundsAvailableController.PutAccountFundsAvailableAsync(It.IsAny<string>(), It.IsAny<AccountFundsAvailable>());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_DeleteAccountFundsAvailableAsync_NotImplemented()
        {
            await _accountFundsAvailableController.DeleteAccountFundsAvailableAsync(It.IsAny<string>());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailable_TransactionsAsync_NotSupported()
        {
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailable_TransactionsAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_GetAccountFundsAvailable_TransactionsByGuidAsync_NotSupported()
        {
            var actual = await _accountFundsAvailableController.GetAccountFundsAvailable_TransactionsByGuidAsync(It.IsAny<string>());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_PostAccountFundsAvailable_TransactionsAsync_NotSupported()
        {
            var actual = await _accountFundsAvailableController.PostAccountFundsAvailable_TransactionsAsync(It.IsAny<AccountFundsAvailable_Transactions>());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_PutAccountFundsAvailable_TransactionsAsync_NotSupported()
        {
            var actual = await _accountFundsAvailableController.PutAccountFundsAvailable_TransactionsAsync(It.IsAny<string>(), It.IsAny<AccountFundsAvailable_Transactions>());
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task AccountFundsAvailableController_DeleteAccountFundsAvailable_TransactionsAsync_NotSupported()
        {
            await _accountFundsAvailableController.DeleteAccountFundsAvailable_TransactionsAsync(It.IsAny<string>());
        }
    }
}