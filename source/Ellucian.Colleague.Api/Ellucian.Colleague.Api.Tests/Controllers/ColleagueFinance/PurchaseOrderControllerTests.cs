﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System.Threading.Tasks;
using System.Collections.Generic;
using Ellucian.Colleague.Configuration.Licensing;
using System.Net.Http;
using System.Web.Http;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Colleague.Api.Controllers.ColleagueFinance;
using Ellucian.Colleague.Dtos;
using Ellucian.Web.Http.Models;
using Ellucian.Colleague.Dtos.EnumProperties;
using Ellucian.Colleague.Domain.Base.Exceptions;
using System.Web.Http.Hosting;

namespace Ellucian.Colleague.Api.Tests.Controllers.ColleagueFinance
{
    [TestClass]
    public class PurchaseOrdersControllerTests
    {
        public TestContext TestContext { get; set; }
        private Mock<IPurchaseOrderService> _mockPurchaseOrdersService;
        private Mock<ILogger> _loggerMock;
        private PurchaseOrdersController _purchaseOrdersController;

        private Dtos.PurchaseOrders _purchaseOrder;
        private List<Dtos.PurchaseOrders> _purchaseOrders;
        private string Guid = "02dc2629-e8a7-410e-b4df-572d02822f8b";
        private Paging page;
        private int limit;
        private int offset;
        private Tuple<IEnumerable<PurchaseOrders>, int> purchaseOrdersTuple;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            _loggerMock = new Mock<ILogger>();
            _mockPurchaseOrdersService = new Mock<IPurchaseOrderService>();

            _purchaseOrders = new List<PurchaseOrders>();

            _purchaseOrder = new PurchaseOrders()
            {
                Id = Guid,
                Buyer = new GuidObject2("bdc82e04-52ea-49b4-8317-dcefaabf702c"),
                Classification = new GuidObject2("b61b3a19-f164-47ad-afbc-dc5947340cdc"),
                Comments = new List<PurchaseOrdersCommentsDtoProperty>()
                {
                    new PurchaseOrdersCommentsDtoProperty()
                    {
                        Comment = "Hello World",
                        Type = CommentTypes.Printed
                    }
                },
                DeliveredBy = new DateTime(2017, 1, 1),
                Initiator = new Dtos.DtoProperties.PurchaseOrdersInitiatorDtoProperty()
                {
                    Name = "John Doe",
                    Detail = new GuidObject2("1b64f194-ed6b-4131-82fe-0ede17271509")
                },
                OrderedOn = new DateTime(2017, 1, 1),
                OrderNumber = "P0000001",
                PaymentSource = new GuidObject2("6d9bf0e5-a261-47c6-a245-297473d9acf0"),
                PaymentTerms = new GuidObject2("b61b3a19-f164-47ad-afbc-dc5947340cdc"),
                ReferenceNumbers = new List<string>() { "123" },
                Status = PurchaseOrdersStatus.Closed,
                TransactionDate = new DateTime(2017, 1, 1),
                Vendor = new PurchaseOrdersVendorDtoProperty()
                {
                    ExistingVendor = new GuidObject2("b61b3a19-f164-47ad-afbc-dc5947340cdc")
                },
            };
            _purchaseOrders.Add(_purchaseOrder);

            var purchaseOrder2 = new PurchaseOrders()
            { Id = "NewGuid2" };
            _purchaseOrders.Add(purchaseOrder2);
            var purchaseOrder3 = new PurchaseOrders()
            { Id = "NewGuid3" };
            _purchaseOrders.Add(purchaseOrder3);


            _mockPurchaseOrdersService.Setup(s => s.GetDataPrivacyListByApi(It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(new List<string>());

            _purchaseOrdersController = new PurchaseOrdersController(_mockPurchaseOrdersService.Object, _loggerMock.Object)
            {
                Request = new HttpRequestMessage()
            };
            _purchaseOrdersController.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());

            limit = 100;
            offset = 0;
            page = new Paging(limit, offset);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _loggerMock = null;
            _mockPurchaseOrdersService = null;
            _purchaseOrdersController = null;
        }

        [TestMethod]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersByGuidAsync()
        {
            var expected = _purchaseOrders.FirstOrDefault(po => po.Id == Guid);
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ReturnsAsync(expected);

            var actual = await _purchaseOrdersController.GetPurchaseOrdersByGuidAsync(Guid);
            Assert.IsNotNull(actual);

            Assert.AreEqual(expected.Id, actual.Id);
            Assert.AreEqual(expected.Buyer.Id, "bdc82e04-52ea-49b4-8317-dcefaabf702c");
            Assert.AreEqual(expected.Classification.Id, "b61b3a19-f164-47ad-afbc-dc5947340cdc");
            Assert.AreEqual(1, actual.Comments.Count);
            Assert.AreEqual(expected.Comments[0].Comment, "Hello World");
            Assert.AreEqual(expected.Comments[0].Type, CommentTypes.Printed);
        }

        [TestMethod]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersAsync()
        {

            purchaseOrdersTuple = new Tuple<IEnumerable<PurchaseOrders>, int>(_purchaseOrders, 3);

            _purchaseOrdersController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _purchaseOrdersController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = true };
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersAsync(offset, limit, It.IsAny<bool>())).ReturnsAsync(purchaseOrdersTuple);
            var actuals = await _purchaseOrdersController.GetPurchaseOrdersAsync(page);
            var cancelToken = new System.Threading.CancellationToken(false);
            System.Net.Http.HttpResponseMessage httpResponseMessage = await actuals.ExecuteAsync(cancelToken);
            List<PurchaseOrders> ActualsAPI = ((ObjectContent<IEnumerable<PurchaseOrders>>)httpResponseMessage.Content).Value as List<PurchaseOrders>;
            for (var i = 0; i < ActualsAPI.Count; i++)
            {
                var expected = _purchaseOrders.ToList()[i];
                var actual = ActualsAPI[i];
                Assert.AreEqual(expected.Id, actual.Id);

            }
        }

        [TestMethod]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersAsync_cache()
        {

            purchaseOrdersTuple = new Tuple<IEnumerable<PurchaseOrders>, int>(_purchaseOrders, 3);

            _purchaseOrdersController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _purchaseOrdersController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersAsync(offset, limit, It.IsAny<bool>())).ReturnsAsync(purchaseOrdersTuple);
            var actuals = await _purchaseOrdersController.GetPurchaseOrdersAsync(page);
            var cancelToken = new System.Threading.CancellationToken(false);
            System.Net.Http.HttpResponseMessage httpResponseMessage = await actuals.ExecuteAsync(cancelToken);
            List<PurchaseOrders> ActualsAPI = ((ObjectContent<IEnumerable<PurchaseOrders>>)httpResponseMessage.Content).Value as List<PurchaseOrders>;
            for (var i = 0; i < ActualsAPI.Count; i++)
            {
                var expected = _purchaseOrders.ToList()[i];
                var actual = ActualsAPI[i];
                Assert.AreEqual(expected.Id, actual.Id);

            }
        }

        [TestMethod]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersAsync_NoCache()
        {
            purchaseOrdersTuple = new Tuple<IEnumerable<PurchaseOrders>, int>(_purchaseOrders, 3);

            _purchaseOrdersController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _purchaseOrdersController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = true };
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersAsync(offset, limit, It.IsAny<bool>())).ReturnsAsync(purchaseOrdersTuple);
            var actuals = await _purchaseOrdersController.GetPurchaseOrdersAsync(page);
            var cancelToken = new System.Threading.CancellationToken(false);
            System.Net.Http.HttpResponseMessage httpResponseMessage = await actuals.ExecuteAsync(cancelToken);
            List<PurchaseOrders> ActualsAPI = ((ObjectContent<IEnumerable<PurchaseOrders>>)httpResponseMessage.Content).Value as List<PurchaseOrders>;
            for (var i = 0; i < ActualsAPI.Count; i++)
            {
                var expected = _purchaseOrders.ToList()[i];
                var actual = ActualsAPI[i];
                Assert.AreEqual(expected.Id, actual.Id);

            }
        }

        [TestMethod]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersAsync_Paging()
        {
            var DtosAPI = new List<Dtos.PurchaseOrders>();
            DtosAPI.Add(_purchaseOrder);

            page = new Paging(1, 1);

            purchaseOrdersTuple = new Tuple<IEnumerable<PurchaseOrders>, int>(DtosAPI, 1);

            _purchaseOrdersController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _purchaseOrdersController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersAsync(1, 1, It.IsAny<bool>())).ReturnsAsync(purchaseOrdersTuple);
            var actuals = await _purchaseOrdersController.GetPurchaseOrdersAsync(page);
            var cancelToken = new System.Threading.CancellationToken(false);
            System.Net.Http.HttpResponseMessage httpResponseMessage = await actuals.ExecuteAsync(cancelToken);
            List<PurchaseOrders> ActualsAPI = ((ObjectContent<IEnumerable<PurchaseOrders>>)httpResponseMessage.Content).Value as List<PurchaseOrders>;
            for (var i = 0; i < ActualsAPI.Count; i++)
            {
                var expected = DtosAPI.ToList()[i];
                var actual = ActualsAPI[i];
                Assert.AreEqual(expected.Id, actual.Id);

            }
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersByGuidAsync_KeyNotFoundExecpt()
        {
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ThrowsAsync(new KeyNotFoundException());

            var actual = await _purchaseOrdersController.GetPurchaseOrdersByGuidAsync(Guid);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersByGuidAsync_PermissionsException()
        {
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ThrowsAsync(new PermissionsException());

            var actual = await _purchaseOrdersController.GetPurchaseOrdersByGuidAsync(Guid);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersByGuidAsync_ArgumentException()
        {
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ThrowsAsync(new ArgumentException());

            var actual = await _purchaseOrdersController.GetPurchaseOrdersByGuidAsync(Guid);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersByGuidAsync_RepositoryException()
        {
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ThrowsAsync(new RepositoryException());

            var actual = await _purchaseOrdersController.GetPurchaseOrdersByGuidAsync(Guid);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersByGuidAsync_IntegrationApiException()
        {
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ThrowsAsync(new IntegrationApiException());

            var actual = await _purchaseOrdersController.GetPurchaseOrdersByGuidAsync(Guid);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersByGuidAsync_Exception()
        {
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ThrowsAsync(new Exception());

            var actual = await _purchaseOrdersController.GetPurchaseOrdersByGuidAsync(Guid);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersByGuidAsync_nullGuid()
        {
            // _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ThrowsAsync(new Exception());

            var actual = await _purchaseOrdersController.GetPurchaseOrdersByGuidAsync(null);
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersAsync_KeyNotFoundExecpt()
        {
            _purchaseOrdersController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _purchaseOrdersController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersAsync(offset, limit, It.IsAny<bool>()))
                .ThrowsAsync(new KeyNotFoundException());
            var actuals = await _purchaseOrdersController.GetPurchaseOrdersAsync(page);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersAsync_PermissionsException()
        {
            _purchaseOrdersController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _purchaseOrdersController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersAsync(offset, limit, It.IsAny<bool>())).ThrowsAsync(new PermissionsException());
            var actuals = await _purchaseOrdersController.GetPurchaseOrdersAsync(page);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersAsync_ArgumentException()
        {
            _purchaseOrdersController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _purchaseOrdersController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersAsync(offset, limit, It.IsAny<bool>())).ThrowsAsync(new ArgumentException());
            var actuals = await _purchaseOrdersController.GetPurchaseOrdersAsync(page);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersAsync_RepositoryException()
        {
            _purchaseOrdersController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _purchaseOrdersController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersAsync(offset, limit, It.IsAny<bool>())).ThrowsAsync(new RepositoryException());
            var actuals = await _purchaseOrdersController.GetPurchaseOrdersAsync(page);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersAsync_IntegrationApiException()
        {
            _purchaseOrdersController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _purchaseOrdersController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersAsync(offset, limit, It.IsAny<bool>())).ThrowsAsync(new IntegrationApiException());
            var actuals = await _purchaseOrdersController.GetPurchaseOrdersAsync(page);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task PurchaseOrdersControllerTests_GetPurchaseOrdersAsync_Exception()
        {
            _purchaseOrdersController.Request = new System.Net.Http.HttpRequestMessage() { RequestUri = new Uri("http://localhost") };
            _purchaseOrdersController.Request.Headers.CacheControl =
             new System.Net.Http.Headers.CacheControlHeaderValue { NoCache = false };
            _mockPurchaseOrdersService.Setup(x => x.GetPurchaseOrdersAsync(offset, limit, It.IsAny<bool>())).ThrowsAsync(new Exception());
            var actuals = await _purchaseOrdersController.GetPurchaseOrdersAsync(page);
        }


    }

    [TestClass]
    public class PurchaseOrdersControllerTests_V10
    {
        [TestClass]
        public class PurchaseOrdersControllerTests_POST
        {
            #region DECLARATIONS

            public TestContext TestContext { get; set; }

            private Mock<IPurchaseOrderService> purchaseOrderServiceMock;
            private Mock<ILogger> loggerMock;
            private PurchaseOrdersController purchaseOrdersController;

            private PurchaseOrders purchaseOrders;
            private PurchaseOrdersVendorDtoProperty vendor;
            private OverrideShippingDestinationDtoProperty shippingDestination;
            private AddressPlace place;
            private Dtos.DtoProperties.PurchaseOrdersInitiatorDtoProperty initiator;
            private List<PurchaseOrdersLineItemsDtoProperty> lineItems;
            private Dtos.DtoProperties.Amount2DtoProperty amount;
            private List<Dtos.DtoProperties.PurchaseOrdersAccountDetailDtoProperty> accountDetails;

            #endregion

            #region TEST SETUP

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                loggerMock = new Mock<ILogger>();
                purchaseOrderServiceMock = new Mock<IPurchaseOrderService>();

                InitializeTestData();

                purchaseOrderServiceMock.Setup(s => s.GetDataPrivacyListByApi(It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(new List<string>());

                purchaseOrdersController = new PurchaseOrdersController(purchaseOrderServiceMock.Object, loggerMock.Object)
                {
                    Request = new HttpRequestMessage()
                };
            }

            [TestCleanup]
            public void Cleanup()
            {
                loggerMock = null;
                purchaseOrderServiceMock = null;
                purchaseOrdersController = null;
            }

            private void InitializeTestData()
            {
                amount = new Dtos.DtoProperties.Amount2DtoProperty() { Currency = CurrencyIsoCode.USD, Value = 100 };

                accountDetails = new List<Dtos.DtoProperties.PurchaseOrdersAccountDetailDtoProperty>()
                {
                    new Dtos.DtoProperties.PurchaseOrdersAccountDetailDtoProperty()
                    {
                        AccountingString = "1",
                        Allocation = new Dtos.DtoProperties.PurchaseOrdersAllocationDtoProperty()
                        {
                            Allocated = new Dtos.DtoProperties.PurchaseOrdersAllocatedDtoProperty() { Amount = amount },
                            TaxAmount = amount,
                            AdditionalAmount = amount,
                            DiscountAmount = amount
                        },
                        SubmittedBy = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b")
                    }
                };

                lineItems = new List<PurchaseOrdersLineItemsDtoProperty>()
                {
                    new PurchaseOrdersLineItemsDtoProperty()
                    {
                        CommodityCode = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b"),
                        UnitOfMeasure = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b"),
                        UnitPrice = amount,
                        AdditionalAmount = amount,
                        TaxCodes = new List<GuidObject2>() { new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b") },
                        TradeDiscount = new TradeDiscountDtoProperty() { Amount = amount },
                        AccountDetail = accountDetails,
                        Description = "Desc",
                        Quantity = 1,
                        PartNumber = "0123456789"
                    }
                };

                initiator = new Dtos.DtoProperties.PurchaseOrdersInitiatorDtoProperty()
                {
                    Detail = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b"),
                };

                place = new AddressPlace()
                {
                    Country = new AddressCountry() { Code = IsoCode.USA }
                };

                vendor = new PurchaseOrdersVendorDtoProperty()
                {
                    ExistingVendor = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b")
                  //  ManualVendorDetails = new ManualVendorDetailsDtoProperty() { Place = place }
                };

                shippingDestination = new OverrideShippingDestinationDtoProperty()
                {
                    AddressLines = new List<string>() { "Line1" },
                    Place = place,
                    Contact = new PhoneDtoProperty() { Extension = "1234" }
                };

                purchaseOrders = new PurchaseOrders()
                {
                    Vendor = vendor,
                    OrderedOn = DateTime.Today,
                    TransactionDate = DateTime.Today.AddDays(10),
                    DeliveredBy = DateTime.Today.AddDays(2),
                    OverrideShippingDestination = shippingDestination,
                    PaymentSource = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b"),
                    Comments = new List<PurchaseOrdersCommentsDtoProperty>() { new PurchaseOrdersCommentsDtoProperty() { Comment = "c", Type = CommentTypes.NotPrinted } },
                    Buyer = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b"),
                    Initiator = initiator,
                    PaymentTerms = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b"),
                    Classification = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b"),
                    SubmittedBy = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b"),
                    LineItems = lineItems
                };
            }

            #endregion

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_IntegrationApiException_When_PurchaseOrder_Null()
            {
                await purchaseOrdersController.PostPurchaseOrdersAsync(null);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_Vendor_Null()
            {
                purchaseOrders.Vendor = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_Vendor_ExistingVendor_Null()
            {
                purchaseOrders.Vendor.ExistingVendor.Id = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_OrderOn_Is_Default_Date()
            {
                purchaseOrders.OrderedOn = default(DateTime);

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_TransactionDate_Is_Default_Date()
            {
                purchaseOrders.TransactionDate = default(DateTime);

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_OrderOn_GreaterThan_DeliveredBy()
            {
                purchaseOrders.OrderedOn = DateTime.Today.AddDays(5);

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_Destination_Country_Not_CAN_AND_USA()
            {
                purchaseOrders.OverrideShippingDestination.Place.Country.Code = IsoCode.AUS;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_Contact_Ext_Length_Morethan_4()
            {
                purchaseOrders.OverrideShippingDestination.Contact.Extension = "12345";

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            //Currently ManualVendorDetails is not complete for Colleague needs so Taking out
            // the object from test case and removed this test case.
            //[TestMethod]
            //[ExpectedException(typeof(HttpResponseException))]
            //public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_ManualVendor_Country_Not_CAN_AND_USA()
            //{
            //    purchaseOrders.Vendor.ManualVendorDetails.Place.Country.Code = IsoCode.AUS;

            //    await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            //}

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_PaymentSource_Null()
            {
                purchaseOrders.PaymentSource = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_Comment_Null()
            {
                purchaseOrders.Comments.FirstOrDefault().Comment = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_Comment_Type_NotSet()
            {
                purchaseOrders.Comments.FirstOrDefault().Type = CommentTypes.NotSet;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_Buyer_Null()
            {
                purchaseOrders.Buyer.Id = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_Initiator_Detail_Null()
            {
                purchaseOrders.Initiator.Detail.Id = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_PayTerms_Null()
            {
                purchaseOrders.PaymentTerms.Id = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_Clasification_Null()
            {
                purchaseOrders.Classification.Id = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_SubmittedBy_Null()
            {
                purchaseOrders.SubmittedBy.Id = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_Null()
            {
                purchaseOrders.LineItems = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_CommodityCode_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().CommodityCode.Id = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_UnitOfMeasure_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().UnitOfMeasure.Id = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_UnitPrice_Value_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().UnitPrice.Value = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_UnitPrice_Currency_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().UnitPrice.Currency = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AdditionalAmount_Value_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AdditionalAmount.Value = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AdditionalAmount_Currency_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AdditionalAmount.Currency = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_TaxCodeId_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().TaxCodes.FirstOrDefault().Id = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_TradeDiscount_AmountAndPer_NotNull()
            {
                purchaseOrders.LineItems.FirstOrDefault().TradeDiscount.Percent = 10;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_TradeDiscount_Amount_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().TradeDiscount.Amount.Value = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_TradeDiscount_Currency_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().TradeDiscount.Amount.Currency = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_AccountingString_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().AccountingString = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_AllocatedAmount_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation.Allocated.Amount.Value = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_AllocatedAmount_Currency_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation.Allocated.Amount.Currency = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_AllocationTaxAmount_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation.TaxAmount.Value = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_AllocationTaxAmount_Currency_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation.TaxAmount.Currency = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_Allocation_AdditionalAmount_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation.AdditionalAmount.Value = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_Allocation_AdditionalAmount_Currency_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation.AdditionalAmount.Currency = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_Allocation_DiscountAmount_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation.DiscountAmount.Value = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_Allocation_DiscountAmount_Currency_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation.DiscountAmount.Currency = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_SubmittedBy_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().SubmittedBy.Id = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_Allocation_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_Description_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().Description = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_Quantity_Is_Zero()
            {
                purchaseOrders.LineItems.FirstOrDefault().Quantity = 0;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_UnitPrice_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().UnitPrice = null;

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_PartNumber_Length_Morethan_11()
            {
                purchaseOrders.LineItems.FirstOrDefault().PartNumber = "012345678911";

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_KeyNotFoundException()
            {
                purchaseOrderServiceMock.Setup(s => s.PostPurchaseOrdersAsync(It.IsAny<PurchaseOrders>())).ThrowsAsync(new KeyNotFoundException());

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_PermissionsException()
            {
                purchaseOrderServiceMock.Setup(s => s.PostPurchaseOrdersAsync(It.IsAny<PurchaseOrders>())).ThrowsAsync(new PermissionsException());

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_RepositoryException()
            {
                purchaseOrderServiceMock.Setup(s => s.PostPurchaseOrdersAsync(It.IsAny<PurchaseOrders>())).ThrowsAsync(new RepositoryException());

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_IntegrationApiException()
            {
                purchaseOrderServiceMock.Setup(s => s.PostPurchaseOrdersAsync(It.IsAny<PurchaseOrders>())).ThrowsAsync(new IntegrationApiException());

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ConfigurationException()
            {
                purchaseOrderServiceMock.Setup(s => s.PostPurchaseOrdersAsync(It.IsAny<PurchaseOrders>())).ThrowsAsync(new ConfigurationException());

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_Exception()
            {
                purchaseOrderServiceMock.Setup(s => s.PostPurchaseOrdersAsync(It.IsAny<PurchaseOrders>())).ThrowsAsync(new Exception());

                await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);
            }

            [TestMethod]
            public async Task PoController_PostPurchaseOrdersAsync()
            {
                purchaseOrderServiceMock.Setup(s => s.PostPurchaseOrdersAsync(It.IsAny<PurchaseOrders>())).ReturnsAsync(purchaseOrders);

                var result = await purchaseOrdersController.PostPurchaseOrdersAsync(purchaseOrders);

                Assert.IsNotNull(result);
                Assert.AreEqual(purchaseOrders.Id, result.Id);
            }

        }

        [TestClass]
        public class PurchaseOrdersControllerTests_PUT
        {
            #region DECLARATIONS

            public TestContext TestContext { get; set; }

            private Mock<IPurchaseOrderService> purchaseOrderServiceMock;
            private Mock<ILogger> loggerMock;
            private PurchaseOrdersController purchaseOrdersController;

            private PurchaseOrders purchaseOrders;
            private PurchaseOrdersVendorDtoProperty vendor;
            private OverrideShippingDestinationDtoProperty shippingDestination;
            private AddressPlace place;
            private Dtos.DtoProperties.PurchaseOrdersInitiatorDtoProperty initiator;
            private List<PurchaseOrdersLineItemsDtoProperty> lineItems;
            private Dtos.DtoProperties.Amount2DtoProperty amount;
            private List<Dtos.DtoProperties.PurchaseOrdersAccountDetailDtoProperty> accountDetails;

            private string guid = "1adc2629-e8a7-410e-b4df-572d02822f8b";

            #endregion

            #region TEST SETUP

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                loggerMock = new Mock<ILogger>();
                purchaseOrderServiceMock = new Mock<IPurchaseOrderService>();

                InitializeTestData();

                purchaseOrderServiceMock.Setup(s => s.GetDataPrivacyListByApi(It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(new List<string>());

                purchaseOrdersController = new PurchaseOrdersController(purchaseOrderServiceMock.Object, loggerMock.Object)
                {
                    Request = new HttpRequestMessage()
                };
            }

            [TestCleanup]
            public void Cleanup()
            {
                loggerMock = null;
                purchaseOrderServiceMock = null;
                purchaseOrdersController = null;
            }

            private void InitializeTestData()
            {
                amount = new Dtos.DtoProperties.Amount2DtoProperty() { Currency = CurrencyIsoCode.USD, Value = 100 };

                accountDetails = new List<Dtos.DtoProperties.PurchaseOrdersAccountDetailDtoProperty>()
                {
                    new Dtos.DtoProperties.PurchaseOrdersAccountDetailDtoProperty()
                    {
                        AccountingString = "1",
                        Allocation = new Dtos.DtoProperties.PurchaseOrdersAllocationDtoProperty()
                        {
                            Allocated = new Dtos.DtoProperties.PurchaseOrdersAllocatedDtoProperty() { Amount = amount },
                            TaxAmount = amount,
                            AdditionalAmount = amount,
                            DiscountAmount = amount
                        },
                        SubmittedBy = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b")
                    }
                };

                lineItems = new List<PurchaseOrdersLineItemsDtoProperty>()
                {
                    new PurchaseOrdersLineItemsDtoProperty()
                    {
                        CommodityCode = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b"),
                        UnitOfMeasure = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b"),
                        UnitPrice = amount,
                        AdditionalAmount = amount,
                        TaxCodes = new List<GuidObject2>() { new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b") },
                        TradeDiscount = new TradeDiscountDtoProperty() { Amount = amount },
                        AccountDetail = accountDetails,
                        Description = "Desc",
                        Quantity = 1,
                        PartNumber = "0123456789"
                    }
                };

                initiator = new Dtos.DtoProperties.PurchaseOrdersInitiatorDtoProperty()
                {
                    Detail = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b"),
                };

                place = new AddressPlace()
                {
                    Country = new AddressCountry() { Code = IsoCode.USA }
                };

                //removing manualVendorDetails from payload, Since the object is not complete for Colleague needs
                // and we are now issueing an error if it exist in the payload.
                vendor = new PurchaseOrdersVendorDtoProperty()
                {
                    ExistingVendor = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b")
                    //ManualVendorDetails = new ManualVendorDetailsDtoProperty() { Place = place }
                };

                shippingDestination = new OverrideShippingDestinationDtoProperty()
                {
                    AddressLines = new List<string>() { "Line1" },
                    Place = place,
                    Contact = new PhoneDtoProperty() { Extension = "1234" }
                };

                purchaseOrders = new PurchaseOrders()
                {
                    Id = "1adc2629-e8a7-410e-b4df-572d02822f8b",
                    Vendor = vendor,
                    OrderedOn = DateTime.Today,
                    TransactionDate = DateTime.Today,
                    DeliveredBy = DateTime.Today.AddDays(2),
                    OverrideShippingDestination = shippingDestination,
                    PaymentSource = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b"),
                    Comments = new List<PurchaseOrdersCommentsDtoProperty>() { new PurchaseOrdersCommentsDtoProperty() { Comment = "c", Type = CommentTypes.NotPrinted } },
                    Buyer = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b"),
                    Initiator = initiator,
                    PaymentTerms = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b"),
                    Classification = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b"),
                    SubmittedBy = new GuidObject2("1adc2629-e8a7-410e-b4df-572d02822f8b"),
                    LineItems = lineItems
                };
            }

            #endregion

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_IntegrationApiException_When_Guid_Null()
            {
                await purchaseOrdersController.PutPurchaseOrdersAsync(null, It.IsAny<PurchaseOrders>());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_IntegrationApiException_When_PurchaseOrder_Null()
            {
                await purchaseOrdersController.PutPurchaseOrdersAsync("1", null);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_IntegrationApiException_When_Guid_Is_EmptyGuid()
            {
                await purchaseOrdersController.PutPurchaseOrdersAsync(Guid.Empty.ToString(), purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_IntegrationApiException_When_Guid_And_PurchaseOrderId_NotEqual()
            {
                await purchaseOrdersController.PutPurchaseOrdersAsync("2", purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_Vendor_Null()
            {
                purchaseOrders.Vendor = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_Vendor_ExistingVendor_Null()
            {
                purchaseOrders.Vendor.ExistingVendor.Id = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_OrderOn_Is_Default_Date()
            {
                purchaseOrders.OrderedOn = default(DateTime);

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_TransactionDate_Is_Default_Date()
            {
                purchaseOrders.TransactionDate = default(DateTime);

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_OrderOn_GreaterThan_DeliveredBy()
            {
                purchaseOrders.OrderedOn = DateTime.Today.AddDays(5);

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_Destination_Country_Not_CAN_AND_USA()
            {
                purchaseOrders.OverrideShippingDestination.Place.Country.Code = IsoCode.AUS;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_Contact_Ext_Length_Morethan_4()
            {
                purchaseOrders.OverrideShippingDestination.Contact.Extension = "12345";

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            //Currently ManualVendorDetails is not complete for Colleague needs so Taking out
            // the object from test case and removed this test case.
            //[TestMethod]
            //[ExpectedException(typeof(HttpResponseException))]
            //public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_ManualVendor_Country_Not_CAN_AND_USA()
            //{
            //    purchaseOrders.Vendor.ManualVendorDetails.Place.Country.Code = IsoCode.AUS;

            //    await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            //}

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_PaymentSource_Null()
            {
                purchaseOrders.PaymentSource = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_Comment_Null()
            {
                purchaseOrders.Comments.FirstOrDefault().Comment = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_Comment_Type_NotSet()
            {
                purchaseOrders.Comments.FirstOrDefault().Type = CommentTypes.NotSet;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_Buyer_Null()
            {
                purchaseOrders.Buyer.Id = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_Initiator_Detail_Null()
            {
                purchaseOrders.Initiator.Detail.Id = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_PayTerms_Null()
            {
                purchaseOrders.PaymentTerms.Id = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_Clasification_Null()
            {
                purchaseOrders.Classification.Id = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_SubmittedBy_Null()
            {
                purchaseOrders.SubmittedBy.Id = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_Null()
            {
                purchaseOrders.LineItems = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_CommodityCode_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().CommodityCode.Id = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_UnitOfMeasure_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().UnitOfMeasure.Id = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_UnitPrice_Value_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().UnitPrice.Value = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_UnitPrice_Currency_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().UnitPrice.Currency = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AdditionalAmount_Value_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AdditionalAmount.Value = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AdditionalAmount_Currency_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AdditionalAmount.Currency = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_TaxCodeId_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().TaxCodes.FirstOrDefault().Id = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_TradeDiscount_AmountAndPer_NotNull()
            {
                purchaseOrders.LineItems.FirstOrDefault().TradeDiscount.Percent = 10;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_TradeDiscount_Amount_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().TradeDiscount.Amount.Value = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_TradeDiscount_Currency_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().TradeDiscount.Amount.Currency = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_AccountingString_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().AccountingString = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_AllocatedAmount_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation.Allocated.Amount.Value = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_AllocatedAmount_Currency_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation.Allocated.Amount.Currency = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_AllocationTaxAmount_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation.TaxAmount.Value = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_AllocationTaxAmount_Currency_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation.TaxAmount.Currency = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_Allocation_AdditionalAmount_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation.AdditionalAmount.Value = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_Allocation_AdditionalAmount_Currency_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation.AdditionalAmount.Currency = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_Allocation_DiscountAmount_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation.DiscountAmount.Value = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_Allocation_DiscountAmount_Currency_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation.DiscountAmount.Currency = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_SubmittedBy_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().SubmittedBy.Id = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_AccountDetails_Allocation_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Allocation = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_Description_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().Description = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_Quantity_Is_Zero()
            {
                purchaseOrders.LineItems.FirstOrDefault().Quantity = 0;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_UnitPrice_Null()
            {
                purchaseOrders.LineItems.FirstOrDefault().UnitPrice = null;

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PutPurchaseOrdersAsync_ArgumentNullException_When_PO_LineItems_PartNumber_Length_Morethan_11()
            {
                purchaseOrders.LineItems.FirstOrDefault().PartNumber = "012345678911";

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_KeyNotFoundException()
            {
                purchaseOrderServiceMock.Setup(s => s.PutPurchaseOrdersAsync(It.IsAny<string>(), It.IsAny<PurchaseOrders>())).ThrowsAsync(new KeyNotFoundException());

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_PermissionsException()
            {
                purchaseOrderServiceMock.Setup(s => s.PutPurchaseOrdersAsync(It.IsAny<string>(), It.IsAny<PurchaseOrders>())).ThrowsAsync(new PermissionsException());

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_RepositoryException()
            {
                purchaseOrderServiceMock.Setup(s => s.PutPurchaseOrdersAsync(It.IsAny<string>(), It.IsAny<PurchaseOrders>())).ThrowsAsync(new RepositoryException());

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_IntegrationApiException()
            {
                purchaseOrderServiceMock.Setup(s => s.PutPurchaseOrdersAsync(It.IsAny<string>(), It.IsAny<PurchaseOrders>())).ThrowsAsync(new IntegrationApiException());

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_ConfigurationException()
            {
                purchaseOrderServiceMock.Setup(s => s.PutPurchaseOrdersAsync(It.IsAny<string>(), It.IsAny<PurchaseOrders>())).ThrowsAsync(new ConfigurationException());

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task PoController_PostPurchaseOrdersAsync_Exception()
            {
                purchaseOrderServiceMock.Setup(s => s.PutPurchaseOrdersAsync(It.IsAny<string>(), It.IsAny<PurchaseOrders>())).ThrowsAsync(new Exception());

                await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);
            }

            [TestMethod]
            public async Task PoController_PutPurchaseOrdersAsync()
            {
                purchaseOrderServiceMock.Setup(s => s.PutPurchaseOrdersAsync(It.IsAny<string>(), It.IsAny<PurchaseOrders>())).ReturnsAsync(purchaseOrders);

                purchaseOrders.Id = null;

                var result = await purchaseOrdersController.PutPurchaseOrdersAsync(guid, purchaseOrders);

                Assert.IsNotNull(result);
            }
        }
    }
}