﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Api.Controllers.ColleagueFinance;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Colleague.Dtos;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Ellucian.Colleague.Dtos.DtoProperties;
using Ellucian.Colleague.Dtos.EnumProperties;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.Exceptions;

namespace Ellucian.Colleague.Api.Tests.Controllers.ColleagueFinance
{
    [TestClass]
    public class GeneralLedgerTransactionsControllerTestsGet
    {

        #region Test Context

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #endregion

        private Mock<IAdapterRegistry> _adapterRegistryMock;
        private Mock<ILogger> _loggerMock;
        private Mock<IGeneralLedgerTransactionService> _glServiceMock;

        private GeneralLedgerTransaction _generalLedgerTransaction;
        private List<GeneralLedgerTransaction> _generalLedgerTransactions;
        private HttpResponse _response;

        private GeneralLedgerTransactionsController _generalLedgerTransactionsController;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            _adapterRegistryMock = new Mock<IAdapterRegistry>();
            _loggerMock = new Mock<ILogger>();
            _glServiceMock = new Mock<IGeneralLedgerTransactionService>();

            var generalLedgerDetailDtoProperty = new GeneralLedgerDetailDtoProperty()
            {
                AccountingString = "784545",
                Description = "description",
                SequenceNumber = 1,
                Amount = new AmountDtoProperty() { Currency = CurrencyCodes.USD, Value = 25 }
            };

            var gltDtoProperty = new GeneralLedgerTransactionDtoProperty()
            {
                LedgerDate = DateTimeOffset.Now.DateTime,
                ReferenceNumber = "GL122312321",
                Reference = new GeneralLedgerReferenceDtoProperty()
                {
                    Organization = new GuidObject2("B17F7796-53D1-403C-A883-934D4DE04F1D"),
                    Person = new GuidObject2("C17F7796-53D1-403C-A883-934D4DE04F1D")
                },
                TransactionNumber = "1",
                TransactionTypeReferenceDate = DateTimeOffset.Now,
                Type = GeneralLedgerTransactionType.ActualOpenBalance,
                TransactionDetailLines = new List<GeneralLedgerDetailDtoProperty>() { generalLedgerDetailDtoProperty }
            };

            _generalLedgerTransaction = new GeneralLedgerTransaction
            {
                Id = "0001234",
                ProcessMode = ProcessMode.Update,
                Transactions = new List<GeneralLedgerTransactionDtoProperty>() { gltDtoProperty }
            };

            _generalLedgerTransactions = new List<GeneralLedgerTransaction>() { _generalLedgerTransaction };
           
            _response = new HttpResponse(new StringWriter());
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://doesntMatter.com", ""), _response);

            _glServiceMock.Setup(x => x.GetAsync()).ReturnsAsync(_generalLedgerTransactions);
            _generalLedgerTransactionsController = new GeneralLedgerTransactionsController(_glServiceMock.Object,
                _loggerMock.Object);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _adapterRegistryMock = null;
            _loggerMock = null;
            _glServiceMock = null;
            _generalLedgerTransactionsController = null;
        }

        [TestMethod]
        public async Task GeneralLedgerTransactionsController_GetAsync_Valid()
        {
            _glServiceMock.Setup(x => x.GetAsync()).ReturnsAsync(_generalLedgerTransactions);
            var generalLedgerTransaction = await _generalLedgerTransactionsController.GetAsync();
            Assert.IsNotNull(generalLedgerTransaction);
        }

        

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetAsync_PermissionsException()
        {
            _glServiceMock.Setup(x => x.GetAsync()).ThrowsAsync(new PermissionsException());
            await _generalLedgerTransactionsController.GetAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetAsync_KeyNotFoundException()
        {
            _glServiceMock.Setup(x => x.GetAsync()).ThrowsAsync(new KeyNotFoundException());
            await _generalLedgerTransactionsController.GetAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetAsync_ArgumentNullException()
        {
            _glServiceMock.Setup(x => x.GetAsync()).ThrowsAsync(new ArgumentNullException());
            await _generalLedgerTransactionsController.GetAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetAsync_IntegrationApiException()
        {
            _glServiceMock.Setup(x => x.GetAsync()).ThrowsAsync(new IntegrationApiException());
            await _generalLedgerTransactionsController.GetAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetAsync_ConfigurationException()
        {
            _glServiceMock.Setup(x => x.GetAsync()).ThrowsAsync(new ConfigurationException());
            await _generalLedgerTransactionsController.GetAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetAsync_ArgumentOutOfRangeException()
        {
            _glServiceMock.Setup(x => x.GetAsync()).ThrowsAsync(new ArgumentOutOfRangeException());
            await _generalLedgerTransactionsController.GetAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetAsync_ArgumentException()
        {
            _glServiceMock.Setup(x => x.GetAsync()).ThrowsAsync(new ArgumentException());
            await _generalLedgerTransactionsController.GetAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetAsync_InvalidOperationException()
        {
            _glServiceMock.Setup(x => x.GetAsync()).ThrowsAsync(new InvalidOperationException());
            await _generalLedgerTransactionsController.GetAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetAsync_FormatException()
        {
            _glServiceMock.Setup(x => x.GetAsync()).ThrowsAsync(new FormatException());
            await _generalLedgerTransactionsController.GetAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetAsync_RepositoryException()
        {
            _glServiceMock.Setup(x => x.GetAsync()).ThrowsAsync(new RepositoryException());
            await _generalLedgerTransactionsController.GetAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetAsync_Exception()
        {
            _glServiceMock.Setup(x => x.GetAsync()).ThrowsAsync(new Exception());
            await _generalLedgerTransactionsController.GetAsync();
        }


    }

    [TestClass]
    public class GeneralLedgerTransactionsControllerTestsGet2
    {

        #region Test Context

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #endregion

        private Mock<IAdapterRegistry> _adapterRegistryMock;
        private Mock<ILogger> _loggerMock;
        private Mock<IGeneralLedgerTransactionService> _glServiceMock;

        private GeneralLedgerTransaction2 _generalLedgerTransaction;
        private List<GeneralLedgerTransaction2> _generalLedgerTransactions;
        private HttpResponse _response;

        private GeneralLedgerTransactionsController _generalLedgerTransactionsController;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            _adapterRegistryMock = new Mock<IAdapterRegistry>();
            _loggerMock = new Mock<ILogger>();
            _glServiceMock = new Mock<IGeneralLedgerTransactionService>();

            var generalLedgerDetailDtoProperty = new GeneralLedgerDetailDtoProperty2()
            {
                AccountingString = "784545",
                Description = "description",
                SequenceNumber = 1,
                Amount = new AmountDtoProperty() { Currency = CurrencyCodes.USD, Value = 25 },
                SubmittedBy = new GuidObject2() { Id= "123456" }
            };

            var gltDtoProperty = new GeneralLedgerTransactionDtoProperty2()
            {
                LedgerDate = DateTimeOffset.Now.DateTime,
                ReferenceNumber = "GL122312321",
                Reference = new GeneralLedgerReferenceDtoProperty()
                {
                    Organization = new GuidObject2("B17F7796-53D1-403C-A883-934D4DE04F1D"),
                    Person = new GuidObject2("C17F7796-53D1-403C-A883-934D4DE04F1D")
                },
                TransactionNumber = "1",
                TransactionTypeReferenceDate = DateTimeOffset.Now,
                Type = GeneralLedgerTransactionType.ActualOpenBalance,
                TransactionDetailLines = new List<GeneralLedgerDetailDtoProperty2>() { generalLedgerDetailDtoProperty }
            };

            _generalLedgerTransaction = new GeneralLedgerTransaction2
            {
                Id = "0001234",
                ProcessMode = ProcessMode.Update,
                Transactions = new List<GeneralLedgerTransactionDtoProperty2>() { gltDtoProperty },
                SubmittedBy = new GuidObject2() { Id= "123456" }
            };

            _generalLedgerTransactions = new List<GeneralLedgerTransaction2>() { _generalLedgerTransaction };

            _response = new HttpResponse(new StringWriter());
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://doesntMatter.com", ""), _response);

            _glServiceMock.Setup(x => x.Get2Async()).ReturnsAsync(_generalLedgerTransactions);
            _generalLedgerTransactionsController = new GeneralLedgerTransactionsController(_glServiceMock.Object,
                _loggerMock.Object);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _adapterRegistryMock = null;
            _loggerMock = null;
            _glServiceMock = null;
            _generalLedgerTransactionsController = null;
        }

        [TestMethod]
        public async Task GeneralLedgerTransactionsController_Get2Async_Valid()
        {
            _glServiceMock.Setup(x => x.Get2Async()).ReturnsAsync(_generalLedgerTransactions);
            var generalLedgerTransaction = await _generalLedgerTransactionsController.GetAsync();
            Assert.IsNotNull(generalLedgerTransaction);
        }



        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Get2Async_PermissionsException()
        {
            _glServiceMock.Setup(x => x.Get2Async()).ThrowsAsync(new PermissionsException());
            await _generalLedgerTransactionsController.Get2Async();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Get2Async_KeyNotFoundException()
        {
            _glServiceMock.Setup(x => x.Get2Async()).ThrowsAsync(new KeyNotFoundException());
            await _generalLedgerTransactionsController.Get2Async();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Get2Async_ArgumentNullException()
        {
            _glServiceMock.Setup(x => x.Get2Async()).ThrowsAsync(new ArgumentNullException());
            await _generalLedgerTransactionsController.Get2Async();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Get2Async_IntegrationApiException()
        {
            _glServiceMock.Setup(x => x.Get2Async()).ThrowsAsync(new IntegrationApiException());
            await _generalLedgerTransactionsController.Get2Async();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Get2Async_ConfigurationException()
        {
            _glServiceMock.Setup(x => x.Get2Async()).ThrowsAsync(new ConfigurationException());
            await _generalLedgerTransactionsController.Get2Async();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Get2Async_ArgumentOutOfRangeException()
        {
            _glServiceMock.Setup(x => x.Get2Async()).ThrowsAsync(new ArgumentOutOfRangeException());
            await _generalLedgerTransactionsController.Get2Async();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Get2Async_ArgumentException()
        {
            _glServiceMock.Setup(x => x.Get2Async()).ThrowsAsync(new ArgumentException());
            await _generalLedgerTransactionsController.Get2Async();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Get2Async_InvalidOperationException()
        {
            _glServiceMock.Setup(x => x.Get2Async()).ThrowsAsync(new InvalidOperationException());
            await _generalLedgerTransactionsController.Get2Async();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Get2Async_FormatException()
        {
            _glServiceMock.Setup(x => x.Get2Async()).ThrowsAsync(new FormatException());
            await _generalLedgerTransactionsController.Get2Async();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Get2Async_RepositoryException()
        {
            _glServiceMock.Setup(x => x.Get2Async()).ThrowsAsync(new RepositoryException());
            await _generalLedgerTransactionsController.Get2Async();
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Get2Async_Exception()
        {
            _glServiceMock.Setup(x => x.Get2Async()).ThrowsAsync(new Exception());
            await _generalLedgerTransactionsController.Get2Async();
        }


    }

    [TestClass]
    public class GeneralLedgerTransactionsControllerTestsGetById
    {

        #region Test Context

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        #endregion

        private Mock<IAdapterRegistry> _adapterRegistryMock;
        private Mock<ILogger> _loggerMock;
        private Mock<IGeneralLedgerTransactionService> _glServiceMock;

        private GeneralLedgerTransaction _generalLedgerTransaction;
        private HttpResponse _response;

        private GeneralLedgerTransactionsController _generalLedgerTransactionsController;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            _adapterRegistryMock = new Mock<IAdapterRegistry>();
            _loggerMock = new Mock<ILogger>();
            _glServiceMock = new Mock<IGeneralLedgerTransactionService>();

            var generalLedgerDetailDtoProperty = new GeneralLedgerDetailDtoProperty()
            {
                AccountingString = "784545",
                Description = "description",
                SequenceNumber = 1,
                Amount = new AmountDtoProperty() {Currency = CurrencyCodes.USD, Value = 25}
            };

            var gltDtoProperty = new GeneralLedgerTransactionDtoProperty()
            {
                LedgerDate = DateTimeOffset.Now.DateTime,
                ReferenceNumber = "GL122312321",
                Reference = new GeneralLedgerReferenceDtoProperty()
                {
                    Organization = new GuidObject2("B17F7796-53D1-403C-A883-934D4DE04F1D"),
                    Person = new GuidObject2("C17F7796-53D1-403C-A883-934D4DE04F1D")
                },
                TransactionNumber = "1",
                TransactionTypeReferenceDate = DateTimeOffset.Now,
                Type = GeneralLedgerTransactionType.ActualOpenBalance,
                TransactionDetailLines = new List<GeneralLedgerDetailDtoProperty>() {generalLedgerDetailDtoProperty}
            };

            _generalLedgerTransaction = new GeneralLedgerTransaction
            {
                Id = "0001234",
                ProcessMode = ProcessMode.Update,
                Transactions = new List<GeneralLedgerTransactionDtoProperty>() {gltDtoProperty}
            };

            _response = new HttpResponse(new StringWriter());
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://doesntMatter.com", ""), _response);

            _glServiceMock.Setup(x => x.GetByIdAsync(It.IsAny<string>())).ReturnsAsync(_generalLedgerTransaction);
            _generalLedgerTransactionsController = new GeneralLedgerTransactionsController(_glServiceMock.Object,
                _loggerMock.Object);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _adapterRegistryMock = null;
            _loggerMock = null;
            _glServiceMock = null;
            _generalLedgerTransactionsController = null;
        }

        [TestMethod]
        public async Task GeneralLedgerTransactionsController_GetByIdAsync_Valid()
        {
            _glServiceMock.Setup(x => x.GetByIdAsync(It.IsAny<string>())).ReturnsAsync(_generalLedgerTransaction);
            var generalLedgerTransaction = await _generalLedgerTransactionsController.GetByIdAsync("0001234");
            Assert.IsNotNull(generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetByIdAsync_EmptyID()
        {
            _glServiceMock.Setup(x => x.GetByIdAsync(It.IsAny<string>())).ReturnsAsync(_generalLedgerTransaction);
            await _generalLedgerTransactionsController.GetByIdAsync("");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetByIdAsync_NullID()
        {
            _glServiceMock.Setup(x => x.GetByIdAsync(It.IsAny<string>())).ReturnsAsync(_generalLedgerTransaction);
            await _generalLedgerTransactionsController.GetByIdAsync(null);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetByIdAsync_PermissionsException()
        {
            _glServiceMock.Setup(x => x.GetByIdAsync(It.IsAny<string>())).ThrowsAsync(new PermissionsException());
            await _generalLedgerTransactionsController.GetByIdAsync("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetByIdAsync_KeyNotFoundException()
        {
            _glServiceMock.Setup(x => x.GetByIdAsync(It.IsAny<string>())).ThrowsAsync(new KeyNotFoundException());
            await _generalLedgerTransactionsController.GetByIdAsync("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetByIdAsync_ArgumentNullException()
        {
            _glServiceMock.Setup(x => x.GetByIdAsync(It.IsAny<string>())).ThrowsAsync(new ArgumentNullException());
            await _generalLedgerTransactionsController.GetByIdAsync("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetByIdAsync_IntegrationApiException()
        {
            _glServiceMock.Setup(x => x.GetByIdAsync(It.IsAny<string>())).ThrowsAsync(new IntegrationApiException());
            await _generalLedgerTransactionsController.GetByIdAsync("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetByIdAsync_ConfigurationException()
        {
            _glServiceMock.Setup(x => x.GetByIdAsync(It.IsAny<string>())).ThrowsAsync(new ConfigurationException());
            await _generalLedgerTransactionsController.GetByIdAsync("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetByIdAsync_ArgumentOutOfRangeException()
        {
            _glServiceMock.Setup(x => x.GetByIdAsync(It.IsAny<string>())).ThrowsAsync(new ArgumentOutOfRangeException());
            await _generalLedgerTransactionsController.GetByIdAsync("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetByIdAsync_ArgumentException()
        {
            _glServiceMock.Setup(x => x.GetByIdAsync(It.IsAny<string>())).ThrowsAsync(new ArgumentException());
            await _generalLedgerTransactionsController.GetByIdAsync("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetByIdAsync_InvalidOperationException()
        {
            _glServiceMock.Setup(x => x.GetByIdAsync(It.IsAny<string>())).ThrowsAsync(new InvalidOperationException());
            await _generalLedgerTransactionsController.GetByIdAsync("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetByIdAsync_FormatException()
        {
            _glServiceMock.Setup(x => x.GetByIdAsync(It.IsAny<string>())).ThrowsAsync(new FormatException());
            await _generalLedgerTransactionsController.GetByIdAsync("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetByIdAsync_RepositoryException()
        {
            _glServiceMock.Setup(x => x.GetByIdAsync(It.IsAny<string>())).ThrowsAsync(new RepositoryException());
            await _generalLedgerTransactionsController.GetByIdAsync("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetByIdAsync_Exception()
        {
            _glServiceMock.Setup(x => x.GetByIdAsync(It.IsAny<string>())).ThrowsAsync(new Exception());
            await _generalLedgerTransactionsController.GetByIdAsync("0001234");
        }

    }

    [TestClass]
    public class GeneralLedgerTransactionsControllerTestsGetById2
    {

        #region Test Context

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }

        #endregion

        private Mock<IAdapterRegistry> _adapterRegistryMock;
        private Mock<ILogger> _loggerMock;
        private Mock<IGeneralLedgerTransactionService> _glServiceMock;

        private GeneralLedgerTransaction2 _generalLedgerTransaction;
        private HttpResponse _response;

        private GeneralLedgerTransactionsController _generalLedgerTransactionsController;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            _adapterRegistryMock = new Mock<IAdapterRegistry>();
            _loggerMock = new Mock<ILogger>();
            _glServiceMock = new Mock<IGeneralLedgerTransactionService>();

            var generalLedgerDetailDtoProperty = new GeneralLedgerDetailDtoProperty2()
            {
                AccountingString = "784545",
                Description = "description",
                SequenceNumber = 1,
                Amount = new AmountDtoProperty() { Currency = CurrencyCodes.USD, Value = 25 },
                SubmittedBy = new GuidObject2() { Id = "123456" }
            };

            var gltDtoProperty = new GeneralLedgerTransactionDtoProperty2()
            {
                LedgerDate = DateTimeOffset.Now.DateTime,
                ReferenceNumber = "GL122312321",
                Reference = new GeneralLedgerReferenceDtoProperty()
                {
                    Organization = new GuidObject2("B17F7796-53D1-403C-A883-934D4DE04F1D"),
                    Person = new GuidObject2("C17F7796-53D1-403C-A883-934D4DE04F1D")
                },
                TransactionNumber = "1",
                TransactionTypeReferenceDate = DateTimeOffset.Now,
                Type = GeneralLedgerTransactionType.ActualOpenBalance,
                TransactionDetailLines = new List<GeneralLedgerDetailDtoProperty2>() { generalLedgerDetailDtoProperty }
            };

            _generalLedgerTransaction = new GeneralLedgerTransaction2
            {
                Id = "0001234",
                ProcessMode = ProcessMode.Update,
                SubmittedBy = new GuidObject2() { Id= "123456" },
                Transactions = new List<GeneralLedgerTransactionDtoProperty2>() { gltDtoProperty }
            };

            _response = new HttpResponse(new StringWriter());
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://doesntMatter.com", ""), _response);

            _glServiceMock.Setup(x => x.GetById2Async(It.IsAny<string>())).ReturnsAsync(_generalLedgerTransaction);
            _generalLedgerTransactionsController = new GeneralLedgerTransactionsController(_glServiceMock.Object,
                _loggerMock.Object);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _adapterRegistryMock = null;
            _loggerMock = null;
            _glServiceMock = null;
            _generalLedgerTransactionsController = null;
        }

        [TestMethod]
        public async Task GeneralLedgerTransactionsController_GetById2Async_Valid()
        {
            _glServiceMock.Setup(x => x.GetById2Async(It.IsAny<string>())).ReturnsAsync(_generalLedgerTransaction);
            var generalLedgerTransaction = await _generalLedgerTransactionsController.GetById2Async("0001234");
            Assert.IsNotNull(generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetById2Async_EmptyID()
        {
            _glServiceMock.Setup(x => x.GetById2Async(It.IsAny<string>())).ReturnsAsync(_generalLedgerTransaction);
            await _generalLedgerTransactionsController.GetById2Async("");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetById2Async_NullID()
        {
            _glServiceMock.Setup(x => x.GetById2Async(It.IsAny<string>())).ReturnsAsync(_generalLedgerTransaction);
            await _generalLedgerTransactionsController.GetById2Async(null);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetById2Async_PermissionsException()
        {
            _glServiceMock.Setup(x => x.GetById2Async(It.IsAny<string>())).ThrowsAsync(new PermissionsException());
            await _generalLedgerTransactionsController.GetById2Async("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetById2Async_KeyNotFoundException()
        {
            _glServiceMock.Setup(x => x.GetById2Async(It.IsAny<string>())).ThrowsAsync(new KeyNotFoundException());
            await _generalLedgerTransactionsController.GetById2Async("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetById2Async_ArgumentNullException()
        {
            _glServiceMock.Setup(x => x.GetById2Async(It.IsAny<string>())).ThrowsAsync(new ArgumentNullException());
            await _generalLedgerTransactionsController.GetById2Async("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetById2Async_IntegrationApiException()
        {
            _glServiceMock.Setup(x => x.GetById2Async(It.IsAny<string>())).ThrowsAsync(new IntegrationApiException());
            await _generalLedgerTransactionsController.GetById2Async("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetById2Async_ConfigurationException()
        {
            _glServiceMock.Setup(x => x.GetById2Async(It.IsAny<string>())).ThrowsAsync(new ConfigurationException());
            await _generalLedgerTransactionsController.GetById2Async("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetById2Async_ArgumentOutOfRangeException()
        {
            _glServiceMock.Setup(x => x.GetById2Async(It.IsAny<string>())).ThrowsAsync(new ArgumentOutOfRangeException());
            await _generalLedgerTransactionsController.GetById2Async("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetById2Async_ArgumentException()
        {
            _glServiceMock.Setup(x => x.GetById2Async(It.IsAny<string>())).ThrowsAsync(new ArgumentException());
            await _generalLedgerTransactionsController.GetById2Async("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetById2Async_InvalidOperationException()
        {
            _glServiceMock.Setup(x => x.GetById2Async(It.IsAny<string>())).ThrowsAsync(new InvalidOperationException());
            await _generalLedgerTransactionsController.GetById2Async("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetById2Async_FormatException()
        {
            _glServiceMock.Setup(x => x.GetById2Async(It.IsAny<string>())).ThrowsAsync(new FormatException());
            await _generalLedgerTransactionsController.GetById2Async("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetById2Async_RepositoryException()
        {
            _glServiceMock.Setup(x => x.GetById2Async(It.IsAny<string>())).ThrowsAsync(new RepositoryException());
            await _generalLedgerTransactionsController.GetById2Async("0001234");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_GetById2Async_Exception()
        {
            _glServiceMock.Setup(x => x.GetById2Async(It.IsAny<string>())).ThrowsAsync(new Exception());
            await _generalLedgerTransactionsController.GetById2Async("0001234");
        }

    }

    [TestClass]
    public class GeneralLedgerTransactionsControllerTestsCreate
    {

        #region Test Context

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #endregion

        private Mock<IAdapterRegistry> _adapterRegistryMock;
        private Mock<ILogger> _loggerMock;
        private Mock<IGeneralLedgerTransactionService> _glServiceMock;

        private GeneralLedgerTransaction _generalLedgerTransaction;
        private List<GeneralLedgerTransaction> _generalLedgerTransactions;
        private HttpResponse _response;

        private GeneralLedgerTransactionsController _generalLedgerTransactionsController;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            _adapterRegistryMock = new Mock<IAdapterRegistry>();
            _loggerMock = new Mock<ILogger>();
            _glServiceMock = new Mock<IGeneralLedgerTransactionService>();

            var generalLedgerDetailDtoProperty = new GeneralLedgerDetailDtoProperty()
            {
                AccountingString = "784545",
                Description = "description",
                SequenceNumber = 1,
                Amount = new AmountDtoProperty() { Currency = CurrencyCodes.USD, Value = 25 }
            };

            var gltDtoProperty = new GeneralLedgerTransactionDtoProperty()
            {
                LedgerDate = DateTimeOffset.Now.DateTime,
                ReferenceNumber = "GL122312321",
                Reference = new GeneralLedgerReferenceDtoProperty()
                {
                    Organization = new GuidObject2("B17F7796-53D1-403C-A883-934D4DE04F1D"),
                    Person = new GuidObject2("C17F7796-53D1-403C-A883-934D4DE04F1D")
                },
                TransactionNumber = "1",
                TransactionTypeReferenceDate = DateTimeOffset.Now,
                Type = GeneralLedgerTransactionType.ActualOpenBalance,
                TransactionDetailLines = new List<GeneralLedgerDetailDtoProperty>() { generalLedgerDetailDtoProperty }
            };

            _generalLedgerTransaction = new GeneralLedgerTransaction
            {
                Id = "0001234",
                ProcessMode = ProcessMode.Update,
                Transactions = new List<GeneralLedgerTransactionDtoProperty>() { gltDtoProperty }
            };

            _generalLedgerTransactions = new List<GeneralLedgerTransaction>() { _generalLedgerTransaction };

            _response = new HttpResponse(new StringWriter());
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://doesntMatter.com", ""), _response);

            _glServiceMock.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>())).ReturnsAsync(_generalLedgerTransaction);
            _generalLedgerTransactionsController = new GeneralLedgerTransactionsController(_glServiceMock.Object,
                _loggerMock.Object);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _adapterRegistryMock = null;
            _loggerMock = null;
            _glServiceMock = null;
            _generalLedgerTransactionsController = null;
        }

        [TestMethod]
        public async Task GeneralLedgerTransactionsController_CreateAsync_Valid()
        {
            _glServiceMock.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>())).ReturnsAsync(_generalLedgerTransaction);

            var generalLedgerTransaction = await _generalLedgerTransactionsController.CreateAsync(_generalLedgerTransaction);
            Assert.IsNotNull(generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_CreateAsync_NullBody()
        {
             await _generalLedgerTransactionsController.CreateAsync(null);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_CreateAsync_PermissionsException()
        {
            _glServiceMock.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new PermissionsException());
            await _generalLedgerTransactionsController.CreateAsync(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_CreateAsync_KeyNotFoundException()
        {
            _glServiceMock.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new KeyNotFoundException());
            await _generalLedgerTransactionsController.CreateAsync(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_CreateAsync_ArgumentNullException()
        {
            _glServiceMock.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new ArgumentNullException());
            await _generalLedgerTransactionsController.CreateAsync(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_CreateAsync_IntegrationApiException()
        {
            _glServiceMock.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new IntegrationApiException());
            await _generalLedgerTransactionsController.CreateAsync(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_CreateAsync_ConfigurationException()
        {
            _glServiceMock.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new ConfigurationException());
            await _generalLedgerTransactionsController.CreateAsync(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_CreateAsync_ArgumentOutOfRangeException()
        {
            _glServiceMock.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new ArgumentOutOfRangeException());
            await _generalLedgerTransactionsController.CreateAsync(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_CreateAsync_ArgumentException()
        {
            _glServiceMock.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new ArgumentException());
            await _generalLedgerTransactionsController.CreateAsync(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_CreateAsync_InvalidOperationException()
        {
            _glServiceMock.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new InvalidOperationException());
            await _generalLedgerTransactionsController.CreateAsync(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_CreateAsync_FormatException()
        {
            _glServiceMock.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new FormatException());
            await _generalLedgerTransactionsController.CreateAsync(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_CreateAsync_RepositoryException()
        {
            _glServiceMock.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new RepositoryException());
            await _generalLedgerTransactionsController.CreateAsync(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_CreateAsync_Exception()
        {
            _glServiceMock.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new Exception());
            await _generalLedgerTransactionsController.CreateAsync(_generalLedgerTransaction);
        }

    }

    [TestClass]
    public class GeneralLedgerTransactionsControllerTestsCreate2
    {

        #region Test Context

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #endregion

        private Mock<IAdapterRegistry> _adapterRegistryMock;
        private Mock<ILogger> _loggerMock;
        private Mock<IGeneralLedgerTransactionService> _glServiceMock;

        private GeneralLedgerTransaction2 _generalLedgerTransaction;
        private List<GeneralLedgerTransaction2> _generalLedgerTransactions;
        private HttpResponse _response;

        private GeneralLedgerTransactionsController _generalLedgerTransactionsController;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            _adapterRegistryMock = new Mock<IAdapterRegistry>();
            _loggerMock = new Mock<ILogger>();
            _glServiceMock = new Mock<IGeneralLedgerTransactionService>();

            var generalLedgerDetailDtoProperty = new GeneralLedgerDetailDtoProperty2()
            {
                AccountingString = "784545",
                Description = "description",
                SequenceNumber = 1,
                SubmittedBy = new GuidObject2() { Id = "123456" },
                Amount = new AmountDtoProperty() { Currency = CurrencyCodes.USD, Value = 25 }
            };

            var gltDtoProperty = new GeneralLedgerTransactionDtoProperty2()
            {
                LedgerDate = DateTimeOffset.Now.DateTime,
                ReferenceNumber = "GL122312321",
                Reference = new GeneralLedgerReferenceDtoProperty()
                {
                    Organization = new GuidObject2("B17F7796-53D1-403C-A883-934D4DE04F1D"),
                    Person = new GuidObject2("C17F7796-53D1-403C-A883-934D4DE04F1D")
                },
                TransactionNumber = "1",
                TransactionTypeReferenceDate = DateTimeOffset.Now,
                Type = GeneralLedgerTransactionType.ActualOpenBalance,
                TransactionDetailLines = new List<GeneralLedgerDetailDtoProperty2>() { generalLedgerDetailDtoProperty }
            };

            _generalLedgerTransaction = new GeneralLedgerTransaction2
            {
                Id = "0001234",
                ProcessMode = ProcessMode.Update,
                SubmittedBy = new GuidObject2() { Id = "123456" },
                Transactions = new List<GeneralLedgerTransactionDtoProperty2>() { gltDtoProperty }
            };

            _generalLedgerTransactions = new List<GeneralLedgerTransaction2>() { _generalLedgerTransaction };

            _response = new HttpResponse(new StringWriter());
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://doesntMatter.com", ""), _response);

            _glServiceMock.Setup(x => x.Create2Async(It.IsAny<GeneralLedgerTransaction2>())).ReturnsAsync(_generalLedgerTransaction);
            _generalLedgerTransactionsController = new GeneralLedgerTransactionsController(_glServiceMock.Object,
                _loggerMock.Object);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _adapterRegistryMock = null;
            _loggerMock = null;
            _glServiceMock = null;
            _generalLedgerTransactionsController = null;
        }

        [TestMethod]
        public async Task GeneralLedgerTransactionsController_Create2Async_Valid()
        {
            _glServiceMock.Setup(x => x.Create2Async(It.IsAny<GeneralLedgerTransaction2>())).ReturnsAsync(_generalLedgerTransaction);

            var generalLedgerTransaction = await _generalLedgerTransactionsController.Create2Async(_generalLedgerTransaction);
            Assert.IsNotNull(generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Create2Async_NullBody()
        {
            await _generalLedgerTransactionsController.Create2Async(null);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Create2Async_PermissionsException()
        {
            _glServiceMock.Setup(x => x.Create2Async(It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new PermissionsException());
            await _generalLedgerTransactionsController.Create2Async(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Create2Async_KeyNotFoundException()
        {
            _glServiceMock.Setup(x => x.Create2Async(It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new KeyNotFoundException());
            await _generalLedgerTransactionsController.Create2Async(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Create2Async_ArgumentNullException()
        {
            _glServiceMock.Setup(x => x.Create2Async(It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new ArgumentNullException());
            await _generalLedgerTransactionsController.Create2Async(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Create2Async_IntegrationApiException()
        {
            _glServiceMock.Setup(x => x.Create2Async(It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new IntegrationApiException());
            await _generalLedgerTransactionsController.Create2Async(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Creat2eAsync_ConfigurationException()
        {
            _glServiceMock.Setup(x => x.Create2Async(It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new ConfigurationException());
            await _generalLedgerTransactionsController.Create2Async(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Create2Async_ArgumentOutOfRangeException()
        {
            _glServiceMock.Setup(x => x.Create2Async(It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new ArgumentOutOfRangeException());
            await _generalLedgerTransactionsController.Create2Async(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Create2Async_ArgumentException()
        {
            _glServiceMock.Setup(x => x.Create2Async(It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new ArgumentException());
            await _generalLedgerTransactionsController.Create2Async(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Create2Async_InvalidOperationException()
        {
            _glServiceMock.Setup(x => x.Create2Async(It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new InvalidOperationException());
            await _generalLedgerTransactionsController.Create2Async(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Create2Async_FormatException()
        {
            _glServiceMock.Setup(x => x.Create2Async(It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new FormatException());
            await _generalLedgerTransactionsController.Create2Async(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Create2Async_RepositoryException()
        {
            _glServiceMock.Setup(x => x.Create2Async(It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new RepositoryException());
            await _generalLedgerTransactionsController.Create2Async(_generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Create2Async_Exception()
        {
            _glServiceMock.Setup(x => x.Create2Async(It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new Exception());
            await _generalLedgerTransactionsController.Create2Async(_generalLedgerTransaction);
        }

    }

    [TestClass]
    public class GeneralLedgerTransactionsControllerTestsUpdate
    {

        #region Test Context

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #endregion

        private Mock<IAdapterRegistry> _adapterRegistryMock;
        private Mock<ILogger> _loggerMock;
        private Mock<IGeneralLedgerTransactionService> _glServiceMock;

        private GeneralLedgerTransaction _generalLedgerTransaction;
        private List<GeneralLedgerTransaction> _generalLedgerTransactions;
        private HttpResponse _response;

        private GeneralLedgerTransactionsController _generalLedgerTransactionsController;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            _adapterRegistryMock = new Mock<IAdapterRegistry>();
            _loggerMock = new Mock<ILogger>();
            _glServiceMock = new Mock<IGeneralLedgerTransactionService>();

            var generalLedgerDetailDtoProperty = new GeneralLedgerDetailDtoProperty()
            {
                AccountingString = "784545",
                Description = "description",
                SequenceNumber = 1,
                Amount = new AmountDtoProperty() { Currency = CurrencyCodes.USD, Value = 25 }
            };

            var gltDtoProperty = new GeneralLedgerTransactionDtoProperty()
            {
                LedgerDate = DateTimeOffset.Now.DateTime,
                ReferenceNumber = "GL122312321",
                Reference = new GeneralLedgerReferenceDtoProperty()
                {
                    Organization = new GuidObject2("B17F7796-53D1-403C-A883-934D4DE04F1D"),
                    Person = new GuidObject2("C17F7796-53D1-403C-A883-934D4DE04F1D")
                },
                TransactionNumber = "1",
                TransactionTypeReferenceDate = DateTimeOffset.Now,
                Type = GeneralLedgerTransactionType.ActualOpenBalance,
                TransactionDetailLines = new List<GeneralLedgerDetailDtoProperty>() { generalLedgerDetailDtoProperty }
            };

            _generalLedgerTransaction = new GeneralLedgerTransaction
            {
                Id = "0001234",
                ProcessMode = ProcessMode.Update,
                Transactions = new List<GeneralLedgerTransactionDtoProperty>() { gltDtoProperty }
            };

            _generalLedgerTransactions = new List<GeneralLedgerTransaction>() { _generalLedgerTransaction };

            _response = new HttpResponse(new StringWriter());
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://doesntMatter.com", ""), _response);

            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ReturnsAsync(_generalLedgerTransaction);
            _generalLedgerTransactionsController = new GeneralLedgerTransactionsController(_glServiceMock.Object,
                _loggerMock.Object);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _adapterRegistryMock = null;
            _loggerMock = null;
            _glServiceMock = null;
            _generalLedgerTransactionsController = null;
        }

        [TestMethod]
        public async Task GeneralLedgerTransactionsController_UpdateAsync_Valid()
        {
            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ReturnsAsync(_generalLedgerTransaction);
           
            var generalLedgerTransaction = await _generalLedgerTransactionsController.UpdateAsync(_generalLedgerTransaction.Id, _generalLedgerTransaction);
            Assert.IsNotNull(generalLedgerTransaction);
        }

        [TestMethod]
        public async Task GeneralLedgerTransactionsController_UpdateAsync_Valid_NoGUD()
        {
            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ReturnsAsync(_generalLedgerTransaction);
            var id = _generalLedgerTransaction.Id;
            _generalLedgerTransaction.Id = null;
            var generalLedgerTransaction = await _generalLedgerTransactionsController.UpdateAsync(id, _generalLedgerTransaction);
            Assert.IsNotNull(generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_UpdateAsync_EmptyId()
        {
            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ReturnsAsync(_generalLedgerTransaction);

            var generalLedgerTransaction = await _generalLedgerTransactionsController.UpdateAsync("", _generalLedgerTransaction);
            Assert.IsNotNull(generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_UpdateAsync_NullId()
        {
            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ReturnsAsync(_generalLedgerTransaction);

            var generalLedgerTransaction = await _generalLedgerTransactionsController.UpdateAsync(null, _generalLedgerTransaction);
            Assert.IsNotNull(generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_UpdateAsync_NullBody()
        {
            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ReturnsAsync(_generalLedgerTransaction);

            var generalLedgerTransaction = await _generalLedgerTransactionsController.UpdateAsync(_generalLedgerTransaction.Id, null);
            Assert.IsNotNull(generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_UpdateAsync_PermissionsException()
        {
            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new PermissionsException());
            await _generalLedgerTransactionsController.UpdateAsync(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_UpdateAsync_KeyNotFoundException()
        {
            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new KeyNotFoundException());
            await _generalLedgerTransactionsController.UpdateAsync(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_UpdateAsync_ArgumentNullException()
        {
            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new ArgumentNullException());
            await _generalLedgerTransactionsController.UpdateAsync(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_UpdateAsync_IntegrationApiException()
        {
            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new IntegrationApiException());
            await _generalLedgerTransactionsController.UpdateAsync(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_UpdateAsync_ConfigurationException()
        {
            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new ConfigurationException());
            await _generalLedgerTransactionsController.UpdateAsync(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_UpdateAsync_ArgumentOutOfRangeException()
        {
            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new ArgumentOutOfRangeException());
            await _generalLedgerTransactionsController.UpdateAsync(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_UpdateAsync_ArgumentException()
        {
            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new ArgumentException());
            await _generalLedgerTransactionsController.UpdateAsync(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_UpdateAsync_InvalidOperationException()
        {
            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new InvalidOperationException());
            await _generalLedgerTransactionsController.UpdateAsync(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_UpdateAsync_FormatException()
        {
            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new FormatException());
            await _generalLedgerTransactionsController.UpdateAsync(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_UpdateAsync_RepositoryException()
        {
            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new RepositoryException());
            await _generalLedgerTransactionsController.UpdateAsync(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_UpdateAsync_Exception()
        {
            _glServiceMock.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>())).ThrowsAsync(new Exception());
            await _generalLedgerTransactionsController.UpdateAsync(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

    }

    [TestClass]
    public class GeneralLedgerTransactionsControllerTestsUpdate2
    {

        #region Test Context

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #endregion

        private Mock<IAdapterRegistry> _adapterRegistryMock;
        private Mock<ILogger> _loggerMock;
        private Mock<IGeneralLedgerTransactionService> _glServiceMock;

        private GeneralLedgerTransaction2 _generalLedgerTransaction;
        private List<GeneralLedgerTransaction2> _generalLedgerTransactions;
        private HttpResponse _response;

        private GeneralLedgerTransactionsController _generalLedgerTransactionsController;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            _adapterRegistryMock = new Mock<IAdapterRegistry>();
            _loggerMock = new Mock<ILogger>();
            _glServiceMock = new Mock<IGeneralLedgerTransactionService>();

            var generalLedgerDetailDtoProperty = new GeneralLedgerDetailDtoProperty2()
            {
                AccountingString = "784545",
                Description = "description",
                SequenceNumber = 1,
                SubmittedBy = new GuidObject2() { Id = "123456" },
                Amount = new AmountDtoProperty() { Currency = CurrencyCodes.USD, Value = 25 }
            };

            var gltDtoProperty = new GeneralLedgerTransactionDtoProperty2()
            {
                LedgerDate = DateTimeOffset.Now.DateTime,
                ReferenceNumber = "GL122312321",
                Reference = new GeneralLedgerReferenceDtoProperty()
                {
                    Organization = new GuidObject2("B17F7796-53D1-403C-A883-934D4DE04F1D"),
                    Person = new GuidObject2("C17F7796-53D1-403C-A883-934D4DE04F1D")
                },
                TransactionNumber = "1",
                TransactionTypeReferenceDate = DateTimeOffset.Now,
                Type = GeneralLedgerTransactionType.ActualOpenBalance,
                TransactionDetailLines = new List<GeneralLedgerDetailDtoProperty2>() { generalLedgerDetailDtoProperty }
            };

            _generalLedgerTransaction = new GeneralLedgerTransaction2
            {
                Id = "0001234",
                ProcessMode = ProcessMode.Update,
                SubmittedBy = new GuidObject2() { Id = "123456" },
                Transactions = new List<GeneralLedgerTransactionDtoProperty2>() { gltDtoProperty }
            };

            _generalLedgerTransactions = new List<GeneralLedgerTransaction2>() { _generalLedgerTransaction };

            _response = new HttpResponse(new StringWriter());
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://doesntMatter.com", ""), _response);

            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ReturnsAsync(_generalLedgerTransaction);
            _generalLedgerTransactionsController = new GeneralLedgerTransactionsController(_glServiceMock.Object,
                _loggerMock.Object);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _adapterRegistryMock = null;
            _loggerMock = null;
            _glServiceMock = null;
            _generalLedgerTransactionsController = null;
        }

        [TestMethod]
        public async Task GeneralLedgerTransactionsController_Update2Async_Valid()
        {
            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ReturnsAsync(_generalLedgerTransaction);

            var generalLedgerTransaction = await _generalLedgerTransactionsController.Update2Async(_generalLedgerTransaction.Id, _generalLedgerTransaction);
            Assert.IsNotNull(generalLedgerTransaction);
        }

        [TestMethod]
        public async Task GeneralLedgerTransactionsController_Update2Async_Valid_NoGUD()
        {
            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ReturnsAsync(_generalLedgerTransaction);
            var id = _generalLedgerTransaction.Id;
            _generalLedgerTransaction.Id = null;
            var generalLedgerTransaction = await _generalLedgerTransactionsController.Update2Async(id, _generalLedgerTransaction);
            Assert.IsNotNull(generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Update2Async_EmptyId()
        {
            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ReturnsAsync(_generalLedgerTransaction);

            var generalLedgerTransaction = await _generalLedgerTransactionsController.Update2Async("", _generalLedgerTransaction);
            Assert.IsNotNull(generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Update2Async_NullId()
        {
            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ReturnsAsync(_generalLedgerTransaction);

            var generalLedgerTransaction = await _generalLedgerTransactionsController.Update2Async(null, _generalLedgerTransaction);
            Assert.IsNotNull(generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Update2Async_NullBody()
        {
            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ReturnsAsync(_generalLedgerTransaction);

            var generalLedgerTransaction = await _generalLedgerTransactionsController.Update2Async(_generalLedgerTransaction.Id, null);
            Assert.IsNotNull(generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Update2Async_PermissionsException()
        {
            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new PermissionsException());
            await _generalLedgerTransactionsController.Update2Async(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Update2Async_KeyNotFoundException()
        {
            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new KeyNotFoundException());
            await _generalLedgerTransactionsController.Update2Async(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Update2Async_ArgumentNullException()
        {
            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new ArgumentNullException());
            await _generalLedgerTransactionsController.Update2Async(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Update2Async_IntegrationApiException()
        {
            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new IntegrationApiException());
            await _generalLedgerTransactionsController.Update2Async(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Update2Async_ConfigurationException()
        {
            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new ConfigurationException());
            await _generalLedgerTransactionsController.Update2Async(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Update2Async_ArgumentOutOfRangeException()
        {
            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new ArgumentOutOfRangeException());
            await _generalLedgerTransactionsController.Update2Async(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Update2Async_ArgumentException()
        {
            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new ArgumentException());
            await _generalLedgerTransactionsController.Update2Async(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Update2Async_InvalidOperationException()
        {
            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new InvalidOperationException());
            await _generalLedgerTransactionsController.Update2Async(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Update2Async_FormatException()
        {
            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new FormatException());
            await _generalLedgerTransactionsController.Update2Async(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Update2Async_RepositoryException()
        {
            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new RepositoryException());
            await _generalLedgerTransactionsController.Update2Async(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_Update2Async_Exception()
        {
            _glServiceMock.Setup(x => x.Update2Async(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction2>())).ThrowsAsync(new Exception());
            await _generalLedgerTransactionsController.Update2Async(_generalLedgerTransaction.Id, _generalLedgerTransaction);
        }

    }

    [TestClass]
    public class GeneralLedgerTransactionsControllerTestsDelete
    {

        #region Test Context

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext { get; set; }

        #endregion

        private Mock<IAdapterRegistry> _adapterRegistryMock;
        private Mock<ILogger> _loggerMock;
        private Mock<IGeneralLedgerTransactionService> _glServiceMock;

        private GeneralLedgerTransaction _generalLedgerTransaction;
        private List<GeneralLedgerTransaction> _generalLedgerTransactions;
        private HttpResponse _response;

        private GeneralLedgerTransactionsController _generalLedgerTransactionsController;

        [TestInitialize]
        public void Initialize()
        {
            EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

            _adapterRegistryMock = new Mock<IAdapterRegistry>();
            _loggerMock = new Mock<ILogger>();
            _glServiceMock = new Mock<IGeneralLedgerTransactionService>();

            var generalLedgerDetailDtoProperty = new GeneralLedgerDetailDtoProperty()
            {
                AccountingString = "784545",
                Description = "description",
                SequenceNumber = 1,
                Amount = new AmountDtoProperty() { Currency = CurrencyCodes.USD, Value = 25 }
            };

            var gltDtoProperty = new GeneralLedgerTransactionDtoProperty()
            {
                LedgerDate = DateTimeOffset.Now.DateTime,
                ReferenceNumber = "GL122312321",
                Reference = new GeneralLedgerReferenceDtoProperty()
                {
                    Organization = new GuidObject2("B17F7796-53D1-403C-A883-934D4DE04F1D"),
                    Person = new GuidObject2("C17F7796-53D1-403C-A883-934D4DE04F1D")
                },
                TransactionNumber = "1",
                TransactionTypeReferenceDate = DateTimeOffset.Now,
                Type = GeneralLedgerTransactionType.ActualOpenBalance,
                TransactionDetailLines = new List<GeneralLedgerDetailDtoProperty>() { generalLedgerDetailDtoProperty }
            };

            _generalLedgerTransaction = new GeneralLedgerTransaction
            {
                Id = "0001234",
                ProcessMode = ProcessMode.Update,
                Transactions = new List<GeneralLedgerTransactionDtoProperty>() { gltDtoProperty }
            };

            _generalLedgerTransactions = new List<GeneralLedgerTransaction>() { _generalLedgerTransaction };

            _response = new HttpResponse(new StringWriter());
            HttpContext.Current = new HttpContext(new HttpRequest("", "http://doesntMatter.com", ""), _response);

            _glServiceMock.Setup(x => x.DeleteAsync(It.IsAny<string>())).Verifiable();
            _generalLedgerTransactionsController = new GeneralLedgerTransactionsController(_glServiceMock.Object,
                _loggerMock.Object);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _adapterRegistryMock = null;
            _loggerMock = null;
            _glServiceMock = null;
            _generalLedgerTransactionsController = null;
        }

        [TestMethod]
        [ExpectedException(typeof (HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_DeleteAsync_EmptyId()
        {
            await _generalLedgerTransactionsController.DeleteAsync("");
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_DeleteAsync_NullId()
        {
            await _generalLedgerTransactionsController.DeleteAsync(null);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_DeleteAsync_PermissionsException()
        {
            _glServiceMock.Setup(x => x.DeleteAsync(It.IsAny<string>())).Throws(new PermissionsException());
            await _generalLedgerTransactionsController.DeleteAsync(_generalLedgerTransaction.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_DeleteAsync_KeyNotFoundException()
        {
             _glServiceMock.Setup(x => x.DeleteAsync(It.IsAny<string>())).Throws(new KeyNotFoundException());
            await _generalLedgerTransactionsController.DeleteAsync(_generalLedgerTransaction.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_DeleteAsync_ArgumentNullException()
        {
            _glServiceMock.Setup(x => x.DeleteAsync(It.IsAny<string>())).Throws(new ArgumentNullException());
            await _generalLedgerTransactionsController.DeleteAsync(_generalLedgerTransaction.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_DeleteAsync_IntegrationApiException()
        {
             _glServiceMock.Setup(x => x.DeleteAsync(It.IsAny<string>())).Throws(new IntegrationApiException());
             await _generalLedgerTransactionsController.DeleteAsync(_generalLedgerTransaction.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_DeleteAsync_ConfigurationException()
        {
             _glServiceMock.Setup(x => x.DeleteAsync(It.IsAny<string>())).Throws(new ConfigurationException());
             await _generalLedgerTransactionsController.DeleteAsync(_generalLedgerTransaction.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_DeleteAsync_ArgumentOutOfRangeException()
        {
             _glServiceMock.Setup(x => x.DeleteAsync(It.IsAny<string>())).Throws(new ArgumentOutOfRangeException());
             await _generalLedgerTransactionsController.DeleteAsync(_generalLedgerTransaction.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_DeleteAsync_ArgumentException()
        {
             _glServiceMock.Setup(x => x.DeleteAsync(It.IsAny<string>())).Throws(new ArgumentException());
             await _generalLedgerTransactionsController.DeleteAsync(_generalLedgerTransaction.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_DeleteAsync_InvalidOperationException()
        {
             _glServiceMock.Setup(x => x.DeleteAsync(It.IsAny<string>())).Throws(new InvalidOperationException());
             await _generalLedgerTransactionsController.DeleteAsync(_generalLedgerTransaction.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_DeleteAsync_FormatException()
        {
             _glServiceMock.Setup(x => x.DeleteAsync(It.IsAny<string>())).Throws(new FormatException());
             await _generalLedgerTransactionsController.DeleteAsync(_generalLedgerTransaction.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_DeleteAsync_RepositoryException()
        {
            _glServiceMock.Setup(x => x.DeleteAsync(It.IsAny<string>())).Throws(new RepositoryException());
            await _generalLedgerTransactionsController.DeleteAsync(_generalLedgerTransaction.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(HttpResponseException))]
        public async Task GeneralLedgerTransactionsController_DeleteAsync_Exception()
        {
             _glServiceMock.Setup(x => x.DeleteAsync(It.IsAny<string>())).Throws(new Exception());
             await _generalLedgerTransactionsController.DeleteAsync(_generalLedgerTransaction.Id);
        }

    }
}