﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Ellucian.Colleague.Api.Controllers;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Planning.Services;
using Ellucian.Colleague.Domain.Planning.Tests;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Web.Http.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Ellucian.Colleague.Api.Tests.Controllers.Planning
{
    [TestClass]
    public class DegreePlansControllerTests
    {
        [TestClass]
        public class DegreePlanController_Get
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings = null;

            DegreePlan degreePlan2;
            DegreePlan degreePlan4;

            [TestInitialize]
            public async void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;
                var testDegreePlanRepo = new TestDegreePlanRepository();
                var testTermRepo = new TestTermRepository();
                var testStudentProgramRepo = new TestStudentProgramRepository();

                degreePlan2 = BuildDegreePlanDto(await testDegreePlanRepo.GetAsync(2));
                degreePlanServiceMock.Setup(repo => repo.GetDegreePlanAsync(2)).Returns(Task.FromResult(degreePlan2));

                degreePlan4 = BuildDegreePlanDto(await testDegreePlanRepo.GetAsync(4));
                degreePlanServiceMock.Setup(repo => repo.GetDegreePlanAsync(4)).Returns(Task.FromResult(degreePlan4));

                degreePlanServiceMock.Setup(repo => repo.GetDegreePlanAsync(99)).Throws(new ArgumentException());

                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

            [TestMethod]
            public async Task DegreePlanController_GetAsync_Success()
            {
                var degreePlan = await degreePlanController.GetAsync(2);
                Assert.AreEqual(degreePlan2, degreePlan);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_GetAsync_PlanDoesNotExist()
            {
                var degreePlan = await degreePlanController.GetAsync(99);
            }

            private DegreePlan BuildDegreePlanDto(Ellucian.Colleague.Domain.Planning.Entities.DegreePlan degreePlan)
            {
                var degreePlanDto = new DegreePlan();
                degreePlanDto.Id = degreePlan.Id;
                degreePlanDto.PersonId = degreePlan.PersonId;
                return degreePlanDto;
            }
        }

         [TestClass]
        public class DegreePlanPreview_Get
        {

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;
                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

              [TestMethod]
              public async Task DegreePlanController_GetSamplePlanPreview5Async()
              {
                   // Arrange
                   int id = 12345;
                   string programCode = "ENGL+BA";
                   string term = "2015FA";
                   DegreePlanPreview5 planPreview = new DegreePlanPreview5()
                   {
                        Preview = new DegreePlan4(),
                        MergedDegreePlan = new DegreePlan4(),
                        AcademicHistory = new Dtos.Student.AcademicHistory3()
                   };
                   
                   // Mock the degree plan service that updates a plan.
                   degreePlanServiceMock.Setup(svc => svc.PreviewSampleDegreePlan5Async(id, programCode, term)).Returns(Task.FromResult(planPreview));

                   // Act
                   var response = await degreePlanController.GetSamplePlanPreview5Async(id, programCode, term);

                   // Assert
                   Assert.IsInstanceOfType(response, typeof(DegreePlanPreview5));
              }
        }

        [TestClass]
        public class DegreePlanController_Get2
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings;

            DegreePlan2 degreePlan2;
            DegreePlan2 degreePlan4;

            [TestInitialize]
            public async void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;
                var testDegreePlanRepo = new TestDegreePlanRepository();
                var testTermRepo = new TestTermRepository();
                var testStudentProgramRepo = new TestStudentProgramRepository();

                degreePlan2 = BuildDegreePlanDto(await testDegreePlanRepo.GetAsync(2));
                degreePlanServiceMock.Setup(repo => repo.GetDegreePlan2Async(2)).Returns(Task.FromResult(degreePlan2));

                degreePlan4 = BuildDegreePlanDto(await testDegreePlanRepo.GetAsync(4));
                degreePlanServiceMock.Setup(repo => repo.GetDegreePlan2Async(4)).Returns(Task.FromResult(degreePlan4));

                degreePlanServiceMock.Setup(repo => repo.GetDegreePlan2Async(99)).Throws(new ArgumentException());

                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

            [TestMethod]
            public async Task DegreePlanController_Get2Async_Success()
            {
                var degreePlan = await degreePlanController.Get2Async(2);
                Assert.AreEqual(degreePlan2, degreePlan);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_Get2Async_PlanDoesNotExist()
            {
                var degreePlan = await degreePlanController.Get2Async(99);
            }

            private DegreePlan2 BuildDegreePlanDto(Ellucian.Colleague.Domain.Planning.Entities.DegreePlan degreePlan)
            {
                var degreePlanDto = new DegreePlan2();
                degreePlanDto.Id = degreePlan.Id;
                degreePlanDto.PersonId = degreePlan.PersonId;
                return degreePlanDto;
            }
        }

        [TestClass]
        public class DegreePlanController_Get3
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings;

            DegreePlan3 degreePlan2;
            DegreePlan3 degreePlan4;

            [TestInitialize]
            public async void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;
                var testDegreePlanRepo = new TestDegreePlanRepository();
                var testTermRepo = new TestTermRepository();
                var testStudentProgramRepo = new TestStudentProgramRepository();

                degreePlan2 = BuildDegreePlanDto(await testDegreePlanRepo.GetAsync(2));
                degreePlanServiceMock.Setup(repo => repo.GetDegreePlan3Async(2)).Returns(Task.FromResult(degreePlan2));

                degreePlan4 = BuildDegreePlanDto(await testDegreePlanRepo.GetAsync(4));
                degreePlanServiceMock.Setup(repo => repo.GetDegreePlan3Async(4)).Returns(Task.FromResult(degreePlan4));

                degreePlanServiceMock.Setup(repo => repo.GetDegreePlan3Async(99)).Throws(new ArgumentException());

                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

            [TestMethod]
            public async Task DegreePlanController_Get3Async_Success()
            {
                var degreePlan = await degreePlanController.Get3Async(2);
                Assert.AreEqual(degreePlan2, degreePlan);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_Get3Async_PlanDoesNotExist()
            {
                var degreePlan = await degreePlanController.Get3Async(99);
            }

            private DegreePlan3 BuildDegreePlanDto(Ellucian.Colleague.Domain.Planning.Entities.DegreePlan degreePlan)
            {
                var degreePlanDto = new DegreePlan3();
                degreePlanDto.Id = degreePlan.Id;
                degreePlanDto.PersonId = degreePlan.PersonId;
                return degreePlanDto;
            }
        }

        [TestClass]
        public class DegreePlanController_Get4
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;
                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

            [TestMethod]
            public async Task DegreePlanController_Get4Async_Success()
            {
                var degreePlan = new DegreePlan4();
                degreePlan.Id = 5;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm4>();
                var degreePlanTerm = new DegreePlanTerm4();
                degreePlanTerm.TermId = "2012/FA";
                degreePlanTerm.PlannedCourses = new List<PlannedCourse4>() {
                    new PlannedCourse4() { CourseId = "5" },
                    new PlannedCourse4() { CourseId = "10" },
                    new PlannedCourse4() { CourseId = "15" }
                };
                degreePlan.Terms.Add(degreePlanTerm);
                var returnDto = new DegreePlanAcademicHistory()
                {
                    DegreePlan = degreePlan,
                    AcademicHistory = new Dtos.Student.AcademicHistory2() { StudentId = degreePlan.PersonId, AcademicTerms = new List<Dtos.Student.AcademicTerm2>() }
                };

                // Mock the degree plan service that updates a plan.
                degreePlanServiceMock.Setup(svc => svc.GetDegreePlan4Async(degreePlan.Id, true)).Returns(Task.FromResult(returnDto));

                var result = await degreePlanController.Get4Async(degreePlan.Id);
                Assert.IsTrue(result is DegreePlanAcademicHistory);
                Assert.AreEqual(degreePlan.Id, result.DegreePlan.Id);
                Assert.AreEqual(degreePlan.PersonId, result.AcademicHistory.StudentId);
            }

            [TestMethod]
            public async Task DegreePlanController_GetUnvalidated_Success()
            {
                var degreePlan = new DegreePlan4();
                degreePlan.Id = 5;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm4>();
                var degreePlanTerm = new DegreePlanTerm4();
                degreePlanTerm.TermId = "2012/FA";
                degreePlanTerm.PlannedCourses = new List<PlannedCourse4>() {
                    new PlannedCourse4() { CourseId = "5" },
                    new PlannedCourse4() { CourseId = "10" },
                    new PlannedCourse4() { CourseId = "15" }
                };
                degreePlan.Terms.Add(degreePlanTerm);
                var returnDto = new DegreePlanAcademicHistory()
                {
                    DegreePlan = degreePlan,
                    AcademicHistory = new Dtos.Student.AcademicHistory2() { StudentId = degreePlan.PersonId, AcademicTerms = new List<Dtos.Student.AcademicTerm2>() }
                };

                // Mock the degree plan service that updates a plan.
                degreePlanServiceMock.Setup(svc => svc.GetDegreePlan4Async(degreePlan.Id, false)).Returns(Task.FromResult(returnDto));

                var result = await degreePlanController.Get4Async(degreePlan.Id, false);
                Assert.IsTrue(result is DegreePlanAcademicHistory);
                Assert.AreEqual(degreePlan.Id, result.DegreePlan.Id);
                Assert.AreEqual(degreePlan.PersonId, result.AcademicHistory.StudentId);
            }
        }

        [TestClass]
        public class DegreePlanController_Get5Async
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;
                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

            [TestMethod]
            public async Task DegreePlanController_Get5Async_Success()
            {
                var degreePlan = new DegreePlan4();
                degreePlan.Id = 5;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm4>();
                var degreePlanTerm = new DegreePlanTerm4();
                degreePlanTerm.TermId = "2012/FA";
                degreePlanTerm.PlannedCourses = new List<PlannedCourse4>() {
                    new PlannedCourse4() { CourseId = "5" },
                    new PlannedCourse4() { CourseId = "10" },
                    new PlannedCourse4() { CourseId = "15" }
                };
                degreePlan.Terms.Add(degreePlanTerm);
                var returnDto = new DegreePlanAcademicHistory2()
                {
                    DegreePlan = degreePlan,
                    AcademicHistory = new Dtos.Student.AcademicHistory3() { StudentId = degreePlan.PersonId, AcademicTerms = new List<Dtos.Student.AcademicTerm3>() }
                };

                // Mock the degree plan service that updates a plan.
                degreePlanServiceMock.Setup(svc => svc.GetDegreePlan5Async(degreePlan.Id, true)).Returns(Task.FromResult(returnDto));

                var result = await degreePlanController.Get5Async(degreePlan.Id);
                Assert.IsTrue(result is DegreePlanAcademicHistory2);
                Assert.AreEqual(degreePlan.Id, result.DegreePlan.Id);
                Assert.AreEqual(degreePlan.PersonId, result.AcademicHistory.StudentId);
            }

            [TestMethod]
            public async Task DegreePlanController_GetUnvalidated_Success()
            {
                var degreePlan = new DegreePlan4();
                degreePlan.Id = 5;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm4>();
                var degreePlanTerm = new DegreePlanTerm4();
                degreePlanTerm.TermId = "2012/FA";
                degreePlanTerm.PlannedCourses = new List<PlannedCourse4>() {
                    new PlannedCourse4() { CourseId = "5" },
                    new PlannedCourse4() { CourseId = "10" },
                    new PlannedCourse4() { CourseId = "15" }
                };
                degreePlan.Terms.Add(degreePlanTerm);
                var returnDto = new DegreePlanAcademicHistory2()
                {
                    DegreePlan = degreePlan,
                    AcademicHistory = new Dtos.Student.AcademicHistory3() { StudentId = degreePlan.PersonId, AcademicTerms = new List<Dtos.Student.AcademicTerm3>() }
                };

                // Mock the degree plan service that updates a plan.
                degreePlanServiceMock.Setup(svc => svc.GetDegreePlan5Async(degreePlan.Id, false)).Returns(Task.FromResult(returnDto));

                var result = await degreePlanController.Get5Async(degreePlan.Id, false);
                Assert.IsTrue(result is DegreePlanAcademicHistory2);
                Assert.AreEqual(degreePlan.Id, result.DegreePlan.Id);
                Assert.AreEqual(degreePlan.PersonId, result.AcademicHistory.StudentId);
            }
        }

        [TestClass]
        public class DegreePlanController_Put
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;
                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

            [TestMethod]
            public async Task DegreePlanController_PutAsync_Success()
            {
                var degreePlan = new DegreePlan();
                degreePlan.Id = 5;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm>();
                var degreePlanTerm = new DegreePlanTerm();
                degreePlanTerm.TermId = "2012/FA";
                degreePlanTerm.PlannedCourses = new List<PlannedCourse>() {
                    new PlannedCourse() { CourseId = "5" },
                    new PlannedCourse() { CourseId = "10" },
                    new PlannedCourse() { CourseId = "15" }
                };
                degreePlan.Terms.Add(degreePlanTerm);

                // Mock the degree plan service that updates a plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlanAsync(It.Is<DegreePlan>(r => r.Id == 5))).Returns(Task.FromResult(degreePlan));

                var updatedDegreePlan = await degreePlanController.PutAsync(degreePlan);
                Assert.AreEqual(updatedDegreePlan.Id, degreePlan.Id);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_PutAsync_VersionMismatch()
            {
                // Mock the UpdatreDegreePlan when the service fails to return a valid degree plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlanAsync(It.Is<DegreePlan>(r => r.Id == 10))).Throws(new InvalidOperationException());

                DegreePlan degreePlan = new DegreePlan();
                degreePlan.Id = 10;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm>();

                var updatedDegreePlan = await degreePlanController.PutAsync(degreePlan);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_PutAsync_Failure()
            {
                // Mock the UpdatreDegreePlan when the service fails to return a valid degree plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlanAsync(It.Is<DegreePlan>(r => r.Id == 99))).Throws(new ArgumentException());

                DegreePlan degreePlan = new DegreePlan();
                degreePlan.Id = 99;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm>();

                var updatedDegreePlan = await degreePlanController.PutAsync(degreePlan);
            }
        }

        [TestClass]
        public class DegreePlanController_Put2
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;
                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

            [TestMethod]
            public async Task DegreePlanController_Put2Async_Success()
            {
                var degreePlan = new DegreePlan2();
                degreePlan.Id = 5;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm2>();
                var degreePlanTerm = new DegreePlanTerm2();
                degreePlanTerm.TermId = "2012/FA";
                degreePlanTerm.PlannedCourses = new List<PlannedCourse2>() {
                    new PlannedCourse2() { CourseId = "5" },
                    new PlannedCourse2() { CourseId = "10" },
                    new PlannedCourse2() { CourseId = "15" }
                };
                degreePlan.Terms.Add(degreePlanTerm);

                // Mock the degree plan service that updates a plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlan2Async(It.Is<DegreePlan2>(r => r.Id == 5))).Returns(Task.FromResult(degreePlan));

                var updatedDegreePlan = await degreePlanController.Put2Async(degreePlan);
                Assert.AreEqual(updatedDegreePlan.Id, degreePlan.Id);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_Put2Async_VersionMismatch()
            {
                // Mock the UpdatreDegreePlan when the service fails to return a valid degree plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlan2Async(It.Is<DegreePlan2>(r => r.Id == 10))).Throws(new InvalidOperationException());

                var degreePlan = new DegreePlan2();
                degreePlan.Id = 10;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm2>();

                var updatedDegreePlan = await degreePlanController.Put2Async(degreePlan);                
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_Put2Async_Failure()
            {
                // Mock the UpdatreDegreePlan when the service fails to return a valid degree plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlan2Async(It.Is<DegreePlan2>(r => r.Id == 99))).Throws(new ArgumentException());

                var degreePlan = new DegreePlan2();
                degreePlan.Id = 99;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm2>();

                var updatedDegreePlan = await degreePlanController.Put2Async(degreePlan);
            }
        }

        [TestClass]
        public class DegreePlanController_Put3
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;
                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

            [TestMethod]
            public async Task DegreePlanController_Put3Async_Success()
            {
                var degreePlan = new DegreePlan3();
                degreePlan.Id = 5;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm3>();
                var degreePlanTerm = new DegreePlanTerm3();
                degreePlanTerm.TermId = "2012/FA";
                degreePlanTerm.PlannedCourses = new List<PlannedCourse3>() {
                    new PlannedCourse3() { CourseId = "5" },
                    new PlannedCourse3() { CourseId = "10" },
                    new PlannedCourse3() { CourseId = "15" }
                };
                degreePlan.Terms.Add(degreePlanTerm);

                // Mock the degree plan service that updates a plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlan3Async(It.Is<DegreePlan3>(r => r.Id == 5))).Returns(Task.FromResult(degreePlan));

                var updatedDegreePlan = await degreePlanController.Put3Async(degreePlan);
                Assert.AreEqual(updatedDegreePlan.Id, degreePlan.Id);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_Put3Async_VersionMismatch()
            {
                // Mock the UpdatreDegreePlan when the service fails to return a valid degree plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlan3Async(It.Is<DegreePlan3>(r => r.Id == 10))).Throws(new InvalidOperationException());

                var degreePlan = new DegreePlan3();
                degreePlan.Id = 10;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm3>();

                var updatedDegreePlan = await degreePlanController.Put3Async(degreePlan);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_Put3Async_Failure()
            {
                // Mock the UpdatreDegreePlan when the service fails to return a valid degree plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlan3Async(It.Is<DegreePlan3>(r => r.Id == 99))).Throws(new ArgumentException());

                var degreePlan = new DegreePlan3();
                degreePlan.Id = 99;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm3>();

                var updatedDegreePlan = await degreePlanController.Put3Async(degreePlan);
            }
        }

        [TestClass]
        public class DegreePlanController_Put4
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;
                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

            [TestMethod]
            public async Task DegreePlanController_Put4Async_Success()
            {
                var degreePlan = new DegreePlan4();
                degreePlan.Id = 5;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm4>();
                var degreePlanTerm = new DegreePlanTerm4();
                degreePlanTerm.TermId = "2012/FA";
                degreePlanTerm.PlannedCourses = new List<PlannedCourse4>() {
                    new PlannedCourse4() { CourseId = "5" },
                    new PlannedCourse4() { CourseId = "10" },
                    new PlannedCourse4() { CourseId = "15" }
                };
                degreePlan.Terms.Add(degreePlanTerm);
                var returnDto = new DegreePlanAcademicHistory()
                {
                    DegreePlan = degreePlan,
                    AcademicHistory = new Dtos.Student.AcademicHistory2() { StudentId = degreePlan.PersonId, AcademicTerms = new List<Dtos.Student.AcademicTerm2>() }
                };

                // Mock the degree plan service that updates a plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlan4Async(It.Is<DegreePlan4>(r => r.Id == 5))).Returns(Task.FromResult(returnDto));

                var result = await degreePlanController.Put4Async(degreePlan);
                Assert.IsTrue(result is DegreePlanAcademicHistory);
                Assert.AreEqual(degreePlan.Id, result.DegreePlan.Id);
                Assert.AreEqual(degreePlan.PersonId, result.AcademicHistory.StudentId);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_Put4Async_VersionMismatch()
            {
                // Mock the UpdatreDegreePlan when the service fails to return a valid degree plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlan4Async(It.Is<DegreePlan4>(r => r.Id == 10))).Throws(new InvalidOperationException());

                var degreePlan = new DegreePlan4();
                degreePlan.Id = 10;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm4>();

                var updatedDegreePlan = await degreePlanController.Put4Async(degreePlan);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_Put4Async_Failure()
            {
                // Mock the UpdatreDegreePlan when the service fails to return a valid degree plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlan4Async(It.Is<DegreePlan4>(r => r.Id == 99))).Throws(new ArgumentException());

                var degreePlan = new DegreePlan4();
                degreePlan.Id = 99;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm4>();

                var updatedDegreePlan = await degreePlanController.Put4Async(degreePlan);
            }
        }

        [TestClass]
        public class DegreePlanController_Put5Async
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;
                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

            [TestMethod]
            public async Task DegreePlanController_Put5Async_Success()
            {
                var degreePlan = new DegreePlan4();
                degreePlan.Id = 5;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm4>();
                var degreePlanTerm = new DegreePlanTerm4();
                degreePlanTerm.TermId = "2012/FA";
                degreePlanTerm.PlannedCourses = new List<PlannedCourse4>() {
                    new PlannedCourse4() { CourseId = "5" },
                    new PlannedCourse4() { CourseId = "10" },
                    new PlannedCourse4() { CourseId = "15" }
                };
                degreePlan.Terms.Add(degreePlanTerm);
                var returnDto = new DegreePlanAcademicHistory2()
                {
                    DegreePlan = degreePlan,
                    AcademicHistory = new Dtos.Student.AcademicHistory3() { StudentId = degreePlan.PersonId, AcademicTerms = new List<Dtos.Student.AcademicTerm3>() }
                };

                // Mock the degree plan service that updates a plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlan5Async(It.Is<DegreePlan4>(r => r.Id == 5))).Returns(Task.FromResult(returnDto));

                var result = await degreePlanController.Put5Async(degreePlan);
                Assert.IsTrue(result is DegreePlanAcademicHistory2);
                Assert.AreEqual(degreePlan.Id, result.DegreePlan.Id);
                Assert.AreEqual(degreePlan.PersonId, result.AcademicHistory.StudentId);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_Put5Async_VersionMismatch()
            {
                // Mock the UpdatreDegreePlan when the service fails to return a valid degree plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlan5Async(It.Is<DegreePlan4>(r => r.Id == 10))).Throws(new InvalidOperationException());

                var degreePlan = new DegreePlan4();
                degreePlan.Id = 10;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm4>();

                var updatedDegreePlan = await degreePlanController.Put5Async(degreePlan);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_Put5Async_Failure()
            {
                // Mock the UpdatreDegreePlan when the service fails to return a valid degree plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlan5Async(It.Is<DegreePlan4>(r => r.Id == 99))).Throws(new ArgumentException());

                var degreePlan = new DegreePlan4();
                degreePlan.Id = 99;
                degreePlan.PersonId = "0004444";
                degreePlan.Version = 14;
                degreePlan.Terms = new List<DegreePlanTerm4>();

                var updatedDegreePlan = await degreePlanController.Put5Async(degreePlan);
            }
        }

        [TestClass]
        public class DegreePlanController_PutRegistration
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;

                // Mock the UpdatreDegreePlan when the service fails to return a valid degree plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlanAsync(It.Is<DegreePlan>(r => r.Id == 99))).Throws(new ArgumentException());

                // Mock the UpdatreDegreePlan when the service fails to return a valid degree plan.
                degreePlanServiceMock.Setup(repo => repo.UpdateDegreePlanAsync(It.Is<DegreePlan>(r => r.Id == 10))).Throws(new InvalidOperationException());

                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_InvalidDegreePlanId()
            {
                var messages = await degreePlanController.PutRegistrationAsync(0, "test");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_EmptyTermId()
            {
                var messages = await degreePlanController.PutRegistrationAsync(1, "");
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_NullTermId()
            {
                var messages = await degreePlanController.PutRegistrationAsync(1, null);
            }
        }

        [TestClass]
        public class DegreePlanController_PostArchive
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings;

            TestDegreePlanRepository testDegreePlanRepo = new TestDegreePlanRepository();

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;
                
                var testTermRepo = new TestTermRepository();
                var testStudentProgramRepo = new TestStudentProgramRepository();

                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

            [TestMethod]
            public async Task DegreePlanController_PostArchive_Success()
            {
                var degreePlan2 = BuildDegreePlanDto(await testDegreePlanRepo.GetAsync(2));
                var degreePlanArchive = BuildDegreePlanArchive(degreePlan2);
                degreePlanServiceMock.Setup(service => service.ArchiveDegreePlanAsync(degreePlan2)).Returns(Task.FromResult(degreePlanArchive));

                var newDegreePlanArchive = await degreePlanController.PostArchiveAsync(degreePlan2);
                Assert.AreEqual(degreePlanArchive, newDegreePlanArchive);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_PostArchiveAsync_NullDegreePlan()
            {
                var newDegreePlanArchive = await degreePlanController.PostArchiveAsync(null);
            }
            
            private DegreePlan2 BuildDegreePlanDto(Ellucian.Colleague.Domain.Planning.Entities.DegreePlan degreePlan)
            {
                var degreePlanDto = new DegreePlan2();
                degreePlanDto.Id = degreePlan.Id;
                degreePlanDto.PersonId = degreePlan.PersonId;
                return degreePlanDto;
            }

            private DegreePlanArchive BuildDegreePlanArchive(Ellucian.Colleague.Dtos.Planning.DegreePlan2 degreePlan)
            {
                var degreePlanArchive = new DegreePlanArchive();
                degreePlanArchive.CreatedDate = DateTime.Now;
                return degreePlanArchive;
            }
        }

        [TestClass]
        public class DegreePlanController_PostArchive2
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings;

            TestDegreePlanRepository testDegreePlanRepo = new TestDegreePlanRepository();

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;

                var testTermRepo = new TestTermRepository();
                var testStudentProgramRepo = new TestStudentProgramRepository();

                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

            [TestMethod]
            public async Task DegreePlanController_PostArchive2Async_Success()
            {
                var degreePlanEntity = await testDegreePlanRepo.GetAsync(2);
                var degreePlan = BuildDegreePlanDto(degreePlanEntity);
                var degreePlanArchive = BuildDegreePlanArchive(degreePlan);
                degreePlanServiceMock.Setup(service => service.ArchiveDegreePlan2Async(degreePlan)).Returns(Task.FromResult(degreePlanArchive));

                var newDegreePlanArchive = await degreePlanController.PostArchive2Async(degreePlan);
                Assert.AreEqual(degreePlanArchive, newDegreePlanArchive);
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_PostArchive2Async_NullDegreePlan()
            {
                var newDegreePlanArchive = await degreePlanController.PostArchive2Async(null);
            }

            private DegreePlan3 BuildDegreePlanDto(Ellucian.Colleague.Domain.Planning.Entities.DegreePlan degreePlan)
            {
                var degreePlanDto = new DegreePlan3();
                degreePlanDto.Id = degreePlan.Id;
                degreePlanDto.PersonId = degreePlan.PersonId;
                return degreePlanDto;
            }

            private DegreePlanArchive2 BuildDegreePlanArchive(Ellucian.Colleague.Dtos.Planning.DegreePlan3 degreePlan)
            {
                var degreePlanArchive = new DegreePlanArchive2();
                degreePlanArchive.CreatedDate = DateTime.Now;
                return degreePlanArchive;
            }
        }

        [TestClass]
        public class DegreePlanController_GetDegreePlanArchives
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings;

            TestDegreePlanArchiveRepository testDegreePlanArchiveRepo = new TestDegreePlanArchiveRepository();

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;

                var testTermRepo = new TestTermRepository();
                var testStudentProgramRepo = new TestStudentProgramRepository();

                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

            [TestMethod]
            public async Task DegreePlanController_GetDegreePlanArchivesAsync_Success()
            {
                var degreePlanArchiveEntities = await testDegreePlanArchiveRepo.GetDegreePlanArchivesAsync(2);
                var degreePlanArchiveDtos = BuildDegreePlanArchiveDtos(degreePlanArchiveEntities);
                degreePlanServiceMock.Setup(service => service.GetDegreePlanArchivesAsync(2)).Returns(Task.FromResult(degreePlanArchiveDtos));

                var archives = await degreePlanController.GetDegreePlanArchivesAsync(2);
                Assert.AreEqual(degreePlanArchiveEntities.Count(), archives.Count());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_GetDegreePlanArchivesAsync_ZeroDegreePlanId()
            {
                var degreePlanArchives = await degreePlanController.GetDegreePlanArchivesAsync(0);
            }

            private IEnumerable<DegreePlanArchive> BuildDegreePlanArchiveDtos(IEnumerable<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanArchive> degreePlanArchiveEntities)
            {
                List<DegreePlanArchive> archives = new List<DegreePlanArchive>();
                foreach (var archive in degreePlanArchiveEntities)
                {
                    var degreePlanArchiveDto = new DegreePlanArchive();
                    degreePlanArchiveDto.Id = archive.Id;
                    degreePlanArchiveDto.CreatedBy = archive.CreatedBy;
                    degreePlanArchiveDto.CreatedDate = (archive.CreatedDate.GetValueOrDefault(DateTime.Now)).DateTime;
                    degreePlanArchiveDto.DegreePlanId = archive.DegreePlanId;
                    degreePlanArchiveDto.StudentId = archive.StudentId;
                    degreePlanArchiveDto.ReviewedBy = archive.ReviewedBy;
                    degreePlanArchiveDto.ReviewedDate = archive.ReviewedDate.HasValue ? archive.ReviewedDate.Value.DateTime : (DateTime?)null;
                    archives.Add(degreePlanArchiveDto);
                }
                return archives;
            }
        }

        [TestClass]
        public class DegreePlanController_GetDegreePlanArchives2
        {
            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings;

            TestDegreePlanArchiveRepository testDegreePlanArchiveRepo = new TestDegreePlanArchiveRepository();

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;

                var testTermRepo = new TestTermRepository();
                var testStudentProgramRepo = new TestStudentProgramRepository();

                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

            [TestMethod]
            public async Task DegreePlanController_GetDegreePlanArchives2Async_Success()
            {
                var degreePlanArchiveEntities = await testDegreePlanArchiveRepo.GetDegreePlanArchivesAsync(2);
                var degreePlanArchiveDtos = BuildDegreePlanArchiveDtos(degreePlanArchiveEntities);
                degreePlanServiceMock.Setup(service => service.GetDegreePlanArchives2Async(2)).Returns(Task.FromResult(degreePlanArchiveDtos));

                var archives = await degreePlanController.GetDegreePlanArchives2Async(2);
                Assert.AreEqual(degreePlanArchiveEntities.Count(), archives.Count());
            }

            [TestMethod]
            [ExpectedException(typeof(HttpResponseException))]
            public async Task DegreePlanController_GetDegreePlanArchives2Async_ZeroDegreePlanId()
            {
                var degreePlanArchives = await degreePlanController.GetDegreePlanArchives2Async(0);
            }

            private IEnumerable<DegreePlanArchive2> BuildDegreePlanArchiveDtos(IEnumerable<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanArchive> degreePlanArchiveEntities)
            {
                var archives = new List<DegreePlanArchive2>();
                foreach (var archive in degreePlanArchiveEntities)
                {
                    var degreePlanArchiveDto = new DegreePlanArchive2();
                    degreePlanArchiveDto.Id = archive.Id;
                    degreePlanArchiveDto.CreatedBy = archive.CreatedBy;
                    degreePlanArchiveDto.CreatedDate = (archive.CreatedDate.GetValueOrDefault(DateTime.Now)).DateTime;
                    degreePlanArchiveDto.DegreePlanId = archive.DegreePlanId;
                    degreePlanArchiveDto.StudentId = archive.StudentId;
                    degreePlanArchiveDto.ReviewedBy = archive.ReviewedBy;
                    degreePlanArchiveDto.ReviewedDate = archive.ReviewedDate.HasValue ? archive.ReviewedDate.Value.DateTime : (DateTime?)null;
                    archives.Add(degreePlanArchiveDto);
                }
                return archives;
            }
        }
        [TestClass]
        public class DegreePlanController_CreateDegreePlan5Async
        { 

            #region Test Context

            private TestContext testContextInstance;

            /// <summary>
            ///Gets or sets the test context which provides
            ///information about and functionality for the current test run.
            ///</summary>
            public TestContext TestContext
            {
                get
                {
                    return testContextInstance;
                }
                set
                {
                    testContextInstance = value;
                }
            }

            #endregion

            private Mock<IDegreePlanService> degreePlanServiceMock;
            private IDegreePlanService degreePlanService;
            private DegreePlansController degreePlanController;
            private ILogger logger;
            private ApiSettings apiSettings;

            [TestInitialize]
            public void Initialize()
            {
                EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));

                logger = new Mock<ILogger>().Object;
                degreePlanServiceMock = new Mock<IDegreePlanService>();
                degreePlanService = degreePlanServiceMock.Object;
                degreePlanController = new DegreePlansController(degreePlanService, logger, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                degreePlanController = null;
                degreePlanService = null;
            }

             [Ignore] // Ignore this test because we currently don't know how to mock up the response inside of the controller
             [TestMethod]
             public async Task DegreePlan_CreateDegreePlan5Async()
             {
                var degreePlan = new DegreePlanAcademicHistory2();
                degreePlan.AcademicHistory = new Ellucian.Colleague.Dtos.Student.AcademicHistory3();
                degreePlan.AcademicHistory.OverallGradePointAverage = 3.5m;
                var id = "0000123";
                

                // Mock the degree plan service that updates a plan.
                degreePlanServiceMock.Setup(repo => repo.CreateDegreePlan5Async(id)).Returns(Task.FromResult(degreePlan));

                var newDegreePlanResponse = await degreePlanController.Post5Async(id);
                var newDegreePlan = JsonConvert.DeserializeObject<DegreePlanAcademicHistory2>(newDegreePlanResponse.Content.ReadAsStringAsync().Result);
                Assert.IsInstanceOfType(newDegreePlan, typeof(DegreePlanAcademicHistory2));
                Assert.AreEqual(degreePlan.AcademicHistory.OverallGradePointAverage, newDegreePlan.AcademicHistory.OverallGradePointAverage);
             }
        }
    }
}