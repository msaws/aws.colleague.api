﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Api.Controllers;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System.IO;
using System.Xml.Linq;
using System.Web.Http;
using System;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Dtos.Student.Transcripts;
using System.Net;
using System.Net.Http;
using System.Web.Http.Routing;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Http.Configuration;
using Ellucian.Web.Security;
using Ellucian.Colleague.Coordination.Planning.Services;
using Ellucian.Colleague.Api.Controllers.Planning;
using Ellucian.Colleague.Dtos.Planning;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Api.Tests.Controllers.Planning
{
     [TestClass]
     public class PlanningStudentsControllerTests
     {
          #region Test Context

          private TestContext testContextInstance;

          /// <summary>
          ///Gets or sets the test context which provides
          ///information about and functionality for the current test run.
          ///</summary>
          public TestContext TestContext
          {
               get
               {
                    return testContextInstance;
               }
               set
               {
                    testContextInstance = value;
               }
          }

          #endregion

          private IProgramEvaluationService programEvaluationService;
          private IStudentProgramRepository studentProgramRepo;
          private IPlanningStudentService planningStudentService;
          private IAdapterRegistry adapterRegistry;
          private PlanningStudentsController planningStudentsController;

          Mock<IProgramEvaluationService> programEvaluationServiceMock = new Mock<IProgramEvaluationService>();
          Mock<IStudentProgramRepository> studentProgramRepoMock = new Mock<IStudentProgramRepository>();
          Mock<IPlanningStudentService> planningStudentServiceMock = new Mock<IPlanningStudentService>();

          Mock<IAdapterRegistry> adapterRegistryMock = new Mock<IAdapterRegistry>();

          private ILogger logger;

          [TestInitialize]
          public void Initialize()
          {
               EllucianLicenseProvider.RefreshLicense(System.IO.Path.Combine(TestContext.TestDeploymentDir, "App_Data"));
               programEvaluationService = programEvaluationServiceMock.Object;
               studentProgramRepo = studentProgramRepoMock.Object;
               planningStudentService = planningStudentServiceMock.Object;
               adapterRegistry = adapterRegistryMock.Object;
               logger = new Mock<ILogger>().Object;

               // mock controller
               planningStudentsController = new PlanningStudentsController(adapterRegistry, programEvaluationService, planningStudentService, logger);
          }

          [TestCleanup]
          public void Cleanup()
          {
               planningStudentsController = null;
               adapterRegistry = null;
               studentProgramRepo = null;
               programEvaluationService = null;
          }

          [TestMethod]
          public async Task GetEvaluation()
          {
               // arrange
               var studentId = "0000001";
               var programCode = "ENGL.BA";
               var programEvaluation = new Domain.Planning.Entities.ProgramEvaluation(new List<Domain.Student.Entities.AcademicCredit>(), "ENGL.BA", "2013");
               programEvaluationServiceMock.Setup(svc => svc.EvaluateAsync(It.IsAny<string>(), It.IsAny<List<string>>())).Returns(Task.FromResult((new List<Domain.Planning.Entities.ProgramEvaluation>() { programEvaluation }).AsEnumerable()));
               var programEvaluationDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation>(adapterRegistry, logger);
               adapterRegistryMock.Setup(x => x.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation>()).Returns(programEvaluationDtoAdapter);

               // act
               var result = await planningStudentsController.GetEvaluationAsync(studentId, programCode);

               // assert
               Assert.IsTrue(result is ProgramEvaluation);
          }

          [TestMethod]
          public async Task QueryEvaluations()
          {
               // arrange
               var studentId = "0000001";
               var programCodes = new List<string>() { "ENGL.BA", "MATH.BA" };
               var programEvaluations = new List<Domain.Planning.Entities.ProgramEvaluation>() {
                new Domain.Planning.Entities.ProgramEvaluation(new List<Domain.Student.Entities.AcademicCredit>(), "ENGL.BA", "2013"),
                new Domain.Planning.Entities.ProgramEvaluation(new List<Domain.Student.Entities.AcademicCredit>(), "MATH.BA", "2014")
            };
               programEvaluationServiceMock.Setup(svc => svc.EvaluateAsync(It.IsAny<string>(), It.IsAny<List<string>>())).Returns(Task.FromResult(programEvaluations.AsEnumerable()));
               var programEvaluationDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation>(adapterRegistry, logger);
               adapterRegistryMock.Setup(x => x.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation>()).Returns(programEvaluationDtoAdapter);

               // act
               var result = await planningStudentsController.QueryEvaluationsAsync(studentId, programCodes);

               // assert
               Assert.IsTrue(result is List<ProgramEvaluation>);
               Assert.AreEqual(2, result.Count());
          }


          [TestMethod]
          public async Task GetEvaluationNotices()
          {
               // arrange
               var studentId = "0000001";
               var programCode = "ENGL.BA";
               var text1 = new List<string>() { "line1", "line2" };
               var type1 = EvaluationNoticeType.StudentProgram;
               var noticeDto1 = new EvaluationNotice() { StudentId = studentId, ProgramCode = programCode, Text = text1, Type = type1 };
               var text2 = new List<string>() { "line3", "line4" };
               var type2 = EvaluationNoticeType.End;
               var noticeDto2 = new EvaluationNotice() { StudentId = studentId, ProgramCode = programCode, Text = text2, Type = type2 };
               programEvaluationServiceMock.Setup(svc => svc.GetEvaluationNoticesAsync(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(new List<EvaluationNotice>() { noticeDto1, noticeDto2 }.AsEnumerable()));

               // act
               var result = await planningStudentsController.GetEvaluationNoticesAsync("0000001", "XYZ");

               // assert
               Assert.IsTrue(result is List<EvaluationNotice>);
               Assert.AreEqual(2, result.Count());
          }


          [TestMethod]
          public async Task GetPlanningStudent()
          {
               // arrange
               var planningStudentDto = new PlanningStudent() { Id = "0000001", DegreePlanId = 12, LastName = "smith" };
               planningStudentServiceMock.Setup(svc => svc.GetPlanningStudentAsync(It.IsAny<string>())).Returns(Task.FromResult(planningStudentDto));

               // act
               var result = await planningStudentsController.GetPlanningStudentAsync("0000001");

               // assert
               Assert.IsTrue(result is PlanningStudent);
               Assert.AreEqual("0000001", result.Id);
          }

          [TestMethod]
          [ExpectedException(typeof(HttpResponseException))]
          public async Task GetEvaluationNotices_ConvertsExceptionToHttpResponse()
          {
               // arrange--Mock permissions exception from student service Get
               programEvaluationServiceMock.Setup(svc => svc.GetEvaluationNoticesAsync(It.IsAny<string>(), It.IsAny<string>())).Throws(new PermissionsException());

               // act
               var result = await planningStudentsController.GetEvaluationNoticesAsync("NOPERMISSION", "X");
          }

          [TestMethod]
          public async Task QueryPlanningStudents()
          {
               var planningStudentCriteria = new PlanningStudentCriteria();
               planningStudentCriteria.StudentIds = new List<string>() { "0004723", "0011902" };
               var planningStudents = new List<PlanningStudent>() {
            new PlanningStudent(){Id="0004723"},
            new PlanningStudent(){Id="0011902"}
            };
               planningStudentServiceMock.Setup(s => s.QueryPlanningStudentsAsync(It.IsAny<List<string>>())).Returns(Task.FromResult(planningStudents.AsEnumerable()));
               var result = await planningStudentsController.QueryPlanningStudentsAsync(planningStudentCriteria);
               Assert.IsTrue(result is List<PlanningStudent>);
               Assert.AreEqual(2, result.Count());
          }

          [TestMethod]
          [ExpectedException(typeof(HttpResponseException))]
          public async Task QueryPlanningStudents_ConvertsPermissionExceptionToHttpResponse()
          {
               var planningStudentCriteria = new PlanningStudentCriteria();
               planningStudentCriteria.StudentIds = new List<string>() { "0004723", "0011902" };
               var planningStudents = new List<PlanningStudent>() {
            new PlanningStudent(){Id="0004723"},
            new PlanningStudent(){Id="0011902"}
            };
               planningStudentServiceMock.Setup(svc => svc.QueryPlanningStudentsAsync(It.IsAny<List<string>>())).Throws(new PermissionsException());
               var result = await planningStudentsController.QueryPlanningStudentsAsync(planningStudentCriteria);
          }

          [TestMethod]
          [ExpectedException(typeof(HttpResponseException))]
          public async Task QueryPlanningStudents_ConvertsArgumentExceptionToHttpResponse()
          {
               var planningStudentCriteria = new PlanningStudentCriteria();
               planningStudentCriteria.StudentIds = new List<string>() { "0004723", "0011902" };
               var planningStudents = new List<PlanningStudent>() {
            new PlanningStudent(){Id="0004723"},
            new PlanningStudent(){Id="0011902"}
            };
               planningStudentServiceMock.Setup(svc => svc.QueryPlanningStudentsAsync(It.IsAny<List<string>>())).Throws(new ArgumentException());
               var result = await planningStudentsController.QueryPlanningStudentsAsync(planningStudentCriteria);
          }

          //Tests for versioned Controller Methods
          [TestMethod]
          public async Task GetEvaluation2Async()
          {
               // arrange
               var studentId = "0000001";
               var programCode = "ENGL.BA";
               var programEvaluation = new Domain.Planning.Entities.ProgramEvaluation(new List<Domain.Student.Entities.AcademicCredit>(), "ENGL.BA", "2013");
               programEvaluationServiceMock.Setup(svc => svc.EvaluateAsync(It.IsAny<string>(), It.IsAny<List<string>>())).Returns(Task.FromResult((new List<Domain.Planning.Entities.ProgramEvaluation>() { programEvaluation }).AsEnumerable()));
               var programEvaluationDto2Adapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation2>(adapterRegistry, logger);
               adapterRegistryMock.Setup(x => x.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation2>()).Returns(programEvaluationDto2Adapter);

               // act
               var result = await planningStudentsController.GetEvaluation2Async(studentId, programCode);

               // assert
               Assert.IsTrue(result is ProgramEvaluation2);
          }

          [TestMethod]
          public async Task QueryEvaluations2Async()
          {
               // arrange
               var studentId = "0000001";
               var programCodes = new List<string>() { "ENGL.BA", "MATH.BA" };
               var programEvaluations = new List<Domain.Planning.Entities.ProgramEvaluation>() {
                new Domain.Planning.Entities.ProgramEvaluation(new List<Domain.Student.Entities.AcademicCredit>(), "ENGL.BA", "2013"),
                new Domain.Planning.Entities.ProgramEvaluation(new List<Domain.Student.Entities.AcademicCredit>(), "MATH.BA", "2014")
            };
               programEvaluationServiceMock.Setup(svc => svc.EvaluateAsync(It.IsAny<string>(), It.IsAny<List<string>>())).Returns(Task.FromResult(programEvaluations.AsEnumerable()));
               var programEvaluationDto2Adapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation2>(adapterRegistry, logger);
               adapterRegistryMock.Setup(x => x.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation2>()).Returns(programEvaluationDto2Adapter);

               // act
               var result = await planningStudentsController.QueryEvaluations2Async(studentId, programCodes);

               // assert
               Assert.IsTrue(result is List<ProgramEvaluation2>);
               Assert.AreEqual(2, result.Count());
          }

          //Tests for versioned Controller Methods
          [TestMethod]
          public async Task GetEvaluation3Async()
          {
              // arrange
              var studentId = "0000001";
              var programCode = "ENGL.BA";
              var programEvaluation = new Domain.Planning.Entities.ProgramEvaluation(new List<Domain.Student.Entities.AcademicCredit>(), "ENGL.BA", "2013");
              programEvaluationServiceMock.Setup(svc => svc.EvaluateAsync(It.IsAny<string>(), It.IsAny<List<string>>())).Returns(Task.FromResult((new List<Domain.Planning.Entities.ProgramEvaluation>() { programEvaluation }).AsEnumerable()));
              var programEvaluationDto3Adapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation3>(adapterRegistry, logger);
              adapterRegistryMock.Setup(x => x.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation3>()).Returns(programEvaluationDto3Adapter);

              // act
              var result = await planningStudentsController.GetEvaluation3Async(studentId, programCode);

              // assert
              Assert.IsTrue(result is ProgramEvaluation3);
          }

          [TestMethod]
          public async Task QueryEvaluations3Async()
          {
              // arrange
              var studentId = "0000001";
              var programCodes = new List<string>() { "ENGL.BA", "MATH.BA" };
              var programEvaluations = new List<Domain.Planning.Entities.ProgramEvaluation>() {
                new Domain.Planning.Entities.ProgramEvaluation(new List<Domain.Student.Entities.AcademicCredit>(), "ENGL.BA", "2013"),
                new Domain.Planning.Entities.ProgramEvaluation(new List<Domain.Student.Entities.AcademicCredit>(), "MATH.BA", "2014")
            };
              programEvaluationServiceMock.Setup(svc => svc.EvaluateAsync(It.IsAny<string>(), It.IsAny<List<string>>())).Returns(Task.FromResult(programEvaluations.AsEnumerable()));
              var programEvaluationDto3Adapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation3>(adapterRegistry, logger);
              adapterRegistryMock.Setup(x => x.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation3>()).Returns(programEvaluationDto3Adapter);

              // act
              var result = await planningStudentsController.QueryEvaluations3Async(studentId, programCodes);

              // assert
              Assert.IsTrue(result is List<ProgramEvaluation3>);
              Assert.AreEqual(2, result.Count());
          }

     }
}
