﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.FinancialAid.Entities;

namespace Ellucian.Colleague.Domain.FinancialAid.Repositories
{
    public interface IRuleTableRepository
    {
        IEnumerable<ShoppingSheetRuleTable> GetShoppingSheetRuleTables(IEnumerable<string> awardYears);
    }
}
