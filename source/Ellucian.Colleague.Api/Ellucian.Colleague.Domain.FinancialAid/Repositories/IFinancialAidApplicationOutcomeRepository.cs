﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System.Threading.Tasks;
using Ellucian.Colleague.Domain.FinancialAid.Entities;
using System.Collections.Generic;
using System;

namespace Ellucian.Colleague.Domain.FinancialAid.Repositories
{
    /// <summary>
    /// Definition of methods implemented for a FinancialAidApplicationOutcome repository
    /// </summary>
    public interface IFinancialAidApplicationOutcomeRepository
    {
        Task<Fafsa> GetByIdAsync(string id);

        Task<Tuple<IEnumerable<Fafsa>, int>> GetAsync(int offset, int limit, bool bypassCache, List<string> faSuiteYears);
        
    }
}
