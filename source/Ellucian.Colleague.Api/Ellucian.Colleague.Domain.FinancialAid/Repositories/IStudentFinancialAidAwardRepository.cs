﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System.Threading.Tasks;
using Ellucian.Colleague.Domain.FinancialAid.Entities;
using System.Collections.Generic;
using System;

namespace Ellucian.Colleague.Domain.FinancialAid.Repositories
{
    /// <summary>
    /// Definition of methods implemented for a StudentFinancialAidAwards repository
    /// </summary>
    public interface IStudentFinancialAidAwardRepository
    {
        Task<StudentFinancialAidAward> GetByIdAsync(string id);

        Task<Tuple<IEnumerable<StudentFinancialAidAward>, int>> GetAsync(int offset, int limit, bool bypassCache, bool restricted, IEnumerable<string> unrestrictedFunds, IEnumerable<string> awardYears);
        
        Task<IEnumerable<string>> GetNotAwardedCategoriesAsync();
    }
}
