﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.FinancialAid.Entities;

namespace Ellucian.Colleague.Domain.FinancialAid.Repositories
{
    public interface IStudentBudgetComponentRepository
    {
        IEnumerable<StudentBudgetComponent> GetStudentBudgetComponents(string studentId, IEnumerable<StudentAwardYear> studentAwardYears);
    }
}
