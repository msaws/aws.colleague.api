﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.FinancialAid.Entities;

namespace Ellucian.Colleague.Domain.FinancialAid.Repositories
{
    /// <summary>
    /// Interface for a ProfileApplicationRepository class
    /// </summary>
    public interface IProfileApplicationRepository
    {
        /// <summary>
        /// Get all Profile Applications for the given student
        /// </summary>
        /// <param name="studentId">The Colleague PERSON id for whom to get profile applications</param>
        /// <param name="studentAwardYears">The list of studentAwardYears for which to get profile applications for the student</param>
        /// <returns>A list of profile applications for the given student</returns>
        IEnumerable<ProfileApplication> GetProfileApplications(string studentId, IEnumerable<StudentAwardYear> studentAwardYears);
    }
}
