﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;

namespace Ellucian.Colleague.Domain.FinancialAid
{
    [Serializable]
    public static class FinancialAidPermissionCodes
    {
        // Access to view student financial aid awards
        public const string ViewStudentFinancialAidAwards = "VIEW.STUDENT.FA.AWARDS";

        // Access to view financial aid applications
        public const string ViewFinancialAidApplications = "VIEW.FA.APPLICATIONS";

        // Access to view financial aid application outcomes
        public const string ViewFinancialAidApplicationOutcomes = "VIEW.FA.APPLICATION.OUTCOMES";

        // Access to view student financial aid need summaries
        public const string ViewStudentFinancialAidNeedSummaries = "VIEW.STU.FA.NEED.SUMMARIES";
    }
}
