﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.Student;

namespace Ellucian.Colleague.Dtos.Planning
{
    /// <summary>
    /// Combined result provides a student's Degree Plan (v4) and Academic History (v3)
    /// </summary>
    public class DegreePlanAcademicHistory2
    {
        /// <summary>
        /// Student's Degree Plan
        /// </summary>
        public DegreePlan4 DegreePlan { get; set; }

        /// <summary>
        /// Student's Academic History
        /// </summary>
        public AcademicHistory3 AcademicHistory { get; set; }
    }
}
