﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.Planning
{
    /// <summary>
    /// Enumeration contains all the possible waitlist statuses.
    /// </summary>
    public enum WaitlistStatus
    {
        /// <summary>
        /// Not currently waitlisted
        /// </summary>
        NotWaitlisted, 
        /// <summary>
        /// Actively waitlisted
        /// </summary>
        Active,
        /// <summary>
        /// Waitlisted with permission to register
        /// </summary>
        PermissionToRegister
    }
}
