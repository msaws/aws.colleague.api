﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.Planning
{
    /// <remarks>
    /// All of the possible planned course warnings
    /// </remarks>
    public enum PlannedCourseWarningType
    {
        /// <summary>
        /// Number of planned credits is invalid because it falls outside the planned credit range for the course
        /// </summary>
        InvalidPlannedCredits,
        /// <summary>
        /// Planned credits is negative
        /// </summary>
        NegativePlannedCredits,
        /// <summary>
        /// Planned course has a time conflict with one or more other planned courses.
        /// </summary>
        TimeConflict,
        /// <summary>
        /// Planned course has an unmet requisite
        /// </summary>
        UnmetRequisite,
        /// <summary>
        /// Planned course has been planned in a term in which it may not be offered
        /// </summary>
        CourseOfferingConflict
    }
}
