﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.Planning
{
    /// <summary>
    /// Indicates whether a course is approved or denied for a given term
    /// </summary>
    public enum DegreePlanApprovalStatus
    {
        /// <summary>
        /// Approved
        /// </summary>
        Approved,
        /// <summary>
        /// Denied
        /// </summary>
        Denied
    }
}
