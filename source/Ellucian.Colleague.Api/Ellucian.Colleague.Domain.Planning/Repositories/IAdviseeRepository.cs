﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Planning.Entities;
using Ellucian.Colleague.Domain.Student.Entities;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.Planning.Repositories
{
    public interface IAdviseeRepository
    {
        Task<Planning.Entities.PlanningStudent> GetAsync(string id);
        Task<IEnumerable<Planning.Entities.PlanningStudent>> GetAsync(IEnumerable<string> ids, int pageSize, int pageIndex);
        Task<IEnumerable<Planning.Entities.PlanningStudent>> SearchByNameAsync(string lastName, string firstName = null, string middleName = null, int pageSize = int.MaxValue, int pageIndex = 1, IEnumerable<string> assignedAdvisees = null);
        Task<IEnumerable<Planning.Entities.PlanningStudent>> SearchByAdvisorIdsAsync(IEnumerable<string> advisorIds, int pageSize = int.MaxValue, int pageIndex = 1, IEnumerable<string> assignedAdvisees = null);
    }
}