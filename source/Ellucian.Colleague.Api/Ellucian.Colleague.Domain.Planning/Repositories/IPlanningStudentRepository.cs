﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.Planning.Repositories
{
    public interface IPlanningStudentRepository
    {
        Task<Domain.Planning.Entities.PlanningStudent> GetAsync(string id);
        Task<IEnumerable<Domain.Planning.Entities.PlanningStudent>> GetAsync(IEnumerable<string> ids);
    }
}
