﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.Planning.Repositories
{
    public interface IAdvisorRepository
    {
        /// <summary>
        /// Returns detailed advising information for an advisor: whether an active advisor, list of advisees
        /// </summary>
        Task<Entities.Advisor> GetAsync(string id);
        /// <summary>
        /// Finds faculty advisors given a last, first, middle name.
        /// </summary>
        Task<IEnumerable<string>> SearchAdvisorByNameAsync(string lastName, string firstName = null, string middleName = null);
        /// <summary>
        /// Returns basic contact information for an advisor: name, email addresses
        /// </summary>
        Task<IEnumerable<Entities.Advisor>> GetAdvisorsAsync(IEnumerable<string> advisorIds, bool onlyActiveAdvisees); 
    }
}
