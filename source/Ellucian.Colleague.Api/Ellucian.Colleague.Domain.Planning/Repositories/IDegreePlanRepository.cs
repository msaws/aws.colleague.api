﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Planning.Entities;
using Ellucian.Colleague.Domain.Base;
using Ellucian.Colleague.Domain.Student.Entities;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.Planning.Repositories
{
    public interface IDegreePlanRepository
    {
        Task<DegreePlan> AddAsync(DegreePlan newPlan);
        Task<DegreePlan> GetAsync(int planId);
        Task<DegreePlan> UpdateAsync(DegreePlan plan);
        Task<IEnumerable<DegreePlan>> GetAsync(IEnumerable<string> studentIds);
    }
}
