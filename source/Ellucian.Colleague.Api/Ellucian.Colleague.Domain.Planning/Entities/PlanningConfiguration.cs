﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.Planning.Entities
{
    [Serializable]
    public class PlanningConfiguration
    {
        public string DefaultCurriculumTrack { get; set; }
        public CatalogPolicy DefaultCatalogPolicy { get; set; }
    }
}
