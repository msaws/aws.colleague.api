﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.Planning.Entities
{
    /// <summary>
    /// Approved: Advisor has approved the planned course item.
    /// Denied: Advisor has officially denied their approval of a particular course item.
    /// </summary>
    [Serializable]
    public enum DegreePlanApprovalStatus
    {
        Approved, Denied
    }
}
