﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.Planning.Entities
{
    /// <summary>
    /// Status of a person on a waitlist - related to the planned course on the plan.
    /// </summary>
    [Serializable]
    public enum WaitlistStatus
    {
        NotWaitlisted, Active, PermissionToRegister
    }
}
