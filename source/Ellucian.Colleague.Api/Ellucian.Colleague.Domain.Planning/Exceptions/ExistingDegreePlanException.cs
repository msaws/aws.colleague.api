﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.Planning.Exceptions
{
    public class ExistingDegreePlanException : System.Exception
    {
        private readonly int? _ExistingPlanId;
        public int? ExistingPlanId { get { return _ExistingPlanId; } }
        
        public ExistingDegreePlanException()
        {

        }

        public ExistingDegreePlanException(string message, int existingPlanId)
            : base(message)
        {
            _ExistingPlanId = existingPlanId;
        }
    }
}
