//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 10/10/2014 12:41:14 AM by user kmf
//
//     Type: ENTITY
//     Entity: FIN.AID
//     Application: ST
//     Environment: dvcoll_wstst01
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;
using Ellucian.Data.Colleague;

namespace Ellucian.Colleague.Data.Student.DataContracts
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract(Name = "FinAid")]
	[ColleagueDataContract(GeneratedDateTime = "10/10/2014 12:41:14 AM", User = "kmf")]
	[EntityDataContract(EntityName = "FIN.AID", EntityType = "PHYS")]
	public class FinAid : IColleagueEntity
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		/// <summary>
		/// Record ID
		/// </summary>
		[DataMember]
		public string Recordkey { get; set; }
		
		public void setKey(string key)
		{
			Recordkey = key;
		}	
		
		/// <summary>
		/// CDD Name: FA.COUNSELOR
		/// </summary>
		[DataMember(Order = 47, Name = "FA.COUNSELOR")]
		public List<string> FaCounselor { get; set; }
		
		/// <summary>
		/// CDD Name: FA.COUNSELOR.START.DATE
		/// </summary>
		[DataMember(Order = 48, Name = "FA.COUNSELOR.START.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public List<DateTime?> FaCounselorStartDate { get; set; }
		
		/// <summary>
		/// CDD Name: FA.COUNSELOR.END.DATE
		/// </summary>
		[DataMember(Order = 49, Name = "FA.COUNSELOR.END.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public List<DateTime?> FaCounselorEndDate { get; set; }
		
		/// <summary>
		/// Entity assocation member
		/// </summary>
		[DataMember]
		public List<FinAidFaCounselors> FaCounselorsEntityAssociation { get; set; }
		
	
		// build up all the Associated objects and add them to the properties
		public void buildAssociations()
		{	
			// EntityAssociation Name: FA.COUNSELORS
			
			FaCounselorsEntityAssociation = new List< FinAidFaCounselors >();
			if( FaCounselor != null)
			{
				int numFaCounselors = FaCounselor.Count;
				for (int i = 0; i < numFaCounselors; i++)
				{
					string value0 = "";
					value0 = FaCounselor[i];

					DateTime? value1 = null;
					if (FaCounselorStartDate != null && i < FaCounselorStartDate.Count)
					{
						value1 = FaCounselorStartDate[i];
					}

					DateTime? value2 = null;
					if (FaCounselorEndDate != null && i < FaCounselorEndDate.Count)
					{
						value2 = FaCounselorEndDate[i];
					}

					FaCounselorsEntityAssociation.Add(new FinAidFaCounselors( value0, value1, value2));
				}
			}
			   
		}
	}
	
	// EntityAssociation classes
	
	[Serializable]
	public class FinAidFaCounselors
	{
		public string FaCounselorAssocMember;	
		public DateTime? FaCounselorStartDateAssocMember;	
		public DateTime? FaCounselorEndDateAssocMember;	
		public FinAidFaCounselors() {}
		public FinAidFaCounselors(
			string inFaCounselor,
			DateTime? inFaCounselorStartDate,
			DateTime? inFaCounselorEndDate)
		{
			FaCounselorAssocMember = inFaCounselor;
			FaCounselorStartDateAssocMember = inFaCounselorStartDate;
			FaCounselorEndDateAssocMember = inFaCounselorEndDate;
		}
	}
}