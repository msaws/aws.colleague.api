//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 5/9/2016 4:47:05 PM by user bsf1
//
//     Type: ENTITY
//     Entity: CATALOGS
//     Application: ST
//     Environment: dvcoll_wstst01_rt
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;
using Ellucian.Data.Colleague;

namespace Ellucian.Colleague.Data.Student.DataContracts
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract(Name = "Catalogs")]
	[ColleagueDataContract(GeneratedDateTime = "5/9/2016 4:47:05 PM", User = "bsf1")]
	[EntityDataContract(EntityName = "CATALOGS", EntityType = "PHYS")]
	public class Catalogs : IColleagueGuidEntity
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		/// <summary>
		/// Record Key
		/// </summary>
		[DataMember]
		public string Recordkey { get; set; }
		
		public void setKey(string key)
		{
			Recordkey = key;
		}
	
		/// <summary>
		/// Record GUID
		/// </summary>
		[DataMember(Name = "RecordGuid")]
		public string RecordGuid { get; set; }

		/// <summary>
		/// Record Model Name
		/// </summary>
		[DataMember(Name = "RecordModelName")]
		public string RecordModelName { get; set; }	
		
		/// <summary>
		/// CDD Name: CAT.DESC
		/// </summary>
		[DataMember(Order = 0, Name = "CAT.DESC")]
		public string CatDesc { get; set; }
		
		/// <summary>
		/// CDD Name: CAT.START.DATE
		/// </summary>
		[DataMember(Order = 3, Name = "CAT.START.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public DateTime? CatStartDate { get; set; }
		
		/// <summary>
		/// CDD Name: CAT.END.DATE
		/// </summary>
		[DataMember(Order = 4, Name = "CAT.END.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public DateTime? CatEndDate { get; set; }
		
		/// <summary>
		/// CDD Name: CAT.ACAD.PROGRAMS
		/// </summary>
		[DataMember(Order = 5, Name = "CAT.ACAD.PROGRAMS")]
		public List<string> CatAcadPrograms { get; set; }
		
	
		// build up all the Associated objects and add them to the properties
		public void buildAssociations()
		{	
			   
		}
	}
	
	// EntityAssociation classes
}