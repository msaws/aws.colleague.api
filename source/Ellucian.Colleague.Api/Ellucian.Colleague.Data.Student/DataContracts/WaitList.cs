//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 6/2/2017 1:07:22 PM by user mbscs
//
//     Type: ENTITY
//     Entity: WAIT.LIST
//     Application: ST
//     Environment: colldev
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;
using Ellucian.Data.Colleague;

namespace Ellucian.Colleague.Data.Student.DataContracts
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract(Name = "WaitList")]
	[ColleagueDataContract(GeneratedDateTime = "6/2/2017 1:07:22 PM", User = "mbscs")]
	[EntityDataContract(EntityName = "WAIT.LIST", EntityType = "PHYS")]
	public class WaitList : IColleagueGuidEntity
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		/// <summary>
		/// Record Key
		/// </summary>
		[DataMember]
		public string Recordkey { get; set; }
		
		public void setKey(string key)
		{
			Recordkey = key;
		}
	
		/// <summary>
		/// Record GUID
		/// </summary>
		[DataMember(Name = "RecordGuid")]
		public string RecordGuid { get; set; }

		/// <summary>
		/// Record Model Name
		/// </summary>
		[DataMember(Name = "RecordModelName")]
		public string RecordModelName { get; set; }	
		
		/// <summary>
		/// CDD Name: WAIT.COURSE
		/// </summary>
		[DataMember(Order = 0, Name = "WAIT.COURSE")]
		public string WaitCourse { get; set; }
		
		/// <summary>
		/// CDD Name: WAIT.STUDENT
		/// </summary>
		[DataMember(Order = 1, Name = "WAIT.STUDENT")]
		public string WaitStudent { get; set; }
		
		/// <summary>
		/// CDD Name: WAIT.TERM
		/// </summary>
		[DataMember(Order = 2, Name = "WAIT.TERM")]
		public string WaitTerm { get; set; }
		
		/// <summary>
		/// CDD Name: WAIT.COURSE.SECTION
		/// </summary>
		[DataMember(Order = 3, Name = "WAIT.COURSE.SECTION")]
		public string WaitCourseSection { get; set; }
		
		/// <summary>
		/// CDD Name: WAIT.RATING
		/// </summary>
		[DataMember(Order = 4, Name = "WAIT.RATING")]
		public int? WaitRating { get; set; }
		
		/// <summary>
		/// CDD Name: WAIT.STATUS
		/// </summary>
		[DataMember(Order = 10, Name = "WAIT.STATUS")]
		public string WaitStatus { get; set; }
		
		/// <summary>
		/// CDD Name: WAIT.CRED
		/// </summary>
		[DataMember(Order = 25, Name = "WAIT.CRED")]
		[DisplayFormat(DataFormatString = "{0:N5}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public Decimal? WaitCred { get; set; }
		
	
		// build up all the Associated objects and add them to the properties
		public void buildAssociations()
		{	
			   
		}
	}
	
	// EntityAssociation classes
}