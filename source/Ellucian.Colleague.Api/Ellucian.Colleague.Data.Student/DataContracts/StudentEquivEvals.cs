//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 8/14/2014 10:00:21 AM by user kmf
//
//     Type: ENTITY
//     Entity: STUDENT.EQUIV.EVALS
//     Application: ST
//     Environment: dvcoll_wstst01
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;
using Ellucian.Data.Colleague;

namespace Ellucian.Colleague.Data.Student.DataContracts
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract(Name = "StudentEquivEvals")]
	[ColleagueDataContract(GeneratedDateTime = "8/14/2014 10:00:21 AM", User = "kmf")]
	[EntityDataContract(EntityName = "STUDENT.EQUIV.EVALS", EntityType = "PHYS")]
	public class StudentEquivEvals : IColleagueEntity
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		/// <summary>
		/// Record ID
		/// </summary>
		[DataMember]
		public string Recordkey { get; set; }
		
		public void setKey(string key)
		{
			Recordkey = key;
		}	
		
		/// <summary>
		/// CDD Name: STE.STUDENT.NON.COURSE
		/// </summary>
		[DataMember(Order = 3, Name = "STE.STUDENT.NON.COURSE")]
		public string SteStudentNonCourse { get; set; }
		
	
		// build up all the Associated objects and add them to the properties
		public void buildAssociations()
		{	
			   
		}
	}
	
	// EntityAssociation classes
}