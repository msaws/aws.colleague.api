//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 1/5/2017 11:01:26 AM by user dvcoll-srm
//
//     Type: CTX
//     Transaction ID: DELETE.STUDENT.PAYEMENT
//     Application: ST
//     Environment: dvcoll
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;

namespace Ellucian.Colleague.Data.Student.Transactions
{
	[DataContract]
	public class DeleteStudentPaymentErrors
	{
		[DataMember]
		[SctrqDataMember(AppServerName = "ERROR.CODE", OutBoundData = true)]
		public string ErrorCode { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "ERROR.MSG", OutBoundData = true)]
		public string ErrorMsg { get; set; }
	}

	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract]
	[ColleagueDataContract(ColleagueId = "DELETE.STUDENT.PAYEMENT", GeneratedDateTime = "1/5/2017 11:01:26 AM", User = "dvcoll-srm")]
	[SctrqDataContract(Application = "ST", DataContractVersion = 1)]
	public class DeleteStudentPaymentRequest
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "AR.PAY.ITEMS.INTG.ID", InBoundData = true)]        
		public string ArPayItemsIntgId { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "GUID", InBoundData = true)]        
		public string Guid { get; set; }

		public DeleteStudentPaymentRequest()
		{	
		}
	}

	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract]
	[ColleagueDataContract(ColleagueId = "DELETE.STUDENT.PAYEMENT", GeneratedDateTime = "1/5/2017 11:01:26 AM", User = "dvcoll-srm")]
	[SctrqDataContract(Application = "ST", DataContractVersion = 1)]
	public class DeleteStudentPaymentResponse
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "AR.PAY.ITEMS.INTG.ID", OutBoundData = true)]        
		public string ArPayItemsIntgId { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "Grp:ERROR.CODE", OutBoundData = true)]
		public List<DeleteStudentPaymentErrors> DeleteStudentPaymentErrors { get; set; }

		public DeleteStudentPaymentResponse()
		{	
			DeleteStudentPaymentErrors = new List<DeleteStudentPaymentErrors>();
		}
	}
}
