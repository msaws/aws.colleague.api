//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 6/6/2017 5:58:53 PM by user balvano3
//
//     Type: CTX
//     Transaction ID: UPDATE.MEAL.PLAN.ASSIGN.INTG
//     Application: ST
//     Environment: dvcoll
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;

namespace Ellucian.Colleague.Data.Student.Transactions
{
	[DataContract]
	public class UpdateMealPlanAssignErrors
	{
		[DataMember]
		[SctrqDataMember(AppServerName = "AL.ERROR.MSG", OutBoundData = true)]
		public string AlErrorMsg { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "AL.ERROR.CODE", OutBoundData = true)]
		public string AlErrorCode { get; set; }
	}

	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract]
	[ColleagueDataContract(ColleagueId = "UPDATE.MEAL.PLAN.ASSIGN.INTG", GeneratedDateTime = "6/6/2017 5:58:53 PM", User = "balvano3")]
	[SctrqDataContract(Application = "ST", DataContractVersion = 1)]
	public class UpdateMealPlanAssignIntgRequest
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.PERSON.ID", InBoundData = true)]        
		public string APersonId { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.MEAL.PLAN.ID", InBoundData = true)]        
		public string AMealPlanId { get; set; }

		[DataMember]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[SctrqDataMember(AppServerName = "A.START.DATE", InBoundData = true)]        
		public Nullable<DateTime> AStartDate { get; set; }

		[DataMember]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[SctrqDataMember(AppServerName = "A.END.DATE", InBoundData = true)]        
		public Nullable<DateTime> AEndDate { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.TERM", InBoundData = true)]        
		public string ATerm { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.CURRENT.STATUS", InBoundData = true)]        
		public string ACurrentStatus { get; set; }

		[DataMember]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[SctrqDataMember(AppServerName = "A.CURRENT.STATUS.DATE", InBoundData = true)]        
		public Nullable<DateTime> ACurrentStatusDate { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.NO.RATE.PERIODS", InBoundData = true)]        
		public Nullable<long> ANoRatePeriods { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.USED.RATE.PERIODS", InBoundData = true)]        
		public Nullable<long> AUsedRatePeriods { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.USED.PCT", InBoundData = true)]        
		public Nullable<int> AUsedPct { get; set; }

		[DataMember]
		[DisplayFormat(DataFormatString = "{0:N2}")]
		[SctrqDataMember(AppServerName = "A.OVERRIDE.RATE", InBoundData = true)]        
		public Nullable<Decimal> AOverrideRate { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.OVERRIDE.RATE.REASON", InBoundData = true)]        
		public string AOverrideRateReason { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.OVERRIDE.AR.CODE", InBoundData = true)]        
		public string AOverrideArCode { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.OVERRIDE.AR.TYPE", InBoundData = true)]        
		public string AOverrideArType { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.COMMENTS", InBoundData = true)]        
		public string AComments { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.MEAL.CARD", InBoundData = true)]        
		public string AMealCard { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.ID", InBoundData = true)]        
		public string AId { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.GUID", InBoundData = true)]        
		public string AGuid { get; set; }

		public UpdateMealPlanAssignIntgRequest()
		{	
		}
	}

	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract]
	[ColleagueDataContract(ColleagueId = "UPDATE.MEAL.PLAN.ASSIGN.INTG", GeneratedDateTime = "6/6/2017 5:58:53 PM", User = "balvano3")]
	[SctrqDataContract(Application = "ST", DataContractVersion = 1)]
	public class UpdateMealPlanAssignIntgResponse
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.ID", OutBoundData = true)]        
		public string AId { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.GUID", OutBoundData = true)]        
		public string AGuid { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.ERROR", UseEnvisionBooleanConventions = EnvisionBooleanTypesEnum.OneZero, OutBoundData = true)]        
		public bool AError { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "Grp:AL.ERROR.CODE", OutBoundData = true)]
		public List<UpdateMealPlanAssignErrors> UpdateMealPlanAssignErrors { get; set; }

		public UpdateMealPlanAssignIntgResponse()
		{	
			UpdateMealPlanAssignErrors = new List<UpdateMealPlanAssignErrors>();
		}
	}
}
