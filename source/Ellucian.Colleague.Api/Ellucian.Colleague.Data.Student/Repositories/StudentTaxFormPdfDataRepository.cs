﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Data.Student.DataContracts;
using Ellucian.Colleague.Data.Student.Transactions;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;

namespace Ellucian.Colleague.Data.Student.Repositories
{
    /// <summary>
    /// Repository for the data to be printed in a PDF for any tax form
    /// </summary>
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class StudentTaxFormPdfDataRepository : BaseColleagueRepository, IStudentTaxFormPdfDataRepository
    {
        /// <summary>
        /// Tax Form PDF data repository constructor.
        /// </summary>
        /// <param name="cacheProvider">Cache provider</param>
        /// <param name="transactionFactory">Transaction factory</param>
        /// <param name="logger">Logger</param>
        public StudentTaxFormPdfDataRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            // nothing to do
        }

        /// <summary>
        /// Get the 1098-T data for a PDF.
        /// </summary>
        /// <param name="personId">ID of the person assigned to and requesting the 1098-T.</param>
        /// <param name="recordId">ID of the record containing the pdf data for a 1098-T tax form</param>
        /// <returns>1098-T data for a pdf</returns>
        public async Task<Form1098PdfData> Get1098TPdfAsync(string personId, string recordId)
        {
            // Get student demographic information and box numbers.
            var pdfDataContract = await DataReader.ReadRecordAsync<TaxForm1098Forms>(recordId);

            // Read person record to get SSN.
            var personContract = await DataReader.ReadRecordAsync<Person>(personId);

            try
            {
                if (pdfDataContract == null)
                {
                    throw new NullReferenceException("pdfDataContract cannot be null.");
                }
                // Read CORP to get the institution information.
                var corpContract = await DataReader.ReadRecordAsync<Corp>("PERSON", pdfDataContract.Tf98fInstitution);

                var parm1098Contract = await DataReader.ReadRecordAsync<Parm1098>("ST.PARMS", "PARM.1098");

                var corpFoundsContract = await DataReader.ReadRecordAsync<CorpFounds>(pdfDataContract.Tf98fInstitution);

                if (pdfDataContract.Tf98fTaxForm1098Boxes == null || !pdfDataContract.Tf98fTaxForm1098Boxes.Any())
                {
                    throw new ApplicationException("");
                }
                var form1098BoxDataContracts = await DataReader.BulkReadRecordAsync<TaxForm1098Boxes>(pdfDataContract.Tf98fTaxForm1098Boxes.ToArray());

                var boxCodesDataContracts = await DataReader.BulkReadRecordAsync<BoxCodes>(form1098BoxDataContracts.Select(x => x.Tf98bBoxCode).ToArray());

            
                if (corpFoundsContract == null)
                {
                    throw new NullReferenceException("corpFoundsContract cannot be null.");
                }
                if (pdfDataContract.Tf98fTaxYear == null)
                {
                    throw new NullReferenceException("Tf98fTaxYear cannot be null.");
                }
                if (string.IsNullOrEmpty(corpFoundsContract.CorpTaxId))
                {
                    throw new NullReferenceException("CorpTaxId cannot be null.");
                }

                Form1098PdfData entity = new Form1098PdfData(pdfDataContract.Tf98fTaxYear.ToString(), corpFoundsContract.CorpTaxId);
                entity.Correction = pdfDataContract.Tf98fCorrectionInd.ToUpper() == "Y";

                // Student data
                entity.StudentId = pdfDataContract.Tf98fStudent;
                entity.StudentName = pdfDataContract.Tf98fName;
                entity.StudentName2 = pdfDataContract.Tf98fName2;
                entity.StudentAddressLine1 = pdfDataContract.Tf98fAddress;
                entity.StudentAddressLine2 = pdfDataContract.Tf98fCity + ", " + pdfDataContract.Tf98fState + " " + pdfDataContract.Tf98fZip;
                if (pdfDataContract.Tf98fCountry.ToUpper() != "USA" && pdfDataContract.Tf98fCountry.ToUpper() != "US")
                {
                    entity.StudentAddressLine2 += " " + pdfDataContract.Tf98fCountry;
                }

                // Initialize the SSN
                entity.SSN = "";
                if (personContract != null && !string.IsNullOrEmpty(personContract.Ssn))
                {
                    entity.SSN = personContract.Ssn;
                }

                if (parm1098Contract != null)
                {
                    // Mask the SSN if necessary.
                    if (!string.IsNullOrEmpty(parm1098Contract.P1098MaskSsn) && parm1098Contract.P1098MaskSsn.ToUpper() == "Y")
                    {
                        if (!string.IsNullOrEmpty(entity.SSN))
                        {
                            // Mask SSN
                            if (entity.SSN.Length >= 4)
                            {
                                entity.SSN = "XXX-XX-" + entity.SSN.Substring(entity.SSN.Length - 4);
                            }
                            else
                            {
                                entity.SSN = "XXX-XX-" + entity.SSN;
                            }
                        }
                    }

                    entity.AtLeastHalfTime = false;

                    // Institution data
                    entity.InstitutionId = pdfDataContract.Tf98fInstitution;

                    if (corpContract.CorpName == null || !corpContract.CorpName.Any())
                    {
                        throw new ApplicationException("Institution must have a name.");
                    }

                    entity.InstitutionName = String.Join(" ", corpContract.CorpName.Where(x => !string.IsNullOrEmpty(x)));

                    // Institution phone number
                    if (!string.IsNullOrEmpty(parm1098Contract.P1098TInstPhone))
                    {
                        entity.InstitutionPhoneNumber = parm1098Contract.P1098TInstPhone;

                        if (!string.IsNullOrEmpty(parm1098Contract.P1098TInstPhoneExt))
                        {
                            entity.InstitutionPhoneNumber += " Ext. " + parm1098Contract.P1098TInstPhoneExt;
                        }
                    }

                    // Box 2
                    var boxCode = boxCodesDataContracts.FirstOrDefault(x => x.BxcBoxNumber == "2");
                    if (boxCode != null)
                    {
                        var boxData = form1098BoxDataContracts.Where(x => x.Tf98bBoxCode == boxCode.Recordkey).ToList();
                        entity.AmountsBilledForTuitionAndExpenses = boxData.Sum(x => x.Tf98bAmt ?? 0).ToString("C");
                    }

                    // Box 3
                    entity.ReportingMethodHasBeenChanged = false;
                    var changedReportingMethod = parm1098Contract.P1098TYearInfoEntityAssociation.First(x =>
                        x.P1098TYearsAssocMember.HasValue && x.P1098TYearsAssocMember.ToString() == entity.TaxYear);
                    if (changedReportingMethod != null && !string.IsNullOrEmpty(changedReportingMethod.P1098TYrChgRptMethsAssocMember))
                    {
                        entity.ReportingMethodHasBeenChanged = changedReportingMethod.P1098TYrChgRptMethsAssocMember.ToUpper() == "Y";
                    }

                    // Box 4
                    boxCode = boxCodesDataContracts.FirstOrDefault(x => x.BxcBoxNumber == "4");
                    if (boxCode != null)
                    {
                        var boxData = form1098BoxDataContracts.Where(x => x.Tf98bBoxCode == boxCode.Recordkey
                            && x.Tf98bBoxCode == parm1098Contract.P1098TRefundBoxCode).ToList();
                        entity.AdjustmentsForPriorYear = boxData.Sum(x => x.Tf98bAmt ?? 0).ToString("C");
                    }

                    // Box 5
                    boxCode = boxCodesDataContracts.FirstOrDefault(x => x.BxcBoxNumber == "5");
                    if (boxCode != null)
                    {
                        var boxData = form1098BoxDataContracts.Where(x => x.Tf98bBoxCode == boxCode.Recordkey
                            && x.Tf98bBoxCode == parm1098Contract.P1098TFaBoxCode).ToList();
                        entity.ScholarshipsOrGrants = boxData.Sum(x => x.Tf98bAmt ?? 0).ToString("C");
                    }

                    // Box 6
                    boxCode = boxCodesDataContracts.FirstOrDefault(x => x.BxcBoxNumber == "6");
                    if (boxCode != null)
                    {
                        var boxData = form1098BoxDataContracts.Where(x => x.Tf98bBoxCode == boxCode.Recordkey
                            && x.Tf98bBoxCode == parm1098Contract.P1098TFaRefBoxCode).ToList();
                        entity.AdjustmentsToScholarshipsOrGrantsForPriorYear = boxData.Sum(x => x.Tf98bAmt ?? 0).ToString("C");
                    }

                    // Box 7
                    boxCode = boxCodesDataContracts.FirstOrDefault(x => x.BxcBoxNumber == "7");
                    if (boxCode != null)
                    {
                        var boxData = form1098BoxDataContracts.FirstOrDefault(x => x.Tf98bBoxCode == boxCode.Recordkey);
                        if (boxData != null && boxData.Tf98bBoxCode == parm1098Contract.P1098TNewYrBoxCode)
                        {
                            entity.AmountsBilledAndReceivedForQ1Period = boxData.Tf98bValue.ToUpper() == "X";
                        }
                    }

                    // Box 8
                    boxCode = boxCodesDataContracts.FirstOrDefault(x => x.BxcBoxNumber == "8");
                    if (boxCode != null)
                    {
                        var boxData = form1098BoxDataContracts.FirstOrDefault(x => x.Tf98bBoxCode == boxCode.Recordkey);
                        if (boxData != null && parm1098Contract.P1098TLoadBoxCode == boxData.Tf98bBoxCode)
                        {
                            entity.AtLeastHalfTime = boxData.Tf98bValue.ToUpper() == "X";
                        }
                    }

                    // Box 9
                    boxCode = boxCodesDataContracts.FirstOrDefault(x => x.BxcBoxNumber == "9");
                    if (boxCode != null)
                    {
                        var boxData = form1098BoxDataContracts.FirstOrDefault(x => x.Tf98bBoxCode == boxCode.Recordkey);
                        if (boxData != null && parm1098Contract.P1098TGradBoxCode == boxData.Tf98bBoxCode)
                        {
                            entity.IsGradStudent = boxData.Tf98bValue.ToUpper() == "X";
                        }
                    }
                }

                // Call the PDF accessed CTX to trigger an email notification
                TxUpdate1098AccessTriggerRequest request = new TxUpdate1098AccessTriggerRequest();
                request.TaxFormPdfId = recordId;
                var response = await transactionInvoker.ExecuteAsync<TxUpdate1098AccessTriggerRequest, TxUpdate1098AccessTriggerResponse>(request);
                
                return entity;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
