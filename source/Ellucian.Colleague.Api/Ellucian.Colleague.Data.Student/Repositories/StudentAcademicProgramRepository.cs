﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Ellucian.Colleague.Data.Student.DataContracts;
using Ellucian.Colleague.Data.Student.Transactions;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Entities.Requirements;
using Ellucian.Colleague.Domain.Student.Entities.Requirements.Modifications;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.DataContracts;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Dmi.Runtime;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Web.Http.Configuration;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.Entities;
using System.Text;

namespace Ellucian.Colleague.Data.Student.Repositories
{
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class StudentAcademicProgramRepository : BaseColleagueRepository, IStudentAcademicProgramRepository
    {
        private ApplValcodes studentProgramStatuses;
                public StudentAcademicProgramRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings apiSettings)
            : base(cacheProvider, transactionFactory, logger)
        {

        }
         /// <summary>
        /// Gets Student Academic Programs for a guid
        /// </summary>
        /// <param name="id">Student Academic Programs GUID</param>
        /// <param name="defaultInstitutionId">Default Institution ID to get Acad Credentials record</param>
        /// <returns>Returns StudentAcademicProgram</returns>
        public async Task<StudentAcademicProgram> GetStudentAcademicProgramByGuidAsync(string id, string defaultInstitutionId)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id", "id must be specified.");
            }

            var studentProgramsId = await GetRecordKeyFromGuidAsync(id);
            try
            {
                if (string.IsNullOrEmpty(studentProgramsId))
                {
                    throw new ArgumentNullException("studentProgramsId");
                }

                StudentPrograms stuprog = await DataReader.ReadRecordAsync<StudentPrograms>(studentProgramsId);
                if (stuprog == null)
                {
                    throw new KeyNotFoundException("Invalid Student Programs ID: " + studentProgramsId);
                }
                var studentProg = new Collection<StudentPrograms>() { stuprog };
                var stuprogs = await BuildStudentAcademicProgramsAsync(studentProg, defaultInstitutionId);
                return stuprogs.FirstOrDefault();
            }
            catch (Exception)
            {
                throw new KeyNotFoundException("No Student Academic Program was found for guid " + id);
            }
        }


        /// <summary>
        /// Creates a new Student Academic Program
        /// </summary>
        /// <param name="stuAcadProg">Student Academic Program to be created</param>        
        /// <param name="defaultInstitutionId">Default Institution ID to get Acad Credentials record</param>
        /// <returns>Created Academic Program Entity entity</returns>
        public async Task<StudentAcademicProgram> CreateStudentAcademicProgramAsync(StudentAcademicProgram stuAcadProg, string defaultInstitutionId)
        {
            return await MaintainStudentAcademicProgramAsync(stuAcadProg, false, defaultInstitutionId);
        }

        /// <summary>
        /// Updates an existing Student Academic Program
        /// </summary>
        /// <param name="stuAcadProg">Student Academic Program to be updated</param>
        /// <param name="defaultInstitutionId">Default Institution ID to get Acad Credentials record</param>
        /// <returns>Updated Academic Program Entity entity</returns>
        public async Task<StudentAcademicProgram> UpdateStudentAcademicProgramAsync(StudentAcademicProgram stuAcadProg, string defaultInstitutionId)
        {
            return await MaintainStudentAcademicProgramAsync(stuAcadProg, true, defaultInstitutionId);
        }

        /// <summary>
        /// Creates or updates a Student Academic Program
        /// </summary>
        /// <param name="stuAcadProg">Student Academic Program to be maintained</param>
        /// <param name="isUpdating">Indicates whether an Student Academic Program is being updated</param>        
        /// <param name="defaultInstitutionId">Default Institution ID to get Acad Credentials record</param>
        /// <returns>Created or updated Student Academic Program entity</returns>
        private async Task<StudentAcademicProgram> MaintainStudentAcademicProgramAsync(StudentAcademicProgram stuAcadProg, bool isUpdating, string defaultInstitutionId)
        {
            if (stuAcadProg == null)
            {
                throw new ArgumentNullException(" Student Academic Program", " Student Academic Program must be provided.");
            }

            string studentProgId = null;
            if (isUpdating)
            {
                if (!string.IsNullOrEmpty(stuAcadProg.Guid))
                {
                    studentProgId = GetRecordKeyFromGuid(stuAcadProg.Guid);
                    if (string.IsNullOrEmpty(studentProgId))
                    {
                        isUpdating = false;
                    }
                }
                else
                {
                    isUpdating = false;
                }
            }
           
            var request = new UpdateStuAcadProgramRequest()
            {
                StuProgGuid = stuAcadProg.Guid,
                AcadProg = stuAcadProg.ProgramCode,
                Catalog = stuAcadProg.CatalogCode,
                StudentId = stuAcadProg.StudentId,
                degrees = stuAcadProg.DegreeCode,
                ccds = stuAcadProg.StudentProgramCcds,
                Majors = stuAcadProg.StudentProgramMajors,
                Minors = stuAcadProg.StudentProgramMinors,
                Specializations = stuAcadProg.StudentProgramSpecializations,
                StartDate = stuAcadProg.StartDate,
                EndDate = stuAcadProg.EndDate,
                Status = stuAcadProg.Status,
                Location = stuAcadProg.Location,
                StartTerm = stuAcadProg.StartTerm,
                AcademicLevel = stuAcadProg.AcademicLevelCode,
                Dept = stuAcadProg.DepartmentCode,
                Gpa = stuAcadProg.GradGPA,
                GradDate = stuAcadProg.GraduationDate,
                CredDate = stuAcadProg.CredentialsDate,
                ThesisTitle = stuAcadProg.ThesisTitle,
                Honors = stuAcadProg.StudentProgramHonors,
                CreditEarned = stuAcadProg.CreditsEarned
            };

            var response = await transactionInvoker.ExecuteAsync<UpdateStuAcadProgramRequest, UpdateStuAcadProgramResponse>(request);

            if (response.Error)
            {
                var exception = new RepositoryException("Errors encountered while updating student programs " + request.StudentId + "*" + request.AcadProg);
                foreach (var error in response.UpdateStuAcadProgramError)
                {
                    if (!string.IsNullOrEmpty(error.ErrorCode))
                    {
                        exception.AddError(new RepositoryError(error.ErrorCode, error.ErrorMessage));
                    }
                    else
                    {
                        exception.AddError(new RepositoryError(error.ErrorMessage));
                    }

               }
                throw exception;
            }

            // Create the new object to be returned
            var createdAcad = await GetStudentAcademicProgramByGuidAsync(response.StuProgGuid, defaultInstitutionId);

            return createdAcad;
        }

        /// <summary>
        /// Return a Unidata Formatted Date string from an input argument of string type
        /// </summary>
        /// <param name="date">String representing a Date</param>
        /// <returns>Unidata formatted Date string for use in Colleague Selection.</returns>
        public async Task<string> GetUnidataFormattedDate(string date)
        {
            var internationalParameters = await GetInternationalParametersAsync();
            var newDate = DateTime.Parse(date).Date;
            return UniDataFormatter.UnidataFormatDate(newDate, internationalParameters.HostShortDateFormat, internationalParameters.HostDateDelimiter);
        }

        /// <summary>
        /// Returns studentProgram Entity as per the select criteria
        /// </summary>
        /// <param name="defaultInstitutionId">Default Institution ID to get Acad Credentials record</param>
        ///  <param name="program">academic program Name Contains ...program...</param>
        ///  <param name="startDate">Student Academic Program starts on or after this date</param>
        ///  <param name="endDate">Student Academic Program ends on or before this date</param>
        /// <param name="student">Id of the student enrolled on the academic program</param>
        /// <param name="catalog">Student Academic Program catalog  equal to</param>
        /// <param name="Status">Student Academic Program status equals to </param>
        /// <param name="programOwner">Student Academic Program programOwner equals to </param>
        /// <param name="site">Student Academic Program site equals to </param>
        /// <param name="graduatedOn">Student Academic Program graduatedOn equals to </param>
        /// <param name="ccdCredential">Student Academic Program ccdCredential equals to </param>
        /// <param name="degreeCredential">Student Academic Program degreeCredential equals to </param>
        /// <returns>StudentProgram Entities</returns>
        public async Task<Tuple<IEnumerable<StudentAcademicProgram>, int>> GetStudentAcademicProgramsAsync(string defaultInstitutionId, int offset, int limit, bool bypassCache = false, 
            string program = "", string startDate = "", string endDate = "", string student = "", string catalog = "", string status = "",
            string programOwner = "", string site = "", string academicLevel = "", string graduatedOn = "", string ccdCredential = "", string degreeCredential = "", string graduatedAcademicPeriod = "", string completeStatus = "")
        {
            try
            {
                string criteria = "WITH STPR.STUDENT NE '' AND STPR.ACAD.PROGRAM NE '' AND STPR.START.DATE NE ''";
                var oldCriteria = criteria;
                string acadProgCriteria = string.Empty;
                string acadCredProgCriteria = string.Empty;
                string acadCredCriteria = string.Empty;
                List<string> acadCredStuProgIds = new List<string>();
                string[] studentProgramIds = new string[] { };
                string[] acadStuProgIds = new string[] { };
                string[] stuProgIds = new string[] { };
                if (!string.IsNullOrEmpty(program))
                {
                    criteria += " AND WITH STPR.ACAD.PROGRAM EQ '" + program + "'";
                }
                if (!string.IsNullOrEmpty(startDate))
                {
                    criteria += " AND WITH STPR.LATEST.START.DATE GE '" + startDate + "'";
                }
                if (!string.IsNullOrEmpty(endDate))
                {
                    criteria += " AND WITH STPR.CURRENT.END.DATE NE '' AND WITH STPR.CURRENT.END.DATE LE '" + endDate + "'";
                }
                if (!string.IsNullOrEmpty(student))
                {
                    criteria += " AND WITH STPR.STUDENT EQ '" + student + "'";
                }
                if (!string.IsNullOrEmpty(catalog))
                {
                    criteria += " AND WITH STPR.CATALOG EQ '" + catalog + "'";
                }
                if (!string.IsNullOrEmpty(status))
                {
                    criteria += " AND WITH STPR.CURRENT.STATUS EQ " + status ;
                }
                if (!string.IsNullOrEmpty(programOwner))
                {
                    criteria += " AND WITH STPR.DEPT EQ '" + programOwner + "'";
                }
                if (!string.IsNullOrEmpty(site))
                {
                    criteria += " AND WITH STPR.LOCATION EQ '" + site + "'";
                }
                if (!string.IsNullOrEmpty(academicLevel))
                {
                    criteria += " AND WITH STPR.ACAD.LEVEL EQ '" + academicLevel + "'";
                }
                if (!string.IsNullOrEmpty(graduatedOn))
                {
                    if (string.IsNullOrEmpty(acadCredProgCriteria))
                        acadCredProgCriteria += "WITH ACAD.END.DATE EQ '" + graduatedOn + "'";
                }
                if (!string.IsNullOrEmpty(graduatedAcademicPeriod))
                {
                    if (string.IsNullOrEmpty(acadCredProgCriteria))
                        acadCredProgCriteria += "WITH ACAD.TERM EQ '" + graduatedAcademicPeriod + "'";
                    else
                        acadCredProgCriteria += " AND WITH ACAD.TERM EQ '" + graduatedAcademicPeriod + "'";
                }
                if (!string.IsNullOrEmpty(ccdCredential))
                {
                    if (string.IsNullOrEmpty(acadProgCriteria))
                        acadProgCriteria += "WITH ACPG.CCDS EQ '" + ccdCredential + "'";
                    acadCredCriteria += "WITH ACAD.DEGREE.DATE NE '' AND WITH ACAD.CCD EQ '" + ccdCredential + "'";
                }
                if (!string.IsNullOrEmpty(degreeCredential))
                {
                    if (string.IsNullOrEmpty(acadProgCriteria))
                        acadProgCriteria += "WITH ACPG.DEGREE EQ '" + degreeCredential + "'";
                    else
                        acadProgCriteria += " AND WITH ACPG.DEGREE EQ '" + degreeCredential + "'";
                    if (string.IsNullOrEmpty(acadCredCriteria))
                        acadCredCriteria += "WITH ACAD.DEGREE.DATE NE '' AND WITH ACAD.DEGREE EQ '" + degreeCredential + "'";
                    else
                        acadCredCriteria += " AND WITH ACAD.DEGREE EQ '" + degreeCredential + "'";
                }

                //get student Program IDs from ACAD.CREDENTIALS
                if (!string.IsNullOrEmpty(acadCredProgCriteria))
                {
                    var acadCredProgIds = await DataReader.SelectAsync("ACAD.CREDENTIALS", acadCredProgCriteria);
                    if (acadCredProgIds.Any())
                    {
                        var acadCredData = await DataReader.BulkReadRecordAsync<AcadCredentials>("ACAD.CREDENTIALS", acadCredProgIds);
                        if (acadCredData.Any())
                        {
                            foreach (var cred in acadCredData)
                            {
                                if (!string.IsNullOrEmpty(cred.AcadAcadProgram))
                                    acadCredStuProgIds.Add(cred.AcadPersonId + "*" + cred.AcadAcadProgram);
                            }
                        }
                    }
                }
                //get student program IDs from ACAD.PROGRAMS
                if (!string.IsNullOrEmpty(acadProgCriteria))
                {
                    var acadProgramIds = await DataReader.SelectAsync("ACAD.PROGRAMS", acadProgCriteria);
                    if (acadProgramIds.Any())
                    {
                        // we are excluding those student programs that have graduated
                        var newcriteria = oldCriteria + " AND WITH STPR.CURRENT.STATUS NE " + completeStatus + " AND WITH STPR.ACAD.PROGRAM EQ '?'";
                        acadStuProgIds = await DataReader.SelectAsync("STUDENT.PROGRAMS", newcriteria, acadProgramIds);
                    }
                    if (!string.IsNullOrEmpty(ccdCredential))
                    {
                        // we are excluding those student programs that have graduated
                        var ccdCriteria = oldCriteria + " AND WITH STPR.CURRENT.STATUS NE " + completeStatus + " AND WITH STPR.CCDS EQ '" + ccdCredential + "'";
                        var stuProgCcds = await DataReader.SelectAsync("STUDENT.PROGRAMS", ccdCriteria);
                        acadStuProgIds = acadStuProgIds.Union(stuProgCcds).ToArray();

                    }
                    //we need to include the ACAD.CREDENTIALS record for those who have graduated.
                    if (!string.IsNullOrEmpty(acadCredCriteria))
                    {
                        var acadCredProgIds = await DataReader.SelectAsync("ACAD.CREDENTIALS", acadCredCriteria);
                        List<string> acadCredStuProgs = new List<string>();
                        if (acadCredProgIds.Any())
                        {
                            var acadCredData = await DataReader.BulkReadRecordAsync<AcadCredentials>("ACAD.CREDENTIALS", acadCredProgIds);
                            if (acadCredData.Any())
                            {
                                foreach (var cred in acadCredData)
                                {
                                    if (!string.IsNullOrEmpty(cred.AcadAcadProgram))
                                        acadCredStuProgs.Add(cred.AcadPersonId + "*" + cred.AcadAcadProgram);
                                }
                                acadStuProgIds = acadStuProgIds.Union(acadCredStuProgs).ToArray();
                            }
                        }
                    }

                }
               
                //at this stage, we have student program IDs from ACAD.PROGRAMS & ACAD.CREDENTIALS
                //for GET ALL without any filter, the old criteria will be same as criteria and there is no 

                if (!acadCredStuProgIds.Any() && criteria.Equals(oldCriteria) && !acadStuProgIds.Any())
                {
                    studentProgramIds = await DataReader.SelectAsync("STUDENT.PROGRAMS", criteria);
                }
                else
                {
                    if(!criteria.Equals(oldCriteria))
                    {
                        stuProgIds = await DataReader.SelectAsync("STUDENT.PROGRAMS", criteria);
                        //bypassCache = true;
                    }
                }
                //intersect all the student program ids that we have got so far
                if (stuProgIds.Any())
                {
                    studentProgramIds = studentProgramIds.Union(stuProgIds).ToArray();
                }
                if (!string.IsNullOrEmpty(acadCredProgCriteria))
                {
                    if (studentProgramIds.Any())
                        studentProgramIds = studentProgramIds.Intersect(acadCredStuProgIds).ToArray();
                    else
                        studentProgramIds = studentProgramIds.Union(acadCredStuProgIds).ToArray();
                   if (!string.IsNullOrEmpty(acadProgCriteria))
                       studentProgramIds = studentProgramIds.Intersect(acadStuProgIds).ToArray();

                }
                else if (!string.IsNullOrEmpty(acadProgCriteria))
                {
                    if (studentProgramIds.Any())
                        studentProgramIds = studentProgramIds.Intersect(acadStuProgIds).ToArray();
                    else
                        studentProgramIds = studentProgramIds.Union(acadStuProgIds).ToArray();
                }
                var totalCount = studentProgramIds.Count();
                Array.Sort(studentProgramIds);
                var subList = studentProgramIds.Skip(offset).Take(limit).ToArray();
                var studentProgramData = await DataReader.BulkReadRecordAsync<StudentPrograms>("STUDENT.PROGRAMS", subList);
                var studentProgEntities = await BuildStudentAcademicProgramsAsync(studentProgramData, defaultInstitutionId);
                return new Tuple<IEnumerable<StudentAcademicProgram>, int>(studentProgEntities, totalCount);
               
            }
            catch (RepositoryException e)
            {
                throw e;
            }

        }

        /// <summary>
        /// Returns Student.Program.Statuses valcode data
        /// </summary>
        /// <returns>Student.Program.Statuses Valcode Data contract.</returns>
        private async Task<ApplValcodes> GetStudentProgramStatusesAsync()
        {
            if (studentProgramStatuses != null)
            {
                return studentProgramStatuses;
            }

            // Overriding cache timeout to be 240.
            studentProgramStatuses = await GetOrAddToCacheAsync<ApplValcodes>("StudentProgramStatuses",
                async () =>
                {
                    ApplValcodes statusesTable = await DataReader.ReadRecordAsync<ApplValcodes>("ST.VALCODES", "STUDENT.PROGRAM.STATUSES");
                    if (statusesTable == null)
                    {
                        var errorMessage = "Unable to access STUDENT.PROGRAM.STATUSES valcode table.";
                        logger.Info(errorMessage);
                        throw new Exception(errorMessage);
                    }
                    return statusesTable;
                }, Level1CacheTimeoutValue);
            return studentProgramStatuses;
        }

        /// <summary>
        /// Builds StudentProgram Entity
        /// </summary>
        /// <param name="studentProgramData">Student Programs Data contracts</param>
        /// <param name="defaultInstitutionId">Default Institution ID to get Acad Credentials record</param>
        /// <returns>Returns StudentProgram</returns>
        private async Task<IEnumerable<StudentAcademicProgram>> BuildStudentAcademicProgramsAsync(Collection<StudentPrograms> studentProgramData, string defaultInstitutionId)
        {
            //get needed reference data
            List<StudentAcademicProgram> stuAcadPrograms = new List<StudentAcademicProgram>();

            //if no studentacadprograms passed in return empty list
            if (!studentProgramData.Any())
            {
                return stuAcadPrograms;
            }

            //get academic program data for all the programs in the list
            string[] progCodes = studentProgramData.Select(p => p.Recordkey).Where(x => !string.IsNullOrEmpty(x) && x.Contains("*")).Select(x => x.Split('*')[1]).Where(y => !string.IsNullOrEmpty(y)).ToArray();
            Collection<AcadPrograms> acadProgramCollection = new Collection<AcadPrograms>();
            if (progCodes != null && progCodes.Any())
            {
                acadProgramCollection = await DataReader.BulkReadRecordAsync<AcadPrograms>(progCodes);
            }
            //get the list of student IDs
            string[] studentIds = studentProgramData.Select(p => p.Recordkey).Where(x => !string.IsNullOrEmpty(x) && x.Contains("*")).Select(x => x.Split('*')[0]).Where(y => !string.IsNullOrEmpty(y)).ToArray();
            //get the list of acad data contract objects
            Collection<AcadCredentials> acadCredentialsData = new Collection<AcadCredentials>();
            string[] acadCredIdListStu = null;
            string[] acadCredIdListProg = null;
            string[] acadCredIdList = null;
            
            if (studentIds.Any())
            {
                string studentcriteria = "WITH ACAD.PERSON.ID EQ '?'";
                acadCredIdListStu = await DataReader.SelectAsync("ACAD.CREDENTIALS", studentcriteria, studentIds);
            }
            if (progCodes.Any())
            {
                string programcriteria = "WITH ACAD.ACAD.PROGRAM EQ '?'";
                acadCredIdListProg = await DataReader.SelectAsync("ACAD.CREDENTIALS", programcriteria, progCodes);
            }
            acadCredIdList = acadCredIdListStu.Union(acadCredIdListProg).ToArray();
            if (!string.IsNullOrEmpty(defaultInstitutionId) && acadCredIdList.Any())
            {
                string defaultInstCriteria = "WITH ACAD.INSTITUTIONS.ID EQ '" + defaultInstitutionId + "'";
                acadCredIdList = await DataReader.SelectAsync("ACAD.CREDENTIALS", acadCredIdList, defaultInstCriteria);
               
            }
           
            if (acadCredIdList.Any())
            {
                acadCredentialsData = await DataReader.BulkReadRecordAsync<AcadCredentials>("ACAD.CREDENTIALS", acadCredIdList);
            }
             
           //process each of the student program records           
           
            foreach (var stuProg in studentProgramData)
            {
                try
                {
                    string catcode = stuProg.StprCatalog;
                    string studentid = stuProg.Recordkey.Split('*')[0];
                    string progcode = stuProg.Recordkey.Split('*')[1];
                    string guid = stuProg.RecordGuid;
                    string status = string.Empty;
                    if (stuProg.StprStatus.Any())
                    {
                        status = stuProg.StprStatus.ElementAt(0);
                    }
                    else
                    {
                        //in Colleague, if status is missing, it defaults to active if there is no end date and to withdrawn if there is end date.
                        if (stuProg.StprEndDate.Any())
                        {
                            if (stuProg.StprEndDate.ElementAt(0) != null)
                            {
                                var codeAssoc = (await GetStudentProgramStatusesAsync()).ValsEntityAssociation.FirstOrDefault(v => v.ValActionCode1AssocMember == "4");
                                if (codeAssoc != null)
                                {
                                    status = codeAssoc.ValInternalCodeAssocMember;
                                }
                            }
                            else
                            {
                                var codeAssoc = (await GetStudentProgramStatusesAsync()).ValsEntityAssociation.FirstOrDefault(v => v.ValActionCode1AssocMember == "2");
                                if (codeAssoc != null)
                                {
                                    status = codeAssoc.ValInternalCodeAssocMember;
                                }
                            }
                        }
                        else
                        {
                            var codeAssoc = (await GetStudentProgramStatusesAsync()).ValsEntityAssociation.FirstOrDefault(v => v.ValActionCode1AssocMember == "2");
                            if (codeAssoc != null)
                            {
                                status = codeAssoc.ValInternalCodeAssocMember;
                            }
                        }
                    }
                    DateTime startDate = new DateTime();
                    if (stuProg.StprStartDate != null && stuProg.StprStartDate.Any())
                    {
                        var studentProgramStartDate = stuProg.StprStartDate.ElementAt(0);
                        if (studentProgramStartDate != null && studentProgramStartDate != DateTime.MinValue)
                        {
                            startDate = studentProgramStartDate.Value;
                        }
                    }

                    StudentAcademicProgram studentAcadProgEntity = new StudentAcademicProgram(studentid, progcode, catcode, guid, startDate, status);
                    studentAcadProgEntity.AnticipatedCompletionDate = stuProg.StprAntCmplDate;
                    if (stuProg.StprEndDate != null && stuProg.StprEndDate.Any())
                    {
                        var studentProgramEndDate = stuProg.StprEndDate.ElementAt(0);
                        if (studentProgramEndDate != null && studentProgramEndDate != DateTime.MinValue)
                        {
                            studentAcadProgEntity.EndDate = studentProgramEndDate;
                        }
                    }
                    studentAcadProgEntity.DepartmentCode = stuProg.StprDept;
                    studentAcadProgEntity.Location = stuProg.StprLocation;
                    studentAcadProgEntity.StartTerm = stuProg.StprIntgStartTerm;
                    // get data from ACAD.CREDENTIALS. If the student has already graduated and has a record in ACAD.CREDENTIALS
                    // then we need to display the credentials and disciplines from ACAD.CREDENTIALS instead of ACAD.PROGRAMS and STUDENT.PROGRAMS
                    var acadCredential = acadCredentialsData.FirstOrDefault(cred => cred.AcadPersonId == studentid && cred.AcadAcadProgram == progcode);
                    if (acadCredential != null)
                    {
                        foreach (var honor in acadCredential.AcadHonors)
                        {
                            studentAcadProgEntity.AddHonors(honor);
                        }
                        studentAcadProgEntity.GradGPA = acadCredential.AcadGpa;
                        studentAcadProgEntity.GraduationDate = acadCredential.AcadEndDate;
                        studentAcadProgEntity.CredentialsDate = acadCredential.AcadDegreeDate;
                        studentAcadProgEntity.ThesisTitle = acadCredential.AcadThesis;
                        studentAcadProgEntity.DegreeCode = acadCredential.AcadDegree;
                        // Add majors from the Academic Credentials
                        if (acadCredential.AcadMajors.Any())
                        {
                            foreach (var mjr in acadCredential.AcadMajors)
                            {
                                studentAcadProgEntity.AddMajors(mjr);
                            }
                        }
                        // Add minors from the Academic Credentials
                        if (acadCredential.AcadMinors.Any())
                        {
                            foreach (var minr in acadCredential.AcadMinors)
                            {
                                studentAcadProgEntity.AddMinors(minr);
                            }
                        }
                        // Add specializations from the Academic Credentials
                        if (acadCredential.AcadSpecialization.Any())
                        {
                            foreach (var sp in acadCredential.AcadSpecialization)
                            {
                                studentAcadProgEntity.AddSpecializations(sp);
                            }
                        }
                        // Add ccds from the Academic Credentials
                        if (acadCredential.AcadCcd.Any())
                        {
                            foreach (var ccd in acadCredential.AcadCcd)
                            {
                                studentAcadProgEntity.AddCcds(ccd);
                            }
                        }

                    }
                    else
                    {

                        AcadPrograms acadProgramData = acadProgramCollection.FirstOrDefault(a => a.Recordkey == progcode);
                        if (acadProgramData != null)
                        {
                            studentAcadProgEntity.AcademicLevelCode = acadProgramData.AcpgAcadLevel;
                            studentAcadProgEntity.DegreeCode = acadProgramData.AcpgDegree;
                            // Add majors from the Academic Program
                            if (acadProgramData.AcpgMajors.Any())
                            {
                                foreach (var mjr in acadProgramData.AcpgMajors)
                                {
                                    studentAcadProgEntity.AddMajors(mjr);
                                }
                            }
                            // Add minors from the Academic Program
                            if (acadProgramData.AcpgMinors.Any())
                            {
                                foreach (var minr in acadProgramData.AcpgMinors)
                                {
                                    studentAcadProgEntity.AddMinors(minr);
                                }
                            }
                            // Add specializations from the Academic Program
                            if (acadProgramData.AcpgSpecializations.Any())
                            {
                                foreach (var sp in acadProgramData.AcpgSpecializations)
                                {
                                    studentAcadProgEntity.AddSpecializations(sp);
                                }
                            }
                            // Add ccds from the Academic Program
                            if (acadProgramData.AcpgCcds.Any())
                            {
                                foreach (var ccd in acadProgramData.AcpgCcds)
                                {
                                    studentAcadProgEntity.AddCcds(ccd);
                                }
                            }
                        }

                        // Additional Requirements from student programs
                        // Add majors
                        if (stuProg.StprMajorListEntityAssociation.Any())
                        {
                            foreach (var mjr in stuProg.StprMajorListEntityAssociation)
                            {
                                if (mjr.StprAddnlMajorStartDateAssocMember <= DateTime.Now && mjr.StprAddnlMajorEndDateAssocMember == null || mjr.StprAddnlMajorEndDateAssocMember > DateTime.Now)
                                {
                                    studentAcadProgEntity.AddMajors(mjr.StprAddnlMajorsAssocMember);
                                }
                            }
                        }
                        // Add minors 
                        if (stuProg.StprMinorListEntityAssociation.Any())
                        {
                            foreach (var minr in stuProg.StprMinorListEntityAssociation)
                            {
                                if (minr.StprMinorStartDateAssocMember <= DateTime.Now && minr.StprMinorEndDateAssocMember == null || minr.StprMinorEndDateAssocMember > DateTime.Now)
                                {
                                    studentAcadProgEntity.AddMinors(minr.StprMinorsAssocMember);
                                }
                            }
                        }
                        // Add specializations 
                        if (stuProg.StprSpecialtiesEntityAssociation.Any())
                        {
                            foreach (var sps in stuProg.StprSpecialtiesEntityAssociation)
                            {
                                if (sps.StprSpecializationStartAssocMember <= DateTime.Now && sps.StprSpecializationEndAssocMember == null || sps.StprSpecializationEndAssocMember > DateTime.Now)
                                {
                                    studentAcadProgEntity.AddSpecializations(sps.StprSpecializationsAssocMember);
                                }
                            }
                        }
                        // Add ccds 
                        if (stuProg.StprCcdListEntityAssociation.Any())
                        {
                            foreach (var ccd in stuProg.StprCcdListEntityAssociation)
                            {
                                if (ccd.StprCcdsStartDateAssocMember <= DateTime.Now && ccd.StprCcdsEndDateAssocMember == null || ccd.StprCcdsEndDateAssocMember > DateTime.Now)
                                {
                                    studentAcadProgEntity.AddCcds(ccd.StprCcdsAssocMember);
                                }
                            }
                        }
                    }
                    

                    //get credits earned
                    studentAcadProgEntity.CreditsEarned = stuProg.StprEvalCombinedCred;
                    stuAcadPrograms.Add(studentAcadProgEntity);
                }
                catch (ArgumentException e)
                {
                    throw e;
                }
                catch (Exception e)
                {
                    throw new RepositoryException(string.Format("Could not build program {0} for student {1}", stuProg.Recordkey.Split('*')[1], stuProg.Recordkey.Split('*')[0]));
                }
            }
            return stuAcadPrograms;
        }
    }
}
