﻿// Copyright 2017 Ellucian Company L.P. and its affiliates

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Student.DataContracts;
using Ellucian.Colleague.Data.Student.Transactions;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;
using System.Text;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.Entities;

namespace Ellucian.Colleague.Data.Student.Repositories
{
    /// <summary>
    /// Implement the IStudentPaymentsRepository interface
    /// </summary>
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class StudentPaymentRepository : BaseColleagueRepository, IStudentPaymentRepository
    {
        /// <summary>
        /// Constructor to instantiate a student payments repository object
        /// </summary>
        /// <param name="cacheProvider">Pass in an ICacheProvider object</param>
        /// <param name="transactionFactory">Pass in an IColleagueTransactionFactory object</param>
        /// <param name="logger">Pass in an ILogger object</param>
        public StudentPaymentRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {

        }

        /// <summary>
        /// Get the StudentPayments requested
        /// </summary>
        /// <param name="id">StudentPayments GUID</param>
        /// <exception cref="ArgumentNullException">Thrown if the id argument is null or empty</exception>
        /// <exception cref="KeyNotFoundException">Thrown if no database records exist for the given id argument</exception>
        public async Task<StudentPayment> GetByIdAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }

            // Read the INTG.GL.POSTINGS record
            var recordInfo = await GetRecordInfoFromGuidAsync(id);
            if (recordInfo == null || string.IsNullOrEmpty(recordInfo.PrimaryKey) || recordInfo.Entity != "AR.PAY.ITEMS.INTG")
            {
                throw new KeyNotFoundException(string.Format("No student payment was found for guid '{0}'. ", id));
            }
            var intgStudentPayments = await DataReader.ReadRecordAsync<ArPayItemsIntg>(recordInfo.PrimaryKey);
            {
                if (intgStudentPayments == null)
                {
                    throw new KeyNotFoundException(string.Format("No student payment was found for guid '{0}'. ", id));
                }
            }

            return BuildStudentPayment(intgStudentPayments);
        }

        /// <summary>
        /// Get student payments for specific filters.
        /// </summary>
        /// <param name="offset">Offset for paging results</param>
        /// <param name="limit">Limit for paging results</param>
        /// <param name="bypassCache">Flag to bypass cache</param>
        /// <param name="personId">The person or student ID</param>
        /// <returns>A list of StudentPayment domain entities</returns>
        /// <exception cref="ArgumentNullException">Thrown if the id argument is null or empty</exception>
        /// <exception cref="KeyNotFoundException">Thrown if no database records exist for the given id argument</exception>
        public async Task<Tuple<IEnumerable<StudentPayment>,int>> GetAsync(int offset, int limit, bool bypassCache, string personId = "", string term = "", string arCode = "", string paymentType = "")
        {
            var intgStudentPaymentsEntities = new List<StudentPayment>();
            var criteria = new StringBuilder();
            // Read the AR.PAY.ITEMS.INTG records
            if (!string.IsNullOrEmpty(personId))
            {
                criteria.AppendFormat("WITH ARP.INTG.PERSON.ID = '{0}'", personId);
            }
            if (!string.IsNullOrEmpty(term))
            {
                if (criteria.Length > 0)
                {
                    criteria.Append(" AND ");
                }
                criteria.AppendFormat("WITH ARP.INTG.TERM = '{0}'", term);
            }
            if (!string.IsNullOrEmpty(arCode))
            {
                if (criteria.Length > 0)
                {
                    criteria.Append(" AND ");
                }
                criteria.AppendFormat("WITH ARP.INTG.AR.CODE = '{0}'", arCode);
            }
            if (!string.IsNullOrEmpty(paymentType))
            {
                if (criteria.Length > 0)
                {
                    criteria.Append(" AND ");
                }
                criteria.AppendFormat("WITH ARP.INTG.PAYMENT.TYPE = '{0}'", paymentType.ToLowerInvariant());
            }
            string select = criteria.ToString();
            string[] intgStudentPaymentIds = await DataReader.SelectAsync("AR.PAY.ITEMS.INTG", select);
            var totalCount = intgStudentPaymentIds.Count();

            Array.Sort(intgStudentPaymentIds);

            var subList = intgStudentPaymentIds.Skip(offset).Take(limit).ToArray();
            var intgStudentPayments = await DataReader.BulkReadRecordAsync<ArPayItemsIntg>("AR.PAY.ITEMS.INTG", subList);
            {
                if (intgStudentPayments == null)
                {
                    throw new KeyNotFoundException("No records selected from AR.PAY.ITEMS.INTG in Colleague.");
                }
            }

            foreach (var intgStudentPaymentsEntity in intgStudentPayments)
            {
                intgStudentPaymentsEntities.Add(BuildStudentPayment(intgStudentPaymentsEntity));
            }
            return new Tuple<IEnumerable<StudentPayment>,int>(intgStudentPaymentsEntities, totalCount);
        }

        /// <summary>
        /// Update a single student payment for the data model version 6
        /// </summary>
        /// <param name="id">The GUID to the student payment entity</param>
        /// <param name="studentPayment">Student Payment to update</param>
        /// <returns>A single StudentPayment entity</returns>
        public async Task<StudentPayment> UpdateAsync(string id, StudentPayment studentPayment)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }  
            ////Guid reqdness HEDM-2628, 00000000-0000-0000-0000-000000000000 should not be validated
            if (!studentPayment.Guid.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                var recordInfo = await GetRecordInfoFromGuidAsync(id);
                if (recordInfo != null)
                {
                    throw new InvalidOperationException(string.Format("AR Payment Items Integration record {0} already exists.", id));
                }
            }
            return await CreateStudentPayments(studentPayment);
        }

        /// <summary>
        /// Create a single student payments entity for the data model version 6
        /// </summary>
        /// <param name="studentPayment">StudentPayment to create</param>
        /// <returns>A single StudentPayment</returns>
        public async Task<StudentPayment> CreateAsync(StudentPayment studentPayment)
        {
            if (!string.IsNullOrEmpty(studentPayment.Guid))
            {
                ////Guid reqdness HEDM-2628, 00000000-0000-0000-0000-000000000000 should not be validated
                if (!studentPayment.Guid.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    var recordInfo = await GetRecordInfoFromGuidAsync(studentPayment.Guid);
                    if (recordInfo != null)
                    {
                        throw new InvalidOperationException(string.Format("AR Payment Items Integration record {0} already exists.", studentPayment.Guid));
                    }
                }
            }
            return await CreateStudentPayments(studentPayment);
        }

        /// <summary>
        /// Delete a single student payment for the data model version 6
        /// </summary>
        /// <param name="id">The requested student payment GUID</param>
        /// <returns></returns>
        public async Task<StudentPayment> DeleteAsync(string id)
        {
            var recordInfo = await GetRecordInfoFromGuidAsync(id);
            if (recordInfo == null || string.IsNullOrEmpty(recordInfo.PrimaryKey) || recordInfo.Entity != "AR.PAY.ITEMS.INTG")
            {
                throw new KeyNotFoundException(string.Format("AR Invoice Items Integration record {0} does not exist.", id));
            }
            var request = new DeleteStudentPaymentRequest()
            {
                ArPayItemsIntgId = recordInfo.PrimaryKey,
                Guid = id
            };

            ////Delete
            var response = await transactionInvoker.ExecuteAsync<DeleteStudentPaymentRequest, DeleteStudentPaymentResponse>(request);

            ////if there are any errors throw
            if (response.DeleteStudentPaymentErrors.Any())
            {
                var exception = new RepositoryException("Errors encountered while deleting student-payments: " + id);
                response.DeleteStudentPaymentErrors.ForEach(e => exception.AddError(new RepositoryError(e.ErrorCode, e.ErrorMsg)));
                throw exception;
            }
            return null;
        }

        private StudentPayment BuildStudentPayment(ArPayItemsIntg integStudentPayment)
        {
            var studentPayment = new StudentPayment(integStudentPayment.ArpIntgPersonId,
                integStudentPayment.ArpIntgPaymentType, integStudentPayment.ArpIntgPaymentDate)
                {
                     AccountsReceivableCode = integStudentPayment.ArpIntgArCode,
                     AccountsReceivableTypeCode = integStudentPayment.ArpIntgArType,
                     PaymentAmount = integStudentPayment.ArpIntgAmt,
                     PaymentCurrency = integStudentPayment.ArpIntgAmtCurrency,
                     Comments = !string.IsNullOrEmpty(integStudentPayment.ArpIntgComments) ? new List<string> { integStudentPayment.ArpIntgComments } : null,
                     Guid = integStudentPayment.RecordGuid,
                     PaymentID = integStudentPayment.ArpIntgArPaymentsIds.Any() ? integStudentPayment.ArpIntgArPaymentsIds.ElementAt(0) : string.Empty,
                     Term = integStudentPayment.ArpIntgTerm
                };
            return studentPayment;
        }
 
        private async Task<StudentPayment> CreateStudentPayments(StudentPayment studentPayment)
        {
            var comments = new StringBuilder();
            if (studentPayment.Comments != null)
            {
                foreach (var com in studentPayment.Comments)
                {
                    if (comments.Length > 0)
                    {
                        comments.Append(" ");
                    }
                    comments.Append(com);
                }
            }
            var request = new PostStudentPaymentsRequest()
                {
                    ArpIntgAmt = studentPayment.PaymentAmount,
                    ArpIntgAmtCurrency = studentPayment.PaymentCurrency,
                    ArpIntgArCode = studentPayment.AccountsReceivableCode,
                    ArpIntgArType = studentPayment.AccountsReceivableTypeCode,
                    ArpIntgPaymentType = studentPayment.PaymentType,
                    ArpIntgComments = comments.ToString(),
                    ArpIntgPaymentDate = studentPayment.PaymentDate,
                    ArpIntgGuid = studentPayment.Guid,
                    ArpIntgPersonId = studentPayment.PersonId,
                    ArpIntgTerm = studentPayment.Term
                };

            ////Guid reqdness HEDM-2628, since transaction doesn't support 00000000-0000-0000-0000-000000000000, we have to assign empty string
            if (request.ArpIntgGuid.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                request.ArpIntgGuid = string.Empty;
            }

            var updateResponse = await transactionInvoker.ExecuteAsync<PostStudentPaymentsRequest, PostStudentPaymentsResponse>(request);

            // If there is any error message - throw an exception 
            if (!string.IsNullOrEmpty(updateResponse.Error))
            {
                var errorMessage = string.Format("Error(s) occurred updating student-payments for id: '{0}'.", request.ArpIntgGuid);
                var exception = new RepositoryException(errorMessage);
                foreach (var errMsg in updateResponse.StudentPaymentErrors)
                {
                    exception.AddError(new RepositoryError(errMsg.ErrorCodes, errMsg.ErrorMessages));
                    errorMessage += string.Join(Environment.NewLine, errMsg.ErrorMessages);
                }
                logger.Error(errorMessage.ToString());
                throw exception;
            }

            return await GetByIdAsync(updateResponse.ArpIntgGuid);
        }
    }
}
