﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Data.Student.DataContracts;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.DataContracts;

namespace Ellucian.Colleague.Data.Student.Repositories
{
    /// <summary>
    /// Higher education institution admission applications.
    /// </summary>
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class AdmissionApplicationsRepository : BaseColleagueRepository, IAdmissionApplicationsRepository
    {
         /// <summary>
        /// ..ctor
        /// </summary>
        /// <param name="cacheProvider"></param>
        /// <param name="transactionFactory"></param>
        /// <param name="logger"></param>
        public AdmissionApplicationsRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            // Using level 1 cache time out value for data that rarely changes.
            CacheTimeout = Level1CacheTimeoutValue;
        }

        #region GET methods

        Collection<StudentPrograms> programData = null;

        /// <summary>
        /// Get admission applications
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        public async Task<Tuple<IEnumerable<Domain.Student.Entities.AdmissionApplication>, int>> GetAdmissionApplicationsAsync(int offset, int limit, bool bypassCache)
        {
            List<Domain.Student.Entities.AdmissionApplication> applicationEntities = new List<Domain.Student.Entities.AdmissionApplication>();
            int totalCount = 0;
            string criteria = "WITH APPL.APPLICANT NE '' AND WITH APPL.CURRENT.STATUS NE 'PR'";
            var applicationIds = await DataReader.SelectAsync("APPLICATIONS", criteria);
            totalCount = applicationIds.Count();

            Array.Sort(applicationIds);

            var subList = applicationIds.Skip(offset).Take(limit).ToArray();

            // Now we have criteria, so we can select and read the records
            Collection<DataContracts.Applications> applicationDataContracts = null;
            applicationDataContracts = await DataReader.BulkReadRecordAsync<DataContracts.Applications>("APPLICATIONS", subList);

            if (applicationDataContracts != null && applicationDataContracts.Any())
            {
                var stprIds = new List<string>();
                stprIds.AddRange(applicationDataContracts.Where(i => !string.IsNullOrEmpty(i.ApplAcadProgram)).Select(i => string.Concat(i.ApplApplicant, "*", i.ApplAcadProgram)));
                programData = await DataReader.BulkReadRecordAsync<StudentPrograms>(stprIds.Distinct().ToArray());

                foreach (var applicationDataContract in applicationDataContracts)
                {
                    Domain.Student.Entities.AdmissionApplication applicationEntity = BuildAdmissionApplication(applicationDataContract);
                    applicationEntities.Add(applicationEntity);
                }
            }

            return applicationEntities.Any() ? 
                new Tuple<IEnumerable<Domain.Student.Entities.AdmissionApplication>, int>(applicationEntities, totalCount) :
                new Tuple<IEnumerable<Domain.Student.Entities.AdmissionApplication>, int>(new List<Domain.Student.Entities.AdmissionApplication>(), 0);
        }

        /// <summary>
        /// Get admission application by Id
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public async Task<Domain.Student.Entities.AdmissionApplication> GetAdmissionApplicationByIdAsync(string guid)
        {
            var admissionApplicationId = await GetRecordKeyFromGuidAsync(guid);
            if (string.IsNullOrEmpty(admissionApplicationId))
            {
                throw new KeyNotFoundException("admission-applications key not found for GUID " + guid);
            }

            var admissionApplicationDataContract = await DataReader.ReadRecordAsync<Applications>("APPLICATIONS", admissionApplicationId);
            if (admissionApplicationDataContract == null)
            {
                throw new KeyNotFoundException("admission-applications data contract not found for Id " + admissionApplicationId);
            }

            var stprIds = new List<string>();
            stprIds.Add(string.Concat(admissionApplicationDataContract.ApplApplicant, "*", admissionApplicationDataContract.ApplAcadProgram));
            programData = await DataReader.BulkReadRecordAsync<StudentPrograms>(stprIds.Distinct().ToArray());

            var admissionApplicationEntity = BuildAdmissionApplication(admissionApplicationDataContract);
            return admissionApplicationEntity;
        }

        /// <summary>
        /// Gets all the guids for the person keys
        /// </summary>
        /// <param name="personRecordKeys"></param>
        /// <returns></returns>
        public async Task<Dictionary<string, string>> GetPersonGuidsAsync(IEnumerable<string> personRecordKeys)
        {
            if (personRecordKeys != null && !personRecordKeys.Any())
            {
                return null;
            }

            var personGuids = new Dictionary<string, string>();

            if (personRecordKeys != null && personRecordKeys.Any())
            {
                // convert the person keys to person guids
                var personGuidLookup = personRecordKeys.ToList().ConvertAll(p => new RecordKeyLookup("PERSON", p, false)).ToArray();
                var recordKeyLookupResults = await DataReader.SelectAsync(personGuidLookup);
                foreach (var recordKeyLookupResult in recordKeyLookupResults)
                {
                    string[] splitKeys = recordKeyLookupResult.Key.Split(new[] { "+" }, StringSplitOptions.RemoveEmptyEntries);
                    if (!personGuids.ContainsKey(splitKeys[1]))
                    {
                        if (recordKeyLookupResult.Value != null)
                        {
                            personGuids.Add(splitKeys[1], recordKeyLookupResult.Value.Guid);
                        }
                    }
                }
            }
            return (personGuids != null && personGuids.Any()) ? personGuids : null;
        }


        public async Task<IDictionary<string, string>> GetStaffOperIdsAsync(List<string> ids)
        {
            var opersData = await DataReader.BulkReadRecordAsync<Opers>("UT.OPERS", ids.ToArray(), true);
            var staffData = await DataReader.BulkReadRecordAsync<Staff>(ids.ToArray());
            IDictionary<string, string> combinedIds = new Dictionary<string, string>();

            if (opersData != null && opersData.Any())
            {
                opersData.ToList().ForEach(i => 
                {
                    if (!string.IsNullOrEmpty(i.SysPersonId) && !combinedIds.ContainsKey(i.Recordkey))
                    {
                        combinedIds.Add(i.Recordkey, i.SysPersonId);
                    }
                });
            }

            if (staffData != null && staffData.Any())
            {
                staffData.ToList().ForEach(i =>
                {
                    if (!string.IsNullOrEmpty(i.Recordkey) && !string.IsNullOrEmpty(i.StaffLoginId) && !combinedIds.ContainsKey(i.StaffLoginId))
                    {
                        combinedIds.Add(i.StaffLoginId, i.Recordkey);
                    }
                });
            }

            return combinedIds;

        }

        #endregion

        #region Build method

        /// <summary>
        /// Builds admission application entity
        /// </summary>
        /// <param name="application"></param>
        /// <returns></returns>
        private Domain.Student.Entities.AdmissionApplication BuildAdmissionApplication(Applications application)
        {
            try
            {
                Domain.Student.Entities.AdmissionApplication applicationEntity =
                                new Domain.Student.Entities.AdmissionApplication(application.RecordGuid, application.Recordkey);

                applicationEntity.ApplicantPersonId = application.ApplApplicant;
                applicationEntity.ApplicationNo = application.ApplNo;
                applicationEntity.ApplicationStartTerm = application.ApplStartTerm;
                applicationEntity.AdmissionApplicationStatuses = BuildAdmissionApplicationStatuses(application.ApplStatusesEntityAssociation);
                applicationEntity.ApplicationSource = application.ApplSource;
                applicationEntity.ApplicationAdmissionsRep = application.ApplAdmissionsRep;
                applicationEntity.ApplicationAdmitStatus = application.ApplAdmitStatus;
                applicationEntity.ApplicationLocations = application.ApplLocations;
                applicationEntity.ApplicationResidencyStatus = application.ApplResidencyStatus;
                applicationEntity.ApplicationStudentLoadIntent = application.ApplStudentLoadIntent;
                applicationEntity.ApplicationAcadProgram = application.ApplAcadProgram;
                applicationEntity.ApplicationStprAcadPrograms = BuildProgramCodeList(application);
                applicationEntity.ApplicationWithdrawReason = application.ApplWithdrawReason;
                applicationEntity.ApplicationAttendedInstead = application.ApplAttendedInstead;
                applicationEntity.ApplicationComments = application.ApplComments;
                applicationEntity.ApplicationSchool = BuildSchool(string.Concat(application.ApplApplicant, "*", application.ApplAcadProgram));

                return applicationEntity;
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException(string.Format(ex.Message, "admission application guid: {0}.", application.RecordGuid), ex);
            }
            catch (Exception ex)
            {
                throw new RepositoryException(string.Format(ex.Message, "admission application guid: {0}.", application.RecordGuid), ex);
            }          
        }

        /// <summary>
        /// STPR_MAJOR_LIST, STPR_MINOR_LIST, STPR_SPECIALTIES
        /// </summary>
        /// <param name="application"></param>
        /// <returns></returns>
        private List<string> BuildProgramCodeList(Applications application)
        {
            List<string> programs = new List<string>();

            var key = string.Concat(application.ApplApplicant, "*", application.ApplAcadProgram);

            if (programData == null || (programData != null && !programData.Any()))
            {
                return programs;
            }
            var program = programData.FirstOrDefault(i => i.Recordkey.Equals(key, StringComparison.OrdinalIgnoreCase));
            if (program == null)
            {
                throw new KeyNotFoundException(string.Format("Student academic program not found for: {0}. ", key));
            }

            //STPR_MAJOR_LIST
            if (program.StprMajorListEntityAssociation != null && program.StprMajorListEntityAssociation.Any())
            {
                foreach (var major in program.StprMajorListEntityAssociation)
                {
                    if (!programs.Contains(major.StprAddnlMajorsAssocMember))
                    {
                        programs.Add(major.StprAddnlMajorsAssocMember);
                    }
                }
            }

            //STPR_MINOR_LIST
            if (program.StprMinorListEntityAssociation != null && program.StprMinorListEntityAssociation.Any())
            {
                foreach (var minor in program.StprMinorListEntityAssociation)
                {
                    if (!programs.Contains(minor.StprMinorsAssocMember))
                    {
                        programs.Add(minor.StprMinorsAssocMember);
                    }
                }
            }

            //STPR_SPECIALTIES
            if (program.StprSpecialtiesEntityAssociation != null && program.StprSpecialtiesEntityAssociation.Any())
            {
                foreach (var specialty in program.StprSpecialtiesEntityAssociation)
                {
                    if (!programs.Contains(specialty.StprSpecializationsAssocMember))
                    {
                        programs.Add(specialty.StprSpecializationsAssocMember);
                    }
                }
            }
            return programs.Any() ? programs : null;
        }

        /// <summary>
        /// Gets school guid
        /// </summary>
        /// <param name="acadProgramKey"></param>
        /// <returns></returns>
        private string BuildSchool(string acadProgramKey)
        {
            try
            {
                if (programData == null || (programData != null && !programData.Any()))
                {
                    return string.Empty;
                }
                var program = programData.FirstOrDefault(i => i.Recordkey.Equals(acadProgramKey, StringComparison.OrdinalIgnoreCase));
                if (program == null)
                {
                    throw new KeyNotFoundException(string.Format("Student academic program not found for: {0}. ", acadProgramKey));
                }
                return program.StprSchool;
            }
            catch (KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private List<Domain.Student.Entities.AdmissionApplicationStatus> BuildAdmissionApplicationStatuses(List<ApplicationsApplStatuses> applStatusesAssiciation)
        {
            List<Domain.Student.Entities.AdmissionApplicationStatus> applStatuses = new List<Domain.Student.Entities.AdmissionApplicationStatus>();
            if (applStatusesAssiciation != null && applStatusesAssiciation.Any())
            {
                foreach (var item in applStatusesAssiciation)
                {
                    Domain.Student.Entities.AdmissionApplicationStatus admApplStatus = new Domain.Student.Entities.AdmissionApplicationStatus() 
                    {
                        ApplicationDecisionBy = item.ApplDecisionByAssocMember,
                        ApplicationStatus = item.ApplStatusAssocMember,
                        ApplicationStatusDate = item.ApplStatusDateAssocMember,
                        ApplicationStatusTime = item.ApplStatusTimeAssocMember
                    };
                    applStatuses.Add(admApplStatus);
                }
            }
            return applStatuses;
        }

        private List<string> BuildApplicationDecisionBy(List<ApplicationsApplStatuses> applicationsApplStatuses)
        {
            List<string> decisionBy = new List<string>();

            var items = applicationsApplStatuses.OrderByDescending(i => i.ApplStatusDateAssocMember);
            items.ToList().ForEach(i => 
            {
                decisionBy.Add(i.ApplDecisionByAssocMember);
            });
            return decisionBy.Any() ? decisionBy : null;
        }

        #endregion
    }
}