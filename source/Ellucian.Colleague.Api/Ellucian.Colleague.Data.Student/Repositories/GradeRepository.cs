﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Data.Student.DataContracts;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Web.Utility;
using slf4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.Student.Repositories
{
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class GradeRepository : BaseColleagueRepository, IGradeRepository
    {
        public GradeRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            // Using Level 1 Cache Timeout Value for data that changes rarely.
            CacheTimeout = Level1CacheTimeoutValue;
        }

        public async Task<ICollection<Grade>> GetAsync()
        {
            var grades = await GetOrAddToCacheAsync<List<Grade>>("AllGrades",
                async () =>
                {
                    Collection<Grades> gradesData = await DataReader.BulkReadRecordAsync<Grades>("");
                    var gradeList = await BuildGradesAsync(gradesData);
                    return gradeList;
                }
            );
            return grades;
        }

        private async Task<List<Grade>> BuildGradesAsync(Collection<Grades> gradeData)
        {
            var grades = new List<Grade>();
            // If no data passed in, return a null collection
            if (gradeData != null)
            {
                foreach (var grd in gradeData)
                {
                    try
                    {
                        var grade = new Grade(grd.RecordGuid, grd.Recordkey, grd.GrdGrade, grd.GrdCmplCredFlag, grd.GrdLegend, grd.GrdGradeScheme);
                        grade.GradeValue = grd.GrdValue ?? 0m;
                        grade.GradePriority = grd.GrdRepeatValue ?? 0m;
                        grade.IncompleteGrade = grd.GrdIncompleteGrade;
                        grade.ExcludeFromFacultyGrading = !string.IsNullOrEmpty(grd.GrdExcludeFromFacFlag) && grd.GrdExcludeFromFacFlag.ToUpper() == "Y";
                        grade.RequireLastAttendanceDate = !string.IsNullOrEmpty(grd.GrdFinalRequireLda) && grd.GrdFinalRequireLda.ToUpper() == "Y";
                        grades.Add(grade);
                    }
                    catch (Exception ex)
                    {
                        LogDataError("Grade", grd.Recordkey, grd, ex);
                        //throw new ArgumentException("Error occurred when trying to build grade " + grd.Recordkey);
                    }

                }
                // Loop through all the grades used as withdraw grades and update the boolean in each respective grade object
                foreach (var repoGrade in gradeData.Where(g => !(string.IsNullOrEmpty(g.GrdWithdrawGrade)) && (!(string.IsNullOrEmpty(g.GrdGradeScheme)))))
                {
                    try
                    {
                        var gradeObject = grades.Where(g => g.Id == repoGrade.GrdWithdrawGrade && g.GradeSchemeCode == repoGrade.GrdGradeScheme).First();
                        gradeObject.IsWithdraw = true;
                    }
                    catch
                    {
                        // Log the original exception and a serialized version of the course record
                        logger.Error("Withdraw Grade ID " + repoGrade.GrdWithdrawGrade + " Scheme ID " + repoGrade.GrdGradeScheme + " does not exist in list of Grades built from repository.");
                    }
                }
                // Loop through the phone reg drop grades and update the boolean in each respective grade object
                List<RegDefaultsRgdPhoneDrops> phoneRegDropGrades = await PhoneRegDropGradesAsync();
                if (phoneRegDropGrades != null)
                {
                    foreach (var dropGrade in phoneRegDropGrades.Where(g => !(string.IsNullOrEmpty(g.RgdPhoneDropGradeAssocMember)) && !(string.IsNullOrEmpty(g.RgdPhoneDropGradeSchemeAssocMember))))
                    {
                        try
                        {
                            var gradeObject = grades.Where(g => g.Id == dropGrade.RgdPhoneDropGradeAssocMember && g.GradeSchemeCode == dropGrade.RgdPhoneDropGradeSchemeAssocMember).First();
                            gradeObject.IsWithdraw = true;
                        }
                        catch
                        {
                            // Log the original exception and a serialized version of the course record
                            logger.Error("Drop Grade ID " + dropGrade.RgdPhoneDropGradeAssocMember + " Scheme ID " + dropGrade.RgdPhoneDropGradeSchemeAssocMember + " does not exist in list of Grades built from repository.");
                        }
                    }
                }
                // Loop through grade data for comparison grades, verify the comparison grade, then add the property to the grade object.
                var gradeDataWithComparisonGrade = gradeData.Where(g => !string.IsNullOrEmpty(g.GrdComparisonGrade)).ToList();
                foreach (var grade in gradeDataWithComparisonGrade)
                {
                    var gradeObject = grades.Where(g => g.Id == grade.Recordkey).FirstOrDefault();
                    if (gradeObject != null)
                    {
                        // Add the comparison grade's ID to the grade that uses it
                        gradeObject.ComparisonGrade = grade.GrdComparisonGrade;

                        // need to ensure that the comparison grade object's comparison grade points to the grade object that uses it
                        // find the comparison grade object for the grade object
                        var comparisonGradeObject = grades.Where(g => g.Id == gradeObject.ComparisonGrade).FirstOrDefault();
                        if (comparisonGradeObject != null)
                        {
                            // set the comparison grade in the comparison grade object to the grade object's id
                            comparisonGradeObject.ComparisonGrade = gradeObject.Id;
                        }
                    }
                    else
                    {
                        logger.Error("Comparison Grade ID " + grade.GrdComparisonGrade + " is not a valid Grade entity");
                    }
                }
            }
            return grades;
        }

        /// <summary>
        /// Gets HeDM Grades
        /// </summary>
        /// <param name="bypassCache">Bypass cache flag</param>
        /// <returns>Returns Grades collection</returns>
        public async Task<ICollection<Grade>> GetHedmAsync(bool bypassCache = false)
        {
            if (bypassCache)
            {
                Collection<Grades> gradesData = await DataReader.BulkReadRecordAsync<Grades>("");
                VerifyGrades(gradesData);
                var gradeList = await BuildGradesAsync(gradesData);
                return gradeList;
            }
            else
            {
                return await GetOrAddToCacheAsync<List<Grade>>("AllGrades",
                                async () =>
                                {
                                    Collection<Grades> gradesData = await DataReader.BulkReadRecordAsync<Grades>("");
                                    VerifyGrades(gradesData);
                                    var gradeList = await BuildGradesAsync(gradesData);
                                    return gradeList;
                                }
                            );
            }
        }

        /// <summary>
        /// Gets HeDM Grade based on Id
        /// </summary>
        /// <param name="id">Grade Id</param>
        /// <returns>Returns Grade</returns>
        public async Task<Grade> GetHedmGradeByIdAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id", "id must be specified.");
            }

            var gradeId = await GetRecordKeyFromGuidAsync(id);

            if (string.IsNullOrEmpty(gradeId))
            {
                throw new KeyNotFoundException("Grade not found for id " + id);
            }
            try
            {
                return await GetNewGradeAsync(gradeId);
            }

            catch (ApplicationException aex)
            {
                throw aex;
            }
            catch (Exception)
            {
                throw new KeyNotFoundException("Grade not found for id " + id);
            }
        }

        /// <summary>
        /// Gets Entity Grade based on gradeId
        /// </summary>
        /// <param name="gradeId">Grade Id</param>
        /// <returns>Returns Entity Grade</returns>
        private async Task<Grade> GetNewGradeAsync(string gradeId)
        {
            if (string.IsNullOrEmpty(gradeId))
            {
                throw new ArgumentNullException("gradeId");
            }

            Grades grade = await DataReader.ReadRecordAsync<Grades>(gradeId);
            if (grade == null)
            {
                throw new KeyNotFoundException("Invalid grade ID: " + gradeId);
            }

            var grades = new Collection<Grades>() { grade };
            VerifyGrades(grades);
            var newGrade = await BuildGradesAsync(grades);

            return newGrade.FirstOrDefault();
        }

        //private List<RegDefaultsRgdPhoneDrops> PhoneRegDropGrades
        //{

        //    get
        //    {
        //        var registrationDefaults = GetOrAddToCache<Ellucian.Colleague.Data.Student.DataContracts.RegDefaults>("RegistrationDefaults",
        //            () =>
        //            {
        //                Data.Student.DataContracts.RegDefaults regDefaults = DataReader.ReadRecord<Data.Student.DataContracts.RegDefaults>("ST.PARMS", "REG.DEFAULTS");
        //                if (regDefaults == null)
        //                {
        //                    var errorMessage = "Unable to access REG.DEFAULTS to determine phone registration withdraw grade. Skipping logic.";
        //                    logger.Info(errorMessage);
        //                    regDefaults = new RegDefaults();
        //                }
        //                return regDefaults;
        //            }, Level1CacheTimeoutValue);
        //        return registrationDefaults.RgdPhoneDropsEntityAssociation;
        //    }
        //}

        private async Task<List<RegDefaultsRgdPhoneDrops>> PhoneRegDropGradesAsync()
        {
            var registrationDefaults = await GetOrAddToCacheAsync<Ellucian.Colleague.Data.Student.DataContracts.RegDefaults>("RegistrationDefaults",
              async () =>
              {
                  Data.Student.DataContracts.RegDefaults regDefaults = await DataReader.ReadRecordAsync<Data.Student.DataContracts.RegDefaults>("ST.PARMS", "REG.DEFAULTS");
                  if (regDefaults == null)
                  {
                      var errorMessage = "Unable to access REG.DEFAULTS to determine phone registration withdraw grade. Skipping logic.";
                      logger.Info(errorMessage);
                      regDefaults = new RegDefaults();
                  }
                  return regDefaults;
              }, Level1CacheTimeoutValue);
            return registrationDefaults.RgdPhoneDropsEntityAssociation;
        }


        private void VerifyGrades(Collection<Grades> gradesData)
        {

            // HEDM will not return grades missing grade schemes

            IEnumerable<Grades> badGrades = new List<Grades>();
            badGrades = gradesData.Where(gd => gd.RecordGuid != null && string.IsNullOrEmpty(gd.GrdGradeScheme));
            foreach (var badGrade in badGrades)
            {
                logger.Error("Grade with GUID " + badGrade.RecordGuid + " and ID " + badGrade.Recordkey + " is missing a Grade Scheme.");
            }

            if (badGrades.Count() == 1)
            {
                var msg = "Grade with GUID " + badGrades.First().RecordGuid + " and ID " + badGrades.First().Recordkey + " is missing a Grade Scheme.";
                throw new ApplicationException(msg);
            }

            if (badGrades.Count() > 0)
            {
                throw new ApplicationException("There are grades thaat have no associated grade schemes.  Please check the log for details.");
            }

        }



    }
}
