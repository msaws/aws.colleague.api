﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Data.Student.DataContracts;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Web.Http.Configuration;
using slf4net;

namespace Ellucian.Colleague.Data.Student.Repositories
{
    /// <summary>
    /// Repository for Student tax forms
    /// </summary>
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class StudentTaxFormStatementRepository : BaseColleagueRepository, IStudentTaxFormStatementRepository
    {
        private readonly string colleagueTimeZone;

        /// <summary>
        /// Student Tax Form Statement repository constructor.
        /// </summary>
        /// <param name="settings">Settings</param>
        /// <param name="cacheProvider">Cache provider</param>
        /// <param name="transactionFactory">Transaction factory</param>
        /// <param name="logger">Logger</param>
        public StudentTaxFormStatementRepository(ApiSettings settings, ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            colleagueTimeZone = settings.ColleagueTimeZone;
        }

        /// <summary>
        /// Retrieve set of tax form statements assigned to the specified person for the tax form type.
        /// </summary>
        /// <param name="personId">Person ID</param>
        /// <param name="taxForm">Type of tax form</param>
        /// <returns>Set of tax form statements</returns>
        public async Task<IEnumerable<TaxFormStatement2>> GetAsync(string personId, TaxForms taxForm)
        {
            if (string.IsNullOrEmpty(personId))
                throw new ArgumentNullException("personId", "personId is a required field.");

            var statements = new List<TaxFormStatement2>();

            // Based on the type of tax form, we will obtain the statements from different entities.
            switch (taxForm)
            {
                case TaxForms.Form1098:
                    statements = ( await Get1098TaxStatements(personId) ).ToList();
                    break;
                default:
                    throw new ArgumentException(taxForm.ToString() + " is not accessible within the ST module.", "taxForm");
            }

            return statements;
        }

        private async Task<IEnumerable<TaxFormStatement2>> Get1098TaxStatements(string personId)
        {
            var statements = new List<TaxFormStatement2>();

            var parm1098Contract = await DataReader.ReadRecordAsync<Parm1098>("ST.PARMS", "PARM.1098");
            if (parm1098Contract == null)
            {
                throw new NullReferenceException("PARM.1098 cannot be null.");
            }
            
            // Get all the 1098 records for the person ID that are also 1098Ts.
            var form1098Criteria = "TF98F.STUDENT EQ '" + personId +"' AND WITH TF98F.TAX.FORM EQ '" + parm1098Contract.P1098TTaxForm + "'";
            var form1098StatementRecords = await DataReader.BulkReadRecordAsync<TaxForm1098Forms>(form1098Criteria);
          
            if (form1098StatementRecords != null)
            {
                // Sort the statements.
                var sorted1098StatementRecords = form1098StatementRecords.Where(x => x != null).OrderByDescending(x => x.TaxForm1098FormsAdddate)
                    .ThenByDescending(x => x.TaxForm1098FormsAddtime);

                // Loop through each tax year. We do not need to remove duplicate years because there may be multiple institutions issuing 1098s.
                foreach (var form1098Statement in sorted1098StatementRecords)
                {
                    try
                    {
                        if (form1098Statement.Recordkey == null)
                        {
                            throw new NullReferenceException("RecordKey is a required field");
                        }

                        if (!string.IsNullOrEmpty(form1098Statement.Tf98fStudent) && form1098Statement.Tf98fTaxYear.HasValue)
                        {
                            var statement1098 = new TaxFormStatement2(form1098Statement.Tf98fStudent,
                                form1098Statement.Tf98fTaxYear.ToString(), TaxForms.Form1098, form1098Statement.Recordkey);

                            // Add the statement to the list
                            statements.Add(statement1098);
                        }
                    }
                    catch (Exception e)
                    {
                        LogDataError("TaxFormStatement", personId, new Object(), e, e.Message);
                    }
                }
            }
            return statements;
        }
    }
}


