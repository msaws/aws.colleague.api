﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Data.Student.DataContracts;
using Ellucian.Colleague.Data.Student.Transactions;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Web.Utility;
using slf4net;
using Ellucian.Colleague.Data.Base.DataContracts;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.Student.Repositories
{
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class TermRepository : BaseColleagueRepository, ITermRepository
    {
        public TermRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            // Using level 1 cache time out value for data that rarely changes.
            CacheTimeout = Level1CacheTimeoutValue;
        }

         /// <summary>
        /// Retrieve all terms
        /// </summary>
        /// <returns>All terms</returns>
        public async Task<IEnumerable<Term>> GetAsync()
        {
            return await GetAsync(false);
        }

        /// <summary>
        /// Retrieve all terms
        /// </summary>
        /// <returns>All terms</returns>
        public async Task<IEnumerable<Term>> GetAsync(bool clearCache)
        {
           
            if (clearCache && ContainsKey(BuildFullCacheKey("AllTerms")))
            {
                ClearCache(new List<string>{"AllTerms"});
            }
            
            // Get all terms from cache. If not already in cache, add them.
            var terms = await GetOrAddToCacheAsync<List<Term>>("AllTerms",
                async () =>
                {
                    try
                    {
                    // Get terms from the database if not in cache. 
                    // Limiting this to terms that are "available" for planning based on new field. 
                    // 
                        Collection<Ellucian.Colleague.Data.Student.DataContracts.Terms> termData =await DataReader.BulkReadRecordAsync<Ellucian.Colleague.Data.Student.DataContracts.Terms>("TERMS", "");
                        Collection<TermsLocations> termLocationData = await DataReader.BulkReadRecordAsync<TermsLocations>("TERMS.LOCATIONS", "");
                        Collection<Locations> locationData = await DataReader.BulkReadRecordAsync<Locations>("LOCATIONS", "");
                    var termList = BuildTerms(termData, termLocationData, locationData);
                        return termList;
                    }
                    catch (Exception ex)
                    {
                        string errorMessage = "Unable to read all Terms from the database.";
                        logger.Error(ex.ToString());
                        logger.Error(errorMessage);
                        throw new ApplicationException(errorMessage);
                    }
                }
            );
            return terms;
        }

        public async Task<Term> GetAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("Term Code must be specified");
            }
            try
            {
                return (await GetAsync()).Where(t => t.Code == id).First();
            }
            catch
            {
                // Not including the id of the record in the exception sent back but will include it in the message that is logged.
                logger.Error("No Term found for Code" + id);
                throw new KeyNotFoundException("Term not found");
            }
        }

        /// <summary>
        /// Retrieve specific term codes
        /// </summary>
        /// <param name="termCodes">Ids of the Terms desired </param>
        /// <returns>The requested terms.</returns>
        public async Task<IEnumerable<Term>> GetAsync(IEnumerable<string> termCodes)
        {
            if (termCodes == null || termCodes.Count() == 0)
            {
                throw new ArgumentNullException("termCodes", "Must provide at least one term code.");
            }
            var terms = new List<Term>();
            if ((termCodes != null) && (termCodes.Count() > 0))
            {
                IEnumerable<Term> allTerms = (await GetAsync()).AsEnumerable();
                StringBuilder termsNotFound = new StringBuilder();
                foreach (var termCode in termCodes)
                {
                    try
                    {
                        terms.Add(allTerms.Where(t => t.Code == termCode).First());
                    }
                    catch
                    {
                        termsNotFound.Append(" " + termCode);
                    }
                }
                if (termsNotFound.Length > 0)
                {
                    logger.Info("Term not found for term codes" + termsNotFound.ToString());
                }
            }
            return terms;
        }

        /// <summary>
        /// Returns the terms that have been defined as open for section search and registration
        /// </summary>
        /// <returns>Registration Terms</returns>
        public async Task<IEnumerable<Term>> GetRegistrationTermsAsync()
        {
            // Get list of terms from cache. If not there, read from database.
            var registrationTermsCodes = await GetOrAddToCacheAsync<List<string>>("RegistrationTerms",
                async () =>
                {
                    GetRegistrationTermsRequest registrationTermsRequest = new GetRegistrationTermsRequest();
                    GetRegistrationTermsResponse registrationTermsResponse = await transactionInvoker.ExecuteAsync<GetRegistrationTermsRequest, GetRegistrationTermsResponse>(registrationTermsRequest);
                    return registrationTermsResponse.AlRegistrationTerms;
                });
            if ((registrationTermsCodes == null) || (registrationTermsCodes.Count() == 0))
            {
                // If the list is null or empty, throw an exception
                //throw new KeyNotFoundException("No registration terms found");
                return new List<Term>();
            }
            else
            {
                // If the list has codes, use the "get many" method to return the term objects
                return await GetAsync(registrationTermsCodes);
            }
        }

        /// <summary>
        /// Changes the term data contracts into Term entities
        /// </summary>
        /// <param name="termData">Term data contracts</param>
        /// <returns>Term entities</returns>
        private List<Term> BuildTerms(Collection<Ellucian.Colleague.Data.Student.DataContracts.Terms> termData, Collection<TermsLocations> termLocationData, Collection<Locations> locationData)
        {
            var terms = new List<Term>();

            if (termData != null)
            {
                foreach (var term in termData)
                {
                    try
                    {
                        bool defaultOnPlan = false;
                        if (!string.IsNullOrEmpty(term.TermDefaultOnPlan) && term.TermDefaultOnPlan.ToUpper() == "Y")
                        {
                            defaultOnPlan = true;
                        }
                        bool useInBestFitTermCalc = false;
                        if (!string.IsNullOrEmpty(term.TermUseInBestFitCalc) && term.TermUseInBestFitCalc.Equals("Y", StringComparison.OrdinalIgnoreCase))
                        {
                            useInBestFitTermCalc = true;
                        }
                        bool forPlanning = false;
                        if (!string.IsNullOrEmpty(term.TermDegreePlanning) && term.TermDegreePlanning.ToUpper() == "Y")
                        {
                            forPlanning = true;
                        }
                        bool activeFlag = false;
                        if (term.TermStartDate <= DateTime.Now && term.TermEndDate >= DateTime.Now)
                        {
                            activeFlag = true;
                        }

                        bool checkRegPriority = false;
                        if (!string.IsNullOrEmpty(term.TermRegPriorityFlag) && term.TermRegPriorityFlag.ToUpper() == "Y")
                        {
                            checkRegPriority = true;
                        }
                        PeriodType? financialPeriod = null;
                        switch (term.TermPcfPeriod)
                        {
                            case "1":
                                financialPeriod = PeriodType.Past;
                                break;
                            case "2":
                                financialPeriod = PeriodType.Current;
                                break;
                            case "3":
                                financialPeriod = PeriodType.Future;
                                break;
                        }

                        Term newTerm = new Term(term.RecordGuid, 
                            term.Recordkey, term.TermDesc,
                            term.TermStartDate.GetValueOrDefault(),
                            term.TermEndDate.GetValueOrDefault(),
                            term.TermReportingYear.GetValueOrDefault(0),
                            Convert.ToInt32(term.TermSequenceNo),
                            defaultOnPlan,
                            forPlanning,
                            term.TermReportingTerm,
                            checkRegPriority)
                                {
                                    FinancialPeriod = financialPeriod,    
                                    IsActive = activeFlag,
                                    FinancialAidYears = term.TermFaYears,
                                    UseTermInBestFitCalculations = useInBestFitTermCalc
                                };

                        RegistrationDate registrationDates = new RegistrationDate(string.Empty,
                            term.TermRegStartDate, term.TermRegEndDate,
                            term.TermPreregStartDate, term.TermPreregEndDate,
                            term.TermAddStartDate, term.TermAddEndDate,
                            term.TermDropStartDate, term.TermDropEndDate,
                            term.TermDropGradeReqdDate,
                            term.TermCensusDates);

                        newTerm.AddRegistrationDates(registrationDates);

                        // Check to see if we have term specific dates
                        if (locationData != null)
                        {
                            foreach (var location in locationData)
                            {
                                var key = term.Recordkey + "*" + location.Recordkey;
                                TermsLocations termLocation = termLocationData.Where(t => t.Recordkey == key).FirstOrDefault();
                                if (termLocation != null)
                                {
                                    DateTime? registrationStartDate = (termLocation.TlocRegStartDate.HasValue) ? termLocation.TlocRegStartDate.Value : term.TermRegStartDate;
                                    DateTime? registrationEndDate = (termLocation.TlocRegEndDate.HasValue) ? termLocation.TlocRegEndDate.Value : term.TermRegEndDate;
                                    DateTime? preRegistrationStartDate = (termLocation.TlocPreregStartDate.HasValue) ? termLocation.TlocPreregStartDate.Value : term.TermPreregStartDate;
                                    DateTime? preRegistrationEndDate = (termLocation.TlocPreregEndDate.HasValue) ? termLocation.TlocPreregEndDate.Value : term.TermPreregEndDate;
                                    DateTime? addStartDate = (termLocation.TlocAddStartDate.HasValue) ? termLocation.TlocAddStartDate.Value : term.TermAddStartDate;
                                    DateTime? addEndDate = (termLocation.TlocAddEndDate.HasValue) ? termLocation.TlocAddEndDate.Value : term.TermAddEndDate;
                                    DateTime? dropStartDate = (termLocation.TlocDropStartDate.HasValue) ? termLocation.TlocDropStartDate.Value : term.TermDropStartDate;
                                    DateTime? dropEndDate = (termLocation.TlocDropEndDate.HasValue) ? termLocation.TlocDropEndDate.Value : term.TermDropEndDate;
                                    DateTime? dropGradeRequiredDate = (termLocation.TlocDropGradeReqdDate.HasValue) ? termLocation.TlocDropGradeReqdDate.Value : term.TermDropGradeReqdDate;
                                    List<DateTime?> censusDates = (termLocation.TlocCensusDates != null) ? termLocation.TlocCensusDates : term.TermCensusDates;
                                                                        
                                    RegistrationDate locationRegistrationDates = new RegistrationDate(location.Recordkey,
                                        registrationStartDate, registrationEndDate,
                                        preRegistrationStartDate, preRegistrationEndDate,
                                        addStartDate, addEndDate, dropStartDate, dropEndDate,
                                        dropGradeRequiredDate, censusDates);

                                    newTerm.AddRegistrationDates(locationRegistrationDates);
                                }
                            }
                        }

                        // Add the term academic levels
                        foreach (var level in term.TermAcadLevels)
                        {
                            newTerm.AddAcademicLevel(level);
                        }
                        // Add the term session cycles
                        foreach (var cycle in term.TermSessionCycles)
                        {
                            if (!string.IsNullOrEmpty(cycle))
                            {
                                newTerm.AddSessionCycle(cycle);
                            }
                            
                        }
                        // Add the term yearly cycles
                        foreach (var cycle in term.TermYearlyCycles)
                        {
                            if (!string.IsNullOrEmpty(cycle))
                            {
                                newTerm.AddYearlyCycle(cycle);
                            }
                        }
                        // Add this term to the list
                        terms.Add(newTerm);
                    }
                    catch (Exception ex)
                    {
                        // If a term cannot be added to the domain log an error but continue loading the rest of the terms.
                        LogDataError("Term", term.Recordkey, term, ex);
                    }
                }
            }
            return terms;
        }

        /// <summary>
        /// Wrapper around Async, used by FinancialAid branch for AcademicProgressService
        /// </summary>
        /// <param name="prog"></param>
        /// <param name="cat"></param>
        /// <returns></returns>
        public IEnumerable<Term> Get()
        {
                var x = Task.Run(async () =>
                {
                    return await GetAsync();
                }).GetAwaiter().GetResult();
                return x;
        }

        /// <summary>
        /// Wrapper around Async, used by FinancialAid branch for AcademicProgressService
        /// </summary>
        /// <param name="prog"></param>
        /// <param name="cat"></param>
        /// <returns></returns>
        public Term Get(string id)
        {
            var x = Task.Run(async () =>
            {
                return await GetAsync(id);
            }).GetAwaiter().GetResult();
            return x;
        }

        /// <summary>
        /// Retrieve all Academic Periods
        /// </summary>
        /// <returns>All Academic Periods</returns>
        public IEnumerable<Ellucian.Colleague.Domain.Student.Entities.AcademicPeriod> GetAcademicPeriods(IEnumerable<Term> terms)
        {
            var academicPeriods = new List<Ellucian.Colleague.Domain.Student.Entities.AcademicPeriod>();
            terms = terms.OrderBy(r => r.ReportingYear).ThenBy(s => s.Sequence);

            // Get a collection of Terms by ReportingYear
            var reportingYearGrouping = terms.GroupBy(p => p.ReportingYear);

            if (reportingYearGrouping != null && reportingYearGrouping.Count() > 0)
            {

                // Iterate through the collection for each reporting year
                foreach (var reportingYearGroup in reportingYearGrouping)
                {
                    // First item in the reporting year group will have no preceeding term
                    Term preceedingTerm = null;
                    foreach (var period in reportingYearGroup)
                    {
                        string parentId = null;
                        var preceedingTermID = preceedingTerm == null ? null : preceedingTerm.RecordGuid;

                        // If the reporting term is the same as the term code, then it has no parent
                        if (IsReportingTermEqualCode(period))
                            parentId = period.RecordGuid;
                        else if (!String.IsNullOrEmpty(period.ReportingTerm))
                        {
                            var parentReportingTerm = terms.Where(t => t.Code == period.ReportingTerm).FirstOrDefault();
                            if (parentReportingTerm == null)
                            {
                                string errorMessage = "Unable to locate parent reporting term: " + period.ReportingTerm + " for " + period.Code;
                                logger.Error(errorMessage);
                                throw new KeyNotFoundException(errorMessage);
                            }
                            else
                                parentId = parentReportingTerm.RecordGuid;
                        }
                                                                         
                        var academicPeriod = new Ellucian.Colleague.Domain.Student.Entities.AcademicPeriod(period.RecordGuid,
                                                                period.Code,
                                                                period.Description,
                                                                period.StartDate,
                                                                period.EndDate,
                                                                period.ReportingYear,
                                                                period.Sequence,
                                                                period.ReportingTerm,
                                                                preceedingTermID,
                                                                parentId,
                                                                period.RegistrationDates);

                        academicPeriods.Add(academicPeriod);
                        preceedingTerm = period;
                    }
                }
            }

            return academicPeriods;
        }

        /// <summary>
        /// Determine if a Terms reporting term is the same as its code
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        private bool IsReportingTermEqualCode(Term term)
        {
            if (term == null)
            {
                return false;
            }
            //if the reporting term is the same as the term code, then it has no parent
            return (term.ReportingTerm == term.Code);
        }
    }
}
