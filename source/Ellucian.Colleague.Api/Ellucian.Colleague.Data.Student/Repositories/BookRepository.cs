﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Ellucian.Colleague.Data.Student.DataContracts;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Web.Utility;
using slf4net;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.Student.Repositories
{
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class BookRepository : BaseColleagueRepository, IBookRepository
    {
        public BookRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            // Using level 1 cache time out value for data that rarely changes.
            CacheTimeout = Level1CacheTimeoutValue;
        }

        public async Task<Book> GetAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id", "Book id may not be null or empty");
            }

            Books bookItem = await DataReader.ReadRecordAsync<Books>("BOOKS", id);
            List<Books> bookList = new List<Books>() { bookItem };
            Book book = BuildBooks(bookList).FirstOrDefault();
            return book;
        }

        public async Task<IEnumerable<Book>> GetAsync()
        {
            // This can bring back thousands of books. Is there a way to bring back a reduced set of books? 
            // May only need the 1 by 1 version.
            var books = await GetOrAddToCacheAsync<List<Book>>("AllBooks",
               async () =>
                {
                    Collection<Books> bookData = await DataReader.BulkReadRecordAsync<Books>("BOOKS", "");
                    var bookList = BuildBooks(bookData.ToList());
                    return bookList.ToList();
                }
            );
            return books;
        }

        private IEnumerable<Book> BuildBooks(List<Books> bookData)
        {
            // For now we are only returning book price information, but will eventually need book details.
            List<Book> bookCollection = new List<Book>();
            if (bookData != null)
            {
                foreach (var book in bookData)
                {
                    try
                    {
                        Book bookItem = new Book(book.Recordkey);
                        bookItem.Price = book.BookPrice;
                        bookCollection.Add(bookItem);

                    }
                    catch (Exception ex)
                    {
                        LogDataError("Book", book.Recordkey, bookData, ex);
                    } 
                }
               
            }
            return bookCollection;
        }
    }
}
