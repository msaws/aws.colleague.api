﻿using Ellucian.Colleague.Domain.Student.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Domain.Entities;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Colleague.Data.Student.DataContracts;
using slf4net;

namespace Ellucian.Colleague.Data.Student.Repositories
{
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class StudentAdvisorRelationshipsRepository : BaseColleagueRepository, IStudentAdvisorRelationshipsRepository
    {

        public StudentAdvisorRelationshipsRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
        }

        /// <summary>
        /// Get all records for StudentAdvisorRelationShips from STUDENT.ADVISEMENT entity. This method uses the filters and paging.
        /// </summary>
        /// <param name="offset">Used to offset the values</param>
        /// <param name="limit">Used to limit the number of records</param>
        /// <param name="bypassCache"></param>
        /// <param name="student">Filter the data by student ID</param>
        /// <param name="advisor">Filter the data by faculty ID</param>
        /// <param name="advisorType">Filter the records by advisor type</param>
        /// <returns></returns>
        public async Task<Tuple<IEnumerable<StudentAdvisorRelationship>,int>> GetStudentAdvisorRelationshipsAsync(int offset, int limit, bool bypassCache = false,
            string student = "", string advisor = "", string advisorType = "")
        {
            StringBuilder criteria = new StringBuilder();
            //make sure we get records based on required fields.
            criteria.Append("WITH STAD.STUDENT NE '' AND STAD.FACULTY NE '' AND STAD.START.DATE NE ''");

            // filter by student ID
            if (!string.IsNullOrEmpty(student))
            {
                criteria.Append(" AND WITH STAD.STUDENT = '");
                criteria.Append(student);
                criteria.Append("'");
            }

            //filter by Advisor ID
            if (!string.IsNullOrEmpty(advisor))
            {
                criteria.Append(" AND WITH STAD.FACULTY = '");
                criteria.Append(advisor);
                criteria.Append("'");
            }

            //Fliter by Advisor type
            if (!string.IsNullOrEmpty(advisorType))
            {
                criteria.Append(" AND WITH STAD.TYPE = '");
                criteria.Append(advisorType);
                criteria.Append("'");
            }

            //Get all ID's based on criteria
            var studentAdvisementIds = await DataReader.SelectAsync("STUDENT.ADVISEMENT", criteria.ToString());
            
            // update total count and get the ID's we want to page off of
            var totalCount = studentAdvisementIds.Count();
            Array.Sort(studentAdvisementIds);
            var subList = studentAdvisementIds.Skip(offset).Take(limit).ToArray();

            //get all records data and parse through it to add to our entity
            var studentAdvisementsData = await DataReader.BulkReadRecordAsync<DataContracts.StudentAdvisement>("STUDENT.ADVISEMENT", subList);

            if (studentAdvisementsData == null)
            {
                throw new KeyNotFoundException("No records selected from STUDENT.ADVISEMENT file in Colleague.");
            }

            List<StudentAdvisorRelationship> StudentAdvisorRelationshipsEntity = new List<StudentAdvisorRelationship>();


            foreach (var studentAdvisement in studentAdvisementsData)
            {
                StudentAdvisorRelationshipsEntity.Add(BuildStudentAdvisorRelationship(studentAdvisement));
            }

            return new Tuple<IEnumerable<StudentAdvisorRelationship>, int>(StudentAdvisorRelationshipsEntity.AsEnumerable(), totalCount);

        }

        /// <summary>
        /// Get a student advisor Relationship based on guid
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public async Task<StudentAdvisorRelationship> GetStudentAdvisorRelationshipsByGuidAsync(string guid)
        {
           
            try
            {
                //tranlate the GUID to a valid ID
                string id = await GetStudentAdvisementIdFromGuidAsync(guid);
                if (string.IsNullOrEmpty(id))
                {
                    throw new KeyNotFoundException(string.Concat("Id not found for voucher guid:", guid));
                }
                //Parse the ID to the Entity
                return await GetStudentAdvisementAsync(id);
            }
            catch (ArgumentNullException e)
            {
                logger.Error(e.Message);
                throw new ArgumentNullException(e.Message);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.Message);
                throw new ArgumentException(e.Message);
            }
            catch (KeyNotFoundException e)
            {
                logger.Error(e.Message);
                throw new KeyNotFoundException(e.Message);
            }
            catch (RepositoryException e)
            {
               var RepoExc = new RepositoryException(e.Message, e);
               RepoExc.AddError(new RepositoryError("student.advisement.GetByGUID",e.Message));
               throw RepoExc;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                throw new KeyNotFoundException("STUDENT.ADVISEMENT GUID " + guid + " lookup failed.");
            }
            
        }

        /// <summary>
        /// Get the ID of STUDENT.ADVISOMENT from a GUID
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        private async Task<string> GetStudentAdvisementIdFromGuidAsync(string guid)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw new ArgumentNullException("guid", "STUDENT.ADVISEMENT - GUID can not be null or empty");
            }

            var idDict = await DataReader.SelectAsync(new GuidLookup[] { new GuidLookup(guid) });
            if (idDict == null || idDict.Count == 0)
            {
                //Guid was not found
                throw new KeyNotFoundException("STUDENT.ADVISEMENT GUID " + guid + " not found.");
            }

            var foundEntry = idDict.FirstOrDefault();
            if (foundEntry.Value == null)
            {
                //Guid was not found
                throw new KeyNotFoundException("STUDENT.ADVISEMENT GUID " + guid + " lookup failed.");
            }

            if (foundEntry.Value.Entity != "STUDENT.ADVISEMENT")
            {
                //GUID record was not from a STUDENT.ADVISEMENT record
                throw new RepositoryException("GUID used '" + guid + "' is for a different entity in colleague, it is for" + foundEntry.Value.Entity + ". We need the entity to be STUDENT.ADVISEMENT");
            }

            return foundEntry.Value.PrimaryKey;
        }

        /// <summary>
        /// Get the STUDENT.ADVISEMENT record by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private async Task<StudentAdvisorRelationship> GetStudentAdvisementAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id", "ID is required to get a STUDENT.ADVISEMENT.");
            }

            var studentAdvisement = await DataReader.ReadRecordAsync<StudentAdvisement>(id);
            if (studentAdvisement == null)
            {
                throw new KeyNotFoundException(string.Concat("Record not found for STUDENT.ADVISEMENT with ID ", id, "invalid."));
            }

            return BuildStudentAdvisorRelationship(studentAdvisement);
        }

        /// <summary>
        /// Get a STUDENT.ADVISEMENT record and convert it to a StudentAdvisorRelationship entity
        /// </summary>
        /// <param name="studentAdvisement"></param>
        /// <returns></returns>
        private StudentAdvisorRelationship BuildStudentAdvisorRelationship(StudentAdvisement studentAdvisement)
        {
            StudentAdvisorRelationship sar = new StudentAdvisorRelationship();
            sar.id = studentAdvisement.Recordkey;
            sar.guid = studentAdvisement.RecordGuid;
            sar.advisor = studentAdvisement.StadFaculty;
            sar.student = studentAdvisement.StadStudent;
            sar.advisorType = studentAdvisement.StadType;
            sar.program = studentAdvisement.StadAcadProgram;
            sar.startOn = studentAdvisement.StadStartDate;
            sar.endOn = studentAdvisement.StadEndDate;

            return sar;
        }
    }
}
