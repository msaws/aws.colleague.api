﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Colleague.Data.ColleagueFinance.Utilities;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace Ellucian.Colleague.Data.ColleagueFinance.Tests.Utilities
{
    [TestClass]
    public class GlAccountUtilityTests
    {
        #region Initialize and Cleanup

        GeneralLedgerClassConfiguration glClassConfiguration = null;
        string glAccount = null;

        private string glClassName = "GL.CLASS";
        private List<string> glExpenseValues = new List<string>() { "5", "7" };
        private List<string> glRevenueValues = new List<string>() { "4", "6" };
        private List<string> glAssetValues = new List<string>() { "1" };
        private List<string> glLiabilityValues = new List<string>() { "2" };
        private List<string> glFundBalValues = new List<string>() { "3" };
        private GlClass GlClass = GlClass.Asset;

        [TestInitialize]
        public void Initialize()
        {
            // Initialize the GL Class configuration
            glClassConfiguration = new GeneralLedgerClassConfiguration(glClassName, glExpenseValues, glRevenueValues, glAssetValues, glLiabilityValues, glFundBalValues);
            glClassConfiguration.GlClassStartPosition = 18;
            glClassConfiguration.GlClassLength = 1;
        }

        [TestCleanup]
        public void Cleanup()
        {
            glClassConfiguration = null;
        }
        #endregion

        [TestMethod]
        public void GlClassIs_Asset()
        {
            glAccount = "11_01_01_00_00000_10000";
            Assert.AreEqual(GlAccountUtility.GetGlAccountGlClass(glAccount, glClassConfiguration), GlClass.Asset);
        }

        [TestMethod]
        public void GlClassIs_Liability()
        {
            glAccount = "11_01_01_00_00000_20000";
            Assert.AreEqual(GlAccountUtility.GetGlAccountGlClass(glAccount, glClassConfiguration), GlClass.Liability);
        }

        [TestMethod]
        public void GlClassIs_FundBalance()
        {
            glAccount = "11_01_01_00_00000_30000";
            Assert.AreEqual(GlAccountUtility.GetGlAccountGlClass(glAccount, glClassConfiguration), GlClass.FundBalance);
        }

        [TestMethod]
        public void GlClassIs_Revenue()
        {
            glAccount = "11_01_01_00_00000_40000";
            Assert.AreEqual(GlAccountUtility.GetGlAccountGlClass(glAccount, glClassConfiguration), GlClass.Revenue);
        }

        [TestMethod]
        public void GlClassIs_Expense()
        {
            glAccount = "11_01_01_00_00000_50000";
            Assert.AreEqual(GlAccountUtility.GetGlAccountGlClass(glAccount, glClassConfiguration), GlClass.Expense);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GlClassIs_Missing()
        {
            glAccount = "11_01_01_00_00000_10000";
            glClassConfiguration.GlClassStartPosition = 0;
            glClassConfiguration.GlClassLength = 0;
            GlClass glClass = GlAccountUtility.GetGlAccountGlClass(glAccount, glClassConfiguration);
        }

        [TestMethod]
        public void GlClassIs_Invalid()
        {
            glAccount = "11_01_01_00_00000_90000";
            var expectedMessage = "Invalid glClass for GL account: " + glAccount;
            var actualMessage = "";
            try
            {
                GlClass glClass = GlAccountUtility.GetGlAccountGlClass(glAccount, glClassConfiguration);
            }
            catch (ApplicationException aex)
            {
                actualMessage = aex.Message;
            }
            Assert.AreEqual(expectedMessage, actualMessage);
        }
    }
}