﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.ColleagueFinance.DataContracts;
using Ellucian.Colleague.Data.ColleagueFinance.Repositories;
using Ellucian.Colleague.Data.ColleagueFinance.Transactions;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ColleagueFinance.Tests;
using Ellucian.Data.Colleague;
using Ellucian.Web.Cache;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;

namespace Ellucian.Colleague.Data.ColleagueFinance.Tests.Repositories
{
    [TestClass]
    public class RequisitionRepositoryTests
    {
        #region Initialize and Cleanup

        private Mock<IColleagueDataReader> dataReader = null;
        private Mock<IColleagueTransactionInvoker> transactionInvoker = null;
        private RequisitionRepository requisitionRepository;
        private TestRequisitionRepository testRequisitionRepository;
        private Requisition requisitionDomainEntity;

        // Data contract objects
        private Requisitions requisitionDataContract;
        private Collection<Opers> opersDataContracts;
        private ShipToCodes shipToCodesDataContract;
        private Collection<Items> itemsDataContracts;
        private Collection<Projects> projectDataContracts;
        private Collection<ProjectsLineItems> projectLineItemDataContracts;
        private GetHierarchyNamesForIdsResponse hierarchyNamesForIdsResponse;
        private TxCheckUserGlAccessResponse checkUserGlAccessResponse;
        private Collection<Opers> opersResponse;

        private string personId = "1";
        private string requisitionIdForTransaction;

        [TestInitialize]
        public void Initialize()
        {
            // Set up a mock data reader
            dataReader = new Mock<IColleagueDataReader>();

            // Set up a mock transaction invoker
            transactionInvoker = new Mock<IColleagueTransactionInvoker>();

            // Initialize the data contract object
            requisitionDataContract = new Requisitions();

            // Initialize the requisition repository
            testRequisitionRepository = new TestRequisitionRepository();

            requisitionIdForTransaction = "";

            this.requisitionRepository = BuildRequisitionRepository();
        }

        [TestCleanup]
        public void Cleanup()
        {
            dataReader = null;
            transactionInvoker = null;
            requisitionDataContract = null;
            shipToCodesDataContract = null;
            opersDataContracts = null;
            itemsDataContracts = null;
            projectDataContracts = null;
            projectLineItemDataContracts = null;
            testRequisitionRepository = null;
            requisitionDomainEntity = null;
            hierarchyNamesForIdsResponse = null;
            checkUserGlAccessResponse = null;
            opersResponse = null;
            requisitionIdForTransaction = null;
        }

        #endregion

        #region Base Requisition Test
        [TestMethod]
        public async Task GetRequisition_Base()
        {
            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);

            // Confirm that the SV properties for the requisition are the same
            Assert.AreEqual(this.requisitionDomainEntity.Id, requisition.Id);
            Assert.AreEqual(this.requisitionDomainEntity.VendorId, requisition.VendorId);
            Assert.AreEqual(this.requisitionDomainEntity.VendorName, requisition.VendorName);
            Assert.AreEqual(this.requisitionDomainEntity.InitiatorName, requisition.InitiatorName);
            Assert.AreEqual(this.requisitionDomainEntity.RequestorName, requisition.RequestorName);
            Assert.AreEqual(this.requisitionDomainEntity.Status, requisition.Status);
            Assert.AreEqual(this.requisitionDomainEntity.StatusDate, requisition.StatusDate);
            Assert.AreEqual(this.requisitionDomainEntity.Amount, requisition.Amount);
            Assert.AreEqual(this.requisitionDomainEntity.ApType, requisition.ApType);
            Assert.AreEqual(this.requisitionDomainEntity.Date, requisition.Date);
            Assert.AreEqual(this.requisitionDomainEntity.MaintenanceDate, requisition.MaintenanceDate);
            Assert.AreEqual(this.requisitionDomainEntity.DesiredDate, requisition.DesiredDate);
            Assert.AreEqual(this.requisitionDomainEntity.ShipToCode, requisition.ShipToCode);
            Assert.AreEqual(this.requisitionDomainEntity.Comments, requisition.Comments);
            Assert.AreEqual(this.requisitionDomainEntity.InternalComments, requisition.InternalComments);
            Assert.AreEqual(this.requisitionDomainEntity.CurrencyCode, requisition.CurrencyCode);
            Assert.AreEqual(this.requisitionDomainEntity.BlanketPurchaseOrder, requisition.BlanketPurchaseOrder);
        }
        #endregion

        #region Invalid data tests
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task GetRequisition_NullId()
        {
            var requisition = await this.requisitionRepository.GetRequisitionAsync(null, personId, GlAccessLevel.Full_Access, null);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task GetRequisition_NullRequisition()
        {
            // Mock ReadRecord to return a pre-defined, null requisition data contract
            var nullRequisitionObject = new Requisitions();
            nullRequisitionObject = null;
            dataReader.Setup<Task<Requisitions>>(acc => acc.ReadRecordAsync<Requisitions>(It.IsAny<string>(), true)).Returns(Task.FromResult(nullRequisitionObject));
            var requisition = await this.requisitionRepository.GetRequisitionAsync("1", personId, GlAccessLevel.Full_Access, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task GetRequisition_NullStatus()
        {
            var requisitionDataContract = new Requisitions()
            {
                Recordkey = "10",
                ReqStatus = null
            };

            dataReader.Setup<Task<Requisitions>>(acc => acc.ReadRecordAsync<Requisitions>(It.IsAny<string>(), true)).Returns(Task.FromResult(requisitionDataContract));
            var requisition = await requisitionRepository.GetRequisitionAsync("10", "1", GlAccessLevel.Full_Access, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task GetRequisition_StatusListHasBlankValue()
        {
            var requisitionDataContract = new Requisitions()
            {
                Recordkey = "10",
                ReqStatus = new List<string>() { "" }
            };

            dataReader.Setup<Task<Requisitions>>(acc => acc.ReadRecordAsync<Requisitions>(It.IsAny<string>(), true)).Returns(Task.FromResult(requisitionDataContract));
            var requisition = await requisitionRepository.GetRequisitionAsync("10", "1", GlAccessLevel.Full_Access, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task GetRequisition_StatusDateHasNullValue()
        {
            var requisitionDataContract = new Requisitions()
            {
                Recordkey = "10",
                ReqStatus = new List<string>() { "P" },
                ReqStatusDate = new List<DateTime?>() { null }
            };

            dataReader.Setup<Task<Requisitions>>(acc => acc.ReadRecordAsync<Requisitions>(It.IsAny<string>(), true)).Returns(Task.FromResult(requisitionDataContract));
            var requisition = await requisitionRepository.GetRequisitionAsync("10", "1", GlAccessLevel.Full_Access, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task GetRequisition_InvalidRequisitionStatus()
        {
            // Mock ReadRecord to return a pre-defined, null requisition data contract
            var requisitionObject = new Requisitions()
            {
                Recordkey = "1",
                ReqStatus = new List<string>() { "Z" }
            };
            dataReader.Setup<Task<Requisitions>>(acc => acc.ReadRecordAsync<Requisitions>(It.IsAny<string>(), true)).Returns(Task.FromResult(requisitionObject));
            var requisition = await this.requisitionRepository.GetRequisitionAsync("1", personId, GlAccessLevel.Full_Access, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task GetRequisition_NullStatusDate()
        {
            var requisitionDataContract = new Requisitions()
            {
                Recordkey = "10",
                ReqStatus = new List<string>() { "P" },
                ReqStatusDate = null
            };

            dataReader.Setup<Task<Requisitions>>(acc => acc.ReadRecordAsync<Requisitions>(It.IsAny<string>(), true)).Returns(Task.FromResult(requisitionDataContract));
            var requisition = await requisitionRepository.GetRequisitionAsync("10", "1", GlAccessLevel.Full_Access, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task GetRequisition_StatusDateListHasNullValue()
        {
            var requisitionDataContract = new Requisitions()
            {
                Recordkey = "10",
                ReqStatus = new List<string>() { "P" },
                ReqStatusDate = new List<DateTime?>() { null }
            };

            dataReader.Setup<Task<Requisitions>>(acc => acc.ReadRecordAsync<Requisitions>(It.IsAny<string>(), true)).Returns(Task.FromResult(requisitionDataContract));
            var requisition = await requisitionRepository.GetRequisitionAsync("10", "1", GlAccessLevel.Full_Access, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task GetRequisition_NullReqDate()
        {
            var requisitionDataContract = new Requisitions()
            {
                Recordkey = "10",
                ReqStatus = new List<string>() { "P" },
                ReqStatusDate = new List<DateTime?>() { new DateTime(2015, 1, 1) },
                ReqDate = null
            };

            dataReader.Setup<Task<Requisitions>>(acc => acc.ReadRecordAsync<Requisitions>(It.IsAny<string>(), true)).Returns(Task.FromResult(requisitionDataContract));
            var requisition = await requisitionRepository.GetRequisitionAsync("10", "1", GlAccessLevel.Full_Access, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public async Task GetRequisition_MultipleBpos()
        {
            string requisitionId = "4";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            ConvertDomainEntitiesIntoDataContracts();
            this.requisitionDataContract.ReqBpoNo = new List<string>() { "B0001", "B0002" };
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
        }

        [TestMethod]
        public async Task GetRequisitionAsync_InitiatorAndRequstorAreNull()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.requisitionDataContract.ReqRequestor = null;
            this.requisitionDataContract.ReqDefaultInitiator = null;
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDataContract.ReqMiscName.FirstOrDefault(), requisition.VendorName);
            Assert.IsTrue(string.IsNullOrEmpty(requisition.InitiatorName));
            Assert.IsTrue(string.IsNullOrEmpty(requisition.RequestorName));
        }

        [TestMethod]
        public async Task GetRequisitionAsync_HierarchyNamesCtxReturnsNullNamesList()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.hierarchyNamesForIdsResponse.OutPersonNames = null;
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDataContract.ReqMiscName.FirstOrDefault(), requisition.VendorName);
            Assert.IsTrue(string.IsNullOrEmpty(requisition.InitiatorName));
            Assert.IsTrue(string.IsNullOrEmpty(requisition.RequestorName));
        }

        [TestMethod]
        public async Task GetRequisitionAsync_HierarchyNamesCtxReturnsEmptyNamesList()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.hierarchyNamesForIdsResponse.OutPersonNames = new List<string>();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDataContract.ReqMiscName.FirstOrDefault(), requisition.VendorName);
            Assert.IsTrue(string.IsNullOrEmpty(requisition.InitiatorName));
            Assert.IsTrue(string.IsNullOrEmpty(requisition.RequestorName));
        }

        [TestMethod]
        public async Task GetRequisitionAsync_ReqPoNoIsNull()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.requisitionDataContract.ReqPoNo = null;
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(0, requisition.PurchaseOrders.Count);
        }

        [TestMethod]
        public async Task GetRequisitionAsync_ReqPoNoIsEmpty()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.requisitionDataContract.ReqPoNo = new List<string>();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(0, requisition.PurchaseOrders.Count);
        }

        [TestMethod]
        public async Task GetRequisitionAsync_ReqBpoNoIsNull()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.requisitionDataContract.ReqBpoNo = null;
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.IsTrue(string.IsNullOrEmpty(requisition.BlanketPurchaseOrder));
        }

        [TestMethod]
        public async Task GetRequisitionAsync_ReqBpoNoIsEmpty()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.requisitionDataContract.ReqBpoNo = new List<string>();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.IsTrue(string.IsNullOrEmpty(requisition.BlanketPurchaseOrder));
        }

        [TestMethod]
        public async Task GetRequisitionAsync_OpersBulkReadReturnsNull()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.opersResponse = null;
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(0, requisition.Approvers.Count);
        }

        [TestMethod]
        public async Task GetRequisitionAsync_OpersBulkReadReturnsEmptyList()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.opersResponse = new Collection<Opers>();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(0, requisition.Approvers.Count);
        }

        [TestMethod]
        public async Task GetRequisitionAsync_ApproversAssociationIsNull()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.requisitionDataContract.ReqAuthEntityAssociation = null;
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(this.opersResponse.Count, requisition.Approvers.Count);
            foreach (var approver in requisition.Approvers)
            {
                Assert.IsNull(approver.ApprovalDate);
            }
        }

        [TestMethod]
        public async Task GetRequisitionAsync_ApproversAssociationIsEmpty()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.requisitionDataContract.ReqAuthEntityAssociation = new List<RequisitionsReqAuth>();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(this.opersResponse.Count, requisition.Approvers.Count);
            foreach (var approver in requisition.Approvers)
            {
                Assert.IsNull(approver.ApprovalDate);
            }
        }

        [TestMethod]
        public async Task GetRequisitionAsync_ReqItemIdIsNull()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.requisitionDataContract.ReqItemsId = null;
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(0, requisition.LineItems.Count);
        }

        [TestMethod]
        public async Task GetRequisitionAsync_ReqItemIdIsEmpty()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.requisitionDataContract.ReqItemsId = new List<string>();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(0, requisition.LineItems.Count);
        }

        [TestMethod]
        public async Task GetRequisitionAsync_ItemsBulkReadReturnsNull()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.itemsDataContracts = null;
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(0, requisition.LineItems.Count);
        }

        [TestMethod]
        public async Task GetRequisitionAsync_ItemsBulkReadReturnsEmpty()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.itemsDataContracts = null;
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(0, requisition.LineItems.Count);
        }

        [TestMethod]
        public async Task GetRequisitionAsync_LineItemAssociationIsNull()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            foreach (var lineItem in this.itemsDataContracts)
            {
                lineItem.ItemReqEntityAssociation = null;
            }
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(0, requisition.LineItems.Count);
        }

        [TestMethod]
        public async Task GetRequisitionAsync_LineItemAssociationIsEmpty()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            foreach (var lineItem in this.itemsDataContracts)
            {
                lineItem.ItemReqEntityAssociation = new List<ItemsItemReq>();
            }
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(0, requisition.LineItems.Count);
        }

        [TestMethod]
        public async Task GetRequisitionAsync_LineItem_QuantityPriceAndExtendedPriceAreNull()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            foreach (var lineItem in this.itemsDataContracts)
            {
                lineItem.ItmReqQty = null;
                lineItem.ItmReqPrice = null;
                lineItem.ItmReqExtPrice = null;
            }
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            foreach (var lineItem in requisition.LineItems)
            {
                Assert.AreEqual(0, lineItem.Quantity);
                Assert.AreEqual(0, lineItem.Price);
                Assert.AreEqual(0, lineItem.ExtendedPrice);
            }
        }

        [TestMethod]
        public async Task GetRequisitionAsync_GlDistribution_QuantityAndAmountAreNull()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            foreach (var glDistribution in this.itemsDataContracts.SelectMany(x => x.ItemReqEntityAssociation).ToList())
            {
                glDistribution.ItmReqGlQtyAssocMember = null;
                glDistribution.ItmReqGlAmtAssocMember = null;
            }
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            foreach (var glDistribution in requisition.LineItems.SelectMany(x => x.GlDistributions).ToList())
            {
                Assert.AreEqual(0, glDistribution.Quantity);
                Assert.AreEqual(0, glDistribution.Amount);
            }
        }

        [TestMethod]
        public async Task GetRequisitionAsync_GlDistribution_ForeignAmountIsNull()
        {

            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.requisitionDataContract.ReqCurrencyCode = "CA";
            foreach (var glDistribution in this.itemsDataContracts.SelectMany(x => x.ItemReqEntityAssociation).ToList())
            {
                glDistribution.ItmReqGlForeignAmtAssocMember = null;
                glDistribution.ItmReqGlAmtAssocMember = null;
            }
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            foreach (var glDistribution in requisition.LineItems.SelectMany(x => x.GlDistributions).ToList())
            {
                Assert.AreEqual(0, glDistribution.Amount);
            }
        }

        [TestMethod]
        public async Task GetRequisitionAsync_TaxAssociationIsNull()
        {
            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.requisitionDataContract.ReqCurrencyCode = "CA";
            foreach (var lineItem in this.itemsDataContracts)
            {
                lineItem.ReqGlTaxesEntityAssociation = null;
            }
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            foreach (var lineItem in requisition.LineItems)
            {
                Assert.AreEqual(0, lineItem.LineItemTaxes.Count);
            }
        }

        [TestMethod]
        public async Task GetRequisitionAsync_TaxAssociationIsEmpty()
        {
            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.requisitionDataContract.ReqCurrencyCode = "CA";
            foreach (var lineItem in this.itemsDataContracts)
            {
                lineItem.ReqGlTaxesEntityAssociation = new List<ItemsReqGlTaxes>();
            }
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            foreach (var lineItem in requisition.LineItems)
            {
                Assert.AreEqual(0, lineItem.LineItemTaxes.Count);
            }
        }

        [TestMethod]
        public async Task GetRequisitionAsync_LocalCurrency_AmountIsNull()
        {
            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.requisitionDataContract.ReqCurrencyCode = "";
            foreach (var taxAssociation in this.itemsDataContracts.SelectMany(x => x.ReqGlTaxesEntityAssociation.ToList()))
            {
                taxAssociation.ItmReqGlTaxAmtAssocMember = null;
            }
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            var expectedAmount = this.itemsDataContracts.SelectMany(x => x.ItemReqEntityAssociation).ToList().Sum(x => x.ItmReqGlAmtAssocMember);
            Assert.AreEqual(expectedAmount, requisition.Amount);
            foreach (var tax in requisition.LineItems.SelectMany(x => x.LineItemTaxes).ToList())
            {
                Assert.AreEqual(0, tax.TaxAmount);
            }
        }

        [TestMethod]
        public async Task GetRequisitionAsync_ForeignCurrency_AmountIsNull()
        {
            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.requisitionDataContract.ReqCurrencyCode = "CA";
            foreach (var taxAssociation in this.itemsDataContracts.SelectMany(x => x.ReqGlTaxesEntityAssociation.ToList()))
            {
                taxAssociation.ItmReqGlTaxAmtAssocMember = null;
            }
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            var expectedAmount = this.itemsDataContracts.SelectMany(x => x.ItemReqEntityAssociation).ToList().Sum(x => x.ItmReqGlForeignAmtAssocMember);
            Assert.AreEqual(expectedAmount, requisition.Amount);
            foreach (var tax in requisition.LineItems.SelectMany(x => x.LineItemTaxes).ToList())
            {
                Assert.AreEqual(0, tax.TaxAmount);
            }
        }

        [TestMethod]
        public async Task GetRequisitionAsync_ProjectsBulkReadReturnsNull()
        {
            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.projectDataContracts = null;
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            foreach (var glDistribution in requisition.LineItems.SelectMany(x => x.GlDistributions).ToList())
            {
                Assert.IsTrue(string.IsNullOrEmpty(glDistribution.ProjectNumber));
                Assert.IsTrue(string.IsNullOrEmpty(glDistribution.ProjectLineItemCode));
            }
        }

        [TestMethod]
        public async Task GetRequisitionAsync_ProjectsBulkReadReturnsEmptyCollection()
        {
            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.projectDataContracts = new Collection<Projects>();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            foreach (var glDistribution in requisition.LineItems.SelectMany(x => x.GlDistributions).ToList())
            {
                Assert.IsTrue(string.IsNullOrEmpty(glDistribution.ProjectNumber));
                Assert.IsTrue(string.IsNullOrEmpty(glDistribution.ProjectLineItemCode));
            }
        }

        [TestMethod]
        public async Task GetRequisitionAsync_ProjectsLineItemsBulkReadReturnsNull()
        {
            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.projectLineItemDataContracts = null;
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            foreach (var glDistribution in requisition.LineItems.SelectMany(x => x.GlDistributions).ToList())
            {
                Assert.IsFalse(string.IsNullOrEmpty(glDistribution.ProjectNumber));
                Assert.IsTrue(string.IsNullOrEmpty(glDistribution.ProjectLineItemCode));
            }
        }

        [TestMethod]
        public async Task GetRequisitionAsync_ProjectsLineItemsBulkReadReturnsEmptyCollection()
        {
            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();

            this.projectLineItemDataContracts = new Collection<ProjectsLineItems>();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            foreach (var glDistribution in requisition.LineItems.SelectMany(x => x.GlDistributions).ToList())
            {
                Assert.IsFalse(string.IsNullOrEmpty(glDistribution.ProjectNumber));
                Assert.IsTrue(string.IsNullOrEmpty(glDistribution.ProjectLineItemCode));
            }
        }
        #endregion

        #region Status tests
        [TestMethod]
        public async Task GetRequisition_UStatus()
        {
            string requisitionId = "4";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);
            Assert.AreEqual(RequisitionStatus.InProgress, requisition.Status);
        }

        [TestMethod]
        public async Task GetRequisition_NStatus()
        {
            string requisitionId = "2";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);
            Assert.AreEqual(RequisitionStatus.NotApproved, requisition.Status);
        }

        [TestMethod]
        public async Task GetRequisition_OStatus()
        {
            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);
            Assert.AreEqual(RequisitionStatus.Outstanding, requisition.Status);
        }

        [TestMethod]
        public async Task GetRequisition_PStatus()
        {
            string requisitionId = "6";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);
            Assert.AreEqual(RequisitionStatus.PoCreated, requisition.Status);
        }
        #endregion

        #region Vendor tests
        [TestMethod]
        public async Task GetRequisition_VendorNameOnly_ShortVendorName()
        {
            string requisitionId = "5";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDomainEntity.VendorName, requisition.VendorName);
        }

        [TestMethod]
        public async Task GetRequisition_VendorNameOnly_LongVendorName()
        {
            string requisitionId = "6";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDomainEntity.VendorName, requisition.VendorName);
        }

        [TestMethod]
        public async Task GetRequisition_VendorIdOnly_CTXShortName()
        {
            string requisitionId = "7";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);

            Assert.AreEqual(this.hierarchyNamesForIdsResponse.OutPersonNames.First(), requisition.VendorName);
        }

        [TestMethod]
        public async Task GetRequisition_VendorIdOnly_CTXLongName()
        {
            string requisitionId = "8";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts(true);
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);

            Assert.AreEqual(this.hierarchyNamesForIdsResponse.OutPersonNames.First(), requisition.VendorName);
        }
        [TestMethod]
        public async Task GetRequisition_HasVendorIdAndName()
        {
            string requisitionId = "4";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDomainEntity.VendorName, requisition.VendorName);
        }

        [TestMethod]
        public async Task GetRequisition_NoVendorIdOrName()
        {
            string requisitionId = "10";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);
            Assert.IsTrue(string.IsNullOrEmpty(requisition.VendorName));
        }
        #endregion

        #region PO tests
        [TestMethod]
        public async Task GetRequisition_ConvertedToPurchaseOrder()
        {
            string requisitionId = "3";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDomainEntity.PurchaseOrders.Count(), requisition.PurchaseOrders.Count());

            for (int i = 0; i < this.requisitionDomainEntity.PurchaseOrders.Count(); i++)
            {
                Assert.AreEqual(this.requisitionDomainEntity.PurchaseOrders[i], requisition.PurchaseOrders[i]);
            }
        }
        #endregion

        #region Approvers and Next Approvers tests
        [TestMethod]
        public async Task GetRequisition_HasApproversAndNextApprovers()
        {
            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDomainEntity.Approvers.Count(), requisition.Approvers.Count());
            foreach (var approver in this.requisitionDomainEntity.Approvers)
            {
                Assert.IsTrue(requisition.Approvers.Any(x =>
                    x.ApproverId == approver.ApproverId
                    && x.ApprovalName == approver.ApprovalName
                    && x.ApprovalDate == approver.ApprovalDate));
            }
        }
        #endregion

        #region LineItems tests
        [TestMethod]
        public async Task GetRequisition_LineItems_Base()
        {
            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDomainEntity.LineItems.Count(), requisition.LineItems.Count(), "Requisitions should have the same number of line items.");

            foreach (var lineItem in this.requisitionDomainEntity.LineItems)
            {
                Assert.IsTrue(requisition.LineItems.Any(x =>
                    x.Comments == lineItem.Comments
                    && x.Description == lineItem.Description
                    && x.DesiredDate == lineItem.DesiredDate
                    && x.ExtendedPrice == lineItem.ExtendedPrice
                    && x.Id == lineItem.Id
                    && x.InvoiceNumber == lineItem.InvoiceNumber
                    && x.Price == lineItem.Price
                    && x.Quantity == lineItem.Quantity
                    && x.TaxForm == lineItem.TaxForm
                    && x.TaxFormCode == lineItem.TaxFormCode
                    && x.TaxFormLocation == lineItem.TaxFormLocation
                    && x.UnitOfIssue == lineItem.UnitOfIssue
                    && x.VendorPart == lineItem.VendorPart));
            }
        }

        [TestMethod]
        public async Task GetRequisition_LineItems_LongDescription()
        {
            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();

            this.itemsDataContracts[0].ItmDesc.Add("more training");
            string lineItemDescription = string.Empty;
            foreach (var desc in this.itemsDataContracts[0].ItmDesc)
            {
                lineItemDescription += desc + ' ';
            }

            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);
            var lineItem = requisition.LineItems.FirstOrDefault();
            Assert.AreEqual(lineItemDescription, lineItem.Description, "The line item descriptions should be the same and concatenated using blank spaces.");
        }
        #endregion

        #region GL Distribution tests
        [TestMethod]
        public async Task GetRequisition_GlDistributions_AllLocalAmounts()
        {
            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDomainEntity.Amount, requisition.Amount);
            foreach (var domainLineItem in this.requisitionDomainEntity.LineItems)
            {
                foreach (var domainGlDistribution in domainLineItem.GlDistributions)
                {
                    foreach (var lineItem in requisition.LineItems)
                    {
                        // Since we're comparing two requisition objects that SHOULD be the same, we only
                        // want to execute the assertion if we know we are comparing the same line items.
                        if (domainLineItem.Id == lineItem.Id)
                        {
                            Assert.IsTrue(lineItem.GlDistributions.Any(x =>
                                x.Amount == domainGlDistribution.Amount
                                && x.GlAccountNumber == domainGlDistribution.GlAccountNumber
                                && x.ProjectId == domainGlDistribution.ProjectId
                                && x.ProjectNumber == domainGlDistribution.ProjectNumber
                                && x.ProjectLineItemCode == domainGlDistribution.ProjectLineItemCode
                                && x.ProjectLineItemId == domainGlDistribution.ProjectLineItemId
                                && x.Quantity == domainGlDistribution.Quantity));
                        }
                    }
                }
            }
        }

        [TestMethod]
        public async Task GetRequisition_GlDistributions_AllForeignAmounts()
        {
            string requisitionId = "2";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDomainEntity.Amount, requisition.Amount);
            foreach (var domainLineItem in this.requisitionDomainEntity.LineItems)
            {
                foreach (var domainGlDistribution in domainLineItem.GlDistributions)
                {
                    foreach (var lineItem in requisition.LineItems)
                    {
                        // Since we're comparing two requisition objects that SHOULD be the same, we only
                        // want to execute the assertion if we know we are comparing the same line items.
                        if (domainLineItem.Id == lineItem.Id)
                        {
                            Assert.IsTrue(lineItem.GlDistributions.Any(x =>
                                x.Amount == domainGlDistribution.Amount));
                        }
                    }
                }
            }
        }
        #endregion

        #region Line Item Tax tests
        [TestMethod]
        public async Task GetGetRequisition_LineItemTaxes_AllLocalAmounts()
        {
            string requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDomainEntity.Amount, requisition.Amount);
            foreach (var domainLineItem in this.requisitionDomainEntity.LineItems)
            {
                foreach (var domainTax in domainLineItem.LineItemTaxes)
                {
                    foreach (var lineItem in requisition.LineItems)
                    {
                        // Since we're comparing two requisition objects that SHOULD be the same, we only
                        // want to execute the assertion if we know we are comparing the same line items taxes.
                        if (domainLineItem.Id == lineItem.Id)
                        {
                            Assert.IsTrue(lineItem.LineItemTaxes.Any(x =>
                                x.TaxCode == domainTax.TaxCode
                                && x.TaxAmount == domainTax.TaxAmount));
                        }
                    }
                }
            }
        }

        [TestMethod]
        public async Task GetGetRequisition_LineItemTaxes_AllForeignAmounts()
        {
            string requisitionId = "2";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDomainEntity.Amount, requisition.Amount);
            foreach (var domainLineItem in this.requisitionDomainEntity.LineItems)
            {
                foreach (var domainTax in domainLineItem.LineItemTaxes)
                {
                    foreach (var lineItem in requisition.LineItems)
                    {
                        // Since we're comparing two requisition objects that SHOULD be the same, we only
                        // want to execute the assertion if we know we are comparing the same line items taxes.
                        if (domainLineItem.Id == lineItem.Id)
                        {
                            Assert.IsTrue(lineItem.LineItemTaxes.Any(x =>
                                x.TaxCode == domainTax.TaxCode
                                && x.TaxAmount == domainTax.TaxAmount));
                        }
                    }
                }
            }
        }
        #endregion

        #region GL Security tests
        [TestMethod]
        public async Task UserHasFullAccess()
        {
            var requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDomainEntity.Amount, requisition.Amount, "The requisition amounts should be the same.");
            Assert.AreEqual(this.requisitionDomainEntity.LineItems.Count(), requisition.LineItems.Count(), "We should be able to see all of the requisition line items.");
            foreach (var lineItem in this.requisitionDomainEntity.LineItems)
            {
                Assert.IsTrue(requisition.LineItems.Any(x =>
                    x.Comments == lineItem.Comments
                    && x.Description == lineItem.Description
                    && x.ExtendedPrice == lineItem.ExtendedPrice
                    && x.Id == lineItem.Id
                    && x.DesiredDate == lineItem.DesiredDate
                    && x.Price == lineItem.Price
                    && x.Quantity == lineItem.Quantity
                    && x.TaxForm == lineItem.TaxForm
                    && x.TaxFormCode == lineItem.TaxFormCode
                    && x.TaxFormLocation == lineItem.TaxFormLocation
                    && x.UnitOfIssue == lineItem.UnitOfIssue
                    && x.VendorPart == lineItem.VendorPart));
            }
        }

        [TestMethod]
        public async Task UserHasPossibleAccess_AllLineItemsAvailable()
        {
            var requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDomainEntity.Amount, requisition.Amount, "The requisition amounts should be the same.");
            Assert.AreEqual(this.requisitionDomainEntity.LineItems.Count(), requisition.LineItems.Count(), "We should be able to see all of the requisition line items.");
            foreach (var lineItem in this.requisitionDomainEntity.LineItems)
            {
                Assert.IsTrue(requisition.LineItems.Any(x =>
                    x.Comments == lineItem.Comments
                    && x.Description == lineItem.Description
                    && x.ExtendedPrice == lineItem.ExtendedPrice
                    && x.Id == lineItem.Id
                    && x.DesiredDate == lineItem.DesiredDate
                    && x.Price == lineItem.Price
                    && x.Quantity == lineItem.Quantity
                    && x.TaxForm == lineItem.TaxForm
                    && x.TaxFormCode == lineItem.TaxFormCode
                    && x.TaxFormLocation == lineItem.TaxFormLocation
                    && x.UnitOfIssue == lineItem.UnitOfIssue
                    && x.VendorPart == lineItem.VendorPart));
            }
        }

        [TestMethod]
        public async Task UserHasPossibleAccess_PartialLineItemsAvailable()
        {
            var requisitionId = "31";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDomainEntity.Amount, requisition.Amount, "The requisition amount should show as if we have full access.");
            Assert.AreEqual(this.requisitionDomainEntity.LineItems.Count(), requisition.LineItems.Count(), "The requisition should have all of it's line items.");

            decimal glDistributionTotal = 0.00m;
            decimal taxDistributionTotal = 0.00m;
            foreach (var lineItem in requisition.LineItems)
            {
                glDistributionTotal += lineItem.GlDistributions.Sum(x => x.Amount);
                taxDistributionTotal += lineItem.LineItemTaxes.Sum(x => x.TaxAmount);
            }

            Assert.AreEqual(this.requisitionDomainEntity.Amount, glDistributionTotal + taxDistributionTotal, "The requisition amount should be the same as the sum of the GL and tax distributions for all line items");

            foreach (var lineItem in requisition.LineItems)
            {
                foreach (var glDistribution in lineItem.GlDistributions)
                {
                    if (glDistribution.GlAccountNumber == "11_10_00_01_20601_51000")
                    {
                        Assert.IsFalse(glDistribution.Masked, "GL account should NOT be masked.");
                    }
                    else
                    {
                        Assert.IsTrue(glDistribution.Masked, "GL account SHOULD be masked.");
                    }
                }
            }
        }

        [TestMethod]
        public async Task UserHasPossibleAccess_SomeLineItemsExcluded()
        {
            var requisitionId = "32";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDomainEntity.Amount, requisition.Amount, "The requisition amount should show as if we have full access.");

            var excludedLineItems = new List<LineItem>();
            foreach (var lineItem in requisition.LineItems)
            {
                excludedLineItems.AddRange(this.requisitionDomainEntity.LineItems.Where(x => x.Id != lineItem.Id));
            }
            Assert.AreEqual(this.requisitionDomainEntity.LineItems.Sum(x => x.ExtendedPrice),
                requisition.LineItems.Sum(x => x.ExtendedPrice) + excludedLineItems.Sum(x => x.ExtendedPrice), "The extended price should reflect which line items are included or excluded.");
            Assert.IsTrue(requisition.LineItems.Count() == 1, "The requisition should only have one line item.");

            foreach (var lineItem in requisition.LineItems)
            {
                foreach (var glDistribution in lineItem.GlDistributions)
                {
                    if (glDistribution.GlAccountNumber == "11_10_00_01_20601_52001")
                    {
                        Assert.IsFalse(glDistribution.Masked, "GL account should NOT be masked.");
                    }
                    else
                    {
                        Assert.IsTrue(glDistribution.Masked, "GL account SHOULD be masked.");
                    }
                }
            }
        }

        [TestMethod]
        public async Task UserHasPossibleAccess_NoLineItemsAvailable()
        {
            var requisitionId = "33";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDomainEntity.Amount, requisition.Amount, "The requisition amount should show as if we have full access.");
            Assert.IsTrue(requisition.LineItems.Count() == 0, "The requisition should have no line items.");
        }

        [TestMethod]
        public async Task UserHasNoAccess()
        {
            var requisitionId = "1";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);
            var expenseAccounts = CalculateExpenseAccountsForUser(requisitionId);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.No_Access, expenseAccounts);

            Assert.AreEqual(this.requisitionDomainEntity.Amount, requisition.Amount, "The requisition amount should show as if we have full access.");
            Assert.IsTrue(requisition.LineItems.Count() == 0, "The requisition should have no line items.");
        }

        [TestMethod]
        public async Task GetRequisitionAsync_NullExpenseAccounts()
        {
            var requisitionId = "33";
            this.requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);
            ConvertDomainEntitiesIntoDataContracts();
            InitializeMockMethods();
            var requisition = await requisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Possible_Access, null);

            Assert.AreEqual(0, requisition.LineItems.Count());
            Assert.AreEqual(0, requisition.LineItems.SelectMany(x => x.GlDistributions).Count());
        }
        #endregion

        #region Private methods

        private RequisitionRepository BuildRequisitionRepository()
        {
            // Instantiate all objects necessary to mock data reader and CTX calls.
            var cacheProviderObject = new Mock<ICacheProvider>().Object;
            var transactionFactory = new Mock<IColleagueTransactionFactory>();
            var transactionFactoryObject = transactionFactory.Object;
            var loggerObject = new Mock<ILogger>().Object;

            // The transaction factory has a method to get its data reader
            // Make sure that method returns our mock data reader
            transactionFactory.Setup(transFac => transFac.GetDataReader()).Returns(dataReader.Object);
            transactionFactory.Setup(transFac => transFac.GetTransactionInvoker()).Returns(transactionInvoker.Object);

            return new RequisitionRepository(cacheProviderObject, transactionFactoryObject, loggerObject);
        }

        private void InitializeMockMethods()
        {
            // Mock ReadRecord to return a pre-defined requisition data contract
            dataReader.Setup<Task<Requisitions>>(acc => acc.ReadRecordAsync<Requisitions>(It.IsAny<string>(), true)).Returns(() =>
                {
                    return Task.FromResult(this.requisitionDataContract);
                });

            // Mock ReadRecord to return a pre-defined ShipTo data contract.
            dataReader.Setup<Task<ShipToCodes>>(acc => acc.ReadRecordAsync<ShipToCodes>(It.IsAny<string>(), true)).Returns(Task.FromResult(this.shipToCodesDataContract));

            // Mock ReadRecord to return a pre-defined Opers data contract.
            // Mock bulk read UT.OPERS bulk read
            opersResponse = new Collection<Opers>()
                {
                    new Opers()
                    {
                        // "0000001"
                        Recordkey = "0000001", SysUserName = "Andy Kleehammer"
                    },
                    new Opers()
                    {
                        // ""
                        Recordkey = "0000002", SysUserName = "Gary Thorne"
                    },
                    new Opers()
                    {
                        // "0000003"
                        Recordkey = "0000003", SysUserName = "Teresa Longerbeam"
                    }
                };
            dataReader.Setup<Task<Collection<Opers>>>(acc => acc.BulkReadRecordAsync<Opers>("UT.OPERS", It.IsAny<string[]>(), true)).Returns(() =>
                {
                    return Task.FromResult(opersResponse);
                });


            // Mock BulkReadRecord to return a list of Projects data contracts

            dataReader.Setup<Task<Collection<Projects>>>(acc => acc.BulkReadRecordAsync<Projects>(It.IsAny<string[]>(), true)).Returns(() =>
                {
                    return Task.FromResult(this.projectDataContracts);
                });

            // Mock BulkReadRecord to return a list of ProjectsLineItems data contracts

            dataReader.Setup<Task<Collection<ProjectsLineItems>>>(acc => acc.BulkReadRecordAsync<ProjectsLineItems>(It.IsAny<string[]>(), true)).Returns(() =>
                {
                    return Task.FromResult(this.projectLineItemDataContracts);
                });

            // Mock BulkReadRecord to return a list of Items data contracts.
            dataReader.Setup<Task<Collection<Items>>>(acc => acc.BulkReadRecordAsync<Items>(It.IsAny<string[]>(), true)).Returns(() =>
                {
                    return Task.FromResult(this.itemsDataContracts);
                });

            // Mock Execute within the transaction invoker to return a GetHierarchyNamesForIdsResponse object
            transactionInvoker.Setup(tio => tio.Execute<GetHierarchyNamesForIdsRequest, GetHierarchyNamesForIdsResponse>(It.IsAny<GetHierarchyNamesForIdsRequest>())).Returns(() =>
                {
                    return this.hierarchyNamesForIdsResponse;
                });
        }

        private List<string> CalculateExpenseAccountsForUser(string requisitionId)
        {
            var expenseAccounts = new List<string>();
            switch (requisitionId)
            {
                case "31":
                    expenseAccounts = new List<string>() { "11_10_00_01_20601_51000" };
                    break;
                case "32":
                    expenseAccounts = new List<string>() { "11_10_00_01_20601_52001" };
                    break;
                case "33":
                    // Do nothing; we want to return an empty list
                    break;
                default:
                    if (this.requisitionDomainEntity.LineItems != null)
                    {
                        foreach (var lineItem in this.requisitionDomainEntity.LineItems)
                        {
                            if ((lineItem.GlDistributions != null) && (lineItem.GlDistributions.Count > 0))
                            {
                                foreach (var glDistribution in lineItem.GlDistributions)
                                {
                                    if (!expenseAccounts.Contains(glDistribution.GlAccountNumber))
                                    {
                                        expenseAccounts.Add(glDistribution.GlAccountNumber);
                                    }
                                }
                            }
                        }
                    }
                    break;
            }

            return expenseAccounts;
        }

        private void ConvertDomainEntitiesIntoDataContracts(bool ctxLongName = false)
        {
            // Convert the Requisition object
            this.requisitionDataContract.Recordkey = this.requisitionDomainEntity.Id;
            this.requisitionDataContract.ReqVendor = this.requisitionDomainEntity.VendorId;

            if (this.requisitionDomainEntity.VendorName == "null")
            {
                this.requisitionDataContract.ReqMiscName = new List<string>() { null };
            }
            else
            {
                this.requisitionDataContract.ReqMiscName = new List<string>() { this.requisitionDomainEntity.VendorName };
            }

            // vendor name, initiator name and requestor name come from CTX

            this.requisitionDataContract.ReqDefaultInitiator = "0001687";
            this.requisitionDataContract.ReqRequestor = "0004437";

            // For the unit tests that use requisitions 5 and 6, there is no vendor id, so we do not need to call the CTX
            if ((requisitionDomainEntity.Id != "5") && (requisitionDomainEntity.Id != "6") && (requisitionDomainEntity.Id != "10"))
            {
                string ctxVendorName = "Ellucian Consulting, Inc.";
                if (ctxLongName)
                {
                    ctxVendorName = "Very long vendor name for use in a colleague transaction";
                }

                this.hierarchyNamesForIdsResponse = new GetHierarchyNamesForIdsResponse()
                {
                    IoPersonIds = new List<string>() { this.requisitionDomainEntity.VendorId, this.requisitionDataContract.ReqDefaultInitiator, this.requisitionDataContract.ReqRequestor },
                    IoHierarchies = new List<string>() { "PO", "PREFERRED", "PREFERRED" },
                    OutPersonNames = new List<string>() { ctxVendorName, this.requisitionDomainEntity.InitiatorName, this.requisitionDomainEntity.RequestorName }
                };
            }

            this.requisitionDataContract.ReqNo = this.requisitionDomainEntity.Number;
            this.requisitionDataContract.ReqTotalAmt = this.requisitionDomainEntity.Amount;
            this.requisitionDataContract.ReqApType = this.requisitionDomainEntity.ApType;
            this.requisitionDataContract.ReqDate = this.requisitionDomainEntity.Date;
            this.requisitionDataContract.ReqMaintGlTranDate = this.requisitionDomainEntity.MaintenanceDate;
            this.requisitionDataContract.ReqDesiredDeliveryDate = this.requisitionDomainEntity.DesiredDate;
            this.requisitionDataContract.ReqShipTo = this.requisitionDomainEntity.ShipToCode;
            this.requisitionDataContract.ReqComments = this.requisitionDomainEntity.InternalComments;
            this.requisitionDataContract.ReqPrintedComments = this.requisitionDomainEntity.Comments;
            this.requisitionDataContract.ReqCurrencyCode = this.requisitionDomainEntity.CurrencyCode;

            // A requisition can only be associated to one bpo even though the CDD is defined as a list
            this.requisitionDataContract.ReqBpoNo = new List<string>();
            this.requisitionDataContract.ReqBpoNo.Add(this.requisitionDomainEntity.BlanketPurchaseOrder);

            this.requisitionDataContract.ReqStatus = new List<string>();
            switch (this.requisitionDomainEntity.Status)
            {
                case RequisitionStatus.InProgress:
                    this.requisitionDataContract.ReqStatus.Add("U");
                    break;
                case RequisitionStatus.NotApproved:
                    this.requisitionDataContract.ReqStatus.Add("N");
                    break;
                case RequisitionStatus.Outstanding:
                    this.requisitionDataContract.ReqStatus.Add("O");
                    break;
                case RequisitionStatus.PoCreated:
                    this.requisitionDataContract.ReqStatus.Add("P");
                    break;
                default:
                    throw new Exception("Invalid status specified in PurchaseOrderRepositoryTests");
            }

            // Build the requisition status date
            this.requisitionDataContract.ReqStatusDate = new List<DateTime?>();
            this.requisitionDataContract.ReqStatusDate.Add(this.requisitionDomainEntity.StatusDate);

            // Build the Ship To Code contract
            this.shipToCodesDataContract = new ShipToCodes()
            {
                Recordkey = this.requisitionDomainEntity.ShipToCode,
                ShptName = "Main Campus Delivery"
            };

            // Build a list of purchase orders related to the requisition
            this.requisitionDataContract.ReqPoNo = new List<string>();
            foreach (var po in this.requisitionDomainEntity.PurchaseOrders)
            {
                if (!String.IsNullOrEmpty(po))
                {
                    this.requisitionDataContract.ReqPoNo.Add(po);
                }
            }

            // Build a list of line item IDs
            this.requisitionDataContract.ReqItemsId = new List<string>();
            foreach (var lineItem in this.requisitionDomainEntity.LineItems)
            {
                if (lineItem.Id != "null")
                {
                    this.requisitionDataContract.ReqItemsId.Add(lineItem.Id);
                }
            }

            // amount is cumulated using various line item associations

            // Build a list of Approver data contracts
            ConvertApproversIntoDataContracts();

            // Build a list of line items
            ConvertLineItemsIntoDataContracts();
        }

        private void ConvertApproversIntoDataContracts()
        {
            // Initialize the associations for approvers and next approvers.
            this.requisitionDataContract.ReqAuthEntityAssociation = new List<RequisitionsReqAuth>();
            this.requisitionDataContract.ReqApprEntityAssociation = new List<RequisitionsReqAppr>();
            this.opersDataContracts = new Collection<Opers>();
            this.requisitionDataContract.ReqAuthorizations = new List<string>();
            this.requisitionDataContract.ReqNextApprovalIds = new List<string>();
            foreach (var approver in this.requisitionDomainEntity.Approvers)
            {
                if (approver.ApprovalDate != null)
                {
                    // Populate approvers
                    var dataContract = new RequisitionsReqAuth()
                    {
                        ReqAuthorizationsAssocMember = approver.ApproverId,
                        ReqAuthorizationDatesAssocMember = approver.ApprovalDate
                    };

                    this.requisitionDataContract.ReqAuthEntityAssociation.Add(dataContract);
                    this.requisitionDataContract.ReqAuthorizations.Add(approver.ApproverId);
                }
                else
                {
                    // Populate next approvers
                    var nextApproverDataContract = new RequisitionsReqAppr()
                    {
                        ReqNextApprovalIdsAssocMember = approver.ApproverId
                    };
                    this.requisitionDataContract.ReqApprEntityAssociation.Add(nextApproverDataContract);
                    this.requisitionDataContract.ReqNextApprovalIds.Add(approver.ApproverId);
                }

                // Populate the Opers data contract
                this.opersDataContracts.Add(new Opers()
                {
                    Recordkey = approver.ApproverId,
                    SysUserName = approver.ApprovalName
                });
            }
        }

        private void ConvertLineItemsIntoDataContracts()
        {
            this.itemsDataContracts = new Collection<Items>();
            this.projectDataContracts = new Collection<Projects>();
            this.projectLineItemDataContracts = new Collection<ProjectsLineItems>();

            foreach (var lineItem in this.requisitionDomainEntity.LineItems)
            {
                // Populate the line items directly
                var itemsDataContract = new Items()
                {
                    Recordkey = lineItem.Id,
                    ItmDesc = new List<string>() { lineItem.Description },
                    ItmReqQty = lineItem.Quantity,
                    ItmReqPrice = lineItem.Price,
                    ItmReqExtPrice = lineItem.ExtendedPrice,
                    ItmReqIssue = lineItem.UnitOfIssue,
                    ItmTaxForm = lineItem.TaxForm,
                    ItmTaxFormCode = lineItem.TaxFormCode,
                    ItmTaxFormLoc = lineItem.TaxFormLocation,
                    ItmComments = lineItem.Comments,
                    ItmDesiredDeliveryDate = lineItem.DesiredDate,
                    ItmVendorPart = lineItem.VendorPart,
                    ItemReqEntityAssociation = new List<ItemsItemReq>(),
                    ReqGlTaxesEntityAssociation = new List<ItemsReqGlTaxes>()
                };

                // Populate the GL Distributions
                int counter = 0;
                foreach (var glDistr in lineItem.GlDistributions)
                {
                    counter++;
                    decimal localGlAmount = 0,
                        foreignGlAmount = 0;

                    // The amount from the LineItemGlDistribution domain entity is always going to be a local amount.
                    // If the requisition is in foreign currency, we need to manually set the test foreign amounts
                    // since they cannot be gotten from the domain entity. Currently, there is only one foreign
                    // currency requisition in the test data.
                    localGlAmount = glDistr.Amount;
                    if (!string.IsNullOrEmpty(this.requisitionDomainEntity.CurrencyCode))
                    {
                        if (counter == 1)
                        {
                            foreignGlAmount = 22.22m;
                        }
                        else if (counter == 2)
                        {
                            foreignGlAmount = 110.00m;
                        }
                        else
                        {
                            foreignGlAmount = 60.00m;
                        }
                    }

                    itemsDataContract.ItemReqEntityAssociation.Add(new ItemsItemReq()
                    {
                        ItmReqGlNoAssocMember = glDistr.GlAccountNumber,
                        ItmReqGlQtyAssocMember = glDistr.Quantity,
                        ItmReqProjectCfIdAssocMember = glDistr.ProjectId,
                        ItmReqPrjItemIdsAssocMember = glDistr.ProjectLineItemId,
                        ItmReqGlAmtAssocMember = localGlAmount,
                        ItmReqGlForeignAmtAssocMember = foreignGlAmount
                    });

                    this.projectDataContracts.Add(new Projects()
                    {
                        Recordkey = glDistr.ProjectId,
                        PrjRefNo = glDistr.ProjectNumber
                    });

                    this.projectLineItemDataContracts.Add(new ProjectsLineItems()
                    {
                        Recordkey = glDistr.ProjectLineItemId,
                        PrjlnProjectItemCode = glDistr.ProjectLineItemCode
                    });
                }

                // Populate the taxes
                int taxCounter = 0;
                foreach (var taxDistr in lineItem.LineItemTaxes)
                {
                    taxCounter++;
                    decimal? localTaxAmount = null,
                        foreignTaxAmount = null;

                    // The amount from the LineItemTax domain entity is going to be in local currency
                    //  unless there is a currency code on the requisition.
                    //
                    // If the requisition does not have a currency code, the tax amount in the domain entity
                    // will be in local currency, and the foreign tax amount on the data contract will be null. 
                    //
                    // If the requisition does have a currency code, the tax amount in the domain entity will be in foreign
                    // currency, and we need to manually set the test local tax amounts since they cannot be gotten from
                    // the domain entity. Currently, there is only one foreign currency requisition in the test data.

                    if (string.IsNullOrEmpty(this.requisitionDomainEntity.CurrencyCode))
                    {
                        localTaxAmount = taxDistr.TaxAmount;
                    }
                    else
                    {
                        foreignTaxAmount = taxDistr.TaxAmount;
                        if (counter == 1)
                        {
                            localTaxAmount = 15.00m;
                        }
                        else if (counter == 2)
                        {
                            localTaxAmount = 5.00m;
                        }
                        else
                        {
                            localTaxAmount = 10.00m;
                        }
                    }

                    itemsDataContract.ReqGlTaxesEntityAssociation.Add(new ItemsReqGlTaxes()
                    {
                        ItmReqGlTaxCodeAssocMember = taxDistr.TaxCode,
                        ItmReqGlTaxAmtAssocMember = localTaxAmount,
                        ItmReqGlForeignTaxAmtAssocMember = foreignTaxAmount
                    });
                }

                this.itemsDataContracts.Add(itemsDataContract);
            }
        }
        #endregion
    }
}