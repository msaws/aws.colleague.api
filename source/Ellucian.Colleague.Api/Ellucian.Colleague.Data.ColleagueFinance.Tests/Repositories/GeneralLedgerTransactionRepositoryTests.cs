﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.ColleagueFinance.DataContracts;
using Ellucian.Colleague.Data.ColleagueFinance.Repositories;
using Ellucian.Colleague.Data.ColleagueFinance.Transactions;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.Exceptions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Data.Colleague;
using Ellucian.Web.Cache;
using slf4net;
using Ellucian.Colleague.Domain.ColleagueFinance.Tests;
using Ellucian.Web.Http.Configuration;
//using Ellucian.Colleague.Coordination.ColleagueFinance.Services;

namespace Ellucian.Colleague.Data.ColleagueFinance.Tests.Repositories
{
    [TestClass]
    public class GeneralLedgerTransactionRepositoryTests : BaseRepositorySetup
    {

        private Mock<ICacheProvider> _iCacheProviderMock;
        private Mock<IColleagueTransactionFactory> _iColleagueTransactionFactoryMock;
        private Mock<IColleagueTransactionInvoker> _iColleagueTransactionInvokerMock;
        private Mock<ILogger> _iLoggerMock;
        private Mock<IColleagueDataReader> _dataReaderMock;
        private ApiSettings apiSettings;

        private GeneralLedgerTransactionRepository _generalLedgerTransactionsRepository;
        private GeneralLedgerTransaction _generalLedgerTransaction;
        private Dictionary<string, GuidLookupResult> _guidLookupResults;

        private Ellucian.Colleague.Data.Base.DataContracts.Person _personContract;
        private CreateGLPostingRequest _createGlPostingRequest;
        private CreateGLPostingResponse _createGlPostingResponse;
        private DeleteIntgGlPostingRequest _deleteGeneralLedgerTransactionRequest;
        private DeleteIntgGlPostingResponse _deleteGeneralLedgerTransactionResponse;
        private TestGeneralLedgerConfigurationRepository testGlConfigurationRepository;

        private string _id = string.Empty;
        private string _recKey = string.Empty;
        private const string AccountNumber = "01-02-03-04-05550-66077";
        private const string ReferenceNumber = "GL122312321";
        private const string ProjectId = "A1";

        [TestInitialize]
        public void Initialize()
        {
            _iCacheProviderMock = new Mock<ICacheProvider>();
            _iColleagueTransactionFactoryMock = new Mock<IColleagueTransactionFactory>();
            _iLoggerMock = new Mock<ILogger>();
            _dataReaderMock = new Mock<IColleagueDataReader>();
            _iColleagueTransactionInvokerMock = new Mock<IColleagueTransactionInvoker>();
            _iColleagueTransactionFactoryMock.Setup(transFac => transFac.GetDataReader())
                .Returns(_dataReaderMock.Object);
            _iColleagueTransactionFactoryMock.Setup(transFac => transFac.GetTransactionInvoker())
                .Returns(_iColleagueTransactionInvokerMock.Object);
            apiSettings = new ApiSettings("TEST");

            BuildObjects();

            _generalLedgerTransactionsRepository = new GeneralLedgerTransactionRepository(
                _iCacheProviderMock.Object,
                _iColleagueTransactionFactoryMock.Object, _iLoggerMock.Object, apiSettings);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _generalLedgerTransactionsRepository = null;
            _generalLedgerTransaction = null;
            _guidLookupResults = null;

            _personContract = null;
            _createGlPostingRequest = null;
            _deleteGeneralLedgerTransactionRequest = null;
            _deleteGeneralLedgerTransactionResponse = null;
            _id = string.Empty;
            _recKey = string.Empty;
        }
  
        [TestMethod]
        public async Task GeneralLedgerTransactionRepo_GeneralLedgerTransaction_GetById()
        {
            _recKey = "0012297";
            _guidLookupResults = new Dictionary<string, GuidLookupResult>
            {
                {
                    "INTG.GL.POSTINGS",
                    new GuidLookupResult() {Entity = "INTG.GL.POSTINGS", PrimaryKey = _recKey, SecondaryKey = ""}
                }
            }; // 

            var intgGlPostingsTranDetail = new IntgGlPostingsTranDetail()
            {
                IgpAcctIdAssocMember = "0002024",
                IgpRefNoAssocMember = "12345",
                IgpSourceAssocMember = "PL",
                IgpSysDateAssocMember = DateTime.Now.Date,
                IgpTrDateAssocMember = DateTime.Now.Date,
                IgpTranDetailsAssocMember = "19",
                IgpTranNoAssocMember = "123DHJGSHJGSDJHGSDJKGHSDJHSDGH"
            };

            var intgGlPosting = new IntgGlPostings();
            intgGlPosting.IgpAcctId = new List<string>() {"0002024"};
            intgGlPosting.IgpRefNo = new List<string>() {"12345"};
            intgGlPosting.IgpSource = new List<string>() {"PL"};
            intgGlPosting.IgpSysDate = new List<DateTime?>() {DateTime.Now.Date};
            intgGlPosting.Recordkey = _recKey;
            intgGlPosting.RecordGuid = "D79063C0-3793-455E-A244-1381DD1BC0C4";
            intgGlPosting.RecordModelName = "general-ledger-transactions";
            intgGlPosting.IgpTrDate = new List<DateTime?>() {DateTime.Now.Date};
            intgGlPosting.IgpSubmittedBy = "123456";
            intgGlPosting.TranDetailEntityAssociation = new List<IntgGlPostingsTranDetail>()
            {
                intgGlPostingsTranDetail
            };
            intgGlPosting.IgpTranNo = new List<string>() {"123DHJGSHJGSDJHGSDJKGHSDJHSDGH"};
            intgGlPosting.IgpTranDetails = new List<string>() {"19"};
            var intgGlPostings = new Collection<IntgGlPostings>() {intgGlPosting};

            decimal? credit = 25;
            decimal? debit = 65;

            var intgGlPostingsDetailIgpdTranDetails = new IntgGlPostingsDetailIgpdTranDetails()
            {
                IgpdCreditAssocMember = credit,
                IgpdDebitAssocMember = debit,
                IgpdDescriptionAssocMember = "desc",
                IgpdGlNoAssocMember = "123456",
                IgpdPrjItemsIdsAssocMember = "19",
                IgpdProjectIdsAssocMember = "19",
                IgpdTranSeqNoAssocMember = "1",
                IgpdSubmittedByAssocMember = "123456"
            };

            var intgGlPostingsDetail = new IntgGlPostingsDetail()
            {
                Recordkey = "19",
                IgpdCredit = new List<decimal?>() {credit},
                IgpdSubmittedBy = new List<string>() {"123456"},
                IgpdDescription = new List<string>() {"desc"},
                IgpdGlNo = new List<string>() {"123456"},
                IgpdPrjItemsIds = new List<string>() {"19"},
                IgpdTranSeqNo = new List<string>() {"1"},
                IgpdTranDetailsEntityAssociation =
                    new List<IntgGlPostingsDetailIgpdTranDetails>() {intgGlPostingsDetailIgpdTranDetails}
            };


            var detailIds = intgGlPostings.SelectMany(igp => igp.IgpTranDetails).ToArray();

            _dataReaderMock.Setup(dr => dr.SelectAsync(It.IsAny<GuidLookup[]>())).ReturnsAsync(_guidLookupResults);

            _dataReaderMock.Setup(x => x.BulkReadRecordAsync<IntgGlPostings>(It.IsAny<string>(), It.IsAny<bool>()))
                .ReturnsAsync(intgGlPostings);

            _dataReaderMock.Setup(x => x.BulkReadRecordAsync<IntgGlPostingsDetail>(detailIds, It.IsAny<bool>()))
                .ReturnsAsync(new Collection<IntgGlPostingsDetail>() {intgGlPostingsDetail});

            _dataReaderMock.Setup(x => x.ReadRecordAsync<IntgGlPostings>(It.IsAny<string>(), It.IsAny<bool>()))
                .ReturnsAsync(intgGlPosting);


            var result =
                await _generalLedgerTransactionsRepository.GetByIdAsync(_recKey, "0002024", GlAccessLevel.Full_Access);
            Assert.IsNotNull(result);
            Assert.AreEqual("Update", result.ProcessMode);
            Assert.AreEqual("123456", result.SubmittedBy);


            Assert.IsNotNull(result.GeneralLedgerTransactions);

            var generalLedgerTransaction =
                result.GeneralLedgerTransactions.FirstOrDefault(glt => glt.ReferenceNumber == "12345");
            Assert.IsNotNull(generalLedgerTransaction);
            Assert.AreEqual("12345", generalLedgerTransaction.ReferenceNumber);
            Assert.AreEqual("0002024", generalLedgerTransaction.ReferencePersonId);
            Assert.AreEqual("PL", generalLedgerTransaction.Source);
            Assert.AreEqual(DateTime.Now.Date, generalLedgerTransaction.LedgerDate.Date);
            Assert.AreEqual(DateTime.Now.Date, generalLedgerTransaction.TransactionTypeReferenceDate.Value.Date);

            var generalLedgerTransactionDetail =
                generalLedgerTransaction.TransactionDetailLines.FirstOrDefault(detail => detail.SequenceNumber == 1);
            Assert.IsNotNull(generalLedgerTransactionDetail);
            Assert.AreEqual("19", generalLedgerTransactionDetail.ProjectId);
            Assert.AreEqual("123456", generalLedgerTransactionDetail.SubmittedBy);
            Assert.AreEqual(CreditOrDebit.Credit, generalLedgerTransactionDetail.Type);

            var generalLedgerTransactionDetailAcct = generalLedgerTransactionDetail.GlAccount;
            Assert.IsNotNull(generalLedgerTransactionDetailAcct);
            Assert.AreEqual("desc", generalLedgerTransactionDetailAcct.GlAccountDescription);
            Assert.AreEqual("123456", generalLedgerTransactionDetailAcct.GlAccountNumber);

        }

        [TestMethod]
        public async Task GeneralLedgerTransactionRepo_GetGeneralLedgerTransactionAsync()
        {
            var intgGlPostingsTranDetail = new IntgGlPostingsTranDetail()
            {
                IgpAcctIdAssocMember = "0002024",
                IgpRefNoAssocMember = "12345",
                IgpSourceAssocMember = "PL",
                IgpSysDateAssocMember = DateTime.Now.Date,
                IgpTrDateAssocMember = DateTime.Now.Date,
                IgpTranDetailsAssocMember = "19",
                IgpTranNoAssocMember = "123DHJGSHJGSDJHGSDJKGHSDJHSDGH"
            };

            var intgGlPosting = new IntgGlPostings();
            intgGlPosting.IgpAcctId = new List<string>() {"0002024"};
            intgGlPosting.IgpRefNo = new List<string>() {"12345"};
            intgGlPosting.IgpSource = new List<string>() {"PL"};
            intgGlPosting.IgpSysDate = new List<DateTime?>() {DateTime.Now.Date};
            intgGlPosting.Recordkey = _recKey;
            intgGlPosting.RecordGuid = "D79063C0-3793-455E-A244-1381DD1BC0C4";
            intgGlPosting.RecordModelName = "general-ledger-transactions";
            intgGlPosting.IgpTrDate = new List<DateTime?>() {DateTime.Now.Date};
            intgGlPosting.IgpSubmittedBy = "123456";
            intgGlPosting.TranDetailEntityAssociation = new List<IntgGlPostingsTranDetail>()
            {
                intgGlPostingsTranDetail
            };
            intgGlPosting.IgpTranNo = new List<string>() {"123DHJGSHJGSDJHGSDJKGHSDJHSDGH"};
            intgGlPosting.IgpTranDetails = new List<string>() {"19"};
            var intgGlPostings = new Collection<IntgGlPostings>() {intgGlPosting};

            decimal? credit = 25;
            decimal? debit = 65;

            var intgGlPostingsDetailIgpdTranDetails = new IntgGlPostingsDetailIgpdTranDetails()
            {
                IgpdCreditAssocMember = credit,
                IgpdDebitAssocMember = debit,
                IgpdDescriptionAssocMember = "desc",
                IgpdGlNoAssocMember = "123456",
                IgpdPrjItemsIdsAssocMember = "19",
                IgpdProjectIdsAssocMember = "19",
                IgpdTranSeqNoAssocMember = "1",
                IgpdSubmittedByAssocMember = "123456"
            };

            var intgGlPostingsDetail = new IntgGlPostingsDetail()
            {
                Recordkey = "19",
                IgpdCredit = new List<decimal?>() {credit},
                IgpdDescription = new List<string>() {"desc"},
                IgpdSubmittedBy = new List<string>() { "123456" },
                IgpdGlNo = new List<string>() {"123456"},
                IgpdPrjItemsIds = new List<string>() {"19"},
                IgpdTranSeqNo = new List<string>() {"1"},
                IgpdTranDetailsEntityAssociation =
                    new List<IntgGlPostingsDetailIgpdTranDetails>() {intgGlPostingsDetailIgpdTranDetails}
            };

            _dataReaderMock.Setup(x => x.BulkReadRecordAsync<IntgGlPostings>(It.IsAny<string>(), It.IsAny<bool>()))
                .ReturnsAsync(intgGlPostings);

            var detailIds = intgGlPostings.SelectMany(igp => igp.IgpTranDetails).ToArray();
            _dataReaderMock.Setup(x => x.BulkReadRecordAsync<IntgGlPostingsDetail>(detailIds, It.IsAny<bool>()))
                .ReturnsAsync(new Collection<IntgGlPostingsDetail>() {intgGlPostingsDetail});

            var results = await _generalLedgerTransactionsRepository.GetAsync("0002024", GlAccessLevel.Full_Access);
            Assert.IsNotNull(results);

            var result = results.FirstOrDefault(r => r.Id == "D79063C0-3793-455E-A244-1381DD1BC0C4");
            Assert.IsNotNull(result);
            Assert.AreEqual("Update", result.ProcessMode);
            Assert.AreEqual("123456", result.SubmittedBy);
            Assert.IsNotNull(result.GeneralLedgerTransactions);

            var generalLedgerTransaction =
                result.GeneralLedgerTransactions.FirstOrDefault(glt => glt.ReferenceNumber == "12345");
            Assert.IsNotNull(generalLedgerTransaction);
            Assert.AreEqual("12345", generalLedgerTransaction.ReferenceNumber);
            Assert.AreEqual("0002024", generalLedgerTransaction.ReferencePersonId);
            Assert.AreEqual("PL", generalLedgerTransaction.Source);
            Assert.AreEqual(DateTime.Now.Date, generalLedgerTransaction.LedgerDate.Date);
            Assert.AreEqual(DateTime.Now.Date, generalLedgerTransaction.TransactionTypeReferenceDate.Value.Date);

            var generalLedgerTransactionDetail =
                generalLedgerTransaction.TransactionDetailLines.FirstOrDefault(detail => detail.SequenceNumber == 1);
            Assert.IsNotNull(generalLedgerTransactionDetail);
            Assert.AreEqual("19", generalLedgerTransactionDetail.ProjectId);
            Assert.AreEqual("123456", generalLedgerTransactionDetail.SubmittedBy);
            Assert.AreEqual(CreditOrDebit.Credit, generalLedgerTransactionDetail.Type);

            var generalLedgerTransactionDetailAcct = generalLedgerTransactionDetail.GlAccount;
            Assert.IsNotNull(generalLedgerTransactionDetailAcct);
            Assert.AreEqual("desc", generalLedgerTransactionDetailAcct.GlAccountDescription);
            Assert.AreEqual("123456", generalLedgerTransactionDetailAcct.GlAccountNumber);

        }

        [TestMethod]
        public async Task GeneralLedgerTransactionRepo_UpdateGeneralLedgerTransactionAsync()
        {
            _recKey = "0012297";

            _createGlPostingRequest = new CreateGLPostingRequest()
            {
                PostingGuid = _id,
                Mode = "Update",
                SubmittedBy = "123456",
                TranAcctId = new List<string>() {"0002024"},
                TranDetAmt = new List<string>() {"25"},
                TranDetGl = new List<string>() {"30"},
                TranDetProj = new List<string>() {"A1"},
                TranDetDesc = new List<string>() {"Description"},
                TranDetSeqNo = new List<string>() {"1"},
                TranDetType = new List<string>() {"credit"},
                TranNo = new List<string>() {"454323"},
                TranRefDate = new List<string>() {DateTime.Now.Date.ToString()},
                TranRefNo = new List<string>() {"12345"},
                TranTrDate = new List<DateTime?>() {DateTime.Now.Date},
                TranType = new List<string>() {"credit"},
                TranDetSubmittedBy = new List<string>() {"123456"}



            };
            _createGlPostingResponse = new CreateGLPostingResponse()
            {
                PostingGuid = _id,
                PostingId = _recKey
            };

            _iColleagueTransactionInvokerMock.Setup(
                i =>
                    i.ExecuteAsync<CreateGLPostingRequest, CreateGLPostingResponse>(
                        It.IsAny<CreateGLPostingRequest>())).ReturnsAsync(_createGlPostingResponse);

            _generalLedgerTransaction.Id = Guid.Empty.ToString();
            _generalLedgerTransaction.SubmittedBy = "123456";

            testGlConfigurationRepository = new TestGeneralLedgerConfigurationRepository();

            var testGlAccountStructure = await testGlConfigurationRepository.GetAccountStructureAsync();

            var result =
                await
                    _generalLedgerTransactionsRepository.CreateAsync(_generalLedgerTransaction, "0002024",
                        GlAccessLevel.Full_Access, testGlAccountStructure);

            Assert.IsNotNull(result);
            Assert.AreEqual(_id, result.Id);
            Assert.IsNotNull(result.GeneralLedgerTransactions);
            Assert.AreEqual("123456", result.SubmittedBy);
            

            var generalLedgerTran =
                result.GeneralLedgerTransactions.FirstOrDefault(glt => glt.ReferenceNumber == "GL45645");
            Assert.IsNotNull(generalLedgerTran);
            Assert.AreEqual("DN", generalLedgerTran.Source);

            var generalLedgerTranDetail =
                generalLedgerTran.TransactionDetailLines.FirstOrDefault(gltd => gltd.ProjectId == "A1");
            Assert.IsNotNull(generalLedgerTranDetail);
            Assert.AreEqual(CreditOrDebit.Credit, generalLedgerTranDetail.Type);
            Assert.AreEqual("123456", generalLedgerTranDetail.SubmittedBy);

            var generalLedgerTranDetailGlAcct = generalLedgerTranDetail.GlAccount;
            Assert.IsNotNull(generalLedgerTranDetailGlAcct);
            Assert.AreEqual("DESC", generalLedgerTranDetailGlAcct.GlAccountDescription);
            Assert.AreEqual("01-02-03-04-05550-66077", generalLedgerTranDetailGlAcct.GlAccountNumber);

        }

        [TestMethod]
        public async Task GeneralLedgerTransactionRepo_TestDifferentGLNoDelimiters()
        {
            _recKey = "0012297";

            _createGlPostingRequest = new CreateGLPostingRequest()
            {
                PostingGuid = _id,
                Mode = "Update",
                TranAcctId = new List<string>() { "0002024" },
                TranDetAmt = new List<string>() { "25" },
                TranDetGl = new List<string>() { "30" },
                TranDetProj = new List<string>() { "A1" },
                TranDetDesc = new List<string>() { "Description" },
                TranDetSeqNo = new List<string>() { "1" },
                TranDetType = new List<string>() { "credit" },
                TranNo = new List<string>() { "454323" },
                TranRefDate = new List<string>() { DateTime.Now.Date.ToString() },
                TranRefNo = new List<string>() { "12345" },
                TranTrDate = new List<DateTime?>() { DateTime.Now.Date },
                TranType = new List<string>() { "credit" },
                TranDetSubmittedBy = new List<string>() {"123456"}


            };
            _createGlPostingResponse = new CreateGLPostingResponse()
            {
                PostingGuid = _id,
                PostingId = _recKey
            };

            _iColleagueTransactionInvokerMock.Setup(
                i =>
                    i.ExecuteAsync<CreateGLPostingRequest, CreateGLPostingResponse>(
                        It.IsAny<CreateGLPostingRequest>())).ReturnsAsync(_createGlPostingResponse);


            //use same as the init build but change the GL delimiter.
            var glTransaction = new GeneralLedgerTransaction { Id = "001234" };
            var generalLedgerTransactions = new List<GenLedgrTransaction>();

            var genLedgrTransaction = new GenLedgrTransaction("DN", DateTimeOffset.Now)
            {
                ReferenceNumber = "GL45645",
                TransactionNumber = "1"
            };
            var genLedgrTransactionDetails = new List<GenLedgrTransactionDetail>();
            var genLedgrTransactionDetail = new GenLedgrTransactionDetail("01_02_03_04_05550_66077", ProjectId, "DESC",
                CreditOrDebit.Credit, new AmountAndCurrency(25, CurrencyCodes.USD));
            genLedgrTransactionDetail.SubmittedBy = "123456";
            genLedgrTransactionDetails.Add(genLedgrTransactionDetail);
            genLedgrTransaction.TransactionDetailLines = genLedgrTransactionDetails;
            generalLedgerTransactions.Add(genLedgrTransaction);
            glTransaction.GeneralLedgerTransactions = generalLedgerTransactions;

            _generalLedgerTransaction = glTransaction;

            _generalLedgerTransaction.Id = Guid.Empty.ToString();
            

            testGlConfigurationRepository = new TestGeneralLedgerConfigurationRepository();

            var testGlAccountStructure = await testGlConfigurationRepository.GetAccountStructureAsync();

            var result =
                await
                    _generalLedgerTransactionsRepository.CreateAsync(_generalLedgerTransaction, "0002024",
                        GlAccessLevel.Full_Access, testGlAccountStructure);

            Assert.IsNotNull(result);
            Assert.AreEqual(_id, result.Id);
            Assert.IsNotNull(result.GeneralLedgerTransactions);

            var generalLedgerTran =
                result.GeneralLedgerTransactions.FirstOrDefault(glt => glt.ReferenceNumber == "GL45645");
            Assert.IsNotNull(generalLedgerTran);
            Assert.AreEqual("DN", generalLedgerTran.Source);

            var generalLedgerTranDetail =
                generalLedgerTran.TransactionDetailLines.FirstOrDefault(gltd => gltd.ProjectId == "A1");
            Assert.IsNotNull(generalLedgerTranDetail);
            Assert.AreEqual(CreditOrDebit.Credit, generalLedgerTranDetail.Type);

            var generalLedgerTranDetailGlAcct = generalLedgerTranDetail.GlAccount;
            Assert.IsNotNull(generalLedgerTranDetailGlAcct);
            Assert.AreEqual("DESC", generalLedgerTranDetailGlAcct.GlAccountDescription);
            Assert.AreEqual("01_02_03_04_05550_66077", generalLedgerTranDetailGlAcct.GlAccountNumber);

        }

        [TestMethod]
        [ExpectedException(typeof (InvalidOperationException))]
        public async Task GeneralLedgerTransactionRepo_UpdateGeneralLedgerTransactionAsync_Invalid()
        {
            _recKey = "0012297";
            _guidLookupResults = new Dictionary<string, GuidLookupResult>
            {
                {
                    "INTG.GL.POSTINGS",
                    new GuidLookupResult() {Entity = "INTG.GL.POSTINGS", PrimaryKey = _recKey, SecondaryKey = ""}
                }
            };

            _dataReaderMock.Setup(dr => dr.SelectAsync(It.IsAny<GuidLookup[]>())).ReturnsAsync(_guidLookupResults);

            testGlConfigurationRepository = new TestGeneralLedgerConfigurationRepository();

            var testGlAccountStructure = await testGlConfigurationRepository.GetAccountStructureAsync();

            await _generalLedgerTransactionsRepository.CreateAsync(_generalLedgerTransaction, "0002024",
                GlAccessLevel.Full_Access, testGlAccountStructure);
        }

        [TestMethod]
        public async Task GeneralLedgerTransactionRepo_DeleteGeneralLedgerTransactionAsync()
        {
            _recKey = "0012297";
            _guidLookupResults = new Dictionary<string, GuidLookupResult>
            {
                {
                    "INTG.GL.POSTINGS",
                    new GuidLookupResult() {Entity = "INTG.GL.POSTINGS", PrimaryKey = _recKey, SecondaryKey = ""}
                }
            }; // 

            _dataReaderMock.Setup(dr => dr.SelectAsync(It.IsAny<GuidLookup[]>())).ReturnsAsync(_guidLookupResults);

            _iColleagueTransactionInvokerMock.Setup(
                i =>
                    i.ExecuteAsync<DeleteIntgGlPostingRequest, DeleteIntgGlPostingResponse>(
                        It.IsAny<DeleteIntgGlPostingRequest>()))
                .ReturnsAsync(_deleteGeneralLedgerTransactionResponse);
            var results = await _generalLedgerTransactionsRepository.DeleteAsync(_recKey);
            Assert.IsNull(results);

        }

        [TestMethod]
        [ExpectedException(typeof (RepositoryException))]
        public async Task GeneralLedgerTransactionRepo_DeleteGeneralLedgerTransactionAsync_Error()
        {
            _recKey = "0012297";
            _guidLookupResults = new Dictionary<string, GuidLookupResult>
            {
                {
                    "INTG.GL.POSTINGS",
                    new GuidLookupResult() {Entity = "INTG.GL.POSTINGS", PrimaryKey = _recKey, SecondaryKey = ""}
                }
            };

            _dataReaderMock.Setup(dr => dr.SelectAsync(It.IsAny<GuidLookup[]>())).ReturnsAsync(_guidLookupResults);

            var glPostingErrors = new DeleteIntgGlPostingErrors()
            {
                ErrorCode = "10",
                ErrorMsg = "Record Not Found"
            };

            _deleteGeneralLedgerTransactionResponse = new DeleteIntgGlPostingResponse()
            {
                DeleteIntgGlPostingErrors = new List<DeleteIntgGlPostingErrors>() {glPostingErrors}

            };
            _iColleagueTransactionInvokerMock.Setup(
                i =>
                    i.ExecuteAsync<DeleteIntgGlPostingRequest, DeleteIntgGlPostingResponse>(
                        It.IsAny<DeleteIntgGlPostingRequest>()))
                .ReturnsAsync(_deleteGeneralLedgerTransactionResponse);
            await _generalLedgerTransactionsRepository.DeleteAsync(_recKey);

        }


        private GeneralLedgerTransaction BuildGeneralLedgerTransaction()
        {
            var glTransaction = new GeneralLedgerTransaction { Id = "001234" };
            var generalLedgerTransactions = new List<GenLedgrTransaction>();

            var genLedgrTransaction = new GenLedgrTransaction("DN", DateTimeOffset.Now)
            {
                ReferenceNumber = "GL45645",
                TransactionNumber = "1"
            };
            var genLedgrTransactionDetails = new List<GenLedgrTransactionDetail>();
            var genLedgrTransactionDetail = new GenLedgrTransactionDetail(AccountNumber, ProjectId, "DESC",
                CreditOrDebit.Credit, new AmountAndCurrency(25, CurrencyCodes.USD));
            genLedgrTransactionDetail.SubmittedBy = "123456";
            genLedgrTransactionDetails.Add(genLedgrTransactionDetail);
            genLedgrTransaction.TransactionDetailLines = genLedgrTransactionDetails;
            generalLedgerTransactions.Add(genLedgrTransaction);
            glTransaction.GeneralLedgerTransactions = generalLedgerTransactions;
            return glTransaction;
        }

        private void BuildObjects()
        {
            _id = "375ef15b-f2d2-40ed-ac47-f0d2d45260f0";
            _recKey = "0012297";
            _guidLookupResults = new Dictionary<string, GuidLookupResult>
            {
                {
                    "INTG.GL.POSTINGS",
                    new GuidLookupResult() {Entity = "INTG.GL.POSTINGS", PrimaryKey = "0012297", SecondaryKey = ""}
                }
            };

            _createGlPostingRequest = new CreateGLPostingRequest {};

            _generalLedgerTransaction = BuildGeneralLedgerTransaction();
            
            _deleteGeneralLedgerTransactionRequest = new DeleteIntgGlPostingRequest()
            {
                Guid = _id,
                IntgGlPostingsId = _recKey
            };

            _deleteGeneralLedgerTransactionResponse = new DeleteIntgGlPostingResponse();

        }
    }
}
