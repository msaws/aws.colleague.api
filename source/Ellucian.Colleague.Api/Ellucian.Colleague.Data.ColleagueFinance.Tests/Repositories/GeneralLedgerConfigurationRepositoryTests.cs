﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.ColleagueFinance.DataContracts;
using Ellucian.Colleague.Data.ColleagueFinance.Repositories;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ColleagueFinance.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Ellucian.Colleague.Data.ColleagueFinance.Tests.Repositories
{
    [TestClass]
    public class GeneralLedgerConfigurationRepositoryTests : BaseRepositorySetup
    {
        #region Initialize and Cleanup
        private GeneralLedgerConfigurationRepository actualRepository;
        private TestGeneralLedgerConfigurationRepository expectedRepository;
        private int testFiscalYear = 0;

        [TestInitialize]
        public void Initialize()
        {
            this.MockInitialize();

            // Initialize the journal entry repository
            expectedRepository = new TestGeneralLedgerConfigurationRepository();

            this.actualRepository = BuildRepository();

            dataReaderMock.Setup(dr => dr.ReadRecordAsync<Fiscalyr>(It.IsAny<string>(), It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(this.expectedRepository.FiscalYearDataContract);
            });

            dataReaderMock.Setup(dr => dr.ReadRecordAsync<Glstruct>(It.IsAny<string>(), It.IsAny<string>(), true)).Returns(() =>
            {
                if (this.expectedRepository.GlStructDataContract != null)
                {
                    this.expectedRepository.GlStructDataContract.buildAssociations();
                }

                return Task.FromResult(this.expectedRepository.GlStructDataContract);
            });

            dataReaderMock.Setup(dr => dr.ReadRecordAsync<CfwebDefaults>(It.IsAny<string>(), It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(this.expectedRepository.CfWebDefaultsDataContract);
            });

            dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<FcDescs>(It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(this.expectedRepository.FcDescs);
            });

            dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<FdDescs>(It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(this.expectedRepository.FdDescs);
            });

            dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<LoDescs>(It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(this.expectedRepository.LoDescs);
            });

            dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<ObDescs>(It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(this.expectedRepository.ObDescs);
            });

            dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<SoDescs>(It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(this.expectedRepository.SoDescs);
            });

            dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<UnDescs>(It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(this.expectedRepository.UnDescs);
            });

            dataReaderMock.Setup(dr => dr.ReadRecordAsync<Glclsdef>(It.IsAny<string>(), It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(this.expectedRepository.GlClassDefDataContract);
            });

            dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<GenLdgr>(It.IsAny<string[]>(), true)).Returns(() =>
            {
                return Task.FromResult(this.expectedRepository.GenLdgrDataContracts);
            });
        }

        [TestCleanup]
        public void Cleanup()
        {
            expectedRepository = null;
            testFiscalYear = 0;
        }
        #endregion

        #region Fiscal Year Configuration

        [TestMethod]
        [ExpectedException(typeof(ConfigurationException))]
        public async Task FiscalYearConfiguration_DataReaderReturnsNull()
        {
            this.expectedRepository.FiscalYearDataContract = null;
            var actualConfiguration = await actualRepository.GetFiscalYearConfigurationAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(ConfigurationException))]
        public async Task FiscalYearConfiguration_DataContractContainsNullStartMonth()
        {
            this.expectedRepository.FiscalYearDataContract.FiscalStartMonth = null;
            var actualConfiguration = await actualRepository.GetFiscalYearConfigurationAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(ConfigurationException))]
        public async Task FiscalYearConfiguration_DataContractContainsOutOfRangeStartMonth_0()
        {
            this.expectedRepository.FiscalYearDataContract.FiscalStartMonth = 0;
            var actualConfiguration = await actualRepository.GetFiscalYearConfigurationAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(ConfigurationException))]
        public async Task FiscalYearConfiguration_DataContractContainsOutOfRangeStartMonth_13()
        {
            this.expectedRepository.FiscalYearDataContract.FiscalStartMonth = 13;
            var actualConfiguration = await actualRepository.GetFiscalYearConfigurationAsync();
        }

        [TestMethod]
        public async Task FiscalYearConfiguration_StartMonthIs1_Success()
        {
            this.expectedRepository.FiscalYearDataContract.FiscalStartMonth = 1;
            var expectedConfiguration = await this.expectedRepository.GetFiscalYearConfigurationAsync();
            var actualConfiguration = await actualRepository.GetFiscalYearConfigurationAsync();

            Assert.AreEqual(expectedConfiguration.StartMonth, actualConfiguration.StartMonth);
            Assert.AreEqual(expectedConfiguration.CurrentFiscalYear, actualConfiguration.CurrentFiscalYear);
        }

        [TestMethod]
        public async Task FiscalYearConfiguration_StartMonthIs12_Success()
        {
            this.expectedRepository.FiscalYearDataContract.FiscalStartMonth = 12;
            var expectedConfiguration = await this.expectedRepository.GetFiscalYearConfigurationAsync();
            var actualConfiguration = await actualRepository.GetFiscalYearConfigurationAsync();

            Assert.AreEqual(expectedConfiguration.StartMonth, actualConfiguration.StartMonth);
            Assert.AreEqual(expectedConfiguration.CurrentFiscalYear, actualConfiguration.CurrentFiscalYear);
        }

        [TestMethod]
        [ExpectedException(typeof(ConfigurationException))]
        public async Task FiscalYearConfiguration_DataContractContainsNullCurrentYear()
        {
            this.expectedRepository.FiscalYearDataContract.CfCurrentFiscalYear = null;
            var actualConfiguration = await actualRepository.GetFiscalYearConfigurationAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(ConfigurationException))]
        public async Task FiscalYearConfiguration_DataContractContainsEmptyCurrentYear()
        {
            this.expectedRepository.FiscalYearDataContract.CfCurrentFiscalYear = "";
            var actualConfiguration = await actualRepository.GetFiscalYearConfigurationAsync();
        }

        [TestMethod]
        public async Task FiscalYearConfiguration_Success()
        {
            var expectedConfiguration = await this.expectedRepository.GetFiscalYearConfigurationAsync();
            var actualConfiguration = await actualRepository.GetFiscalYearConfigurationAsync();

            Assert.AreEqual(expectedConfiguration.StartMonth, actualConfiguration.StartMonth);
            Assert.AreEqual(expectedConfiguration.CurrentFiscalYear, actualConfiguration.CurrentFiscalYear);
        }
        #endregion

        #region Account and Cost Center Structure

        [TestMethod]
        [ExpectedException(typeof(ConfigurationException))]
        public async Task AccountStructure_GlstructDataContractIsNull()
        {
            expectedRepository.GlStructDataContract = null;
            var accountStructure = await actualRepository.GetAccountStructureAsync();
        }

        [TestMethod]
        public async Task CostCenterStructure_CfWebDefaultsDataContractIsNull()
        {
            expectedRepository.CfWebDefaultsDataContract = null;

            string message = "";
            try
            {
                var costCenterStructure = await actualRepository.GetCostCenterStructureAsync();
            }
            catch (ConfigurationException cex)
            {
                message = cex.Message;
            }

            Assert.AreEqual("GL component information is not defined.", message);
        }

        [TestMethod]
        public async Task CostCenterStructure_GlStructContractDoesNotContainComponentFromCFWP()
        {
            expectedRepository.GlStructDataContract.AcctNames.RemoveAt(0);

            string message = "";
            try
            {
                var costCenterStructure = await actualRepository.GetCostCenterStructureAsync();
            }
            catch (ConfigurationException cex)
            {
                message = cex.Message;
            }

            Assert.AreEqual("GL structure information is not defined.", message);
        }

        [TestMethod]
        public async Task CostCenterStructure_GlStructContractHasNoAssociationData()
        {
            expectedRepository.GlStructDataContract.AcctNames = new List<string>();
            expectedRepository.GlStructDataContract.AcctStart = new List<string>();
            expectedRepository.GlStructDataContract.AcctLength = new List<int?>();
            expectedRepository.GlStructDataContract.AcctComponentType = new List<string>();

            string message = "";
            try
            {
                var costCenterStructure = await actualRepository.GetCostCenterStructureAsync();
            }
            catch (ConfigurationException cex)
            {
                message = cex.Message;
            }

            Assert.AreEqual("GL structure information is not defined.", message);
        }

        [TestMethod]
        public async Task CostCenterStructure_CostCenterNullComponentName()
        {
            expectedRepository.CfWebDefaultsDataContract.CfwebCkrCostCenterComps[0] = null;

            string message = "";
            try
            {
                var costCenterStructure = await actualRepository.GetCostCenterStructureAsync();
            }
            catch (ConfigurationException cex)
            {
                message = cex.Message;
            }

            Assert.AreEqual("Component name for GL structure is not defined.", message);
        }

        [TestMethod]
        public async Task CostCenterStructure_CostCenterEmptyComponentName()
        {
            expectedRepository.CfWebDefaultsDataContract.CfwebCkrCostCenterComps[0] = "";

            string message = "";
            try
            {
                var costCenterStructure = await actualRepository.GetCostCenterStructureAsync();
            }
            catch (ConfigurationException cex)
            {
                message = cex.Message;
            }

            Assert.AreEqual("Component name for GL structure is not defined.", message);
        }

        [TestMethod]
        public async Task CostCenterStructure_CostCenterNullStartPosition()
        {
            expectedRepository.GlStructDataContract.AcctStart[0] = null;

            string message = "";
            try
            {
                var costCenterStructure = await actualRepository.GetCostCenterStructureAsync();
            }
            catch (ConfigurationException cex)
            {
                message = cex.Message;
            }

            Assert.AreEqual("Start position for GL component not defined.", message);
        }

        [TestMethod]
        public async Task AccountStructure_CostCenterEmptyStartPosition()
        {
            expectedRepository.GlStructDataContract.AcctStart[0] = "";

            string message = "";
            try
            {
                var accountStructure = await actualRepository.GetAccountStructureAsync();
            }
            catch (ConfigurationException cex)
            {
                message = cex.Message;
            }

            Assert.AreEqual("Start position for GL component not defined.", message);
        }

        [TestMethod]
        public async Task AccountStructure_CostCenterNullComponentLength()
        {
            expectedRepository.GlStructDataContract.AcctLength[0] = null;

            string message = "";
            try
            {
                var accountStructure = await actualRepository.GetAccountStructureAsync();
            }
            catch (ConfigurationException cex)
            {
                message = cex.Message;
            }

            Assert.AreEqual("Length for GL component not defined.", message);
        }

        [TestMethod]
        public async Task AccountStructure_CostCenterInvalidComponentLength()
        {
            expectedRepository.GlStructDataContract.AcctLength[0] = 0;

            string message = "";
            try
            {
                var accountStructure = await actualRepository.GetAccountStructureAsync();
            }
            catch (ConfigurationException cex)
            {
                message = cex.Message;
            }

            Assert.AreEqual("Invalid length specified for GL component.", message);
        }

        [TestMethod]
        public async Task AccountStructure_Success()
        {
            var accountStructure = await actualRepository.GetAccountStructureAsync();

            // Make sure the start positions and full access role match
            for (int i = 0; i < expectedRepository.GlStructDataContract.AcctStart.Count; i++)
            {
                Assert.AreEqual(expectedRepository.GlStructDataContract.AcctStart[i], accountStructure.MajorComponentStartPositions[i]);
            }
            Assert.AreEqual(expectedRepository.GlStructDataContract.GlFullAccessRole, accountStructure.FullAccessRole);


            // Make sure the major component data matches.
            Assert.AreEqual(expectedRepository.GlStructDataContract.AcctNames.Count, accountStructure.MajorComponents.Count);
            for (int i = 0; i < expectedRepository.GlStructDataContract.AcctNames.Count; i++)
            {
                // Compare to the data coming from GLSTRUCT
                var glStructData = expectedRepository.GlStructDataContract.GlmajorEntityAssociation
                    .Where(x => x.AcctNamesAssocMember == expectedRepository.GlStructDataContract.AcctNames[i]).ToList().FirstOrDefault();
                if (glStructData != null)
                {
                    Assert.AreEqual(glStructData.AcctNamesAssocMember, accountStructure.MajorComponents[i].ComponentName);
                    Assert.AreEqual(Convert.ToInt32(glStructData.AcctStartAssocMember) - 1, accountStructure.MajorComponents[i].StartPosition);
                    Assert.AreEqual(glStructData.AcctLengthAssocMember.Value, accountStructure.MajorComponents[i].ComponentLength);

                    // Confirm the component type
                    var componentType = DetermineComponentType(glStructData.AcctComponentTypeAssocMember);
                    Assert.AreEqual(componentType, accountStructure.MajorComponents[i].ComponentType);
                }
            }
        }

        [TestMethod]
        public async Task CostCenterStructure_Success()
        {
            var costCenterStructure = await actualRepository.GetCostCenterStructureAsync();

            // Make sure the cost center component data matches
            Assert.AreEqual(expectedRepository.CfWebDefaultsDataContract.CfwebCkrCostCenterComps.Count, costCenterStructure.CostCenterComponents.Count);
            for (int i = 0; i < expectedRepository.CfWebDefaultsDataContract.CfwebCkrCostCenterComps.Count; i++)
            {
                // Compare to the data coming from CF.WEB.DEFAULTS
                Assert.AreEqual(expectedRepository.CfWebDefaultsDataContract.CfwebCkrCostCenterComps[i], costCenterStructure.CostCenterComponents[i].ComponentName);
                Assert.AreEqual(expectedRepository.CfWebDefaultsDataContract.CfwebCkrCostCenterDescs[i].ToUpper() == "Y", costCenterStructure.CostCenterComponents[i].IsPartOfDescription);

                // Compare to the data coming from GLSTRUCT
                var glStructData = expectedRepository.GlStructDataContract.GlmajorEntityAssociation
                    .Where(x => x.AcctNamesAssocMember == expectedRepository.CfWebDefaultsDataContract.CfwebCkrCostCenterComps[i]).ToList().FirstOrDefault();
                if (glStructData != null)
                {
                    Assert.AreEqual(glStructData.AcctNamesAssocMember, costCenterStructure.CostCenterComponents[i].ComponentName);
                    Assert.AreEqual(Convert.ToInt32(glStructData.AcctStartAssocMember) - 1, costCenterStructure.CostCenterComponents[i].StartPosition);
                    Assert.AreEqual(glStructData.AcctLengthAssocMember.Value, costCenterStructure.CostCenterComponents[i].ComponentLength);

                    // Confirm the component type
                    var componentType = DetermineComponentType(glStructData.AcctComponentTypeAssocMember);
                    Assert.AreEqual(componentType, costCenterStructure.CostCenterComponents[i].ComponentType);
                }
            }

            // Make sure the object component data matches
            Assert.AreEqual(expectedRepository.CfWebDefaultsDataContract.CfwebCkrObjectCodeComps.Count, costCenterStructure.ObjectComponents.Count);
            for (int i = 0; i < expectedRepository.CfWebDefaultsDataContract.CfwebCkrObjectCodeComps.Count; i++)
            {
                // Compare to the data coming from CF.WEB.DEFAULTS
                Assert.AreEqual(expectedRepository.CfWebDefaultsDataContract.CfwebCkrObjectCodeComps[i], costCenterStructure.ObjectComponents[i].ComponentName);
                Assert.AreEqual(expectedRepository.CfWebDefaultsDataContract.CfwebCkrObjectCodeDescs[i].ToUpper() == "Y", costCenterStructure.ObjectComponents[i].IsPartOfDescription);

                // Compare to the data coming from GLSTRUCT
                var glStructData = expectedRepository.GlStructDataContract.GlmajorEntityAssociation
                    .Where(x => x.AcctNamesAssocMember == expectedRepository.CfWebDefaultsDataContract.CfwebCkrObjectCodeComps[i]).ToList().FirstOrDefault();
                if (glStructData != null)
                {
                    Assert.AreEqual(glStructData.AcctNamesAssocMember, costCenterStructure.ObjectComponents[i].ComponentName);
                    Assert.AreEqual(Convert.ToInt32(glStructData.AcctStartAssocMember) - 1, costCenterStructure.ObjectComponents[i].StartPosition);
                    Assert.AreEqual(glStructData.AcctLengthAssocMember.Value, costCenterStructure.ObjectComponents[i].ComponentLength);

                    // Confirm the component type
                    var componentType = DetermineComponentType(glStructData.AcctComponentTypeAssocMember);
                    Assert.AreEqual(componentType, costCenterStructure.ObjectComponents[i].ComponentType);
                }
            }
        }

        [TestMethod]
        public async Task CostCenterStructure_InvalidCostCenterData()
        {
            expectedRepository.CfWebDefaultsDataContract.CfwebCkrCostCenterDescs.RemoveAt(0);
            var costCenterStructure = await actualRepository.GetCostCenterStructureAsync();

            // Make sure the cost center component data matches
            Assert.AreEqual(expectedRepository.CfWebDefaultsDataContract.CfwebCkrCostCenterComps.Count, costCenterStructure.CostCenterComponents.Count);
            for (int i = 0; i < expectedRepository.CfWebDefaultsDataContract.CfwebCkrCostCenterComps.Count; i++)
            {
                Assert.AreEqual(expectedRepository.CfWebDefaultsDataContract.CfwebCkrCostCenterComps[i], costCenterStructure.CostCenterComponents[i].ComponentName);

                // Compare the description values until we the index gets out of bounds; at that point we expect false.
                if (i < expectedRepository.CfWebDefaultsDataContract.CfwebCkrCostCenterDescs.Count)
                    Assert.AreEqual(expectedRepository.CfWebDefaultsDataContract.CfwebCkrCostCenterDescs[i].ToUpper() == "Y", costCenterStructure.CostCenterComponents[i].IsPartOfDescription);
                else
                    Assert.IsFalse(costCenterStructure.CostCenterComponents[i].IsPartOfDescription);
            }
        }

        [TestMethod]
        public async Task CostCenterStructure_InvalidObjectData()
        {
            expectedRepository.CfWebDefaultsDataContract.CfwebCkrObjectCodeDescs.RemoveAt(0);
            var costCenterStructure = await actualRepository.GetCostCenterStructureAsync();

            // Make sure the object component data matches
            Assert.AreEqual(expectedRepository.CfWebDefaultsDataContract.CfwebCkrObjectCodeComps.Count, costCenterStructure.ObjectComponents.Count);
            for (int i = 0; i < expectedRepository.CfWebDefaultsDataContract.CfwebCkrObjectCodeComps.Count; i++)
            {
                Assert.AreEqual(expectedRepository.CfWebDefaultsDataContract.CfwebCkrObjectCodeComps[i], costCenterStructure.ObjectComponents[i].ComponentName);

                // Compare the description values until we the index gets out of bounds; at that point we expect false.
                if (i < expectedRepository.CfWebDefaultsDataContract.CfwebCkrObjectCodeDescs.Count)
                    Assert.AreEqual(expectedRepository.CfWebDefaultsDataContract.CfwebCkrObjectCodeDescs[i].ToUpper() == "Y", costCenterStructure.ObjectComponents[i].IsPartOfDescription);
                else
                    Assert.IsFalse(costCenterStructure.ObjectComponents[i].IsPartOfDescription);
            }
        }
        #endregion

        #region GL Class Configuration
        [TestMethod]
        [ExpectedException(typeof(ConfigurationException))]
        public async Task GetClassConfiguration_DataReaderReturnsNull()
        {
            expectedRepository.GlClassDefDataContract = null;
            var classConfiguration = await actualRepository.GetClassConfigurationAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(ConfigurationException))]
        public async Task GetClassConfiguration_NullGlClassName()
        {
            expectedRepository.GlClassDefDataContract.GlClassDict = null;
            var classConfiguration = await actualRepository.GetClassConfigurationAsync();
        }

        [TestMethod]
        [ExpectedException(typeof(ConfigurationException))]
        public async Task GetClassConfiguration_NullExpenseValueList()
        {
            expectedRepository.GlClassDefDataContract.GlClassExpenseValues = null;
            var classConfiguration = await actualRepository.GetClassConfigurationAsync();
        }

        [TestMethod]
        public async Task GetClassConfiguration_NullRevenueValueList()
        {
            var expectedMessage = "GL class revenue values are not defined.";
            var actualMessage = "";
            try
            {
                expectedRepository.GlClassDefDataContract.GlClassRevenueValues = null;
                var classConfiguration = await actualRepository.GetClassConfigurationAsync();
            }
            catch (ConfigurationException cex)
            {
                actualMessage = cex.Message;
            }
            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task GetClassConfiguration_Success()
        {
            var classConfiguration = await actualRepository.GetClassConfigurationAsync();
            Assert.AreEqual(expectedRepository.GlClassDefDataContract.GlClassDict, classConfiguration.ClassificationName);

            Assert.AreEqual(expectedRepository.GlClassDefDataContract.GlClassExpenseValues.Count,
                classConfiguration.ExpenseClassValues.Count);
            foreach (var expectedValue in expectedRepository.GlClassDefDataContract.GlClassExpenseValues)
            {
                Assert.IsTrue(classConfiguration.ExpenseClassValues.Contains(expectedValue));
            }

            Assert.AreEqual(expectedRepository.GlClassDefDataContract.GlClassRevenueValues.Count,
                classConfiguration.RevenueClassValues.Count);
            foreach (var expectedValue in expectedRepository.GlClassDefDataContract.GlClassRevenueValues)
            {
                Assert.IsTrue(classConfiguration.RevenueClassValues.Contains(expectedValue));
            }
        }
        #endregion

        #region Available fiscal years
        [TestMethod]
        public async Task GetAllFiscalYearsAsync_BulkReadReturnsNull()
        {
            expectedRepository.GenLdgrDataContracts = null;

            // Set up the FY data
            var fiscalYearInfo = await expectedRepository.GetFiscalYearConfigurationAsync();

            string message = "";
            try
            {
                var actualFiscalYears = await actualRepository.GetAllFiscalYearsAsync(testFiscalYear);
            }
            catch (ConfigurationException anex)
            {
                message = anex.Message;
            }
            Assert.AreEqual("No fiscal years have been set up.", message);
        }

        [TestMethod]
        public async Task GetAllFiscalYearsAsync_BulkReadReturnsEmptySet()
        {
            // Remove all fiscal years
            while (expectedRepository.GenLdgrDataContracts.Count > 0)
                expectedRepository.GenLdgrDataContracts.RemoveAt(0);

            // Set up the FY data
            var fiscalYearInfo = await expectedRepository.GetFiscalYearConfigurationAsync();

            string message = "";
            try
            {
                var actualFiscalYears = await actualRepository.GetAllFiscalYearsAsync(testFiscalYear);
            }
            catch (ConfigurationException anex)
            {
                message = anex.Message;
            }
            Assert.AreEqual("No fiscal years have been set up.", message);
        }

        [TestMethod]
        public async Task GetAllFiscalYearsAsync_OneGenLdgrContractIsNull()
        {
            // Set up the FY data
            var genLdgrDataContract = expectedRepository.GenLdgrDataContracts[2] = null;

            var fiscalYearInfo = await expectedRepository.GetFiscalYearConfigurationAsync();
            var expectedFiscalYears = await expectedRepository.GetAllFiscalYearsAsync(fiscalYearInfo.FiscalYearForToday);

            var actualFiscalYears = await actualRepository.GetAllFiscalYearsAsync(fiscalYearInfo.FiscalYearForToday);

            // Make sure the repository returns the same number of years.
            Assert.AreEqual(expectedRepository.GenLdgrDataContracts.Count - 1, actualFiscalYears.Count());

            foreach (var year in actualFiscalYears)
            {
                // Make sure the lists contain the same values.
                int yearInt = 0;
                Int32.TryParse(year, out yearInt);
                Assert.IsTrue(expectedFiscalYears.Contains(year));
            }
        }

        [TestMethod]
        public async Task GetAllFiscalYearsAsync_OneGenLdgrContractHasNullRecordKey()
        {
            // Set up the FY data
            var genLdgrDataContract = expectedRepository.GenLdgrDataContracts[2].Recordkey = null;

            var fiscalYearInfo = await expectedRepository.GetFiscalYearConfigurationAsync();
            var expectedFiscalYears = await expectedRepository.GetAllFiscalYearsAsync(fiscalYearInfo.FiscalYearForToday);

            var actualFiscalYears = await actualRepository.GetAllFiscalYearsAsync(fiscalYearInfo.FiscalYearForToday);

            // Make sure the repository returns the same number of years.
            Assert.AreEqual(expectedRepository.GenLdgrDataContracts.Count - 1, actualFiscalYears.Count());

            foreach (var year in actualFiscalYears)
            {
                // Make sure the lists contain the same values.
                int yearInt = 0;
                Int32.TryParse(year, out yearInt);
                Assert.IsTrue(expectedFiscalYears.Contains(year));
            }
        }

        [TestMethod]
        public async Task GetAllFiscalYearsAsync_MoreYearsThanRequested()
        {
            // Set up the FY data
            expectedRepository.GenLdgrDataContracts.Add(new GenLdgr() { Recordkey = "1995", GenLdgrStatus = "C" });
            expectedRepository.GenLdgrDataContracts.Add(new GenLdgr() { Recordkey = "1994", GenLdgrStatus = "C" });
            expectedRepository.GenLdgrDataContracts.Add(new GenLdgr() { Recordkey = "1993", GenLdgrStatus = "C" });

            var fiscalYearInfo = await expectedRepository.GetFiscalYearConfigurationAsync();
            var expectedFiscalYears = await expectedRepository.GetAllFiscalYearsAsync(fiscalYearInfo.FiscalYearForToday);

            var actualFiscalYears = await actualRepository.GetAllFiscalYearsAsync(fiscalYearInfo.FiscalYearForToday);

            // Make sure the repository returns the same number of years.
            Assert.AreEqual(expectedFiscalYears.Count(), actualFiscalYears.Count());

            // Make sure the first year returned is the fiscal year for the current date.
            Assert.AreEqual(fiscalYearInfo.FiscalYearForToday.ToString(), actualFiscalYears.ElementAt(1));

            foreach (var year in actualFiscalYears)
            {
                // Make sure the lists contain the same values.
                int yearInt = 0;
                Int32.TryParse(year, out yearInt);
                Assert.IsTrue(expectedFiscalYears.Contains(yearInt.ToString()));
            }
        }

        [TestMethod]
        public async Task GetAllFiscalYearsAsync_SameYearsAsRequested()
        {
            // Set up the FY data
            var fiscalYearInfo = await expectedRepository.GetFiscalYearConfigurationAsync();
            var expectedFiscalYears = await expectedRepository.GetAllFiscalYearsAsync(fiscalYearInfo.FiscalYearForToday);

            var actualFiscalYears = await actualRepository.GetAllFiscalYearsAsync(fiscalYearInfo.FiscalYearForToday);

            // Make sure the repository returns the same number of years.
            Assert.AreEqual(expectedFiscalYears.Count(), actualFiscalYears.Count());

            // Make sure the first year returned is the fiscal year for the current date.
            Assert.AreEqual(fiscalYearInfo.FiscalYearForToday.ToString(), actualFiscalYears.ElementAt(1));

            var previousFiscalYear = 0;
            foreach (var year in actualFiscalYears)
            {
                // Make sure the lists contain the same values.
                int yearInt = 0;
                Int32.TryParse(year, out yearInt);
                Assert.IsTrue(expectedFiscalYears.Contains(yearInt.ToString()));

                // Make sure the actual list contains decremented values.
                if (previousFiscalYear != 0)
                    Assert.AreEqual(previousFiscalYear, yearInt + 1);

                previousFiscalYear = yearInt;
            }
        }

        [TestMethod]
        public async Task GetAllFiscalYearsAsync_FewerYearsThanRequested()
        {
            // Delete two of the six original fiscal years.
            expectedRepository.GenLdgrDataContracts.RemoveAt(6);
            expectedRepository.GenLdgrDataContracts.RemoveAt(5);

            // Set up the FY data
            var fiscalYearInfo = await expectedRepository.GetFiscalYearConfigurationAsync();
            var expectedFiscalYears = await expectedRepository.GetAllFiscalYearsAsync(fiscalYearInfo.FiscalYearForToday);

            var actualFiscalYears = await actualRepository.GetAllFiscalYearsAsync(fiscalYearInfo.FiscalYearForToday);

            // Make sure the repository returns the same number of years.
            Assert.AreEqual(expectedRepository.GenLdgrDataContracts.Count, actualFiscalYears.Count());

            // Make sure the first year returned is the fiscal year for the current date.
            Assert.AreEqual(fiscalYearInfo.FiscalYearForToday.ToString(), actualFiscalYears.ElementAt(1));

            foreach (var year in actualFiscalYears)
            {
                // Make sure the lists contain the same values.
                int yearInt = 0;
                Int32.TryParse(year, out yearInt);
                Assert.IsTrue(expectedFiscalYears.Contains(yearInt.ToString()));
            }
        }
        #endregion

        #region Private methods
        private GeneralLedgerConfigurationRepository BuildRepository()
        {
            return new GeneralLedgerConfigurationRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);
        }

        private GeneralLedgerComponentType DetermineComponentType(string componentTypeString)
        {
            var componentType = GeneralLedgerComponentType.Function;
            switch (componentTypeString.ToUpper())
            {
                case "FD":
                    componentType = GeneralLedgerComponentType.Fund;
                    break;
                case "FC":
                    componentType = GeneralLedgerComponentType.Function;
                    break;
                case "OB":
                    componentType = GeneralLedgerComponentType.Object;
                    break;
                case "UN":
                    componentType = GeneralLedgerComponentType.Unit;
                    break;
                case "SO":
                    componentType = GeneralLedgerComponentType.Source;
                    break;
                case "LO":
                    componentType = GeneralLedgerComponentType.Location;
                    break;
            }

            return componentType;
        }
        #endregion
    }
}