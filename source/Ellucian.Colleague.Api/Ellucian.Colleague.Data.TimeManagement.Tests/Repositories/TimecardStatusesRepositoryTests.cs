﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.TimeManagement.DataContracts;
using Ellucian.Colleague.Data.TimeManagement.Repositories;
using Ellucian.Colleague.Data.TimeManagement.Transactions;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.TimeManagement;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Tests;
using Ellucian.Web.Http.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.TimeManagement.Tests.Repositories
{
    [TestClass]
    public class TimecardStatusesRepositoryTests : BaseRepositorySetup
    {
        public TestTimecardStatusesRepository testData;
        public TimecardStatusesRepository statusesRepository;
        public CreateTimecardStatusesRequest actualCreateTimecardStatusesRequest;


        public TimecardStatusesRepository BuildRepository()
        {

            dataReaderMock.Setup(d => d.BulkReadRecordAsync<DataContracts.TimecardStatus>(It.IsAny<string>(), It.IsAny<bool>()))
                .Returns<string, bool>((criteria, b) =>
                    Task.FromResult(new Collection<DataContracts.TimecardStatus>(
                            testData.timecardStatusRecords
                                .Where(rec => criteria.Contains(rec.timecardId))
                                .Select(rec => ConvertTestDataToDataContract(rec)).ToList())));                        

            transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardStatusesRequest, CreateTimecardStatusesResponse>(It.IsAny<CreateTimecardStatusesRequest>()))
                .Callback<CreateTimecardStatusesRequest>((req) =>
                {
                    actualCreateTimecardStatusesRequest = req;
                })
                .Returns<CreateTimecardStatusesRequest>((req) => 
                    Task.FromResult(new CreateTimecardStatusesResponse() 
                        {
                            ErrorMessage = new List<string>(),
                            NewKey = new List<string>() { (testData.CreateTimecardStatusRecordHelper(req.TimecardStatusGroup.ElementAt(0).TsActionerId, req.TimecardStatusGroup.ElementAt(0).TsActionType, null, req.TimecardStatusGroup.ElementAt(0).TsTimecardId).id) }
                        }));

            dataReaderMock.Setup(d => d.ReadRecordAsync<DataContracts.TimecardStatus>(It.IsAny<string>(), It.IsAny<bool>()))
                .Returns<string, bool>((id, b) => Task.FromResult(testData.timecardStatusRecords
                    .Where(t => t.id == id).Select(rec => ConvertTestDataToDataContract(rec)).FirstOrDefault()));

            loggerMock.Setup(l => l.IsErrorEnabled).Returns(true);

            //the 
            dataReaderMock.Setup(d => d.SelectAsync("TIMECARD", "WITH TCD.EMPLOYEE.ID EQ ?", It.IsAny<string[]>(), "?", true, 425))
                .Returns<string, string, string[], string, bool, int>((f, q, personIds, p, b, i) =>
                    Task.FromResult(testData.timecardStatusRecords.Select(ts => ts.timecardId).ToArray())
                );

            dataReaderMock.Setup(d => d.SelectAsync("TIMECARD.STATUS", "WITH TS.TIMECARD.ID EQ ?", It.IsAny<string[]>(), "?", true, 425))
                .Returns<string, string, string[], string, bool, int>((f, q, timecardIds, p, b, i) =>
                    Task.FromResult(testData.timecardStatusRecords
                        .Where(ts => timecardIds.Select(v => v.Replace("\"", "").Replace("\\", "")).Contains(ts.timecardId))
                        .Select(ts => ts.id)
                        .ToArray())
                    );

            dataReaderMock.Setup(d => d.BulkReadRecordAsync<DataContracts.TimecardStatus>(It.IsAny<string[]>(), true))
                .Returns<string[], bool>((ids, b) => Task.FromResult(new Collection<DataContracts.TimecardStatus>(
                    testData.timecardStatusRecords
                    .Where(ts => ids.Contains(ts.id))
                    .Select(ts => ConvertTestDataToDataContract(ts)).ToList())));

            return new TimecardStatusesRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettings);
        }

        public DataContracts.TimecardStatus ConvertTestDataToDataContract(TestTimecardStatusesRepository.TimecardStatusRecord testData)
        {
            return new DataContracts.TimecardStatus()
            {
                Recordkey = testData.id,
                TimecardStatusAdddate = testData.addDate,
                TimecardStatusAddopr = testData.addOpr,
                TimecardStatusAddtime = testData.addTime,
                TimecardStatusChgdate = testData.chgDate,
                TimecardStatusChgopr = testData.chgOpr,
                TimecardStatusChgtime = testData.chgTime,
                TsActionerId = testData.actionerId,
                TsActionType = testData.actionType,
                TsTimecardId = testData.timecardId
            };
        }

        public void TimecardStatusesRepositoryTestsInitialize()
        {
            MockInitialize();
            testData = new TestTimecardStatusesRepository();
            statusesRepository = BuildRepository();
        }

        [TestClass]
        public class CreateTimecardStatusTests : TimecardStatusesRepositoryTests
        {
            public Domain.TimeManagement.Entities.TimecardStatus inputStatus;

            [TestInitialize]
            public void Initialize()
            {
                inputStatus = new Domain.TimeManagement.Entities.TimecardStatus("555", "0003914", StatusAction.Submitted, "152") { };
                base.TimecardStatusesRepositoryTestsInitialize();
            }

            [TestMethod]
            public async Task StatusIsReturnedTest()
            {
                var results = await statusesRepository.CreateTimecardStatusesAsync(new List<Domain.TimeManagement.Entities.TimecardStatus> () { inputStatus });
                var actual = results.ElementAt(0);
                Assert.IsTrue(!string.IsNullOrWhiteSpace(actual.Id));
                Assert.AreEqual(inputStatus.TimecardId, actual.TimecardId);
                Assert.AreEqual(inputStatus.ActionerId, actual.ActionerId);
                Assert.AreEqual(inputStatus.ActionType, actual.ActionType);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NullTimecardStatusCannotbeCreatedTest()
            {
                try
                {
                    await statusesRepository.CreateTimecardStatusesAsync(null);
                }
                catch (Exception ane)
                {
                    loggerMock.Verify(e => e.Error(It.IsAny<string>()));
                    throw ane;
                }
            }


            [TestMethod]
            public async Task ActualCtxRequestTest()
            {
                await statusesRepository.CreateTimecardStatusesAsync(new List<Domain.TimeManagement.Entities.TimecardStatus>() { inputStatus });
                Assert.IsNotNull(actualCreateTimecardStatusesRequest);
                Assert.AreEqual(inputStatus.TimecardId, actualCreateTimecardStatusesRequest.TimecardStatusGroup.ElementAt(0).TsTimecardId);
                Assert.AreEqual(inputStatus.ActionerId, actualCreateTimecardStatusesRequest.TimecardStatusGroup.ElementAt(0).TsActionerId);
                Assert.AreEqual(inputStatus.ActionType.ToString(), actualCreateTimecardStatusesRequest.TimecardStatusGroup.ElementAt(0).TsActionType);
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NullResponseFromTransactionErrorTest()
            {
                try
                {
                    transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardStatusesRequest, CreateTimecardStatusesResponse>(It.IsAny<CreateTimecardStatusesRequest>()))
                        .Returns<CreateTimecardStatusesResponse>(req => Task.FromResult<CreateTimecardStatusesResponse>(null));
                    await statusesRepository.CreateTimecardStatusesAsync(new List<Domain.TimeManagement.Entities.TimecardStatus>() { inputStatus });
                }
                catch (Exception ae)
                {
                    loggerMock.Verify(e => e.Error(It.IsAny<string>()));
                    throw ae;
                }
            }

            [TestMethod]
            public async Task ErrorMessageFromTransactionTest_LogsErrors()
            {
                try
                {
                    transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardStatusesRequest, CreateTimecardStatusesResponse>(It.IsAny<CreateTimecardStatusesRequest>()))
                        .Returns<CreateTimecardStatusesRequest>(req =>
                            Task.FromResult(new CreateTimecardStatusesResponse() { ErrorMessage = new List<string>() {"foobar"} }));
                    await statusesRepository.CreateTimecardStatusesAsync(new List<Domain.TimeManagement.Entities.TimecardStatus>() {inputStatus} );
                }
                catch (Exception ae)
                {
                    loggerMock.Verify(e => e.Error(It.IsAny<string>()));
                    throw ae;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task ErrorGettingCreatedStatusRecordTest()
            {
                dataReaderMock.Setup(d => d.BulkReadRecordAsync<DataContracts.TimecardStatus>(It.IsAny<string[]>(), It.IsAny<bool>()))
                    .Returns<string[], bool>((id, b) => Task.FromResult<Collection<DataContracts.TimecardStatus>>(new Collection<DataContracts.TimecardStatus>()));

                await statusesRepository.CreateTimecardStatusesAsync(new List<Domain.TimeManagement.Entities.TimecardStatus>() { inputStatus });

            }
        }

        [TestClass]
        public class GetTimecardStatusesByTimecardIdTests : TimecardStatusesRepositoryTests
        {
            public string inputTimecardId;

            [TestInitialize]
            public void Initialize()
            {
                base.TimecardStatusesRepositoryTestsInitialize();
                inputTimecardId = testData.timecardStatusRecords.First().timecardId;
            }

            [TestMethod]
            public async Task ExpectedEqualsActualTest()
            {
                var expected = await testData.GetTimecardStatusesByTimecardIdAsync(inputTimecardId);
                var actual = await statusesRepository.GetTimecardStatusesByTimecardIdAsync(inputTimecardId);
                CollectionAssert.AreEqual(expected.ToArray(), actual.ToArray());
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task CannotGetStatuesForNullTimecardIdTest()
            {
                try
                {
                    await statusesRepository.GetTimecardStatusesByTimecardIdAsync(null);
                }
                catch (ArgumentNullException ane)
                {
                    loggerMock.Verify(e => e.Error(It.IsAny<string>()));
                    throw ane;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task BulkReadRecordMethodReturnsNullTest()
            {
                dataReaderMock.Setup(d => d.BulkReadRecordAsync<DataContracts.TimecardStatus>(It.IsAny<string>(), It.IsAny<bool>()))
                    .Returns<string, bool>((criteria, b) => Task.FromResult<Collection<DataContracts.TimecardStatus>>(null));

                await statusesRepository.GetTimecardStatusesByTimecardIdAsync(inputTimecardId);
            }

            [TestMethod]
            public async Task UnknownStatusActionLogsErrorTest()
            {
                testData.timecardStatusRecords.ForEach(r => r.actionType = "FOOBAR");
                var actual = await statusesRepository.GetTimecardStatusesByTimecardIdAsync(inputTimecardId);

                Assert.IsFalse(actual.Any());

                loggerMock.Verify(l => l.Error(It.IsAny<ApplicationException>(), It.IsAny<string>()));
            }

            [TestMethod]
            public async Task CorruptDataBuildingEntityLogsErrorTest()
            {
                testData.timecardStatusRecords.ForEach(r => r.actionType = null);
                var actual = await statusesRepository.GetTimecardStatusesByTimecardIdAsync(inputTimecardId);

                Assert.IsFalse(actual.Any());

                loggerMock.Verify(l => l.Error(It.IsAny<Exception>(), It.IsAny<string>()));
            }

        }


        [TestClass]
        public class GetTimecardStatusesByPersonIdTests : TimecardStatusesRepositoryTests
        {
            public List<string> inputPersonIds;

            [TestInitialize]
            public void Initialize()
            {
                TimecardStatusesRepositoryTestsInitialize();

                inputPersonIds = new List<string>() { "1", "2", "3", "4", "5" };
            }

            [TestMethod]
            public async Task GetStatusesTest()
            {
                var expected = await testData.GetTimecardStatusesByPersonIdsAsync(inputPersonIds);
                var actual = await statusesRepository.GetTimecardStatusesByPersonIdsAsync(inputPersonIds);

                CollectionAssert.AreEqual(expected.ToArray(), actual.ToArray());
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NullInputPersonIdsTest()
            {
                inputPersonIds = null;

                await statusesRepository.GetTimecardStatusesByPersonIdsAsync(inputPersonIds);
            }

            [TestMethod]
            public async Task EmptyPersonIdsTest()
            {
                inputPersonIds = new List<string>();

                var actual = await statusesRepository.GetTimecardStatusesByPersonIdsAsync(inputPersonIds);

                Assert.IsFalse(actual.Any());
            }

            [TestMethod]
            public async Task NoSelectedTimecardIdsTest()
            {
                testData.timecardStatusRecords = new List<TestTimecardStatusesRepository.TimecardStatusRecord>();

                var actual = await statusesRepository.GetTimecardStatusesByPersonIdsAsync(inputPersonIds);

                Assert.IsFalse(actual.Any());
            }

            [TestMethod]
            public async Task NoSelectedTimecardStatusIdsTest()
            {
                dataReaderMock.Setup(d => d.SelectAsync("TIMECARD.STATUS", "WITH TS.TIMECARD.ID EQ ?", It.IsAny<string[]>(), "?", true, 425))
                    .ReturnsAsync(new string[0] { });

                var actual = await statusesRepository.GetTimecardStatusesByPersonIdsAsync(inputPersonIds);

                Assert.IsFalse(actual.Any());
            }

            [TestMethod]
            public async Task TimecardStatusBulkReadsAreChunkedTest()
            {
                apiSettings.BulkReadSize = 1;
                statusesRepository = BuildRepository();

                await statusesRepository.GetTimecardStatusesByPersonIdsAsync(inputPersonIds);

                dataReaderMock.Verify(d => d.BulkReadRecordAsync<DataContracts.TimecardStatus>(It.IsAny<string[]>(), true), Times.Exactly(testData.timecardStatusRecords.Count()));                    
                    
            }

            [TestMethod]
            public async Task ErrorBuildingStatusesTest()
            {
                testData.timecardStatusRecords.ForEach(rec => rec.actionType = "foobar");

                var actual = await statusesRepository.GetTimecardStatusesByPersonIdsAsync(inputPersonIds);

                loggerMock.Verify(l => l.Error(It.IsAny<ApplicationException>(), It.IsAny<string>()));

                Assert.IsFalse(actual.Any());
            }
        }
    }
}
