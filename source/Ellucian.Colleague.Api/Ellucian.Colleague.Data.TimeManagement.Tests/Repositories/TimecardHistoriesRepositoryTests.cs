﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.TimeManagement.Repositories;
using Ellucian.Colleague.Data.TimeManagement.Transactions;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.TimeManagement.Tests.Repositories
{
    [TestClass]
    public class TimecardHistoriesRepositoryTests : BaseRepositorySetup
    {
        public TestTimecardHistoriesRepository testData; // just source our testdata from here because it is the same
        public TestTimecardsRepository testTimecardData;
        public TestTimecardStatusesRepository testTimecardStatusData;
        public TestTimeEntryCommentsRepository testTimeEntryCommentsData;
        public TimecardHistoriesRepository historiesRepository;
        public string historyId;
        public DateTime getQueryStartDate;
        public DateTime getQueryEndDate;

        public CreateTimecardHistoryRequest createHistoryRequest;

        public TimecardHistoriesRepository BuildRepository()
        {
            // creates a history
            transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardHistoryRequest, CreateTimecardHistoryResponse>(It.IsAny<Ellucian.Colleague.Data.TimeManagement.Transactions.CreateTimecardHistoryRequest>()))
                .Callback<CreateTimecardHistoryRequest>((req) =>
                {
                    createHistoryRequest = req;
                })
                .Returns<CreateTimecardHistoryRequest>((req) => Task.FromResult(new CreateTimecardHistoryResponse() { ErrorMessage = "", NewTimecardHistoryId = historyId }));

            // gets some histories
            // I hope we can all take a moment to appreciate how easy to read this setup is
            dataReaderMock.Setup(d => d.SelectAsync("TIMECARD.HISTORY", 
                It.IsAny<string>(),//string.Format("WITH TCDH.EMPLOYEE.ID EQ ? AND TCDH.END.DATE GE '{0}' AND TCDH.START.DATE LE '{1}'", getQueryStartDate, getQueryEndDate), 
                It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>()))
                .Returns<string, string, string[], string, bool, int>((f, c, values, p, r, s) =>
                        Task.FromResult((testData.DatabaseTimecardHistories == null) ? null :
                            testData.DatabaseTimecardHistories
                                .Where(rec => values.Select(v => v.Replace("\"", "").Replace("\\", "").Replace("\\", "")).Contains(rec.TcdhEmployeeId))
                                .Where(rec => rec.TcdhEndDate >= getQueryStartDate && rec.TcdhStartDate <= getQueryEndDate)
                                .Select(rec => rec.Recordkey).ToArray()
                        ));

            dataReaderMock.Setup(d => d.SelectAsync("TIME.ENTRY.HISTORY", 
                "WITH TEH.TIMECARD.HISTORY.ID EQ ?", 
                It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>()))
                .Returns<string, string, string[], string, bool, int>((f, c, values, p, r, s) =>
                        Task.FromResult((testData.DatabaseTimeEntryHistories == null) ? null :
                            testData.DatabaseTimeEntryHistories
                                .Where(rec => values.Select(v => v.Replace("\"", "").Replace("\\", "")).Contains(rec.TehTimecardHistoryId))
                                .Select(rec => rec.Recordkey).ToArray()
                        ));

            dataReaderMock.Setup(datamock => datamock.BulkReadRecordAsync<DataContracts.TimecardHistory>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .Returns<string[], bool>((ids, x) =>
                    Task.FromResult(
                    new Collection<Data.TimeManagement.DataContracts.TimecardHistory>(
                        testData.DatabaseTimecardHistories
                            .Where(tc => ids.Contains(tc.Recordkey))
                            .Select(tc => new DataContracts.TimecardHistory()
                            {
                                Recordkey = tc.Recordkey,
                                TcdhEmployeeId = tc.TcdhEmployeeId,
                                TcdhEndDate = tc.TcdhEndDate,
                                TcdhPaycycleId = tc.TcdhPaycycleId,
                                TcdhPeriodEndDate = tc.TcdhPeriodEndDate,
                                TcdhPeriodStartDate = tc.TcdhPeriodStartDate,
                                TcdhPositionId = tc.TcdhPositionId,
                                TcdhStartDate = tc.TcdhStartDate,
                                TcdhStatusActionType = tc.TcdhStatusActionType,
                                TimecardHistoryAdddate = tc.TimecardHistoryAdddate,
                                TimecardHistoryAddopr = tc.TimecardHistoryAddopr,
                                TimecardHistoryAddtime = tc.TimecardHistoryAddtime,
                                TimecardHistoryChgdate = tc.TimecardHistoryChgdate,
                                TimecardHistoryChgopr = tc.TimecardHistoryChgopr,
                                TimecardHistoryChgtime = tc.TimecardHistoryChgtime
                            }).ToList())));

            dataReaderMock.Setup(d => d.BulkReadRecordAsync<DataContracts.TimeEntryHistory>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .Returns<string[], bool>((ids, x) =>
                    Task.FromResult(
                        new Collection<DataContracts.TimeEntryHistory>(
                            testData.DatabaseTimeEntryHistories
                                .Where(te => ids.Contains(te.Recordkey))
                                .Select(te => new DataContracts.TimeEntryHistory()
                                {
                                    Recordkey = te.Recordkey,
                                    TehEarntypeId = te.TehEarntypeId,
                                    TehInDate = te.TehInDate,
                                    TehInTime = te.TehInTime,
                                    TehOutDate = te.TehOutDate,
                                    TehOutTime = te.TehOutTime,
                                    TehPerleaveId = te.TehPerleaveId,
                                    TehProjectId = te.TehProjectId,
                                    TehTimecardHistoryId = te.TehTimecardHistoryId,
                                    TehWorkedDate = te.TehWorkedDate,
                                    TehWorkedMinutes = te.TehWorkedMinutes,
                                    TimeEntryHistoryAdddate = te.TimeEntryHistoryAdddate,
                                    TimeEntryHistoryChgdate = te.TimeEntryHistoryChgdate,
                                    TimeEntryHistoryChgtime = te.TimeEntryHistoryChgtime,
                                    TimeEntryHistoryChgopr = te.TimeEntryHistoryChgopr,
                                    TimeEntryHistoryAddtime = te.TimeEntryHistoryAddtime,
                                    TimeEntryHistoryAddopr = te.TimeEntryHistoryAddopr,
                                }).ToList())));

            loggerMock.Setup(l => l.IsErrorEnabled).Returns(true);

            return new TimecardHistoriesRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettings);
        }

        public void TimecardHistoryTestsInitialize()
        {
            historyId = "1492";
            MockInitialize();
            testData = new TestTimecardHistoriesRepository();
            testTimecardData = new TestTimecardsRepository();
            testTimecardStatusData = new TestTimecardStatusesRepository();
            testTimeEntryCommentsData = new TestTimeEntryCommentsRepository();
            historiesRepository = BuildRepository();


        }

        #region VERSION 1
        [TestClass]
        public class CreateTimecardHistoriesTests : TimecardHistoriesRepositoryTests
        {
            public async Task<Timecard> getInputTimecard() 
            {
                return await testTimecardData.GetTimecardAsync(testTimecardData.timecardRecords[0].id);
            }

            public async Task<TimecardStatus> getInputTimecardStatus()
            {
                return (await testTimecardStatusData.GetTimecardStatusesByTimecardIdAsync(testTimecardStatusData.timecardStatusRecords[0].timecardId)).First();
            }

            public async Task<IEnumerable<TimeEntryComment>> getInputTimeEntryComments()
            {

                var timecard = await getInputTimecard();
                testTimeEntryCommentsData.timecardCommentRecords.ForEach(t => { t.employeeID = timecard.EmployeeId; t.timecardId = timecard.Id; });
                return (await testTimeEntryCommentsData.GetCommentsAsync(new string[1] { testTimeEntryCommentsData.timecardCommentRecords[0].employeeID }))
                    .Where(tc => tc.IsLinkedToTimecard(timecard));
            }

            public async Task<string> createHistory()
            {
                return await historiesRepository.CreateTimecardHistoryAsync(await getInputTimecard(), await getInputTimecardStatus(), await getInputTimeEntryComments());
            }

            [TestInitialize]
            public void Initialize()
            {
                
                base.TimecardHistoryTestsInitialize(); 
            }

            [TestMethod]
            public async Task RequestCtxTest()
            {
                await createHistory();
                Assert.IsNotNull(createHistoryRequest);
                var timecard = await getInputTimecard();
                var status = await getInputTimecardStatus();
                var comments = await getInputTimeEntryComments();

                Assert.AreEqual(timecard.EmployeeId, createHistoryRequest.TcdhEmployeeId);
                Assert.AreEqual(timecard.EndDate, createHistoryRequest.TcdhEndDate);
                Assert.AreEqual(timecard.PayCycleId, createHistoryRequest.TcdhPaycycleId);
                Assert.AreEqual(timecard.PeriodEndDate, createHistoryRequest.TcdhPeriodEndDate);
                Assert.AreEqual(timecard.PeriodStartDate, createHistoryRequest.TcdhPeriodStartDate);
                Assert.AreEqual(timecard.PositionId, createHistoryRequest.TcdhPositionId);
                Assert.AreEqual(timecard.StartDate, createHistoryRequest.TcdhStartDate);
                Assert.AreEqual(timecard.TimeEntries.Count(), createHistoryRequest.CreateTimeEntryHistories.Count());

                Assert.AreEqual(status.ActionType.ToString(), createHistoryRequest.TcdhStatusActionType);
                Assert.AreEqual(status.ActionerId, createHistoryRequest.TcdhStatusActionerId);

                Assert.AreEqual(comments.Count(), createHistoryRequest.CreateTimecardComments.Count());
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NullTimecardErrorTest()
            {

                await historiesRepository.CreateTimecardHistoryAsync(null, await getInputTimecardStatus(), await getInputTimeEntryComments());

            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NullTimecardStatusErrorTest()
                {
                await historiesRepository.CreateTimecardHistoryAsync(await getInputTimecard(), null, await getInputTimeEntryComments());
                }

            [TestMethod]
            public async Task NullTimeEntryCommentsLogsMessageTest()
                {
                await historiesRepository.CreateTimecardHistoryAsync(await getInputTimecard(), await getInputTimecardStatus(), null);
                loggerMock.Verify(l => l.Info("timeEntryComments is null in history repository. no comments will be saved for this history record"));
                }

            [TestMethod]
            public async Task CreatedStatusReturnsIdTest()
            {
                var newHistoryId = await createHistory();
                Assert.AreEqual(historyId, newHistoryId);
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NullCreateResponseErrorTest()
            {
                try
                {
                    transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardHistoryRequest, CreateTimecardHistoryResponse>(It.IsAny<Ellucian.Colleague.Data.TimeManagement.Transactions.CreateTimecardHistoryRequest>()))
                        .ReturnsAsync(null);
                    await createHistory();

                }
                catch (ApplicationException ae)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ae;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NoHistoryIdReturnedErrorTest()
            {
                try
                {
                    transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardHistoryRequest, CreateTimecardHistoryResponse>(It.IsAny<Ellucian.Colleague.Data.TimeManagement.Transactions.CreateTimecardHistoryRequest>()))
                        .Returns<CreateTimecardHistoryRequest>((req) => Task.FromResult(new CreateTimecardHistoryResponse() { ErrorMessage = "", NewTimecardHistoryId = null }));
                    await createHistory();
                }
                catch (ApplicationException ae)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ae;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task GenericErrorTest()
            {

                    transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardHistoryRequest, CreateTimecardHistoryResponse>(It.IsAny<Ellucian.Colleague.Data.TimeManagement.Transactions.CreateTimecardHistoryRequest>()))
                        .Throws(new Exception("something"));
                await createHistory();

            }

        }

        [TestClass]
        public class GetTimecardHistoriesTests : TimecardHistoriesRepositoryTests
        {
            [TestInitialize]
            public void Initialize()
            {
                getQueryStartDate = new DateTime(2016, 09, 01);
                getQueryEndDate = new DateTime(2016, 09, 05);
                base.TimecardHistoryTestsInitialize();
                return;
            }

            [TestMethod]
            public async Task ExpectedTimecardHistoriesAreReturned()
            {
                var start = getQueryStartDate;
                var end = getQueryEndDate;
                var employeeIds = new List<string>() { "24601" };
                var expected = (await testData.GetTimecardHistoriesAsync(start, end, employeeIds)).OrderBy(t => t.Id);
                var actual = (await historiesRepository.GetTimecardHistoriesAsync(start, end, employeeIds)).OrderBy(t => t.Id);
                var props = 0;
                var props2 = 0;
                try
                {
                    for (var i = 0; i < expected.Count(); i++ )
                    {
                        foreach (var prop in expected.ElementAt(i).GetType().GetProperties())
                        {
                            if(prop.GetType() != typeof(List<TimeEntryHistory>))
                            {
                                props++;
                                Assert.AreEqual(prop.GetValue(expected.ElementAt(i)), prop.GetValue(actual.ElementAt(i)));
                            }                            
                            else if (1 == Math.Log10(99))
                            {
                                for(var j = 0; j < expected.ElementAt(i).TimeEntryHistories.Count; j++)
                                {
                                    foreach(var prop2 in expected.ElementAt(i).TimeEntryHistories[j].GetType().GetProperties())
                                    {
                                        props2++;

                                        Assert.AreEqual(
                                            prop2.GetValue(expected.ElementAt(i).TimeEntryHistories[j]), 
                                            prop2.GetValue(actual.ElementAt(i).TimeEntryHistories[j])
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
                catch(Exception e)
                {
                    logger.Error(e.Message);
                    logger.Error(props.ToString());
                    logger.Error(props2.ToString());
                }
            }

            //  exception tests...
        }
        #endregion

        #region VERSION 2
        [TestClass]
        public class CreateTimecard2HistoriesTests : TimecardHistoriesRepositoryTests
        {
            public async Task<Timecard2> getInputTimecard2()
            {
                return await testTimecardData.GetTimecard2Async(testTimecardData.timecardRecords[0].id);
            }

            public async Task<TimecardStatus> getInputTimecardStatus()
            {
                return (await testTimecardStatusData.GetTimecardStatusesByTimecardIdAsync(testTimecardStatusData.timecardStatusRecords[0].timecardId)).First();
            }

            public async Task<IEnumerable<TimeEntryComment>> getInputTimeEntryComments()
            {

                Timecard2 timecard = await getInputTimecard2();
                testTimeEntryCommentsData.timecardCommentRecords.ForEach(t => { t.employeeID = timecard.EmployeeId; t.timecardId = timecard.Id; });
                return (await testTimeEntryCommentsData.GetCommentsAsync(new string[1] { testTimeEntryCommentsData.timecardCommentRecords[0].employeeID }))
                    .Where(tc => tc.IsLinkedToTimecard(timecard));
            }

            public async Task<string> createHistory2()
            {
                return await historiesRepository.CreateTimecardHistory2Async(await getInputTimecard2(), await getInputTimecardStatus(), await getInputTimeEntryComments());
            }

            [TestInitialize]
            public void Initialize()
            {

                base.TimecardHistoryTestsInitialize();
            }

            [TestMethod]
            public async Task RequestCtxTest()
            {
                await createHistory2();
                Assert.IsNotNull(createHistoryRequest);
                var timecard = await getInputTimecard2();
                var status = await getInputTimecardStatus();
                var comments = await getInputTimeEntryComments();

                Assert.AreEqual(timecard.EmployeeId, createHistoryRequest.TcdhEmployeeId);
                Assert.AreEqual(timecard.EndDate, createHistoryRequest.TcdhEndDate);
                Assert.AreEqual(timecard.PayCycleId, createHistoryRequest.TcdhPaycycleId);
                Assert.AreEqual(timecard.PeriodEndDate, createHistoryRequest.TcdhPeriodEndDate);
                Assert.AreEqual(timecard.PeriodStartDate, createHistoryRequest.TcdhPeriodStartDate);
                Assert.AreEqual(timecard.PositionId, createHistoryRequest.TcdhPositionId);
                Assert.AreEqual(timecard.StartDate, createHistoryRequest.TcdhStartDate);
                Assert.AreEqual(timecard.TimeEntries.Count(), createHistoryRequest.CreateTimeEntryHistories.Count());

                Assert.AreEqual(status.ActionType.ToString(), createHistoryRequest.TcdhStatusActionType);
                Assert.AreEqual(status.ActionerId, createHistoryRequest.TcdhStatusActionerId);

                Assert.AreEqual(comments.Count(), createHistoryRequest.CreateTimecardComments.Count());
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NullTimecardErrorTest()
            {

                await historiesRepository.CreateTimecardHistoryAsync(null, await getInputTimecardStatus(), await getInputTimeEntryComments());

            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NullTimecardStatusErrorTest()
            {
                await historiesRepository.CreateTimecardHistory2Async(await getInputTimecard2(), null, await getInputTimeEntryComments());
            }

            [TestMethod]
            public async Task NullTimeEntryCommentsLogsMessageTest()
            {
                await historiesRepository.CreateTimecardHistory2Async(await getInputTimecard2(), await getInputTimecardStatus(), null);
                loggerMock.Verify(l => l.Info("timeEntryComments is null in history repository. no comments will be saved for this history record"));
            }

            [TestMethod]
            public async Task CreatedStatusReturnsIdTest()
            {
                var newHistoryId = await createHistory2();
                Assert.AreEqual(historyId, newHistoryId);
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NullCreateResponseErrorTest()
            {
                try
                {
                    transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardHistoryRequest, CreateTimecardHistoryResponse>(It.IsAny<Ellucian.Colleague.Data.TimeManagement.Transactions.CreateTimecardHistoryRequest>()))
                        .ReturnsAsync(null);
                    await createHistory2();

                }
                catch (ApplicationException ae)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ae;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NoHistoryIdReturnedErrorTest()
            {
                try
                {
                    transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardHistoryRequest, CreateTimecardHistoryResponse>(It.IsAny<Ellucian.Colleague.Data.TimeManagement.Transactions.CreateTimecardHistoryRequest>()))
                        .Returns<CreateTimecardHistoryRequest>((req) => Task.FromResult(new CreateTimecardHistoryResponse() { ErrorMessage = "", NewTimecardHistoryId = null }));
                    await createHistory2();
                }
                catch (ApplicationException ae)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ae;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task GenericErrorTest()
            {

                transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardHistoryRequest, CreateTimecardHistoryResponse>(It.IsAny<Ellucian.Colleague.Data.TimeManagement.Transactions.CreateTimecardHistoryRequest>()))
                    .Throws(new Exception("something"));
                await createHistory2();

            }

        }

        [TestClass]
        public class GetTimecardHistories2Tests : TimecardHistoriesRepositoryTests
        {
            [TestInitialize]
            public void Initialize()
            {
                getQueryStartDate = new DateTime(2016, 09, 01);
                getQueryEndDate = new DateTime(2016, 09, 05);
                base.TimecardHistoryTestsInitialize();
                return;
            }

            [TestMethod]
            public async Task ExpectedTimecardHistoriesAreReturned()
            {
                var start = getQueryStartDate;
                var end = getQueryEndDate;
                var employeeIds = new List<string>() { "24601" };
                var expected = (await testData.GetTimecardHistoriesAsync(start, end, employeeIds)).OrderBy(t => t.Id);
                var actual = (await historiesRepository.GetTimecardHistoriesAsync(start, end, employeeIds)).OrderBy(t => t.Id);
                var props = 0;
                var props2 = 0;
                try
                {
                    for (var i = 0; i < expected.Count(); i++)
                    {
                        foreach (var prop in expected.ElementAt(i).GetType().GetProperties())
                        {
                            if (prop.GetType() != typeof(List<TimeEntryHistory>))
                            {
                                props++;
                                Assert.AreEqual(prop.GetValue(expected.ElementAt(i)), prop.GetValue(actual.ElementAt(i)));
                            }
                            else if (1 == Math.Log10(99))
                            {
                                for (var j = 0; j < expected.ElementAt(i).TimeEntryHistories.Count; j++)
                                {
                                    foreach (var prop2 in expected.ElementAt(i).TimeEntryHistories[j].GetType().GetProperties())
                                    {
                                        props2++;

                                        Assert.AreEqual(
                                            prop2.GetValue(expected.ElementAt(i).TimeEntryHistories[j]),
                                            prop2.GetValue(actual.ElementAt(i).TimeEntryHistories[j])
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    logger.Error(e.Message);
                    logger.Error(props.ToString());
                    logger.Error(props2.ToString());
                }
            }

            //  exception tests...
        }
        #endregion
    }
}
