﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.TimeManagement.DataContracts;
using Ellucian.Colleague.Data.TimeManagement.Repositories;
using Ellucian.Colleague.Data.TimeManagement.Transactions;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.TimeManagement;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Tests;
using Ellucian.Web.Http.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.TimeManagement.Tests.Repositories
{
    [TestClass]
    public class TimeEntryCommentsRepositoryTests : BaseRepositorySetup
    {
        public TestTimeEntryCommentsRepository testData;
        public TimeEntryCommentsRepository commentsRepository;
          public TimeEntryComment commentsToCreate;
        public CreateTimecardCommentsRequest createCommentsRequest;
        public CreateTimecardCommentsResponse createCommentsResponse;
        public List<string> employeeIds;

        public TimeEntryCommentsRepository BuildRepository()
        {
            // select moq 
               dataReaderMock.Setup(r => r.SelectAsync("TIMECARD.COMMENTS", "WITH TCC.EMPLOYEE.ID EQ ?", It.IsAny<string[]>(), "?", true, 425))
                .Returns<string, string, string[], string, bool, int>((f, q, empIds, d, b, n) =>
                    Task.FromResult(
                    testData.timecardCommentRecords
                    .Where(c => empIds.Select(id => id.Replace("\"", "").Replace("\\", "")).Contains(c.employeeID))
                    .Select(r => r.id).ToArray()
                    )
                );
            // read moq
            dataReaderMock.Setup(d => d.BulkReadRecordAsync<DataContracts.TimecardComments>(It.IsAny<string[]>(), true))
                .Returns<string[], bool>((keys, b) => Task.FromResult(new Collection<DataContracts.TimecardComments>(
                    testData.timecardCommentRecords
                    .Where(tc => keys.Contains(tc.id))
                    .Select(record => new DataContracts.TimecardComments()
                    {
                        Recordkey = record.id,
                        TccComments = record.comments,
                        TccEmployeeId = record.employeeID,
                        TccPositionId = record.positionId,
                        TccPaycycleId = record.payCycleId,
                        TccPayPeriodEndDate = record.payPeriodEndDate,
                        TccTimecardId = record.timecardId,
                        TimecardCommentsAdddate = record.addDate,
                        TimecardCommentsAddopr = record.addopr,
                        TimecardCommentsAddtime = record.addTime,
                        TimecardCommentsChgdate = record.chgDate,
                        TimecardCommentsChgopr = record.chgOpr,
                        TimecardCommentsChgtime = record.chgTime
                    })
                    .ToList())));
            // create moq
            transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardCommentsRequest, CreateTimecardCommentsResponse>(It.IsAny<CreateTimecardCommentsRequest>()))
                .Callback<CreateTimecardCommentsRequest>((req) =>
                {
                    createCommentsRequest = req;
                })
                .Returns<CreateTimecardCommentsRequest>((req) => Task.FromResult(
                    new CreateTimecardCommentsResponse()
                    {
                        NewKey = testData.AddCommentRecord(new TestTimeEntryCommentsRepository.TimecardCommentRecord()
                        {
                            comments = req.Comments.Aggregate((total, c) => total + " " + c),
                            employeeID = req.EmployeeId,
                            payCycleId = req.PaycycleId,
                            payPeriodEndDate = req.PayPeriodEndDate,
                            positionId = req.PositionId,
                            timecardId = req.TimecardId,
                        })
                    }));

            dataReaderMock.Setup(r => r.ReadRecordAsync<DataContracts.TimecardComments>(It.IsAny<string>(), It.IsAny<bool>()))
                .Returns<string, bool>((key, b) => Task.FromResult(
                    testData.timecardCommentRecords.Where(r => r.id == key).Select(record =>
                    new DataContracts.TimecardComments()
                    {
                        Recordkey = record.id,
                        TccComments = record.comments,
                        TccEmployeeId = record.employeeID,
                        TccPositionId = record.positionId,
                        TccPaycycleId = record.payCycleId,
                        TccPayPeriodEndDate = record.payPeriodEndDate,
                        TccTimecardId = record.timecardId,
                        TimecardCommentsAdddate = record.addDate,
                        TimecardCommentsAddopr = record.addopr,
                        TimecardCommentsAddtime = record.addTime,
                        TimecardCommentsChgdate = record.chgDate,
                        TimecardCommentsChgopr = record.chgOpr,
                        TimecardCommentsChgtime = record.chgTime

                    }).FirstOrDefault()));


            loggerMock.Setup(l => l.IsErrorEnabled).Returns(true);

            return new TimeEntryCommentsRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettings);
        }

        public void BaseTestInitialize()
        {
            MockInitialize();
            employeeIds = new List<string>() { "24601" };
               commentsToCreate = new TimeEntryComment("", "0003914", "ZPOS", "BW", new DateTime(2016, 8, 26), "This is the comment");
            commentsRepository = BuildRepository();
            testData = new TestTimeEntryCommentsRepository();
        }

        [TestClass]
        public class GetTests : TimeEntryCommentsRepositoryTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.BaseTestInitialize();
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NullEmployeeIdsArgumentTest()
            {
                employeeIds = null;
                await commentsRepository.GetCommentsAsync(employeeIds);
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NullCommentKeysReturnedErrorTest()
            {
                    dataReaderMock.Setup(r => r.SelectAsync("TIMECARD.COMMENTS", "WITH TCC.EMPLOYEE.ID EQ ?", It.IsAny<string[]>(), "?", true, 425))
                    .ReturnsAsync(null);
                try { await commentsRepository.GetCommentsAsync(employeeIds); }
                catch (ApplicationException) { loggerMock.Verify(e => e.Error(It.IsAny<string>())); throw; }
            }

            [TestMethod]
            public async Task NoCommentKeysReturnedLogTest()
            {
                    dataReaderMock.Setup(r => r.SelectAsync("TIMECARD.COMMENTS", "WITH TCC.EMPLOYEE.ID EQ ?", It.IsAny<string[]>(), "?", true, 425))
                    .ReturnsAsync(new string[] { });
                await commentsRepository.GetCommentsAsync(employeeIds);
                loggerMock.Verify(i => i.Info(It.IsAny<string>()));
            }

            [TestMethod]
            public async Task NullRecordsReturnedLogTest()
            {
                dataReaderMock.Setup(d => d.BulkReadRecordAsync<DataContracts.TimecardComments>(It.IsAny<string[]>(), true))
                    .ReturnsAsync(null);
                await commentsRepository.GetCommentsAsync(employeeIds);
                loggerMock.Verify(i => i.Error(It.IsAny<string>()));
            }

            [TestMethod]
            public async Task NoRecordKeyWhenBuildingEntityErrorTest()
            {
                testData.timecardCommentRecords.ForEach(com => com.id = null);

                var emptyActual = await commentsRepository.GetCommentsAsync(employeeIds);
                Assert.IsFalse(emptyActual.Any());

            }

            [TestMethod]
            public async Task ExpectedTimecardIsReturnedTest()
            {
                var expected = testData.timecardCommentRecords.Where(db => employeeIds.Contains(db.employeeID)).Select(db => testData.BuildCommentsEntity(db)).ToList();
                var actual = (await commentsRepository.GetCommentsAsync(employeeIds)).ToList();

                Assert.AreEqual(expected.Count, actual.Count);

                for (var i = 0; i < expected.Count; i++)
                {
                    Assert.AreEqual(expected[i].Id, actual[i].Id);
                    Assert.AreEqual(expected[i].EmployeeId, actual[i].EmployeeId);
                    Assert.AreEqual(expected[i].TimecardId, actual[i].TimecardId);
                    Assert.AreEqual(expected[i].PayCycleId, actual[i].PayCycleId);
                    Assert.AreEqual(expected[i].PayPeriodEndDate, actual[i].PayPeriodEndDate);
                    Assert.AreEqual(expected[i].Comments, actual[i].Comments);
                }
            }
        }

        [TestClass]
        public class CreateTests : TimeEntryCommentsRepositoryTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.BaseTestInitialize();
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NullTimeEntryCommentsArgumentTest()
            {
                commentsToCreate = null;
                await commentsRepository.CreateCommentsAsync(commentsToCreate);
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NoRecordKeyReturnedErrorTest()
            {
                transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardCommentsRequest, CreateTimecardCommentsResponse>(It.IsAny<CreateTimecardCommentsRequest>()))
                    .Callback<CreateTimecardCommentsRequest>((req) => { createCommentsRequest = req; })
                    .Returns<CreateTimecardCommentsRequest>((req) => Task.FromResult(new CreateTimecardCommentsResponse() { NewKey = null }));
                try
                {
                    await commentsRepository.CreateCommentsAsync(commentsToCreate);
                }
                catch (ApplicationException)
                {
                    loggerMock.Verify(e => e.Error(It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NoCommentsReturnedFromRecordKeyErrorTest()
            {
                dataReaderMock.Setup(r => r.ReadRecordAsync<DataContracts.TimecardComments>(It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(null);

                try
                {
                    await commentsRepository.CreateCommentsAsync(commentsToCreate);
                }
                catch (ApplicationException)
                {
                    loggerMock.Verify(e => e.Error(It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            public async Task ExpectedCommentsAreCreatedTest()
            {

                var expected = commentsToCreate;
                var actual = await commentsRepository.CreateCommentsAsync(commentsToCreate);


                Assert.AreEqual(expected.Comments, actual.Comments);
                Assert.AreEqual(expected.EmployeeId, actual.EmployeeId);
                Assert.AreEqual(expected.PayCycleId, actual.PayCycleId);
                Assert.AreEqual(expected.PayPeriodEndDate, actual.PayPeriodEndDate);
                Assert.AreEqual(expected.TimecardId, actual.TimecardId);
                Assert.AreEqual(expected.PositionId, actual.PositionId);
            }
        }
    }
}
