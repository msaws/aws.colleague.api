﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Domain.TimeManagement.Tests;
using Ellucian.Colleague.Data.TimeManagement.Repositories;
using System.Threading.Tasks;
using System.Linq;
using Moq;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using Ellucian.Colleague.Domain.TimeManagement.Entities;

namespace Ellucian.Colleague.Data.TimeManagement.Tests.Repositories
{
    [TestClass]
    public class OvertimeCalculationDefinitionsRepositoryTests : BaseRepositorySetup
    {
        #region fields'n'props

        public TestOvertimeCalculationDefinitionsRepository testData;
        public OvertimeCalculationDefinitionsRepository testRepository;

        #endregion

        private OvertimeCalculationDefinitionsRepository BuildRepository()
        {

            dataReaderMock.Setup(datamock => datamock.BulkReadRecordAsync<Data.TimeManagement.DataContracts.OtCalcDefinition>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()))
                            .Returns<string, string, bool>((table, s, b) =>
                                Task.FromResult(
                                new Collection<Data.TimeManagement.DataContracts.OtCalcDefinition>(
                                    testData.OtCalcDefinitionRecords.Select(r => new DataContracts.OtCalcDefinition()
                                    {
                                        OtCalculationMethod = r.OtCalculationMethod,
                                        OtDescription = r.OtDescription,
                                        Recordkey = r.Recordkey
                                    }).ToList())));

            dataReaderMock.Setup(datamock => datamock.BulkReadRecordAsync<Data.TimeManagement.DataContracts.OtcdWeeklyParms>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()))
                            .Returns<string, string, bool>((table, s, b) =>
                                Task.FromResult(new Collection<DataContracts.OtcdWeeklyParms>(testData.OtcdWeeklyParmsRecords.Select(r => new DataContracts.OtcdWeeklyParms()
                                {
                                    OtcdWeeklyAltEarntype = r.OtcdWeeklyAltEarntype,
                                    Recordkey = r.Recordkey,
                                    OtcdWeeklyEarntype = r.OtcdWeeklyEarntype,
                                    OtcdWeeklyHours = r.OtcdWeeklyHours,
                                    OtcdWeeklyOtCalcDefId = r.OtcdWeeklyOtCalcDefId
                                }).ToList())));

            dataReaderMock.Setup(datamock => datamock.BulkReadRecordAsync<Data.TimeManagement.DataContracts.OtcdDailyParms>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()))
                            .Returns<string, string, bool>((table, s, b) =>
                                Task.FromResult(
                                new Collection<Data.TimeManagement.DataContracts.OtcdDailyParms>(
                                    testData.OtcdDailyParmsRecords.Select(r => new DataContracts.OtcdDailyParms()
                                    {
                                        Recordkey = r.Recordkey,
                                        OtcdDailyAltEarntype = r.OtcdDailyAltEarntype,
                                        OtcdDailyEarntype = r.OtcdDailyEarntype,
                                        OtcdDailyHours = r.OtcdDailyHours,
                                        OtcdDailyOtCalcDefId = r.OtcdDailyOtCalcDefId
                                    }).ToList())));

            dataReaderMock.Setup(datamock => datamock.BulkReadRecordAsync<Data.TimeManagement.DataContracts.OtcdInclEarntypes>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()))
                            .Returns<string, string, bool>((table, s, b) =>
                                Task.FromResult(
                                new Collection<Data.TimeManagement.DataContracts.OtcdInclEarntypes>(
                                    testData.OtcdInclEarntypesRecords.Select(r => new DataContracts.OtcdInclEarntypes()
                                    {
                                        Recordkey = r.Recordkey,
                                        OtcdInclEarntype = r.OtcdInclEarntype,
                                        OtcdOvertimeCalcDefId = r.OtcdOvertimeCalcDefId,
                                        OtcdUseAltOtEarntype = r.OtcdUseAltOtEarntype
                                    }).ToList())));

            dataReaderMock.Setup(datamock => datamock.BulkReadRecordAsync<Data.TimeManagement.DataContracts.Earntype>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()))
                            .Returns<string, string, bool>((table, s, b) =>
                                Task.FromResult(
                                new Collection<Data.TimeManagement.DataContracts.Earntype>(
                                    testData.Earntypes.Select(r => new DataContracts.Earntype()
                                    {
                                        Recordkey = r.Recordkey,
                                        EtpOtFactor = r.EtpOtFactor
                                    }).ToList())));

            loggerMock.Setup(l => l.IsErrorEnabled).Returns(true);

            return new OvertimeCalculationDefinitionsRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettings);
        }

        #region setup
        public void OvertimeCalculationDefinitionsRepositoryTestsInitialize()
        {
            MockInitialize();
            testData = new TestOvertimeCalculationDefinitionsRepository();
            testRepository = BuildRepository();
        }
        public async Task<IEnumerable<Domain.TimeManagement.Entities.OvertimeCalculationDefinition>> getExpectedOvertimeCalculationDefinitions()
        {
            return await testData.GetOvertimeCalculationDefinitionsAsync();
        }

        public async Task<IEnumerable<Domain.TimeManagement.Entities.OvertimeCalculationDefinition>> getActualOvertimeCalculationDefinitions()
        {
            return await testRepository.GetOvertimeCalculationDefinitionsAsync();
        }
        #endregion

        #region get tests
        [TestClass] // all of em
        public class GetOvertimeCalculationDefinitionTests : OvertimeCalculationDefinitionsRepositoryTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.OvertimeCalculationDefinitionsRepositoryTestsInitialize();
            }
            [TestCleanup]
            public void CleanUp()
            {
                base.testData = null;
                base.testRepository = null;
            }

            //Remember to ask matt how to properly compare the OTCDs -- NRV 6/12/16
            [TestMethod]
            public async Task ExpectedOvertimeCalculationDefinitionsEqualActualTest()
            {
                var expected = await base.getExpectedOvertimeCalculationDefinitions();
                var actual = await base.getActualOvertimeCalculationDefinitions();

                Assert.AreEqual(expected.Count(), actual.Count());
            }

        }
        #endregion

        //private bool OvertimeCalculationDefinitionComparer(OvertimeCalculationDefinition a, OvertimeCalculationDefinition b)
        //{
        //     var idBool = a.Id == b.Id &&
        //      a.Description == b.Description;
        //     var weeklyBool = false;
        //     for (var x = 0; x < a.WeeklyHoursThreshold.Count(); x++)
        //     {
        //          if(a.WeeklyHoursThreshold[x].Threshold == b.WeeklyHoursThreshold[x].Threshold &&
        //               a.WeeklyHoursThreshold[x].DefaultEarningsTypeId == ){

        //          }
        //     }

        //}

    }
}
