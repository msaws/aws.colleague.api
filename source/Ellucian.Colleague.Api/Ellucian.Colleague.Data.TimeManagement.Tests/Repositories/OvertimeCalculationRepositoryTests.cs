﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.TimeManagement.Repositories;
using Ellucian.Colleague.Data.TimeManagement.Transactions;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.TimeManagement.Tests.Repositories
{
    [TestClass]
    public class OvertimeCalculationRepositoryTests : BaseRepositorySetup
    {
        public TestOvertimeCalculationRepository overtimeCalculationTestData;

        public OvertimeCalculationRepository repositoryUnderTest;

        public CalculateOvertimeRequest actualCalculateOvertimeRequestCtx;

        public void OvertimeCalculationRepositoryTestsInitialize()
        {
            MockInitialize();
            overtimeCalculationTestData = new TestOvertimeCalculationRepository();
            repositoryUnderTest = BuildRepository();
        }

        public OvertimeCalculationRepository BuildRepository()
        {
            transManagerMock.Setup(t => t.ExecuteAsync<CalculateOvertimeRequest, CalculateOvertimeResponse>(It.IsAny<CalculateOvertimeRequest>()))
                .Callback<CalculateOvertimeRequest>((req) => actualCalculateOvertimeRequestCtx = req)
                .Returns<CalculateOvertimeRequest>((req) =>
                    Task.FromResult(new CalculateOvertimeResponse()
                    {
                        PersonId = req.PersonId,
                        StartDate = req.StartDate,
                        PaycycleId = req.PaycycleId,
                        EndDate = req.EndDate,
                        ErrorMessage = "",
                        Thresholds = overtimeCalculationTestData.testResponse.thresholds.Select(th =>
                            new Thresholds()
                            {
                                ThresholdEarningsTypeId = th.earningsTypeId,
                                ThresholdRateFactor = th.earningsFactor,
                                ThresholdTotalOvertimeMinutes = th.totalOvertimeMinutes
                            }).ToList(),
                        DayAllocations = overtimeCalculationTestData.testResponse.dailyAllocations.Select(da =>
                            new DayAllocations()
                            {
                                DayAllocationsDate = da.date,
                                DayAllocationsOvertimeMinutes = da.overtimeMinutes,
                                DayAllocationsThresholdPosition = da.thresholdPosition
                            }).ToList()
                    }));

            return new OvertimeCalculationRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);
        }

        [TestClass]
        public class CalculateOvertimeTests : OvertimeCalculationRepositoryTests
        {
            public string personId;
            public string payCycleId;
            public DateTime startDate;
            public DateTime endDate;

            public async Task<OvertimeCalculationResult> getExpected()
            {
                return await overtimeCalculationTestData.CalculateOvertime(personId, startDate, endDate, payCycleId);
            }

            public async Task<OvertimeCalculationResult> getActual()
            {
                return await repositoryUnderTest.CalculateOvertime(personId, startDate, endDate, payCycleId);
            }

            [TestInitialize]
            public void Initialize()
            {
                OvertimeCalculationRepositoryTestsInitialize();
                personId = "0003914";
                payCycleId = "BW";
                startDate = new DateTime(2016, 6, 4);
                endDate = new DateTime(2016, 6, 10);
            }

            [TestMethod]
            public async Task ExpectedEqualsActual()
            {
                var expected = await getExpected();
                var actual = await getActual();
                Assert.AreEqual(expected, actual);

                Assert.AreEqual(expected.Thresholds.Count, actual.Thresholds.Count);
                Assert.AreEqual(expected.Thresholds.SelectMany(t => t.DailyAllocations).Count(), actual.Thresholds.SelectMany(t => t.DailyAllocations).Count());

            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task PersonIdRequiredTest()
            {
                personId = null;
                await getActual();
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task PayCycleIdRequiredTest()
            {
                payCycleId = "";
                await getActual();
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task StartDateAfterEndDateTest()
            {
                startDate = endDate.AddDays(1);
                await getActual();
            }

            [TestMethod]
            public async Task CtxRequestTest()
            {
                await getActual();
                Assert.AreEqual(personId, actualCalculateOvertimeRequestCtx.PersonId);
                Assert.AreEqual(startDate, actualCalculateOvertimeRequestCtx.StartDate);
                Assert.AreEqual(endDate, actualCalculateOvertimeRequestCtx.EndDate);
                Assert.AreEqual(payCycleId, actualCalculateOvertimeRequestCtx.PaycycleId);
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NullResponseFromCtxTest()
            {
                transManagerMock.Setup(t => t.ExecuteAsync<CalculateOvertimeRequest, CalculateOvertimeResponse>(It.IsAny<CalculateOvertimeRequest>()))
                   .Returns(Task.FromResult<CalculateOvertimeResponse>(null));

                await getActual();
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task ErrorMessageResponseFromCtxTest()
            {
                transManagerMock.Setup(t => t.ExecuteAsync<CalculateOvertimeRequest, CalculateOvertimeResponse>(It.IsAny<CalculateOvertimeRequest>()))
                   .Returns(Task.FromResult(new CalculateOvertimeResponse()
                   {
                        ErrorMessage = "foobar"
                   }));

                await getActual();
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task StartDateInResponseHasNoValueTest()
            {
                transManagerMock.Setup(t => t.ExecuteAsync<CalculateOvertimeRequest, CalculateOvertimeResponse>(It.IsAny<CalculateOvertimeRequest>()))
                   .Returns(Task.FromResult(new CalculateOvertimeResponse()
                   {
                       StartDate = null
                   }));

                await getActual();
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task EndDateInResponseHasNoValueTest()
            {
                transManagerMock.Setup(t => t.ExecuteAsync<CalculateOvertimeRequest, CalculateOvertimeResponse>(It.IsAny<CalculateOvertimeRequest>()))
                   .Returns(Task.FromResult(new CalculateOvertimeResponse()
                   {
                       StartDate = DateTime.Now,
                       EndDate = null
                   }));

                await getActual();
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task TotalOvertimeHasNoValueTest()
            {
                transManagerMock.Setup(t => t.ExecuteAsync<CalculateOvertimeRequest, CalculateOvertimeResponse>(It.IsAny<CalculateOvertimeRequest>()))
                   .Returns(Task.FromResult(new CalculateOvertimeResponse()
                   {
                       StartDate = DateTime.Now,
                       EndDate = DateTime.Now,
                       Thresholds = new List<Thresholds>() { new Thresholds() { } }
                   }));

                await getActual();
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task DayAllocationDateHasNoValueTest()
            {
                transManagerMock.Setup(t => t.ExecuteAsync<CalculateOvertimeRequest, CalculateOvertimeResponse>(It.IsAny<CalculateOvertimeRequest>()))
                   .Returns(Task.FromResult(new CalculateOvertimeResponse()
                   {
                       StartDate = DateTime.Now,
                       EndDate = DateTime.Now,
                       Thresholds = new List<Thresholds>() { new Thresholds() { ThresholdTotalOvertimeMinutes = 50 } },
                       DayAllocations = new List<DayAllocations>() { new DayAllocations() { } }
                   }));

                await getActual();
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task DayAllocationOvertimeHasNoValueTest()
            {
                transManagerMock.Setup(t => t.ExecuteAsync<CalculateOvertimeRequest, CalculateOvertimeResponse>(It.IsAny<CalculateOvertimeRequest>()))
                   .Returns(Task.FromResult(new CalculateOvertimeResponse()
                   {
                       StartDate = DateTime.Now,
                       EndDate = DateTime.Now,
                       Thresholds = new List<Thresholds>() { new Thresholds() { ThresholdTotalOvertimeMinutes = 50 } },
                       DayAllocations = new List<DayAllocations>() { new DayAllocations() { DayAllocationsDate = DateTime.Now } }
                   }));

                await getActual();
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task DayAllocationThresholdPositionHasNoValueTest()
            {
                transManagerMock.Setup(t => t.ExecuteAsync<CalculateOvertimeRequest, CalculateOvertimeResponse>(It.IsAny<CalculateOvertimeRequest>()))
                   .Returns(Task.FromResult(new CalculateOvertimeResponse()
                   {
                       StartDate = DateTime.Now,
                       EndDate = DateTime.Now,
                       Thresholds = new List<Thresholds>() { new Thresholds() { ThresholdTotalOvertimeMinutes = 50 } },
                       DayAllocations = new List<DayAllocations>() { new DayAllocations() { DayAllocationsDate = DateTime.Now, DayAllocationsOvertimeMinutes = 50 } }
                   }));

                await getActual();
            }
        }
    }
}
