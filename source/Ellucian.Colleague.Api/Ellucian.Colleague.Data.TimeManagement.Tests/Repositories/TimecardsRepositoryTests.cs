﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.TimeManagement.DataContracts;
using Ellucian.Colleague.Data.TimeManagement.Repositories;
using Ellucian.Colleague.Data.TimeManagement.Transactions;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.TimeManagement;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Tests;
using Ellucian.Web.Http.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.TimeManagement.Tests.Repositories
{
    [TestClass]
    public class TimecardsRepositoryTests : BaseRepositorySetup
    {

        public List<string> employeeIds;
        public TestTimecardsRepository testData;
        public TimecardsRepository repositoryUnderTest;


        public CreateTimecardRequest actualCreateTimecardRequest;
        public UpdateTimecardRequest actualUpdateTimecardRequest;

        private TimecardsRepository BuildRepository()
        {
            // Gets a lot of timecards
            dataReaderMock.Setup(d => d.SelectAsync("TIMECARD", "WITH TCD.EMPLOYEE.ID EQ ?", It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>()))
                .Returns<string, string, string[], string, bool, int>((f, c, values, p, r, s) =>
                        Task.FromResult((testData.timecardRecords == null) ? null :
                            testData.timecardRecords
                                .Where(rec => values.Select(v => v.Replace("\"", "").Replace("\\", "").Replace("\\", "")).Contains(rec.employeeId))
                                .Select(rec => rec.id).ToArray()
                        ));

            dataReaderMock.Setup(d => d.SelectAsync("TIME.ENTRY", "WITH TE.TIMECARD.ID EQ ?", It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>()))
                .Returns<string, string, string[], string, bool, int>((f, c, values, p, r, s) =>
                        Task.FromResult((testData.timeEntryRecords == null) ? null :
                            testData.timeEntryRecords
                                .Where(rec => values.Select(v => v.Replace("\"", "").Replace("\\", "")).Contains(rec.timecardId))
                                .Select(rec => rec.id).ToArray()
                        ));

            dataReaderMock.Setup(datamock => datamock.BulkReadRecordAsync<DataContracts.Timecard>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .Returns<string[], bool>((ids, x) =>
                    Task.FromResult(
                    new Collection<Data.TimeManagement.DataContracts.Timecard>(
                        testData.timecardRecords
                            .Where(tc => ids.Contains(tc.id))
                            .Select(tc => new DataContracts.Timecard()
                                {
                                    Recordkey = tc.id,
                                    TcdEmployeeId = tc.employeeId,
                                    TcdEndDate = tc.endDate,
                                    TcdPaycycleId = tc.payCycleId,
                                    TcdPeriodEndDate = tc.periodEndDate,
                                    TcdPeriodStartDate = tc.periodStartDate,
                                    TcdPositionId = tc.positionId,
                                    TcdStartDate = tc.startDate,
                                    TimecardAdddate = tc.addDate,
                                    TimecardAddopr = tc.addOpr,
                                    TimecardAddtime = tc.addTime,
                                    TimecardChgdate = tc.chgDate,
                                    TimecardChgopr = tc.chgOpr,
                                    TimecardChgtime = tc.chgTime
                                }).ToList())));

            dataReaderMock.Setup(d => d.BulkReadRecordAsync<DataContracts.TimeEntry>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .Returns<string[], bool>((ids, x) =>
                    Task.FromResult(
                        new Collection<DataContracts.TimeEntry>(
                            testData.timeEntryRecords
                                .Where(te => ids.Contains(te.id))
                                .Select(te => BuildTimeEntryDataContract(te)).ToList())));

            // Gets a single timecard
            dataReaderMock.Setup(d => d.ReadRecordAsync<DataContracts.Timecard>(It.IsAny<string>(), It.IsAny<bool>()))
                .Returns<string, bool>((id, b) =>
                    Task.FromResult(testData.timecardRecords
                        .Where(tc => tc.id == id)
                        .Select(tc => new DataContracts.Timecard()
                                {
                                    Recordkey = tc.id,
                                    TcdEmployeeId = tc.employeeId,
                                    TcdEndDate = tc.endDate,
                                    TcdPaycycleId = tc.payCycleId,
                                    TcdPeriodEndDate = tc.periodEndDate,
                                    TcdPeriodStartDate = tc.periodStartDate,
                                    TcdPositionId = tc.positionId,
                                    TcdStartDate = tc.startDate,
                                    TimecardAdddate = tc.addDate,
                                    TimecardAddopr = tc.addOpr,
                                    TimecardAddtime = tc.addTime,
                                    TimecardChgdate = tc.chgDate,
                                    TimecardChgopr = tc.chgOpr,
                                    TimecardChgtime = tc.chgTime
                                }).FirstOrDefault())
                        );

            dataReaderMock.Setup(d => d.BulkReadRecordAsync<DataContracts.TimeEntry>(It.IsAny<string>(), It.IsAny<bool>()))
                .Returns<string, bool>((query, b) =>
                    Task.FromResult(new Collection<DataContracts.TimeEntry>(testData.timeEntryRecords
                        .Where(te => query.Contains(te.timecardId))
                        .Select(te => BuildTimeEntryDataContract(te)).ToList())));

            // creates a timecard
            transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardRequest, CreateTimecardResponse>(It.IsAny<CreateTimecardRequest>()))
                .Callback<CreateTimecardRequest>((req) =>
                {
                    actualCreateTimecardRequest = req;
                })
                .Returns<CreateTimecardRequest>((req) =>
                    Task.FromResult(new CreateTimecardResponse()
                        {
                            ErrorMessage = "",
                            OutTimecardId = testData.CreateTimecardRecordHelper(req.TcdEmployeeId, req.TcdPayCycleId, req.TcdPeriodStartDate, req.TcdPeriodEndDate, req.TcdStartDate, req.TcdEndDate, req.TcdPositionId, "")
                        }));

            // updates a timecard
            transManagerMock.Setup(t => t.ExecuteAsync<UpdateTimecardRequest, UpdateTimecardResponse>(It.IsAny<UpdateTimecardRequest>()))
                .Callback<UpdateTimecardRequest>((req) =>
                {
                    actualUpdateTimecardRequest = req;
                })
                .Returns<UpdateTimecardRequest>((req) => Task.FromResult(new UpdateTimecardResponse() { ErrorMessage = "" }));

            loggerMock.Setup(l => l.IsErrorEnabled).Returns(true);

            return new TimecardsRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettings);
        }



        public void TimecardsRepositoryTestsInitialize()
        {
            MockInitialize();
            employeeIds = new List<string>() { "24601", "0003914" };
            testData = new TestTimecardsRepository();
            repositoryUnderTest = BuildRepository();
        }
        //public async Task<IEnumerable<Domain.TimeManagement.Entities.Timecard>> expected()
        //{
        //    return await testData.GetTimecardsAsync(employeeIds);
        //}

        //public async Task<IEnumerable<Domain.TimeManagement.Entities.Timecard>> getActualTimecards(List<string> employeeIds)
        //{
        //    return await testRepository.GetTimecardsAsync(employeeIds);
        //}


        #region helpers
        public bool TimecardDataContractComparer(TimeManagement.DataContracts.Timecard dataTcd, Domain.TimeManagement.Entities.Timecard domainTcd)
        {
            if
                (
                dataTcd.Recordkey == domainTcd.Id &&
                dataTcd.TcdEmployeeId == domainTcd.EmployeeId &&
                dataTcd.TcdPaycycleId == domainTcd.PayCycleId &&
                dataTcd.TcdPeriodStartDate == domainTcd.PeriodStartDate &&
                dataTcd.TcdPeriodEndDate == domainTcd.PeriodEndDate &&
                dataTcd.TcdPositionId == domainTcd.PositionId &&
                dataTcd.TimecardAddopr == domainTcd.Timestamp.AddOperator &&
                dataTcd.TimecardChgopr == domainTcd.Timestamp.ChangeOperator
                )
                return true;
            else
                return false;
        }

        public bool CompareUpdateRequest(UpdateTimecardRequest req, Domain.TimeManagement.Entities.Timecard tc)
        {
            if
                (
                req.TimecardId == tc.Id &&
                req.TcdEmployeeId == tc.EmployeeId &&
                req.TcdPayCycleId == tc.PayCycleId &&
                req.TcdPeriodStartDate == tc.PeriodStartDate &&
                req.TcdPeriodEndDate == tc.PeriodEndDate &&
                req.TcdPositionId == tc.PositionId
                )
                return true;
            else
                return false;
        }

        public DataContracts.TimeEntry BuildTimeEntryDataContract(TestTimecardsRepository.TimeEntryRecord timeEntryData)
        {
            return new DataContracts.TimeEntry()
            {
                Recordkey = timeEntryData.id,
                TeEarntypeId = timeEntryData.earnTypeId,
                TeInDate = timeEntryData.inDate,
                TeInTime = timeEntryData.inTime,
                TeOutDate = timeEntryData.outDate,
                TeOutTime = timeEntryData.outTime,
                TePerleaveId = timeEntryData.personLeaveId,
                TeProjectId = timeEntryData.projectId,
                TeTimecardId = timeEntryData.timecardId,
                TeWorkedDate = timeEntryData.workedDate,
                TeWorkedMinutes = timeEntryData.workedMinutes,
                TimeEntryAdddate = timeEntryData.addDate,
                TimeEntryAddopr = timeEntryData.addOpr,
                TimeEntryAddtime = timeEntryData.addTime,
                TimeEntryChgdate = timeEntryData.chgDate,
                TimeEntryChgopr = timeEntryData.chgOpr,
                TimeEntryChgtime = timeEntryData.chgTime
            };
        }

        public bool TimecardEqualityComparer(Domain.TimeManagement.Entities.Timecard a, Domain.TimeManagement.Entities.Timecard b)
        {
            if (!a.CompareTimecard(b))
                return false;

            var at = a.TimeEntries;
            var bt = b.TimeEntries;
            for (var i = 0; i < at.Count; i++)
            {
                if (!at[i].CompareTimeEntry(bt[i]))
                    return false;
            }

            return true;
        }

        public bool Timecard2EqualityComparer(Domain.TimeManagement.Entities.Timecard2 a, Domain.TimeManagement.Entities.Timecard2 b)
        {
            if (!a.CompareTimecard(b))
                return false;

            var at = a.TimeEntries;
            var bt = b.TimeEntries;
            for (var i = 0; i < at.Count; i++)
            {
                if (!at[i].CompareTimeEntry(bt[i]))
                    return false;
            }

            return true;
        }
        #endregion

        #region timecard v1
        #region get tests
        [TestClass] // all of em
        public class GetTimecardsTests : TimecardsRepositoryTests
        {
            public async Task<IEnumerable<Domain.TimeManagement.Entities.Timecard>> getExpected()
            {
                return await testData.GetTimecardsAsync(employeeIds);
            }
            public async Task<IEnumerable<Domain.TimeManagement.Entities.Timecard>> getActual()
            {
                return await repositoryUnderTest.GetTimecardsAsync(employeeIds);
            }

            [TestInitialize]
            public void Initialize()
            {
                base.TimecardsRepositoryTestsInitialize();
            }
            [TestCleanup]
            public void CleanUp()
            {
                base.testData = null;
                base.repositoryUnderTest = null;
            }

            [TestMethod]
            public async Task ExpectedTimecardsEqualActualTest()
            {
                var expected = await getExpected();
                var actual = await getActual();

                Assert.AreEqual(expected.Count(), actual.Count());
                for (int i = 0; i < expected.Count(); i++)
                {
                    var e = expected.ElementAt(i);
                    var a = actual.ElementAt(i);
                    Assert.IsTrue(this.TimecardEqualityComparer(e, a));
                }

            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NullEmployeeIdsLoggedAndCaughtTest()
            {
                try
                {
                    employeeIds = null;
                    await getActual();
                }
                catch (ArgumentNullException ex)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ex;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task NoEmployeeIdsLoggedAndCaughtTest()
            {
                try
                {
                    employeeIds = new List<string>() { string.Empty };
                    await getActual();
                }
                catch (ArgumentNullException ex)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ex;
                }
            }

            [TestMethod]
            public async Task AbsentTimeEntryTimestampErrorAndLoggedTest()
            {

                testData.timeEntryRecords.ForEach(te => te.addDate = null);
                var actual = await getActual();
                Assert.IsTrue(actual.All(t => !t.TimeEntries.Any()));
                loggerMock.Verify(l => l.Error(It.IsAny<string>()));

            }

            [TestMethod]
            public async Task AbsentTimecardTimestampErrorAndLoggedTest()
            {

                testData.timecardRecords.ForEach(tc => tc.addTime = null);
                var actual = await getActual();
                Assert.IsFalse(actual.Any());
                loggerMock.Verify(l => l.Error(It.IsAny<string>()));

            }

            [TestMethod]
            public async Task AbsentTimecardPeriodDateErrorAndLoggedTest()
            {

                testData.timecardRecords.ForEach(tc => tc.periodStartDate = null);
                var actual = await getActual();
                Assert.IsFalse(actual.Any());
                loggerMock.Verify(l => l.Error(It.IsAny<string>()));

            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NullTimecardRecordKeysTest()
            {
                dataReaderMock.Setup(d => d.SelectAsync("TIMECARD", "WITH TCD.EMPLOYEE.ID EQ ?", It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>()))
                    .Returns<string, string, string[], string, bool, int>((f, c, values, p, r, s) =>
                            Task.FromResult<string[]>(
                                null
                            ));
                await getActual();
            }

            [TestMethod]
            public async Task NoTimecardRecordKeysTest()
            {
                dataReaderMock.Setup(d => d.SelectAsync("TIMECARD", "WITH TCD.EMPLOYEE.ID EQ ?", It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>()))
                    .Returns<string, string, string[], string, bool, int>((f, c, values, p, r, s) =>
                            Task.FromResult<string[]>(
                                new string[] { }
                            ));

                var actual = await getActual();
                Assert.IsFalse(actual.Any());
                loggerMock.Verify(l => l.Info(It.IsAny<string>()));

            }
            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NullTimeEntryRecordKeysTest()
            {
                dataReaderMock.Setup(d => d.SelectAsync("TIME.ENTRY", "WITH TE.TIMECARD.ID EQ ?", It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>()))
                    .Returns<string, string, string[], string, bool, int>((f, c, values, p, r, s) =>
                            Task.FromResult<string[]>(
                                null
                            ));
                await getActual();
            }

            [TestMethod]
            public async Task NoTimeEntryRecordKeysTest()
            {
                dataReaderMock.Setup(d => d.SelectAsync("TIME.ENTRY", "WITH TE.TIMECARD.ID EQ ?", It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>()))
                    .Returns<string, string, string[], string, bool, int>((f, c, values, p, r, s) =>
                            Task.FromResult<string[]>(
                                new string[] { }
                            ));
                var actual = await getActual();
                Assert.IsTrue(actual.All(tc => !tc.TimeEntries.Any()));

                loggerMock.Verify(l => l.Info(It.IsAny<string>()));
            }

        }

        [TestClass] // just one
        public class GetTimecardTests : TimecardsRepositoryTests
        {
            public string inputTimecardId;

            public async Task<Domain.TimeManagement.Entities.Timecard> getExpected()
            {
                return await testData.GetTimecardAsync(inputTimecardId);
            }
            public async Task<Domain.TimeManagement.Entities.Timecard> getActual()
            {
                return await repositoryUnderTest.GetTimecardAsync(inputTimecardId);
            }

            [TestInitialize]
            public void Initialize()
            {
                base.TimecardsRepositoryTestsInitialize();
                inputTimecardId = testData.timecardRecords.First().id;
            }
            [TestCleanup]
            public void CleanUp()
            {
                base.testData = null;
                base.repositoryUnderTest = null;
            }

            [TestMethod]
            public async Task CorrectTimecardIsReturnedTest()
            {
                var expected = await getExpected();
                var actual = await getActual();
                Assert.IsTrue(expected.CompareTimecard(actual));
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NoTimecardIdThrowsExceptionTest()
            {
                try
                {
                    inputTimecardId = string.Empty;
                    await getActual();
                }
                catch (ArgumentNullException ane)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ane;
                }
            }
            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task NullTimecardFromDbThrowsKnifeTest()
            {
                dataReaderMock.Setup(d => d.ReadRecordAsync<Ellucian.Colleague.Data.TimeManagement.DataContracts.Timecard>(It.IsAny<string>(), It.IsAny<bool>()))
                    .ReturnsAsync(null);
                try
                {
                    await getActual();
                }
                catch (KeyNotFoundException knife)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw knife;
                }
            }
            [TestMethod]
            public async Task NullTimeEntriesFromDbTest()
            {
                dataReaderMock.Setup(d => d.BulkReadRecordAsync<Ellucian.Colleague.Data.TimeManagement.DataContracts.TimeEntry>(It.IsAny<string>(), It.IsAny<bool>()))
                    .ReturnsAsync(null);
                var actual = await getActual();
                Assert.IsFalse(actual.TimeEntries.Any());
                loggerMock.Verify(l => l.Error(It.IsAny<string>()));

            }

            [TestMethod]
            public async Task SomeOtherErrorInTimeEntryThrowsExceptionTest()
            {
                testData.timeEntryRecords.ForEach(te => te.addDate = null);
                var actual = await getActual();
                Assert.IsFalse(actual.TimeEntries.Any());
                loggerMock.Verify(l => l.Error(It.IsAny<string>()));
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task StartDatesRequiredForTimecardTest()
            {
                testData.timecardRecords.ForEach(t => t.startDate = null);
                await getActual();
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task EndDatesRequiredForTimecardTest()
            {
                testData.timecardRecords.ForEach(t => t.endDate = null);
                try
                {
                    await getActual();
                }
                catch (ApplicationException)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            public async Task WorkedDateRequiredForAllTimeEntriesTest()
            {
                testData.timeEntryRecords.ForEach(t => t.workedDate = null);
                var actual = await getActual();
                Assert.IsFalse(actual.TimeEntries.Any());
                loggerMock.Verify(l => l.Error(It.IsAny<ApplicationException>(), It.IsAny<string>()));
            }

            [TestMethod]
            public async Task WorkedMinutesNotRequiredForTotalTimeTest()
            {
                testData.timeEntryRecords.ForEach(t =>
                    {
                        t.inDate = null;
                        t.outDate = null;
                        t.workedMinutes = null;
                    });

                var actual = await getActual();
                Assert.IsTrue(actual.TimeEntries.All(t => !t.WorkedTime.HasValue));
                 
            }
        }
        #endregion

        #region create tests
        [TestClass]
        public class CreateTimecardTests : TimecardsRepositoryTests
        {
            public Domain.TimeManagement.Entities.Timecard inputTimecard;

            public async Task<Domain.TimeManagement.Entities.Timecard> createActual()
            {
                return await repositoryUnderTest.CreateTimecardAsync(inputTimecard);
            }

            [TestInitialize]
            public void Initialize()
            {
                base.TimecardsRepositoryTestsInitialize();
                inputTimecard = new Domain.TimeManagement.Entities.Timecard("0003914", "M1", "POLYSCIPROF", new DateTime(2016, 6, 1), new DateTime(2016, 6, 30), new DateTime(2016, 6, 1), new DateTime(2016, 6, 7));
            }
            [TestCleanup]
            public void CleanUp()
            {
                base.testData = null;
                base.repositoryUnderTest = null;
            }

            [TestMethod]
            public async Task CreateTimecardReturnsCreatedTimecard()
            {
                var timecard = await createActual();
                Assert.IsTrue(timecard.CompareTimecard(inputTimecard));
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NoTimecardSuppliedThrowsExceptionTest()
            {
                try
                {
                    inputTimecard = null;
                    await createActual();

                }
                catch (ArgumentNullException ane)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ane;
                }
            }
            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NullCtxResponseThrowsExceptionTest()
            {
                try
                {
                    transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardRequest, CreateTimecardResponse>(It.IsAny<CreateTimecardRequest>()))
                        .Returns<CreateTimecardRequest>(req => Task.FromResult<CreateTimecardResponse>(null));

                    await createActual();
                }
                catch (ApplicationException ane)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ane;
                }
            }
            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task OtherCtxResponseThrowsExceptionTest()
            {
                try
                {
                    transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardRequest, CreateTimecardResponse>(It.IsAny<CreateTimecardRequest>()))
                        .Returns<CreateTimecardRequest>(req => Task.FromResult(new CreateTimecardResponse() { ErrorMessage = "This is an error" }));

                    await createActual();
                }
                catch (ApplicationException ane)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ane;
                }
            }
        }
        #endregion

        #region update tests
        [TestClass]
        public class UpdateTimecardTests : TimecardsRepositoryTests
        {
            private new bool CompareUpdateRequest(UpdateTimecardRequest req, Domain.TimeManagement.Entities.Timecard tc)
            {
                if
                    (
                    req.TimecardId == tc.Id &&
                    req.TcdEmployeeId == tc.EmployeeId &&
                    req.TcdPayCycleId == tc.PayCycleId &&
                    req.TcdPeriodStartDate == tc.PeriodStartDate &&
                    req.TcdPeriodEndDate == tc.PeriodEndDate
                    )
                    return true;
                else
                    return false;
            }

            public TimecardHelper inputTimecardHelper;

            [TestInitialize]
            public void Initialize()
            {
                base.TimecardsRepositoryTestsInitialize();
                inputTimecardHelper = new TimecardHelper(
                    new Domain.TimeManagement.Entities.Timecard("0003914", "M1", "POLYSCIPROF", new DateTime(2016, 6, 1), new DateTime(2016, 6, 30), new DateTime(2016, 6, 1), new DateTime(2016, 6, 7), testData.timecardRecords[0].id));

                var timeEntry1 = new Domain.TimeManagement.Entities.TimeEntry(inputTimecardHelper.Timecard.Id, "WORK", null, new DateTime(2016, 6, 1), "123");
                var timeEntry2 = new Domain.TimeManagement.Entities.TimeEntry(inputTimecardHelper.Timecard.Id, "COMP", null, new DateTime(2016, 6, 2), "321");
                var timeEntry3 = new Domain.TimeManagement.Entities.TimeEntry(inputTimecardHelper.Timecard.Id, "WORK", null, new DateTime(2016, 6, 1));
                inputTimecardHelper.Timecard.TimeEntries.Add(timeEntry1);
                inputTimecardHelper.Timecard.TimeEntries.Add(timeEntry2);
                inputTimecardHelper.TimeEntriesToCreate.Add(timeEntry3);
                inputTimecardHelper.TimeEntriesToDelete.Add(timeEntry2);
                inputTimecardHelper.TimeEntriesToUpdate.Add(timeEntry1);

            }
            [TestCleanup]
            public void CleanUp()
            {
                base.testData = null;
                base.repositoryUnderTest = null;
            }

            [TestMethod]
            public async Task RequestIsCorrectlyCreatedFromHelperTest()
            {
                await repositoryUnderTest.UpdateTimecardAsync(inputTimecardHelper);
                transManagerMock.Verify(t => t.ExecuteAsync<UpdateTimecardRequest, UpdateTimecardResponse>(It.Is<UpdateTimecardRequest>(r => this.CompareUpdateRequest(r, inputTimecardHelper.Timecard))));
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NoTimecardSuppliedThrowsExceptionTest()
            {
                try
                {
                    await repositoryUnderTest.UpdateTimecardAsync(null);
                }
                catch (ArgumentNullException ane)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ane;
                }
            }


            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NullCtxResponseThrowsExceptionTest()
            {
                try
                {
                    transManagerMock.Setup(t => t.ExecuteAsync<UpdateTimecardRequest, UpdateTimecardResponse>(It.IsAny<UpdateTimecardRequest>()))
                        .Returns<UpdateTimecardRequest>(req => Task.FromResult<UpdateTimecardResponse>(null));

                    await repositoryUnderTest.UpdateTimecardAsync(inputTimecardHelper);
                }
                catch (ApplicationException ane)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ane;
                }
            }
            [TestMethod]
            [ExpectedException(typeof(RecordLockException))]
            public async Task CtxConflictErrorMessageThrowsExceptionTest()
            {
                try
                {
                    transManagerMock.Setup(t => t.ExecuteAsync<UpdateTimecardRequest, UpdateTimecardResponse>(It.IsAny<UpdateTimecardRequest>()))

                        .Returns<UpdateTimecardRequest>(req => Task.FromResult(new UpdateTimecardResponse() { ErrorMessage = "CONFLICT: This is a conflict" }));
                    await repositoryUnderTest.UpdateTimecardAsync(inputTimecardHelper);
                }
                catch (ArgumentNullException ane)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ane;
                }
            }
            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task CtxOtherErrorMessageThrowsException()
            {
                try
                {
                    transManagerMock.Setup(t => t.ExecuteAsync<UpdateTimecardRequest, UpdateTimecardResponse>(It.IsAny<UpdateTimecardRequest>()))
                        .Returns<UpdateTimecardRequest>(req => Task.FromResult(new UpdateTimecardResponse() { ErrorMessage = "error" }));
                    await repositoryUnderTest.UpdateTimecardAsync(inputTimecardHelper);
                }
                catch (ArgumentNullException ane)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ane;
                }
            }
        }
        #endregion
        #endregion

        #region Timecard2 v2
        #region get tests
        [TestClass] // all of em
        public class GetTimecards2Tests : TimecardsRepositoryTests
        {
            public async Task<IEnumerable<Domain.TimeManagement.Entities.Timecard2>> getExpected()
            {
                return await testData.GetTimecards2Async(employeeIds);
            }
            public async Task<IEnumerable<Domain.TimeManagement.Entities.Timecard2>> getActual()
            {
                return await repositoryUnderTest.GetTimecards2Async(employeeIds);
            }

            [TestInitialize]
            public void Initialize()
            {
                base.TimecardsRepositoryTestsInitialize();
            }
            [TestCleanup]
            public void CleanUp()
            {
                base.testData = null;
                base.repositoryUnderTest = null;
            }

            [TestMethod]
            public async Task ExpectedTimecards2EqualActualTest()
            {
                var expected = await getExpected();
                var actual = await getActual();

                Assert.AreEqual(expected.Count(), actual.Count());
                for (int i = 0; i < expected.Count(); i++)
                {
                    var e = expected.ElementAt(i);
                    var a = actual.ElementAt(i);
                    Assert.IsTrue(this.Timecard2EqualityComparer(e, a));
                }

            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NullEmployeeIdsLoggedAndCaughtTest()
            {
                try
                {
                    employeeIds = null;
                    await getActual();
                }
                catch (ArgumentNullException ex)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ex;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task NoEmployeeIdsLoggedAndCaughtTest()
            {
                try
                {
                    employeeIds = new List<string>() { string.Empty };
                    await getActual();
                }
                catch (ArgumentNullException ex)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ex;
                }
            }

            [TestMethod]
            public async Task AbsentTimeEntry2TimestampErrorAndLoggedTest()
            {

                testData.timeEntryRecords.ForEach(te => te.addDate = null);
                var actual = await getActual();
                Assert.IsTrue(actual.All(t => !t.TimeEntries.Any()));
                loggerMock.Verify(l => l.Error(It.IsAny<string>()));

            }

            [TestMethod]
            public async Task AbsentTimecard2TimestampErrorAndLoggedTest()
            {

                testData.timecardRecords.ForEach(tc => tc.addTime = null);
                var actual = await getActual();
                Assert.IsFalse(actual.Any());
                loggerMock.Verify(l => l.Error(It.IsAny<string>()));

            }

            [TestMethod]
            public async Task AbsentTimecard2PeriodDateErrorAndLoggedTest()
            {

                testData.timecardRecords.ForEach(tc => tc.periodStartDate = null);
                var actual = await getActual();
                Assert.IsFalse(actual.Any());
                loggerMock.Verify(l => l.Error(It.IsAny<string>()));

            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NullTimecard2RecordKeysTest()
            {
                dataReaderMock.Setup(d => d.SelectAsync("TIMECARD", "WITH TCD.EMPLOYEE.ID EQ ?", It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>()))
                    .Returns<string, string, string[], string, bool, int>((f, c, values, p, r, s) =>
                            Task.FromResult<string[]>(
                                null
                            ));
                await getActual();
            }

            [TestMethod]
            public async Task NoTimecard2RecordKeysTest()
            {
                dataReaderMock.Setup(d => d.SelectAsync("TIMECARD", "WITH TCD.EMPLOYEE.ID EQ ?", It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>()))
                    .Returns<string, string, string[], string, bool, int>((f, c, values, p, r, s) =>
                            Task.FromResult<string[]>(
                                new string[] { }
                            ));

                var actual = await getActual();
                Assert.IsFalse(actual.Any());
                loggerMock.Verify(l => l.Info(It.IsAny<string>()));

            }
            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NullTimeEntry2RecordKeysTest()
            {
                dataReaderMock.Setup(d => d.SelectAsync("TIME.ENTRY", "WITH TE.TIMECARD.ID EQ ?", It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>()))
                    .Returns<string, string, string[], string, bool, int>((f, c, values, p, r, s) =>
                            Task.FromResult<string[]>(
                                null
                            ));
                await getActual();
            }

            [TestMethod]
            public async Task NoTimeEntry2RecordKeysTest()
            {
                dataReaderMock.Setup(d => d.SelectAsync("TIME.ENTRY", "WITH TE.TIMECARD.ID EQ ?", It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>()))
                    .Returns<string, string, string[], string, bool, int>((f, c, values, p, r, s) =>
                            Task.FromResult<string[]>(
                                new string[] { }
                            ));
                var actual = await getActual();
                Assert.IsTrue(actual.All(tc => !tc.TimeEntries.Any()));

                loggerMock.Verify(l => l.Info(It.IsAny<string>()));
            }

        }

        [TestClass] // just one
        public class GetTimecard2Tests : TimecardsRepositoryTests
        {
            public string inputTimecard2Id;

            public async Task<Domain.TimeManagement.Entities.Timecard2> getExpected()
            {
                return await testData.GetTimecard2Async(inputTimecard2Id);
            }
            public async Task<Domain.TimeManagement.Entities.Timecard2> getActual()
            {
                return await repositoryUnderTest.GetTimecard2Async(inputTimecard2Id);
            }

            [TestInitialize]
            public void Initialize()
            {
                base.TimecardsRepositoryTestsInitialize();
                inputTimecard2Id = testData.timecardRecords.First().id;
            }
            [TestCleanup]
            public void CleanUp()
            {
                base.testData = null;
                base.repositoryUnderTest = null;
            }

            [TestMethod]
            public async Task CorrectTimecard2IsReturnedTest()
            {
                var expected = await getExpected();
                var actual = await getActual();
                Assert.IsTrue(expected.CompareTimecard(actual));
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NoTimecard2IdThrowsExceptionTest()
            {
                try
                {
                    inputTimecard2Id = string.Empty;
                    await getActual();
                }
                catch (ArgumentNullException ane)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ane;
                }
            }
            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task NullTimecard2FromDbThrowsKnifeTest()
            {
                dataReaderMock.Setup(d => d.ReadRecordAsync<Ellucian.Colleague.Data.TimeManagement.DataContracts.Timecard>(It.IsAny<string>(), It.IsAny<bool>()))
                    .ReturnsAsync(null);
                try
                {
                    await getActual();
                }
                catch (KeyNotFoundException knife)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw knife;
                }
            }
            [TestMethod]
            public async Task NullTimeEntriesFromDbTest()
            {
                dataReaderMock.Setup(d => d.BulkReadRecordAsync<Ellucian.Colleague.Data.TimeManagement.DataContracts.TimeEntry>(It.IsAny<string>(), It.IsAny<bool>()))
                    .ReturnsAsync(null);
                var actual = await getActual();
                Assert.IsFalse(actual.TimeEntries.Any());
                loggerMock.Verify(l => l.Error(It.IsAny<string>()));

            }

            [TestMethod]
            public async Task SomeOtherErrorInTimeEntry2ThrowsExceptionTest()
            {
                testData.timeEntryRecords.ForEach(te => te.addDate = null);
                var actual = await getActual();
                Assert.IsFalse(actual.TimeEntries.Any());
                loggerMock.Verify(l => l.Error(It.IsAny<string>()));
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task StartDatesRequiredForTimecard2Test()
            {
                testData.timecardRecords.ForEach(t => t.startDate = null);
                await getActual();
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task EndDatesRequiredForTimecard2Test()
            {
                testData.timecardRecords.ForEach(t => t.endDate = null);
                try
                {
                    await getActual();
                }
                catch (ApplicationException)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            public async Task WorkedDateRequiredForAllTimeEntriesTest()
            {
                testData.timeEntryRecords.ForEach(t => t.workedDate = null);
                var actual = await getActual();
                Assert.IsFalse(actual.TimeEntries.Any());
                loggerMock.Verify(l => l.Error(It.IsAny<ApplicationException>(), It.IsAny<string>()));
            }

            [TestMethod]
            public async Task WorkedMinutesNotRequiredForTotalTimeTest()
            {
                testData.timeEntryRecords.ForEach(t =>
                {
                    t.inDate = null;
                    t.outDate = null;
                    t.workedMinutes = null;
                });

                var actual = await getActual();
                Assert.IsTrue(actual.TimeEntries.All(t => !t.WorkedTime.HasValue));

            }
        }
        #endregion

        #region create tests
        [TestClass]
        public class CreateTimecard2Tests : TimecardsRepositoryTests
        {
            public Domain.TimeManagement.Entities.Timecard2 inputTimecard2;

            public async Task<Domain.TimeManagement.Entities.Timecard2> createActual()
            {
                return await repositoryUnderTest.CreateTimecard2Async(inputTimecard2);
            }

            [TestInitialize]
            public void Initialize()
            {
                base.TimecardsRepositoryTestsInitialize();
                inputTimecard2 = new Domain.TimeManagement.Entities.Timecard2("0003914", "M1", "POLYSCIPROF", new DateTime(2016, 6, 1), new DateTime(2016, 6, 30), new DateTime(2016, 6, 1), new DateTime(2016, 6, 7));
            }
            [TestCleanup]
            public void CleanUp()
            {
                base.testData = null;
                base.repositoryUnderTest = null;
            }

            [TestMethod]
            public async Task CreateTimecard2ReturnsCreatedTimecard2()
            {
                var Timecard2 = await createActual();
                Assert.IsTrue(Timecard2.CompareTimecard(inputTimecard2));
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NoTimecards2uppliedThrowsExceptionTest()
            {
                try
                {
                    inputTimecard2 = null;
                    await createActual();

                }
                catch (ArgumentNullException ane)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ane;
                }
            }
            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NullCtxResponseThrowsExceptionTest()
            {
                try
                {
                    transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardRequest, CreateTimecardResponse>(It.IsAny<CreateTimecardRequest>()))
                        .Returns<CreateTimecardRequest>(req => Task.FromResult<CreateTimecardResponse>(null));

                    await createActual();
                }
                catch (ApplicationException ane)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ane;
                }
            }
            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task OtherCtxResponseThrowsExceptionTest()
            {
                try
                {
                    transManagerMock.Setup(t => t.ExecuteAsync<CreateTimecardRequest, CreateTimecardResponse>(It.IsAny<CreateTimecardRequest>()))
                        .Returns<CreateTimecardRequest>(req => Task.FromResult(new CreateTimecardResponse() { ErrorMessage = "This is an error" }));

                    await createActual();
                }
                catch (ApplicationException ane)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ane;
                }
            }
        }
        #endregion

        #region update tests
        [TestClass]
        public class UpdateTimecard2Tests : TimecardsRepositoryTests
        {
            private new bool CompareUpdateRequest(UpdateTimecardRequest req, Domain.TimeManagement.Entities.Timecard2 tc)
            {
                if
                    (
                    req.TimecardId == tc.Id &&
                    req.TcdEmployeeId == tc.EmployeeId &&
                    req.TcdPayCycleId == tc.PayCycleId &&
                    req.TcdPeriodStartDate == tc.PeriodStartDate &&
                    req.TcdPeriodEndDate == tc.PeriodEndDate
                    )
                    return true;
                else
                    return false;
            }

            public Timecard2Helper inputTimecard2Helper;

            [TestInitialize]
            public void Initialize()
            {
                base.TimecardsRepositoryTestsInitialize();
                inputTimecard2Helper = new Timecard2Helper(
                    new Domain.TimeManagement.Entities.Timecard2("0003914", "M1", "POLYSCIPROF", new DateTime(2016, 6, 1), new DateTime(2016, 6, 30), new DateTime(2016, 6, 1), new DateTime(2016, 6, 7), testData.timecardRecords[0].id));

                var TimeEntry21 = new Domain.TimeManagement.Entities.TimeEntry2(inputTimecard2Helper.Timecard.Id, "WORK", null, new DateTime(2016, 6, 1), "123");
                var TimeEntry22 = new Domain.TimeManagement.Entities.TimeEntry2(inputTimecard2Helper.Timecard.Id, "COMP", null, new DateTime(2016, 6, 2), "321");
                var TimeEntry23 = new Domain.TimeManagement.Entities.TimeEntry2(inputTimecard2Helper.Timecard.Id, "WORK", null, new DateTime(2016, 6, 1));
                inputTimecard2Helper.Timecard.TimeEntries.Add(TimeEntry21);
                inputTimecard2Helper.Timecard.TimeEntries.Add(TimeEntry22);
                inputTimecard2Helper.TimeEntrysToCreate.Add(TimeEntry23);
                inputTimecard2Helper.TimeEntrysToDelete.Add(TimeEntry22);
                inputTimecard2Helper.TimeEntrysToUpdate.Add(TimeEntry21);

            }
            [TestCleanup]
            public void CleanUp()
            {
                base.testData = null;
                base.repositoryUnderTest = null;
            }

            [TestMethod]
            public async Task RequestIsCorrectlyCreatedFromHelperTest()
            {
                await repositoryUnderTest.UpdateTimecard2Async(inputTimecard2Helper);
                transManagerMock.Verify(t => t.ExecuteAsync<UpdateTimecardRequest, UpdateTimecardResponse>(It.Is<UpdateTimecardRequest>(r => this.CompareUpdateRequest(r, inputTimecard2Helper.Timecard))));
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NoTimecards2uppliedThrowsExceptionTest()
            {
                try
                {
                    await repositoryUnderTest.UpdateTimecard2Async(null);
                }
                catch (ArgumentNullException ane)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ane;
                }
            }


            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NullCtxResponseThrowsExceptionTest()
            {
                try
                {
                    transManagerMock.Setup(t => t.ExecuteAsync<UpdateTimecardRequest, UpdateTimecardResponse>(It.IsAny<UpdateTimecardRequest>()))
                        .Returns<UpdateTimecardRequest>(req => Task.FromResult<UpdateTimecardResponse>(null));

                    await repositoryUnderTest.UpdateTimecard2Async(inputTimecard2Helper);
                }
                catch (ApplicationException ane)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ane;
                }
            }
            [TestMethod]
            [ExpectedException(typeof(RecordLockException))]
            public async Task CtxConflictErrorMessageThrowsExceptionTest()
            {
                try
                {
                    transManagerMock.Setup(t => t.ExecuteAsync<UpdateTimecardRequest, UpdateTimecardResponse>(It.IsAny<UpdateTimecardRequest>()))

                        .Returns<UpdateTimecardRequest>(req => Task.FromResult(new UpdateTimecardResponse() { ErrorMessage = "CONFLICT: This is a conflict" }));
                    await repositoryUnderTest.UpdateTimecard2Async(inputTimecard2Helper);
                }
                catch (ArgumentNullException ane)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ane;
                }
            }
            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task CtxOtherErrorMessageThrowsException()
            {
                try
                {
                    transManagerMock.Setup(t => t.ExecuteAsync<UpdateTimecardRequest, UpdateTimecardResponse>(It.IsAny<UpdateTimecardRequest>()))
                        .Returns<UpdateTimecardRequest>(req => Task.FromResult(new UpdateTimecardResponse() { ErrorMessage = "error" }));
                    await repositoryUnderTest.UpdateTimecard2Async(inputTimecard2Helper);
                }
                catch (ArgumentNullException ane)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw ane;
                }
            }
        }
        #endregion
        #endregion


    }
}
