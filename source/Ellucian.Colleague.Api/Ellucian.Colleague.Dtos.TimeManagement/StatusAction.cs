﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Ellucian.Colleague.Dtos.TimeManagement
{
    /// <summary>
    /// Records they types of actions performed on a timecard. The status action is stored on the status record.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum StatusAction
    {
        /// <summary>
        /// Indicates the timecard was submitted for approval
        /// </summary>
        Submitted,
        /// <summary>
        /// Indiciates the timecard was unsubmitted after previously being submitted
        /// </summary>
        Unsubmitted,

        /// <summary>
        /// Indicates the timecard was rejected
        /// </summary>
        Rejected,

        /// <summary>
        /// Indicates the timecard was approved
        /// </summary>
        Approved,

        /// <summary>
        /// Indicates the timecard was processed through payroll
        /// </summary>
        Paid
    }
}
