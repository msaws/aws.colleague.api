﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Dtos.TimeManagement
{
    /// <summary>
    /// Part of an OvertimeCalculationResult that indicates overtime
    /// for a specific date when a daily overtime threshold has been reached.
    /// </summary>
    public class DailyOvertimeAllocation
    {
        /// <summary>
        /// The date this overtime applies to
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// The amount of overtime
        /// </summary>
        public TimeSpan Overtime { get; set; }
    }
}
