﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using System;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Colleague.Dtos.TimeManagement
{
    /// <summary>
    /// Time Entry object for use with data transfer
    /// </summary>
    public class TimeEntry2
    {
        /// <summary>
        /// Id of the time entry
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Id of the associated time card (as version Timecard2)
        /// </summary>
        public string TimecardId { get; set; }
        /// <summary>
        /// Id of the associated project
        /// </summary>
        public string ProjectId { get; set; }
        /// <summary>
        /// Id of the associated earnings type
        /// </summary>
        public string EarningsTypeId { get; set; }
        /// <summary>
        /// Id of the associated person leave plan
        /// </summary>
        public string PersonLeaveId { get; set; }
        /// <summary>
        /// DateTime of this time entry start, i.e. beginning of a person's unit of work
        /// </summary>
        public DateTime? InDateTime { get; set; }
        /// <summary>
        /// DateTime of this time entry end, i.e. end of a person's unit of work
        /// </summary>
        public DateTime? OutDateTime { get; set; }
        /// <summary>
        /// Total time amount for this entry.
        /// This may be entered directly, else it is derived from the InDateTime/OutDateTime.
        /// This value will be null when the InDateTime is set but the OutDateTime is not set.
        /// </summary>
        public TimeSpan? WorkedTime { get; set; }
        /// <summary>
        /// Date associated with start of this time entry
        /// </summary>
        public DateTime WorkedDate { get; set; }
        /// <summary>
        /// Timestamp of record creation and change (read only)
        /// </summary>
        public Timestamp Timestamp { get; set; }
    }
}
