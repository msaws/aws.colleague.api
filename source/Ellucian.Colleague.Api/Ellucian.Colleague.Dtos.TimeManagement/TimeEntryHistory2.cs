﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using System;
using Ellucian.Colleague.Dtos.Base;
using System.Collections.Generic;

namespace Ellucian.Colleague.Dtos.TimeManagement
{
    /// <summary>
    /// Time Entry History object for use with data transfer
    /// </summary>
    public class TimeEntryHistory2
    {
        /// <summary>
        /// The identifier of this time entry history
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// This time entry's associated timecard history (as version TimecardHistory2) identifier
        /// </summary>
        public string TimecardHistoryId { get; set; }
        /// <summary>
        /// This time entry history's associated project identifier
        /// </summary>
        public string ProjectId { get; set; }
        /// <summary>
        /// This time entry history's associated earn type identifier
        /// </summary>
        public string EarningsTypeId { get; set; }
        /// <summary>
        /// This time entry history's associated person leave identifier
        /// </summary>
        public string PersonLeaveId { get; set; }
        /// <summary>
        /// The time of this entry history's commencement
        /// </summary>
        public DateTime? InDateTime { get; set; }
        /// <summary>
        /// The time of this entry history's termination
        /// </summary>
        public DateTime? OutDateTime { get; set; }
        /// <summary>
        /// The total time amount for this entry history
        /// </summary>
        public TimeSpan? WorkedTime { get; set; }
        /// <summary>
        /// The date associated with commencement of this time entry
        /// </summary>
        public DateTime WorkedDate { get; set; }
        /// <summary>
        /// The record timestamp
        /// </summary>
        public Timestamp Timestamp { get; set; }

    }
}
