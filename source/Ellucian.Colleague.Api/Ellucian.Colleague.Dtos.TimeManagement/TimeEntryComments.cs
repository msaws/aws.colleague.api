﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Dtos.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Ellucian.Colleague.Dtos.TimeManagement
{
     /// <summary>
     /// Comments associated to a timecard, timecard status, or an employee's position within a pay period
     /// </summary>
     public class TimeEntryComments
     {
          /// <summary>
          /// The database ID of the Comments object
          /// </summary>
          public string Id { get; set; }

          /// <summary>
          /// the ID of the employee this Comment is associated to
          /// </summary>
          public string EmployeeId { get; set; }

          /// <summary>
          /// The ID of the timecard this Comment is associated to. Not required if the comment is assigned to
          /// a timecardStatusId, or to a PositionId + PayCycleId + PayPeriodEndDate grouping
          /// </summary>
          public string TimecardId { get; set; }

          /// <summary>
          /// The ID of the timecardStatus this Comment is associated to. Not required if the comment is assigned to
          /// a timecardId,  or to a PositionId + PayCycleId + PayPeriodEndDate grouping.
          /// </summary>
          public string TimecardStatusId { get; set; }

          /// <summary>
          /// The ID of the PayCycle this Comment is associated to. Required if the comment is assigned to a
          /// PositionId + PayCycleId + PayPeriodEndDate grouping. Not Required if the comment is assigned to a TimecardId or
          /// timecardStatusId
          /// </summary>
          public string PayCycleId { get; set; }

          /// <summary>
          /// The end date of the pay period this Comment is associated to. Required if the comment is assigned to a
          /// PositionId + PayCycleId + PayPeriodEndDate grouping. Not Required if the comment is assigned to a TimecardId or
          /// timecardStatusId
          /// </summary>
          public DateTime PayPeriodEndDate { get; set; }

          /// <summary>
          /// The ID of the Position this Comment is associated to. Required if the comment is assigned to a
          /// PositionId + PayCycleId + PayPeriodEndDate grouping. Not Required if the comment is assigned to a TimecardId or
          /// timecardStatusId
          /// </summary>
          public string PositionId { get; set; }

          /// <summary>
          /// The actual Comment content
          /// </summary>
          public string Comments { get; set; }

          /// <summary>
          /// A timestamp of when this Comment object was created and changed in the database. Required if the Comment database record
          /// this object represents already exists. Not required if this is a new Comment
          /// </summary>
          public Timestamp CommentsTimestamp { get; set; }

          /// <summary>
          /// The name of the person who created the comment
          /// </summary>
          public string CommentAuthorName { get; set; }
     }
}
