﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Dtos.TimeManagement
{
    /// <summary>
    /// OvertimeQueryCriteria is the request object to be used 
    /// with the qapi/overtime api. Most overtime is calculated for 
    /// a date range of a week in length, but any date range may be supplied.
    /// </summary>
    public class OvertimeQueryCriteria
    {
        /// <summary>
        /// The Id of the Person for whom to calculate overtime. Required
        /// </summary>
        public string PersonId { get; set; }


        /// <summary>
        /// The start of the date range for which to calculate overtime. Required.
        /// </summary>
        public DateTime StartDate { get; set; }


        /// <summary>
        /// The end of the date range for which to calculate overtime. Required
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// The Id of the pay cycle for which to calculate overtime. Required.
        /// Overtime can only be calculated on a per-paycycle basis.
        /// </summary>
        public string PayCycleId { get; set; }



    }
}
