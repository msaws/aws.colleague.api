﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Dtos.TimeManagement
{
    /// <summary>
    /// The result of an overtime calculation.
    /// </summary>
    public class OvertimeCalculationResult
    {
        /// <summary>
        /// The Id of the person for whom this overtime result applies
        /// </summary>
        public string PersonId { get; set; }

        /// <summary>
        /// The start of the date range for which this overtime result applies.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// The end of the date range for which the overtime result applies.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// The paycycle for which the overtime result applies.
        /// </summary>
        public string PayCycleId { get; set; }

        /// <summary>
        /// The date and time this result was calculated.
        /// </summary>
        public DateTimeOffset ResultDateTime { get; set; }

        /// <summary>
        /// A list of the overtime thresholds reached by the person for the given timeframe
        /// </summary>
        public List<OvertimeThresholdResult> Thresholds { get; set; }
    }
}
