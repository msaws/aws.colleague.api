﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Ellucian.Colleague.Dtos.TimeManagement
{
     /// <summary>
     /// Enum for each method of calculating overtime pay
     /// </summary>
     [Serializable]
     [JsonConverter(typeof(StringEnumConverter))]
     public enum OvertimePayCalculationMethod
     {
          /// <summary>
          /// Value for depicting the WeightedAverage method of calculating overtime pay
          /// </summary>
          WeightedAverage,

          /// <summary>
          /// Value for depicting the PositionRate method of calculating overtime pay
          /// </summary>
          PositionRate
     } 
}
