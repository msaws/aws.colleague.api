﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Dtos.TimeManagement
{
    /// <summary>
    /// Represents the paramters used to calculate overtime.
    /// </summary>
     public class OvertimeCalculationDefinition
     {
          /// <summary>
          /// The Id associated with the OvertimeCalculationDefinition
          /// </summary>
          public string Id { get; set; }

          /// <summary>
          /// The description of the OvertimeCalculationDefinition
          /// </summary>
          public string Description { get; set; }

          /// <summary>
          /// The method of calculating the overtime pay
          /// </summary>
          public OvertimePayCalculationMethod OvertimePayCalculationMethod { get; set; }

          /// <summary>
          /// The list of HoursThresholds for weekly overtime
          /// </summary>
          public List<HoursThreshold> WeeklyHoursThreshold { get; set; }

          /// <summary>
          /// The list of HoursThresholds for daily overtime
          /// </summary>
          public List<HoursThreshold> DailyHoursThreshold { get; set; }

          /// <summary>
          /// The list of EarningsTypes to be included in the OvertimeCalculationDefinition
          /// </summary>
          public List<IncludedEarningsType> IncludedEarningsTypes { get; set; }

     }
}
