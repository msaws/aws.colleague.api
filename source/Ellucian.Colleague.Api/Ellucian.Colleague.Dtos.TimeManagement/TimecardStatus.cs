﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Colleague.Dtos.TimeManagement
{
    /// <summary>
    /// A representation of a timecard status instance.
    /// </summary>
    public class TimecardStatus
    {
        /// <summary>
        /// The identifier of this status.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The timecard associated to this status.
        /// </summary>
        public string TimecardId { get; set; }

        /// <summary>
        /// The person taking the action on the timecard represented by this status.
        /// </summary>
        public string ActionerId { get; set; }

        /// <summary>
        /// The type of action to be performed.
        /// </summary>
        public StatusAction ActionType { get; set; }

        /// <summary>
        /// Associated history record
        /// </summary>
        public string HistoryId { get; set; }

        /// <summary>
        /// Timestamp for record add and change (read only)
        /// </summary>
        public Timestamp Timestamp { get; set; }

    }
}
