﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Dtos.TimeManagement
{
    /// <summary>
    /// An Earnings Type that's indcluded in Overtime Calculations
    /// </summary>
    public class IncludedEarningsType
     {
          /// <summary>
          /// The ID associated with the earnings type
          /// </summary>
          public string EarningsTypeId { get; set; }

          /// <summary>
          /// Boolean that states whether to use the alternate earntype or not
          /// </summary>
          public bool UseAlternate { get; set; }
     }
}
