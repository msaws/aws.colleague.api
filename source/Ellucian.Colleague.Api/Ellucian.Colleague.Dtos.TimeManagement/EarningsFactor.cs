﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Dtos.TimeManagement
{
    /// <summary>
    /// Represents the multiplier and earnings type id that can be applied to overtime hours
    /// above a specific threshold.
    /// </summary>
     public class EarningsFactor
     {
          /// <summary>
          /// The Id representing the EarningsType
          /// </summary>
          public string EarningsTypeId { get; set; }

          /// <summary>
          /// The factor associated with the EarningsType
          /// </summary>
          public decimal Factor { get; set; }
     }
}
