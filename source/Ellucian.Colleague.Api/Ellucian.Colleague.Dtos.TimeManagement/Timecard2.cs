﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;

namespace Ellucian.Colleague.Dtos.TimeManagement
{
    /// <summary>
    /// Timecard used to record time worked for an employee. Each time card is unique for each person's position and
    /// is associated to a set of time entry objects.
    /// </summary>
    public class Timecard2
    {
        /// <summary>
        /// Unique identifier of this timecard. This id is not required on POST.
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Employee associated to this timecard
        /// </summary>
        public string EmployeeId { get; set; }
        /// <summary>
        /// Position associated to this timecard
        /// </summary>
        public string PositionId { get; set; }
        /// <summary>
        /// Pay cycle associated to this timecard
        /// </summary>
        public string PayCycleId { get; set; }
        /// <summary>
        /// Start Date of the pay period. 
        /// </summary>
        public DateTime PeriodStartDate { get; set; }
        /// <summary>
        /// End date of the pay period
        /// </summary>
        public DateTime PeriodEndDate { get; set; }
        /// <summary>
        /// Start date of the work week
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// End date of the work week
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// List of associated time entries (as version TimeEntry2)
        /// </summary>
        public List<TimeEntry2> TimeEntries { get; set; }
        /// <summary>
        /// Timestamp for record add and change (read only)
        /// </summary>
        public Timestamp Timestamp { get; set; }
    }
}
