﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Base;
using System.Linq;


namespace Ellucian.Colleague.Dtos.TimeManagement
{
    /// <summary>
    /// Timecard history used to retain information about time worked for an employee
    /// </summary>
    public class TimecardHistory2
    {
        /// <summary>
        /// Unique identifier of this timecard history. This id is not required on POST.
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Employee associated to this timecard history
        /// </summary>
        public string EmployeeId { get; set; }
        /// <summary>
        /// Position associated to this timecard history
        /// </summary>
        public string PositionId { get; set; }
        /// <summary>
        /// Pay cycle associated to this timecard history
        /// </summary>
        public string PayCycleId { get; set; }
        /// <summary>
        /// Start Date of the pay period. 
        /// </summary>
        public DateTime PeriodStartDate { get; set; }
        /// <summary>
        /// End date of the pay period
        /// </summary>
        public DateTime PeriodEndDate { get; set; }
        /// <summary>
        /// Start date of the work week
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// End date of the work week
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// List of associated time entry histories (as version TimeEntryHistory2)
        /// </summary>
        public List<TimeEntryHistory2> TimeEntryHistories { get; set; }
        /// <summary>
        /// Timestamp for record add and change
        /// </summary>
        public Timestamp Timestamp { get; set; }

        /// <summary>
        /// The StatusAction that created this history record
        /// </summary>
        public StatusAction StatusAction { get; set; }
    }
}
