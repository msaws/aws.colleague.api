﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Dtos.TimeManagement
{
    /// <summary>
    /// Represents part of the overtime calculation result that indicates 
    /// the earnings factor applied to overtime above a particular threshold.
    /// </summary>
    public class OvertimeThresholdResult
    {
        /// <summary>
        /// The total overtime for the specified threshold
        /// </summary>
        public TimeSpan TotalOvertime { get; set; }

        /// <summary>
        /// The Id representing the EarningsType
        /// </summary>
        public string EarningsTypeId { get; set; }

        /// <summary>
        /// The factor (pay multiplier) associated this threshold
        /// </summary>
        public decimal Factor { get; set; }

        /// <summary>
        /// A list of daily overtime allocations for daily thresholds.
        /// If null or empty, the threshold is for the entire time frame
        /// </summary>
        public List<DailyOvertimeAllocation> DailyAllocations { get; set; }

    }
}
