﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Dtos.TimeManagement
{
    /// <summary>
    /// Represents A threshold above which an Earnings Factor is applied to time worked
    /// </summary>
     public class HoursThreshold
     {
          /// <summary>
          /// The number of hours associated with this threshold
          /// </summary>
          public decimal Threshold { get; set; }

          /// <summary>
          /// The default EarningsFactor for overtime hours
          /// </summary>
          public EarningsFactor DefaultEarningsType { get; set; }

          /// <summary>
          /// The alternate EarningsFactor for overtime hours
          /// </summary>
          public EarningsFactor AlternateEarningsType { get; set; }       
     }
}
