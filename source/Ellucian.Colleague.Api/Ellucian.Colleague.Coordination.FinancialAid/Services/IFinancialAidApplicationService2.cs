﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;

namespace Ellucian.Colleague.Coordination.FinancialAid.Services
{
    /// <summary>
    /// Interface for FinancialAidApplications services
    /// </summary>
    public interface IFinancialAidApplicationService2 : IBaseService
    {
        Task<Tuple<IEnumerable<Ellucian.Colleague.Dtos.FinancialAidApplication>, int>> GetAsync(int offset, int limit, bool bypassCache);
        Task<Ellucian.Colleague.Dtos.FinancialAidApplication> GetByIdAsync(string id);
    }
}
