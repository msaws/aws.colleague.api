﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System.Threading.Tasks;
using Ellucian.Colleague.Dtos;
using System.Collections.Generic;
using System;
using Ellucian.Colleague.Coordination.Base.Services;

namespace Ellucian.Colleague.Coordination.FinancialAid.Services
{
    /// <summary>
    /// Interface for Student Financial Aid Awards
    /// </summary>
    public interface IStudentFinancialAidAwardService : IBaseService
    {
        Task<Dtos.StudentFinancialAidAward> GetByIdAsync(string id, bool restricted);

        Task<Tuple<IEnumerable<Dtos.StudentFinancialAidAward>, int>> GetAsync(int offset, int limit, bool bypassCache, bool restricted);
    }
}
