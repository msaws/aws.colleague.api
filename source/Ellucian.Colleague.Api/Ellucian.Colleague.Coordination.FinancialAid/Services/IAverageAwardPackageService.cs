﻿//Copyright 2014 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Colleague.Coordination.FinancialAid.Services
{
    /// <summary>
    /// Interface for the AverageAwardPackageService
    /// </summary>
    public interface IAverageAwardPackageService
    {
        /// <summary>
        /// Get a set of AverageAwardPackage DTO objects for the given student id
        /// for the student award years active on their record
        /// </summary>
        /// <param name="studentId">Student's Colleague PERSON Id</param>
        /// <returns>List of AverageAwardPackage objects</returns>
        IEnumerable<AverageAwardPackage> GetAverageAwardPackages(string studentId);
    }
}
