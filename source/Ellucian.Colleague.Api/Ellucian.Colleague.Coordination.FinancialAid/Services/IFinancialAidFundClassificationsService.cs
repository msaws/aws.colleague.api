//Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.FinancialAid.Services
{
    /// <summary>
    /// Interface for FinancialAidFundClassifications services
    /// </summary>
    public interface IFinancialAidFundClassificationsService
    {
        Task<IEnumerable<Ellucian.Colleague.Dtos.FinancialAidFundClassifications>> GetFinancialAidFundClassificationsAsync(bool bypassCache = false);
        Task<Ellucian.Colleague.Dtos.FinancialAidFundClassifications> GetFinancialAidFundClassificationsByGuidAsync(string id);
    }
}
