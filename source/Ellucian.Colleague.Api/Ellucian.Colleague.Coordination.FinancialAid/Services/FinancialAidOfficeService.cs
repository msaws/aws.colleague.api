﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System.Threading.Tasks;
using System;
using System.Linq;
using Ellucian.Colleague.Domain.Base.Repositories;

namespace Ellucian.Colleague.Coordination.FinancialAid.Services
{
    [RegisterType]
    public class FinancialAidOfficeService : FinancialAidCoordinationService, IFinancialAidOfficeService
    {
        private IFinancialAidOfficeRepository financialAidOfficeRepository;
        private readonly IConfigurationRepository configurationRepository;

        public FinancialAidOfficeService(IAdapterRegistry adapterRegistry,
            IFinancialAidOfficeRepository financialAidOfficeRepository,
            IConfigurationRepository configurationRepository,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(configurationRepository, adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.financialAidOfficeRepository = financialAidOfficeRepository;
            this.configurationRepository = configurationRepository;
        }

        /// <summary>
        /// Get a list of Financial Aid Offices
        /// </summary>
        /// <returns>A List of FinancialAidOffice3 objects</returns>
        public async Task<IEnumerable<Dtos.FinancialAid.FinancialAidOffice3>> GetFinancialAidOffices3Async()
        {
            var officeDtoAdapter = _adapterRegistry.GetAdapter<Domain.FinancialAid.Entities.FinancialAidOffice, Dtos.FinancialAid.FinancialAidOffice3>();

            var officeEntityList = await financialAidOfficeRepository.GetFinancialAidOfficesAsync();

            if (officeEntityList == null)
            {
                var message = "Null FinancialAidOffice object returned by repository";
                logger.Error(message);
                throw new KeyNotFoundException(message);
            }

            var officeDtoList = new List<Dtos.FinancialAid.FinancialAidOffice3>();
            foreach (var officeEntity in officeEntityList)
            {
                officeDtoList.Add(officeDtoAdapter.MapToType(officeEntity));
            }

            return officeDtoList;
        }

        /// <summary>
        /// Get a list of Financial Aid Offices
        /// </summary>
        /// <returns>A List of FinancialAidOffice2 objects</returns>
        [Obsolete("Obsolete as of Api version 1.15, use version 3 of this method")]
        public IEnumerable<Dtos.FinancialAid.FinancialAidOffice2> GetFinancialAidOffices2()
        {
            var officeDtoAdapter = _adapterRegistry.GetAdapter<Domain.FinancialAid.Entities.FinancialAidOffice, Dtos.FinancialAid.FinancialAidOffice2>();

            var officeEntityList = financialAidOfficeRepository.GetFinancialAidOffices();

            if (officeEntityList == null)
            {
                var message = "Null FinancialAidOffice object returned by repository";
                logger.Error(message);
                throw new KeyNotFoundException(message);
            }

            var officeDtoList = new List<Dtos.FinancialAid.FinancialAidOffice2>();
            foreach (var officeEntity in officeEntityList)
            {
                officeDtoList.Add(officeDtoAdapter.MapToType(officeEntity));
            }

            return officeDtoList;
        }

        /// <summary>
        /// Get a list of Financial Aid Offices
        /// </summary>
        /// <returns>A List of FinancialAidOffice2 objects</returns>
        [Obsolete("Obsolete as of Api version 1.15, use version 3 of this method")]
        public async Task<IEnumerable<Dtos.FinancialAid.FinancialAidOffice2>> GetFinancialAidOffices2Async()
        {
            var officeDtoAdapter = _adapterRegistry.GetAdapter<Domain.FinancialAid.Entities.FinancialAidOffice, Dtos.FinancialAid.FinancialAidOffice2>();

            var officeEntityList = await financialAidOfficeRepository.GetFinancialAidOfficesAsync();

            if (officeEntityList == null)
            {
                var message = "Null FinancialAidOffice object returned by repository";
                logger.Error(message);
                throw new KeyNotFoundException(message);
            }

            var officeDtoList = new List<Dtos.FinancialAid.FinancialAidOffice2>();
            foreach (var officeEntity in officeEntityList)
            {
                officeDtoList.Add(officeDtoAdapter.MapToType(officeEntity));
            }

            return officeDtoList;
        }


        /// <summary>
        /// Get a list of Financial Aid Offices
        /// </summary>
        /// <returns>A List of FinancialAidOffice objects</returns>
        [Obsolete("Obsolete as of Api version 1.14, use version 2 of this API")]
        public IEnumerable<Ellucian.Colleague.Dtos.FinancialAid.FinancialAidOffice> GetFinancialAidOffices()
        {
            var officeDtoAdapter = _adapterRegistry.GetAdapter<Domain.FinancialAid.Entities.FinancialAidOffice, Dtos.FinancialAid.FinancialAidOffice>();

            var officeEntityList = financialAidOfficeRepository.GetFinancialAidOffices();

            if (officeEntityList == null)
            {
                var message = "Null FinancialAidOffice object returned by repository";
                logger.Error(message);
                throw new KeyNotFoundException(message);
            }

            var officeDtoList = new List<Dtos.FinancialAid.FinancialAidOffice>();
            foreach (var officeEntity in officeEntityList)
            {
                officeDtoList.Add(officeDtoAdapter.MapToType(officeEntity));
            }

            return officeDtoList;
        }

        /// <summary>
        /// Get a list of Financial Aid Offices
        /// </summary>
        /// <returns>A List of FinancialAidOffice objects</returns>
        [Obsolete("Obsolete as of Api version 1.14, use version 2 of this API")]
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.FinancialAid.FinancialAidOffice>> GetFinancialAidOfficesAsync()
        {
            var officeDtoAdapter = _adapterRegistry.GetAdapter<Domain.FinancialAid.Entities.FinancialAidOffice, Ellucian.Colleague.Dtos.FinancialAid.FinancialAidOffice>();

            var officeEntityList = await financialAidOfficeRepository.GetFinancialAidOfficesAsync();

            if (officeEntityList == null)
            {
                var message = "Null FinancialAidOffice object returned by repository";
                logger.Error(message);
                throw new KeyNotFoundException(message);
            }

            var officeDtoList = new List<Ellucian.Colleague.Dtos.FinancialAid.FinancialAidOffice>();
            foreach (var officeEntity in officeEntityList)
            {
                officeDtoList.Add(officeDtoAdapter.MapToType(officeEntity));
            }

            return officeDtoList;
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Gets all financial aid offices
        /// </summary>
        /// <returns>Collection of FinancialAidOffice DTO objects</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.FinancialAidOffice>> GetFinancialAidOfficesAsync(bool bypassCache = false)
        {
            var financialAidOfficeCollection = new List<Ellucian.Colleague.Dtos.FinancialAidOffice>();

            var financialAidOfficeEntities = await financialAidOfficeRepository.GetFinancialAidOfficesAsync(bypassCache);
            if (financialAidOfficeEntities != null && financialAidOfficeEntities.Count() > 0)
            {
                foreach (var financialAidOffice in financialAidOfficeEntities)
                {
                    financialAidOfficeCollection.Add(ConvertFinancialAidOfficeEntityToDto(financialAidOffice));
                }
            }
            return financialAidOfficeCollection;
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Get an financial aid office from its GUID
        /// </summary>
        /// <returns>FinancialAidOffice DTO object</returns>
        public async Task<Ellucian.Colleague.Dtos.FinancialAidOffice> GetFinancialAidOfficeByGuidAsync(string guid)
        {
            try
            {
                return ConvertFinancialAidOfficeEntityToDto((await financialAidOfficeRepository.GetFinancialAidOfficesAsync(true)).Where(fa => fa.Guid == guid).First());
            }
            catch (InvalidOperationException ex)
            {
                throw new KeyNotFoundException("Financial aid office not found for GUID " + guid, ex);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Converts an Financial Aid Office domain entity to its corresponding FinancialAidOffice DTO
        /// </summary>
        /// <param name="source">FinancialAidOffice domain entity</param>
        /// <returns>FinancialAidOffice DTO</returns>
        private Ellucian.Colleague.Dtos.FinancialAidOffice ConvertFinancialAidOfficeEntityToDto(Ellucian.Colleague.Domain.FinancialAid.Entities.FinancialAidOfficeItem source)
        {
            var financialAidOffice = new Ellucian.Colleague.Dtos.FinancialAidOffice();

            financialAidOffice.Id = source.Guid;
            financialAidOffice.Code = source.Code;
            //financialAidOffice.Title = source.Description;
            financialAidOffice.Description = null;
            financialAidOffice.AidAdministrator = source.aidAdministrator;
            financialAidOffice.AddressLines = source.addressLines;
            financialAidOffice.City = source.city;
            financialAidOffice.State = source.state;
            financialAidOffice.PostalCode = source.postalCode;
            financialAidOffice.PhoneNumber = new Dtos.DtoProperties.NumberDtoProperty() { Number = source.phoneNumber };
            financialAidOffice.FaxNumber = new Dtos.DtoProperties.NumberDtoProperty() { Number = source.faxNumber };
            financialAidOffice.EmailAddress = source.emailAddress;
            financialAidOffice.Name = source.name;

            return financialAidOffice;
        }
    }
}
