﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System.Threading.Tasks;
using System;
using System.Linq;
using Ellucian.Colleague.Dtos.EnumProperties;
using Ellucian.Colleague.Domain.Base.Repositories;

namespace Ellucian.Colleague.Coordination.FinancialAid.Services
{
    [RegisterType]
    public class FinancialAidFundCategoryService : FinancialAidCoordinationService, IFinancialAidFundCategoryService
    {
        private IFinancialAidReferenceDataRepository financialAidReferenceDataRepository;
        private readonly IConfigurationRepository configurationRepository;

        public FinancialAidFundCategoryService(IAdapterRegistry adapterRegistry,
            IFinancialAidReferenceDataRepository financialAidReferenceDataRepository,
            IConfigurationRepository configurationRepository,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(configurationRepository, adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.financialAidReferenceDataRepository = financialAidReferenceDataRepository;
            this.configurationRepository = configurationRepository;
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Gets all financial aid fund categories
        /// </summary>
        /// <returns>Collection of FinancialAidFundCategory DTO objects</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.FinancialAidFundCategory>> GetFinancialAidFundCategoriesAsync(bool bypassCache = false)
        {
            var financialAidFundCategoryCollection = new List<Ellucian.Colleague.Dtos.FinancialAidFundCategory>();

            var financialAidFundCategoryEntities = await financialAidReferenceDataRepository.GetFinancialAidFundCategoriesAsync(bypassCache);
            if (financialAidFundCategoryEntities != null && financialAidFundCategoryEntities.Count() > 0)
            {
                foreach (var financialAidFundCategory in financialAidFundCategoryEntities)
                {
                    financialAidFundCategoryCollection.Add(ConvertFinancialAidFundCategoryEntityToDto(financialAidFundCategory));
                }
            }
            return financialAidFundCategoryCollection;
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Get an financial aid Fund Category from its GUID
        /// </summary>
        /// <returns>FinancialAidFundCategory DTO object</returns>
        public async Task<Ellucian.Colleague.Dtos.FinancialAidFundCategory> GetFinancialAidFundCategoryByGuidAsync(string guid)
        {
            try
            {
                return ConvertFinancialAidFundCategoryEntityToDto((await financialAidReferenceDataRepository.GetFinancialAidFundCategoriesAsync(true)).Where(fa => fa.Guid == guid).First());
            }
            catch (InvalidOperationException ex)
            {
                throw new KeyNotFoundException("Financial aid fund category not found for GUID " + guid, ex);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Converts an Financial Aid Fund Category domain entity to its corresponding FinancialAidFundCategory DTO
        /// </summary>
        /// <param name="source">FinancialAidFundCategory domain entity</param>
        /// <returns>FinancialAidFundCategory DTO</returns>
        private Ellucian.Colleague.Dtos.FinancialAidFundCategory ConvertFinancialAidFundCategoryEntityToDto(Ellucian.Colleague.Domain.FinancialAid.Entities.FinancialAidFundCategory source)
        {
            var financialAidFundCategory = new Ellucian.Colleague.Dtos.FinancialAidFundCategory();

            financialAidFundCategory.Id = source.Guid;
            financialAidFundCategory.Code = source.Code;
            financialAidFundCategory.Title = source.Description;
            financialAidFundCategory.Description = null;

            return financialAidFundCategory;
        }
    }
}
