﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Colleague.Coordination.FinancialAid.Services
{
    /// <summary>
    /// Interface to a ProfileApplicationService class
    /// </summary>
    public interface IProfileApplicationService
    {
        /// <summary>
        /// Get the ProfileApplications for the given student
        /// </summary>
        /// <param name="studentId">The Colleague PERSON id of the student for whom ProfileApplications are being retrieved.</param>
        /// <returns>A list of ProfileApplication DTOs for the given student id</returns>
        IEnumerable<ProfileApplication> GetProfileApplications(string studentId);
    }
}
