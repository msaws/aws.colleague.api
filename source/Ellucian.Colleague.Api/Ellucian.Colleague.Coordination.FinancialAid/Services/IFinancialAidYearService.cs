﻿//Copyright 2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.FinancialAid;
using System.Threading.Tasks;
namespace Ellucian.Colleague.Coordination.FinancialAid.Services
{
    public interface IFinancialAidYearService
    {
        Task<IEnumerable<Ellucian.Colleague.Dtos.FinancialAidYear>> GetFinancialAidYearsAsync(bool bypassCache);
        Task<Ellucian.Colleague.Dtos.FinancialAidYear> GetFinancialAidYearByGuidAsync(string guid);
    }
}
