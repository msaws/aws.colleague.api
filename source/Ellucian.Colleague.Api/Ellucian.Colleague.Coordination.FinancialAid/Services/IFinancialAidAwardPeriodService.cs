﻿//Copyright 2017 Ellucian Company L.P. and its affiliates
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.FinancialAid;
using System.Threading.Tasks;
namespace Ellucian.Colleague.Coordination.FinancialAid.Services
{
    public interface IFinancialAidAwardPeriodService
    {
        Task<IEnumerable<Ellucian.Colleague.Dtos.FinancialAidAwardPeriod>> GetFinancialAidAwardPeriodsAsync(bool bypassCache);
        Task<Ellucian.Colleague.Dtos.FinancialAidAwardPeriod> GetFinancialAidAwardPeriodByGuidAsync(string guid);
    }
}
