﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System.Threading.Tasks;
using System;
using System.Linq;
using Ellucian.Colleague.Dtos.EnumProperties;
using Ellucian.Colleague.Domain.Base.Repositories;

namespace Ellucian.Colleague.Coordination.FinancialAid.Services
{
    [RegisterType]
    public class FinancialAidAwardPeriodService : FinancialAidCoordinationService, IFinancialAidAwardPeriodService
    {
        private IFinancialAidReferenceDataRepository financialAidReferenceDataRepository;
        private readonly IConfigurationRepository configurationRepository;

        public FinancialAidAwardPeriodService(IAdapterRegistry adapterRegistry,
            IFinancialAidReferenceDataRepository financialAidReferenceDataRepository,
            IConfigurationRepository configurationRepository,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(configurationRepository, adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.financialAidReferenceDataRepository = financialAidReferenceDataRepository;
            this.configurationRepository = configurationRepository;
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Gets all financial aid award periods
        /// </summary>
        /// <returns>Collection of FinancialAidAwardPeriod DTO objects</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.FinancialAidAwardPeriod>> GetFinancialAidAwardPeriodsAsync(bool bypassCache = false)
        {
            var financialAidAwardPeriodCollection = new List<Ellucian.Colleague.Dtos.FinancialAidAwardPeriod>();

            var financialAidAwardPeriodEntities = await financialAidReferenceDataRepository.GetFinancialAidAwardPeriodsAsync(bypassCache);
            if (financialAidAwardPeriodEntities != null && financialAidAwardPeriodEntities.Count() > 0)
            {
                foreach (var financialAidAwardPeriod in financialAidAwardPeriodEntities)
                {
                    financialAidAwardPeriodCollection.Add(ConvertFinancialAidAwardPeriodEntityToDto(financialAidAwardPeriod));
                }
            }
            return financialAidAwardPeriodCollection;
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Get an financial aid award period from its GUID
        /// </summary>
        /// <returns>FinancialAidAwardPeriod DTO object</returns>
        public async Task<Ellucian.Colleague.Dtos.FinancialAidAwardPeriod> GetFinancialAidAwardPeriodByGuidAsync(string guid)
        {
            try
            {
                return ConvertFinancialAidAwardPeriodEntityToDto((await financialAidReferenceDataRepository.GetFinancialAidAwardPeriodsAsync(true)).Where(fa => fa.Guid == guid).First());
            }
            catch (InvalidOperationException ex)
            {
                throw new KeyNotFoundException("Financial aid award period not found for GUID " + guid, ex);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Converts an Financial Aid Award Period domain entity to its corresponding FinancialAidAwardPeriod DTO
        /// </summary>
        /// <param name="source">FinancialAidAwardPeriod domain entity</param>
        /// <returns>FinancialAidAwardPeriod DTO</returns>
        private Ellucian.Colleague.Dtos.FinancialAidAwardPeriod ConvertFinancialAidAwardPeriodEntityToDto(Ellucian.Colleague.Domain.FinancialAid.Entities.FinancialAidAwardPeriod source)
        {
            var financialAidAwardPeriod = new Ellucian.Colleague.Dtos.FinancialAidAwardPeriod();

            financialAidAwardPeriod.Id = source.Guid;
            financialAidAwardPeriod.Code = source.Code;
            financialAidAwardPeriod.Title = source.Description;
            financialAidAwardPeriod.Description = null;
            financialAidAwardPeriod.Start = source.StartDate;
            financialAidAwardPeriod.End = source.EndDate;
            financialAidAwardPeriod.Status = source.status;

            return financialAidAwardPeriod;
        }
    }
}
