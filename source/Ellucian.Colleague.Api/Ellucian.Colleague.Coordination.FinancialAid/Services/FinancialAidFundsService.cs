//Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Domain.FinancialAid.Entities;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos;
using Ellucian.Colleague.Domain.Base.Repositories;

namespace Ellucian.Colleague.Coordination.FinancialAid.Services
{
    [RegisterType]
    public class FinancialAidFundsService : FinancialAidCoordinationService, IFinancialAidFundsService
    {

        private readonly IFinancialAidReferenceDataRepository _referenceDataRepository;
        private readonly IFinancialAidFundRepository _fundRepository;
        private readonly IFinancialAidOfficeRepository _officeRepository;
        private readonly IConfigurationRepository configurationRepository;

        public FinancialAidFundsService(

            IFinancialAidReferenceDataRepository referenceDataRepository,
            IFinancialAidFundRepository fundRepository,
            IFinancialAidOfficeRepository officeRepository,
            IConfigurationRepository configurationRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(configurationRepository, adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.configurationRepository = configurationRepository;
            _referenceDataRepository = referenceDataRepository;
            _fundRepository = fundRepository;
            _officeRepository = officeRepository;
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM VERSION 7</remarks>
        /// <summary>
        /// Gets all financial-aid-funds
        /// </summary>
        /// <returns>Collection of FinancialAidFunds DTO objects</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.FinancialAidFunds>> GetFinancialAidFundsAsync(bool bypassCache = false)
        {
            var financialAidFundsCollection = new List<Ellucian.Colleague.Dtos.FinancialAidFunds>();

            var financialAidFundsEntities = await _fundRepository.GetFinancialAidFundsAsync(bypassCache);

            var aidYearsEntities = await _referenceDataRepository.GetFinancialAidYearsAsync(bypassCache);
            var aidYears = aidYearsEntities.Where(ay => ay.status != "D").Select(ay => ay.Code).Distinct().ToList();
            var hostCountry = await _referenceDataRepository.GetHostCountryAsync();

            var financialAidFundFinancials = await _fundRepository.GetFinancialAidFundFinancialsAsync("", aidYears, hostCountry);
            
            if (financialAidFundsEntities != null && financialAidFundsEntities.Any())
            {
                foreach (var financialAidFunds in financialAidFundsEntities)
                {
                    financialAidFundsCollection.Add(await ConvertFinancialAidFundsEntityToDto(financialAidFunds, financialAidFundFinancials, bypassCache));
                }
            }
            return financialAidFundsCollection;
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM VERSION 7</remarks>
        /// <summary>
        /// Get a FinancialAidFunds from its GUID
        /// </summary>
        /// <returns>FinancialAidFunds DTO object</returns>
        public async Task<Ellucian.Colleague.Dtos.FinancialAidFunds> GetFinancialAidFundsByGuidAsync(string guid)
        {
            try
            {
                var entity = (await _fundRepository.GetFinancialAidFundsAsync(true)).Where(r => r.Guid == guid).First();

                var aidYearsEntities = await _referenceDataRepository.GetFinancialAidYearsAsync(true);
                var aidYears = aidYearsEntities.Where(ay => ay.status != "D").Select(ay => ay.Code).Distinct().ToList();
                var hostCountry = await _referenceDataRepository.GetHostCountryAsync();

                var financialAidFundFinancials = await _fundRepository.GetFinancialAidFundFinancialsAsync(entity.Code, aidYears, hostCountry);

                return await ConvertFinancialAidFundsEntityToDto(entity, financialAidFundFinancials, true);
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException("financial-aid-funds not found for GUID " + guid, ex);
            }
            catch (InvalidOperationException ex)
            {
                throw new KeyNotFoundException("financial-aid-funds not found for GUID " + guid, ex);
            }
        }


        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Converts a FinancialAidFunds domain entity to its corresponding FinancialAidFunds DTO
        /// </summary>
        /// <param name="source">FinancialAidFunds domain entity</param>
        /// <returns>FinancialAidFunds DTO</returns>
        private async Task<Ellucian.Colleague.Dtos.FinancialAidFunds> ConvertFinancialAidFundsEntityToDto(FinancialAidFund source, IEnumerable<FinancialAidFundsFinancialProperty> financialSource, bool bypassCache = false)
        {
            var financialAidFunds = new Ellucian.Colleague.Dtos.FinancialAidFunds();

            financialAidFunds.Id = source.Guid;
            financialAidFunds.Code = source.Code;
            financialAidFunds.Title = source.Description;
            financialAidFunds.Description = source.Description2;
            financialAidFunds.Source = ConvertFinancialAidFundsSourceDomainEnumToFinancialAidFundsSourceDtoEnum(source.Source);

            var financialAidFundAwardCategories = await FinancialAidFundCategories(bypassCache);

            Domain.FinancialAid.Entities.FinancialAidFundCategory faCat = null;
            if (financialAidFundAwardCategories != null && financialAidFundAwardCategories.Any())
            {
                faCat = financialAidFundAwardCategories.Where(a => a.Code == source.CategoryCode).FirstOrDefault();
            }

           
            if (faCat != null) {
                financialAidFunds.Category = new Dtos.DtoProperties.FinancialAidFundsCategoryProperty();
                financialAidFunds.Category.Detail = new GuidObject2(faCat.Guid);
                financialAidFunds.Category.CategoryName = ConvertFinancialAidFundsAwardCategoryDomainEnumToFinancialAidFundsCategoryDtoEnum(faCat.AwardCategoryName);
                if (faCat.AwardCategoryType != null)
                {
                    financialAidFunds.AidType = ConvertFinancialAidFundsAidTypeDomainEnumToFinancialAidFundsAidTypeDtoEnum(faCat.AwardCategoryType);
                }
                financialAidFunds.Privacy = faCat.restrictedFlag ? Dtos.EnumProperties.FinancialAidFundsPrivacy.Restricted : Dtos.EnumProperties.FinancialAidFundsPrivacy.Nonrestricted;
            }

            var financialAidFundClassifications = await _referenceDataRepository.GetFinancialAidFundClassificationsAsync(bypassCache);
           
            Domain.FinancialAid.Entities.FinancialAidFundClassification faClass = null;
            if (financialAidFundClassifications != null && financialAidFundClassifications.Any())
            {
                faClass = financialAidFundClassifications.Where(a => a.FundingTypeCode == source.FundingType).FirstOrDefault();
            }

            if (faClass != null && !string.IsNullOrEmpty(faClass.Guid))
            {
                financialAidFunds.Classifications = new List<GuidObject2>();
                financialAidFunds.Classifications.Add(new GuidObject2(faClass.Guid));
            }

            var financialAidFundFinancials = financialSource.Where(f => f.AwardCode == source.Code);
            financialAidFunds.Financials = new List<Dtos.DtoProperties.FinancialAidFundsFinancialProperty>();

            foreach (var financials in financialAidFundFinancials)
            {
                financialAidFunds.Financials.Add(await ConvertFinancialAidFundFinancialsEntityToDto(financials));
            }
                                    
            return financialAidFunds;
        }
       

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Converts a FinancialAidFunds domain entity to its corresponding FinancialAidFunds DTO
        /// </summary>
        /// <param name="source">FinancialAidFunds domain entity</param>
        /// <returns>FinancialAidFunds DTO</returns>
        private async Task<Dtos.DtoProperties.FinancialAidFundsFinancialProperty> ConvertFinancialAidFundFinancialsEntityToDto(Domain.FinancialAid.Entities.FinancialAidFundsFinancialProperty source)
        {
            var financialDtos = new Dtos.DtoProperties.FinancialAidFundsFinancialProperty();

            financialDtos.AidYear = await ConvertEntityToAidYearGuidObjectAsync(source.AidYear);
            financialDtos.Office = await ConvertEntityToOfficeGuidObjectAsync(source.Office);
            financialDtos.BudgetedAmount = new Dtos.DtoProperties.FinancialDtoProperty();
            financialDtos.BudgetedAmount.Value = source.BudgetedAmount;
            financialDtos.BudgetedAmount.Currency = (await _referenceDataRepository.GetHostCountryAsync()).ToUpper() == "USA" ? Dtos.EnumProperties.CurrencyCodes.USD : Dtos.EnumProperties.CurrencyCodes.CAD;
            financialDtos.MaximumOfferedBudgetAmount = new Dtos.DtoProperties.FinancialDtoProperty();
            financialDtos.MaximumOfferedBudgetAmount.Value = source.MaximumOfferedBudgetAmount;
            if (source.MaximumOfferedBudgetAmount != null)
            {
                financialDtos.MaximumOfferedBudgetAmount.Currency = (await _referenceDataRepository.GetHostCountryAsync()).ToUpper() == "USA" ? Dtos.EnumProperties.CurrencyCodes.USD : Dtos.EnumProperties.CurrencyCodes.CAD;
            }

            return financialDtos;
        }

        private async Task<GuidObject2> ConvertEntityToAidYearGuidObjectAsync(string sourceCode, bool bypassCache = false)
        {
            if (string.IsNullOrEmpty(sourceCode))
            {
                return null;
            }
            var source = (await _referenceDataRepository.GetFinancialAidYearsAsync(bypassCache)).FirstOrDefault(i => i.Code.Equals(sourceCode, StringComparison.OrdinalIgnoreCase));
            if (source == null)
            {
                throw new KeyNotFoundException("Aid year not found for code " + sourceCode);
            }
            return string.IsNullOrEmpty(source.Guid) ? null : new GuidObject2(source.Guid);
        }

        private async Task<GuidObject2> ConvertEntityToOfficeGuidObjectAsync(string sourceCode, bool bypassCache = false)
        {
            if (string.IsNullOrEmpty(sourceCode))
            {
                return null;
            }
            var source = (await _officeRepository.GetFinancialAidOfficesAsync(bypassCache)).FirstOrDefault(i => i.Code.Equals(sourceCode, StringComparison.OrdinalIgnoreCase));
            if (source == null)
            {
                throw new KeyNotFoundException("Office not found for code " + sourceCode);
            }
            return string.IsNullOrEmpty(source.Guid) ? null : new GuidObject2(source.Guid);
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Converts a FinancialAidFundsSource domain enumeration value to its corresponding FinancialAidFundsSource DTO enumeration value
        /// </summary>
        /// <param name="source">FinancialAidFundsSource domain enumeration value</param>
        /// <returns>FinancialAidFundsSource DTO enumeration value</returns>
        private Ellucian.Colleague.Dtos.EnumProperties.FinancialAidFundAidCategoryType ConvertFinancialAidFundsAwardCategoryDomainEnumToFinancialAidFundsCategoryDtoEnum(Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType? source)
        {
            switch (source)
            {

                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.academicCompetitivenessGrant:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.academicCompetitivenessGrant;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.bureauOfIndianAffairsFederalGrant:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.bureauOfIndianAffairsFederalGrant;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.federalPerkinsLoan:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.federalPerkinsLoan;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.federalSubsidizedLoan:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.federalSubsidizedLoan;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.federalSupplementaryEducationalOpportunityGrant:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.federalSupplementaryEducationalOpportunityGrant;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.federalSupplementaryLoanForParent:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.federalSupplementaryLoanForParent;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.federalUnsubsidizedLoan:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.federalUnsubsidizedLoan;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.federalWorkStudyProgram:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.federalWorkStudyProgram;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.generalTitleIVloan:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.generalTitleIVloan;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.graduatePlusLoan:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.graduatePlusLoan;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.graduateTeachingGrant:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.graduateTeachGrant;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.healthEducationAssistanceLoan:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.healthEducationAssistanceLoan;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.healthProfessionalStudentLoan:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.healthProfessionalStudentLoan;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.incomeContingentLoan:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.incomeContingentLoan;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.iraqAfghanistanServiceGrant:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.iraqAfghanistanServiceGrant;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.leveragingEducationalAssistancePartnership:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.leveragingEducationalAssistancePartnership;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.loanForDisadvantagesStudent:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.loanForDisadvantagesStudent;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.nationalHealthServicesCorpsScholarship:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.nationalHealthServicesCorpsScholarship;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.nationalSmartGrant:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.nationalSmartGrant;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.NotSet:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.NotSet;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.nursingStudentLoan:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.nursingStudentLoan;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.parentPlusLoan:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.parentPlusLoan;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.paulDouglasTeacherScholarship:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.paulDouglasTeacherScholarship;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.pellGrant:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.pellGrant;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.primaryCareLoan:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.primaryCareLoan;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.robertCByrdScholarshipProgram:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.robertCByrdScholarshipProgram;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.rotcScholarship:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.rotcScholarship;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.stateStudentIncentiveGrant:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.stateStudentIncentiveGrant;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.stayInSchoolProgram:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.stayInSchoolProgram;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.undergraduateTeachingGrant:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.undergraduateTeachGrant;
                case Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.vaHealthProfessionsScholarship:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.vaHealthProfessionsScholarship;
         
                default:
                    return Dtos.EnumProperties.FinancialAidFundAidCategoryType.nonGovernmental;
            }
        }
   
        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Converts a FinancialAidFundsSource domain enumeration value to its corresponding FinancialAidFundsSource DTO enumeration value
        /// </summary>
        /// <param name="source">FinancialAidFundsSource domain enumeration value</param>
        /// <returns>FinancialAidFundsSource DTO enumeration value</returns>
        private Ellucian.Colleague.Dtos.EnumProperties.FinancialAidFundsSource ConvertFinancialAidFundsSourceDomainEnumToFinancialAidFundsSourceDtoEnum(string source)
        {
            switch (source)
            {

                case "F":
                    return Dtos.EnumProperties.FinancialAidFundsSource.Federal;
                case "I":
                    return Dtos.EnumProperties.FinancialAidFundsSource.Institutional;
                case "S":
                    return Dtos.EnumProperties.FinancialAidFundsSource.State;
                case "O":
                    return Dtos.EnumProperties.FinancialAidFundsSource.Other;
                default:
                    return Dtos.EnumProperties.FinancialAidFundsSource.Other;
            }
        }
   
        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Converts a FinancialAidFundsAidType domain enumeration value to its corresponding FinancialAidFundsAidType DTO enumeration value
        /// </summary>
        /// <param name="source">FinancialAidFundsAidType domain enumeration value</param>
        /// <returns>FinancialAidFundsAidType DTO enumeration value</returns>
        private Ellucian.Colleague.Dtos.EnumProperties.FinancialAidFundsAidType ConvertFinancialAidFundsAidTypeDomainEnumToFinancialAidFundsAidTypeDtoEnum(Domain.FinancialAid.Entities.AwardCategoryType? source)
        {
            switch (source)
            {

                case Domain.FinancialAid.Entities.AwardCategoryType.Loan:
                    return Dtos.EnumProperties.FinancialAidFundsAidType.Loan;
                case Domain.FinancialAid.Entities.AwardCategoryType.Grant:
                    return Dtos.EnumProperties.FinancialAidFundsAidType.Grant;
                case Domain.FinancialAid.Entities.AwardCategoryType.Scholarship:
                    return Dtos.EnumProperties.FinancialAidFundsAidType.Scholarship;
                case Domain.FinancialAid.Entities.AwardCategoryType.Work:
                    return Dtos.EnumProperties.FinancialAidFundsAidType.Work;
                default:
                    return Dtos.EnumProperties.FinancialAidFundsAidType.Loan;
            }
        }

        /// <summary>
        /// Financial Aid Fund Categories
        /// </summary>
        private IEnumerable<Domain.FinancialAid.Entities.FinancialAidFundCategory> _financialAidFundCategories;
        private async Task<IEnumerable<Domain.FinancialAid.Entities.FinancialAidFundCategory>> FinancialAidFundCategories(bool bypassCache)
        {
            if (_financialAidFundCategories == null)
            {
                _financialAidFundCategories = await _referenceDataRepository.GetFinancialAidFundCategoriesAsync(bypassCache);
            }
            return _financialAidFundCategories;
        }

   }
}