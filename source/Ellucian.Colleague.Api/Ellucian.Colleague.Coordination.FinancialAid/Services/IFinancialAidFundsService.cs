//Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.FinancialAid.Services
{
    /// <summary>
    /// Interface for FinancialAidFunds services
    /// </summary>
    public interface IFinancialAidFundsService
    {
        Task<IEnumerable<Ellucian.Colleague.Dtos.FinancialAidFunds>> GetFinancialAidFundsAsync(bool bypassCache = false);
        Task<Ellucian.Colleague.Dtos.FinancialAidFunds> GetFinancialAidFundsByGuidAsync(string id);
    }
}
