﻿/*Copyright 2014 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Colleague.Coordination.FinancialAid.Services
{
    /// <summary>
    /// Interface to a LoanRequestService
    /// </summary>
    public interface ILoanRequestService
    {
        /// <summary>
        /// Get a LoanRequest with the given Id
        /// </summary>
        /// <param name="id">Id of the loanRequest object to get</param>
        /// <returns>LoanRequest DTO with the given Id</returns>
        LoanRequest GetLoanRequest(string id);

        /// <summary>
        /// Create a new LoanRequest
        /// </summary>
        /// <param name="loanRequest">A LoanRequest object containing the new data</param>
        /// <returns>A new LoanRequest object</returns>
        LoanRequest CreateLoanRequest(LoanRequest loanRequest);
    }
}
