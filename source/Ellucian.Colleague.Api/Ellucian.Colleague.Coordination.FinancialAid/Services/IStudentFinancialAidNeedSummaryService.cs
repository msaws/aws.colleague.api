﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;

namespace Ellucian.Colleague.Coordination.FinancialAid.Services
{
    /// <summary>
    /// Interface for StudentFinancialAidNeedSummary services
    /// </summary>
    public interface IStudentFinancialAidNeedSummaryService : IBaseService
    {
        Task<Tuple<IEnumerable<Ellucian.Colleague.Dtos.StudentFinancialAidNeedSummary>, int>> GetAsync(int offset, int limit, bool bypassCache);
        Task<Ellucian.Colleague.Dtos.StudentFinancialAidNeedSummary> GetByIdAsync(string id);
    }
}
