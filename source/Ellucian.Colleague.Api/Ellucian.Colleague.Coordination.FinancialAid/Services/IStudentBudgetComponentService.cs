﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.FinancialAid;

namespace Ellucian.Colleague.Coordination.FinancialAid.Services
{
    /// <summary>
    /// Interface for a StudentBudgetComponentService
    /// </summary>
    public interface IStudentBudgetComponentService
    {
        /// <summary>
        /// Get all StudentBudgetComponents for the given studentId for all award years
        /// </summary>
        /// <param name="studentId">The Colleague PERSON id of the student for whom to get budget components</param>
        /// <returns>A list of StudentBudgetComponents</returns>
        IEnumerable<StudentBudgetComponent> GetStudentBudgetComponents(string studentId);
    }
}
