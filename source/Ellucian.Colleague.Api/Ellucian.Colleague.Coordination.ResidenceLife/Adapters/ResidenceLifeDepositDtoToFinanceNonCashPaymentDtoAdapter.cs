﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Adapters;
using slf4net;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Adapters
{
    /// <summary>
    /// Adapt a Finance NonCashPayment DTO from a Residence Life Deposit DTO 
    /// </summary>
    public class ResidenceLifeDepositDtoToFinanceNonCashPaymentDtoAdapter : AutoMapperAdapter<Dtos.ResidenceLife.Deposit,
                                                                              Dtos.Finance.NonCashPayment>
    {
        /// <summary>
        /// Constructor for the adapter
        /// </summary>
        /// <param name="adapterRegistry">An adapter registry</param>
        /// <param name="logger">A logger</param>
        public ResidenceLifeDepositDtoToFinanceNonCashPaymentDtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
        }
    }
}
