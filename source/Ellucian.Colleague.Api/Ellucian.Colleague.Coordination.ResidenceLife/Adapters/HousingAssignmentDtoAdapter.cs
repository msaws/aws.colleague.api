﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;
using Ellucian.Colleague.Dtos.ResidenceLife;
using slf4net;
using Ellucian.Web.Adapters;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Adapters
{
    
    /// <summary>
    /// An adapter mapping the HousingAssignment dto to the HousingAssignment domain entity
    /// </summary>
    public class HousingAssignmentDtoAdapter : BaseAdapter<Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment, 
                                                            Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment>
    {
        /// <summary>
        /// Constructor for the adapter
        /// </summary>
        /// <param name="adapterRegistry">An adapter registry</param>
        /// <param name="logger">A logger</param>
        public HousingAssignmentDtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger) : base(adapterRegistry, logger) { }

        /// <summary>
        /// Override of the MapToType method
        /// </summary>
        /// <param name="source">The housing assignment dto</param>
        /// <returns>Returns a housing assignment domain entity</returns>
        public override Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment MapToType(Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment source)
        {
            // Most entity properties are set via the constructor or methods. Do so below.
            var housingAssignment = new Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment(
                source.PersonId, source.StartDate, source.EndDate, source.CurrentStatus, source.CurrentStatusDate);

            housingAssignment.SetRateInformation(
                source.TermId,
                source.RoomRateTable,
                (Ellucian.Colleague.Domain.ResidenceLife.Entities.RoomRatePeriods?) source.RoomRatePeriod,
                source.OverrideRate,source.OverrideRateReason, source.OverrideChargeCode,
                source.OverrideReceivableType, source.OverrideRefundFormula);

            if (source.ExternalIds != null)
            {
                foreach (var externalId in source.ExternalIds)
                {
                    housingAssignment.AddExternalId(externalId.ExternalId, externalId.ExternalIdSource);
                }
            }

            housingAssignment.Id = source.Id;

            housingAssignment.SetBuildingAndRoom(source.Building, source.Room);

            if (source.AdditionalChargesOrCredits != null)
            {
                foreach (var acc in source.AdditionalChargesOrCredits)
                {
                    housingAssignment.AddAdditionalChargeOrCredit(acc.ArCode, acc.Description, acc.Amount);
                }
            }

            housingAssignment.Comments = source.Comments;
            housingAssignment.Contract = source.Contract;
            housingAssignment.ResidentStaffIndicator = source.ResidentStaffIndicator;

            return housingAssignment;
        }
    }
}

