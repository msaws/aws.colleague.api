﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Web.Adapters;
using slf4net;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Adapters
{
    /// <summary>
    /// An adapter mapping a ResidenceLife ReceivableInvoice dto to a Finance ReceivableInvoice dto
    /// </summary>    
    public class ResidenceLifeReceivableInvoiceDtoToDtoAdapter : AutoMapperAdapter<Dtos.ResidenceLife.ReceivableInvoice,
                                                                              Dtos.Finance.ReceivableInvoice>
    {
    
        /// <summary>
        /// Constructor for the adapter
        /// </summary>
        /// <param name="adapterRegistry">An adapter registry</param>
        /// <param name="logger">A logger</param>
        public ResidenceLifeReceivableInvoiceDtoToDtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger) : base(adapterRegistry, logger) 
        {
            // Specify that the Residence Life ReceivableCharge child-object should auto-map to the corresponding Finance ReceivableCharge object
            AddMappingDependency<Dtos.ResidenceLife.ReceivableCharge, Dtos.Finance.ReceivableCharge>();
        }

    }
}
