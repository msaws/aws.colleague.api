﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Adapters;
using slf4net;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Adapters
{
    /// <summary>
    /// Adapt a Finance Deposit entity from a Residence Life Deposit DTO 
    /// </summary>
    public class ResidenceLifeDepositDtoToFinanceDepositDtoAdapter : AutoMapperAdapter<Dtos.ResidenceLife.Deposit,
        Dtos.Finance.Deposit>
    {
        /// <summary>
        /// Constructor for the adapter
        /// </summary>
        /// <param name="adapterRegistry">An adapter registry</param>
        /// <param name="logger">A logger</param>
        public ResidenceLifeDepositDtoToFinanceDepositDtoAdapter(IAdapterRegistry adapter, ILogger logger)
            : base(adapter, logger)
        {

        }
    }
}
