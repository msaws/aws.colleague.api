﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using Ellucian.Web.Adapters;
using slf4net;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Adapters
{
    /// <summary>
    /// Adapt a Finance Receipt DTO from a Residence Life Deposit DTO 
    /// </summary>
    public class ResidenceLifeDepositDtoToFinanceReceiptDtoAdapter : BaseAdapter<Dtos.ResidenceLife.Deposit,
                                                                              Dtos.Finance.Receipt>
    {
        /// <summary>
        /// Constructor for the adapter
        /// </summary>
        /// <param name="adapterRegistry">An adapter registry</param>
        /// <param name="logger">A logger</param>
        public ResidenceLifeDepositDtoToFinanceReceiptDtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
        }

        public override Dtos.Finance.Receipt MapToType(Dtos.ResidenceLife.Deposit Source)
        {
            Dtos.Finance.NonCashPayment payment = new Dtos.Finance.NonCashPayment()
            {
                PaymentMethodCode = Source.PaymentMethod,
                Amount = Source.Amount,
            };
            List<Dtos.Finance.NonCashPayment> payments = new List<Dtos.Finance.NonCashPayment>() { payment };

            return new Dtos.Finance.Receipt()
            {
                PayerId = Source.PayerId,
                PayerName = Source.PayerName,
                Date = Source.Date,
                ExternalIdentifier = Source.ExternalIdentifier,
                NonCashPayments = payments,
            };
        }

    }
}
