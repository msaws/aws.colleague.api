﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Web.Adapters;
using slf4net;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Adapters
{
    /// <summary>
    /// An adapter mapping a MealPlanAssignment domain entity to a MealPlanAssignment dto
    /// </summary>    
    public class MealPlanAssignmentEntityAdapter : AutoMapperAdapter<Ellucian.Colleague.Domain.ResidenceLife.Entities.MealPlanAssignment, 
                                                            Ellucian.Colleague.Dtos.ResidenceLife.MealPlanAssignment>
    {
    
        /// <summary>
        /// Constructor for the adapter
        /// </summary>
        /// <param name="adapterRegistry">An adapter registry</param>
        /// <param name="logger">A logger</param>
        public MealPlanAssignmentEntityAdapter(IAdapterRegistry adapterRegistry, ILogger logger) : base(adapterRegistry, logger) 
        {
            // Specify that the external id entity child-object should auto-map to the corresponding dto child-object
            AddMappingDependency<Ellucian.Colleague.Domain.ResidenceLife.Entities.ResidenceLifeExternalId,
                Ellucian.Colleague.Dtos.ResidenceLife.ResidenceLifeExternalId>();

        }

    }
}
