﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Web.Adapters;
using slf4net;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Adapters
{
    /// <summary>
    /// An adapter mapping a Finance ReceivableInvoice dto to a ResidenceLife ReceivableInvoice dto
    /// </summary>    
    public class FinanceReceivableInvoiceDtoToDtoAdapter : AutoMapperAdapter<Dtos.Finance.ReceivableInvoice,
                                                                              Dtos.ResidenceLife.ReceivableInvoice>
    {
    
        /// <summary>
        /// Constructor for the adapter
        /// </summary>
        /// <param name="adapterRegistry">An adapter registry</param>
        /// <param name="logger">A logger</param>
        public FinanceReceivableInvoiceDtoToDtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger) : base(adapterRegistry, logger) 
        {
            // Specify that the Finance ReceivableCharge child-object should auto-map to the corresponding ResidenceLife ReceivableCharge object
            AddMappingDependency<Dtos.Finance.ReceivableCharge, Dtos.ResidenceLife.ReceivableCharge>();
        }

    }
}
