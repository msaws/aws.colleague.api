﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using Ellucian.Colleague.Dtos.ResidenceLife;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Services
{
    /// <summary>
    /// Coordination layer services related to accounts receivable in the context of residence life
    /// </summary>
    public interface IResidenceLifeAccountsReceivableService
    {

        /// <summary>
        /// Create a new receivable invoice, specific to residence life purposes.
        /// </summary>
        /// <param name="residenceLifeReceivableInvoice">A residence life receivable invoice dto</param>
        /// <returns>A finance invoice dto</returns>
        Dtos.Finance.ReceivableInvoice CreateReceivableInvoice(Dtos.ResidenceLife.ReceivableInvoice residenceLifeReceivableInvoice);

        /// <summary>
        /// Create a new deposit, specific to residence life purposes.
        /// </summary>
        /// <param name="sourceDeposit">A residence life deposit DTO</param>
        /// <returns>a finance deposit DTO representing the created deposit</returns>
        Dtos.Finance.Deposit CreateDeposit(Dtos.ResidenceLife.Deposit sourceDeposit);
    }
}
