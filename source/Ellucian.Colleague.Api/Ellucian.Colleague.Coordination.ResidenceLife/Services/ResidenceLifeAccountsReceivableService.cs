﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Coordination.Finance;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.ResidenceLife;
using Ellucian.Colleague.Domain.ResidenceLife.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Services
{
    /// <summary>
    /// Implementation of the IResidenceLifeAccountsReceivableService coordination layer interface
    /// </summary>
    [RegisterType]
    public class ResidenceLifeAccountsReceivableService : BaseCoordinationService, IResidenceLifeAccountsReceivableService
    {
        private IAccountsReceivableService accountsReceivableService;
        private IResidenceLifeConfigurationRepository residenceLifeConfigurationRepository;
        private IReceiptService _receiptService;
        
        /// <summary>
        /// Constructor for the service
        /// </summary>
        /// <param name="adapterRegistry">An adapter registry</param>
        /// <param name="currentUserFactory">A current user factory</param>
        /// <param name="roleRepository">A role repository</param>
        /// <param name="logger">A logger</param>
        /// <param name="accountsReceivableService">The general Accounts Receivable coordination service </param>
        /// <param name="residenceLifeConfigurationRepository">A Residence Life Configuration repository</param>
        /// <param name="receiptService">The Receipt service</param>
        public ResidenceLifeAccountsReceivableService(IAdapterRegistry adapterRegistry, ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger,
                                        IAccountsReceivableService accountsReceivableService, IResidenceLifeConfigurationRepository residenceLifeConfigurationRepository, IReceiptService receiptService)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger, null)
        {
            this.accountsReceivableService = accountsReceivableService;
            this.residenceLifeConfigurationRepository = residenceLifeConfigurationRepository;
            _receiptService = receiptService;
        }

        /// <summary>
        /// Create a new receivable invoice, specific to residence life purposes.
        /// </summary>
        /// <param name="residenceLifeReceivableInvoice">A residence life receivable invoice dto</param>
        /// <returns>A finance receivable invoice dto</returns>
        public Dtos.Finance.ReceivableInvoice CreateReceivableInvoice(Dtos.ResidenceLife.ReceivableInvoice residenceLifeReceivableInvoice)
        {
            // User must have proper permissions
            if (!HasPermission(ResidenceLifePermissionCodes.CreateRlInvoices))
            {
                logger.Info(CurrentUser + " does not have permission code " + ResidenceLifePermissionCodes.CreateRlInvoices);
                throw new PermissionsException();
            }
            
            try
            {  
                // Do a quick Res Life-specific validation of invoice item amounts.  Should properly be done in an
                // entity.
                if (residenceLifeReceivableInvoice.Charges != null)
                {
                    foreach (var charge in residenceLifeReceivableInvoice.Charges)
                    {
                        if (charge.BaseAmount == 0)
                        {
                            logger.Error("Charge amount cannot be 0");
                            throw new ArgumentOutOfRangeException("BaseAmount", "Amount cannot be 0.");
                        }
                    }
                }

                // Convert our dto to a general finance ReceivableInvoice dto
                var residenceLifeToFinanceDtoAdapter = _adapterRegistry.GetAdapter<Dtos.ResidenceLife.ReceivableInvoice,
                    Dtos.Finance.ReceivableInvoice>();
                Dtos.Finance.ReceivableInvoice financeReceivableInvoice =
                    residenceLifeToFinanceDtoAdapter.MapToType(residenceLifeReceivableInvoice);

                // Populate additional properties of the finance dto from residence life configuration settings
                // (Note, whether these values are required or not is implemented inside the ResidenceLifeConfiguration domain.)
                Domain.ResidenceLife.Entities.ResidenceLifeConfiguration residenceLifeConfiguration = 
                    residenceLifeConfigurationRepository.GetResidenceLifeConfiguration();
                financeReceivableInvoice.ReceivableType = residenceLifeConfiguration.InvoiceReceivableTypeCode;
                financeReceivableInvoice.InvoiceType = residenceLifeConfiguration.InvoiceTypeCode;
                financeReceivableInvoice.ExternalSystem = residenceLifeConfiguration.ExternalSystemId;

                // Invoke the general finance service to create an invoice. Return the result
                return accountsReceivableService.PostReceivableInvoice(financeReceivableInvoice);

            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// Create a new deposit, specific to Residence Life purposes
        /// </summary>
        /// <param name="sourceDeposit">A residence life deposit DTO</param>
        /// <returns>A finance deposit DTO representing the created deposit</returns>
        public Dtos.Finance.Deposit CreateDeposit(Dtos.ResidenceLife.Deposit sourceDeposit)
        {
            if (sourceDeposit == null)
            {
                throw new ArgumentNullException("sourceDeposit", "A deposit must be specified.");
            }


            // User must have proper permissions
            if (!HasPermission(ResidenceLifePermissionCodes.CreateRlDeposits))
            {
                logger.Info(CurrentUser + " does not have permission code " + ResidenceLifePermissionCodes.CreateRlDeposits);
                throw new PermissionsException();
            }

            // Populate additional properties of the deposit dto from residence life configuration settings
            Domain.ResidenceLife.Entities.ResidenceLifeConfiguration residenceLifeConfiguration =
                residenceLifeConfigurationRepository.GetResidenceLifeConfiguration();

            // create a Dtos.Finance.Receipt from the Dtos.ResidenceLife.Deposit parameter
            var receiptAdapter = _adapterRegistry.GetAdapter<Dtos.ResidenceLife.Deposit, Dtos.Finance.Receipt>();
            var dtoReceipt = receiptAdapter.MapToType(sourceDeposit);
            dtoReceipt.DistributionCode = residenceLifeConfiguration.DepositDistribution;
            dtoReceipt.CashierId = residenceLifeConfiguration.Cashier;
            dtoReceipt.ExternalSystem = residenceLifeConfiguration.ExternalSystemId;

            // create a Dtos.Finance.Deposit from the Dtos.ResidenceLife.Deposit parameter
            var depositAdapter = _adapterRegistry.GetAdapter<Dtos.ResidenceLife.Deposit, Dtos.Finance.Deposit>();
            var dtoDeposit = depositAdapter.MapToType(sourceDeposit);
            dtoDeposit.ExternalSystem = residenceLifeConfiguration.ExternalSystemId;
            var dtoDeposits = new List<Dtos.Finance.Deposit>(){dtoDeposit};

            // get cashier entity so we can validate the cashier attributes
            Domain.Finance.Entities.Cashier cashier;
            try
            {
                cashier = _receiptService.GetCashier(dtoReceipt.CashierId);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                logger.Error(ex.StackTrace);
                throw new ArgumentException("Cannot retrieve information for cashier " + dtoReceipt.CashierId);
            }
            Domain.ResidenceLife.Services.ResidenceLifeAccountsReceivableProcessor.ValidateResidenceLifeReceipt(dtoReceipt, dtoDeposits, cashier);

            // create the cash receipt and deposit
            var responseReceipt = _receiptService.CreateReceipt(dtoReceipt, null, dtoDeposits);

            // use the returned deposit id to get the newly created deposit entity
            var newDepositId = responseReceipt.DepositIds.FirstOrDefault();
            if (string.IsNullOrEmpty(newDepositId))
            {
                throw new Exception("Deposit creation failed");
            }
            
            return(accountsReceivableService.GetDeposit(newDepositId));
        }
    }
}
