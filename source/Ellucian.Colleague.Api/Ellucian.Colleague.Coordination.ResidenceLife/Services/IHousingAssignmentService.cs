﻿using System;
using Ellucian.Colleague.Dtos.ResidenceLife;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Services
{
    /// <summary>
    /// Coordination layer services related to housing assignments.
    /// </summary>
    public interface IHousingAssignmentService
    {
        /// <summary>
        /// Create a new housing assignment and persist through a repository
        /// </summary>
        /// <param name="housingAssignment">A fully populated new housing assignment dto</param>
        /// <returns></returns>
        Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment CreateHousingAssignment(Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignment);
        
        /// <summary>
        /// Update an existing housing assignment entity through a repository
        /// </summary>
        /// <param name="housingAssignment">A fully populated housing assignment dto containing updated attributes</param>
        /// <returns></returns>
        Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment UpdateHousingAssignment(Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignment);

        /// <summary>
        /// Get a housing assignment by the ID in the native system
        /// </summary>
        /// <param name="id">The ID of the housing assignment in the native system</param>
        /// <returns></returns>
        Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment GetHousingAssignment(string id);
    }
}
