﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.ResidenceLife;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;
using Ellucian.Colleague.Domain.ResidenceLife.Repositories;
using Ellucian.Colleague.Dtos.ResidenceLife;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Repositories;
using slf4net;
using Ellucian.Web.Dependency;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Services
{
    /// <summary>
    /// Implementation of the IHousingAssignmentService coordination layer interface
    /// </summary>
    [RegisterType]
    public class HousingAssignmentService : BaseCoordinationService, IHousingAssignmentService
    {
        private IHousingAssignmentRepository housingAssignmentRepository;
        
        /// <summary>
        /// Constructor for the service
        /// </summary>
        /// <param name="adapterRegistry">An adapter registry</param>
        /// <param name="currentUserFactory">A current user factory</param>
        /// <param name="roleRepository">A role repository</param>
        /// <param name="logger">A logger</param>
        /// <param name="housingAssignmentRepository">An implementation of the housing assignment repository interface </param>
        public HousingAssignmentService(IAdapterRegistry adapterRegistry, ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger,
                                        IHousingAssignmentRepository housingAssignmentRepository)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.housingAssignmentRepository = housingAssignmentRepository;
        }

        /// <summary>
        /// Create a new housing assignment and persist through a repository
        /// </summary>
        /// <param name="housingAssignment">A fully populated new housing assignment dto</param>
        /// <returns></returns>
        public Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment CreateHousingAssignment(Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignment)
        {
            // User must have proper permissions
            IEnumerable<string> userPermissions = GetUserPermissionCodes();
            if (!userPermissions.Contains(ResidenceLifePermissionCodes.CreateUpdateHousingAssignments))
            {
                logger.Error("Permissions error accessing CreateHousingAssignment. User " + CurrentUser.UserId);
                throw new PermissionsException();
            }

            if (!string.IsNullOrEmpty(housingAssignment.Id))
            {
                string message = "Id cannot be supplied to the Create operation.";
                logger.Error("Error in CreateHousingAssignment." + message);
                throw new ArgumentException(message);
            }

            try
            {
                // Convert housing assignment DTO to domain entity
                var housingAssignmentToEntityAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment,
                    Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment>();
                Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment housingAssignmentEntity =
                    housingAssignmentToEntityAdapter.MapToType(housingAssignment);

                // Repository Add method
                Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment newHousingAssignment =
                    housingAssignmentRepository.Add(housingAssignmentEntity);

                // Convert housing assignment domain entity to DTO
                var housingAssignmentToDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment,
                    Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment>();
                return housingAssignmentToDtoAdapter.MapToType(newHousingAssignment);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// Update an existing housing assignment entity through a repository
        /// </summary>
        /// <param name="housingAssignment">A fully populated housing assignment dto containing updated attributes</param>
        /// <returns></returns>
        public Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment UpdateHousingAssignment(Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignment)
        {

            // User must have proper permissions
            IEnumerable<string> userPermissions = GetUserPermissionCodes();
            if (!userPermissions.Contains(ResidenceLifePermissionCodes.CreateUpdateHousingAssignments))
            {
                logger.Error("Permissions error accessing UpdateHousingAssignment. User " + CurrentUser.UserId);
                throw new PermissionsException();
            }

            if ((housingAssignment.Id == null) && (housingAssignment.ExternalIds == null || housingAssignment.ExternalIds.Count == 0))
            {
                string message = "An Id or External Id must be supplied to the update operation.";
                logger.Error("Error in UpdateHousingAssignment. " + message);
                throw new ArgumentException(message);
            }

            try
            {
                // Convert housing assignment DTO to domain entity
                var housingAssignmentToEntityAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment,
                    Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment>();
                Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment housingAssignmentEntity = 
                    housingAssignmentToEntityAdapter.MapToType(housingAssignment);

                // Repository Update method
                Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment newHousingAssignment =
                    housingAssignmentRepository.Update(housingAssignmentEntity);

                // Convert housing assignment domain entity to DTO
                var housingAssignmentToDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment,
                    Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment>();
                
                return housingAssignmentToDtoAdapter.MapToType(newHousingAssignment);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// Get a housing assignment by the ID in the native system
        /// </summary>
        /// <param name="id">The ID of the housing assignment in the native system</param>
        /// <returns></returns>
        public Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment GetHousingAssignment(string id)
        {
            // User must have proper permissions
            IEnumerable<string> userPermissions = GetUserPermissionCodes();
            if (!userPermissions.Contains(ResidenceLifePermissionCodes.ViewHousingAssignments))
            {
                logger.Error("Permissions error accessing GetHousingAssignment. User " + CurrentUser.UserId);
                throw new PermissionsException();
            }

            try
            {
                // Repository Get method
                Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment newHousingAssignment =
                    housingAssignmentRepository.Get(id);

                // Convert housing assignment domain entity to DTO
                var housingAssignmentToDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment,
                    Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment>();
                return housingAssignmentToDtoAdapter.MapToType(newHousingAssignment);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

    }
}
