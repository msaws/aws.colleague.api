﻿using System;
using Ellucian.Colleague.Dtos.ResidenceLife;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Services
{
    /// <summary>
    /// Coordination layer services related to housing assignments.
    /// </summary>
    public interface IMealPlanAssignmentService
    {

        /// <summary>
        /// Get a meal plan assignment by the ID in the native system
        /// </summary>
        /// <param name="id">The ID of the meal plan assignment in the native system</param>
        /// <returns></returns>
        Dtos.ResidenceLife.MealPlanAssignment GetMealPlanAssignment(string id);

        /// <summary>
        /// Create a new meal plan assignment and persist through a repository
        /// </summary>
        /// <param name="mealPlanAssignment">A fully populated new meal plan assignment dto</param>
        /// <returns></returns>
        Dtos.ResidenceLife.MealPlanAssignment CreateMealPlanAssignment(Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignment);

        /// <summary>
        /// Update an existing meal plan assignment entity through a repository
        /// </summary>
        /// <param name="housingAssignment">A fully populated meal plan assignment dto containing updated attributes</param>
        /// <returns></returns>
        Dtos.ResidenceLife.MealPlanAssignment UpdateMealPlanAssignment(Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignment);
    }
}
