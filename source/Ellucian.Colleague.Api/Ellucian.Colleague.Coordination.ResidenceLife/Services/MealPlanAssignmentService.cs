﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.ResidenceLife;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;
using Ellucian.Colleague.Domain.ResidenceLife.Repositories;
using Ellucian.Colleague.Dtos.ResidenceLife;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Repositories;
using slf4net;
using Ellucian.Web.Dependency;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Services
{
    /// <summary>
    /// Implementation of the IMealPlanAssignmentService coordination layer interface
    /// </summary>
    [RegisterType]
    public class MealPlanAssignmentService : BaseCoordinationService, IMealPlanAssignmentService
    {
        private IMealPlanRepository mealPlanRepository;
        
        /// <summary>
        /// Constructor for the service
        /// </summary>
        /// <param name="adapterRegistry">An adapter registry</param>
        /// <param name="currentUserFactory">A current user factory</param>
        /// <param name="roleRepository">A role repository</param>
        /// <param name="logger">A logger</param>
        /// <param name="mealPlanAssignmentRepository">An implementation of the meal plan assignment repository interface </param>
        public MealPlanAssignmentService(IAdapterRegistry adapterRegistry, ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger,
                                        IMealPlanRepository mealPlanRepository)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.mealPlanRepository = mealPlanRepository;
        }

        /// <summary>
        /// Create a new meal plan assignment and persist through a repository
        /// </summary>
        /// <param name="housingAssignment">A fully populated new meal plan assignment dto</param>
        /// <returns></returns>
        public Dtos.ResidenceLife.MealPlanAssignment CreateMealPlanAssignment(Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignment)
        {
            // User must have proper permissions
            IEnumerable<string> userPermissions = GetUserPermissionCodes();
            if (!userPermissions.Contains(ResidenceLifePermissionCodes.CreateUpdateMealPlanAssignments))
            {
                logger.Error("Permissions error accessing CreateMealPlanAssignment. User " + CurrentUser.UserId);
                throw new PermissionsException();
            }

            // Cannot supply an ID to the create service
            if (!string.IsNullOrEmpty(mealPlanAssignment.Id))
            {
                string message = "Id cannot be supplied to the Create operation.";
                logger.Error("Exception in CreateMealPlanAssignment. " + message);
                throw new ArgumentException(message);
            }

            Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignmentEntity = CreateMealPlanAssignmentEntityFromDto(mealPlanAssignment);

            try
            {
                // Repository Add method
                Ellucian.Colleague.Domain.ResidenceLife.Entities.MealPlanAssignment newMealPlanAssignment =
                    mealPlanRepository.AddMealPlanAssignment(mealPlanAssignmentEntity);

                // Convert housing assignment domain entity to DTO
                var mealPlanAssignmentToDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.ResidenceLife.Entities.MealPlanAssignment,
                    Ellucian.Colleague.Dtos.ResidenceLife.MealPlanAssignment>();
                return mealPlanAssignmentToDtoAdapter.MapToType(newMealPlanAssignment);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// Update an existing meal plan assignment entity through a repository
        /// </summary>
        /// <param name="housingAssignment">A fully populated meal plan assignment dto containing updated attributes</param>
        /// <returns></returns>
        public Ellucian.Colleague.Dtos.ResidenceLife.MealPlanAssignment UpdateMealPlanAssignment(Ellucian.Colleague.Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignment)
        {

            // User must have proper permissions
            IEnumerable<string> userPermissions = GetUserPermissionCodes();
            if (!userPermissions.Contains(ResidenceLifePermissionCodes.CreateUpdateMealPlanAssignments))
            {
                logger.Error("Permissions error accessing UpdateMealPlanAssignment. User " + CurrentUser.UserId);
                throw new PermissionsException();
            }

            // Must specify an ID to update
            if ((mealPlanAssignment.Id == null) && (mealPlanAssignment.ExternalIds == null || mealPlanAssignment.ExternalIds.Count == 0))
            {
                string message = "An Id or External Id must be supplied to the update operation.";
                logger.Error("Exception in UpdateMealPlanAssignment. " + message);
                throw new ArgumentException(message);
            }
            Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignmentEntity = CreateMealPlanAssignmentEntityFromDto(mealPlanAssignment);

            try
            {
                // Repository Update method
                Ellucian.Colleague.Domain.ResidenceLife.Entities.MealPlanAssignment newMealPlanAssignment =
                    mealPlanRepository.UpdateMealPlanAssignment(mealPlanAssignmentEntity);

                // Convert housing assignment domain entity to DTO
                var mealPlanAssignmentToDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.ResidenceLife.Entities.MealPlanAssignment,
                    Ellucian.Colleague.Dtos.ResidenceLife.MealPlanAssignment>();
                return mealPlanAssignmentToDtoAdapter.MapToType(newMealPlanAssignment);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }

        // Create a MealPlanAssignment domain entity from a MealPlanAssignment dto
        private Domain.ResidenceLife.Entities.MealPlanAssignment CreateMealPlanAssignmentEntityFromDto(Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentDto)
        {
            
            // Create meal plan assignment entity from the DTO.
            Domain.ResidenceLife.Entities.MealPlan mealPlan;
            try
            {
                // Obtain a MealPlan entity from the repository
                mealPlan = mealPlanRepository.Get(mealPlanAssignmentDto.MealPlanId);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }

            // Obtain a MealPlanAssignmentStatus entity from the repository
            Domain.ResidenceLife.Entities.MealPlanAssignmentStatus mealPlanAssignmentStatus;
            try
            {
                mealPlanAssignmentStatus = mealPlanRepository.MealPlanAssignmentStatuses.Single(s => s.Code == mealPlanAssignmentDto.CurrentStatusCode);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }

            Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignmentEntity = new Domain.ResidenceLife.Entities.MealPlanAssignment(
                mealPlanAssignmentDto.PersonId, mealPlan, mealPlanAssignmentDto.NumberOfRatePeriods, mealPlanAssignmentDto.StartDate,
                mealPlanAssignmentDto.EndDate, mealPlanAssignmentStatus, mealPlanAssignmentDto.CurrentStatusDate, mealPlanAssignmentDto.TermId,
                mealPlanAssignmentDto.UsedRatePeriods, mealPlanAssignmentDto.UsedPercent);

            mealPlanAssignmentEntity.CardNumber = mealPlanAssignmentDto.CardNumber;
            mealPlanAssignmentEntity.SetOverrideRate(mealPlanAssignmentDto.OverrideRate, mealPlanAssignmentDto.OverrideRateReason);
            mealPlanAssignmentEntity.OverrideChargeCode = mealPlanAssignmentDto.OverrideChargeCode;
            mealPlanAssignmentEntity.OverrideReceivableType = mealPlanAssignmentDto.OverrideReceivableType;
            mealPlanAssignmentEntity.OverrideRefundFormula = mealPlanAssignmentDto.OverrideRefundFormula;
            mealPlanAssignmentEntity.Comments = mealPlanAssignmentDto.Comments;
            if (mealPlanAssignmentDto.ExternalIds != null)
            {
                foreach (var externalId in mealPlanAssignmentDto.ExternalIds)
                {
                    mealPlanAssignmentEntity.AddExternalId(externalId.ExternalId, externalId.ExternalIdSource);
                }
            }
            mealPlanAssignmentEntity.Id = mealPlanAssignmentDto.Id;

            return mealPlanAssignmentEntity;
        }

        /// <summary>
        /// Get a meal plan assignment by the ID in the native system
        /// </summary>
        /// <param name="id">The ID of the meal plan assignment in the native system</param>
        /// <returns></returns>
        public Dtos.ResidenceLife.MealPlanAssignment GetMealPlanAssignment(string id)
        {

            // User must have proper permissions
            IEnumerable<string> userPermissions = GetUserPermissionCodes();
            if (!userPermissions.Contains(ResidenceLifePermissionCodes.ViewMealPlanAssignments))
            {
                logger.Error("Permissions error accessing GetMealPlanAssignment. User " + CurrentUser.UserId);
                throw new PermissionsException();
            }

            try
            {
                // Repository Get method
                Domain.ResidenceLife.Entities.MealPlanAssignment newMealPlanAssignment =
                    mealPlanRepository.GetMealPlanAssignment(id);

                // Convert meal plan assignment domain entity to DTO
                var mealPlanAssignmentToDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.ResidenceLife.Entities.MealPlanAssignment,
                    Ellucian.Colleague.Dtos.ResidenceLife.MealPlanAssignment>();
                return mealPlanAssignmentToDtoAdapter.MapToType(newMealPlanAssignment);
            
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw;
            }
        }
    }
}
