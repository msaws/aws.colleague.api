﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.ResidenceLife
{
    /// <summary>
    /// Constant permission code strings for residence life
    /// </summary>
    public class ResidenceLifePermissionCodes
    {
        public const string CreateUpdateHousingAssignments = "CREATE.UPDATE.HOUSING.ASSIGNMENTS";
        public const string ViewHousingAssignments = "VIEW.HOUSING.ASSIGNMENTS";
        public const string CreateUpdateMealPlanAssignments = "CREATE.UPDATE.MEAL.PLAN.ASSIGNMENTS";
        public const string ViewMealPlanAssignments = "VIEW.MEAL.PLAN.ASSIGNMENTS";
        /// <summary>
        /// Permission to create a Residence Life miscellaneous invoice
        /// </summary>
        public const string CreateRlInvoices = "CREATE.RL.INVOICES";
        /// <summary>
        /// Permission to create a Residence Life deposit
        /// </summary>
        public const string CreateRlDeposits = "CREATE.RL.DEPOSITS";
    }
}
