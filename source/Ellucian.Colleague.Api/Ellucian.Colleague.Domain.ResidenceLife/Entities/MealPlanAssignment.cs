﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.ResidenceLife.Entities
{
    /// <summary>
    /// This class represents the assignment of a person to a meal plan
    /// </summary>
    [Serializable]
    public class MealPlanAssignment
    {

        /***********************
         * PRIVATE PROPERTIES
         ***********************/
        private string id;
        private List<ResidenceLifeExternalId> externalIds = new List<ResidenceLifeExternalId>();
        private string personId;
        // Only the meal plan ID is persisted to the data store. But the MealPlan entity is populated in the object because
        // its attributes are used for business logic.
        private MealPlan mealPlan;
        private int numberOfRatePeriods;
        private DateTime startDate;
        private DateTime? endDate;
        // Only the Current Status code is persisted to the data store. But the MealPlanAssignmentStatus entity is populated in the 
        // object because its attributes are used for business logic.
        private MealPlanAssignmentStatus currentStatus;
        private DateTime currentStatusDate;
        private string termId;
        private int? usedRatePeriods;
        private int? usedPercent;
        private string cardNumber;
        private decimal? overrideRate;
        private string overrideRateReason;
        private string overrideChargeCode;
        private string overrideReceivableType;
        private string overrideRefundFormula;
        private string comments;

        /// <summary>
        /// Constructor for MealPlanAssignment.
        /// </summary>
        /// <param name="personId">ID of the Person</param>
        /// <param name="mealPlan">The MealPlan</param>
        /// <param name="numberOfRatePeriods">Number of rate periods</param>
        /// <param name="startDate">Start Date</param>
        /// <param name="endDate">End Date (optional)</param>
        /// <param name="currentStatus">Current Status</param>
        /// <param name="currentStatusDate">Current Status Date</param>
        /// <param name="termId">Term Id (required if the meal plan is billed by term.)</param>
        /// <param name="usedRatePeriods">Used Rate Periods (Used Rate Periods or Used Percent required if the Current Status is
        /// of the type of Early Termination</param>
        /// <param name="usedPercent">Used Percent (Used Rate Periods or Used Percent required if the Current Status is 
        /// of the type Early Termination</param>
        public MealPlanAssignment( string personId, MealPlan mealPlan, int numberOfRatePeriods, DateTime startDate, DateTime? endDate,
                                   MealPlanAssignmentStatus currentStatus, DateTime currentStatusDate, string termId,
                                   int? usedRatePeriods, int? usedPercent)
        {
            // Person ID validations
            if (string.IsNullOrWhiteSpace(personId))
            {
                throw new ArgumentNullException("Person ID is required.");
            }

            // Meal Plan validations
            if (string.IsNullOrEmpty(mealPlan.Id))
            {
                throw new ArgumentException("The Meal Plan must have an Id");
            }

            // Number of Rate Periods validations
            if (mealPlan.RatePeriod != null && mealPlan.RatePeriod == MealPlanRatePeriods.Term)
            {
                if (numberOfRatePeriods != 1)
                {
                    throw new ArgumentException("Number of Rate Periods must be 1 for a Term rate period.");
                }
            }
            else
            {
                if (numberOfRatePeriods < 1 || numberOfRatePeriods > 999)
                {
                    throw new ArgumentException("Number of Rate Periods must be between 1 and 999");
                }
            }

            // Start Date and End Date validations
            if (startDate == default(DateTime))
            {
                throw new ArgumentNullException("Start Date is required.");
            }
            if (startDate < mealPlan.StartDate)
            {
                throw new ArgumentException("Start Date cannot be earlier than the meal plan start date.");
            }

            if (mealPlan.EndDate.HasValue)
            {
                if (endDate == null || endDate.Value == default(DateTime))
                {
                    throw new ArgumentException("End Date must be specified when the meal plan specifies and end date.");
                }

                if (endDate > mealPlan.EndDate)
                {
                    throw new ArgumentException("End Date cannot be later than the Meal Plan end date.");
                }
            }

            if (endDate.HasValue && endDate.Value != default(DateTime))
            {
                if (endDate < startDate)
                {
                    throw new ArgumentException("End Date cannot be earlier than Start Date.");
                }
            }

            // Current Status Date validations
            if (currentStatusDate == default(DateTime))
            {
                throw new ArgumentNullException("Current Status Date is required.");
            }

            // Term validations
            if (mealPlan.RatePeriod != null && mealPlan.RatePeriod.Value == MealPlanRatePeriods.Term)
            {
                if (string.IsNullOrEmpty(termId))
                {
                    throw new ArgumentException("A Term Id must be specified if the rate period of the meal plan is term.");
                }
            }

            // Used Rate Periods and Used Percent validations
            if (usedRatePeriods.HasValue && usedRatePeriods.Value < 0)
            {
                throw new ArgumentException("Used Rate Periods must be zero or greater.");
            }
            if (usedRatePeriods.HasValue && usedRatePeriods.Value > numberOfRatePeriods)
            {
                throw new ArgumentException("Used Rate Periods cannot be more than the Number of Rate Periods.");
            }
            if (usedPercent.HasValue && (usedPercent.Value < 0 || usedPercent.Value > 100))
            {
                throw new ArgumentException("Used Percent must be between 0 and 100");
            }
            
            if (currentStatus.Type == MealPlanAssignmentStatusType.EarlyTermination)
            {
                // 4/16/15 BFE CR 456.19. A Terminated status is now allowed with no Used Rate Periods or Used Percent values.
                // The terminated status date is used instead. Remove the prior validation that required either Used Rate 
                // Periods or Used Percent with a terminated status. The check that both cannot be set together remains.
                if (usedPercent != null && usedRatePeriods != null)
                {
                    throw new ArgumentException("Only one of Used Percent or Used Rate Periods is allowed.");
                }
            }

            // Set properties
            this.personId = personId;
            this.mealPlan = mealPlan;
            this.numberOfRatePeriods = numberOfRatePeriods;
            this.startDate = startDate;
            this.endDate = endDate;
            this.currentStatus = currentStatus;
            this.currentStatusDate = currentStatusDate;
            this.termId = termId;
            this.usedRatePeriods = usedRatePeriods;
            this.usedPercent = usedPercent;

            ExternalIds = externalIds.AsReadOnly();
        }

        /*********************
         * PUBLIC PROPERTIES
         *********************/

        /// <summary>
        /// The unique identifier of the meal plan assignment.
        /// </summary>
        public string Id
        {
            get { return id; }
            set
            {
                // Cannot change the ID once set to a non-null, non-empty value
                if (!string.IsNullOrEmpty(id))
                {
                    if (string.IsNullOrEmpty(value) || value != id)
                    {
                        throw new ArgumentException("Cannot change Id once assigned a value.");
                    }
                }

                this.id = value;
            }
        }

        /// <summary>
        /// A list of external unique Ids for the meal plan assignment
        /// </summary>
        public ReadOnlyCollection<ResidenceLifeExternalId> ExternalIds { get; private set; }

        /// <summary>
        /// The identifier of the person in Colleague.
        /// </summary>
        public string PersonId { get { return personId; } }

        /// <summary>
        /// The identifier of the meal plan.
        /// </summary>
        public string MealPlanId{ get { return mealPlan.Id; } }

        /// <summary>
        /// Number of Rate Periods of the meal plan assignment
        /// </summary>
        public int NumberOfRatePeriods { get { return numberOfRatePeriods; } }

        /// <summary>
        /// The start date of the meal plan assignment.
        /// </summary>
        public DateTime StartDate { get { return startDate; } }

        /// <summary>
        /// The end date of the meal plan assignment.
        /// </summary>
        public DateTime? EndDate { get { return endDate; } }

        /// <summary>
        /// The code of the current status
        /// </summary>
        public string CurrentStatusCode { get { return currentStatus.Code; } }

        /// <summary>
        /// The date of the current status
        /// </summary>
        public DateTime CurrentStatusDate { get { return currentStatusDate; } }

        /// <summary>
        /// Term Id of the meal plan assignment
        /// </summary>
        public string TermId { get { return termId; } }

        /// <summary>
        /// Number of Rate Periods Used of the meal plan assignment. Applies to an early termination.
        /// </summary>
        public int? UsedRatePeriods { get { return usedRatePeriods; } }

        /// <summary>
        /// Percentage of the meal plan used. Applies to an early termination.
        /// </summary>
        public int? UsedPercent { get { return usedPercent; } }

        /// <summary>
        /// Card Number of the meal plan assignment
        /// </summary>
        public string CardNumber
        { 
            get { return cardNumber; }
            set { cardNumber = value; }
        }

        /// <summary>
        /// An override rate that replaces the rate derived from the meal plan
        /// </summary>
        public decimal? OverrideRate { get { return overrideRate; } }

        /// <summary>
        /// A reason code for an override rate
        /// </summary>
        public string OverrideRateReason { get { return overrideRateReason; } }

        /// <summary>
        /// An override AR code for the meal plan assignment rate
        /// </summary>
        public string OverrideChargeCode { get { return overrideChargeCode; } set { overrideChargeCode = value; } }

        /// <summary>
        /// An override AR type code for the meal plan assignment rate
        /// </summary>
        public string OverrideReceivableType { get { return overrideReceivableType; } set { overrideReceivableType = value; } }

        /// <summary>
        /// An override refund formula code for the meal plan assignment
        /// </summary>
        public string OverrideRefundFormula { get { return overrideRefundFormula; } set { overrideRefundFormula = value; } }

        /// <summary>
        /// Comments about the meal plan assignment.
        /// </summary>
        public string Comments { get { return comments; } set { comments = value; } }

        /**********
         * METHODS
         **********/
        /// <summary>
        /// Add an external Id to the meal plan assignment
        /// </summary>
        /// <param name="externalId">The external ID value</param>
        /// <param name="externalIdSource">The code for the source of the external ID</param>
        public void AddExternalId(string externalId, string externalIdSource)
        {
            // Only one record per source value allowed.
            if (externalIds.Any(e => e.ExternalIdSource == externalIdSource))
            {
                throw new ArgumentException("Only one External ID per Source allowed.");
            }
            externalIds.Add(new ResidenceLifeExternalId(externalId, externalIdSource));
        }

        /// <summary>
        /// Set the override rate and reason
        /// </summary>
        /// <param name="overrideRate">The override rate</param>
        /// <param name="overrideRateReason">The override rate reason. Required if override rate is not null</param>
        public void SetOverrideRate(decimal? overrideRate, string overrideRateReason)
        {
            if ((overrideRate.HasValue) && string.IsNullOrWhiteSpace(overrideRateReason))
            {
                throw new ArgumentException("The override rate reason must have a value if an override rate is specified.");
            }
            if (overrideRate.HasValue && overrideRate.Value < 0) 
            {
                throw new ArgumentException("The override rate may not be a negative number.");
            }

            this.overrideRate = overrideRate;
            this.overrideRateReason = overrideRateReason;            
        }

    }
}
