﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.ResidenceLife.Entities
{
    /// <summary>
    /// A building and optional room for a meal plan.
    /// </summary>
    [Serializable]
    public class MealPlanBuildingAndRoom : IEquatable<MealPlanBuildingAndRoom>
    {
        private string building;
        private string room;

        /// <summary>
        /// The building. Required.
        /// </summary>
        public string Building { get { return building; } }

        /// <summary>
        /// The room. Optional.
        /// </summary>
        public string Room { get { return room; } }

        /// <summary>
        /// Constructor for MealPlanBuildingAndRoom
        /// </summary>
        /// <param name="building">The building. Required. </param>
        /// <param name="room">The room. May be null or empty.</param>
        public MealPlanBuildingAndRoom(string building, string room)
        {
            if (string.IsNullOrEmpty(building) )
            {
                throw new ArgumentNullException("building");
            }

            this.building = building;
            this.room = room;
        }

        /// <summary>
        /// Override of object.Equals
        /// </summary>
        /// <param name="obj">The object to compare to this object</param>
        /// <returns></returns>
        public override bool Equals(System.Object obj)
        {
            // If parameter is null return false.
            if (obj == null)
            {
                return false;
            }

            // If parameter cannot be cast to MealPlanBuildingAndRoom return false.
            MealPlanBuildingAndRoom mealPlanBuildingAndRoom = obj as MealPlanBuildingAndRoom;
            if ((System.Object)mealPlanBuildingAndRoom == null)
            {
                return false;
            }

            return Equals(mealPlanBuildingAndRoom);
        }

        /// <summary>
        /// Override of MealPlanBuildingsAndRoom.Equals
        /// </summary>
        /// <param name="mealPlanBuildingAndRoom">The object to compare to this</param>
        /// <returns></returns>
        public bool Equals(MealPlanBuildingAndRoom mealPlanBuildingAndRoom)
        {
            // If parameter is null return false:
            if ((object)mealPlanBuildingAndRoom == null)
            {
                return false;
            }

            // All properties must match to be equal
            return (building == mealPlanBuildingAndRoom.Building) && (room == mealPlanBuildingAndRoom.room);
        }

        /// <summary>
        /// Override of GetHashCode. 
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (building== null ? 0 : building.GetHashCode()) ^ (room == null ? 0 : room.GetHashCode());
        }

    }
}
