﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.ResidenceLife.Entities
{
    /// <summary>
    /// An additional charge or credit against a housing assignment, in addition to the charge based 
    /// on the room rate.
    /// </summary>
    [Serializable]
    public class HousingAdditionalChargeOrCredit
    {
        private string chargeCode;
        private string description;
        private decimal amount;

        /// <summary>
        /// The charge code of the additional charge or credit. Required.
        /// </summary>
        public string ArCode { get { return chargeCode; } }

        /// <summary>
        /// A description of the additional charge or credit. Required.
        /// </summary>
        public string Description { get { return description; } }

        /// <summary>
        /// The amount of the charge or credit. A positive number is a charge. A negative number is a credit.
        /// Required. Absolute value must be greater than  0 and less than or equal to 999,999.99
        /// </summary>
        public decimal Amount{ get { return amount; } }

        /// <summary>
        /// Constructor for HousingAdditionalChargeOrCredit
        /// </summary>
        /// <param name="chargeCode">The charge code of the additional charge or credit</param>
        /// <param name="description">The description of the additional charge or credit</param>
        /// <param name="amount">The charge or credit. A charge is positive, a credit negative</param>
        public HousingAdditionalChargeOrCredit(string chargeCode, string description, decimal amount)
        {
            if (string.IsNullOrWhiteSpace(chargeCode))
            {
                throw new ArgumentNullException("chargeCode");
            }

            if (string.IsNullOrWhiteSpace(description))
            {
                throw new ArgumentNullException("description");
            }

            if (amount == 0M)
            {
                throw new ArgumentException("A non-zero amount must be specified");
            }

            if (Math.Abs((decimal)amount) > 999999.99M)
            {
                throw new ArgumentException("The amount cannot be larger than 999,999.99");
            }

            this.chargeCode = chargeCode;
            this.description = description;
            this.amount = amount;
        }
    }
}
