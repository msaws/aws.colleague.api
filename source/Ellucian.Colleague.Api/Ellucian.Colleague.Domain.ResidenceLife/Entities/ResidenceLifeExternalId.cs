﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.ResidenceLife.Entities
{
    /// <summary>
    /// An ID/Source pair that comprise an external unique ID for a residence life entity.
    /// </summary>
    [Serializable]
    public class ResidenceLifeExternalId
    {
        private string externalId;
        private string externalIdSource;

        /// <summary>
        /// The unique identifier of the entity in the external system.
        /// </summary>
        public string ExternalId { get { return externalId; } }

        /// <summary>
        /// The source of ExternalId
        /// </summary>
        public string ExternalIdSource { get { return externalIdSource; } }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="externalId">The external ID value</param>
        /// <param name="externalIdSource">The code for the source of the external ID</param>
        public ResidenceLifeExternalId(string externalId, string externalIdSource)
        {
            // Both must have a value
            if (string.IsNullOrWhiteSpace(externalId) || string.IsNullOrWhiteSpace(externalIdSource))
            {
                throw new ArgumentNullException();
            }

            this.externalId = externalId;
            this.externalIdSource = externalIdSource;
        }
    }
}
