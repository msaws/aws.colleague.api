﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.ResidenceLife.Entities
{
    /// <summary>
    /// This class represents a meal plan.
    /// </summary>
    [Serializable]
    public class MealPlan
    {     

        /***********************
         * PRIVATE PROPERTIES
         ***********************/
        private string id;
        private DateTime startDate;
        private DateTime? endDate;
        private List<string> locations = new List<string>();
        private List<MealPlanRate> rates = new List<MealPlanRate>();
        private string description;
        private List<string> mealTypes = new List<string>();
        private List<MealPlanBuildingAndRoom> buildingsAndRooms = new List<MealPlanBuildingAndRoom>();

        /***************
         * CONSTRUCTOR
         ***************/
        /// <summary>
        /// Constructor for MealPlan with required properties
        /// </summary>
        /// <param name="startDate">Starting date on which the meal plan is available</param>
        /// <param name="description">Description of the meal plan</param>
        public MealPlan(DateTime startDate, string description)
        {
            if (string.IsNullOrWhiteSpace(description))
            {
                throw new ArgumentNullException("Description is required.");
            }

            if (startDate == default(DateTime))
            {
                throw new ArgumentNullException("Start Date is required.");
            }

            this.startDate = startDate;
            this.description = description;

            // Initialize public readonly list properties as read only interface to the corresponding private list property
            Rates = rates.AsReadOnly();
            Locations = locations.AsReadOnly();
            MealTypes = mealTypes.AsReadOnly();
            BuildingsAndRooms = buildingsAndRooms.AsReadOnly();
        }

        /*********************
         * PUBLIC PROPERTIES
         *********************/
        /// <summary>
        /// The unique identifier of the meal plan.
        /// </summary>
        public string Id 
        { 
            get { return id; } 
            set
            {
                // Cannot change the ID once set to a non-null, non-empty value
                if (!string.IsNullOrEmpty(id))
                {
                    if (string.IsNullOrEmpty(value) || value != id)
                    {
                        throw new ArgumentException("Cannot change Id once assigned a value.");
                    }
                }

                this.id = value;
            }
        }

        /// <summary>
        /// The start date on which the meal plan is available
        /// </summary>
        public DateTime StartDate
        {
            get { return startDate; }
            set
            {
                // Assume the default value reflects an uninitialized value.
                if (value == default(DateTime))
                {
                    throw new ArgumentNullException("Start Date must have a value.");
                }

                if (endDate.HasValue && value > endDate.Value)
                {
                    throw new ArgumentException("Start Date cannot be later than End Date.");
                }

                this.startDate = value;
            }
        }

        /// <summary>
        /// The end date after which the meal plan is no longer available
        /// </summary>
        public DateTime? EndDate
        {
            get { return endDate; }
            set
            {
                // Treat an uninitialized value as null
                if (value == default(DateTime))
                {
                    value = null;
                }

                if (value.HasValue && value.Value < startDate)
                {
                    throw new ArgumentException("Start date cannot be earlier than end date");
                }

                this.endDate = value;
            }
        }

        /// <summary>
        /// A list of locations in which the meal plan is available.
        /// </summary>
        public ReadOnlyCollection<string> Locations {get; private set;}
        // Set via SetLocations

        /// <summary>
        /// The period to which the meal plan rate applies
        /// </summary>
        public MealPlanRatePeriods? RatePeriod { get; set; }

        /// <summary>
        /// The charge code of the meal plan. Required if a meal plan rate is specified.
        /// </summary>
        public string ChargeCode { get; private set;}
        // Set via SetRatesAndChargeCode

        /// <summary>
        /// A list of rates for the meal plan by effective date. Required if a charge code is specified.
        /// </summary>
        public ReadOnlyCollection<MealPlanRate> Rates { get; private set; }
        // Set via SetRatesAndChargeCode

        /// <summary>
        /// The charge code to use when the meal plan is canceled.
        /// </summary>
        public string CancelationChargeCode { get; set; }

        /// <summary>
        /// The description of the meal plan. Required.
        /// </summary>
        public string Description
        {
            get { return description; }
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    throw new ArgumentNullException("Description may not be null or blank.");
                }
                description = value;
            }
        }

        /// <summary>
        /// The class of the meal plan.
        /// </summary>
        public string MealClass { get; set; }
        
        /// <summary>
        /// The number of meals per Frequency
        /// </summary>
        public int? NumberOfMealsPer { get; set; }
        
        /// <summary>
        ///  The frequency that NumberOfMealsPer applies to
        /// </summary>
        public string MealsPerFrequency { get; set; }
        
        /// <summary>
        /// A list of  meal types of the meal plan.
        /// </summary>
        public ReadOnlyCollection<string> MealTypes { get; private set; }
        // Set via SetMealTypes
        
        /// <summary>
        /// A list of buildings and rooms that the meal plan applies to.
        /// </summary>
        public ReadOnlyCollection<MealPlanBuildingAndRoom> BuildingsAndRooms { get; private set; }
        // Set via SetBuildingsAndRooms
        
        /// <summary>
        /// Comments about the meal plan.
        /// </summary>
        public string Comments { get; set; }

        ///***********
        // * METHODS
        // ***********/

        /// <summary>
        /// Set the Locations property
        /// </summary>
        /// <param name="locations">A list of unique locations.</param>        
        public void SetLocations(List<string> locations)
        {
            // Empty locations not allowed
            if (locations != null)
            {
                if (locations.Any(l => l == string.Empty || l == null))
                {
                    throw new ArgumentException("A location value may not be null or empty.");
                }
            }

            // The list may not contain duplicates
            if (locations != null) 
            {
                if (locations.Count != locations.Distinct().Count()) 
                {
                    throw new ArgumentException("Each location can appear in the list only once.");
                }
            }
             
            // Replace the entire existing list with the new list.
            this.locations.Clear();

            // If the new list is null, just leave our internal list empty. This preserves the ReadOnlyList wrapper around locations.
            if (locations != null && locations.Count > 0)
            {
                // Add the new list to our existing internal list object.
                this.locations.AddRange(locations);
            }
        }

        /// <summary>
        /// Set the list of meal plan rates and a meal plan charge.
        /// Both rates and a charge code must have values, or both must be null.
        /// </summary>
        /// <param name="rates">A list of rates by effective date</param>
        /// <param name="chargeCode">Charge code. </param>
        public void SetRatesAndChargeCode(List<MealPlanRate> rates, string chargeCode)
        {
            // These two properties must either both have a value, or both be null
            if ( ((rates == null) || (rates.Count == 0)) && !string.IsNullOrEmpty(chargeCode)) 
            {
                throw new ArgumentException("Meal Plan Rates must be specified if a Charge Code is specified.");
            }

            if (((rates != null) && (rates.Count > 0)) && string.IsNullOrEmpty(chargeCode))
            {
                throw new ArgumentException("A Charge Code must be specified if Meal Plan Rates are specified.");
            }

            // The effective date of each meal plan rate must be unique in the list
            if (rates
                .GroupBy(r => r.EffectiveDate)
                .Where(g => g.Count() > 1)
                .Select(g => g.Key).Count<DateTime>() > 0)
            {
                throw new ArgumentException("Two Meal Plan Rates in the list cannot share the same Effective Date.");
            }

            // Assign the values
            this.rates.Clear();
            // If the new rates is null, just leave the list of rates cleared.
            // This preserves the ReadOnlyList wrapper around rates.
            if (rates != null)
            {
                this.rates.AddRange(rates);
            }
            this.ChargeCode = chargeCode;
        }

        /// <summary>
        /// Set the MealTypes property
        /// </summary>
        /// <param name="mealTypes">A list of meal types</param>
        public void SetMealTypes(List<string> mealTypes)
        {
            // Empty locations not allowed
            if (mealTypes != null)
            {
                if (mealTypes.Any(m => m == string.Empty || m == null))
                {
                    throw new ArgumentException("A Meal Type value may not be null or empty.");
                }
            }

            // The list may not contain duplicates
            if (mealTypes != null)
            {
                if (mealTypes.Count != mealTypes.Distinct().Count())
                {
                    throw new ArgumentException("Each Meal Type can appear in the list only once.");
                }
            }

            // Replace the entire existing list with the new list
            this.mealTypes.Clear();

            // If the new list is null, just leave our internal list empty.
            // This preserves the ReadOnlyList wrapper around mealTypes.
            if (mealTypes != null && mealTypes.Count > 0)
            {
                // Add the new list to our existing internal list object.
                this.mealTypes.AddRange(mealTypes);
            }
        }

        /// <summary>
        /// Set the BuildingsAndRooms property
        /// </summary>
        /// <param name="buildingsAndRooms"></param>
        public void SetBuildingsAndRooms(List<MealPlanBuildingAndRoom> buildingsAndRooms)
        {
            // The list may not contain duplicates
            if (buildingsAndRooms != null)
            {
                if (buildingsAndRooms.Count != buildingsAndRooms.Distinct().Count())
                {
                    throw new ArgumentException("Each Building and Room pair can appear in the list only once.");
                }
            }

            // Replace the entire existing list with the new list
            this.buildingsAndRooms.Clear();

            // If the new list is null, just leave our internal list empty.
            // This preserves the ReadOnlyList wrapper around buildingsAndRooms.
            if (buildingsAndRooms != null && buildingsAndRooms.Count > 0)
            {
                // Add the new list to our existing internal list object.
                this.buildingsAndRooms.AddRange(buildingsAndRooms);
            }
        }
    }
}
