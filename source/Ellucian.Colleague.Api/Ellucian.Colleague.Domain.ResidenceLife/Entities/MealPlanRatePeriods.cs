﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.ResidenceLife.Entities
{
    /// <summary>
    /// Meal plan rate periods
    /// </summary>
    [Serializable]
    public enum MealPlanRatePeriods
    {
        /// <summary>
        /// Rate applied per meal
        /// </summary>
        PerMeal,
        
        /// <summary>
        /// Rate applied per day
        /// </summary>
        Daily, 

        /// <summary>
        /// Rate applied per week
        /// </summary>
        Weekly,

        /// <summary>
        /// Rate applied per month
        /// </summary>
        Monthly,

        /// <summary>
        /// Rate applied per year
        /// </summary>
        Yearly,

        /// <summary>
        /// Rate applied per term
        /// </summary>
        Term
    }
}
