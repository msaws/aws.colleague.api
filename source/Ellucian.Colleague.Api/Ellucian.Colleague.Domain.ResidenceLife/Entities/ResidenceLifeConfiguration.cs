﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
namespace Ellucian.Colleague.Domain.ResidenceLife.Entities
{
    /// <summary>
    /// Configuration for Residence Life interface.
    /// </summary>
    [Serializable]
    public class ResidenceLifeConfiguration
    {
        private readonly string _invoiceReceivableTypeCode;
        private readonly string _invoiceTypeCode;
        private readonly string _externalSystemId;
        private readonly string _cashier;
        private readonly string _depositDistribution;

        /// <summary>
        /// The receivable type for invoice imported through Residence Life
        /// </summary>
        public string InvoiceReceivableTypeCode { get { return _invoiceReceivableTypeCode; } }

        /// <summary>
        /// The type for the invoice imported through Residence Life
        /// </summary>
        public string InvoiceTypeCode { get { return _invoiceTypeCode; } }

        // TODO SXO Rename so does not have "Id"
        /// <summary>
        /// The external system that generated the imported invoice
        /// </summary>
        public string ExternalSystemId { get { return _externalSystemId; } }

        /// <summary>
        /// The cashier to use for payments imported through Residence Life
        /// </summary>
        public string Cashier { get { return _cashier; } }

        /// <summary>
        /// The distribution to use for deposits imported through Residence Life
        /// </summary>
        public string DepositDistribution { get { return _depositDistribution; } }

        public ResidenceLifeConfiguration(string invoiceReceivableType, string invoiceType, string externalSystem, string cashier, string distribution)
        {
            if (string.IsNullOrEmpty(invoiceReceivableType))
            {
                throw new ArgumentNullException("invoiceReceivableType", "Invoice receivable type is required");
            }

            if (string.IsNullOrEmpty(invoiceType))
            {
                throw new ArgumentNullException("invoiceType", "Invoice type is required");
            }

            if (string.IsNullOrEmpty(externalSystem))
            {
                throw new ArgumentNullException("externalSystem", "External system is required");
            }

            // cashier and distribution must both be null or both have values
            if (string.IsNullOrEmpty(cashier) && !string.IsNullOrEmpty(distribution))
            {
                throw new ArgumentNullException("cashier", "Cashier must have a value when distribution has a value");
            }

            if (!string.IsNullOrEmpty(cashier) && string.IsNullOrEmpty(distribution))
            {
                throw new ArgumentNullException("distribution", "Distribution must have a value when cashier has a value");
            }
            
            _invoiceReceivableTypeCode = invoiceReceivableType;
            _invoiceTypeCode = invoiceType;
            _externalSystemId = externalSystem;
            _cashier = cashier;
            _depositDistribution = distribution;
        }

    }
}
