﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.ResidenceLife.Entities
{
    /// <summary>
    /// A rate and effective date for a meal plan.
    /// The rate is relative to the
    /// </summary>
    [Serializable]
    public struct MealPlanRate
    {
        private decimal rate;
        private DateTime effectiveDate;

        /// <summary>
        /// The dollar amount of the rate per rate period. (Rate period is
        /// stored in the MealPlan class.)
        /// </summary>
        public decimal Rate { get { return rate; } }

        /// <summary>
        /// The effective date of the rate
        /// </summary>
        public DateTime EffectiveDate { get { return effectiveDate; } }

        /// <summary>
        /// Constructor for MealPlanRate
        /// </summary>
        /// <param name="rate">The rate</param>
        /// <param name="effectiveDate">The effective date of the rate</param>
        public MealPlanRate(decimal rate, DateTime effectiveDate)
        {
            if (rate < 0M || rate > 99999.99M)
            {
                throw new ArgumentException("Rate must be between 0 and 99,999.99");
            }

            if (effectiveDate == default(DateTime))
            {
                throw new ArgumentException("Effective Date must be assigned a value.");
            }

            this.rate = rate;
            this.effectiveDate = effectiveDate;
        }
    }
}
