﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Entities;

namespace Ellucian.Colleague.Domain.ResidenceLife.Entities
{
    /// <summary>
    /// The status of a Meal Plan Assignment
    /// </summary>
    [Serializable]
    public class MealPlanAssignmentStatus : CodeItem
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="code">A unique code for the status</param>
        /// <param name="description">Description of the status</param>
        /// <param name="type">Type of the status</param>
        public MealPlanAssignmentStatus(string code, string description, MealPlanAssignmentStatusType type)
            : base(code, description)
        {
            Type = type;
        }
        
        public MealPlanAssignmentStatusType Type { get; private set; }
    }
}
