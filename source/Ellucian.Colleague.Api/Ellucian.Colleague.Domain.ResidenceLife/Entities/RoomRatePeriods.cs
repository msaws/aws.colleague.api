﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.ResidenceLife.Entities
{
    /// <summary>
    /// Room rate period values for a housing assignment.
    /// </summary>
    [Serializable]
    public enum RoomRatePeriods
    {
        /// <summary>
        /// Rate applied per day
        /// </summary>
        Daily, 

        /// <summary>
        /// Rate applied per week
        /// </summary>
        Weekly,

        /// <summary>
        /// Rate applied per month
        /// </summary>
        Monthly,

        /// <summary>
        /// Rate applied per year
        /// </summary>
        Yearly,

        /// <summary>
        /// Rate applied per term
        /// </summary>
        Term
    }
}
