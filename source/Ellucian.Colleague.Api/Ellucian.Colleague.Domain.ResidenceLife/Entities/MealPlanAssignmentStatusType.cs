﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.ResidenceLife.Entities
{
    /// <summary>
    /// A Meal Plan Assignment Status may be assigned one of these types, which indicate a 
    /// particular meaning for the processing of the meal plan assignment.
    /// </summary>
    [Serializable]
    public enum MealPlanAssignmentStatusType
    {

        /// <summary>
        /// The meal plan status has no explicit processing meaning
        /// </summary>
        None,

        /// <summary>
        /// The meal plan assignment is canceled
        /// </summary>
        Canceled, 

        /// <summary>
        /// The meal plan assignment has been terminated early
        /// </summary>
        EarlyTermination,

        /// <summary>
        /// The meal plan assignment will start late
        /// </summary>
        LateStart

    }
}
