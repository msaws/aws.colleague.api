﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.ResidenceLife.Entities
{
    /// <summary>
    /// This class represents a housing assignment. A housing assignment is the assignment of 
    /// one person to one residence room for a period of time.
    /// </summary>
    [Serializable]
    public class HousingAssignment
    {
        /***********************
         * PRIVATE PROPERTIES
         ***********************/

        private string id;
        private List<ResidenceLifeExternalId> externalIds = new List<ResidenceLifeExternalId>();
        private string personId;
        private DateTime startDate; 
        private DateTime endDate;   
        private string currentStatus;
        private DateTime currentStatusDate;
        private string building;
        private string room;
        private string termId;
        private string roomRateTable;
        private RoomRatePeriods? roomRatePeriod;
        private decimal? overrideRate;
        private string overrideRateReason;
        private string overrideChargeCode;
        private string overrideReceivableType;
        private string overrideRefundFormula;
        private List<HousingAdditionalChargeOrCredit> additionalChargesOrCredits = new List<HousingAdditionalChargeOrCredit>();

        /*********************
         * PUBLIC PROPERTIES
         *********************/

        /// <summary>
        /// The unique identifier of the housing assignment.
        /// </summary>
        public string Id 
        { 
            get { return id; } 
            set
            {
                // Cannot change the ID once set to a non-null, non-empty value
                if (!string.IsNullOrEmpty(id))
                {
                    if (string.IsNullOrEmpty(value) || value != id)
                    {
                        throw new ArgumentException("Cannot change Id once assigned a value.");
                    }
                }

                this.id = value;
            }
        }

        /// <summary>
        /// A list of external unique Ids for the housing assignment
        /// </summary>
        public ReadOnlyCollection<ResidenceLifeExternalId> ExternalIds { get; private set; }

        /// <summary>
        /// The identifier of the person in Colleague.
        /// </summary>
        public string PersonId { get { return personId; } }

        /// <summary>
        /// The start date of the housing assignment.
        /// </summary>
        public DateTime StartDate { get { return startDate; } }

        /// <summary>
        /// The end date of the housing assignment.
        /// </summary>
        public DateTime EndDate { get { return endDate; } }

        /// <summary>
        /// The current status of the housing assignment.
        /// </summary>
        public string CurrentStatus { get { return currentStatus; } }

        /// <summary>
        /// The effective date of the current status of the housing assignment.
        /// </summary>
        public DateTime CurrentStatusDate { get { return currentStatusDate; } }

        /// <summary>
        /// Building of the housing assignment
        /// </summary>
        public string Building { get { return building; } }

        /// <summary>
        /// Room of the housing assignment
        /// </summary>
        public string Room { get { return room; } }

        /// <summary>
        /// The term ID of the housing assignment.
        /// </summary>
        public string TermId { get { return termId; } }

        /// <summary>
        /// The room rate table of the housing assignment.
        /// </summary>
        public string RoomRateTable{ get { return roomRateTable; } }

        /// <summary>
        /// The rate period of the room rate table
        /// </summary>
        public RoomRatePeriods? RoomRatePeriod { get { return roomRatePeriod; } }

        /// <summary>
        /// An override rate that replaces the rate derived from the rate table
        /// </summary>
        public decimal? OverrideRate { get { return overrideRate; } }

        /// <summary>
        /// A reason code for an override rate
        /// </summary>
        public string OverrideRateReason { get { return overrideRateReason; } }

        /// <summary>
        /// An override AR code for the room assignment rate
        /// </summary>
        public string OverrideChargeCode { get { return overrideChargeCode; } }

        /// <summary>
        /// An override AR type code for the room assignment rate
        /// </summary>
        public string OverrideReceivableType { get { return overrideReceivableType; } }

        /// <summary>
        /// An override refund formula code for the room assignment
        /// </summary>
        public string OverrideRefundFormula { get { return overrideRefundFormula; } }

        /// <summary>
        /// A list of additional charges against the housing assignment.
        /// </summary>
        public ReadOnlyCollection<HousingAdditionalChargeOrCredit> AdditionalChargesOrCredits { get; private set; }

        /// <summary>
        /// A unique contract number associated with the housing assignment.
        /// </summary>
        public string Contract { get; set; }

        /// <summary>
        /// Comments about the housing assignment.
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// A code that identifies the type of staff member the room resident is.
        /// </summary>
        public string ResidentStaffIndicator { get; set; }

        /**************
         * CONSTRUCTOR
         **************/

        /// <summary>
        /// Constructor for HousingAssignment with required properties
        /// </summary>
        /// <param name="personId">ID of a person assigned to the room</param>
        /// <param name="startDate">Start Date of the housing assignment</param>
        /// <param name="endDate">End Date of the housing assignment</param>
        /// <param name="currentStatus">The code of the current status of the housing assignment</param>
        /// <param name="currentStatusDate">The effective date of the current status of the housing assignment</param>
        public HousingAssignment(string personId, DateTime startDate, DateTime endDate, string currentStatus, DateTime currentStatusDate)
        {
            if (string.IsNullOrWhiteSpace(personId))
            {
                throw new ArgumentNullException("personId");
            }

            if (string.IsNullOrWhiteSpace(currentStatus))
            {
                throw new ArgumentNullException("currentStatus");
            }

            if (startDate == default(DateTime))
            {
                throw new ArgumentNullException("startDate");
            }
            
            if (endDate == default(DateTime))
            {
                throw new ArgumentNullException("endDate");
            }

            if (currentStatusDate== default(DateTime))
            {
                throw new ArgumentNullException("currentStatusDate");
            }

            if (startDate > endDate)
            {
                throw new ApplicationException("Start date cannot be after the end date.");
            }

            // Set properties
            this.personId = personId;
            this.startDate = startDate;
            this.endDate = endDate;
            this.currentStatus = currentStatus;
            this.currentStatusDate = currentStatusDate;

            // Initialize public readonly list properties in relation to associated private list property
            AdditionalChargesOrCredits = additionalChargesOrCredits.AsReadOnly();
            ExternalIds = externalIds.AsReadOnly();
        }

        /***********
         * METHODS
         ***********/

        /// <summary>
        /// Add an external Id to the housing assignment
        /// </summary>
        /// <param name="externalId">The external ID value</param>
        /// <param name="externalIdSource">The code for the source of the external ID</param>
        public void AddExternalId(string externalId, string externalIdSource)
        {
            // Only one record per source value allowed.
            if (externalIds.Any(e => e.ExternalIdSource == externalIdSource))
            {
                throw new ApplicationException("Only one External ID per Source allowed.");
            }
            externalIds.Add(new ResidenceLifeExternalId(externalId, externalIdSource));
        }

        /// <summary>
        /// Set the building and/or room. 
        /// </summary>
        /// <param name="building">The ID of the building of the housing assignment</param>
        /// <param name="room">The ID of the room of the housing assignment. Building is required if room is not null</param>
        public void SetBuildingAndRoom(string building, string room)
        {
            if (!string.IsNullOrWhiteSpace(room) && string.IsNullOrWhiteSpace(building))
            {
                throw new ArgumentException("Room is not allowed without a building.");                    
            }

            this.building = building;
            this.room = room;            
        }

        /// <summary>
        /// Set rate information for housing assignment.
        /// </summary>
        /// <param name="termId">The term Id of the housing assignment</param>
        /// <param name="roomRateTable">The code for the room rate table</param>
        /// <param name="roomRatePeriod">The rate period</param>
        /// <param name="overrideRate">An override rate</param>
        /// <param name="overrideRateReason">An override rate reason</param>
        /// <param name="overrideChargeCode">An override charge code</param>
        /// <param name="overrideArType">An override receivable type</param>
        /// <param name="overrideRefundFormula">An override refund formula</param>
        public void SetRateInformation(string termId, string roomRateTable, RoomRatePeriods? roomRatePeriod, decimal? overrideRate, 
                                       string overrideRateReason, string overrideChargeCode, string overrideReceivableType, string overrideRefundFormula)
        {
            
            if (string.IsNullOrWhiteSpace(roomRateTable) && roomRatePeriod != null)
            {
                throw new ArgumentException("The room rate table must have a value if a room rate period is specified.");
            }

            if (!string.IsNullOrWhiteSpace(roomRateTable) && roomRatePeriod == null) 
            {
                // "valid" in the message because an invalid value passed to the endpoint will become a null in the DTO, which 
                // in turn may be passed to this method. I don't prefer to employ this knowledge within the domain, but this
                // is the best practical solution for the end users.
                throw new ArgumentException("The room rate period must have a valid value if a room rate table is specified.");
            }

            if ((roomRatePeriod != null) && (roomRatePeriod == RoomRatePeriods.Term) && string.IsNullOrWhiteSpace(termId))
            {
                throw new ApplicationException("The term Id must have a value if the room rate period is T for term.");
            }

            if ((overrideRate != null) && string.IsNullOrWhiteSpace(overrideRateReason)) 
            {
                throw new ArgumentException("The override rate reason must have a value if an override rate is specified.");
            }

            this.termId = termId;
            this.roomRateTable = roomRateTable;
            this.roomRatePeriod = roomRatePeriod;
            this.overrideRate = overrideRate;
            this.overrideRateReason = overrideRateReason;
            this.overrideChargeCode = overrideChargeCode;
            this.overrideReceivableType = overrideReceivableType;
            this.overrideRefundFormula = overrideRefundFormula;
        }

        /// <summary>
        /// Add an additional charge or credit to the housing assignment
        /// </summary>
        /// <param name="chargeCode">The charge code of the additional charge or credit</param>
        /// <param name="description">The description of the additional charge or credit</param>
        /// <param name="amount">The charge or credit. A charge is positive, a credit negative</param>
        public void AddAdditionalChargeOrCredit(string chargeCode, string description, decimal amount)
        {
            additionalChargesOrCredits.Add(new HousingAdditionalChargeOrCredit(chargeCode, description, amount));
        }

    }
}
