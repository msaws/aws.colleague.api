﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Domain.ResidenceLife.Entities;

namespace Ellucian.Colleague.Domain.ResidenceLife.Repositories
{
    /// <summary>
    /// Interface to the Residence Life Configuration repository
    /// </summary>
    public interface IResidenceLifeConfigurationRepository
    {
        /// <summary>
        /// Get the base configuration for Residence Life
        /// </summary>
        /// <returns>Residence Life configuration</returns>
        ResidenceLifeConfiguration GetResidenceLifeConfiguration();
    }
}
