﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;

namespace Ellucian.Colleague.Domain.ResidenceLife.Repositories
{
    /// <summary>
    /// Interface of the repository for the HousingAssignment domain entity
    /// </summary>
    public interface IHousingAssignmentRepository
    {
        /// <summary>
        /// Get a housing assignment entity by a unique external ID
        /// </summary>
        /// <param name="externalID">The external ID</param>
        /// <param name="externalIdSource">The corresponding external ID source</param>
        /// <returns>Returns a housing assignment domain entity</returns>
        HousingAssignment GetByExternalId(string externalID, string externalIdSource);

        /// <summary>
        /// Get a housing assignment entity by the Id in the native system
        /// </summary>
        /// <param name="id">The Id</param>
        /// <returns>Returns a housing assignment domain entity</returns>        
        HousingAssignment Get(string id);

        /// <summary>
        /// Persist a new housing assignment
        /// </summary>
        /// <param name="housingAssignment">The new housing assignment domain entity</param>
        /// <returns>Returns a get of housing assignment after persisting</returns>
        HousingAssignment Add(HousingAssignment housingAssignment);

        /// <summary>
        /// Persist an updated housing assignment
        /// </summary>
        /// <param name="housingAssignment">The updated housing assignment domain entity</param>
        /// <returns>Returns a get of housing assignment after persisting</returns>
        HousingAssignment Update(HousingAssignment housingAssignment);
    }
}
