﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;

namespace Ellucian.Colleague.Domain.ResidenceLife.Repositories
{
    /// <summary>
    /// Interface of the repository for the MealPlan domain entity.
    /// This interface also specifies the repository for related entities MealPlanAssignment and MealPlanAssignmentStatus
    /// </summary>
    public interface IMealPlanRepository
    {
        /// <summary>
        /// Get a MealPlan entity by the Id
        /// </summary>
        /// <param name="id">The Id</param>
        /// <returns>Returns a housing assignment domain entity</returns>        
        MealPlan Get(string id);

        /// <summary>
        /// Get a MealPlanAssignment entity by the Id
        /// </summary>
        /// <param name="id">The Id</param>
        /// <returns>Returns a meal plan assignment domain entity</returns>
        MealPlanAssignment GetMealPlanAssignment(string id);

        /// <summary>
        /// Return a list of MealPlanAssignmentStatus entities, based on the Colleague MEAL.ASSIGN.STATUSES code table
        /// </summary>
        IEnumerable<MealPlanAssignmentStatus> MealPlanAssignmentStatuses { get; }

        /// <summary>
        /// Persist a new meal plan assignment
        /// </summary>
        /// <param name="housingAssignment">The new meal plan assignment domain entity</param>
        /// <returns>Returns a get of meal plan assignment after persisting</returns>
        Domain.ResidenceLife.Entities.MealPlanAssignment AddMealPlanAssignment(Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignment);

        /// <summary>
        /// Persist an updated meal plan assignment
        /// </summary>
        /// <param name="housingAssignment">The updated meal plan assignment domain entity</param>
        /// <returns>Returns a get of meal plan assignment after persisting</returns>
        Domain.ResidenceLife.Entities.MealPlanAssignment UpdateMealPlanAssignment(Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignment);

    }
}

