//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 8/14/2014 9:35:21 AM by user kmf
//
//     Type: ENTITY
//     Entity: DEGREE_PLAN_TERMS
//     Application: ST
//     Environment: dvcoll_wstst01
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;
using Ellucian.Data.Colleague;

namespace Ellucian.Colleague.Data.Planning.DataContracts
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract(Name = "DegreePlanTerms")]
	[ColleagueDataContract(GeneratedDateTime = "8/14/2014 9:35:21 AM", User = "kmf")]
	[EntityDataContract(EntityName = "DEGREE_PLAN_TERMS", EntityType = "PHYS")]
	public class DegreePlanTerms : IColleagueEntity
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }		

		/// <summary>
		/// Record ID
		/// </summary>
		[DataMember]
		public string Recordkey { get; set; }
		
		public void setKey(string key)
		{
			Recordkey = key;
		}	
		
		/// <summary>
		/// CDD Name: DPT.DEGREE.PLAN
		/// </summary>
		[DataMember(Order = 0, Name = "DPT.DEGREE.PLAN")]
		public string DptDegreePlan { get; set; }
		
		/// <summary>
		/// CDD Name: DPT.TERM
		/// </summary>
		[DataMember(Order = 1, Name = "DPT.TERM")]
		public string DptTerm { get; set; }
		
		/// <summary>
		/// CDD Name: DPT.COURSES
		/// </summary>
		[DataMember(Order = 3, Name = "DPT.COURSES")]
		public List<string> DptCourses { get; set; }
		
		/// <summary>
		/// CDD Name: DPT.SECTIONS
		/// </summary>
		[DataMember(Order = 4, Name = "DPT.SECTIONS")]
		public List<string> DptSections { get; set; }
		
		/// <summary>
		/// CDD Name: DPT.CREDITS
		/// </summary>
		[DataMember(Order = 5, Name = "DPT.CREDITS")]
		[DisplayFormat(DataFormatString = "{0:N5}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public List<Decimal?> DptCredits { get; set; }
		
		/// <summary>
		/// CDD Name: DPT.ALTERNATE.FLAGS
		/// </summary>
		[DataMember(Order = 6, Name = "DPT.ALTERNATE.FLAGS")]
		public List<string> DptAlternateFlags { get; set; }
		
		/// <summary>
		/// CDD Name: DPT.GRADING.TYPE
		/// </summary>
		[DataMember(Order = 7, Name = "DPT.GRADING.TYPE")]
		public List<string> DptGradingType { get; set; }
		
		/// <summary>
		/// CDD Name: DPT.ADDED.BY
		/// </summary>
		[DataMember(Order = 12, Name = "DPT.ADDED.BY")]
		public List<string> DptAddedBy { get; set; }
		
		/// <summary>
		/// CDD Name: DPT.ADDED.ON.DATE
		/// </summary>
		[DataMember(Order = 13, Name = "DPT.ADDED.ON.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public List<DateTime?> DptAddedOnDate { get; set; }
		
		/// <summary>
		/// CDD Name: DPT.ADDED.ON.TIME
		/// </summary>
		[DataMember(Order = 14, Name = "DPT.ADDED.ON.TIME")]
		[DisplayFormat(DataFormatString = "{0:T}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public List<DateTime?> DptAddedOnTime { get; set; }
		
		/// <summary>
		/// CDD Name: DPT.PROTECTED
		/// </summary>
		[DataMember(Order = 15, Name = "DPT.PROTECTED")]
		public List<string> DptProtected { get; set; }

        /// <summary>
        /// Entity assocation member
        /// </summary>
        [DataMember]
		public List<DegreePlanTermsPlannedCourses> PlannedCoursesEntityAssociation { get; set; }
		
	
		// build up all the Associated objects and add them to the properties
		public void buildAssociations()
		{	
			// EntityAssociation Name: PLANNED_COURSES
			
			PlannedCoursesEntityAssociation = new List< DegreePlanTermsPlannedCourses >();
			if( DptCourses != null)
			{
				int numPlannedCourses = DptCourses.Count;
				for (int i = 0; i < numPlannedCourses; i++)
				{
					string value0 = "";
					value0 = DptCourses[i];

					string value1 = "";
					if (DptSections != null && i < DptSections.Count)
					{
						value1 = DptSections[i];
					}

					Decimal? value2 = null;
					if (DptCredits != null && i < DptCredits.Count)
					{
						value2 = DptCredits[i];
					}

					string value3 = "";
					if (DptAlternateFlags != null && i < DptAlternateFlags.Count)
					{
						value3 = DptAlternateFlags[i];
					}

					string value4 = "";
					if (DptGradingType != null && i < DptGradingType.Count)
					{
						value4 = DptGradingType[i];
					}

					string value5 = "";
					if (DptAddedBy != null && i < DptAddedBy.Count)
					{
						value5 = DptAddedBy[i];
					}

					DateTime? value6 = null;
					if (DptAddedOnDate != null && i < DptAddedOnDate.Count)
					{
						value6 = DptAddedOnDate[i];
					}

					DateTime? value7 = null;
					if (DptAddedOnTime != null && i < DptAddedOnTime.Count)
					{
						value7 = DptAddedOnTime[i];
					}

					string value8 = "";
					if (DptProtected != null && i < DptProtected.Count)
					{
						value8 = DptProtected[i];
				}

					PlannedCoursesEntityAssociation.Add(new DegreePlanTermsPlannedCourses( value0, value1, value2, value3, value4, value5, value6, value7, value8));
			}
			}
			   
		}
	}
	
	// EntityAssociation classes
	
	[Serializable]
	public class DegreePlanTermsPlannedCourses
	{
		public string DptCoursesAssocMember;	
		public string DptSectionsAssocMember;	
		public Decimal? DptCreditsAssocMember;	
		public string DptAlternateFlagsAssocMember;	
		public string DptGradingTypeAssocMember;	
		public string DptAddedByAssocMember;	
		public DateTime? DptAddedOnDateAssocMember;	
		public DateTime? DptAddedOnTimeAssocMember;	
		public string DptProtectedAssocMember;	
		public DegreePlanTermsPlannedCourses() {}
		public DegreePlanTermsPlannedCourses(
			string inDptCourses,
			string inDptSections,
			Decimal? inDptCredits,
			string inDptAlternateFlags,
			string inDptGradingType,
			string inDptAddedBy,
			DateTime? inDptAddedOnDate,
			DateTime? inDptAddedOnTime,
			string inDptProtected)
		{
			DptCoursesAssocMember = inDptCourses;
			DptSectionsAssocMember = inDptSections;
			DptCreditsAssocMember = inDptCredits;
			DptAlternateFlagsAssocMember = inDptAlternateFlags;
			DptGradingTypeAssocMember = inDptGradingType;
			DptAddedByAssocMember = inDptAddedBy;
			DptAddedOnDateAssocMember = inDptAddedOnDate;
			DptAddedOnTimeAssocMember = inDptAddedOnTime;
			DptProtectedAssocMember = inDptProtected;
		}
	}
}