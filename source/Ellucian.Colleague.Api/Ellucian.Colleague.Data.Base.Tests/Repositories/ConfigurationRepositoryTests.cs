﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Caching;
using System.Threading;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Data.Base.Repositories;
using Ellucian.Colleague.Data.Base.Transactions;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Data.Colleague;
using Ellucian.Web.Cache;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using Ellucian.Data.Colleague.DataContracts;

namespace Ellucian.Colleague.Data.Base.Tests.Repositories
{
    [TestClass]
    public class ConfigurationRepositoryTests : BaseRepositorySetup
    {
        #region Initialize and Cleanup
        ConfigurationRepository configRepository;
        static Collection<GetIntegrationConfigResponse> responses = TestCdmIntegrationRepository.Responses;
        static Collection<ElfTranslateTables> elfTranslateTables = TestElfTranslateTablesRepository.ElfTranslateTables;
        private HrwebDefaults expectedHrWebDefaults = new HrwebDefaults()
        {
            HrwebW2oConText = "I consent to receive my W-2 online.",
            HrwebW2oWhldText = "I withhold consent to receive my W-2 online.",

            Hrweb1095cConText = "I consent to receive my 1095-C online.",
            Hrweb1095cWhldText = "I withhold consent to receive my 1095-C online."
        };

        private Parm1098 parm1098DataContract = new Parm1098()
        {
            P1098ConsentText = "I consent to receive my 1098 online.",
            P1098WhldConsentText = "I withhold consent to receive my 1098 online.",
            P1098TTaxForm = "1098T"
        };
        private CorewebDefaults corewebDefaults;
        private Dflts dflts;

        private QtdYtdParameterW2 expectedQtdYtdParameterW2 = new QtdYtdParameterW2()
        {
            QypWebW2Years = new List<string>() { "2010", "2011", "2012", "2013", "2014" },
            QypWebW2AvailableDates = new List<DateTime?>()
            {
                new DateTime(2011, 01, 24),
                new DateTime(2012, 01, 26),
                new DateTime(2013, 02, 03),
                new DateTime(2014, 01, 16),
                new DateTime(2015, 01, 20),
            }
        };

        private QtdYtdParameter1095C expectedQtdYtdParameter1095C = new QtdYtdParameter1095C()
        {
            QypWeb1095cYears = new List<string>() { "2010", "2011", "2012", "2013", "2014" },
            QypWeb1095cAvailDates = new List<DateTime?>()
            {
                new DateTime(2011, 01, 24),
                new DateTime(2012, 01, 26),
                new DateTime(2013, 02, 03),
                new DateTime(2014, 01, 16),
                new DateTime(2015, 01, 20),
            }
        };

        private Collection<TaxForm1098Years> taxForm1098YearsContracts = new Collection<TaxForm1098Years>()
        {
            new TaxForm1098Years() { Recordkey = "1", Tf98yTaxYear = 2015, Tf98yWebEnabled = "Y" },
            new TaxForm1098Years() { Recordkey = "1", Tf98yTaxYear = 2014, Tf98yWebEnabled = "Y" },
            new TaxForm1098Years() { Recordkey = "1", Tf98yTaxYear = 2013, Tf98yWebEnabled = "Y" },
        };

        private TaxFormStatus taxFormStatusContract = new TaxFormStatus()
        {
            Recordkey = "1098T",
            TfsGenDate = new DateTime(2015, 1, 1),
            TfsStatus = "",
            TfsTaxYear = "2015"
        };

        [TestInitialize]
        public void Initialize()
        {
            // Initialize Mock framework
            MockInitialize();

            // Build the test repository
            configRepository = BuildValidRepository();
        }

        [TestCleanup]
        public void Cleanup()
        {
            configRepository = null;
        }

        #endregion

        [TestClass]
        public class ConfigurationRepository_GetDefaultsConfiguration : ConfigurationRepositoryTests
        {
            [TestMethod]
            public void ConfigurationRepository_GetDefaultsConfiguration_Verify()
            {
                var config = this.configRepository.GetDefaultsConfiguration();
                Assert.AreEqual(config.CampusCalendarId, "MAIN");
            }
        }

        [TestClass]
        public class ConfigurationRepository_GetIntegrationConfiguration : ConfigurationRepositoryTests
        {
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ConfigurationRepository_GetIntegrationConfiguration_NullConfigId()
            {
                var result = configRepository.GetIntegrationConfiguration(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ConfigurationRepository_GetIntegrationConfiguration_EmptyConfigId()
            {
                var result = configRepository.GetIntegrationConfiguration(string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidOperationException))]
            public void ConfigurationRepository_GetIntegrationConfiguration_NoRecord()
            {
                var result = configRepository.GetIntegrationConfiguration("NORECORD");
            }

            [TestMethod]
            public void ConfigurationRepository_GetIntegrationConfiguration_ValidRecord_NoMappings()
            {
                var result = configRepository.GetIntegrationConfiguration("TEST");
                Assert.AreEqual(responses[0].CdmIntegrationId, result.Id);
                Assert.AreEqual(new System.Uri(responses[0].CintServerBaseUrl).Host, result.AmqpMessageServerBaseUrl);
                Assert.AreEqual(new System.Uri(responses[0].CintServerBaseUrl).Scheme == "https", result.AmqpConnectionIsSecure);
                Assert.AreEqual(new System.Uri(responses[0].CintServerBaseUrl).Port, result.AmqpMessageServerPortNumber);
                Assert.AreEqual(responses[0].CintServerUsername, result.AmqpUsername);
                Assert.AreEqual(responses[0].CintServerPassword, result.AmqpPassword);
                Assert.AreEqual(responses[0].CintBusEventExchange, result.BusinessEventExchangeName);
                Assert.AreEqual(responses[0].CintBusEventQueue, result.BusinessEventQueueName);
                Assert.AreEqual(responses[0].CintOutboundExchange, result.OutboundExchangeName);
                Assert.AreEqual(responses[0].CintInboundExchange, result.InboundExchangeName);
                Assert.AreEqual(responses[0].CintInboundQueue, result.InboundExchangeQueueName);
                Assert.AreEqual(responses[0].CintApiUsername, result.ApiUsername);
                Assert.AreEqual(responses[0].CintApiPassword, result.ApiPassword);
                Assert.AreEqual(responses[0].CintApiErp, result.ApiErpName);
                Assert.AreEqual(responses[0].CintServerVirtHost, result.AmqpMessageServerVirtualHost);
                Assert.AreEqual(responses[0].CintServerTimeout, result.AmqpMessageServerConnectionTimeout);
                Assert.AreEqual(responses[0].CintServerAutorecoverFlag, result.AutomaticallyRecoverAmqpMessages);
                Assert.AreEqual(responses[0].CintServerHeartbeat, result.AmqpMessageServerHeartbeat);
                Assert.AreEqual(responses[0].CintUseIntegrationHub, result.UseIntegrationHub);
                Assert.AreEqual(responses[0].CintHubApiKey, result.ApiKey);
                Assert.AreEqual(responses[0].CintHubTokenUrl, result.TokenUrl);
                Assert.AreEqual(responses[0].CintHubSubscribeUrl, result.SubscribeUrl);
                Assert.AreEqual(responses[0].CintHubPublishUrl, result.PublishUrl);
                Assert.AreEqual(responses[0].CintHubErrorUrl, result.ErrorUrl);
                Assert.AreEqual(responses[0].CintHubMediaType, result.HubMediaType);
                Assert.AreEqual(responses[0].ApiBusEventMap.Count, result.ResourceBusinessEventMappings.Count);
            }

            [TestMethod]
            public void ConfigurationRepository_GetIntegrationConfiguration_ValidRecordWithMappings()
            {
                var result = configRepository.GetIntegrationConfiguration("TEST2");
                Assert.AreEqual(responses[1].CdmIntegrationId, result.Id);
                Assert.AreEqual(new System.Uri(responses[1].CintServerBaseUrl).Host, result.AmqpMessageServerBaseUrl);
                Assert.AreEqual(new System.Uri(responses[1].CintServerBaseUrl).Scheme == "https", result.AmqpConnectionIsSecure);
                Assert.AreEqual(new System.Uri(responses[1].CintServerBaseUrl).Port, result.AmqpMessageServerPortNumber);
                Assert.AreEqual(responses[1].CintServerUsername, result.AmqpUsername);
                Assert.AreEqual(responses[1].CintServerPassword, result.AmqpPassword);
                Assert.AreEqual(responses[1].CintBusEventExchange, result.BusinessEventExchangeName);
                Assert.AreEqual(responses[1].CintBusEventQueue, result.BusinessEventQueueName);
                Assert.AreEqual(responses[1].CintOutboundExchange, result.OutboundExchangeName);
                Assert.AreEqual(responses[1].CintInboundExchange, result.InboundExchangeName);
                Assert.AreEqual(responses[1].CintInboundQueue, result.InboundExchangeQueueName);
                Assert.AreEqual(responses[1].CintApiUsername, result.ApiUsername);
                Assert.AreEqual(responses[1].CintApiPassword, result.ApiPassword);
                Assert.AreEqual(responses[1].CintApiErp, result.ApiErpName);
                Assert.AreEqual(responses[1].CintServerVirtHost, result.AmqpMessageServerVirtualHost);
                Assert.AreEqual(responses[1].CintServerTimeout, result.AmqpMessageServerConnectionTimeout);
                Assert.AreEqual(responses[1].CintServerAutorecoverFlag, result.AutomaticallyRecoverAmqpMessages);
                Assert.AreEqual(responses[1].CintServerHeartbeat, result.AmqpMessageServerHeartbeat);
                Assert.AreEqual(responses[1].CintUseIntegrationHub, result.UseIntegrationHub);
                Assert.AreEqual(responses[1].CintHubApiKey, result.ApiKey);
                Assert.AreEqual(responses[1].CintHubTokenUrl, result.TokenUrl);
                Assert.AreEqual(responses[1].CintHubSubscribeUrl, result.SubscribeUrl);
                Assert.AreEqual(responses[1].CintHubPublishUrl, result.PublishUrl);
                Assert.AreEqual(responses[1].CintHubErrorUrl, result.ErrorUrl);
                Assert.AreEqual(responses[1].CintHubMediaType, result.HubMediaType);

                Assert.AreEqual(responses[1].ApiBusEventMap.Count, result.ResourceBusinessEventMappings.Count);
                for (int i = 0; i < result.ResourceBusinessEventMappings.Count; i++)
                {
                    Assert.AreEqual(responses[1].ApiBusEventMap[i].CintApiResource, result.ResourceBusinessEventMappings[i].ResourceName);
                    Assert.AreEqual(responses[1].ApiBusEventMap[i].CintApiRsrcSchemaVer, result.ResourceBusinessEventMappings[i].ResourceVersion);
                    Assert.AreEqual(responses[1].ApiBusEventMap[i].CintApiPath, result.ResourceBusinessEventMappings[i].PathSegment);
                    Assert.AreEqual(responses[1].ApiBusEventMap[i].CintApiBusEvents, result.ResourceBusinessEventMappings[i].BusinessEvent);
                }

                Assert.AreEqual(responses[1].CintInboundRoutingKeys.Count, result.InboundExchangeRoutingKeys.Count);
                for (int j = 0; j < result.InboundExchangeRoutingKeys.Count; j++)
                {
                    Assert.AreEqual(responses[1].CintInboundRoutingKeys[j], result.InboundExchangeRoutingKeys[j]);
                }

                Assert.AreEqual(responses[1].CintBusEventRoutingKeys.Count, result.BusinessEventRoutingKeys.Count);
                for (int k = 0; k < result.BusinessEventRoutingKeys.Count; k++)
                {
                    Assert.AreEqual(responses[1].CintBusEventRoutingKeys[k], result.BusinessEventRoutingKeys[k]);
                }
            }
        }

        [TestClass]
        public class ConfigurationRepository_GetExternalMapping : ConfigurationRepositoryTests
        {
            [TestInitialize]
            public void GetExternalMapping_Initialize()
            {
                var corruptRecord = new ElfTranslateTables() { Recordkey = "LDM.TEST" };
                dataReaderMock.Setup<ElfTranslateTables>(
                    reader => reader.ReadRecord<ElfTranslateTables>(It.IsAny<string>(), It.IsAny<bool>()))
                    .Returns<string, bool>((id, replaceVm) =>
                    {
                        if (id == corruptRecord.Recordkey)
                        {
                            return corruptRecord;
                        }
                        else
                        {
                            var table = elfTranslateTables.FirstOrDefault(x => x.Recordkey == id);
                            return table;
                        }
                    }
                );
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ConfigurationRepository_GetExternalMapping_NullId()
            {
                var map = this.configRepository.GetExternalMapping(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void ConfigurationRepository_GetExternalMapping_EmptyId()
            {
                var map = this.configRepository.GetExternalMapping(string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public void ConfigurationRepository_GetExternalMapping_InvalidId()
            {
                var map = this.configRepository.GetExternalMapping("INVALID");
            }

            [TestMethod]
            public void ConfigurationRepository_GetExternalMapping_Verify()
            {
                for (int i = 0; i < elfTranslateTables.Count; i++)
                {
                    var map = this.configRepository.GetExternalMapping(elfTranslateTables[i].Recordkey);
                    Assert.AreEqual(elfTranslateTables[i].Recordkey, map.Code);
                    Assert.AreEqual(elfTranslateTables[i].ElftDesc, map.Description);
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ConfigurationException))]
            public void ConfigurationRepository_GetExternalMapping_CorruptRecord()
            {
                var map = this.configRepository.GetExternalMapping("LDM.TEST");
            }
        }

        [TestClass, System.Runtime.InteropServices.GuidAttribute("23894E0F-C5FD-4566-A92B-776992289D6A")]
        public class ConfigurationRepository_GetTaxFormConfiguration : ConfigurationRepositoryTests
        {
            [TestMethod]
            public async Task GetTaxFormConfigurationAsync_Success_W2()
            {
                var actualConfiguration = await this.configRepository.GetTaxFormConsentConfigurationAsync(TaxForms.FormW2);

                // Check that the W-2 consent paragraphs are the same
                Assert.AreEqual(expectedHrWebDefaults.HrwebW2oConText, actualConfiguration.ConsentParagraphs.ConsentText);
                Assert.AreEqual(expectedHrWebDefaults.HrwebW2oWhldText, actualConfiguration.ConsentParagraphs.ConsentWithheldText);

                var actualAvailabilityConfiguration = await this.configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.FormW2);
                // Make sure the domain entity set has the same number of year/date entries as the data contract set for W-2.
                Assert.AreEqual(expectedQtdYtdParameterW2.WebW2ParametersEntityAssociation.Count, actualAvailabilityConfiguration.Availabilities.Count);

                // Make sure each data contract entry is reflected in the domain entity set for W-2.
                foreach (var dataContract in expectedQtdYtdParameterW2.WebW2ParametersEntityAssociation)
                {
                    // Get the domain entity with matching data
                    var domainEntity = actualAvailabilityConfiguration.Availabilities.Where(x =>
                        x.TaxYear == dataContract.QypWebW2YearsAssocMember
                        && x.Available == DateTime.Compare(dataContract.QypWebW2AvailableDatesAssocMember.Value, DateTime.Now) <= 0).ToList();

                    // Make sure the domainEntity list contains only one object.
                    Assert.AreEqual(1, domainEntity.Count);
                }
            }

            [TestMethod]
            public async Task GetTaxFormConfigurationAsync_Success_1095C()
            {
                var actualConfiguration = await this.configRepository.GetTaxFormConsentConfigurationAsync(TaxForms.Form1095C);

                // Check that the 1095-C consent paragraphs are the same
                Assert.AreEqual(expectedHrWebDefaults.Hrweb1095cConText, actualConfiguration.ConsentParagraphs.ConsentText);
                Assert.AreEqual(expectedHrWebDefaults.Hrweb1095cWhldText, actualConfiguration.ConsentParagraphs.ConsentWithheldText);

                var actualAvailabilityConfiguration = await this.configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.Form1095C);
                // Make sure the domain entity set has the same number of year/date entries as the data contract set for 1095-C.
                Assert.AreEqual(expectedQtdYtdParameter1095C.Qyp1095cParametersEntityAssociation.Count, actualAvailabilityConfiguration.Availabilities.Count);

                foreach (var dataContract in expectedQtdYtdParameter1095C.Qyp1095cParametersEntityAssociation)
                {
                    // Get the domain entity with matching data
                    var domainEntity = actualAvailabilityConfiguration.Availabilities.Where(x =>
                        x.TaxYear == dataContract.QypWeb1095cYearsAssocMember
                        && x.Available == DateTime.Compare(dataContract.QypWeb1095cAvailDatesAssocMember.Value, DateTime.Now) <= 0).ToList();

                    // Make sure the domainEntity list contains only one object.
                    Assert.AreEqual(1, domainEntity.Count);
                }
            }

            [TestMethod]
            public async Task GetTaxFormConfigurationAsync_Success_1098()
            {
                var actualConfiguration = await this.configRepository.GetTaxFormConsentConfigurationAsync(TaxForms.Form1098);

                // Check that the 1095-C consent paragraphs are the same
                Assert.AreEqual(parm1098DataContract.P1098ConsentText, actualConfiguration.ConsentParagraphs.ConsentText);
                Assert.AreEqual(parm1098DataContract.P1098WhldConsentText, actualConfiguration.ConsentParagraphs.ConsentWithheldText);

            }

            [TestMethod]
            public async Task GetTaxFormConfigurationAsync_DataReaderReturnsNull_W2()
            {
                dataReaderMock.Setup<Task<HrwebDefaults>>(datareader => datareader.ReadRecordAsync<HrwebDefaults>(It.IsAny<string>(), It.IsAny<string>(), true)).Returns(() =>
                {
                    HrwebDefaults defaults = new HrwebDefaults();
                    defaults = null;
                    return Task.FromResult(defaults);
                });

                dataReaderMock.Setup<Task<QtdYtdParameterW2>>(datareader => datareader.ReadRecordAsync<QtdYtdParameterW2>(It.IsAny<string>(), It.IsAny<string>(), true)).Returns(() =>
                {
                    QtdYtdParameterW2 parameters = new QtdYtdParameterW2();
                    parameters = null;
                    return Task.FromResult(parameters);
                });

                var actualConfiguration = await this.configRepository.GetTaxFormConsentConfigurationAsync(TaxForms.FormW2);

                // Check W-2
                Assert.IsTrue(actualConfiguration is TaxFormConfiguration);
                Assert.AreEqual(null, actualConfiguration.ConsentParagraphs.ConsentText);
                Assert.AreEqual(null, actualConfiguration.ConsentParagraphs.ConsentWithheldText);
                Assert.AreEqual(0, actualConfiguration.Availabilities.Count);
            }

            [TestMethod]
            public async Task GetTaxFormConfigurationAsync_DataReaderReturnsNull_1095C()
            {
                dataReaderMock.Setup<Task<HrwebDefaults>>(datareader => datareader.ReadRecordAsync<HrwebDefaults>(It.IsAny<string>(), It.IsAny<string>(), true)).Returns(() =>
                {
                    HrwebDefaults defaults = new HrwebDefaults();
                    defaults = null;
                    return Task.FromResult(defaults);
                });

                dataReaderMock.Setup<Task<QtdYtdParameter1095C>>(datareader => datareader.ReadRecordAsync<QtdYtdParameter1095C>(It.IsAny<string>(), It.IsAny<string>(), true)).Returns(() =>
                {
                    QtdYtdParameter1095C parameters = new QtdYtdParameter1095C();
                    parameters = null;
                    return Task.FromResult(parameters);
                });

                var actualConfiguration = await this.configRepository.GetTaxFormConsentConfigurationAsync(TaxForms.Form1095C);

                // Check 1095-C
                Assert.IsTrue(actualConfiguration is TaxFormConfiguration);
                Assert.AreEqual(null, actualConfiguration.ConsentParagraphs.ConsentText);
                Assert.AreEqual(null, actualConfiguration.ConsentParagraphs.ConsentWithheldText);
                Assert.AreEqual(0, actualConfiguration.Availabilities.Count);
            }

            [TestMethod]
            public async Task GetTaxFormConfigurationAsync_DataReaderReturnsNull_1098()
            {
                dataReaderMock.Setup<Task<Parm1098>>(datareader => datareader.ReadRecordAsync<Parm1098>(It.IsAny<string>(), It.IsAny<string>(), true)).Returns(() =>
                {
                    Parm1098 defaults = new Parm1098();
                    defaults = null;
                    return Task.FromResult(defaults);
                });

                var actualConfiguration = await this.configRepository.GetTaxFormConsentConfigurationAsync(TaxForms.Form1098);

                // Check 1098
                Assert.IsTrue(actualConfiguration is TaxFormConfiguration);
                Assert.AreEqual(null, actualConfiguration.ConsentParagraphs.ConsentText);
                Assert.AreEqual(null, actualConfiguration.ConsentParagraphs.ConsentWithheldText);
                Assert.AreEqual(0, actualConfiguration.Availabilities.Count);
            }

            [TestMethod]
            public async Task GetTaxFormConfigurationAsync_NullTaxYear_W2()
            {
                expectedQtdYtdParameterW2.QypWebW2Years[0] = null;

                var actualAvailabilityConfiguration = await this.configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.FormW2);

                // Make sure the domain entity set has the same number of year/date entries as the data contract set for W-2
                Assert.AreEqual(expectedQtdYtdParameterW2.WebW2ParametersEntityAssociation.Count - 1, actualAvailabilityConfiguration.Availabilities.Count);

                // Make sure each domain entity returned is reflected in the original data contract set for W-2
                foreach (var domainEntity in actualAvailabilityConfiguration.Availabilities)
                {
                    // Get the domain entity with matching data
                    var dataContract = expectedQtdYtdParameterW2.WebW2ParametersEntityAssociation.Where(x =>
                        x.QypWebW2YearsAssocMember == domainEntity.TaxYear
                        && (DateTime.Compare(x.QypWebW2AvailableDatesAssocMember.Value, DateTime.Now) <= 0) == domainEntity.Available).ToList();

                    // Make sure the domainEntity list contains only one object
                    Assert.AreEqual(1, dataContract.Count);
                }
            }

            [TestMethod]
            public async Task GetTaxFormConfigurationAsync_NullTaxYear_1095C()
            {
                expectedQtdYtdParameter1095C.QypWeb1095cYears[0] = null;

                var actualAvailabilityConfiguration = await this.configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.Form1095C);

                // Make sure the domain entity set has the same number of year/date entries as the data contract set for 1095-C
                Assert.AreEqual(expectedQtdYtdParameter1095C.Qyp1095cParametersEntityAssociation.Count - 1, actualAvailabilityConfiguration.Availabilities.Count);

                // Make sure each domain entity returned is reflected in the original data contract set for 1095-C
                foreach (var domainEntity in actualAvailabilityConfiguration.Availabilities)
                {
                    // Get the domain entity with matching data
                    var dataContract = expectedQtdYtdParameter1095C.Qyp1095cParametersEntityAssociation.Where(x =>
                        x.QypWeb1095cYearsAssocMember == domainEntity.TaxYear
                        && (DateTime.Compare(x.QypWeb1095cAvailDatesAssocMember.Value, DateTime.Now) <= 0) == domainEntity.Available).ToList();

                    // Make sure the domainEntity list contains only one object
                    Assert.AreEqual(1, dataContract.Count);
                }
            }

            [TestMethod]
            public async Task GetTaxFormConfigurationAsync_EmptyTaxYear_W2()
            {
                expectedQtdYtdParameterW2.QypWebW2Years[0] = string.Empty;

                var actualAvailabilityConfiguration = await this.configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.FormW2);

                // Make sure the domain entity set has the same number of year/date entries as the data contract set for W-2
                Assert.AreEqual(expectedQtdYtdParameterW2.WebW2ParametersEntityAssociation.Count - 1, actualAvailabilityConfiguration.Availabilities.Count);

                // Make sure each domain entity returned is reflected in the original data contract set for W-2
                foreach (var domainEntity in actualAvailabilityConfiguration.Availabilities)
                {
                    // Get the domain entity with matching data
                    var dataContract = expectedQtdYtdParameterW2.WebW2ParametersEntityAssociation.Where(x =>
                        x.QypWebW2YearsAssocMember == domainEntity.TaxYear
                        && (DateTime.Compare(x.QypWebW2AvailableDatesAssocMember.Value, DateTime.Now) <= 0) == domainEntity.Available).ToList();

                    // Make sure the domainEntity list contains only one object
                    Assert.AreEqual(1, dataContract.Count);
                }
            }

            [TestMethod]
            public async Task GetTaxFormConfigurationAsync_EmptyTaxYear_1095C()
            {
                expectedQtdYtdParameter1095C.QypWeb1095cYears[0] = string.Empty;

                var actualAvailabilityConfiguration = await this.configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.Form1095C);

                // Make sure the domain entity set has the same number of year/date entries as the data contract set for 1095-C
                Assert.AreEqual(expectedQtdYtdParameter1095C.Qyp1095cParametersEntityAssociation.Count - 1, actualAvailabilityConfiguration.Availabilities.Count);

                // Make sure each domain entity returned is reflected in the original data contract set for 1095-C
                foreach (var domainEntity in actualAvailabilityConfiguration.Availabilities)
                {
                    // Get the domain entity with matching data
                    var dataContract = expectedQtdYtdParameter1095C.Qyp1095cParametersEntityAssociation.Where(x =>
                        x.QypWeb1095cYearsAssocMember == domainEntity.TaxYear
                        && (DateTime.Compare(x.QypWeb1095cAvailDatesAssocMember.Value, DateTime.Now) <= 0) == domainEntity.Available).ToList();

                    // Make sure the domainEntity list contains only one object
                    Assert.AreEqual(1, dataContract.Count);
                }
            }

            [TestMethod]
            public async Task GetTaxFormConfigurationAsync_NullAvailableDate_W2()
            {
                expectedQtdYtdParameterW2.QypWebW2AvailableDates[0] = null;

                var actualAvailabilityConfiguration = await this.configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.FormW2);

                // Make sure the domain entity set has the same number of year/date entries as the data contract set
                Assert.AreEqual(expectedQtdYtdParameterW2.WebW2ParametersEntityAssociation.Count - 1, actualAvailabilityConfiguration.Availabilities.Count);

                // Make sure each domain entity returned is reflected in the original data contract set.
                foreach (var domainEntity in actualAvailabilityConfiguration.Availabilities)
                {
                    // Get the domain entity with matching data
                    var dataContract = expectedQtdYtdParameterW2.WebW2ParametersEntityAssociation.Where(x =>
                        x.QypWebW2YearsAssocMember == domainEntity.TaxYear
                        && (DateTime.Compare(x.QypWebW2AvailableDatesAssocMember.Value, DateTime.Now) <= 0) == domainEntity.Available).ToList();

                    // Make sure the domainEntity list contains only one object.
                    Assert.AreEqual(1, dataContract.Count);
                }
            }

            [TestMethod]
            public async Task GetTaxFormConfigurationAsync_NullAvailableDate_1095C()
            {
                expectedQtdYtdParameter1095C.QypWeb1095cAvailDates[0] = null;

                var actualAvailabilityConfiguration = await this.configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.Form1095C);

                // Make sure the domain entity set has the same number of year/date entries as the data contract set for 1095-C
                Assert.AreEqual(expectedQtdYtdParameter1095C.Qyp1095cParametersEntityAssociation.Count - 1, actualAvailabilityConfiguration.Availabilities.Count);

                // Make sure each domain entity returned is reflected in the original data contract set for 1095-C
                foreach (var domainEntity in actualAvailabilityConfiguration.Availabilities)
                {
                    // Get the domain entity with matching data
                    var dataContract = expectedQtdYtdParameter1095C.Qyp1095cParametersEntityAssociation.Where(x =>
                        x.QypWeb1095cYearsAssocMember == domainEntity.TaxYear
                        && (DateTime.Compare(x.QypWeb1095cAvailDatesAssocMember.Value, DateTime.Now) <= 0) == domainEntity.Available).ToList();

                    // Make sure the domainEntity list contains only one object
                    Assert.AreEqual(1, dataContract.Count);
                }
            }

            [TestMethod]
            public async Task GetTaxFormConfigurationAsync_EmptyAssociation_W2()
            {
                dataReaderMock.Setup<Task<QtdYtdParameterW2>>(
                    dataReader => dataReader.ReadRecordAsync<QtdYtdParameterW2>(It.IsAny<string>(), It.IsAny<string>(), true))
                    .Returns(() =>
                    {
                        // Build the association for W2 data
                        expectedQtdYtdParameterW2.buildAssociations();

                        // Make the W-2 association null
                        expectedQtdYtdParameterW2.WebW2ParametersEntityAssociation = null;

                        return Task.FromResult(expectedQtdYtdParameterW2);
                    });

                var actualConfiguration = await this.configRepository.GetTaxFormConsentConfigurationAsync(TaxForms.FormW2);

                // The availability list should be empty
                Assert.AreEqual(0, actualConfiguration.Availabilities.Count);
            }

            [TestMethod]
            public async Task GetTaxFormConfigurationAsync_EmptyAssociation_1095C()
            {
                dataReaderMock.Setup<Task<QtdYtdParameter1095C>>(
                    dataReader => dataReader.ReadRecordAsync<QtdYtdParameter1095C>(It.IsAny<string>(), It.IsAny<string>(), true))
                    .Returns(() =>
                    {
                        // Build the association for 1095-C data
                        expectedQtdYtdParameter1095C.buildAssociations();

                        // Make the 1095-C association null
                        expectedQtdYtdParameter1095C.Qyp1095cParametersEntityAssociation = null;

                        return Task.FromResult(expectedQtdYtdParameter1095C);
                    });

                var actualConfiguration = await this.configRepository.GetTaxFormConsentConfigurationAsync(TaxForms.Form1095C);

                // The availability list should be empty.
                Assert.AreEqual(0, actualConfiguration.Availabilities.Count);
            }

            #region GetTaxFormAvailabilityConfigurationAsync for 1098s
            [TestMethod]
            public async Task GetTaxFormAvailabilityConfigurationAsync_AllYearsAvailable()
            {
                var actualConfiguration = await configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.Form1098);

                Assert.AreEqual(TaxForms.Form1098, actualConfiguration.TaxFormId);
                foreach (var dataContract in taxForm1098YearsContracts)
                {
                    var actualAvailabilityYears = actualConfiguration.Availabilities.Where(x =>
                        x.TaxYear == dataContract.Tf98yTaxYear.ToString()
                        && x.Available).ToList();
                    Assert.AreEqual(1, actualAvailabilityYears.Count);
                }
            }

            [TestMethod]
            public async Task GetTaxFormAvailabilityConfigurationAsync_NullParm1098Contract()
            {
                parm1098DataContract = null;
                var actualConfiguration = await configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.Form1098);

                Assert.AreEqual(0, actualConfiguration.Availabilities.Count);
            }

            [TestMethod]
            public async Task GetTaxFormAvailabilityConfigurationAsync_NullTaxFormInParm1098()
            {
                parm1098DataContract.P1098TTaxForm = null;
                var actualConfiguration = await configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.Form1098);

                Assert.AreEqual(0, actualConfiguration.Availabilities.Count);
            }

            [TestMethod]
            public async Task GetTaxFormAvailabilityConfigurationAsync_EmptyTaxFormInParm1098()
            {
                parm1098DataContract.P1098TTaxForm = "";
                var actualConfiguration = await configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.Form1098);

                Assert.AreEqual(0, actualConfiguration.Availabilities.Count);
            }

            [TestMethod]
            public async Task GetTaxFormAvailabilityConfigurationAsync_TaxFormStatusDataReadReturnsNull()
            {
                taxFormStatusContract = null;
                var actualConfiguration = await configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.Form1098);

                Assert.AreEqual(0, actualConfiguration.Availabilities.Count);
            }

            [TestMethod]
            public async Task GetTaxFormAvailabilityConfigurationAsync_TaxFormStatusTfsGenDateNull()
            {
                taxFormStatusContract.TfsGenDate = null;
                var actualConfiguration = await configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.Form1098);

                Assert.AreEqual(2, actualConfiguration.Availabilities.Where(a => a.Available).Count());
            }

            [TestMethod]
            public async Task GetTaxFormAvailabilityConfigurationAsync_TaxFormStatusTfsStatusGEN()
            {
                taxFormStatusContract.TfsStatus = "GEN";
                var actualConfiguration = await configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.Form1098);

                Assert.AreEqual(taxForm1098YearsContracts.Count - 1, actualConfiguration.Availabilities.Where(a => a.Available).Count());
            }

            [TestMethod]
            public async Task GetTaxFormAvailabilityConfigurationAsync_TaxFormStatusTfsStatusMOD()
            {
                taxFormStatusContract.TfsStatus = "MOD";
                var actualConfiguration = await configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.Form1098);

                Assert.AreEqual(taxForm1098YearsContracts.Count - 1, actualConfiguration.Availabilities.Where(a => a.Available).Count());
            }

            [TestMethod]
            public async Task GetTaxFormAvailabilityConfigurationAsync_TaxFormStatusTfsStatusUNF()
            {
                taxFormStatusContract.TfsStatus = "UNF";
                var actualConfiguration = await configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.Form1098);

                Assert.AreEqual(taxForm1098YearsContracts.Count - 1, actualConfiguration.Availabilities.Where(a => a.Available).Count());
            }

            [TestMethod]
            public async Task GetTaxFormAvailabilityConfigurationAsync_TaxForm1098YearsTf98yTaxYearNull()
            {
                foreach (var year in taxForm1098YearsContracts)
                {
                    year.Tf98yTaxYear = null;
                }
                var actualConfiguration = await configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.Form1098);

                Assert.AreEqual(0, actualConfiguration.Availabilities.Count);
            }

            [TestMethod]
            public async Task GetTaxFormAvailabilityConfigurationAsync_TaxForm1098YearsTf98yWebEnabledNull()
            {
                foreach (var year in taxForm1098YearsContracts)
                {
                    year.Tf98yWebEnabled = null;
                }
                var actualConfiguration = await configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.Form1098);

                Assert.AreEqual(0, actualConfiguration.Availabilities.Count);
            }

            [TestMethod]
            public async Task GetTaxFormAvailabilityConfigurationAsync_TaxForm1098YearsTf98yWebEnabledNo()
            {
                foreach (var year in taxForm1098YearsContracts)
                {
                    year.Tf98yWebEnabled = "N";
                }
                var actualConfiguration = await configRepository.GetTaxFormAvailabilityConfigurationAsync(TaxForms.Form1098);

                Assert.AreEqual(3, actualConfiguration.Availabilities.Count);
            }
            #endregion
        }

        #region Configuration for UserProfile
        [TestClass]
        public class ConfigurationRepository_GetUserProfileConfigurationAsync : ConfigurationRepositoryTests
        {

            [TestMethod]
            public async Task GetsUserProfileConfiguration_WithViewableAndUpdatableLists()
            {
                var configuration = await configRepository.GetUserProfileConfigurationAsync();
                Assert.AreEqual(corewebDefaults.CorewebUserProfileText, configuration.Text);
                foreach (var expectedAddressType in corewebDefaults.CorewebAddressViewTypes)
                {
                    Assert.IsTrue(configuration.ViewableAddressTypes.Contains(expectedAddressType));
                }
                foreach (var expectedPhoneType in corewebDefaults.CorewebPhoneViewTypes)
                {
                    Assert.IsTrue(configuration.ViewablePhoneTypes.Contains(expectedPhoneType));
                }
                foreach (var expectedEmailType in corewebDefaults.CorewebEmailViewTypes)
                {
                    Assert.IsTrue(configuration.ViewableEmailTypes.Contains(expectedEmailType));
                }
                foreach (var expectedEmailType in corewebDefaults.CorewebEmailUpdtTypes)
                {
                    Assert.IsTrue(configuration.UpdatableEmailTypes.Contains(expectedEmailType));
                }
                foreach (var expectedPhoneType in corewebDefaults.CorewebPhoneUpdtTypes)
                {
                    Assert.IsTrue(configuration.UpdatablePhoneTypes.Contains(expectedPhoneType));
                }

                Assert.AreEqual(dflts.DfltsWebAdrelType, configuration.UpdatableAddressTypes.First());
            }

            [TestMethod]
            public async Task GetsUserProfileConfiguration_AllSettingsNull()
            {
                var corewebDefaultsNull = new CorewebDefaults()
                {
                    CorewebAddressViewTypes = null,
                    CorewebEmailViewTypes = null,
                    CorewebPhoneViewTypes = null
                };

                // Setup response to phoneType valcode read
                dataReaderMock.Setup(r => r.ReadRecordAsync<CorewebDefaults>("CORE.PARMS", "COREWEB.DEFAULTS", true))
                       .ReturnsAsync(corewebDefaultsNull);

                var configuration = await configRepository.GetUserProfileConfigurationAsync();
                Assert.AreEqual(0, configuration.ViewableAddressTypes.Count());
                Assert.AreEqual(0, configuration.ViewablePhoneTypes.Count());
                Assert.AreEqual(0, configuration.ViewableEmailTypes.Count());
                Assert.IsNull(configuration.Text);
            }

            [TestMethod]
            public async Task GetsUserProfileConfiguration_AllTypesViewableAndUpdatable()
            {
                var dflts = new Dflts()
                {
                    DfltsWebAdrelType = "WB"
                };

                var corewebDefaults = new CorewebDefaults()
                {
                    CorewebAddressViewTypes = null,
                    CorewebEmailViewTypes = null,
                    CorewebPhoneViewTypes = null,
                    CorewebAllEmailViewable = "Y",
                    CorewebAllEmailUpdatable = "Y",
                    CorewebAllPhoneViewable = "Y",
                    CorewebAllAddrViewable = "Y",
                    CorewebEmailUpdtNoPerm = "Y",
                    CorewebPhoneUpdtNoPerm = "Y",
                    CorewebAddrUpdtNoPerm = "Y",
                    CorewebAddrUpdatable = "Y",
                };

                dataReaderMock.Setup(r => r.ReadRecordAsync<Dflts>("CORE.PARMS", "DEFAULTS", true));
                dataReaderMock.Setup(r => r.ReadRecordAsync<CorewebDefaults>("CORE.PARMS", "COREWEB.DEFAULTS", true))
                       .ReturnsAsync(corewebDefaults);

                var configuration = await configRepository.GetUserProfileConfigurationAsync();
                Assert.AreEqual(0, configuration.ViewableAddressTypes.Count());
                Assert.AreEqual(0, configuration.ViewablePhoneTypes.Count());
                Assert.AreEqual(0, configuration.ViewableEmailTypes.Count());
                Assert.IsTrue(configuration.AllAddressTypesAreViewable);
                Assert.IsTrue(configuration.AllEmailTypesAreViewable);
                Assert.IsTrue(configuration.AllPhoneTypesAreViewable);
                Assert.IsTrue(configuration.AllEmailTypesAreUpdatable);
                Assert.IsTrue(configuration.CanUpdateEmailWithoutPermission);
                Assert.IsTrue(configuration.CanUpdatePhoneWithoutPermission);
            }

        }

        #endregion

        [TestClass]
        public class ConfigurationRepository_GetEmergencyInformationConfigurationAsync : ConfigurationRepositoryTests
        {
            [TestMethod]
            public async Task GetEmergencyInformationConfiguration_RetrievesTrueValues()
            {
                var corewebDefaultsWithYes = new CorewebDefaults()
                {
                    CorewebEmerAllowOptout = "Y",
                    CorewebEmerHideHlthCond = "Y",
                    CorewebEmerHideOtherInfo = "Y",
                    CorewebEmerRequireContact = "Y"
                };

                dataReaderMock.Setup(r => r.ReadRecordAsync<CorewebDefaults>("CORE.PARMS", "COREWEB.DEFAULTS", true))
                       .ReturnsAsync(corewebDefaultsWithYes);

                var emerConfig = await configRepository.GetEmergencyInformationConfigurationAsync();
                Assert.IsTrue(emerConfig.AllowOptOut);
                Assert.IsTrue(emerConfig.HideHealthConditions);
                Assert.IsTrue(emerConfig.HideOtherInformation);
                Assert.IsTrue(emerConfig.RequireContact);
            }

            [TestMethod]
            public async Task GetEmergencyInformationConfiguration_RetrievesFalseValues()
            {
                var corewebDefaultsWithNo = new CorewebDefaults()
                {
                    CorewebEmerAllowOptout = "N",
                    CorewebEmerHideHlthCond = "N",
                    CorewebEmerHideOtherInfo = "N",
                    CorewebEmerRequireContact = "N"
                };

                dataReaderMock.Setup(r => r.ReadRecordAsync<CorewebDefaults>("CORE.PARMS", "COREWEB.DEFAULTS", true))
                       .ReturnsAsync(corewebDefaultsWithNo);

                var emerConfig = await configRepository.GetEmergencyInformationConfigurationAsync();
                Assert.IsFalse(emerConfig.AllowOptOut);
                Assert.IsFalse(emerConfig.HideHealthConditions);
                Assert.IsFalse(emerConfig.HideOtherInformation);
                Assert.IsFalse(emerConfig.RequireContact);
            }

            [TestMethod]
            public async Task GetEmergencyInformationConfiguration_NullValuesDefaultToFalse()
            {
                var corewebDefaultsWithNull = new CorewebDefaults()
                {
                    CorewebEmerAllowOptout = null,
                    CorewebEmerHideHlthCond = null,
                    CorewebEmerHideOtherInfo = null,
                    CorewebEmerRequireContact = null
                };

                dataReaderMock.Setup(r => r.ReadRecordAsync<CorewebDefaults>("CORE.PARMS", "COREWEB.DEFAULTS", true))
                       .ReturnsAsync(corewebDefaultsWithNull);

                var emerConfig = await configRepository.GetEmergencyInformationConfigurationAsync();
                Assert.IsFalse(emerConfig.AllowOptOut);
                Assert.IsFalse(emerConfig.HideHealthConditions);
                Assert.IsFalse(emerConfig.HideOtherInformation);
                Assert.IsFalse(emerConfig.RequireContact);
            }
        }

        #region RestrictionConfiguration

        [TestClass]
        public class ConfigurationRepository_GetRestrictionConfigurationAsync : ConfigurationRepositoryTests
        {
            [TestMethod]
            public async Task GetRestrictionConfigurationTest()
            {
                var start = 0;
                var end = 100;
                var style = "I";

                var corewebDefaults = new CorewebDefaults()
                {
                    CorewebSeverityStart = new List<int?>(),
                    CorewebSeverityEnd = new List<int?>(),
                    CorewebStyle = new List<string>()
                };
                corewebDefaults.CorewebStyle.Add(style);
                corewebDefaults.CorewebSeverityStart.Add(start);
                corewebDefaults.CorewebSeverityEnd.Add(end);

                // Setup response to phoneType valcode read
                dataReaderMock.Setup(r => r.ReadRecordAsync<CorewebDefaults>("CORE.PARMS", "COREWEB.DEFAULTS", true))
                       .ReturnsAsync(corewebDefaults);

                var configuration = await configRepository.GetRestrictionConfigurationAsync();
                Assert.AreEqual(1, configuration.Mapping.Count);
                Assert.AreEqual(start, configuration.Mapping[0].SeverityStart);
                Assert.AreEqual(end, configuration.Mapping[0].SeverityEnd);
                Assert.AreEqual("Information", configuration.Mapping[0].Style.ToString());
            }

            [TestMethod]
            public async Task GetEmptyRestrictionConfigurationTest()
            {

                var corewebDefaults = new CorewebDefaults()
                {
                    CorewebSeverityStart = new List<int?>(),
                    CorewebSeverityEnd = new List<int?>(),
                    CorewebStyle = new List<string>()
                };

                // Setup response to phoneType valcode read
                dataReaderMock.Setup(r => r.ReadRecordAsync<CorewebDefaults>("CORE.PARMS", "COREWEB.DEFAULTS", true))
                       .ReturnsAsync(corewebDefaults);

                var configuration = await configRepository.GetRestrictionConfigurationAsync();
                Assert.AreEqual(0, configuration.Mapping.Count);
            }
        }

        #endregion

        #region PrivacyConfiguration

        [TestClass]
        public class ConfigurationRepository_GetPrivacyConfigurationAsync : ConfigurationRepositoryTests
        {
            [TestMethod]
            public async Task ConfigurationRepository_GetPrivacyConfigurationAsync_Valid()
            {
                var dflts = new Dflts()
                {
                    DfltsRecordDenialMsg = "Record not accesible due to a privacy request"
                };
                dataReaderMock.Setup(r => r.ReadRecordAsync<Dflts>("CORE.PARMS", "DEFAULTS", true))
                       .ReturnsAsync(dflts);

                var configuration = await configRepository.GetPrivacyConfigurationAsync();
                Assert.AreEqual(dflts.DfltsRecordDenialMsg, configuration.RecordDenialMessage);
            }

            [TestMethod]
            [ExpectedException(typeof(ConfigurationException))]
            public async Task ConfigurationRepository_GetPrivacyConfigurationAsync_Null_Dflts()
            {
                Dflts dflts = null;
                dataReaderMock.Setup(r => r.ReadRecordAsync<Dflts>("CORE.PARMS", "DEFAULTS", true))
                       .ReturnsAsync(dflts);

                var configuration = await configRepository.GetPrivacyConfigurationAsync();
            }
        }


        #endregion

        #region OrganizationalRelationshipConfiguration
        [TestClass]
        public class ConfigurationRepository_GetOrganizationalRelationshipConfigurationAsync : ConfigurationRepositoryTests
        {
            [TestMethod]
            public async Task ConfigurationRepository_GetOrganizationalRelationshipConfigurationAsync_Valid()
            {
                // mock data accessor PHONE.TYPES
                dataReaderMock.Setup<Task<ApplValcodes>>(a =>
                    a.ReadRecordAsync<ApplValcodes>("UT.VALCODES", "RELATIONSHIP.CATEGORIES", true))
                    .ReturnsAsync(new ApplValcodes()
                    {
                        ValInternalCode = new List<string>() { "NOTMGR", "MGR" },
                        ValExternalRepresentation = new List<string>() { "Not a manager", "Manager" },
                        ValActionCode1 = new List<string>() { "", "ORG" },
                        ValsEntityAssociation = new List<ApplValcodesVals>()
                        {
                            new ApplValcodesVals()
                            {
                                ValInternalCodeAssocMember = "NOTMGR",
                                ValExternalRepresentationAssocMember = "Not a manager",
                                ValActionCode1AssocMember = ""
                            },
                             new ApplValcodesVals()
                            {
                                ValInternalCodeAssocMember = "MGR",
                                ValExternalRepresentationAssocMember = "Manager",
                                ValActionCode1AssocMember = "ORG"
                            },
                        }
                    });
                var orgRelConfig = await configRepository.GetOrganizationalRelationshipConfigurationAsync();
                Assert.AreEqual(1, orgRelConfig.RelationshipTypeCodeMapping.Keys.Count);
                Assert.AreEqual(1, orgRelConfig.RelationshipTypeCodeMapping.Values.Count);
                Assert.AreEqual(OrganizationalRelationshipType.Manager, orgRelConfig.RelationshipTypeCodeMapping.Keys.First());
                Assert.AreEqual("MGR", orgRelConfig.RelationshipTypeCodeMapping.Values.First().First());
            }
        }
        #endregion

        private ConfigurationRepository BuildValidRepository()
        {
            var transFactoryMock = new Mock<IColleagueTransactionFactory>();

            var loggerMock = new Mock<ILogger>();

            // Cache mocking
            var cacheProviderMock = new Mock<ICacheProvider>();
            var localCacheMock = new Mock<ObjectCache>();
            cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x => x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

            // Set up data accessor for mocking 
            dataReaderMock = new Mock<IColleagueDataReader>();

            // Set up data accessor for mocking 
            transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(dataReaderMock.Object);
            transFactoryMock.Setup(transFac => transFac.GetTransactionInvoker()).Returns(transManagerMock.Object);

            transManagerMock.Setup<GetIntegrationConfigResponse>(
                manager => manager.Execute<GetIntegrationConfigRequest, GetIntegrationConfigResponse>(
                    It.IsAny<GetIntegrationConfigRequest>())).Returns<GetIntegrationConfigRequest>(request =>
                    {
                        switch (request.CdmIntegrationId)
                        {
                            case "TEST":
                                return responses.Where(r => r.CdmIntegrationId == "TEST").FirstOrDefault();
                            case "TEST2":
                                return responses.Where(r => r.CdmIntegrationId == "TEST2").FirstOrDefault();
                            default:
                                return new GetIntegrationConfigResponse()
                                {
                                    ErrorMsgs = new List<string>() { "Record not found." }
                                };
                        }
                    });

            dataReaderMock.Setup<LdmDefaults>(
                reader => reader.ReadRecord<LdmDefaults>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()))
                .Returns<string, string, bool>((file, record, replaceVm) =>
                {
                    return new LdmDefaults()
                    {
                        LdmdAddrDuplCriteria = "LDM.ADDR",
                        LdmdAddrTypeMapping = "LDM.ADDR.TYPE",
                        LdmdCourseActStatus = "A",
                        LdmdCourseInactStatus = "I",
                        LdmdEmailTypeMapping = "LDM.EMAIL",
                        LdmdPersonDuplCriteria = "LDM.PERSON",
                        LdmdSectionActStatus = "A",
                        LdmdSectionInactStatus = "I",
                        LdmdSubjDeptMapping = "LDM.SUBJ.DEPT",
                        Recordkey = "LDM.DEFAULTS",
                        LdmdCollDefaultsEntityAssociation = new List<LdmDefaultsLdmdCollDefaults>()
                            {
                                new LdmDefaultsLdmdCollDefaults()
                                {
                                    LdmdCollDefaultValueAssocMember = "A",
                                    LdmdCollFieldNameAssocMember = "B",
                                    LdmdCollFieldNumberAssocMember = 5,
                                    LdmdCollFileNameAssocMember = "COURSES"
                                }
                            },
                        LdmdPhoneTypeMappingEntityAssociation = new List<LdmDefaultsLdmdPhoneTypeMapping>()
                            {
                                new LdmDefaultsLdmdPhoneTypeMapping()
                                {
                                    LdmdLdmPhoneTypeAssocMember = "BUS",
                                    LdmdCollPhoneTypesAssocMember = "BU"
                                }
                            }
                    };
                }
            );

            dflts = new Dflts()
            {
                DfltsCampusCalendar = "MAIN",
                DfltsWebAdrelType = "WB"
            };

            dataReaderMock.Setup<Dflts>(
                reader => reader.ReadRecord<Dflts>("CORE.PARMS", "DEFAULTS", It.IsAny<bool>()))
                .Returns<string, string, bool>((file, record, replaceVm) =>
                {
                    return dflts;
                }
            );

            dataReaderMock.Setup<Task<HrwebDefaults>>(
                datareader => datareader.ReadRecordAsync<HrwebDefaults>(It.IsAny<string>(), It.IsAny<string>(), true))
                .Returns(() =>
                {
                    return Task.FromResult(expectedHrWebDefaults);
                });

            corewebDefaults = new CorewebDefaults()
            {
                CorewebAddressViewTypes = new List<string>() { "HO", "B" },
                CorewebEmailViewTypes = new List<string>() { "WEB", "I" },
                CorewebPhoneViewTypes = new List<string>() { "H", "BUS" },
                CorewebEmailUpdtTypes = new List<string>() { "WEB" },
                CorewebPhoneUpdtTypes = new List<string>() { "H" },
                CorewebUserProfileText = "Please contact the administration with any changes.",
                CorewebAddrUpdatable = "Y",
                CorewebAllEmailUpdatable = "N",
                CorewebAddrUpdtNoPerm = "Y",
                CorewebAllAddrViewable = "N",
                CorewebAllPhoneViewable = "N",
                CorewebAllEmailViewable = "N",
                CorewebEmailUpdtNoPerm = "Y",
                CorewebPhoneUpdtNoPerm = "Y",
                CorewebEmerAllowOptout = "Y",
                CorewebEmerHideHlthCond = "Y",
                CorewebEmerRequireContact = "Y"
            };
            dataReaderMock.Setup(r => r.ReadRecordAsync<CorewebDefaults>("CORE.PARMS", "COREWEB.DEFAULTS", true))
                   .ReturnsAsync(corewebDefaults);

            dataReaderMock.Setup<Task<QtdYtdParameterW2>>(
                dataReader => dataReader.ReadRecordAsync<QtdYtdParameterW2>(It.IsAny<string>(), It.IsAny<string>(), true))
                .Returns(() =>
                {
                    // Make sure we build the association
                    expectedQtdYtdParameterW2.buildAssociations();
                    return Task.FromResult(expectedQtdYtdParameterW2);
                });

            dataReaderMock.Setup<Task<QtdYtdParameter1095C>>(
                dataReader => dataReader.ReadRecordAsync<QtdYtdParameter1095C>(It.IsAny<string>(), It.IsAny<string>(), true))
                .Returns(() =>
                {
                    // Make sure we build the association
                    expectedQtdYtdParameter1095C.buildAssociations();
                    return Task.FromResult(expectedQtdYtdParameter1095C);
                });

            dataReaderMock.Setup<Task<Parm1098>>(
                dataReader => dataReader.ReadRecordAsync<Parm1098>(It.IsAny<string>(), It.IsAny<string>(), true))
                .Returns(() =>
                {
                    // Make sure we build the association
                    expectedQtdYtdParameter1095C.buildAssociations();
                    return Task.FromResult(parm1098DataContract);
                });

            dataReaderMock.Setup(x => x.BulkReadRecordAsync<TaxForm1098Years>(It.IsAny<string>(), true)).Returns(() =>
                {
                    return Task.FromResult(taxForm1098YearsContracts);
                });

            dataReaderMock.Setup(x => x.ReadRecordAsync<TaxFormStatus>(It.IsAny<string>(), true)).Returns(() =>
                {
                    return Task.FromResult(taxFormStatusContract);
                });

            dataReaderMock.Setup(x => x.ReadRecordAsync<Parm1098>("ST.PARMS", "PARM.1098", true)).Returns(() =>
                {
                    return Task.FromResult(parm1098DataContract);
                });

            cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x => x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));
            // Construct referenceData repository
            configRepository = new ConfigurationRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);

            return configRepository;
        }
    }
}