﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Data.Base.Repositories;
using Ellucian.Colleague.Data.Base.Transactions;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Tests.Builders;
using Ellucian.Data.Colleague;
using Ellucian.Web.Cache;
using Ellucian.Web.Http.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;

namespace Ellucian.Colleague.Data.Base.Tests.Repositories
{
    [TestClass]
    public class TaxFormConsentRepositoryTests
    {
        #region Initialize and Cleanup
        private ApiSettings apiSettings = new ApiSettings("API") { ColleagueTimeZone = "Eastern Standard Time" };
        private Mock<IColleagueDataReader> dataReaderMock = null;
        private Mock<IColleagueTransactionInvoker> transactionInvoker = null;
        private CreateDocConsentHistoryResponse createDocConsentHistoryResponse = new CreateDocConsentHistoryResponse();
        private TaxFormConsentRepository taxFormConsentRepository;
        private TaxFormConsentBuilder builder;
        private Collection<DocConsentHistory> docConsentHistoryContracts;

        private Collection<W2ConsentHistory> w2ConsentHistoryContracts = new Collection<W2ConsentHistory>()
        { 
            new W2ConsentHistory() 
            { 
                Recordkey = "1234", 
                W2chHrperId = "0001234", 
                W2chNewStatus = "C", 
                W2ConsentHistoryAdddate = DateTime.Now, 
                W2ConsentHistoryAddtime = DateTime.Now
            }, 
            new W2ConsentHistory() 
            { 
                Recordkey = "1235", 
                W2chHrperId = "0001234",  
                W2chNewStatus = "W", 
                W2ConsentHistoryAdddate = DateTime.Now, 
                W2ConsentHistoryAddtime = DateTime.Now 
            }, 
        };

        [TestInitialize]
        public void Initialize()
        {
            dataReaderMock = new Mock<IColleagueDataReader>();
            transactionInvoker = new Mock<IColleagueTransactionInvoker>();
            taxFormConsentRepository = BuildTaxFormConsentRepository();

            docConsentHistoryContracts = new Collection<DocConsentHistory>();

            dataReaderMock.Setup<Task<Collection<W2ConsentHistory>>>(datareader => datareader.BulkReadRecordAsync<W2ConsentHistory>(It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(w2ConsentHistoryContracts);
            });

            dataReaderMock.Setup<Task<Collection<DocConsentHistory>>>(datareader => datareader.BulkReadRecordAsync<DocConsentHistory>(It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(docConsentHistoryContracts);
            });

            transactionInvoker.Setup(tio => tio.ExecuteAsync<CreateDocConsentHistoryRequest, CreateDocConsentHistoryResponse>(It.IsAny<CreateDocConsentHistoryRequest>())).Returns(Task.FromResult(this.createDocConsentHistoryResponse));
            this.builder = new TaxFormConsentBuilder();
        }

        [TestCleanup]
        public void Cleanup()
        {
            docConsentHistoryContracts = null;
            dataReaderMock = null;
            transactionInvoker = null;
            taxFormConsentRepository = null;
            this.builder = null;
        }
        #endregion

        #region Tests for GetAsync
        [TestMethod]
        public async Task TaxFormConsentRepository_W2GetAsync()
        {
            var personId = w2ConsentHistoryContracts.ElementAt(0).W2chHrperId;

            var taxFormConsentEntities = await taxFormConsentRepository.GetAsync(personId, TaxForms.FormW2);

            foreach (var consentContract in w2ConsentHistoryContracts)
            {
                // For each consent history object in my input set, confirmat that 
                // there is one and only matching consent history object in the output set
                var expectedPersonId = consentContract.W2chHrperId;
                var expectedTaxForm = TaxForms.FormW2;
                var expectedStatus = consentContract.W2chNewStatus == "C";
                var expectedTimeStamp = ColleagueTimeZoneUtility.ToPointInTimeDateTimeOffset(consentContract.W2ConsentHistoryAddtime, consentContract.W2ConsentHistoryAdddate, apiSettings.ColleagueTimeZone).GetValueOrDefault();

                var consentEntities = taxFormConsentEntities.Where(x =>
                    x.PersonId == expectedPersonId
                 && x.TaxForm == expectedTaxForm
                 && x.HasConsented == expectedStatus
                 && x.TimeStamp == expectedTimeStamp);

                Assert.AreEqual(1, consentEntities.Count());
            }
        }

        [TestMethod]
        public async Task TaxFormConsentRepository_1095CGetAsync()
        {
            docConsentHistoryContracts.Add(new DocConsentHistory()
                {
                    Recordkey = "998",
                    DchistDocument = "1095C",
                    DchistPersonId = "0001234",
                    DchistStatus = "C",
                    DchistStatusDate = DateTime.Now,
                    DchistStatusTime = DateTime.Now
                });
            docConsentHistoryContracts.Add(new DocConsentHistory()
                {
                    Recordkey = "999",
                    DchistDocument = "1095C",
                    DchistPersonId = "0001234",
                    DchistStatus = "W",
                    DchistStatusDate = DateTime.Now,
                    DchistStatusTime = DateTime.Now
                });
            var personId = docConsentHistoryContracts.ElementAt(0).DchistPersonId;
            var taxFormConsentEntities = await taxFormConsentRepository.GetAsync(personId, TaxForms.Form1095C);


            Assert.AreEqual(docConsentHistoryContracts.Where(x => x.DchistDocument == "1095C").Count(), taxFormConsentEntities.Count());
            foreach (var consentContract in docConsentHistoryContracts)
            {
                // For each consent history object in my input set, confirmat that 
                // there is one and only matching consent history object in the output set
                var expectedPersonId = consentContract.DchistPersonId;
                var expectedTaxForm = TaxForms.Form1095C;
                var expectedStatus = consentContract.DchistStatus == "C";
                var expectedTimeStamp = ColleagueTimeZoneUtility.ToPointInTimeDateTimeOffset(consentContract.DchistStatusTime, consentContract.DchistStatusDate, apiSettings.ColleagueTimeZone).GetValueOrDefault();

                var consentEntities = taxFormConsentEntities.Where(x =>
                    x.PersonId == expectedPersonId
                    && x.TaxForm == expectedTaxForm
                    && x.HasConsented == expectedStatus
                    && x.TimeStamp == expectedTimeStamp);

                Assert.AreEqual(1, consentEntities.Count());
            }
        }

        [TestMethod]
        public async Task TaxFormConsentRepository_1098GetAsync()
        {
            docConsentHistoryContracts.Add(new DocConsentHistory()
                {
                    Recordkey = "999",
                    DchistDocument = "1098",
                    DchistPersonId = "0001234",
                    DchistStatus = "C",
                    DchistStatusDate = DateTime.Now,
                    DchistStatusTime = DateTime.Now
                });
            var personId = docConsentHistoryContracts.ElementAt(0).DchistPersonId;
            var taxFormConsentEntities = await taxFormConsentRepository.GetAsync(personId, TaxForms.Form1098);

            Assert.AreEqual(docConsentHistoryContracts.Where(x => x.DchistDocument == "1098").Count(), taxFormConsentEntities.Count());
            foreach (var consentContract in docConsentHistoryContracts)
            {
                // For each consent history object in my input set, confirmat that 
                // there is one and only matching consent history object in the output set
                var expectedPersonId = consentContract.DchistPersonId;
                var expectedTaxForm = TaxForms.Form1098;
                var expectedStatus = consentContract.DchistStatus == "C";
                var expectedTimeStamp = ColleagueTimeZoneUtility.ToPointInTimeDateTimeOffset(consentContract.DchistStatusTime, consentContract.DchistStatusDate, apiSettings.ColleagueTimeZone).GetValueOrDefault();

                var consentEntities = taxFormConsentEntities.Where(x =>
                    x.PersonId == expectedPersonId
                    && x.TaxForm == expectedTaxForm
                    && x.HasConsented == expectedStatus
                    && x.TimeStamp == expectedTimeStamp);

                Assert.AreEqual(1, consentEntities.Count());
            }
        }

        [TestMethod]
        public async Task TaxFormConsentRepository_1095CGetAsync_ContractWithWrongDocumentReturned()
        {
            docConsentHistoryContracts.Add(new DocConsentHistory()
            {
                Recordkey = "9999",
                DchistDocument = "1098",
                DchistPersonId = "0001234",
                DchistStatus = "W",
                DchistStatusDate = DateTime.Now,
                DchistStatusTime = DateTime.Now
            });

            var personId = docConsentHistoryContracts.ElementAt(0).DchistPersonId;
            var taxFormConsentEntities = await taxFormConsentRepository.GetAsync(personId, TaxForms.Form1095C);

            Assert.AreEqual(docConsentHistoryContracts.Count - 1, taxFormConsentEntities.Count());
        }

        [TestMethod]
        public async Task TaxFormConsentRepository_1098GetAsync_ContractWithWrongDocumentReturned()
        {
            docConsentHistoryContracts.Add(new DocConsentHistory()
            {
                Recordkey = "9999",
                DchistDocument = "1095C",
                DchistPersonId = "0001234",
                DchistStatus = "W",
                DchistStatusDate = DateTime.Now,
                DchistStatusTime = DateTime.Now
            });

            var personId = docConsentHistoryContracts.ElementAt(0).DchistPersonId;
            var taxFormConsentEntities = await taxFormConsentRepository.GetAsync(personId, TaxForms.Form1098);

            Assert.AreEqual(docConsentHistoryContracts.Count - 1, taxFormConsentEntities.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task TaxFormConsentRepository_Get_NullPersonId()
        {
            await taxFormConsentRepository.GetAsync(null, TaxForms.FormW2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task TaxFormConsentRepository_Get_EmptyPersonId()
        {
            await taxFormConsentRepository.GetAsync(string.Empty, TaxForms.FormW2);
        }

        [TestMethod]
        public async Task GetAsync_W2NullDateTimeOffset()
        {
            this.w2ConsentHistoryContracts[0].W2ConsentHistoryAdddate = null;
            this.w2ConsentHistoryContracts[0].W2ConsentHistoryAddtime = null;
            var consentEntities = await taxFormConsentRepository.GetAsync("0003946", TaxForms.FormW2);

            // All contracts - except one - should be processed and returned as domain entities.
            Assert.AreEqual(this.w2ConsentHistoryContracts.Count - 1, consentEntities.Count());
        }

        [TestMethod]
        public async Task GetAsync_1095CNullDateTimeOffset()
        {
            docConsentHistoryContracts.Add(new DocConsentHistory()
            {
                Recordkey = "998",
                DchistDocument = "1095C",
                DchistPersonId = "0001234",
                DchistStatus = "C",
                DchistStatusDate = null,
                DchistStatusTime = DateTime.Now
            });
            var consentEntities = await taxFormConsentRepository.GetAsync("0003946", TaxForms.Form1095C);

            // All contracts - except one - should be processed and returned as domain entities.
            Assert.AreEqual(docConsentHistoryContracts.Count - 1, consentEntities.Count());
        }

        [TestMethod]
        public async Task GetAsync_1098NullDateTimeOffset()
        {
            docConsentHistoryContracts.Add(new DocConsentHistory()
            {
                Recordkey = "998",
                DchistDocument = "1098",
                DchistPersonId = "0001234",
                DchistStatus = "C",
                DchistStatusDate = null,
                DchistStatusTime = DateTime.Now
            });
            var consentEntities = await taxFormConsentRepository.GetAsync("0003946", TaxForms.Form1098);

            // All contracts - except one - should be processed and returned as domain entities.
            Assert.AreEqual(docConsentHistoryContracts.Count - 1, consentEntities.Count());
        }
        #endregion

        #region Tests for PostAsync
        [TestMethod]
        public async Task PostAsync_Success_HasConsented()
        {
            var incomingConsent = this.builder.WithHasConsented(true).Build();
            var returnedConsent = await taxFormConsentRepository.PostAsync(incomingConsent);

            Assert.AreEqual(incomingConsent.HasConsented, returnedConsent.HasConsented);
            Assert.AreEqual(incomingConsent.PersonId, returnedConsent.PersonId);
            Assert.AreEqual(incomingConsent.TaxForm, returnedConsent.TaxForm);
            Assert.AreEqual(incomingConsent.TimeStamp, returnedConsent.TimeStamp);
        }

        [TestMethod]
        public async Task PostAsync_Success_HasNotConsented()
        {
            var incomingConsent = this.builder.WithHasConsented(false).Build();
            var returnedConsent = await taxFormConsentRepository.PostAsync(incomingConsent);

            Assert.AreEqual(incomingConsent.HasConsented, returnedConsent.HasConsented);
            Assert.AreEqual(incomingConsent.PersonId, returnedConsent.PersonId);
            Assert.AreEqual(incomingConsent.TaxForm, returnedConsent.TaxForm);
            Assert.AreEqual(incomingConsent.TimeStamp, returnedConsent.TimeStamp);
        }

        [TestMethod]
        public async Task PostAsync_Success_FormIs1095C()
        {
            var incomingConsent = this.builder.WithTaxForm(TaxForms.Form1095C).Build();
            var returnedConsent = await taxFormConsentRepository.PostAsync(incomingConsent);

            Assert.AreEqual(incomingConsent.HasConsented, returnedConsent.HasConsented);
            Assert.AreEqual(incomingConsent.PersonId, returnedConsent.PersonId);
            Assert.AreEqual(incomingConsent.TaxForm, returnedConsent.TaxForm);
            Assert.AreEqual(incomingConsent.TimeStamp, returnedConsent.TimeStamp);
        }

        [TestMethod]
        public async Task PostAsync_Success_FormIs1098()
        {
            var incomingConsent = this.builder.WithTaxForm(TaxForms.Form1098).Build();
            var returnedConsent = await taxFormConsentRepository.PostAsync(incomingConsent);

            Assert.AreEqual(incomingConsent.HasConsented, returnedConsent.HasConsented);
            Assert.AreEqual(incomingConsent.PersonId, returnedConsent.PersonId);
            Assert.AreEqual(incomingConsent.TaxForm, returnedConsent.TaxForm);
            Assert.AreEqual(incomingConsent.TimeStamp, returnedConsent.TimeStamp);
        }
        #endregion

        private TaxFormConsentRepository BuildTaxFormConsentRepository()
        {
            // Instantiate all objects necessary to mock data reader and CTX calls.
            var cacheProviderObject = new Mock<ICacheProvider>().Object;
            var transactionFactory = new Mock<IColleagueTransactionFactory>();
            var transactionFactoryObject = transactionFactory.Object;
            var loggerObject = new Mock<ILogger>().Object;

            // The transaction factory has a method to get its data reader
            // Make sure that method returns our mock data reader
            transactionFactory.Setup(transFac => transFac.GetDataReader()).Returns(dataReaderMock.Object);
            transactionFactory.Setup(transFac => transFac.GetTransactionInvoker()).Returns(transactionInvoker.Object);

            return new TaxFormConsentRepository(apiSettings, cacheProviderObject, transactionFactoryObject, loggerObject);
        }
    }
}
