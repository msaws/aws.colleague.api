//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 9/23/2014 2:21:42 PM by user kmf
//
//     Type: CTX
//     Transaction ID: GET.GRADUATE.LEVEL
//     Application: ST
//     Environment: dvcoll_wstst01
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;

namespace Ellucian.Colleague.Data.FinancialAid.Transactions
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract]
	[ColleagueDataContract(ColleagueId = "GET.GRADUATE.LEVEL", GeneratedDateTime = "9/23/2014 2:21:42 PM", User = "kmf")]
	[SctrqDataContract(Application = "ST", DataContractVersion = 1)]
	public class GetGraduateLevelRequest
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		[DataMember(IsRequired = true)]
		[SctrqDataMember(AppServerName = "STUDENT.ID", InBoundData = true)]        
		public string StudentId { get; set; }

		public GetGraduateLevelRequest()
		{	
		}
	}

	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract]
	[ColleagueDataContract(ColleagueId = "GET.GRADUATE.LEVEL", GeneratedDateTime = "9/23/2014 2:21:42 PM", User = "kmf")]
	[SctrqDataContract(Application = "ST", DataContractVersion = 1)]
	public class GetGraduateLevelResponse
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "GRADUATE.LEVEL", OutBoundData = true)]        
		public string GraduateLevel { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "ERROR.MESSAGE", OutBoundData = true)]        
		public string ErrorMessage { get; set; }

		public GetGraduateLevelResponse()
		{	
		}
	}
}
