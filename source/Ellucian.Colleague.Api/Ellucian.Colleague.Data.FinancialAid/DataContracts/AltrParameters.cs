//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 7/9/2015 11:25:26 AM by user otorres
//
//     Type: ENTITY
//     Entity: ALTR.PARAMETERS
//     Application: ST
//     Environment: dvcoll
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;
using Ellucian.Data.Colleague;

namespace Ellucian.Colleague.Data.FinancialAid.DataContracts
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract(Name = "AltrParameters")]
	[ColleagueDataContract(GeneratedDateTime = "7/9/2015 11:25:26 AM", User = "otorres")]
	[EntityDataContract(EntityName = "ALTR.PARAMETERS", EntityType = "PHYS")]
	public class AltrParameters : IColleagueEntity
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		/// <summary>
		/// Record Key
		/// </summary>
		[DataMember]
		public string Recordkey { get; set; }
		
		public void setKey(string key)
		{
			Recordkey = key;
		}
		
		/// <summary>
		/// CDD Name: ALTR.INTRO.TEXT
		/// </summary>
		[DataMember(Order = 1, Name = "ALTR.INTRO.TEXT")]
		public string AltrIntroText { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.CLOSING.TEXT
		/// </summary>
		[DataMember(Order = 2, Name = "ALTR.CLOSING.TEXT")]
		public string AltrClosingText { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.TITLE.GROUP1
		/// </summary>
		[DataMember(Order = 3, Name = "ALTR.TITLE.GROUP1")]
		public string AltrTitleGroup1 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.TITLE.GROUP2
		/// </summary>
		[DataMember(Order = 4, Name = "ALTR.TITLE.GROUP2")]
		public string AltrTitleGroup2 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.TITLE.GROUP3
		/// </summary>
		[DataMember(Order = 5, Name = "ALTR.TITLE.GROUP3")]
		public string AltrTitleGroup3 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.TITLE.COLUMN1
		/// </summary>
		[DataMember(Order = 6, Name = "ALTR.TITLE.COLUMN1")]
		public string AltrTitleColumn1 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.TITLE.COLUMN2
		/// </summary>
		[DataMember(Order = 7, Name = "ALTR.TITLE.COLUMN2")]
		public string AltrTitleColumn2 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.TITLE.COLUMN3
		/// </summary>
		[DataMember(Order = 8, Name = "ALTR.TITLE.COLUMN3")]
		public string AltrTitleColumn3 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.TITLE.COLUMN4
		/// </summary>
		[DataMember(Order = 9, Name = "ALTR.TITLE.COLUMN4")]
		public string AltrTitleColumn4 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.TITLE.AWD.NAME
		/// </summary>
		[DataMember(Order = 10, Name = "ALTR.TITLE.AWD.NAME")]
		public string AltrTitleAwdName { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.TITLE.AWD.TOTAL
		/// </summary>
		[DataMember(Order = 11, Name = "ALTR.TITLE.AWD.TOTAL")]
		public string AltrTitleAwdTotal { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.CATEGORY.GROUP1
		/// </summary>
		[DataMember(Order = 12, Name = "ALTR.CATEGORY.GROUP1")]
		public List<string> AltrCategoryGroup1 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.CATEGORY.GROUP2
		/// </summary>
		[DataMember(Order = 13, Name = "ALTR.CATEGORY.GROUP2")]
		public List<string> AltrCategoryGroup2 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.AWD.PER.COLUMN1
		/// </summary>
		[DataMember(Order = 14, Name = "ALTR.AWD.PER.COLUMN1")]
		public List<string> AltrAwdPerColumn1 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.AWD.PER.COLUMN2
		/// </summary>
		[DataMember(Order = 15, Name = "ALTR.AWD.PER.COLUMN2")]
		public List<string> AltrAwdPerColumn2 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.AWD.PER.COLUMN3
		/// </summary>
		[DataMember(Order = 16, Name = "ALTR.AWD.PER.COLUMN3")]
		public List<string> AltrAwdPerColumn3 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.AWD.PER.COLUMN4
		/// </summary>
		[DataMember(Order = 17, Name = "ALTR.AWD.PER.COLUMN4")]
		public List<string> AltrAwdPerColumn4 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.OFFICE.BLOCK
		/// </summary>
		[DataMember(Order = 19, Name = "ALTR.OFFICE.BLOCK")]
		public string AltrOfficeBlock { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.NEED.BLOCK
		/// </summary>
		[DataMember(Order = 20, Name = "ALTR.NEED.BLOCK")]
		public string AltrNeedBlock { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.TITLE.COLUMN5
		/// </summary>
		[DataMember(Order = 22, Name = "ALTR.TITLE.COLUMN5")]
		public string AltrTitleColumn5 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.TITLE.COLUMN6
		/// </summary>
		[DataMember(Order = 23, Name = "ALTR.TITLE.COLUMN6")]
		public string AltrTitleColumn6 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.AWD.PER.COLUMN5
		/// </summary>
		[DataMember(Order = 24, Name = "ALTR.AWD.PER.COLUMN5")]
		public List<string> AltrAwdPerColumn5 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.AWD.PER.COLUMN6
		/// </summary>
		[DataMember(Order = 25, Name = "ALTR.AWD.PER.COLUMN6")]
		public List<string> AltrAwdPerColumn6 { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.PARA.SPACING
		/// </summary>
		[DataMember(Order = 26, Name = "ALTR.PARA.SPACING")]
		public string AltrParaSpacing { get; set; }
		
		/// <summary>
		/// CDD Name: ALTR.HOUSING.CODE
		/// </summary>
		[DataMember(Order = 27, Name = "ALTR.HOUSING.CODE")]
		public string AltrHousingCode { get; set; }
		
	
		// build up all the Associated objects and add them to the properties
		public void buildAssociations()
		{	
			   
		}
	}
	
	// EntityAssociation classes
}