﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using Ellucian.Colleague.Data.FinancialAid.DataContracts;
using Ellucian.Colleague.Domain.FinancialAid.Entities;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Dmi.Runtime;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;
using Ellucian.Data.Colleague.DataContracts;
using System.Threading.Tasks;
using System.Text;

namespace Ellucian.Colleague.Data.FinancialAid.Repositories
{
    /// <summary>
    /// Provides read-only access to fundamental FinancialAid data
    /// </summary>
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class FinancialAidFundRepository : BaseColleagueRepository, IFinancialAidFundRepository
    {

        private Data.Base.DataContracts.IntlParams internationalParameters;

        /// <summary>
        /// Constructor for the FinancialAidReferenceDataRepository. 
        /// CacheTimeout value is set for Level1
        /// </summary>
        /// <param name="cacheProvider">CacheProvider</param>
        /// <param name="transactionFactory">TransactionFactory</param>
        /// <param name="logger">Logger</param>
        public FinancialAidFundRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            CacheTimeout = Level1CacheTimeoutValue;
        }
   
        public async Task<IEnumerable<FinancialAidFund>> GetFinancialAidFundsAsync(bool ignoreCache = false)
        {
            return await GetGuidCodeItemAsync<Awards, FinancialAidFund>("AllFinancialAidFunds", "AWARDS",
                (fa, g) => new FinancialAidFund(g, fa.Recordkey, !string.IsNullOrEmpty(fa.AwDescription) ? fa.AwDescription : fa.Recordkey) { Description2 = fa.AwExplanationText, Source = fa.AwType, CategoryCode = fa.AwCategory, FundingType = fa.AwReportingFundingType }, bypassCache: ignoreCache);
        }

        /// <summary>
        /// Get FinancialAidFundsFinancialProperty for the given award years
        /// </summary>
        /// <param name="fundYears">The funding years for which to get funds</param>
        /// <returns>A list of FinancialAidFundsFinancialProperty objects for the given award id and fund years</returns>
        public async Task<IEnumerable<FinancialAidFundsFinancialProperty>> GetFinancialAidFundFinancialsAsync(string awardId, IEnumerable<string> fundYears, string hostCountry)
        {
            var criteria = new StringBuilder();

            if (!string.IsNullOrEmpty(awardId))
            {
            //    throw new ArgumentNullException("awardId");
                criteria.AppendFormat("WITH FUNDOFC.FUND.ID EQ '{0}'", awardId);
            }

            //var criteria = string.Format("WITH FUNDOFC.FUND.ID EQ '{0}'", awardId);

            if (fundYears == null || !fundYears.Any())
            {
                logger.Info(string.Format("Cannot get budget components for student {0} with no studentAwardYears", awardId));
                return new List<FinancialAidFundsFinancialProperty>();
            }

            var faFinancials = new List<FinancialAidFundsFinancialProperty>();
            foreach (var faFundYear in fundYears)
            {
                var fundOfficeAcyrFile = "FUND.OFFICE." + faFundYear;
                var fofcRecord = await DataReader.BulkReadRecordAsync<FundOfficeAcyr>(fundOfficeAcyrFile, criteria.ToString());
                if (fofcRecord != null)
                {
                    foreach (var fofcEntity in fofcRecord)
                    {
                        try
                        {
                            if (fofcEntity.FundofcBudgetAmt != null) {
                                faFinancials.Add(
                                    new FinancialAidFundsFinancialProperty(
                                        faFundYear,
                                        fofcEntity.Recordkey.Split('*')[1],
                                        (decimal) fofcEntity.FundofcBudgetAmt,
                                        fofcEntity.Recordkey.Split('*')[0],
                                        fofcEntity.FundofcBudgetAmt + (fofcEntity.FundofcOverAmt != null ? fofcEntity.FundofcOverAmt : 0) + (fofcEntity.FundofcOverPct != null ? fofcEntity.FundofcOverPct*fofcEntity.FundofcBudgetAmt : 0))
                                    );
                            }
                        }
                        catch (Exception e)
                        {
                            var message =
                                string.Format("Unable to create financial {0} for award {1}, fund year {2}", fofcEntity.Recordkey, awardId, faFundYear);
                            logger.Error(e, message);
                        }
                    }
                }
            }

            return faFinancials;
        }
    }
}
