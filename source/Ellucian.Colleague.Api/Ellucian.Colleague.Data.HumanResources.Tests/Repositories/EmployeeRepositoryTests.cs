﻿using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.HumanResources.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Colleague.Data.HumanResources.DataContracts;
using Ellucian.Colleague.Data.Base.DataContracts;

namespace Ellucian.Colleague.Data.HumanResources.Tests.Repositories
{
    [TestClass]
    public class EmployeeRepositoryTests : BaseRepositorySetup
    {
        private EmployeeRepository repositoryUnderTest;

        //test data objects
        private List<string> employeeIdList;
        private List<string> personIdList;
        private List<string> hrperIdList;        
        private Collection<Employes> employesCollDataList;
        private Collection<Perpos> perposCollDataList;
        private Collection<Perposwg> perposwgCollDataList;
        private Collection<Perstat> perstatCollDataList;
        private Collection<Perben> perbenCollDataList;
        private Collection<Hrper> hrperCollDataList;
        private List<string> excludeBenefits = new List<string>() { "1EJR", "4JAN" };
        private List<string> leaveStatuses = new List<string>() { "PR", "SUSP" };
            
            
        [TestInitialize]
        public void Initialize()
        {
            MockInitialize();
            repositoryUnderTest = new EmployeeRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettings);

            TestDataSetup();

            dataReaderMock.Setup(repo => repo.SelectAsync("EMPLOYES", It.IsAny<string>())).ReturnsAsync(employeeIdList.ToArray());

            dataReaderMock.Setup(repo => repo.SelectAsync("HRPER", It.IsAny<string>())).ReturnsAsync(hrperIdList.ToArray());

            dataReaderMock.Setup(repo => repo.BulkReadRecordAsync<Employes>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .ReturnsAsync(employesCollDataList);

            dataReaderMock.Setup(repo => repo.SelectAsync("PERPOS", It.IsAny<string>(),
                        It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>())).ReturnsAsync(personIdList.ToArray());

            dataReaderMock.Setup(repo => repo.BulkReadRecordAsync<Perpos>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .ReturnsAsync(perposCollDataList);

            dataReaderMock.Setup(repo => repo.SelectAsync("PERPOSWG", It.IsAny<string>(),
                        It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>())).ReturnsAsync(personIdList.ToArray());

            dataReaderMock.Setup(repo => repo.BulkReadRecordAsync<Perposwg>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .ReturnsAsync(perposwgCollDataList);

            dataReaderMock.Setup(repo => repo.SelectAsync("PERSTAT", It.IsAny<string>(),
                        It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>())).ReturnsAsync(personIdList.ToArray());

            dataReaderMock.Setup(repo => repo.BulkReadRecordAsync<Perstat>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .ReturnsAsync(perstatCollDataList);

            dataReaderMock.Setup(repo => repo.SelectAsync("PERBEN", It.IsAny<string>(),
                        It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>())).ReturnsAsync(personIdList.ToArray());

            dataReaderMock.Setup(repo => repo.BulkReadRecordAsync<Perben>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .ReturnsAsync(perbenCollDataList);

            dataReaderMock.Setup(repo => repo.BulkReadRecordAsync<Hrper>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .ReturnsAsync(hrperCollDataList);

            string fileName = "CORE.PARMS";
            string field = "LDM.DEFAULTS";
            LdmDefaults ldmDefaults = new LdmDefaults() { LdmdExcludeBenefits = excludeBenefits, LdmdLeaveStatusCodes = leaveStatuses };
            dataReaderMock.Setup(repo => repo.ReadRecord<LdmDefaults>(fileName, field, It.IsAny<bool>())).Returns(ldmDefaults);
        }

        [TestMethod]
        public async Task GetAllEmployees_V7()
        {
            var testData = repositoryUnderTest.GetEmployeesAsync(0, 100, campus: Guid.NewGuid().ToString()).Result;

            Assert.AreEqual(testData.Item2, 40);
        }

        [TestMethod]
        public async Task GetNoEmployees_V7()
        {
            var hrperIdList2 = new List<string>();
            dataReaderMock.Setup(repo => repo.SelectAsync("HRPER", It.IsAny<string>())).ReturnsAsync(hrperIdList2.ToArray());
            var testData = repositoryUnderTest.GetEmployeesAsync(0, 100, campus: Guid.NewGuid().ToString()).Result;

            Assert.AreEqual(testData.Item2, 0);
        }


        public void TestDataSetup()
        {
            employeeIdList = new List<string>()
            {
                "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
                "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40",
                "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60",
                "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80"
            };

            hrperIdList = new List<string>()
            {
                "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60",
                "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80",
                "91", "92", "93", "94", "95", "96", "97", "98", "99", "100", "101", "102", "103", "104", "105", "106", "107", "108", "109", "110",
                "111", "112", "113", "114", "115", "116", "117", "118", "119", "120", "121", "122", "123", "124", "125", "126", "127", "128", "129", "130",
                "131", "132", "133", "134", "135", "136", "137", "138", "139", "140", "141", "142", "143", "144", "145", "146", "147", "148", "149", "150"
            };

            personIdList = new List<string>()
            {
                "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60",
                "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80"
            };

            employesCollDataList = new Collection<Employes>();

            for (int i = 41; i < 81; i++)
            {
                employesCollDataList.Add(new Employes()
                {
                   RecordGuid = Guid.NewGuid().ToString(),
                   RecordModelName = "employees",
                   Recordkey = i.ToString()
                });
            }

            perposCollDataList = new Collection<Perpos>();

            for (int i = 41; i < 81; i++)
            {
                perposCollDataList.Add(new Perpos()
                {
                    PerposAltSupervisorId = i.ToString(),
                    PerposHrpId = i.ToString(),
                    PerposSupervisorHrpId = i.ToString(),
                    PerposPositionId = i.ToString(),
                    PerposStartDate = DateTime.Now.AddDays(-1),
                    PerposEndDate = DateTime.Now.AddYears(1),
                    Recordkey = i.ToString()
                });
            }

            perposwgCollDataList = new Collection<Perposwg>();

            for (int i = 41; i < 81; i++)
            {
                perposwgCollDataList.Add(new Perposwg()
                {
                    PpwgBaseEt = "et",
                    PpwgEndDate = DateTime.Now.AddYears(1),
                    PpwgStartDate = DateTime.Now.AddDays(-1),
                    PpwgHrpId = i.ToString(),
                    Recordkey = i.ToString()
                });
            }

            perstatCollDataList = new Collection<Perstat>();

            //for (int i = 41; i < 81; i++)
            for (int i = 41; i < 80; i++)
            {
                perstatCollDataList.Add(new Perstat()
                {
                    Recordkey = i.ToString(),
                    PerstatHrpId = i.ToString(),
                    PerstatEndDate = DateTime.Now.AddYears(1),
                    PerstatEndReason = "End Time",
                    PerstatPrimaryPerposId = i.ToString(),
                    PerstatPrimaryPosId = i.ToString(),
                    PerstatStartDate = DateTime.Now.AddDays(-1),
                    PerstatStatus = "FT"
                });
            }
            for (int i = 80; i < 81; i++)
            {
                perstatCollDataList.Add(new Perstat()
                {
                    Recordkey = i.ToString(),
                    PerstatHrpId = i.ToString(),
                    PerstatEndDate = DateTime.Now.AddYears(1),
                    PerstatEndReason = "End Time",
                    PerstatPrimaryPerposId = i.ToString(),
                    PerstatPrimaryPosId = i.ToString(),
                    PerstatStartDate = DateTime.Now.AddDays(-1),
                    PerstatStatus = "PR"
                });
            }

            perbenCollDataList = new Collection<Perben>();

            for (int i = 41; i < 81; i++)
            {
                perbenCollDataList.Add(new Perben()
                {
                    Recordkey = i.ToString(),
                    PerbenBdId = i.ToString(),
                    PerbenCancelDate = DateTime.Now,
                    PerbenHrpId = i.ToString()
                });
            }

            hrperCollDataList = new Collection<Hrper>();

            for (int i = 41; i < 81; i++)
            {
                hrperCollDataList.Add(new Hrper()
                {
                    Recordkey = i.ToString()
                });
            }
            
        }

    }
}
