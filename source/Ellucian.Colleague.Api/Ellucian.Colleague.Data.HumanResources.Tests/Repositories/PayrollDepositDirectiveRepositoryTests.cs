﻿using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.HumanResources.DataContracts;
using Ellucian.Colleague.Data.HumanResources.Repositories;
using Ellucian.Colleague.Data.HumanResources.Transactions;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.HumanResources.Tests;
using Ellucian.Data.Colleague;
using Ellucian.Web.Cache;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Caching;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.HumanResources.Tests.Repositories
{
    [TestClass]
    public class PayrollDepositDirectiveRepositoryTests : BaseRepositorySetup
    {

        public PayrollDepositDirectivesRepository repositoryUnderTest;
        public TestPayrollDepositDirectivesRepository testData;
        public Mock<BaseCachingRepository> cacheRepoMock;

        public UpdatePayrollDepositsRequest updateRequest;
        public UpdatePayrollDepositsResponse updateResponse;
        public CreatePayrollDepositRequest createRequest;
        public CreatePayrollDepositResponse createResponse;
        public DeletePayrollDepositsRequest deleteRequest;
        public DeletePayrollDepositsResponse deleteResponse;

        public void PayrollDepositDirectiveRepositoryTestsInitialize()
        {
            testData = new TestPayrollDepositDirectivesRepository();
            MockInitialize();
            repositoryUnderTest = new PayrollDepositDirectivesRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettings);

            #region get mocks

            dataReaderMock.Setup(r => r.SelectAsync("PR.DEPOSIT.CODES", "WITH DDC.IS.ARCHIVED NE 'Y'"))
                .Returns<string,string>((file,query) => Task.FromResult(testData.BankRecords.Select(r => r.Code).ToArray()));    


            dataReaderMock.Setup(d => d.BulkReadRecordAsync<PrDepositCodes>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .Returns<string[], bool>((x, y) =>
                    Task.FromResult(
                    new Collection<PrDepositCodes>(testData.BankRecords.Select(bankRecord =>
                        new PrDepositCodes()
                        {
                            Recordkey = bankRecord.Code,
                            DdcDescription = bankRecord.Description,
                            DdcTransitNo = bankRecord.RoutingNumber,
                            DdcFinInstNumber = bankRecord.InstitutionNumber,
                            DdcBrTransitNumber = bankRecord.BranchNumber
            }).ToList()))); 

            dataReaderMock.Setup(d => d.ReadRecordAsync<Employes>(It.IsAny<string>(),It.IsAny<bool>()))
                .Returns<string,bool>((employeeId,b) => Task.FromResult(new Employes() 
                { 
                    Recordkey = employeeId,
                    DirDepEntityAssociation = testData.PayrollRecords.Select(dd =>
                                    new EmployesDirDep()
                                    {
                                        EmpDepositIdAssocMember = dd.RecordKey,
                                        EmpDepAcctsLast4AssocMember = dd.Last4,                                        
                                        EmpDepositCodesAssocMember = dd.BankCode,                                        
                                        EmpDepositAmountsAssocMember = dd.Amount,
                                        EmpDepositEndDatesAssocMember = dd.EndDate,
                                        EmpDepositPrioritiesAssocMember = dd.Priority,
                                        EmpDepositStartDatesAssocMember = dd.StartDate,
                                        EmpDepositTypesAssocMember = dd.Type,
                                        EmpDepositAdddateAssocMember = dd.AddDate,
                                        EmpDepositAddtimeAssocMember = dd.AddTime,
                                        EmpDepositAddoprAssocMember = dd.AddOperator,
                                        EmpDepositChgdateAssocMember = dd.ChangeDate,
                                        EmpDepositChgtimeAssocMember = dd.ChangeTime,
                                        EmpDepositChgoprAssocMember = dd.ChangeOperator,
                                        EmpDepositChangeFlagsAssocMember = dd.ChangeFlag,
                                        EmpDepositNicknameAssocMember = dd.Nickname
                                    }).ToList()
                
                })
            );
            #endregion

            #region ctx mocks
            /*
            transManagerMock.Setup(t => t.ExecuteAsync<UpdatePayrollDepositsRequest, UpdatePayrollDepositsResponse>(It.IsAny<UpdatePayrollDepositsRequest>()))
                .Callback<UpdatePayrollDepositsRequest>(req =>
                {
                    updateRequest = req;
                    testData.UpdateEmployeeDirectDepositTestData(new TestDirectDepositsRepository.UpdateDirectDepositTransaction()
                    {
                        employeeId = req.EmployeeId,
                        directDeposits = req.EmpDirectDeposits.Select(dd =>
                            new TestDirectDepositsRepository.UpdateDirectDepositRecord()
                            {
                                amount = dd.DepositAmounts,
                                accountNumber = dd.DepositAccounts,
                                addDate = dd.DepositAddDate,
                                addOperator = dd.DepositAddOpr,
                                addTime = dd.DepositAddTime,
                                changeDate = dd.DepositChgDate,
                                changeOperator = dd.DepositChgOpr,
                                changeTime = dd.DepositChgTime,
                                code = dd.DepositCodes,
                                endDate = dd.DepositEndDates,
                                priority = dd.DepositPriorites,
                                startDate = dd.DepositStartDates,
                                type = dd.DepositTypes,
                                nickname = dd.DepositNicknames
                            }).ToList()
                    });
                })
                .Returns<UpdatePayrollDepositsRequest>(req =>
                    Task.FromResult(updateResponse)); 
             */
            #endregion

        }

        [TestClass]
        public class GetPayrollDepositDirectives : PayrollDepositDirectiveRepositoryTests
        {
            public string inputEmployeeId = "0003914";

            [TestInitialize]
            public void Initialize()
            {
                PayrollDepositDirectiveRepositoryTestsInitialize();
                
            }

            //[TestMethod]
            //public async Task ExpectedEqualsActual()
            //{
            //    var expected = await testData.GetPayrollDepositDirectivesAsync("24601");
            //    var actual = await repositoryUnderTest.GetPayrollDepositDirectivesAsync("24601");
            //    Assert.AreEqual(expected, actual);
            //}

            //[TestMethod, ExpectedException(typeof(ArgumentNullException))]
            //public async Task NullEmployeeIdExceptionTest()
            //{
            //    await repositoryUnderTest.GetPayrollDepositDirectivesAsync(null);
            //}
            
        }


        [TestClass]
        public class GetPayrollDepositDirective : PayrollDepositDirectiveRepositoryTests
        {            
            [TestInitialize]
            public void Initialize()
            {
                PayrollDepositDirectiveRepositoryTestsInitialize();
            }

            //[TestMethod]
            //public async Task ExpectedEqualsActual()
            //{
            //    var id = "001";
            //    var expected = await testData.GetPayrollDepositDirectiveAsync(id, "24601");
            //    var actual = await repositoryUnderTest.GetPayrollDepositDirectiveAsync(id,"24601");
            //    Assert.AreEqual(expected, actual);
            //}

            //[TestMethod, ExpectedException(typeof(ArgumentNullException))]
            //public async Task NullEmployeeIdExceptionTest()
            //{
            //    await repositoryUnderTest.GetPayrollDepositDirectivesAsync(null);
            //}

        }

        [TestClass]
        public class AuthenticatePayrollDepositDirectiveTests : PayrollDepositDirectiveRepositoryTests
        {
            public string inputDirectiveId;
            public string inputAccountId;
            public string inputEmployeeId;

            public AuthenticatePayrollDepositDirectiveRequest actualAuthenticateRequest;

            public Guid expectedToken;
            public DateTimeOffset expectedExpiration;

            [TestInitialize]
            public void Initialize()
            {
                PayrollDepositDirectiveRepositoryTestsInitialize();
                inputDirectiveId = "foo";
                inputAccountId = "bar";
                inputEmployeeId = "0003914";

                expectedToken = Guid.NewGuid();
                expectedExpiration = new DateTimeOffset(new DateTime(2017, 3, 21, 14, 59, 0), TimeSpan.FromHours(-7));

                transManagerMock.Setup(t => t.ExecuteAsync<AuthenticatePayrollDepositDirectiveRequest, AuthenticatePayrollDepositDirectiveResponse>(It.IsAny<AuthenticatePayrollDepositDirectiveRequest>()))
                    .Callback<AuthenticatePayrollDepositDirectiveRequest>(req => actualAuthenticateRequest = req)
                    .Returns<AuthenticatePayrollDepositDirectiveRequest>((req) => Task.FromResult(new AuthenticatePayrollDepositDirectiveResponse()
                    {
                        Token = expectedToken.ToString(),
                        ExpirationDate = expectedExpiration.ToLocalDateTime(apiSettings.ColleagueTimeZone),
                        ExpirationTime = expectedExpiration.ToLocalDateTime(apiSettings.ColleagueTimeZone)
                    }));

            }

            //[TestMethod]
            //public async Task Test()
            //{
            //    var result = await repositoryUnderTest.AuthenticatePayrollDepositDirective(inputEmployeeId, inputDirectiveId, inputAccountId);

            //    Assert.IsInstanceOfType(result, typeof(BankingAuthenticationToken));
            //    Assert.AreEqual(expectedToken, result.Token);
            //    Assert.AreEqual(expectedExpiration, result.ExpirationDateTimeOffset);
            //}
        }
    }
}
