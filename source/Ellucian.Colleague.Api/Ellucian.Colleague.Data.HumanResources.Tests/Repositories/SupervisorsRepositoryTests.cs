﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.HumanResources.DataContracts;
using Ellucian.Colleague.Data.HumanResources.Repositories;
using Ellucian.Colleague.Domain.HumanResources.Entities;
using Ellucian.Colleague.Domain.HumanResources.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.HumanResources.Tests.Repositories
{
    [TestClass]
    public class SupervisorsRepositoryTests : BaseRepositorySetup
    {
        // test data
        public TestSupervisorsRepository testRepository;
        public SupervisorsRepository actualRepository;

        public string supervisorId;
        
        // mocks
        public SupervisorsRepository BuildRepository()
        {
            // select perpos with hrpId
            dataReaderMock.Setup(d => d.SelectAsync("PERPOS", It.IsAny<string>()))
                .Returns<string, string>((f, c) =>
                        Task.FromResult((testRepository.PerposRecords == null) ? null :
                            testRepository.PerposRecords
                            .Where(rec => c.Contains(rec.PerposSupervisorHrpId))
                            .Select(rec => rec.RecordKey).ToArray()
                        ));

            // select perpos with list
            dataReaderMock.Setup(d => d.SelectAsync("PERPOS", "WITH PERPOS.SUPERVISOR.HRP.ID EQ ?", It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>()))
                .Returns<string, string, string[], string, bool, int>((f, c, values, p, r, s) =>
                        Task.FromResult((testRepository.PerposRecords == null) ? null :
                            testRepository.PerposRecords
                            .Where(rec => values.Contains(rec.PerposHrpId))
                            .Select(rec => rec.RecordKey).ToArray()
                        ));

            // read perpos with list
            dataReaderMock.Setup(d => d.BulkReadRecordAsync<Perpos>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .Returns<string[], bool>((ids, b) => Task.FromResult(testRepository.PerposRecords == null ? null :
                    new Collection<Perpos>(
                        testRepository.PerposRecords
                        .Where(rec => ids.Contains(rec.RecordKey))
                        .Select(rec => new Perpos()
                        {
                            Recordkey = rec.RecordKey,
                            PerposAltSupervisorId = rec.PerposAltSupervisorId,
                            PerposSupervisorHrpId = rec.PerposSupervisorHrpId,
                            PerposStartDate = rec.PerposStartDate,
                            PerposPositionId = rec.PerposPositionId,
                            PerposHrpId = rec.PerposHrpId,
                            PerposEndDate = rec.PerposEndDate                           
                        }).ToList()
                    )));
            // read from perpos with single
            dataReaderMock.Setup(d => d.BulkReadRecordAsync<Perpos>(It.IsAny<string>(), It.IsAny<bool>()))
                .Returns<string, bool>((ids, b) => Task.FromResult(testRepository.PerposRecords == null ? null :
                    new Collection<Perpos>(
                        testRepository.PerposRecords
                        .Where(rec => ids.Contains(rec.RecordKey))
                        .Select(rec => new Perpos()
                        {
                            Recordkey = rec.RecordKey,
                            PerposAltSupervisorId = rec.PerposAltSupervisorId,
                            PerposSupervisorHrpId = rec.PerposSupervisorHrpId,
                            PerposStartDate = rec.PerposStartDate,
                            PerposPositionId = rec.PerposPositionId,
                            PerposHrpId = rec.PerposHrpId,
                            PerposEndDate = rec.PerposEndDate
                        }).ToList()
                    )));
            // read from perpos with keys

            // read from position with supervisor position ids criteria

            // read from perpos with subordinate ids

            loggerMock.Setup(l => l.IsErrorEnabled).Returns(true);
            apiSettings.BulkReadSize = 1;

            return new SupervisorsRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettings);
        }

        // test setup
        public void SupervisorsRepositoryTestsInitialize()
        {
            supervisorId = "24601";

            base.MockInitialize();
            testRepository = new TestSupervisorsRepository();
            actualRepository = BuildRepository();
        }

        // tests
        [TestClass]
        public class GetSuperviseesBySupervisorAsyncTests : SupervisorsRepositoryTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.SupervisorsRepositoryTestsInitialize();
            }

            #region DIRECT SUPERVISORS

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NullOrEmptySupervisorIdArgTest()
            {
                supervisorId = string.Empty;
                await actualRepository.GetSuperviseesBySupervisorAsync(supervisorId);
            }

            [TestMethod]
            public async Task NullPerposKeysTest()
            {
                dataReaderMock.Setup(d => d.SelectAsync("PERPOS", It.IsAny<string>()))
                    .ReturnsAsync(null);
                
                var emptyList = await actualRepository.GetSuperviseesBySupervisorAsync(supervisorId);

                Assert.IsFalse(emptyList.Any());
                
                
            }

            // no perpos keys noted logger info 54

            // null bulkrecordread error 66

            // null perposhrpid logdataerror 74

            [TestMethod]
            public async Task ExpectedSubordinateIdsReturnedTest()
            {
                var expected = await testRepository.GetSuperviseesBySupervisorAsync(supervisorId);
                var actual = await actualRepository.GetSuperviseesBySupervisorAsync(supervisorId);
                CollectionAssert.AreEqual(expected.ToList(), actual.ToList());
            }

            #endregion

            #region POSITION SUPERVISORS


            #endregion
        }

    }
}
