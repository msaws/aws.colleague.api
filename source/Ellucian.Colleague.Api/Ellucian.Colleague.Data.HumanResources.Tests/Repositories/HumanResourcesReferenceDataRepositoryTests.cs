﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Data.HumanResources.DataContracts;
using Ellucian.Colleague.Data.HumanResources.Repositories;
using Ellucian.Colleague.Domain.HumanResources.Entities;
using Ellucian.Colleague.Domain.HumanResources.Tests;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.DataContracts;
using Ellucian.Web.Cache;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Caching;
using System.Threading;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.Base.Tests;
using Ellucian.Colleague.HumanResources.Base.Tests;

namespace Ellucian.Colleague.Data.Base.Tests.Repositories
{
    [TestClass]
    public class HumanResourcesReferenceDataRepositoryTests
    {
        
      
        /// <summary>
        /// Test class for Employment Classifications
        /// </summary>
        [TestClass]
        public class EmploymentClassifications
        {
            Mock<IColleagueTransactionFactory> _transFactoryMock;
            Mock<ICacheProvider> _cacheProviderMock;
            Mock<IColleagueDataReader> _dataAccessorMock;
            Mock<ILogger> _loggerMock;
            IEnumerable<EmploymentClassification> _allEmploymentClassifications;
            ApplValcodes _employmentClassificationValcodeResponse;
            string _valcodeName;

            HumanResourcesReferenceDataRepository _referenceDataRepo;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = new Mock<ILogger>();

                // Build employment classifications responses used for mocking
                _allEmploymentClassifications = new TestEmploymentClassRepository().GetEmploymentClassifications();
                _employmentClassificationValcodeResponse = BuildValcodeResponse(_allEmploymentClassifications);

                // Build privacy statuses repository
                _referenceDataRepo = BuildValidReferenceDataRepository();
                _valcodeName = _referenceDataRepo.BuildFullCacheKey("HR_CLASSIFICATIONS_GUID");
            }

            [TestCleanup]
            public void Cleanup()
            {
                _transFactoryMock = null;
                _dataAccessorMock = null;
                _cacheProviderMock = null;
                _employmentClassificationValcodeResponse = null;
                _allEmploymentClassifications = null;
                _referenceDataRepo = null;
            }

            [TestMethod]
            public async Task GetsEmploymentClassificationsCacheAsync()
            {
                var employmentClassifications = await _referenceDataRepo.GetEmploymentClassificationsAsync(false);
                for (int i = 0; i < employmentClassifications.Count(); i++)
                {
                    Assert.AreEqual(_allEmploymentClassifications.ElementAt(i).Code, employmentClassifications.ElementAt(i).Code);
                    Assert.AreEqual(_allEmploymentClassifications.ElementAt(i).Description, employmentClassifications.ElementAt(i).Description);
                }
            }

            [TestMethod]
            public async Task GetsEmploymentClassificationsNonCacheAsync()
            {
                var employmentClassifications = await _referenceDataRepo.GetEmploymentClassificationsAsync(true);
                for (int i = 0; i < employmentClassifications.Count(); i++)
                {
                    Assert.AreEqual(_allEmploymentClassifications.ElementAt(i).Code, employmentClassifications.ElementAt(i).Code);
                    Assert.AreEqual(_allEmploymentClassifications.ElementAt(i).Description, employmentClassifications.ElementAt(i).Description);
                }
            }

            [TestMethod]
            public async Task GetEmploymentClassifications_WritesToCacheAsync()
            {

                // Set up local cache mock to respond to cache request:
                //  -to "Contains" request, return "false" to indicate item is not in cache
                //  -to cache "Get" request, return null so we know it's reading from the "repository"
                _cacheProviderMock.Setup(x => x.Contains(_valcodeName, null)).Returns(false);
                _cacheProviderMock.Setup(x => x.Get(_valcodeName, null)).Returns(null);

                // return a valid response to the data accessor request
                _dataAccessorMock.Setup(acc => acc.ReadRecordAsync<ApplValcodes>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(_employmentClassificationValcodeResponse);

                _cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
                 x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                 .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

                // But after data accessor read, set up mocking so we can verify the list of employment classifications was written to the cache
                _cacheProviderMock.Setup(x => x.Add(It.IsAny<string>(), It.IsAny<Task<IEnumerable<EmploymentClassification>>>(), It.IsAny<CacheItemPolicy>(), null)).Verifiable();

                _cacheProviderMock.Setup(x => x.Contains(_referenceDataRepo.BuildFullCacheKey("HR_CLASSIFICATIONS"), null)).Returns(true);
                var employmentClassifications = await _referenceDataRepo.GetEmploymentClassificationsAsync(false);
                _cacheProviderMock.Setup(x => x.Get(_referenceDataRepo.BuildFullCacheKey("HR_CLASSIFICATIONS"), null)).Returns(employmentClassifications);
                // Verify that employment classifications were returned, which means they came from the "repository".
                Assert.IsTrue(employmentClassifications.Count() == 4);

                // Verify that the employment classification item was added to the cache after it was read from the repository
                _cacheProviderMock.Verify(m => m.Add(It.IsAny<string>(), It.IsAny<Task<IEnumerable<EmploymentClassification>>>(), It.IsAny<CacheItemPolicy>(), null), Times.Never);

            }

            [TestMethod]
            public async Task GetEmploymentClassifications_GetsCachedEmploymentClassificationsAsync()
            {
                // Set up local cache mock to respond to cache request:
                //  -to "Contains" request, return "true" to indicate item is in cache
                //  -to "Get" request, return the cache item (in this case the "CLASSIFICATIONS" cache item)
                _cacheProviderMock.Setup(x => x.Contains(_valcodeName, null)).Returns(true);
                _cacheProviderMock.Setup(x => x.Get(_valcodeName, null)).Returns(_allEmploymentClassifications).Verifiable();

                // return null for request, so that if we have a result, it wasn't the data accessor that returned it.
                _dataAccessorMock.Setup(acc => acc.ReadRecordAsync<ApplValcodes>("HR.VALCODES", "CLASSIFICATIONS", true)).ReturnsAsync(new ApplValcodes());

                // Assert the employment classifications are returned
                Assert.IsTrue((await _referenceDataRepo.GetEmploymentClassificationsAsync(false)).Count() == 4);
                // Verify that the employment classifications were retrieved from cache
                _cacheProviderMock.Verify(m => m.Get(_valcodeName, null));
            }

            private HumanResourcesReferenceDataRepository BuildValidReferenceDataRepository()
            {
                // transaction factory mock
                _transFactoryMock = new Mock<IColleagueTransactionFactory>();
                // Cache Provider Mock
                _cacheProviderMock = new Mock<ICacheProvider>();
                // Set up data accessor for mocking 
                _dataAccessorMock = new Mock<IColleagueDataReader>();

                // Set up dataAccessorMock as the object for the DataAccessor
                _transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(_dataAccessorMock.Object);

                // Setup response to privacy status valcode read
                _dataAccessorMock.Setup(acc => acc.ReadRecordAsync<ApplValcodes>("HR.VALCODES", "CLASSIFICATIONS", It.IsAny<bool>())).ReturnsAsync(_employmentClassificationValcodeResponse);
                _cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x => x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

                _dataAccessorMock.Setup(acc => acc.SelectAsync(It.IsAny<RecordKeyLookup[]>())).Returns<RecordKeyLookup[]>(recordKeyLookups =>
                {
                    var result = new Dictionary<string, RecordKeyLookupResult>();
                    foreach (var recordKeyLookup in recordKeyLookups)
                    {
                        var employmentClassification = _allEmploymentClassifications.Where(e => e.Code == recordKeyLookup.SecondaryKey).FirstOrDefault();
                        result.Add(string.Join("+", new string[] { "HR.VALCODES", "CLASSIFICATIONS", employmentClassification.Code }),
                            new RecordKeyLookupResult() { Guid = employmentClassification.Guid });
                    }
                    return Task.FromResult(result);
                });

                // Construct repository
                _referenceDataRepo = new HumanResourcesReferenceDataRepository(_cacheProviderMock.Object, _transFactoryMock.Object, _loggerMock.Object);

                return _referenceDataRepo;
            }

            private ApplValcodes BuildValcodeResponse(IEnumerable<EmploymentClassification> employmentClassifications)
            {
                ApplValcodes valcodeResponse = new ApplValcodes();
                valcodeResponse.ValsEntityAssociation = new List<ApplValcodesVals>();
                foreach (var item in employmentClassifications)
                {
                    valcodeResponse.ValsEntityAssociation.Add(new ApplValcodesVals("", item.Description, "", item.Code, "", "", ""));
                }
                return valcodeResponse;
            }
        }

        /// <summary>
        /// Test class for Employment Proficiencies codes
        /// </summary>
        [TestClass]
        public class EmploymentProficienciesTests
        {
            Mock<IColleagueTransactionFactory> transFactoryMock;
            Mock<ICacheProvider> cacheProviderMock;
            Mock<IColleagueDataReader> dataAccessorMock;
            Mock<ILogger> loggerMock;
            IEnumerable<EmploymentProficiency> allEmploymentProficiencies;
            string codeItemName;

            HumanResourcesReferenceDataRepository referenceDataRepo;

            [TestInitialize]
            public void Initialize()
            {
                loggerMock = new Mock<ILogger>();

                // Build responses used for mocking
                allEmploymentProficiencies = new TestEmploymentProficiencyRepository().GetEmploymentProficiencies();

                // Build repository
                referenceDataRepo = BuildValidReferenceDataRepository();
                codeItemName = referenceDataRepo.BuildFullCacheKey("AllEmploymentProficiencies");
            }

            [TestCleanup]
            public void Cleanup()
            {
                transFactoryMock = null;
                dataAccessorMock = null;
                cacheProviderMock = null;
                allEmploymentProficiencies = null;
                referenceDataRepo = null;
            }

            [TestMethod]
            public async Task GetsEmploymentProficienciesCacheAsync()
            {
                var employmentProficiencies = await referenceDataRepo.GetEmploymentProficienciesAsync(false);

                for (int i = 0; i < allEmploymentProficiencies.Count(); i++)
                {
                    Assert.AreEqual(allEmploymentProficiencies.ElementAt(i).Guid, employmentProficiencies.ElementAt(i).Guid);
                    Assert.AreEqual(allEmploymentProficiencies.ElementAt(i).Code, employmentProficiencies.ElementAt(i).Code);
                    Assert.AreEqual(allEmploymentProficiencies.ElementAt(i).Description, employmentProficiencies.ElementAt(i).Description);
                }
            }

            [TestMethod]
            public async Task GetsEmploymentProficienciesNonCacheAsync()
            {
                var employmentProficiencies = await referenceDataRepo.GetEmploymentProficienciesAsync(true);

                for (int i = 0; i < allEmploymentProficiencies.Count(); i++)
                {
                    Assert.AreEqual(allEmploymentProficiencies.ElementAt(i).Guid, employmentProficiencies.ElementAt(i).Guid);
                    Assert.AreEqual(allEmploymentProficiencies.ElementAt(i).Code, employmentProficiencies.ElementAt(i).Code);
                    Assert.AreEqual(allEmploymentProficiencies.ElementAt(i).Description, employmentProficiencies.ElementAt(i).Description);
                }
            }

            private HumanResourcesReferenceDataRepository BuildValidReferenceDataRepository()
            {
                // transaction factory mock
                transFactoryMock = new Mock<IColleagueTransactionFactory>();
                // Cache Provider Mock
                cacheProviderMock = new Mock<ICacheProvider>();
                // Set up data accessor for mocking 
                dataAccessorMock = new Mock<IColleagueDataReader>();

                // Set up dataAccessorMock as the object for the DataAccessor
                transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(dataAccessorMock.Object);

                // Setup response to EmploymentProficiencies read
                var employmentProficienciesCollection = new Collection<Jobskills>(allEmploymentProficiencies.Select(record =>
                    new Data.HumanResources.DataContracts.Jobskills()
                    {
                        Recordkey = record.Code,
                        JskDesc = record.Description,
                        RecordGuid = record.Guid,
                        JskLicenseCert = "Y",
                        JskAuthority = "AUTH",
                        JskComment = "COMMENT"
                    }).ToList());

                dataAccessorMock.Setup(acc => acc.BulkReadRecordAsync<Jobskills>("JOBSKILLS", "", It.IsAny<bool>()))
                    .ReturnsAsync(employmentProficienciesCollection);

                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x => x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                    .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));


                dataAccessorMock.Setup(acc => acc.SelectAsync(It.IsAny<RecordKeyLookup[]>())).Returns<RecordKeyLookup[]>(recordKeyLookups =>
                {
                    var result = new Dictionary<string, RecordKeyLookupResult>();
                    foreach (var recordKeyLookup in recordKeyLookups)
                    {
                        var employmentProficiency = allEmploymentProficiencies.Where(e => e.Code == recordKeyLookup.PrimaryKey).FirstOrDefault();
                        result.Add(string.Join("+", new string[] { "JOBSKILLS", employmentProficiency.Code }),
                            new RecordKeyLookupResult() { Guid = employmentProficiency.Guid });
                    }
                    return Task.FromResult(result);
                });

                // Construct repository
                referenceDataRepo = new HumanResourcesReferenceDataRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);

                return referenceDataRepo;
            }
        }

        [TestClass]
        public class DeductionTypesTests : BaseRepositorySetup
        {
            public TestDeductionTypeRepository testDataRepository;

            public HumanResourcesReferenceDataRepository repositoryUnderTest;

            public void HumanResourcesReferenceDataRepositoryTestsInitialize()
            {
                MockInitialize();
                testDataRepository = new TestDeductionTypeRepository();

                repositoryUnderTest = BuildRepository();
            }

            [TestInitialize]
            public void Initialize()
            {
                HumanResourcesReferenceDataRepositoryTestsInitialize();
            }

            public HumanResourcesReferenceDataRepository BuildRepository()
            {
                dataReaderMock.Setup(d => d.SelectAsync("BENDED", "WITH BD.PAYERS = 'E''S'"))
                    .Returns<string, string>((f, c) => Task.FromResult(testDataRepository.GetDeductionTypes() == null ? null :
                        testDataRepository.GetDeductionTypes().Select(dtype => dtype.Guid).ToArray()));

                dataReaderMock.Setup(d => d.BulkReadRecordAsync<Bended>(It.IsAny<string[]>(), It.IsAny<bool>()))
                    .Returns<string[], bool>((ids, b) =>
                        Task.FromResult(testDataRepository.GetDeductionTypes() == null ? null :
                            new Collection<Bended>(testDataRepository.GetDeductionTypes()
                                .Where(record => ids.Contains(record.Guid))
                                .Select(record =>
                                    (record == null) ? null : new Bended()
                                    {
                                        Recordkey = record.Code,
                                        BdDesc = record.Description,
                                        RecordGuid = record.Guid
                                    }).ToList())
                        ));

                apiSettings.BulkReadSize = 1;

                loggerMock.Setup(l => l.IsErrorEnabled).Returns(true);

                return new HumanResourcesReferenceDataRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);
            }

            public async Task<IEnumerable<DeductionType>> getExpectedDeductionTypes()
            {
                return testDataRepository.GetDeductionTypes();
            }

            public async Task<IEnumerable<DeductionType>> getActualDeductionTypes(bool ignoreCache = false)
            {
                return await repositoryUnderTest.GetDeductionTypesAsync(ignoreCache);
            }

            [TestMethod]
            public async Task ExpectedEqualsActualTest()
            {
                var expected = (await getExpectedDeductionTypes()).ToList();
                var actual = (await getActualDeductionTypes()).ToList();
                CollectionAssert.AreEqual(expected, actual);
            }

            [TestMethod]
            public async Task AttributesTest()
            {
                var expected = (await getExpectedDeductionTypes()).ToArray();
                var actual = (await getActualDeductionTypes()).ToArray();
                for (int i = 0; i < expected.Count(); i++)
                {
                    Assert.AreEqual(expected[i].Code, actual[i].Code);
                    Assert.AreEqual(expected[i].Guid, actual[i].Guid);
                    Assert.AreEqual(expected[i].Description, actual[i].Description);
                }
            }

            [TestMethod]
            public async Task ExpectedEqualsActualTest_Cached()
            {
                var expected = (await getExpectedDeductionTypes()).ToList();
                var actual = (await getActualDeductionTypes(true)).ToList();
                CollectionAssert.AreEqual(expected, actual);
            }

        }

        [TestClass]
        public class EmploymentStatusEndingReason_GETAll
        {
            Mock<IColleagueTransactionFactory> transFactoryMock;
            Mock<ICacheProvider> cacheProviderMock;
            Mock<IColleagueDataReader> dataAccessorMock;
            Mock<ILogger> loggerMock;
            IEnumerable<EmploymentStatusEndingReason> allEmploymentStatusEndingReason;
            ApplValcodes employmentStatusEndingReasonValcodeResponse;
            string valcodeName;

            HumanResourcesReferenceDataRepository referenceDataRepo;

            [TestInitialize]
            public void Initialize()
            {
                loggerMock = new Mock<ILogger>();

                // Build EmploymentStatusEndingReason responses used for mocking
                allEmploymentStatusEndingReason = new TestEmploymentStatusEndingReasonRepository().GetEmploymentStatusEndingReasons();
                employmentStatusEndingReasonValcodeResponse = BuildValcodeResponse(allEmploymentStatusEndingReason);

                // Build privacy statuses repository
                referenceDataRepo = BuildValidReferenceDataRepository();
                valcodeName = referenceDataRepo.BuildFullCacheKey("HR_STATUS_ENDING_REASONS_GUID");
            }

            [TestCleanup]
            public void Cleanup()
            {
                transFactoryMock = null;
                dataAccessorMock = null;
                cacheProviderMock = null;
                employmentStatusEndingReasonValcodeResponse = null;
                allEmploymentStatusEndingReason = null;
                referenceDataRepo = null;
            }

            [TestMethod]
            public async Task GetsEmploymentStatusEndingReasonsCacheAsync()
            {
                var EmploymentStatusEndingReasons = await referenceDataRepo.GetEmploymentStatusEndingReasonsAsync(false);
                for (int i = 0; i < EmploymentStatusEndingReasons.Count(); i++)
                {
                    Assert.AreEqual(allEmploymentStatusEndingReason.ElementAt(i).Code, EmploymentStatusEndingReasons.ElementAt(i).Code);
                    Assert.AreEqual(allEmploymentStatusEndingReason.ElementAt(i).Description, EmploymentStatusEndingReasons.ElementAt(i).Description);
                }
            }

            [TestMethod]
            public async Task GetsEmploymentStatusEndingReasonsNonCacheAsync()
            {
                var EmploymentStatusEndingReasons = await referenceDataRepo.GetEmploymentStatusEndingReasonsAsync(true);
                for (int i = 0; i < EmploymentStatusEndingReasons.Count(); i++)
                {
                    Assert.AreEqual(allEmploymentStatusEndingReason.ElementAt(i).Code, EmploymentStatusEndingReasons.ElementAt(i).Code);
                    Assert.AreEqual(allEmploymentStatusEndingReason.ElementAt(i).Description, EmploymentStatusEndingReasons.ElementAt(i).Description);
                }
            }

            [TestMethod]
            public async Task GetEmploymentStatusEndingReasons_WritesToCacheAsync()
            {
                cacheProviderMock.Setup(x => x.Contains(valcodeName, null)).Returns(false);
                cacheProviderMock.Setup(x => x.Get(valcodeName, null)).Returns(null);

                // return a valid response to the data accessor request
                dataAccessorMock.Setup(acc => acc.ReadRecordAsync<ApplValcodes>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(employmentStatusEndingReasonValcodeResponse);

                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
                 x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                 .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

                // But after data accessor read, set up mocking so we can verify the list of employment classifications was written to the cache
                cacheProviderMock.Setup(x => x.Add(It.IsAny<string>(), It.IsAny<Task<IEnumerable<EmploymentStatusEndingReason>>>(), It.IsAny<CacheItemPolicy>(), null)).Verifiable();

                cacheProviderMock.Setup(x => x.Contains(referenceDataRepo.BuildFullCacheKey("HR_TERMINATION_REASONS"), null)).Returns(true);
                var employmentClassifications = await referenceDataRepo.GetEmploymentStatusEndingReasonsAsync(false);
                cacheProviderMock.Setup(x => x.Get(referenceDataRepo.BuildFullCacheKey("HR_TERMINATION_REASONS"), null)).Returns(employmentClassifications);
                // Verify that employment termination reasons were returned, which means they came from the "repository".
                Assert.IsTrue(employmentClassifications.Count() == 6);

                // Verify that the employment classification item was added to the cache after it was read from the repository
                cacheProviderMock.Verify(m => m.Add(It.IsAny<string>(), It.IsAny<Task<IEnumerable<EmploymentStatusEndingReason>>>(), It.IsAny<CacheItemPolicy>(), null), Times.Never);

            }

            private HumanResourcesReferenceDataRepository BuildValidReferenceDataRepository()
            {
                // transaction factory mock
                transFactoryMock = new Mock<IColleagueTransactionFactory>();
                // Cache Provider Mock
                cacheProviderMock = new Mock<ICacheProvider>();
                // Set up data accessor for mocking 
                dataAccessorMock = new Mock<IColleagueDataReader>();

                // Set up dataAccessorMock as the object for the DataAccessor
                transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(dataAccessorMock.Object);

                // Setup response to privacy status valcode read
                dataAccessorMock.Setup(acc => acc.ReadRecordAsync<ApplValcodes>("HR.VALCODES", "STATUS.ENDING.REASONS", It.IsAny<bool>())).ReturnsAsync(employmentStatusEndingReasonValcodeResponse);
                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x => x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

                dataAccessorMock.Setup(acc => acc.SelectAsync(It.IsAny<RecordKeyLookup[]>())).Returns<RecordKeyLookup[]>(recordKeyLookups =>
                {
                    var result = new Dictionary<string, RecordKeyLookupResult>();
                    foreach (var recordKeyLookup in recordKeyLookups)
                    {
                        var employmentStatusEndingReasons = allEmploymentStatusEndingReason.Where(e => e.Code == recordKeyLookup.SecondaryKey).FirstOrDefault();
                        result.Add(string.Join("+", new string[] { "HR.VALCODES", "STATUS.ENDING.REASONS", employmentStatusEndingReasons.Code }),
                            new RecordKeyLookupResult() { Guid = employmentStatusEndingReasons.Guid });
                    }
                    return Task.FromResult(result);
                });

                // Construct repository
                referenceDataRepo = new HumanResourcesReferenceDataRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);

                return referenceDataRepo;
            }

            private ApplValcodes BuildValcodeResponse(IEnumerable<Ellucian.Colleague.Domain.HumanResources.Entities.EmploymentStatusEndingReason> employmentStatusEndingReasons)
            {
                ApplValcodes valcodeResponse = new ApplValcodes();
                valcodeResponse.ValsEntityAssociation = new List<ApplValcodesVals>();
                foreach (var item in employmentStatusEndingReasons)
                {
                    valcodeResponse.ValsEntityAssociation.Add(new ApplValcodesVals("", item.Description, "", item.Code, "", "", ""));
                }
                return valcodeResponse;
            }
        }

        [TestClass]
        public class PayrollDeductionArrangementChangeReason_GETAll
        {
            Mock<IColleagueTransactionFactory> transFactoryMock;
            Mock<ICacheProvider> cacheProviderMock;
            Mock<IColleagueDataReader> dataAccessorMock;
            Mock<ILogger> loggerMock;
            IEnumerable<PayrollDeductionArrangementChangeReason> allPayrollDeductionArrangementChangeReason;
            ApplValcodes payrollDeductionArrangementChangeReasonValcodeResponse;
            string valcodeName;

            HumanResourcesReferenceDataRepository referenceDataRepo;

            [TestInitialize]
            public void Initialize()
            {
                loggerMock = new Mock<ILogger>();

                // Build PayrollDeductionArrangementChangeReason used for mocking
                allPayrollDeductionArrangementChangeReason = new TestPayrollDeductionArrangementChangeReasonRepository().GetPayrollDeductionArrangementChangeReasons();
                payrollDeductionArrangementChangeReasonValcodeResponse = BuildValcodeResponse(allPayrollDeductionArrangementChangeReason);

                // Build privacy statuses repository
                referenceDataRepo = BuildValidReferenceDataRepository();
                valcodeName = referenceDataRepo.BuildFullCacheKey("HR_BENDED_CHANGE_REASONS_GUID");
            }

            [TestCleanup]
            public void Cleanup()
            {
                transFactoryMock = null;
                dataAccessorMock = null;
                cacheProviderMock = null;
                payrollDeductionArrangementChangeReasonValcodeResponse = null;
                allPayrollDeductionArrangementChangeReason = null;
                referenceDataRepo = null;
            }

            [TestMethod]
            public async Task GetsPayrollDeductionArrangementChangeReasonsCacheAsync()
            {
                var payrollDeductionArrangementChangeReasons = await referenceDataRepo.GetPayrollDeductionArrangementChangeReasonsAsync(false);
                for (int i = 0; i < payrollDeductionArrangementChangeReasons.Count(); i++)
                {
                    Assert.AreEqual(allPayrollDeductionArrangementChangeReason.ElementAt(i).Code, payrollDeductionArrangementChangeReasons.ElementAt(i).Code);
                    Assert.AreEqual(allPayrollDeductionArrangementChangeReason.ElementAt(i).Description, payrollDeductionArrangementChangeReasons.ElementAt(i).Description);
                }
            }

            [TestMethod]
            public async Task GetsPayrollDeductionArrangementChangeReasonsNonCacheAsync()
            {
                var payrollDeductionArrangementChangeReasons = await referenceDataRepo.GetPayrollDeductionArrangementChangeReasonsAsync(true);
                for (int i = 0; i < payrollDeductionArrangementChangeReasons.Count(); i++)
                {
                    Assert.AreEqual(allPayrollDeductionArrangementChangeReason.ElementAt(i).Code, payrollDeductionArrangementChangeReasons.ElementAt(i).Code);
                    Assert.AreEqual(allPayrollDeductionArrangementChangeReason.ElementAt(i).Description, payrollDeductionArrangementChangeReasons.ElementAt(i).Description);
                }
            }

            [TestMethod]
            public async Task GetPayrollDeductionArrangementChangeReasons_WritesToCacheAsync()
            {
                cacheProviderMock.Setup(x => x.Contains(valcodeName, null)).Returns(false);
                cacheProviderMock.Setup(x => x.Get(valcodeName, null)).Returns(null);

                // return a valid response to the data accessor request
                dataAccessorMock.Setup(acc => acc.ReadRecordAsync<ApplValcodes>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(payrollDeductionArrangementChangeReasonValcodeResponse);

                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
                 x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                 .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

                // But after data accessor read, set up mocking so we can verify the list of employment classifications was written to the cache
                cacheProviderMock.Setup(x => x.Add(It.IsAny<string>(), It.IsAny<Task<IEnumerable<PayrollDeductionArrangementChangeReason>>>(), It.IsAny<CacheItemPolicy>(), null)).Verifiable();

                cacheProviderMock.Setup(x => x.Contains(referenceDataRepo.BuildFullCacheKey("HR_BENDED_CHANGE_REASONS"), null)).Returns(true);
                var payrollDeductionArrangementChangeReasons = await referenceDataRepo.GetPayrollDeductionArrangementChangeReasonsAsync(false);
                cacheProviderMock.Setup(x => x.Get(referenceDataRepo.BuildFullCacheKey("HR_BENDED_CHANGE_REASONS"), null)).Returns(payrollDeductionArrangementChangeReasons);
                // Verify that employment termination reasons were returned, which means they came from the "repository".
                Assert.IsTrue(payrollDeductionArrangementChangeReasons.Count() == 6);

                // Verify that the employment classification item was added to the cache after it was read from the repository
                cacheProviderMock.Verify(m => m.Add(It.IsAny<string>(), It.IsAny<Task<IEnumerable<PayrollDeductionArrangementChangeReason>>>(), It.IsAny<CacheItemPolicy>(), null), Times.Never);

            }

            private HumanResourcesReferenceDataRepository BuildValidReferenceDataRepository()
            {
                // transaction factory mock
                transFactoryMock = new Mock<IColleagueTransactionFactory>();
                // Cache Provider Mock
                cacheProviderMock = new Mock<ICacheProvider>();
                // Set up data accessor for mocking 
                dataAccessorMock = new Mock<IColleagueDataReader>();

                // Set up dataAccessorMock as the object for the DataAccessor
                transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(dataAccessorMock.Object);

                // Setup response to privacy status valcode read
                dataAccessorMock.Setup(acc => acc.ReadRecordAsync<ApplValcodes>("HR.VALCODES", "BENDED.CHANGE.REASONS", It.IsAny<bool>())).ReturnsAsync(payrollDeductionArrangementChangeReasonValcodeResponse);
                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x => x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

                dataAccessorMock.Setup(acc => acc.SelectAsync(It.IsAny<RecordKeyLookup[]>())).Returns<RecordKeyLookup[]>(recordKeyLookups =>
                {
                    var result = new Dictionary<string, RecordKeyLookupResult>();
                    foreach (var recordKeyLookup in recordKeyLookups)
                    {
                        var payrollDeductionArrangementChangeReasons = allPayrollDeductionArrangementChangeReason.Where(e => e.Code == recordKeyLookup.SecondaryKey).FirstOrDefault();
                        result.Add(string.Join("+", new string[] { "HR.VALCODES", "BENDED.CHANGE.REASONS", payrollDeductionArrangementChangeReasons.Code }),
                            new RecordKeyLookupResult() { Guid = payrollDeductionArrangementChangeReasons.Guid });
                    }
                    return Task.FromResult(result);
                });

                // Construct repository
                referenceDataRepo = new HumanResourcesReferenceDataRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);

                return referenceDataRepo;
            }

            private ApplValcodes BuildValcodeResponse(IEnumerable<Ellucian.Colleague.Domain.HumanResources.Entities.PayrollDeductionArrangementChangeReason> payrollDeductionArrangementChangeReasons)
            {
                ApplValcodes valcodeResponse = new ApplValcodes();
                valcodeResponse.ValsEntityAssociation = new List<ApplValcodesVals>();
                foreach (var item in payrollDeductionArrangementChangeReasons)
                {
                    valcodeResponse.ValsEntityAssociation.Add(new ApplValcodesVals("", item.Description, "", item.Code, "", "", ""));
                }
                return valcodeResponse;
            }
        }

        /// <summary>
        /// Test class for Job Change Reasons
        /// </summary>
        [TestClass]
        public class JobChangeReasons
        {
            Mock<IColleagueTransactionFactory> transFactoryMock;
            Mock<ICacheProvider> cacheProviderMock;
            Mock<IColleagueDataReader> dataAccessorMock;
            Mock<ILogger> loggerMock;
            IEnumerable<JobChangeReason> allJobChangeReasons;
            ApplValcodes jobChangeReasonValcodeResponse;
            string valcodeName;

            HumanResourcesReferenceDataRepository referenceDataRepo;

            [TestInitialize]
            public void Initialize()
            {
                loggerMock = new Mock<ILogger>();

                // Build job change reasons responses used for mocking
                allJobChangeReasons = new TestJobChangeReasonRepository().GetJobChangeReasons();
                jobChangeReasonValcodeResponse = BuildValcodeResponse(allJobChangeReasons);

                // Build job change reasons repository
                referenceDataRepo = BuildValidReferenceDataRepository();
                valcodeName = referenceDataRepo.BuildFullCacheKey("HR_POSITION_ENDING_REASONS_GUID");
            }

            [TestCleanup]
            public void Cleanup()
            {
                transFactoryMock = null;
                dataAccessorMock = null;
                cacheProviderMock = null;
                jobChangeReasonValcodeResponse = null;
                allJobChangeReasons = null;
                referenceDataRepo = null;
            }

            [TestMethod]
            public async Task GetsJobChangeReasonsCacheAsync()
            {
                var jobChangeReasons = await referenceDataRepo.GetJobChangeReasonsAsync(false);
                for (int i = 0; i < jobChangeReasons.Count(); i++)
                {
                    Assert.AreEqual(allJobChangeReasons.ElementAt(i).Code, jobChangeReasons.ElementAt(i).Code);
                    Assert.AreEqual(allJobChangeReasons.ElementAt(i).Description, jobChangeReasons.ElementAt(i).Description);
                }
            }

            [TestMethod]
            public async Task GetsJobChangeReasonsNonCacheAsync()
            {
                var jobChangeReasons = await referenceDataRepo.GetJobChangeReasonsAsync(true);
                for (int i = 0; i < jobChangeReasons.Count(); i++)
                {
                    Assert.AreEqual(allJobChangeReasons.ElementAt(i).Code, jobChangeReasons.ElementAt(i).Code);
                    Assert.AreEqual(allJobChangeReasons.ElementAt(i).Description, jobChangeReasons.ElementAt(i).Description);
                }
            }

            [TestMethod]
            public async Task GetJobChangeReasons_WritesToCacheAsync()
            {

                // Set up local cache mock to respond to cache request:
                //  -to "Contains" request, return "false" to indicate item is not in cache
                //  -to cache "Get" request, return null so we know it's reading from the "repository"
                cacheProviderMock.Setup(x => x.Contains(valcodeName, null)).Returns(false);
                cacheProviderMock.Setup(x => x.Get(valcodeName, null)).Returns(null);

                // return a valid response to the data accessor request
                dataAccessorMock.Setup(acc => acc.ReadRecordAsync<ApplValcodes>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(jobChangeReasonValcodeResponse);

                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
                 x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                 .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

                // But after data accessor read, set up mocking so we can verify the list of job change reasons was written to the cache
                cacheProviderMock.Setup(x => x.Add(It.IsAny<string>(), It.IsAny<Task<IEnumerable<JobChangeReason>>>(), It.IsAny<CacheItemPolicy>(), null)).Verifiable();

                cacheProviderMock.Setup(x => x.Contains(referenceDataRepo.BuildFullCacheKey("HR_POSITION_ENDING_REASONS"), null)).Returns(true);
                var jobChangeReasons = await referenceDataRepo.GetJobChangeReasonsAsync(false);
                cacheProviderMock.Setup(x => x.Get(referenceDataRepo.BuildFullCacheKey("HR_POSITION_ENDING_REASONS"), null)).Returns(jobChangeReasons);
                // Verify that job change reasons were returned, which means they came from the "repository".
                Assert.IsTrue(jobChangeReasons.Count() == 4);

                // Verify that the job change reason item was added to the cache after it was read from the repository
                cacheProviderMock.Verify(m => m.Add(It.IsAny<string>(), It.IsAny<Task<IEnumerable<JobChangeReason>>>(), It.IsAny<CacheItemPolicy>(), null), Times.Never);

            }

            [TestMethod]
            public async Task GetJobChangeReasons_GetsCachedJobChangeReasonsAsync()
            {
                // Set up local cache mock to respond to cache request:
                //  -to "Contains" request, return "true" to indicate item is in cache
                //  -to "Get" request, return the cache item (in this case the "REHIRE.ELIGIBILITY.CODES" cache item)
                //cacheProviderMock.Setup(x => x.Contains(valcodeName, null)).Returns(true);
                //cacheProviderMock.Setup(x => x.Get(valcodeName, null)).Returns(allJobChangeReasons).Verifiable();

                // return null for request, so that if we have a result, it wasn't the data accessor that returned it.
                //dataAccessorMock.Setup(acc => acc.ReadRecordAsync<ApplValcodes>("HR.VALCODES", "REHIRE.ELIGIBILITY.CODES", true)).ReturnsAsync(new ApplValcodes());

                // Assert the job change reasons are returned
                Assert.IsTrue((await referenceDataRepo.GetJobChangeReasonsAsync(false)).Count() == 4);
                // Verify that the rehire types were retrieved from cache
                //cacheProviderMock.Verify(m => m.Get(valcodeName, null));
            }

            private HumanResourcesReferenceDataRepository BuildValidReferenceDataRepository()
            {
                // transaction factory mock
                transFactoryMock = new Mock<IColleagueTransactionFactory>();
                // Cache Provider Mock
                cacheProviderMock = new Mock<ICacheProvider>();
                // Set up data accessor for mocking 
                dataAccessorMock = new Mock<IColleagueDataReader>();

                // Set up dataAccessorMock as the object for the DataAccessor
                transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(dataAccessorMock.Object);

                // Setup response to job change reason valcode read
                dataAccessorMock.Setup(acc => acc.ReadRecordAsync<ApplValcodes>("HR.VALCODES", "POSITION.ENDING.REASONS", It.IsAny<bool>())).ReturnsAsync(jobChangeReasonValcodeResponse);
                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x => x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

                dataAccessorMock.Setup(acc => acc.SelectAsync(It.IsAny<RecordKeyLookup[]>())).Returns<RecordKeyLookup[]>(recordKeyLookups =>
                {
                    var result = new Dictionary<string, RecordKeyLookupResult>();
                    foreach (var recordKeyLookup in recordKeyLookups)
                    {
                        var jobChangeReason = allJobChangeReasons.Where(e => e.Code == recordKeyLookup.SecondaryKey).FirstOrDefault();
                        result.Add(string.Join("+", new string[] { "HR.VALCODES", "POSITION.ENDING.REASONS", jobChangeReason.Code }),
                            new RecordKeyLookupResult() { Guid = jobChangeReason.Guid });
                    }
                    return Task.FromResult(result);
                });

                // Construct repository
                referenceDataRepo = new HumanResourcesReferenceDataRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);

                return referenceDataRepo;
            }

            private ApplValcodes BuildValcodeResponse(IEnumerable<JobChangeReason> jobChangeReasons)
            {
                ApplValcodes valcodeResponse = new ApplValcodes();
                valcodeResponse.ValsEntityAssociation = new List<ApplValcodesVals>();
                foreach (var item in jobChangeReasons)
                {
                    valcodeResponse.ValsEntityAssociation.Add(new ApplValcodesVals("", item.Description, "", item.Code, "", "", ""));
                }
                return valcodeResponse;
            }
        }

        /// <summary>
        /// Test class for Rehire Types
        /// </summary>
        [TestClass]
        public class RehireTypes
        {
            Mock<IColleagueTransactionFactory> transFactoryMock;
            Mock<ICacheProvider> cacheProviderMock;
            Mock<IColleagueDataReader> dataAccessorMock;
            Mock<ILogger> loggerMock;
            IEnumerable<RehireType> allRehireTypes;
            ApplValcodes rehireTypeValcodeResponse;
            string valcodeName;

            HumanResourcesReferenceDataRepository referenceDataRepo;

            [TestInitialize]
            public void Initialize()
            {
                loggerMock = new Mock<ILogger>();

                // Build rehire types responses used for mocking
                allRehireTypes = new TestRehireTypeRepository().GetRehireTypes();
                rehireTypeValcodeResponse = BuildValcodeResponse(allRehireTypes);

                // Build rehire types repository
                referenceDataRepo = BuildValidReferenceDataRepository();
                valcodeName = referenceDataRepo.BuildFullCacheKey("HR_REHIRE_ELIGIBILITY_CODES_GUID");
            }

            [TestCleanup]
            public void Cleanup()
            {
                transFactoryMock = null;
                dataAccessorMock = null;
                cacheProviderMock = null;
                rehireTypeValcodeResponse = null;
                allRehireTypes = null;
                referenceDataRepo = null;
            }

            [TestMethod]
            public async Task GetsRehireTypesCacheAsync()
            {
                var rehireTypes = await referenceDataRepo.GetRehireTypesAsync(false);
                for (int i = 0; i < rehireTypes.Count(); i++)
                {
                    Assert.AreEqual(allRehireTypes.ElementAt(i).Code, rehireTypes.ElementAt(i).Code);
                    Assert.AreEqual(allRehireTypes.ElementAt(i).Description, rehireTypes.ElementAt(i).Description);
                }
            }

            [TestMethod]
            public async Task GetsRehireTypesNonCacheAsync()
            {
                var rehireTypes = await referenceDataRepo.GetRehireTypesAsync(true);
                for (int i = 0; i < rehireTypes.Count(); i++)
                {
                    Assert.AreEqual(allRehireTypes.ElementAt(i).Code, rehireTypes.ElementAt(i).Code);
                    Assert.AreEqual(allRehireTypes.ElementAt(i).Description, rehireTypes.ElementAt(i).Description);
                }
            }

            [TestMethod]
            public async Task GetRehireTypes_WritesToCacheAsync()
            {

                // Set up local cache mock to respond to cache request:
                //  -to "Contains" request, return "false" to indicate item is not in cache
                //  -to cache "Get" request, return null so we know it's reading from the "repository"
                cacheProviderMock.Setup(x => x.Contains(valcodeName, null)).Returns(false);
                cacheProviderMock.Setup(x => x.Get(valcodeName, null)).Returns(null);

                // return a valid response to the data accessor request
                dataAccessorMock.Setup(acc => acc.ReadRecordAsync<ApplValcodes>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(rehireTypeValcodeResponse);

                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
                 x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                 .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

                // But after data accessor read, set up mocking so we can verify the list of rehire types was written to the cache
                cacheProviderMock.Setup(x => x.Add(It.IsAny<string>(), It.IsAny<Task<IEnumerable<RehireType>>>(), It.IsAny<CacheItemPolicy>(), null)).Verifiable();

                cacheProviderMock.Setup(x => x.Contains(referenceDataRepo.BuildFullCacheKey("HR_REHIRE_ELIGIBILITY_CODES"), null)).Returns(true);
                var rehireTypes = await referenceDataRepo.GetRehireTypesAsync(false);
                cacheProviderMock.Setup(x => x.Get(referenceDataRepo.BuildFullCacheKey("HR_REHIRE_ELIGIBILITY_CODES"), null)).Returns(rehireTypes);
                // Verify that rehire types were returned, which means they came from the "repository".
                Assert.IsTrue(rehireTypes.Count() == 4);

                // Verify that the rehire type item was added to the cache after it was read from the repository
                cacheProviderMock.Verify(m => m.Add(It.IsAny<string>(), It.IsAny<Task<IEnumerable<RehireType>>>(), It.IsAny<CacheItemPolicy>(), null), Times.Never);

            }

            [TestMethod]
            public async Task GetRehireTypes_GetsCachedRehireTypesAsync()
            {
                // Set up local cache mock to respond to cache request:
                //  -to "Contains" request, return "true" to indicate item is in cache
                //  -to "Get" request, return the cache item (in this case the "REHIRE.ELIGIBILITY.CODES" cache item)
                //cacheProviderMock.Setup(x => x.Contains(valcodeName, null)).Returns(true);
                //cacheProviderMock.Setup(x => x.Get(valcodeName, null)).Returns(allRehireTypes).Verifiable();

                // return null for request, so that if we have a result, it wasn't the data accessor that returned it.
                //dataAccessorMock.Setup(acc => acc.ReadRecordAsync<ApplValcodes>("HR.VALCODES", "REHIRE.ELIGIBILITY.CODES", true)).ReturnsAsync(new ApplValcodes());

                // Assert the rehire types are returned
                Assert.IsTrue((await referenceDataRepo.GetRehireTypesAsync(false)).Count() == 4);
                // Verify that the rehire types were retrieved from cache
                //cacheProviderMock.Verify(m => m.Get(valcodeName, null));
            }

            private HumanResourcesReferenceDataRepository BuildValidReferenceDataRepository()
            {
                // transaction factory mock
                transFactoryMock = new Mock<IColleagueTransactionFactory>();
                // Cache Provider Mock
                cacheProviderMock = new Mock<ICacheProvider>();
                // Set up data accessor for mocking 
                dataAccessorMock = new Mock<IColleagueDataReader>();

                // Set up dataAccessorMock as the object for the DataAccessor
                transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(dataAccessorMock.Object);

                // Setup response to rehire type valcode read
                dataAccessorMock.Setup(acc => acc.ReadRecordAsync<ApplValcodes>("HR.VALCODES", "REHIRE.ELIGIBILITY.CODES", It.IsAny<bool>())).ReturnsAsync(rehireTypeValcodeResponse);
                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x => x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

                dataAccessorMock.Setup(acc => acc.SelectAsync(It.IsAny<RecordKeyLookup[]>())).Returns<RecordKeyLookup[]>(recordKeyLookups =>
                {
                    var result = new Dictionary<string, RecordKeyLookupResult>();
                    foreach (var recordKeyLookup in recordKeyLookups)
                    {
                        var rehireType = allRehireTypes.Where(e => e.Code == recordKeyLookup.SecondaryKey).FirstOrDefault();
                        result.Add(string.Join("+", new string[] { "HR.VALCODES", "REHIRE.ELIGIBILITY.CODES", rehireType.Code }),
                            new RecordKeyLookupResult() { Guid = rehireType.Guid });
                    }
                    return Task.FromResult(result);
                });

                // Construct repository
                referenceDataRepo = new HumanResourcesReferenceDataRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);

                return referenceDataRepo;
            }

            private ApplValcodes BuildValcodeResponse(IEnumerable<RehireType> rehireTypes)
            {
                ApplValcodes valcodeResponse = new ApplValcodes();
                valcodeResponse.ValsEntityAssociation = new List<ApplValcodesVals>();
                foreach (var item in rehireTypes)
                {
                    valcodeResponse.ValsEntityAssociation.Add(new ApplValcodesVals("", item.Description, "", item.Code, "", "", ""));
                }
                return valcodeResponse;
            }
        }
    }
}