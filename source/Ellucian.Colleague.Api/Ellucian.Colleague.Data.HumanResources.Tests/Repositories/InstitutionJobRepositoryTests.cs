﻿/* Copyright 2016-2017 Ellucian Company L.P. and its affiliates. */

using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.HumanResources.Repositories;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Colleague.Data.HumanResources.DataContracts;
using Ellucian.Data.Colleague;

namespace Ellucian.Colleague.Data.HumanResources.Tests.Repositories
{
    [TestClass]
    public class InstitutionJobRepositoryTests : BaseRepositorySetup
    {
        private InstitutionJobsRepository repositoryUnderTest;

        //test data objects
        private List<string> institutionJobIdList;
        private List<string> personIdList;
        private List<string> hrperIdList;        
        private Collection<Position> positionsCollDataList;
        private Collection<Perpos> perposCollDataList;
        private Collection<Perposwg> perposwgCollDataList;
        private Collection<Perstat> perstatCollDataList;
        private Collection<Perben> perbenCollDataList;
        private Collection<Hrper> hrperCollDataList;

        string guid = "4f937f08-f6a0-4a1c-8d55-9f2a6dd6be46";
            
        [TestInitialize]
        public void Initialize()
        {
            MockInitialize();
            repositoryUnderTest = new InstitutionJobsRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettings);

            TestDataSetup();

            GuidLookupResult result = new GuidLookupResult() { Entity = "PERPOS", PrimaryKey = "1" };
            Dictionary<string, GuidLookupResult> resultDict = new Dictionary<string, GuidLookupResult>();
            resultDict.Add(guid, result);

            dataReaderMock.Setup(repo => repo.SelectAsync(It.IsAny<GuidLookup[]>())).ReturnsAsync(resultDict);

            dataReaderMock.Setup(repo => repo.SelectAsync("POSITION", It.IsAny<string>(), It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>())).ReturnsAsync(institutionJobIdList.ToArray());

            dataReaderMock.Setup(repo => repo.SelectAsync("POSITION", It.IsAny<string>())).ReturnsAsync(institutionJobIdList.ToArray());

            dataReaderMock.Setup(repo => repo.SelectAsync("HRPER", It.IsAny<string>())).ReturnsAsync(hrperIdList.ToArray());

            dataReaderMock.Setup(repo => repo.BulkReadRecordAsync<Position>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .ReturnsAsync(positionsCollDataList);

            dataReaderMock.Setup(repo => repo.SelectAsync("PERPOS", It.IsAny<string>())).ReturnsAsync(personIdList.ToArray());

            dataReaderMock.Setup(repo => repo.BulkReadRecordAsync<Perpos>("PERPOS", It.IsAny<string[]>(), It.IsAny<bool>()))
                .ReturnsAsync(perposCollDataList);

            dataReaderMock.Setup(repo => repo.ReadRecordAsync<Perpos>("PERPOS", It.IsAny<string>(), It.IsAny<bool>()))
                .ReturnsAsync(perposCollDataList[0]);

            dataReaderMock.Setup(repo => repo.SelectAsync("PERPOSWG", It.IsAny<string>(),
                        It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>())).ReturnsAsync(personIdList.ToArray());

            dataReaderMock.Setup(repo => repo.SelectAsync("PERPOSWG", It.IsAny<string>())).ReturnsAsync(personIdList.ToArray());

            dataReaderMock.Setup(repo => repo.BulkReadRecordAsync<Perposwg>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .ReturnsAsync(perposwgCollDataList);

            dataReaderMock.Setup(repo => repo.SelectAsync("PERSTAT", It.IsAny<string>(),
                        It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>())).ReturnsAsync(personIdList.ToArray());

            dataReaderMock.Setup(repo => repo.SelectAsync("PERSTAT", It.IsAny<string>())).ReturnsAsync(personIdList.ToArray());

            dataReaderMock.Setup(repo => repo.BulkReadRecordAsync<Perstat>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .ReturnsAsync(perstatCollDataList);

            dataReaderMock.Setup(repo => repo.SelectAsync("PERBEN", It.IsAny<string>(),
                        It.IsAny<string[]>(), It.IsAny<string>(), It.IsAny<bool>(), It.IsAny<int>())).ReturnsAsync(personIdList.ToArray());

            dataReaderMock.Setup(repo => repo.SelectAsync("PERBEN", It.IsAny<string>())).ReturnsAsync(personIdList.ToArray());

            dataReaderMock.Setup(repo => repo.BulkReadRecordAsync<Perben>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .ReturnsAsync(perbenCollDataList);

            dataReaderMock.Setup(repo => repo.BulkReadRecordAsync<Hrper>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .ReturnsAsync(hrperCollDataList);

        }

        [TestMethod]
        public async Task GetAllInstitutionJobs_V8()
        {
            var testData = repositoryUnderTest.GetInstitutionJobsAsync(0, 100, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), false).Result;

            Assert.AreEqual(testData.Item2, 40);
        }

        [TestMethod]
        public async Task GetAllInstitutionJobsFilter_V8()
        {
            var testData = repositoryUnderTest.GetInstitutionJobsAsync(0, 100, "e43e7195-6eca-451d-b6c3-1e52fe540083", It.IsAny<string>(), "d5f5eafb-3192-4479-8dca-6fe79bbde6e4", "50aadc94-3b09-4bec-bca6-a9c588ee8c11", It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), false).Result;

            Assert.AreEqual(testData.Item2, 40);
        }

        [TestMethod]
        public async Task GetAllInstitutionJobsFilterAll_V8()
        {
            var testData = repositoryUnderTest.GetInstitutionJobsAsync(0, 100, "e43e7195-6eca-451d-b6c3-1e52fe540083",
                "27e30a4c-1071-48c9-8f7d-3e0209349a4c", "fadbb5f0-e39d-4b1e-82c9-77617ee2164c", "Math", "2000-01-01 00:00:00.000",
                "2020-12-31 00:00:00.000", "active", "4950f23d-4927-49d9-aa42-be4d97b4aed0", "primary", It.IsAny<bool>()).Result;

            Assert.AreEqual(testData.Item2, 0);
        }

        [TestMethod]
        public async Task GetByGuidInstitutionJob_V8()
        {
            var testData = repositoryUnderTest.GetInstitutionJobsByGuidAsync(perposCollDataList[0].RecordGuid).Result;

            Assert.AreEqual(testData.Guid, perposCollDataList[0].RecordGuid);
        }

        public void TestDataSetup()
        {
            institutionJobIdList = new List<string>()
            {
                "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
                "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40",
                "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60",
                "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80"
            };

            hrperIdList = new List<string>()
            {
                "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60",
                "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80",
                "91", "92", "93", "94", "95", "96", "97", "98", "99", "100", "101", "102", "103", "104", "105", "106", "107", "108", "109", "110",
                "111", "112", "113", "114", "115", "116", "117", "118", "119", "120", "121", "122", "123", "124", "125", "126", "127", "128", "129", "130",
                "131", "132", "133", "134", "135", "136", "137", "138", "139", "140", "141", "142", "143", "144", "145", "146", "147", "148", "149", "150"
            };

            personIdList = new List<string>()
            {
                "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59", "60",
                "61", "62", "63", "64", "65", "66", "67", "68", "69", "70", "71", "72", "73", "74", "75", "76", "77", "78", "79", "80"
            };

            positionsCollDataList = new Collection<Position>();

            for (int i = 41; i < 81; i++)
            {
                positionsCollDataList.Add(new Position()
                {
                   RecordGuid = Guid.NewGuid().ToString(),
                   RecordModelName = "positions",
                   PosHrlyOrSlry = "S",
                   Recordkey = i.ToString()
                });
            }

            perposCollDataList = new Collection<Perpos>();

            for (int i = 41; i < 81; i++)
            {
                perposCollDataList.Add(new Perpos()
                {
                    RecordGuid = Guid.NewGuid().ToString(),
                    PerposAltSupervisorId = i.ToString(),
                    PerposHrpId = i.ToString(),
                    PerposSupervisorHrpId = i.ToString(),
                    PerposPositionId = i.ToString(),
                    PerposStartDate = DateTime.Now,
                    PerposEndDate = DateTime.Now.AddYears(1),
                    Recordkey = i.ToString()
                });
            }

            perposwgCollDataList = new Collection<Perposwg>();

            for (int i = 41; i < 81; i++)
            {
                perposwgCollDataList.Add(new Perposwg()
                {
                    PpwgBaseEt = "et",
                    PpwgEndDate = DateTime.Now.AddYears(1),
                    PpwgStartDate = DateTime.Now,
                    PpwgHrpId = i.ToString(),
                    PpwitemsEntityAssociation = new List<PerposwgPpwitems>()
                    {
                        new PerposwgPpwitems( "value0", "value1", "value2", "value3", "value4", new decimal(4.0), new decimal(5.0), new decimal(6.0), new decimal(7.0), new decimal(8.0), "value10", "value11", new decimal(9.0))
                    },
                    Recordkey = i.ToString()
                });
            }

            perstatCollDataList = new Collection<Perstat>();

            for (int i = 41; i < 81; i++)
            {
                perstatCollDataList.Add(new Perstat()
                {
                    Recordkey = i.ToString(),
                    PerstatHrpId = i.ToString(),
                    PerstatEndDate = DateTime.Now.AddYears(1),
                    PerstatEndReason = "End Time",
                    PerstatPrimaryPerposId = i.ToString(),
                    PerstatPrimaryPosId = i.ToString(),
                    PerstatStartDate = DateTime.Now
                });
            }

            perbenCollDataList = new Collection<Perben>();

            for (int i = 41; i < 81; i++)
            {
                perbenCollDataList.Add(new Perben()
                {
                    Recordkey = i.ToString(),
                    PerbenBdId = i.ToString(),
                    PerbenCancelDate = DateTime.Now,
                    PerbenHrpId = i.ToString()
                });
            }

            hrperCollDataList = new Collection<Hrper>();

            for (int i = 41; i < 81; i++)
            {
                hrperCollDataList.Add(new Hrper()
                {
                    Recordkey = i.ToString()
                });
            }
            
        }

    }
}
