//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 1/25/2017 9:39:10 AM by user bsf1
//
//     Type: ENTITY
//     Entity: VOUCHERS
//     Application: CF
//     Environment: dvcoll_wstst01_rt
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;
using Ellucian.Data.Colleague;

namespace Ellucian.Colleague.Data.ColleagueFinance.DataContracts
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract(Name = "Vouchers")]
	[ColleagueDataContract(GeneratedDateTime = "1/25/2017 9:39:10 AM", User = "bsf1")]
	[EntityDataContract(EntityName = "VOUCHERS", EntityType = "PHYS")]
	public class Vouchers : IColleagueGuidEntity
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		/// <summary>
		/// Record Key
		/// </summary>
		[DataMember]
		public string Recordkey { get; set; }
		
		public void setKey(string key)
		{
			Recordkey = key;
		}
	
		/// <summary>
		/// Record GUID
		/// </summary>
		[DataMember(Name = "RecordGuid")]
		public string RecordGuid { get; set; }

		/// <summary>
		/// Record Model Name
		/// </summary>
		[DataMember(Name = "RecordModelName")]
		public string RecordModelName { get; set; }	
		
		/// <summary>
		/// CDD Name: VOU.ITEMS.ID
		/// </summary>
		[DataMember(Order = 0, Name = "VOU.ITEMS.ID")]
		public List<string> VouItemsId { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.AUTHORIZATION.DATES
		/// </summary>
		[DataMember(Order = 5, Name = "VOU.AUTHORIZATION.DATES")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public List<DateTime?> VouAuthorizationDates { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.VENDOR.TERMS
		/// </summary>
		[DataMember(Order = 6, Name = "VOU.VENDOR.TERMS")]
		public string VouVendorTerms { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.DISC.AMT
		/// </summary>
		[DataMember(Order = 7, Name = "VOU.DISC.AMT")]
		[DisplayFormat(DataFormatString = "{0:N2}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public Decimal? VouDiscAmt { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.PO.NO
		/// </summary>
		[DataMember(Order = 9, Name = "VOU.PO.NO")]
		public string VouPoNo { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.VENDOR
		/// </summary>
		[DataMember(Order = 10, Name = "VOU.VENDOR")]
		public string VouVendor { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.DUE.DATE
		/// </summary>
		[DataMember(Order = 11, Name = "VOU.DUE.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public DateTime? VouDueDate { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.NET
		/// </summary>
		[DataMember(Order = 14, Name = "VOU.NET")]
		[DisplayFormat(DataFormatString = "{0:N2}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public Decimal? VouNet { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.DATE
		/// </summary>
		[DataMember(Order = 15, Name = "VOU.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public DateTime? VouDate { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.CHECK.NO
		/// </summary>
		[DataMember(Order = 16, Name = "VOU.CHECK.NO")]
		public string VouCheckNo { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.CHECK.DATE
		/// </summary>
		[DataMember(Order = 17, Name = "VOU.CHECK.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public DateTime? VouCheckDate { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.AP.TYPE
		/// </summary>
		[DataMember(Order = 20, Name = "VOU.AP.TYPE")]
		public string VouApType { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.RCVS.ID
		/// </summary>
		[DataMember(Order = 23, Name = "VOU.RCVS.ID")]
		public string VouRcvsId { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.STATUS
		/// </summary>
		[DataMember(Order = 24, Name = "VOU.STATUS")]
		public List<string> VouStatus { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.STATUS.DATE
		/// </summary>
		[DataMember(Order = 25, Name = "VOU.STATUS.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public List<DateTime?> VouStatusDate { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.MISC.NAME
		/// </summary>
		[DataMember(Order = 26, Name = "VOU.MISC.NAME")]
		public List<string> VouMiscName { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.REFERENCE.NO
		/// </summary>
		[DataMember(Order = 28, Name = "VOU.REFERENCE.NO")]
		public List<string> VouReferenceNo { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.COMMENTS
		/// </summary>
		[DataMember(Order = 34, Name = "VOU.COMMENTS")]
		public string VouComments { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.AUTHORIZATIONS
		/// </summary>
		[DataMember(Order = 38, Name = "VOU.AUTHORIZATIONS")]
		public List<string> VouAuthorizations { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.CURRENCY.CODE
		/// </summary>
		[DataMember(Order = 47, Name = "VOU.CURRENCY.CODE")]
		public string VouCurrencyCode { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.VOID.GL.TRAN.DATE
		/// </summary>
		[DataMember(Order = 48, Name = "VOU.VOID.GL.TRAN.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public DateTime? VouVoidGlTranDate { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.PAY.FLAG
		/// </summary>
		[DataMember(Order = 49, Name = "VOU.PAY.FLAG")]
		public string VouPayFlag { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.MAINT.GL.TRAN.DATE
		/// </summary>
		[DataMember(Order = 50, Name = "VOU.MAINT.GL.TRAN.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public DateTime? VouMaintGlTranDate { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.DEFAULT.INVOICE.NO
		/// </summary>
		[DataMember(Order = 51, Name = "VOU.DEFAULT.INVOICE.NO")]
		public string VouDefaultInvoiceNo { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.DEFAULT.INVOICE.DATE
		/// </summary>
		[DataMember(Order = 52, Name = "VOU.DEFAULT.INVOICE.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public DateTime? VouDefaultInvoiceDate { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.INVOICE.AMT
		/// </summary>
		[DataMember(Order = 53, Name = "VOU.INVOICE.AMT")]
		[DisplayFormat(DataFormatString = "{0:N2}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public Decimal? VouInvoiceAmt { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.NEXT.APPROVAL.IDS
		/// </summary>
		[DataMember(Order = 65, Name = "VOU.NEXT.APPROVAL.IDS")]
		public List<string> VouNextApprovalIds { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.TAX.CODES
		/// </summary>
		[DataMember(Order = 66, Name = "VOU.TAX.CODES")]
		public List<string> VouTaxCodes { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.TAX.AMTS
		/// </summary>
		[DataMember(Order = 67, Name = "VOU.TAX.AMTS")]
		[DisplayFormat(DataFormatString = "{0:N2}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public List<Decimal?> VouTaxAmts { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.BPO.ID
		/// </summary>
		[DataMember(Order = 73, Name = "VOU.BPO.ID")]
		public string VouBpoId { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.APPROVAL.LEVELS
		/// </summary>
		[DataMember(Order = 84, Name = "VOU.APPROVAL.LEVELS")]
		public List<string> VouApprovalLevels { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.AUTHORIZATION.LEVELS
		/// </summary>
		[DataMember(Order = 85, Name = "VOU.AUTHORIZATION.LEVELS")]
		public List<string> VouAuthorizationLevels { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.ADDRESS.ID
		/// </summary>
		[DataMember(Order = 101, Name = "VOU.ADDRESS.ID")]
		public string VouAddressId { get; set; }
		
		/// <summary>
		/// CDD Name: VOU.REQUESTOR
		/// </summary>
		[DataMember(Order = 103, Name = "VOU.REQUESTOR")]
		public string VouRequestor { get; set; }
		
		/// <summary>
		/// Entity assocation member
		/// </summary>
		[DataMember]
		public List<VouchersVouAuth> VouAuthEntityAssociation { get; set; }
		
		/// <summary>
		/// Entity assocation member
		/// </summary>
		[DataMember]
		public List<VouchersVoucherStatus> VoucherStatusEntityAssociation { get; set; }
		
		/// <summary>
		/// Entity assocation member
		/// </summary>
		[DataMember]
		public List<VouchersVouAppr> VouApprEntityAssociation { get; set; }
		
		/// <summary>
		/// Entity assocation member
		/// </summary>
		[DataMember]
		public List<VouchersVouTaxes> VouTaxesEntityAssociation { get; set; }
		
	
		// build up all the Associated objects and add them to the properties
		public void buildAssociations()
		{	
			// EntityAssociation Name: VOU.AUTH
			
			VouAuthEntityAssociation = new List< VouchersVouAuth >();
			if( VouAuthorizations != null)
			{
				int numVouAuth = VouAuthorizations.Count;
				for (int i = 0; i < numVouAuth; i++)
				{
					DateTime? value0 = null;
					if (VouAuthorizationDates != null && i < VouAuthorizationDates.Count)
					{
						value0 = VouAuthorizationDates[i];
					}

					string value1 = "";
					value1 = VouAuthorizations[i];

					string value2 = "";
					if (VouAuthorizationLevels != null && i < VouAuthorizationLevels.Count)
					{
						value2 = VouAuthorizationLevels[i];
					}

					VouAuthEntityAssociation.Add(new VouchersVouAuth( value0, value1, value2));
				}
			}
			// EntityAssociation Name: VOUCHER.STATUS
			
			VoucherStatusEntityAssociation = new List< VouchersVoucherStatus >();
			if( VouStatus != null)
			{
				int numVoucherStatus = VouStatus.Count;
				for (int i = 0; i < numVoucherStatus; i++)
				{
					string value0 = "";
					value0 = VouStatus[i];

					DateTime? value1 = null;
					if (VouStatusDate != null && i < VouStatusDate.Count)
					{
						value1 = VouStatusDate[i];
					}

					VoucherStatusEntityAssociation.Add(new VouchersVoucherStatus( value0, value1));
				}
			}
			// EntityAssociation Name: VOU.APPR
			
			VouApprEntityAssociation = new List< VouchersVouAppr >();
			if( VouNextApprovalIds != null)
			{
				int numVouAppr = VouNextApprovalIds.Count;
				for (int i = 0; i < numVouAppr; i++)
				{
					string value0 = "";
					value0 = VouNextApprovalIds[i];

					string value1 = "";
					if (VouApprovalLevels != null && i < VouApprovalLevels.Count)
					{
						value1 = VouApprovalLevels[i];
					}

					VouApprEntityAssociation.Add(new VouchersVouAppr( value0, value1));
				}
			}
			// EntityAssociation Name: VOU.TAXES
			
			VouTaxesEntityAssociation = new List< VouchersVouTaxes >();
			if( VouTaxCodes != null)
			{
				int numVouTaxes = VouTaxCodes.Count;
				for (int i = 0; i < numVouTaxes; i++)
				{
					string value0 = "";
					value0 = VouTaxCodes[i];

					Decimal? value1 = null;
					if (VouTaxAmts != null && i < VouTaxAmts.Count)
					{
						value1 = VouTaxAmts[i];
					}

					VouTaxesEntityAssociation.Add(new VouchersVouTaxes( value0, value1));
				}
			}
			   
		}
	}
	
	// EntityAssociation classes
	
	[Serializable]
	public class VouchersVouAuth
	{
		public DateTime? VouAuthorizationDatesAssocMember;	
		public string VouAuthorizationsAssocMember;	
		public string VouAuthorizationLevelsAssocMember;	
		public VouchersVouAuth() {}
		public VouchersVouAuth(
			DateTime? inVouAuthorizationDates,
			string inVouAuthorizations,
			string inVouAuthorizationLevels)
		{
			VouAuthorizationDatesAssocMember = inVouAuthorizationDates;
			VouAuthorizationsAssocMember = inVouAuthorizations;
			VouAuthorizationLevelsAssocMember = inVouAuthorizationLevels;
		}
	}
	
	[Serializable]
	public class VouchersVoucherStatus
	{
		public string VouStatusAssocMember;	
		public DateTime? VouStatusDateAssocMember;	
		public VouchersVoucherStatus() {}
		public VouchersVoucherStatus(
			string inVouStatus,
			DateTime? inVouStatusDate)
		{
			VouStatusAssocMember = inVouStatus;
			VouStatusDateAssocMember = inVouStatusDate;
		}
	}
	
	[Serializable]
	public class VouchersVouAppr
	{
		public string VouNextApprovalIdsAssocMember;	
		public string VouApprovalLevelsAssocMember;	
		public VouchersVouAppr() {}
		public VouchersVouAppr(
			string inVouNextApprovalIds,
			string inVouApprovalLevels)
		{
			VouNextApprovalIdsAssocMember = inVouNextApprovalIds;
			VouApprovalLevelsAssocMember = inVouApprovalLevels;
		}
	}
	
	[Serializable]
	public class VouchersVouTaxes
	{
		public string VouTaxCodesAssocMember;	
		public Decimal? VouTaxAmtsAssocMember;	
		public VouchersVouTaxes() {}
		public VouchersVouTaxes(
			string inVouTaxCodes,
			Decimal? inVouTaxAmts)
		{
			VouTaxCodesAssocMember = inVouTaxCodes;
			VouTaxAmtsAssocMember = inVouTaxAmts;
		}
	}
}