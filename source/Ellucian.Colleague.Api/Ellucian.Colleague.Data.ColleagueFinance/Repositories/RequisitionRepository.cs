﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.ColleagueFinance.DataContracts;
using Ellucian.Colleague.Data.ColleagueFinance.Transactions;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;

namespace Ellucian.Colleague.Data.ColleagueFinance.Repositories
{
    /// <summary>
    /// This class implements the IRequisitionRepository interface
    /// </summary>
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class RequisitionRepository : BaseColleagueRepository, IRequisitionRepository
    {
        /// <summary>
        /// The constructor to instantiate a requisition repository object
        /// </summary>
        /// <param name="cacheProvider">Pass in an ICacheProvider object</param>
        /// <param name="transactionFactory">Pass in an IColleagueTransactionFactory object</param>
        /// <param name="logger">Pass in an ILogger object</param>
        public RequisitionRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            
        }

        /// <summary>
        /// Get a single requisition
        /// </summary>
        /// <param name="id">Requisition ID</param>
        /// <param name="personId">The user ID</param>
        /// <param name="glAccessLevel">The user GL account security level</param>
        /// <param name="expenseAccounts">Set of GL Accounts to which the user has access.</param>
        /// <returns>A requisition domain entity</returns>
        /// <exception cref="ArgumentNullException">Thrown if the id argument is null or empty</exception>
        /// <exception cref="KeyNotFoundException">Thrown if no database records exist for the given id argument</exception>
        public async Task<Requisition> GetRequisitionAsync(string id, string personId, GlAccessLevel glAccessLevel, IEnumerable<string> expenseAccounts)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }

            if (expenseAccounts == null)
            {
                expenseAccounts = new List<string>();
            }

            var requisition = await DataReader.ReadRecordAsync<Requisitions>(id);
            if (requisition == null)
            {
                throw new KeyNotFoundException(string.Format("Requisition record {0} does not exist.", id));
            }

            // Translate the status code into a RequisitionStatus enumeration value
            RequisitionStatus requisitionStatus = new RequisitionStatus();

            // Get the first status in the list of requisition statuses and check it has a value
            if (requisition.ReqStatus != null && !string.IsNullOrEmpty(requisition.ReqStatus.FirstOrDefault().ToUpper()))
            {
                switch (requisition.ReqStatus.FirstOrDefault().ToUpper())
                {
                    case "U":
                        requisitionStatus = RequisitionStatus.InProgress;
                        break;
                    case "N":
                        requisitionStatus = RequisitionStatus.NotApproved;
                        break;
                    case "O":
                        requisitionStatus = RequisitionStatus.Outstanding;
                        break;
                    case "P":
                        requisitionStatus = RequisitionStatus.PoCreated;
                        break;
                    default:
                        // if we get here, we have corrupt data.
                        throw new ApplicationException("Invalid requisition status for requisition: " + requisition.Recordkey);
                }
            }
            else
            {
                throw new ApplicationException("Missing status for requisition: " + requisition.Recordkey);
            }

            // Check that the requisition status date has a value
            if (requisition.ReqStatusDate == null || !requisition.ReqStatusDate.First().HasValue)
            {
                throw new ApplicationException("Missing status date for requisition: " + requisition.Recordkey);
            }

            // The requisition status date contains one to many dates
            var requisitionStatusDate = requisition.ReqStatusDate.First().Value;

            if (!requisition.ReqDate.HasValue)
            {
                throw new ApplicationException("Missing date for requisition: " + requisition.Recordkey);
            }

            #region Get Hierarchy Names

            // Determine the vendor name for the requisition. If there is a miscellaneous name, use it.
            // Otherwise, we will get the name for the id further down.
            var requisitionVendorName = requisition.ReqMiscName.FirstOrDefault();

            // Use a colleague transaction to get all names at once. 
            List<string> personIds = new List<string>();
            List<string> hierarchies = new List<string>();
            List<string> personNames = new List<string>();
            string initiatorName = null;
            string requestorName = null;

            // If there is no vendor name and there is a vendor id, use the PO hierarchy to get the vendor name.
            if ((string.IsNullOrEmpty(requisitionVendorName)) && (!string.IsNullOrEmpty(requisition.ReqVendor)))
            {
                personIds.Add(requisition.ReqVendor);
                hierarchies.Add("PO");
            }

            // Use the PREFERRED hierarchy for the initiator and the requestor.
            if (!string.IsNullOrEmpty(requisition.ReqDefaultInitiator))
            {
                personIds.Add(requisition.ReqDefaultInitiator);
                hierarchies.Add("PREFERRED");
            }

            // Sometimes the requestor is the same person as the initiator. f they are the same,
            // there is no need to add it to the list because the hierarchy is the same.
            if ((!string.IsNullOrEmpty(requisition.ReqRequestor)) && (requisition.ReqRequestor != requisition.ReqDefaultInitiator))
            {
                personIds.Add(requisition.ReqRequestor);
                hierarchies.Add("PREFERRED");
            }

            // Call a colleague transaction to get the person names based on their hierarchies, if necessary
            if ((personIds != null) && (personIds.Count > 0))
            {
                GetHierarchyNamesForIdsRequest request = new GetHierarchyNamesForIdsRequest()
                {
                    IoPersonIds = personIds,
                    IoHierarchies = hierarchies
                };
                GetHierarchyNamesForIdsResponse response = transactionInvoker.Execute<GetHierarchyNamesForIdsRequest, GetHierarchyNamesForIdsResponse>(request);

                // The transaction returns the hierarchy names. If the name is multivalued, 
                // the transaction only returns the first value of the name.
                if (response != null)
                {
                    if (!((response.OutPersonNames == null) || (response.OutPersonNames.Count < 1)))
                    {
                        for (int x = 0; x < response.IoPersonIds.Count(); x++)
                        {
                            var ioPersonId = response.IoPersonIds[x];
                            var hierarchy = response.IoHierarchies[x];
                            var name = response.OutPersonNames[x];
                            if (!string.IsNullOrEmpty(name))
                            {
                                if ((ioPersonId == requisition.ReqVendor) && (hierarchy == "PO"))
                                {
                                    requisitionVendorName = name;
                                }
                                if ((ioPersonId == requisition.ReqDefaultInitiator) && (hierarchy == "PREFERRED"))
                                {
                                    initiatorName = name;
                                }
                                if ((ioPersonId == requisition.ReqRequestor) && (hierarchy == "PREFERRED"))
                                {
                                    requestorName = name;
                                }
                            }
                        }
                    }
                }
            }
            #endregion

            var requisitionDomainEntity = new Requisition(requisition.Recordkey, requisition.ReqNo, requisitionVendorName, requisitionStatus, requisitionStatusDate, requisition.ReqDate.Value.Date);

            requisitionDomainEntity.VendorId = requisition.ReqVendor;
            if (!string.IsNullOrEmpty(initiatorName))
            {
                requisitionDomainEntity.InitiatorName = initiatorName;
            }
            if (!string.IsNullOrEmpty(requestorName))
            {
                requisitionDomainEntity.RequestorName = requestorName;
            }

            requisitionDomainEntity.Amount = 0;
            requisitionDomainEntity.CurrencyCode = requisition.ReqCurrencyCode;
            if (requisition.ReqMaintGlTranDate.HasValue)
            {
                requisitionDomainEntity.MaintenanceDate = requisition.ReqMaintGlTranDate.Value.Date;
            }

            if (requisition.ReqDesiredDeliveryDate.HasValue)
            {
                requisitionDomainEntity.DesiredDate = requisition.ReqDesiredDeliveryDate.Value.Date;
            }
            requisitionDomainEntity.ApType = requisition.ReqApType;
            requisitionDomainEntity.Comments = requisition.ReqPrintedComments;
            requisitionDomainEntity.InternalComments = requisition.ReqComments;
            requisitionDomainEntity.ShipToCode = requisition.ReqShipTo;

            // Add any associated purchase orders to the requisition domain entity
            if ((requisition.ReqPoNo != null) && (requisition.ReqPoNo.Count > 0))
            {
                foreach (var purchaseOrderId in requisition.ReqPoNo)
                {
                    if (!string.IsNullOrEmpty(purchaseOrderId))
                    {
                        requisitionDomainEntity.AddPurchaseOrder(purchaseOrderId);
                    }
                }
            }

            // Add any associated blanket purchase orders to the requisition domain entity.
            // Even though ReqBpoNo is a list of string, only one bpo can be associated to a requisition
            if ((requisition.ReqBpoNo != null) && (requisition.ReqBpoNo.Count > 0))
            {
                if (requisition.ReqBpoNo.Count > 1)
                {
                    throw new ApplicationException("Only one blanket purchase order can be associated with the requisition: " + requisition.Recordkey);
                }
                else
                {
                    requisitionDomainEntity.BlanketPurchaseOrder = requisition.ReqBpoNo.FirstOrDefault();
                }
            }

            // Read the OPERS records associated with the approval signatures and 
            // next approvers on the requisiton, and build approver objects.
            var operators = new List<string>();
            if (requisition.ReqAuthorizations != null)
            {
                operators.AddRange(requisition.ReqAuthorizations);
            }
            if (requisition.ReqNextApprovalIds != null)
            {
                operators.AddRange(requisition.ReqNextApprovalIds);
            }
            var uniqueOperators = operators.Distinct().ToList();
            if (uniqueOperators.Count > 0)
            {
                var Approvers = await DataReader.BulkReadRecordAsync<Opers>("UT.OPERS", uniqueOperators.ToArray(), true);
                if ((Approvers != null) && (Approvers.Count > 0))
                {
                    // loop through the opers, create Approver objects, add the name, and if they
                    // are one of the approvers of the requisition, add the approval date.
                    foreach (var appr in Approvers)
                    {
                        Approver approver = new Approver(appr.Recordkey);
                        var approverName = appr.SysUserName;
                        approver.SetApprovalName(approverName);
                        if ((requisition.ReqAuthEntityAssociation != null) && (requisition.ReqAuthEntityAssociation.Count > 0))
                        {
                            foreach (var approval in requisition.ReqAuthEntityAssociation)
                            {
                                if (approval.ReqAuthorizationsAssocMember == appr.Recordkey)
                                {
                                    approver.ApprovalDate = approval.ReqAuthorizationDatesAssocMember.Value;
                                }
                            }
                        }

                        // Add any approvals to the requisition domain entity
                        requisitionDomainEntity.AddApprover(approver);
                    }
                }
            }

            // Populate the line item domain entities and add them to the requisition domain entity
            var lineItemIds = requisition.ReqItemsId;
            if (lineItemIds != null && lineItemIds.Count() > 0)
            {
                // Read the item records for the list of IDs in the requisition record
                var lineItemRecords = await DataReader.BulkReadRecordAsync<Items>(lineItemIds.ToArray());
                if ((lineItemRecords != null) && (lineItemRecords.Count > 0))
                {
                    // If the user has the full access GL role, they have access to all GL accounts.
                    // There is no need to check for GL account access security. If they have partial 
                    // access, we need to call the CTX to check security.

                    bool hasGlAccess = false;
                    List<string> glAccountsAllowed = new List<string>();
                    if (glAccessLevel == GlAccessLevel.Full_Access)
                    {
                        hasGlAccess = true;
                    }
                    else if (glAccessLevel == GlAccessLevel.Possible_Access)
                    {
                        // Put together a list of unique GL accounts for all the items
                        foreach (var lineItem in lineItemRecords)
                        {
                            if ((lineItem.ItemReqEntityAssociation != null) && (lineItem.ItemReqEntityAssociation.Count > 0))
                            {
                                foreach (var glDist in lineItem.ItemReqEntityAssociation)
                                {
                                    if (expenseAccounts.Contains(glDist.ItmReqGlNoAssocMember))
                                    {
                                        hasGlAccess = true;
                                        glAccountsAllowed.Add(glDist.ItmReqGlNoAssocMember);
                                    }
                                }
                            }
                        }
                    }

                    List<string> itemProjectIds = new List<string>();
                    List<string> itemProjectLineIds = new List<string>();

                    // If this requisition has a currency code, the requisition amount has to be in foreign currency.
                    // We can only obtain that amount by adding up the foreign amounts from its items, so we still have to
                    // process the item information, regardless of GL account security.

                    foreach (var lineItem in lineItemRecords)
                    {
                        // The item description is a list of strings                     
                        string itemDescription = string.Empty;
                        foreach (var desc in lineItem.ItmDesc)
                        {
                            if (lineItem.ItmDesc.Count() > 1)
                            {
                                // If it is not a blank line, added it to the string.
                                // We are going to display all description as it if were one paragraph
                                // even if the user entered it in different paragraphs.
                                if (desc.Length > 0)
                                {
                                    itemDescription += desc + ' ';
                                }
                            }
                            else
                            {
                                // If the line item description is just one line, don't add a space at the end of it.
                                itemDescription = desc;
                            }
                        }

                        decimal itemQuantity = lineItem.ItmReqQty.HasValue ? lineItem.ItmReqQty.Value : 0;
                        decimal itemPrice = lineItem.ItmReqPrice.HasValue ? lineItem.ItmReqPrice.Value : 0;
                        decimal extendedPrice = lineItem.ItmReqExtPrice.HasValue ? lineItem.ItmReqExtPrice.Value : 0;

                        LineItem lineItemDomainEntity = new LineItem(lineItem.Recordkey, itemDescription, itemQuantity, itemPrice, extendedPrice);

                        if (lineItem.ItmDesiredDeliveryDate != null)
                        {
                            lineItemDomainEntity.DesiredDate = lineItem.ItmDesiredDeliveryDate.Value.Date;
                        }
                        lineItemDomainEntity.UnitOfIssue = lineItem.ItmReqIssue;
                        lineItemDomainEntity.VendorPart = lineItem.ItmVendorPart;
                        lineItemDomainEntity.TaxForm = lineItem.ItmTaxForm;
                        lineItemDomainEntity.TaxFormCode = lineItem.ItmTaxFormCode;
                        lineItemDomainEntity.TaxFormLocation = lineItem.ItmTaxFormLoc;
                        lineItemDomainEntity.Comments = lineItem.ItmComments;
                        lineItemDomainEntity.VendorPart = lineItem.ItmVendorPart;

                        // Populate the GL distribution domain entities and add them to the line items
                        if ((lineItem.ItemReqEntityAssociation != null) && (lineItem.ItemReqEntityAssociation.Count > 0))
                        {
                            foreach (var glDist in lineItem.ItemReqEntityAssociation)
                            {
                                // The GL Distribution always uses the local currency amount.
                                decimal gldistGlQty = glDist.ItmReqGlQtyAssocMember.HasValue ? glDist.ItmReqGlQtyAssocMember.Value : 0;
                                decimal gldistGlAmount = glDist.ItmReqGlAmtAssocMember.HasValue ? glDist.ItmReqGlAmtAssocMember.Value : 0;
                                LineItemGlDistribution glDistribution = new LineItemGlDistribution(glDist.ItmReqGlNoAssocMember, gldistGlQty, gldistGlAmount);

                                if (!(string.IsNullOrEmpty(glDist.ItmReqProjectCfIdAssocMember)))
                                {
                                    glDistribution.ProjectId = glDist.ItmReqProjectCfIdAssocMember;
                                    if (!itemProjectIds.Contains(glDist.ItmReqProjectCfIdAssocMember))
                                    {
                                        itemProjectIds.Add(glDist.ItmReqProjectCfIdAssocMember);
                                    }
                                }

                                if (!(string.IsNullOrEmpty(glDist.ItmReqPrjItemIdsAssocMember)))
                                {
                                    glDistribution.ProjectLineItemId = glDist.ItmReqPrjItemIdsAssocMember;
                                    if (!itemProjectLineIds.Contains(glDist.ItmReqPrjItemIdsAssocMember))
                                    {
                                        itemProjectLineIds.Add(glDist.ItmReqPrjItemIdsAssocMember);
                                    }
                                }

                                lineItemDomainEntity.AddGlDistribution(glDistribution);

                                // Check the currency code to see if we need the local or foreign amount
                                if (string.IsNullOrEmpty(requisition.ReqCurrencyCode))
                                {
                                    requisitionDomainEntity.Amount += glDist.ItmReqGlAmtAssocMember.HasValue ? glDist.ItmReqGlAmtAssocMember.Value : 0;
                                }
                                else
                                {
                                    requisitionDomainEntity.Amount += glDist.ItmReqGlForeignAmtAssocMember.HasValue ? glDist.ItmReqGlForeignAmtAssocMember.Value : 0;
                                }
                            }
                        }

                        // Add taxes to the line item
                        if ((lineItem.ReqGlTaxesEntityAssociation != null) && (lineItem.ReqGlTaxesEntityAssociation.Count > 0))
                        {
                            foreach (var taxGlDist in lineItem.ReqGlTaxesEntityAssociation)
                            {
                                decimal itemTaxAmount = 0;
                                string lineItemTaxCode = taxGlDist.ItmReqGlTaxCodeAssocMember;

                                if (taxGlDist.ItmReqGlForeignTaxAmtAssocMember.HasValue)
                                {
                                    itemTaxAmount = taxGlDist.ItmReqGlForeignTaxAmtAssocMember.HasValue ? taxGlDist.ItmReqGlForeignTaxAmtAssocMember.Value : 0;
                                }
                                else
                                {
                                    itemTaxAmount = taxGlDist.ItmReqGlTaxAmtAssocMember.HasValue ? taxGlDist.ItmReqGlTaxAmtAssocMember.Value : 0;
                                }

                                LineItemTax itemTax = new LineItemTax(lineItemTaxCode, itemTaxAmount);

                                lineItemDomainEntity.AddTax(itemTax);

                                if (string.IsNullOrEmpty(requisition.ReqCurrencyCode))
                                {
                                    requisitionDomainEntity.Amount += taxGlDist.ItmReqGlTaxAmtAssocMember.HasValue ? taxGlDist.ItmReqGlTaxAmtAssocMember.Value : 0;
                                }
                                else
                                {
                                    requisitionDomainEntity.Amount += taxGlDist.ItmReqGlForeignTaxAmtAssocMember.HasValue ? taxGlDist.ItmReqGlForeignTaxAmtAssocMember.Value : 0;
                                }
                            }
                        }

                        // Now apply GL account security to the line items.
                        // If hasGlAccess is true, it indicates the user has full access or has some
                        // access to the GL accounts in this requisition. If hasGlAccess if false, 
                        // no line items will be added to the requisition domain entity.


                        if (hasGlAccess == true)
                        {
                            // Now apply GL account access security when creating the line items.
                            // Check to see if the user has access to the GL accounts for each line item:
                            // - if they do not have access to any of them, we will not add the line item to the requisition domain entity.
                            // - if the user has access to some of the GL accounts, the ones they do not have access to will be masked.

                            bool addItem = false;
                            if (glAccessLevel == GlAccessLevel.Full_Access)
                            {
                                // The user has full access and there is no need to check further
                                addItem = true;
                            }
                            else
                            {
                                // We have the list of GL accounts the user can access in the argument fron the CTX.
                                // Check if the user has GL access to at least one GL account in the list of GL accounts for this item
                                // If the user has access to at least one GL account in the line item, add it to the domain

                                if ((lineItemDomainEntity.GlDistributions != null) && (lineItemDomainEntity.GlDistributions.Count > 0))
                                {
                                    foreach (var glDististribution in lineItemDomainEntity.GlDistributions)
                                    {
                                        if (glAccountsAllowed.Contains(glDististribution.GlAccountNumber))
                                        {
                                            addItem = true;
                                        }
                                        else
                                        {
                                            glDististribution.Masked = true;
                                        }
                                    }
                                }
                            }
                            if (addItem)
                            {
                                requisitionDomainEntity.AddLineItem(lineItemDomainEntity);
                            }
                        }
                    }

                    // If there are project IDs, we need to get the project number,
                    // and also the project line item code for each project line item ID 
                    if ((itemProjectIds != null) && (itemProjectIds.Count > 0))
                    {
                        // For each project ID, get the project number
                        var projectRecords = await DataReader.BulkReadRecordAsync<Projects>(itemProjectIds.ToArray());

                        // If there are project IDs, there should be project line item IDs
                        if ((itemProjectLineIds != null) && (itemProjectLineIds.Count > 0))
                        {
                            // For each project line item ID, get the project line item code
                            var projectLineItemRecords = await DataReader.BulkReadRecordAsync<ProjectsLineItems>(itemProjectLineIds.ToArray());

                            if ((projectRecords != null) && (projectRecords.Count > 0))
                            {
                                for (int i = 0; i < requisitionDomainEntity.LineItems.Count(); i++)
                                {
                                    foreach (var glDist in requisitionDomainEntity.LineItems[i].GlDistributions)
                                    {
                                        // Only populate project information if the GL account is not masked.

                                        if (glDist.Masked == false)
                                        {
                                            foreach (var project in projectRecords)
                                            {
                                                if (project.Recordkey == glDist.ProjectId)
                                                {
                                                    glDist.ProjectNumber = project.PrjRefNo;
                                                }
                                            }

                                            if ((projectLineItemRecords != null) && (projectLineItemRecords.Count > 0))
                                            {
                                                foreach (var projectItem in projectLineItemRecords)
                                                {
                                                    if (projectItem.Recordkey == glDist.ProjectLineItemId)
                                                    {
                                                        glDist.ProjectLineItemCode = projectItem.PrjlnProjectItemCode;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return requisitionDomainEntity;
        }
    }
}
