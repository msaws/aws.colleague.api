﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.ColleagueFinance.DataContracts;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;
using System.Collections.ObjectModel;
using System.Linq;
using System;

namespace Ellucian.Colleague.Data.ColleagueFinance.Repositories
{
    /// <summary>
    /// Repository for Colleague Finance reference data
    /// </summary>
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class ColleagueFinanceReferenceDataRepository : BaseColleagueRepository, IColleagueFinanceReferenceDataRepository
    {
        public ColleagueFinanceReferenceDataRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            CacheTimeout = Level1CacheTimeoutValue;
        }

        #region Public Methods
        
        /// <summary>
        /// Get a collection of AccountComponents
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of AccountComponents</returns>
        public async Task<IEnumerable<AccountComponents>> GetAccountComponentsAsync(bool ignoreCache)
        {
            return await GetGuidValcodeAsync<AccountComponents>("CF", "ACCOUNT.COMPONENTS",
                (e, g) => new AccountComponents(g, e.ValInternalCodeAssocMember, e.ValExternalRepresentationAssocMember), bypassCache: ignoreCache);
        }

        /// <summary>
        /// Gets a collection of GlSourceCodes
        /// </summary>
        /// <param name="ignoreCache"></param>
        /// <returns></returns>
        public async Task<IEnumerable<GlSourceCodes>> GetGlSourceCodesValcodeAsync(bool ignoreCache)
        {
            return await GetGuidValcodeAsync<GlSourceCodes>("CF", "GL.SOURCE.CODES",
                (e, g) => new GlSourceCodes(g, e.ValInternalCodeAssocMember, e.ValExternalRepresentationAssocMember, e.ValActionCode3AssocMember), bypassCache: ignoreCache);
        }

        public async Task<AccountingStringComponentValues> GetAccountingStringComponentValueByGuid(string Guid)
        {
            var recordInfo = await GetRecordInfoFromGuidAsync(Guid);
            if (recordInfo == null || string.IsNullOrEmpty(recordInfo.PrimaryKey) )
            {
                throw new KeyNotFoundException(string.Format("No accounting string componenent value was found for guid '{0}'. ", Guid));
            }
            AccountingStringComponentValues ASCV = new AccountingStringComponentValues();
            switch (recordInfo.Entity)
            {
                case "PROJECTS":
                    var project = await DataReader.ReadRecordAsync<Projects>(recordInfo.PrimaryKey);
                    if (project == null)
                    {
                        throw new KeyNotFoundException(string.Format("No accounting string componenent value was found for guid '{0}'. ", Guid));
                    }
                    ASCV = convertProjectsToASCV(project);
                    break;
                case "GL.ACCTS":
                    var glAccount = await DataReader.ReadRecordAsync<DataContracts.GlAccts>(recordInfo.PrimaryKey);
                    if (glAccount == null)
                    {
                        throw new KeyNotFoundException(string.Format("No accounting string componenent value was found for guid '{0}'. ", Guid));
                    }
                    var glClassDef = await DataReader.ReadRecordAsync<DataContracts.Glclsdef>("ACCOUNT.PARAMETERS", "GL.CLASS.DEF", true);
                    var glAcctCC = await DataReader.ReadRecordAsync<DataContracts.GlAcctsCc>(recordInfo.PrimaryKey);
                    var fiscalYearDataContract = await DataReader.ReadRecordAsync<Fiscalyr>("ACCOUNT.PARAMETERS", "FISCAL.YEAR", true);
                    ASCV = ConvertGLtoASCV(glAccount, glClassDef, glAcctCC, fiscalYearDataContract);
                    break;
                default:
                    throw new KeyNotFoundException(string.Format("No accounting string componenent value was found for guid '{0}'. ", Guid));
            }

            return ASCV;

        }


        public async Task<Tuple<IEnumerable<AccountingStringComponentValues>, int>> GetAccountingStringComponentValuesAsync(int Offset, int Limit, string component, 
            string transactionStatus, string typeAccount, string typeFund, bool ignoreCache)
        {
            List<AccountingStringComponentValues> glAccounts = new List<AccountingStringComponentValues>();
            List<AccountingStringComponentValues> projects = new List<AccountingStringComponentValues>();
            if (ignoreCache)
            {
                switch (component)
                {
                    case "GL.ACCT":
                        glAccounts = await BuildAllGLAccounts();
                        break;
                    case "PROJECT":
                        projects = await BuildAllProjects();
                        break;
                    default:
                        glAccounts = await BuildAllGLAccounts();
                        projects = await BuildAllProjects();
                        break;
                }
            }
            else
            {
                string GlcacheId = "AllAccountStringCompValuesGLA";
                string prjtCacheId = "AllAccountStringCompValuesPRJTS";
                switch (component)
                {
                    case "GL.ACCT":
                        glAccounts = await GetOrAddToCacheAsync<List<AccountingStringComponentValues>>(GlcacheId, async () => await this.BuildAllGLAccounts(), Level1CacheTimeoutValue);
                        break;
                    case "PROJECT":
                        projects = await GetOrAddToCacheAsync<List<AccountingStringComponentValues>>(prjtCacheId, async () => await this.BuildAllProjects(), Level1CacheTimeoutValue);
                        break;
                    default:
                        glAccounts = await GetOrAddToCacheAsync<List<AccountingStringComponentValues>>(GlcacheId, async () => await this.BuildAllGLAccounts(), Level1CacheTimeoutValue);
                        projects = await GetOrAddToCacheAsync<List<AccountingStringComponentValues>>(prjtCacheId, async () => await this.BuildAllProjects(), Level1CacheTimeoutValue);
                        break;
                }                
            }

            List<AccountingStringComponentValues> allASCV = new List<AccountingStringComponentValues>();
            allASCV.AddRange(glAccounts);
            allASCV.AddRange(projects);
            if (!string.IsNullOrEmpty(transactionStatus) && allASCV.Count > 0)
            {
                var temp = allASCV.Where(x => x.Status == transactionStatus);
                allASCV = new List<AccountingStringComponentValues>();
                allASCV.AddRange(temp);
            }
            if (!string.IsNullOrEmpty(typeAccount) && allASCV.Count > 0)
            {
                var temp = allASCV.Where(x => x.Type == typeAccount);
                allASCV = new List<AccountingStringComponentValues>();
                allASCV.AddRange(temp);
            }

            allASCV.OrderBy(o => o.AccountNumber);
            int totalCount = allASCV.Count();
            var pageList = allASCV.Skip(Offset).Take(Limit).ToArray();

            return new Tuple<IEnumerable<AccountingStringComponentValues>, int>(pageList,totalCount);
        }



       

        /// <summary>
        /// Get a collection of AccountComponents
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of AccountComponents</returns>
        public async Task<IEnumerable<AccountingFormat>> GetAccountFormatsAsync(bool ignoreCache)
        {
            return await GetGuidValcodeAsync<AccountingFormat>("CF", "INTG.ACCOUNTING.STRING.FORMATS",
                (e, g) => new AccountingFormat(g, e.ValInternalCodeAssocMember, e.ValExternalRepresentationAssocMember), bypassCache: ignoreCache);
        }

        /// <summary>
        /// Get a collection of AccountsPayableSources
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of AccountsPayableSources</returns>
        public async Task<IEnumerable<AccountsPayableSources>> GetAccountsPayableSourcesAsync(bool ignoreCache)
        {

            if (ignoreCache)
            {
                return await BuildAllAccountsPayableSources();
            }
            else
            {
                return await GetOrAddToCacheAsync<IEnumerable<AccountsPayableSources>>("AllAccountsPayableSources", async () => await this.BuildAllAccountsPayableSources(), Level1CacheTimeoutValue);
            }
        }

        private async Task<IEnumerable<AccountsPayableSources>> BuildAllAccountsPayableSources()
        {
            var accountsPayableSourcesEntities = new List<AccountsPayableSources>();
            var accountsPayableSourcesRecords = await DataReader.BulkReadRecordAsync<DataContracts.ApTypes>("AP.TYPES", "");
            var bankCodesRecords = (await DataReader.BulkReadRecordAsync<Base.DataContracts.BankCodes>("BANK.CODES", ""));


            foreach (var accountsPayableSourcesRecord in accountsPayableSourcesRecords)
            {

                var accountsPayableSource = new AccountsPayableSources(accountsPayableSourcesRecord.RecordGuid, accountsPayableSourcesRecord.Recordkey, accountsPayableSourcesRecord.ApTypesDesc);
                var bankCode = bankCodesRecords.FirstOrDefault(b => b.Recordkey == accountsPayableSourcesRecord.AptBankCode);
                if (bankCode != null)
                    accountsPayableSource.directDeposit = bankCode.BankEftActiveFlag;
                accountsPayableSourcesEntities.Add(accountsPayableSource);
            }


            return accountsPayableSourcesEntities;
        }
        /// <summary>
        /// Return a list of Accounts Payable Tax codes.
        /// Cache them for the maximum time. It is very stable information.
        /// </summary>
        public async Task<IEnumerable<AccountsPayableTax>> GetAccountsPayableTaxCodesAsync()
        {
            return await GetOrAddToCacheAsync<IEnumerable<AccountsPayableTax>>("AccountsPayableTaxes", async () =>
            {
                return await GetCodeItemAsync<ApTaxes, AccountsPayableTax>("AllApTaxes", "AP.TAXES",
                itemCode => new AccountsPayableTax(itemCode.Recordkey, itemCode.ApTaxDesc));
            });
        }

        /// <summary>
        /// Return a list of AP type codes.
        /// Cache them for the maximum time. It is very stable information.
        /// </summary>
        public async Task<IEnumerable<AccountsPayableType>> GetAccountsPayableTypeCodesAsync()
        {
            return await GetOrAddToCacheAsync<IEnumerable<AccountsPayableType>>("AccountsPayableTypes", async () =>
            {
                return await GetCodeItemAsync<ApTypes, AccountsPayableType>("AllApTypes", "AP.TYPES",
                itemCode => new AccountsPayableType(itemCode.Recordkey, itemCode.ApTypesDesc) {BankCode = itemCode.AptBankCode});
            });
        }

        /// <summary>
        /// Gets CommodityCodes
        /// </summary>
        /// <param name="ignoreCache"></param>
        /// <returns></returns>
        public async Task<IEnumerable<CommodityCode>> GetCommodityCodesAsync(bool ignoreCache)
        {
            return await GetGuidCodeItemAsync<CommodityCodes, CommodityCode>("AllCommodityCodes", "COMMODITY.CODES",
            (cc, g) => new CommodityCode(g, cc.Recordkey, cc.CmdtyDesc), bypassCache: ignoreCache);
        }

        /// <summary>
        /// Gets CommodityUnitTypes
        /// </summary>
        /// <param name="ignoreCache"></param>
        /// <returns></returns>
        public async Task<IEnumerable<CommodityUnitType>> GetCommodityUnitTypesAsync(bool ignoreCache)
        {
            return await GetGuidCodeItemAsync<UnitIssues, CommodityUnitType>("AllCommodityUnitTypes", "UNIT.ISSUES",
            (cu, g) => new CommodityUnitType(g, cu.Recordkey, cu.UiDesc), bypassCache: ignoreCache);
        }

        /// <summary>
        /// Return a list of CurrencyCodes.
        /// Cache them for the maximum time. It is very stable information.
        /// </summary>
        public async Task<IEnumerable<CurrencyConversion>> GetCurrencyConversionAsync()
        {
            return await GetOrAddToCacheAsync<IEnumerable<CurrencyConversion>>("CurrencyCodes", async () =>
            {
                return await GetCodeItemAsync<CurrencyConv, CurrencyConversion>("AllCurrencyCodes", "CURRENCY.CONV",
                    itemCode => new CurrencyConversion(itemCode.Recordkey, itemCode.CurrencyConvDesc) { CurrencyCode = ConvertCurrencyConvIsoCodeToCurrencyCode(itemCode.CurrencyConvIsoCode) });
            });
        }

        /// <summary>
        /// Gets FreeOnBoardType
        /// </summary>
        /// <param name="ignoreCache"></param>
        /// <returns></returns>
        public async Task<IEnumerable<FreeOnBoardType>> GetFreeOnBoardTypesAsync(bool ignoreCache)
        {
            return await GetGuidCodeItemAsync<Fobs, FreeOnBoardType>("AllFreeOnBoardTypes", "FOBS",
            (fob, g) => new FreeOnBoardType(g, fob.Recordkey, fob.FobsDesc), bypassCache: ignoreCache);
        }

        /// <summary>
        /// Gets ShippingMethod
        /// </summary>
        /// <param name="ignoreCache"></param>
        /// <returns></returns>
        public async Task<IEnumerable<ShippingMethod>> GetShippingMethodsAsync(bool ignoreCache)
        {
            return await GetGuidCodeItemAsync<ShipVias, ShippingMethod>("AllShippingMethods", "SHIP.VIAS",
            (sm, g) => new ShippingMethod(g, sm.Recordkey, sm.ShipViasDesc), bypassCache: ignoreCache);
        }

        /// <summary>
        /// Get a collection of ShipToDestinations
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of ShipToDestinations</returns>
        public async Task<IEnumerable<ShipToDestination>> GetShipToDestinationsAsync(bool ignoreCache)
        {
            return await GetGuidCodeItemAsync<ColleagueFinance.DataContracts.ShipToCodes, ShipToDestination>("AllShipToDestinations", "SHIP.TO.CODES",
            (stc, g) => new ShipToDestination(g, stc.Recordkey, stc.ShptName) { 
                addressLines = stc.ShptAddress,
                placeCountryRegionCode = stc.ShptState,
                placeCountryLocality = stc.ShptCity,
                placeCountryPostalCode = stc.ShptZip,
                contactName = stc.ShptName,
                phoneNumber = stc.ShptPhone,
                phoneExtension = stc.ShptExt
            }, bypassCache: ignoreCache);
        }

        /// <summary>
        /// Get a collection of Collection of VendorHoldReasons domain objects
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of VendorHoldReasons domain objects></returns>
        public async Task<IEnumerable<VendorHoldReasons>> GetVendorHoldReasonsAsync(bool ignoreCache)
        {
            return await GetGuidValcodeAsync<VendorHoldReasons>("CF", "INTG.VENDOR.HOLD.REASONS",
                (cl, g) => new VendorHoldReasons(g, cl.ValInternalCodeAssocMember, cl.ValExternalRepresentationAssocMember), bypassCache: ignoreCache);

        }

        /// <summary>
        /// Get a collection of Collection of VendorTerm domain objects
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of VendorTerm domain objects</returns>
        public async Task<IEnumerable<VendorTerm>> GetVendorTermsAsync(bool ignoreCache)
        {
            return await GetGuidCodeItemAsync<VendorTerms, VendorTerm>("AllVendorTerms", "VENDOR.TERMS",
                (e, g) => new VendorTerm(g, e.Recordkey, e.VendorTermsDesc), bypassCache: ignoreCache);
        }

      
        /// <summary>
        /// Gets Vendor types
        /// </summary>
        /// <param name="ignoreCache"></param>
        /// <returns></returns>
        public async Task<IEnumerable<VendorType>> GetVendorTypesAsync(bool ignoreCache)
        {
            return await GetGuidCodeItemAsync<VendorTypes, VendorType>("AllVendorTypes", "VENDOR.TYPES",
            (cu, g) => new VendorType(g, cu.Recordkey, cu.VendorTypesDesc), bypassCache: ignoreCache);
        }

        
        #endregion

        #region Private methods

        private CurrencyCodes? ConvertCurrencyConvIsoCodeToCurrencyCode(string code)
        {
            if (string.IsNullOrEmpty(code))
                return null;

            switch (code)
            {
                case "CAD":
                    return CurrencyCodes.CAD;
                case "EUR":
                    return CurrencyCodes.EUR;
                case "USD":
                    return CurrencyCodes.USD;
                case "AED":
                    return CurrencyCodes.AED;
                case "AFN":
                    return CurrencyCodes.AFN;
                case "ALL":
                    return CurrencyCodes.ALL;
                case "AMD":
                    return CurrencyCodes.AMD;
                case "ANG":
                    return CurrencyCodes.ANG;
                case "AOA":
                    return CurrencyCodes.AOA;
                case "ARS":
                    return CurrencyCodes.ARS;
                case "AUD":
                    return CurrencyCodes.AUD;
                case "AWG":
                    return CurrencyCodes.AWG;
                case "AZN":
                    return CurrencyCodes.AZN;
                case "BAM":
                    return CurrencyCodes.BAM;
                case "BBD":
                    return CurrencyCodes.BBD;
                case "BDT":
                    return CurrencyCodes.BDT;
                case "BGN":
                    return CurrencyCodes.BGN;
                case "BHD":
                    return CurrencyCodes.BHD;
                case "BIF":
                    return CurrencyCodes.BIF;
                case "BMD":
                    return CurrencyCodes.BMD;
                case "BND":
                    return CurrencyCodes.BND;
                case "BOB":
                    return CurrencyCodes.BOB;
                case "BRL":
                    return CurrencyCodes.BRL;
                case "BSD":
                    return CurrencyCodes.BSD;
                case "BTN":
                    return CurrencyCodes.BTN;
                case "BWP":
                    return CurrencyCodes.BWP;
                case "BYR":
                    return CurrencyCodes.BYR;
                case "BZD":
                    return CurrencyCodes.BZD;
                case "CDF":
                    return CurrencyCodes.CDF;
                case "CHF":
                    return CurrencyCodes.CHF;
                case "CLP":
                    return CurrencyCodes.CLP;
                case "CNY":
                    return CurrencyCodes.CNY;
                case "COP":
                    return CurrencyCodes.COP;
                case "CRC":
                    return CurrencyCodes.CRC;
                case "CUC":
                    return CurrencyCodes.CUC;
                case "CUP":
                    return CurrencyCodes.CUP;
                case "CVE":
                    return CurrencyCodes.CVE;
                case "CZK":
                    return CurrencyCodes.CZK;
                case "DJF":
                    return CurrencyCodes.DJF;
                case "DKK":
                    return CurrencyCodes.DKK;
                case "DOP":
                    return CurrencyCodes.DOP;
                case "DZD":
                    return CurrencyCodes.DZD;
                case "EGP":
                    return CurrencyCodes.EGP;
                case "ERN":
                    return CurrencyCodes.ERN;
                case "ETB":
                    return CurrencyCodes.ETB;
                case "FJD":
                    return CurrencyCodes.FJD;
                case "FKP":
                    return CurrencyCodes.FKP;
                case "GBP":
                    return CurrencyCodes.GBP;
                case "GEL":
                    return CurrencyCodes.GEL;
                case "GHS":
                    return CurrencyCodes.GHS;
                case "GIP":
                    return CurrencyCodes.GIP;
                case "GMD":
                    return CurrencyCodes.GMD;
                case "GNF":
                    return CurrencyCodes.GNF;
                case "GTQ":
                    return CurrencyCodes.GTQ;
                case "GYD":
                    return CurrencyCodes.GYD;
                case "HKD":
                    return CurrencyCodes.HKD;
                case "HNL":
                    return CurrencyCodes.HNL;
                case "HRK":
                    return CurrencyCodes.HRK;
                case "HTG":
                    return CurrencyCodes.HTG;
                case "HUF":
                    return CurrencyCodes.HUF;
                case "IDR":
                    return CurrencyCodes.IDR;
                case "ILS":
                    return CurrencyCodes.ILS;
                case "INR":
                    return CurrencyCodes.INR;
                case "IQD":
                    return CurrencyCodes.IQD;
                case "IRR":
                    return CurrencyCodes.IRR;
                case "ISK":
                    return CurrencyCodes.ISK;
                case "JMD":
                    return CurrencyCodes.JMD;
                case "JOD":
                    return CurrencyCodes.JOD;
                case "JPY":
                    return CurrencyCodes.JPY;
                case "KES":
                    return CurrencyCodes.KES;
                case "KGS":
                    return CurrencyCodes.KGS;
                case "KHR":
                    return CurrencyCodes.KHR;
                case "KMF":
                    return CurrencyCodes.KMF;
                case "KPW":
                    return CurrencyCodes.KPW;
                case "KRW":
                    return CurrencyCodes.KRW;
                case "KWD":
                    return CurrencyCodes.KWD;
                case "KYD":
                    return CurrencyCodes.KYD;
                case "KZT":
                    return CurrencyCodes.KZT;
                case "LAK":
                    return CurrencyCodes.LAK;
                case "LBP":
                    return CurrencyCodes.LBP;
                case "LKR":
                    return CurrencyCodes.LKR;
                case "LRD":
                    return CurrencyCodes.LRD;
                case "LSL":
                    return CurrencyCodes.LSL;
                case "LYD":
                    return CurrencyCodes.LYD;
                case "MAD":
                    return CurrencyCodes.MAD;
                case "MDL":
                    return CurrencyCodes.MDL;
                case "MGA":
                    return CurrencyCodes.MGA;
                case "MKD":
                    return CurrencyCodes.MKD;
                case "MMK":
                    return CurrencyCodes.MMK;
                case "MNT":
                    return CurrencyCodes.MNT;
                case "MOP":
                    return CurrencyCodes.MOP;
                case "MRO":
                    return CurrencyCodes.MRO;
                case "MUR":
                    return CurrencyCodes.MUR;
                case "MVR":
                    return CurrencyCodes.MVR;
                case "MWK":
                    return CurrencyCodes.MWK;
                case "MXN":
                    return CurrencyCodes.MXN;
                case "MYR":
                    return CurrencyCodes.MYR;
                case "MZN":
                    return CurrencyCodes.MZN;
                case "NAD":
                    return CurrencyCodes.NAD;
                case "NGN":
                    return CurrencyCodes.NGN;
                case "NIO":
                    return CurrencyCodes.NIO;
                case "NOK":
                    return CurrencyCodes.NOK;
                case "NPR":
                    return CurrencyCodes.NPR;
                case "NZD":
                    return CurrencyCodes.NZD;
                case "OMR":
                    return CurrencyCodes.OMR;
                case "PAB":
                    return CurrencyCodes.PAB;
                case "PEN":
                    return CurrencyCodes.PEN;
                case "PGK":
                    return CurrencyCodes.PGK;
                case "PHP":
                    return CurrencyCodes.PHP;
                case "PKR":
                    return CurrencyCodes.PKR;
                case "PLN":
                    return CurrencyCodes.PLN;
                case "PYG":
                    return CurrencyCodes.PYG;
                case "QAR":
                    return CurrencyCodes.QAR;
                case "RON":
                    return CurrencyCodes.RON;
                case "RSD":
                    return CurrencyCodes.RSD;
                case "RUB":
                    return CurrencyCodes.RUB;
                case "RWF":
                    return CurrencyCodes.RWF;
                case "SAR":
                    return CurrencyCodes.SAR;
                case "SBD":
                    return CurrencyCodes.SBD;
                case "SCR":
                    return CurrencyCodes.SCR;
                case "SDG":
                    return CurrencyCodes.SDG;
                case "SEK":
                    return CurrencyCodes.SEK;
                case "SGD":
                    return CurrencyCodes.SGD;
                case "SHP":
                    return CurrencyCodes.SHP;
                case "SLL":
                    return CurrencyCodes.SLL;
                case "SOS":
                    return CurrencyCodes.SOS;
                case "SRD":
                    return CurrencyCodes.SRD;
                case "SSP":
                    return CurrencyCodes.SSP;
                case "STD":
                    return CurrencyCodes.STD;
                case "SVC":
                    return CurrencyCodes.SVC;
                case "SYP":
                    return CurrencyCodes.SYP;
                case "SZL":
                    return CurrencyCodes.SZL;
                case "THB":
                    return CurrencyCodes.THB;
                case "TJS":
                    return CurrencyCodes.TJS;
                case "TMT":
                    return CurrencyCodes.TMT;
                case "TND":
                    return CurrencyCodes.TND;
                case "TOP":
                    return CurrencyCodes.TOP;
                case "TRY":
                    return CurrencyCodes.TRY;
                case "TTD":
                    return CurrencyCodes.TTD;
                case "TWD":
                    return CurrencyCodes.TWD;
                case "TZS":
                    return CurrencyCodes.TZS;
                case "UAH":
                    return CurrencyCodes.UAH;
                case "UGX":
                    return CurrencyCodes.UGX;
                case "UYU":
                    return CurrencyCodes.UYU;
                case "UZS":
                    return CurrencyCodes.UZS;
                case "VEF":
                    return CurrencyCodes.VEF;
                case "VND":
                    return CurrencyCodes.VND;
                case "VUV":
                    return CurrencyCodes.VUV;
                case "WST":
                    return CurrencyCodes.WST;
                case "XAF":
                    return CurrencyCodes.XAF;
                case "XCD":
                    return CurrencyCodes.XCD;
                case "XOF":
                    return CurrencyCodes.XOF;
                case "XPF":
                    return CurrencyCodes.XPF;
                case "YER":
                    return CurrencyCodes.YER;
                case "ZAR":
                    return CurrencyCodes.ZAR;
                case "ZMW":
                    return CurrencyCodes.ZMW;
                case "ZWL":
                    return CurrencyCodes.ZWL;

                default:
                    return null;
            }
        }

        private async Task<List<AccountingStringComponentValues>> BuildAllProjects()
        {
            List<AccountingStringComponentValues> allProjects = new List<AccountingStringComponentValues>();
            var projects = await DataReader.BulkReadRecordAsync<DataContracts.Projects>("PROJECTS", "");

            foreach (var project in projects)
            {
                var newASCV = convertProjectsToASCV(project);
                allProjects.Add(newASCV);
            }

            return allProjects;
        }

        private async Task<List<AccountingStringComponentValues>> BuildAllGLAccounts()
        {
            List<AccountingStringComponentValues> AllGlAccounts = new List<AccountingStringComponentValues>();
            try
            {
                var glAccounts = await DataReader.BulkReadRecordAsync<DataContracts.GlAccts>("GL.ACCTS", "");
                var glClassDefs = await DataReader.ReadRecordAsync<DataContracts.Glclsdef>("ACCOUNT.PARAMETERS", "GL.CLASS.DEF", true);
                var glAcctCCs = await DataReader.BulkReadRecordAsync<DataContracts.GlAcctsCc>("GL.ACCTS.CC", "");
                var fiscalYearDataContract = await DataReader.ReadRecordAsync<Fiscalyr>("ACCOUNT.PARAMETERS", "FISCAL.YEAR", true);

                foreach (var glAccount in glAccounts)
                {
                    var glAcctCc = glAcctCCs.FirstOrDefault(x => x.Recordkey == glAccount.Recordkey);
                    var newASCV = ConvertGLtoASCV(glAccount, glClassDefs, glAcctCc, fiscalYearDataContract);

                    AllGlAccounts.Add(newASCV);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Failed becuase " + e.Message);
            }

            return AllGlAccounts;
        }

        private AccountingStringComponentValues convertProjectsToASCV(Projects project)
        {
            AccountingStringComponentValues newASCV = new AccountingStringComponentValues()
            {
                Guid = project.RecordGuid,
                AccountDef = "Project",
                AccountNumber = project.PrjRefNo,
                Description = project.PrjTitle,
                Type = "expense"
            };

            switch (project.PrjCurrentStatus)
            {
                case "A":
                    newASCV.Status = "available";
                    break;
                case "X":
                    newASCV.Status = "unavailable";
                    break;
                case "I":
                    newASCV.Status = "unavailable";
                    break;
            }
            return newASCV;
        }

        private AccountingStringComponentValues ConvertGLtoASCV(GlAccts glAccount, Glclsdef glClassDef, GlAcctsCc glAcctCC, Fiscalyr fiscalYearDataContract)
        {
            AccountingStringComponentValues newASCV = new AccountingStringComponentValues();
            try
            {
                newASCV = new AccountingStringComponentValues()
                {
                    Guid = glAccount.RecordGuid,
                    AccountDef = "GL",
                    AccountNumber = glAccount.Recordkey
                };
            }
            catch (Exception e)
            {
                throw new Exception("failed to load accountingstring");
            }

            try
            {
                // get the Description for this GL
                if (glAcctCC != null)
                {
                    newASCV.Description = glAcctCC.GlccAcctDesc;
                }
            }
            catch (Exception e)
            {
                throw new Exception("failed to load description");
            }

            try
            {
                //get the Status
                if (fiscalYearDataContract != null && glAccount.MemosEntityAssociation != null && glAccount.MemosEntityAssociation.Count > 0)
                {
                    string fiscalYearStatus = glAccount.MemosEntityAssociation.FirstOrDefault(x => x.AvailFundsControllerAssocMember == fiscalYearDataContract.CfCurrentFiscalYear).GlFreezeFlagsAssocMember;
                    if ((fiscalYearStatus == "O" || fiscalYearStatus == "I") && glAccount.GlInactive == "A")
                    {
                        newASCV.Status = "available";
                    }
                    else if (glAccount.GlInactive == "I" || fiscalYearStatus == "F")
                    {
                        newASCV.Status = "unavailable";
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception(" failed in status retrieval; GL number " + glAccount.Recordkey);
            }


            try
            {
                if (glClassDef.GlClassLocation != null)
                {
                    int startPos = glClassDef.GlClassLocation[0].GetValueOrDefault() - 1;
                    int length = glClassDef.GlClassLocation[1].GetValueOrDefault();

                    string component = glAccount.Recordkey.Substring(startPos, length);

                    var test = glClassDef.GlClassAssetValues.FirstOrDefault(x => x == component);

                    if (test != null)
                    {
                        newASCV.Type = "asset";
                    }
                    else
                    {
                        test = glClassDef.GlClassLiabilityValues.FirstOrDefault(x => x == component);
                        if (test != null)
                        {
                            newASCV.Type = "liability";
                        }
                        else
                        {
                            test = glClassDef.GlClassFundBalValues.FirstOrDefault(x => x == component);
                            if (test != null)
                            {
                                newASCV.Type = "fundBalance";
                            }
                            else
                            {
                                test = glClassDef.GlClassRevenueValues.FirstOrDefault(x => x == component);
                                if (test != null)
                                {
                                    newASCV.Type = "revenue";
                                }
                                else
                                {
                                    test = glClassDef.GlClassExpenseValues.FirstOrDefault(x => x == component);
                                    if (test != null)
                                    {
                                        newASCV.Type = "expense";
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new Exception("Failed to get type");
            }
            //Get the Account type

            return newASCV;
        }



        #endregion

    }
}
