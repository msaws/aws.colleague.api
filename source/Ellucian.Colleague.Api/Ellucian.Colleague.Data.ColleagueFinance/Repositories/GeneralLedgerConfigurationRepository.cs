﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.ColleagueFinance.DataContracts;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;
using Ellucian.Dmi.Runtime;

namespace Ellucian.Colleague.Data.ColleagueFinance.Repositories
{
    [RegisterType]
    public class GeneralLedgerConfigurationRepository : BaseColleagueRepository, IGeneralLedgerConfigurationRepository
    {
        public static char _SM = Convert.ToChar(DynamicArray.SM);
        private List<GeneralLedgerComponentDescription> componentDescriptions;

        public GeneralLedgerConfigurationRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            CacheTimeout = Level1CacheTimeoutValue;
            componentDescriptions = new List<GeneralLedgerComponentDescription>();
        }

        #region Fiscal Year Configuration
        /// <summary>
        /// Returns the GL fiscal year information.
        /// </summary>
        public async Task<GeneralLedgerFiscalYearConfiguration> GetFiscalYearConfigurationAsync()
        {
            var fiscalYearDataContract = await DataReader.ReadRecordAsync<Fiscalyr>("ACCOUNT.PARAMETERS", "FISCAL.YEAR", true);

            if (fiscalYearDataContract == null)
                throw new ConfigurationException("Fiscal year data is not set up.");

            if (!fiscalYearDataContract.FiscalStartMonth.HasValue)
                throw new ConfigurationException("Fiscal year start month must have a value.");

            if (fiscalYearDataContract.FiscalStartMonth < 1 || fiscalYearDataContract.FiscalStartMonth > 12)
                throw new ConfigurationException("Fiscal year start month must be in between 1 and 12.");

            if (string.IsNullOrEmpty(fiscalYearDataContract.CfCurrentFiscalYear))
                throw new ConfigurationException("Current fiscal year must have a value.");

            return new GeneralLedgerFiscalYearConfiguration(fiscalYearDataContract.FiscalStartMonth.Value, fiscalYearDataContract.CfCurrentFiscalYear);
        }
        #endregion

        #region Account Structure
        /// <summary>
        /// Get the General Ledger Account structure for Colleague Financials.
        /// </summary>
        /// <returns>General Ledger account configuration.</returns>
        public async Task<GeneralLedgerAccountStructure> GetAccountStructureAsync()
        {
            var accountStructure = await GetOrAddToCacheAsync<GeneralLedgerAccountStructure>("GeneralLedgerAccountStructure",
                async () => await BuildAccountStructure());

            return accountStructure;
        }

        private async Task<GeneralLedgerAccountStructure> BuildAccountStructure()
        {
            var glAccountStructure = new GeneralLedgerAccountStructure();

            var glStruct = await GetGlstructAsync();

            glAccountStructure.SetMajorComponentStartPositions(glStruct.AcctStart);
            glAccountStructure.FullAccessRole = glStruct.GlFullAccessRole;
            glAccountStructure.CheckAvailableFunds = glStruct.AcctCheckAvailFunds;

            // Get the major components.
            for (int i = 0; i < glStruct.AcctNames.Count; i++)
            {
                var componentName = glStruct.AcctNames[i];
                bool isPartOfDescription = false;

                var majorComponent = BuildGeneralLedgerComponent(componentName, isPartOfDescription, glStruct);

                glAccountStructure.AddMajorComponent(majorComponent);
            }

            glAccountStructure.glDelimiter = glStruct.AcctDlm;

            return glAccountStructure;
        }

        #endregion

        #region Cost Center Structure
        /// <summary>
        /// Get the GL Cost Center configuration for Colleague Financials.
        /// </summary>
        /// <returns>General Ledger configuration</returns>
        public async Task<CostCenterStructure> GetCostCenterStructureAsync()
        {
            var costCenterStructure = await GetOrAddToCacheAsync<CostCenterStructure>("GeneralLedgerCostCenterStructure",
                async () => await BuildCostCenterStructure());

            return costCenterStructure;
        }

        private async Task<CostCenterStructure> BuildCostCenterStructure()
        {
            var costCenterStructure = new CostCenterStructure();

            var glStruct = await GetGlstructAsync();

            // Get the cost center and object components.
            var componentInfoDataContract = await DataReader.ReadRecordAsync<CfwebDefaults>("CF.PARMS", "CFWEB.DEFAULTS");
            if (componentInfoDataContract == null)
                throw new ConfigurationException("GL component information is not defined.");

            // Add the cost center components
            for (int i = 0; i < componentInfoDataContract.CfwebCkrCostCenterComps.Count; i++)
            {
                var componentName = componentInfoDataContract.CfwebCkrCostCenterComps[i];
                bool isPartOfDescription = false;

                // Only look at the index in the the descriptions list if we're still within the bounds of the array.
                if (i < componentInfoDataContract.CfwebCkrCostCenterDescs.Count)
                    isPartOfDescription = componentInfoDataContract.CfwebCkrCostCenterDescs[i].ToUpper() == "Y";

                var costCenterComponent = BuildGeneralLedgerComponent(componentName, isPartOfDescription, glStruct);

                costCenterStructure.AddCostCenterComponent(costCenterComponent);
            }

            // Add the object components
            for (int i = 0; i < componentInfoDataContract.CfwebCkrObjectCodeComps.Count; i++)
            {
                var componentName = componentInfoDataContract.CfwebCkrObjectCodeComps[i];
                bool isPartOfDescription = false;

                // Only look at the index in the the descriptions list if we're still within the bounds of the array.
                if (i < componentInfoDataContract.CfwebCkrObjectCodeDescs.Count)
                    isPartOfDescription = componentInfoDataContract.CfwebCkrObjectCodeDescs[i].ToUpper() == "Y";

                var objectComponent = BuildGeneralLedgerComponent(componentName, isPartOfDescription, glStruct);

                costCenterStructure.AddObjectComponent(objectComponent);
            }

            // Obtain the information for the cost center subtotals. If there is no value 
            // for the subtotal, use the object major component to subtotal the cost center.

            // AcctSubName is a list of strings but each string contains each major component's subcomponents separated by subvalue marks.
            // Example glStruct.AcctSubName[0] contains "FUND.GROUP":@SV:"FUND"
            var subcomponentList = new List<string>();
            if (glStruct.AcctSubName != null && glStruct.AcctSubName.Any())
            {
                foreach (var subName in glStruct.AcctSubName)
                {
                    string[] subvalues = subName.Split(_SM);
                    foreach (var sub in subvalues)
                    {
                        subcomponentList.Add(sub);
                    }
                }
            }

            // Same with the list of subcomponent start positions.
            var subcomponentStartList = new List<string>();
            if (glStruct.AcctSubStart != null && glStruct.AcctSubStart.Any())
            {
                foreach (var subStart in glStruct.AcctSubStart)
                {
                    string[] subvalues = subStart.Split(_SM);
                    foreach (var sub in subvalues)
                    {
                        subcomponentStartList.Add(sub);
                    }
                }
            }

            // Same with the list of subcomponent lengths.
            var subcomponentLengthList = new List<string>();
            if (glStruct.AcctSubLgth != null && glStruct.AcctSubLgth.Any())
            {
                foreach (var subLgth in glStruct.AcctSubLgth)
                {
                    string[] subvalues = subLgth.Split(_SM);
                    foreach (var sub in subvalues)
                    {
                        subcomponentLengthList.Add(sub);
                    }
                }
            }

            // Determine the information for the subtotal component
            bool subtotalDescription = false;
            if (componentInfoDataContract.CfwebCostCenterSubtotals != null && componentInfoDataContract.CfwebCostCenterSubtotals.Any())
            {
                var costCenterSubtotalComponent = componentInfoDataContract.CfwebCostCenterSubtotals.FirstOrDefault();
                var subtotalPosition = subcomponentList.FindIndex(x => x.Equals(costCenterSubtotalComponent));
                var subtotalStartPosition = subcomponentStartList[subtotalPosition];
                var subtotalLength = subcomponentLengthList[subtotalPosition];

                if (string.IsNullOrEmpty(costCenterSubtotalComponent))
                    throw new ArgumentNullException("costCenterSubtotalComponent", "costCenterSubtotalComponent must have a value.");

                if (string.IsNullOrEmpty(subtotalLength))
                    throw new ArgumentNullException("subtotalLength", "subtotalLength must have a value.");

                if (string.IsNullOrEmpty(subtotalStartPosition))
                    throw new ArgumentNullException("subtotalStartPosition", "subtotalStartPosition must have a value.");

                int requestedStartPosition;
                if (Int32.TryParse(subtotalStartPosition, out requestedStartPosition))
                {
                    if ((requestedStartPosition - 1) < 0)
                    {
                        throw new ApplicationException("The component start position cannot be negative.");
                    }
                }
                else
                {
                    throw new ApplicationException("The component start is not an integer.");
                }

                int requestedLength;
                bool result = Int32.TryParse(subtotalLength, out requestedLength);
                if (result)
                {
                    if ((requestedLength - 1) < 0)
                    {
                        throw new ApplicationException("Invalid length specified for GL component.");
                    }
                }
                else
                {
                    throw new ApplicationException("The component length is not an integer.");
                }

                try
                {
                    costCenterStructure.CostCenterSubtotal = new GeneralLedgerComponent(costCenterSubtotalComponent, subtotalDescription, GeneralLedgerComponentType.Object, subtotalStartPosition, subtotalLength);
                }
                catch (ArgumentNullException anex)
                {
                    logger.Info(anex.Message);
                }
                catch (ApplicationException apex)
                {
                    logger.Info(apex.Message);
                }
            }
            else
            {
                // If there is not a value for the cost center subtotal, use the major component for the OB type.
                foreach (var glMajorAssoc in glStruct.GlmajorEntityAssociation)
                {
                    if (glMajorAssoc.AcctComponentTypeAssocMember == "OB")
                    {
                        var costCenterSubtotalComponent = glMajorAssoc.AcctNamesAssocMember;
                        var subtotalStartPosition = glMajorAssoc.AcctStartAssocMember;
                        var subtotalLength = glMajorAssoc.AcctLengthAssocMember.ToString();

                        if (string.IsNullOrEmpty(costCenterSubtotalComponent))
                            throw new ArgumentNullException("costCenterSubtotalComponent", "costCenterSubtotalComponent must have a value.");

                        if (string.IsNullOrEmpty(subtotalLength))
                            throw new ArgumentNullException("subtotalLength", "subtotalLength must have a value.");

                        if (string.IsNullOrEmpty(subtotalStartPosition))
                            throw new ArgumentNullException("subtotalStartPosition", "subtotalStartPosition must have a value.");

                        int requestedStartPosition;
                        if (Int32.TryParse(subtotalStartPosition, out requestedStartPosition))
                        {
                            if ((requestedStartPosition - 1) < 0)
                            {
                                throw new ApplicationException("The component start position cannot be negative.");
                            }
                        }
                        else
                        {
                            throw new ApplicationException("The component start is not an integer.");
                        }

                        int requestedLength;
                        if (Int32.TryParse(subtotalLength, out requestedLength))
                        {
                            if ((requestedLength - 1) < 0)
                            {
                                throw new ApplicationException("Invalid length specified for GL component.");
                            }
                        }
                        else
                        {
                            throw new ApplicationException("The component length is not an integer.");
                        }

                        try
                        {
                            costCenterStructure.CostCenterSubtotal = new GeneralLedgerComponent(costCenterSubtotalComponent, subtotalDescription, GeneralLedgerComponentType.Object, subtotalStartPosition, subtotalLength);
                        }
                        catch (ArgumentNullException anex)
                        {
                            logger.Info(anex.Message);
                        }
                        catch (ApplicationException apex)
                        {
                            logger.Info(apex.Message);
                        }
                    }
                }
            }

            // Determine the information for the UN component
            bool unDescription = false;
            foreach (var glMajorAssoc in glStruct.GlmajorEntityAssociation)
            {
                if (glMajorAssoc.AcctComponentTypeAssocMember == "UN")
                {
                    var unComponent = glMajorAssoc.AcctNamesAssocMember;
                    var unStartPosition = glMajorAssoc.AcctStartAssocMember;
                    var unLength = glMajorAssoc.AcctLengthAssocMember.ToString();

                    if (string.IsNullOrEmpty(unComponent))
                        throw new ArgumentNullException("costCenterSubtotalComponent", "costCenterSubtotalComponent must have a value.");

                    if (string.IsNullOrEmpty(unLength))
                        throw new ArgumentNullException("unLength", "unLength must have a value.");

                    if (string.IsNullOrEmpty(unStartPosition))
                        throw new ArgumentNullException("unStartPosition", "unStartPosition must have a value.");

                    int requestedStartPosition;
                    if (Int32.TryParse(unStartPosition, out requestedStartPosition))
                    {
                        if ((requestedStartPosition - 1) < 0)
                        {
                            throw new ApplicationException("The component start position cannot be negative.");
                        }
                    }
                    else
                    {
                        throw new ApplicationException("The component start is not an integer.");
                    }

                    int requestedLength;
                    if (Int32.TryParse(unLength, out requestedLength))
                    {
                        if ((requestedLength - 1) < 0)
                        {
                            throw new ApplicationException("Invalid length specified for GL component.");
                        }
                    }
                    else
                    {
                        throw new ApplicationException("The component length is not an integer.");
                    }

                    try
                    {
                        costCenterStructure.Unit = new GeneralLedgerComponent(unComponent, unDescription, GeneralLedgerComponentType.Unit, unStartPosition, unLength);
                    }
                    catch (ArgumentNullException anex)
                    {
                        logger.Info(anex.Message);
                    }
                    catch (ApplicationException apex)
                    {
                        logger.Info(apex.Message);
                    }
                }
            }

            return costCenterStructure;
        }
        #endregion

        #region GL Class Configuration
        public async Task<GeneralLedgerClassConfiguration> GetClassConfigurationAsync()
        {
            var dataContract = await DataReader.ReadRecordAsync<Glclsdef>("ACCOUNT.PARAMETERS", "GL.CLASS.DEF", true);
            if (dataContract == null)
            {
                throw new ConfigurationException("GL class definition is not defined.");
            }

            if (string.IsNullOrEmpty(dataContract.GlClassDict))
            {
                throw new ConfigurationException("GL class name is not defined.");
            }

            if (dataContract.GlClassExpenseValues == null)
            {
                throw new ConfigurationException("GL class expense values are not defined.");
            }

            if (dataContract.GlClassRevenueValues == null)
            {
                throw new ConfigurationException("GL class revenue values are not defined.");
            }

            if (dataContract.GlClassAssetValues == null)
            {
                throw new ConfigurationException("GL class asset values are not defined.");
            }

            if (dataContract.GlClassLiabilityValues == null)
            {
                throw new ConfigurationException("GL class liability values are not defined.");
            }

            if (dataContract.GlClassFundBalValues == null)
            {
                throw new ConfigurationException("GL class fund balance values are not defined.");
            }

            var classConfiguration = new GeneralLedgerClassConfiguration(dataContract.GlClassDict,
                dataContract.GlClassExpenseValues,
                dataContract.GlClassRevenueValues,
                dataContract.GlClassAssetValues,
                dataContract.GlClassLiabilityValues,
                dataContract.GlClassFundBalValues);

            var glStruct = await GetGlstructAsync();

            // Locate the GL Class name, dataContract.GlGlClassDict, in the list of subcomponents and get the start position and length.

            var subcomponentList = BuildSubcomponentList(glStruct);
            var subcomponentStartList = BuildSubcomponentStartList(glStruct);
            var subcomponentLengthList = BuildSubcomponentLengthtList(glStruct);

            var glClassPosition = subcomponentList.FindIndex(x => x.Equals(dataContract.GlClassDict));
            var glClassStartPosition = subcomponentStartList[glClassPosition];
            var glClassLength = subcomponentLengthList[glClassPosition];

            if (string.IsNullOrEmpty(glClassStartPosition))
                throw new ArgumentNullException("glClassStartPosition", "glClassStartPosition must have a value.");

            if (string.IsNullOrEmpty(glClassLength))
                throw new ArgumentNullException("glClassLength", "glClassLength must have a value.");

            int requestedStartPosition;
            if (Int32.TryParse(glClassStartPosition, out requestedStartPosition))
            {
                if ((requestedStartPosition - 1) < 0)
                {
                    throw new ApplicationException("The GL class subcomponent start position cannot be negative.");
                }
            }
            else
            {
                throw new ApplicationException("The GL class subcomponent start position is not an integer.");
            }

            int requestedLength;
            bool result = Int32.TryParse(glClassLength, out requestedLength);
            if (result)
            {
                if ((requestedLength - 1) < 0)
                {
                    throw new ApplicationException("The GL class subcomponent has an invalid length specified.");
                }
            }
            else
            {
                throw new ApplicationException("The GL class subcomponent length is not an integer.");
            }

            // Adjust the start position since C# index starts at zero.
            classConfiguration.GlClassStartPosition = requestedStartPosition - 1;
            classConfiguration.GlClassLength = requestedLength;

            return classConfiguration;
        }
        #endregion

        #region Available fiscal years
        /// <summary>
        /// Return a set of fiscal years; the current year, up to five previous years and one future year.
        /// </summary>
        /// <param name="currentFiscalYear"></param>
        /// <returns>Set of fiscal years.</returns>
        public async Task<IEnumerable<string>> GetAllFiscalYearsAsync(int currentFiscalYear)
        {
            // It will return up to 7 fiscal years.
            var fiscalYears = new List<string>();

            // Calculate which one is a future fiscal year.
            int futureYear = currentFiscalYear + 1;
            fiscalYears.Add(futureYear.ToString());

            // Add the current fiscal year and calculate which ones are the previous five fiscal years.
            while (fiscalYears.Count < 7)
            {
                fiscalYears.Add(currentFiscalYear.ToString());
                currentFiscalYear--;
            }

            var genLdgrDataContracts = await DataReader.BulkReadRecordAsync<GenLdgr>(fiscalYears.ToArray());

            if (genLdgrDataContracts == null || genLdgrDataContracts.Count <= 0)
                throw new ConfigurationException("No fiscal years have been set up.");

            // Return those fiscal years for which there is a GEN.LDGR record created.
            return genLdgrDataContracts.Where(x => x != null && fiscalYears.Contains(x.Recordkey))
                .Select(x => x.Recordkey).ToList();
        }
        #endregion

        #region private methods

        private GeneralLedgerComponentType DetermineComponentType(string componentTypeString)
        {
            var componentType = GeneralLedgerComponentType.Function;
            switch (componentTypeString.ToUpper())
            {
                case "FD":
                    componentType = GeneralLedgerComponentType.Fund;
                    break;
                case "FC":
                    componentType = GeneralLedgerComponentType.Function;
                    break;
                case "OB":
                    componentType = GeneralLedgerComponentType.Object;
                    break;
                case "UN":
                    componentType = GeneralLedgerComponentType.Unit;
                    break;
                case "SO":
                    componentType = GeneralLedgerComponentType.Source;
                    break;
                case "LO":
                    componentType = GeneralLedgerComponentType.Location;
                    break;
            }

            return componentType;
        }

        private GeneralLedgerComponent BuildGeneralLedgerComponent(string componentName, bool isPartOfDescription, Glstruct glStruct)
        {
            GeneralLedgerComponent glComponent = null;

            if (string.IsNullOrEmpty(componentName))
                throw new ConfigurationException("Component name for GL structure is not defined.");

            // Locate the GLSTRUCT entry for this component.
            var glStructEntry = glStruct.GlmajorEntityAssociation.Where(x => x.AcctNamesAssocMember == componentName).FirstOrDefault();
            if (glStructEntry == null)
                throw new ConfigurationException("GL structure information is not defined.");

            if (string.IsNullOrEmpty(glStructEntry.AcctStartAssocMember))
                throw new ConfigurationException("Start position for GL component not defined.");

            if (glStructEntry.AcctLengthAssocMember < 1)
                throw new ConfigurationException("Invalid length specified for GL component.");

            if (string.IsNullOrEmpty(glStructEntry.AcctLengthAssocMember.ToString()))
                throw new ConfigurationException("Length for GL component not defined.");

            int requestedStartPosition;
            if (Int32.TryParse(glStructEntry.AcctStartAssocMember, out requestedStartPosition))
            {
                if ((requestedStartPosition - 1) < 0)
                {
                    throw new ApplicationException("The component start position cannot be negative.");
                }
            }
            else
            {
                throw new ApplicationException("The component start is not an integer.");
            }

            var componentType = DetermineComponentType(glStructEntry.AcctComponentTypeAssocMember);

            try
            {
                glComponent = new GeneralLedgerComponent(componentName, isPartOfDescription, componentType, glStructEntry.AcctStartAssocMember, glStructEntry.AcctLengthAssocMember.ToString());
            }
            catch (ArgumentNullException anex)
            {
                logger.Info(anex.Message);
            }
            catch (ApplicationException apex)
            {
                logger.Info(apex.Message);
            }

            return glComponent;
        }

        /// <summary>
        /// Obtain the ACCT.STRUCTURE record from Colleague.
        ///  This record contains General Ledger setup parameters.
        /// </summary>
        /// <returns>The Glstruct data record.</returns>
        private async Task<Glstruct> GetGlstructAsync()
        {
            // Get General Ledger parameters from the ACCT.STRUCTURE record in ACCOUNT.PARAMETERS.
            var glStruct = new Glstruct();

            glStruct = await DataReader.ReadRecordAsync<Glstruct>("ACCOUNT.PARAMETERS", "ACCT.STRUCTURE");
            if (glStruct == null)
                // GLSTRUCT must exist for Colleague Financials to function properly
                throw new ConfigurationException("GL account structure is not defined.");

            return glStruct;
        }

        private List<string> BuildSubcomponentList(Glstruct glStruct)
        {
            // AcctSubName is a list of strings but each string contains each major component's subcomponents separated by subvalue marks.
            // Example glStruct.AcctSubName[0] contains "FUND.GROUP":@SV:"FUND"
            var subcomponentList = new List<string>();
            if (glStruct.AcctSubName != null && glStruct.AcctSubName.Any())
            {
                foreach (var subName in glStruct.AcctSubName)
                {
                    string[] subvalues = subName.Split(_SM);
                    foreach (var sub in subvalues)
                    {
                        subcomponentList.Add(sub);
                    }
                }
            }
            return subcomponentList;
        }

        // AcctSubStart is a list of strings but each string contains each subcomponent's start position separated by subvalue marks.
        // Example glStruct.AcctSubStart[0] contains "1":@SV:"3"
        private List<string> BuildSubcomponentStartList(Glstruct glStruct)
        {
            var subcomponentStartList = new List<string>();
            if (glStruct.AcctSubStart != null && glStruct.AcctSubStart.Any())
            {
                foreach (var subStart in glStruct.AcctSubStart)
                {
                    string[] subvalues = subStart.Split(_SM);
                    foreach (var sub in subvalues)
                    {
                        subcomponentStartList.Add(sub);
                    }
                }
            }
            return subcomponentStartList;
        }

        // AcctSubLgth is a list of strings but each string contains each subcomponent' length separated by subvalue marks.
        // Example glStruct.AcctSubLgth[0] contains "1":@SV:"2"
        private List<string> BuildSubcomponentLengthtList(Glstruct glStruct)
        {
            var subcomponentLengthList = new List<string>();
            if (glStruct.AcctSubLgth != null && glStruct.AcctSubLgth.Any())
            {
                foreach (var subLgth in glStruct.AcctSubLgth)
                {
                    string[] subvalues = subLgth.Split(_SM);
                    foreach (var sub in subvalues)
                    {
                        subcomponentLengthList.Add(sub);
                    }
                }
            }
            return subcomponentLengthList;
        }

        #endregion
    }
}