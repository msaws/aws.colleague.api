﻿// Copyright 2016 Ellucian Company L.P. and its affiliates

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.ColleagueFinance.DataContracts;
using Ellucian.Colleague.Data.ColleagueFinance.Transactions;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;
using System.Text;
using Ellucian.Dmi.Runtime;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.Entities;
using System.Text.RegularExpressions;
using Ellucian.Web.Http.Configuration;

namespace Ellucian.Colleague.Data.ColleagueFinance.Repositories
{
    /// <summary>
    /// Implement the IJournalEntryRepository interface
    /// </summary>
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class GeneralLedgerTransactionRepository : BaseColleagueRepository, IGeneralLedgerTransactionRepository
    {
        public static char _SM = Convert.ToChar(DynamicArray.SM);

        public int GlSecurityTransactionCallCount { get; set; }

        private readonly string _colleagueTimeZone;


        /// <summary>
        /// Constructor to instantiate a general ledger transaction repository object
        /// </summary>
        /// <param name="cacheProvider">Pass in an ICacheProvider object</param>
        /// <param name="transactionFactory">Pass in an IColleagueTransactionFactory object</param>
        /// <param name="logger">Pass in an ILogger object</param>
        public GeneralLedgerTransactionRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings settings)
            : base(cacheProvider, transactionFactory, logger)
        {
            this.GlSecurityTransactionCallCount = 0;
            _colleagueTimeZone = settings.ColleagueTimeZone;
        }

        /// <summary>
        /// Get the general ledger transaction requested
        /// </summary>
        /// <param name="id">general ledger transaction GUID</param>
        /// <param name="personId">The user ID</param>
        /// <param name="glAccessLevel">The user GL account security level</param>
        /// <returns>A general ledger transaction domain entity</returns>
        /// <exception cref="ArgumentNullException">Thrown if the id argument is null or empty</exception>
        /// <exception cref="KeyNotFoundException">Thrown if no database records exist for the given id argument</exception>
        public async Task<GeneralLedgerTransaction> GetByIdAsync(string id, string personId, GlAccessLevel glAccessLevel)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }

            // Read the INTG.GL.POSTINGS record
            var recordInfo = await GetRecordInfoFromGuidAsync(id);
            if (recordInfo == null || string.IsNullOrEmpty(recordInfo.PrimaryKey) || recordInfo.Entity != "INTG.GL.POSTINGS")
            {
                throw new KeyNotFoundException(string.Format("Integration GL Postings record {0} does not exist.", id));
            }
            var intgGlPostings = await DataReader.ReadRecordAsync<IntgGlPostings>(recordInfo.PrimaryKey);
            {
                if (intgGlPostings == null)
                {
                    throw new KeyNotFoundException(string.Format("Integration GL Postings record {0} does not exist.", id));
                }
            }
            // Read the INTG.GL.POSTINGS.DETAIL records
            var detailIds = intgGlPostings.IgpTranDetails.ToArray();
            var intgGlPostingsDetail = await DataReader.BulkReadRecordAsync<IntgGlPostingsDetail>(detailIds);

            return BuildGeneralLedgerTransaction(intgGlPostings, intgGlPostingsDetail);
        }

        /// <summary>
        /// Get the general ledger transaction requested
        /// </summary>
        /// <param name="id">general ledger transaction GUID</param>
        /// <param name="personId">The user ID</param>
        /// <param name="glAccessLevel">The user GL account security level</param>
        /// <returns>A general ledger transaction domain entity</returns>
        /// <exception cref="ArgumentNullException">Thrown if the id argument is null or empty</exception>
        /// <exception cref="KeyNotFoundException">Thrown if no database records exist for the given id argument</exception>
        public async Task<IEnumerable<GeneralLedgerTransaction>> GetAsync(string personId, GlAccessLevel glAccessLevel)
        {
            var intgGlPostingsEntities = new List<GeneralLedgerTransaction>();
            // Read the INTG.GL.POSTINGS record
            var criteria = "WITH IGP.SOURCE NE ''";
            var intgGlPostings = await DataReader.BulkReadRecordAsync<IntgGlPostings>(criteria);
            {
                if (intgGlPostings == null)
                {
                    throw new KeyNotFoundException("No records selected from INTG.GL.POSTINGS in Colleague.");
                }
            }
            // Read the INTG.GL.POSTINGS.DETAIL records
            var detailIds = intgGlPostings.SelectMany(igp => igp.IgpTranDetails).ToArray();
            var intgGlPostingsDetail = await DataReader.BulkReadRecordAsync<IntgGlPostingsDetail>(detailIds);

            foreach (var intgGlPostingEntity in intgGlPostings)
            {
                intgGlPostingsEntities.Add(BuildGeneralLedgerTransaction(intgGlPostingEntity, intgGlPostingsDetail));
            }
            return intgGlPostingsEntities;
        }

        /// <summary>
        /// Update a single general ledger transaction for the data model version 6
        /// </summary>
        /// <param name="generalLedgerTransaction">General Ledger Transaction to update</param>
        /// <param name="id">The general ledger transaction GUID</param>
        /// <param name="personId">The user ID</param>
        /// <param name="glAccessLevel">The user GL account security level</param>
        /// <returns>A single GeneralLedgerTransaction</returns>
        public async Task<GeneralLedgerTransaction> UpdateAsync(string id, GeneralLedgerTransaction generalLedgerTransaction, string personId, GlAccessLevel glAccessLevel, GeneralLedgerAccountStructure GlConfig)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }  
            ////Guid reqdness HEDM-2628, 00000000-0000-0000-0000-000000000000 should not be validated
            if (!generalLedgerTransaction.Id.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                var recordInfo = await GetRecordInfoFromGuidAsync(id);
                if (recordInfo != null)
                {
                    throw new InvalidOperationException(string.Format("Integration GL Postings record {0} already exists.  Cannot update an existing entity in Colleague.", id));
                }
            }
            return await CreateGeneralLedgerTransaction(generalLedgerTransaction, GlConfig);
        }

        /// <summary>
        /// Create a single general ledger transaction for the data model version 6
        /// </summary>
        /// <param name="generalLedgerTransaction">General Ledger Transaction to create</param>
        /// <param name="personId">The user ID</param>
        /// <param name="glAccessLevel">The user GL account security level</param>
        /// <returns>A single GeneralLedgerTransaction</returns>
        public async Task<GeneralLedgerTransaction> CreateAsync(GeneralLedgerTransaction generalLedgerTransaction, string personId, GlAccessLevel glAccessLevel, GeneralLedgerAccountStructure GlConfig)
        {
            if (!string.IsNullOrEmpty(generalLedgerTransaction.Id))
            {
                ////Guid reqdness HEDM-2628, 00000000-0000-0000-0000-000000000000 should not be validated
                if (!generalLedgerTransaction.Id.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    var recordInfo = await GetRecordInfoFromGuidAsync(generalLedgerTransaction.Id);
                    if (recordInfo != null)
                    {
                        throw new InvalidOperationException(string.Format("Integration GL Postings record {0} already exists.", generalLedgerTransaction.Id));
                    }
                }
            }
            return await CreateGeneralLedgerTransaction(generalLedgerTransaction, GlConfig);
        }

        /// <summary>
        /// Delete a single general ledger transaction for the data model version 6
        /// </summary>
        /// <param name="id">The requested general ledger transaction GUID</param>
        /// <returns></returns>
        public async Task<GeneralLedgerTransaction> DeleteAsync(string id)
        {
            var recordInfo = await GetRecordInfoFromGuidAsync(id);
            if (recordInfo == null || string.IsNullOrEmpty(recordInfo.PrimaryKey) || recordInfo.Entity != "INTG.GL.POSTINGS")
            {
                throw new KeyNotFoundException(string.Format("Integration GL Postings record {0} does not exist.", id));
            }
            var request = new DeleteIntgGlPostingRequest()
            {
                IntgGlPostingsId = recordInfo.PrimaryKey,
                Guid = id
            };

            ////Delete
            var response = await transactionInvoker.ExecuteAsync<DeleteIntgGlPostingRequest, DeleteIntgGlPostingResponse>(request);

            ////if there are any errors throw
            if (response.DeleteIntgGlPostingErrors.Any())
            {
                var exception = new RepositoryException("Errors encountered while deleting general-ledger-transactions: " + id);
                response.DeleteIntgGlPostingErrors.ForEach(e => exception.AddError(new RepositoryError(e.ErrorCode, e.ErrorMsg)));
                throw exception;
            }
            return null;
        }

        private GeneralLedgerTransaction BuildGeneralLedgerTransaction(IntgGlPostings integGlPosting, IEnumerable<IntgGlPostingsDetail> inteGlPostingDetails)
        {
            var generalLedgerTransaction = new GeneralLedgerTransaction() { Id = integGlPosting.RecordGuid, ProcessMode = "Update",SubmittedBy = integGlPosting.IgpSubmittedBy };
            generalLedgerTransaction.GeneralLedgerTransactions = new List<GenLedgrTransaction>();

            foreach (var glTrans in integGlPosting.TranDetailEntityAssociation)
            {
                if (!string.IsNullOrEmpty(glTrans.IgpSourceAssocMember) && glTrans.IgpTrDateAssocMember.HasValue && !string.IsNullOrEmpty(glTrans.IgpTranDetailsAssocMember))
                {
                    GenLedgrTransaction transactionItem = new GenLedgrTransaction(glTrans.IgpSourceAssocMember.ToUpper(), glTrans.IgpTrDateAssocMember.Value.AddHours(12))
                        {
                            ReferenceNumber = glTrans.IgpRefNoAssocMember,
                            ReferencePersonId = glTrans.IgpAcctIdAssocMember,
                            TransactionTypeReferenceDate = glTrans.IgpSysDateAssocMember,
                            TransactionDetailLines = new List<GenLedgrTransactionDetail>()
                        };

                    try
                    {
                        var detailItem = inteGlPostingDetails.FirstOrDefault(igp => igp.Recordkey == glTrans.IgpTranDetailsAssocMember);
                        foreach (var transDetail in detailItem.IgpdTranDetailsEntityAssociation)
                        {
                            decimal amount = 0;
                            CreditOrDebit type = CreditOrDebit.Credit;
                            if (transDetail.IgpdCreditAssocMember != null && transDetail.IgpdCreditAssocMember.Value != 0)
                            {
                                amount = transDetail.IgpdCreditAssocMember.Value;
                            }
                            else
                            {
                                if (transDetail.IgpdDebitAssocMember != null && transDetail.IgpdDebitAssocMember.Value != 0)
                                {
                                    amount = transDetail.IgpdDebitAssocMember.Value;
                                    type = CreditOrDebit.Debit;
                                }
                            }
                            var amountAndCurrency = new AmountAndCurrency(amount, CurrencyCodes.USD);

                            var genLdgrTransactionDetail = new GenLedgrTransactionDetail(transDetail.IgpdGlNoAssocMember, transDetail.IgpdProjectIdsAssocMember, transDetail.IgpdDescriptionAssocMember, type, amountAndCurrency);
                            try
                            {
                                genLdgrTransactionDetail.SequenceNumber = (!string.IsNullOrEmpty(transDetail.IgpdTranSeqNoAssocMember)) ? (int?)int.Parse(transDetail.IgpdTranSeqNoAssocMember) : null;
                                genLdgrTransactionDetail.SubmittedBy = transDetail.IgpdSubmittedByAssocMember;
                            }
                            catch
                            {
                                // Leave sequence number off the domain entity
                            }
                            transactionItem.TransactionDetailLines.Add(genLdgrTransactionDetail);
                        }
                        generalLedgerTransaction.GeneralLedgerTransactions.Add(transactionItem);
                    }
                    catch
                    {
                        throw new KeyNotFoundException(string.Format("INTG.GL.POSTINGS.DETAIL key {0} is missing from Colleague.", glTrans.IgpTranDetailsAssocMember));
                    }
                }
            }
            return generalLedgerTransaction;
        }

        private async Task<GeneralLedgerTransaction> CreateGeneralLedgerTransaction(GeneralLedgerTransaction generalLedgerTransaction, GeneralLedgerAccountStructure GlConfig)
        {
            var tranType = new List<string>();
            var tranRefNo = new List<string>();
            var tranNo = new List<string>();
            var tranTrDate = new List<Nullable<DateTime>>();
            var tranRefDate = new List<string>();
            var tranAcctId = new List<string>();
            var tranDetSeqNo = new List<string>();
            var tranDetGl = new List<string>();
            var tranDetProj = new List<string>();
            var tranDetDesc = new List<string>();
            var tranDetAmt = new List<string>();
            var tranDetType = new List<string>();
            var tranDetSubmittedBy = new List<string>();

            foreach (var transaction in generalLedgerTransaction.GeneralLedgerTransactions)
            {
                tranType.Add(transaction.Source);
                tranRefNo.Add(transaction.ReferenceNumber);
                tranNo.Add(transaction.TransactionNumber);
                tranTrDate.Add(transaction.LedgerDate.Date);
                //tranRefDate.Add(transaction.TransactionTypeReferenceDate.Date);
                tranAcctId.Add(transaction.ReferencePersonId);

                if (transaction.TransactionDetailLines.Any())
                {
                    int xIndex = transaction.TransactionDetailLines.Count();
                    string[] detailSeqNo = new string[xIndex];
                    string[] detailGl = new string[xIndex];
                    string[] detailProj = new string[xIndex];
                    string[] detailDesc = new string[xIndex];
                    string[] detailAmt = new string[xIndex];
                    string[] detailType = new string[xIndex];
                    string[] detailSubmittedBy = new string[xIndex];

                    int xCtr = 0;
                    foreach (var detail in transaction.TransactionDetailLines)
                    {
                        detailSeqNo[xCtr] = detail.SequenceNumber.ToString();
                        string formatGl = detail.GlAccount.GlAccountNumber;
                        
                        //strip any delimiter sent in so only numbers show.
                        formatGl = Regex.Replace(formatGl, "[^0-9a-zA-Z]", "");
                        //if A GL number is larger then 15 in length then we need to add the Underscores
                        if (formatGl.Length > 15)
                        {
                            int startLoc = 0;
                            string tempGlNo = string.Empty;
                            int x = 0, glCount = GlConfig.MajorComponents.Count();
                            foreach (var glMajor in GlConfig.MajorComponents)
                            {
                                x++;
                                if (x < glCount) {tempGlNo = tempGlNo + formatGl.Substring(startLoc, glMajor.ComponentLength) + "_"; }
                                else { tempGlNo = tempGlNo + formatGl.Substring(startLoc, glMajor.ComponentLength); }
                                
                                startLoc += glMajor.ComponentLength;
                            }
                            formatGl = tempGlNo;
                        } 

                        detailGl[xCtr] = formatGl;
                        detailProj[xCtr] = detail.ProjectId;
                        detailDesc[xCtr] = detail.GlAccount.GlAccountDescription;
                        detailAmt[xCtr] = detail.Amount.Value.ToString();
                        detailType[xCtr] = detail.Type.ToString();
                        detailSubmittedBy[xCtr]= detail.SubmittedBy;
                        xCtr += 1;
                    }
                    tranDetSeqNo.Add(string.Join(_SM.ToString(), detailSeqNo));
                    tranDetGl.Add(string.Join(_SM.ToString(), detailGl));
                    tranDetProj.Add(string.Join(_SM.ToString(), detailProj));
                    tranDetDesc.Add(string.Join(_SM.ToString(), detailDesc));
                    tranDetAmt.Add(string.Join(_SM.ToString(), detailAmt));
                    tranDetType.Add(string.Join(_SM.ToString(), detailType));
                    tranDetSubmittedBy.Add(string.Join(_SM.ToString(), detailSubmittedBy));
                }
            }
            var request = new CreateGLPostingRequest()
                {
                    PostingGuid = generalLedgerTransaction.Id,
                    Mode = generalLedgerTransaction.ProcessMode,
                    SubmittedBy = generalLedgerTransaction.SubmittedBy,
                    TranType = tranType,
                    TranRefNo = tranRefNo,
                    TranNo = tranNo,
                    TranRefDate = tranRefDate,
                    TranTrDate = tranTrDate,
                    TranAcctId = tranAcctId,
                    TranDetSeqNo = tranDetSeqNo,
                    TranDetGl = tranDetGl,
                    TranDetProj = tranDetProj,
                    TranDetDesc = tranDetDesc,
                    TranDetAmt = tranDetAmt,
                    TranDetType = tranDetType,
                    TranDetSubmittedBy = tranDetSubmittedBy
                };

            ////Guid reqdness HEDM-2628, since transaction doesn't support 00000000-0000-0000-0000-000000000000, we have to assign empty string
            if (request.PostingGuid.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                request.PostingGuid = string.Empty;
            }

            var updateResponse = await transactionInvoker.ExecuteAsync<CreateGLPostingRequest, CreateGLPostingResponse>(request);

            // If there is any error message - throw an exception 
            if (!string.IsNullOrEmpty(updateResponse.Error))
            {
                var errorMessage = string.Format("Error(s) occurred updating general-ledger-transactions for id: '{0}'.", request.PostingGuid);
                var exception = new RepositoryException(errorMessage);
                foreach (var errMsg in updateResponse.CreateGlPostingError)
                {
                    exception.AddError(new RepositoryError(errMsg.ErrorCodes, errMsg.ErrorMessages));
                    errorMessage += string.Join(Environment.NewLine, errMsg.ErrorMessages);
                }
                logger.Error(errorMessage.ToString());
                throw exception;
            }

            // Process the messages returned by colleague
            if (updateResponse.WarningMessages.Any())
            {
                foreach (var message in updateResponse.WarningMessages)
                {
                    logger.Warn(message);
                }
            }

            var generalLedgerTransactionResponse = new GeneralLedgerTransaction()
            {
                Id = updateResponse.PostingGuid,
                ProcessMode = generalLedgerTransaction.ProcessMode,
                SubmittedBy = generalLedgerTransaction.SubmittedBy,
                GeneralLedgerTransactions = generalLedgerTransaction.GeneralLedgerTransactions
            };

            return generalLedgerTransactionResponse;
        }
    }
}
