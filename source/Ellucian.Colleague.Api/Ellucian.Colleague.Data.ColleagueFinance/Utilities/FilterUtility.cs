﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System.Linq;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;

namespace Ellucian.Colleague.Data.ColleagueFinance.Utilities
{
    /// <summary>
    /// Provides various methods to perform operations on filter criteria.
    /// </summary>
    public class FilterUtility
    {
        /// <summary>
        /// Determines if the supplied filter is "wide open" (i.e. has criteria).
        /// </summary>
        /// <param name="criteria">Cost center query criteria</param>
        /// <returns>Boolean indicating whether or not the filter is "wide open".</returns>
        public static bool IsFilterWideOpen(CostCenterQueryCriteria criteria)
        {
            // Are we limiting the results using individual components?
            if (criteria.ComponentCriteria.Where(x => x.IndividualComponentValues != null && x.IndividualComponentValues.Any()).Any())
            {
                return false;
            }

            // Are we limiting the results using range components?
            if (criteria.ComponentCriteria.Where(x => x.RangeComponentValues != null && x.RangeComponentValues.Any()).Any())
            {
                return false;
            }

            return true;
        }
    }
}