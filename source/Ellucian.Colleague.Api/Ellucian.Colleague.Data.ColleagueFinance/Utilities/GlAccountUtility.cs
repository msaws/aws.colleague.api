﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using System;

namespace Ellucian.Colleague.Data.ColleagueFinance.Utilities
{
    public class GlAccountUtility
    {
        /// <summary>
        /// Obtain the GL Class for a GL account.
        /// </summary>
        /// <param name="glAccount">A GL account number.</param>
        /// <param name="glClassConfiguration">General Ledger Class configuration.</param>
        /// <exception cref="ApplicationException">Application exception</exception>
        public static GlClass GetGlAccountGlClass(string glAccount, GeneralLedgerClassConfiguration glClassConfiguration)
        {
            GlClass glClass = new GlClass();
            string glAccountGlClass = glAccount.Substring(glClassConfiguration.GlClassStartPosition, glClassConfiguration.GlClassLength);

            if (string.IsNullOrEmpty(glAccountGlClass))
            {
                throw new ApplicationException("Missing glClass for GL account: " + glAccount);
            }

            if (glClassConfiguration.ExpenseClassValues.Contains(glAccountGlClass))
            {
                glClass = GlClass.Expense;
            }
            else if (glClassConfiguration.RevenueClassValues.Contains(glAccountGlClass))
            {
                glClass = GlClass.Revenue;
            }
            else if (glClassConfiguration.AssetClassValues.Contains(glAccountGlClass))
            {
                glClass = GlClass.Asset;
            }
            else if (glClassConfiguration.LiabilityClassValues.Contains(glAccountGlClass))
            {
                glClass = GlClass.Liability;
            }
            else if (glClassConfiguration.FundBalanceClassValues.Contains(glAccountGlClass))
            {
                glClass = GlClass.FundBalance;
            }
            else
            {
                throw new ApplicationException("Invalid glClass for GL account: " + glAccount);
            }

            return glClass;
        }
    }
}
