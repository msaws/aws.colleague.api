//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 2/14/2017 11:59:04 AM by user bsf1
//
//     Type: CTX
//     Transaction ID: CREATE.UPDATE.VENDOR
//     Application: CF
//     Environment: dvcoll_wstst01_rt
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;

namespace Ellucian.Colleague.Data.ColleagueFinance.Transactions
{
	[DataContract]
	public class Errors
	{
		[DataMember]
		[SctrqDataMember(AppServerName = "ERROR.CODES", OutBoundData = true)]
		public string ErrorCodes { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "ERROR.MESSAGES", OutBoundData = true)]
		public string ErrorMessages { get; set; }
	}

	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract]
	[ColleagueDataContract(ColleagueId = "CREATE.UPDATE.VENDOR", GeneratedDateTime = "2/14/2017 11:59:04 AM", User = "bsf1")]
	[SctrqDataContract(Application = "CF", DataContractVersion = 1)]
	public class CreateUpdateVendorRequest
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "VENDOR.ID", InBoundData = true)]        
		public string VendorId { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "VENDOR.GUID", InBoundData = true)]        
		public string VendorGuid { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "ORG.ID", InBoundData = true)]        
		public string OrgId { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "PERSON.ID", InBoundData = true)]        
		public string PersonId { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "INSTITUTION.ID", InBoundData = true)]        
		public string InstitutionId { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "CLASSIFICATIONS", InBoundData = true)]        
		public List<string> ClassificationsId { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "PAYMENT.TERMS", InBoundData = true)]        
		public List<string> PaymentTermsId { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "PAYMENT.SOURCES", InBoundData = true)]        
		public List<string> PaymentSourcesId { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "STATUSES", InBoundData = true)]        
		public List<string> Statuses { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "VENDOR.HOLD.REASONS", InBoundData = true)]        
		public List<string> VendorHoldReasonsId { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "DEFAULT.CURRENCY", InBoundData = true)]        
		public string DefaultCurrency { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "COMMENTS", InBoundData = true)]        
		public string Comments { get; set; }

		public CreateUpdateVendorRequest()
		{	
			ClassificationsId = new List<string>();
			PaymentTermsId = new List<string>();
			PaymentSourcesId = new List<string>();
			Statuses = new List<string>();
			VendorHoldReasonsId = new List<string>();
		}
	}

	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract]
	[ColleagueDataContract(ColleagueId = "CREATE.UPDATE.VENDOR", GeneratedDateTime = "2/14/2017 11:59:04 AM", User = "bsf1")]
	[SctrqDataContract(Application = "CF", DataContractVersion = 1)]
	public class CreateUpdateVendorResponse
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "VENDOR.ID", OutBoundData = true)]        
		public string VendorId { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "VENDOR.GUID", OutBoundData = true)]        
		public string VendorGuid { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "ERROR", OutBoundData = true)]        
		public string Error { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "Grp:ERROR.CODES", OutBoundData = true)]
		public List<Errors> Errors { get; set; }

		public CreateUpdateVendorResponse()
		{	
			Errors = new List<Errors>();
		}
	}
}
