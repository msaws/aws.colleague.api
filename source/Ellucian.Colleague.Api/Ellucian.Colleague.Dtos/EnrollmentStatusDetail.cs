﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Ellucian.Colleague.Dtos
{
    /// <summary>
    /// Property to contain the enrollment status enum and id
    /// </summary>
    [DataContract]
    public class EnrollmentStatusDetail
    {
        /// <summary>
        /// The enrollment status enumerations
        /// </summary>
        [DataMember(Name = "status")]
        public EnrollmentStatusType? EnrollStatus { get; set; }

        /// <summary>
        /// Id of the Enrollment status
        /// </summary>
        [DataMember(Name="detail")]
        public GuidObject2 Detail { get; set; }
    }
}
