﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace Ellucian.Colleague.Dtos
{
    /// <summary>
    /// Unit of Measure
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum CreditMeasure2
    {
        /// <summary>
        /// Credits
        /// </summary>
        [EnumMember(Value = "credit")]
        Credit,

        /// <summary>
        /// Continuing Education Units
        /// </summary>
        [EnumMember(Value = "ceu")]
        CEU,

        /// <summary>
        /// Hours
        /// </summary>
        [EnumMember(Value = "hours")]
        Hours
    }
}
