﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System.Runtime.Serialization;

namespace Ellucian.Colleague.Dtos
{
    /// <summary>
    /// A list of units of the educational institution (optionally, hierarchical) that own or are responsible for a course
    /// </summary>
    [DataContract]
    public class OwningInstitutionUnit
    {
        /// <summary>
        /// Globally unique identifier for the Institution Unit
        /// </summary>
        [DataMember(Name = "institutionUnit")]
        public GuidObject2 InstitutionUnit { get; set; }

        /// <summary>
        /// Organization's percentage of responsibility
        /// </summary>
        [DataMember(Name = "ownershipPercentage", EmitDefaultValue = false)]
        public decimal OwnershipPercentage { get; set; }

        /// <summary>
        /// constructor for property initialization
        /// </summary>
        public OwningInstitutionUnit()
        {
            InstitutionUnit = new GuidObject2();
        }
    }
}