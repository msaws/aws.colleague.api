﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Ellucian.Colleague.Dtos
{
    /// <summary>
    /// A collection of people organized together into a community or other social, commercial or political structure for the purposes of education.
    /// </summary>
    [DataContract]
    public class Organization : CodeItem
    {
        /// <summary>
        /// The type of organization.
        /// </summary>
        [DataMember(Name = "organizationType")]
        public OrganizationType Type { get; set; }

        /// <summary>
        /// Collection of Site GUIDs.
        /// </summary>
        [DataMember(Name = "sites")]
        public IEnumerable<GuidObject> Sites { get; set; }
    }
}