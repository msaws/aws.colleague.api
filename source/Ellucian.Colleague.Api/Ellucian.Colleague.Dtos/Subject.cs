﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Runtime.Serialization;

namespace Ellucian.Colleague.Dtos
{
    /// <summary>
    /// A branch of knowledge typically used as a course of study.
    /// </summary>
    [DataContract]
    public class Subject : CodeItem
    {
    }
}