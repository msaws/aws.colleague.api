﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Ellucian.Colleague.Dtos.Converters;
using Newtonsoft.Json;

namespace Ellucian.Colleague.Dtos
{
    /// <summary>
    /// An event, possibly recurring, at which instruction occurs
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class InstructionalEvent : BaseModel
    {
        /// <summary>
        /// Title of the instructional event
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }

        /// <summary>
        /// Long description of the event
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// The section for which this event occurs
        /// </summary>
        [JsonProperty("section")]
        public GuidObject Section { get; set; }

        /// <summary>
        /// The type of instruction at this event
        /// </summary>
        [JsonProperty("instructionalMethod")]
        public GuidObject InstructionalMethod { get; set; }

        /// <summary>
        /// The total faculty workload for this event
        /// </summary>
        [JsonProperty("workLoad")]
        public decimal? Workload { get; set; }

        /// <summary>
        /// The day on which this event begins
        /// </summary>
        [JsonConverter(typeof(DateOnlyConverter))]
        [JsonProperty("startDate")]
        public DateTime? StartDate { get; set; }
        
        /// <summary>
        /// The date on which this event completes
        /// </summary>
        [JsonConverter(typeof(DateOnlyConverter))]
        [JsonProperty("endDate")]
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// The time at which this event starts
        /// </summary>
        [JsonConverter(typeof(TimeOnlyConverter))]
        [JsonProperty("startTime")]
        public DateTimeOffset? StartTime { get; set; }

        /// <summary>
        /// The time at which this event ends
        /// </summary>
        [JsonConverter(typeof(TimeOnlyConverter))]
        [JsonProperty("endTime")]
        public DateTimeOffset? EndTime { get; set; }

        /// <summary>
        /// The recurrence information for this instructional event
        /// </summary>
        [JsonProperty("recurrence")]
        public Recurrence Recurrence { get; set; }

        /// <summary>
        /// The location information for this instructional event
        /// </summary>
        [JsonProperty("locations", ItemConverterType = typeof(InstructionalLocationConverter))]
        public List<InstructionalLocation> Locations { get; set; }

        /// <summary>
        /// The instructor information for this instructional event
        /// </summary>
        [JsonProperty("instructorRoster")]
        public List<InstructionalEventInstructor> Instructors { get; set; }

        /// <summary>
        /// Approvals controlling the behavior of related validations
        /// </summary>
        [JsonProperty("approvals")]
        public List<InstructionalEventApproval> Approvals { get; set; }

        /// <summary>
        /// Constructor for an InstructionalEvent
        /// </summary>
        [JsonConstructor]
        public InstructionalEvent() : base()
        {
            Recurrence = new Recurrence();
            Locations = new List<InstructionalLocation>();
            Instructors = new List<InstructionalEventInstructor>();
            Approvals = new List<InstructionalEventApproval>();
        }
    }
}
