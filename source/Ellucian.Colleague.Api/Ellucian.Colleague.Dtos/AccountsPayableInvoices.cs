﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Ellucian.Colleague.Dtos.Converters;
using Ellucian.Colleague.Dtos.DtoProperties;
using Ellucian.Colleague.Dtos.EnumProperties;
using Newtonsoft.Json;

namespace Ellucian.Colleague.Dtos
{
    /// <summary>
    /// A request for payment for goods sold, services provided or expenses incurred, containing individual prices, additional charges, payment terms and account information 
    /// </summary>
    [DataContract]
    public class AccountsPayableInvoices : BaseModel2
    {
      
        /// <summary>
        /// The vendor associated with the invoice.
        /// </summary>
        [DataMember(Name = "vendor")]
        public GuidObject2 Vendor { get; set; }

        /// <summary>
        /// The vendor address to be used for this invoice (defaults to the vendors primary address).
        /// </summary>
        [DataMember(Name = "vendorAddress", EmitDefaultValue = false)]
        public GuidObject2 VendorAddress { get; set; }

        /// <summary>
        /// The purchase order number, encumbrance number, or other reference number associated with the invoice.
        /// </summary>
        [DataMember(Name = "referenceNumber", EmitDefaultValue = false)]
        public string ReferenceNumber { get; set; }

        /// <summary>
        /// The invoice number specified by the vendor.
        /// </summary>
        [DataMember(Name = "vendorInvoiceNumber", EmitDefaultValue = false)]
        public string VendorInvoiceNumber { get; set; }

        /// <summary>
        /// The transaction date for the invoice. This will be the date used when entering the transactions into the general ledger.
        /// </summary>
        [JsonConverter(typeof(DateOnlyConverter))]
        [DataMember(Name = "transactionDate")]
        public DateTime TransactionDate { get; set; }

        /// <summary>
        /// The invoice date as specified by the vendor.
        /// </summary>
        [JsonConverter(typeof(DateOnlyConverter))]
        [DataMember(Name = "vendorInvoiceDate", EmitDefaultValue = false)]
        public DateTime? VendorInvoiceDate { get; set; }

        /// <summary>
        /// The date on which the invoice was voided, if applicable.
        /// </summary>
        [JsonConverter(typeof(DateOnlyConverter))]
        [DataMember(Name = "voidDate", EmitDefaultValue = false)]
        public DateTime? VoidDate { get; set; }
        
        /// <summary>
        /// The current state of the processing of the invoice.
        /// </summary>
        [DataMember(Name = "processState", EmitDefaultValue = false)]
        public AccountsPayableInvoicesProcessState ProcessState { get; set; }
        
        /// <summary>
        /// An indicator specifying if payment for the invoice is on hold.
        /// </summary>
        [DataMember(Name = "paymentStatus", EmitDefaultValue = false)]
        public AccountsPayableInvoicesPaymentStatus PaymentStatus { get; set; }

        /// <summary>
        /// The billed amount as specified by the vendor.
        /// </summary>
        [DataMember(Name = "vendorBilledAmount", EmitDefaultValue = false)]
        public Amount2DtoProperty VendorBilledAmount { get; set; }

        /// <summary>
        /// The discount applied to the overall invoice.
        /// </summary>
        [DataMember(Name = "invoiceDiscountAmount", EmitDefaultValue = false)]
        public Amount2DtoProperty InvoiceDiscountAmount { get; set; }

        /// <summary>
        /// The taxes that are applicable to the line item if different from the invoice.
        /// </summary>
        [DataMember(Name = "taxes", EmitDefaultValue = false)]
        public List<AccountsPayableInvoicesTaxesDtoProperty> Taxes { get; set; }
    
        /// <summary>
        /// An indicator specifying if the invoice is a standard or a credit invoice.
        /// </summary>
        [DataMember(Name = "invoiceType", EmitDefaultValue = false)]
        public AccountsPayableInvoicesInvoiceType InvoiceType { get; set; }

        /// <summary>
        /// The taxes that are applicable to the line item if different from the invoice.
        /// </summary>
        [DataMember(Name = "payment", EmitDefaultValue = false)]
        public AccountsPayableInvoicesPaymentDtoProperty Payment { get; set; }

        /// <summary>
        /// Comments on the invoice as a whole.
        /// </summary>
        [DataMember(Name = "invoiceComment", EmitDefaultValue = false)]
        public string InvoiceComment { get; set; }

        /// <summary>
        /// Information required for government reporting.
        /// </summary>
        [DataMember(Name = "governmentReporting", EmitDefaultValue = false)]
        public List<GovernmentReportingDtoProperty> GovernmentReporting { get; set; }

        /// <summary>
        /// The person who made the submit request for the invoice
        /// </summary>
        [JsonProperty("submittedBy", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public GuidObject2 SubmittedBy { get; set; }

        /// <summary>
        /// The individual line items associated with an invoice.
        /// </summary>
        [DataMember(Name = "lineItems", EmitDefaultValue = false)]
        public List<AccountsPayableInvoicesLineItemDtoProperty> LineItems { get; set; }


    }
}
