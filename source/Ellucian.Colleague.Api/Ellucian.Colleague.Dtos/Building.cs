﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Ellucian.Colleague.Dtos
{
    /// <summary>
    /// A building on a site.
    /// </summary>
    [DataContract]
    public class Building : CodeItem
    {
        /// <summary>
        /// A numbering scheme to distinguish different buildings, floors, and rooms located in the same site.
        /// </summary>
        [DataMember(Name = "number")]
        public string Number { get; set; }

        /// <summary>
        /// A physical location
        /// </summary>
        [DataMember(Name = "site")]
        public GuidObject SiteGuid { get; set; }

        /// <summary>
        /// Collection of parts or divisions of a building enclosed by walls, floor, and ceiling.
        /// </summary>
        [DataMember(Name = "rooms")]
        public IEnumerable<GuidObject> RoomGuids { get; set; }
    }
}