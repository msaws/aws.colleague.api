﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System.Runtime.Serialization;

namespace Ellucian.Colleague.Dtos.DtoProperties
{
    /// <summary>
    /// The vendor assigned to receive payment for this vendor or the parent vendor.
    /// </summary>
    [DataContract]
    public class RelatedVendorDtoProperty
    {
        /// <summary>
        ///  The type of related vendor.
        /// </summary>
        [DataMember(Name = "type")]
        public Ellucian.Colleague.Dtos.EnumProperties.VendorType Type { get; set; }

        /// <summary>
        /// The related vendor
        /// </summary>
        [DataMember(Name = "vendor")]
        public GuidObject2 Vendor { get; set; }
    }
}