﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Ellucian.Colleague.Dtos.DtoProperties
{
    /// <summary>
    /// Academic Levels DTO property
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class OwningOrganizationDtoProperty //: BaseCodeTitleDetailDtoProperty
    {
        /// <summary>
        /// Organization's percentage of responsibility
        /// </summary>
        [JsonProperty("ownershipPercentage")]
        public decimal? OwnershipPercentage { get; set; }

        /// <summary>
        /// Id of the InstructionalPlatform
        /// </summary>
        [JsonProperty("institutionUnit")]
        public GuidObject2 Detail { get; set; }

        /// <summary>
        /// Code of the InstructionalPlatform
        /// </summary>
        [JsonProperty("code")]
        public string Code { get; set; }

        /// <summary>
        /// Title of the InstructionalPlatform
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        [JsonConstructor]
        public OwningOrganizationDtoProperty() : base() { }
    }
}
