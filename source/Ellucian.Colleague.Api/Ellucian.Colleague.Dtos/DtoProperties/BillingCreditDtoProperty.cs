﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Ellucian.Colleague.Dtos.DtoProperties
{
    /// <summary>
    /// Billing credit DTO Property
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class BillingCreditDtoProperty
    { 
        /// <summary>
        /// Billing credit minimum
        /// </summary>
        [JsonProperty("minimum")]
        public decimal Minimum { get; set; }

        /// <summary>
        /// Billing credit maximum
        /// </summary>
        [JsonProperty("maximum")]
        public decimal? Maximum { get; set; }

        /// <summary>
        /// Billing credit increment
        /// </summary>
        [JsonProperty("increment")]
        public decimal? Increment { get; set; }  
    }
}
