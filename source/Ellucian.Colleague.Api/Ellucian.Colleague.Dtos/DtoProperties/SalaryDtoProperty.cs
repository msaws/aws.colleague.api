﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System.Runtime.Serialization;

namespace Ellucian.Colleague.Dtos.DtoProperties
{
    /// <summary>
    /// The details of the salary associated with the job.
    /// </summary>
    [DataContract]
    public class SalaryDtoProperty
    {
        /// <summary>
        ///  The grade of salary for the job.
        /// </summary>
        [DataMember(Name = "grade", EmitDefaultValue = false)]
        public string Grade { get; set; }

        /// <summary>
        /// The step of salary for the job.
        /// </summary>
        [DataMember(Name = "step", EmitDefaultValue = false)]
        public string Step { get; set; }

        /// <summary>
        /// The amount of salary for the job.
        /// </summary>
        [DataMember(Name = "amount")]
        public SalaryAmountDtoProperty SalaryAmount { get; set; }
    }
}