﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace Ellucian.Colleague.Dtos.DtoProperties
{
    /// <summary>
    /// Instrucitonal Event DTO Property
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class InstructionalMethodDtoProperty
    {
        /// <summary>
        /// Id of the InstructionalMethod
        /// </summary>
        [JsonProperty("detail")]
        public GuidObject2 Detail { get; set; }

        /// <summary>
        /// Title of the InstructionalMethod
        /// </summary>
        [JsonProperty("title")]
        public string Title { get; set; }

        /// <summary>
        /// Abbreviation of the InstructionalMethod
        /// </summary>
        [JsonProperty("abbreviation", DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Abbreviation { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        [JsonConstructor]
        public InstructionalMethodDtoProperty()
        {
            Detail = new GuidObject2();
        }
    }
}