﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using Newtonsoft.Json;

namespace Ellucian.Colleague.Dtos.DtoProperties
{
    /// <summary>
    /// Credit DTO Property
    /// </summary>
    [JsonObject(MemberSerialization.OptIn)]
    public class Credit3DtoProperty
    { 
        /// <summary>
        /// Credit Category for credit.
        /// </summary>
        [JsonProperty("creditCategory")]
        public CreditIdAndTypeProperty2 CreditCategory { get; set; }

        /// <summary>
        /// The type of credit earned for the course or section.
        /// </summary>
        [JsonProperty("measure", NullValueHandling = NullValueHandling.Ignore)]
        public CreditMeasure2? Measure { get; set; }

        /// <summary>
        /// The academic credit for the section assessed at the registration.
        /// </summary>
        [JsonProperty("attemptedCredit", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? AttemptedCredit { get; set; }

        /// <summary>
        /// The credit the student actually earned by completing the section.
        /// </summary>
        [JsonProperty("earnedCredit", NullValueHandling = NullValueHandling.Ignore)]
        public decimal? EarnedCredit { get; set; }       
    }
}
