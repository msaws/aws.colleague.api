﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.EnumProperties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Ellucian.Colleague.Dtos.DtoProperties
{
    /// <summary>
    /// Property to contain the credittype enum and id
    /// </summary>
    [DataContract]
    public class CreditIdAndTypeProperty2
    {
        /// <summary>
        /// The higher-level category of academic credits
        /// </summary>
        [DataMember(Name = "creditType")]
        public CreditCategoryType3? CreditType { get; set; }

        /// <summary>
        /// Id of the creditType
        /// </summary>
        [DataMember(Name = "detail")]
        public GuidObject2 Detail { get; set; }

        /// <summary>
        /// constructor to initialize properties
        /// </summary>
        public CreditIdAndTypeProperty2()
        {
            //Detail = new GuidObject2();
        }
    }
}
