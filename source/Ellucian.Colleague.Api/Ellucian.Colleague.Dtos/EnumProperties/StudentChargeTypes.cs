﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace Ellucian.Colleague.Dtos.EnumProperties
{
    /// <summary>
    /// Type of the charge the student incurred. 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum StudentChargeTypes
    {
        /// <summary>
        /// No value set for this enumeration
        /// </summary>
        notset = 0,
        /// <summary>
        /// Tuition
        /// </summary>
        tuition,
        /// <summary>
        /// Fees
        /// </summary>
        fee,
        /// <summary>
        /// Housing
        /// </summary>
        housing,
        /// <summary>
        /// Meal Plans
        /// </summary>
        meal
    }
}