// Copyright 2017 Ellucian Company L.P. and its affiliates.

using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Runtime.Serialization;


namespace Ellucian.Colleague.Dtos.EnumProperties
{   
   /// <summary>
    /// Type of the financial aid fund categories.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum FinancialAidFundAidCategoryType
    {     
        /// <summary>
        /// Used when the value is not set or an invalid enumeration is used
        /// </summary>
        NotSet = 0,
                     
        /// <summary>
        /// Pell Grant
        /// </summary>
        [EnumMember(Value = "pellGrant")]
        pellGrant,

        /// <summary>
        /// Federal Unsubsidized Loan
        /// </summary>
        [EnumMember(Value = "federalUnsubsidizedLoan")]
        federalUnsubsidizedLoan,

        /// <summary>
        /// Federal Subsidized Loan
        /// </summary>
        [EnumMember(Value = "federalSubsidizedLoan")]
        federalSubsidizedLoan,

        /// <summary>
        /// Graduate Teach Grant
        /// </summary>
        [EnumMember(Value = "graduateTeachGrant")]
        graduateTeachGrant,

        /// <summary>
        /// Undergraduate Teach Grant
        /// </summary>
        [EnumMember(Value = "undergraduateTeachGrant")]
        undergraduateTeachGrant,

        /// <summary>
        /// Parent Plus Loan
        /// </summary>
        [EnumMember(Value = "parentPlusLoan")]
        parentPlusLoan,

        /// <summary>
        /// Graduate Plus Loan
        /// </summary>
        [EnumMember(Value = "graduatePlusLoan")]
        graduatePlusLoan,

        /// <summary>
        /// Federal Work Study Program
        /// </summary>
        [EnumMember(Value = "federalWorkStudyProgram")]
        federalWorkStudyProgram,

        /// <summary>
        /// Irag Afghanastan Service Grant
        /// </summary>
        [EnumMember(Value = "iraqAfghanistanServiceGrant")]
        iraqAfghanistanServiceGrant,

        /// <summary>
        /// Academic Competitiveness Grant
        /// </summary>
        [EnumMember(Value = "academicCompetitivenessGrant")]
        academicCompetitivenessGrant,

        /// <summary>
        /// Bureau of Indian Affairs Federal Grant
        /// </summary>
        [EnumMember(Value = "bureauOfIndianAffairsFederalGrant")]
        bureauOfIndianAffairsFederalGrant,

        /// <summary>
        /// Robert C Byrd Scholarship Program
        /// </summary>
        [EnumMember(Value = "robertCByrdScholarshipProgram")]
        robertCByrdScholarshipProgram,

        /// <summary>
        /// Paul Douglas Teacher Scholarship
        /// </summary>
        [EnumMember(Value = "paulDouglasTeacherScholarship")]
        paulDouglasTeacherScholarship,

        /// <summary>
        /// General Title IV Loan
        /// </summary>
        [EnumMember(Value = "generalTitleIVloan")]
        generalTitleIVloan,

        /// <summary>
        /// Health Education Assistance Loan
        /// </summary>
        [EnumMember(Value = "healthEducationAssistanceLoan")]
        healthEducationAssistanceLoan,

        /// <summary>
        /// Health Professional Student Loan
        /// </summary>
        [EnumMember(Value = "healthProfessionalStudentLoan")]
        healthProfessionalStudentLoan,

        /// <summary>
        /// Income Contingent Loan
        /// </summary>
        [EnumMember(Value = "incomeContingentLoan")]
        incomeContingentLoan,

        /// <summary>
        /// Loan for Disadvantages Student
        /// </summary>
        [EnumMember(Value = "loanForDisadvantagesStudent")]
        loanForDisadvantagesStudent,

        /// <summary>
        /// Leveraging Educational Assistance Partnership
        /// </summary>
        [EnumMember(Value = "leveragingEducationalAssistancePartnership")]
        leveragingEducationalAssistancePartnership,

        /// <summary>
        /// National Health Services Corps Scholarship
        /// </summary>
        [EnumMember(Value = "nationalHealthServicesCorpsScholarship")]
        nationalHealthServicesCorpsScholarship,

        /// <summary>
        /// Nursing Student Loan
        /// </summary>
        [EnumMember(Value = "nursingStudentLoan")]
        nursingStudentLoan,

        /// <summary>
        /// Primary Care Loan
        /// </summary>
        [EnumMember(Value = "primaryCareLoan")]
        primaryCareLoan,

        /// <summary>
        /// Federal Perkins Loan
        /// </summary>
        [EnumMember(Value = "federalPerkinsLoan")]
        federalPerkinsLoan,

        /// <summary>
        /// ROTC Scholarship
        /// </summary>
        [EnumMember(Value = "rotcScholarship")]
        rotcScholarship,

        /// <summary>
        /// Federal Supplementary Educational Opportunity Grant
        /// </summary>
        [EnumMember(Value = "federalSupplementaryEducationalOpportunityGrant")]
        federalSupplementaryEducationalOpportunityGrant,

        /// <summary>
        /// Stay in School Program
        /// </summary>
        [EnumMember(Value = "stayInSchoolProgram")]
        stayInSchoolProgram,

        /// <summary>
        /// Federal Supplementary Loan for Parent
        /// </summary>
        [EnumMember(Value = "federalSupplementaryLoanForParent")]
        federalSupplementaryLoanForParent,

        /// <summary>
        /// National Smart Grant
        /// </summary>
        [EnumMember(Value = "nationalSmartGrant")]
        nationalSmartGrant,

        /// <summary>
        /// States Student Incentive Grant
        /// </summary>
        [EnumMember(Value = "stateStudentIncentiveGrant")]
        stateStudentIncentiveGrant,

        /// <summary>
        /// VA Health Professionals Scholarship
        /// </summary>
        [EnumMember(Value = "vaHealthProfessionsScholarship")]
        vaHealthProfessionsScholarship,

        /// <summary>
        /// Non-Governmental
        /// </summary>
        [EnumMember(Value = "nonGovernmental")]
        nonGovernmental
    }
} 


