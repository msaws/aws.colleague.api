﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System.Runtime.Serialization;

namespace Ellucian.Colleague.Dtos.EnumProperties
{
    /// <summary>
    /// Type of the charge the student incurred. 
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum StudentPaymentTypes
    {
        /// <summary>
        /// No value set for this enumeration
        /// </summary>
        notset = 0,
        /// <summary>
        /// Financial Aid
        /// </summary>
        financialAid,
        /// <summary>
        /// Deposit
        /// </summary>
        deposit,
        /// <summary>
        /// Sponsor
        /// </summary>
        sponsor,
        /// <summary>
        /// Payroll
        /// </summary>
        payroll,
        /// <summary>
        /// Cash
        /// </summary>
        cash
    }
}