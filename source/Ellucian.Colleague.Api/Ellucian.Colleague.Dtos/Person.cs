﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Ellucian.Colleague.Dtos.Converters;
using Newtonsoft.Json;

namespace Ellucian.Colleague.Dtos
{
    /// <summary>
    /// Information about a person
    /// </summary>
    [DataContract]
    public class Person : BaseModel
    {
        /// <summary>
        /// <see cref="PersonName">Names</see> of the person
        /// </summary>
        [DataMember(Name="names")]
        public IEnumerable<PersonName> PersonNames { get; set; }

        /// <summary>
        /// Date of birth
        /// </summary>
        [JsonConverter(typeof(DateOnlyConverter))]
        [DataMember(Name = "dateOfBirth")]
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Deceased date
        /// </summary>
        [JsonConverter(typeof(DateOnlyConverter))]
        [DataMember(Name = "dateDeceased")]
        public DateTime? DeceasedDate { get; set; }

        /// <summary>
        /// The <see cref="GenderType">gender type</see> of the person
        /// </summary>
        [DataMember(Name = "gender")]
        public GenderType? GenderType { get; set; }

        /// <summary>
        /// Globally unique identifier for ethnicity
        /// </summary>
        [DataMember(Name = "ethnicity")]
        public GuidObject Ethnicity { get; set; }

        /// <summary>
        /// Collection of globally unique identifiers for races
        /// </summary>
        [DataMember(Name = "races")]
        public IEnumerable<GuidObject> Races { get; set; }

        /// <summary>
        /// Globally unique identifier for martial status
        /// </summary>
        [DataMember(Name = "maritalStatus")]
        public GuidObject MaritalStatus { get; set; }

        /// <summary>
        /// <see cref="Role">Roles</see> of the person
        /// </summary>
        [DataMember(Name = "roles")]
        public IEnumerable<Role> Roles { get; set; }

        /// <summary>
        /// <see cref="Credential">Credentials</see> of the person
        /// </summary>
        [DataMember(Name = "credentials")]
        public IEnumerable<Credential> Credentials { get; set; }

        /// <summary>
        /// <see cref="Address">Addresses</see> of the person
        /// </summary>
        [DataMember(Name = "addresses")]
        public IEnumerable<Address> Addresses { get; set; }

        /// <summary>
        /// <see cref="Phone">Phones</see> of the person
        /// </summary>
        [DataMember(Name = "phones")]
        public IEnumerable<Phone> Phones { get; set; }

        /// <summary>
        /// <see cref="EmailAddress">Email addresses</see> of the person
        /// </summary>
        [DataMember(Name = "emails")]
        public IEnumerable<EmailAddress> EmailAddresses { get; set; }

        /// <summary>
        /// Person constructor
        /// </summary>
        public Person() : base()
        {
        }
    }
}