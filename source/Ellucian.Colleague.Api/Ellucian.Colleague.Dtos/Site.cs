﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Ellucian.Colleague.Dtos
{
    /// <summary>
    /// A physical location within an organization.
    /// </summary>
    [DataContract]
    public class Site : CodeItem
    {
        /// <summary>
        /// Globally unique identifier for associated organization
        /// </summary>
        [DataMember(Name = "organization")]
        public GuidObject OrganizationGuid { get; set; }

        /// <summary>
        /// Collection of globally unique identifiers buildings at the site
        /// </summary>
        [DataMember(Name = "buildings")]
        public IEnumerable<GuidObject> BuildingGuids { get; set; }
    }
}