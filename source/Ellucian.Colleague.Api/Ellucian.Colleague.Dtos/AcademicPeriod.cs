﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos.Converters;
using Newtonsoft.Json;
using System;
using System.Runtime.Serialization;

namespace Ellucian.Colleague.Dtos
{
    /// <summary>
    /// Academic Period 
    /// </summary>
    [DataContract]
    public class AcademicPeriod : CodeItem
    {
        /// <summary>
        /// The <see cref="AcademicPeriodCategory">Academic Period category</see>
        /// </summary>
        [DataMember(Name = "category")]
        public AcademicPeriodCategory category { get; set; }

        /// <summary>
        /// Start
        /// </summary>
        [JsonConverter(typeof(DateOnlyConverter))]
        [DataMember(Name = "start")]
        public DateTime? Start { get; set; }


        /// <summary>
        /// End
        /// </summary>
        [JsonConverter(typeof(DateOnlyConverter))]
        [DataMember(Name = "end")]
        public DateTime? End { get; set; }


    }
}
