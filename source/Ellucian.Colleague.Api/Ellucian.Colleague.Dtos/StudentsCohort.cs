﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System.Runtime.Serialization;

namespace Ellucian.Colleague.Dtos
{
    /// <summary>
    /// The type of students cohort.
    /// </summary>
    [DataContract]
    public class StudentCohort : CodeItem2
    {      
    }
}