﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Domain.HumanResources.Entities;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.HumanResources.Tests
{
    public class TestPersonPositionWageRepository : IPersonPositionWageRepository
    {
        public class PersonPositionWageRecord
        {
            public string id;
            public string personId;
            public string positionId;
            public string personPositionId;
            public string payClassId;
            public string payCycleId;
            public string positionPayDefaultId;
            public DateTime? startDate;
            public DateTime? endDate;
            public string regularWorkEarningsType;
            public string paySuspendedFlag;
            public List<PayItemRecord> payItems;
        }

        public class PayItemRecord
        {
            public string fundSourceId;
            public string projectId;
        }

        public List<PersonPositionWageRecord> personPositionWageRecords = new List<PersonPositionWageRecord>() 
        {
            new PersonPositionWageRecord() {
                id = "5",
                personId = "0003914",
                positionId = "MUSICPROF12345",
                personPositionId = "12345",
                payClassId = "MC",
                payCycleId = "BM",
                positionPayDefaultId = "555",
                startDate = new DateTime(2010, 1, 1),
                endDate = new DateTime(2015, 12, 31),
                regularWorkEarningsType = "REG",        
                paySuspendedFlag = "N",
                payItems = new List<PayItemRecord>()
                {
                    new PayItemRecord() {fundSourceId = "COMP"},
                    new PayItemRecord() {fundSourceId = "COMP"}
                }
            },
            new PersonPositionWageRecord() {
                id = "6",
                personId = "0003914",
                positionId = "MUSICPROF12345",
                personPositionId = "45432",
                payClassId = "MC",
                payCycleId = "BM",
                positionPayDefaultId = "555",
                startDate = new DateTime(2016, 1, 1),
                endDate = null,
                regularWorkEarningsType = "REG",  
                paySuspendedFlag = "N",
                payItems = new List<PayItemRecord>()
                {
                    new PayItemRecord() {fundSourceId = "COMP"},
                    new PayItemRecord() {fundSourceId = "PROJ", projectId = "11"}
                }
            },

            new PersonPositionWageRecord() {
                id = "7",
                personId = "0004409",
                positionId = "COMPSCIASSOC",
                personPositionId = "44432",
                payClassId = "CK",
                payCycleId = "MW",
                positionPayDefaultId = "143",
                startDate = new DateTime(2010, 1, 1),
                endDate = null,
                regularWorkEarningsType = "REG",       
                paySuspendedFlag = "Y",
                payItems = new List<PayItemRecord>()
                {
                    new PayItemRecord() {fundSourceId = "COMP"},
                    new PayItemRecord() {fundSourceId = "COMP"}
                }
            },

            new PersonPositionWageRecord() {
                id = "8",
                personId = "0004409",
                positionId = "LABTECH12343",
                personPositionId = "67676",
                payClassId = "CK",
                payCycleId = "MW",
                positionPayDefaultId = "143",
                startDate = new DateTime(2010, 1, 1),
                endDate = null,
                regularWorkEarningsType = "REG",                
                payItems = new List<PayItemRecord>()
                {
                    new PayItemRecord() {fundSourceId = "COMP"},
                    new PayItemRecord() {fundSourceId = "COMP"}
                }
            },
        };

        public IEnumerable<string> PersonIdsUsedInTestData
        {
            get
            {
                return personPositionWageRecords.Select(ppw => ppw.personId).Distinct();
            }
        }

        public async Task<IEnumerable<PersonPositionWage>> GetPersonPositionWagesAsync(IEnumerable<string> personIds)
        {
            var personPositionWages = personPositionWageRecords
                .Where(r => personIds.Contains(r.personId))
                .Select(r =>
                {
                    try
                    {
                        return BuildPersonPositionWage(r);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                })
                .Where(e => e != null);

            return await Task.FromResult(personPositionWages);
        }

        public PersonPositionWage BuildPersonPositionWage(PersonPositionWageRecord record)
        {
            if (record == null)
            {
                throw new ArgumentNullException("record");
            }

            return new PersonPositionWage(record.id, record.personId, record.positionId, record.personPositionId, record.positionPayDefaultId, record.payClassId, record.payCycleId, record.regularWorkEarningsType, record.startDate.Value)
            {
                EndDate = record.endDate,
                IsPaySuspended = !string.IsNullOrEmpty(record.paySuspendedFlag) && record.paySuspendedFlag.Equals("Y", StringComparison.InvariantCultureIgnoreCase),
                FundingSources = record.payItems
                    .Select(item => new PositionFundingSource(item.fundSourceId, record.payItems.IndexOf(item)) { ProjectId = item.projectId }).ToList()
            };
        }
    }
}
