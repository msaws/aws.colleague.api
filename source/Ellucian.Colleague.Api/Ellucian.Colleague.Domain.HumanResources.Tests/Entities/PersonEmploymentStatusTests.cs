﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Domain.HumanResources.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.HumanResources.Tests.Entities
{
    [TestClass]
    public class PersonEmploymentStatusTests
    {
        string id;
        string personId;
        string primaryPositionId;
        string personPositionId;
        DateTime? startDate;
        DateTime? endDate;



        public PersonEmploymentStatus personEmploymentStatus;

        [TestClass]
        public class PersonEmploymentStatusTestsConstructorTests : PersonEmploymentStatusTests
        {

            [TestInitialize]
            public void Initialize()
            {
                id = "001";
                personId = "24601";
                primaryPositionId = "abc";
                personPositionId = "999";
                startDate = new DateTime();
                endDate = null;
            }

            [TestMethod]
            public void ConstructorSetsPropertiesTest()
            {
                personEmploymentStatus = new PersonEmploymentStatus(id, personId, primaryPositionId, personPositionId, startDate, endDate);
                Assert.AreEqual(id, personEmploymentStatus.Id);
                Assert.AreEqual(personId, personEmploymentStatus.PersonId);
                Assert.AreEqual(primaryPositionId, personEmploymentStatus.PrimaryPositionId);
                Assert.AreEqual(personPositionId, personEmploymentStatus.PersonPositionId);
                Assert.AreEqual(startDate, personEmploymentStatus.StartDate);
                Assert.AreEqual(endDate, personEmploymentStatus.EndDate);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void IdRequiredTest()
            {
                id = "";
                personEmploymentStatus = new PersonEmploymentStatus(id, personId, primaryPositionId, personPositionId, startDate, endDate);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonIdRequiredTest()
            {
                personId = "";
                personEmploymentStatus = new PersonEmploymentStatus(id, personId, primaryPositionId, personPositionId, startDate, endDate);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PrimaryPositionIdRequiredTest()
            {
                primaryPositionId = "";
                personEmploymentStatus = new PersonEmploymentStatus(id, personId, primaryPositionId, personPositionId, startDate, endDate);
            }
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonPositionIdRequiredTest()
            {
                personPositionId = "";
                personEmploymentStatus = new PersonEmploymentStatus(id, personId, primaryPositionId, personPositionId, startDate, endDate);
            }
            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public void NullStartDatePassedInTest()
            {
                startDate = null;
                personEmploymentStatus = new PersonEmploymentStatus(id, personId, primaryPositionId, personPositionId, startDate, endDate);
            }
        }
    }
}
