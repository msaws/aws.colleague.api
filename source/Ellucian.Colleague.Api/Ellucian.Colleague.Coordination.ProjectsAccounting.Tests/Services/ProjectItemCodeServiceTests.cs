﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.ProjectsAccounting.Services;
using Ellucian.Colleague.Coordination.ProjectsAccounting.Tests.UserFactories;
using Ellucian.Colleague.Data.ProjectsAccounting.Tests;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;

namespace Ellucian.Colleague.Coordination.ProjectsAccounting.Tests.Services
{
    /// <summary>
    /// This class tests the ProjectItemCodeService class.
    /// </summary>
    [TestClass]
    public class ProjectItemCodeServiceTests : ProjectsAccountingCurrentUser
    {
        #region Initialize and Cleanup
        private ProjectItemCodeService projectItemCodeService = null;
        private TestProjectsAccountingReferenceDataRepository testColleagueFinanceReferenceRepository = null;
        private UserFactory userFactory = new ProjectsAccountingCurrentUser.UserFactory();

        [TestInitialize]
        public void Initialize()
        {
            // Build the service object to use the project fatory built above
            BuildValidProjectItemCodeService();
        }

        [TestCleanup]
        public void Cleanup()
        {
            // Reset all of the services and repository variables.
            projectItemCodeService = null;
            testColleagueFinanceReferenceRepository = null;
        }
        #endregion

        [TestMethod]
        public async Task GetProjectItemCodes()
        {
            // Get the project item codes
            var projectItemCodeDtos = await projectItemCodeService.GetProjectItemCodesAsync();
            var projectItemCodeDomainEntities = await testColleagueFinanceReferenceRepository.GetLineItemCodesAsync();

            // Confirm that we have the correct number of project types
            Assert.IsTrue(projectItemCodeDtos.Count() == projectItemCodeDomainEntities.Count());

            // Confirm that the DTO and domain entities lists have the same data
            foreach (var itemCode in projectItemCodeDtos)
            {
                Assert.IsTrue(projectItemCodeDomainEntities.Any(x =>
                    x.Code == itemCode.Code
                    && x.Description == itemCode.Description));
            }
        }

        /// <summary>
        /// Builds a project type service object.
        /// </summary>
        /// <returns>Nothing.</returns>
        private void BuildValidProjectItemCodeService()
        {
            // A ProjectTypeService requires six parameters for its constructor:
            //   1. a project type repository object.
            //   2. an adapterRegistry object.
            //   3. a UserFactory object.
            //   4. a role repository object.
            //   5. a logger object.
            // We need the unit tests to be independent of "real" implementations of these classes,
            // so we use Moq to create mock implementations that are based on the same interfaces
            // Set up mock objects
            var roleRepository = new Mock<IRoleRepository>().Object;
            var loggerObject = new Mock<ILogger>().Object;

            // Initialize test repository
            testColleagueFinanceReferenceRepository = new TestProjectsAccountingReferenceDataRepository();

            // Set up and mock the adapter, and setup the GetAdapter method.
            var adapterRegistry = new Mock<IAdapterRegistry>();
            var projectItemCodeDtoAdapter = new AutoMapperAdapter<Domain.ProjectsAccounting.Entities.ProjectItemCode, Dtos.ProjectsAccounting.ProjectItemCode>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Domain.ProjectsAccounting.Entities.ProjectItemCode, Dtos.ProjectsAccounting.ProjectItemCode>()).Returns(projectItemCodeDtoAdapter);

            // Set up a list of project types and set up the service.
            projectItemCodeService = new ProjectItemCodeService(testColleagueFinanceReferenceRepository, adapterRegistry.Object, userFactory, roleRepository, loggerObject);
        }
    }
}
