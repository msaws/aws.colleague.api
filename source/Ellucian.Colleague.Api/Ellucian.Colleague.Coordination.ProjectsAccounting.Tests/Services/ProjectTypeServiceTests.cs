﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.ProjectsAccounting.Services;
using Ellucian.Colleague.Coordination.ProjectsAccounting.Tests.UserFactories;
using Ellucian.Colleague.Data.ProjectsAccounting.Tests;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Tests.Services
{
    /// <summary>
    /// This class tests the ProjectTypeService class to confirm that it properly
    /// returns a list of ProjectType DTOs.
    /// </summary>
    [TestClass]
    public class ProjectTypeServiceTests : ProjectsAccountingCurrentUser
    {
        #region Initialize and Cleanup
        private ProjectTypeService projectTypeService = null;
        private TestProjectsAccountingReferenceDataRepository testColleagueFinanceReferenceRepository;

        // Define a user factory
        private UserFactory userFactory = new ProjectsAccountingCurrentUser.UserFactory();

        [TestInitialize]
        public void Initialize()
        {
            // Build the service object to use the project fatory built above
            BuildValidProjectTypeService();
        }

        [TestCleanup]
        public void Cleanup()
        {
            // Reset all of the services and repository variables.
            projectTypeService = null;
            testColleagueFinanceReferenceRepository = null;
        }
        #endregion

        [TestMethod]
        public async Task GetProjectTypes()
        {
            // Get all the project types
            var projectTypesDtos = await projectTypeService.GetProjectTypesAsync();

            var projectTypeDomainEntity = await testColleagueFinanceReferenceRepository.GetProjectTypesAsync();

            // Assert that we have the correct number of project types
            Assert.IsTrue(projectTypesDtos.Count() == projectTypeDomainEntity.Count());

            // Confirm that each project type in the Dtos is in the project types repository
            foreach (var projectType in projectTypesDtos)
            {
                Assert.IsTrue(projectTypeDomainEntity.Any(x => x.Code == projectType.Code));
            }
        }

        /// <summary>
        /// Builds a project type service object.
        /// </summary>
        /// <returns>Nothing.</returns>
        private void BuildValidProjectTypeService()
        {
            // A ProjectTypeService requires six parameters for its constructor:
            //   1. a project type repository object.
            //   2. an adapterRegistry object.
            //   3. a UserFactory object.
            //   4. a role repository object.
            //   5. a logger object.
            // We need the unit tests to be independent of "real" implementations of these classes,
            // so we use Moq to create mock implementations that are based on the same interfaces
            // Set up mock objects
            var roleRepository = new Mock<IRoleRepository>().Object;
            var loggerObject = new Mock<ILogger>().Object;

            // Initialize test repository
            testColleagueFinanceReferenceRepository = new TestProjectsAccountingReferenceDataRepository();

            // Set up and mock the adapter, and setup the GetAdapter method.
            var adapterRegistry = new Mock<IAdapterRegistry>();
            var projectTypeDtoAdapter = new AutoMapperAdapter<Domain.ProjectsAccounting.Entities.ProjectType, Dtos.ProjectsAccounting.ProjectType>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Domain.ProjectsAccounting.Entities.ProjectType, Dtos.ProjectsAccounting.ProjectType>()).Returns(projectTypeDtoAdapter);

            // Set up a list of project types and set up the service.
            projectTypeService = new ProjectTypeService(testColleagueFinanceReferenceRepository, adapterRegistry.Object, userFactory, roleRepository, loggerObject);
        }
    }
}
