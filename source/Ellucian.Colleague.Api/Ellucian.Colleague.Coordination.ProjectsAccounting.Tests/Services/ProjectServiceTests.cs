﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.ProjectsAccounting.Services;
using Ellucian.Colleague.Coordination.ProjectsAccounting.Tests.UserFactories;
using Ellucian.Colleague.Domain.ColleagueFinance.Tests;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Tests;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Tests.Services
{
    /// <summary>
    /// This class tests that the service returns the proper list of projects for a given user.
    /// It tests scenario where the user has partial access, no access, and access
    /// to projects that no longer exist.
    /// </summary>
    [TestClass]
    public class ProjectServiceTests : ProjectsAccountingCurrentUser
    {
        #region Initialize and Cleanup
        private ProjectService serviceSubset = null;
        private ProjectService serviceNone = null;
        private ProjectService serviceNonexistant = null;
        private ProjectService serviceAll = null;
        private ProjectService serviceOne = null;

        private TestProjectsAccountingUserRepository testProjectsAccountingUserRepository;
        private TestProjectRepository testProjectRepository;
        private ProjectsAccountingUser projectsAccountingUser;
        private TestGeneralLedgerConfigurationRepository testGeneralLedgerConfigurationRepository;

        private List<Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectType> projectTypeObjects = null;

        // Define user factories
        private UserFactorySubset currentUserFactorySubset = new ProjectsAccountingCurrentUser.UserFactorySubset();
        private UserFactoryNone currentUserFactoryNone = new ProjectsAccountingCurrentUser.UserFactoryNone();
        private UserFactoryNonexistant currentUserFactoryNonexistant = new ProjectsAccountingCurrentUser.UserFactoryNonexistant();
        private UserFactoryAll currentUserFactoryAll = new ProjectsAccountingCurrentUser.UserFactoryAll();

        // Other misc variables
        private List<UrlFilter> filters;

        [TestInitialize]
        public void Initialize()
        {
            // Build all service objects to use each of the user factories built above
            BuildValidProjectService();

            // Set up a list of ProjectTypes
            projectTypeObjects = new List<Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectType>();
            projectTypeObjects.Add(new Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectType("R", "Research"));
            projectTypeObjects.Add(new Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectType("C", "Construction"));
            projectTypeObjects.Add(new Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectType("H", "Housing"));
            projectTypeObjects.Add(new Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectType("T", "Training"));

            projectsAccountingUser = null;

            filters = new List<UrlFilter>();
        }

        [TestCleanup]
        public void Cleanup()
        {
            // Reset all of the services and repository variables.
            serviceSubset = null;
            serviceNone = null;
            serviceNonexistant = null;
            serviceAll = null;
            serviceOne = null;
            testProjectsAccountingUserRepository = null;
            projectsAccountingUser = null;
            testProjectRepository = null;
            testGeneralLedgerConfigurationRepository = null;

            projectTypeObjects = null;
            filters = null;
        }
        #endregion

        #region GetProjectsForUser test methods
        [TestMethod]
        public async Task GetProjectsForUser_Subset()
        {
            // Get all projects for the user
            var actualProjectDtos = await serviceSubset.GetProjectsAsync(true, null);

            // Build the projects accounting user object and get the list of project domain entities from the test repository.
            projectsAccountingUser = await testProjectsAccountingUserRepository.GetProjectsAccountingUserAsync(currentUserFactorySubset.CurrentUser.PersonId);
            var projectDomainEntities = await testProjectRepository.GetProjectsAsync(projectsAccountingUser.Projects, true, null);

            // Assert that we have the correct number of projects
            Assert.IsTrue(actualProjectDtos.Count() == projectDomainEntities.Count());

            // Confirm that the each project in the Dtos is in the project repository.
            foreach (var project in actualProjectDtos)
            {
                Assert.IsTrue(projectDomainEntities.Any(x => x.ProjectId == project.Id));
            }
        }

        [TestMethod]
        public async Task GetProjectsForUser_None()
        {
            // Get all projects 
            var actualProjectDtos = await serviceNone.GetProjectsAsync(true, null);

            // Build the projects accounting user object and get the list of project domain entities from the test repository.
            projectsAccountingUser = await testProjectsAccountingUserRepository.GetProjectsAccountingUserAsync(currentUserFactoryNone.CurrentUser.PersonId);
            var projectDomainEntities = await testProjectRepository.GetProjectsAsync(projectsAccountingUser.Projects, true, null);

            // Assert that we truly have no projects.
            Assert.IsTrue(actualProjectDtos.Count() == 0);
        }

        [TestMethod]
        public async Task GetProjectsForUser_Nonexistant()
        {
            // Get all projects 
            var actualProjectDtos = await serviceNonexistant.GetProjectsAsync(true, new List<UrlFilter>());

            // Build the projects accounting user object and get the list of project domain entities from the test repository.
            projectsAccountingUser = await testProjectsAccountingUserRepository.GetProjectsAccountingUserAsync(currentUserFactoryNonexistant.CurrentUser.PersonId);
            var projectDomainEntities = await testProjectRepository.GetProjectsAsync(projectsAccountingUser.Projects, true, null);

            // Make sure each project DTO does NOT exist in the repository
            foreach (var project in actualProjectDtos)
            {
                Assert.IsTrue(projectDomainEntities.Any(x => x.ProjectId != project.Id));
            }
        }
        #endregion

        #region Tests for GetProjects
        [TestMethod]
        public async Task GetProjects_NullCriteria()
        {
            // Get filtered projects 
            var projectDtos = await serviceAll.GetProjectsAsync(true, null);

            // Get the list of project domain entities from the test repository.
            projectsAccountingUser = await testProjectsAccountingUserRepository.GetProjectsAccountingUserAsync(currentUserFactoryAll.CurrentUser.PersonId);
            var projectDomainEntities = await testProjectRepository.GetProjectsAsync(projectsAccountingUser.Projects, true, null);

            // Make sure that none of the projects have been filtered out by the service
            Assert.AreEqual(projectDomainEntities.Count(), projectDtos.Count());

            foreach (var project in projectDtos)
            {
                Assert.IsTrue(projectDomainEntities.Any(x =>
                    x.ProjectId == project.Id
                    && x.Status.ToString() == project.Status.ToString()));
            }
        }

        [TestMethod]
        public async Task GetProjects_IncompleteFilter_NullField()
        {
            // Get filtered projects
            var filters = new List<UrlFilter>() { new UrlFilter() { Field = null, Value = "A" } };
            var projectDtos = await serviceAll.GetProjectsAsync(true, filters);

            // Get the list of project domain entities from the test repository.
            projectsAccountingUser = await testProjectsAccountingUserRepository.GetProjectsAccountingUserAsync(currentUserFactoryAll.CurrentUser.PersonId);
            var projectDomainEntities = await testProjectRepository.GetProjectsAsync(projectsAccountingUser.Projects, true, null);

            // Make sure that none of the projects have been filtered out by the service
            Assert.AreEqual(projectDomainEntities.Count(), projectDtos.Count());

            foreach (var project in projectDtos)
            {
                Assert.IsTrue(projectDomainEntities.Any(x =>
                    x.ProjectId == project.Id
                    && x.Status.ToString() == project.Status.ToString()));
            }
        }

        [TestMethod]
        public async Task GetProjects_IncompleteFilter_NullOperator()
        {
            // Get filtered projects
            var filters = new List<UrlFilter>() { new UrlFilter() { Field = "status", Operator = null, Value = "A" } };
            var projectDtos = await serviceAll.GetProjectsAsync(true, filters);

            // Get the list of project domain entities from the test repository.
            projectsAccountingUser = await testProjectsAccountingUserRepository.GetProjectsAccountingUserAsync(currentUserFactoryAll.CurrentUser.PersonId);
            var projectDomainEntities = await testProjectRepository.GetProjectsAsync(projectsAccountingUser.Projects, true, null);

            // Make sure that none of the projects have been filtered out by the service
            Assert.AreEqual(projectDomainEntities.Count(), projectDtos.Count());

            foreach (var project in projectDtos)
            {
                Assert.IsTrue(projectDomainEntities.Any(x =>
                    x.ProjectId == project.Id
                    && x.Status.ToString() == project.Status.ToString()));
            }
        }

        [TestMethod]
        public async Task GetProjects_IncompleteFilter_NullValue()
        {
            // Get filtered projects
            var filters = new List<UrlFilter>() { new UrlFilter() { Field = "status", Value = null } };
            var projectDtos = await serviceAll.GetProjectsAsync(true, filters);

            // Get the list of project domain entities from the test repository.
            projectsAccountingUser = await testProjectsAccountingUserRepository.GetProjectsAccountingUserAsync(currentUserFactoryAll.CurrentUser.PersonId);
            var projectDomainEntities = await testProjectRepository.GetProjectsAsync(projectsAccountingUser.Projects, true, null);

            // Make sure that none of the projects have been filtered out by the service
            Assert.AreEqual(projectDomainEntities.Count(), projectDtos.Count());

            foreach (var project in projectDtos)
            {
                Assert.IsTrue(projectDomainEntities.Any(x =>
                    x.ProjectId == project.Id
                    && x.Status.ToString() == project.Status.ToString()));
            }
        }

        [TestMethod]
        public async Task GetProjects_NoOptionsSelected()
        {
            // Get filtered projects 
            var projectDtos = await serviceAll.GetProjectsAsync(true, filters);

            // Get the list of project domain entities from the test repository.
            projectsAccountingUser = await testProjectsAccountingUserRepository.GetProjectsAccountingUserAsync(currentUserFactoryAll.CurrentUser.PersonId);
            var projectDomainEntities = await testProjectRepository.GetProjectsAsync(projectsAccountingUser.Projects, true, null);

            // Make sure that none of the projects have been filtered out by the service
            Assert.AreEqual(projectDomainEntities.Count(), projectDtos.Count());

            foreach (var project in projectDtos)
            {
                Assert.IsTrue(projectDomainEntities.Any(x =>
                    x.ProjectId == project.Id
                    && x.Status.ToString() == project.Status.ToString()));
            }
        }

        [TestMethod]
        public async Task GetProjects_AllOptionsSelected()
        {
            // Set up filter criteria
            filters.Add(new UrlFilter()
            {
                Field = "Status",
                Operator = "eq",
                Value = Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectStatus.Active.ToString()
            });
            filters.Add(new UrlFilter()
            {
                Field = "Status",
                Operator = "eq",
                Value = Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectStatus.Inactive.ToString()
            });
            filters.Add(new UrlFilter()
            {
                Field = "Status",
                Operator = "eq",
                Value = Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectStatus.Closed.ToString()
            });

            filters.Add(new UrlFilter()
            {
                Field = "ProjectType",
                Operator = "eq",
                Value = "C"
            });
            filters.Add(new UrlFilter()
            {
                Field = "ProjectType",
                Operator = "eq",
                Value = "H"
            });
            filters.Add(new UrlFilter()
            {
                Field = "ProjectType",
                Operator = "eq",
                Value = "R"
            });
            filters.Add(new UrlFilter()
            {
                Field = "ProjectType",
                Operator = "eq",
                Value = "T"
            });

            // Get filtered projects 
            var projectDtos = await serviceAll.GetProjectsAsync(true, filters);

            // Get the list of project domain entities from the test repository.
            projectsAccountingUser = await testProjectsAccountingUserRepository.GetProjectsAccountingUserAsync(currentUserFactoryAll.CurrentUser.PersonId);
            var projectDomainEntities = await testProjectRepository.GetProjectsAsync(projectsAccountingUser.Projects, true, null);

            // Make sure that none of the projects have been filtered out by the service
            Assert.AreEqual(projectDomainEntities.Count(), projectDtos.Count());

            foreach (var project in projectDtos)
            {
                Assert.IsTrue(projectDomainEntities.Any(x =>
                    x.ProjectId == project.Id
                   && x.Status.ToString() == project.Status.ToString()));
            }
        }

        [TestMethod]
        public async Task GetProjects_OnlyTypeSelected()
        {
            // Set up filter criteria
            filters.Add(new UrlFilter()
            {
                Field = "ProjectType",
                Operator = "eq",
                Value = "C"
            });

            // Get filtered projects 
            var projectDtos = await serviceAll.GetProjectsAsync(true, filters);

            // Get the list of project domain entities from the test repository.
            projectsAccountingUser = await testProjectsAccountingUserRepository.GetProjectsAccountingUserAsync(currentUserFactoryAll.CurrentUser.PersonId);
            var projectDomainEntities = await testProjectRepository.GetProjectsAsync(projectsAccountingUser.Projects, true, null);

            // Make sure that none of the projects have been filtered out by the service
            Assert.AreEqual(1, projectDtos.Count());

            foreach (var project in projectDtos)
            {
                Assert.IsTrue(projectDomainEntities.Any(x =>
                    x.ProjectId == project.Id
                   && x.Status.ToString() == project.Status.ToString()));
            }
        }

        [TestMethod]
        public async Task GetProjects_OnlyStatusSelected()
        {
            // Set up filter criteria
            filters.Add(new UrlFilter()
            {
                Field = "Status",
                Operator = "eq",
                Value = Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectStatus.Active.ToString()
            });

            // Get filtered projects 
            var projectDtos = await serviceAll.GetProjectsAsync(true, filters);

            // Get the list of project domain entities from the test repository.
            projectsAccountingUser = await testProjectsAccountingUserRepository.GetProjectsAccountingUserAsync(currentUserFactoryAll.CurrentUser.PersonId);
            var projectDomainEntities = await testProjectRepository.GetProjectsAsync(projectsAccountingUser.Projects, true, null);

            // Make sure that none of the projects have been filtered out by the service
            Assert.AreEqual(2, projectDtos.Count());

            foreach (var project in projectDtos)
            {
                Assert.IsTrue(projectDomainEntities.Any(x =>
                    x.ProjectId == project.Id
                    && x.Status.ToString() == project.Status.ToString()));
            }
        }

        [TestMethod]
        public async Task GetProjects_OneOptionFromEachSelected()
        {
            // Set up filter criteria
            filters.Add(new UrlFilter()
            {
                Field = "Status",
                Operator = "eq",
                Value = Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectStatus.Active.ToString()
            });

            filters.Add(new UrlFilter()
            {
                Field = "ProjectType",
                Operator = "eq",
                Value = "C"
            });

            // Get filtered projects 
            var projectDtos = await serviceAll.GetProjectsAsync(true, filters);

            // Get the list of project domain entities from the test repository.
            projectsAccountingUser = await testProjectsAccountingUserRepository.GetProjectsAccountingUserAsync(currentUserFactoryAll.CurrentUser.PersonId);
            var projectDomainEntities = await testProjectRepository.GetProjectsAsync(projectsAccountingUser.Projects, true, null);

            // Make sure that none of the projects have been filtered out by the service
            Assert.AreEqual(1, projectDtos.Count());

            foreach (var project in projectDtos)
            {
                Assert.IsTrue(projectDomainEntities.Any(x =>
                    x.ProjectId == project.Id
                    && x.Status.ToString() == project.Status.ToString()));
            }
        }
        #endregion

        #region Tests for GetProject
        [TestMethod]
        public async Task GetProject()
        {
            // Get all projects for the user
            var projectId = "8";
            var projectDto = await serviceOne.GetProjectAsync(projectId, false, null);

            // Build the projects accounting user object and get the list of project domain entities from the test repository.
            projectsAccountingUser = await testProjectsAccountingUserRepository.GetProjectsAccountingUserAsync(currentUserFactorySubset.CurrentUser.PersonId);
            var projectDomainEntity = await testProjectRepository.GetProjectAsync(projectId, false, null);

            // Confirm that the data in the DTO matches the domain entity
            Assert.AreEqual(projectDto.Id, projectDomainEntity.ProjectId);
        }

        [TestMethod]
        [ExpectedException(typeof(PermissionsException))]
        public async Task GetProject_NoAccess()
        {
            // Get all projects for the user
            var projectId = "GTT";
            var projectDto = await serviceOne.GetProjectAsync(projectId, false, null);
        }
        #endregion

        #region Build service method
        /// <summary>
        /// Builds multiple project service objects.
        /// </summary>
        /// <returns>Nothing.</returns>
        private void BuildValidProjectService()
        {
            #region Initialize mock objects
            // A ProjectService requires six parameters for its constructor:
            //   1. a project repository object.
            //   2. a projects accounting user repository object.
            //   3. an adapterRegistry object.
            //   4. a currentUserFactory object.
            //   5. a role repository object.
            //   6. a logger object.
            // We need the unit tests to be independent of "real" implementations of these classes,
            // so we use Moq to create mock implementations that are based on the same interfaces
            var roleRepository = new Mock<IRoleRepository>().Object;
            var loggerObject = new Mock<ILogger>().Object;

            testProjectsAccountingUserRepository = new TestProjectsAccountingUserRepository();
            testProjectRepository = new TestProjectRepository();
            testGeneralLedgerConfigurationRepository = new TestGeneralLedgerConfigurationRepository();

            // Set up and mock the adapter, and setup the GetAdapter method.
            var adapterRegistry = new Mock<IAdapterRegistry>();
            var projectDtoAdapter = new AutoMapperAdapter<Domain.ProjectsAccounting.Entities.Project, Dtos.ProjectsAccounting.Project>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Domain.ProjectsAccounting.Entities.Project, Dtos.ProjectsAccounting.Project>()).Returns(projectDtoAdapter);

            // Set up the custom adapter
            var customAdapterRegistry = new Mock<IAdapterRegistry>();
            var customProjectDtoAdapter = new AutoMapperAdapter<Domain.ProjectsAccounting.Entities.Project, Dtos.ProjectsAccounting.Project>(customAdapterRegistry.Object, loggerObject);
            #endregion

            #region Set up the services
            // Set up the current user with a subset of projects and set up the service.
            serviceSubset = new ProjectService(testProjectRepository, testProjectsAccountingUserRepository, testGeneralLedgerConfigurationRepository, adapterRegistry.Object, currentUserFactorySubset, roleRepository, loggerObject);

            // Set up the current user with no projects and set up the service.
            serviceNone = new ProjectService(testProjectRepository, testProjectsAccountingUserRepository, testGeneralLedgerConfigurationRepository, adapterRegistry.Object, currentUserFactoryNone, roleRepository, loggerObject);

            // Set up the current user with access to projects that don't exist and set up the service.
            serviceNonexistant = new ProjectService(testProjectRepository, testProjectsAccountingUserRepository, testGeneralLedgerConfigurationRepository, adapterRegistry.Object, currentUserFactoryNonexistant, roleRepository, loggerObject);

            // Set up the current user with no projects and set up the service.
            serviceAll = new ProjectService(testProjectRepository, testProjectsAccountingUserRepository, testGeneralLedgerConfigurationRepository, adapterRegistry.Object, currentUserFactoryAll, roleRepository, loggerObject);

            // Set up the current user with a subset of projects and set up the service and send in the custom adapter for getting a single project.
            serviceOne = new ProjectService(testProjectRepository, testProjectsAccountingUserRepository, testGeneralLedgerConfigurationRepository, customAdapterRegistry.Object, currentUserFactorySubset, roleRepository, loggerObject);
            #endregion
        }
        #endregion
    }
}
