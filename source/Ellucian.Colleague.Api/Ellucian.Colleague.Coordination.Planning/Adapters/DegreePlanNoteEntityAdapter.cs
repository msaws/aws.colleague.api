﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Ellucian.Colleague.Coordination.Base.Adapters;
using Ellucian.Web.Adapters;
using slf4net;

namespace Ellucian.Colleague.Coordination.Planning.Adapters
{
    public class DegreePlanNoteEntityAdapter : AutoMapperAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanNote, Ellucian.Colleague.Dtos.Planning.DegreePlanNote>
    {
        public DegreePlanNoteEntityAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
        }

        public override Dtos.Planning.DegreePlanNote MapToType(Domain.Planning.Entities.DegreePlanNote Source)
        {
            var dpnDto = base.MapToType(Source);
            // for some unknown reason the DateTimeOffsetToDateTimeAdapter assigns a default DateTime
            // to the target object's Date property. So we still need to manually handle this mapping.
            dpnDto.Date = Source.Date.HasValue ? Source.Date.Value.DateTime : (DateTime?)null;
            return dpnDto;
        }
    }
}
