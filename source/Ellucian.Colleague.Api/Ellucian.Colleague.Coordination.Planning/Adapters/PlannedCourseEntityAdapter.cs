﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Ellucian.Colleague.Coordination.Base.Adapters;
using Ellucian.Web.Adapters;
using slf4net;

namespace Ellucian.Colleague.Coordination.Planning.Adapters
{
    public class PlannedCourseToPlannedCourse2DtoAdapter : AutoMapperAdapter<Ellucian.Colleague.Domain.Planning.Entities.PlannedCourse, Ellucian.Colleague.Dtos.Planning.PlannedCourse2>
    {
        public PlannedCourseToPlannedCourse2DtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
        }

        public override Dtos.Planning.PlannedCourse2 MapToType(Domain.Planning.Entities.PlannedCourse Source)
        {
            var pcDto = base.MapToType(Source);
            pcDto.AddedOn = Source.AddedOn.HasValue ? Source.AddedOn.Value.DateTime : (DateTime?)null;
            return pcDto;
        }
    }
}
