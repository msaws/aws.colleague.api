﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Planning.Entities;
using Ellucian.Web.Adapters;
using slf4net;

namespace Ellucian.Colleague.Coordination.Planning.Adapters
{
    public class DegreePlanPreviewEntity3Adapter : BaseAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview, Ellucian.Colleague.Dtos.Planning.DegreePlanPreview3>
    {
        public DegreePlanPreviewEntity3Adapter(IAdapterRegistry adapterRegistry, ILogger logger) : base(adapterRegistry, logger) { }

        public override Ellucian.Colleague.Dtos.Planning.DegreePlanPreview3 MapToType(Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview Source)
        {
            Ellucian.Colleague.Dtos.Planning.DegreePlanPreview3 degreePlanDto = new Ellucian.Colleague.Dtos.Planning.DegreePlanPreview3();
            var degreePlanEntityAdapter = adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan3>();
            degreePlanDto.Preview = degreePlanEntityAdapter.MapToType(Source.Preview);
            degreePlanDto.MergedDegreePlan = degreePlanEntityAdapter.MapToType(Source.MergedDegreePlan);
            return degreePlanDto;
        }
    }
}
