﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMapper;
using Ellucian.Colleague.Coordination.Base.Adapters;
using Ellucian.Web.Adapters;
using slf4net;

namespace Ellucian.Colleague.Coordination.Planning.Adapters
{
    public class DegreePlanApprovalEntityAdapter : AutoMapperAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApproval, Ellucian.Colleague.Dtos.Planning.DegreePlanApproval>
    {
        public DegreePlanApprovalEntityAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
        }

        public override Dtos.Planning.DegreePlanApproval MapToType(Domain.Planning.Entities.DegreePlanApproval Source)
        {
            var dpaDto = base.MapToType(Source);
            // for some unknown reason the DateTimeOffsetToDateTimeAdapter assigns a default DateTime
            // to the target object's Date property. So we still need to manually handle this mapping.
            dpaDto.Date = Source.Date.DateTime;
            return dpaDto;
        }
    }
}
