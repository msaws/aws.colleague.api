﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Ellucian.Colleague.Domain.Planning.Entities;
using Ellucian.Colleague.Dtos.Planning;
using slf4net;
using Ellucian.Web.Adapters;

namespace Ellucian.Colleague.Coordination.Planning.Adapters
{
    /// <summary>
    /// This class maps an inbound degree plan DTO to a degree plan entity
    /// </summary>
    public class DegreePlanDtoAdapter : BaseAdapter<Ellucian.Colleague.Dtos.Planning.DegreePlan, Ellucian.Colleague.Domain.Planning.Entities.DegreePlan>
    {
        public DegreePlanDtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger) : base(adapterRegistry, logger) { }

        // Since AutoMapper cannot be used here, inherit from BaseAdapter and override the MapToType method for custom mapping
        public override Ellucian.Colleague.Domain.Planning.Entities.DegreePlan MapToType(Ellucian.Colleague.Dtos.Planning.DegreePlan Source)
        {
            // You could add validation code here before passing the properties to the Degree Plan entity constructor
            var degreePlanEntity = new Ellucian.Colleague.Domain.Planning.Entities.DegreePlan(Source.Id, Source.PersonId, Source.Version, Source.ReviewRequested);
            degreePlanEntity.LastReviewedAdvisorId = Source.LastReviewedAdvisorId;
            degreePlanEntity.LastReviewedDate = Source.LastReviewedDate;
            if (Source.Terms != null)
            {
                foreach (var tc in Source.Terms)
                {
                    if (!String.IsNullOrEmpty(tc.TermId))
                    {
                        degreePlanEntity.AddTerm(tc.TermId);
                        if (tc.PlannedCourses != null && tc.PlannedCourses.Count() > 0)
                        {
                            foreach (var plannedcourse in tc.PlannedCourses)
                            {
                                if (!string.IsNullOrEmpty(plannedcourse.CourseId))
                                {
                                    Ellucian.Colleague.Domain.Student.Entities.GradingType gradingType = Domain.Student.Entities.GradingType.Graded;
                                    switch (plannedcourse.GradingType)
                                    {
                                        case Ellucian.Colleague.Dtos.Student.GradingType.PassFail:
                                            gradingType = Ellucian.Colleague.Domain.Student.Entities.GradingType.PassFail;
                                            break;
                                        case Ellucian.Colleague.Dtos.Student.GradingType.Audit:
                                            gradingType = Ellucian.Colleague.Domain.Student.Entities.GradingType.Audit;
                                            break;
                                        default:
                                            break;
                                    }
                                    Ellucian.Colleague.Domain.Planning.Entities.WaitlistStatus waitStatus = Domain.Planning.Entities.WaitlistStatus.NotWaitlisted;
                                    switch (plannedcourse.SectionWaitlistStatus)
                                    {
                                        case Ellucian.Colleague.Dtos.Planning.WaitlistStatus.NotWaitlisted:
                                            waitStatus = Ellucian.Colleague.Domain.Planning.Entities.WaitlistStatus.NotWaitlisted;
                                            break;
                                        case Ellucian.Colleague.Dtos.Planning.WaitlistStatus.Active:
                                            waitStatus = Ellucian.Colleague.Domain.Planning.Entities.WaitlistStatus.Active;
                                            break;
                                        case Ellucian.Colleague.Dtos.Planning.WaitlistStatus.PermissionToRegister:
                                            waitStatus = Ellucian.Colleague.Domain.Planning.Entities.WaitlistStatus.PermissionToRegister;
                                            break;
                                        default:
                                            break;
                                    }

                                    degreePlanEntity.AddCourse(new Ellucian.Colleague.Domain.Planning.Entities.PlannedCourse(plannedcourse.CourseId, plannedcourse.SectionId, gradingType, waitStatus) { Credits = plannedcourse.Credits }, tc.TermId);
                                }
                            }
                        }
                    }
                }
            }
            if (Source.NonTermPlannedCourses != null)
            {
                foreach (var ntpc in Source.NonTermPlannedCourses)
                {
                    if (!string.IsNullOrEmpty(ntpc.CourseId))
                    {
                        Ellucian.Colleague.Domain.Student.Entities.GradingType gradingType = Domain.Student.Entities.GradingType.Graded;
                        switch (ntpc.GradingType)
                        {
                            case Ellucian.Colleague.Dtos.Student.GradingType.PassFail:
                                gradingType = Ellucian.Colleague.Domain.Student.Entities.GradingType.PassFail;
                                break;
                            case Ellucian.Colleague.Dtos.Student.GradingType.Audit:
                                gradingType = Ellucian.Colleague.Domain.Student.Entities.GradingType.Audit;
                                break;
                            default:
                                break;
                        }
                        Ellucian.Colleague.Domain.Planning.Entities.WaitlistStatus waitStatus = Domain.Planning.Entities.WaitlistStatus.NotWaitlisted;
                        switch (ntpc.SectionWaitlistStatus)
                        {
                            case Ellucian.Colleague.Dtos.Planning.WaitlistStatus.NotWaitlisted:
                                waitStatus = Ellucian.Colleague.Domain.Planning.Entities.WaitlistStatus.NotWaitlisted;
                                break;
                            case Ellucian.Colleague.Dtos.Planning.WaitlistStatus.Active:
                                waitStatus = Ellucian.Colleague.Domain.Planning.Entities.WaitlistStatus.Active;
                                break;
                            case Ellucian.Colleague.Dtos.Planning.WaitlistStatus.PermissionToRegister:
                                waitStatus = Ellucian.Colleague.Domain.Planning.Entities.WaitlistStatus.PermissionToRegister;
                                break;
                            default:
                                break;
                        }

                        degreePlanEntity.AddCourse(new Ellucian.Colleague.Domain.Planning.Entities.PlannedCourse(ntpc.CourseId, ntpc.SectionId, gradingType, waitStatus) { Credits = ntpc.Credits }, null);
                    }
                }
            }
            // Map approvals back to entity
            var approvals = new List<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApproval>();
            if (Source.Approvals != null && Source.Approvals.Count() > 0)
            {
                // Add each approval for this term to the degree plan entity term approvals
                foreach (var approval in Source.Approvals)
                {
                    if (approval.Date == null)
                    {
                        approval.Date = DateTime.Now;
                    }
                    if (!string.IsNullOrEmpty(approval.PersonId) && !string.IsNullOrEmpty(approval.CourseId) && !string.IsNullOrEmpty(approval.TermCode))
                    {
                        try
                        {
                            approvals.Add(new Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApproval(approval.PersonId, ConvertStatus(approval.Status), approval.Date, approval.CourseId, approval.TermCode));
                        }
                        catch
                        {
                            // No action taken, approval simply skipped if an exception is generated
                        }
                    }
                }
            }
            degreePlanEntity.Approvals = approvals;

            // Move notes from the degree plan into the dto
            var notes = new List<Domain.Planning.Entities.DegreePlanNote>();
            if (Source.Notes != null && Source.Notes.Count() > 0)
            {
                foreach (var note in Source.Notes)
                {
                    if (note.Id == 0)
                    {
                        notes.Add(new Domain.Planning.Entities.DegreePlanNote(note.Text));
                    }
                    else
                    {
                        notes.Add(new Domain.Planning.Entities.DegreePlanNote(note.Id, note.PersonId, note.Date, note.Text));
                    }
                }
            }
            degreePlanEntity.Notes = notes;
            return degreePlanEntity;
        }

        private Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApprovalStatus ConvertStatus(Ellucian.Colleague.Dtos.Planning.DegreePlanApprovalStatus dtoStatus)
        {
            var status = Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApprovalStatus.Approved;
            switch (dtoStatus)
            {
                case Ellucian.Colleague.Dtos.Planning.DegreePlanApprovalStatus.Approved:
                    status = Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApprovalStatus.Approved;
                    break;
                case Ellucian.Colleague.Dtos.Planning.DegreePlanApprovalStatus.Denied:
                    status = Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApprovalStatus.Denied;
                    break;
            }
            return status;
        }
    }
}
