﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Resources;
using Ellucian.Colleague.Domain.Planning.Entities;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Base;
using slf4net;
using Ellucian.Web.Adapters;

namespace Ellucian.Colleague.Coordination.Planning.Adapters
{
    /// <summary>
    /// This class maps a degree plan entity to an outbound degree plan DTO
    /// </summary>

    public class DegreePlanEntityAdapter : BaseAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan>
    {
        public DegreePlanEntityAdapter(IAdapterRegistry adapterRegistry, ILogger logger) : base(adapterRegistry, logger) { }

        public override Ellucian.Colleague.Dtos.Planning.DegreePlan MapToType(Ellucian.Colleague.Domain.Planning.Entities.DegreePlan Source)
        {
            var degreePlanDto = new Ellucian.Colleague.Dtos.Planning.DegreePlan();
            degreePlanDto.Id = Source.Id;
            degreePlanDto.PersonId = Source.PersonId;
            degreePlanDto.Version = Source.Version;
            degreePlanDto.ReviewRequested = Source.ReviewRequested;
            degreePlanDto.LastReviewedAdvisorId = Source.LastReviewedAdvisorId;
            degreePlanDto.LastReviewedDate = Source.LastReviewedDate;

            degreePlanDto.Terms = new List<Ellucian.Colleague.Dtos.Planning.DegreePlanTerm>();
            degreePlanDto.NonTermPlannedCourses = new List<Ellucian.Colleague.Dtos.Planning.PlannedCourse>();
            // Move all approvals to the degree plan dto
            degreePlanDto.Approvals = new List<Ellucian.Colleague.Dtos.Planning.DegreePlanApproval>();
            var approvalDtoAdapter = adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApproval, Ellucian.Colleague.Dtos.Planning.DegreePlanApproval>();
            foreach (var approval in Source.Approvals)
            {
                degreePlanDto.Approvals.Add(approvalDtoAdapter.MapToType(approval));
            }
            foreach (var term in Source.TermIds)
            {
                Ellucian.Colleague.Dtos.Planning.DegreePlanTerm planTerm = new Ellucian.Colleague.Dtos.Planning.DegreePlanTerm();
                planTerm.TermId = term;
                planTerm.PlannedCourses = new List<Ellucian.Colleague.Dtos.Planning.PlannedCourse>();
                var plannedcourses = Source.GetPlannedCourses(term);
                if (plannedcourses != null)
                {
                    foreach (var plannedcourse in plannedcourses)
                    {
                        // Grading Type defaults to graded.
                        Ellucian.Colleague.Dtos.Student.GradingType gradingType = Dtos.Student.GradingType.Graded;
                        switch (plannedcourse.GradingType)
                        {
                            case Ellucian.Colleague.Domain.Student.Entities.GradingType.Graded:
                                gradingType = GradingType.Graded;
                                break;
                            case Ellucian.Colleague.Domain.Student.Entities.GradingType.PassFail:
                                gradingType = GradingType.PassFail;
                                break;
                            case Ellucian.Colleague.Domain.Student.Entities.GradingType.Audit:
                                gradingType = GradingType.Audit;
                                break;
                            default:
                                break;
                        }
                        // Convert the waitlist status
                        Ellucian.Colleague.Dtos.Planning.WaitlistStatus waitStatus = Dtos.Planning.WaitlistStatus.NotWaitlisted;
                        switch (plannedcourse.WaitlistedStatus)
                        {
                            case Domain.Planning.Entities.WaitlistStatus.NotWaitlisted:
                                waitStatus = Ellucian.Colleague.Dtos.Planning.WaitlistStatus.NotWaitlisted;
                                break;
                            case Ellucian.Colleague.Domain.Planning.Entities.WaitlistStatus.Active:
                                waitStatus = Ellucian.Colleague.Dtos.Planning.WaitlistStatus.Active;
                                break;
                            case Ellucian.Colleague.Domain.Planning.Entities.WaitlistStatus.PermissionToRegister:
                                waitStatus = Ellucian.Colleague.Dtos.Planning.WaitlistStatus.PermissionToRegister;
                                break;
                            default:
                                break;
                        }
                        var plannedCourseDto = new Ellucian.Colleague.Dtos.Planning.PlannedCourse() { CourseId = plannedcourse.CourseId, SectionId = plannedcourse.SectionId, GradingType = gradingType, TermId = term, Credits = plannedcourse.Credits, SectionWaitlistStatus = waitStatus };
                        plannedCourseDto.Warnings = new List<Ellucian.Colleague.Dtos.Planning.DegreePlanWarning>();
                        foreach (var warning in plannedcourse.Warnings)
                        {
                            Ellucian.Colleague.Dtos.Planning.DegreePlanWarning warningDto = new Dtos.Planning.DegreePlanWarning();

                            // Do all the other types of warnings.
                            switch (warning.Type)
                            {
                                case PlannedCourseWarningType.InvalidPlannedCredits:
                                    warningDto.Type = Dtos.Planning.DegreePlanWarningType.InvalidPlannedCredits;
                                    break;
                                case PlannedCourseWarningType.NegativePlannedCredits:
                                    warningDto.Type = Dtos.Planning.DegreePlanWarningType.NegativePlannedCredits;
                                    break;
                                case PlannedCourseWarningType.TimeConflict:
                                    warningDto.Type = Dtos.Planning.DegreePlanWarningType.TimeConflict;
                                    break;
                                case PlannedCourseWarningType.UnmetRequisite:
                                    if (warning.Requisite != null)
                                    {
                                        if (warning.Requisite.CompletionOrder == Domain.Student.Entities.RequisiteCompletionOrder.Previous)
                                        {
                                            // this is an unmet prereq
                                            warningDto.Type = Dtos.Planning.DegreePlanWarningType.PrerequisiteUnsatisfied;
                                            warningDto.RequirementCode = warning.Requisite.RequirementCode;
                                        }
                                        else
                                        {
                                            warningDto.Type = warning.Requisite.IsRequired == true ? Dtos.Planning.DegreePlanWarningType.CorequisiteRequiredCourse : Dtos.Planning.DegreePlanWarningType.CorequisiteOptionalCourse;
                                            // Pre-conversion data should have the coreq course filled in.
                                            if (!string.IsNullOrEmpty(warning.Requisite.CorequisiteCourseId))
                                            {
                                                warningDto.CourseId = warning.Requisite.CorequisiteCourseId;
                                                
                                            }
                                            else
                                            {
                                                // If there is no course will send the requirement, but this won't show up in Self-Service 2.2
                                                warningDto.RequirementCode = warning.Requisite.RequirementCode;
                                            }
                                        }
                                        
                                    }
                                    if (warning.SectionRequisite != null)
                                    {
                                            warningDto.SectionId = warning.SectionId;
                                            warningDto.Type = warning.SectionRequisite.IsRequired == true ? Dtos.Planning.DegreePlanWarningType.CorequisiteRequiredSection : Dtos.Planning.DegreePlanWarningType.CorequisiteOptionalSection;
                                    }
                                    break;
                                default:
                                    break;
                            }
                            warningDto.SectionId = warning.SectionId;
                            plannedCourseDto.Warnings.Add(warningDto);
                        }
                        planTerm.PlannedCourses.Add(plannedCourseDto);
                    }
                }

                degreePlanDto.Terms.Add(planTerm);
            }
            foreach (var pc in Source.NonTermPlannedCourses)
            {
                // Create planned course dto for each nonterm planned course
                // Grading type defaults to graded.
                Ellucian.Colleague.Dtos.Student.GradingType gradingType = Dtos.Student.GradingType.Graded;
                switch (pc.GradingType)
                {
                    case Ellucian.Colleague.Domain.Student.Entities.GradingType.Graded:
                        gradingType = GradingType.Graded;
                        break;
                    case Ellucian.Colleague.Domain.Student.Entities.GradingType.PassFail:
                        gradingType = GradingType.PassFail;
                        break;
                    case Ellucian.Colleague.Domain.Student.Entities.GradingType.Audit:
                        gradingType = GradingType.Audit;
                        break;
                    default:
                        break;
                }
                // Convert the waitlist status
                Ellucian.Colleague.Dtos.Planning.WaitlistStatus waitStatus = Dtos.Planning.WaitlistStatus.NotWaitlisted;
                switch (pc.WaitlistedStatus)
                {
                    case Domain.Planning.Entities.WaitlistStatus.NotWaitlisted:
                        waitStatus = Ellucian.Colleague.Dtos.Planning.WaitlistStatus.NotWaitlisted;
                        break;
                    case Ellucian.Colleague.Domain.Planning.Entities.WaitlistStatus.Active:
                        waitStatus = Ellucian.Colleague.Dtos.Planning.WaitlistStatus.Active;
                        break;
                    case Ellucian.Colleague.Domain.Planning.Entities.WaitlistStatus.PermissionToRegister:
                        waitStatus = Ellucian.Colleague.Dtos.Planning.WaitlistStatus.PermissionToRegister;
                        break;
                    default:
                        break;
                }
                var plannedCourseDto = new Ellucian.Colleague.Dtos.Planning.PlannedCourse() { CourseId = pc.CourseId, SectionId = pc.SectionId, GradingType = gradingType, Credits = pc.Credits, SectionWaitlistStatus = waitStatus };
                // Add empty warning list
                plannedCourseDto.Warnings = new List<Ellucian.Colleague.Dtos.Planning.DegreePlanWarning>();
                // For each warning associated with this nonterm course
                foreach (var warning in pc.Warnings)
                {
                    Ellucian.Colleague.Dtos.Planning.DegreePlanWarning warningDto = new Dtos.Planning.DegreePlanWarning();

                    // Do all the other types of warnings.
                    switch (warning.Type)
                    {
                        case PlannedCourseWarningType.InvalidPlannedCredits:
                            warningDto.Type = Dtos.Planning.DegreePlanWarningType.InvalidPlannedCredits;
                            break;
                        case PlannedCourseWarningType.NegativePlannedCredits:
                            warningDto.Type = Dtos.Planning.DegreePlanWarningType.NegativePlannedCredits;
                            break;
                        case PlannedCourseWarningType.TimeConflict:
                            warningDto.Type = Dtos.Planning.DegreePlanWarningType.TimeConflict;
                            break;
                        case PlannedCourseWarningType.UnmetRequisite:
                            if (warning.Requisite != null)
                            {
                                if (warning.Requisite.CompletionOrder == Domain.Student.Entities.RequisiteCompletionOrder.Previous)
                                {
                                    // this is an unmet prereq
                                    warningDto.Type = Dtos.Planning.DegreePlanWarningType.PrerequisiteUnsatisfied;
                                    warningDto.RequirementCode = warning.Requisite.RequirementCode;
                                }
                                // Now for anything with a coreq course - convert it into appropriate coreq.
                                if (!string.IsNullOrEmpty(warning.Requisite.CorequisiteCourseId))
                                {
                                    warningDto.CourseId = warning.Requisite.CorequisiteCourseId;
                                    warningDto.Type = warning.Requisite.IsRequired == true ? Dtos.Planning.DegreePlanWarningType.CorequisiteRequiredCourse : Dtos.Planning.DegreePlanWarningType.CorequisiteOptionalCourse;
                                }
                            }
                            if (warning.SectionRequisite != null)
                            {
                                warningDto.SectionId = warning.SectionId;
                                warningDto.Type = warning.SectionRequisite.IsRequired == true ? Dtos.Planning.DegreePlanWarningType.CorequisiteRequiredSection : Dtos.Planning.DegreePlanWarningType.CorequisiteOptionalSection;
                            }
                            break;
                        default:
                            break;
                    }
                    warningDto.SectionId = warning.SectionId;
                    plannedCourseDto.Warnings.Add(warningDto);
                }
                // Add planned course dto to the list of nonterm planned courses
                degreePlanDto.NonTermPlannedCourses.Add(plannedCourseDto);
            }
            // Move notes into dto
            degreePlanDto.Notes = new List<Dtos.Planning.DegreePlanNote>();
            foreach (var note in Source.Notes)
            {
                degreePlanDto.Notes.Add(new Dtos.Planning.DegreePlanNote()
                {
                    Id = note.Id,
                    PersonId = note.PersonId,
                    Date = note.Date.GetValueOrDefault().DateTime,
                    Text = note.Text,
                    PersonType = (Source.PersonId == note.PersonId ? PersonType.Student : PersonType.Advisor)
                });
            }
            return degreePlanDto;
        }

    }
}
