﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Resources;
using Ellucian.Colleague.Domain.Planning.Entities;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Base;
using slf4net;
using Ellucian.Web.Adapters;

namespace Ellucian.Colleague.Coordination.Planning.Adapters
{
    /// <summary>
    /// This class maps a degree plan entity to an outbound degree plan DTO
    /// </summary>

    public class DegreePlanEntity3Adapter : BaseAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan3>
    {
        public DegreePlanEntity3Adapter(IAdapterRegistry adapterRegistry, ILogger logger) : base(adapterRegistry, logger) { }

        public override Ellucian.Colleague.Dtos.Planning.DegreePlan3 MapToType(Ellucian.Colleague.Domain.Planning.Entities.DegreePlan Source)
        {
            var degreePlanDto = new Ellucian.Colleague.Dtos.Planning.DegreePlan3();
            degreePlanDto.Id = Source.Id;
            degreePlanDto.PersonId = Source.PersonId;
            degreePlanDto.Version = Source.Version;
            degreePlanDto.ReviewRequested = Source.ReviewRequested;
            degreePlanDto.LastReviewedAdvisorId = Source.LastReviewedAdvisorId;
            degreePlanDto.LastReviewedDate = Source.LastReviewedDate;

            degreePlanDto.Terms = new List<Ellucian.Colleague.Dtos.Planning.DegreePlanTerm3>();
            degreePlanDto.NonTermPlannedCourses = new List<Ellucian.Colleague.Dtos.Planning.PlannedCourse3>();
            // Move all approvals to the degree plan dto
            degreePlanDto.Approvals = new List<Ellucian.Colleague.Dtos.Planning.DegreePlanApproval2>();
            var approvalDtoAdapter = adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApproval, Ellucian.Colleague.Dtos.Planning.DegreePlanApproval2>();
            foreach (var approval in Source.Approvals)
            {
                degreePlanDto.Approvals.Add(approvalDtoAdapter.MapToType(approval));
            }
            foreach (var term in Source.TermIds)
            {
                Ellucian.Colleague.Dtos.Planning.DegreePlanTerm3 planTerm = new Ellucian.Colleague.Dtos.Planning.DegreePlanTerm3();
                planTerm.TermId = term;
                planTerm.PlannedCourses = new List<Ellucian.Colleague.Dtos.Planning.PlannedCourse3>();
                var plannedcourses = Source.GetPlannedCourses(term);
                if (plannedcourses != null)
                {
                    foreach (var plannedcourse in plannedcourses)
                    {
                        // Grading Type defaults to graded.
                        Ellucian.Colleague.Dtos.Student.GradingType gradingType = Dtos.Student.GradingType.Graded;
                        switch (plannedcourse.GradingType)
                        {
                            case Ellucian.Colleague.Domain.Student.Entities.GradingType.Graded:
                                gradingType = GradingType.Graded;
                                break;
                            case Ellucian.Colleague.Domain.Student.Entities.GradingType.PassFail:
                                gradingType = GradingType.PassFail;
                                break;
                            case Ellucian.Colleague.Domain.Student.Entities.GradingType.Audit:
                                gradingType = GradingType.Audit;
                                break;
                            default:
                                break;
                        }
                        // Convert the waitlist status
                        Ellucian.Colleague.Dtos.Planning.WaitlistStatus waitStatus = Dtos.Planning.WaitlistStatus.NotWaitlisted;
                        switch (plannedcourse.WaitlistedStatus)
                        {
                            case Domain.Planning.Entities.WaitlistStatus.NotWaitlisted:
                                waitStatus = Ellucian.Colleague.Dtos.Planning.WaitlistStatus.NotWaitlisted;
                                break;
                            case Ellucian.Colleague.Domain.Planning.Entities.WaitlistStatus.Active:
                                waitStatus = Ellucian.Colleague.Dtos.Planning.WaitlistStatus.Active;
                                break;
                            case Ellucian.Colleague.Domain.Planning.Entities.WaitlistStatus.PermissionToRegister:
                                waitStatus = Ellucian.Colleague.Dtos.Planning.WaitlistStatus.PermissionToRegister;
                                break;
                            default:
                                break;
                        }

                        var plannedCourseDto = new Ellucian.Colleague.Dtos.Planning.PlannedCourse3() 
                        { 
                            CourseId = plannedcourse.CourseId, 
                            SectionId = plannedcourse.SectionId, 
                            GradingType = gradingType, 
                            TermId = term, 
                            Credits = plannedcourse.Credits, 
                            SectionWaitlistStatus = waitStatus, 
                            AddedBy = plannedcourse.AddedBy,
                            AddedOn = plannedcourse.AddedOn,
                        };
                        plannedCourseDto.Warnings = new List<Ellucian.Colleague.Dtos.Planning.PlannedCourseWarning2>();
                        foreach (var warning in plannedcourse.Warnings)
                        {
                            var plannedCourseWarningDtoAdapter = adapterRegistry.GetAdapter<PlannedCourseWarning, Ellucian.Colleague.Dtos.Planning.PlannedCourseWarning2>();
                            var warningDto = plannedCourseWarningDtoAdapter.MapToType(warning);
                            plannedCourseDto.Warnings.Add(warningDto);
                        }
                        planTerm.PlannedCourses.Add(plannedCourseDto);
                    }
                }

                degreePlanDto.Terms.Add(planTerm);
            }
            foreach (var pc in Source.NonTermPlannedCourses)
            {
                // Create planned course dto for each nonterm planned course
                // Grading type defaults to graded.
                Ellucian.Colleague.Dtos.Student.GradingType gradingType = Dtos.Student.GradingType.Graded;
                switch (pc.GradingType)
                {
                    case Ellucian.Colleague.Domain.Student.Entities.GradingType.Graded:
                        gradingType = GradingType.Graded;
                        break;
                    case Ellucian.Colleague.Domain.Student.Entities.GradingType.PassFail:
                        gradingType = GradingType.PassFail;
                        break;
                    case Ellucian.Colleague.Domain.Student.Entities.GradingType.Audit:
                        gradingType = GradingType.Audit;
                        break;
                    default:
                        break;
                }
                // Convert the waitlist status
                Ellucian.Colleague.Dtos.Planning.WaitlistStatus waitStatus = Dtos.Planning.WaitlistStatus.NotWaitlisted;
                switch (pc.WaitlistedStatus)
                {
                    case Domain.Planning.Entities.WaitlistStatus.NotWaitlisted:
                        waitStatus = Ellucian.Colleague.Dtos.Planning.WaitlistStatus.NotWaitlisted;
                        break;
                    case Ellucian.Colleague.Domain.Planning.Entities.WaitlistStatus.Active:
                        waitStatus = Ellucian.Colleague.Dtos.Planning.WaitlistStatus.Active;
                        break;
                    case Ellucian.Colleague.Domain.Planning.Entities.WaitlistStatus.PermissionToRegister:
                        waitStatus = Ellucian.Colleague.Dtos.Planning.WaitlistStatus.PermissionToRegister;
                        break;
                    default:
                        break;
                }

                var plannedCourseDto = new Ellucian.Colleague.Dtos.Planning.PlannedCourse3() 
                { 
                    CourseId = pc.CourseId, 
                    SectionId = pc.SectionId, 
                    GradingType = gradingType, 
                    Credits = pc.Credits, 
                    SectionWaitlistStatus = waitStatus, 
                    AddedBy = pc.AddedBy, 
                    AddedOn = pc.AddedOn,
                };
                // Add empty warning list
                plannedCourseDto.Warnings = new List<Ellucian.Colleague.Dtos.Planning.PlannedCourseWarning2>();
                // For each warning associated with this nonterm course
                foreach (var warning in pc.Warnings)
                {
                    var degreePlanWarningDtoAdapter = adapterRegistry.GetAdapter<PlannedCourseWarning, Ellucian.Colleague.Dtos.Planning.PlannedCourseWarning2>();
                    var warningDto = degreePlanWarningDtoAdapter.MapToType(warning);
                    plannedCourseDto.Warnings.Add(warningDto);
                }
                // Add planned course dto to the list of nonterm planned courses
                degreePlanDto.NonTermPlannedCourses.Add(plannedCourseDto);
            }
            // Move notes into dto
            degreePlanDto.Notes = new List<Dtos.Planning.DegreePlanNote2>();
            foreach (var note in Source.Notes)
            {
                degreePlanDto.Notes.Add(new Dtos.Planning.DegreePlanNote2()
                {
                    Id = note.Id,
                    PersonId = note.PersonId,
                    Date = note.Date.GetValueOrDefault(),
                    Text = note.Text,
                    PersonType = (Source.PersonId == note.PersonId ? PersonType.Student : PersonType.Advisor)
                });
            }
            return degreePlanDto;
        }

    }
}
