﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Planning.Entities;
using Ellucian.Web.Adapters;
using slf4net;

namespace Ellucian.Colleague.Coordination.Planning.Adapters
{
    public class DegreePlanPreviewEntityAdapter : BaseAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview, Ellucian.Colleague.Dtos.Planning.DegreePlanPreview>
    {
        public DegreePlanPreviewEntityAdapter(IAdapterRegistry adapterRegistry, ILogger logger) : base(adapterRegistry, logger) { }
        
        public override Ellucian.Colleague.Dtos.Planning.DegreePlanPreview MapToType(Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview Source)
        {
            Ellucian.Colleague.Dtos.Planning.DegreePlanPreview degreePlanDto = new Ellucian.Colleague.Dtos.Planning.DegreePlanPreview();
            var degreePlanEntityAdapter = adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan>();
            degreePlanDto.Preview = degreePlanEntityAdapter.MapToType(Source.Preview);
            degreePlanDto.MergedDegreePlan = degreePlanEntityAdapter.MapToType(Source.MergedDegreePlan);
            return degreePlanDto;
        }
    }
}



