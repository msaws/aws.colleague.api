﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Planning.Entities;
using slf4net;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Web.Adapters;

namespace Ellucian.Colleague.Coordination.Planning.Adapters
{
    public class PlannedCourseWarningEntity2Adaptor : AutoMapperAdapter<PlannedCourseWarning, Ellucian.Colleague.Dtos.Planning.PlannedCourseWarning2>
    {
        public PlannedCourseWarningEntity2Adaptor(IAdapterRegistry adapterRegistry, ILogger logger) 
            : base(adapterRegistry, logger)
        {
            // Mapping dependency
            AddMappingDependency<Requisite, Ellucian.Colleague.Dtos.Student.Requisite>();
            AddMappingDependency<SectionRequisite, Ellucian.Colleague.Dtos.Student.SectionRequisite>();
        }
    }
}
