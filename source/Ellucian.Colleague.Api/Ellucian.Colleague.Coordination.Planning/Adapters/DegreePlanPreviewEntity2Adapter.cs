﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Planning.Entities;
using Ellucian.Web.Adapters;
using slf4net;

namespace Ellucian.Colleague.Coordination.Planning.Adapters
{
    public class DegreePlanPreviewEntity2Adapter : BaseAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview, Ellucian.Colleague.Dtos.Planning.DegreePlanPreview2>
    {
        public DegreePlanPreviewEntity2Adapter(IAdapterRegistry adapterRegistry, ILogger logger) : base(adapterRegistry, logger) { }

        public override Ellucian.Colleague.Dtos.Planning.DegreePlanPreview2 MapToType(Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview Source)
        {
            Ellucian.Colleague.Dtos.Planning.DegreePlanPreview2 degreePlanDto = new Ellucian.Colleague.Dtos.Planning.DegreePlanPreview2();
            var degreePlanEntityAdapter = adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan2>();
            degreePlanDto.Preview = degreePlanEntityAdapter.MapToType(Source.Preview);
            degreePlanDto.MergedDegreePlan = degreePlanEntityAdapter.MapToType(Source.MergedDegreePlan);
            return degreePlanDto;
        }
    }
}
