﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Planning.Services
{
    public interface IPlanningStudentService
    {
        Task<Dtos.Planning.PlanningStudent> GetPlanningStudentAsync(string studentId);
        Task<IEnumerable<Ellucian.Colleague.Dtos.Planning.PlanningStudent>> QueryPlanningStudentsAsync(IEnumerable<string> studentIds);
    }
}
