﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Coordination.Planning.Reports;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Dtos.Student;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Planning.Services
{
    public interface IDegreePlanService
    {

        Task<DegreePlan2> GetDegreePlan2Async(int id);
        Task<DegreePlan2> CreateDegreePlan2Async(string personId);
        Task<DegreePlan2> UpdateDegreePlan2Async(DegreePlan2 degreePlan);
        Task<DegreePlanPreview2> PreviewSampleDegreePlan2Async(int degreePlanId, string programCode, string firstTermCode);

        [Obsolete("Obsolete on version 1.6 of the Api. Use GetDegreePlan4 going forward.")]        
        Task<DegreePlan3> GetDegreePlan3Async(int id);
        [Obsolete("Obsolete on version 1.6 of the Api. Use CreateDegreePlan4 going forward.")]
        Task<DegreePlan3> CreateDegreePlan3Async(string personId);
        [Obsolete("Obsolete on version 1.6 of the Api. Use UpdateDegreePlan4 going forward.")]
        Task<DegreePlan3> UpdateDegreePlan3Async(DegreePlan3 degreePlan);
        [Obsolete("Obsolete on version 1.6 of the Api. Use UpdateDegreePlan4 going forward.")]
        Task<DegreePlanPreview3> PreviewSampleDegreePlan3Async(int degreePlanId, string programCode, string firstTermCode);

        [Obsolete("Obsolete on version 1.11 of the Api. Use GetDegreePlan5Async going forward.")]
        Task<DegreePlanAcademicHistory> GetDegreePlan4Async(int id, bool validate = true);
        [Obsolete("Obsolete on version 1.11 of the Api. Use CreateDegreePlan5Async going forward.")]
        Task<DegreePlanAcademicHistory> CreateDegreePlan4Async(string personId);
        [Obsolete("Obsolete on version 1.11 of the Api. Use UpdateDegreePlan5Async going forward.")]
        Task<DegreePlanAcademicHistory> UpdateDegreePlan4Async(DegreePlan4 degreePlan);
        [Obsolete("Obsolete on version 1.11 of the Api. Use PreviewSampleDegreePlan5Async going forward.")]
        Task<DegreePlanPreview4> PreviewSampleDegreePlan4Async(int degreePlanId, string programCode, string firstTermCode);

        Task<DegreePlanAcademicHistory2> GetDegreePlan5Async(int id, bool validate = true);
        Task<DegreePlanAcademicHistory2> CreateDegreePlan5Async(string personId);
        Task<DegreePlanAcademicHistory2> UpdateDegreePlan5Async(DegreePlan4 degreePlan);
        Task<DegreePlanPreview5> PreviewSampleDegreePlan5Async(int degreePlanId, string programCode, string firstTermCode);

        Task<bool> CheckForSampleAsync(string programCode, string catalog);
        Task<RegistrationResponse> RegisterAsync(int degreePlanId, string termId);
        Task<RegistrationResponse> RegisterSectionsAsync(int degreePlanId, IEnumerable<SectionRegistration> sectionRegistrations);

        [Obsolete("Obsolete on version 1.5 of the Api. Use ArchiveDegreePlan2 going forward.")]
        Task<DegreePlanArchive> ArchiveDegreePlanAsync(DegreePlan2 degreePlan);
        [Obsolete("Obsolete on version 1.7 of the Api. Use ArchiveDegreePlan3 going forward.")]
        Task<DegreePlanArchive2> ArchiveDegreePlan2Async(DegreePlan3 degreePlan);
        Task<DegreePlanArchive2> ArchiveDegreePlan3Async(DegreePlan4 degreePlan);
        Task<IEnumerable<DegreePlanArchive>> GetDegreePlanArchivesAsync(int degreePlanId);
        Task<IEnumerable<DegreePlanArchive2>> GetDegreePlanArchives2Async(int degreePlanId);
        Task<DegreePlanArchiveReport> GetDegreePlanArchiveReportAsync(int archiveId);
        Byte[] GenerateDegreePlanArchiveReport(DegreePlanArchiveReport archiveReport, string path, string reportLogoPath);

        // Prior versions - used with version 1 endpoints
        Task<DegreePlan> GetDegreePlanAsync(int id);
        Task<DegreePlan> CreateDegreePlanAsync(string personId);
        Task<DegreePlan> UpdateDegreePlanAsync(DegreePlan degreePlan);
        Task<DegreePlanPreview> PreviewSampleDegreePlanAsync(int degreePlanId, string programCode);
        [Obsolete("Deprecated on version 1.2 of the Api. Use PreviewSampleDegreePlan along with the UpdateDegreePlan methods going forward.")]
        Task<DegreePlan> ApplySampleDegreePlanAsync(string studentId, string programCode);



    }
}
