﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Coordination.Planning.Reports;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Planning.Entities;
using Ellucian.Colleague.Domain.Planning.Repositories;
using Ellucian.Colleague.Domain.Planning.Services;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Student;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Entities.Requirements;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using Microsoft.Reporting.WebForms;
using slf4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Planning.Services
{
    [RegisterType]
    public class DegreePlanService : StudentCoordinationService, IDegreePlanService
    {
        private readonly IDegreePlanRepository _degreePlanRepository;
        private readonly ITermRepository _termRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly IStudentProgramRepository _studentProgramRepository;
        private readonly IPlanningStudentRepository _planningStudentRepository;
        private readonly IProgramRepository _programRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly ISectionRepository _sectionRepository;
        private readonly IAcademicCreditRepository _academicCreditRepository;
        private readonly IRequirementRepository _requirementRepository;
        private readonly IRuleRepository _ruleRepository;
        private readonly IProgramRequirementsRepository _programRequirementsRepository;
        private readonly ISampleDegreePlanRepository _curriculumTrackRepository;
        private readonly IPlanningConfigurationRepository _planningConfigurationRepository;
        private readonly ICatalogRepository _catalogRepository;
        private readonly IDegreePlanArchiveRepository _degreePlanArchiveRepository;
        private readonly IAdvisorRepository _advisorRepository;
        private readonly IGradeRepository _gradeRepository;
        private readonly IAcademicHistoryService _academicHistoryService;
        private readonly IConfigurationRepository _configurationRepository;

        public DegreePlanService(IAdapterRegistry adapterRegistry, IDegreePlanRepository degreePlanRepository, ITermRepository termRepository, IStudentRepository studentRepository, IPlanningStudentRepository planningStudentRepository,
            IStudentProgramRepository studentProgramRepository, ICourseRepository courseRepository, ISectionRepository sectionRepository, IProgramRepository programRepository,
            IAcademicCreditRepository academicCreditRepository, IRequirementRepository requirementRepository, IRuleRepository ruleRepository, IProgramRequirementsRepository programRequirementsRepository,
            ISampleDegreePlanRepository curriculumTrackRepository, IPlanningConfigurationRepository planningConfigurationRepository, ICatalogRepository catalogRepository,
            IDegreePlanArchiveRepository degreePlanArchiveRepository, IAdvisorRepository advisorRepository, IGradeRepository gradeRepository, IAcademicHistoryService academicHistoryService,
            ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger,
            IConfigurationRepository configurationRepository)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger, studentRepository, configurationRepository)
        {
            _configurationRepository = configurationRepository;
            _degreePlanRepository = degreePlanRepository;
            _termRepository = termRepository;
            _studentRepository = studentRepository;
            _planningStudentRepository = planningStudentRepository;
            _studentProgramRepository = studentProgramRepository;
            _courseRepository = courseRepository;
            _sectionRepository = sectionRepository;
            _programRepository = programRepository;
            _academicCreditRepository = academicCreditRepository;
            _ruleRepository = ruleRepository;
            _requirementRepository = requirementRepository;
            _programRequirementsRepository = programRequirementsRepository;
            _curriculumTrackRepository = curriculumTrackRepository;
            _planningConfigurationRepository = planningConfigurationRepository;
            _catalogRepository = catalogRepository;
            _degreePlanArchiveRepository = degreePlanArchiveRepository;
            _advisorRepository = advisorRepository;
            _gradeRepository = gradeRepository;
            _academicHistoryService = academicHistoryService;
        }


        /// <summary>
        /// Creates a new degree plan for a student unless one already exists.
        /// </summary>
        /// <param name="studentId">Id of student for whom new plan is to be created</param>
        /// <returns>A DegreePlan2 Dto</returns>
        [Obsolete("Obsolete")]
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlan2> CreateDegreePlan2Async(string studentId)
        {
            if (string.IsNullOrEmpty(studentId))
            {
                throw new ArgumentNullException(studentId, "Must specify a student Id to create a new plan.");
            }

            // Get student from repository
            Ellucian.Colleague.Domain.Student.Entities.Student student = await _studentRepository.GetAsync(studentId);

            // Verify current user has the permissions to create a degree plan
            await CheckCreatePlanPermissionsAsync(student); 

            // Get data needed to build a degree plan
            var planningTerms = await _termRepository.GetAsync(); 
            var studentPrograms = await _studentProgramRepository.GetAsync(studentId); 
            var programs = await _programRepository.GetAsync(); 

            // Create a degree plan for a student - business logic resides in the domain
            var degreePlan = Ellucian.Colleague.Domain.Planning.Entities.DegreePlan.CreateDegreePlan(student, studentPrograms, planningTerms, programs);

            // Have Repository do the add - could throw exception if student already has a plan.
            var updatedDegreePlan = await _degreePlanRepository.AddAsync(degreePlan);

            // Map the degree plan to a DTO 
            var newDegreePlanDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan2>();
            var newPlanDto = newDegreePlanDtoAdapter.MapToType(updatedDegreePlan);
            return newPlanDto;
        }

        //TODO
        /// <summary>
        /// Creates a new degree plan for a student unless one already exists.
        /// </summary>
        /// <param name="studentId">Id of student for whom new plan is to be created</param>
        /// <returns>A DegreePlan3 Dto</returns>
        [Obsolete("Obsolete as of API 1.6. Use CreateDegreePlan4.")]
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlan3> CreateDegreePlan3Async(string studentId)
        {
            if (string.IsNullOrEmpty(studentId))
            {
                throw new ArgumentNullException(studentId, "Must specify a student Id to create a new plan.");
            }

            // Get student from repository
            Ellucian.Colleague.Domain.Student.Entities.Student student = await _studentRepository.GetAsync(studentId);

            // Verify current user has the permissions to create a degree plan
            await CheckCreatePlanPermissionsAsync(student); //TODO

            // Get data needed to build a degree plan
            var planningTerms = await _termRepository.GetAsync(); //TODO
            var studentPrograms = await _studentProgramRepository.GetAsync(studentId); //TODO
            var programs = await _programRepository.GetAsync();

            // Create a degree plan for a student - business logic resides in the domain
            var degreePlan = Ellucian.Colleague.Domain.Planning.Entities.DegreePlan.CreateDegreePlan(student, studentPrograms, planningTerms, programs);

            // Have Repository do the add - could throw exception if student already has a plan.
            var updatedDegreePlan = await _degreePlanRepository.AddAsync(degreePlan);

            // Map the degree plan to a DTO 
            var newDegreePlanDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan3>();
            var newPlanDto = newDegreePlanDtoAdapter.MapToType(updatedDegreePlan);
            return newPlanDto;
        }

        //TODO
        /// <summary>
        /// Creates a new degree plan for a student unless one already exists.
        /// </summary>
        /// <param name="studentId">Id of student for whom new plan is to be created</param>
        /// <returns>A DegreePlanAcademicHistory Dto which includes combination of DegreePlan and AcademicHistory</returns>
        [Obsolete("Obsolete as of API 1.11. Use CreateDegreePlan5Async.")]
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlanAcademicHistory> CreateDegreePlan4Async(string studentId)
        {
            if (string.IsNullOrEmpty(studentId))
            {
                throw new ArgumentNullException(studentId, "Must specify a student Id to create a new plan.");
            }

            // Get student from repository
            Ellucian.Colleague.Domain.Student.Entities.Student student = await _studentRepository.GetAsync(studentId);

            // Verify current user has the permissions to create a degree plan
            await CheckCreatePlanPermissionsAsync(student);

            // Get data needed to build a degree plan
            var planningTerms = await _termRepository.GetAsync(); //TODO
            var studentPrograms = await _studentProgramRepository.GetAsync(studentId); //TODO
            var programs = await _programRepository.GetAsync();

            // Create a degree plan for a student - business logic resides in the domain
            var degreePlan = Ellucian.Colleague.Domain.Planning.Entities.DegreePlan.CreateDegreePlan(student, studentPrograms, planningTerms, programs);

            // Have Repository do the add - could throw exception if student already has a plan.
            var updatedDegreePlan = await _degreePlanRepository.AddAsync(degreePlan);

            // Object to return
            var degreePlanAcademicHistoryDto = new DegreePlanAcademicHistory();

            // Map the degree plan to a DTO 
            var newDegreePlanDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan4>();
            degreePlanAcademicHistoryDto.DegreePlan = newDegreePlanDtoAdapter.MapToType(updatedDegreePlan);

            // Add the academic history dto
            var studentAcademicCredits = await _academicCreditRepository.GetAsync(student.AcademicCreditIds);
            degreePlanAcademicHistoryDto.AcademicHistory =await _academicHistoryService.ConvertAcademicCreditsToAcademicHistoryDtoAsync(studentId, studentAcademicCredits, student);

            return degreePlanAcademicHistoryDto;
        }

        /// <summary>
        /// Creates a new degree plan for a student unless one already exists.
        /// </summary>
        /// <param name="studentId">Id of student for whom new plan is to be created</param>
        /// <returns>A DegreePlanAcademicHistory Dto which includes combination of DegreePlan and AcademicHistory</returns>
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlanAcademicHistory2> CreateDegreePlan5Async(string studentId)
        {
            if (string.IsNullOrEmpty(studentId))
            {
                throw new ArgumentNullException(studentId, "Must specify a student Id to create a new plan.");
            }

            // Get student from repository
            Ellucian.Colleague.Domain.Student.Entities.Student student = await _studentRepository.GetAsync(studentId);

            // Verify current user has the permissions to create a degree plan
            await CheckCreatePlanPermissionsAsync(student);

            // Get data needed to build a degree plan
            var planningTerms = await _termRepository.GetAsync(); 
            var studentPrograms = await _studentProgramRepository.GetAsync(studentId); 
            var programs = await _programRepository.GetAsync();

            // Create a degree plan for a student - business logic resides in the domain
            var degreePlan = Ellucian.Colleague.Domain.Planning.Entities.DegreePlan.CreateDegreePlan(student, studentPrograms, planningTerms, programs);

            // Have Repository do the add - could throw exception if student already has a plan.
            var updatedDegreePlan = await _degreePlanRepository.AddAsync(degreePlan);

            // Object to return
            var degreePlanAcademicHistoryDto = new DegreePlanAcademicHistory2();

            // Map the degree plan to a DTO 
            var newDegreePlanDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan4>();
            degreePlanAcademicHistoryDto.DegreePlan = newDegreePlanDtoAdapter.MapToType(updatedDegreePlan);

            // Add the academic history dto
            var studentAcademicCredits = await _academicCreditRepository.GetAsync(student.AcademicCreditIds);
            degreePlanAcademicHistoryDto.AcademicHistory = await _academicHistoryService.ConvertAcademicCreditsToAcademicHistoryDto2Async(studentId, studentAcademicCredits, student);

            return degreePlanAcademicHistoryDto;
        }
        //TODO
        /// <summary>
        /// Get a course plan by ID number
        /// </summary>
        /// <param name="id">id of plan to retrieve</param>
        /// <returns>A DegreePlan2 DTO</returns>
        [Obsolete("Obsolete as of API 1.5. Use GetDegreePlan3.")]
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlan2> GetDegreePlan2Async(int id)
        {
            // Get the degree plan entity with an ID of id.
            var degreePlan = await _degreePlanRepository.GetAsync(id);

            if (!UserIsSelf(degreePlan.PersonId))
            {
                // Make sure user has permissions to view this degree plan. 
                // If not, an PermissionsException will be thrown.
                await CheckViewPlanPermissionsAsync(degreePlan.PersonId);
            }

            // Check for conflicts in degree plan
            degreePlan = await CheckForConflictsInDegreePlanAsync(degreePlan);

            // Get the right adapter for the type mapping
            var degreePlanDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan2>();

            // Map the degree plan entity to the degree plan DTO
            var degreePlanDto = degreePlanDtoAdapter.MapToType(degreePlan);

            return degreePlanDto;
        }

        //TODO
        /// <summary>
        /// Get a course plan by ID number
        /// </summary>
        /// <param name="id">id of plan to retrieve</param>
        /// <returns>A DegreePlan3 DTO</returns>
        [Obsolete("Obsolete as of API 1.6. Use GetDegreePlan4.")]
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlan3> GetDegreePlan3Async(int id)
        {
            // Get the degree plan entity with an ID of id.
            var degreePlan = await _degreePlanRepository.GetAsync(id);

            if (!UserIsSelf(degreePlan.PersonId))
            {
                // Make sure user has permissions to view this degree plan. 
                // If not, an PermissionsException will be thrown.
                await CheckViewPlanPermissionsAsync(degreePlan.PersonId); //TODO
            }

            // Check for conflicts in degree plan
            degreePlan = await CheckForConflictsInDegreePlanAsync(degreePlan);

            // Get the right adapter for the type mapping
            var degreePlanDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan3>();

            // Map the degree plan entity to the degree plan DTO
            var degreePlanDto = degreePlanDtoAdapter.MapToType(degreePlan);

            return degreePlanDto;
        }

        //TODO
        /// <summary>
        /// Get a course plan by ID number
        /// </summary>
        /// <param name="id">id of plan to retrieve</param>
        /// <param name="validate">true returns a validated degree plan, false does not validate</param>
        /// <returns>A combined dto containing DegreePlan3 and AcademicHistory2 DTO</returns>
        [Obsolete("Obsolete as of API 1.11. Use GetDegreePlan5Async")]
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlanAcademicHistory> GetDegreePlan4Async(int id, bool validate = true)
        {
            Stopwatch watch = null;
            if (logger.IsInfoEnabled)
            {
                watch = new Stopwatch();
                watch.Start();
            }

            // Get the degree plan entity with an ID of id.
            var degreePlan = await _degreePlanRepository.GetAsync(id);
            if (logger.IsInfoEnabled)
            {
                watch.Stop();
                logger.Info("DegreePlan Timing: (GetDegreePlan) _degreePlanRepository.Get completed in " + watch.ElapsedMilliseconds.ToString() + " ms");
            }

            // Make sure user has permissions to view this degree plan. 
            // If not, an PermissionsException will be thrown.
            await CheckViewPlanPermissionsAsync(degreePlan.PersonId);

            if (logger.IsInfoEnabled)
            {
                watch.Restart();
            }

            // Get the academic credits for the given student
            var studentAcademicCredits = new List<AcademicCredit>();
            var creditsByStudentDict =await _academicCreditRepository.GetAcademicCreditByStudentIdsAsync(new List<string>() { degreePlan.PersonId });
            if (creditsByStudentDict.ContainsKey(degreePlan.PersonId))
            {
                studentAcademicCredits = creditsByStudentDict[degreePlan.PersonId];
            }
            if (logger.IsInfoEnabled)
            {
                watch.Stop();
                logger.Info("DegreePlan Timing: (GetDegreePlan) _academidCreditRepository.Get completed in " + watch.ElapsedMilliseconds.ToString() + " ms");
            }

            // Check for conflicts in degree plan. This will be done only if the verify argument is true (default)
            if (validate)
            {
                if (logger.IsInfoEnabled)
                {
                    logger.Info("DegreePlan Timing: (GetDegreePlan) CheckForConflictsInDegreePlan starting");
                    watch.Restart();
                }

                degreePlan = await CheckForConflictsInDegreePlanAsync(degreePlan, studentAcademicCredits);

                if (logger.IsInfoEnabled)
                {
                    watch.Stop();
                    logger.Info("DegreePlan Timing: (GetDegreePlan) CheckForConflictsInDegreePlan completed in " + watch.ElapsedMilliseconds.ToString() + " ms");

                    watch.Restart();
                }
            }

            DegreePlanAcademicHistory resultDto = new DegreePlanAcademicHistory();
            // Get the right adapter for the type mapping
            var degreePlanDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan4>();

            // Map the degree plan entity to the degree plan DTO
            resultDto.DegreePlan = degreePlanDtoAdapter.MapToType(degreePlan);

            // Take Student and Academic credits we already have and build the academic history object
            resultDto.AcademicHistory =await  _academicHistoryService.ConvertAcademicCreditsToAcademicHistoryDtoAsync(degreePlan.PersonId, studentAcademicCredits); //TODO
            if (logger.IsInfoEnabled)
            {
                watch.Stop();
                logger.Info("DegreePlan Timing: (GetDegreePlan) conversion to dto completed in " + watch.ElapsedMilliseconds.ToString() + " ms");
            }
            return resultDto;
        }

        
        /// <summary>
        /// Get a course plan by ID number
        /// </summary>
        /// <param name="id">id of plan to retrieve</param>
        /// <param name="validate">true returns a validated degree plan, false does not validate</param>
        /// <returns>A combined dto containing DegreePlan3 and AcademicHistory2 DTO</returns>
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlanAcademicHistory2> GetDegreePlan5Async(int id, bool validate = true)
        {
            Stopwatch watch = null;
            if (logger.IsInfoEnabled)
            {
                watch = new Stopwatch();
                watch.Start();
            }

            // Get the degree plan entity with an ID of id.
            var degreePlan = await _degreePlanRepository.GetAsync(id);
            if (logger.IsInfoEnabled)
            {
                watch.Stop();
                logger.Info("DegreePlan Timing: (GetDegreePlan) _degreePlanRepository.Get completed in " + watch.ElapsedMilliseconds.ToString() + " ms");
            }

            // Make sure user has permissions to view this degree plan. 
            // If not, an PermissionsException will be thrown.
            await CheckViewPlanPermissionsAsync(degreePlan.PersonId);

            if (logger.IsInfoEnabled)
            {
                watch.Restart();
            }

            // Get the academic credits for the given student
            var studentAcademicCredits = new List<AcademicCredit>();
            var creditsByStudentDict =await _academicCreditRepository.GetAcademicCreditByStudentIdsAsync(new List<string>() { degreePlan.PersonId });
            if (creditsByStudentDict.ContainsKey(degreePlan.PersonId))
            {
                studentAcademicCredits = creditsByStudentDict[degreePlan.PersonId];
            }
            if (logger.IsInfoEnabled)
            {
                watch.Stop();
                logger.Info("DegreePlan Timing: (GetDegreePlan) _academidCreditRepository.Get completed in " + watch.ElapsedMilliseconds.ToString() + " ms");
            }

            // Check for conflicts in degree plan. This will be done only if the verify argument is true (default)
            if (validate)
            {
                if (logger.IsInfoEnabled)
                {
                    logger.Info("DegreePlan Timing: (GetDegreePlan) CheckForConflictsInDegreePlan starting");
                    watch.Restart();
                }

                degreePlan = await CheckForConflictsInDegreePlanAsync(degreePlan, studentAcademicCredits);

                if (logger.IsInfoEnabled)
                {
                    watch.Stop();
                    logger.Info("DegreePlan Timing: (GetDegreePlan) CheckForConflictsInDegreePlan completed in " + watch.ElapsedMilliseconds.ToString() + " ms");

                    watch.Restart();
                }
            }

            DegreePlanAcademicHistory2 resultDto = new DegreePlanAcademicHistory2();
            // Get the right adapter for the type mapping
            var degreePlanDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan4>();

            // Map the degree plan entity to the degree plan DTO
            resultDto.DegreePlan = degreePlanDtoAdapter.MapToType(degreePlan);

            // Take Student and Academic credits we already have and build the academic history object
            resultDto.AcademicHistory =await  _academicHistoryService.ConvertAcademicCreditsToAcademicHistoryDto2Async(degreePlan.PersonId, studentAcademicCredits);
            if (logger.IsInfoEnabled)
            {
                watch.Stop();
                logger.Info("DegreePlan Timing: (GetDegreePlan) conversion to dto completed in " + watch.ElapsedMilliseconds.ToString() + " ms");
            }
            return resultDto;
        }

        /// <summary>
        /// Accept an updated DegreePlan2 DTO and apply it to the Colleague database.
        /// </summary>
        /// <param name="degreePlan">Degree plan to update</param>
        /// <returns>The updated degree plan - with new version number - if successful</returns>
        [Obsolete("Obsolete as of API 1.5. Use UpdateDegreePlan3")]
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlan2> UpdateDegreePlan2Async(Ellucian.Colleague.Dtos.Planning.DegreePlan2 degreePlan)
        {
            // Get the right adapter for the type mapping and convert degree plan DTO to degree plan entity
            var degreePlanEntityAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Dtos.Planning.DegreePlan2, Ellucian.Colleague.Domain.Planning.Entities.DegreePlan>();
            var degreePlanToUpdate = degreePlanEntityAdapter.MapToType(degreePlan);

            // Get the stored version of the degree plan for verification of changes
            var storedDegreePlan = await _degreePlanRepository.GetAsync(degreePlanToUpdate.Id);
            degreePlanToUpdate.UpdateMissingProtectionFlags(storedDegreePlan);

            // Check that the user has permissions to do these particular updates
            await  CheckUpdatePermissions2Async(degreePlanToUpdate, storedDegreePlan); 

            // Update the degree plan
            var updatedDegreePlan = await _degreePlanRepository.UpdateAsync(degreePlanToUpdate);

            // Check for conflicts in degree plan and add them to plan
            updatedDegreePlan =  await CheckForConflictsInDegreePlanAsync(updatedDegreePlan);

            // Get the right adapter for the type mapping and convert the degree plan entity to degree plan DTO
            var degreePlanDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan2>();
            var degreePlanDto = degreePlanDtoAdapter.MapToType(updatedDegreePlan);

            return degreePlanDto;
        }

        //TODO
        /// <summary>
        /// Accept an updated DegreePlan3 DTO and apply it to the Colleague database.
        /// </summary>
        /// <param name="degreePlan">Degree plan to update</param>
        /// <returns>The updated degree plan - with new version number - if successful</returns>
        [Obsolete("Obsolete as of API 1.6. Use UpdateDegreePlan4")]
       public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlan3> UpdateDegreePlan3Async(Ellucian.Colleague.Dtos.Planning.DegreePlan3 degreePlan)
        {
            // Get the right adapter for the type mapping and convert degree plan DTO to degree plan entity
            var degreePlanEntityAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Dtos.Planning.DegreePlan3, Ellucian.Colleague.Domain.Planning.Entities.DegreePlan>();
            var degreePlanToUpdate = degreePlanEntityAdapter.MapToType(degreePlan);

            // Get the stored version of the degree plan for verification of changes
            var storedDegreePlan = await _degreePlanRepository.GetAsync(degreePlanToUpdate.Id);
            degreePlanToUpdate.UpdateMissingProtectionFlags(storedDegreePlan);

            // Check that the user has permissions to do these particular updates
            await  CheckUpdatePermissions2Async(degreePlanToUpdate, storedDegreePlan); 

            // Update the degree plan
            var updatedDegreePlan = await _degreePlanRepository.UpdateAsync(degreePlanToUpdate);

            // Check for conflicts in degree plan and add them to plan
            updatedDegreePlan = await CheckForConflictsInDegreePlanAsync(updatedDegreePlan);

            // Get the right adapter for the type mapping and convert the degree plan entity to degree plan DTO
            var degreePlanDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan3>();
            var degreePlanDto = degreePlanDtoAdapter.MapToType(updatedDegreePlan);

            return degreePlanDto;
        }

        /// <summary>
        /// Accept an updated DegreePlan3 DTO and apply it to the Colleague database.
        /// </summary>
        /// <param name="degreePlan">Degree plan to update</param>
        /// <returns>The updated <see cref="DegreePlan4">DegreePlan4</see> DTO - with new version number - if successful, in a combined DTO with the AcademicHistory2 dto</returns>
        [Obsolete("Obsolete as of API 1.11. Use UpdateDegreePlan5Async instead")]
       public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlanAcademicHistory> UpdateDegreePlan4Async(Ellucian.Colleague.Dtos.Planning.DegreePlan4 degreePlan)
        {
            Stopwatch watch1 = null;
            if (logger.IsInfoEnabled)
            {
                watch1 = new Stopwatch();
                watch1.Start();
            }
            // Get the right adapter for the type mapping and convert degree plan DTO to degree plan entity
            var degreePlanEntityAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Dtos.Planning.DegreePlan4, Ellucian.Colleague.Domain.Planning.Entities.DegreePlan>();
            var degreePlanToUpdate = degreePlanEntityAdapter.MapToType(degreePlan);

            var storedDegreePlan = await _degreePlanRepository.GetAsync(degreePlanToUpdate.Id);

            // Check that the user has permissions to do these particular updates
            await CheckUpdatePermissions2Async(degreePlanToUpdate, storedDegreePlan); 

            // Update the degree plan
            var updatedDegreePlan = await _degreePlanRepository.UpdateAsync(degreePlanToUpdate);

            // Check for conflicts in degree plan and add them to plan
            var studentId = updatedDegreePlan.PersonId;
            var studentAcademicCredits = new List<AcademicCredit>();
           var creditsByStudentDict = await _academicCreditRepository.GetAcademicCreditByStudentIdsAsync(new List<string>() { degreePlan.PersonId });
            if (creditsByStudentDict.ContainsKey(degreePlan.PersonId))
            {
                studentAcademicCredits = creditsByStudentDict[degreePlan.PersonId];
            }

           updatedDegreePlan =await CheckForConflictsInDegreePlanAsync(updatedDegreePlan, studentAcademicCredits);

            var degreePlanAcademicHistoryDto = new DegreePlanAcademicHistory();

            // Get the right adapter for the type mapping and convert the degree plan entity to degree plan DTO
            var degreePlanDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan4>();

            degreePlanAcademicHistoryDto.DegreePlan = degreePlanDtoAdapter.MapToType(updatedDegreePlan);

            // Take Student and Academic credits we already have and build the academic history object
            //TODO?
           degreePlanAcademicHistoryDto.AcademicHistory = await _academicHistoryService.ConvertAcademicCreditsToAcademicHistoryDtoAsync(studentId, studentAcademicCredits);

            if (logger.IsInfoEnabled)
            {
                watch1.Stop();
                logger.Info("DegreePlan Timing: (UpdateDegreePlan) UpdateDegreePlan service method complete in " + watch1.ElapsedMilliseconds.ToString() + " ms");
            }
            return degreePlanAcademicHistoryDto;
        }

        /// <summary>
        /// Accept an updated DegreePlan4 DTO and apply it to the Colleague database.
        /// </summary>
        /// <param name="degreePlan">Degree plan to update</param>
        /// <returns>The updated <see cref="DegreePlanAcademicHistory2">DegreePlan4</see> DTO - if successful, in a combined DTO with the AcademicHistory2 dto</returns>
       public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlanAcademicHistory2> UpdateDegreePlan5Async(Ellucian.Colleague.Dtos.Planning.DegreePlan4 degreePlan)
        {
            Stopwatch watch1 = null;
            if (logger.IsInfoEnabled)
            {
                watch1 = new Stopwatch();
                watch1.Start();
            }
            // Get the right adapter for the type mapping and convert degree plan DTO to degree plan entity
            var degreePlanEntityAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Dtos.Planning.DegreePlan4, Ellucian.Colleague.Domain.Planning.Entities.DegreePlan>();
            var degreePlanToUpdate = degreePlanEntityAdapter.MapToType(degreePlan);

            var storedDegreePlan = await _degreePlanRepository.GetAsync(degreePlanToUpdate.Id);

            // Check that the user has permissions to do these particular updates
            await CheckUpdatePermissions2Async(degreePlanToUpdate, storedDegreePlan); 

            // Update the degree plan
            var updatedDegreePlan = await _degreePlanRepository.UpdateAsync(degreePlanToUpdate);

            // Check for conflicts in degree plan and add them to plan
            var studentId = updatedDegreePlan.PersonId;
            var studentAcademicCredits = new List<AcademicCredit>();
           var creditsByStudentDict = await _academicCreditRepository.GetAcademicCreditByStudentIdsAsync(new List<string>() { degreePlan.PersonId });
            if (creditsByStudentDict.ContainsKey(degreePlan.PersonId))
            {
                studentAcademicCredits = creditsByStudentDict[degreePlan.PersonId];
            }

           updatedDegreePlan =await CheckForConflictsInDegreePlanAsync(updatedDegreePlan, studentAcademicCredits);

            var degreePlanAcademicHistoryDto = new DegreePlanAcademicHistory2();

            // Get the right adapter for the type mapping and convert the degree plan entity to degree plan DTO
            var degreePlanDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan4>();

            degreePlanAcademicHistoryDto.DegreePlan = degreePlanDtoAdapter.MapToType(updatedDegreePlan);

            // Take Student and Academic credits we already have and build the academic history object
           degreePlanAcademicHistoryDto.AcademicHistory = await _academicHistoryService.ConvertAcademicCreditsToAcademicHistoryDto2Async(studentId, studentAcademicCredits);

            if (logger.IsInfoEnabled)
            {
                watch1.Stop();
                logger.Info("DegreePlan Timing: (UpdateDegreePlan) UpdateDegreePlan service method complete in " + watch1.ElapsedMilliseconds.ToString() + " ms");
            }
            return degreePlanAcademicHistoryDto;
        }

        //TODO
        /// <summary>
        /// Returns "true" if a sample degree plan for the given program/catalog
        /// Returns "false" if only the default sample degree plan is available
        /// Throws an error if neither is found.
        /// </summary>
        /// <param name="programCode"></param>
        /// <param name="catalog"></param>
        /// <returns></returns>
        public async Task<bool> CheckForSampleAsync(string programCode, string catalog)
        {
            bool sampleFound = false;
            if (!string.IsNullOrEmpty(programCode) && !string.IsNullOrEmpty(catalog))
            {
                sampleFound = ((await GetProgramSampleAsync(programCode, catalog)) != null);
            }
            if (!sampleFound)
            {
                if (await GetDefaultSampleAsync() == null)
                {
                    throw new InvalidOperationException("There is no sample degree plan available");
                }
            }
            return sampleFound;
        }

        //TODO
        /// <summary>
        /// Returns a sample degree plan if it exists for the specified program code and catalog.
        /// </summary>
        /// <param name="programCode"></param>
        /// <param name="catalog"></param>
        /// <returns></returns>
        private async Task<Ellucian.Colleague.Domain.Planning.Entities.SampleDegreePlan> GetProgramSampleAsync(string programCode, string catalog)
        {
            Ellucian.Colleague.Domain.Planning.Entities.SampleDegreePlan sampleDegreePlan = null;

            string pgmCurrTrackCode = null;
            try
            {
                pgmCurrTrackCode =( await _programRequirementsRepository.GetAsync(programCode, catalog)).CurriculumTrackCode;
                sampleDegreePlan = await _curriculumTrackRepository.GetAsync(pgmCurrTrackCode);
            }
            catch { }

            return sampleDegreePlan;
        }

        //TODO
        /// <summary>
        /// Returns the default sample degree plan
        /// </summary>
        /// <returns></returns>
        private async Task<Ellucian.Colleague.Domain.Planning.Entities.SampleDegreePlan> GetDefaultSampleAsync()
        {

            Ellucian.Colleague.Domain.Planning.Entities.SampleDegreePlan sampleDegreePlan = null;

            string currTrackCode = null;
            try
            {
                currTrackCode = (await _planningConfigurationRepository.GetPlanningConfigurationAsync()).DefaultCurriculumTrack; 
                sampleDegreePlan = await _curriculumTrackRepository.GetAsync(currTrackCode);
            }
            catch
            { }

            return sampleDegreePlan;
        }

        //TODO
        /// <summary>
        /// Provides a temporary version of the student's plan, overlaid with the sample plan from the program provided.
        /// The catalog year used to obtain the sample plan is determined from a parameter in Colleague.
        /// The changes to the plan are not saved.
        /// </summary>
        /// <param name="degreePlanId">Id of the degree plan</param>
        /// <param name="programCode">Program code from which the sample plan should be obtained</param>
        /// <returns>The updated Degree Plan DTO</returns>
        [Obsolete("Obsolete as of API 1.5. Use PreviewSampleDegreePlan3")]
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlanPreview2> PreviewSampleDegreePlan2Async(int degreePlanId, string programCode, string firstTermCode)
        {
            if (string.IsNullOrEmpty(programCode))
            {
                throw new ArgumentNullException("programCode", "Program code must be provided.");
            }
            if (degreePlanId <= 0)
            {
                throw new ArgumentNullException("degreePlan", "Degree plan is required.");
            }
            Domain.Planning.Entities.DegreePlan degreePlan = null;
            try
            {
                degreePlan = await _degreePlanRepository.GetAsync(degreePlanId);
                if (degreePlan == null)
                {
                    throw new KeyNotFoundException("Degree plan id " + degreePlanId + " not found.");
                }
            }
            catch
            {
                throw new KeyNotFoundException("Degree plan id " + degreePlanId + " not found.");
            }

            // Prevent action without proper permissions
            if (!UserIsSelf(degreePlan.PersonId))
            {
                // Make sure user has permissions to view this degree plan. 
                // If not, an PermissionsException will be thrown.
                await CheckViewPlanPermissionsAsync(degreePlan.PersonId); //TODO
            }

            // Get academic credits for the student
            // Get student
            Domain.Student.Entities.Student student = await _studentRepository.GetAsync(degreePlan.PersonId);

            var studentAcademicCredits =  await _academicCreditRepository.GetAsync(student.AcademicCreditIds);
            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Term> planningTerms = (await _termRepository.GetAsync()).Where(t => t.ForPlanning == true);
            try
            {
                // Get the sample degree plan to use for this student and program. Student is used to figure out the catalog year.
                Ellucian.Colleague.Domain.Planning.Entities.SampleDegreePlan sampleDegreePlan = await GetSampleDegreePlanAsync(degreePlan.PersonId, programCode);
                // Build the the degree plan for the student with the sample degree plan applied.
                Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview degreePlanPreview = new Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview(degreePlan, sampleDegreePlan, studentAcademicCredits, planningTerms, firstTermCode, CurrentUser.PersonId);
                // Convert the fully merged degree plan to a degree plan DTO
                var degreePlanPreviewDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview, Ellucian.Colleague.Dtos.Planning.DegreePlanPreview2>();
                var degreePlanPreviewDto = degreePlanPreviewDtoAdapter.MapToType(degreePlanPreview);
                return degreePlanPreviewDto;
            }
            catch (Exception ex)
            {
                // If no sample plan can be found, or if it couldn't overlay this plan on the student's plan for any reason then...
                throw new Exception(ex.Message);
            }
        }

        //TODO
        /// <summary>
        /// Provides a temporary version of the student's plan, overlaid with the sample plan from the program provided.
        /// The catalog year used to obtain the sample plan is determined from a parameter in Colleague.
        /// The changes to the plan are not saved.
        /// </summary>
        /// <param name="degreePlanId">Id of the degree plan</param>
        /// <param name="programCode">Program code from which the sample plan should be obtained</param>
        /// <returns>The updated Degree Plan DTO</returns>
        [Obsolete("Obsolete as of API 1.6. Use PreviewSampleDegreePlan4")]
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlanPreview3> PreviewSampleDegreePlan3Async(int degreePlanId, string programCode, string firstTermCode)
        {
            if (string.IsNullOrEmpty(programCode))
            {
                throw new ArgumentNullException("programCode", "Program code must be provided.");
            }
            if (degreePlanId <= 0)
            {
                throw new ArgumentNullException("degreePlan", "Degree plan is required.");
            }
            Domain.Planning.Entities.DegreePlan degreePlan = null;
            try
            {
                degreePlan = await _degreePlanRepository.GetAsync(degreePlanId);
                if (degreePlan == null)
                {
                    throw new KeyNotFoundException("Degree plan id " + degreePlanId + " not found.");
                }
            }
            catch
            {
                throw new KeyNotFoundException("Degree plan id " + degreePlanId + " not found.");
            }

            // Make sure user has permissions to view this degree plan. 
            // If not, an PermissionsException will be thrown.
            await CheckViewPlanPermissionsAsync(degreePlan.PersonId);

            // Get the student's academic credits
            var studentAcademicCredits = new List<AcademicCredit>();
            var creditsByStudentDict =  await _academicCreditRepository.GetAcademicCreditByStudentIdsAsync(new List<string>() { degreePlan.PersonId });
            if (creditsByStudentDict.ContainsKey(degreePlan.PersonId))
            {
                studentAcademicCredits = creditsByStudentDict[degreePlan.PersonId];
            }

            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Term> planningTerms = (await _termRepository.GetAsync()).Where(t => t.ForPlanning == true); //TODO
            try
            {
                // Get the sample degree plan to use for this student and program. Student is used to figure out the catalog year.
                Ellucian.Colleague.Domain.Planning.Entities.SampleDegreePlan sampleDegreePlan = await GetSampleDegreePlanAsync(degreePlan.PersonId, programCode);
                // Build the the degree plan for the student with the sample degree plan applied.
                Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview degreePlanPreview = new Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview(degreePlan, sampleDegreePlan, studentAcademicCredits, planningTerms, firstTermCode, CurrentUser.PersonId);
                // Convert the fully merged degree plan to a degree plan DTO
                var degreePlanPreviewDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview, Ellucian.Colleague.Dtos.Planning.DegreePlanPreview3>();
                var degreePlanPreviewDto = degreePlanPreviewDtoAdapter.MapToType(degreePlanPreview);
                return degreePlanPreviewDto;
            }
            catch (Exception ex)
            {
                // If no sample plan can be found, or if it couldn't overlay this plan on the student's plan for any reason then...
                throw new Exception(ex.Message);
            }
        }

        //TODO
        /// <summary>
        /// CURRENT VERSION: Provides a temporary version of the student's plan, overlaid with the sample plan from the program provided.
        /// The catalog year used to obtain the sample plan is determined from a parameter in Colleague.
        /// The changes to the plan are not saved.
        /// </summary>
        /// <param name="degreePlanId">Id of the degree plan</param>
        /// <param name="programCode">Program code from which the sample plan should be obtained</param>
        /// <returns>The updated Degree Plan DTO</returns>
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlanPreview4> PreviewSampleDegreePlan4Async(int degreePlanId, string programCode, string firstTermCode)
        {
            if (string.IsNullOrEmpty(programCode))
            {
                throw new ArgumentNullException("programCode", "Program code must be provided.");
            }
            if (degreePlanId <= 0)
            {
                throw new ArgumentNullException("degreePlan", "Degree plan is required.");
            }
            Domain.Planning.Entities.DegreePlan degreePlan = null;
            try
            {
                degreePlan = await _degreePlanRepository.GetAsync(degreePlanId);
                if (degreePlan == null)
                {
                    throw new KeyNotFoundException("Degree plan id " + degreePlanId + " not found.");
                }
            }
            catch
            {
                throw new KeyNotFoundException("Degree plan id " + degreePlanId + " not found.");
            }

            // Make sure user has permissions to view this degree plan. 
            // If not, an PermissionsException will be thrown.
            await CheckViewPlanPermissionsAsync(degreePlan.PersonId);

            // Get the student's academic credits
            var studentAcademicCredits = new List<AcademicCredit>();
            var creditsByStudentDict = await _academicCreditRepository.GetAcademicCreditByStudentIdsAsync(new List<string>() { degreePlan.PersonId });
            if (creditsByStudentDict.ContainsKey(degreePlan.PersonId))
            {
                studentAcademicCredits = creditsByStudentDict[degreePlan.PersonId];
            }

            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Term> planningTerms = (await _termRepository.GetAsync()).Where(t => t.ForPlanning == true);
            try
            {
                // Get the sample degree plan to use for this student and program. Student is used to figure out the catalog year.
                Ellucian.Colleague.Domain.Planning.Entities.SampleDegreePlan sampleDegreePlan = await GetSampleDegreePlanAsync(degreePlan.PersonId, programCode);
                // Build the the degree plan for the student with the sample degree plan applied.
                Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview degreePlanPreview = new Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview(degreePlan, sampleDegreePlan, studentAcademicCredits, planningTerms, firstTermCode, CurrentUser.PersonId);
                // Convert the fully merged degree plan to a degree plan DTO
                var degreePlanPreviewDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview, Ellucian.Colleague.Dtos.Planning.DegreePlanPreview4>();
                var degreePlanPreviewDto = degreePlanPreviewDtoAdapter.MapToType(degreePlanPreview);

                // Take Student and Academic credits we already have and build the academic history object and add to each DegreePlan3 item
                var academicHistoryDto =await _academicHistoryService.ConvertAcademicCreditsToAcademicHistoryDtoAsync(degreePlan.PersonId, studentAcademicCredits); //TODO
                degreePlanPreviewDto.AcademicHistory = academicHistoryDto;

                return degreePlanPreviewDto;
            }
            catch (Exception ex)
            {
                // If no sample plan can be found, or if it couldn't overlay this plan on the student's plan for any reason then...
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// CURRENT VERSION: Provides a temporary version of the student's plan, overlaid with the sample plan from the program provided.
        /// The catalog year used to obtain the sample plan is determined from a parameter in Colleague.
        /// The changes to the plan are not saved.
        /// </summary>
        /// <param name="degreePlanId">Id of the degree plan</param>
        /// <param name="programCode">Program code from which the sample plan should be obtained</param>
        /// <returns>The updated Degree Plan DTO</returns>
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlanPreview5> PreviewSampleDegreePlan5Async(int degreePlanId, string programCode, string firstTermCode)
        {
            if (string.IsNullOrEmpty(programCode))
            {
                throw new ArgumentNullException("programCode", "Program code must be provided.");
            }
            if (degreePlanId <= 0)
            {
                throw new ArgumentNullException("degreePlan", "Degree plan is required.");
            }
            Domain.Planning.Entities.DegreePlan degreePlan = null;
            try
            {
                degreePlan = await _degreePlanRepository.GetAsync(degreePlanId);
                if (degreePlan == null)
                {
                    throw new KeyNotFoundException("Degree plan id " + degreePlanId + " not found.");
                }
            }
            catch
            {
                throw new KeyNotFoundException("Degree plan id " + degreePlanId + " not found.");
            }

            // Make sure user has permissions to view this degree plan. 
            // If not, an PermissionsException will be thrown.
            await CheckViewPlanPermissionsAsync(degreePlan.PersonId);

            // Get the student's academic credits
            var studentAcademicCredits = new List<AcademicCredit>();
            var creditsByStudentDict = await _academicCreditRepository.GetAcademicCreditByStudentIdsAsync(new List<string>() { degreePlan.PersonId });
            if (creditsByStudentDict.ContainsKey(degreePlan.PersonId))
            {
                studentAcademicCredits = creditsByStudentDict[degreePlan.PersonId];
            }

            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Term> planningTerms = (await _termRepository.GetAsync()).Where(t => t.ForPlanning == true);
            try
            {
                // Get the sample degree plan to use for this student and program. Student is used to figure out the catalog year.
                Ellucian.Colleague.Domain.Planning.Entities.SampleDegreePlan sampleDegreePlan = await GetSampleDegreePlanAsync(degreePlan.PersonId, programCode);
                // Build the the degree plan for the student with the sample degree plan applied.
                Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview degreePlanPreview = new Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview(degreePlan, sampleDegreePlan, studentAcademicCredits, planningTerms, firstTermCode, CurrentUser.PersonId);
                // Convert the fully merged degree plan to a degree plan DTO
                var degreePlanPreviewDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview, Ellucian.Colleague.Dtos.Planning.DegreePlanPreview5>();
                var degreePlanPreviewDto = degreePlanPreviewDtoAdapter.MapToType(degreePlanPreview);

                // Take Student and Academic credits we already have and build the academic history object and add to each DegreePlan4 item
                var academicHistoryDto =await _academicHistoryService.ConvertAcademicCreditsToAcademicHistoryDto2Async(degreePlan.PersonId, studentAcademicCredits); 
                degreePlanPreviewDto.AcademicHistory = academicHistoryDto;

                return degreePlanPreviewDto;
            }
            catch (Exception ex)
            {
                // If no sample plan can be found, or if it couldn't overlay this plan on the student's plan for any reason then...
                throw new Exception(ex.Message);
            }
        }

        //TODO: needs converting to async
        /// <summary>
        /// Given a student and a program, determine the best sample plan to use
        /// </summary>
        /// <param name="studentId">student Id of the person requesting the sample plan - needed to determine catalog year</param>
        /// <param name="programCode">program from which to pull the sample degree plan</param>
        /// <returns></returns>
        private async Task<Ellucian.Colleague.Domain.Planning.Entities.SampleDegreePlan> GetSampleDegreePlanAsync(string studentId, string programCode)
        {
            // GET SAMPLE DEGREE PLAN
            if (string.IsNullOrEmpty(programCode))
            {
                throw new ArgumentNullException("programCode", "Program code must be provided.");
            }
            if (string.IsNullOrEmpty(studentId))
            {
                throw new ArgumentNullException("studentId", "Student Id is required to get a sample degree plan.");
            }
            // Determine the catalog year to use to get the sample degree plan.
            string catalogCode = null;

            // See if the sample plans's program is one of the student's programs - if so use catalog code from that.
            Ellucian.Colleague.Domain.Student.Entities.StudentProgram studentProgram =await  _studentProgramRepository.GetAsync(studentId, programCode); //TODO
            if (studentProgram == null)
            {
                // If student is not currently enrolled in the sample plan's program, find the best-match catalog for the student
                var studentPrograms = await _studentProgramRepository.GetAsync(studentId); //TODO
                if (studentPrograms == null || studentPrograms.Count() == 0)
                {
                    throw new KeyNotFoundException("StudentPrograms");
                }
                var planProgram = await _programRepository.GetAsync(programCode);
                // Determine the program catalog year to use.
                PlanningConfiguration planningConfiguration = await _planningConfigurationRepository.GetPlanningConfigurationAsync();
                ICollection<Catalog> catalogs =await _catalogRepository.GetAsync();
                catalogCode = ProgramCatalogService.DeriveDefaultCatalog(planProgram, studentPrograms, catalogs, planningConfiguration.DefaultCatalogPolicy);
            }
            else
            {
                catalogCode = studentProgram.CatalogCode;
            }
            //var catalog = "2012";
            var sampleDegreePlan = await GetProgramSampleAsync(programCode, catalogCode);
            if (sampleDegreePlan == null)
            {
                sampleDegreePlan = await GetDefaultSampleAsync();//TODO
            }
            if (sampleDegreePlan == null)
            {
                throw new ArgumentOutOfRangeException("There is no sample degree plan available");
            }
            return sampleDegreePlan;
        }

        /// <summary>
        /// Verify the degree plan
        /// </summary>
        /// <param name="degreePlan"></param>
        /// <returns></returns>
        private async Task<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan> CheckForConflictsInDegreePlanAsync(Ellucian.Colleague.Domain.Planning.Entities.DegreePlan degreePlan, IEnumerable<AcademicCredit> credits = null)
        {
            if (degreePlan.PlannedCourses.Count > 0)
            {
                // Simplify gathering of course and section data for conflict checking for now
                Stopwatch watch = null;
                if (logger.IsInfoEnabled)
                {
                    watch = new Stopwatch();
                    watch.Start();
                }
                var terms = await _termRepository.GetAsync();
                if (logger.IsInfoEnabled)
                {
                    watch.Stop();
                    logger.Info("DegreePlan Timing: (CheckForConflictsInDegreePlan) Completed await _termRepository.GetAsync in " + watch.ElapsedMilliseconds.ToString() + " ms");

                    watch.Restart();
                }
                var courses = await _courseRepository.GetAsync();
                if (logger.IsInfoEnabled)
                {
                    watch.Stop();
                    logger.Info("DegreePlan Timing: (CheckForConflictsInDegreePlan) Completed _courseRepository.Get in " + watch.ElapsedMilliseconds.ToString() + " ms");

                    watch.Restart();
                }
                var registrationTerms = await _termRepository.GetRegistrationTermsAsync();
                if (logger.IsInfoEnabled)
                {
                    watch.Stop();
                    logger.Info("DegreePlan Timing: (CheckForConflictsInDegreePlan) Completed await _termRepository.GetRegistrationTermsAsync in " + watch.ElapsedMilliseconds.ToString() + " ms");
                }
                var currentTermMinDate = DateTime.Today;

                if (credits == null)
                {
                    if (logger.IsInfoEnabled)
                    {

                        watch.Restart();
                    }
                    var creditsDict = await _academicCreditRepository.GetAcademicCreditByStudentIdsAsync(new List<string>() { degreePlan.PersonId });
                    if (creditsDict.ContainsKey(degreePlan.PersonId))
                    {
                        credits = creditsDict[degreePlan.PersonId];
                    }
                    else
                    {
                        credits = new List<AcademicCredit>();
                    }
                    if (logger.IsInfoEnabled)
                    {
                        watch.Stop();
                        logger.Info("DegreePlan Timing: (CheckForConflictsInDegreePlan) Completed _academicCreditRepository.GetAcademicCreditByStudentIds in " + watch.ElapsedMilliseconds.ToString() + " ms");
                    }
                }

                var sections = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
                if (registrationTerms != null && registrationTerms.Count() > 0)
                {
                    if (logger.IsInfoEnabled)
                    {

                        watch.Restart();
                    }
                    sections = (await _sectionRepository.GetRegistrationSectionsAsync(registrationTerms)).ToList();
                    if (logger.IsInfoEnabled)
                    {
                        watch.Stop();
                        logger.Info("DegreePlan Timing: (CheckForConflictsInDegreePlan) Completed __sectionRepository.GetRegistrationSections in " + watch.ElapsedMilliseconds.ToString() + " ms");
                    }
                }
                // Get list of terms/preqrequisites from degree plan
                if (logger.IsInfoEnabled)
                {
                    watch.Restart();
                }
                var requirementCodes = degreePlan.GetRequirementCodes(terms, registrationTerms, courses, credits, sections);
                if (logger.IsInfoEnabled)
                {
                    watch.Stop();
                    logger.Info("DegreePlan Timing: (CheckForConflictsInDegreePlan) Completed degreePlan.GetRequirementCodes in " + watch.ElapsedMilliseconds.ToString() + " ms");
                }


                // Get requirements needed for prerequisite validation
                if (logger.IsInfoEnabled)
                {

                    watch.Restart();
                }
                var requirements =await _requirementRepository.GetAsync(requirementCodes);
                if (logger.IsInfoEnabled)
                {
                    watch.Stop();
                    logger.Info("DegreePlan Timing: (CheckForConflictsInDegreePlan) Completed _requirementrepository.Get in " + watch.ElapsedMilliseconds.ToString() + " ms");

                    // Preprocess rules from prerequisite requirements
                    watch.Restart();
                }

                var allRules = new List<RequirementRule>();
                if (requirements != null)
                {
                    requirements.ToList().ForEach(req => allRules.AddRange(req.GetAllRules()));
                }

                var creditRequests = new List<RuleRequest<Domain.Student.Entities.AcademicCredit>>();
                var courseRequests = new List<RuleRequest<Domain.Student.Entities.Course>>();

                // Run all credits against credit rules
                foreach (var credit in credits)
                {
                    foreach (var rule in allRules.Where(rr => rr.CreditRule != null))
                    {
                        creditRequests.Add(new RuleRequest<Domain.Student.Entities.AcademicCredit>(rule.CreditRule, credit));
                    }
                }
                // Run all courses from credits against course rules
                foreach (var creditCourse in credits.Where(stc => stc.Course != null).Select(stc => stc.Course))
                {
                    foreach (var rule in allRules.Where(rr => rr.CourseRule != null))
                    {
                        courseRequests.Add(new RuleRequest<Domain.Student.Entities.Course>(rule.CourseRule, creditCourse));
                    }
                }

                // Run all planned courses against course rules
                var plannedCourseIds = degreePlan.PlannedCourses.Select(pc => pc.CourseId);
                foreach (var course in courses.Where(c => plannedCourseIds.Contains(c.Id)))
                {
                    foreach (var rule in allRules.Where(rr => rr.CourseRule != null))
                    {
                        courseRequests.Add(new RuleRequest<Domain.Student.Entities.Course>(rule.CourseRule, course));
                    }
                }
                if (logger.IsInfoEnabled)
                {
                    watch.Stop();
                    logger.Info("DegreePlan Timing: (CheckForConflictsInDegreePlan) Completed building rule requests in " + watch.ElapsedMilliseconds.ToString() + " ms");

                    watch.Restart();
                }
                // Execute all the rules; it'll skip the ones with .NET expressions
                var creditResults = await _ruleRepository.ExecuteAsync<Domain.Student.Entities.AcademicCredit>(creditRequests);
                var courseResults = await _ruleRepository.ExecuteAsync<Domain.Student.Entities.Course>(courseRequests);
                var ruleResults = creditResults.Union(courseResults);
                if (logger.IsInfoEnabled)
                {
                    watch.Stop();
                    logger.Info("DegreePlan Timing: (CheckForConflictsInDegreePlan) Completed _ruleRepository.Execute in " + watch.ElapsedMilliseconds.ToString() + " ms");

                    watch.Restart();
                }
                string studentPrimaryLocation = null;
                // If any courses have cycle restrictions by location, determine the student's best location - needed for CheckForConflicts in looking at session and yearly cycle restrictions by location
                if (courses != null && courses.Any(c => c.LocationCycleRestrictions != null && c.LocationCycleRestrictions.Any()))
                {
                    studentPrimaryLocation = await GetStudentLocationAsync(degreePlan.PersonId);
                }
                // Call domain method to validate plan and update with related messages
                degreePlan.CheckForConflicts(terms, registrationTerms, courses, sections, credits, requirements, ruleResults, studentPrimaryLocation);
                if (logger.IsInfoEnabled)
                {
                    watch.Stop();
                    logger.Info("DegreePlan Timing: (CheckForConflictsInDegreePlan) Completed degreePlan.CheckForConflicts in " + watch.ElapsedMilliseconds.ToString() + " ms");
                }
            }
            // Return validated degree plan
            return degreePlan;
        }

        /// <summary>
        /// Register the sections on the degree plan for the term specified
        /// </summary>
        /// <param name="degreePlanId">Degree Plan Id</param>
        /// <param name="termId">Term Id</param>
        /// <returns>A list of registration messages.</returns>
        [Obsolete("Obsolete with version 1.5 of the Api. Use StudentService Register method going forward.")]
        public async Task<Ellucian.Colleague.Dtos.Student.RegistrationResponse> RegisterAsync(int degreePlanId, string termId)
        {
            if (degreePlanId == 0)
            {
                throw new ArgumentException("Invalid degreePlanId", "degreePlanId");
            }
            if (string.IsNullOrEmpty(termId))
            {
                throw new ArgumentException("Invalid termId", "termId");
            }

            var messages = new List<Ellucian.Colleague.Dtos.Student.RegistrationMessage>();

            var sectionRegistrations = new List<Ellucian.Colleague.Domain.Student.Entities.SectionRegistration>();

            var degreePlan = await _degreePlanRepository.GetAsync(degreePlanId);

            // Prevent action without proper permissions
            if (!UserIsSelf(degreePlan.PersonId))
            {
                await CheckRegisterPermissionsAsync(degreePlan.PersonId);
            }

            var plannedCourses = degreePlan.GetPlannedCourses(termId).Where(pc => !string.IsNullOrEmpty(pc.SectionId));

            foreach (var course in plannedCourses)
            {
                if (!string.IsNullOrEmpty(course.SectionId))
                {
                    Ellucian.Colleague.Domain.Student.Entities.RegistrationAction action = Ellucian.Colleague.Domain.Student.Entities.RegistrationAction.Add;
                    switch (course.GradingType)
                    {
                        case Ellucian.Colleague.Domain.Student.Entities.GradingType.Graded:
                            break;
                        case Ellucian.Colleague.Domain.Student.Entities.GradingType.PassFail:
                            action = Ellucian.Colleague.Domain.Student.Entities.RegistrationAction.PassFail;
                            break;
                        case Ellucian.Colleague.Domain.Student.Entities.GradingType.Audit:
                            action = Ellucian.Colleague.Domain.Student.Entities.RegistrationAction.Audit;
                            break;
                        default:
                            break;
                    }
                    sectionRegistrations.Add(new Ellucian.Colleague.Domain.Student.Entities.SectionRegistration() { SectionId = course.SectionId, Action = action, Credits = course.Credits });
                }
            }

            // Next, find any "non-term" sections with a start date in the specified term and submit those to registration too.
            var term = await _termRepository.GetAsync(termId);
            var nonTermPlannedCourses = degreePlan.GetPlannedCourses(null);
            foreach (var nonCourse in nonTermPlannedCourses)
            {
                if (!string.IsNullOrEmpty(nonCourse.SectionId))
                {
                    var nonTermSection =  (await _sectionRepository.GetCachedSectionsAsync(new List<string>() { nonCourse.SectionId })).FirstOrDefault();
                    if (nonTermSection != null)
                    {
                        if (nonTermSection.StartDate >= term.StartDate && nonTermSection.StartDate <= term.EndDate)
                        {
                            Ellucian.Colleague.Domain.Student.Entities.RegistrationAction action = Ellucian.Colleague.Domain.Student.Entities.RegistrationAction.Add;
                            switch (nonCourse.GradingType)
                            {
                                case Ellucian.Colleague.Domain.Student.Entities.GradingType.Graded:
                                    break;
                                case Ellucian.Colleague.Domain.Student.Entities.GradingType.PassFail:
                                    action = Ellucian.Colleague.Domain.Student.Entities.RegistrationAction.PassFail;
                                    break;
                                case Ellucian.Colleague.Domain.Student.Entities.GradingType.Audit:
                                    action = Ellucian.Colleague.Domain.Student.Entities.RegistrationAction.Audit;
                                    break;
                                default:
                                    break;
                            }
                            sectionRegistrations.Add(new Ellucian.Colleague.Domain.Student.Entities.SectionRegistration() { SectionId = nonCourse.SectionId, Action = action, Credits = nonCourse.Credits ?? default(decimal) });
                        }
                    }
                }
            }

            if (sectionRegistrations.Count() == 0)
            {
                messages.Add(new Ellucian.Colleague.Dtos.Student.RegistrationMessage() { Message = "No sections selected within this term" });
                //return messages;
                return new Ellucian.Colleague.Dtos.Student.RegistrationResponse() { Messages = new List<Ellucian.Colleague.Dtos.Student.RegistrationMessage>(messages) };
            }

            var request = new RegistrationRequest(degreePlan.PersonId, sectionRegistrations);
            //var messageEntities = _studentRepository.Register(request);
            var responseEntity = await _studentRepository.RegisterAsync(request);
            var responseDto = new Ellucian.Colleague.Dtos.Student.RegistrationResponse();
            responseDto.Messages = new List<Ellucian.Colleague.Dtos.Student.RegistrationMessage>();
            responseDto.PaymentControlId = responseEntity.PaymentControlId;

            foreach (var message in responseEntity.Messages)
            {
                responseDto.Messages.Add(new Ellucian.Colleague.Dtos.Student.RegistrationMessage { Message = message.Message, SectionId = message.SectionId });
            }

            return responseDto;
        }

        /// <summary>
        /// Take a registration action on a specific section on a degree plan.
        /// </summary>
        /// <param name="degreePlanId">Degree Plan Id</param>
        /// <param name="sectionRegistrationsDto">A section registration item that describes the section and the action to take</param>
        /// <returns>A list of registration messages</returns>
        [Obsolete("Obsolete with version 1.5 of the Api. Use StudentService Register method going forward.")]
        public async Task<Ellucian.Colleague.Dtos.Student.RegistrationResponse> RegisterSectionsAsync(int degreePlanId, IEnumerable<Ellucian.Colleague.Dtos.Student.SectionRegistration> sectionRegistrationsDto)
        {
            if (degreePlanId == 0)
            {
                throw new ArgumentException("Invalid degreePlanId", "degreePlanId");
            }
            if (sectionRegistrationsDto == null)
            {
                throw new ArgumentException("Invalid sectionsRegistration", "sectionsRegistration");
            }

            var messages = new List<Ellucian.Colleague.Dtos.Student.RegistrationMessage>();

            var degreePlan = await _degreePlanRepository.GetAsync(degreePlanId);

            // Prevent action without proper permissions - If user is self continue - otherwise check permissions.
            if (!UserIsSelf(degreePlan.PersonId))
            {
                // Make sure user has permissions to update this degree plan. 
                // If not, an PermissionsException will be thrown.
                await CheckRegisterPermissionsAsync(degreePlan.PersonId);
            }

            var sectionRegistrations = new List<Ellucian.Colleague.Domain.Student.Entities.SectionRegistration>();
            foreach (var sectionReg in sectionRegistrationsDto)
            {
                sectionRegistrations.Add(new Ellucian.Colleague.Domain.Student.Entities.SectionRegistration()
                {
                    Action = (Ellucian.Colleague.Domain.Student.Entities.RegistrationAction)sectionReg.Action,
                    Credits = sectionReg.Credits,
                    SectionId = sectionReg.SectionId
                });
            }

            var request = new RegistrationRequest(degreePlan.PersonId, sectionRegistrations);
            //var messageEntities = _studentRepository.Register(request);
            var responseEntity = await _studentRepository.RegisterAsync(request);
            var responseDto = new Ellucian.Colleague.Dtos.Student.RegistrationResponse();
            responseDto.Messages = new List<Ellucian.Colleague.Dtos.Student.RegistrationMessage>();
            responseDto.PaymentControlId = responseEntity.PaymentControlId;

            foreach (var message in responseEntity.Messages)
            {
                responseDto.Messages.Add(new Ellucian.Colleague.Dtos.Student.RegistrationMessage { Message = message.Message, SectionId = message.SectionId });
            }

            return responseDto;
        }

        //TODO
        /// <summary>
        /// Check the permissions on the user to see if they have privileges to make the update to the plan.
        /// </summary>
        /// <param name="student">Student who's plan is being updated - needed to determine assigned advisors</param>
        /// <param name="degreePlanToUpdate">The degree plan update being submitted - needed to determine the type of change being made</param>
        [Obsolete("Obsolete with version 1.7 of the Api. Use await CheckUpdatePermissions2Async method going forward.")]
        protected async Task CheckUpdatePermissionsAsync(Ellucian.Colleague.Domain.Student.Entities.Student student, Ellucian.Colleague.Domain.Planning.Entities.DegreePlan degreePlanToUpdate)
        {
            // Get user permissions
            IEnumerable<string> userPermissions = await GetUserPermissionCodesAsync();

            //Access is Ok if this is an advisor with full or update access to any student.
            if (userPermissions.Contains(PlanningPermissionCodes.AllAccessAnyAdvisee) || userPermissions.Contains(PlanningPermissionCodes.UpdateAnyAdvisee))
            {
                return;
            }

            bool userIsAssignedAdvisor = (await UserIsAssignedAdvisorAsync(student.Id, (student == null ? null : student.ConvertToStudentAccess()))); //TODO

            // Access is Ok if this is an advisor with full or update acesss to assigned advisees AND this is an assigned advisee
            if ((userPermissions.Contains(PlanningPermissionCodes.AllAccessAssignedAdvisees) && userIsAssignedAdvisor) || (userPermissions.Contains(PlanningPermissionCodes.UpdateAssignedAdvisees) && userIsAssignedAdvisor))
            {
                return;
            }

            // Access is Ok if this is an advisor with review access to the advisee AND this is an review type of change.
            if (userPermissions.Contains(PlanningPermissionCodes.ReviewAnyAdvisee) || (userPermissions.Contains(PlanningPermissionCodes.ReviewAssignedAdvisees) && userIsAssignedAdvisor))
            {
                // Test the plan to find out what type of update is happening. If it is just a ReviewOnly type allow it.
                var currentDegreePlan = await _degreePlanRepository.GetAsync(student.DegreePlanId.GetValueOrDefault(0));
                if (currentDegreePlan.ReviewOnlyChange(degreePlanToUpdate))
                {
                    return;
                }
            }

            // User does not have permissions and error needs to be thrown and logged
            logger.Info(CurrentUser + " does not have permissions to update this degree plan");
            throw new PermissionsException();
        }

        //TODO
        /// <summary>
        /// Check the permissions on the user and the changes to the plan to verify whether the given updates are legal for the particular user.
        /// Users with view-only permission have no change privilege except for create.
        /// </summary>
        /// <param name="degreePlanToUpdate">The degree plan update being submitted - needed to determine the type of change being made</param>
        /// <param name="student">Student who's plan is being updated - needed to determine assigned advisors</param>
        protected async Task  CheckUpdatePermissions2Async(Ellucian.Colleague.Domain.Planning.Entities.DegreePlan degreePlanToUpdate, Ellucian.Colleague.Domain.Planning.Entities.DegreePlan storedDegreePlan, Ellucian.Colleague.Domain.Planning.Entities.PlanningStudent student = null)
        {
            // Check updates if this is the student in the degree plan. Student cannot make move, remove, or add protected items and cannot make review changes.
            if (UserIsSelf(degreePlanToUpdate.PersonId))
            {
                // Since any user with All/Update access has been returned, make sure remaining user has not made illegal changes.
                if (degreePlanToUpdate.HasProtectedChange(storedDegreePlan))
                {
                    logger.Info(CurrentUser + " does not have permissions to update protected items on this degree plan");
                    throw new PermissionsException();
                }
                if (degreePlanToUpdate.HasReviewChange(storedDegreePlan))
                {
                    logger.Info(CurrentUser + " does not have permissions to update approvals on this degree plan");
                    throw new PermissionsException();
                }
            }
            else
            {
                // Check updates if this is not the student. 
                //    * Advisors with all access and with update access to this student may make any update change
                //    * Advisors with review access may make review changes only
                //    * Exception thrown for any other (view only) advisors
                var userPermissions = await GetUserPermissionCodesAsync();

                // Any update is Ok if this is an advisor with full or update access to any student.
                if (userPermissions.Contains(PlanningPermissionCodes.AllAccessAnyAdvisee) || userPermissions.Contains(PlanningPermissionCodes.UpdateAnyAdvisee))
                {
                    return;
                }

                // Any update is ok if this is an advisor with full or update access and this is an assigned advisee
                if (userPermissions.Contains(PlanningPermissionCodes.AllAccessAssignedAdvisees) || userPermissions.Contains(PlanningPermissionCodes.UpdateAssignedAdvisees))
                {
                    if (await UserIsAssignedAdvisorAsync(degreePlanToUpdate.PersonId, (student == null ? null : student.ConvertToStudentAccess()))) //TODO
                    {
                        return;
                    }
                }

                // If Advisor has review access to the student, verify only a review type of change has been made.
                var hasReviewPermissions = false;
                if (userPermissions.Contains(PlanningPermissionCodes.ReviewAnyAdvisee))
                {
                    hasReviewPermissions = true;
                }
                if (!hasReviewPermissions && userPermissions.Contains(PlanningPermissionCodes.ReviewAssignedAdvisees))
                {
                    if (await UserIsAssignedAdvisorAsync(degreePlanToUpdate.PersonId, (student == null ? null : student.ConvertToStudentAccess()))) //TODO
                    {
                        hasReviewPermissions = true;
                    }
                }
                if (hasReviewPermissions)
                {
                    // Test the plan to find out what type of update is happening. If it is just a ReviewOnly type allow it.
                    // If more than review-type changes are found, or protection changes are found, exception is thrown.
                    if (storedDegreePlan.ReviewOnlyChange(degreePlanToUpdate) && !degreePlanToUpdate.HasProtectedChange(storedDegreePlan))
                    {
                        return;
                    }
                }
                // Anyone who is left does not have any permissions that allow update of this student's degree plan
                logger.Info(CurrentUser + " does not have permissions to update this degree plan");
                throw new PermissionsException();
            }
        }


        //TODO
        /// <summary>
        /// Determines if the user has permission to view the student's degree plan
        /// </summary>
        /// <param name="student">Student who plan is being viewed.</param>
        protected async Task  CheckViewPlanPermissionsAsync(string studentId, Ellucian.Colleague.Domain.Student.Entities.Student student = null)
        {
            // If user is self, access is ok
            if (UserIsSelf(studentId))
            {
                return;
            }

            // Get user permissions
            IEnumerable<string> userPermissions = await GetUserPermissionCodesAsync();

            // Access is Ok if this is an advisor with all access, update access or review access to any student.
            if (userPermissions.Contains(PlanningPermissionCodes.AllAccessAnyAdvisee) ||
                userPermissions.Contains(PlanningPermissionCodes.UpdateAnyAdvisee) ||
                userPermissions.Contains(PlanningPermissionCodes.ReviewAnyAdvisee) ||
                userPermissions.Contains(PlanningPermissionCodes.ViewAnyAdvisee))
            {
                return;
            }

            // Access is also OK if this is an advisor with all access, update access or review access to their assigned advisees and this is an assigned advisee.
            bool userIsAssignedAdvisor = await UserIsAssignedAdvisorAsync(studentId, (student == null ? null : student.ConvertToStudentAccess()));
            if (userIsAssignedAdvisor &&
                (userPermissions.Contains(PlanningPermissionCodes.AllAccessAssignedAdvisees) ||
                userPermissions.Contains(PlanningPermissionCodes.UpdateAssignedAdvisees) ||
                userPermissions.Contains(PlanningPermissionCodes.ReviewAssignedAdvisees) ||
                userPermissions.Contains(PlanningPermissionCodes.ViewAssignedAdvisees)))
            {
                return;
            }

            // User does not have permissions and error needs to be thrown and logged
            logger.Info(CurrentUser + " does not have permissions to view this degree plan");
            throw new PermissionsException();
        }

        //TODO
        /// <summary>
        /// Determines if the user has permission to create a degree plan for this student - will throw a PermissionException if not permitted.
        /// </summary>
        /// <param name="student">Student for whom the degree plan is being created</param>
        protected async Task  CheckCreatePlanPermissionsAsync(Ellucian.Colleague.Domain.Student.Entities.Student student)
        {
            // Access is Ok if the current user is this student
            if (UserIsSelf(student.Id)) { return; }

            bool userIsAssignedAdvisor = await UserIsAssignedAdvisorAsync(student.Id, student.ConvertToStudentAccess()); //TODO

            // Get user permissions
            IEnumerable<string> userPermissions = await GetUserPermissionCodesAsync();

            // Access is Ok if this is an advisor with all access, update access, review access or view access to any student.  
            // Access is also OK if this is an advisor with all access, update access, review or view access to their assigned advisees and this is an assigned advisee.
            // Need to allow advisors with view access to be able to create a plan because if a student doesn't have a plan one must be created.
            if (userPermissions.Contains(PlanningPermissionCodes.AllAccessAnyAdvisee) || userPermissions.Contains(PlanningPermissionCodes.UpdateAnyAdvisee) || userPermissions.Contains(PlanningPermissionCodes.ReviewAnyAdvisee) || userPermissions.Contains(PlanningPermissionCodes.ViewAnyAdvisee) || (userPermissions.Contains(PlanningPermissionCodes.AllAccessAssignedAdvisees) && userIsAssignedAdvisor) || (userPermissions.Contains(PlanningPermissionCodes.UpdateAssignedAdvisees) && userIsAssignedAdvisor) || (userPermissions.Contains(PlanningPermissionCodes.ReviewAssignedAdvisees) && userIsAssignedAdvisor) || (userPermissions.Contains(PlanningPermissionCodes.ViewAssignedAdvisees) && userIsAssignedAdvisor))
            {
                return;
            }

            // User does not have permissions and error needs to be thrown and logged
            logger.Info(CurrentUser + " does not have permissions to update this degree plan");
            throw new PermissionsException();
        }

        /// <summary>
        /// VERSION 1: OBSOLETE - USE GETDEGREEPLAN2 WITH API 1.3 AND LATER. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Obsolete("Obsolete with version 1.2 of the Api. Use GetDegreePlan3 going forward.")]
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlan> GetDegreePlanAsync(int id)
        {
            // Get the degree plan entity with an ID of id.
            var degreePlan = await _degreePlanRepository.GetAsync(id);

            if (!UserIsSelf(degreePlan.PersonId))
            {
                // Make sure user has permissions to view this degree plan. 
                // If not, an PermissionsException will be thrown.
                await CheckViewPlanPermissionsAsync(degreePlan.PersonId); //TODO
            }

            // Check for conflicts in degree plan
            degreePlan =  await CheckForConflictsInDegreePlanAsync(degreePlan);

            // Get the right adapter for the type mapping
            var degreePlanDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan>();

            // Map the degree plan entity to the degree plan DTO
            var degreePlanDto = degreePlanDtoAdapter.MapToType(degreePlan);

            return degreePlanDto;
        }

        //TODO
        /// <summary>
        /// Obsolete routine that is just temporarily needed for the now obsolete ApplySampleDegreePlan method.
        /// </summary>
        /// <param name="student"></param>
        [Obsolete("Deprecated on version 1.2 of the Api. Use CheckUpdatePermissions going forward.")]
        protected async Task CheckApplyPlanPermissionsAsync(Ellucian.Colleague.Domain.Student.Entities.Student student)
        {
            // Access is Ok if the current user is this student
            if (UserIsSelf(student.Id)) { return; }

            bool userIsAssignedAdvisor =await UserIsAssignedAdvisorAsync(student.Id, student.ConvertToStudentAccess()); //TODO
            // Get user permissions
            IEnumerable<string> userPermissions = await GetUserPermissionCodesAsync();

            // Access is Ok if this is an advisor with all access, update access or review access to any student.
            // Access is also OK if this is an advisor with all access, update access or review access to their assigned advisees and this is an assigned advisee.
            if (userPermissions.Contains(PlanningPermissionCodes.AllAccessAnyAdvisee) || userPermissions.Contains(PlanningPermissionCodes.UpdateAnyAdvisee) || (userPermissions.Contains(PlanningPermissionCodes.AllAccessAssignedAdvisees) && userIsAssignedAdvisor) || (userPermissions.Contains(PlanningPermissionCodes.UpdateAssignedAdvisees) && userIsAssignedAdvisor))
            {
                return;
            }

            // User does not have permissions and error needs to be thrown and logged
            logger.Info(CurrentUser + " does not have permissions to update this degree plan");
            throw new PermissionsException();
        }

        /// <summary>
        /// VERSION 1: OBSOLETE - USE CREATEDEGREEPLAN2 WITH API 1.3 AND LATER.
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlan> CreateDegreePlanAsync(string studentId)
        {
            if (string.IsNullOrEmpty(studentId))
            {
                throw new ArgumentNullException(studentId, "Must specify a student Id to create a new plan.");
            }

            // Get student from repository
            Ellucian.Colleague.Domain.Student.Entities.Student student = await _studentRepository.GetAsync(studentId);

            // Verify current user has the permissions to create a degree plan
            await CheckCreatePlanPermissionsAsync(student); //TODO

            // Get data needed to build a degree plan
            var planningTerms = await _termRepository.GetAsync(); //TODO
            var studentPrograms = await _studentProgramRepository.GetAsync(studentId); //TODO
            var programs = await _programRepository.GetAsync();

            // Create a degree plan for a student - business logic resides in the domain
            var degreePlan = Ellucian.Colleague.Domain.Planning.Entities.DegreePlan.CreateDegreePlan(student, studentPrograms, planningTerms, programs);

            // Have Repository do the add - could throw exception if student already has a plan.
            var updatedDegreePlan = await _degreePlanRepository.AddAsync(degreePlan);

            // Map the degree plan to a DTO 
            var newDegreePlanDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan>();
            var newPlanDto = newDegreePlanDtoAdapter.MapToType(updatedDegreePlan);
            return newPlanDto;
        }

        /// <summary>
        /// Update the student's plan with a sample degree plan, using the sample degree plan for the supplied program.
        /// </summary>
        /// <param name="studentId">Id of the student</param>
        /// <param name="programCode">Program code from where the sample degree plan should be derived</param>
        /// <returns>An Updated Degree Plan DTO.</returns>
        [Obsolete("Deprecated on version 1.2 of the Api. Use PreviewSampleDegreePlan along with the UpdateDegreePlan methods going forward.")]
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlan> ApplySampleDegreePlanAsync(string studentId, string programCode)
        {
            // Get student from repository
            Ellucian.Colleague.Domain.Student.Entities.Student student = await _studentRepository.GetAsync(studentId);
            // Prevent action without proper permissions
            // Make sure user has permissions to update this degree plan. 
            // If not, an PermissionsException will be thrown.

            // Intentionally using deprecated await CheckApplyPlanPermissionsAsync for this deprecated method.  We know this isn't a review type of change
            // so there is no reason to go through all the logic in CheckUpdatePermissions.             
            await CheckApplyPlanPermissionsAsync(student); //TODO

            // GET OR CREATE DEGREE PLAN

            // Get the degree plan for this student
            Domain.Planning.Entities.DegreePlan degreePlanToUpdate = null;
            try
            {
                degreePlanToUpdate = (await _degreePlanRepository.GetAsync(new List<string>() { studentId })).FirstOrDefault();
                if (degreePlanToUpdate == null)
                {
                    throw new ArgumentException();
                }
            }
            catch // If there is an error thrown and/or degree plan returned is null, create a new one
            {
                try
                {
                    // No degree plan found for this student, call the service method to create one and write it to the repository
                    // Convert the returned, new degree plan back to the domain entity from dto
                    var degreePlan = Task.Run(async () =>
                    {
                        return await CreateDegreePlanAsync(studentId);
                    }).GetAwaiter().GetResult();
                    var degreePlanEntityAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Dtos.Planning.DegreePlan, Ellucian.Colleague.Domain.Planning.Entities.DegreePlan>();
                    degreePlanToUpdate = degreePlanEntityAdapter.MapToType(degreePlan);
                }
                catch
                {
                    throw new ArgumentOutOfRangeException("Cannot find or create a degree plan");
                }
            }
            // Get the sample degree plan to use for this student and program.
            Ellucian.Colleague.Domain.Planning.Entities.SampleDegreePlan sampleDegreePlan = await GetSampleDegreePlanAsync(studentId, programCode);

            // Build the the degree plan for the student with the sample degree plan applied.
            // In this version, the academic credits are immaterial, all courses on the sample plan should be loaded.
            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.AcademicCredit> studentAcademicCredits = new List<Ellucian.Colleague.Domain.Student.Entities.AcademicCredit>();
            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Term> planningTerms = (await _termRepository.GetAsync()).Where(t => t.ForPlanning == true); //TODO
            Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview degreePlanPreview = new Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview(degreePlanToUpdate, sampleDegreePlan, studentAcademicCredits, planningTerms, string.Empty, CurrentUser.PersonId);
            var degreePlanPreviewDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview, Ellucian.Colleague.Dtos.Planning.DegreePlanPreview>();
            var degreePlanPreviewDto = degreePlanPreviewDtoAdapter.MapToType(degreePlanPreview);
            // Update the degree plan via the local service routine
            var updatedDegreePlanDto = await UpdateDegreePlanAsync(degreePlanPreviewDto.MergedDegreePlan);
            return updatedDegreePlanDto;
        }

        /// <summary>
        /// VERSION 1: OBSOLETE - USE UPDATEDEGREEPLAN2 WITH API 1.3 AND LATER.
        /// </summary>
        /// <param name="degreePlan"></param>
        /// <returns></returns>
        [Obsolete("Obsoloete with version 1.3 of the API. Use UpdateDegreePlan3.")]
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlan> UpdateDegreePlanAsync(Ellucian.Colleague.Dtos.Planning.DegreePlan degreePlan)
        {
            // Get the right adapter for the type mapping and convert degree plan DTO to degree plan entity
            var degreePlanEntityAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Dtos.Planning.DegreePlan, Ellucian.Colleague.Domain.Planning.Entities.DegreePlan>();
            var degreePlanToUpdate = degreePlanEntityAdapter.MapToType(degreePlan);

            // If user is not self check user permissions.
            if (!UserIsSelf(degreePlan.PersonId))
            {
                // Get student from repository
                Ellucian.Colleague.Domain.Student.Entities.Student student = await _studentRepository.GetAsync(degreePlan.PersonId);
                await CheckUpdatePermissionsAsync(student, degreePlanToUpdate); //TODO
            }

            // Update the degree plan
            var updatedDegreePlan = await _degreePlanRepository.UpdateAsync(degreePlanToUpdate);

            // Check for conflicts in degree plan and add them to plan
            updatedDegreePlan =  await CheckForConflictsInDegreePlanAsync(updatedDegreePlan);

            // Get the right adapter for the type mapping and convert the degree plan entity to degree plan DTO
            var degreePlanDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlan, Ellucian.Colleague.Dtos.Planning.DegreePlan>();
            var degreePlanDto = degreePlanDtoAdapter.MapToType(updatedDegreePlan);

            return degreePlanDto;
        }

        //TODO
        /// <summary>
        /// Provides a temporary version of the student's plan, overlaid with the sample plan from the program provided.
        /// The catalog year used to obtain the sample plan is determined from a parameter in Colleague.
        /// The changes to the plan are not saved.
        /// </summary>
        /// <param name="degreePlanId">Id of the degree plan</param>
        /// <param name="programCode">Program code from which the sample plan should be obtained</param>
        /// <returns>The updated Degree Plan DTO</returns>
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlanPreview> PreviewSampleDegreePlanAsync(int degreePlanId, string programCode)
        {
            if (string.IsNullOrEmpty(programCode))
            {
                throw new ArgumentNullException("programCode", "Program code must be provided.");
            }
            if (degreePlanId <= 0)
            {
                throw new ArgumentNullException("degreePlan", "Degree plan is required.");
            }
            Domain.Planning.Entities.DegreePlan degreePlan = null;
            try
            {
                degreePlan = await _degreePlanRepository.GetAsync(degreePlanId);
                if (degreePlan == null)
                {
                    throw new KeyNotFoundException("Degree plan id " + degreePlanId + " not found.");
                }
            }
            catch
            {
                throw new KeyNotFoundException("Degree plan id " + degreePlanId + " not found.");
            }

            // Get student
            Domain.Student.Entities.Student student = await _studentRepository.GetAsync(degreePlan.PersonId);

            // Prevent action without proper permissions
            if (!UserIsSelf(degreePlan.PersonId))
            {
                // Make sure user has permissions to view this degree plan. 
                // If not, an PermissionsException will be thrown.
                await CheckViewPlanPermissionsAsync(degreePlan.PersonId, student);
            }

            // Get academic credits for the student
            var studentAcademicCredits = await _academicCreditRepository.GetAsync(student.AcademicCreditIds);
            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Term> planningTerms = (await _termRepository.GetAsync()).Where(t => t.ForPlanning == true);
            try
            {
                // Get the sample degree plan to use for this student and program. Student is used to figure out the catalog year.
                Ellucian.Colleague.Domain.Planning.Entities.SampleDegreePlan sampleDegreePlan = await GetSampleDegreePlanAsync(degreePlan.PersonId, programCode);
                // Build the the degree plan for the student with the sample degree plan applied.
                Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview degreePlanPreview = new Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview(degreePlan, sampleDegreePlan, studentAcademicCredits, planningTerms, string.Empty, CurrentUser.PersonId);
                // Convert the fully merged degree plan to a degree plan DTO
                var degreePlanPreviewDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanPreview, Ellucian.Colleague.Dtos.Planning.DegreePlanPreview>();
                var degreePlanPreviewDto = degreePlanPreviewDtoAdapter.MapToType(degreePlanPreview);
                return degreePlanPreviewDto;
            }
            catch (Exception ex)
            {
                // If no sample plan can be found, or if it couldn't overlay this plan on the student's plan for any reason then...
                throw new Exception(ex.Message);
            }
        }

        //TODO
        /// <summary>
        /// Given a degree plan DTO, create and persist a degree plan archive, then return the archive DTO
        /// </summary>
        /// <param name="degreePlan"></param>
        /// <returns></returns>
        [Obsolete("Obsoloete with version 1.5 of the API. Use ArchiveDegreePlan2.")]
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlanArchive> ArchiveDegreePlanAsync(DegreePlan2 degreePlan)
        {
            // Get the right adapter for the type mapping and convert degree plan DTO to degree plan entity
            var degreePlanEntityAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Dtos.Planning.DegreePlan2, Ellucian.Colleague.Domain.Planning.Entities.DegreePlan>();
            var degreePlanToArchive = degreePlanEntityAdapter.MapToType(degreePlan);

            // Get the student. Need academic credits to build the degree plan archive
            Ellucian.Colleague.Domain.Student.Entities.Student student = await _studentRepository.GetAsync(degreePlan.PersonId);

            // Make sure the current user has permission to create the archive
            if (!UserIsSelf(degreePlan.PersonId))
            {
                // Get student from repository
                await CheckUpdatePermissionsAsync(student, degreePlanToArchive); //TODO
            }
            // Archive the degree plan

            // First, get the student's active programs
            var studentPrograms = await _studentProgramRepository.GetAsync(degreePlan.PersonId);
            var courses = await _courseRepository.GetAsync();
            var registrationTerms = await _termRepository.GetRegistrationTermsAsync();
            // Get all sections on the degree plan - used to get section titles and numbers for the archive
            List<string> sectionsOnPlan = degreePlan.NonTermPlannedCourses.Where(c => !string.IsNullOrEmpty(c.SectionId)).Select(s => s.SectionId).ToList();
            foreach (var term in degreePlan.Terms)
            {
                var sectionsToAdd = term.PlannedCourses.Where(c => !string.IsNullOrEmpty(c.SectionId)).Select(p => p.SectionId).ToList();
                sectionsOnPlan.AddRange(sectionsToAdd);
            }
            var degreePlanSections =  await _sectionRepository.GetCachedSectionsAsync(sectionsOnPlan);
            var academicCredits = await _academicCreditRepository.GetAsync(student.AcademicCreditIds, true);
            var grades = await _gradeRepository.GetAsync();
            var newDegreePlanArchive = Ellucian.Colleague.Domain.Planning.Entities.DegreePlanArchive.CreateDegreePlanArchive(degreePlanToArchive, CurrentUser.PersonId, studentPrograms, courses, degreePlanSections, academicCredits, grades);
            var degreePlanArchive = await _degreePlanArchiveRepository.AddAsync(newDegreePlanArchive);

            // Get the right adapter for the type mapping and convert the degree plan entity to degree plan DTO
            var degreePlanArchiveDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanArchive, Ellucian.Colleague.Dtos.Planning.DegreePlanArchive>();
            var degreePlanArchiveDto = degreePlanArchiveDtoAdapter.MapToType(degreePlanArchive);

            return degreePlanArchiveDto;
        }

        //TODO
        /// <summary>
        /// Given a degree plan DTO, create and persist a degree plan archive, then return the archive DTO
        /// </summary>
        /// <param name="degreePlan">The degree plan to be archived</param>
        /// <returns>The <see cref="DegreePlanArchive2"/>archive of the degree plan</see></returns>
        [Obsolete("Obsoloete with version 1.7 of the API. Use ArchiveDegreePlan3.")]
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlanArchive2> ArchiveDegreePlan2Async(DegreePlan3 degreePlan)
        {
            // Get the right adapter for the type mapping and convert degree plan DTO to degree plan entity
            var degreePlanEntityAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Dtos.Planning.DegreePlan3, Ellucian.Colleague.Domain.Planning.Entities.DegreePlan>();
            var degreePlanToArchive = degreePlanEntityAdapter.MapToType(degreePlan);

            // Get the student. Need academic credits to build the degree plan archive
            Ellucian.Colleague.Domain.Planning.Entities.PlanningStudent student = await _planningStudentRepository.GetAsync(degreePlan.PersonId); //TODO

            var storedDegreePlan = await _degreePlanRepository.GetAsync(degreePlan.Id);
            degreePlanToArchive.UpdateMissingProtectionFlags(storedDegreePlan); //TODO?

            // Leave the door open for student to create an archive
            if (!UserIsSelf(degreePlan.PersonId))
            {
                // Make sure this user has permissions to create an archive
               await  CheckUpdatePermissions2Async(degreePlanToArchive, storedDegreePlan, student); //TODO
            }

            // Archive the degree plan

            // First, get the student's active programs
            var studentPrograms = await _studentProgramRepository.GetAsync(degreePlan.PersonId);
            var courses = await _courseRepository.GetAsync();
            var registrationTerms = await _termRepository.GetRegistrationTermsAsync();
            // Get all sections on the degree plan - used to get section titles and numbers for the archive
            List<string> sectionsOnPlan = degreePlan.NonTermPlannedCourses.Where(c => !string.IsNullOrEmpty(c.SectionId)).Select(s => s.SectionId).ToList();
            foreach (var term in degreePlan.Terms)
            {
                var sectionsToAdd = term.PlannedCourses.Where(c => !string.IsNullOrEmpty(c.SectionId)).Select(p => p.SectionId).ToList();
                sectionsOnPlan.AddRange(sectionsToAdd);
            }
            var degreePlanSections = await _sectionRepository.GetCachedSectionsAsync(sectionsOnPlan);
            var academicCredits = new List<AcademicCredit>();
            var academicCreditsDict = await _academicCreditRepository.GetAcademicCreditByStudentIdsAsync(new List<string> { student.Id }, true);
            if (academicCreditsDict.ContainsKey(student.Id))
            {
                academicCredits = academicCreditsDict[student.Id];
            }
            var grades = await _gradeRepository.GetAsync();
            var newDegreePlanArchive = Ellucian.Colleague.Domain.Planning.Entities.DegreePlanArchive.CreateDegreePlanArchive(degreePlanToArchive, CurrentUser.PersonId, studentPrograms, courses, degreePlanSections, academicCredits, grades);
            var degreePlanArchive = await _degreePlanArchiveRepository.AddAsync(newDegreePlanArchive);

            // Get the right adapter for the type mapping and convert the degree plan entity to degree plan DTO
            var degreePlanArchiveDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanArchive, Ellucian.Colleague.Dtos.Planning.DegreePlanArchive2>();
            var degreePlanArchiveDto = degreePlanArchiveDtoAdapter.MapToType(degreePlanArchive);

            return degreePlanArchiveDto;
        }

        //TODO
        /// <summary>
        /// Given a degree plan DTO, create and persist a degree plan archive, then return the archive DTO
        /// </summary>
        /// <param name="degreePlan">The degree plan to be archived</param>
        /// <returns>The <see cref="DegreePlanArchive2"/>archive of the degree plan</see></returns>
        public async Task<Ellucian.Colleague.Dtos.Planning.DegreePlanArchive2> ArchiveDegreePlan3Async(DegreePlan4 degreePlan)
        {
            // Get the right adapter for the type mapping and convert degree plan DTO to degree plan entity
            var degreePlanEntityAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Dtos.Planning.DegreePlan4, Ellucian.Colleague.Domain.Planning.Entities.DegreePlan>();
            var degreePlanToArchive = degreePlanEntityAdapter.MapToType(degreePlan);

            // Get the student. Need academic credits to build the degree plan archive
            Ellucian.Colleague.Domain.Planning.Entities.PlanningStudent student = await _planningStudentRepository.GetAsync(degreePlan.PersonId); //TODO

            var storedDegreePlan = await _degreePlanRepository.GetAsync(degreePlan.Id);
            degreePlanToArchive.UpdateMissingProtectionFlags(storedDegreePlan); //TODO

            // Leave the door open for student to create an archive
            if (!UserIsSelf(degreePlan.PersonId))
            {
                // Make sure this user has permissions to create an archive
               await  CheckUpdatePermissions2Async(degreePlanToArchive, storedDegreePlan, student); //TODO
            }

            // Archive the degree plan

            // First, get the student's active programs
            var studentPrograms = await _studentProgramRepository.GetAsync(degreePlan.PersonId);
            var courses = await _courseRepository.GetAsync();
            var registrationTerms = await _termRepository.GetRegistrationTermsAsync();
            // Get all sections on the degree plan - used to get section titles and numbers for the archive
            List<string> sectionsOnPlan = degreePlan.NonTermPlannedCourses.Where(c => !string.IsNullOrEmpty(c.SectionId)).Select(s => s.SectionId).ToList();
            foreach (var term in degreePlan.Terms)
            {
                var sectionsToAdd = term.PlannedCourses.Where(c => !string.IsNullOrEmpty(c.SectionId)).Select(p => p.SectionId).ToList();
                sectionsOnPlan.AddRange(sectionsToAdd);
            }
            var degreePlanSections = await _sectionRepository.GetCachedSectionsAsync(sectionsOnPlan);
            var academicCreditsDict = await _academicCreditRepository.GetAcademicCreditByStudentIdsAsync(new List<string> { student.Id }, true);
            var studentAcademicCredits = new List<AcademicCredit>();
            if (academicCreditsDict.ContainsKey(degreePlan.PersonId))
            {
                studentAcademicCredits = academicCreditsDict[student.Id];
            }
            //var academicCredits = await _academicCreditRepository.GetAsync(student.AcademicCreditIds, true);
            var grades = await _gradeRepository.GetAsync();
            var newDegreePlanArchive = Ellucian.Colleague.Domain.Planning.Entities.DegreePlanArchive.CreateDegreePlanArchive(degreePlanToArchive, CurrentUser.PersonId, studentPrograms, courses, degreePlanSections, studentAcademicCredits, grades);
            var degreePlanArchive = await _degreePlanArchiveRepository.AddAsync(newDegreePlanArchive);

            // Get the right adapter for the type mapping and convert the degree plan entity to degree plan DTO
            var degreePlanArchiveDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanArchive, Ellucian.Colleague.Dtos.Planning.DegreePlanArchive2>();
            var degreePlanArchiveDto = degreePlanArchiveDtoAdapter.MapToType(degreePlanArchive);

            return degreePlanArchiveDto;
        }

        //TODO
        /// <summary>
        /// Retrieves all degree plan archives for a specified degree plan id.
        /// </summary>
        /// <param name="degreePlanId">Id of the degree plan</param>
        /// <returns></returns>
        [Obsolete("Obsolete with version 1.5 of the Api. Use GetDegreePlanArchives2.")]
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.Planning.DegreePlanArchive>> GetDegreePlanArchivesAsync(int degreePlanId)
        {
            // Get the degree plan from the repository
            Ellucian.Colleague.Domain.Planning.Entities.DegreePlan degreePlan = await _degreePlanRepository.GetAsync(degreePlanId);

            // Make sure the current user has permission to view the degree plan (and therefore it's archives) 
            if (!UserIsSelf(degreePlan.PersonId))
            {
                await CheckViewPlanPermissionsAsync(degreePlan.PersonId); //TODO
            }

            // Archive the degree plan  //TODO
            IEnumerable<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanArchive> degreePlanArchives = await _degreePlanArchiveRepository.GetDegreePlanArchivesAsync(degreePlanId);

            List<Ellucian.Colleague.Dtos.Planning.DegreePlanArchive> degreePlanArchiveDtos = new List<Ellucian.Colleague.Dtos.Planning.DegreePlanArchive>();

            // Get the right adapter for the type mapping and convert the degree plan entity to degree plan DTO
            var degreePlanArchiveDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanArchive, Ellucian.Colleague.Dtos.Planning.DegreePlanArchive>();

            foreach (var degreePlanArchive in degreePlanArchives)
            {
                var degreePlanArchiveDto = degreePlanArchiveDtoAdapter.MapToType(degreePlanArchive);
                degreePlanArchiveDtos.Add(degreePlanArchiveDto);
            }

            return degreePlanArchiveDtos;
        }

        //TODO
        /// <summary>
        /// Retrieves all degree plan archives for a specified degree plan id.
        /// </summary>
        /// <param name="degreePlanId">Id of the degree plan</param>
        /// <returns>List of <see cref="DegreePlanArchive2">Degree Plan Archive</see> objects.</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.Planning.DegreePlanArchive2>> GetDegreePlanArchives2Async(int degreePlanId)
        {
            // Get the degree plan from the repository
            Ellucian.Colleague.Domain.Planning.Entities.DegreePlan degreePlan = await _degreePlanRepository.GetAsync(degreePlanId);

            // Make sure the current user has permission to view the degree plan (and therefore it's archives) 
            await CheckViewPlanPermissionsAsync(degreePlan.PersonId);

            // Archive the degree plan
            IEnumerable<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanArchive> degreePlanArchives = await _degreePlanArchiveRepository.GetDegreePlanArchivesAsync(degreePlanId);

            List<Ellucian.Colleague.Dtos.Planning.DegreePlanArchive2> degreePlanArchiveDtos = new List<Ellucian.Colleague.Dtos.Planning.DegreePlanArchive2>();

            // Get the right adapter for the type mapping and convert the degree plan entity to degree plan DTO
            var degreePlanArchiveDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanArchive, Ellucian.Colleague.Dtos.Planning.DegreePlanArchive2>();

            foreach (var degreePlanArchive in degreePlanArchives)
            {
                var degreePlanArchiveDto = degreePlanArchiveDtoAdapter.MapToType(degreePlanArchive);
                degreePlanArchiveDtos.Add(degreePlanArchiveDto);
            }

            return degreePlanArchiveDtos;
        }

        //TODO
        /// <summary>
        /// Given an archive ID, return the archive report object, which contains the data required to generate the pdf.
        /// </summary>
        /// <param name="values">The system ID for the requested plan archive</param>
        /// <returns>A DegreePlanArchiveReport object</returns>
        public async Task<DegreePlanArchiveReport> GetDegreePlanArchiveReportAsync(int archiveId)
        {
            try
            {
                logger.Info("Begin GetDegreePlanArchiveReport");
                var degreePlanArchive = await _degreePlanArchiveRepository.GetDegreePlanArchiveAsync(archiveId);

                // Get student from repository, it's required for permission checking, and then for building the report DTO
                Ellucian.Colleague.Domain.Student.Entities.Student student = await _studentRepository.GetAsync(degreePlanArchive.StudentId);

                // Make sure the current user has permission to view the degree plan (and therefore it's archives) 
                if (!UserIsSelf(degreePlanArchive.StudentId))
                {
                    await CheckViewPlanPermissionsAsync(degreePlanArchive.StudentId, student);
                }

                // Gather information needed to build the DegreePlanArchiveReport model.
                var programs = await _programRepository.GetAsync();
                // Set the planned term data in the report
                var terms = (await _termRepository.GetAsync()).ToList(); //TODO
                logger.Info("GetDegreePlanArchiveReport: After retrieval of programs and terms.");

                // Get all related advisors needed to build the plan (created by, reviewed by, note writers, approvers, and added by)
                var peopleIds = new List<string>();
                if (!string.IsNullOrEmpty(degreePlanArchive.CreatedBy)) { peopleIds.Add(degreePlanArchive.CreatedBy); }
                if (!string.IsNullOrEmpty(degreePlanArchive.ReviewedBy)) { peopleIds.Add(degreePlanArchive.ReviewedBy); }
                var writtenByIds = degreePlanArchive.Notes.Select(n => n.PersonId);
                peopleIds.AddRange(writtenByIds);
                var approvedByIds = degreePlanArchive.ArchivedCourses.Select(a => a.ApprovedBy);
                peopleIds.AddRange(approvedByIds);
                var addedByIds = degreePlanArchive.ArchivedCourses.Select(a => a.AddedBy);
                peopleIds.AddRange(addedByIds);
                peopleIds = peopleIds.Distinct().ToList();
                var degreePlanPeople = new List<Ellucian.Colleague.Domain.Planning.Entities.Advisor>();
                logger.Info("GetDegreePlanArchiveReport: Getting People for Ids ");
                foreach (var id in peopleIds)
                {
                    try
                    {
                        var advisor = await _advisorRepository.GetAsync(id);
                        degreePlanPeople.Add(advisor);
                    }
                    catch (Exception)
                    {
                        // Can't find advisor 
                    }
                }
                logger.Info("GetDegreePlanArchiveReport: Call to DegreePlanArchiveReport constructor");
                // Now create the DegreePlanReportArchive
                var report = new DegreePlanArchiveReport(degreePlanArchive, student, programs, degreePlanPeople, terms);
                logger.Info("GetDegreePlanArchiveReport complete.");
                return report;
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }

        public Byte[] GenerateDegreePlanArchiveReport(DegreePlanArchiveReport archiveReport, string path, string reportLogoPath)
        {

            // Create the report object, set it's path, and set permissions for the sandboxed app domain in which the report runs to unrestricted
            LocalReport report = new LocalReport();
            report.ReportPath = path;
            report.SetBasePermissionsForSandboxAppDomain(new System.Security.PermissionSet(System.Security.Permissions.PermissionState.Unrestricted));
            report.EnableExternalImages = true;

            var parameters = new List<ReportParameter>();

            parameters.Add(new ReportParameter("LogoPath", reportLogoPath));
            parameters.Add(new ReportParameter("ReportTitle", archiveReport.ReportTitle));
            parameters.Add(new ReportParameter("StudentName", archiveReport.StudentName));
            parameters.Add(new ReportParameter("Label_StudentName", "Student"));

            parameters.Add(new ReportParameter("StudentId", archiveReport.StudentId));
            parameters.Add(new ReportParameter("Label_StudentId", "ID"));

            var programStrings = new List<string>();
            foreach (var program in archiveReport.StudentPrograms)
            {
                programStrings.Add(program.Key + ", " + program.Value);
            }
            parameters.Add(new ReportParameter("StudentPrograms", programStrings.ToArray()));
            parameters.Add(new ReportParameter("Label_StudentPrograms", "Programs"));

            parameters.Add(new ReportParameter("LastReviewed", archiveReport.ReviewedBy + " on " + archiveReport.ReviewedOn.DateTime.ToShortDateString()));
            parameters.Add(new ReportParameter("Label_LastReviewed", "Reviewed By"));

            parameters.Add(new ReportParameter("Label_Notes", "Notes"));

            parameters.Add(new ReportParameter("ArchivedBy", archiveReport.ArchivedBy + " on " + archiveReport.ArchivedOn.DateTime.ToShortDateString() + " at " + archiveReport.ArchivedOn.DateTime.ToShortTimeString()));
            parameters.Add(new ReportParameter("Label_ArchivedBy", "Archived By"));
            report.SetParameters(parameters);

            report.DataSources.Add(new ReportDataSource("ArchivedCourses", archiveReport.ArchivedCoursesDataSet.Tables[0]));

            report.DataSources.Add(new ReportDataSource("ArchivedNotes", archiveReport.ArchivedNotesDataSet.Tables[0]));

            // Set up some options for the report
            string mimeType = string.Empty;
            string encoding;
            string fileNameExtension;
            Warning[] warnings;
            string[] streams;

            // Render the report as a byte array
            var renderedBytes = report.Render(
                PdfReportConstants.ReportType,
                PdfReportConstants.DeviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            return renderedBytes;
        }

        /// <summary>
        /// Determines the best location to use for a specific student to use when checking location cycle restrictions 
        /// First check to see if the student has any applicable home locations (based on date ranges)
        /// If no applicable location is found in the student's home locations, take the first location from the student's active programs.
        /// </summary>
        /// <param name="studentId">Id of the student</param>
        /// <returns>Location string or null</returns>
        private async Task<string> GetStudentLocationAsync(string studentId)
        {

            var student = await _studentRepository.GetAsync(studentId);
            // Note:, the second parameter for GetStudentProgramsByIdsAsync is includeInactivePrograms. Setting that to false will only return active programs.
            var activeStudentPrograms = await _studentProgramRepository.GetStudentProgramsByIdsAsync(new List<string>() { studentId }, false);
            return student != null ? student.GetPrimaryLocation(activeStudentPrograms) : null;
        }
    }
}
