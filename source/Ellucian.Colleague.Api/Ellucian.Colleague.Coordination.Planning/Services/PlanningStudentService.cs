﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Planning.Entities;
using Ellucian.Colleague.Domain.Planning.Repositories;
using Ellucian.Colleague.Domain.Planning.Services;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Entities.Requirements;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Ellucian.Colleague.Domain.Student;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Planning.Services
{
    [RegisterType]
    public class PlanningStudentService : StudentCoordinationService, IPlanningStudentService
    {
        private readonly IPlanningStudentRepository _planningStudentRepository;
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly ILogger logger;
        private readonly IConfigurationRepository _configurationRepository;

        public PlanningStudentService(IAdapterRegistry adapterRegistry, IPlanningStudentRepository planningStudentRepository, ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger, IStudentRepository studentRepository,
            IConfigurationRepository configurationRepository)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger, studentRepository, configurationRepository)
        {
            _configurationRepository = configurationRepository;
            _planningStudentRepository = planningStudentRepository;
            _adapterRegistry = adapterRegistry;
            this.logger = logger;
        }


        /// <summary>
        /// Get the PlanningStudent information for the given student ID. Throws exception if user is not authorized to
        /// access this student's information.
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns></returns>
        public async Task<Ellucian.Colleague.Dtos.Planning.PlanningStudent> GetPlanningStudentAsync(string studentId)
        {

            var planningStudentEntity = await _planningStudentRepository.GetAsync(studentId);

            // Determine if this user has the rights to access this student 

            if (!(UserIsSelf(studentId)))
            {
                // If permissions not found, this will throw a permissions exception. 
                await CheckUserAccessAsync(studentId, planningStudentEntity.ConvertToStudentAccess());
            }

            var adapter = _adapterRegistry.GetAdapter<Domain.Planning.Entities.PlanningStudent, Dtos.Planning.PlanningStudent>();
            var planningStudent = adapter.MapToType(planningStudentEntity);

            return planningStudent;
        }

        /// <summary>
        /// Get the list of skinny PlanningStudents information for the list of student ids
        /// </summary>
        /// <param name="studentIds">list of student Ids</param>
        /// <returns>list of planning students Dtos</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.Planning.PlanningStudent>> QueryPlanningStudentsAsync(IEnumerable<string> studentIds)
        {
            if (null == studentIds || studentIds.Count() == 0)
            { 
                throw new ArgumentException("ids", "You must specify at least 1 id to retrieve.");
            }
            List<Ellucian.Colleague.Dtos.Planning.PlanningStudent> planningStudents = new List<Ellucian.Colleague.Dtos.Planning.PlanningStudent>();
            if (!HasPermission(StudentPermissionCodes.ViewStudentInformation))
            {
                throw new PermissionsException("User does not have permissions to query students");
            }

            var planningStudentsEntity = await _planningStudentRepository.GetAsync(studentIds);
            var adapter = _adapterRegistry.GetAdapter<Domain.Planning.Entities.PlanningStudent, Dtos.Planning.PlanningStudent>();
            foreach (var planningStudentEntity in planningStudentsEntity)
            {
                var planningStudent = adapter.MapToType(planningStudentEntity);
                planningStudents.Add(planningStudent);
            }
            return planningStudents;
        }
    }
}
