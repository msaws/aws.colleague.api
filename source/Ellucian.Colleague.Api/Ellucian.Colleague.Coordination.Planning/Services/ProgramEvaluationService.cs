﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Planning.Entities;
using Ellucian.Colleague.Domain.Planning.Repositories;
using Ellucian.Colleague.Domain.Planning.Services;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Entities.Requirements;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Planning.Services
{
    [RegisterType]
    public class ProgramEvaluationService : StudentCoordinationService, IProgramEvaluationService
    {
        private const string _DefaultSortSpecId = "DEFAULT";
        private readonly IProgramRequirementsRepository _programRequirementsRepository;
        private readonly IRequirementRepository _requirementRepository;
        private readonly IStudentProgramRepository _studentProgramRepository;
        private readonly IAcademicCreditRepository _academicCreditRepository;
        private readonly IDegreePlanRepository _degreePlanRepository;
        private readonly ICourseRepository _courseRepository;
        private readonly IStudentRepository _studentRepository;
        private readonly IPlanningStudentRepository _planningStudentRepository;
        private readonly ITermRepository _termRepository;
        private readonly IRuleRepository _ruleRepository;
        private readonly IProgramRepository _programRepository;
        private readonly ICatalogRepository _catalogRepository;
        private readonly IPlanningConfigurationRepository _configurationRepository;
        private readonly IReferenceDataRepository _referenceDataRepository;
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly ILogger logger;
        private readonly IConfigurationRepository _baseConfigurationRepository;

        public ProgramEvaluationService(IAdapterRegistry adapterRegistry, IProgramRequirementsRepository programRequirementsRepository,
                                        IStudentRepository studentRepository, IPlanningStudentRepository planningStudentRepository, IStudentProgramRepository studentProgramRepository,
                                        IRequirementRepository requirementRepository, IAcademicCreditRepository academicCreditRepository,
                                        IDegreePlanRepository degreePlanRepository, ICourseRepository courseRepository, ITermRepository termRepository,
                                        IRuleRepository ruleRepository, IProgramRepository programRepository, ICatalogRepository catalogRepository, IPlanningConfigurationRepository configurationRepository,IReferenceDataRepository referenceDataRepository, ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger,
                                        IConfigurationRepository baseConfigurationRepository)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger, studentRepository, baseConfigurationRepository)
        {
            _baseConfigurationRepository = baseConfigurationRepository;
            _programRequirementsRepository = programRequirementsRepository;
            _requirementRepository = requirementRepository;
            _studentRepository = studentRepository;
            _planningStudentRepository = planningStudentRepository;
            _studentProgramRepository = studentProgramRepository;
            _academicCreditRepository = academicCreditRepository;
            _degreePlanRepository = degreePlanRepository;
            _courseRepository = courseRepository;
            _termRepository = termRepository;
            _ruleRepository = ruleRepository;
            _programRepository = programRepository;
            _catalogRepository = catalogRepository;
            _referenceDataRepository = referenceDataRepository;
            _configurationRepository = configurationRepository;
            _adapterRegistry = adapterRegistry;
            this.logger = logger;
        }

        /// <summary>
        /// Return a list of program evaluations for the given student and program
        /// </summary>
        /// <param name="studentId">Id of the student</param>
        /// <param name="programCodes">List of program codes</param>
        /// <returns>A list of <see cref="ProgramEvaluation">ProgramEvaluation</see> objects</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation>> EvaluateAsync(string studentId, List<string> programCodes)
        {
            IEnumerable<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation> evalResults;

            // Get the student so that permissions can be checked first
            Ellucian.Colleague.Domain.Planning.Entities.PlanningStudent student;
            student = await _planningStudentRepository.GetAsync(studentId);
            if (student == null)
            {
                throw new KeyNotFoundException("Student with ID " + studentId + " not found in the repository.");
            }

            //// First, check permissions for this function. If user is not the student id, verify permissions for another user.
            if (!(UserIsSelf(studentId)))
            {
                // If permissions not found, this will throw a permissions exception. 
                await CheckUserAccessAsync(studentId, student.ConvertToStudentAccess());
            }

            var watch = new Stopwatch();
            watch.Start();
            evalResults = await EvaluateProgramsAsync(studentId, programCodes, student);
            watch.Stop();
            logger.Info("EvaluationTiming: Completed EvaluatePrograms in " + watch.ElapsedMilliseconds.ToString() + " ms");

            return evalResults;
        }

        private async Task<IEnumerable<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation>> EvaluateProgramsAsync(string studentId, List<string> programCodes, Ellucian.Colleague.Domain.Planning.Entities.PlanningStudent student)
        {
            var evaluations = new List<Domain.Planning.Entities.ProgramEvaluation>();

            var watch = new Stopwatch();

            // Get the student's academic credits.
            List<AcademicCredit> credits = new List<AcademicCredit>();
            var creditsDict = await _academicCreditRepository.GetAcademicCreditByStudentIdsAsync(new List<string>() { student.Id });
            if (creditsDict.ContainsKey(student.Id))
            {
                credits = creditsDict[student.Id];
            }

            // List of courses needed for the evaluation (added for evaluating equated courses)
            IEnumerable<Course> courses = new List<Course>();
            courses = await _courseRepository.GetAsync();

            // Get the degree plan for this student
            DegreePlan degreePlan = null;
            if (student.DegreePlanId != null)
            {
                degreePlan = await _degreePlanRepository.GetAsync((int)student.DegreePlanId);
                if (degreePlan == null)
                {
                    throw new KeyNotFoundException("DegreePlan");
                }
            }

            // Get all terms for later use.
            var terms = await _termRepository.GetAsync();

            // Get all of the student's programs
            var studentPrograms = await _studentProgramRepository.GetStudentProgramsByIdsAsync(new List<string>() { student.Id });
            if (studentPrograms == null || studentPrograms.Count() == 0)
            {
                throw new KeyNotFoundException("StudentPrograms");
            }

            foreach (var programCode in programCodes)
            {
                IEnumerable<AcademicCredit> FilteredCredits;
                StudentProgram StudentProgram;
                Program Program;
                var EvaluationPlannedCourses = new List<PlannedCredit>();
                List<Override> Overrides = new List<Override>();

                ProgramEvaluation eval = null;

                // Get the program itself
                Program = await _programRepository.GetAsync(programCode);

                // Apply the program's credit filter ("Transcript Grouping")
                FilteredCredits = credits.Where(cr => Program.CreditFilter.Passes(cr)).ToList();

                // Get the student's program from the list of all of the student's programs
                StudentProgram = studentPrograms.Where(sp => sp.ProgramCode == programCode).FirstOrDefault();

                string catalogCode = null;
                if (StudentProgram == null)
                {
                    // If the student program isn't found, set up for a "what-if?" evaluation.
                    // Determine the program catalog year to use.
                    PlanningConfiguration planningConfiguration = await _configurationRepository.GetPlanningConfigurationAsync();
                    ICollection<Catalog> catalogs = await _catalogRepository.GetAsync();
                    catalogCode = ProgramCatalogService.DeriveDefaultCatalog(Program, studentPrograms, catalogs, planningConfiguration.DefaultCatalogPolicy);

                    // Build a temporary student program for what-if evaluation
                    // Note: Catalog code above could come back null - constructor for the new program will throw an exception.
                    StudentProgram = new StudentProgram(studentId, programCode, catalogCode);
                }

                // Get the student's overrides (if any)
                if (StudentProgram.Overrides != null && StudentProgram.Overrides.Count() > 0)
                {
                    Overrides = StudentProgram.Overrides.ToList();
                }

                // Get only the planned courses that are not in credits and will be complete by the given date
                if (degreePlan != null)
                {
                    // Get the planned courses that should be considered by this evaluation
                    var UnfilteredPlannedCourses = degreePlan.GetCoursesForValidation(terms, null, FilteredCredits, courses);

                    // Apply the parts of the CreditFilter that can apply to a course
                    EvaluationPlannedCourses = UnfilteredPlannedCourses.Where(upc => Program.CreditFilter.Passes(upc.Course)).ToList();

                    IDictionary<Course, Tuple<IEnumerable<Department>, IEnumerable<string>>> deptLookup = await CreateTranscriptGroupingLookupsAsync(EvaluationPlannedCourses);
                    
                    //validate creditFilter for divisions, schools by passing departments with respect to that course
                    EvaluationPlannedCourses = EvaluationPlannedCourses.Where(upc => Program.CreditFilter.Passes(deptLookup[upc.Course])).ToList();
                }

                // If the client has modified the sort spec, be sure DEFAULT is in the list of sortSpecIds
                var degreeAuditParameters = await _requirementRepository.GetDegreeAuditParametersAsync();
                bool modifiedSortSpec = degreeAuditParameters != null ? degreeAuditParameters.ModifiedDefaultSort : false;

                // Get the main requirements from the student's program
                ProgramRequirements ProgramRequirements = null;
                try
                {
                    ProgramRequirements = await _programRequirementsRepository.GetAsync(StudentProgram.ProgramCode, StudentProgram.CatalogCode);
                    /// If the Default sort spec has been modified, apply DEFAULT to all requirements missing a sort spec and then cascade it down to lower levels missing a sort spec
                    ApplyDefaultSortSpecWhereApplicable(modifiedSortSpec, ProgramRequirements.Requirements);
                }
                catch
                {
                    // Requirements not found: Null program requirements will result in exit with empty program.
                }

                // Get any additional requirements
                List<Requirement> AdditionalRequirements = new List<Requirement>();
                var additionalRequirementIds = StudentProgram.AdditionalRequirements.Where(r => !string.IsNullOrEmpty(r.RequirementCode)).Select(r => r.RequirementCode).ToList();
                if (additionalRequirementIds.Count() > 0)
                {
                    // Make sure to apply default sort specifications if it has been modified.
                    AdditionalRequirements = (await _requirementRepository.GetAsync(additionalRequirementIds)).ToList();

                    /// If the Default sort spec has been modified, apply DEFAULT to all addl requirements missing a sort spec 
                    ApplyDefaultSortSpecWhereApplicable(modifiedSortSpec, AdditionalRequirements);
                }

                // If we don't have any requirements or additional requirements then create an empty program evaluation for this program.
                if (ProgramRequirements == null || StudentProgram == null ||
                    (ProgramRequirements.Requirements == null && ProgramRequirements.Requirements.Count() == 0) &&
                    (StudentProgram.AdditionalRequirements == null && StudentProgram.AdditionalRequirements.Count() == 0))
                {
                    // Create a blank evaluation result and loop to the next program code.
                    eval = new ProgramEvaluation(new List<AcademicCredit>(), programCode, StudentProgram != null ? StudentProgram.CatalogCode : string.Empty);
                    evaluations.Add(eval);
                    continue;
                }

                // Build rules (if any) and evaluate the program.

                // Get rules for program requirements
                var allRules = new List<RequirementRule>();

                if (ProgramRequirements != null)
                {
                    allRules.AddRange(ProgramRequirements.GetAllRules());
                }
                // Get rules for additional requirements
                if (AdditionalRequirements != null)
                {
                    foreach (var req in AdditionalRequirements)
                    {
                        allRules.AddRange(req.GetAllRules());
                    }
                }

                var creditRequests = new List<RuleRequest<AcademicCredit>>();

                // Run all credits against credit rules
                foreach (var credit in credits)
                {
                    var creditRules = allRules.Where(rr => rr.CreditRule != null).ToList();
                    foreach (var rule in creditRules)
                    {
                        creditRequests.Add(new RuleRequest<AcademicCredit>(rule.CreditRule, credit));
                    }
                }

                // Create hard list of rules that are course rules
                var courseRules = allRules.Where(rr => rr.CourseRule != null).ToList();

                // Run all courses from credits against course rules
                var courseRequests = new List<RuleRequest<Course>>();
                var creditsWithCourse = credits.Where(stc => stc.Course != null).Select(stc => stc.Course).ToList();
                foreach (var rule in courseRules)
                {
                    foreach (var creditCourse in creditsWithCourse)
                    {
                        courseRequests.Add(new RuleRequest<Course>(rule.CourseRule, creditCourse));
                    }
                }

                // Run all planned courses against course rules
                foreach (var rule in courseRules)
                {
                    foreach (var plannedCourse in EvaluationPlannedCourses)
                    {
                        courseRequests.Add(new RuleRequest<Course>(rule.CourseRule, plannedCourse.Course));
                    }
                }

                // Execute all the rules. The rule repository will not need to get the ones with .NET expressions
                var courseResults = await _ruleRepository.ExecuteAsync(courseRequests);
                var creditResults = await _ruleRepository.ExecuteAsync(creditRequests);
                var ruleResults = courseResults.Union(creditResults);

                Dictionary<string, List<AcademicCredit>> filteredCreditsDict = null;
                if (FilteredCredits != null && FilteredCredits.Any())
                {
                    // If the student has any academic credits, build a dictionary of sorted filtered credits, using only unique, non-null/non-empty spec IDs
                var reqSortSpecs = ProgramRequirements.Requirements.Select(r => r.SortSpecificationId).ToList();
                var subreqSortSpecs = ProgramRequirements.Requirements.SelectMany(r => r.SubRequirements).Select(sub => sub.SortSpecificationId).ToList();
                var groupSortSpecs = ProgramRequirements.Requirements.SelectMany(r => r.SubRequirements).SelectMany(sub => sub.Groups).Select(g => g.SortSpecificationId).ToList();
                var additionalReqSortSpecs = AdditionalRequirements.Select(ar => ar.SortSpecificationId).ToList();
                var sortSpecIds = reqSortSpecs.Union(subreqSortSpecs).Union(groupSortSpecs).Union(additionalReqSortSpecs).Where(id => !string.IsNullOrEmpty(id)).Distinct().ToList();

                    if (modifiedSortSpec && !sortSpecIds.Contains(_DefaultSortSpecId))
                    {
                        sortSpecIds.Add(_DefaultSortSpecId);
                    }
                    try
                    {
                        filteredCreditsDict = await _academicCreditRepository.GetSortedAcademicCreditsBySortSpecificationIdAsync(FilteredCredits, sortSpecIds);
                    }
                    catch (Exception)
                    {
                        // Either there are no credits to sort or it couldn't sort them using the sort specs. Either way, continue without sort overrides.
                    }
                }
                // All pieces in place, run the eval
                var ProgramEvaluator = new ProgramEvaluator(StudentProgram, ProgramRequirements, AdditionalRequirements, FilteredCredits, EvaluationPlannedCourses, ruleResults, Overrides, courses, logger);

                if (logger.IsInfoEnabled) watch.Restart();

                eval = ProgramEvaluator.Evaluate(filteredCreditsDict);

                if (logger.IsInfoEnabled)
                {
                    watch.Stop();
                    logger.Info("EvaluationTiming: (EvaluatePrograms)(programCode " + programCode + ") Completed ProgramEvaluator.Evaluate in " + watch.ElapsedMilliseconds.ToString() + " ms");
                }

                // Optimize credit/course application
                List<string> groupsToSkip = new List<string>();
                List<string> subsToSkip = new List<string>();

                if (OptimizeEval(eval, groupsToSkip, subsToSkip))
                {
                    if (groupsToSkip.Count > 0) { ProgramEvaluator.AddGroupsToSkip(groupsToSkip); }
                    if (subsToSkip.Count > 0) { ProgramEvaluator.AddSubRequirementsToSkip(subsToSkip); }
                    eval = null;

                    watch.Restart();

                    eval = ProgramEvaluator.Evaluate(filteredCreditsDict);

                    watch.Stop();
                    logger.Info("EvaluationTiming: (EvaluatePrograms)(programCode " + programCode + ") Completed OptimizeEval ProgramEvaluator.Evaluate in " + watch.ElapsedMilliseconds.ToString() + " ms");
                }

                evaluations.Add(eval);

            }

            return evaluations;
        }

        // Identifies the list of groups and subrequirements that do not need to have items applied to them because the requirement or subrequirement
        // (respectively) is satisfied. Create a list of these items that can be ignored the next time through the evaluation.
        private bool OptimizeEval(Domain.Planning.Entities.ProgramEvaluation eval, List<string> groupsToSkip, List<string> subsToSkip)
        {
            // If there are any requirements that are satisfied and don't require all of their subrequirements to be completed
            var satisfiedReqsWithEnoughSubreqs = eval.RequirementResults.Where(rq => rq.IsSatisfied() && (rq.Requirement.MinSubRequirements < rq.Requirement.SubRequirements.Count)).ToList();
            foreach (var req in satisfiedReqsWithEnoughSubreqs)
            {
                // For what its worth, the first pass would have stopped evaluating groups as soona as the requirement was satisfied, but there may be some stray
                // partially satisfied subrequirements from which we can free up courses/credits.
                var unsatisfiedSubreqs = req.SubRequirementResults.Where(sb => !sb.IsSatisfied()).ToList();
                foreach (var sub in unsatisfiedSubreqs)
                {
                    // just for clarity's sake, let's only "skip" this if it had something applied to it, otherwise
                    // the optimizer will look like it is doing this a lot more often than it really needs to
                    if (sub.PlanningStatus == PlanningStatus.PartiallyPlanned || sub.CompletionStatus != CompletionStatus.PartiallyCompleted)
                    {
                        subsToSkip.Add(sub.SubRequirement.Id);
                    }
                }
            }

            // Same for subrequirements/groups
            var satisfiedSubreqsWithEnoughGroups = eval.RequirementResults.SelectMany(rq => rq.SubRequirementResults).Where(sb => sb.IsSatisfied() && (sb.SubRequirement.MinGroups < sb.SubRequirement.Groups.Count)).ToList();
            foreach (var sub in satisfiedSubreqsWithEnoughGroups)
            {
                var unsatisfiedGroups = sub.GroupResults.Where(gr => !gr.IsSatisfied()).ToList();
                foreach (var grp in unsatisfiedGroups)
                {
                    if (grp.PlanningStatus == PlanningStatus.PartiallyPlanned || grp.CompletionStatus == CompletionStatus.PartiallyCompleted)
                    {
                        groupsToSkip.Add(grp.Group.Id);
                    }
                }
            }

            return (groupsToSkip.Count + subsToSkip.Count > 0);

        }

        /// <summary>
        /// Gets notices pertaining to the evaluation for a given student and program.
        /// </summary>
        /// <param name="studentId">Id of the student</param>
        /// <param name="programCode">Code of the program</param>
        /// <returns>List of <see cref="EvaluationNotice">EvaluationNotice</see> Dtos</returns>
        public async Task<IEnumerable<Dtos.Planning.EvaluationNotice>> GetEvaluationNoticesAsync(string studentId, string programCode)
        {
            List<Dtos.Planning.EvaluationNotice> noticeDtos = new List<Dtos.Planning.EvaluationNotice>();

            // First, check permissions for this function. If user is not the student id, read student and verify permissions
            // for another user.
            if (!(UserIsSelf(studentId)))
            {
                // If permissions not found, this will throw a permissions exception. 
               await CheckUserAccessAsync(studentId);
            }

            // If still here... continue on to get student program notices from the repository, convert to dto
            var notices = await _studentProgramRepository.GetStudentProgramEvaluationNoticesAsync(studentId, programCode);
            if (notices != null)
            {
                var noticeDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.EvaluationNotice, Dtos.Planning.EvaluationNotice>();

                foreach (var notice in notices)
                {
                    try
                    {
                        noticeDtos.Add(noticeDtoAdapter.MapToType(notice));
                    }
                    catch
                    {
                        var message = "Error converting notice type " + notice.Type.ToString() + " for student " + studentId + " program " + programCode;
                        logger.Error(message);
                        throw new Exception(message);
                    }
                }
            }
            return noticeDtos;
        }

        private async Task<IDictionary<Course, Tuple<IEnumerable<Department>, IEnumerable<string>>>> CreateTranscriptGroupingLookupsAsync(IEnumerable<PlannedCredit> EvaluationPlannedCourses)
        {
            try
            {
                //Filter planned courses on divisions and schools. Division and Schools are set at department level
                //first retrieve departments details only for the planned courses
                //create a dictionary beforehand with course and departments associated with those course
                IEnumerable<Department> departments = (await _referenceDataRepository.DepartmentsAsync());
                IDictionary<Course, Tuple<IEnumerable<Department>, IEnumerable<string>>> deptLookup = new Dictionary<Course, Tuple<IEnumerable<Department>, IEnumerable<string>>>();
                foreach (PlannedCredit pc in EvaluationPlannedCourses)
                {
                    List<string> schools = new List<string>();
                    IEnumerable<string> courseDepartments = pc.Course.DepartmentCodes.Distinct();
                    IEnumerable<Department> academicDepartments = departments.Where(d => courseDepartments.Contains(d.Code));
                    if (!deptLookup.ContainsKey(pc.Course))
                    {
                        //get schools from divisions

                        IEnumerable<string> divisions = academicDepartments.Where(ad => !string.IsNullOrEmpty(ad.Division)).Select(a => a.Division);
                        if (divisions != null && divisions.Any())
                        {
                            //retrieve schools from divisions
                            List<Division> allDivisions = (await _referenceDataRepository.GetDivisionsAsync(false)).Where(d => divisions.Contains(d.Code)).ToList();
                            if (allDivisions != null && allDivisions.Any())
                            {
                                schools.AddRange(allDivisions.Select(d => d.SchoolCode).Distinct());
                            }
                        }

                        schools = schools.Union(academicDepartments.Where(d => !string.IsNullOrEmpty(d.School)).Select(d => d.School).Distinct()).ToList<string>();



                        deptLookup.Add(pc.Course, Tuple.Create<IEnumerable<Department>, IEnumerable<string>>(academicDepartments, schools));
                    }

                }
                return deptLookup;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Determine if the DEFAULT sort spec Id should be applied to any requirements that do not have a sort spec Id of there own.
        /// </summary>
        /// <param name="modifiedSortSpec"></param>
        /// <param name="requirements"></param>
        private void ApplyDefaultSortSpecWhereApplicable(bool modifiedSortSpec, IEnumerable<Requirement> requirements)
        {
            if (modifiedSortSpec && requirements != null && requirements.Any())
            {
                foreach (var req in requirements)
                {
                    if (string.IsNullOrEmpty(req.SortSpecificationId))
                    {
                        req.SortSpecificationId = _DefaultSortSpecId;
                        req.CascadeSortSpecificationWhenNecessary();
                    }
                }

            }
        }
    }
}
