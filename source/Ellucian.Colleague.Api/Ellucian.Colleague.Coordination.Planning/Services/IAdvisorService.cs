﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Coordination.Base;
using Ellucian.Colleague.Dtos.Planning;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Planning.Services
{
    public interface IAdvisorService
    {
        Task<Advisor> GetAdvisorAsync(string id);
        Task<PrivacyWrapper<List<Advisee>>> GetAdviseesAsync(string id, int pageSize = int.MaxValue, int pageIndex = 1);
        Task<IEnumerable<string>> SearchAsync(string searchString, int pageSize = int.MaxValue, int pageIndex = 1);
        Task<IEnumerable<Advisee>> Search2Async(string searchString, int pageSize = int.MaxValue, int pageIndex = 1);
        Task<PrivacyWrapper<List<Advisee>>> Search3Async(Dtos.Planning.AdviseeSearchCriteria criteria, int pageSize = int.MaxValue, int pageIndex = 1);
        Task<IEnumerable<string>> GetAdvisorPermissionsAsync();
        Task<PrivacyWrapper<Advisee>> GetAdviseeAsync(string advisorId, string adviseeId);
        Task<IEnumerable<Advisor>> GetAdvisorsAsync(AdvisorQueryCriteria advisorQueryCriteria);

    }
}
