//Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    /// <summary>
    /// Interface for ShipToDestinations services
    /// </summary>
    public interface IShipToDestinationsService
    {
          
        Task<IEnumerable<Ellucian.Colleague.Dtos.ShipToDestinations>> GetShipToDestinationsAsync(bool bypassCache = false);
               
        Task<Ellucian.Colleague.Dtos.ShipToDestinations> GetShipToDestinationsByGuidAsync(string id);
    }
}
