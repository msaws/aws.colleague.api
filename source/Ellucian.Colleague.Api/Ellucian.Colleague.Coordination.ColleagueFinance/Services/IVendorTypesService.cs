// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    /// <summary>
    /// Interface to VendorTypes service
    /// </summary>
    public interface IVendorTypesService
    {
        Task<IEnumerable<Ellucian.Colleague.Dtos.VendorType>> GetVendorTypesAsync(bool bypassCache);
        Task<Ellucian.Colleague.Dtos.VendorType> GetVendorTypeByIdAsync(string id);
    }
}
