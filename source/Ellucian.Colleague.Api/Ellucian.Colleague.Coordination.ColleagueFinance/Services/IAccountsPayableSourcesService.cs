﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Colleague.Dtos;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    /// <summary>
    /// Interface for AccountsPayableSources services
    /// </summary>
    public interface IAccountsPayableSourcesService
    {
        Task<IEnumerable<AccountsPayableSources>> GetAccountsPayableSourcesAsync(bool bypassCache = false);
        Task<AccountsPayableSources> GetAccountsPayableSourcesByGuidAsync(string id);
    }
}
