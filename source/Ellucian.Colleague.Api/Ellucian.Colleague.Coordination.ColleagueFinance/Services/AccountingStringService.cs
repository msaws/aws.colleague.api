﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Web.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Web.Adapters;
using slf4net;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    [RegisterType]
    public class AccountingStringService : BaseCoordinationService, IAccountingStringService
    {
        private readonly IAccountingStringRepository _accountingStringRepository;
        private readonly IColleagueFinanceReferenceDataRepository _colleagueFinanceReferenceDataRepository;
        private readonly ILogger _logger;

        // This constructor initializes the private attributes.
        public AccountingStringService(IAccountingStringRepository accountingStringRepository, 
            IColleagueFinanceReferenceDataRepository colleagueFinanceReferenceDataRepository,
            IAdapterRegistry adapterRegistry, ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            _accountingStringRepository = accountingStringRepository;
            _colleagueFinanceReferenceDataRepository = colleagueFinanceReferenceDataRepository;
            _logger = logger;
        }

        #region Accounting String
        /// <summary>
        /// Gets accountingstring by filter criteria
        /// </summary>
        /// <param name="accountingStringValue">accounting string to filter by</param>
        /// <param name="validOn">date to check if valid on</param>
        /// <returns>accounting string if found and valid</returns>
        public async Task<Dtos.AccountingString> GetAccoutingStringByFilterCriteriaAsync(string accountingStringValue, DateTime? validOn = null)
        {
            try
            {
                //default accountingString to what just came in
                var accountingString = string.Empty;
                //default project number to empty
                string projectNumber = string.Empty;

                //check if the incoming string contains a * which is the expected delimeter for the split between account string and project number
                if (accountingStringValue.Contains("*"))
                {
                    var accountNumberWithProject = accountingStringValue.Split("*".ToCharArray());
                    accountingString = accountNumberWithProject[0].ToString().Replace("-", "_");
                    projectNumber = accountNumberWithProject[1].ToString();
                }
                else
                {
                    accountingString = accountingStringValue.Replace("-", "_"); 
                }

                //create accountingString entity with transformed input filters
                var accountingStringEntity = new Domain.ColleagueFinance.Entities.AccountingString(accountingString, projectNumber, validOn);

                //run validation method from repository, will return the entity if validation succeeded, will error if fails
                var validationResult = await _accountingStringRepository.GetValidAccountingString(accountingStringEntity);

                return new Dtos.AccountingString()
                {
                    AccountingStringValue = accountingStringValue,
                    Description = validationResult.Description
                };
            }
            catch (RepositoryException repositoryException)
            {
                throw repositoryException;
            }
            catch (ArgumentNullException argumentNullException)
            {
                throw argumentNullException;
            }
            catch (Exception exception)
            {
                _logger.Error(exception, "Unexpected Error in AccountingString Service");
                throw exception;
            }
        }

        #endregion

        #region Accounting String Components

        /// <remarks>FOR USE WITH ELLUCIAN DATA MODEL</remarks>
        /// <summary>
        /// Gets all accounting-string-components
        /// </summary>
        /// <returns>Collection of AccountingStringComponents DTO objects</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.AccountingStringComponent>> GetAccountingStringComponentsAsync(bool bypassCache = false)
        {
            var accountingStringComponentsCollection = new List<Ellucian.Colleague.Dtos.AccountingStringComponent>();

            var accountingStringComponentsEntities = await _colleagueFinanceReferenceDataRepository.GetAccountComponentsAsync(bypassCache);
            if (accountingStringComponentsEntities != null && accountingStringComponentsEntities.Any())
            {
                foreach (var accountingStringComponents in accountingStringComponentsEntities)
                {
                    accountingStringComponentsCollection.Add(ConvertAccountComponentsEntityToDto(accountingStringComponents));
                }
            }
            return accountingStringComponentsCollection;
        }

        /// <remarks>FOR USE WITH ELLUCIAN DATA MODEL</remarks>
        /// <summary>
        /// Get a AccountingStringComponents from its GUID
        /// </summary>
        /// <returns>AccountingStringComponents DTO object</returns>
        public async Task<Ellucian.Colleague.Dtos.AccountingStringComponent> GetAccountingStringComponentsByGuidAsync(string guid)
        {
            try
            {
                return ConvertAccountComponentsEntityToDto((await _colleagueFinanceReferenceDataRepository.GetAccountComponentsAsync(true)).Where(r => r.Guid == guid).First());
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException("accounting-string-components not found for GUID " + guid, ex);
            }
            catch (InvalidOperationException ex)
            {
                throw new KeyNotFoundException("accounting-string-components not found for GUID " + guid, ex);
            }
        }


        /// <remarks>FOR USE WITH ELLUCIAN DATA MODEL</remarks>
        /// <summary>
        /// Converts a AccountComponents domain entity to its corresponding AccountingStringComponents DTO
        /// </summary>
        /// <param name="source">AccountComponents domain entity</param>
        /// <returns>AccountingStringComponent DTO</returns>
        private Ellucian.Colleague.Dtos.AccountingStringComponent ConvertAccountComponentsEntityToDto(AccountComponents source)
        {
            var accountingStringComponents = new Ellucian.Colleague.Dtos.AccountingStringComponent
            {
                Id = source.Guid,
                Code = source.Code,
                Title = source.Description,
                Description = null
            };

            return accountingStringComponents;
        }
        #endregion

        #region Accounting String Formats

        /// <remarks>FOR USE WITH ELLUCIAN EEDM VERSION 8</remarks>
        /// <summary>
        /// Gets all accounting-string-formats
        /// </summary>
        /// <returns>Collection of AccountingStringFormats DTO objects</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.AccountingStringFormats>> GetAccountingStringFormatsAsync(bool bypassCache = false)
        {
            var accountingStringFormatsCollection = new List<Ellucian.Colleague.Dtos.AccountingStringFormats>();

            var accountingStringFormatsEntities = await _colleagueFinanceReferenceDataRepository.GetAccountFormatsAsync(bypassCache);

            var accountingComponents = await _colleagueFinanceReferenceDataRepository.GetAccountComponentsAsync(true);

            if (accountingStringFormatsEntities != null && accountingStringFormatsEntities.Any())
            {
                foreach (var accountingStringFormats in accountingStringFormatsEntities)
                {
                    accountingStringFormatsCollection.Add(ConvertAccountingStringFormatsEntityToDto(accountingStringFormats, accountingComponents));
                }
            }
            return accountingStringFormatsCollection;
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM VERSION 7</remarks>
        /// <summary>
        /// Get a AccountingStringFormats from its GUID
        /// </summary>
        /// <returns>AccountingStringFormats DTO object</returns>
        public async Task<Ellucian.Colleague.Dtos.AccountingStringFormats> GetAccountingStringFormatsByGuidAsync(string guid)
        {
            try
            {
                var accountingComponents = await _colleagueFinanceReferenceDataRepository.GetAccountComponentsAsync(true);
                var asf = (await _colleagueFinanceReferenceDataRepository.GetAccountFormatsAsync(true)).Where(r => r.Guid == guid).First();
                return ConvertAccountingStringFormatsEntityToDto(asf, accountingComponents);
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException("accounting-string-formats not found for GUID " + guid, ex);
            }
            catch (InvalidOperationException ex)
            {
                throw new KeyNotFoundException("accounting-string-formats not found for GUID " + guid, ex);
            }
        }


        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Converts a AccountingStringFormats domain entity to its corresponding AccountingStringFormats DTO
        /// </summary>
        /// <param name="source">AccountingStringFormats domain entity</param>
        /// <returns>AccountingStringFormats DTO</returns>
        private Ellucian.Colleague.Dtos.AccountingStringFormats ConvertAccountingStringFormatsEntityToDto(AccountingFormat source, IEnumerable<AccountComponents> accountingComponents)
        {
            var accountingStringFormats = new Ellucian.Colleague.Dtos.AccountingStringFormats();

            accountingStringFormats.Id = source.Guid;
            accountingStringFormats.Delimiter = "*";

            accountingStringFormats.Components = new List<Components>();

            int x = 1;
            foreach(var ac in accountingComponents)
            {
                var c = new Dtos.Components() {Component = new GuidObject2(ac.Guid), order = x };
                accountingStringFormats.Components.Add(c);
                x++;
            }

            return accountingStringFormats;
        }


        #endregion

        #region Accounting string components values

        /// <remarks>FOR USE WITH ELLUCIAN EEDM VERSION 7</remarks>
        /// <summary>
        /// Gets all accounting-string-component-values
        /// </summary>
        /// <returns>Collection of AccountingStringComponentValues DTO objects</returns>
        public async Task<Tuple<IEnumerable<Ellucian.Colleague.Dtos.AccountingStringComponentValues>, int>> GetAccountingStringComponentValuesAsync(int offset, int limit, string component, string transactionStatus, string typeAccount, string typeFund,bool bypassCache = false)
        {
            var accountingStringComponentValuesCollection = new List<Ellucian.Colleague.Dtos.AccountingStringComponentValues>();

            if (!string.IsNullOrEmpty(typeFund))
            {
                return new Tuple<IEnumerable<Dtos.AccountingStringComponentValues>, int>(accountingStringComponentValuesCollection, 0);
            }

            var accountingComponents = await _colleagueFinanceReferenceDataRepository.GetAccountComponentsAsync(true);

            string guidComponent = string.Empty;
            if (!string.IsNullOrEmpty(component))
            {
                try
                {
                    guidComponent = accountingComponents.FirstOrDefault(x => x.Guid == component).Code;
                }catch (Exception e)
                {
                    return new Tuple<IEnumerable<Dtos.AccountingStringComponentValues>, int>(accountingStringComponentValuesCollection, 0);
                }
                
                
            }

            var accountingStringComponentValuesEntities = await _colleagueFinanceReferenceDataRepository.GetAccountingStringComponentValuesAsync(
                offset, limit, guidComponent, transactionStatus, typeAccount, typeFund, bypassCache);

            if (accountingStringComponentValuesEntities.Item1 != null && accountingStringComponentValuesEntities.Item1.Any())
            {
                foreach (var accountingStringComponentValues in accountingStringComponentValuesEntities.Item1)
                {
                    accountingStringComponentValuesCollection.Add(ConvertAccountingStringComponentValuesEntityToDto(accountingStringComponentValues, accountingComponents));
                }
            }
            return new Tuple<IEnumerable<Dtos.AccountingStringComponentValues>, int>(accountingStringComponentValuesCollection, accountingStringComponentValuesEntities.Item2);
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM VERSION 7</remarks>
        /// <summary>
        /// Get a AccountingStringComponentValues from its GUID
        /// </summary>
        /// <returns>AccountingStringComponentValues DTO object</returns>
        public async Task<Ellucian.Colleague.Dtos.AccountingStringComponentValues> GetAccountingStringComponentValuesByGuidAsync(string guid)
        {
            try
            {
                var accountingComponents = await _colleagueFinanceReferenceDataRepository.GetAccountComponentsAsync(true);
                return ConvertAccountingStringComponentValuesEntityToDto((await _colleagueFinanceReferenceDataRepository.GetAccountingStringComponentValueByGuid(guid)), accountingComponents);
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException("accounting-string-component-values not found for GUID " + guid, ex);
            }
            catch (InvalidOperationException ex)
            {
                throw new KeyNotFoundException("accounting-string-component-values not found for GUID " + guid, ex);
            }
        }


        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Converts a AccountingStringComponentValues domain entity to its corresponding AccountingStringComponentValues DTO
        /// </summary>
        /// <param name="source">AccountingStringComponentValues domain entity</param>
        /// <returns>AccountingStringComponentValues DTO</returns>
        private Ellucian.Colleague.Dtos.AccountingStringComponentValues ConvertAccountingStringComponentValuesEntityToDto(Domain.ColleagueFinance.Entities.AccountingStringComponentValues source, IEnumerable<AccountComponents> accountingComponents)
        {
            var accountingStringComponentValues = new Ellucian.Colleague.Dtos.AccountingStringComponentValues();

            accountingStringComponentValues.Id = source.Guid;
            var accountingString = source.AccountNumber.Replace("_", "-");
            accountingStringComponentValues.Value = accountingString;
            accountingStringComponentValues.Description = source.Description;

            switch (source.AccountDef)
            {
                case "GL":
                    accountingStringComponentValues.Component = new GuidObject2(accountingComponents.FirstOrDefault(x => x.Code == "GL.ACCT").Guid);
                    break;
                case "Project":
                    accountingStringComponentValues.Component = new GuidObject2(accountingComponents.FirstOrDefault(x => x.Code == "PROJECT").Guid.ToString());
                    break;
            }
            
            switch (source.Status)
            {
                case "available":
                    accountingStringComponentValues.TransactionStatus = Dtos.EnumProperties.AccountingTransactionStatus.available;
                    break;
                case "unavailable":
                    accountingStringComponentValues.TransactionStatus = Dtos.EnumProperties.AccountingTransactionStatus.unavailable;
                    break;
            }

            accountingStringComponentValues.Type = new AccountingStringComponentValuesType();
            switch (source.Type)
            {
                case "asset":
                    accountingStringComponentValues.Type.Account = Dtos.EnumProperties.AccountingTypeAccount.asset;
                    break;
                case "liability":
                    accountingStringComponentValues.Type.Account = Dtos.EnumProperties.AccountingTypeAccount.liability;
                    break;
                case "fundBalance":
                    accountingStringComponentValues.Type.Account = Dtos.EnumProperties.AccountingTypeAccount.fundBalance;
                    break;
                case "revenue":
                    accountingStringComponentValues.Type.Account = Dtos.EnumProperties.AccountingTypeAccount.revenue;
                    break;
                case "expense":
                    accountingStringComponentValues.Type.Account = Dtos.EnumProperties.AccountingTypeAccount.expense;
                    break;
            }

            return accountingStringComponentValues;
        }

        #endregion Accounting string components values

    }
}