// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Web.Dependency;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    [RegisterType]
    public class VendorTypesService : IVendorTypesService
    {
        private readonly IColleagueFinanceReferenceDataRepository _cfReferenceDataRepository;
        private readonly ILogger _logger;

        public VendorTypesService(IColleagueFinanceReferenceDataRepository cfReferenceDataRepository, ILogger logger)
        {
            _cfReferenceDataRepository = cfReferenceDataRepository;
            _logger = logger;
        }
        #region ICommodityUnitTypesService Members

        /// <summary>
        /// Returns all Vendor types
        /// </summary>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.VendorType>> GetVendorTypesAsync(bool bypassCache)
        {
            var vendorTypeCollection = new List<Ellucian.Colleague.Dtos.VendorType>();

            var vendorTypes = await _cfReferenceDataRepository.GetVendorTypesAsync(bypassCache);
            if (vendorTypes != null && vendorTypes.Any())
            {
                foreach (var vendorType in vendorTypes)
                {
                    vendorTypeCollection.Add(ConvertVendorTypeEntityToDto(vendorType));
                }
            }
            return vendorTypeCollection;
        }

        /// <summary>
        /// Returns an vendor type from an ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Dtos.VendorType> GetVendorTypeByIdAsync(string id)
        {
            var vendorTypeEntity = (await _cfReferenceDataRepository.GetVendorTypesAsync(true)).FirstOrDefault(ct => ct.Guid == id);
            if (vendorTypeEntity == null)
            {
                throw new KeyNotFoundException("Vendor Type is not found.");
            }

            var vendorType = ConvertVendorTypeEntityToDto(vendorTypeEntity);
            return vendorType;
        }
        #endregion

        #region Convert method(s)

        /// <summary>
        /// Converts from VendorType entity to VendorType dto
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private Dtos.VendorType ConvertVendorTypeEntityToDto(VendorType source)
        {
            Dtos.VendorType vendorType = new Dtos.VendorType();
            vendorType.Id = source.Guid;
            vendorType.Code = source.Code;
            vendorType.Title = source.Description;
            vendorType.Description = string.Empty;
            return vendorType;
        }

        #endregion
    }
}
