﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    /// <summary>
    /// Interface for AccountsPayableInvoices services
    /// </summary>
    public interface IAccountsPayableInvoicesService : IBaseService
    {
        Task<Tuple<IEnumerable<Ellucian.Colleague.Dtos.AccountsPayableInvoices>, int>> GetAccountsPayableInvoicesAsync(int offset, int limit, bool bypassCache = false);
        Task<Ellucian.Colleague.Dtos.AccountsPayableInvoices> GetAccountsPayableInvoicesByGuidAsync(string guid);

        Task<Ellucian.Colleague.Dtos.AccountsPayableInvoices> PutAccountsPayableInvoicesAsync(string guid, Ellucian.Colleague.Dtos.AccountsPayableInvoices accountsPayableInvoices);
        Task<Ellucian.Colleague.Dtos.AccountsPayableInvoices> PostAccountsPayableInvoicesAsync(Ellucian.Colleague.Dtos.AccountsPayableInvoices accountsPayableInvoices);
    }
}
