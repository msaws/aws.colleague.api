﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    /// <summary>
    /// Interface for Vendors services
    /// </summary>
    public interface IVendorsService : IBaseService
    {
        Task<Tuple<IEnumerable<Ellucian.Colleague.Dtos.Vendors>, int>> GetVendorsAsync(int offset, int limit,
            string vendorDetail = "", string classifications = "", string status = "", bool bypassCache = false);
        Task<Ellucian.Colleague.Dtos.Vendors> GetVendorsByGuidAsync(string id);

        Task<Ellucian.Colleague.Dtos.Vendors> PutVendorAsync(string guid, Ellucian.Colleague.Dtos.Vendors vendor);
        Task<Ellucian.Colleague.Dtos.Vendors> PostVendorAsync(Ellucian.Colleague.Dtos.Vendors vendor);
   
    }
}
