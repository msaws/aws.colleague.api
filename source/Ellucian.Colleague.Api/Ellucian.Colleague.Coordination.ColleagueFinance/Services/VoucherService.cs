﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Coordination.ColleagueFinance.Adapters;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    /// <summary>
    /// This class implements the IVoucherService interface.
    /// </summary>
    [RegisterType]
    public class VoucherService : BaseCoordinationService, IVoucherService
    {
        private IVoucherRepository voucherRepository;
        private IGeneralLedgerConfigurationRepository generalLedgerConfigurationRepository;
        private IGeneralLedgerUserRepository generalLedgerUserRepository;

        // This constructor initializes the private attributes.
        public VoucherService(IVoucherRepository voucherRepository,
            IGeneralLedgerConfigurationRepository generalLedgerConfigurationRepository,
            IGeneralLedgerUserRepository generalLedgerUserRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.voucherRepository = voucherRepository;
            this.generalLedgerConfigurationRepository = generalLedgerConfigurationRepository;
            this.generalLedgerUserRepository = generalLedgerUserRepository;
        }

        /// <summary>
        /// Returns the DTO for the specified voucher.
        /// </summary>
        /// <param name="id">ID of the requested voucher.</param>
        /// <returns>Voucher DTO.</returns>
        [Obsolete("OBSOLETE as of API 1.15. Please use GetVoucher2Async")]
        public async Task<Ellucian.Colleague.Dtos.ColleagueFinance.Voucher> GetVoucherAsync(string id)
        {
            // Get the GL Configuration so we know how to format the GL numbers, and get the full GL access role
            var glConfiguration = await generalLedgerConfigurationRepository.GetAccountStructureAsync();

            // Get the GL class configuration because it is used by the GL user repository.
            var glClassConfiguration = await generalLedgerConfigurationRepository.GetClassConfigurationAsync();

            // Get the ID for the person who is logged in, and use the ID to get their GL access level.
            var generalLedgerUser = await generalLedgerUserRepository.GetGeneralLedgerUserAsync(CurrentUser.PersonId, glConfiguration.FullAccessRole, glClassConfiguration.ClassificationName, glClassConfiguration.ExpenseClassValues);

            // Get the voucher domain entity from the repository.
            int versionNumber = 1;
            var voucherDomainEntity = await voucherRepository.GetVoucherAsync(id, CurrentUser.PersonId, generalLedgerUser.GlAccessLevel, generalLedgerUser.AllAccounts, versionNumber);

            if (voucherDomainEntity == null)
            {
                throw new ArgumentNullException("voucherDomainEntity", "voucherDomainEntity cannot be null.");
            }

            // Convert the project and all its child objects into DTOs.
            var voucherDtoAdapter = new VoucherEntityToDtoAdapter(_adapterRegistry, logger);
            var voucherDto = voucherDtoAdapter.MapToType(voucherDomainEntity, glConfiguration.MajorComponentStartPositions);

            return voucherDto;
        }

        /// <summary>
        /// Returns the DTO for the specified voucher.
        /// </summary>
        /// <param name="id">ID of the requested voucher.</param>
        /// <returns>Voucher DTO.</returns>
        public async Task<Ellucian.Colleague.Dtos.ColleagueFinance.Voucher2> GetVoucher2Async(string id)
        {
            // Get the GL Configuration so we know how to format the GL numbers, and get the full GL access role
            var glConfiguration = await generalLedgerConfigurationRepository.GetAccountStructureAsync();

            // Get the GL class configuration because it is used by the GL user repository.
            var glClassConfiguration = await generalLedgerConfigurationRepository.GetClassConfigurationAsync();

            // Get the ID for the person who is logged in, and use the ID to get their GL access level.
            var generalLedgerUser = await generalLedgerUserRepository.GetGeneralLedgerUserAsync2(CurrentUser.PersonId, glConfiguration.FullAccessRole, glClassConfiguration);

            // Get the voucher domain entity from the repository.
            int versionNumber = 2;
            var voucherDomainEntity = await voucherRepository.GetVoucherAsync(id, CurrentUser.PersonId, generalLedgerUser.GlAccessLevel, generalLedgerUser.AllAccounts, versionNumber);

            if (voucherDomainEntity == null)
            {
                throw new ArgumentNullException("voucherDomainEntity", "voucherDomainEntity cannot be null.");
            }

            // Convert the project and all its child objects into DTOs.
            var voucherDtoAdapter = new Voucher2EntityToDtoAdapter(_adapterRegistry, logger);
            var voucherDto = voucherDtoAdapter.MapToType(voucherDomainEntity, glConfiguration.MajorComponentStartPositions);

            return voucherDto;
        }

    }
}
