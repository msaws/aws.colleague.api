﻿// Copyright 2016 Ellucian Company L.P. and its affiliates

using System;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Coordination.ColleagueFinance.Adapters;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System.Collections.Generic;
using Ellucian.Colleague.Domain.Base.Repositories;
using System.Text.RegularExpressions;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    /// <summary>
    /// Implements the IGeneralLedgerTransactionService
    /// </summary>
    [RegisterType]
    public class GeneralLedgerTransactionService : BaseCoordinationService, IGeneralLedgerTransactionService
    {
        private IGeneralLedgerTransactionRepository generalLedgerTransactionRepository;
        private IPersonRepository personRepository;
        private IGeneralLedgerConfigurationRepository generalLedgerConfigurationRepository;
        private IGeneralLedgerUserRepository generalLedgerUserRepository;

        // Constructor to initialize the private attributes
        public GeneralLedgerTransactionService(IGeneralLedgerTransactionRepository generalLedgerTransactionRepository,
            IPersonRepository personRepository,
            IGeneralLedgerConfigurationRepository generalLedgerConfigurationRepository,
            IGeneralLedgerUserRepository generalLedgerUserRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.generalLedgerTransactionRepository = generalLedgerTransactionRepository;
            this.personRepository = personRepository;
            this.generalLedgerConfigurationRepository = generalLedgerConfigurationRepository;
            this.generalLedgerUserRepository = generalLedgerUserRepository;
        }

        /// <summary>
        /// Returns the DTO for the specified general ledger transaction
        /// </summary>
        /// <param name="id">Guid to General Ledger Transaction</param>
        /// <returns>General Ledger Transaction DTO</returns>
        public async Task<Ellucian.Colleague.Dtos.GeneralLedgerTransaction> GetByIdAsync(string id)
        {
            // Get the GL Configuration to get the name of the full GL account access role
            // and also provides the information to format the GL accounts
            var glConfiguration = await generalLedgerConfigurationRepository.GetAccountStructureAsync();

            // Get the GL class configuration because it is used by the GL user repository.
            var glClassConfiguration = await generalLedgerConfigurationRepository.GetClassConfigurationAsync();

            // Get the ID for the person who is logged in, and use the ID to get their GL access level.
            var generalLedgerUser = await generalLedgerUserRepository.GetGeneralLedgerUserAsync(CurrentUser.PersonId, glConfiguration.FullAccessRole, glClassConfiguration.ClassificationName, glClassConfiguration.ExpenseClassValues);

            // Get the general ledger transaction domain entity from the repository
            var generalLedgerTransactionDomainEntity = await generalLedgerTransactionRepository.GetByIdAsync(id, CurrentUser.PersonId, generalLedgerUser.GlAccessLevel);

            if (generalLedgerTransactionDomainEntity == null)
            {
                throw new ArgumentNullException("GeneralLedgerTransactionDomainEntity", "GeneralLedgerTransactionDomainEntity cannot be null.");
            }

            // Convert the general ledger transaction and all its child objects into DTOs.
            return await BuildGeneralLedgerTransactionDtoAsync(generalLedgerTransactionDomainEntity, glConfiguration);
        }

        /// <summary>
        /// Returns the DTO for the specified general ledger transaction for Ethos version 8
        /// </summary>
        /// <param name="id">Guid to General Ledger Transaction</param>
        /// <returns>General Ledger Transaction DTO</returns>
        public async Task<Ellucian.Colleague.Dtos.GeneralLedgerTransaction2> GetById2Async(string id)
        {
            // Get the GL Configuration to get the name of the full GL account access role
            // and also provides the information to format the GL accounts
            var glConfiguration = await generalLedgerConfigurationRepository.GetAccountStructureAsync();

            // Get the GL class configuration because it is used by the GL user repository.
            var glClassConfiguration = await generalLedgerConfigurationRepository.GetClassConfigurationAsync();

            // Get the ID for the person who is logged in, and use the ID to get their GL access level.
            var generalLedgerUser = await generalLedgerUserRepository.GetGeneralLedgerUserAsync(CurrentUser.PersonId, glConfiguration.FullAccessRole, glClassConfiguration.ClassificationName, glClassConfiguration.ExpenseClassValues);

            // Get the general ledger transaction domain entity from the repository
            var generalLedgerTransactionDomainEntity = await generalLedgerTransactionRepository.GetByIdAsync(id, CurrentUser.PersonId, generalLedgerUser.GlAccessLevel);

            if (generalLedgerTransactionDomainEntity == null)
            {
                throw new ArgumentNullException("GeneralLedgerTransactionDomainEntity", "GeneralLedgerTransactionDomainEntity cannot be null.");
            }

            // Convert the general ledger transaction and all its child objects into DTOs.
            return await BuildGeneralLedgerTransactionDto2Async(generalLedgerTransactionDomainEntity, glConfiguration);
        }

        /// <summary>
        /// Returns all general ledger transactions for the data model version 6
        /// </summary>
        /// <returns>Collection of GeneralLedgerTransactions</returns>
        public async Task<IEnumerable<Dtos.GeneralLedgerTransaction>> GetAsync()
        {
            var generalLedgerTransactionDtos = new List<Dtos.GeneralLedgerTransaction>();
            // Get the GL Configuration to get the name of the full GL account access role
            // and also provides the information to format the GL accounts
            var glConfiguration = await generalLedgerConfigurationRepository.GetAccountStructureAsync();

            // Get the GL class configuration because it is used by the GL user repository.
            var glClassConfiguration = await generalLedgerConfigurationRepository.GetClassConfigurationAsync();

            // Get the ID for the person who is logged in, and use the ID to get their GL access level.
            var generalLedgerUser = await generalLedgerUserRepository.GetGeneralLedgerUserAsync(CurrentUser.PersonId, glConfiguration.FullAccessRole, glClassConfiguration.ClassificationName, glClassConfiguration.ExpenseClassValues);

            // Get the general ledger transaction domain entity from the repository
            var generalLedgerTransactionDomainEntities = await generalLedgerTransactionRepository.GetAsync(CurrentUser.PersonId, generalLedgerUser.GlAccessLevel);

            if (generalLedgerTransactionDomainEntities == null)
            {
                throw new ArgumentNullException("GeneralLedgerTransactionDomainEntity", "GeneralLedgerTransactionDomainEntity cannot be null.");
            }

            // Convert the general ledger transaction and all its child objects into DTOs.
            foreach (var entity in generalLedgerTransactionDomainEntities)
            {
                if (entity != null)
                {
                    var generalLedgerTransactionDto = await BuildGeneralLedgerTransactionDtoAsync(entity, glConfiguration);
                    generalLedgerTransactionDtos.Add(generalLedgerTransactionDto);
                }
            }
            return generalLedgerTransactionDtos;
        }

        /// <summary>
        /// Returns all general ledger transactions for the data model version 8
        /// </summary>
        /// <returns>Collection of GeneralLedgerTransactions</returns>
        public async Task<IEnumerable<Dtos.GeneralLedgerTransaction2>> Get2Async()
        {
            var generalLedgerTransactionDtos = new List<Dtos.GeneralLedgerTransaction2>();
            // Get the GL Configuration to get the name of the full GL account access role
            // and also provides the information to format the GL accounts
            var glConfiguration = await generalLedgerConfigurationRepository.GetAccountStructureAsync();

            // Get the GL class configuration because it is used by the GL user repository.
            var glClassConfiguration = await generalLedgerConfigurationRepository.GetClassConfigurationAsync();

            // Get the ID for the person who is logged in, and use the ID to get their GL access level.
            var generalLedgerUser = await generalLedgerUserRepository.GetGeneralLedgerUserAsync(CurrentUser.PersonId, glConfiguration.FullAccessRole, glClassConfiguration.ClassificationName, glClassConfiguration.ExpenseClassValues);

            // Get the general ledger transaction domain entity from the repository
            var generalLedgerTransactionDomainEntities = await generalLedgerTransactionRepository.GetAsync(CurrentUser.PersonId, generalLedgerUser.GlAccessLevel);

            if (generalLedgerTransactionDomainEntities == null)
            {
                throw new ArgumentNullException("GeneralLedgerTransactionDomainEntity", "GeneralLedgerTransactionDomainEntity cannot be null.");
            }

            // Convert the general ledger transaction and all its child objects into DTOs.
            foreach (var entity in generalLedgerTransactionDomainEntities)
            {
                if (entity != null)
                {
                    var generalLedgerTransactionDto = await BuildGeneralLedgerTransactionDto2Async(entity, glConfiguration);
                    generalLedgerTransactionDtos.Add(generalLedgerTransactionDto);
                }
            }
            return generalLedgerTransactionDtos;
        }

        /// <summary>
        /// Update a single general ledger transaction for the data model version 6
        /// </summary>
        /// <returns>A single GeneralLedgerTransaction</returns>
        public async Task<Dtos.GeneralLedgerTransaction> UpdateAsync(string id, Dtos.GeneralLedgerTransaction generalLedgerDto)
        {
            ValidateGeneralLedgerDto(generalLedgerDto);
            if (!generalLedgerDto.Id.Equals(id, StringComparison.InvariantCultureIgnoreCase))
            {
                throw new ArgumentOutOfRangeException("generalLedgerDto.Id", "The id in the body must match the id in the request.");
            }

            var generalLedgerTransactionDto = new Dtos.GeneralLedgerTransaction();

            var generalLedgerTransactionDtos = new List<Dtos.GeneralLedgerTransaction>();
            // Get the GL Configuration to get the name of the full GL account access role
            // and also provides the information to format the GL accounts
            var glConfiguration = await generalLedgerConfigurationRepository.GetAccountStructureAsync();

            // Get the GL class configuration because it is used by the GL user repository.
            var glClassConfiguration = await generalLedgerConfigurationRepository.GetClassConfigurationAsync();

            // Get the ID for the person who is logged in, and use the ID to get their GL access level.
            var generalLedgerUser = await generalLedgerUserRepository.GetGeneralLedgerUserAsync(CurrentUser.PersonId, glConfiguration.FullAccessRole, glClassConfiguration.ClassificationName, glClassConfiguration.ExpenseClassValues);

            var generalLedgerTransactionEntity = await BuildGeneralLedgerTransactionEntityAsync(generalLedgerDto, glConfiguration.MajorComponents.Count);
            var entity = await generalLedgerTransactionRepository.UpdateAsync(id, generalLedgerTransactionEntity, CurrentUser.PersonId, generalLedgerUser.GlAccessLevel, glConfiguration);
            
            generalLedgerTransactionDto = await BuildGeneralLedgerTransactionDtoAsync(entity, glConfiguration);

            return generalLedgerTransactionDto;
        }

        /// <summary>
        /// Update a single general ledger transaction for the data model version 8
        /// </summary>
        /// <returns>A single GeneralLedgerTransaction</returns>
        public async Task<Dtos.GeneralLedgerTransaction2> Update2Async(string id, Dtos.GeneralLedgerTransaction2 generalLedgerDto)
        {
            ValidateGeneralLedgerDto2(generalLedgerDto);
            if (!generalLedgerDto.Id.Equals(id, StringComparison.InvariantCultureIgnoreCase))
            {
                throw new ArgumentOutOfRangeException("generalLedgerDto.Id", "The id in the body must match the id in the request.");
            }

            var generalLedgerTransactionDto = new Dtos.GeneralLedgerTransaction2();

            var generalLedgerTransactionDtos = new List<Dtos.GeneralLedgerTransaction2>();
            // Get the GL Configuration to get the name of the full GL account access role
            // and also provides the information to format the GL accounts
            var glConfiguration = await generalLedgerConfigurationRepository.GetAccountStructureAsync();

            // Get the GL class configuration because it is used by the GL user repository.
            var glClassConfiguration = await generalLedgerConfigurationRepository.GetClassConfigurationAsync();

            // Get the ID for the person who is logged in, and use the ID to get their GL access level.
            var generalLedgerUser = await generalLedgerUserRepository.GetGeneralLedgerUserAsync(CurrentUser.PersonId, glConfiguration.FullAccessRole, glClassConfiguration.ClassificationName, glClassConfiguration.ExpenseClassValues);

            var generalLedgerTransactionEntity = await BuildGeneralLedgerTransactionEntity2Async(generalLedgerDto, glConfiguration.MajorComponents.Count);
            var entity = await generalLedgerTransactionRepository.UpdateAsync(id, generalLedgerTransactionEntity, CurrentUser.PersonId, generalLedgerUser.GlAccessLevel, glConfiguration);

            generalLedgerTransactionDto = await BuildGeneralLedgerTransactionDto2Async(entity, glConfiguration);

            return generalLedgerTransactionDto;
        }

        /// <summary>
        /// Create a single general ledger transaction for the data model version 6
        /// </summary>
        /// <returns>A single GeneralLedgerTransaction</returns>
        public async Task<Dtos.GeneralLedgerTransaction> CreateAsync(Dtos.GeneralLedgerTransaction generalLedgerDto)
        {
            ValidateGeneralLedgerDto(generalLedgerDto);

            var generalLedgerTransactionDto = new Dtos.GeneralLedgerTransaction();

            var generalLedgerTransactionDtos = new List<Dtos.GeneralLedgerTransaction>();
            // Get the GL Configuration to get the name of the full GL account access role
            // and also provides the information to format the GL accounts
            var glConfiguration = await generalLedgerConfigurationRepository.GetAccountStructureAsync();

            // Get the GL class configuration because it is used by the GL user repository.
            var glClassConfiguration = await generalLedgerConfigurationRepository.GetClassConfigurationAsync();

            // Get the ID for the person who is logged in, and use the ID to get their GL access level.
            var generalLedgerUser = await generalLedgerUserRepository.GetGeneralLedgerUserAsync(CurrentUser.PersonId, glConfiguration.FullAccessRole, glClassConfiguration.ClassificationName, glClassConfiguration.ExpenseClassValues);

            var generalLedgerTransactionEntity = await BuildGeneralLedgerTransactionEntityAsync(generalLedgerDto, glConfiguration.MajorComponents.Count);
            var entity = await generalLedgerTransactionRepository.CreateAsync(generalLedgerTransactionEntity, CurrentUser.PersonId, generalLedgerUser.GlAccessLevel, glConfiguration);

            generalLedgerTransactionDto = await BuildGeneralLedgerTransactionDtoAsync(entity, glConfiguration);

            return generalLedgerTransactionDto;
        }

        /// <summary>
        /// Create a single general ledger transaction for the data model version 8
        /// </summary>
        /// <returns>A single GeneralLedgerTransaction</returns>
        public async Task<Dtos.GeneralLedgerTransaction2> Create2Async(Dtos.GeneralLedgerTransaction2 generalLedgerDto)
        {
            ValidateGeneralLedgerDto2(generalLedgerDto);

            var generalLedgerTransactionDto = new Dtos.GeneralLedgerTransaction2();

            var generalLedgerTransactionDtos = new List<Dtos.GeneralLedgerTransaction2>();
            // Get the GL Configuration to get the name of the full GL account access role
            // and also provides the information to format the GL accounts
            var glConfiguration = await generalLedgerConfigurationRepository.GetAccountStructureAsync();

            // Get the GL class configuration because it is used by the GL user repository.
            var glClassConfiguration = await generalLedgerConfigurationRepository.GetClassConfigurationAsync();

            // Get the ID for the person who is logged in, and use the ID to get their GL access level.
            var generalLedgerUser = await generalLedgerUserRepository.GetGeneralLedgerUserAsync(CurrentUser.PersonId, glConfiguration.FullAccessRole, glClassConfiguration.ClassificationName, glClassConfiguration.ExpenseClassValues);

            var generalLedgerTransactionEntity = await BuildGeneralLedgerTransactionEntity2Async(generalLedgerDto, glConfiguration.MajorComponents.Count);
            var entity = await generalLedgerTransactionRepository.CreateAsync(generalLedgerTransactionEntity, CurrentUser.PersonId, generalLedgerUser.GlAccessLevel, glConfiguration);

            generalLedgerTransactionDto = await BuildGeneralLedgerTransactionDto2Async(entity, glConfiguration);

            return generalLedgerTransactionDto;
        }

        /// <summary>
        /// validate general ledger transaction for the data model version 6
        /// </summary>
        /// <returns></returns>
        private void ValidateGeneralLedgerDto(Dtos.GeneralLedgerTransaction generalLedgerDto)
        {
            decimal? creditAmount = 0;
            decimal? debitAmount = 0;
            if (generalLedgerDto == null)
            {
                throw new ArgumentNullException("generalLedgerDto", "The body of the request is required for updates.");
            }
            if (generalLedgerDto.ProcessMode != Dtos.EnumProperties.ProcessMode.Update &&
                generalLedgerDto.ProcessMode != Dtos.EnumProperties.ProcessMode.Validate)
            {
                throw new ArgumentOutOfRangeException("generalLedgerDto.processMode", "Process Mode of update or validate is required.");
            }
            if (generalLedgerDto.Transactions == null)
            {
                throw new ArgumentNullException("generalLedgerDto.transactions", "Transactions are missing from the request.");
            }
            foreach (var trans in generalLedgerDto.Transactions)
            {
                if (trans.Type != Dtos.EnumProperties.GeneralLedgerTransactionType.Donation &&
                    trans.Type != Dtos.EnumProperties.GeneralLedgerTransactionType.DonationEndowed &&
                    trans.Type != Dtos.EnumProperties.GeneralLedgerTransactionType.Pledge &&
                    trans.Type != Dtos.EnumProperties.GeneralLedgerTransactionType.PledgeEndowed)
                {
                    throw new ArgumentOutOfRangeException("generalLedgerDto.transactions.type", "Transaction type only supports Donations and Pledges");
                }
                if (trans.LedgerDate == null)
                {
                    throw new ArgumentNullException("generalLedgerDto.transactions.ledgerDate", "Each transaction must have a ledgerDate assigned.");
                }
                if (trans.TransactionDetailLines == null)
                {
                    throw new ArgumentNullException("generalLedgerDto.transactions.transactionDetailLines", "Transaction Detail is required.");
                }
                foreach (var transDetail in trans.TransactionDetailLines)
                {
                    if (string.IsNullOrEmpty(transDetail.AccountingString))
                    {
                        throw new ArgumentNullException("generalLedgerDto.transactions.transactionDetailLines.accountingString", "Each transaction detail must have an accounting string.");
                    }
                    if (string.IsNullOrEmpty(transDetail.Description))
                    {
                        throw new ArgumentNullException("generalLedgerDto.transactions.transactionDetailLines.description", "Each transaction detail must have a description.");
                    }
                    if (transDetail.Amount == null || transDetail.Amount.Value == 0)
                    {
                        throw new ArgumentOutOfRangeException("generalLedgerDto.transactions.transactionDetailLines.amount.value", "Each transaction detail must have an amount and value.");
                    }
                    if (transDetail.Amount.Currency != Dtos.EnumProperties.CurrencyCodes.USD &&
                        transDetail.Amount.Currency != Dtos.EnumProperties.CurrencyCodes.CAD)
                    {
                        throw new ArgumentOutOfRangeException("generalLedgerDto.transactions.transactionDetailLines.amount.currency", "Each transaction detail must have currency of either USD or CAD.");
                    }
                    if (transDetail.Type != Dtos.EnumProperties.CreditOrDebit.Credit &&
                        transDetail.Type != Dtos.EnumProperties.CreditOrDebit.Debit)
                    {
                        throw new ArgumentOutOfRangeException("generalLedgerDto.transactions.transactionDetailLines.type", "Each Detail item must be specified as either Debit or Credit.");
                    }
                    if (transDetail.Type == Dtos.EnumProperties.CreditOrDebit.Credit)
                    {
                        creditAmount += transDetail.Amount.Value;
                    }
                    else
                    {
                        debitAmount += transDetail.Amount.Value;
                    }
                }
            }
            if (debitAmount != creditAmount)
            {
                throw new ArgumentOutOfRangeException("generalLedgerDto.transactions.transactionDetailLines.amount.value", string.Format("Total Credits ({0}) and Debits ({1}) must be equal across all transaction detail entries.", creditAmount, debitAmount));
            }
        }

        /// <summary>
        /// validate general ledger transaction for the data model version 6
        /// </summary>
        /// <returns></returns>

        private void ValidateGeneralLedgerDto2(Dtos.GeneralLedgerTransaction2 generalLedgerDto)
        {
            decimal? creditAmount = 0;
            decimal? debitAmount = 0;
            if (generalLedgerDto == null)
            {
                throw new ArgumentNullException("generalLedgerDto", "The body of the request is required for updates.");
            }
            if (generalLedgerDto.ProcessMode != Dtos.EnumProperties.ProcessMode.Update &&
                generalLedgerDto.ProcessMode != Dtos.EnumProperties.ProcessMode.Validate)
            {
                throw new ArgumentOutOfRangeException("generalLedgerDto.processMode", "Process Mode of update or validate is required.");
            }
            if (generalLedgerDto.Transactions == null)
            {
                throw new ArgumentNullException("generalLedgerDto.transactions", "Transactions are missing from the request.");
            }
            foreach (var trans in generalLedgerDto.Transactions)
            {
                if (trans.Type != Dtos.EnumProperties.GeneralLedgerTransactionType.Donation &&
                    trans.Type != Dtos.EnumProperties.GeneralLedgerTransactionType.DonationEndowed &&
                    trans.Type != Dtos.EnumProperties.GeneralLedgerTransactionType.Pledge &&
                    trans.Type != Dtos.EnumProperties.GeneralLedgerTransactionType.PledgeEndowed)
                {
                    throw new ArgumentOutOfRangeException("generalLedgerDto.transactions.type", "Transaction type only supports Donations and Pledges");
                }
                if (trans.LedgerDate == null)
                {
                    throw new ArgumentNullException("generalLedgerDto.transactions.ledgerDate", "Each transaction must have a ledgerDate assigned.");
                }
                if (trans.TransactionDetailLines == null)
                {
                    throw new ArgumentNullException("generalLedgerDto.transactions.transactionDetailLines", "Transaction Detail is required.");
                }
                foreach (var transDetail in trans.TransactionDetailLines)
                {
                    if (string.IsNullOrEmpty(transDetail.AccountingString))
                    {
                        throw new ArgumentNullException("generalLedgerDto.transactions.transactionDetailLines.accountingString", "Each transaction detail must have an accounting string.");
                    }
                    if (string.IsNullOrEmpty(transDetail.Description))
                    {
                        throw new ArgumentNullException("generalLedgerDto.transactions.transactionDetailLines.description", "Each transaction detail must have a description.");
                    }
                    if (transDetail.Amount == null || transDetail.Amount.Value == 0)
                    {
                        throw new ArgumentOutOfRangeException("generalLedgerDto.transactions.transactionDetailLines.amount.value", "Each transaction detail must have an amount and value.");
                    }
                    if (transDetail.Amount.Currency != Dtos.EnumProperties.CurrencyCodes.USD &&
                        transDetail.Amount.Currency != Dtos.EnumProperties.CurrencyCodes.CAD)
                    {
                        throw new ArgumentOutOfRangeException("generalLedgerDto.transactions.transactionDetailLines.amount.currency", "Each transaction detail must have currency of either USD or CAD.");
                    }
                    if (transDetail.Type != Dtos.EnumProperties.CreditOrDebit.Credit &&
                        transDetail.Type != Dtos.EnumProperties.CreditOrDebit.Debit)
                    {
                        throw new ArgumentOutOfRangeException("generalLedgerDto.transactions.transactionDetailLines.type", "Each Detail item must be specified as either Debit or Credit.");
                    }
                    if (transDetail.Type == Dtos.EnumProperties.CreditOrDebit.Credit)
                    {
                        creditAmount += transDetail.Amount.Value;
                    }
                    else
                    {
                        debitAmount += transDetail.Amount.Value;
                    }
                }
            }
            if (debitAmount != creditAmount)
            {
                throw new ArgumentOutOfRangeException("generalLedgerDto.transactions.transactionDetailLines.amount.value", string.Format("Total Credits ({0}) and Debits ({1}) must be equal across all transaction detail entries.", creditAmount, debitAmount));
            }
        }

        /// <summary>
        /// Delete a single general ledger transaction for the data model version 6 or higher
        /// </summary>
        /// <param name="id">The requested general ledger transaction GUID</param>
        /// <returns></returns>
        public async Task DeleteAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id", "Must provide a general ledger transaction guid for deletion.");
            }

            await generalLedgerTransactionRepository.DeleteAsync(id);
        }

        /// <summary>
        /// Convert general ledger transaction entity to DTO for the data model version 6 
        /// </summary>
        /// <param name="generalLedgerTransactionEntity">General Ledger Entity</param>
        /// /// <param name="GlConfig">General Ledger Config</param>
        /// <returns>General Ledger Transaction DTO</returns>
        private async Task<Dtos.GeneralLedgerTransaction> BuildGeneralLedgerTransactionDtoAsync(GeneralLedgerTransaction generalLedgerTransactionEntity, GeneralLedgerAccountStructure GlConfig)
        {
            var generalLedgerTransactionDto = new Dtos.GeneralLedgerTransaction();

            generalLedgerTransactionDto.Id = generalLedgerTransactionEntity.Id;
            if (string.IsNullOrEmpty(generalLedgerTransactionDto.Id)) generalLedgerTransactionDto.Id = "00000000-0000-0000-0000-000000000000";
            generalLedgerTransactionDto.ProcessMode = (Dtos.EnumProperties.ProcessMode)Enum.Parse(typeof(Dtos.EnumProperties.ProcessMode), generalLedgerTransactionEntity.ProcessMode);
            generalLedgerTransactionDto.Transactions = new List<Dtos.DtoProperties.GeneralLedgerTransactionDtoProperty>();

            foreach (var transaction in generalLedgerTransactionEntity.GeneralLedgerTransactions)
            {
                var genLedgrTransactionDto = new Dtos.DtoProperties.GeneralLedgerTransactionDtoProperty()
                {
                    LedgerDate = transaction.LedgerDate,
                    Type = ConverEntityTypeToDtoType(transaction.Source),
                    TransactionNumber = transaction.TransactionNumber,
                    ReferenceNumber = transaction.ReferenceNumber,
                    Reference = new Dtos.DtoProperties.GeneralLedgerReferenceDtoProperty()
                    {
                        Person = (!string.IsNullOrEmpty(transaction.ReferencePersonId) && !(await personRepository.IsCorpAsync(transaction.ReferencePersonId)) ? new Dtos.GuidObject2(await personRepository.GetPersonGuidFromIdAsync(transaction.ReferencePersonId)) : null),
                        Organization = (!string.IsNullOrEmpty(transaction.ReferencePersonId) && (await personRepository.IsCorpAsync(transaction.ReferencePersonId)) ? new Dtos.GuidObject2(await personRepository.GetPersonGuidFromIdAsync(transaction.ReferencePersonId)) : null),
                    },
                    TransactionTypeReferenceDate = transaction.TransactionTypeReferenceDate,
                    TransactionDetailLines = new List<Dtos.DtoProperties.GeneralLedgerDetailDtoProperty>()
                };
                foreach (var transactionDetail in transaction.TransactionDetailLines)
                {
                    string accountingString = transactionDetail.GlAccount.GetFormattedGlAccount(GlConfig.MajorComponentStartPositions);
                    accountingString = GetFormattedGlAccount(accountingString, GlConfig);

                    if (!string.IsNullOrEmpty(transactionDetail.ProjectId))
                    {
                        accountingString = string.Concat(accountingString, "*", transactionDetail.ProjectId);
                    }
                    var genLedgrTransactionDetailDto = new Dtos.DtoProperties.GeneralLedgerDetailDtoProperty()
                    {
                        AccountingString = accountingString,
                        Amount = new Dtos.DtoProperties.AmountDtoProperty() { Value = transactionDetail.Amount.Value, Currency = (Dtos.EnumProperties.CurrencyCodes)Enum.Parse(typeof(Dtos.EnumProperties.CurrencyCodes), transactionDetail.Amount.Currency.ToString())},
                        Description = transactionDetail.GlAccount.GlAccountDescription,
                        SequenceNumber = transactionDetail.SequenceNumber,
                        Type = (Dtos.EnumProperties.CreditOrDebit)Enum.Parse(typeof(Dtos.EnumProperties.CreditOrDebit), transactionDetail.Type.ToString())
                    };
                    genLedgrTransactionDto.TransactionDetailLines.Add(genLedgrTransactionDetailDto);
                }
                generalLedgerTransactionDto.Transactions.Add(genLedgrTransactionDto);
            }

            return generalLedgerTransactionDto;
        }

        /// <summary>
        /// Convert general ledger transaction entity to DTO for the data model version 8
        /// </summary>
        /// <param name="generalLedgerTransactionEntity">General Ledger Entity</param>
        /// /// <param name="GlConfig">General Ledger Config</param>
        /// <returns>General Ledger Transaction DTO</returns>
        private async Task<Dtos.GeneralLedgerTransaction2> BuildGeneralLedgerTransactionDto2Async(GeneralLedgerTransaction generalLedgerTransactionEntity, GeneralLedgerAccountStructure GlConfig)
        {
            var generalLedgerTransactionDto = new Dtos.GeneralLedgerTransaction2();

            generalLedgerTransactionDto.Id = generalLedgerTransactionEntity.Id;
            if (string.IsNullOrEmpty(generalLedgerTransactionDto.Id)) generalLedgerTransactionDto.Id = "00000000-0000-0000-0000-000000000000";
            generalLedgerTransactionDto.ProcessMode = (Dtos.EnumProperties.ProcessMode)Enum.Parse(typeof(Dtos.EnumProperties.ProcessMode), generalLedgerTransactionEntity.ProcessMode);
            if (!string.IsNullOrEmpty(generalLedgerTransactionEntity.SubmittedBy))
            {
                var SubmittedByGuid = await personRepository.GetPersonGuidFromIdAsync(generalLedgerTransactionEntity.SubmittedBy);
                if (!string.IsNullOrEmpty(SubmittedByGuid))
                {
                    generalLedgerTransactionDto.SubmittedBy = new Dtos.GuidObject2(SubmittedByGuid);
                }
            }
            generalLedgerTransactionDto.Transactions = new List<Dtos.DtoProperties.GeneralLedgerTransactionDtoProperty2>();

            foreach (var transaction in generalLedgerTransactionEntity.GeneralLedgerTransactions)
            {
                var genLedgrTransactionDto = new Dtos.DtoProperties.GeneralLedgerTransactionDtoProperty2()
                {
                    LedgerDate = transaction.LedgerDate,
                    Type = ConverEntityTypeToDtoType(transaction.Source),
                    TransactionNumber = transaction.TransactionNumber,
                    ReferenceNumber = transaction.ReferenceNumber,
                    Reference = new Dtos.DtoProperties.GeneralLedgerReferenceDtoProperty()
                    {
                        Person = (!string.IsNullOrEmpty(transaction.ReferencePersonId) && !(await personRepository.IsCorpAsync(transaction.ReferencePersonId)) ? new Dtos.GuidObject2(await personRepository.GetPersonGuidFromIdAsync(transaction.ReferencePersonId)) : null),
                        Organization = (!string.IsNullOrEmpty(transaction.ReferencePersonId) && (await personRepository.IsCorpAsync(transaction.ReferencePersonId)) ? new Dtos.GuidObject2(await personRepository.GetPersonGuidFromIdAsync(transaction.ReferencePersonId)) : null),
                    },
                    TransactionTypeReferenceDate = transaction.TransactionTypeReferenceDate,
                    TransactionDetailLines = new List<Dtos.DtoProperties.GeneralLedgerDetailDtoProperty2>()
                };
                foreach (var transactionDetail in transaction.TransactionDetailLines)
                {
                    string accountingString = transactionDetail.GlAccount.GetFormattedGlAccount(GlConfig.MajorComponentStartPositions);
                    accountingString = GetFormattedGlAccount(accountingString, GlConfig);

                    if (!string.IsNullOrEmpty(transactionDetail.ProjectId))
                    {
                        accountingString = string.Concat(accountingString, "*", transactionDetail.ProjectId);
                    }
                                        
                    var genLedgrTransactionDetailDto = new Dtos.DtoProperties.GeneralLedgerDetailDtoProperty2()
                    {
                        AccountingString = accountingString,
                        Amount = new Dtos.DtoProperties.AmountDtoProperty() { Value = transactionDetail.Amount.Value, Currency = (Dtos.EnumProperties.CurrencyCodes)Enum.Parse(typeof(Dtos.EnumProperties.CurrencyCodes), transactionDetail.Amount.Currency.ToString()) },
                        Description = transactionDetail.GlAccount.GlAccountDescription,
                        SequenceNumber = transactionDetail.SequenceNumber,
                        Type = (Dtos.EnumProperties.CreditOrDebit)Enum.Parse(typeof(Dtos.EnumProperties.CreditOrDebit), transactionDetail.Type.ToString()),
                        SubmittedBy = !string.IsNullOrEmpty(transactionDetail.SubmittedBy) ? new Dtos.GuidObject2(await personRepository.GetPersonGuidFromIdAsync(transactionDetail.SubmittedBy)) : null
                    };
                    genLedgrTransactionDto.TransactionDetailLines.Add(genLedgrTransactionDetailDto);
                }
                generalLedgerTransactionDto.Transactions.Add(genLedgrTransactionDto);
            }

            return generalLedgerTransactionDto;
        }

        private string GetFormattedGlAccount(string accountNumber, GeneralLedgerAccountStructure GlConfig)
        {
            string formattedGlAccount = string.Empty;
            string tempGlNo = string.Empty;
            formattedGlAccount = Regex.Replace(accountNumber, "[^0-9a-zA-Z]", "");

            int startLoc = 0;
            int x = 0, glCount = GlConfig.MajorComponents.Count;

            foreach (var glMajor in GlConfig.MajorComponents)
            {
                x++;
                if (x < glCount) { tempGlNo = tempGlNo + formattedGlAccount.Substring(startLoc, glMajor.ComponentLength) + GlConfig.glDelimiter; }
                else { tempGlNo = tempGlNo + formattedGlAccount.Substring(startLoc, glMajor.ComponentLength); }
                startLoc += glMajor.ComponentLength;
            }
            formattedGlAccount = tempGlNo;

            return formattedGlAccount;
        }

        private Dtos.EnumProperties.GeneralLedgerTransactionType ConverEntityTypeToDtoType(string entityType)
        {
            switch (entityType)
            {
                case "DN":
                    return Dtos.EnumProperties.GeneralLedgerTransactionType.Donation;
                case "DNE":
                    return Dtos.EnumProperties.GeneralLedgerTransactionType.DonationEndowed;
                case "PL":
                    return Dtos.EnumProperties.GeneralLedgerTransactionType.Pledge;
                case "PLE":
                    return Dtos.EnumProperties.GeneralLedgerTransactionType.PledgeEndowed;
                default:
                    return Dtos.EnumProperties.GeneralLedgerTransactionType.Donation;
            }
        }


        private async Task<GeneralLedgerTransaction> BuildGeneralLedgerTransactionEntityAsync(Dtos.GeneralLedgerTransaction generalLedgerTransactionDto, int GLCompCount)
        {
            var generalLedgerTransactionEntity = new GeneralLedgerTransaction()
                {
                    Id = (generalLedgerTransactionDto.Id != null && !string.IsNullOrEmpty(generalLedgerTransactionDto.Id)) ? generalLedgerTransactionDto.Id : string.Empty
                };

            generalLedgerTransactionEntity.ProcessMode = generalLedgerTransactionDto.ProcessMode.ToString();
            generalLedgerTransactionEntity.GeneralLedgerTransactions = new List<GenLedgrTransaction>();

            foreach (var transaction in generalLedgerTransactionDto.Transactions)
            {
                string personId = string.Empty;
                if (transaction.Reference != null)
                {
                    if (transaction.Reference.Person != null && !string.IsNullOrEmpty(transaction.Reference.Person.Id))
                    {
                        personId = await personRepository.GetPersonIdFromGuidAsync(transaction.Reference.Person.Id);
                        if (string.IsNullOrEmpty(personId) || (await personRepository.IsCorpAsync(personId)))
                        {
                            throw new ArgumentException(string.Format("The id '{0}' is not a valid person in Colleague.", transaction.Reference.Person.Id), "generalLedgerDto.transactions.reference.person.id");
                        }
                    }
                    if (transaction.Reference.Organization != null && !string.IsNullOrEmpty(transaction.Reference.Organization.Id))
                    {
                        personId = await personRepository.GetPersonIdFromGuidAsync(transaction.Reference.Organization.Id);
                        if (string.IsNullOrEmpty(personId) || !(await personRepository.IsCorpAsync(personId)))
                        {
                            throw new ArgumentException(string.Format("The id '{0}' is not a valid organization in Colleague.", transaction.Reference.Organization.Id), "generalLedgerDto.transactions.reference.organization.id");
                        }
                    }
                }
                var genLedgrTransactionEntity = new GenLedgrTransaction(ConvertDtoTypeToEntityType(transaction.Type), transaction.LedgerDate)
                {
                    TransactionNumber = transaction.TransactionNumber,
                    ReferenceNumber = transaction.ReferenceNumber,
                    ReferencePersonId = personId,
                    TransactionTypeReferenceDate = transaction.TransactionTypeReferenceDate,
                    TransactionDetailLines = new List<GenLedgrTransactionDetail>()
                };
                foreach (var transactionDetail in transaction.TransactionDetailLines)
                {
                    string accountingString = transactionDetail.AccountingString;

                    //if GL number contains an Astrisks and if there more then one we are
                    // assuming that the delimiter is an astrisk, So we'll convert that delimiter to something
                    // else while perserving the project code if present.

                    if ((accountingString.Split('*').Length -1) > 1 )
                    {
                        int CountAstrisks = accountingString.Split('*').Length - 1;
                        if ((GLCompCount - 1) < CountAstrisks)
                        {
                            int lastIndex = accountingString.LastIndexOf('*');
                            accountingString = accountingString.Substring(0, lastIndex).Replace("*", "")
                                  + accountingString.Substring(lastIndex);
                        } else
                        {
                            accountingString = Regex.Replace(accountingString, "[^0-9a-zA-Z]", "");
                        }
                    }

                    string project = "";
                    var amount = new AmountAndCurrency(transactionDetail.Amount.Value, (CurrencyCodes)Enum.Parse(typeof(CurrencyCodes), transactionDetail.Amount.Currency.ToString()));
                    var type = (CreditOrDebit)Enum.Parse(typeof(CreditOrDebit), transactionDetail.Type.ToString());
                    var genLedgrTransactionDetailEntity = new GenLedgrTransactionDetail(accountingString, project, transactionDetail.Description, type, amount)
                    {
                        SequenceNumber = transactionDetail.SequenceNumber
                    };
                    genLedgrTransactionEntity.TransactionDetailLines.Add(genLedgrTransactionDetailEntity);
                }
                generalLedgerTransactionEntity.GeneralLedgerTransactions.Add(genLedgrTransactionEntity);
            }

            return generalLedgerTransactionEntity;
        }

        private async Task<GeneralLedgerTransaction> BuildGeneralLedgerTransactionEntity2Async(Dtos.GeneralLedgerTransaction2 generalLedgerTransactionDto, int GLCompCount)
        {
            var generalLedgerTransactionEntity = new GeneralLedgerTransaction()
            {
                Id = (generalLedgerTransactionDto.Id != null && !string.IsNullOrEmpty(generalLedgerTransactionDto.Id)) ? generalLedgerTransactionDto.Id : string.Empty
            };

            generalLedgerTransactionEntity.ProcessMode = generalLedgerTransactionDto.ProcessMode.ToString();
            if (generalLedgerTransactionDto.SubmittedBy != null && !string.IsNullOrEmpty(generalLedgerTransactionDto.SubmittedBy.Id))
            {
                var submittedById = await personRepository.GetPersonIdFromGuidAsync(generalLedgerTransactionDto.SubmittedBy.Id);
                if (string.IsNullOrEmpty(submittedById))
                {
                    throw new ArgumentException(string.Concat(" SubmittedBy ID '", generalLedgerTransactionDto.SubmittedBy.Id.ToString(), "' was not found. Valid Person Id is required."));
                }
                generalLedgerTransactionEntity.SubmittedBy = submittedById;
            }
            generalLedgerTransactionEntity.GeneralLedgerTransactions = new List<GenLedgrTransaction>();

            foreach (var transaction in generalLedgerTransactionDto.Transactions)
            {
                string personId = string.Empty;
                if (transaction.Reference != null)
                {
                    if (transaction.Reference.Person != null && !string.IsNullOrEmpty(transaction.Reference.Person.Id))
                    {
                        personId = await personRepository.GetPersonIdFromGuidAsync(transaction.Reference.Person.Id);
                        if (string.IsNullOrEmpty(personId) || (await personRepository.IsCorpAsync(personId)))
                        {
                            throw new ArgumentException(string.Format("The id '{0}' is not a valid person in Colleague.", transaction.Reference.Person.Id), "generalLedgerDto.transactions.reference.person.id");
                        }
                    }
                    if (transaction.Reference.Organization != null && !string.IsNullOrEmpty(transaction.Reference.Organization.Id))
                    {
                        personId = await personRepository.GetPersonIdFromGuidAsync(transaction.Reference.Organization.Id);
                        if (string.IsNullOrEmpty(personId) || !(await personRepository.IsCorpAsync(personId)))
                        {
                            throw new ArgumentException(string.Format("The id '{0}' is not a valid organization in Colleague.", transaction.Reference.Organization.Id), "generalLedgerDto.transactions.reference.organization.id");
                        }
                    }
                }
                var genLedgrTransactionEntity = new GenLedgrTransaction(ConvertDtoTypeToEntityType(transaction.Type), transaction.LedgerDate)
                {
                    TransactionNumber = transaction.TransactionNumber,
                    ReferenceNumber = transaction.ReferenceNumber,
                    ReferencePersonId = personId,
                    TransactionTypeReferenceDate = transaction.TransactionTypeReferenceDate,
                    TransactionDetailLines = new List<GenLedgrTransactionDetail>()
                };
                foreach (var transactionDetail in transaction.TransactionDetailLines)
                {
                    string accountingString = transactionDetail.AccountingString;

                    //if GL number contains an Astrisks and if there more then one we are
                    // assuming that the delimiter is an astrisk, So we'll convert that delimiter to something
                    // else while perserving the project code if present.

                    if ((accountingString.Split('*').Length - 1) > 1)
                    {
                        int CountAstrisks = accountingString.Split('*').Length - 1;
                        if ((GLCompCount - 1) < CountAstrisks)
                        {
                            int lastIndex = accountingString.LastIndexOf('*');
                            accountingString = accountingString.Substring(0, lastIndex).Replace("*", "")
                                  + accountingString.Substring(lastIndex);
                        }
                        else
                        {
                            accountingString = Regex.Replace(accountingString, "[^0-9a-zA-Z]", "");
                        }
                    }

                    string project = "";
                    var amount = new AmountAndCurrency(transactionDetail.Amount.Value, (CurrencyCodes)Enum.Parse(typeof(CurrencyCodes), transactionDetail.Amount.Currency.ToString()));
                    var type = (CreditOrDebit)Enum.Parse(typeof(CreditOrDebit), transactionDetail.Type.ToString());
                    var submittedById =string.Empty;
                    if (transactionDetail.SubmittedBy != null && !string.IsNullOrEmpty(transactionDetail.SubmittedBy.Id))
                    {
                        submittedById = await personRepository.GetPersonIdFromGuidAsync(transactionDetail.SubmittedBy.Id);
                        if (string.IsNullOrEmpty(submittedById))
                        {
                            throw new ArgumentException(string.Concat(" Transaction Detail SubmittedBy ID '", transactionDetail.SubmittedBy.Id.ToString(), "' was not found. Valid Person Id is required."));
                        }
                    }
                    var genLedgrTransactionDetailEntity = new GenLedgrTransactionDetail(accountingString, project, transactionDetail.Description, type, amount)
                    {
                        SequenceNumber = transactionDetail.SequenceNumber,
                        SubmittedBy = submittedById
                    };
                    genLedgrTransactionEntity.TransactionDetailLines.Add(genLedgrTransactionDetailEntity);
                }
                generalLedgerTransactionEntity.GeneralLedgerTransactions.Add(genLedgrTransactionEntity);
            }

            return generalLedgerTransactionEntity;
        }

        private string ConvertDtoTypeToEntityType(Dtos.EnumProperties.GeneralLedgerTransactionType? dtoType)
        {
            switch (dtoType)
            {
                case Dtos.EnumProperties.GeneralLedgerTransactionType.Donation:
                    return "DN";
                case Dtos.EnumProperties.GeneralLedgerTransactionType.DonationEndowed:
                    return "DNE";
                case Dtos.EnumProperties.GeneralLedgerTransactionType.Pledge:
                    return "PL";
                case Dtos.EnumProperties.GeneralLedgerTransactionType.PledgeEndowed:
                    return "PLE";
                default:
                    return "DN";
            }
        }
    }
}
