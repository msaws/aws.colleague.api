﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Coordination.ColleagueFinance.Adapters;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    /// <summary>
    /// This class implements the IRecurringVoucherService interface
    /// </summary>
    [RegisterType]
    public class RecurringVoucherService : BaseCoordinationService, IRecurringVoucherService
    {
        private IRecurringVoucherRepository recurringVoucherRepository;

        // This constructor initializes the private attributes
        public RecurringVoucherService(IRecurringVoucherRepository recurringVoucherRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.recurringVoucherRepository = recurringVoucherRepository;
        }

        /// <summary>
        /// Returns the DTO for the specified recurring voucher
        /// </summary>
        /// <param name="id">ID of the requested recurring voucher</param>
        /// <returns>Recurring Voucher DTO</returns>
        public async Task<Ellucian.Colleague.Dtos.ColleagueFinance.RecurringVoucher> GetRecurringVoucherAsync(string id)
        {
            // Get the recurring voucher domain entity from the repository
            var recurringVoucherDomainEntity = await recurringVoucherRepository.GetRecurringVoucherAsync(id);

            if (recurringVoucherDomainEntity == null)
            {
                throw new ArgumentNullException("recurringVoucherDomainEntity", "recurringVoucherDomainEntity cannot be null.");
            }

            // Convert the recurring voucher and all its child objects into DTOs
            var recurringVoucherDtoAdapter = new RecurringVoucherEntityToDtoAdapter(_adapterRegistry, logger);
            var recurringVoucherDto = recurringVoucherDtoAdapter.MapToType(recurringVoucherDomainEntity);

            return recurringVoucherDto;
        }

    }
}
