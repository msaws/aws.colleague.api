﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Colleague.Dtos;
using System.Collections.Generic;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    /// <summary>
    /// Interface for General Ledger Transaction
    /// </summary>
    public interface IGeneralLedgerTransactionService
    {
        /// <summary>
        /// Returns a single general ledger transaction for the data model version 6
        /// </summary>
        /// <param name="id">The requested general ledger transaction GUID</param>
        /// <returns>GeneralLedgerTransaction DTO</returns>
        Task<Dtos.GeneralLedgerTransaction> GetByIdAsync(string id);

        /// <summary>
        /// Returns a single general ledger transaction for the data model version 8
        /// </summary>
        /// <param name="id">The requested general ledger transaction GUID</param>
        /// <returns>GeneralLedgerTransaction DTO</returns>
        Task<Dtos.GeneralLedgerTransaction2> GetById2Async(string id);

        /// <summary>
        /// Returns all general ledger transactions for the data model version 6
        /// </summary>
        /// <returns>Collection of GeneralLedgerTransactions</returns>
        Task<IEnumerable<Dtos.GeneralLedgerTransaction>> GetAsync();

        /// <summary>
        /// Returns all general ledger transactions for the data model version 8
        /// </summary>
        /// <returns>Collection of GeneralLedgerTransactions</returns>
        Task<IEnumerable<Dtos.GeneralLedgerTransaction2>> Get2Async();

        /// <summary>
        /// Update a single general ledger transaction for the data model version 6
        /// </summary>
        /// <returns>A single GeneralLedgerTransaction</returns>
        Task<Dtos.GeneralLedgerTransaction> UpdateAsync(string id, Dtos.GeneralLedgerTransaction generalLedgerDto);

        /// <summary>
        /// Update a single general ledger transaction for the data model version 8
        /// </summary>
        /// <returns>A single GeneralLedgerTransaction</returns>
        Task<Dtos.GeneralLedgerTransaction2> Update2Async(string id, Dtos.GeneralLedgerTransaction2 generalLedgerDto);

        /// <summary>
        /// Create a single general ledger transaction for the data model version 6
        /// </summary>
        /// <returns>A single GeneralLedgerTransaction</returns>
        Task<Dtos.GeneralLedgerTransaction> CreateAsync(Dtos.GeneralLedgerTransaction generalLedgerDto);

        /// <summary>
        /// Create a single general ledger transaction for the data model version 8
        /// </summary>
        /// <returns>A single GeneralLedgerTransaction</returns>
        Task<Dtos.GeneralLedgerTransaction2> Create2Async(Dtos.GeneralLedgerTransaction2 generalLedgerDto);
        
        /// <summary>
        /// Delete a single general ledger transaction for the data model version 6
        /// </summary>
        /// <param name="id">The requested general ledger transaction GUID</param>
        /// <returns></returns>
        Task DeleteAsync(string id);
    }
}
