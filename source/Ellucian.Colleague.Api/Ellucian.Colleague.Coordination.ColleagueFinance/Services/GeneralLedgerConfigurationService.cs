﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Coordination.ColleagueFinance.Adapters;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Linq;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    /// <summary>
    /// Provider for General Ledger configuration services.
    /// </summary>
    [RegisterType]
    public class GeneralLedgerConfigurationService : BaseCoordinationService, IGeneralLedgerConfigurationService
    {
        private IGeneralLedgerConfigurationRepository generalLedgerConfigurationRepository;

        // Constructor for the General Ledger Configuration coordination service.
        public GeneralLedgerConfigurationService(IGeneralLedgerConfigurationRepository generalLedgerConfigurationRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.generalLedgerConfigurationRepository = generalLedgerConfigurationRepository;
        }

        /// <summary>
        /// Returns a GeneralLedgerConfiguration DTO.
        /// </summary>
        /// <returns>A GeneralLedgerConfiguration DTO.</returns>
        public async Task<Dtos.ColleagueFinance.GeneralLedgerConfiguration> GetGeneralLedgerConfigurationAsync()
        {
            // Obtain the configuration for the GL account structure.
             var glAccountStructure = await generalLedgerConfigurationRepository.GetAccountStructureAsync();

             var glconfigurationAdapter = new GlAccountStructureEntityToGlConfigurationDtoAdapter(_adapterRegistry, logger);
             var glConfigurationDto = new Dtos.ColleagueFinance.GeneralLedgerConfiguration();
             if (glAccountStructure != null)
             {
                 glConfigurationDto = glconfigurationAdapter.MapToType(glAccountStructure);
             }

             return glConfigurationDto;
        }
    }
}
