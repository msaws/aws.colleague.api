﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using System.Collections.Generic;
using System;
using Ellucian.Colleague.Coordination.Base.Services;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    /// <summary>
    /// Interface for Purchase Orders
    /// </summary>
    public interface IPurchaseOrderService : IBaseService
    {
        /// <summary>
        /// Returns the purchase order selected by the user
        /// </summary>
        /// <param name="purchaseOrderId">The requested purchase order ID</param>
        /// <returns>Purchase Order DTO</returns>
        Task<PurchaseOrder> GetPurchaseOrderAsync(string purchaseOrderId);

        Task<Tuple<IEnumerable<Ellucian.Colleague.Dtos.PurchaseOrders>, int>> GetPurchaseOrdersAsync(int offset, int limit, bool bypassCache = false);
    
        Task<Ellucian.Colleague.Dtos.PurchaseOrders> GetPurchaseOrdersByGuidAsync(string id);

        Task<Ellucian.Colleague.Dtos.PurchaseOrders> PutPurchaseOrdersAsync(string guid, Ellucian.Colleague.Dtos.PurchaseOrders purchaseOrders);
        Task<Ellucian.Colleague.Dtos.PurchaseOrders> PostPurchaseOrdersAsync(Ellucian.Colleague.Dtos.PurchaseOrders purchaseOrders);

    }
}
