﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    /// <summary>
    /// Implements IGeneralLedgerAccountService.
    /// </summary>
    [RegisterType]
    public class GeneralLedgerAccountService : BaseCoordinationService, IGeneralLedgerAccountService
    {
        private IGeneralLedgerUserRepository generalLedgerUserRepository;
        private IGeneralLedgerConfigurationRepository generalLedgerConfigurationRepository;
        private IGeneralLedgerAccountRepository generalLedgerAccountRepository;

        // This constructor initializes the private attributes.
        public GeneralLedgerAccountService(IGeneralLedgerUserRepository generalLedgerUserRepository,
            IGeneralLedgerConfigurationRepository generalLedgerConfigurationRepository,
            IGeneralLedgerAccountRepository generalLedgerAccountRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.generalLedgerUserRepository = generalLedgerUserRepository;
            this.generalLedgerConfigurationRepository = generalLedgerConfigurationRepository;
            this.generalLedgerAccountRepository = generalLedgerAccountRepository;
        }

        /// <summary>
        /// Retrieves a General ledger account DTO.
        /// </summary>
        /// <param name="generalLedgerAccountId">General ledger account ID.</param>
        /// <returns>General ledger account DTO.</returns>
        public async Task<Dtos.ColleagueFinance.GeneralLedgerAccount> GetAsync(string generalLedgerAccountId)
        {
            #region Null checks
            if (string.IsNullOrEmpty(generalLedgerAccountId))
            {
                throw new ArgumentNullException("generalLedgerAccountId", "generalLedgerAccountId is required.");
            }

            // Get the account structure configuration.
            var glAccountStructure = await generalLedgerConfigurationRepository.GetAccountStructureAsync();
            if (glAccountStructure == null)
            {
                throw new ApplicationException("Account structure must be defined.");
            }

            // Get the GL class configuration because it is used by the GL user repository.
            var glClassConfiguration = await generalLedgerConfigurationRepository.GetClassConfigurationAsync();
            if (glClassConfiguration == null)
            {
                throw new ApplicationException("GL class configuration must be defined.");
            }

            // Get the ID for the person who is logged in, and use the ID to get his list of assigned expense and revenue GL accounts.
            var generalLedgerUser = await generalLedgerUserRepository.GetGeneralLedgerUserAsync2(CurrentUser.PersonId, glAccountStructure.FullAccessRole, glClassConfiguration);
            if (generalLedgerUser == null)
            {
                throw new ApplicationException("GL user must be defined.");
            }

            var glAccountEntity = await generalLedgerAccountRepository.GetAsync(generalLedgerAccountId, glAccountStructure.MajorComponentStartPositions);
            if (glAccountEntity == null)
            {
                throw new ApplicationException("No general ledger account entity returned.");
            }
            #endregion

            var glAccountDto = new Dtos.ColleagueFinance.GeneralLedgerAccount();
            glAccountDto.Id = glAccountEntity.Id;
            glAccountDto.Description = "";
            glAccountDto.FormattedId = "";

            // Only send back the description if the user has access to the account.
            if (generalLedgerUser.AllAccounts != null && generalLedgerUser.AllAccounts.Contains(generalLedgerAccountId))
            {
                glAccountDto.FormattedId = glAccountEntity.FormattedGlAccount;
                glAccountDto.Description = glAccountEntity.Description;
            }

            return glAccountDto;
        }
    }
}