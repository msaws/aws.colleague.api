﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    /// <summary>
    /// Definition of the methods that must be implemented for a requisition service
    /// </summary>
    public interface IRequisitionService
    {
        /// <summary>
        /// Returns a specified requisition
        /// </summary>
        /// <param name="requisitionId">The requested requisition ID</param>
        /// <returns>Requisition DTO</returns>
        Task<Requisition> GetRequisitionAsync(string requisitionId);
    }
}
