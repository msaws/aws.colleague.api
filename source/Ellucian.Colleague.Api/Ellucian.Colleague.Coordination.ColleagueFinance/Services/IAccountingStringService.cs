﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    /// <summary>
    /// Interface for the AccountingString service.
    /// </summary>
    public interface IAccountingStringService
    {
        /// <summary>
        /// Returns the AccountingString if found and valid
        /// </summary>
        /// <param name="accountingStringValue">accountingString to check</param>
        /// <param name="validOn">date to check</param>
        ///  <returns>Returns the AccountingString if found and valid.</returns>
        Task<AccountingString> GetAccoutingStringByFilterCriteriaAsync(string accountingStringValue, DateTime? validOn = null);

        Task<IEnumerable<Ellucian.Colleague.Dtos.AccountingStringComponent>> GetAccountingStringComponentsAsync(bool bypassCache = false);
        Task<Ellucian.Colleague.Dtos.AccountingStringComponent> GetAccountingStringComponentsByGuidAsync(string id);

        Task<IEnumerable<Ellucian.Colleague.Dtos.AccountingStringFormats>> GetAccountingStringFormatsAsync(bool bypassCache = false);
        Task<Ellucian.Colleague.Dtos.AccountingStringFormats> GetAccountingStringFormatsByGuidAsync(string id);

        /// <summary>
        /// Get the all values for accounting String Component values
        /// </summary>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        Task<Tuple<IEnumerable<Ellucian.Colleague.Dtos.AccountingStringComponentValues>,int>> GetAccountingStringComponentValuesAsync(int Offset, int Limit, string component, string transactionStatus, string typeAccount, string typeFund, bool bypassCache = false);

        /// <summary>
        /// Get a specific value for accounting string component values
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Ellucian.Colleague.Dtos.AccountingStringComponentValues> GetAccountingStringComponentValuesByGuidAsync(string id);
    }
}
