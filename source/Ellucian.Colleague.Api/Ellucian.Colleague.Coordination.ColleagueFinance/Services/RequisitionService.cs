﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Coordination.ColleagueFinance.Adapters;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    /// <summary>
    /// This class implements the IRequisitionService interface
    /// </summary>
    [RegisterType]
    public class RequisitionService : BaseCoordinationService, IRequisitionService
    {
        private IRequisitionRepository requisitionRepository;
        private IGeneralLedgerConfigurationRepository generalLedgerConfigurationRepository;
        private IGeneralLedgerUserRepository generalLedgerUserRepository;

        // This constructor initializes the private attributes
        public RequisitionService(IRequisitionRepository requisitionRepository,
            IGeneralLedgerConfigurationRepository generalLedgerConfigurationRepository,
            IGeneralLedgerUserRepository generalLedgerUserRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.requisitionRepository = requisitionRepository;
            this.generalLedgerConfigurationRepository = generalLedgerConfigurationRepository;
            this.generalLedgerUserRepository = generalLedgerUserRepository;
        }

        /// <summary>
        /// Returns the requisition selected by the user
        /// </summary>
        /// <param name="id">ID for the requested requisition</param>
        /// <returns>Requisition DTO</returns>
        public async Task<Ellucian.Colleague.Dtos.ColleagueFinance.Requisition> GetRequisitionAsync(string id)
        {
            // Get the GL Configuration to get the name of the full GL account access role
            // and also provides the information to format the GL accounts
            var glConfiguration = await generalLedgerConfigurationRepository.GetAccountStructureAsync();

            // Get the GL class configuration because it is used by the GL user repository.
            var glClassConfiguration = await generalLedgerConfigurationRepository.GetClassConfigurationAsync();

            // Get the ID for the person who is logged in, and use the ID to get their GL access level.
            var generalLedgerUser = await generalLedgerUserRepository.GetGeneralLedgerUserAsync(CurrentUser.PersonId, glConfiguration.FullAccessRole, glClassConfiguration.ClassificationName, glClassConfiguration.ExpenseClassValues);

            // Get the requisition domain entity from the repository
            var requisitionDomainEntity = await requisitionRepository.GetRequisitionAsync(id, CurrentUser.PersonId, generalLedgerUser.GlAccessLevel, generalLedgerUser.AllAccounts);

            if (requisitionDomainEntity == null)
            {
                throw new ArgumentNullException("requisitionDomainEntity", "requisitionDomainEntity cannot be null.");
            }

            // Convert the requisition and all its child objects into DTOs
            var requisitionDtoAdapter = new RequisitionEntityToDtoAdapter(_adapterRegistry, logger);
            var requisitionDto = requisitionDtoAdapter.MapToType(requisitionDomainEntity, glConfiguration.MajorComponentStartPositions);

            return requisitionDto;
        }
    }
}
