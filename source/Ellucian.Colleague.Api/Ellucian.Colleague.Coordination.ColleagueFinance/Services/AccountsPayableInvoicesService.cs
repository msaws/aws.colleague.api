﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.ColleagueFinance;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Dtos;
using Ellucian.Colleague.Dtos.DtoProperties;
using Ellucian.Colleague.Dtos.EnumProperties;
using AccountsPayableInvoices = Ellucian.Colleague.Dtos.AccountsPayableInvoices;
using CurrencyCodes = Ellucian.Colleague.Dtos.EnumProperties.CurrencyCodes;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    [RegisterType]
    public class AccountsPayableInvoicesService : BaseCoordinationService, IAccountsPayableInvoicesService
    {

        private readonly IColleagueFinanceReferenceDataRepository _colleagueFinanceReferenceDataRepository;
        private readonly IReferenceDataRepository _referenceDataRepository;
        private readonly IAccountsPayableInvoicesRepository _accountsPayableInvoicesRepository;
        private readonly IAddressRepository _addressRepository;
        private readonly IVendorsRepository _vendorsRepository;
        private readonly IGeneralLedgerConfigurationRepository _generalLedgerConfigurationRepository;
        private readonly IPersonRepository _personRepository;
        
        private readonly IConfigurationRepository _configurationRepository;
        private readonly IAccountFundsAvailableRepository _accountFundAvailableRepository;

        public AccountsPayableInvoicesService(

            IColleagueFinanceReferenceDataRepository colleagueFinanceReferenceDataRepository,
            IReferenceDataRepository referenceDataRepository,
            IAccountsPayableInvoicesRepository accountsPayableInvoicesRepository,
            IAddressRepository addressRepository,
            IVendorsRepository vendorsRepository,
            IGeneralLedgerConfigurationRepository generalLedgerConfigurationRepository,
            IPersonRepository personRepository,
            IConfigurationRepository configurationRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            IAccountFundsAvailableRepository accountFundAvailableRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger, configurationRepository: configurationRepository)
        {
            _vendorsRepository = vendorsRepository;
            _colleagueFinanceReferenceDataRepository = colleagueFinanceReferenceDataRepository;
            _addressRepository = addressRepository;
            _accountsPayableInvoicesRepository = accountsPayableInvoicesRepository;
            _referenceDataRepository = referenceDataRepository;
            _generalLedgerConfigurationRepository = generalLedgerConfigurationRepository;
            _personRepository = personRepository;
            _accountFundAvailableRepository = accountFundAvailableRepository;
            
            _configurationRepository = configurationRepository;
        }

        private IEnumerable<Domain.Base.Entities.CommerceTaxCode> _commerceTaxCodes;
        private IEnumerable<Domain.ColleagueFinance.Entities.AccountsPayableSources> _accountsPayableSources;
        private IEnumerable<Domain.ColleagueFinance.Entities.CommodityCode> _commodityCodes;
        private IEnumerable<Domain.ColleagueFinance.Entities.CommodityUnitType> _commodityUnitTypes;
        private IEnumerable<Domain.ColleagueFinance.Entities.VendorTerm> _vendorTerms;
        private Domain.ColleagueFinance.Entities.GeneralLedgerAccountStructure _glAccountStructure;

        
        /// <remarks>FOR USE WITH ELLUCIAN DATA MODEL</remarks>
        /// <summary>
        /// Get AccountsPayableInvoices data.
        /// </summary>
        /// <param name="offset">Paging offset</param>
        /// <param name="limit">Paging limit</param>
        /// <param name="bypassCache">Bypass cache flag.  If set to true, will requery cached items</param>
        /// <returns>List of <see cref="Dtos.AccountsPayableInvoices">AccountsPayableInvoices</see></returns>
        public async Task<Tuple<IEnumerable<Dtos.AccountsPayableInvoices>, int>> GetAccountsPayableInvoicesAsync(int offset, int limit, bool bypassCache = false)
        {
            CheckViewApInvoicesPermission();

            var accountsPayableInvoicesCollection = new List<Ellucian.Colleague.Dtos.AccountsPayableInvoices>();

            var accountsPayableInvoicesEntities = await _accountsPayableInvoicesRepository.GetAccountsPayableInvoicesAsync(offset, limit);
            var totalRecords = accountsPayableInvoicesEntities.Item2;

            foreach (var accountsPayableInvoiceEntity in accountsPayableInvoicesEntities.Item1)
            {
                if (accountsPayableInvoiceEntity.Guid != null)
                {
                    var accountsPayableInvoiceDto = await this.ConvertAccountsPayableInvoicesEntityToDtoAsync(accountsPayableInvoiceEntity, bypassCache);
                    accountsPayableInvoicesCollection.Add(accountsPayableInvoiceDto);
                }
            }
            return new Tuple<IEnumerable<Dtos.AccountsPayableInvoices>, int>(accountsPayableInvoicesCollection, totalRecords);
        }

        /// <remarks>FOR USE WITH ELLUCIAN DATA MODEL</remarks>
        /// <summary>
        /// Get AccountsPayableInvoices data from a Guid
        /// </summary>
        /// <param name="guid">Guid</param>
        /// <returns><see cref="Dtos.AccountsPayableInvoices">AccountsPayableInvoices</see></returns>
        public async Task<Dtos.AccountsPayableInvoices> GetAccountsPayableInvoicesByGuidAsync(string guid)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw new ArgumentNullException("guid", "A GUID is required to obtain an Accounts Payable Invoice.");
            }
            CheckViewApInvoicesPermission();

            try
            {
                return await ConvertAccountsPayableInvoicesEntityToDtoAsync(await _accountsPayableInvoicesRepository.GetAccountsPayableInvoicesByGuidAsync(guid, false));
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException("No Accounts Payable Invoices was found for guid  " + guid, ex);
            }
            catch (InvalidOperationException ex)
            {
                throw new InvalidOperationException("No Accounts Payable Invoices was found for guid  " + guid, ex);
            }
            catch (RepositoryException ex)
            {
                throw new RepositoryException("No Accounts Payable Invoices was found for guid  " + guid, ex);
            }
            catch (ArgumentException ex)
            {
                throw new ArgumentException(ex.Message, ex);
            }
            catch (ApplicationException ae)
            {
                throw new ApplicationException(ae.Message);
            }
            catch (Exception ex)
            {
                throw new Exception("No Accounts Payable Invoices was found for guid  " + guid, ex);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN DATA MODEL</remarks>
        /// <summary>
        /// Put (Update) an Accounts Payable Invoices domain entity
        /// </summary>
        /// <param name="guid">guid</param>
        /// <param name="accountsPayableInvoicesDto"><see cref="Dtos.AccountsPayableInvoices">AccountsPayableInvoices</see></param>
        /// <returns><see cref="Dtos.AccountsPayableInvoices">AccountsPayableInvoices</see></returns>
        public async Task<AccountsPayableInvoices> PutAccountsPayableInvoicesAsync(string guid, Dtos.AccountsPayableInvoices accountsPayableInvoicesDto)
        {
            if (accountsPayableInvoicesDto == null)
                throw new ArgumentNullException("accountsPayableInvoices", "Must provide a accountsPayableInvoices for update");
            if (string.IsNullOrEmpty(accountsPayableInvoicesDto.Id))
                throw new ArgumentNullException("accountsPayableInvoices", "Must provide a guid for accountsPayableInvoices update");

            // get the person ID associated with the incoming guid
            var accountsPayableInvoicesId = await _accountsPayableInvoicesRepository.GetAccountsPayableInvoicesIdFromGuidAsync(accountsPayableInvoicesDto.Id);

            //Used to figure out if there are funds that exceed the budget.
            var overrideAvailable = new List<Domain.ColleagueFinance.Entities.FundsAvailable>();

            // verify the GUID exists to perform an update.  If not, perform a create instead
            if (!string.IsNullOrEmpty(accountsPayableInvoicesId))
            {
                try
                {
                    // verify the user has the permission to update a accountsPayableInvoices
                    CheckUpdateApInvoicesPermission();

                    //Fund checking.
                    overrideAvailable = await checkFunds(accountsPayableInvoicesDto);


                    // map the DTO to entities
                    var accountsPayableInvoicesEntity
                    = await ConvertAccountsPayableInvoicesDtoToEntityAsync(accountsPayableInvoicesId, accountsPayableInvoicesDto);

                    // update the entity in the database
                    var updatedAccountsPayableInvoicesEntity =
                        await _accountsPayableInvoicesRepository.UpdateAccountsPayableInvoicesAsync(accountsPayableInvoicesEntity);

                    // convert the entity to a DTO
                    var dtoAccountsPayableInvoice = await this.ConvertAccountsPayableInvoicesEntityToDtoAsync(updatedAccountsPayableInvoicesEntity, true);

                    //populate our response if we had a submitted by and if there was items Overridden when a Budget was exceeded.
                    if (accountsPayableInvoicesDto.SubmittedBy != null && !string.IsNullOrWhiteSpace(accountsPayableInvoicesDto.SubmittedBy.Id))
                        dtoAccountsPayableInvoice.SubmittedBy = accountsPayableInvoicesDto.SubmittedBy;

                    if (dtoAccountsPayableInvoice.LineItems != null && dtoAccountsPayableInvoice.LineItems.Any() && overrideAvailable != null && overrideAvailable.Any())
                    {
                        int lineCount = 0;
                        foreach (var lineItem in dtoAccountsPayableInvoice.LineItems)
                        {
                            int detailCount = 0;
                            lineCount++;
                            foreach (var detail in lineItem.AccountDetails)
                            {
                                detailCount++;
                                string VouID = lineCount.ToString() + "." + detailCount.ToString();
                                var findOvr = overrideAvailable.FirstOrDefault(a => a.Sequence == VouID || a.Sequence == VouID + ".DS");
                                if (findOvr != null)
                                {
                                    if (!string.IsNullOrEmpty(findOvr.SubmittedBy) && findOvr.Sequence == VouID + ".DS")
                                    {
                                        var submittedByGuid = await _personRepository.GetPersonGuidFromIdAsync(findOvr.SubmittedBy);
                                        if (string.IsNullOrEmpty(submittedByGuid))
                                        {
                                            throw new Exception(string.Concat("Process finsihed by we couldn't return a Submitted By GUID purchase order: ", dtoAccountsPayableInvoice.Id, " Submitted By: ", findOvr.SubmittedBy));
                                        }
                                        detail.SubmittedBy = new GuidObject2(submittedByGuid);
                                    }
                                    if (findOvr.AvailableStatus == FundsAvailableStatus.Override)
                                        detail.BudgetCheck = AccountsPayableInvoicesAccountBudgetCheck.Overridden;
                                }
                                //if (overrideAvailable.Contains(detail.AccountingString))
                                //{
                                //    detail.BudgetCheck = AccountsPayableInvoicesAccountBudgetCheck.Overridden;
                                //}
                            }
                        }
                    }
                    // return the newly updated DTO
                    return dtoAccountsPayableInvoice;

                }
                catch (RepositoryException ex)
                {
                    throw ex;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
            // perform a create instead
            return await PostAccountsPayableInvoicesAsync(accountsPayableInvoicesDto);
        }

        /// <remarks>FOR USE WITH ELLUCIAN DATA MODEL</remarks>
        /// <summary>
        /// Post (Create) an Accounts Payable Invoices doamin entity
        /// </summary>
        /// <param name="accountsPayableInvoicesDto"><see cref="Dtos.AccountsPayableInvoices">AccountsPayableInvoices</see></param>
        /// <returns><see cref="Dtos.AccountsPayableInvoices">AccountsPayableInvoices</see></returns>
        public async Task<AccountsPayableInvoices> PostAccountsPayableInvoicesAsync(Dtos.AccountsPayableInvoices accountsPayableInvoicesDto)
        {
            if (accountsPayableInvoicesDto == null)
                throw new ArgumentNullException("accountsPayableInvoices", "Must provide a accountsPayableInvoices for update");
            if (string.IsNullOrEmpty(accountsPayableInvoicesDto.Id))
                throw new ArgumentNullException("accountsPayableInvoices", "Must provide a guid for accountsPayableInvoices update");


            Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableInvoices createdAccountsPayableInvoices = null;

            //Used to figure out if there are funds that exceed the budget.
            var overrideAvailable = new List<Domain.ColleagueFinance.Entities.FundsAvailable>();

            // verify the user has the permission to create a AccountsPayableInvoices
            CheckUpdateApInvoicesPermission();

            try
            {
                //Fund checking.
                overrideAvailable = await checkFunds(accountsPayableInvoicesDto);

                var accountsPayableInvoicesEntity
                         = await ConvertAccountsPayableInvoicesDtoToEntityAsync(accountsPayableInvoicesDto.Id, accountsPayableInvoicesDto);

                // create a AccountsPayableInvoices entity in the database
                createdAccountsPayableInvoices =
                    await _accountsPayableInvoicesRepository.CreateAccountsPayableInvoicesAsync(accountsPayableInvoicesEntity);

                var dtoAccountsPayableInvoice = await this.ConvertAccountsPayableInvoicesEntityToDtoAsync(createdAccountsPayableInvoices, true);

                //populate our response if we had a submitted by and if there was items Overridden when a Budget was exceeded.
                if (accountsPayableInvoicesDto.SubmittedBy != null && !string.IsNullOrWhiteSpace(accountsPayableInvoicesDto.SubmittedBy.Id))
                    dtoAccountsPayableInvoice.SubmittedBy = accountsPayableInvoicesDto.SubmittedBy;

                if (dtoAccountsPayableInvoice.LineItems != null && dtoAccountsPayableInvoice.LineItems.Any() && overrideAvailable != null && overrideAvailable.Any())
                {
                    int lineCount = 0;
                    foreach (var lineItem in dtoAccountsPayableInvoice.LineItems)
                    {
                        int detailCount = 0;
                        lineCount++;
                        foreach (var detail in lineItem.AccountDetails)
                        {
                            detailCount++;
                            string VouID = lineCount.ToString() + "." + detailCount.ToString();
                            var findOvr = overrideAvailable.FirstOrDefault(a => a.Sequence == VouID || a.Sequence == VouID + ".DS");
                            if (findOvr != null)
                            {
                                if (!string.IsNullOrEmpty(findOvr.SubmittedBy) && findOvr.Sequence == VouID + ".DS")
                                {
                                    var submittedByGuid = await _personRepository.GetPersonGuidFromIdAsync(findOvr.SubmittedBy);
                                    if (string.IsNullOrEmpty(submittedByGuid))
                                    {
                                        throw new Exception(string.Concat("Process finsihed by we couldn't return a Submitted By GUID purchase order: ", dtoAccountsPayableInvoice.Id, " Submitted By: ", findOvr.SubmittedBy));
                                    }
                                    detail.SubmittedBy = new GuidObject2(submittedByGuid);
                                }
                                if (findOvr.AvailableStatus == FundsAvailableStatus.Override)
                                    detail.BudgetCheck = AccountsPayableInvoicesAccountBudgetCheck.Overridden;
                            }
                        }
                    }
                }
                // return the newly created AccountsPayableInvoices
                return dtoAccountsPayableInvoice;
            }
            catch (RepositoryException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }         
        }

        /// <summary>
        /// Convert AccountsPayableInvoices Dto To AccountsPayableInvoicesEntity
        /// </summary>
        /// <param name="accountsPayableInvoicesId">guid</param>
        /// <param name="accountsPayableInvoices"><see cref="Dtos.AccountsPayableInvoices">AccountsPayableInvoices</see></param>
        /// <returns><see cref="Domain.ColleagueFinance.Entities.AccountsPayableInvoices">AccountsPayableInvoices</see></returns>
        private async Task<Domain.ColleagueFinance.Entities.AccountsPayableInvoices> ConvertAccountsPayableInvoicesDtoToEntityAsync(string accountsPayableInvoicesId, Dtos.AccountsPayableInvoices accountsPayableInvoices)
        {
            if (accountsPayableInvoices == null || string.IsNullOrEmpty(accountsPayableInvoices.Id))
                throw new ArgumentNullException("accountsPayableInvoices", "Must provide guid for accountsPayableInvoices");

            if ((accountsPayableInvoices.Vendor != null) && (string.IsNullOrEmpty(accountsPayableInvoices.Vendor.Id)))
                throw new ArgumentNullException("accountsPayableInvoices", "Must provide vendor.id for accountsPayableInvoices");

            var voucherStatus = VoucherStatus.InProgress;
            if (accountsPayableInvoices.ProcessState != AccountsPayableInvoicesProcessState.NotSet)
            {

                switch (accountsPayableInvoices.ProcessState)
                {
                    case AccountsPayableInvoicesProcessState.Inprogress:
                        voucherStatus = VoucherStatus.InProgress; //"U";
                        break;
                    case AccountsPayableInvoicesProcessState.Notapproved:
                        voucherStatus = VoucherStatus.NotApproved; //"N";
                        break;
                    case AccountsPayableInvoicesProcessState.Outstanding:
                        voucherStatus = VoucherStatus.Outstanding; //"O";
                        break;
                    case AccountsPayableInvoicesProcessState.Paid:
                        voucherStatus = VoucherStatus.Paid; //"P";
                        break;
                    case AccountsPayableInvoicesProcessState.Reconciled:
                        voucherStatus = VoucherStatus.Reconciled; //"R";
                        break;
                    case AccountsPayableInvoicesProcessState.Voided:
                        voucherStatus = VoucherStatus.Voided; //"V";
                        break;

                    default:
                        // if we get here, we have corrupt data.
                        throw new ApplicationException("Invalid voucher status for voucher: " + accountsPayableInvoices.ProcessState.ToString());
                }
            }

            var guid = accountsPayableInvoices.Id;
            var voucherId = await _accountsPayableInvoicesRepository.GetAccountsPayableInvoicesIdFromGuidAsync(guid);
            var date = (accountsPayableInvoices.TransactionDate == DateTime.MinValue)
                ? DateTime.Now.Date : accountsPayableInvoices.TransactionDate;
            var invoiceNumber = accountsPayableInvoices.VendorInvoiceNumber;
            var invoiceDate = accountsPayableInvoices.VendorInvoiceDate;

            var accountsPayableInvoicesEntity = new Domain.ColleagueFinance.Entities.AccountsPayableInvoices(
                guid, voucherId ?? new Guid().ToString(), date, voucherStatus, "", invoiceNumber, invoiceDate);

            if ((accountsPayableInvoices.Vendor != null) && (!string.IsNullOrEmpty(accountsPayableInvoices.Vendor.Id)))
            {
                string vendorId = string.Empty;
                try
                {
                    vendorId = await _vendorsRepository.GetVendorIdFromGuidAsync(accountsPayableInvoices.Vendor.Id);
                }
                catch (ArgumentException)
                {
                    throw new ArgumentException
                        (string.Concat("The vendor id must correspond with a valid vendor record : ", accountsPayableInvoices.Vendor.Id));
                }
                if (!string.IsNullOrEmpty(vendorId))
                    accountsPayableInvoicesEntity.VendorId = vendorId;
                else
                {
                    throw new ArgumentNullException("accountsPayableInvoices", "Must provide valid vendor.id for accountsPayableInvoices");
                }
            }
            if ((accountsPayableInvoices.VendorAddress != null) && (!string.IsNullOrEmpty(accountsPayableInvoices.VendorAddress.Id)))
            {
                var addressId = await _addressRepository.GetAddressFromGuidAsync(accountsPayableInvoices.VendorAddress.Id);
                if (!string.IsNullOrEmpty(addressId))
                    accountsPayableInvoicesEntity.VendorAddressId = addressId;
                else
                {
                    throw new ArgumentNullException("accountsPayableInvoices", "Must provide valid address.id for accountsPayableInvoices");
                }
            }

            if (!string.IsNullOrEmpty(accountsPayableInvoices.ReferenceNumber))
            {
                accountsPayableInvoicesEntity.VoucherReferenceNo = new List<string>() { accountsPayableInvoices.ReferenceNumber};
            }

            accountsPayableInvoicesEntity.VoucherVoidGlTranDate = accountsPayableInvoices.VoidDate;


            if (accountsPayableInvoices.Payment != null)
            {
                var payment = accountsPayableInvoices.Payment;
                 if ((payment.Source != null) && (!string.IsNullOrEmpty(payment.Source.Id)))
                 {
                     var accountsPayableSource = (await this.GetAllAccountsPayableSourcesAsync(true)).FirstOrDefault(ap => ap.Guid == payment.Source.Id);
                    if ( accountsPayableSource == null)
                        throw new KeyNotFoundException("AccountsPayableSources not found for guid: " + payment.Source.Id);
                     accountsPayableInvoicesEntity.ApType = accountsPayableSource.Code;
                 }

                 if ((payment.PaymentTerms != null) && (!string.IsNullOrEmpty(payment.PaymentTerms.Id)))
                 {
                     var vendorTerms = (await this.GetAllVendorTermsAsync(true)).FirstOrDefault(ap => ap.Guid == payment.PaymentTerms.Id);
                     if (vendorTerms == null)
                         throw new KeyNotFoundException("PaymentTerm not found for guid: " + payment.PaymentTerms.Id);
                     accountsPayableInvoicesEntity.VoucherVendorTerms = vendorTerms.Code;
                 }

                if (payment.PaymentDueOn.HasValue)
                    accountsPayableInvoicesEntity.DueDate = payment.PaymentDueOn;
                else
                    accountsPayableInvoicesEntity.DueDate = DateTime.Now.Date;

            }
            accountsPayableInvoicesEntity.VoucherPayFlag =  
                accountsPayableInvoices.PaymentStatus == AccountsPayableInvoicesPaymentStatus.Nohold ? "Y" : "N";

            if (!string.IsNullOrEmpty(accountsPayableInvoices.InvoiceComment))
                accountsPayableInvoicesEntity.Comments = accountsPayableInvoices.InvoiceComment;

            if (accountsPayableInvoices.SubmittedBy != null)
            {
                var submittedById = await _personRepository.GetPersonIdFromGuidAsync(accountsPayableInvoices.SubmittedBy.Id);
                if (string.IsNullOrEmpty(submittedById))
                {
                    throw new Exception(string.Concat("SubmittedBy GUID is not found: ", accountsPayableInvoices.Id, " Guid: ", accountsPayableInvoices.SubmittedBy.Id));
                }
                accountsPayableInvoicesEntity.SubmittedBy = submittedById;
            }

            if (accountsPayableInvoices.InvoiceDiscountAmount != null && accountsPayableInvoices.InvoiceDiscountAmount.Value.HasValue)
            {
                accountsPayableInvoicesEntity.VoucherDiscAmt = accountsPayableInvoices.InvoiceDiscountAmount.Value;
            }

            if (accountsPayableInvoices.VendorBilledAmount != null)
            {
                if (accountsPayableInvoices.VendorBilledAmount.Value.HasValue)
                {
                    accountsPayableInvoicesEntity.VoucherInvoiceAmt = accountsPayableInvoices.VendorBilledAmount.Value;
                }
                if (accountsPayableInvoices.VendorBilledAmount.Currency != null)
                {
                    string currency;
                    switch (accountsPayableInvoices.VendorBilledAmount.Currency)
                    {
                        case CurrencyIsoCode.USD:
                            currency =  "USD";
                            break;
                        case CurrencyIsoCode.CAD:
                            currency =  "CAD";
                            break;
                        default:
                            currency = accountsPayableInvoices.VendorBilledAmount.Currency.ToString();
                            break;
                    }

                    accountsPayableInvoicesEntity.CurrencyCode = currency;
                }
            }

            if ( (accountsPayableInvoices.LineItems != null) && (accountsPayableInvoices.LineItems.Any()))
            {
                var allCommodityCodes = (await GetAllCommodityCodesAsync(true));
                if ((allCommodityCodes == null) || (!allCommodityCodes.Any()))
                {
                    throw new Exception("An error occurred extracting all commodity codes");
                }

                var allCommodityUnitTypes = (await this.GetAllCommodityUnitTypesAsync(true));
                if ((allCommodityUnitTypes == null) || (!allCommodityUnitTypes.Any()))
                {
                    throw new Exception("An error occurred extracting all commodity unit types");
                }

                foreach (var lineItem in accountsPayableInvoices.LineItems)
                {
                    if (lineItem.ReferenceDocument != null)
                    {
                        if (!(string.IsNullOrEmpty(lineItem.ReferenceDocument.PurchaseOrder)))
                        {
                            if ((!string.IsNullOrEmpty(accountsPayableInvoicesEntity.PurchaseOrderId))
                                  && ( accountsPayableInvoicesEntity.PurchaseOrderId != lineItem.ReferenceDocument.PurchaseOrder))
                            {
                                throw new ArgumentException("PurchaseOrder values do not match.  Must use the same PurchaseOrder in all lineItems.");
                            }
                            accountsPayableInvoicesEntity.PurchaseOrderId = lineItem.ReferenceDocument.PurchaseOrder;
                        }
                        if (!(string.IsNullOrEmpty(lineItem.ReferenceDocument.BlanketPurchaseOrder)))
                        {
                            if ((!string.IsNullOrEmpty(accountsPayableInvoicesEntity.BlanketPurchaseOrderId))
                                 && (accountsPayableInvoicesEntity.BlanketPurchaseOrderId != lineItem.ReferenceDocument.BlanketPurchaseOrder))
                            {
                                throw new ArgumentException("BlanketPurchaseOrder values do not match.  Must use the same BlanketPurchaseOrder in all lineItems.");
                            }
                            accountsPayableInvoicesEntity.BlanketPurchaseOrderId = lineItem.ReferenceDocument.BlanketPurchaseOrder;
                        }
                        if (!(string.IsNullOrEmpty(lineItem.ReferenceDocument.RecurringVoucher)))
                        {
                            if ((!string.IsNullOrEmpty(accountsPayableInvoicesEntity.RecurringVoucherId))
                                 && (accountsPayableInvoicesEntity.RecurringVoucherId != lineItem.ReferenceDocument.RecurringVoucher))
                            {
                                throw new ArgumentException("RecurringVoucher values do not match.  Must use the same RecurringVoucher in all lineItems.");
                            }
                            accountsPayableInvoicesEntity.RecurringVoucherId = lineItem.ReferenceDocument.RecurringVoucher;
                        }
                    }

                    var id = string.Empty;

                    if ((!string.IsNullOrEmpty(lineItem.ReferenceDocumentLineItemNumber))
                        && (!string.IsNullOrEmpty(lineItem.LineItemNumber)))
                    {
                        if (lineItem.ReferenceDocumentLineItemNumber != lineItem.LineItemNumber)
                        {
                            throw new ArgumentException("If providing both a ReferenceDocumentLineItemNumber and a LineItemNumber, they must be the same value. ");
                        }
                        id = lineItem.LineItemNumber;
                    }
                    else if (!string.IsNullOrEmpty(lineItem.ReferenceDocumentLineItemNumber))
                    {
                        id = lineItem.LineItemNumber;
                    }
                    else if (!string.IsNullOrEmpty(lineItem.LineItemNumber))
                    {
                        id = lineItem.LineItemNumber;
                    }
                    else
                    {
                        id = new Guid().ToString();
                    }


                    var description = lineItem.Description;
                    var quantity = lineItem.Quantity;
                    decimal price = 0;
                    if ((lineItem.UnitPrice != null) && (lineItem.UnitPrice.Value.HasValue))
                    {
                        price = Convert.ToDecimal(lineItem.UnitPrice.Value);
                    }
                    decimal extendedPrice = 0;
                    if ((lineItem.VendorBilledUnitPrice != null) && (lineItem.VendorBilledUnitPrice.Value.HasValue))
                    {
                        extendedPrice = Convert.ToDecimal(lineItem.VendorBilledUnitPrice.Value);
                    }
                    var apLineItem = new AccountsPayableInvoicesLineItem(id, description, quantity, price, extendedPrice);

                    apLineItem.DocLineItemId = lineItem.ReferenceDocumentLineItemNumber != null ? lineItem.ReferenceDocumentLineItemNumber : new Guid().ToString();

                    if ((lineItem.CommodityCode != null) && (!string.IsNullOrEmpty(lineItem.CommodityCode.Id)))
                    {
                        var commodityCode = allCommodityCodes.FirstOrDefault(c => c.Guid == lineItem.CommodityCode.Id);
                        if (commodityCode == null)
                        {
                            throw new Exception("Unable to determine commodity code represented by guid: " + lineItem.CommodityCode.Id);
                        }
                        apLineItem.CommodityCode = commodityCode.Code;
                    }

        
                    if ((lineItem.UnitofMeasure != null) && (!string.IsNullOrEmpty(lineItem.UnitofMeasure.Id)))
                    {
                        var commodityUnitType = allCommodityUnitTypes.FirstOrDefault(c => c.Guid == lineItem.UnitofMeasure.Id);
                        if (commodityUnitType == null)
                        {
                            throw new Exception("Unable to determine commodity unit type represented by guid: " + lineItem.UnitofMeasure.Id);
                        }
                        apLineItem.UnitOfMeasure = commodityUnitType.Code;

                    }
                    apLineItem.Comments = lineItem.Comment;

                    if (lineItem.Discount != null && lineItem.Discount.Amount.Value.HasValue)
                    {
                        apLineItem.TradeDiscountAmount = lineItem.Discount.Amount.Value;
                        apLineItem.TradeDiscountPercent = lineItem.Discount.Percent;
                    }

                    var lineItemTaxCodes = new List<string>();
                    if ((lineItem.Taxes != null) && (lineItem.Taxes.Any()))
                    {
                        var taxCodesEntities = await GetAllCommerceTaxCodesAsync(true);
                        if (taxCodesEntities != null)
                        {
                            foreach (var lineItemTax in lineItem.Taxes)
                            {
                                if (lineItemTax != null && lineItemTax.TaxCode != null && !string.IsNullOrEmpty(lineItemTax.TaxCode.Id))
                                {
                                    var taxCode = taxCodesEntities.FirstOrDefault(tax => tax.Guid == lineItemTax.TaxCode.Id);
                                    if (taxCode != null)
                                    {
                                        lineItemTaxCodes.Add(taxCode.Code);
                                    }
                                }
                            }
                        }
                    }

                    if (lineItem.AccountDetails != null && lineItem.AccountDetails.Any())
                    {    
                        foreach (var accountDetails in lineItem.AccountDetails)
                        {
                            decimal distributionQuantity = 0;
                            decimal distributionAmount = 0;
                            decimal distributionPercent = 0;
                            var allocated = accountDetails.Allocation != null ? accountDetails.Allocation.Allocated : null;
                            if (allocated != null)
                            {
                                if (allocated.Quantity.HasValue)
                                    distributionQuantity = Convert.ToDecimal(allocated.Quantity);
                                if (allocated.Amount != null && allocated.Amount.Value.HasValue)
                                    distributionAmount = Convert.ToDecimal(allocated.Amount.Value);
                                if (allocated.Percentage.HasValue)
                                    distributionPercent = Convert.ToDecimal(allocated.Percentage);
                            }
                            var accountingString = accountDetails.AccountingString;
                            if (!string.IsNullOrEmpty(accountingString))
                            {
                                accountingString = accountingString.Replace("-", "_");
                                var tempAccountingString = accountingString.Replace("_", "");
                                if (tempAccountingString.Length <= 14) { accountingString = tempAccountingString; }
                            }
                                
                            var glDistribution = new LineItemGlDistribution(accountingString, distributionQuantity, distributionAmount, distributionPercent);

                            apLineItem.AddGlDistribution(glDistribution);
                      
                            if ((accountDetails.SubmittedBy != null) && (!string.IsNullOrEmpty(accountDetails.SubmittedBy.Id)))
                            {
                                var personid = await _personRepository.GetPersonIdFromGuidAsync(accountDetails.SubmittedBy.Id);
                                if (!string.IsNullOrEmpty(personid))
                                {
                                    if ( (!string.IsNullOrEmpty(accountsPayableInvoicesEntity.VoucherRequestor)) && 
                                        (accountsPayableInvoicesEntity.VoucherRequestor !=  personid) )
                                    {
                                        throw new ArgumentException("Can not provided different submitted by ids in the same request.");
                                    }
                                    accountsPayableInvoicesEntity.VoucherRequestor = personid;
                                }
                            } 
                        }
                      
                        if (lineItemTaxCodes != null && lineItemTaxCodes.Any())
                        {
                            var accountsPayableLineItemTaxes = new List<LineItemTax>();
                            foreach (var lineItemTaxCode in lineItemTaxCodes)
                            {
                                accountsPayableLineItemTaxes.Add(new LineItemTax(lineItemTaxCode, 0));
                            }
                            if (accountsPayableLineItemTaxes.Any())
                                apLineItem.AccountsPayableLineItemTaxes = accountsPayableLineItemTaxes;
                        }
                    }
                    accountsPayableInvoicesEntity.AddAccountsPayableInvoicesLineItem(apLineItem);
                }
            }
            return accountsPayableInvoicesEntity;
        }
      
        /// <summary>
        /// Converts an AccountsPayableInvoices domain entity to its corresponding AccountsPayableInvoices DTO
        /// </summary>
        /// <param name="source">AccountsPayableInvoices domain entity</param>
        /// <param name="bypassCache">Bypass cache flag.  If set to true, will requery cached items</param>
        /// <returns><see cref="Dtos.AccountsPayableInvoices">AccountsPayableInvoices</see></returns>
        private async Task<Dtos.AccountsPayableInvoices> ConvertAccountsPayableInvoicesEntityToDtoAsync(Domain.ColleagueFinance.Entities.AccountsPayableInvoices source, bool bypassCache = false)
        {

            var accountsPayableInvoices = new Ellucian.Colleague.Dtos.AccountsPayableInvoices();
            Dtos.EnumProperties.CurrencyIsoCode currency = Dtos.EnumProperties.CurrencyIsoCode.USD;
            try
            {
                if (!(string.IsNullOrEmpty(source.CurrencyCode)))
                {
                    currency = (Dtos.EnumProperties.CurrencyIsoCode) Enum.Parse(typeof (Dtos.EnumProperties.CurrencyIsoCode), source.CurrencyCode);
                }
                else
                {
                    var hostCountry = source.HostCountry;
                    currency = ((hostCountry == "CAN") || (hostCountry == "CANADA")) ? CurrencyIsoCode.CAD :
                        CurrencyIsoCode.USD;
                }
            }
            catch (Exception ex)
            {
                var hostCountry = source.HostCountry;
                currency = ((hostCountry == "CAN") || (hostCountry == "CANADA")) ? CurrencyIsoCode.CAD :
                    CurrencyIsoCode.USD;
            }

            accountsPayableInvoices.Id = source.Guid;

            if (string.IsNullOrEmpty(source.VendorId))
            {
                throw new ArgumentException(string.Concat("Missing vendor information for voucher:", source.Id, " Guid: ", source.Guid));
            }

            var vendorGuid = await _vendorsRepository.GetVendorGuidFromIdAsync(source.VendorId);
            if (!(string.IsNullOrEmpty(vendorGuid)))
            {
                accountsPayableInvoices.Vendor = new GuidObject2(vendorGuid);
            }
            else
            {
                throw new ArgumentException(string.Concat("Missing vendor information for voucher:", source.Id, " Guid: ", source.Guid));

            }

            var addressId = string.IsNullOrEmpty(source.VoucherAddressId) ? source.VendorAddressId : source.VoucherAddressId;
            if (!(string.IsNullOrEmpty(addressId)))
            {
                var addressGuid = await _addressRepository.GetAddressGuidFromIdAsync(addressId.Trim());
                if (!(string.IsNullOrWhiteSpace(addressGuid)))
                {
                    accountsPayableInvoices.VendorAddress = new GuidObject2(addressGuid);
                }
            }

            if ((source.VoucherReferenceNo != null) && (source.VoucherReferenceNo.Any()))
                accountsPayableInvoices.ReferenceNumber = source.VoucherReferenceNo.FirstOrDefault();

            accountsPayableInvoices.VendorInvoiceNumber = source.InvoiceNumber;
            accountsPayableInvoices.TransactionDate = source.Date;
            accountsPayableInvoices.VendorInvoiceDate = source.InvoiceDate;
            accountsPayableInvoices.VoidDate = source.VoucherVoidGlTranDate;

            var processState = AccountsPayableInvoicesProcessState.NotSet;
            switch (source.Status)
            {
                case VoucherStatus.InProgress:
                    processState = AccountsPayableInvoicesProcessState.Inprogress;
                    break;
                case VoucherStatus.Outstanding:
                    processState = AccountsPayableInvoicesProcessState.Outstanding;
                    break;
                case VoucherStatus.Voided:
                    processState = AccountsPayableInvoicesProcessState.Voided;
                    break;
                case VoucherStatus.Paid:
                    processState = AccountsPayableInvoicesProcessState.Paid;
                    break;
                case VoucherStatus.NotApproved:
                    processState = AccountsPayableInvoicesProcessState.Notapproved;
                    break;
                case VoucherStatus.Reconciled:
                    processState = AccountsPayableInvoicesProcessState.Reconciled;
                    break;
                default:
                    break;

            }
            if (processState != AccountsPayableInvoicesProcessState.NotSet)
                accountsPayableInvoices.ProcessState = processState;

            accountsPayableInvoices.PaymentStatus = (source.VoucherPayFlag == "Y") ?
                AccountsPayableInvoicesPaymentStatus.Nohold : AccountsPayableInvoicesPaymentStatus.Hold;

            if (source.VoucherInvoiceAmt != null)
            {
                var vendorBilledAmount = new Amount2DtoProperty
                {
                    Currency = currency,
                    Value = source.VoucherInvoiceAmt
                };

                accountsPayableInvoices.VendorBilledAmount = vendorBilledAmount;
            }

            if (source.VoucherDiscAmt != null)
            {
                var invoiceDiscountAmount = new Amount2DtoProperty
                {
                    Currency = currency,
                    Value = source.VoucherDiscAmt
                };
                accountsPayableInvoices.InvoiceDiscountAmount = invoiceDiscountAmount;
            }

            var accountsPayableInvoicesTaxes = new List<AccountsPayableInvoicesTaxesDtoProperty>();
            foreach (var voucherTax in source.VoucherTaxes)
            {
                var accountsPayableInvoicesTax = new AccountsPayableInvoicesTaxesDtoProperty();

                var taxCodesEntities = await GetAllCommerceTaxCodesAsync(bypassCache);
                if (taxCodesEntities != null)
                {
                    var taxCode = taxCodesEntities.FirstOrDefault(tax => tax.Code == voucherTax.TaxCode);
                    if (taxCode != null)
                    {
                        accountsPayableInvoicesTax.TaxCode = new GuidObject2(taxCode.Guid);
                    }
                }

                var vendorAmount = new Amount2DtoProperty
                {
                    Currency = currency,
                    Value = voucherTax.TaxAmount
                };

                accountsPayableInvoicesTax.VendorAmount = vendorAmount;

                accountsPayableInvoicesTaxes.Add(accountsPayableInvoicesTax);
            }
            if (accountsPayableInvoicesTaxes.Any())
                accountsPayableInvoices.Taxes = accountsPayableInvoicesTaxes;

            if (source.VoucherNet.HasValue)
            {
                accountsPayableInvoices.InvoiceType = (source.VoucherNet < 0) ? AccountsPayableInvoicesInvoiceType.Creditinvoice
                    : AccountsPayableInvoicesInvoiceType.Invoice;
            }

            if (!(string.IsNullOrWhiteSpace(source.ApType)))
            {
                var payment = new AccountsPayableInvoicesPaymentDtoProperty();
                var accountsPayableSources = await GetAllAccountsPayableSourcesAsync(bypassCache);
                if (accountsPayableSources != null)
                {
                    var apType = accountsPayableSources.FirstOrDefault(aps => aps.Code == source.ApType);
                    if (apType != null)
                    {
                        payment.Source = new GuidObject2(apType.Guid);
                        payment.PaymentDueOn = source.DueDate;

                        var vendorTerm = source.VoucherVendorTerms;
                        if (!string.IsNullOrEmpty(vendorTerm))
                        {
                            var vendorTerms = await GetAllVendorTermsAsync(bypassCache);
                            if (vendorTerms != null)
                            {
                                var term = vendorTerms.FirstOrDefault(vt => vt.Code == vendorTerm);
                                if (term != null)
                                {
                                    payment.PaymentTerms = new GuidObject2(term.Guid);
                                }
                            }
                        }
                    }
                    accountsPayableInvoices.Payment = payment;
                }
            }

            if (!(string.IsNullOrWhiteSpace(source.Comments)))
                accountsPayableInvoices.InvoiceComment = source.Comments;

          
            if (source.LineItems != null && source.LineItems.Any())
            {
                var accountsPayableInvoicesLineItems = new List<AccountsPayableInvoicesLineItemDtoProperty>();
                foreach (var lineItem in source.LineItems)
                {
                    var accountsPayableInvoicesLineItem = new AccountsPayableInvoicesLineItemDtoProperty();

                    if (!string.IsNullOrEmpty(source.PurchaseOrderId))
                    {
                        accountsPayableInvoicesLineItem.ReferenceDocument = new LineItemReferenceDocumentDtoProperty()
                        {
                            PurchaseOrder = source.PurchaseOrderId
                        };
                    }
                    else if (!string.IsNullOrEmpty(source.BlanketPurchaseOrderId))
                    {
                        accountsPayableInvoicesLineItem.ReferenceDocument = new LineItemReferenceDocumentDtoProperty()
                        {
                            BlanketPurchaseOrder = source.BlanketPurchaseOrderId
                        };
                    }
                    else if (!string.IsNullOrEmpty(source.RecurringVoucherId))
                    {
                        accountsPayableInvoicesLineItem.ReferenceDocument = new LineItemReferenceDocumentDtoProperty()
                        {
                            RecurringVoucher = source.RecurringVoucherId
                        };

                    }
                    if (accountsPayableInvoicesLineItem.ReferenceDocument != null)
                        accountsPayableInvoicesLineItem.ReferenceDocumentLineItemNumber = lineItem.Id;

                    accountsPayableInvoicesLineItem.LineItemNumber = lineItem.Id;

                    accountsPayableInvoicesLineItem.Description = lineItem.Description;

                    if (!(string.IsNullOrEmpty(lineItem.CommodityCode)))
                    {
                        var commodityCodes = await GetAllCommodityCodesAsync(bypassCache);
                        if (commodityCodes != null)
                        {
                            var commodityCode = commodityCodes.FirstOrDefault(cc => cc.Code == lineItem.CommodityCode);
                            if (commodityCode != null)
                            {
                                accountsPayableInvoicesLineItem.CommodityCode = new GuidObject2(commodityCode.Guid);
                            }
                        }
                    }
                    accountsPayableInvoicesLineItem.Quantity = lineItem.Quantity;

                    if (!(string.IsNullOrEmpty(lineItem.UnitOfIssue)))
                    {
                        var commodityUnitTypes = await GetAllCommodityUnitTypesAsync(bypassCache);
                        if (commodityUnitTypes != null)
                        {
                            var commodityUnitType = commodityUnitTypes.FirstOrDefault(x => x.Code == lineItem.UnitOfIssue);
                            if (commodityUnitType != null)
                            {
                                accountsPayableInvoicesLineItem.UnitofMeasure = new GuidObject2(commodityUnitType.Guid);
                            }
                        }
                    }

                    accountsPayableInvoicesLineItem.UnitPrice = new Amount2DtoProperty()
                    {
                        Value = lineItem.Price,
                        Currency = currency
                    };

                    var accountsPayableInvoicesLineItemTaxes = new List<AccountsPayableInvoicesTaxesDtoProperty>();

                    if ((lineItem.AccountsPayableLineItemTaxes != null) && (lineItem.AccountsPayableLineItemTaxes.Any()))
                    {
                        var taxCodesEntities = await GetAllCommerceTaxCodesAsync(bypassCache);
                        if (taxCodesEntities != null)
                        {
                            //need to combine similar taxcodes
                            var lineItemTaxesTuple = lineItem.AccountsPayableLineItemTaxes
                                .GroupBy(l => l.TaxCode)
                                .Select(cl => new Tuple<string, decimal>(                                                                      
                                       cl.First().TaxCode,
                                       cl.Sum(c => c.TaxAmount)
                                    )).ToList();

                            foreach (var lineItemTax in lineItemTaxesTuple)
                            {

                                var accountsPayableInvoicesTax = new AccountsPayableInvoicesTaxesDtoProperty();

                                var taxCode = taxCodesEntities.FirstOrDefault(tax => tax.Code == lineItemTax.Item1);
                                if (taxCode != null)
                                {
                                    accountsPayableInvoicesTax.TaxCode = new GuidObject2(taxCode.Guid);
                                }
                                accountsPayableInvoicesTax.VendorAmount = new Amount2DtoProperty()
                                {
                                    Currency = currency,
                                    Value = lineItemTax.Item2
                                };

                                accountsPayableInvoicesLineItemTaxes.Add(accountsPayableInvoicesTax);
                            }
                        }
                        if (accountsPayableInvoicesLineItemTaxes.Any())
                            accountsPayableInvoicesLineItem.Taxes = accountsPayableInvoicesLineItemTaxes;
                    }

                    decimal discountAmount = (lineItem.CashDiscountAmount ?? 0)
                                             + (lineItem.TradeDiscountAmount ?? 0);
                    if (discountAmount > 0)
                    {
                        var accountsPayableInvoicesLineItemDiscount = new AccountsPayableInvoicesDiscountDtoProperty();

                        accountsPayableInvoicesLineItemDiscount.Amount = new Amount2DtoProperty()
                        {
                            Currency = currency,
                            Value = discountAmount
                        };

                        accountsPayableInvoicesLineItemDiscount.Percent = lineItem.TradeDiscountPercent;
                        accountsPayableInvoicesLineItem.Discount = accountsPayableInvoicesLineItemDiscount;
                    }
                    accountsPayableInvoicesLineItem.PaymentStatus = (source.VoucherPayFlag == "Y") ?
                        AccountsPayableInvoicesPaymentStatus.Nohold : AccountsPayableInvoicesPaymentStatus.Hold;

                    if (!(string.IsNullOrWhiteSpace(lineItem.Comments)))
                    {
                        accountsPayableInvoicesLineItem.Comment = lineItem.Comments;
                    }
                    accountsPayableInvoicesLineItem.Status = source.Status == VoucherStatus.InProgress
                        ? AccountsPayableInvoicesStatus.Open : AccountsPayableInvoicesStatus.Closed;

                    var accountsPayableInvoicesLineItemAccountDetails = new List<AccountsPayableInvoicesAccountDetailDtoProperty>();


                    foreach (var glDistribution in lineItem.GlDistributions)
                    {
                        if (!string.IsNullOrEmpty(glDistribution.GlAccountNumber))
                        {
                            var accountsPayableInvoicesAccountDetail = new AccountsPayableInvoicesAccountDetailDtoProperty();

                            var acctNumber = glDistribution.GlAccountNumber.Replace("_", "-");
                            ;
                            accountsPayableInvoicesAccountDetail.AccountingString =
                                string.IsNullOrEmpty(glDistribution.ProjectId) ?
                                    acctNumber : string.Concat(acctNumber, '*', glDistribution.ProjectId);

                            var accountDetailAllocation = new AccountsPayableInvoicesAllocationDtoProperty();

                            var accountsPayableInvoicesAllocated = new AccountsPayableInvoicesAllocatedDtoProperty();
                            accountsPayableInvoicesAllocated.Amount = new Amount2DtoProperty()
                            {

                                Currency = currency,
                                Value = glDistribution.Amount
                            };

                            accountsPayableInvoicesAllocated.Quantity = glDistribution.Quantity; // lineItem.Quantity;
                            accountsPayableInvoicesAllocated.Percentage = glDistribution.Percent;
                            accountDetailAllocation.Allocated = accountsPayableInvoicesAllocated;

                            //not used: accountDetailAllocation.AdditionalAmount = new AmountDtoProperty();
                            //not used: accountDetailAllocation.DiscountAmount = new AmountDtoProperty();
                            if ((lineItem.AccountsPayableLineItemTaxes != null) && (lineItem.AccountsPayableLineItemTaxes.Any()))
                            {
                                var glTax = lineItem.AccountsPayableLineItemTaxes.Where(lit => lit.LineGlNumber == glDistribution.GlAccountNumber)
                                    .Sum(c => c.TaxAmount);
              
                                if (glTax != null)
                                {
                                    accountDetailAllocation.TaxAmount = new Amount2DtoProperty()
                                    {
                                        Value = glTax, 
                                        Currency = currency
                                    };
                                }
                            }
                            accountsPayableInvoicesAccountDetail.Allocation = accountDetailAllocation;


                            if (!(string.IsNullOrWhiteSpace(source.ApType)))
                            {
                                var accountsPayableSources = await GetAllAccountsPayableSourcesAsync(bypassCache);
                                if (accountsPayableSources != null)
                                {
                                    var accountsPayableSource = accountsPayableSources.FirstOrDefault(aps => aps.Code == source.ApType);
                                    if (accountsPayableSource != null)
                                    {
                                        accountsPayableInvoicesAccountDetail.Source = new GuidObject2(accountsPayableSource.Guid);
                                    }
                                }
                            }

                            var glAcctStructure = await this.GetGeneralLedgerAccountStructure();
                            accountsPayableInvoicesAccountDetail.BudgetCheck =  ((glAcctStructure != null) && (glAcctStructure.CheckAvailableFunds == "Y"))
                                ? AccountsPayableInvoicesAccountBudgetCheck.Enforced :  AccountsPayableInvoicesAccountBudgetCheck.NotRequired;
                            
                            if (!string.IsNullOrEmpty(source.VoucherRequestor))
                            {
                                var personGuid = await _personRepository.GetPersonGuidFromIdAsync(source.VoucherRequestor);
                                if (!string.IsNullOrEmpty(personGuid))
                                {
                                    accountsPayableInvoicesAccountDetail.SubmittedBy = new GuidObject2(personGuid);
                                }
                            }
                            accountsPayableInvoicesLineItemAccountDetails.Add(accountsPayableInvoicesAccountDetail);
                        }
                    }
                    if (accountsPayableInvoicesLineItemAccountDetails.Any())
                        accountsPayableInvoicesLineItem.AccountDetails = accountsPayableInvoicesLineItemAccountDetails;

                    accountsPayableInvoicesLineItems.Add(accountsPayableInvoicesLineItem);

                }
                if (accountsPayableInvoicesLineItems.Any())
                    accountsPayableInvoices.LineItems = accountsPayableInvoicesLineItems;
            }

            return accountsPayableInvoices;
        }

        /// <summary>
        /// Permissions code that allows an external system to do a READ operation. This API will integrate information related to outgoing payments that 
        /// could be deemed personal.
        /// </summary>
        /// <exception><see cref="PermissionsException">PermissionsException</see></exception>
        private void CheckViewApInvoicesPermission()
        {
            var hasPermission = HasPermission(ColleagueFinancePermissionCodes.ViewApInvoices);

            if (!hasPermission)
            {
                throw new PermissionsException("User " + CurrentUser.UserId + " does not have permission to view AP.INVOICES.");
            }
        }

        /// <summary>
        /// Permissions code that allows an external system to do a UPDATE operation. This API will integrate information related to outgoing payments that 
        /// could be deemed personal.
        /// </summary>
        /// <exception><see cref="PermissionsException">PermissionsException</see></exception>
        private void CheckUpdateApInvoicesPermission()
        {
            var hasPermission = HasPermission(ColleagueFinancePermissionCodes.UpdateApInvoices);

            if (!hasPermission)
            {
                throw new PermissionsException("User " + CurrentUser.UserId + " does not have permission to update AP.INVOICES.");
            }
        }

        /// <summary>
        /// Get all CommerceTaxCode Entity Objects
        /// </summary>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        private async Task<IEnumerable<Domain.Base.Entities.CommerceTaxCode>> GetAllCommerceTaxCodesAsync(bool bypassCache)
        {
            return _commerceTaxCodes ?? (_commerceTaxCodes = await _referenceDataRepository.GetCommerceTaxCodesAsync(bypassCache));
        }

        /// <summary>
        /// Get all AccountsPayableSources Entity Objects
        /// </summary>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        private async Task<IEnumerable<Domain.ColleagueFinance.Entities.AccountsPayableSources>> GetAllAccountsPayableSourcesAsync(bool bypassCache)
        {
            return _accountsPayableSources ?? (_accountsPayableSources = await _colleagueFinanceReferenceDataRepository.GetAccountsPayableSourcesAsync(bypassCache));
        }

        /// <summary>
        /// Get all CommodityCode Entity Objects
        /// </summary>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        private async Task<IEnumerable<Domain.ColleagueFinance.Entities.CommodityCode>> GetAllCommodityCodesAsync(bool bypassCache)
        {
            return _commodityCodes ?? (_commodityCodes = await _colleagueFinanceReferenceDataRepository.GetCommodityCodesAsync(bypassCache));
        }

        /// <summary>
        /// Get all CommodityUnitType Entity Objects
        /// </summary>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        private async Task<IEnumerable<Domain.ColleagueFinance.Entities.CommodityUnitType>> GetAllCommodityUnitTypesAsync(bool bypassCache)
        {
            return _commodityUnitTypes ?? (_commodityUnitTypes = await _colleagueFinanceReferenceDataRepository.GetCommodityUnitTypesAsync(bypassCache));
        }

        /// <summary>
        /// Get all VendorTerms Entity Objects
        /// </summary>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        private async Task<IEnumerable<Domain.ColleagueFinance.Entities.VendorTerm>> GetAllVendorTermsAsync(bool bypassCache)
        {
            return _vendorTerms ?? (_vendorTerms = await _colleagueFinanceReferenceDataRepository.GetVendorTermsAsync(bypassCache));
        }

        /// <summary>
        /// Get GeneralLedgerAccountStructureEntity domain object
        /// </summary>
        /// <returns>GeneralLedgerAccountStructureEntity domain object</returns>
        private async Task<Domain.ColleagueFinance.Entities.GeneralLedgerAccountStructure> GetGeneralLedgerAccountStructure()
        {
            return _glAccountStructure ?? (_glAccountStructure = await _generalLedgerConfigurationRepository.GetAccountStructureAsync());
        }

        /// <summary>
        /// Check the GL's amounts and make sure they have funds avialable before proceeding.
        /// </summary>
        /// <param name="api"></param>
        /// <param name="ApiId"></param>
        /// <returns></returns>
        private async Task<List<Domain.ColleagueFinance.Entities.FundsAvailable>> checkFunds(AccountsPayableInvoices api, string ApiId = "")
        {
            var fundsAvailable = new List<Domain.ColleagueFinance.Entities.FundsAvailable>();
            var overrideAvailable = new List<Domain.ColleagueFinance.Entities.FundsAvailable>();
            //check if Accounting string has funds
            int lineCount = 0;

            foreach (var lineItems in api.LineItems)
            {
                int detailCount = 0;
                lineCount++;
                List<string> accountingStringList = new List<string>();
                foreach (var details in lineItems.AccountDetails)
                {
                    detailCount++;
                    var submittedBy = (details.SubmittedBy != null) ? details.SubmittedBy.Id :
                                    (api.SubmittedBy != null) ? api.SubmittedBy.Id : "";
                    var submittedById = (!string.IsNullOrEmpty(submittedBy)) ? await _personRepository.GetPersonIdFromGuidAsync(submittedBy) : "";

                    if (details.Allocation != null && details.Allocation.Allocated != null &&
                            details.Allocation.Allocated.Amount != null && details.Allocation.Allocated.Amount.Value != null
                            && details.Allocation.Allocated.Amount.Value.HasValue)
                    {
                        string PosID = lineCount.ToString() + "." + detailCount.ToString();
                        if (details.SubmittedBy != null)
                            PosID = PosID + ".DS";
                        fundsAvailable.Add(new Domain.ColleagueFinance.Entities.FundsAvailable(details.AccountingString)
                        {
                            Sequence = PosID,
                            SubmittedBy = submittedById,
                            Amount = details.Allocation.Allocated.Amount.Value.Value,
                            ItemId = lineItems.LineItemNumber ?? lineItems.LineItemNumber,
                            TransactionDate = api.TransactionDate
                        });
                    }

                    var accountingString = accountingStringList.Find(x => x.Equals(details.AccountingString));
                    if (string.IsNullOrWhiteSpace(accountingString))
                    {
                        accountingStringList.Add(details.AccountingString);
                    }
                    else
                    {
                        throw new Exception("A line item has two account details with the same GL number " + accountingString + " this is not allowed");
                    }
                }
            }
            if (fundsAvailable != null && fundsAvailable.Any())
            {
                if (string.IsNullOrEmpty(ApiId))
                {
                    ApiId = "NEW";
                }

                var availableFunds = await _accountFundAvailableRepository.CheckAvailableFundsAsync(fundsAvailable,"", ApiId);
                if (availableFunds != null)
                {
                    foreach (var availableFund in availableFunds)
                    {
                        if (availableFund.AvailableStatus == FundsAvailableStatus.NotAvailable)
                        {
                            throw new ArgumentException("The accounting string " + availableFund.AccountString + " does not have funds available");
                        }
                        if (availableFund.AvailableStatus == FundsAvailableStatus.Override ||
                            !string.IsNullOrEmpty(availableFund.SubmittedBy))
                        {
                            overrideAvailable.Add(availableFund);
                        }
                    }
                }
            }

            return overrideAvailable;
        }
    }
}