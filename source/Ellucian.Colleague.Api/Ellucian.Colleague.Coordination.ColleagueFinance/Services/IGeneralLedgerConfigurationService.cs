﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.ColleagueFinance;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Services
{
    /// <summary>
    /// Methods implemented for a General Ledger Configuration service.
    /// </summary>
    public interface IGeneralLedgerConfigurationService
    {
        /// <summary>
        /// Get General Ledger Configuration parameters.
        /// </summary>
        /// <returns></returns>
        Task<GeneralLedgerConfiguration> GetGeneralLedgerConfigurationAsync();
    }
}

