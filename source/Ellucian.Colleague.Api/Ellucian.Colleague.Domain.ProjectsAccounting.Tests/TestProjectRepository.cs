﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Repositories;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Tests
{
    // This class represents a set of available projects test data.
    public class TestProjectRepository : IProjectRepository
    {
        private List<Project> projects = new List<Project>();
        private bool projectsPopulated;

        #region Define all projects data
        private string[,] projectsArray = {
            //  ID      number      title                   status      type
            //                                              "A"ctive
            //                                              "I"nactive
            //                                              "X"C losed
            {   "1",    "AJK-100",  "Andy's 100 Project",   "A",        "C"},
            {   "2",    "AJK-200",  "Andy's 200 Project",   "I",        "R"},
            {   "3",    "AJK-300",  "Andy's 300 Project",   "X",        "R"},
            {   "4",    "AJK-400",  "Andy's 400 Project",   "A",        "R"},
            {   "5",    "AJK-500",  "Andy's 500 Project",   "A",        "R"},       // Has no line items
            {   "6",    "AJK-600",  "Andy's 600 Project",   "A",        "R"},       // Has no budget periods
            {   "null", "AJK-600",  "Andy's 700 Project",   "A",        "R"},       // Null ID for repository test
            {   "8",    "AJK-800",  "Andy's 800 Project",   "A",        "R"},       // Has only one line item
            {   "9",    "AJK-900",  "Andy's 900 Project",   "A",        "R"},       // Has only one line item
            {   "10",   "AJK-1000", "Andy's 1000 Project",  "A",        "R"},
        };

        // This array has an extra field to store the project ID so we know
        // which project its associated to.
        private string[,] budgetPeriodsArray = {
            //  Sequence Number     start date      end date        project ID
            {   "1",                "01/01/2012",   "12/31/2012",   "1"},
            {   "2",                "01/01/2013",   "12/31/2013",   "1"},
            {   "3",                "01/01/2014",   "12/31/2014",   "1"},
            {   "1",                "01/01/2010",   "12/31/2010",   "2"},
            {   "1",                "01/01/2011",   "12/31/2011",   "3"},
            {   "1",                "01/01/2012",   "12/31/2012",   "4"},
            {   "1",                "03/01/2015",   "06/30/2015",   "8"},
            {   "2",                "07/01/2015",   "09/30/2015",   "8"},
            {   "3",                "10/01/2015",   "12/31/2015",   "8"},
            {   "1",                "03/01/2015",   "06/30/2015",   "9"},
            {   "2",                "07/01/2015",   "09/30/2015",   "9"},
            {   "3",                "10/01/2015",   "12/31/2015",   "9"},
            {   "1",                "03/01/2015",   "06/30/2015",   "10"},
        };

        // This array has an extra field to store the project ID so we know
        // which project its associated to.
        private string[,] lineItemsArray = {
            //  Line Item ID      GL class        item code   project ID
            //                   "E"xpense
            //                   "A"sset
            //                   "R"evenue
            //                   "F"und Balance
            //                   "L"iability
            {   "11",            "E",             "C",        "1"},
            {   "12",            "E",             "C",        "1"},
            {   "13",            "E",             "C",        "1"},
            {   "14",            "R",             "C",        "1"},
            {   "24",            "E",             "C",        "2"},
            {   "35",            "E",             "C",        "3"}, // Has no GL Accounts associated
            {   "36",            "E",             "C",        "3"}, // Has no Budget Period Amounts associated
            {   "17",            "E",             "C",        "1"},
            {   "28",            "E",             "C",        "2"},
            {   "49",            "E",             "C",        "4"},
            {   "10",            "E",             "C",        "1"},
            {   "81",            "E",             "R",        "8"},
            {   "82",            "E",             "R",        "9"},

            {   "83",            "E",             "R",        "10"},
            {   "84",            "A",             "R",        "10"},
            {   "85",            "R",             "R",        "10"},
            {   "86",            "F",             "R",        "10"},
            {   "87",            "L",             "R",        "10"},
        };

        // This array has an extra field to store the line item ID so we know
        // which line item its associated to.
        private string[,] glAccountsArray = {
            //  GL number                   description         line item ID
            {"10_00_01_02_20601_51000",     "GL Number 1",      "11" },
            {"10_00_01_02_20601_52000",     "GL Number 2",      "11" },
            {"10_00_01_02_20601_52010",     "GL Number 3",      "12" },
            {"10_00_01_02_20601_53010",     "GL Number 4",      "12" },
            {"10_00_01_02_20601_53020",     "GL Number 5",      "13" },
            {"10_00_01_02_20601_55000",     "GL Number 6",      "24" },
            {"10_00_01_02_20601_51000",     "GL Number 1",      "28" },
            {"10_00_01_02_20601_51000",     "GL Number 1",      "81" },
            {"10_00_01_02_20601_53010",     "GL Number 4",      "81" },
            {"10_00_01_02_20601_51000",     "GL Number 1",      "82" },

            {"10_00_01_02_20601_51000",     "GL Number 1",      "83" },
            {"10_00_01_02_20601_51000",     "GL Number 1",      "84" },
            {"10_00_01_02_20601_51000",     "GL Number 1",      "85" },
            {"10_00_01_02_20601_51000",     "GL Number 1",      "86" },
            {"10_00_01_02_20601_51000",     "GL Number 1",      "87" },
        };

        // This array has an extra field to store the line item ID so we know
        // which line item its associated to.
        private string[,] budgetPeriodAmountsArray = {
            //                      <---------------------------------------------- amounts --------------------------------------------->
            //  budget prd    budget       requisition     memo enc     posted enc     memo actuals    posted actuals     line item ID
            {   "1",          "100.00",    "100.00",         "0.00",    "150.00",        "0.00",       "100.00",          "11"},
            {   "2",          "550.00",      "0.00",       "100.00",      "0.00",      "100.00",         "0.00",          "11"},
            {   "3",          "225.00",      "0.00",         "0.00",    "100.00",        "0.00",         "0.00",          "11"},
            {   "1",          "225.00",      "5.00",       "460.00",    "100.00",      "150.00",        "40.00",          "14"},
            {   "1",          "114.00",     "50.00",        "50.00",     "50.00",      "100.00",         "0.00",          "49"},
            {   "1",           "97.00",     "25.00",        "25.00",     "25.00",        "0.00",       "100.00",          "28"},
            {   "1",          "110.00",      "0.00",        "26.00",      "0.00",        "0.00",       "100.00",          "12"},
            {   "1",           "10.00",    "200.00",        "15.00",     "25.00",       "10.00",       "444.00",          "81"},
            {   "2",           "10.00",      "0.00",        "35.00",    "197.00",       "20.00",       "580.00",          "81"},
            {   "3",           "10.00",      "0.00",       "111.00",    "219.00",       "30.00",       "710.00",          "81"},
            {   "1",           "10.00",    "200.00",        "15.00",     "25.00",       "10.00",       "444.00",          "82"},
            {   "2",           "10.00",      "0.00",        "35.00",    "197.00",       "20.00",       "580.00",          "82"},
            {   "3",           "10.00",      "0.00",       "111.00",    "219.00",       "30.00",       "710.00",          "82"},
            
            {   "1",           "10.00",    "200.00",        "15.00",     "25.00",       "10.00",       "444.00",          "82"},
        };

        // This array has an extra field to store the line item ID so we know
        // which line item its associated to. The amounts in this array represent ONLY posted amounts.
        // These transactions belong to project line item 81 in project 8.
        private string[,] paGlaTransactionsArray = {
            //  ID     type    gl number                   amount       ref no        date            trans desc     line item ID     budget period
            {   "7",   "ER",  "10_00_01_02_20601_51000",   "200.00",    "0004444",    "06/01/2015",   "WalMart",     "81",            "1"},
            {  "14",   "EP",  "10_00_01_02_20601_51000",    "25.00",    "P0001111",   "03/01/2015",   "OfficeMax",   "81",            "1"},
            {  "13",   "EP",  "10_00_01_02_20601_51000",   "-25.00",    "P0001111",   "08/15/2015",   "OfficeMax",   "81",            "2"},
            {  "10",   "AE",  "10_00_01_02_20601_51000",   "111.00",    "E0001234",   "10/23/2015",   "Safeway",     "81",            "3"},
            {   "9",   "AE",  "10_00_01_02_20601_51000",   "-25.00",    "E0001234",   "11/14/2015",   "Safeway",     "81",            "3"},
            {   "6",   "EP",  "10_00_01_02_20601_51000",   "222.00",    "RV005555",   "06/21/2015",   "Food Lion",   "81",            "2"},
            {   "5",   "AE",  "10_00_01_02_20601_51000",   "333.00",    "E0006666",   "11/01/2015",   "Safeway",     "81",            "3"},
            {   "1",   "AE",  "10_00_01_02_20601_51000",  "-200.00",    "E0006666",   "12/01/2015",   "Safeway",     "81",            "3"},
            {  "15",   "PJ",  "10_00_01_02_20601_51000",    "25.00",    "V0000222",   "09/01/2015",   "Costco",      "81",            "2"},
            {  "12",   "PJ",  "10_00_01_02_20601_51000",   "-25.00",    "V0000222",   "09/09/2015",   "Costco",      "81",            "2"},
            {  "11",   "PJ",  "10_00_01_02_20601_51000",    "25.00",    "V0000222",   "09/09/2015",   "Costco",      "81",            "2"},           
            {   "8",   "JE",  "10_00_01_02_20601_51000",    "44.00",    "J0003333",   "10/30/2015",   "Target",      "81",            "3"},
            {   "4",   "AA",  "10_00_01_02_20601_51000",   "444.00",    "CR007777",   "04/01/2015",   "Giant",       "81",            "1"},
            {   "3",   "AA",  "10_00_01_02_20601_51000",   "555.00",    "I0008888",   "08/11/2015",   "Home Depot",  "81",            "2"},
            {   "2",   "AA",  "10_00_01_02_20601_51000",   "666.00",    "CK009999",   "11/17/2015",   "Michaels",    "81",            "3"},

            {  "16",   "ER",  "10_00_01_02_20601_51000",   "333.00",    "000123",   "11/01/2015",   "Safeway",     "82",            "1"},
            {  "17",   "ER",  "10_00_01_02_20601_51000",  "-200.00",    "000456",   "12/01/2015",   "Safeway",     "82",            "2"},
            {  "18",   "ER",  "10_00_01_02_20601_51000",    "25.00",    "000789",   "09/01/2015",   "Costco",      "82",            "3"},
            {  "19",   "EP",  "10_00_01_02_20601_51000",   "333.00",    "P0000123",   "11/01/2015",   "Safeway",     "82",            "1"},
            {  "20",   "EP",  "10_00_01_02_20601_51000",  "-200.00",    "P0000456",   "12/01/2015",   "Safeway",     "82",            "2"},
            {  "21",   "EP",  "10_00_01_02_20601_51000",    "25.00",    "P0000789",   "09/01/2015",   "Costco",      "82",            "3"},
            {  "22",   "EP",  "10_00_01_02_20601_51000",   "333.00",    "B0000123",   "11/01/2015",   "Safeway",     "82",            "1"},
            {  "23",   "EP",  "10_00_01_02_20601_51000",  "-200.00",    "B0000456",   "12/01/2015",   "Safeway",     "82",            "2"},
            {  "24",   "EP",  "10_00_01_02_20601_51000",    "25.00",    "B0000789",   "09/01/2015",   "Costco",      "82",            "3"},
        };
        #endregion

        public TestProjectRepository()
        {
            projectsPopulated = false;
        }

        public async Task<IEnumerable<Project>> GetProjectsAsync(IEnumerable<string> projectIds, bool summaryOnly, string sequenceNumber)
        {
            if (!projectsPopulated)
            {
                Populate(sequenceNumber);
            }

            var projectsList = new List<Project>();
            foreach (var projectId in projectIds)
            {
                try
                {
                    projectsList.Add(projects.Single(x => x.ProjectId == projectId));
                }
                catch (InvalidOperationException exception)
                {
                    // Log this exception
                }
            }
            return await Task.Run(() => projectsList);
        }

        public async Task<Project> GetProjectAsync(string projectId, bool summaryOnly, string sequenceNumber)
        {
            if (!projectsPopulated)
            {
                Populate(sequenceNumber);
            }

            // Return the first project that matches the supplied ID.
            // return Projects.FirstOrDefault(x => x.ProjectId == projectId);
            return await Task.Run(() => projects.FirstOrDefault(x => x.ProjectId == projectId));
        }

        private void Populate(string sequenceNumber)
        {
            #region Populate the project objects
            // Loop through the projects array and create project domain entities
            string projectId,
                number,
                title,
                type;
            ProjectStatus status;
            for (var i = 0; i < projectsArray.GetLength(0); i++)
            {
                projectId = projectsArray[i, 0];
                number = projectsArray[i, 1];
                title = projectsArray[i, 2];
                switch (projectsArray[i, 3])
                {
                    case "A":
                        status = ProjectStatus.Active;
                        break;
                    case "I":
                        status = ProjectStatus.Inactive;
                        break;
                    case "X":
                        status = ProjectStatus.Closed;
                        break;
                    default:
                        throw new Exception("Invalid status specified in TestProjectRepository.");
                }
                type = projectsArray[i, 4];
                projects.Add(new Project(projectId, number, title, status, type));
            }
            #endregion

            #region Populate the budget periods for each project
            // Loop through the budget periods array and add the budget period domain entity appropriate projects
            string budgetPeriodSequenceNumber,
                budgetPeriodProjectId;
            DateTime startDate,
                endDate;
            for (var i = 0; i < budgetPeriodsArray.GetLength(0); i++)
            {
                budgetPeriodSequenceNumber = budgetPeriodsArray[i, 0];
                startDate = Convert.ToDateTime(budgetPeriodsArray[i, 1]);
                endDate = Convert.ToDateTime(budgetPeriodsArray[i, 2]);
                budgetPeriodProjectId = budgetPeriodsArray[i, 3];

                foreach (var project in projects)
                {
                    if (project.ProjectId == budgetPeriodProjectId)
                    {
                        project.AddBudgetPeriod(new ProjectBudgetPeriod(budgetPeriodSequenceNumber, startDate, endDate));
                    }
                }
            }
            #endregion

            #region Populate the line items for each project
            // Loop through the line items array and add the line item domain entity to the appropriate project
            string lineItemId,
                itemCode,
                lineItemProjectId;
            GlClass glClass;
            for (var i = 0; i < lineItemsArray.GetLength(0); i++)
            {
                lineItemId = lineItemsArray[i, 0];
                switch (lineItemsArray[i, 1])
                {
                    case "E":
                        glClass = GlClass.Expense;
                        break;
                    case "A":
                        glClass = GlClass.Asset;
                        break;
                    case "R":
                        glClass = GlClass.Revenue;
                        break;
                    case "F":
                        glClass = GlClass.FundBalance;
                        break;
                    case "L":
                        glClass = GlClass.Liability;
                        break;
                    default:
                        throw new Exception("Invalid GL Class specified in TestProjectRepository.");
                }
                itemCode = lineItemsArray[i, 2];
                lineItemProjectId = lineItemsArray[i, 3];

                foreach (var project in projects)
                {
                    if (project.ProjectId == lineItemProjectId)
                    {
                        project.AddLineItem(new ProjectLineItem(lineItemId, glClass, itemCode));
                    }
                }
            }
            #endregion

            #region Populate the GL accounts for each line item
            string glAccountNumber,
                glDescription,
                glAccountLineItemId;
            ProjectLineItemGlAccount glAccount;
            for (var i = 0; i < glAccountsArray.GetLength(0); i++)
            {
                glAccountNumber = glAccountsArray[i, 0];
                glDescription = glAccountsArray[i, 1];
                glAccountLineItemId = glAccountsArray[i, 2];
                glAccount = new ProjectLineItemGlAccount(glAccountNumber);
                glAccount.GlAccountDescription = glDescription;

                foreach (var project in projects)
                {
                    foreach (var lineItem in project.LineItems)
                    {
                        if (lineItem.Id == glAccountLineItemId)
                        {
                            lineItem.AddGlAccount(glAccount);
                        }
                    }
                }
            }
            #endregion

            #region Populate the budget period amounts for each line item
            string budgetPeriodAmountSequenceNumber,
                budgetPeriodAmountLineItemId;
            decimal budgetAmount = 0,
                requisitionAmount = 0,
                memoEncumbranceAmount = 0,
                postedEncumbranceAmount = 0,
                memoActualsAmount = 0,
                postedActualsAmount = 0;
            BudgetPeriodAmount budgetPeriodAmount;
            for (var i = 0; i < budgetPeriodAmountsArray.GetLength(0); i++)
            {
                budgetPeriodAmountSequenceNumber = budgetPeriodAmountsArray[i, 0];
                budgetAmount = Convert.ToDecimal(budgetPeriodAmountsArray[i, 1]);
                requisitionAmount = Convert.ToDecimal(budgetPeriodAmountsArray[i, 2]);
                memoEncumbranceAmount = Convert.ToDecimal(budgetPeriodAmountsArray[i, 3]);
                postedEncumbranceAmount = Convert.ToDecimal(budgetPeriodAmountsArray[i, 4]);
                memoActualsAmount = Convert.ToDecimal(budgetPeriodAmountsArray[i, 5]);
                postedActualsAmount = Convert.ToDecimal(budgetPeriodAmountsArray[i, 6]);
                budgetPeriodAmountLineItemId = budgetPeriodAmountsArray[i, 7];
                budgetPeriodAmount = new BudgetPeriodAmount(budgetPeriodAmountSequenceNumber, budgetAmount, requisitionAmount, memoEncumbranceAmount,
                    postedEncumbranceAmount, memoActualsAmount, postedActualsAmount);

                foreach (var project in projects)
                {
                    foreach (var lineItem in project.LineItems)
                    {
                        if (lineItem.Id == budgetPeriodAmountLineItemId)
                        {
                            lineItem.AddBudgetPeriodAmount(budgetPeriodAmount);
                        }
                    }
                }
            }
            #endregion

            #region Populate the project transactions for each line item GL account
            string transactionId,
                transactionSource,
                transactionGlNumber,
                transactionReferenceNumber,
                transactionDescription,
                transactionLineItemId,
                transactionSequenceNumber;
            GlTransactionType transactionType;
            decimal transactionAmount;
            DateTime transactionDate;
            for (var i = 0; i < paGlaTransactionsArray.GetLength(0); i++)
            {
                transactionId = paGlaTransactionsArray[i, 0];
                switch (paGlaTransactionsArray[i, 1])
                {
                    case "AA":
                        transactionType = GlTransactionType.Actual;
                        break;
                    case "PJ":
                        transactionType = GlTransactionType.Actual;
                        break;
                    case "JE":
                        transactionType = GlTransactionType.Actual;
                        break;
                    case "AE":
                        transactionType = GlTransactionType.Encumbrance;
                        break;
                    case "EP":
                        transactionType = GlTransactionType.Encumbrance;
                        break;
                    case "ER":
                        transactionType = GlTransactionType.Requisition;
                        break;

                    default:
                        throw new Exception("Invalid transaction type specified in TestProjectRepository.");
                }
                transactionGlNumber = paGlaTransactionsArray[i, 2];
                transactionAmount = Convert.ToDecimal(paGlaTransactionsArray[i, 3]);
                transactionReferenceNumber = paGlaTransactionsArray[i, 4];
                transactionDate = Convert.ToDateTime(paGlaTransactionsArray[i, 5]);
                transactionDescription = paGlaTransactionsArray[i, 6];
                transactionLineItemId = paGlaTransactionsArray[i, 7];
                transactionSequenceNumber = paGlaTransactionsArray[i, 8];
                transactionSource = paGlaTransactionsArray[i, 1];
                foreach (var project in projects)
                {
                    foreach (var lineItem in project.LineItems)
                    {
                        foreach (var transactionGlAccount in lineItem.GlAccounts)
                        {
                            if (lineItem.Id == transactionLineItemId
                                && (transactionSequenceNumber == sequenceNumber || string.IsNullOrEmpty(sequenceNumber))
                                && transactionGlAccount.GlAccountNumber == transactionGlNumber)
                            {
                                transactionGlAccount.AddProjectTransaction(new ProjectTransaction(transactionId, transactionType, transactionSource, transactionGlNumber, transactionAmount,
                                                                           transactionReferenceNumber, transactionDate, transactionDescription, transactionLineItemId));
                            }
                        }
                    }
                            }
                        }

            // Remove those encumbrance or requisition transactions whose amount is $0.00
            foreach (var project in projects)
            {
                foreach (var lineItem in project.LineItems)
                {
                    foreach (var glAccountObject in lineItem.GlAccounts)
                    {
                        glAccountObject.RemoveZeroEncumbranceTransactions();
                    }
                }
            }

            #endregion

            projectsPopulated = true;
        }
    }
}
