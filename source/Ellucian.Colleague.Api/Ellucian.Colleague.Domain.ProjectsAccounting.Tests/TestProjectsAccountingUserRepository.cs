﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Repositories;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Tests
{
    /// <summary>
    /// This class creates a list of ProjectsAccountingUser objects. The list of objects allows
    /// for testing users that have access to all, a subset, and none of the project in the 
    /// project repository. It also allows for a test of a user that has access to a project
    /// that does not exist in the project repository.
    /// </summary>
    public class TestProjectsAccountingUserRepository : IProjectsAccountingUserRepository
    {
        private List<ProjectsAccountingUser> ProjectsAccountingUsers;

        public TestProjectsAccountingUserRepository()
        {
            ProjectsAccountingUsers = new List<ProjectsAccountingUser>();

            // This user has a subset of projects
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000001", "LastName"));
            var projectIds = new List<string>();
            projectIds.Add("1");
            projectIds.Add("2");
            projectIds.Add("3");
            projectIds.Add("8");
            ProjectsAccountingUsers[0].AddProjects(projectIds);

            // This user has no projects
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000002", "LastName"));

            // This user has projects that dont exist
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000003", "LastName"));
            ProjectsAccountingUsers[2].AddProjects(new List<string>() { "ZZZ" });

            // This user has all projects
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000004", "LastName"));
            projectIds = new List<string>();
            projectIds.Add("1");
            projectIds.Add("2");
            projectIds.Add("3");
            projectIds.Add("4");
            ProjectsAccountingUsers[3].AddProjects(projectIds);
            
            // This user has the full GL access role active
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000005", "Thorne"));

            // This user has at least one active GL role
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000006", "Kleehammer"));

            // This user has no active GL roles
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000007", "Longerbeam"));

            // This user has an expired GL access record
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000008", "Longerbeam"));

            // This user does not have a GL access record
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000009", "Longerbeam"));

            // This user does not have a staff login ID
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000010", "Longerbeam"));

            // This user does not have a staff record
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000011", "Longerbeam"));

            // This user has the full GL access role active and an additional role active
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000012", "Thorne"));

            // This user has the full GL access role active and an additional role active
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000013", "Thorne"));

            // This user has the full GL access role active and an additional role active
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000014", "Thorne"));

            // This user has the full GL access role active and an additional role active
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000015", "Thorne"));

            // This user has the full GL access role which is null and has no other active role
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000016", "Thorne"));

            // This user has three active roles where the 2nd role has no start date
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000017", "Thorne"));

            // This user has three active roles where the 2nd role has no end date
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000018", "Thorne"));

            // This user has three active roles where the 3rd role has no end date
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000019", "Thorne"));

            // This user has three active roles where the 3rd role has no start date
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000020", "Thorne"));

            // This user has one active role where the role has no start date
            ProjectsAccountingUsers.Add(new ProjectsAccountingUser("0000021", "Thorne"));
        }
        
        public async Task<ProjectsAccountingUser> GetProjectsAccountingUserAsync(string id)
        {
            // get the first project accounting user that matches the id passed into the method.
            var p = ProjectsAccountingUsers.First(x => x.Id == id);
            return await Task.Run(() => p);
        }
    }
}