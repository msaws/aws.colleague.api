﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Tests.Entities
{
    /// <summary>
    /// This class tests the valid and invalid conditions of a ProjectType object.
    /// </summary>
    [TestClass]
    public class ProjecItemCodeTests
    {
        #region Initialize and Cleanup
        [TestInitialize]
        public void Initialize()
        {

        }

        [TestCleanup]
        public void Cleanup()
        {
        }
        #endregion

        #region Constructor Tests
        [TestMethod]
        public void ProjectItemCodeConstructor_Success()
        {
            var code = "R";
            var description = "Research";
            var projectItemCode = new ProjectItemCode(code, description);
            Assert.AreEqual(code, projectItemCode.Code);
            Assert.AreEqual(description, projectItemCode.Description);
        }
        #endregion
    }
}