﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Tests.Entities
{
    /// <summary>
    /// This class tests valid and invalid conditions for a Project Line Item GL account object.
    /// </summary>
    [TestClass]
    public class ProjectLineItemGLAccountTests
    {
        #region Initialize and Cleanup
        private ProjectLineItemGlAccount glAccount;
        private ProjectTransaction projectTransaction;
        private ProjectTransaction projectTransaction2;
        private string glAccountNnumber;
        private string description;
        private TestProjectRepository testProjectRepository;

        [TestInitialize]
        public void Initialize()
        {
            // Initialize the GL Account object.
            glAccountNnumber = "01_02_00_01_3333_33333";
            description = String.Empty;
            glAccount = new ProjectLineItemGlAccount(glAccountNnumber);

            // Initialize the project transaction object.
            string id = "1";
            GlTransactionType type = GlTransactionType.Encumbrance;
            string source = "EP";
            string glAccountNumber = "13_12345_111_2222_33333";
            decimal amount = 50.00m;
            string referenceNumber = "P0001234";
            DateTime transactionDate = Convert.ToDateTime("01/01/2014");
            string transactionDescription = "OfficeMax Supplies";
            string projectLineItemId = "101";
            projectTransaction = new ProjectTransaction(id, type, source, glAccountNumber, amount, referenceNumber, transactionDate, transactionDescription, projectLineItemId);

            string id2 = "2";
            GlTransactionType type2 = GlTransactionType.Actual;
            string source2 = "PJ";
            string glAccountNumber2 = "13_12345_111_2222_33333";
            decimal amount2 = 50.00m;
            string referenceNumber2 = "V0001234";
            DateTime transactionDate2 = Convert.ToDateTime("01/01/2014");
            string transactionDescription2 = "OfficeMax Supplies";
            string projectLineItemId2 = "101";
            projectTransaction2 = new ProjectTransaction(id2, type2, source2, glAccountNumber2, amount2, referenceNumber2, transactionDate2, transactionDescription2, projectLineItemId2);

            testProjectRepository = new TestProjectRepository();
        }

        [TestCleanup]
        public void Cleanup()
        {
            glAccount = null;
            projectTransaction = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void ProjectLineItemGLAccountConstructor_Success()
        {
            // Initialize the GL account object.
            decimal actuals = 0.00m;
            decimal encumbrances = 0.00m;
            Assert.AreEqual(glAccountNnumber, glAccount.GlAccountNumber);
            Assert.AreEqual(description, glAccount.GlAccountDescription);
            Assert.AreEqual(0, glAccount.ProjectTransactions.Count());
            Assert.AreEqual(actuals, glAccount.Actuals);
            Assert.AreEqual(encumbrances, glAccount.Encumbrances);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ProjectLineItemGLAccountConstructor_NullGLAccount()
        {
            string glAccount = null;
            ProjectLineItemGlAccount projectGlAccount = new ProjectLineItemGlAccount(glAccount);
        }
        #endregion

        #region AddTransactions Tests
        [TestMethod]
        public void AddTransaction_Success()
        {
            Assert.AreEqual(0, glAccount.ProjectTransactions.Count());
            glAccount.AddProjectTransaction(projectTransaction);
            Assert.AreEqual(1, glAccount.ProjectTransactions.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddTransaction_NullTransaction()
        {
            ProjectTransaction transaction = null;
            var transactions = new List<ProjectTransaction>();
            transactions.Add(transaction);
            glAccount.AddProjectTransaction(transaction);
        }

        [TestMethod]
        public void AddTransaction_DuplicateActualsTransaction()
        {
            Assert.AreEqual(0, glAccount.ProjectTransactions.Count(), "Count should start at 0.");
            glAccount.AddProjectTransaction(projectTransaction2);
            Assert.AreEqual(1, glAccount.ProjectTransactions.Count(), "Count should be 1.");
            glAccount.AddProjectTransaction(projectTransaction2);
            Assert.AreEqual(1, glAccount.ProjectTransactions.Count(), "Count should still be 1.");
        }
         

        [TestMethod]
        public async Task AddTransactions_EncSumAndActualsLeftAlone()
        {
            var project = await testProjectRepository.GetProjectAsync("2", false, null);
            var lineItem = project.LineItems.FirstOrDefault(x => x.Id == "28");
            var testGlAccount = lineItem.GlAccounts.FirstOrDefault(x => x.GlAccountNumber == "10_00_01_02_20601_51000");

            // Put the transactions into a list. Make a copy for future verification.
            var transactions = testGlAccount.ProjectTransactions.ToList();
            var transactionsCopy = transactions;

            foreach (var transaction in transactions)
            {
                glAccount.AddProjectTransaction(transaction);
            }

            // Confirm that the new consolidated list has added the amounts for encumbrance and requisitions,
            // and if necessary removed the encumbrance and requisition transactions that added to 0.00.
            // The transactions are sorted by descendant transaction date.
            // Actuals transactions are not consolidated.
            // Currently there are no budget amounts in PA.GLA (source file).
            foreach (var transaction in transactions)
            {
                // First, differentiate between Actuals and Encumbrances
                if (transaction.GlTransactionType == GlTransactionType.Actual)
                {
                    // Get the list of actuals transactions from the cloned list where ref no is the same.
                    // Loop through those txs and confirm that they all exist (with the same data) in the new, consolidated list.
                    // Confirm that the resulting number of transactions is equal in each list.
                    var originalTransactions = transactionsCopy.Where(x =>
                        x.ReferenceNumber == transaction.ReferenceNumber
                        && x.GlTransactionType == GlTransactionType.Actual);
                    var newTransactions = glAccount.ProjectTransactions.Where(x =>
                        x.ReferenceNumber == transaction.ReferenceNumber
                        && x.GlTransactionType == GlTransactionType.Actual);

                    // Confirm that the number of transactions with the same reference number is the same in both lists.
                    Assert.AreEqual(originalTransactions.Count(), newTransactions.Count());

                    // Confirm that the ids, amounts and transaction dates for the involved transactions is the same.
                    Assert.IsTrue(newTransactions.Any(x =>
                        x.Id == transaction.Id
                        && x.Amount == transaction.Amount
                        && x.TransactionDate == transaction.TransactionDate));
                }
                else
                {
                    // Get all encumbrance and requisition transactions for the current reference number from the cloned list.
                    // Get sum of the transactions.
                    // Confirm the following:
                    //    - the new list contains only one transaction with that reference number.
                    //    - the amount is equal to the sum of the amounts from the original list.
                    //    - the date is the most recent date (need to compare date in new list to dates in original list).
                    var origTransactions = transactionsCopy.Where(x =>
                        x.ReferenceNumber == transaction.ReferenceNumber
                        && (x.GlTransactionType == GlTransactionType.Encumbrance || x.GlTransactionType == GlTransactionType.Requisition));
                    var transactionSum = origTransactions.Sum(x => x.Amount);
                    var newTransactions = glAccount.ProjectTransactions.Where(x =>
                        x.ReferenceNumber == transaction.ReferenceNumber
                        && (x.GlTransactionType == GlTransactionType.Encumbrance || x.GlTransactionType == GlTransactionType.Requisition));

                    if (transactionSum != 0)
                    {
                        Assert.AreEqual(newTransactions.Count(), 1);
                        Assert.AreEqual(transactionSum, newTransactions.Sum(x => x.Amount));
                        Assert.AreEqual(origTransactions.FirstOrDefault().TransactionDate, newTransactions.FirstOrDefault().TransactionDate);
                    }
                    
                }
            }
        }

        [TestMethod]
        public async Task AddTransactions_EncumbranceTransactionsSumToZero()
        {
            // Obtain the list of transactions from our test repository.
            var project = await testProjectRepository.GetProjectAsync("2", false, null);
            var lineItem = project.LineItems.FirstOrDefault(x => x.Id == "28");
            var testGlAccount = lineItem.GlAccounts.FirstOrDefault(x => x.GlAccountNumber == "10_00_01_02_20601_51000");

            // Put the transactions into a list. Make a copy for future verification.
            var transactions = testGlAccount.ProjectTransactions.ToList();
            var transactionsCopy = transactions;

            glAccount.AddProjectTransaction(projectTransaction);

            // Confirm that the new consolidated list has added the amounts for encumbrance and requisitions,
            // and if necessary removed the encumbrance and requisition transactions that added to 0.00.
            // The transactions are sorted by descendant transaction date.
            // Actuals transactions are not consolidated.
            // Currently there are no budget amounts in PA.GLA (source file).
            foreach (var transaction in transactions)
            {
                // Get all encumbrance and requisition transactions for the current reference number from the cloned list.
                var tempTransactions = transactionsCopy.Where(x =>
                    x.ReferenceNumber == transaction.ReferenceNumber
                    && (x.GlTransactionType == GlTransactionType.Encumbrance || x.GlTransactionType == GlTransactionType.Requisition));

                // Get the sum of the transactions and if the sum is $0 make sure that those transactions are not in the new list.
                var transactionSum = tempTransactions.Sum(x => x.Amount);

                if (transactionSum == 0)
                {
                    Assert.AreEqual(glAccount.ProjectTransactions.Where(x => x.ReferenceNumber == transaction.ReferenceNumber).Count(), 0);
                }
            }
        }
        #endregion
    }
}
