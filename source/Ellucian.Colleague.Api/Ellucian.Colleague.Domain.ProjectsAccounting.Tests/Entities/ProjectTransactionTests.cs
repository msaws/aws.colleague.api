﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Tests.Entities
{
    [TestClass]
    public class ProjectTransactionTests
    {
        #region Initialize and Cleanup
        private ProjectTransaction projectTransaction;

        [TestInitialize]
        public void Initialize()
        {

        }

        [TestCleanup]
        public void Cleanup()
        {
            projectTransaction = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void ProjectTransactionConstructor_Success()
        {
            // Initialize the GL transaction object.
            string id = "1";
            GlTransactionType type = GlTransactionType.Encumbrance;
            string source = "EP";
            string glAccount = "13_12345_111_2222_33333";
            decimal amount = 50.00m;
            string referenceNumber = "P0001234";
            DateTime transactionDate = Convert.ToDateTime("01/01/2014");
            string description = "OfficeMax Supplies";
            string projectLineItemId = "101";
            projectTransaction = new ProjectTransaction(id, type, source, glAccount, amount, referenceNumber, transactionDate, description, projectLineItemId);

            Assert.AreEqual(projectLineItemId, projectTransaction.ProjectLineItemId);
            Assert.AreEqual(type, projectTransaction.GlTransactionType);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ProjectTransactionConstructor_NullProjectLineItemId()
        {
            // Initialize the GL transaction object.
            string id = "1";
            GlTransactionType type = GlTransactionType.Actual;
            string source = "EP";
            string glAccount = "13_12345_111_2222_33333";
            decimal amount = 50.00m;
            string referenceNumber = "P0001234";
            DateTime transactionDate = Convert.ToDateTime("01/01/2014");
            string description = "OfficeMax Supplies";
            string projectLineItemId = null;
            projectTransaction = new ProjectTransaction(id, type, source, glAccount, amount, referenceNumber, transactionDate, description, projectLineItemId);
        }
        #endregion
    }
}
