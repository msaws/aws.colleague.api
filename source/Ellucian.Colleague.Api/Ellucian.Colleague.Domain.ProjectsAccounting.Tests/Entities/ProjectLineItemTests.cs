﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Tests.Entities
{
    [TestClass]
    public class ProjectLineItemTests
    {
        #region Initialize and Cleanup
        TestProjectRepository testProjectRepository;

        [TestInitialize]
        public void Initialize()
        {
            testProjectRepository = new TestProjectRepository();
        }

        [TestCleanup]
        public void Cleanup()
        {
            testProjectRepository = null;
        }
        #endregion

        #region Constructor tests
        /// <summary>
        /// We currently do not have any failure tests because you cannot pass
        /// in a null for the enumeration GlClassType.
        /// </summary>
        [TestMethod]
        public void ProjectLineItemConstructor_Success()
        {
            string id = "11";
            GlClass glClass = GlClass.Expense;
            string itemCode = "R";
            ProjectLineItem lineItem = new ProjectLineItem(id, glClass, itemCode);
            Assert.AreEqual(id, lineItem.Id);
            Assert.AreEqual(glClass, lineItem.GlClass);
            Assert.AreEqual(itemCode, lineItem.ItemCode);
            Assert.IsTrue(lineItem.GlAccounts.Count() == 0);
            Assert.IsTrue(lineItem.BudgetPeriodAmounts.Count() == 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ProjectLineItemConstructor_NullId()
        {
            string id = null;
            GlClass glClass = GlClass.Expense;
            string itemCode = "R";
            ProjectLineItem lineItem = new ProjectLineItem(id, glClass, itemCode);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ProjectLineItemConstructor_NullItemCode()
        {
            string id = "11";
            GlClass glClass = GlClass.Expense;
            string itemCode = null;
            ProjectLineItem lineItem = new ProjectLineItem(id, glClass, itemCode);
        }
        #endregion

        #region Tests for TotalBudget
        [TestMethod]
        public async Task TotalBudget_HappyPath()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - We want the total amount for all budget periods
             */
            string projectId = "1";
            bool summaryOnly = true;
            string sequenceNumber = null;
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Get the first line item on the project
            var lineItem = project.LineItems.FirstOrDefault(x => x.Id == "11");

            // Set up the expected total
            decimal totalBudget_Expected = lineItem.BudgetPeriodAmounts.Sum(x => x.BudgetAmount);
            Assert.AreEqual(totalBudget_Expected, lineItem.TotalBudget(sequenceNumber));
        }

        [TestMethod]
        public async Task TotalBudget_OnlyOneBudgetPeriod()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - We want the total amount for only one budget period
             */
            string projectId = "1";
            bool summaryOnly = true;
            string sequenceNumber = "2";
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Get the first line item on the project
            var lineItem = project.LineItems.FirstOrDefault(x => x.Id == "11");

            // Set up the expected total
            var budgetPeriodAmount = lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber);
            decimal totalBudget_Expected = budgetPeriodAmount.Sum(x => x.BudgetAmount);
            Assert.AreEqual(totalBudget_Expected, lineItem.TotalBudget(sequenceNumber));
        }
        #endregion

        #region Tests for TotalActuals
        [TestMethod]
        public async Task TotalActuals_HappyPath()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - We want the total amount for all budget periods
             */
            string projectId = "1";
            bool summaryOnly = true;
            string sequenceNumber = null;
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Get the first line item on the project
            var lineItem = project.LineItems.FirstOrDefault(x => x.Id == "11");

            // Set up the expected total
            decimal totalActuals_Expected = 0.00m;
            totalActuals_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoActualsAmount);
            totalActuals_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedActualsAmount);
            Assert.AreEqual(totalActuals_Expected, lineItem.TotalActuals(sequenceNumber));
        }

        [TestMethod]
        public async Task TotalActuals_OnlyOneBudgetPeriod()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - We want the total amount for only one budget period
             */
            string projectId = "1";
            bool summaryOnly = true;
            string sequenceNumber = "2";
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Get the first line item on the project
            var lineItem = project.LineItems.FirstOrDefault(x => x.Id == "11");

            // Set up the expected total
            decimal totalActuals_Expected = 0.00m;
            var budgetPeriodAmount = lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber);
            totalActuals_Expected += budgetPeriodAmount.Sum(x => x.MemoActualsAmount);
            totalActuals_Expected += budgetPeriodAmount.Sum(x => x.PostedActualsAmount);
            Assert.AreEqual(totalActuals_Expected, lineItem.TotalActuals(sequenceNumber));
        }
        #endregion

        #region Tests for TotalEncumbrances
        [TestMethod]
        public async Task TotalEncumbrances_HappyPath()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - We want the total amount for all budget periods
             */
            string projectId = "1";
            bool summaryOnly = true;
            string sequenceNumber = null;
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Get the first line item on the project
            var lineItem = project.LineItems.FirstOrDefault(x => x.Id == "11");

            // Set up the expected total
            decimal totalEncumbrances_Expected = 0.00m;
            totalEncumbrances_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.RequisitionAmount);
            totalEncumbrances_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoEncumbranceAmount);
            totalEncumbrances_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedEncumbranceAmount);
            Assert.AreEqual(totalEncumbrances_Expected, lineItem.TotalEncumbrances(sequenceNumber));
        }

        [TestMethod]
        public async Task TotalEncumbrances_OnlyOneBudgetPeriod()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - We want the total amount for only one budget period
             */
            string projectId = "1";
            bool summaryOnly = true;
            string sequenceNumber = "2";
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Get the first line item on the project
            var lineItem = project.LineItems.FirstOrDefault(x => x.Id == "11");

            // Set up the expected total
            decimal totalEncumbrances_Expected = 0.00m;
            var budgetPeriodAmount = lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber);
            totalEncumbrances_Expected += budgetPeriodAmount.Sum(x => x.RequisitionAmount);
            totalEncumbrances_Expected += budgetPeriodAmount.Sum(x => x.MemoEncumbranceAmount);
            totalEncumbrances_Expected += budgetPeriodAmount.Sum(x => x.PostedEncumbranceAmount);
            Assert.AreEqual(totalEncumbrances_Expected, lineItem.TotalEncumbrances(sequenceNumber));
        }
        #endregion

        #region Tests for AddGlAccount
        [TestMethod]
        public async Task AddGlAccount_HappyPath()
        {
            // Set up the line item with no GL Accounts
            string projectId = "3";
            bool summaryOnly = true;
            string sequenceNumber = "";
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);
            var lineItem = project.LineItems.FirstOrDefault(x => x.Id == "35");

            Assert.IsTrue(lineItem.GlAccounts.Count() == 0);
            lineItem.AddGlAccount(new ProjectLineItemGlAccount("10_00_01_02_52010"));
            Assert.IsTrue(lineItem.GlAccounts.Count() == 1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task AddGlAccount_NullGlAccount()
        {
            // Set up the line item with no GL Accounts
            string projectId = "3";
            bool summaryOnly = true;
            string sequenceNumber = "";
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);
            var lineItem = project.LineItems.FirstOrDefault(x => x.Id == "35");
            lineItem.AddGlAccount(null);
        }

        [TestMethod]
        public async Task AddGlAccount_AddDuplicate()
        {
            // Set up the line item with no GL Accounts
            string projectId = "3";
            bool summaryOnly = true;
            string sequenceNumber = "";
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);
            var lineItem = project.LineItems.FirstOrDefault(x => x.Id == "35");

            Assert.IsTrue(lineItem.GlAccounts.Count() == 0);
            lineItem.AddGlAccount(new ProjectLineItemGlAccount("10_00_01_02_51000"));
            Assert.IsTrue(lineItem.GlAccounts.Count() == 1);
            lineItem.AddGlAccount(new ProjectLineItemGlAccount("10_00_01_02_52010"));
            Assert.IsTrue(lineItem.GlAccounts.Count() == 2);
            lineItem.AddGlAccount(new ProjectLineItemGlAccount("10_00_01_02_52010"));
            Assert.IsTrue(lineItem.GlAccounts.Count() == 2);
        }
        #endregion

        #region Tests for AddBudgetPeriodAmount
        [TestMethod]
        public async Task AddBudgetPeriodAmount_HappyPath()
        {
            // Set up the line item with no budget period amounts
            string projectId = "3";
            bool summaryOnly = true;
            string sequenceNumber = "";
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);
            var lineItem = project.LineItems.FirstOrDefault(x => x.Id == "36");

            Assert.IsTrue(lineItem.BudgetPeriodAmounts.Count() == 0);
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("1", 10.00m, 20.00m, 30.00m, 40.00m, 50.00m, 60.00m));
            Assert.IsTrue(lineItem.BudgetPeriodAmounts.Count() == 1);
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("1", 10.00m, 20.00m, 30.00m, 40.00m, 50.00m, 60.00m));
            Assert.IsTrue(lineItem.BudgetPeriodAmounts.Count() == 2);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task AddBudgetPeriodAmount_NullBudgetPeriodAmount()
        {
            // Set up the line item with no budget period amounts
            string projectId = "3";
            bool summaryOnly = true;
            string sequenceNumber = "";
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);
            var lineItem = project.LineItems.FirstOrDefault(x => x.Id == "36");
            lineItem.AddBudgetPeriodAmount(null);
        }
        #endregion
    }
}