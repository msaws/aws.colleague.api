﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Tests.Entities
{
    /// <summary>
    /// This class tests the valid and invalid conditions of a project repository object.
    /// The project repository object requires a project id, number, title, and status.
    /// </summary>
    [TestClass]
    public class ProjectTests
    {
        #region Initialize and Cleanup
        private TestProjectRepository testProjectRepository;
        private Project project;

        [TestInitialize]
        public void Initialize()
        {
            testProjectRepository = new TestProjectRepository();
            this.project = new Project("1", "GTT-007", "Gary's Bond Project", ProjectStatus.Active, "R");

            var lineItem = new ProjectLineItem("1", GlClass.Expense, "C");
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("1", 10000, 100, 200, 300, 400, 500));
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("2", 20000, 110, 210, 310, 410, 510));
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("3", 30000, 120, 220, 320, 420, 520));
            this.project.AddLineItem(lineItem);

            lineItem = new ProjectLineItem("2", GlClass.Expense, "C");
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("1", 10000, 100, 200, 300, 400, 500));
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("2", 20000, 110, 210, 310, 410, 510));
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("3", 30000, 120, 220, 320, 420, 520));
            this.project.AddLineItem(lineItem);

            lineItem = new ProjectLineItem("3", GlClass.Revenue, "C");
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("1", 10000, 100, 200, 300, 400, 500));
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("2", 20000, 110, 210, 310, 410, 510));
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("3", 30000, 120, 220, 320, 420, 520));
            this.project.AddLineItem(lineItem);

            lineItem = new ProjectLineItem("4", GlClass.Revenue, "C");
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("1", 10000, 100, 200, 300, 400, 500));
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("2", 20000, 110, 210, 310, 410, 510));
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("3", 30000, 120, 220, 320, 420, 520));
            this.project.AddLineItem(lineItem);

            lineItem = new ProjectLineItem("5", GlClass.Asset, "C");
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("1", 10000, 100, 200, 300, 400, 500));
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("2", 20000, 110, 210, 310, 410, 510));
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("3", 30000, 120, 220, 320, 420, 520));
            this.project.AddLineItem(lineItem);

            lineItem = new ProjectLineItem("6", GlClass.Liability, "C");
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("1", 10000, 100, 200, 300, 400, 500));
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("2", 20000, 110, 210, 310, 410, 510));
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("3", 30000, 120, 220, 320, 420, 520));
            this.project.AddLineItem(lineItem);

            lineItem = new ProjectLineItem("7", GlClass.FundBalance, "C");
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("1", 10000, 100, 200, 300, 400, 500));
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("2", 20000, 110, 210, 310, 410, 510));
            lineItem.AddBudgetPeriodAmount(new BudgetPeriodAmount("3", 30000, 120, 220, 320, 420, 520));
            this.project.AddLineItem(lineItem);
        }

        [TestCleanup]
        public void Cleanup()
        {
            testProjectRepository = null;
        }
        #endregion

        #region Constructor
        [TestMethod]
        public void ProjectConstructor_Success()
        {
            string id = "1";
            string number = "Number-0001";
            string title = "Title";
            ProjectStatus status = new ProjectStatus();
            status = ProjectStatus.Active;
            Project project = new Project(id, number, title, status, "R");
            Assert.AreEqual(id, project.ProjectId);
            Assert.AreEqual(number, project.Number);
            Assert.AreEqual(title, project.Title);
            Assert.AreEqual(status, project.Status);
            Assert.AreEqual("R", project.ProjectType);
            Assert.IsTrue(project.BudgetPeriods.Count() == 0);
            Assert.IsTrue(project.LineItems.Count == 0);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ProjectConstructor_NullId()
        {
            string id = null;
            string number = "Number-0001";
            string title = "Title";
            string type = "R";
            ProjectStatus status = new ProjectStatus();
            status = ProjectStatus.Active;
            Project project = new Project(id, number, title, status, type);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ProjectConstructor_NullNumber()
        {
            string id = "1";
            string number = null;
            string title = "Title";
            ProjectStatus status = new ProjectStatus();
            status = ProjectStatus.Active;
            Project project = new Project(id, number, title, status, "R");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ProjectConstructor_NullTitle()
        {
            string id = "1";
            string number = "Number-0001";
            string title = null;
            string type = "R";
            ProjectStatus status = new ProjectStatus();
            status = ProjectStatus.Active;
            Project project = new Project(id, number, title, status, type);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ProjectConstructor_NullType()
        {
            string id = "1";
            string number = "Number-0001";
            string title = "title";
            string type = null;
            ProjectStatus status = new ProjectStatus();
            status = ProjectStatus.Active;
            Project project = new Project(id, number, title, status, type);
        }
        #endregion

        #region TotalBudgetExpenses
        [TestMethod]
        public async Task TotalBudget_HappyPath()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - All line items have a GL Class/type of "Expense"
             *  - We want the total amount for all budget periods
             */
            string projectId = "1";
            bool summaryOnly = true;
            string sequenceNumber = null;
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Set up the expected total
            decimal totalBudget_Expected = 0.00m;
            foreach (var lineItem in project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Expense)
                {
                    totalBudget_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.BudgetAmount);
                }
            }

            string sequenceNumberForActual = "";
            Assert.AreEqual(totalBudget_Expected, project.TotalBudgetExpenses(sequenceNumberForActual));
        }

        [TestMethod]
        public async Task TotalBudget_OneNonExpenseLineItem()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - At least one of the line items have a GL Class/type of "Expense"
             *  - We want the total amount for all budget periods
             */
            string projectId = "2";
            bool summaryOnly = true;
            string sequenceNumber = null;
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Set up the expected total
            decimal totalBudget_Expected = 0.00m;
            foreach (var lineItem in project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Expense)
                {
                    totalBudget_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.BudgetAmount);
                }
            }

            string sequenceNumberForActual = "";
            Assert.AreEqual(totalBudget_Expected, project.TotalBudgetExpenses(sequenceNumberForActual));
        }

        [TestMethod]
        public async Task TotalBudget_OnlyOneBudgetPeriod()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - All line items have a GL Class/type of "Expense"
             *  - We want the total amount for only one budget period
             */
            string projectId = "1";
            bool summaryOnly = true;
            string sequenceNumber = null;
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Set up the expected total
            decimal totalBudget_Expected = 0.00m;
            string sequenceNumberForActual = "1";
            foreach (var lineItem in project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Expense)
                {
                    var budgetPeriodAmounts = lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumberForActual);
                    totalBudget_Expected += budgetPeriodAmounts.Sum(x => x.BudgetAmount);
                }
            }

            Assert.AreEqual(totalBudget_Expected, project.TotalBudgetExpenses(sequenceNumberForActual));
        }
        #endregion

        #region TotalBudgetRevenue
        [TestMethod]
        public void TotalBudgetRevenue_AllBudgetPeriods()
        {
            // Set up the expected total
            decimal totalRevenueBudget_Expected = 0.00m;
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Revenue)
                {
                    totalRevenueBudget_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.BudgetAmount);
                }
            }

            string sequenceNumberForActual = "";
            Assert.AreEqual(totalRevenueBudget_Expected, project.TotalBudgetRevenue(sequenceNumberForActual));
        }

        [TestMethod]
        public void TotalBudgetRevenue_OneBudgetPeriod()
        {
            // Set up the expected total
            decimal totalRevenueBudget_Expected = 0.00m;
            string sequenceNumber = "1";
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Revenue)
                {
                    totalRevenueBudget_Expected += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.BudgetAmount);
                }
            }

            Assert.AreEqual(totalRevenueBudget_Expected, project.TotalBudgetRevenue(sequenceNumber));
        }
        #endregion

        #region Tests for TotalExpenses
        [TestMethod]
        public async Task TotalExpenses_HappyPath()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - All line items have a GL Class/type of "Expense"
             *  - We want the total amount for all budget periods
             */
            string projectId = "1";
            bool summaryOnly = true;
            string sequenceNumber = null;
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Set up the expected total
            decimal totalExpenses_Expected = 0.00m;
            foreach (var lineItem in project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Expense)
                {
                    totalExpenses_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoActualsAmount);
                    totalExpenses_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedActualsAmount);
                    totalExpenses_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoEncumbranceAmount);
                    totalExpenses_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedEncumbranceAmount);
                    totalExpenses_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.RequisitionAmount);
                }
            }

            string sequenceNumberForActual = "";
            Assert.AreEqual(totalExpenses_Expected, project.TotalExpenses(sequenceNumberForActual));
        }

        [TestMethod]
        public async Task TotalExpenses_OneNonExpenseLineItem()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - At least one of the line items have a GL Class/type of "Expense"
             *  - We want the total amount for all budget periods
             */
            string projectId = "2";
            bool summaryOnly = true;
            string sequenceNumber = null;
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Set up the expected total
            decimal totalExpenses_Expected = 0.00m;
            foreach (var lineItem in project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Expense)
                {
                    totalExpenses_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoActualsAmount);
                    totalExpenses_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedActualsAmount);
                    totalExpenses_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoEncumbranceAmount);
                    totalExpenses_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedEncumbranceAmount);
                    totalExpenses_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.RequisitionAmount);
                }
            }

            string sequenceNumberForActual = "";
            Assert.AreEqual(totalExpenses_Expected, project.TotalExpenses(sequenceNumberForActual));
        }

        [TestMethod]
        public async Task TotalExpenses_OnlyOneBudgetPeriod()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - All line items have a GL Class/type of "Expense"
             *  - We want the total amount for only one budget period
             */
            string projectId = "1";
            bool summaryOnly = true;
            string sequenceNumber = null;
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Set up the expected total
            decimal totalExpenses_Expected = 0.00m;
            string sequenceNumberForActual = "1";
            foreach (var lineItem in project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Expense)
                {
                    var budgetPeriodAmounts = lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumberForActual);
                    totalExpenses_Expected += budgetPeriodAmounts.Sum(x => x.MemoActualsAmount);
                    totalExpenses_Expected += budgetPeriodAmounts.Sum(x => x.PostedActualsAmount);
                    totalExpenses_Expected += budgetPeriodAmounts.Sum(x => x.MemoEncumbranceAmount);
                    totalExpenses_Expected += budgetPeriodAmounts.Sum(x => x.PostedEncumbranceAmount);
                    totalExpenses_Expected += budgetPeriodAmounts.Sum(x => x.RequisitionAmount);
                }
            }

            Assert.AreEqual(totalExpenses_Expected, project.TotalExpenses(sequenceNumberForActual));
        }
        #endregion

        #region TotalActualsExpenses
        [TestMethod]
        public async Task TotalActuals_HappyPath()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - All line items have a GL Class/type of "Expense"
             *  - We want the total amount for all budget periods
             */
            string projectId = "1";
            bool summaryOnly = true;
            string sequenceNumber = null;
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Set up the expected total
            decimal totalActuals_Expected = 0.00m;
            foreach (var lineItem in project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Expense)
                {
                    totalActuals_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoActualsAmount);
                    totalActuals_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedActualsAmount);
                }
            }

            string sequenceNumberForActual = "";
            Assert.AreEqual(totalActuals_Expected, project.TotalActualsExpenses(sequenceNumberForActual));
        }

        [TestMethod]
        public async Task TotalActuals_OneNonExpenseLineItem()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - At least one of the line items have a GL Class/type of "Expense"
             *  - We want the total amount for all budget periods
             */
            string projectId = "2";
            bool summaryOnly = true;
            string sequenceNumber = null;
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Set up the expected total
            decimal totalActuals_Expected = 0.00m;
            foreach (var lineItem in project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Expense)
                {
                    totalActuals_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoActualsAmount);
                    totalActuals_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedActualsAmount);
                }
            }

            string sequenceNumberForActual = "";
            Assert.AreEqual(totalActuals_Expected, project.TotalActualsExpenses(sequenceNumberForActual));
        }

        [TestMethod]
        public async Task TotalActuals_OnlyOneBudgetPeriod()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - All line items have a GL Class/type of "Expense"
             *  - We want the total amount for only one budget period
             */
            string projectId = "1";
            bool summaryOnly = true;
            string sequenceNumber = null;
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Set up the expected total
            decimal totalActuals_Expected = 0.00m;
            string sequenceNumberForActual = "1";
            foreach (var lineItem in project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Expense)
                {
                    var budgetPeriodAmounts = lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumberForActual);
                    totalActuals_Expected += budgetPeriodAmounts.Sum(x => x.MemoActualsAmount);
                    totalActuals_Expected += budgetPeriodAmounts.Sum(x => x.PostedActualsAmount);
                }
            }

            Assert.AreEqual(totalActuals_Expected, project.TotalActualsExpenses(sequenceNumberForActual));
        }
        #endregion

        #region TotalActualsRevenue
        [TestMethod]
        public void TotalActualsRevenue_AllBudgetPeriods()
        {
            // Set up the expected total
            decimal expectedActualRevenue = 0.00m;
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Revenue)
                {
                    expectedActualRevenue += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoActualsAmount);
                    expectedActualRevenue += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedActualsAmount);
                }
            }

            string sequenceNumberForActual = "";
            Assert.AreEqual(expectedActualRevenue, project.TotalActualsRevenue(sequenceNumberForActual));
        }

        [TestMethod]
        public void TotalActualsRevenue_OneBudgetPeriod()
        {
            // Set up the expected total
            decimal expectedActualRevenue = 0.00m;
            string sequenceNumber = "1";
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Revenue)
                {
                    expectedActualRevenue += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.MemoActualsAmount);
                    expectedActualRevenue += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.PostedActualsAmount);
                }
            }

            
            Assert.AreEqual(expectedActualRevenue, project.TotalActualsRevenue(sequenceNumber));
        }
        #endregion

        #region TotalActualsAsset

        [TestMethod]
        public void TotalActualsAsset_AllBudgetPeriods()
        {
            // Set up the expected total
            decimal expectedActualAsset = 0.00m;
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Asset)
                {
                    expectedActualAsset += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoActualsAmount);
                    expectedActualAsset += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedActualsAmount);
                }
            }

            string sequenceNumberForActual = "";
            Assert.AreEqual(expectedActualAsset, project.TotalActualsAsset(sequenceNumberForActual));
        }

        [TestMethod]
        public void TotalActualsAsset_OneBudgetPeriod()
        {
            // Set up the expected total
            decimal expectedActualAsset = 0.00m;
            string sequenceNumber = "1";
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Asset)
                {
                    expectedActualAsset += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.MemoActualsAmount);
                    expectedActualAsset += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.PostedActualsAmount);
                }
            }


            Assert.AreEqual(expectedActualAsset, project.TotalActualsAsset(sequenceNumber));
        }
        #endregion

        #region TotalActualsLiability

        [TestMethod]
        public void TotalActualsLiability_AllBudgetPeriods()
        {
            // Set up the expected total
            decimal expectedActualLiability = 0.00m;
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Liability)
                {
                    expectedActualLiability += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoActualsAmount);
                    expectedActualLiability += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedActualsAmount);
                }
            }

            string sequenceNumberForActual = "";
            Assert.AreEqual(expectedActualLiability, project.TotalActualsLiability(sequenceNumberForActual));
        }

        [TestMethod]
        public void TotalActualsLiability_OneBudgetPeriod()
        {
            // Set up the expected total
            decimal expectedActualLiability = 0.00m;
            string sequenceNumber = "1";
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Liability)
                {
                    expectedActualLiability += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.MemoActualsAmount);
                    expectedActualLiability += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.PostedActualsAmount);
                }
            }


            Assert.AreEqual(expectedActualLiability, project.TotalActualsLiability(sequenceNumber));
        }
        #endregion

        #region TotalActualsFundBalance

        [TestMethod]
        public void TotalActualsFundBalance_AllBudgetPeriods()
        {
            // Set up the expected total
            decimal expectedActualFundBalance = 0.00m;
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.FundBalance)
                {
                    expectedActualFundBalance += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoActualsAmount);
                    expectedActualFundBalance += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedActualsAmount);
                }
            }

            string sequenceNumberForActual = "";
            Assert.AreEqual(expectedActualFundBalance, project.TotalActualsFundBalance(sequenceNumberForActual));
        }

        [TestMethod]
        public void TotalActualsFundBalance_OneBudgetPeriod()
        {
            // Set up the expected total
            decimal expectedActualFundBalance = 0.00m;
            string sequenceNumber = "1";
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.FundBalance)
                {
                    expectedActualFundBalance += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.MemoActualsAmount);
                    expectedActualFundBalance += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.PostedActualsAmount);
                }
            }


            Assert.AreEqual(expectedActualFundBalance, project.TotalActualsFundBalance(sequenceNumber));
        }
        #endregion

        #region TotalEncumbranceExpenses
        [TestMethod]
        public async Task TotalEncumbrances_HappyPath()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - All line items have a GL Class/type of "Expense"
             *  - We want the total amount for all budget periods
             */
            string projectId = "1";
            bool summaryOnly = true;
            string sequenceNumber = null;
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Set up the expected total
            decimal totalEncumbrances_Expected = 0.00m;
            foreach (var lineItem in project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Expense)
                {
                    totalEncumbrances_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoEncumbranceAmount);
                    totalEncumbrances_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedEncumbranceAmount);
                    totalEncumbrances_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.RequisitionAmount);
                }
            }

            string sequenceNumberForActual = "";
            Assert.AreEqual(totalEncumbrances_Expected, project.TotalEncumbranceExpenses(sequenceNumberForActual));
        }

        [TestMethod]
        public async Task TotalEncumbrances_OneNonExpenseLineItem()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - At least one of the line items have a GL Class/type of "Expense"
             *  - We want the total amount for all budget periods
             */
            string projectId = "2";
            bool summaryOnly = true;
            string sequenceNumber = null;
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Set up the expected total
            decimal totalEncumbrances_Expected = 0.00m;
            foreach (var lineItem in project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Expense)
                {
                    totalEncumbrances_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoEncumbranceAmount);
                    totalEncumbrances_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedEncumbranceAmount);
                    totalEncumbrances_Expected += lineItem.BudgetPeriodAmounts.Sum(x => x.RequisitionAmount);
                }
            }

            string sequenceNumberForActual = "";
            Assert.AreEqual(totalEncumbrances_Expected, project.TotalEncumbranceExpenses(sequenceNumberForActual));
        }

        [TestMethod]
        public async Task TotalEncumbrances_OnlyOneBudgetPeriod()
        {
            /*
             * This test confirms that the TotalBudget method works under the following conditions:
             *  - All line items have a GL Class/type of "Expense"
             *  - We want the total amount for only one budget period
             */
            string projectId = "1";
            bool summaryOnly = true;
            string sequenceNumber = null;
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Set up the expected total
            decimal totalEncumbrances_Expected = 0.00m;
            string sequenceNumberForActual = "1";
            foreach (var lineItem in project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Expense)
                {
                    var budgetPeriodAmounts = lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumberForActual);
                    totalEncumbrances_Expected += budgetPeriodAmounts.Sum(x => x.MemoEncumbranceAmount);
                    totalEncumbrances_Expected += budgetPeriodAmounts.Sum(x => x.PostedEncumbranceAmount);
                    totalEncumbrances_Expected += budgetPeriodAmounts.Sum(x => x.RequisitionAmount);
                }
            }

            Assert.AreEqual(totalEncumbrances_Expected, project.TotalEncumbranceExpenses(sequenceNumberForActual));
        }
        #endregion

        #region TotalEncumbranceRevenue
        [TestMethod]
        public void TotalEncumbranceRevenue_AllBudgetPeriods()
        {
            // Set up the expected total
            decimal expectedEncumbranceRevenue = 0.00m;
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Revenue)
                {
                    expectedEncumbranceRevenue += lineItem.BudgetPeriodAmounts.Sum(x => x.RequisitionAmount);
                    expectedEncumbranceRevenue += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoEncumbranceAmount);
                    expectedEncumbranceRevenue += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedEncumbranceAmount);
                }
            }

            Assert.AreEqual(expectedEncumbranceRevenue, project.TotalEncumbranceRevenue(""));
        }

        [TestMethod]
        public void TotalEncumbranceRevenue_OneBudgetPeriod()
        {
            // Set up the expected total
            decimal expectedEncumbranceRevenue = 0.00m;
            string sequenceNumber = "1";
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Revenue)
                {
                    expectedEncumbranceRevenue += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.RequisitionAmount);
                    expectedEncumbranceRevenue += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.MemoEncumbranceAmount);
                    expectedEncumbranceRevenue += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.PostedEncumbranceAmount);
                }
            }

            Assert.AreEqual(expectedEncumbranceRevenue, project.TotalEncumbranceRevenue(sequenceNumber));
        }
        #endregion

        #region TotalEncumbranceAsset

        [TestMethod]
        public void TotalEncumbranceAsset_AllBudgetPeriods()
        {
            // Set up the expected total
            decimal expectedEncumbranceAsset = 0.00m;
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Asset)
                {
                    expectedEncumbranceAsset += lineItem.BudgetPeriodAmounts.Sum(x => x.RequisitionAmount);
                    expectedEncumbranceAsset += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoEncumbranceAmount);
                    expectedEncumbranceAsset += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedEncumbranceAmount);
                }
            }

            Assert.AreEqual(expectedEncumbranceAsset, project.TotalEncumbranceAsset(""));
        }

        [TestMethod]
        public void TotalEncumbranceAsset_OneBudgetPeriod()
        {
            // Set up the expected total
            decimal expectedEncumbranceAsset = 0.00m;
            string sequenceNumber = "1";
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Asset)
                {
                    expectedEncumbranceAsset += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.RequisitionAmount);
                    expectedEncumbranceAsset += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.MemoEncumbranceAmount);
                    expectedEncumbranceAsset += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.PostedEncumbranceAmount);
                }
            }

            Assert.AreEqual(expectedEncumbranceAsset, project.TotalEncumbranceAsset(sequenceNumber));
        }

        #endregion

        #region TotalEncumbranceLiability

        [TestMethod]
        public void TotalEncumbranceLiability_AllBudgetPeriods()
        {
            // Set up the expected total
            decimal expectedEncumbranceLiability = 0.00m;
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Liability)
                {
                    expectedEncumbranceLiability += lineItem.BudgetPeriodAmounts.Sum(x => x.RequisitionAmount);
                    expectedEncumbranceLiability += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoEncumbranceAmount);
                    expectedEncumbranceLiability += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedEncumbranceAmount);
                }
            }

            Assert.AreEqual(expectedEncumbranceLiability, project.TotalEncumbranceLiability(""));
        }

        [TestMethod]
        public void TotalEncumbranceLiability_OneBudgetPeriod()
        {
            // Set up the expected total
            decimal expectedEncumbranceLiability = 0.00m;
            string sequenceNumber = "1";
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.Liability)
                {
                    expectedEncumbranceLiability += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.RequisitionAmount);
                    expectedEncumbranceLiability += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.MemoEncumbranceAmount);
                    expectedEncumbranceLiability += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.PostedEncumbranceAmount);
                }
            }

            Assert.AreEqual(expectedEncumbranceLiability, project.TotalEncumbranceLiability(sequenceNumber));
        }

        #endregion

        #region TotalEncumbranceFundBalance

        [TestMethod]
        public void TotalEncumbranceFundBalance_AllBudgetPeriods()
        {
            // Set up the expected total
            decimal expectedEncumbranceFundBalance = 0.00m;
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.FundBalance)
                {
                    expectedEncumbranceFundBalance += lineItem.BudgetPeriodAmounts.Sum(x => x.RequisitionAmount);
                    expectedEncumbranceFundBalance += lineItem.BudgetPeriodAmounts.Sum(x => x.MemoEncumbranceAmount);
                    expectedEncumbranceFundBalance += lineItem.BudgetPeriodAmounts.Sum(x => x.PostedEncumbranceAmount);
                }
            }

            Assert.AreEqual(expectedEncumbranceFundBalance, project.TotalEncumbranceFundBalance(""));
        }

        [TestMethod]
        public void TotalEncumbranceFundBalance_OneBudgetPeriod()
        {
            // Set up the expected total
            decimal expectedEncumbranceFundBalance = 0.00m;
            string sequenceNumber = "1";
            foreach (var lineItem in this.project.LineItems)
            {
                if (lineItem.GlClass == GlClass.FundBalance)
                {
                    expectedEncumbranceFundBalance += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.RequisitionAmount);
                    expectedEncumbranceFundBalance += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.MemoEncumbranceAmount);
                    expectedEncumbranceFundBalance += lineItem.BudgetPeriodAmounts.Where(x => x.SequenceNumber == sequenceNumber).Sum(x => x.PostedEncumbranceAmount);
                }
            }

            Assert.AreEqual(expectedEncumbranceFundBalance, project.TotalEncumbranceFundBalance(sequenceNumber));
        }

        #endregion

        #region AddLineItem
        [TestMethod]
        public async Task AddLineItem_HappyPath()
        {
            // Set up a project with no line items
            string projectId = "5";
            bool summaryOnly = true;
            string sequenceNumber = "";
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            Assert.IsTrue(project.LineItems.Count() == 0);
            project.AddLineItem(new ProjectLineItem("1", GlClass.Expense, "SP"));
            Assert.IsTrue(project.LineItems.Count() == 1);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task AddLineItem_NullLineItem()
        {
            // Set up a project with no line items
            string projectId = "5";
            bool summaryOnly = true;
            string sequenceNumber = "";
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            project.AddLineItem(null);
        }

        [TestMethod]
        public async Task AddLineItem_AddDuplicate()
        {
            // Set up a project with no line items
            string projectId = "5";
            bool summaryOnly = true;
            string sequenceNumber = "";
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            Assert.IsTrue(project.LineItems.Count() == 0);
            project.AddLineItem(new ProjectLineItem("1", GlClass.Expense, "SP"));
            Assert.IsTrue(project.LineItems.Count() == 1);
            project.AddLineItem(new ProjectLineItem("2", GlClass.Expense, "SP"));
            Assert.IsTrue(project.LineItems.Count() == 2);
            project.AddLineItem(new ProjectLineItem("2", GlClass.Expense, "SP"));
            Assert.IsTrue(project.LineItems.Count() == 2);
        }
        #endregion

        #region AddBudgetPeriod
        [TestMethod]
        public async Task AddBudgetPeriod_HappyPath()
        {
            // Set up a project with no budget periods
            string projectId = "6";
            bool summaryOnly = true;
            string sequenceNumber = "";
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            // Set up budget period
            string budgetPeriodSequenceNumber = "1";
            DateTime startDate = Convert.ToDateTime("01/01/2014");
            DateTime endDate = Convert.ToDateTime("06/30/2014");
            var budgetPeriod = new ProjectBudgetPeriod(budgetPeriodSequenceNumber, startDate, endDate);

            // Add the budget period
            Assert.AreEqual(0, project.BudgetPeriods.Count());
            project.AddBudgetPeriod(budgetPeriod);
            Assert.AreEqual(1, project.BudgetPeriods.Count());
            Assert.AreEqual(budgetPeriod.StartDate, project.BudgetPeriods[0].StartDate);
            Assert.AreEqual(budgetPeriod.EndDate, project.BudgetPeriods[0].EndDate);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task AddBudgetPeriod_NullBudgetPeriod()
        {
            // Set up a project with no budget periods
            string projectId = "6";
            bool summaryOnly = true;
            string sequenceNumber = "";
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);
            project.AddBudgetPeriod(null);
        }

        [TestMethod]
        public async Task AddBudgetPeriod_AddDuplicate()
        {
            // Set up a project with no budget periods
            string projectId = "6";
            bool summaryOnly = true;
            string sequenceNumber = "";
            var project = await testProjectRepository.GetProjectAsync(projectId, summaryOnly, sequenceNumber);

            Assert.IsTrue(project.BudgetPeriods.Count() == 0);
            project.AddBudgetPeriod(new ProjectBudgetPeriod("1", new DateTime(2012, 1, 1), new DateTime(2012, 12, 31)));
            Assert.IsTrue(project.BudgetPeriods.Count() == 1);
            project.AddBudgetPeriod(new ProjectBudgetPeriod("2", new DateTime(2013, 1, 1), new DateTime(2013, 12, 31)));
            Assert.IsTrue(project.BudgetPeriods.Count() == 2);
            project.AddBudgetPeriod(new ProjectBudgetPeriod("2", new DateTime(2014, 1, 1), new DateTime(2014, 12, 31)));
            Assert.IsTrue(project.BudgetPeriods.Count() == 2);
        }
        #endregion
    }
}
