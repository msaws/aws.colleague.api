﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Tests.Entities
{
    /// <summary>
    /// This class tests valid and invalid conditions for a Project Budget Period object.
    /// </summary>
    [TestClass]
    public class ProjectBudgetPeriodTests
    {
        #region Initialize and Cleanup
        [TestInitialize]
        public void Initialize()
        {
            // Nothing to do here.
        }

        [TestCleanup]
        public void Cleanup()
        {
            // Nothing to do here.
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void ProjectBudgetPeriodConstructor_Success()
        {
            string sequenceNumber = "1";
            DateTime startDate = Convert.ToDateTime("01/01/2014");
            DateTime endDate = Convert.ToDateTime("06/30/2014");
            ProjectBudgetPeriod budgetPeriod = new ProjectBudgetPeriod(sequenceNumber, startDate, endDate);
            Assert.AreEqual(sequenceNumber, budgetPeriod.SequenceNumber);
            Assert.AreEqual(startDate, budgetPeriod.StartDate);
            Assert.AreEqual(endDate, budgetPeriod.EndDate);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ProjectBudgetPeriodConstructor_NullSequenceNumber()
        {
            string sequenceNumber = null;
            DateTime startDate = Convert.ToDateTime("01/01/2014");
            DateTime endDate = Convert.ToDateTime("06/30/2014");
            ProjectBudgetPeriod budgetPeriod = new ProjectBudgetPeriod(sequenceNumber, startDate, endDate);
        }

        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void ProjectBudgetPeriodConstructor_EndDateBfrStartDate()
        {
            string sequenceNumber = "1";
            DateTime startDate = Convert.ToDateTime("06/30/2014");
            DateTime endDate = Convert.ToDateTime("05/30/2014");
            ProjectBudgetPeriod budgetPeriod = new ProjectBudgetPeriod(sequenceNumber, startDate, endDate);
        }
        #endregion
    }
}
