﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Tests.Entities
{
    /// <summary>
    /// This class tests that ProjectsAccountingUser object is initialized with an empty list of project ids.
    /// </summary>
    [TestClass]
    public class ProjectsAccountingUserTests
    {
        #region Initialize and Cleanup
        [TestInitialize]
        public void Initialize()
        {

        }

        [TestCleanup]
        public void Cleanup()
        {

        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void ProjectsAccountingUserConstructor_Success()
        {
            string id = "0000001";
            string lastName = "Kleehammer";
            ProjectsAccountingUser projectsAccountingUser = new ProjectsAccountingUser(id, lastName);
            Assert.IsTrue(projectsAccountingUser.Projects.Count == 0);
            Assert.AreEqual(projectsAccountingUser.GlAccessLevel, GlAccessLevel.No_Access);
        }
        #endregion

        #region Tests for AddProjects and RemoveAll
        [TestMethod]
        public void AddProjects_ProjectsAdded()
        {
            string id = "0000001";
            string lastName = "Kleehammer";
            ProjectsAccountingUser projectsAccountingUser = new ProjectsAccountingUser(id, lastName);
            var projectIds = new List<string>();
            projectIds.Add("1");
            projectIds.Add("2");
            projectsAccountingUser.AddProjects(projectIds);

            // Confirm that the number of projects added is the same as the number in the local list
            Assert.AreEqual(projectIds.Count(), projectsAccountingUser.Projects.Count());

            // Confirm that the IDs of the local list match the IDs of the projects in the ProjectsAccountingUser object
            for (var i = 0; i < projectIds.Count(); i++)
            {
                Assert.AreEqual(projectIds[i], projectsAccountingUser.Projects[i]);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddProjects_ProjectListContainsNullValue()
        {
            string id = "0000001";
            string lastName = "Kleehammer";
            ProjectsAccountingUser projectsAccountingUser = new ProjectsAccountingUser(id, lastName);
            var projectIds = new List<string>();
            projectIds.Add("1");
            projectIds.Add(null);
            projectIds.Add("2");
            projectsAccountingUser.AddProjects(projectIds);
        }

        [TestMethod]
        public void RemoveAll_Success()
        {
            string id = "0000001";
            string lastName = "Kleehammer";
            ProjectsAccountingUser projectsAccountingUser = new ProjectsAccountingUser(id, lastName);
            var projectIds = new List<string>();
            projectIds.Add("1");
            projectIds.Add("2");
            projectsAccountingUser.AddProjects(projectIds);

            // Confirm that the number of projects added is the same as the number in the local list
            Assert.AreEqual(projectIds.Count(), projectsAccountingUser.Projects.Count());

            // Confirm that the IDs of the local list match the IDs of the projects in the ProjectsAccountingUser object
            for (var i = 0; i < projectIds.Count(); i++)
            {
                Assert.AreEqual(projectIds[i], projectsAccountingUser.Projects[i]);
            }

            // Now call the RemoveAll method and confirm that the list has been cleared.
            projectsAccountingUser.RemoveAll();
            Assert.IsTrue(projectsAccountingUser.Projects.Count() == 0);
        }
        #endregion

        #region tests for SetGlAccessLevel
        [TestMethod]
        public void SetGlAccessLevel_Full_Access()
        {
            string id = "0000001";
            string lastName = "Kleehammer";
            ProjectsAccountingUser projectsAccountingUser = new ProjectsAccountingUser(id, lastName);
            projectsAccountingUser.SetGlAccessLevel(GlAccessLevel.Full_Access);
            Assert.AreEqual(projectsAccountingUser.GlAccessLevel, GlAccessLevel.Full_Access);
        }
        [TestMethod]
        public void SetGlAccessLevel_Possible_Access()
        {
            string id = "0000001";
            string lastName = "Kleehammer";
            ProjectsAccountingUser projectsAccountingUser = new ProjectsAccountingUser(id, lastName);
            projectsAccountingUser.SetGlAccessLevel(GlAccessLevel.Possible_Access);
            Assert.AreEqual(projectsAccountingUser.GlAccessLevel, GlAccessLevel.Possible_Access);
        }
        #endregion
    }
}
