﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Colleague.Domain.ColleagueFinance.Tests.Entities
{
    [TestClass]
    public class BudgetPeriodAmountTests
    {
        #region Initialize and Cleanup
        private BudgetPeriodAmount budgetPeriodAmount;

        [TestInitialize]
        public void Initialize()
        {
        }

        [TestCleanup]
        public void Cleanup()
        {
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void BudgetPeriodAmountConstructor_Success()
        {
            string sequenceNumber = "1";
            decimal budgetAmount = 500.00m,
                requisitionAmount = 10.00m,
                memoEncumbranceAmount = 20.00m,
                postedEncumbranceAmount = 30.00m,
                memoActualsAmount = 40.00m,
                postedActualsAmount = 50.00m;
            budgetPeriodAmount = new BudgetPeriodAmount(sequenceNumber, budgetAmount, requisitionAmount, memoEncumbranceAmount,
                postedEncumbranceAmount, memoActualsAmount, postedActualsAmount);

            Assert.AreEqual(sequenceNumber, budgetPeriodAmount.SequenceNumber);
            Assert.AreEqual(budgetAmount, budgetPeriodAmount.BudgetAmount);
            Assert.AreEqual(requisitionAmount, budgetPeriodAmount.RequisitionAmount);
            Assert.AreEqual(memoEncumbranceAmount, budgetPeriodAmount.MemoEncumbranceAmount);
            Assert.AreEqual(postedEncumbranceAmount, budgetPeriodAmount.PostedEncumbranceAmount);
            Assert.AreEqual(memoActualsAmount, budgetPeriodAmount.MemoActualsAmount);
            Assert.AreEqual(postedActualsAmount, budgetPeriodAmount.PostedActualsAmount);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullSequenceNumber()
        {
            string sequenceNumber = null;
            decimal budgetAmount = 500.00m,
                requisitionAmount = 10.00m,
                memoEncumbranceAmount = 20.00m,
                postedEncumbranceAmount = 30.00m,
                memoActualsAmount = 40.00m,
                postedActualsAmount = 50.00m;
            budgetPeriodAmount = new BudgetPeriodAmount(sequenceNumber, budgetAmount, requisitionAmount, memoEncumbranceAmount,
                postedEncumbranceAmount, memoActualsAmount, postedActualsAmount);
        }
        #endregion
    }
}
