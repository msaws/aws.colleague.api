﻿// Copyright 2013-2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using Ellucian.Web.Mvc.Filter;
using System.Runtime.Caching;
using System.Collections;
using Ellucian.Web.Cache;

namespace Ellucian.Colleague.Api.Controllers
{
    /// <summary>
    /// Provides a top-level controller for API utilities.
    /// </summary>
    [LocalRequest]
    public class UtilitiesController : Controller
    {
        /// <summary>
        /// Gets the index page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            return View();
        }

        /// <summary>
        /// Gets the clear cache page. This action currently clears the cache.
        /// </summary>
        /// <returns></returns>
        public ActionResult ClearCache()
        {
            var cacheProvider = DependencyResolver.Current.GetService<ICacheProvider>();
            List<string> filter = new List<string> { "Repositories" };

            UtilityCacheRepository cacheRepo = new UtilityCacheRepository(cacheProvider);
            cacheRepo.ClearCache(filter);

            return View();
        }

        /// <summary>
        /// Private repository class which extends the base caching repository; a bit of a hack so this controller
        /// can access methods in the abstract base caching repository
        /// </summary>
        private class UtilityCacheRepository : BaseCachingRepository
        {
            protected internal UtilityCacheRepository(ICacheProvider cacheProvider)
                : base(cacheProvider)
            {
            }
        }
    }
}
