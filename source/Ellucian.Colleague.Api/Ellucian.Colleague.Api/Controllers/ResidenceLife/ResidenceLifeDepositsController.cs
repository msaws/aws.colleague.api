﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.ComponentModel;
using System.Net;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.ResidenceLife.Services;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;

namespace Ellucian.Colleague.Api.Controllers.ResidenceLife
{
    /// <summary>
    /// APIs related to deposits in the context of residence life
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.ResidenceLife)]

    public class ResidenceLifeDepositsController : BaseCompressedApiController
    {
        private readonly IResidenceLifeAccountsReceivableService _residenceLifeAccountsReceivableService;
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor for the ResidenceLifeDepositsController
        /// </summary>
        /// <param name="residenceLifeAccountsReceivableService">Service of type <see cref="IResidenceLifeAccountsReceivableService">IResidenceLifeAccountsReceivableService</see></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public ResidenceLifeDepositsController(IResidenceLifeAccountsReceivableService residenceLifeAccountsReceivableService, ILogger logger)
        {
            _residenceLifeAccountsReceivableService = residenceLifeAccountsReceivableService;
            _logger = logger;
        }

        /// <summary>
        /// Create a new deposit in the context of residence life
        /// </summary>
        /// <param name="deposit">The deposit to create</param>
        /// <returns>The resulting deposit</returns>
        [HttpPost]
        public Dtos.Finance.Deposit PostDeposit(Dtos.ResidenceLife.Deposit deposit)
        {
            try
            {
                return _residenceLifeAccountsReceivableService.CreateDeposit(deposit);
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.ToString());
                throw CreateHttpResponseException(string.Empty, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }
    }
}