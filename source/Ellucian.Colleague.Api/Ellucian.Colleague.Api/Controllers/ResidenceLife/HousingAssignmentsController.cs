﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System;
using System.Net;
using System.Web.Http;
using slf4net;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Dtos.ResidenceLife;
using Ellucian.Colleague.Coordination.ResidenceLife.Services;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using Ellucian.Web.Security;

namespace Ellucian.Colleague.Api.Controllers.ResidenceLife
{
    /// <summary>
    /// APIs related to housing assignments.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.ResidenceLife)]
    public class HousingAssignmentsController : BaseCompressedApiController
    {

        private readonly IHousingAssignmentService housingAssignmentService;
        private readonly ILogger _logger;

        /// <summary>
        /// Constructor for the HousingAssignment Controller
        /// </summary>
        /// <param name="housingAssignmentService"></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public HousingAssignmentsController(IHousingAssignmentService housingAssignmentService, ILogger logger)
        {
            this.housingAssignmentService = housingAssignmentService;
            _logger = logger;
        }

        /// <summary>
        /// Create a new housing assignment in the target system.
        /// </summary>
        /// <param name="housingAssignment">The housing assignment to create</param>
        /// <returns>The resulting housing assignment</returns>
        [HttpPost]
        public HousingAssignment PostHousingAssignment(HousingAssignment housingAssignment)
        {
            try
            {
                return housingAssignmentService.CreateHousingAssignment(housingAssignment);
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.ToString());
                throw CreateHttpResponseException(string.Empty, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Update an existing housing assignment in the target system.
        /// The existing housing assignment must be specified by Id and/or External Id.
        /// All attributes of the existing housing assignment will be overwritten with the attributes
        /// supplied to this API. An attribute that is supported, but is not supplied to
        /// the API, will be treated as a blank value and overwrite the corresponding
        /// attribute in the existing record with a blank.
        /// </summary>
        /// <param name="housingAssignment">The new housing assignment data</param>
        /// <returns>The resulting housing assignment</returns>
        [HttpPut]
        public HousingAssignment PutHousingAssignment(HousingAssignment housingAssignment)
        {
            try
            {
                return housingAssignmentService.UpdateHousingAssignment(housingAssignment);
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.ToString());
                throw CreateHttpResponseException(string.Empty, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Get a housing assignment by the system id
        /// </summary>
        /// <param name="id">The system id of the housing assignment</param>
        /// <returns>The resulting housing assignment</returns>
        [HttpGet]
        public HousingAssignment GetHousingAssignment(string id)
        {
            try
            {
                return housingAssignmentService.GetHousingAssignment(id);
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.ToString());
                throw CreateHttpResponseException(string.Empty, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }
    }
}