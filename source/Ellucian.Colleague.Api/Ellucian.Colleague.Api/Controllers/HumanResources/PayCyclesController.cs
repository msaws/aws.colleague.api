﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.HumanResources.Services;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.HumanResources
{
    /// <summary>
    /// Expose Human Resources Pay Cycles data
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.HumanResources)]
    public class PayCyclesController : BaseCompressedApiController
    {
        private readonly ILogger logger;
        private readonly IAdapterRegistry adapterRegistry;
        private readonly IPayCycleService payCycleService;
        

        /// <summary>
        /// PayCyclesController constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="adapterRegistry"></param>
        /// <param name="payCycleService"></param>
        public PayCyclesController(ILogger logger, IAdapterRegistry adapterRegistry, IPayCycleService payCycleService)
        {
            this.logger = logger;
            this.adapterRegistry = adapterRegistry;
            this.payCycleService = payCycleService;
        }

        /// <summary>
        /// Gets all the pay cycles available for an institution.
        /// A pay cycle describes a date interval to which employee time worked is applied and processed
        /// </summary>
        /// <returns>A List of pay cycle dtos</returns>
        [HttpGet]
        public async Task<IEnumerable<PayCycle>> GetPayCyclesAsync()
        {
            try
            {
                return await payCycleService.GetPayCyclesAsync();
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error occurred");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }
    }
}