﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Web.Http.Controllers;
using System.Web.Http;
using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using slf4net;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Colleague.Coordination.HumanResources.Services;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http;
using Newtonsoft.Json;
using Ellucian.Colleague.Domain.Base.Exceptions;

namespace Ellucian.Colleague.Api.Controllers.HumanResources
{
    /// <summary>
    /// Provides access to InstitutionJobs
    /// </summary>
    [System.Web.Http.Authorize]
    [LicenseProvider(typeof (EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.HumanResources)]
    public class InstitutionJobsController : BaseCompressedApiController
    {
        private readonly IInstitutionJobsService _institutionJobsService;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the InstitutionJobsController class.
        /// </summary>
        /// <param name="institutionJobsService">Service of type <see cref="IInstitutionJobsService">IInstitutionJobsService</see></param>
        /// <param name="logger">Interface to logger</param>
        public InstitutionJobsController(IInstitutionJobsService institutionJobsService, ILogger logger)
        {
            _institutionJobsService = institutionJobsService;
            _logger = logger;
        }

        /// <summary>
        /// Return all InstitutionJobs
        /// </summary>
        /// <returns>List of InstitutionJobs <see cref="Dtos.InstitutionJobs"/> objects representing matching institutionJobs</returns>
        [System.Web.Http.HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetInstitutionJobsAsync(Paging page, [FromUri] string criteria = "")
        {
            var bypassCache = false;

            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                if (page == null)
                {
                    page = new Paging(100, 0);
                }

               string person = string.Empty, employer = string.Empty, position = string.Empty, department = string.Empty,
                   startOn = string.Empty, endOn = string.Empty, status = string.Empty, classification = string.Empty,
                   preference = string.Empty;
                var criteriaValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(criteria);

                if (criteriaValues != null)
                {
                    foreach (var value in criteriaValues)
                    {
                        switch (value.Key.ToLower())
                        {
                            case "person":
                                person = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value ;
                                break;
                            case "employer":
                                employer = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "position":
                                position = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value ;
                                break;
                            case "department":
                                department = string.IsNullOrWhiteSpace(value.Value) ? string.Empty :value.Value ;
                                break;
                            case "starton":
                                startOn = string.IsNullOrWhiteSpace(value.Value) ?  string.Empty :value.Value ;
                                break;
                            case "endon":
                                endOn = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "status":
                                status = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "classification":
                                classification = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "preference":
                                preference = string.IsNullOrWhiteSpace(value.Value) ? string.Empty :value.Value;
                                break;
                            default:
                                throw new ArgumentException(string.Concat("Invalid filter value received: ", value.Key));
                        }
                    }
                }

                AddDataPrivacyContextProperty((await _institutionJobsService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await _institutionJobsService.GetInstitutionJobsAsync(page.Offset, page.Limit, person, employer, position,
                    department, startOn, endOn, status, classification, preference, bypassCache);

                return new PagedHttpActionResult<IEnumerable<Ellucian.Colleague.Dtos.InstitutionJobs>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);

            }
            catch (JsonSerializationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch
                (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }


        /// <summary>
        /// Return all InstitutionJobs
        /// </summary>
        /// <returns>List of InstitutionJobs <see cref="Dtos.InstitutionJobs"/> objects representing matching institutionJobs</returns>
        [System.Web.Http.HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100)]
        public async Task<IHttpActionResult> GetInstitutionJobs2Async(Paging page, [FromUri] string criteria = "")
        {
            var bypassCache = false;

            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                string person = string.Empty, employer = string.Empty, position = string.Empty, department = string.Empty,
                    startOn = string.Empty, endOn = string.Empty, status = string.Empty, classification = string.Empty,
                    preference = string.Empty;
                var criteriaValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(criteria);

                if (criteriaValues != null)
                {
                    foreach (var value in criteriaValues)
                    {
                        switch (value.Key.ToLower())
                        {
                            case "person":
                                person = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "employer":
                                employer = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "position":
                                position = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "department":
                                department = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "starton":
                                startOn = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "endon":
                                endOn = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "status":
                                status = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "classification":
                                classification = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "preference":
                                preference = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            default:
                                throw new ArgumentException(string.Concat("Invalid filter value received: ", value.Key));
                        }
                    }
                }
                var pageOfItems = await _institutionJobsService.GetInstitutionJobsAsync(page.Offset, page.Limit, person, employer, position,
                    department, startOn, endOn, status, classification, preference, bypassCache);

                return new PagedHttpActionResult<IEnumerable<Ellucian.Colleague.Dtos.InstitutionJobs>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);

            }
            catch (JsonSerializationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch
                (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Read (GET) an InstitutionJobs using a GUID
        /// </summary>
        /// <param name="guid">GUID to desired institutionJobs</param>
        /// <returns>An InstitutionJobs DTO object <see cref="Dtos.InstitutionJobs"/> in EEDM format</returns>
        [System.Web.Http.HttpGet, EedmResponseFilter]
        public async Task<Dtos.InstitutionJobs> GetInstitutionJobsByGuidAsync(string guid)
        {
            var bypassCache = false;

            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            if (string.IsNullOrEmpty(guid))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                AddDataPrivacyContextProperty((await _institutionJobsService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _institutionJobsService.GetInstitutionJobsByGuidAsync(guid);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Read (GET) an InstitutionJobs using a GUID
        /// </summary>
        /// <param name="guid">GUID to desired institutionJobs</param>
        /// <returns>An InstitutionJobs DTO object <see cref="Dtos.InstitutionJobs"/> in EEDM format</returns>
        [System.Web.Http.HttpGet]
        public async Task<Dtos.InstitutionJobs> GetInstitutionJobsByGuid2Async(string guid)
        {
            var bypassCache = false;

            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            if (string.IsNullOrEmpty(guid))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                return await _institutionJobsService.GetInstitutionJobsByGuidAsync(guid);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new institutionJobs
        /// </summary>
        /// <param name="institutionJobs">DTO of the new institutionJobs</param>
        /// <returns>An InstitutionJobs DTO object <see cref="Dtos.InstitutionJobs"/> in EEDM format</returns>
        [System.Web.Http.HttpPost]
        public async Task<Dtos.InstitutionJobs> PostInstitutionJobsAsync([FromBody] Dtos.InstitutionJobs institutionJobs)
        {
            //Update is not supported for Colleague but EEDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Create (POST) a new institutionJobs
        /// </summary>
        /// <param name="institutionJobs">DTO of the new institutionJobs</param>
        /// <returns>An InstitutionJobs DTO object <see cref="Dtos.InstitutionJobs"/> in EEDM format</returns>
        [System.Web.Http.HttpPost]
        public async Task<Dtos.InstitutionJobs> PostInstitutionJobs2Async([FromBody] Dtos.InstitutionJobs institutionJobs)
        {
           
            if (institutionJobs == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null institutionJobs argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }
          
            try
            {

                ValidateInstitutionJobs(institutionJobs);

                return await _institutionJobsService.PostInstitutionJobsAsync(institutionJobs);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                if (e.Errors == null || e.Errors.Count <= 0)
                {
                    throw CreateHttpResponseException(e.Message);
                }
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }

        }

        /// <summary>
        /// Update (PUT) an existing institutionJobs
        /// </summary>
        /// <param name="guid">GUID of the institutionJobs to update</param>
        /// <param name="institutionJobs">DTO of the updated institutionJobs</param>
        /// <returns>An InstitutionJobs DTO object <see cref="Dtos.InstitutionJobs"/> in EEDM format</returns>
        [System.Web.Http.HttpPut]
        public async Task<Dtos.InstitutionJobs> PutInstitutionJobsAsync([FromUri] string guid, [FromBody] Dtos.InstitutionJobs institutionJobs)
        {
            //Update is not supported for Colleague but EEDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Update (PUT) an existing institutionJobs
        /// </summary>
        /// <param name="guid">GUID of the institutionJobs to update</param>
        /// <param name="institutionJobs">DTO of the updated institutionJobs</param>
        /// <returns>An InstitutionJobs DTO object <see cref="Dtos.InstitutionJobs"/> in EEDM format</returns>
        [System.Web.Http.HttpPut]
        public async Task<Dtos.InstitutionJobs> PutInstitutionJobs2Async([FromUri] string guid, [FromBody] Dtos.InstitutionJobs institutionJobs)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            if (institutionJobs == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null institutionJobs argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }
            if (string.IsNullOrEmpty(institutionJobs.Id))
            {
                institutionJobs.Id = guid.ToLowerInvariant();
            }
            else if ((string.Equals(guid, Guid.Empty.ToString())) || (string.Equals(institutionJobs.Id, Guid.Empty.ToString())))
            {
                throw CreateHttpResponseException(new IntegrationApiException("GUID empty",
                    IntegrationApiUtility.GetDefaultApiError("GUID must be specified.")));
            }
            else if (guid.ToLowerInvariant() != institutionJobs.Id.ToLowerInvariant())
            {
                throw CreateHttpResponseException(new IntegrationApiException("GUID mismatch",
                    IntegrationApiUtility.GetDefaultApiError("GUID not the same as in request body.")));
            }

            try
            {

                ValidateInstitutionJobs(institutionJobs);

                return await _institutionJobsService.PutInstitutionJobsAsync(guid, institutionJobs);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                if (e.Errors == null || e.Errors.Count <= 0)
                {
                    throw CreateHttpResponseException(e.Message);
                }
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }
        /// <summary>
        /// Delete (DELETE) a institutionJobs
        /// </summary>
        /// <param name="guid">GUID to desired institutionJobs</param>
        [System.Web.Http.HttpDelete]
        public async Task DeleteInstitutionJobsAsync(string guid)
        {
            //Update is not supported for Colleague but EEDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Helper method to validate Institution-Jobs PUT/POST.
        /// </summary>
        /// <param name="institutionJobs"><see cref="Dtos.InstitutionJobs"/>InstitutionJobs DTO object of type</param>
        private void ValidateInstitutionJobs(Dtos.InstitutionJobs institutionJobs)
        {
           
            if (institutionJobs == null)
            {
                throw new ArgumentNullException("institutionJobs", "The body is required when submitting an institutionJobs. ");
            }

        }
    }
 }