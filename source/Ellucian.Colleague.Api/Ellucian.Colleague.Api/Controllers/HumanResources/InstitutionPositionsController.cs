﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */

using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;
using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Colleague.Coordination.HumanResources.Services;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Dtos;
using Ellucian.Web.Http;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Security;

namespace Ellucian.Colleague.Api.Controllers.HumanResources
{
    /// <summary>
    /// Expose Human Resources Institution Positions data
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.HumanResources)]
    public class InstitutionPositionsController : BaseCompressedApiController
    {
        private readonly ILogger _logger;
        private readonly IInstitutionPositionService _institutionPositionService;

        /// <summary>
        /// InstitutionPositionsController constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="institutionPositionService"></param>
        public InstitutionPositionsController(ILogger logger, IInstitutionPositionService institutionPositionService)
        {
            this._logger = logger;
            this._institutionPositionService = institutionPositionService;
        }


        /// <summary>
        /// Retrieves an Institution Positions by ID.
        /// </summary>
        /// <returns>An <see cref="Dtos.InstitutionPosition">InstitutionPosition</see>object.</returns>
        [HttpGet]
        public async Task<InstitutionPosition> GetInstitutionPositionsByGuidAsync(string guid)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _institutionPositionService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _institutionPositionService.GetInstitutionPositionByGuidAsync(guid);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Return a list of InstitutionPosition objects based on selection criteria.
        /// </summary>
        /// <param name="page">page</param>
        /// <param name="campus">The physical location of the institution position</param>
        /// <param name="status">The status of the position (e.g. active, frozen, cancelled, inactive)</param>
        /// <param name="bargainingUnit">The group or union associated with the position</param>
        /// <param name="reportsToPosition">The position to which this position reports</param>
        /// <param name="exemptionType">An indicator if the position is exempt or non-exempt</param>
        /// <param name="compensationType">The type of compensation awarded (e.g. salary, wages, etc.)</param>
        /// <param name="startOn">The date when the position is first available</param>
        /// <param name="endOn">The date when the position is last available</param>
        /// <returns>List of InstitutionPositions <see cref="Dtos.InstitutionPosition"/> objects representing matching Institution Positions</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100)]
        public async Task<IHttpActionResult> GetInstitutionPositionsAsync(Paging page, [FromUri] string campus = "", [FromUri] string status = "", [FromUri] string bargainingUnit = "",
            [FromUri] string reportsToPosition = "", [FromUri] string exemptionType = "", [FromUri] string compensationType = "", [FromUri] string startOn = "", [FromUri] string endOn = "") 
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                if (!string.IsNullOrEmpty(status))
                {
                    if (!status.Equals("active") && !status.Equals("frozen") && !status.Equals("cancelled") && !status.Equals("inactive"))
                    {
                        throw new ArgumentException(
                            string.Concat("The filter status must be one of these values (active, frozen, cancelled, inactive) and the value sent in was '",
                                status, "'"));
                    }

                    if (status.Equals("frozen"))
                    {
                        throw new ArgumentException(string.Concat("The filter status frozen is not supported in Colleague"));
                    }
                }

                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                AddDataPrivacyContextProperty((await _institutionPositionService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await _institutionPositionService.GetInstitutionPositionsAsync(page.Offset, page.Limit, campus, status,
                            bargainingUnit, reportsToPosition, exemptionType, compensationType, startOn, endOn, bypassCache);

                return new PagedHttpActionResult<IEnumerable<InstitutionPosition>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Creates a Institution Position.
        /// </summary>
        /// <param name="institutionPosition"><see cref="Dtos.InstitutionPosition">InstitutionPosition</see> to create</param>
        /// <returns>Newly created <see cref="Dtos.InstitutionPosition">InstitutionPosition</see></returns>
        [HttpPost]
        public async Task<Dtos.InstitutionPosition> CreateInstitutionPositionsAsync([FromBody] Dtos.InstitutionPosition institutionPosition)
        {
            //Create is not supported for Colleague but Data Model requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Updates an Institution Position.
        /// </summary>
        /// <param name="guid">Guid of the Institution Position to update</param>
        /// <param name="institutionPosition"><see cref="Dtos.InstitutionPosition">InstitutionPosition</see> to create</param>
        /// <returns>Updated <see cref="Dtos.InstitutionPosition">InstitutionPosition</see></returns>
        [HttpPut]
        public async Task<Dtos.InstitutionPosition> UpdateInstitutionPositionsAsync([FromUri] string guid, [FromBody] Dtos.InstitutionPosition institutionPosition)
        {

            //Update is not supported for Colleague but Data Model requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Deletes an Institution Positions.
        /// </summary>
        /// <param name="guid">Guid of the Institution Position to be deleted</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task DefaultDeleteInstitutionPositions(string guid)
        {
            //Delete is not supported for Colleague but Data Model requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

     
    }
}