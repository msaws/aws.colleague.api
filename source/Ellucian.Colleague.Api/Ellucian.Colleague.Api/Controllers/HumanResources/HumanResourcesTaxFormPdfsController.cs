﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.ComponentModel;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.HumanResources.Services;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;

namespace Ellucian.Colleague.Api.Controllers.HumanResources
{
    /// <summary>
    /// This is the controller for tax form pdf.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.HumanResources)]
    public class HumanResourcesTaxFormPdfsController : BaseCompressedApiController
    {
        private readonly IAdapterRegistry adapterRegistry;
        private readonly ILogger logger;
        private readonly IHumanResourcesTaxFormPdfService taxFormPdfService;

        /// <summary>
        /// Initialize the Tax Form pdf controller.
        /// </summary>
        public HumanResourcesTaxFormPdfsController(IAdapterRegistry adapterRegistry, ILogger logger, IHumanResourcesTaxFormPdfService taxFormPdfService)
        {
            this.adapterRegistry = adapterRegistry;
            this.logger = logger;
            this.taxFormPdfService = taxFormPdfService;
        }

        /// <summary>
        /// Returns the data to be printed on the pdf for the W-2 tax form.
        /// </summary>
        /// <param name="personId">ID of the person assigned to and requesting the W-2.</param>
        /// <param name="recordId">The record ID where the W-2 pdf data is stored</param>
        /// <returns>HttpResponseMessage</returns>
        public async Task<HttpResponseMessage> GetW2TaxFormPdf(string personId, string recordId)
        {
            if (string.IsNullOrEmpty(personId))
                throw CreateHttpResponseException("Person ID must be specified.", HttpStatusCode.BadRequest);

            if (string.IsNullOrEmpty(recordId))
                throw CreateHttpResponseException("Record ID must be specified.", HttpStatusCode.BadRequest);

            string pdfTemplatePath = string.Empty;
            try
            {
                var pdfData = await taxFormPdfService.GetW2TaxFormData(personId, recordId);

                // Determine which PDF template to use.
                switch (pdfData.TaxYear)
                {
                    case "2016":
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/HumanResources/2016-W2-W2ST.pdf");
                        break;
                    case "2015":
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/HumanResources/2015-W2-W2ST.pdf");
                        break;
                    case "2014":
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/HumanResources/2014-W2-W2ST.pdf");
                        break;
                    case "2013":
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/HumanResources/2013-W2-W2ST.pdf");
                        break;
                    case "2012":
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/HumanResources/2012-W2-W2ST.pdf");
                        break;
                    case "2011":
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/HumanResources/2011-W2-W2ST.pdf");
                        break;
                    case "2010":
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/HumanResources/2010-W2.pdf");
                        break;
                    default:
                        var message = string.Format("Incorrect Tax Year {0}", pdfData.TaxYear);
                        logger.Error(message);
                        throw new ApplicationException(message);
                }

                var pdfBytes = taxFormPdfService.PopulateW2Pdf(pdfData, pdfTemplatePath);

                // Create and return the HTTP response object
                var response = new HttpResponseMessage();
                response.Content = new ByteArrayContent(pdfBytes);

                var fileNameString = "TaxFormW2" + "_" + recordId;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = fileNameString + ".pdf"
                };
                response.Content.Headers.ContentLength = pdfBytes.Length;
                return response;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                throw CreateHttpResponseException("Error retrieving W-2 PDF data.", HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Returns the data to be printed on the pdf for the 1095-C tax form.
        /// </summary>
        /// <param name="personId">ID of the person assigned to and requesting the 1095-C.</param>
        /// <param name="recordId">ID of the record where the 1095-C pdf data is stored</param>
        /// <returns>HttpResponseMessage</returns>
        public async Task<HttpResponseMessage> Get1095cTaxFormPdf(string personId, string recordId)
        {
            if (string.IsNullOrEmpty(personId))
                throw CreateHttpResponseException("Person ID must be specified.", HttpStatusCode.BadRequest);

            if (string.IsNullOrEmpty(recordId))
                throw CreateHttpResponseException("Record ID must be specified.", HttpStatusCode.BadRequest);

            string pdfTemplatePath = string.Empty;
            try
            {
                var pdfData = await taxFormPdfService.Get1095cTaxFormData(personId, recordId);

                switch (pdfData.TaxYear)
                {
                    case "2016":
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/HumanResources/2016-1095C.pdf");
                        break;
                    case "2015":
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/HumanResources/2015-1095C.pdf");
                        break;
                    default:
                        var message = string.Format("Incorrect Tax Year {0}", pdfData.TaxYear);
                        logger.Error(message);
                        throw new ApplicationException(message);
                }

                var pdfBytes = taxFormPdfService.Populate1095cPdf(pdfData, pdfTemplatePath);

                // Create and return the HTTP response object
                var response = new HttpResponseMessage();
                response.Content = new ByteArrayContent(pdfBytes);

                var fileNameString = "TaxForm1095c" + "_" + recordId;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = fileNameString + ".pdf"
                };
                response.Content.Headers.ContentLength = pdfBytes.Length;
                return response;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                throw CreateHttpResponseException("Error retrieving 1095-C PDF data.", HttpStatusCode.BadRequest);
            }
        }
    }
}