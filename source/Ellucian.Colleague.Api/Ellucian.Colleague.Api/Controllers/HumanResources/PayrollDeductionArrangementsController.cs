﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.HumanResources.Services;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http.Models;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.HumanResources
{
    /// <summary>
    /// Exposes payroll deduction arrangement change data
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.HumanResources)]
    public class PayrollDeductionArrangementsController : BaseCompressedApiController
    {
        private readonly ILogger logger;
        private readonly IPayrollDeductionArrangementService _payrollDeductionArrangementService;

        /// <summary>
        /// ..ctor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="payrollDeductionArrangementService"></param>
        public PayrollDeductionArrangementsController(ILogger logger, IPayrollDeductionArrangementService payrollDeductionArrangementService)
        {
            this.logger = logger;
            this._payrollDeductionArrangementService = payrollDeductionArrangementService;
        }

        /// <summary>
        /// Accept requests from external systems for new employee deductions in the authoritative HR system.
        /// </summary>
        /// <param name="page">Page of items for Paging</param>
        /// <param name="person">Person GUID filter</param>
        /// <param name="contribution">Contribution ID filter</param>
        /// <param name="deductionType">Deposit Type filter</param>
        /// <param name="status">Status Type filter</param>
        /// <returns>HTTP action results object containing <see cref="Dtos.PayrollDeductionArrangement"/></returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetPayrollDeductionArrangementsAsync(Paging page,
            [FromUri] string person = "", [FromUri] string contribution = "", [FromUri] string deductionType = "",
            [FromUri] string status = "")
        {
            try
            {
                bool bypassCache = false;
                if (Request.Headers.CacheControl != null)
                {
                    if (Request.Headers.CacheControl.NoCache)
                    {
                        bypassCache = true;
                    }
                }
                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                AddDataPrivacyContextProperty((await _payrollDeductionArrangementService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await _payrollDeductionArrangementService.GetPayrollDeductionArrangementsAsync(page.Offset, page.Limit, bypassCache,
                    person, contribution, deductionType, status);

                return new PagedHttpActionResult<IEnumerable<Dtos.PayrollDeductionArrangement>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error getting payroll deduction arrangement.");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Returns a payroll deduction arrangement.
        /// </summary>
        /// <param name="id">Global Identifier for PayrollDeductionArrangement</param>
        /// <returns>Object of type <see cref="Dtos.PayrollDeductionArrangement"/></returns>
        [EedmResponseFilter]
        public async Task<Dtos.PayrollDeductionArrangement> GetPayrollDeductionArrangementByIdAsync([FromUri] string id)
        {
            bool bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _payrollDeductionArrangementService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _payrollDeductionArrangementService.GetPayrollDeductionArrangementByIdAsync(id);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (KeyNotFoundException e)
            {
                logger.Error(e, string.Format("No payroll deduction arrangement was found for guid '{0}'.", id));
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error getting payroll deduction arrangement.");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// PutPayrollDeductionArrangementAsync
        /// </summary>
        /// <param name="id">Id for the PayrollDeduction Arrangement</param>
        /// <param name="payrollDeductionArrangement">The full request to update payroll deduction arrangement</param>
        /// <returns>Object of type <see cref="Dtos.PayrollDeductionArrangement"/></returns>
        [HttpPut]
        public async Task<Dtos.PayrollDeductionArrangement> PutPayrollDeductionArrangementAsync([FromUri] string id, [FromBody] Dtos.PayrollDeductionArrangement payrollDeductionArrangement)
        {
            VerifyPayrollDeductionArrangement(payrollDeductionArrangement, id);

            try
            {
                await _payrollDeductionArrangementService.DoesUpdateViolateDataPrivacySettings(GetRouteResourceName(), payrollDeductionArrangement);
                return await _payrollDeductionArrangementService.PutPayrollDeductionArrangementAsync(id, payrollDeductionArrangement);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (KeyNotFoundException e)
            {
                logger.Error(e, string.Format("No payroll deduction arrangement was found for guid '{0}'.", id));
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error getting payroll deduction arrangement.");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// PostPayrollDeductionArrangementAsync
        /// </summary>
        /// <param name="payrollDeductionArrangement">The full request to create a new payroll deduction arrangement</param>
        /// <returns>Object of type <see cref="Dtos.PayrollDeductionArrangement"/></returns>
        [HttpPost]
        public async Task<Dtos.PayrollDeductionArrangement> PostPayrollDeductionArrangementAsync([FromBody] Dtos.PayrollDeductionArrangement payrollDeductionArrangement)
        {
            VerifyPayrollDeductionArrangement(payrollDeductionArrangement);

            try
            {
                return await _payrollDeductionArrangementService.PostPayrollDeductionArrangementAsync(payrollDeductionArrangement);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error getting payroll deduction arrangement.");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// DeletePayrollDeductionArrangementAsync
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Unsupported Default message of type <see cref="IntegrationApiUtility.DefaultNotSupportedApiErrorMessage"/></returns>
        [HttpDelete]
        public async Task DeletePayrollDeductionArrangementAsync(string id)
        {
            //Delete is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Validate the payload coming into the API within the body of the request.
        /// </summary>
        /// <param name="payrollDeductionArrangement">From Body, the request payload</param>
        /// <param name="id">From a PUT, the ID from the URI</param>
        private void VerifyPayrollDeductionArrangement(Dtos.PayrollDeductionArrangement payrollDeductionArrangement, string id = "")
        {
            bool postRequest = false;
            if (string.IsNullOrEmpty(id))
            {
                postRequest = true;
            }

            // Validate ID properties
            if (string.IsNullOrEmpty(payrollDeductionArrangement.Id))
            {
                throw new ArgumentNullException("id", "The id must be specified for an update or create request. ");
            }
            if (!string.IsNullOrEmpty(id))
            {
                if (payrollDeductionArrangement.Id == null || !payrollDeductionArrangement.Id.Equals(id))
                {
                    throw new ArgumentOutOfRangeException("id", string.Format("Id on PUT request doesn't match Id in the request body of '{0}'. ", payrollDeductionArrangement.Id));
                }
            }
            var employeeId = payrollDeductionArrangement.Person != null ? payrollDeductionArrangement.Person.Id : string.Empty;
            if (string.IsNullOrEmpty(employeeId))
            {
                throw new ArgumentNullException("person.id", "The person id must be specified for an update or create request. ");
            }

            // Validate status property
            Dtos.EnumProperties.PayrollDeductionArrangementStatuses statusValue = payrollDeductionArrangement.Status;
            if (postRequest && statusValue != Dtos.EnumProperties.PayrollDeductionArrangementStatuses.Active)
            {
                throw new ArgumentOutOfRangeException("status", "A request for a new payroll deduction must have a status of 'active' to be accepted. ");
            }
            if (statusValue == Dtos.EnumProperties.PayrollDeductionArrangementStatuses.NotSet)
            {
                throw new ArgumentNullException("status", "The status is either invalid or missing.  A status is required for a payroll deduction. ");
            }

            // Validate Payment Target property
            if (payrollDeductionArrangement.PaymentTarget == null)
            {
                throw new ArgumentNullException("paymentTarget", "The paymentTarget property is required for a payroll deduction. ");
            }
            else
            {
                if (payrollDeductionArrangement.PaymentTarget.Commitment == null && payrollDeductionArrangement.PaymentTarget.Deduction == null)
                {
                    throw new ArgumentNullException("paymentTarget", "You must have either a commitment property or deduction property defined within the paymentTarget. ");
                }
                if (payrollDeductionArrangement.PaymentTarget.Commitment != null && payrollDeductionArrangement.PaymentTarget.Commitment.Type == Dtos.EnumProperties.CommitmentTypes.NotSet)
                {
                    throw new ArgumentNullException("paymentTarget.commitment.type", "The commitment type is either invalid or missing.  The commitment type is required when submitting with the commitment object. ");
                }
                if (payrollDeductionArrangement.PaymentTarget.Deduction != null && (payrollDeductionArrangement.PaymentTarget.Deduction.DeductionType == null || string.IsNullOrEmpty(payrollDeductionArrangement.PaymentTarget.Deduction.DeductionType.Id)))
                {
                    throw new ArgumentNullException("paymentTarget.deduction.deductionType.id", "The paymentTarget.deduction.deductionType.id is required when submitting with the deduction object. ");
                }
            }
            // Validate Payment Amount
            if (payrollDeductionArrangement.amountPerPayment == null || !payrollDeductionArrangement.amountPerPayment.Value.HasValue)
            {
                throw new ArgumentNullException("amountPerPayment.value", "The amountPerPayment.value property is required for a payroll deduction. ");
            }
            if (payrollDeductionArrangement.amountPerPayment != null && payrollDeductionArrangement.amountPerPayment.Value.HasValue)
            {
                if (payrollDeductionArrangement.amountPerPayment.Currency != Dtos.EnumProperties.CurrencyCodes.CAD && payrollDeductionArrangement.amountPerPayment.Currency != Dtos.EnumProperties.CurrencyCodes.USD)
                {
                    throw new ArgumentException("Only USD and CAD currency values are allowed. ", "amountPerPayment.Currency");
                }
            }
            // Validate Total Amount
            if (payrollDeductionArrangement.TotalAmount != null && !payrollDeductionArrangement.TotalAmount.Value.HasValue)
            {
                throw new ArgumentNullException("totalAmount.value", "The totalAmount.value property must be set when submitting the totalAmount object. ");
            }
            if (payrollDeductionArrangement.TotalAmount != null && payrollDeductionArrangement.TotalAmount.Value.HasValue)
            {
                if (payrollDeductionArrangement.TotalAmount.Currency != Dtos.EnumProperties.CurrencyCodes.CAD && payrollDeductionArrangement.TotalAmount.Currency != Dtos.EnumProperties.CurrencyCodes.USD)
                {
                    throw new ArgumentException("Only USD and CAD currency values are allowed. ", "totalAmount.Currency");
                }
            }
            // Validate start and end dates
            if (!payrollDeductionArrangement.StartDate.HasValue)
            {
                throw new ArgumentNullException("startOn", "The startOn property is required when submitting a payroll deduction. ");
            }
            if (payrollDeductionArrangement.EndDate.HasValue && payrollDeductionArrangement.EndDate < payrollDeductionArrangement.StartDate)
            {
                throw new ArgumentNullException("endOn", "The endOn property, if included, must be greater than the startOn propertly. ");
            }
            // Validate Pay Period Occurance
            if (payrollDeductionArrangement.PayPeriodOccurence == null)
            {
                throw new ArgumentNullException("payPeriodOccurence", "The payPeriodOccurance property is required for a payroll deduction. ");
            }
            else
            {
                if (payrollDeductionArrangement.PayPeriodOccurence.Interval == null && payrollDeductionArrangement.PayPeriodOccurence.MonthlyPayPeriods == null)
                {
                    throw new ArgumentNullException("payPeriodOccurence", "You must have either a interval property or monthlyPayPeriods property defined within the payPeriodOccurence. ");
                }
                if (payrollDeductionArrangement.PayPeriodOccurence.Interval != null && (payrollDeductionArrangement.PayPeriodOccurence.Interval <= 0 || payrollDeductionArrangement.PayPeriodOccurence.Interval > 31))
                {
                    throw new ArgumentNullException("payPeriodOccurence.interval", "The payPeriodOccurence.interval must be a positive number between 1 and 31. ");
                }
                if (payrollDeductionArrangement.PayPeriodOccurence.MonthlyPayPeriods != null)
                {
                    foreach (var payPeriod in payrollDeductionArrangement.PayPeriodOccurence.MonthlyPayPeriods)
                    {
                        if (payPeriod == 0 || payPeriod > 31 || payPeriod < -1)
                        {
                            throw new ArgumentNullException("payPeriodOccurence.monthlyPayPeriods", "The payPeriodOccurence.monthlyPayPeriods must be a positive number between -1 and 31, excluding 0. ");
                        }
                    }
                
                }
                if (payrollDeductionArrangement.PayPeriodOccurence.Interval != null && payrollDeductionArrangement.PayPeriodOccurence.MonthlyPayPeriods != null)
                {
                    throw new ArgumentNullException("payPeriodOccurence", "You have entered both interval property and monthlyPayPeriods property. You can only specify one");
                }
            }
            // Validate Change Reason
            if (payrollDeductionArrangement.ChangeReason != null && string.IsNullOrEmpty(payrollDeductionArrangement.ChangeReason.Id))
            {
                throw new ArgumentNullException("changeReason.id", "The changeReason.id property is required when submitting the changeReason object. ");
            }
        }
    }
}