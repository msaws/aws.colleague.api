﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.HumanResources
{
    /// <summary>
    /// Expose Human Resources Earnings Types data
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.HumanResources)]
    public class EarningsTypesController : BaseCompressedApiController
    {
        private readonly ILogger logger;
        private readonly IAdapterRegistry adapterRegistry;
        private readonly IEarningsTypeRepository earningsTypeRepository;

        /// <summary>
        /// EarningsTypesController constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="adapterRegistry"></param>
        /// <param name="earningsTypeRepository"></param>
        public EarningsTypesController(ILogger logger, IAdapterRegistry adapterRegistry, IEarningsTypeRepository earningsTypeRepository)
        {
            this.logger = logger;
            this.adapterRegistry = adapterRegistry;
            this.earningsTypeRepository = earningsTypeRepository;
        }

        /// <summary>
        /// Gets a list of earnings types. An earnings type is an identifier for wages or leave associated with an employment position.   
        /// The returned list should contain all active and inactive earn types available for an institution
        /// </summary>
        /// <returns>A List of earnings type objects</returns>
        [HttpGet]
        public async Task<IEnumerable<EarningsType>> GetEarningsTypesAsync()
        {
            try
            {
                var earningsTypeEntities = await earningsTypeRepository.GetEarningsTypesAsync();
                var entityToDtoAdapter = adapterRegistry.GetAdapter<Domain.HumanResources.Entities.EarningsType, EarningsType>();
                return earningsTypeEntities.Select(earningsType => entityToDtoAdapter.MapToType(earningsType));
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error occurred");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }
    }
}
