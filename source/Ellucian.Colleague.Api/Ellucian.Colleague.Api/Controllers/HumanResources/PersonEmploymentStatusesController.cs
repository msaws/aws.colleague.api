﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.HumanResources.Services;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.HumanResources
{
    /// <summary>
    /// Exposes personemploymentstatus data
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.HumanResources)]
    public class PersonEmploymentStatusesController : BaseCompressedApiController
    {
        private readonly ILogger logger;
        private readonly IPersonEmploymentStatusService personEmploymentStatusService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="personEmploymentStatusService"></param>
        public PersonEmploymentStatusesController(ILogger logger, IPersonEmploymentStatusService personEmploymentStatusService)
        {
            this.logger = logger;
            this.personEmploymentStatusService = personEmploymentStatusService;
        }

        /// <summary>
        /// Get personEmploymentStatus objects. This endpoint returns objects based on the current
        /// user's permissions.
        /// Example: If the current user is an employee, this endpoint returns that employee's personEmploymentStatuses
        /// Example: If the current user is a manager, this endpoint returns all the personEmploymentStatuses of the employees reporting to the manager
        /// </summary>
        /// <returns>A list of personEmploymentStatus objects</returns>
        public async Task<IEnumerable<PersonEmploymentStatus>> GetPersonEmploymentStatusesAsync()
        {
            try
            {
                return await personEmploymentStatusService.GetPersonEmploymentStatusesAsync();
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error getting person employment statuses");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }
    }
}