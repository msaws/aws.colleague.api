﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.HumanResources.Services;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Http;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http.Models;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.HumanResources
{
    /// <summary>
    /// Exposes PersonPosition data
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.HumanResources)]
    public class EmployeesController : BaseCompressedApiController
    {
        private readonly ILogger logger;
        private readonly IEmployeeService employeeService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="employeeService"></param>
        public EmployeesController(ILogger logger, IEmployeeService employeeService)
        {
            this.logger = logger;
            this.employeeService = employeeService;
        }

        /// <summary>
        /// Get a single employee using a guid.
        /// </summary>
        /// <param name="id">Guid of the employee to retrieve</param>
        /// <returns>Returns a single Employee object. <see cref="Dtos.Employee"/></returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.Employee> GetEmployeeByIdAsync([FromUri] string id)
        {
            bool bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await employeeService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await employeeService.GetEmployeeByIdAsync(id);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (KeyNotFoundException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error getting employee");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Get all employees using paging and including filters if necessary.
        /// </summary>
        /// <param name="page">Paging offset and limit.</param>
        /// <param name="person">Person id filter.</param>
        /// <param name="campus">Primary campus or location filter.</param>
        /// <param name="status">Status ("active", "terminated", or "leave") filter.</param>
        /// <param name="startOn">Start on a specific date filter.</param>
        /// <param name="endOn">End on a specific date filter.</param>
        /// <param name="rehireableStatusEligibility">Rehireable status ("eligible" or "ineligible") filter.</param>
        /// <param name="rehireableStatusType">Rehireable code filter.</param>
        /// <returns>Returns a list of Employee objects using paging.  <see cref="Dtos.Employee"/></returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetEmployeesAsync(Paging page,
            [FromUri] string person = "", [FromUri] string campus = "", [FromUri] string status = "",
            [FromUri] string startOn = "", [FromUri] string endOn = "", [FromUri] string rehireableStatusEligibility = "", [FromUri] string rehireableStatusType = "")
        {
            try
            {
                bool bypassCache = false;
                if (Request.Headers.CacheControl != null)
                {
                    if (Request.Headers.CacheControl.NoCache)
                    {
                        bypassCache = true;
                    }
                }
                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                AddDataPrivacyContextProperty((await employeeService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());

                var pageOfItems = await employeeService.GetEmployeesAsync(page.Offset, page.Limit, bypassCache, person, campus, status, startOn, endOn, rehireableStatusEligibility, rehireableStatusType);
                return new PagedHttpActionResult<IEnumerable<Dtos.Employee>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch(Exception e)
            {
                logger.Error(e, "Unknown error getting employee");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Update an existing employee
        /// </summary>
        /// <param name="id">Employee GUID for update.</param>
        /// <param name="employeeDto">Employee DTO request for update</param>
        /// <returns>Currently not implemented.  Returns default not supported API error message.</returns>
        [HttpPut]
        public async Task<Dtos.Employee> PutEmployeeAsync([FromUri] string id, [FromBody] Dtos.Employee employeeDto)
        {
            //Put is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Create a new employee record
        /// </summary>
        /// <param name="employeeDto">Employee DTO request for update</param>
        /// <returns>Currently not implemented.  Returns default not supported API error message.</returns>
        [HttpPost]
        public async Task<Dtos.Employee> PostEmployeeAsync([FromBody] Dtos.Employee employeeDto)
        {
            //Delete is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Delete an existing employee
        /// </summary>
        /// <param name="id">Employee GUID for update.</param>
        /// <returns>Currently not implemented.  Returns default not supported API error message.</returns>
        [HttpDelete]
        public async Task<Dtos.Employee> DeleteEmployeeAsync([FromUri] string id)
        {
            //Delete is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }
    }
}