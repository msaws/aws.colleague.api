﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.HumanResources.Services;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Ellucian.Colleague.Api.Controllers.HumanResources
{

     /// <summary>
     /// Expose Human Resources Employment Positions data
     /// </summary>
     [Authorize]
     [LicenseProvider(typeof(EllucianLicenseProvider))]
     [EllucianLicenseModule(ModuleConstants.HumanResources)]
     public class HumanResourcesController : BaseCompressedApiController
     {
          private readonly ILogger logger;
          private readonly IAdapterRegistry adapterRegistry;
          private readonly IHumanResourceDemographicsService humanResourceDemographicsService;

          /// <summary>
          /// HumanResourcesController constructor
          /// </summary>
          /// <param name="logger"></param>
          /// <param name="adapterRegistry"></param>
          /// <param name="humanResourceDemographicsService"></param>
          public HumanResourcesController(ILogger logger, IAdapterRegistry adapterRegistry, IHumanResourceDemographicsService humanResourceDemographicsService)
          {
               this.logger = logger;
               this.adapterRegistry = adapterRegistry;
               this.humanResourceDemographicsService = humanResourceDemographicsService;
          }

          /// <summary>
          /// Gets a list filled with all of the HumanResourceDemographics the current user is able to access
          /// </summary>
          /// <returns></returns>
          [HttpGet]
          public async Task<IEnumerable<HumanResourceDemographics>> GetHumanResourceDemographicsAsync()
          {
               try
               {
                    return await humanResourceDemographicsService.GetHumanResourceDemographicsAsync();
               }
               catch (PermissionsException pe)
               {
                    var message = "You do not have permission to GetHumanResourceDemographicsAsync";
                    logger.Error(pe, message);
                    throw CreateHttpResponseException(message, HttpStatusCode.Forbidden);
               }
               catch (Exception e)
               {
                    logger.Error(e, e.Message);
                    throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
               }

          }

          /// <summary>
          /// Gets a list filled with all of the HumanResourceDemographics the current user is able to access
          /// </summary>
          /// <returns></returns>
          [HttpGet]
          public async Task<HumanResourceDemographics> GetSpecificHumanResourceDemographicsAsync(string id)
          {
               try
               {
                    return await humanResourceDemographicsService.GetSpecificHumanResourceDemographicsAsync(id);
               }
               catch (PermissionsException pe)
               {
                    var message = "You do not have permission to GetHumanResourceDemographicsAsync";
                    logger.Error(pe, message);
                    throw CreateHttpResponseException(message, HttpStatusCode.Forbidden);
               }
               catch (ArgumentNullException ane)
               {
                    var message = "Supplied Id was null";
                    logger.Error(ane, message);
                    throw new ArgumentNullException(message);
               }
               catch (ApplicationException ap)
               {
                    var message = "Supplied Id was not found within the list of accessible Ids";
                    logger.Error(message);
                    throw CreateHttpResponseException(message, HttpStatusCode.NotFound);

               }
               catch (Exception e)
               {
                    logger.Error(e, e.Message);
                    throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
               }

          }
     }
}