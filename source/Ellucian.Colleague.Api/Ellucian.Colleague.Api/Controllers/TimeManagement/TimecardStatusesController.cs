﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.TimeManagement.Services;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.TimeManagement
{
    /// <summary>
    /// TimecardStatuses Controller
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.TimeManagement)]
    public class TimecardStatusesController : BaseCompressedApiController
    {
        private readonly ILogger logger;
        private readonly ITimecardStatusesService timecardStatusService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="timecardStatusService"></param>
        public TimecardStatusesController(ILogger logger, ITimecardStatusesService timecardStatusService)
        {
            this.logger = logger;
            this.timecardStatusService = timecardStatusService;
        }

        /// <summary>
        /// Creates a timecard status for the given timecard. A TimecardStatus represents an action performed on a Timecard at a point in time.
        /// Along with the TimecardStatus, a TimecardHistory record is also created. The TimecardHistory is a copy of the Timecard
        /// during the state in which the TimecardStatus was actioned.
        /// Once  a status record is created, it can be read, but never modified.
        /// Changes in status should create a new timecard status record for the associated timecard.
        /// 
        /// Example: If an employee submits a Timecard for approval, a TimecardStatus is created indicating the action performed by the employee. A TimecardHistory record is also created which is a copy of the Timecard when it was submitted.
        /// 
        /// The endpoint will reject the creation of a TimecardStatus if:
        ///     1. 400 - TimecardStatus object is not included in request body
        ///     2. 400 - TimecardStatus timecardId is not same as uri timecardId
        /// </summary>
        /// 
        /// <param name="id"></param>
        /// <param name="status"></param>
        /// <returns>The newly created timecard status</returns>
        [HttpPost]
        public async Task<TimecardStatus> CreateTimecardStatusAsync([FromUri] string id, [FromBody]TimecardStatus status)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException("id is required");
            }
            if (status == null)
            {
                throw CreateHttpResponseException("TimecardStatus object is missing from request body", HttpStatusCode.BadRequest);
            }
            if (id != status.TimecardId)
            {
                throw CreateHttpResponseException("timecardId from uri is not same as status timecardId", HttpStatusCode.BadRequest);
            }
            try
            {
                var response = await timecardStatusService.CreateTimecardStatusesAsync(new List<TimecardStatus> () {status} );
                return response.FirstOrDefault();
            }
            catch (PermissionsException pe)
            {
                logger.Error(pe, pe.Message);
                throw CreateHttpResponseException("You are not allowed to create a timecard status for the specified timecard", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Creates a set of timecard statuses. Replaces the individual TimecardStatus post method.
        /// Each TimecardStatus represents an action performed on a Timecard at a point in time.
        /// Along with the TimecardStatus, a TimecardHistory record is also created. The TimecardHistory is a copy of the Timecard
        /// during the state in which the TimecardStatus was actioned.
        /// Once  a status record is created, it can be read, but never modified.
        /// Changes in status should create a new timecard status record for the associated timecard.
        /// 
        /// Example: If an employee submits a Timecard for approval, a TimecardStatus is created indicating the action performed by the employee. A TimecardHistory record is also created which is a copy of the Timecard when it was submitted.
        /// 
        /// The endpoint will reject the creation of TimecardStatus if:
        ///     1. Bad Request: 400 - No TimecardStatus objects are included, or any TimecardStatus object does not provide valid data
        ///     2. Forbidden: 403 - User does not have permissions to create a TimecardStatus object for any incoming object
        /// </summary>
        /// 
        /// <param name="statuses"></param>
        /// <returns>The list of all successfully created timecard statuses. Any errors are logged but do not fail the request unless all items fail to post.</returns>
        [HttpPost]
        public async Task<IEnumerable<TimecardStatus>> CreateTimecardStatusesAsync([FromBody]List<TimecardStatus> statuses)
        {
            if (statuses == null || statuses.Count() == 0)
            {
                throw CreateHttpResponseException("TimecardStatus list is missing from request body", HttpStatusCode.BadRequest);
            }
            foreach (var status in statuses)
            {
                if (status == null || string.IsNullOrEmpty(status.TimecardId))
                {
                    throw CreateHttpResponseException("Invalid TimecardStatus object found in the request body", HttpStatusCode.BadRequest);
                }
            }
            try
            {
                return await timecardStatusService.CreateTimecardStatusesAsync(statuses);
            }
            catch (PermissionsException pe)
            {
                logger.Error(pe, pe.Message);
                throw CreateHttpResponseException("You are not allowed to create a timecard status for the specified timecard", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Gets all timecard statuses for a given Timecard id.
        /// The status with the most recent timestamp add datetime should be considered the active status for the timecard
        /// 
        /// The endpoint will reject the update of a Timecard if:
        ///     1. 400 - Timecard id is not provided in URI 
        /// </summary>
        /// <param name="id"></param>
        /// <returns>All statuses for the timecard</returns>
        [HttpGet]
        public async Task<IEnumerable<TimecardStatus>> GetTimecardStatusesByTimecardIdAsync([FromUri]string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException("Timecard id required in request URI", HttpStatusCode.BadRequest);
            }
            try
            {
                return await timecardStatusService.GetTimecardStatusesByTimecardIdAsync(id);
            }
            catch (KeyNotFoundException knfe)
            {
                logger.Error(knfe, knfe.Message);
                throw CreateNotFoundException("Timecard", id);
            }
            catch (PermissionsException pe)
            {
                logger.Error(pe, pe.Message);
                throw CreateHttpResponseException("You are not allowed to get timecard statuses for the specified timecard", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Get the latest timecard statuses for which you have permission to view. A single timecard can have multiple
        /// statuses, and this endpoint returns the most recent of the statuses.
        /// </summary>
        /// <returns>Latest statuses for all timecards for which you have permission to view</returns>
        [HttpGet]
        public async Task<IEnumerable<TimecardStatus>> GetLatestTimecardStatusesAsync()
        {
            try
            {
                return await timecardStatusService.GetTimecardStatusesAsync(false);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }
    }
}