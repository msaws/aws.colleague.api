﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.TimeManagement.Services;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using Microsoft.Practices.Unity;
using slf4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.TimeManagement
{
    /// <summary>
    /// Comments controller
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.TimeManagement)]
    public class TimeEntryCommentsController : BaseCompressedApiController
    {
        private readonly ILogger logger;
        private readonly ITimeEntryCommentsService commentsService;

        /// <summary>
        ///  Constructs the comments controller
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="commentsService"></param>
        public TimeEntryCommentsController(ILogger logger, ITimeEntryCommentsService commentsService)
        {
            this.logger = logger;
            this.commentsService = commentsService;
        }

        /// <summary>
        /// Gets all time entry comments records the logged in user has permission to get. 
        /// These time entry comments records may be associated to the time entry of the current user, or to the time entry of a subordinate if the current user has a supervisor role.
        /// See the TimeEntryComments for specific properties of the created and returned objects.
        /// Error conditions:
        /// 1.  403 - Permissions exception - the user is attempting to create a time entry comments for an entity other than self or subordinates. 
        /// 2.  400 - Bad request.
        /// </summary>
        /// <returns>A list of time entry comments records</returns>
        [HttpGet]
        public async Task<IEnumerable<TimeEntryComments>> GetTimeEntryCommentsAsync()
        {
            try
            {
                return await commentsService.GetCommentsAsync();
            }
            catch (PermissionsException pe)
            {
                var message = "You do not have permission to GetTimeCardsAsync";
                logger.Error(pe, message);
                throw CreateHttpResponseException(message, HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }
        /// <summary>
        /// Creates a time entry comments record. 
        /// This time entry comments record may be associated to the time entry of the current user, or to the time entry of a subordinate if the current user has a supervisor role.
        /// See the TimeEntryComments for specific properties of the created and returned object.
        /// Error conditions:
        /// 1.  403 - Permissions exception - the user is attempting to create a time entry comments for an entity other than self or subordinates. 
        /// 2.  400 - Bad request.
        /// </summary>
        /// <param name="comments"></param>
        /// <returns>TimeEntryComments</returns>
        [HttpPost]
        public async Task<TimeEntryComments> CreateTimeEntryCommentsAsync([FromBody] TimeEntryComments comments)
        {
            try
            {
                return await commentsService.CreateCommentsAsync(comments);
            }
            catch(PermissionsException pe)
            {
                var message = string.Format("Current user does have permission to CreateCommentsAsync for employee {0}", comments.EmployeeId);
                logger.Error(pe, message);
                throw CreateHttpResponseException(message, HttpStatusCode.Forbidden);
            }
            catch(Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }

        }
    }
}