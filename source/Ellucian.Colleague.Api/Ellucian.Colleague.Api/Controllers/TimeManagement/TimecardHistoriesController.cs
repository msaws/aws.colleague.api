﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Coordination.TimeManagement.Services;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using Microsoft.Practices.Unity;
using slf4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.TimeManagement
{
    /// <summary>
    /// Exposes access to employee time cards for time entry
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.TimeManagement)]
    public class TimecardHistoriesController : BaseCompressedApiController
    {
        private readonly ILogger logger;
        private readonly ITimecardHistoriesService timecardHistoriesService;

        /// <summary>
        /// Timecard Histories controller constructor
        /// </summary>
        /// 
        /// <param name="logger"></param>
        /// <param name="timecardHistoriesService"></param>
        public TimecardHistoriesController(ILogger logger, ITimecardHistoriesService timecardHistoriesService)
        {
            this.logger = logger;
            this.timecardHistoriesService = timecardHistoriesService;
        }

        /// <summary>
        /// Gets all timecard histories for the currently authenticated API user (as an employee) that exist between a start date and an end date.
        /// 
        /// Example:  if the current user is an employee, all of the employees timecard histories between the start date and end date will be returned.
        /// Example:  if the current user is a manager, all of his/her supervised employees' timecard histories between a start date and an end date will be returned.
        /// 
        /// The endpoint will not return the requested timecard histories if:
        ///     1.  400 - the startDate or endDate is not included in request URI
        ///     2.  403 - User does not have permission to get requested timecard histories
        /// </summary>
        /// 
        /// <returns>
        /// A list of timecard histories.  
        /// See the documentation for TimecardHistory for specific property information.
        /// </returns>
        [HttpGet]
        [Obsolete("Obsolete as of API verson 1.15; use version 2 of this endpoint")]
        public async Task<IEnumerable<TimecardHistory>> GetTimecardHistoriesAsync(
            [FromUri(Name = "startDate")]DateTime startDate,
            [FromUri(Name = "endDate")]DateTime endDate)
        {
            try
            {
                return await timecardHistoriesService.GetTimecardHistoriesAsync(startDate, endDate);
            }
            catch (PermissionsException pe)
            {
                var message = "You do not have permission to GetTimecardHistoriesAsync";
                logger.Error(pe, message);
                throw CreateHttpResponseException(message, HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }

        }

        /// <summary>
        /// Gets all timecard histories for the currently authenticated API user (as an employee) that exist between a start date and an end date.
        /// 
        /// Example:  if the current user is an employee, all of the employees timecard histories between the start date and end date will be returned.
        /// Example:  if the current user is a manager, all of his/her supervised employees' timecard histories between a start date and an end date will be returned.
        /// 
        /// The endpoint will not return the requested timecard histories if:
        ///     1.  400 - the startDate or endDate is not included in request URI
        ///     2.  403 - User does not have permission to get requested timecard histories
        /// </summary>
        /// 
        /// <returns>
        /// A list of timecard histories.  
        /// See the documentation for TimecardHistory for specific property information.
        /// </returns>
        [HttpGet]
        public async Task<IEnumerable<TimecardHistory2>> GetTimecardHistories2Async(
            [FromUri(Name = "startDate")]DateTime startDate,
            [FromUri(Name = "endDate")]DateTime endDate)
        {
            try
            {
                return await timecardHistoriesService.GetTimecardHistories2Async(startDate, endDate);
            }
            catch (PermissionsException pe)
            {
                var message = "You do not have permission to GetTimecardHistoriesAsync";
                logger.Error(pe, message);
                throw CreateHttpResponseException(message, HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }

        }
    }
}