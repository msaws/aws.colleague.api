﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.TimeManagement.Services;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.TimeManagement
{
    /// <summary>
    /// OvertimeController
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.TimeManagement)]
    public class OvertimeController : BaseCompressedApiController
    {
        private readonly ILogger logger;
        private readonly IOvertimeCalculationService overtimeCalculationService;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="overtimeCalculationService"></param>
        /// <param name="logger"></param>
        public OvertimeController(IOvertimeCalculationService overtimeCalculationService, ILogger logger)
        {
            this.overtimeCalculationService = overtimeCalculationService;
            this.logger = logger;
        }

        /// <summary>
        /// Requests an overtime calculation result for a provided the person and date range.
        /// The person and date range are specified in the OvertimeQueryCriteria object in the request body.
        /// See documentation for OvertimeQueryCriteria for information on individual properties.
        /// 
        /// This endpoint will return an error if:
        ///     1. 400 - The OvertimeQueryCriteria is not provided
        ///     2. 400 - The OvertimeQueryCriteria is incorrectly formatted
        ///     3. 403 - The logged in user does not have permission to access the information requested in the criteria
        ///     4. 400 - An unhandled exception occurs on the server
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns>An Overtime Calculation Result</returns>
        [HttpPost]
        public async Task<OvertimeCalculationResult> QueryByPostOvertime(OvertimeQueryCriteria criteria)
        {
            if (criteria == null)
            {
                throw CreateHttpResponseException("criteria is required argument", HttpStatusCode.BadRequest);
            }

            try
            {
                return await overtimeCalculationService.CalculateOvertime(criteria);
            }
            catch (ArgumentException ae)
            {
                logger.Error(ae, "Argument error in QueryByPostOvertime endpoint");
                throw CreateHttpResponseException(ae.Message, HttpStatusCode.BadRequest);
            }
            catch (ApplicationException ae)
            {
                logger.Error(ae, "Known error thrown by QueryByPostOvertime endpoint");
                throw CreateHttpResponseException(ae.Message, HttpStatusCode.BadRequest);
            }
            catch(PermissionsException pe)
            {
                logger.Error(pe, "Permissions error getting overtime");
                throw CreateHttpResponseException("You don't have permission to query overtime for the given criteria", HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }
    }
}