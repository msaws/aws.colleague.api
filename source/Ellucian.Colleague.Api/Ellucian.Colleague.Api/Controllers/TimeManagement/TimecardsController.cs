﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Coordination.TimeManagement.Services;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using Microsoft.Practices.Unity;
using slf4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;


namespace Ellucian.Colleague.Api.Controllers.TimeManagement
{
    /// <summary>
    /// Exposes access to employee time cards for time entry
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.TimeManagement)]
    public class TimecardsController : BaseCompressedApiController
    {
        private readonly ILogger logger;
        private readonly ITimecardsService timecardsService;

        private const string getTimecardRouteId = "GetTimecardAsync";
        private const string getTimecard2RouteId = "GetTimecard2Async";

        /// <summary>
        /// TimeCards controller constructor
        /// </summary>
        /// 
        /// <param name="logger"></param>
        /// <param name="timecardsService"></param>
        public TimecardsController(ILogger logger, ITimecardsService timecardsService)
        {

            this.logger = logger;
            this.timecardsService = timecardsService;

        }

        /// <summary>
        /// Gets all timecards for the currently authenticated API user (as an employee).
        /// All timecards will be returned regardless of status.
        /// 
        /// Example:  if the current user is an employee, all of the employees timecards will be returned.
        /// Example:  if the current user is a manager, all of his/her supervised employees' timecards will be returned.
        /// 
        /// The endpoint will not return the requested timecard if:
        ///     1.  400 - Id is not included in request URI
        ///     2.  403 - User does not have permisttion to get requested timecard
        /// </summary>
        /// 
        /// <returns>A list of timecards</returns>
        [HttpGet]
        [Obsolete("Obsolete as of API verson 1.15; use version 2 of this endpoint")]
        public async Task<IEnumerable<Timecard>> GetTimecardsAsync()
        {
            try
            {
                return await timecardsService.GetTimecardsAsync();
            }
            catch (PermissionsException pe)
            {
                var message = "You do not have permission to GetTimeCardsAsync";
                logger.Error(pe, message);
                throw CreateHttpResponseException(message, HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }

        }


        /// <summary>
        /// Gets a single timecard for a given timecard Id
        /// 
        /// The endpoint will not return the requested timecard if:
        ///     1.  400 - Id is not included in request URI
        ///     2.  400 - Unhandled application exception             
        ///     3.  403 - User does not have permisttion to get requested timecard
        ///     4.  404 - Given timecard Id is not found
        /// </summary>
        /// 
        /// <param name="id"></param>
        /// <returns>The requested timecard object</returns>
        [HttpGet]
        [Obsolete("Obsolete as of API verson 1.15; use version 2 of this endpoint")]
        public async Task<Timecard> GetTimecardAsync([FromUri]string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw CreateHttpResponseException("Id is a required argument", HttpStatusCode.BadRequest);
            }
            try
            {
                return await timecardsService.GetTimecardAsync(id);
            }
            catch (PermissionsException pe)
            {
                var message = "You do not have permission to GetTimeCardAsync";
                logger.Error(pe, message);
                throw CreateHttpResponseException(message, HttpStatusCode.Forbidden);
            }
            catch (KeyNotFoundException knfe)
            {
                logger.Error(knfe, knfe.Message);
                throw CreateNotFoundException("Timecard", id);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }

        }

        /// <summary>
        /// Create a single Timecard. This POST endpoint will create a Timecard along with it's associated time entries.
        /// 
        /// The endpoint will reject the creation of a Timecard if:
        ///     1. 403 - Employee does not have the correct permissions to create the Timecard
        ///     2. 409 - The Timecard resource has changed on server
        ///     3. 409 - The Timecard resource is locked by another resource
        ///     
        /// See the descriptions of the individual properties for more information about acceptable data values
        /// </summary>
        /// 
        /// <param name="timecard"></param>
        /// <returns>The Timecard created, including the new id returned in the response.</returns>
        [HttpPost]
        [Obsolete("Obsolete as of API verson 1.15; use version 2 of this endpoint")]
        public async Task<HttpResponseMessage> CreateTimecardAsync([FromBody]Timecard timecard)
        {
            if (timecard == null)
            {
                throw CreateHttpResponseException("timecard DTO is required in body of request");
            }
            try
            {
                var newTimecard = await timecardsService.CreateTimecardAsync(timecard);
                var response = Request.CreateResponse<Timecard>(HttpStatusCode.Created, newTimecard);
                SetResourceLocationHeader(getTimecardRouteId, new { id = newTimecard.Id });
                return response;
            }
            catch (PermissionsException pe)
            {
                var message = "You do not have permission to CreateTimecardAsync";
                logger.Error(pe, message);
                throw CreateHttpResponseException(message, HttpStatusCode.Forbidden);
            }
            catch (ExistingResourceException ere)
            {
                logger.Error(ere, ere.Message);
                SetResourceLocationHeader(getTimecardRouteId, new { id = ere.ExistingResourceId });
                throw CreateHttpResponseException(ere.Message, HttpStatusCode.Conflict);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Updates the requested Timecard and associated time entries.
        /// 
        /// Performs the following actions on associated time entries:
        ///     1.  A new time entry record will be created.
        ///     2.  An existing time entry record will be modified.
        ///     3.  The absence of a prior time entry record will prompt a deletion.
        /// 
        /// The endpoint will reject the update of a Timecard if:
        ///     1. 403 - Person does not have the correct permissions to update the Timecard
        ///     2. 404 - The Timecard resource requested for update does not exist
        ///     2. 409 - The Timecard resource has changed on server
        ///     3. 409 - The Timecard resource is locked by another resource
        /// </summary>
        /// 
        /// <param name="timecard"></param>
        /// <returns>A timecard</returns>
        [HttpPut]
        [Obsolete("Obsolete as of API verson 1.15; use version 2 of this endpoint")]
        public async Task<Timecard> UpdateTimecardAsync([FromBody]Timecard timecard)
        {
            if (timecard == null)
            {
                throw CreateHttpResponseException("timecard is required in body of request", HttpStatusCode.BadRequest);
            }
            try
            {
                var updatedTimecard = await timecardsService.UpdateTimecardAsync(timecard);
                return updatedTimecard;
            }
            catch (PermissionsException pe)
            {
                var message = "You do not have permission to UpdateTimeCardAsync";
                logger.Error(pe, message);
                throw CreateHttpResponseException(message, HttpStatusCode.Forbidden);
            }
            catch (KeyNotFoundException cnfe)
            {
                logger.Error(cnfe, cnfe.Message);
                throw CreateNotFoundException("Timecard", timecard.Id);
            }
            catch (RecordLockException rle)
            {
                logger.Error(rle, rle.Message);
                throw CreateHttpResponseException(rle.Message, HttpStatusCode.Conflict);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Gets all timecards for the currently authenticated API user (as an employee).
        /// All timecards will be returned regardless of status.
        /// 
        /// Example:  if the current user is an employee, all of the employees timecards will be returned.
        /// Example:  if the current user is a manager, all of his/her supervised employees' timecards will be returned.
        /// 
        /// The endpoint will not return the requested timecard if:
        ///     1.  400 - Id is not included in request URI
        ///     2.  403 - User does not have permisttion to get requested timecard
        /// </summary>
        /// 
        /// <returns>A list of timecards</returns>
        [HttpGet]
        public async Task<IEnumerable<Timecard2>> GetTimecards2Async()
        {
            try
            {
                return await timecardsService.GetTimecards2Async();
            }
            catch (PermissionsException pe)
            {
                var message = "You do not have permission to GetTimeCards2Async";
                logger.Error(pe, message);
                throw CreateHttpResponseException(message, HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }

        }


        /// <summary>
        /// Gets a single timecard for a given timecard Id
        /// 
        /// The endpoint will not return the requested timecard if:
        ///     1.  400 - Id is not included in request URI
        ///     2.  400 - Unhandled application exception             
        ///     3.  403 - User does not have permisttion to get requested timecard
        ///     4.  404 - Given timecard Id is not found
        /// </summary>
        /// 
        /// <param name="id"></param>
        /// <returns>The requested timecard object</returns>
        [HttpGet]
        public async Task<Timecard2> GetTimecard2Async([FromUri]string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw CreateHttpResponseException("Id is a required argument", HttpStatusCode.BadRequest);
            }
            try
            {
                return await timecardsService.GetTimecard2Async(id);
            }
            catch (PermissionsException pe)
            {
                var message = "You do not have permission to GetTimeCard2Async";
                logger.Error(pe, message);
                throw CreateHttpResponseException(message, HttpStatusCode.Forbidden);
            }
            catch (KeyNotFoundException knfe)
            {
                logger.Error(knfe, knfe.Message);
                throw CreateNotFoundException("Timecard", id);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }

        }

        /// <summary>
        /// Create a single Timecard. This POST endpoint will create a Timecard along with it's associated time entries.
        /// 
        /// The endpoint will reject the creation of a Timecard if:
        ///     1. 403 - Employee does not have the correct permissions to create the Timecard
        ///     2. 409 - The Timecard resource has changed on server
        ///     3. 409 - The Timecard resource is locked by another resource
        ///     
        /// See the descriptions of the individual properties for more information about acceptable data values
        /// </summary>
        /// 
        /// <param name="timecard2"></param>
        /// <returns>The Timecard created, including the new id returned in the response.</returns>
        [HttpPost]
        public async Task<HttpResponseMessage> CreateTimecard2Async([FromBody]Timecard2 timecard2)
        {
            if (timecard2 == null)
            {
                throw CreateHttpResponseException("timecard DTO is required in body of request");
            }
            try
            {
                var newTimecard = await timecardsService.CreateTimecard2Async(timecard2);
                var response = Request.CreateResponse<Timecard2>(HttpStatusCode.Created, newTimecard);
                SetResourceLocationHeader(getTimecard2RouteId, new { id = newTimecard.Id });
                return response;
            }
            catch (PermissionsException pe)
            {
                var message = "You do not have permission to CreateTimecardAsync";
                logger.Error(pe, message);
                throw CreateHttpResponseException(message, HttpStatusCode.Forbidden);
            }
            catch (ExistingResourceException ere)
            {
                logger.Error(ere, ere.Message);
                SetResourceLocationHeader(getTimecard2RouteId, new { id = ere.ExistingResourceId });
                throw CreateHttpResponseException(ere.Message, HttpStatusCode.Conflict);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Updates the requested Timecard and associated time entries.
        /// 
        /// Performs the following actions on associated time entries:
        ///     1.  A new time entry record will be created.
        ///     2.  An existing time entry record will be modified.
        ///     3.  The absence of a prior time entry record will prompt a deletion.
        /// 
        /// The endpoint will reject the update of a Timecard if:
        ///     1. 403 - Person does not have the correct permissions to update the Timecard
        ///     2. 404 - The Timecard resource requested for update does not exist
        ///     2. 409 - The Timecard resource has changed on server
        ///     3. 409 - The Timecard resource is locked by another resource
        /// </summary>
        /// 
        /// <param name="timecard2"></param>
        /// <returns>A timecard</returns>
        [HttpPut]
        public async Task<Timecard2> UpdateTimecard2Async([FromBody]Timecard2 timecard2)
        {
            if (timecard2 == null)
            {
                throw CreateHttpResponseException("timecard is required in body of request", HttpStatusCode.BadRequest);
            }
            try
            {
                var updatedTimecard = await timecardsService.UpdateTimecard2Async(timecard2);
                return updatedTimecard;
            }
            catch (PermissionsException pe)
            {
                var message = "You do not have permission to UpdateTimeCardAsync";
                logger.Error(pe, message);
                throw CreateHttpResponseException(message, HttpStatusCode.Forbidden);
            }
            catch (KeyNotFoundException cnfe)
            {
                logger.Error(cnfe, cnfe.Message);
                throw CreateNotFoundException("Timecard", timecard2.Id);
            }
            catch (RecordLockException rle)
            {
                logger.Error(rle, rle.Message);
                throw CreateHttpResponseException(rle.Message, HttpStatusCode.Conflict);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }
    }
}