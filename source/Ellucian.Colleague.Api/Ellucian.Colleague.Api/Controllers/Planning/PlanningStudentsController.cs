﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Planning.Services;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Web.Http;
using System.Linq;
using Ellucian.Colleague.Dtos.Student;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Api.Controllers.Planning
{
    /// <summary>
    /// Supplements the Students controller in the student module namespace
    /// with functionality that is only available from within the planning module namespace.
    /// </summary>
    [System.Web.Http.Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Planning)]
    public class PlanningStudentsController : BaseCompressedApiController
    {
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly IProgramEvaluationService _programEvaluationService;
        private readonly IPlanningStudentService _planningStudentService;
        private readonly ILogger _logger;

        /// <summary>
        /// PlanningStudentsController constructor
        /// </summary>
        /// <param name="adapterRegistry">Adapter of type <see cref="IAdapterRegistry">IAdapterRegistry</see></param>
        /// <param name="programEvaluationService">Program Evaluation Service of type <see cref="IProgramEvaluationService">IProgramEvaluationService</see></param>
        /// <param name="planningStudentService">Planning Student Service</param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public PlanningStudentsController(IAdapterRegistry adapterRegistry, IProgramEvaluationService programEvaluationService, IPlanningStudentService planningStudentService, ILogger logger)
        {
            _adapterRegistry = adapterRegistry;
            _programEvaluationService = programEvaluationService;
            _planningStudentService = planningStudentService;
            _logger = logger;
        }

        /// <summary>
        /// Retrieves the program evaluation results for the student's specified program.
        /// </summary>
        /// <remarks>
        /// Routing is used to expose this action under the /Students path.
        /// </remarks>
        /// <param name="id">The student's ID</param>
        /// <param name="program">The student's program code</param>
        /// <returns>The <see cref="ProgramEvaluation">Program Evaluation</see> result</returns>
        [Obsolete("Obsolete as of Colleague API 1.11, use GetEvaluation2Async instead.")]
        [ParameterSubstitutionFilter(ParameterNames = new string[] { "program" })]
        public async Task<Ellucian.Colleague.Dtos.Planning.ProgramEvaluation> GetEvaluationAsync(string id, string program)
        {
            try
            {
                // Call the coordination-layer evaluation service
                var programEvaluationEntity = (await _programEvaluationService.EvaluateAsync(id, new List<string>() { program })).First();

                // Get the adapter
                var programEvaluationDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation>();

                // use adapter to map data to DTO
                var evaluation = programEvaluationDtoAdapter.MapToType(programEvaluationEntity);
                return evaluation;
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.Message);
                throw CreateHttpResponseException("User does not have permissions to access this student.", HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves the program evaluation results of the given student against a list of programs.
        /// </summary>
        /// <remarks>
        /// Routing is used to expose this action under the /Students path.
        /// </remarks>
        /// <param name="id">The student's ID</param>
        /// <param name="programCodes">The list of programs to evaluate</param>
        /// <returns>The <see cref="ProgramEvaluation">Program Evaluation</see> result</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.Planning.ProgramEvaluation>> QueryEvaluationsAsync(string id, [FromBody] List<string> programCodes)
        {
            try
            {
                // Call the coordination-layer evaluation service
                var programEvaluationEntities = await _programEvaluationService.EvaluateAsync(id, programCodes);

                // Get the adapter
                var programEvaluationDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation>();

                // use adapter to map data to DTO
                var evaluations = new List<ProgramEvaluation>();
                foreach (var evaluation in programEvaluationEntities)
                {
                    evaluations.Add(programEvaluationDtoAdapter.MapToType(evaluation));
                }
                return evaluations;
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.Message);
                throw CreateHttpResponseException("User does not have permissions to access this student.", HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves the notice for the specified student and program. Student does not have to be currently enrolled in the program.
        /// </summary>
        /// <param name="studentId">The student's ID</param>
        /// <param name="programCode">The program code</param>
        /// <returns>List of <see cref="EvaluationNotice">Evaluation Notices</see></returns>
        [ParameterSubstitutionFilter(ParameterNames = new string[] { "programCode" })]
        public async Task<IEnumerable<EvaluationNotice>> GetEvaluationNoticesAsync(string studentId, string programCode)
        {
            try
            {
                var notices = await _programEvaluationService.GetEvaluationNoticesAsync(studentId, programCode);
                return notices;
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.Message);
                throw CreateHttpResponseException("User does not have permissions to access this student.", HttpStatusCode.Forbidden);
            }
            catch (Exception exception)
            {
                _logger.Error(exception.ToString());
                throw CreateHttpResponseException(exception.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves a student that contains only information needed for degree planning.
        /// </summary>
        /// <param name="studentId"></param>
        /// <returns>A <see cref="PlanningStudent">PlanningStudent</see> object</returns>
        public async Task<PlanningStudent> GetPlanningStudentAsync(string studentId)
        {
            try
            {
                var planningStudent = await _planningStudentService.GetPlanningStudentAsync(studentId);
                return planningStudent;
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.Message);
                throw CreateHttpResponseException("User does not have permissions to access this student.", HttpStatusCode.Forbidden);
            }
            catch (Exception exception)
            {
                _logger.Error(exception.ToString());
                throw CreateHttpResponseException(exception.Message, HttpStatusCode.BadRequest);
            }
        }


        /// <summary>
        /// Retrieves a planning student Dtos containing fewer properties than student Dtos for the specified list of student ids
        /// </summary>
        /// <param name="criteria">Planning Student Criteria</param>
        /// <returns>List of <see cref="PlanningStudent">Planning Students</see></returns>
        [HttpPost]
        public async Task<IEnumerable<PlanningStudent>> QueryPlanningStudentsAsync([FromBody]PlanningStudentCriteria criteria)
        {
            try
            {
                //this will call planning student service with ienumerable of student ids
                var planningStudents = await _planningStudentService.QueryPlanningStudentsAsync(criteria.StudentIds);
                return planningStudents;
            }
            catch (Exception exception)
            {
                _logger.Error(exception.ToString());
                throw CreateHttpResponseException(exception.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves the program evaluation results for the student's specified program.
        /// </summary>
        /// <remarks>
        /// Routing is used to expose this action under the /Students path.
        /// </remarks>
        /// <param name="id">The student's ID</param>
        /// <param name="program">The student's program code</param>
        /// <returns>The <see cref="ProgramEvaluation2">Program Evaluation</see> result</returns>
        [Obsolete("Obsolete as of Colleague API 1.13, use GetEvaluation3Async instead.")]
        [ParameterSubstitutionFilter(ParameterNames = new string[] { "program" })]
        public async Task<Ellucian.Colleague.Dtos.Planning.ProgramEvaluation2> GetEvaluation2Async(string id, string program)
        {
            try
            {
                // Call the coordination-layer evaluation service
                var programEvaluationEntity = (await _programEvaluationService.EvaluateAsync(id, new List<string>() { program })).First();

                // Get the adapter
                var programEvaluation2DtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation2>();

                // use adapter to map data to DTO
                var evaluation = programEvaluation2DtoAdapter.MapToType(programEvaluationEntity);
                return evaluation;
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.Message);
                throw CreateHttpResponseException("User does not have permissions to access this student.", HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves the program evaluation results of the given student against a list of programs.
        /// </summary>
        /// <remarks>
        /// Routing is used to expose this action under the /Students path.
        /// </remarks>
        /// <param name="id">The student's ID</param>
        /// <param name="programCodes">The list of programs to evaluate</param>
        /// <returns>The <see cref="ProgramEvaluation2">Program Evaluation</see> result</returns>
        [Obsolete("Obsolete as of Colleague API 1.13, use QueryEvaluations3Async instead.")]
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.Planning.ProgramEvaluation2>> QueryEvaluations2Async(string id, [FromBody] List<string> programCodes)
        {
            try
            {
                // Call the coordination-layer evaluation service
                var programEvaluationEntities = await _programEvaluationService.EvaluateAsync(id, programCodes);

                // Get the adapter
                var programEvaluation2DtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation2>();

                // use adapter to map data to DTO
                var evaluations = new List<ProgramEvaluation2>();
                foreach (var evaluation in programEvaluationEntities)
                {
                    evaluations.Add(programEvaluation2DtoAdapter.MapToType(evaluation));
                }
                return evaluations;
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.Message);
                throw CreateHttpResponseException("User does not have permissions to access this student.", HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }


        /// <summary>
        /// Retrieves the program evaluation results for the student's specified program.
        /// </summary>
        /// <remarks>
        /// Routing is used to expose this action under the /Students path.
        /// </remarks>
        /// <param name="id">The student's ID</param>
        /// <param name="program">The student's program code</param>
        /// <returns>The <see cref="ProgramEvaluation3">Program Evaluation</see> result</returns>
        [ParameterSubstitutionFilter(ParameterNames = new string[] { "program" })]
        public async Task<Ellucian.Colleague.Dtos.Planning.ProgramEvaluation3> GetEvaluation3Async(string id, string program)
        {
            try
            {
                // Call the coordination-layer evaluation service
                var programEvaluationEntity = (await _programEvaluationService.EvaluateAsync(id, new List<string>() { program })).First();

                // Get the adapter
                var programEvaluation3DtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation3>();

                // use adapter to map data to DTO
                var evaluation = programEvaluation3DtoAdapter.MapToType(programEvaluationEntity);
                return evaluation;
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.Message);
                throw CreateHttpResponseException("User does not have permissions to access this student.", HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves the program evaluation results of the given student against a list of programs.
        /// </summary>
        /// <remarks>
        /// Routing is used to expose this action under the /Students path.
        /// </remarks>
        /// <param name="id">The student's ID</param>
        /// <param name="programCodes">The list of programs to evaluate</param>
        /// <returns>The <see cref="ProgramEvaluation3">Program Evaluation</see> result</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.Planning.ProgramEvaluation3>> QueryEvaluations3Async(string id, [FromBody] List<string> programCodes)
        {
            try
            {
                // Call the coordination-layer evaluation service
                var programEvaluationEntities = await _programEvaluationService.EvaluateAsync(id, programCodes);

                // Get the adapter
                var programEvaluation3DtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation, Ellucian.Colleague.Dtos.Planning.ProgramEvaluation3>();

                // use adapter to map data to DTO
                var evaluations = new List<ProgramEvaluation3>();
                foreach (var evaluation in programEvaluationEntities)
                {
                    evaluations.Add(programEvaluation3DtoAdapter.MapToType(evaluation));
                }
                return evaluations;
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.Message);
                throw CreateHttpResponseException("User does not have permissions to access this student.", HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }
    }
}

