﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Planning.Services;
using Ellucian.Colleague.Domain.Planning.Exceptions;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Http.Configuration;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Api.Controllers
{
    /// <summary>
    /// Provides access to get and update Degree Plans.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Planning)]
    public class DegreePlansController : BaseCompressedApiController
    {
        private readonly IDegreePlanService _degreePlanService;
        private readonly ILogger _logger;
        private readonly ApiSettings apiSettings;

        /// <summary>
        /// DegreePlansController class constructor
        /// </summary>
        /// <param name="degreePlanService">Service of type <see cref="IDegreePlanService">IDegreePlanService</see></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        /// <param name="apiSettings"><see cref="ApiSettings"/>instance</param>
        public DegreePlansController(IDegreePlanService degreePlanService, ILogger logger, ApiSettings apiSettings)
        {
            _degreePlanService = degreePlanService;
            _logger = logger;
            this.apiSettings = apiSettings;
        }

        /// <summary>
        /// Retrieves a student's degree plan using the unique DegreePlanId.
        /// </summary>
        /// <param name="id">id of the degree plan</param>
        /// <returns>The student's <see cref="DegreePlan">Degree Plan</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to access this degree plan</exception>
        [Obsolete("Obsolete as of API version 1.3, use version 3 of this API")]
        public async Task<DegreePlan> GetAsync(int id)
        {
            try
            {
                return await _degreePlanService.GetDegreePlanAsync(id);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception)
            {
                throw CreateNotFoundException("Degree Plan", id.ToString());
            }
        }

        /// <summary>
        /// Retrieves a student's degree plan using the unique DegreePlanId.
        /// </summary>
        /// <remarks>This is the current version.</remarks>
        /// <param name="id">id of the degree plan</param>
        /// <returns>The student's <see cref="DegreePlan2">Degree Plan</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to access this degree plan</exception>
        [Obsolete("Obsolete as of API version 1.5, use version 3 of this API")]
        public async Task<DegreePlan2> Get2Async(int id)
        {
            try
            {
                return await _degreePlanService.GetDegreePlan2Async(id);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception)
            {
                throw CreateNotFoundException("Degree Plan", id.ToString());
            }
        }


        /// <summary>
        /// Retrieves a student's degree plan using the unique DegreePlanId.
        /// </summary>
        /// <remarks>This is the current version.</remarks>
        /// <param name="id">id of the degree plan</param>
        /// <returns>The student's <see cref="DegreePlan3">Degree Plan</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to access this degree plan</exception>
        [Obsolete("Obsolete as of API version 1.6, use version 4 of this API")]
        public async Task<DegreePlan3> Get3Async(int id)
        {
            try
            {
                return await _degreePlanService.GetDegreePlan3Async(id);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception)
            {
                throw CreateNotFoundException("Degree Plan", id.ToString());
            }
        }

        /// <summary>
        /// Retrieves a student's degree plan using the unique DegreePlanId. 
        /// </summary>
        /// <remarks>This is not the current version.</remarks>
        /// <param name="id">id of the degree plan</param>
        /// <param name="validate">Defaults to true. If false, returns a non-validated degree plan (use when planned course warnings are not needed to improve performance)</param>
        /// <returns>A combined dto <see cref="DegreePlanAcademicHistory">DegreePlanAcademicHistory</see> which includes the student's <see cref="DegreePlan3">Degree Plan</see> and <see cref="AcademicHistory2">AcademicHistory</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to access this degree plan</exception>
        [Obsolete("Obsolete as of API version 1.11, use Get5Async going forward")]
        public async Task<DegreePlanAcademicHistory> Get4Async(int id, bool validate = true)
        {
            try
            {
                return await _degreePlanService.GetDegreePlan4Async(id, validate);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception)
            {
                throw CreateNotFoundException("Degree Plan", id.ToString());
            }
        }

        /// <summary>
        /// Retrieves a student's degree plan using the unique DegreePlanId. 
        /// </summary>
        /// <remarks>This is the current version.</remarks>
        /// <param name="id">id of the degree plan</param>
        /// <param name="validate">Defaults to true. If false, returns a non-validated degree plan (use when planned course warnings are not needed to improve performance)</param>
        /// <returns>A combined dto <see cref="DegreePlanAcademicHistory2">DegreePlanAcademicHistory2</see> which includes the student's <see cref="DegreePlan4">Degree Plan</see> and <see cref="AcademicHistory3">AcademicHistory</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to access this degree plan</exception>
        public async Task<DegreePlanAcademicHistory2> Get5Async(int id, bool validate = true)
        {
            try
            {
                return await _degreePlanService.GetDegreePlan5Async(id, validate);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Info(ex.ToString(),"Unable to get DegreePlanAcademicHistory for plan " + id);
                throw CreateNotFoundException("DegreePlanAcademicHistory", id.ToString());
            }
        }

        /// <summary>
        /// Creates a new degree plan for the specified student.
        /// </summary>
        /// <remarks>In MVC4 RC the [FromBody] attribute is required on the studentId parameter. Without this
        /// the parameter is always mapped as null and the call will fail since by default, simple types line
        /// strings are pulled from the Uri and not the body.</remarks>
        /// <param name="studentId">The ID of the student for whom to create a new degree plan</param>
        /// <returns>An HttpResponseMessage which includes the newly created <see cref="DegreePlan">degree plan</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to create a degree plan for this student</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Conflict returned if a degree plan already exists for this student</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if invalid student id or student locked or any other creation problem.</exception>
        [Obsolete("Obsolete as of API version 1.3, use version 4 of this API")]
        public async Task<HttpResponseMessage> PostAsync([FromBody]string studentId)
        {
            try
            {
                DegreePlan newPlanDto = await _degreePlanService.CreateDegreePlanAsync(studentId);
                var response = Request.CreateResponse<DegreePlan>(HttpStatusCode.Created, newPlanDto);
                SetResourceLocationHeader("GetDegreePlan", new { id = newPlanDto.Id });
                return response;
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (ExistingDegreePlanException eex)
            {
                _logger.Info(eex.ToString());
                SetResourceLocationHeader("GetDegreePlan", new { id = eex.ExistingPlanId });
                throw CreateHttpResponseException(eex.Message, HttpStatusCode.Conflict);
            }
            catch (Exception ex)
            {
                // Student Id not provided, student Id does not exist, student is locked, etc.
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Creates a new degree plan for the specified student.
        /// </summary>
        /// <remarks>This is the current version. In MVC4 RC the [FromBody] attribute is required on the studentId parameter. Without this
        /// the parameter is always mapped as null and the call will fail since by default, simple types line
        /// strings are pulled from the Uri and not the body.</remarks>
        /// <param name="studentId">The ID of the student for whom to create a new degree plan</param>
        /// <returns>An HttpResponseMessage which includes the newly created <see cref="DegreePlan">degree plan</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to create a degree plan for this student</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Conflict returned if a degree plan already exists for this student</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if invalid student id or student locked or any other creation problem.</exception>
        [Obsolete("Obsolete as of API version 1.5, use version 4 of this API")]
        public async Task<HttpResponseMessage> Post2Async([FromBody]string studentId)
        {
            try
            {
                DegreePlan2 newPlanDto = await _degreePlanService.CreateDegreePlan2Async(studentId);
                var response = Request.CreateResponse<DegreePlan2>(HttpStatusCode.Created, newPlanDto);
                SetResourceLocationHeader("GetDegreePlan", new { id = newPlanDto.Id });
                return response;
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (ExistingDegreePlanException eex)
            {
                _logger.Info(eex.ToString());
                SetResourceLocationHeader("GetDegreePlan", new { id = eex.ExistingPlanId });
                throw CreateHttpResponseException(eex.Message, HttpStatusCode.Conflict);
            }
            catch (Exception ex)
            {
                // Student Id not provided, student Id does not exist, student is locked, etc.
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Creates a new degree plan for the specified student.
        /// </summary>
        /// <remarks>This is the current version. In MVC4 RC the [FromBody] attribute is required on the studentId parameter. Without this
        /// the parameter is always mapped as null and the call will fail since by default, simple types line
        /// strings are pulled from the Uri and not the body.</remarks>
        /// <param name="studentId">The ID of the student for whom to create a new degree plan</param>
        /// <returns>An HttpResponseMessage which includes the newly created <see cref="DegreePlan">degree plan</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to create a degree plan for this student</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Conflict returned if a degree plan already exists for this student</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if invalid student id or student locked or any other creation problem.</exception>
        [Obsolete("Obsolete as of API version 1.6, use version 4 of this API")]
        public async Task<HttpResponseMessage> Post3Async([FromBody]string studentId)
        {
            try
            {
                DegreePlan3 newPlanDto = await _degreePlanService.CreateDegreePlan3Async(studentId);
                var response = Request.CreateResponse<DegreePlan3>(HttpStatusCode.Created, newPlanDto);
                SetResourceLocationHeader("GetDegreePlan", new { id = newPlanDto.Id });
                return response;
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (ExistingDegreePlanException eex)
            {
                _logger.Info(eex.ToString());
                SetResourceLocationHeader("GetDegreePlan", new { id = eex.ExistingPlanId });
                throw CreateHttpResponseException(eex.Message, HttpStatusCode.Conflict);
            }
            catch (Exception ex)
            {
                // Student Id not provided, student Id does not exist, student is locked, etc.
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Creates a new degree plan for the specified student.
        /// </summary>
        /// <remarks>This is the current version. In MVC4 RC the [FromBody] attribute is required on the studentId parameter. Without this
        /// the parameter is always mapped as null and the call will fail since by default, simple types line
        /// strings are pulled from the Uri and not the body.</remarks>
        /// <param name="studentId">The ID of the student for whom to create a new degree plan</param>
        /// <returns>An HttpResponseMessage which includes the newly created <see cref="DegreePlan3">degree plan</see> and the <see cref="AcademicHistory2">student's AcademicHistory</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to create a degree plan for this student</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Conflict returned if a degree plan already exists for this student</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if invalid student id or student locked or any other creation problem.</exception>
        [Obsolete("Obsolete as of API version 1.11, use Post5Async instead")]
        public async Task<HttpResponseMessage> Post4Async([FromBody]string studentId)
        {
            try
            {
                DegreePlanAcademicHistory newPlanDto = await _degreePlanService.CreateDegreePlan4Async(studentId);
                var response = Request.CreateResponse<DegreePlanAcademicHistory>(HttpStatusCode.Created, newPlanDto);
                SetResourceLocationHeader("GetDegreePlan", new { id = newPlanDto.DegreePlan.Id });
                return response;
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (ExistingDegreePlanException eex)
            {
                _logger.Info(eex.ToString());
                SetResourceLocationHeader("GetDegreePlan", new { id = eex.ExistingPlanId });
                throw CreateHttpResponseException(eex.Message, HttpStatusCode.Conflict);
            }
            catch (Exception ex)
            {
                // Student Id not provided, student Id does not exist, student is locked, etc.
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Creates a new degree plan for the specified student.
        /// </summary>
        /// <remarks>This is the current version. In MVC4 RC the [FromBody] attribute is required on the studentId parameter. Without this
        /// the parameter is always mapped as null and the call will fail since by default, simple types line
        /// strings are pulled from the Uri and not the body.</remarks>
        /// <param name="studentId">The ID of the student for whom to create a new degree plan</param>
        /// <returns>An HttpResponseMessage which includes the newly created <see cref="DegreePlan4">degree plan</see> and the <see cref="AcademicHistory3">student's AcademicHistory</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to create a degree plan for this student</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Conflict returned if a degree plan already exists for this student</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if invalid student id or student locked or any other creation problem.</exception>
        public async Task<HttpResponseMessage> Post5Async([FromBody]string studentId)
        {
            try
            {
                DegreePlanAcademicHistory2 newPlanDto = await _degreePlanService.CreateDegreePlan5Async(studentId);
                var response = Request.CreateResponse<DegreePlanAcademicHistory2>(HttpStatusCode.Created, newPlanDto);
                SetResourceLocationHeader("GetDegreePlan", new { id = newPlanDto.DegreePlan.Id });
                return response;
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (ExistingDegreePlanException eex)
            {
                _logger.Info(eex.ToString());
                SetResourceLocationHeader("GetDegreePlan", new { id = eex.ExistingPlanId });
                throw CreateHttpResponseException(eex.Message, HttpStatusCode.Conflict);
            }
            catch (Exception ex)
            {
                // Student Id not provided, student Id does not exist, student is locked, etc.
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }
        /// <summary>
        /// Updates an existing degree plan.
        /// </summary>
        /// <param name="degreePlan">The degree plan in its entirety</param>
        /// <returns>The updated <see cref="DegreePlan">degree plan</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Conflict returned if version number of passed degree plan object does not match the version in the database, indicating that an update has occurred on the degree plan by another user and this action has not been saved.</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to update the degree plan</exception>
        [Obsolete("Obsolete as of API version 1.3, use version 3 of this API")]
        public async Task<DegreePlan> PutAsync(DegreePlan degreePlan)
        {
            DegreePlan returnDegreePlanDto = null;

            try
            {
                returnDegreePlanDto = await _degreePlanService.UpdateDegreePlanAsync(degreePlan);
            }
            catch (InvalidOperationException ioex)
            {
                // Version number mismatch
                _logger.Error(ioex.ToString());
                throw CreateHttpResponseException(ioex.Message, HttpStatusCode.Conflict);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Info(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
            return returnDegreePlanDto;
        }

        /// <summary>
        /// Updates an existing degree plan.
        /// </summary>
        /// <param name="degreePlan">The degree plan in its entirety (DegreePlan2)</param>
        /// <returns>The updated <see cref="DegreePlan2">degree plan</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Conflict returned if version number of passed degree plan object does not match the version in the database, indicating that an update has occurred on the degree plan by another user and this action has not been saved.</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to update the degree plan</exception>
        [Obsolete("Obsolete as of API version 1.3, use version 3 of this API")]
        public async Task<DegreePlan2> Put2Async(DegreePlan2 degreePlan)
        {
            DegreePlan2 returnDegreePlanDto = null;

            try
            {
                returnDegreePlanDto = await _degreePlanService.UpdateDegreePlan2Async(degreePlan);
            }
            catch (InvalidOperationException ioex)
            {
                // Version number mismatch
                _logger.Error(ioex.ToString());
                throw CreateHttpResponseException(ioex.Message, HttpStatusCode.Conflict);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Info(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
            return returnDegreePlanDto;
        }

        /// <summary>
        /// Updates an existing degree plan.
        /// </summary>
        /// <param name="degreePlan">The degree plan in its entirety (DegreePlan3)</param>
        /// <returns>The updated <see cref="DegreePlan3">degree plan</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Conflict returned if version number of passed degree plan object does not match the version in the database, indicating that an update has occurred on the degree plan by another user and this action has not been saved.</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to update the degree plan</exception>
        [Obsolete("Obsolete as of API version 1.6, use version 4 of this API")]
        public async Task<DegreePlan3> Put3Async(DegreePlan3 degreePlan)
        {
            DegreePlan3 returnDegreePlanDto = null;

            try
            {
                returnDegreePlanDto = await _degreePlanService.UpdateDegreePlan3Async(degreePlan);
            }
            catch (InvalidOperationException ioex)
            {
                // Version number mismatch
                _logger.Error(ioex.ToString());
                throw CreateHttpResponseException(ioex.Message, HttpStatusCode.Conflict);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Info(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
            return returnDegreePlanDto;
        }

        /// <summary>
        /// Updates an existing degree plan.
        /// </summary>
        /// <param name="degreePlan">The degree plan in its entirety (DegreePlan3)</param>
        /// <returns>The updated <see cref="DegreePlan3">degree plan</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Conflict returned if version number of passed degree plan object does not match the version in the database, indicating that an update has occurred on the degree plan by another user and this action has not been saved.</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to update the degree plan</exception>
        [Obsolete("Obsolete as of API version 1.11, use Put5Async instead")]
        public async Task<DegreePlanAcademicHistory> Put4Async(DegreePlan4 degreePlan)
        {
            DegreePlanAcademicHistory returnDto = null;

            try
            {
                returnDto = await _degreePlanService.UpdateDegreePlan4Async(degreePlan);
            }
            catch (InvalidOperationException ioex)
            {
                // Version number mismatch
                _logger.Error(ioex.ToString());
                throw CreateHttpResponseException(ioex.Message, HttpStatusCode.Conflict);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Info(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
            return returnDto;
        }

        /// <summary>
        /// Updates an existing degree plan.
        /// </summary>
        /// <param name="degreePlan">The degree plan in its entirety (DegreePlan4)</param>
        /// <returns>The updated <see cref="DegreePlanAcademicHistory2">degree plan</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Conflict returned if version number of passed degree plan object does not match the version in the database, indicating that an update has occurred on the degree plan by another user and this action has not been saved.</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to update the degree plan</exception>
        public async Task<DegreePlanAcademicHistory2> Put5Async(DegreePlan4 degreePlan)
        {
            DegreePlanAcademicHistory2 returnDto = null;

            try
            {
                returnDto = await _degreePlanService.UpdateDegreePlan5Async(degreePlan);
            }
            catch (InvalidOperationException ioex)
            {
                // Version number mismatch
                _logger.Error(ioex.ToString());
                throw CreateHttpResponseException(ioex.Message, HttpStatusCode.Conflict);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Info(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
            return returnDto;
        }

        /// <summary>
        /// Given a degree plan and a term, submit a registration request.
        /// Any sections selected on the plan should be submitted to registration
        /// </summary>
        /// <param name="degreePlanId">The degree plan, which contains the sections the student has planned and now wishes to register for </param>
        /// <param name="termId">The term for which the student is registering</param>
        /// <returns>A list of <see cref="RegistrationMessage">registration messages</see> returned from the registration system.
        /// </returns>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if either the degreePlanId or termId argument is not specified</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to update the student's degree plan</exception>
        [Obsolete("Obsolete as of API version 1.3, use version 2 of this API")]
        public async Task<IEnumerable<RegistrationMessage>> PutRegistrationAsync(int degreePlanId, string termId)
        {
            if (degreePlanId == 0)
            {
                _logger.Error("Invalid degreePlanId");
                throw CreateHttpResponseException("Invalid degreePlanId", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrEmpty(termId))
            {
                _logger.Error("Invalid termId");
                throw CreateHttpResponseException("Invalid termId", HttpStatusCode.BadRequest);
            }
            try
            {
                IEnumerable<RegistrationMessage> messages = (await _degreePlanService.RegisterAsync(degreePlanId, termId)).Messages;
                return messages;
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }

        }

        /// <summary>
        /// Given a degree plan and a term, submit a registration request.
        /// Any sections selected on the plan should be submitted to registration
        /// </summary>
        /// <param name="degreePlanId">The degree plan, which contains the sections the student has planned and now wishes to register for </param>
        /// <param name="termId">The term for which the student is registering</param>
        /// <returns>A <see cref="RegistrationResponse">response</see> returned from the registration system.
        /// </returns>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if either the degreePlanId or termId argument is not specified</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to update the student's degree plan</exception>
        [Obsolete("Obsolete as of API version 1.5, use students/{studentId}/register moving forward.")]
        public async Task<RegistrationResponse> PutRegistration2Async(int degreePlanId, string termId)
        {
            if (degreePlanId == 0)
            {
                _logger.Error("Invalid degreePlanId");
                throw CreateHttpResponseException("Invalid degreePlanId", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrEmpty(termId))
            {
                _logger.Error("Invalid termId");
                throw CreateHttpResponseException("Invalid termId", HttpStatusCode.BadRequest);
            }
            try
            {
                RegistrationResponse response = await _degreePlanService.RegisterAsync(degreePlanId, termId);
                return response;
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }

        }

        /// <summary>
        /// Takes registration action (add drop waitlist) on specified course sections.
        /// </summary>
        /// <param name="degreePlanId">Id of the degree plan for which section registration actions are being taken</param>
        /// <param name="sectionRegistrations">List of <see cref="SectionRegistration">section registration actions</see> to take</param>
        /// <returns>A list of <see cref="RegistrationMessage">registration messages</see> from the registration process indicating success or failure</returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if either the degreePlanId argument or the sectionRegistration information is not specified</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to update the degree plan</exception>
        [Obsolete("Obsolete as of API version 1.3, use version 2 of this API")]
        public async Task<IEnumerable<RegistrationMessage>> PutSectionRegistrationAsync(int degreePlanId, IEnumerable<SectionRegistration> sectionRegistrations)
        {
            if (degreePlanId == 0)
            {
                _logger.Error("Invalid degreePlanId");
                throw CreateHttpResponseException("Invalid degreePlanId", HttpStatusCode.BadRequest);
            }
            if (sectionRegistrations == null)
            {
                _logger.Error("Invalid sectionRegistration");
                throw CreateHttpResponseException("Invalid sectionRegistration", HttpStatusCode.BadRequest);
            }
            try
            {
                IEnumerable<RegistrationMessage> messages = (await _degreePlanService.RegisterSectionsAsync(degreePlanId, sectionRegistrations)).Messages;
                return messages;
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
        }

        /// <summary>
        /// Takes registration action (add drop waitlist) on specified course sections.
        /// </summary>
        /// <param name="degreePlanId">Id of the degree plan for which section registration actions are being taken</param>
        /// <param name="sectionRegistrations">List of <see cref="SectionRegistration">section registration actions</see> to take</param>
        /// <returns>A <see cref="RegistrationResponse">response</see> returned from the registration system.</returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if either the degreePlanId argument or the sectionRegistration information is not specified</exception>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to update the degree plan</exception>
        [Obsolete("Obsolete as of API version 1.5, use students/{studentId}/register moving forward.")]
        public async Task<RegistrationResponse> PutSectionRegistration2Async(int degreePlanId, IEnumerable<SectionRegistration> sectionRegistrations)
        {
            if (degreePlanId == 0)
            {
                _logger.Error("Invalid degreePlanId");
                throw CreateHttpResponseException("Invalid degreePlanId", HttpStatusCode.BadRequest);
            }
            if (sectionRegistrations == null)
            {
                _logger.Error("Invalid sectionRegistration");
                throw CreateHttpResponseException("Invalid sectionRegistration", HttpStatusCode.BadRequest);
            }
            try
            {
                RegistrationResponse response = await _degreePlanService.RegisterSectionsAsync(degreePlanId, sectionRegistrations);
                return response;
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }

        }

        /// <summary>
        /// Applies a sample degree plan to the degree plan and updates it in the database.
        /// </summary>
        /// <param name="request">The <see cref="LoadDegreePlanRequest">Load DegreePlan request</see> specifying which sample plan to load into the degree plan</param>
        /// <returns>The updated <see cref="DegreePlan">degree plan</see></returns>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.NotFound returned if a sample degree plan is not found, either for the specified program or a default sample</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Conflict returned if version number of passed degree plan object does not match the version in the database, indicating that an update has occurred on the degree plan by another user and this action has not been saved.</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to update the degree plan</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned for other errors that may occur</exception>
        [Obsolete("Obsolete as of API version 1.2, use GET degree-plans/{degreePlanId}/preview-sample and PUT degree-plans to apply a sample plan")]
        public async Task<DegreePlan> PutApplySampleAsync([FromBody]LoadDegreePlanRequest request)
        {
            DegreePlan returnDegreePlanDto;
            try
            {
                returnDegreePlanDto = await _degreePlanService.ApplySampleDegreePlanAsync(request.StudentId, request.ProgramCode);
            }
            catch (ArgumentOutOfRangeException aoure)
            {
                // no sample available for this program
                _logger.Error(aoure.ToString());
                throw CreateHttpResponseException(aoure.Message, HttpStatusCode.NotFound);
            }
            catch (InvalidOperationException ioex)
            {
                // Version number mismatch only
                _logger.Error(ioex.ToString());
                throw CreateHttpResponseException(ioex.Message, HttpStatusCode.Conflict);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                // Any other error, described in returned message
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }

            return returnDegreePlanDto;
        }

        /// <summary>
        /// Returns a student degree plan preview given a sample degree plan for the supplied program. The student's plan is unchanged in the database.
        /// </summary>
        /// <param name="degreePlanId">The degree plan id of the plan to use as the basis for the plan preview.</param>
        /// <param name="programCode">The program from which the sample plan should be derived.</param>
        /// <returns>A degree plan preview which contains both a limited preview of courses suggested along with a version of the student's <see cref="DegreePlan">DegreePlan</see> now including the overlaid sample degree plan.</returns>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.NotFound returned if a sample degree plan is not found, either for the specified program or a default sample</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if the degree plan id or programCode is not provided.</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to view the degree plan</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned for other errors that may occur</exception>
        [Obsolete("Obsolete as of API version 1.3, use version 3 of this API")]
        public async Task<DegreePlanPreview> GetSamplePlanPreviewAsync(int degreePlanId, string programCode)
        {
            DegreePlanPreview degreePlanPreviewDto;
            try
            {
                degreePlanPreviewDto = await _degreePlanService.PreviewSampleDegreePlanAsync(degreePlanId, programCode);
            }
            catch (ArgumentOutOfRangeException aoure)
            {
                // no sample available for this program
                _logger.Error(aoure.ToString());
                throw CreateHttpResponseException(aoure.Message, HttpStatusCode.NotFound);
            }
            catch (KeyNotFoundException knfex)
            {
                // no sample available for this program
                _logger.Error(knfex.ToString());
                throw CreateHttpResponseException(knfex.Message, HttpStatusCode.NotFound);
            }
            catch (ArgumentNullException anex)
            {
                // degree plan id or program code not provided.
                _logger.Error(anex.ToString());
                throw CreateHttpResponseException(anex.Message, HttpStatusCode.BadRequest);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                // Any other error, described in returned message
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }

            return degreePlanPreviewDto;
        }

        /// <summary>
        /// Returns a student degree plan preview given a sample degree plan for the supplied program. The student's plan is unchanged in the database.
        /// </summary>
        /// <param name="degreePlanId">The degree plan id of the plan to use as the basis for the plan preview.</param>
        /// <param name="programCode">The program from which the sample plan should be derived.</param>
        /// <param name="firstTermCode">Code for the term at which to start the sample plan</param>
        /// <returns>A degree plan preview which contains both a limited preview of courses suggested along with a version of the student's <see cref="DegreePlan">DegreePlan</see> now including the overlaid sample degree plan.</returns>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.NotFound returned if a sample degree plan is not found, either for the specified program or a default sample</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if the degree plan id or programCode is not provided.</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to view the degree plan</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned for other errors that may occur</exception>
        [Obsolete("Obsolete as of API version 1.5, use version 3 of this API")]
        public async Task<DegreePlanPreview2> GetSamplePlanPreview2Async(int degreePlanId, string programCode, string firstTermCode)
        {
            DegreePlanPreview2 degreePlanPreviewDto;
            try
            {
                degreePlanPreviewDto = await _degreePlanService.PreviewSampleDegreePlan2Async(degreePlanId, programCode, firstTermCode);
            }
            catch (ArgumentOutOfRangeException aoure)
            {
                // no sample available for this program
                _logger.Error(aoure.ToString());
                throw CreateHttpResponseException(aoure.Message, HttpStatusCode.NotFound);
            }
            catch (KeyNotFoundException knfex)
            {
                // no sample available for this program
                _logger.Error(knfex.ToString());
                throw CreateHttpResponseException(knfex.Message, HttpStatusCode.NotFound);
            }
            catch (ArgumentNullException anex)
            {
                // degree plan id or program code not provided.
                _logger.Error(anex.ToString());
                throw CreateHttpResponseException(anex.Message, HttpStatusCode.BadRequest);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                // Any other error, described in returned message
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }

            return degreePlanPreviewDto;
        }
        
        /// <summary>
        /// Returns a student degree plan preview given a sample degree plan for the supplied program. The student's plan is unchanged in the database.
        /// </summary>
        /// <param name="degreePlanId">The degree plan id of the plan to use as the basis for the plan preview.</param>
        /// <param name="programCode">The program from which the sample plan should be derived.</param>
        /// <param name="firstTermCode">Code for the term at which to start the sample plan</param>
        /// <returns>A degree plan preview which contains both a limited preview of courses suggested along with a version of the student's <see cref="DegreePlan">DegreePlan</see> now including the overlaid sample degree plan.</returns>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.NotFound returned if a sample degree plan is not found, either for the specified program or a default sample</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if the degree plan id or programCode is not provided.</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to view the degree plan</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned for other errors that may occur</exception>
        public async Task<DegreePlanPreview3> GetSamplePlanPreview3Async(int degreePlanId, string programCode, string firstTermCode)
        {
            DegreePlanPreview3 degreePlanPreviewDto;
            try
            {
                degreePlanPreviewDto = await _degreePlanService.PreviewSampleDegreePlan3Async(degreePlanId, programCode, firstTermCode);
            }
            catch (ArgumentOutOfRangeException aoure)
            {
                // no sample available for this program
                _logger.Error(aoure.ToString());
                throw CreateHttpResponseException(aoure.Message, HttpStatusCode.NotFound);
            }
            catch (KeyNotFoundException knfex)
            {
                // no sample available for this program
                _logger.Error(knfex.ToString());
                throw CreateHttpResponseException(knfex.Message, HttpStatusCode.NotFound);
            }
            catch (ArgumentNullException anex)
            {
                // degree plan id or program code not provided.
                _logger.Error(anex.ToString());
                throw CreateHttpResponseException(anex.Message, HttpStatusCode.BadRequest);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                // Any other error, described in returned message
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }

            return degreePlanPreviewDto;
        }

        /// <summary>
        /// Returns a student degree plan preview given a sample degree plan for the supplied program. The student's plan is unchanged in the database.
        /// </summary>
        /// <param name="degreePlanId">The degree plan id of the plan to use as the basis for the plan preview.</param>
        /// <param name="programCode">The program from which the sample plan should be derived.</param>
        /// <param name="firstTermCode">Code for the term at which to start the sample plan</param>
        /// <returns>A degree plan preview which contains both a limited preview of courses suggested along with a version of the student's <see cref="DegreePlan">DegreePlan</see> now including the overlaid sample degree plan.</returns>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.NotFound returned if a sample degree plan is not found, either for the specified program or a default sample</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if the degree plan id or programCode is not provided.</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to view the degree plan</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned for other errors that may occur</exception>
        public async Task<DegreePlanPreview4> GetSamplePlanPreview4Async(int degreePlanId, string programCode, string firstTermCode)
        {
            DegreePlanPreview4 degreePlanPreviewDto;
            try
            {
                degreePlanPreviewDto = await _degreePlanService.PreviewSampleDegreePlan4Async(degreePlanId, programCode, firstTermCode);
            }
            catch (ArgumentOutOfRangeException aoure)
            {
                // no sample available for this program
                _logger.Error(aoure.ToString());
                throw CreateHttpResponseException(aoure.Message, HttpStatusCode.NotFound);
            }
            catch (KeyNotFoundException knfex)
            {
                // no sample available for this program
                _logger.Error(knfex.ToString());
                throw CreateHttpResponseException(knfex.Message, HttpStatusCode.NotFound);
            }
            catch (ArgumentNullException anex)
            {
                // degree plan id or program code not provided.
                _logger.Error(anex.ToString());
                throw CreateHttpResponseException(anex.Message, HttpStatusCode.BadRequest);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                // Any other error, described in returned message
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }

            return degreePlanPreviewDto;
        }

        /// <summary>
        /// Returns a student degree plan preview given a sample degree plan for the supplied program. The student's plan is unchanged in the database.
        /// </summary>
        /// <param name="degreePlanId">The degree plan id of the plan to use as the basis for the plan preview.</param>
        /// <param name="programCode">The program from which the sample plan should be derived.</param>
        /// <param name="firstTermCode">Code for the term at which to start the sample plan</param>
        /// <returns>A degree plan preview which contains both a limited preview of courses suggested along with a version of the student's <see cref="DegreePlan4">DegreePlan</see> now including the overlaid sample degree plan.</returns>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.NotFound returned if a sample degree plan is not found, either for the specified program or a default sample</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if the degree plan id or programCode is not provided.</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if the user does not have the role or permissions required to view the degree plan</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned for other errors that may occur</exception>
        public async Task<DegreePlanPreview5> GetSamplePlanPreview5Async(int degreePlanId, string programCode, string firstTermCode)
        {
            DegreePlanPreview5 degreePlanPreviewDto;
            try
            {
                degreePlanPreviewDto = await _degreePlanService.PreviewSampleDegreePlan5Async(degreePlanId, programCode, firstTermCode);
            }
            catch (ArgumentOutOfRangeException aoure)
            {
                // no sample available for this program
                _logger.Error(aoure.ToString());
                throw CreateHttpResponseException(aoure.Message, HttpStatusCode.NotFound);
            }
            catch (KeyNotFoundException knfex)
            {
                // no sample available for this program
                _logger.Error(knfex.ToString());
                throw CreateHttpResponseException(knfex.Message, HttpStatusCode.NotFound);
            }
            catch (ArgumentNullException anex)
            {
                // degree plan id or program code not provided.
                _logger.Error(anex.ToString());
                throw CreateHttpResponseException(anex.Message, HttpStatusCode.BadRequest);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                // Any other error, described in returned message
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }

            return degreePlanPreviewDto;
        }

        /// <summary>
        /// Create an archive (snap-shot) of the degree plan
        /// </summary>
        /// <param name="degreePlan">The degree plan to be archived</param>
        /// <returns>The updated degree plan</returns>
        [Obsolete("Obsolete as of API version 1.5, use version 2 of this API")]
        public async Task<DegreePlanArchive> PostArchiveAsync(DegreePlan2 degreePlan)
        {
            try
            {
                if (degreePlan == null)
                {
                    throw new ArgumentNullException("degreePlan", "You must specify a degree plan id to retrieve archives.");
                }
                var degreePlanArchive = await _degreePlanService.ArchiveDegreePlanAsync(degreePlan);
                return degreePlanArchive;
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception exception)
            {
                _logger.Error(exception.ToString());
                throw CreateHttpResponseException(exception.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Create an archive (snap-shot) of the degree plan
        /// </summary>
        /// <param name="degreePlan">The degree plan to be archived</param>
        /// <returns>The <see cref="DegreePlanArchive2">degree plan archive</see></returns>
        [Obsolete("Obsolete as of API version 1.7, use version 3 of this API")]
        public async Task<DegreePlanArchive2> PostArchive2Async(DegreePlan3 degreePlan)
        {
            try
            {
                if (degreePlan == null)
                {
                    throw new ArgumentNullException("degreePlan", "You must specify a degree plan id to retrieve archives.");
                }
                var degreePlanArchive = await _degreePlanService.ArchiveDegreePlan2Async(degreePlan);
                return degreePlanArchive;
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception exception)
            {
                _logger.Error(exception.ToString());
                throw CreateHttpResponseException(exception.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Create an archive (snap-shot) of the degree plan
        /// </summary>
        /// <param name="degreePlan">The degree plan to be archived</param>
        /// <returns>The <see cref="DegreePlanArchive2">degree plan archive</see></returns>
        public async Task<DegreePlanArchive2> PostArchive3Async(DegreePlan4 degreePlan)
        {
            try
            {
                if (degreePlan == null)
                {
                    throw new ArgumentNullException("degreePlan", "You must specify a degree plan id to retrieve archives.");
                }
                var degreePlanArchive = await _degreePlanService.ArchiveDegreePlan3Async(degreePlan);
                return degreePlanArchive;
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception exception)
            {
                _logger.Error(exception.ToString());
                throw CreateHttpResponseException(exception.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves a list of all archives for a particular degree plan.
        /// </summary>
        /// <param name="degreePlanId">The degree plan for which the archives are requested.</param>
        /// <returns>All degree plan archives that have been created for the specified degree plan. The list may not contain any items.</returns>
        [Obsolete("Obsolete as of API version 1.5, use version 2 of this API")]
        public async Task<IEnumerable<DegreePlanArchive>> GetDegreePlanArchivesAsync(int degreePlanId)
        {
            try
            {
                if (degreePlanId == 0)
                {
                    throw new ArgumentNullException("degreePlanId");
                }
                var degreePlanArchive = await _degreePlanService.GetDegreePlanArchivesAsync(degreePlanId);

                return degreePlanArchive;
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception exception)
            {
                _logger.Error(exception.ToString());
                throw CreateHttpResponseException(exception.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves a list of all archives for a particular degree plan.
        /// </summary>
        /// <param name="degreePlanId">The degree plan for which the archives are requested.</param>
        /// <returns>All <see cref="DegreePlanArchive2">degree plan archives</see> that have been created for the specified degree plan. An empty list may be returned.</returns>
        public async Task<IEnumerable<DegreePlanArchive2>> GetDegreePlanArchives2Async(int degreePlanId)
        {
            try
            {
                if (degreePlanId == 0)
                {
                    throw new ArgumentNullException("degreePlanId");
                }
                var degreePlanArchive = await _degreePlanService.GetDegreePlanArchives2Async(degreePlanId);

                return degreePlanArchive;
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception exception)
            {
                _logger.Error(exception.ToString());
                throw CreateHttpResponseException(exception.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Given a plan archive ID, return the pdf report for that archive.
        /// </summary>
        /// <param name="id">The system id for the plan archive object being requested</param>
        /// <returns>The pdf report based on the requested archive.</returns>
        public async Task<HttpResponseMessage> GetPlanArchiveAsync(int id)
        {
            try
            {
                if (Request.Headers.Accept.Where(rqa => rqa.MediaType == "application/pdf").Count() > 0)
                {
                    var path = HttpContext.Current.Server.MapPath("~/Reports/Planning/DegreePlanArchive.rdlc");
                    var reportLogoPath = !string.IsNullOrEmpty(apiSettings.ReportLogoPath) ? apiSettings.ReportLogoPath : "";
                    if (!reportLogoPath.StartsWith("~"))
                    {
                        reportLogoPath = "~" + reportLogoPath;
                    }
                    reportLogoPath = HttpContext.Current.Server.MapPath(reportLogoPath);
                    // Get the archive report object (this contains the archived plan data, which gets fed into the report object)
                    var archiveReport = await _degreePlanService.GetDegreePlanArchiveReportAsync(id);
                    var renderedBytes = _degreePlanService.GenerateDegreePlanArchiveReport(archiveReport, path, reportLogoPath);

                    // Create the http response object, use the byte array for the response content, and set header content type to the mime type of the report
                    var response = new HttpResponseMessage();
                    response.Content = new ByteArrayContent(renderedBytes);

                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = Regex.Replace(
                            (archiveReport.StudentLastName +
                            " " + archiveReport.StudentFirstName + 
                            " " + archiveReport.StudentId + 
                            " " + archiveReport.ArchivedOn.DateTime.ToShortDateString() +
                            " " + archiveReport.ArchivedOn.DateTime.ToShortTimeString()),
                            "[^a-zA-Z0-9_]", "_")
                            + ".pdf"
                    };
                    response.Content.Headers.ContentLength = renderedBytes.Length;
                    return response;
                }
                // If the request didn't specify pdf, it's an unsupported request
                else
                {
                    throw new NotSupportedException();
                }
            }
            catch (NotSupportedException)
            {
                throw CreateHttpResponseException("Only application/pdf is served from this endpoint", HttpStatusCode.NotAcceptable);
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (KeyNotFoundException)
            {
                throw CreateNotFoundException("Degree Plan Archive", id.ToString());
            }
            catch (Exception e)
            {
                _logger.Error(e, e.Message);
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }
    }
}
