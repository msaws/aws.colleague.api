﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Planning.Services;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;
using System.Threading.Tasks;
using System.Web;

namespace Ellucian.Colleague.Api.Controllers
{
    /// <summary>
    /// AdvisorsController
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Planning)]
    public class AdvisorsController : BaseCompressedApiController
    {
        private readonly IAdvisorService _advisorService;
        private readonly ILogger _logger;

        /// <summary>
        /// AdvisorsController constructor
        /// </summary>
        /// <param name="advisorService">Service of type <see cref="IAdvisorService">IAdvisorService</see></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public AdvisorsController(IAdvisorService advisorService, ILogger logger)
        {
            _advisorService = advisorService;
            _logger = logger;
        }

        /// <summary>
        /// Retrieves basic advisor information, such as advisor name. 
        /// This is intended to retrieve merely reference information for a person who may have currently or previously performed the functions of an advisor. 
        /// Use for name identification only. Does not confirm authorization to perform as an advisor.
        /// </summary>
        /// <param name="id">Id of the advisor to retrieve</param>
        /// <returns>An <see cref="Advisor">Advisor</see> object containing advisor name</returns>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden exception returned if user does not have role and permission to access this advisor's data. No special role is needed to get this basic information.</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest exception returned for any other unexpected error.</exception>
        public async Task<Advisor> GetAdvisorAsync(string id)
        {
            try
            {
                return await _advisorService.GetAdvisorAsync(id);
            }
            catch (PermissionsException ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves basic advisor information for a the given Advisor query criteria (list of advisor ids). 
        /// Use for name identification only. Does not confirm authorization to perform as an advisor.
        /// This is intended to retrieve merely reference information (name, email) for any person who may have currently 
        /// or previously performed the functions of an advisor. If a specified ID not found to be a potential advisor,
        /// does not cause an exception, item is simply not returned in the list.
        /// </summary>
        /// <param name="advisorQueryCriteria">Criteria of the advisors to retrieve</param>
        /// <returns>A list of <see cref="Advisor">Advisors</see> object containing advisor name</returns>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden exception returned if user does not have role and permission to access this endpoint. Advisor permissions required.</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest exception returned for any other unexpected error.</exception>
        [HttpPost]
        public async Task<IEnumerable<Advisor>> QueryAdvisorsByPostAsync([FromBody] AdvisorQueryCriteria advisorQueryCriteria)
        {
            try
            {
                return await _advisorService.GetAdvisorsAsync(advisorQueryCriteria);
            }
            catch (PermissionsException ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves a list of advisees for this advisor. Secured to advisor "ViewAssignedAdvisee" permission.
        /// </summary>
        /// <param name="advisorId">id of advisor</param>
        /// <param name="pageIndex">Index of page to return</param>
        /// <param name="pageSize">Number of records per page</param>
        /// <returns>A list of <see cref="Advisee">Advisees</see> including associated program and approval request status for each advisee. Advisee privacy is enforced 
        ///  by this response. If any advisee has an assigned privacy code that the advisor is not authorized to access, the Advisee response object is returned with a
        /// X-Content-Restricted header with a value of "partial" to indicate only partial information is returned for some subset of advisees. In this situation, 
        /// all details except the advisee name are cleared from the specific advisee object.</returns>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden exception returned if user does not have role and permission to access this advisee data.</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest exception returned for any other unexpected error.</exception>
        public async Task<IEnumerable<Advisee>> GetAdvisees2Async(string advisorId, int pageSize = int.MaxValue, int pageIndex = 1)
        {
            _logger.Info("Entering GetAdviseesAsync");
            var watch = new Stopwatch();
            watch.Start();

            try
            {
                var privacyWrapper = await _advisorService.GetAdviseesAsync(advisorId, pageSize, pageIndex);
                var advisees = privacyWrapper.Dto as List<Advisee>;
                if (privacyWrapper.HasPrivacyRestrictions)
                {
                    HttpContext.Current.Response.AppendHeader("X-Content-Restricted", "partial");
                }

                watch.Stop();
                _logger.Info("GetAdviseesAsync... completed in " + watch.ElapsedMilliseconds.ToString());

                return advisees;
            }
            catch (PermissionsException ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves an advisee for this advisor if advisor has required permission.
        /// </summary>
        /// <param name="advisorId">id of advisor</param>
        /// <param name="adviseeId">id of the advisee requested</param>
        /// <returns>An <see cref="Advisee">Advisee</see> including associated program and approval request status. Advisee privacy is enforced by this 
        /// response. If an advisee has an assigned privacy code that the advisor is not authorized to access, the Advisee response object is returned with a
        /// X-Content-Restricted header with a value of "partial" to indicate only partial information is returned. In this situation, all details except 
        /// the advisee name are cleared from the response object.</returns>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden exception returned if user does not have role and permission to access this advisee data.</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest exception returned for any other unexpected error.</exception>
        public async Task<Advisee> GetAdvisee2Async(string advisorId, string adviseeId)
        {
            try
            {
                var privacyWrapper = await _advisorService.GetAdviseeAsync(advisorId, adviseeId);
                var advisee = privacyWrapper.Dto as Advisee;
                if (privacyWrapper.HasPrivacyRestrictions)
                {
                    HttpContext.Current.Response.AppendHeader("X-Content-Restricted", "partial");
                }
                return advisee;
            }
            catch (PermissionsException ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.Forbidden);
            }
            catch (KeyNotFoundException kex)
            {
                _logger.Error(kex.ToString());
                throw CreateNotFoundException("Advisee", adviseeId.ToString());
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves permissions for the current user to determine which functions the user is allowed, such as ability to advise all students or only their own advisees, or whether advisor can just view or may also update a advisee's plan.
        /// </summary>
        /// <returns>List of strings representing the permissions of this user</returns>
        public async Task<IEnumerable<string>> GetPermissionsAsync()
        {
            return await _advisorService.GetAdvisorPermissionsAsync();
        }

    }
}

