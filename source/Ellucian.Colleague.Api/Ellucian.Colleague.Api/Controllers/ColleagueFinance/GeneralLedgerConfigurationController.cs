﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;
using System;
using System.ComponentModel;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.ColleagueFinance
{
    /// <summary>
    /// General Ledger Configuration controller.
    /// </summary>
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.ColleagueFinance)]
    [Authorize]
    public class GeneralLedgerConfigurationController : BaseCompressedApiController
    {
        private readonly IGeneralLedgerConfigurationService glConfigurationService;
        private readonly ILogger logger;

        /// <summary>
        /// This constructor initializes the General Ledger Configuration controller.
        /// </summary>
        /// <param name="glConfigurationService">General Ledger Configuration service object.</param>
        /// <param name="logger">Logger object.</param>
        public GeneralLedgerConfigurationController(IGeneralLedgerConfigurationService glConfigurationService, ILogger logger)
        {
            this.glConfigurationService = glConfigurationService;
            this.logger = logger;
        }

        /// <summary>
        /// Returns the General Ledger configuration.
        /// </summary>
        /// <returns>The General Ledger configuration.</returns>
        public async Task<GeneralLedgerConfiguration> GetGeneralLedgerConfigurationAsync()
        {
            try
            {
                // Call the service method to obtain necessary GL configuration parameters.
                var glConfiguration = await glConfigurationService.GetGeneralLedgerConfigurationAsync();
                return glConfiguration;
            }
            catch (ConfigurationException ce)
            {
                logger.Error(ce, ce.Message);
                throw CreateHttpResponseException("Invalid configuration.", HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw CreateHttpResponseException();
            }
        }
    }
}
