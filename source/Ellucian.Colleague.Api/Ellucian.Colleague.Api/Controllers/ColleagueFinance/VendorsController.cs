﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Web.Http.Controllers;
using System.Web.Http;
using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using slf4net;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Dtos;
using Ellucian.Colleague.Dtos.EnumProperties;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http;
using Newtonsoft.Json;

namespace Ellucian.Colleague.Api.Controllers.ColleagueFinance
{
    /// <summary>
    /// Provides access to Vendors
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof (EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.ColleagueFinance)]
    public class VendorsController : BaseCompressedApiController
    {
        private readonly IVendorsService _vendorsService;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the VendorsController class.
        /// </summary>
        /// <param name="vendorsService">Service of type <see cref="IVendorsService">IVendorsService</see></param>
        /// <param name="logger">Interface to logger</param>
        public VendorsController(IVendorsService vendorsService, ILogger logger)
        {
            _vendorsService = vendorsService;
            _logger = logger;
        }

        /// <summary>
        /// Return all vendors
        /// </summary>
        /// <param name="page">API paging info for used to Offset and limit the amount of data being returned.</param>
        /// <param name="criteria">The default named query implementation for filtering</param>
        /// <returns>List of Vendors <see cref="Dtos.Vendors"/> objects representing matching vendors</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetVendorsAsync(Paging page, [FromUri] string criteria = "")
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                string vendorDetail = string.Empty, classifications = string.Empty, status = string.Empty;

                if (!string.IsNullOrEmpty(criteria))
                {
                    IDictionary<string, string> criteriaValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(criteria);

                    foreach (var value in criteriaValues)
                    {
                        switch (value.Key.ToLower())
                        {
                            case "vendordetail":
                                vendorDetail = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value.ToString();
                                break;
                            case "classifications":
                                classifications = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value.ToString();
                                break;
                            case "status":
                                status = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value.ToString();
                                break;
                        }
                    }
                }

                AddDataPrivacyContextProperty((await _vendorsService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());

                var pageOfItems = await _vendorsService.GetVendorsAsync(page.Offset, page.Limit, vendorDetail, classifications, status, bypassCache);
                return new PagedHttpActionResult<IEnumerable<Dtos.Vendors>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Read (GET) a vendor using a GUID
        /// </summary>
        /// <param name="guid">GUID to desired vendor</param>
        /// <returns>A vendor object <see cref="Dtos.Vendors"/> in EEDM format</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.Vendors> GetVendorsByGuidAsync(string guid)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }

            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _vendorsService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _vendorsService.GetVendorsByGuidAsync(guid);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new vendor
        /// </summary>
        /// <param name="vendor">DTO of the new vendor</param>
        /// <returns>A vendor object <see cref="Dtos.Vendors"/> in EEDM format</returns>
        [HttpPost]
        public async Task<Dtos.Vendors> PostVendorsAsync([FromBody] Dtos.Vendors vendor)
        {
            if (vendor == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null vendor argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }
            try
            {
                ValidateVendor(vendor);

                var vendorDetail = vendor.VendorDetail;

                if ((vendorDetail.Institution != null) && ((string.IsNullOrEmpty(vendorDetail.Institution.Id))
                     || (string.Equals(vendorDetail.Institution.Id, Guid.Empty.ToString()))))
                {
                    throw new ArgumentNullException("Vendor.VendorDetail.Institution", "The institution id is required when submitting a vendorDetail institution. ");
                }
                if ((vendorDetail.Organization != null) && ((string.IsNullOrEmpty(vendorDetail.Organization.Id))
                     || (string.Equals(vendorDetail.Organization.Id, Guid.Empty.ToString()))))
                {
                    throw new ArgumentNullException("Vendor.VendorDetail.Organization", "The organization id is required when submitting a vendorDetail organization. ");
                }
                if ((vendorDetail.Person != null) && ((string.IsNullOrEmpty(vendorDetail.Person.Id))
                    || (string.Equals(vendorDetail.Person.Id, Guid.Empty.ToString()))))
                {
                    throw new ArgumentNullException("Vendor.VendorDetail.Person", "The person id is required when submitting a vendorDetail person. ");
                }

                return await _vendorsService.PostVendorAsync(vendor);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Update (PUT) an existing vendor
        /// </summary>
        /// <param name="guid">GUID of the vendor to update</param>
        /// <param name="vendor">DTO of the updated vendor</param>
        /// <returns>A vendor object <see cref="Dtos.Vendors"/> in EEDM format</returns>
        [HttpPut]
        public async Task<Dtos.Vendors> PutVendorsAsync([FromUri] string guid, [FromBody] Dtos.Vendors vendor)
        {
            ValidateUpdateRequest(guid, vendor);

            try
            {
                ValidateVendor(vendor);
                await _vendorsService.DoesUpdateViolateDataPrivacySettings(GetRouteResourceName(), vendor);
                return await _vendorsService.PutVendorAsync(guid, vendor);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Delete (DELETE) a vendor
        /// </summary>
        /// <param name="guid">GUID to desired vendor</param>
        [HttpDelete]
        public async Task DeleteVendorsAsync(string guid)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Validate the request on Put meets conditions for guid consistency 
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="request"></param>
        private void ValidateUpdateRequest(string guid, BaseModel2 request)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            if (request == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }
            if (string.IsNullOrEmpty(request.Id))
            {
                request.Id = guid.ToLowerInvariant();
            }
            else if ((string.Equals(guid, Guid.Empty.ToString())) || (string.Equals(request.Id, Guid.Empty.ToString())))
            {
                throw CreateHttpResponseException(new IntegrationApiException("GUID empty",
                    IntegrationApiUtility.GetDefaultApiError("GUID must be specified.")));
            }
            else if (guid.ToLowerInvariant() != request.Id.ToLowerInvariant())
            {
                throw CreateHttpResponseException(new IntegrationApiException("GUID mismatch",
                    IntegrationApiUtility.GetDefaultApiError("GUID not the same as in request body.")));
            }
        }

        /// <summary>
        /// Helper method to validate vendors PUT/POST.
        /// </summary>
        /// <param name="vendor">Vendors DTO object of type <see cref="Dtos.Vendors"/></param>

        private void ValidateVendor(Vendors vendor)
        {
            if (vendor == null)
            {
                throw new ArgumentNullException("Vendor", "The id is required when submitting a vendor. ");
            }

            if (vendor.EndOn != null)
            {
                throw new ArgumentNullException("Vendor.EndOn", "The endOn date can not be updated when submitting a vendor. ");
            }

            if (vendor.VendorDetail == null)
            {
                throw new ArgumentNullException("Vendor.VendorDetail", "The vendorDetail is required when submitting a vendor. ");
            }
            
            var vendorDetail = vendor.VendorDetail;
            if ((vendorDetail.Institution == null) && (vendorDetail.Organization == null) && (vendorDetail.Person == null))
            {
                throw new ArgumentNullException("Vendor.VendorDetail", "Either a Institution, Organizatation, or Person is required when submitting a vendorDetail. ");
            }

            if ((vendorDetail.Organization != null) && ((vendorDetail.Person != null) || (vendorDetail.Institution != null)))
            {
                throw new ArgumentNullException("Vendor.VendorDetail", "Only one of either an organization, person or institution can be specified as a vendor. ");
            }
            if ((vendorDetail.Person != null) && ((vendorDetail.Organization != null) || (vendorDetail.Institution != null)))
            {
                throw new ArgumentNullException("Vendor.VendorDetail", "Only one of either an organization, person or institution can be specified as a vendor. ");
            }
            if ((vendorDetail.Institution != null) && ((vendorDetail.Person != null) || (vendorDetail.Organization != null)))
            {
                throw new ArgumentNullException("Vendor.VendorDetail", "Only one of either an organization, person or institution can be specified as a vendor. ");
            }
           
            
            if (vendor.Classifications != null)
            {
                foreach (var classification in vendor.Classifications)
                {
                    if ( string.IsNullOrEmpty(classification.Id))
                        throw new ArgumentNullException("Vendor.Classification", "The classification id is required when submitting classifications. ");
                }
            }

            if (vendor.PaymentTerms!= null)
            {
                foreach (var paymentTerm in vendor.PaymentTerms)
                {
                    if (string.IsNullOrEmpty(paymentTerm.Id))
                        throw new ArgumentNullException("Vendor.PaymentTerms", "The paymentTerms id is required when submitting paymentTerms. ");
                }
            }

            if (vendor.VendorHoldReasons != null)
            {
                foreach (var vendorHoldReason in vendor.VendorHoldReasons)
                {
                    if (string.IsNullOrEmpty(vendorHoldReason.Id))
                    {
                        throw new ArgumentNullException("Vendor.VendorHoldReasons", "The vendorHoldReason id is required when submitting vendorHoldReasons. ");
                    }
                }
            }

        }
    }
}