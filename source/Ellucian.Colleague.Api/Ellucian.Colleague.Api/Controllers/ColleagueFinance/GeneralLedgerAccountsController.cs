﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;

namespace Ellucian.Colleague.Api.Controllers.ColleagueFinance
{
    /// <summary>
    /// Provides access to general ledger objects.
    /// </summary>
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.ColleagueFinance)]
    [Authorize]
    public class GeneralLedgerAccountsController : BaseCompressedApiController
    {
        private readonly IGeneralLedgerAccountService generalLedgerAccountService;
        private readonly ILogger logger;

        /// <summary>
        /// This constructor initializes the GL account controller.
        /// </summary>
        /// <param name="generalLedgerAccountService">General ledger account service object</param>
        /// <param name="logger">Logger object</param>
        public GeneralLedgerAccountsController(IGeneralLedgerAccountService generalLedgerAccountService, ILogger logger)
        {
            this.generalLedgerAccountService = generalLedgerAccountService;
            this.logger = logger;
        }

        /// <summary>
        /// Retrieves a single general ledger object using the supplied GL account ID.
        /// </summary>
        /// <param name="generalLedgerAccountId">General ledger account ID</param>
        /// <returns>General ledger account DTO</returns>
        public async Task<GeneralLedgerAccount> GetAsync(string generalLedgerAccountId)
        {
            try
            {
                return await generalLedgerAccountService.GetAsync(generalLedgerAccountId);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw CreateHttpResponseException();
            }
        }
    }
}