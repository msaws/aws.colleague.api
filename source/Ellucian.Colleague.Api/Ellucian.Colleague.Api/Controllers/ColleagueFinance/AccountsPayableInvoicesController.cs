﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Web.Http.Controllers;
using System.Web.Http;
using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using slf4net;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Dtos.EnumProperties;
using Ellucian.Web.Http;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http.Models;

namespace Ellucian.Colleague.Api.Controllers.ColleagueFinance
{
    /// <summary>
    /// Provides access to AccountsPayableInvoices
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.ColleagueFinance)]
    public class AccountsPayableInvoicesController : BaseCompressedApiController
    {
        private readonly IAccountsPayableInvoicesService _accountsPayableInvoicesService;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the AccountsPayableInvoicesController class.
        /// </summary>
        /// <param name="accountsPayableInvoicesService">Service of type <see cref="IAccountsPayableInvoicesService">IAccountsPayableInvoicesService</see></param>
        /// <param name="logger">Interface to logger</param>
        public AccountsPayableInvoicesController(IAccountsPayableInvoicesService accountsPayableInvoicesService, ILogger logger)
        {
            _accountsPayableInvoicesService = accountsPayableInvoicesService;
            _logger = logger;
        }

        /// <summary>
        /// Return all accountsPayableInvoices
        /// </summary>
        /// <returns>List of AccountsPayableInvoices <see cref="Dtos.AccountsPayableInvoices"/> objects representing matching accountsPayableInvoices</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetAccountsPayableInvoicesAsync(Paging page)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                AddDataPrivacyContextProperty((await _accountsPayableInvoicesService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await _accountsPayableInvoicesService.GetAccountsPayableInvoicesAsync(page.Offset, page.Limit, bypassCache);
                return new PagedHttpActionResult<IEnumerable<Dtos.AccountsPayableInvoices>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }


        /// <summary>
        /// Read (GET) a accountsPayableInvoices using a GUID
        /// </summary>
        /// <param name="guid">GUID to desired accountsPayableInvoices</param>
        /// <returns>A accountsPayableInvoices object <see cref="Dtos.AccountsPayableInvoices"/> in EEDM format</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.AccountsPayableInvoices> GetAccountsPayableInvoicesByGuidAsync(string guid)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }

            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _accountsPayableInvoicesService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _accountsPayableInvoicesService.GetAccountsPayableInvoicesByGuidAsync(guid);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new accountsPayableInvoices
        /// </summary>
        /// <param name="accountsPayableInvoices">DTO of the new accountsPayableInvoices</param>
        /// <returns>A accountsPayableInvoices object <see cref="Dtos.AccountsPayableInvoices"/> in EEDM format</returns>
        [HttpPost]
        public async Task<Dtos.AccountsPayableInvoices> PostAccountsPayableInvoicesAsync([FromBody] Dtos.AccountsPayableInvoices accountsPayableInvoices)
        {
            if (accountsPayableInvoices == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null accountsPayableInvoices argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }
            try
            {
                ValidateAccountsPayableInvoices(accountsPayableInvoices);

                if ((accountsPayableInvoices.TransactionDate != DateTime.MinValue) && (DateTime.Compare(accountsPayableInvoices.TransactionDate, DateTime.Now.Date) < 0))
                {
                    throw new ArgumentNullException("accountsPayableInvoices.TransactionDate", "The transactionDate can not be prior to the current date when submitting an accounts payable invoice. ");
                }
                
                if ((accountsPayableInvoices.TransactionDate == DateTime.MinValue))
                {
                    throw new ArgumentNullException("accountsPayableInvoices.TransactionDate", "The transactionDate is a required field");
                }

                if (accountsPayableInvoices.VoidDate != null)
                {
                    throw new ArgumentNullException("accountsPayableInvoices.VoidDate", "Cannot have a void date on a POST");
                }

                return await _accountsPayableInvoicesService.PostAccountsPayableInvoicesAsync(accountsPayableInvoices);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                if (e.Errors == null || e.Errors.Count() <= 0)
                {
                    throw CreateHttpResponseException(e.Message);
                }
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Update (PUT) an existing accountsPayableInvoices
        /// </summary>
        /// <param name="guid">GUID of the accountsPayableInvoices to update</param>
        /// <param name="accountsPayableInvoices">DTO of the updated accountsPayableInvoices</param>
        /// <returns>A accountsPayableInvoices object <see cref="Dtos.AccountsPayableInvoices"/> in EEDM format</returns>
        [HttpPut]
        public async Task<Dtos.AccountsPayableInvoices> PutAccountsPayableInvoicesAsync([FromUri] string guid, [FromBody] Dtos.AccountsPayableInvoices accountsPayableInvoices)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            if (accountsPayableInvoices == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null accountsPayableInvoices argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }
            if (string.IsNullOrEmpty(accountsPayableInvoices.Id))
            {
                accountsPayableInvoices.Id = guid.ToLowerInvariant();
            }
            else if ((string.Equals(guid, Guid.Empty.ToString())) || (string.Equals(accountsPayableInvoices.Id, Guid.Empty.ToString())))
            {
                throw CreateHttpResponseException(new IntegrationApiException("GUID empty",
                    IntegrationApiUtility.GetDefaultApiError("GUID must be specified.")));
            }
            else if (guid.ToLowerInvariant() != accountsPayableInvoices.Id.ToLowerInvariant())
            {
                throw CreateHttpResponseException(new IntegrationApiException("GUID mismatch",
                    IntegrationApiUtility.GetDefaultApiError("GUID not the same as in request body.")));
            }

            try
            {
                await _accountsPayableInvoicesService.DoesUpdateViolateDataPrivacySettings(GetRouteResourceName(), accountsPayableInvoices);

                ValidateAccountsPayableInvoices(accountsPayableInvoices);

                return await _accountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, accountsPayableInvoices);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                if (e.Errors == null || e.Errors.Count() <= 0)
                {
                    throw CreateHttpResponseException(e.Message);
                }
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }

        }

        /// <summary>
        /// Delete (DELETE) a accountsPayableInvoices
        /// </summary>
        /// <param name="guid">GUID to desired accountsPayableInvoices</param>
        [HttpDelete]
        public async Task DeleteAccountsPayableInvoicesAsync(string guid)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Helper method to validate Accounts Payable Invoices PUT/POST.
        /// </summary>
        /// <param name="accountsPayableInvoices">accounts Payable Invoices DTO object of type <see cref="Dtos.AccountsPayableInvoices"/></param>
        private void ValidateAccountsPayableInvoices(Dtos.AccountsPayableInvoices accountsPayableInvoices)
        {
            var defaultCurrency = new CurrencyIsoCode?();
            if (accountsPayableInvoices.Vendor == null)
            {
                throw new ArgumentNullException("accountsPayableInvoices.Vendor", "The vendor is required when submitting an accounts payable invoice. ");
            }
            if (accountsPayableInvoices.Vendor != null && string.IsNullOrEmpty(accountsPayableInvoices.Vendor.Id))
            {
                throw new ArgumentNullException("accountsPayableInvoices.Vendor", "The vendor id is required when submitting an accounts payable invoice. ");
            }

            if (accountsPayableInvoices.ProcessState == AccountsPayableInvoicesProcessState.NotSet)
            {
                throw new ArgumentNullException("accountsPayableInvoices.ProcessState", "The processState is required when submitting an accounts payable invoice. ");
            }
            if (accountsPayableInvoices.PaymentStatus == AccountsPayableInvoicesPaymentStatus.NotSet)
            {
                throw new ArgumentNullException("accountsPayableInvoices.PaymentStatus", "The paymentStatus is required when submitting an accounts payable invoice. ");
            }
            if (accountsPayableInvoices.Payment == null)
            {
                throw new ArgumentNullException("accountsPayableInvoices.Payment", "The payment is required when submitting an accounts payable invoice. ");
            }
            if (accountsPayableInvoices.Payment != null)
            {
                if (accountsPayableInvoices.Payment.Source == null || !accountsPayableInvoices.Payment.PaymentDueOn.HasValue)
                {
                    throw new ArgumentNullException("accountsPayableInvoices.Payment", "The payment source and paymentDueOn are required when submitting an accounts payable invoice payment. ");
                }
                if (accountsPayableInvoices.Payment.Source != null && string.IsNullOrEmpty(accountsPayableInvoices.Payment.Source.Id))
                {
                    throw new ArgumentNullException("accountsPayableInvoices.Payment.Source", "The source id is required when submitting an Payment Source. ");
                }
                if (accountsPayableInvoices.Payment.PaymentTerms != null && string.IsNullOrEmpty(accountsPayableInvoices.Payment.PaymentTerms.Id))
                {
                    throw new ArgumentNullException("accountsPayableInvoices.Payment.PaymentTerms", "The payment term id is required when submitting an Payment Term. ");
                }
            }
            if (accountsPayableInvoices.VendorAddress != null && string.IsNullOrEmpty(accountsPayableInvoices.VendorAddress.Id))
            {
                throw new ArgumentNullException("accountsPayableInvoices.VendorAddress", "The vendor address id is required when submitting an vendor address. ");
            }
            if (accountsPayableInvoices.VendorBilledAmount != null && (!accountsPayableInvoices.VendorBilledAmount.Value.HasValue || accountsPayableInvoices.VendorBilledAmount.Currency == null))
            {
                throw new ArgumentNullException("accountsPayableInvoices.VendorBilledAmount", "The vendor billed amount value and currency are required when submitting an vendor billed amount. ");
            }
            if (accountsPayableInvoices.VendorBilledAmount != null) { defaultCurrency = checkCurrency(defaultCurrency, accountsPayableInvoices.VendorBilledAmount.Currency); }
            if (accountsPayableInvoices.InvoiceDiscountAmount != null && (!accountsPayableInvoices.InvoiceDiscountAmount.Value.HasValue || accountsPayableInvoices.InvoiceDiscountAmount.Currency == null))
            {
                throw new ArgumentNullException("accountsPayableInvoices.InvoiceDiscountAmount", "The vendor billed amount value and currency are required when submitting an vendor billed amount. ");
            }

            if(accountsPayableInvoices.VoidDate != null && accountsPayableInvoices.VoidDate < accountsPayableInvoices.TransactionDate)
                {
                    throw new ArgumentNullException("accountsPayableInvoices.VoidDate", "Void date cannot be before transaction date");
            }

            if (accountsPayableInvoices.InvoiceDiscountAmount != null)
            { defaultCurrency = checkCurrency(defaultCurrency, accountsPayableInvoices.InvoiceDiscountAmount.Currency); }
            if (accountsPayableInvoices.Taxes != null && accountsPayableInvoices.Taxes.Any())
            {
                foreach (var tax in accountsPayableInvoices.Taxes)
                {
                    if (tax.TaxCode != null && string.IsNullOrEmpty(tax.TaxCode.Id))
                    {
                        throw new ArgumentNullException("accountsPayableInvoices.Taxes.TaxCode", "The tax code id is required when submitting Taxes. ");
                    }
                    if (tax.VendorAmount != null && (!tax.VendorAmount.Value.HasValue || tax.VendorAmount.Currency == null))
                    {
                        throw new ArgumentNullException("accountsPayableInvoices.Taxes.VendorAmount.", "The vendor amount value and currency are required when submitting a taxes vendor amount. ");
                    }
                    if (tax.VendorAmount != null)
                    {
                        defaultCurrency = checkCurrency(defaultCurrency, tax.VendorAmount.Currency);
                    }
                }
            }
            if (accountsPayableInvoices.LineItems == null)
            {
                throw new ArgumentNullException("accountsPayableInvoices.LineItems.", "At least one line item must be provided when submitting an accounts-payable-invoice. ");
            }

            if (accountsPayableInvoices.LineItems != null && accountsPayableInvoices.LineItems.Any())
            {
                foreach (var lineItem in accountsPayableInvoices.LineItems)
                {
                    if (lineItem.CommodityCode != null && string.IsNullOrEmpty(lineItem.CommodityCode.Id))
                    {
                        throw new ArgumentNullException("accountsPayableInvoices.LineItems.CommodityCode", "The commodity code id is required when submitting a commodity code. ");
                    }
                    if (lineItem.UnitofMeasure != null && string.IsNullOrEmpty(lineItem.UnitofMeasure.Id))
                    {
                        throw new ArgumentNullException("accountsPayableInvoices.LineItems.UnitofMeasure", "The UnitofMeasure id is required when submitting a UnitofMeasure. ");
                    }
                    if (lineItem.UnitPrice != null && (!lineItem.UnitPrice.Value.HasValue || lineItem.UnitPrice.Currency == null))
                    {
                        throw new ArgumentNullException("accountsPayableInvoices.Taxes.UnitPrice.", "Both the unit price amount value and currency are required when submitting a line item unit price. ");
                    }
                    if (lineItem.UnitPrice != null)
                    {
                        defaultCurrency = checkCurrency(defaultCurrency, lineItem.UnitPrice.Currency);
                    }
                    if (lineItem.VendorBilledUnitPrice != null && (!lineItem.VendorBilledUnitPrice.Value.HasValue || lineItem.VendorBilledUnitPrice.Currency == null))
                    {
                        throw new ArgumentNullException("accountsPayableInvoices.VendorBilledUnitPrice.UnitPrice.", "The vendor billed unit price amount value and currency are required when submitting a line item vendor billed unit prioe. ");
                    }
                    if (lineItem.VendorBilledUnitPrice != null)
                    {
                        defaultCurrency = checkCurrency(defaultCurrency, lineItem.VendorBilledUnitPrice.Currency);
                    }
                    if (lineItem.AdditionalAmount != null && (!lineItem.AdditionalAmount.Value.HasValue || lineItem.AdditionalAmount.Currency == null))
                    {
                        throw new ArgumentNullException("accountsPayableInvoices.AdditionalAmount.UnitPrice", "The additional amount value and currency are required when submitting a line item additional price. ");
                    }
                    if (lineItem.AdditionalAmount != null)
                    {
                        defaultCurrency = checkCurrency(defaultCurrency, lineItem.AdditionalAmount.Currency);
                    }
                    if (lineItem.Taxes != null && lineItem.Taxes.Any())
                    {
                        foreach (var lineItemTaxes in lineItem.Taxes)
                        {
                            if (lineItemTaxes.TaxCode == null)
                            {
                                throw new ArgumentNullException("accountsPayableInvoices.LineItems.Taxes.TaxCode", "The Taxes.TaxCode is required when submitting a line item Tax Code. ");
                            }
                            if (lineItemTaxes.TaxCode != null && string.IsNullOrEmpty(lineItemTaxes.TaxCode.Id))
                            {
                                throw new ArgumentNullException("accountsPayableInvoices.LineItems.Taxes.TaxCode", "The Taxes.TaxCode id is required when submitting a line item Tax Code. ");
                            }
                            if (lineItemTaxes.VendorAmount != null && (!lineItemTaxes.VendorAmount.Value.HasValue || lineItemTaxes.VendorAmount.Currency == null))
                            {
                                throw new ArgumentNullException("accountsPayableInvoices.LineItems.Taxes.VendorAmount", "The VendorAmount value and currency are required when submitting a line item tax vendorAmount. ");
                            }
                            if (lineItemTaxes.VendorAmount != null)
                            {
                                defaultCurrency = checkCurrency(defaultCurrency, lineItemTaxes.VendorAmount.Currency);
                            }
                        }
                    }

                    if (lineItem.Discount != null)
                    {
                        if (lineItem.Discount.Amount != null && (!lineItem.Discount.Amount.Value.HasValue || lineItem.Discount.Amount.Currency == null))
                        {
                            throw new ArgumentNullException("accountsPayableInvoices.LineItems.Discount.Amount", "The discount amount value and currency are required when submitting a line item discount amount. ");
                        }
                        if (lineItem.Discount.Amount != null)
                        {
                            defaultCurrency = checkCurrency(defaultCurrency, lineItem.Discount.Amount.Currency);
                        }
                    }
                    if (lineItem.AccountDetails != null && lineItem.AccountDetails.Any())
                    {
                        foreach (var accountDetail in lineItem.AccountDetails)
                        {
                            if (accountDetail.Allocation != null)
                            {
                                var allocation = accountDetail.Allocation;
                                if (allocation.Allocated != null)
                                {
                                    var allocated = allocation.Allocated;
                                    if (allocated.Amount != null && (!allocated.Amount.Value.HasValue || allocated.Amount.Currency == null))
                                    {
                                        throw new ArgumentNullException("accountsPayableInvoices.LineItems.AccountDetail.Allocation.Allocated",
                                            "The Allocation.Allocated value and currency are required when submitting a line item AccountDetail.Allocation.Allocated. ");
                                    }
                                    if (allocated.Amount != null)
                                    {
                                        defaultCurrency = checkCurrency(defaultCurrency, allocated.Amount.Currency);
                                    }
                                }
                                if (allocation.TaxAmount != null && (!allocation.TaxAmount.Value.HasValue || allocation.TaxAmount.Currency == null))
                                {
                                    throw new ArgumentNullException("accountsPayableInvoices.LineItems.AccountDetail.Allocation.TaxAmount",
                                        "The tax amount value and currency are required when submitting a line item account detail allocation tax amount. ");
                                }
                                if (allocation.TaxAmount != null)
                                {
                                    defaultCurrency = checkCurrency(defaultCurrency, allocation.TaxAmount.Currency);
                                }
                                if (allocation.AdditionalAmount != null && (!allocation.AdditionalAmount.Value.HasValue || allocation.AdditionalAmount.Currency == null))
                                {
                                    throw new ArgumentNullException("accountsPayableInvoices.LineItems.AccountDetail.Allocation.AdditionalAmount",
                                        "The additional amount value and currency are required when submitting a line item account detail allocation additional amount. ");
                                }
                                if (allocation.AdditionalAmount != null)
                                {
                                    defaultCurrency = checkCurrency(defaultCurrency, allocation.AdditionalAmount.Currency);
                                }
                                if (allocation.DiscountAmount != null && (!allocation.DiscountAmount.Value.HasValue || allocation.AdditionalAmount.Currency == null))
                                {
                                    throw new ArgumentNullException("accountsPayableInvoices.LineItems.AccountDetails.DiscountAmount.Allocation.AdditionalAmount",
                                        "The discount amount value and currency are required when submitting a line item account detail allocation discount amount. ");
                                }
                                if (allocation.DiscountAmount != null)
                                {
                                    defaultCurrency = checkCurrency(defaultCurrency, allocation.DiscountAmount.Currency);
                                }

                            }
                            if (accountDetail.Source != null && string.IsNullOrEmpty(accountDetail.Source.Id))
                            {
                                throw new ArgumentNullException("accountsPayableInvoices.LineItems.AccountDetails.Source", "The Source id is required when submitting a line item account detail source. ");
                            }
                            if (accountDetail.SubmittedBy != null && string.IsNullOrEmpty(accountDetail.SubmittedBy.Id))
                            {
                                throw new ArgumentNullException("accountsPayableInvoices.LineItems.AccountDetails.SubmittedBy", "The SubmittedBy id is required when submitting a line item account detail SubmittedBy. ");
                            }
                            if (string.IsNullOrEmpty(accountDetail.AccountingString))
                            {
                                throw new ArgumentNullException("accountsPayableInvoices.LineItems.AccountDetails.AccountingString", "The AccountingString id is required when submitting a line item account detail AccountingString. ");
                            }
                            if (accountDetail.Allocation == null)
                            {
                                throw new ArgumentNullException("accountsPayableInvoices.LineItems.AccountDetails.Allocation", "The Allocation is required when submitting a line item account detail. ");
                            }
                            if (accountDetail.BudgetCheck == AccountsPayableInvoicesAccountBudgetCheck.NotSet)
                            {
                                throw new ArgumentNullException("accountsPayableInvoices.LineItems.AccountDetails.BudgetCheck", "The BudgetCheck is required when submitting a line item account detail.  ");
                            }
                        }

                    }
                    if (string.IsNullOrEmpty(lineItem.Description))
                    {
                        throw new ArgumentNullException("accountsPayableInvoices.LineItems.Description", "The Description is required when submitting a line item. ");
                    }
                    if (lineItem.Quantity == 0)
                    {
                        throw new ArgumentNullException("accountsPayableInvoices.LineItems.Quantity", "The Quantity is required when submitting a line item. ");
                    }
                    if (lineItem.UnitPrice == null)
                    {
                        throw new ArgumentNullException("accountsPayableInvoices.LineItems.UnitPrice", "The UnitPrice is required when submitting a line item. ");
                    }
                    if (lineItem.PaymentStatus == null)
                    {
                        throw new ArgumentNullException("accountsPayableInvoices.LineItems.PaymentStatus", "The PaymentStatus is required when submitting a line item. ");
                    }
                    if (lineItem.Status == null)
                    {
                        throw new ArgumentNullException("accountsPayableInvoices.LineItems.Status", "The Status is required when submitting a line item. ");
                    }
                }
            }
        }
        private CurrencyIsoCode? checkCurrency(CurrencyIsoCode? defaultValue, CurrencyIsoCode? newValue)
        {
            if (defaultValue != null && defaultValue != newValue && newValue != null)
            {
                throw new ArgumentException("accountsPayableInvoices.Currency", "All currency codes in the request must be the same and cannot differ.");
            }
            CurrencyIsoCode? cc = newValue == null ? defaultValue : newValue;
            return cc;
        }
    }
}