﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;
using Ellucian.Web.Security;
using System;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using System.Net;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http.Models;
using Newtonsoft.Json;
using Ellucian.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.ColleagueFinance
{
    /// <summary>
    /// Provides access to Accounting Strings
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.ColleagueFinance)]
    public class AccountingStringsController : BaseCompressedApiController
    {
        private readonly IAccountingStringService _accountingStringService;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the AccountingStringsController class.
        /// </summary>
        /// <param name="accountingStringService">Service of type <see cref="IAccountingStringService">IAccountingStringService</see></param>
        /// <param name="logger">Interface to logger</param>
        public AccountingStringsController(IAccountingStringService accountingStringService, ILogger logger)
        {
            _accountingStringService = accountingStringService;
            _logger = logger;
        }

        #region Accounting String
        /// <summary>
        /// Read (GET) an AccountingString using an accounting string as a filter, return confirms it exists and is valid
        /// </summary>
        /// <param name="accountingString">Accounting String to search for, may contain project number</param>
        /// <param name="validOn">date to check for to see if accounting string is valid on the date</param>
        /// <returns>An AccountingString object <see cref="Dtos.AccountingString"/> in DataModel format</returns>
        [HttpGet]
        public async Task<Dtos.AccountingString> GetAccountingStringByFilterAsync([FromUri] string accountingString = "", [FromUri] string validOn = "")
        {
            if (string.IsNullOrEmpty(accountingString))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null accountingString argument",
                    IntegrationApiUtility.GetDefaultApiError("The accountingString must be specified in the request URL.")));
            }

            try
            {
                DateTime? validOnDate = null;

                if (!string.IsNullOrEmpty(validOn))
                {
                    DateTime outDate;
                    if (DateTime.TryParse(validOn, out outDate))
                    {
                        validOnDate = outDate;
                    }
                    else
                    {
                        throw new ArgumentException(
                            "The value provided for validOn filter could not be converted into a date");
                    }
                }

                return await _accountingStringService.GetAccoutingStringByFilterCriteriaAsync(accountingString, validOnDate);
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                var exception = new Exception(e.Message);
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(exception));
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// AccountingString Get all
        /// </summary>
        /// <returns>MethodNotAllowed error</returns>
        [HttpGet]
        public async Task<IEnumerable<Dtos.AccountingString>> GetAccountingStringsAsync()
        {
            //Get all is not supported for Colleague but Data Model requires full crud support.
            throw CreateHttpResponseException(
                new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage,
                    IntegrationApiUtility.DefaultNotSupportedApiError), HttpStatusCode.MethodNotAllowed);
        }

        /// <summary>
        /// AccountingString Post
        /// </summary>
        /// <returns>MethodNotAllowed error</returns>
        [HttpPost]
        public async Task<Dtos.AccountingString> PostAccountingStringsAsync([FromBody] Dtos.AccountingString accountingString)
        {
            //Post is not supported for Colleague but Data Model requires full crud support.
            throw CreateHttpResponseException(
                new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage,
                    IntegrationApiUtility.DefaultNotSupportedApiError), HttpStatusCode.MethodNotAllowed);
        }

        #endregion

        #region Accounting String Components

        /// <summary>
        /// Return all accountingStringComponents
        /// </summary>
        /// <returns>List of AccountingStringComponents <see cref="Dtos.AccountingStringComponent"/> objects representing matching accountingStringComponents</returns>
        [HttpGet]
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.AccountingStringComponent>> GetAccountingStringComponentsAsync()
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                return await _accountingStringService.GetAccountingStringComponentsAsync(bypassCache);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Read (GET) a accountingStringComponents using a GUID
        /// </summary>
        /// <param name="guid">GUID to desired accountingStringComponents</param>
        /// <returns>A accountingStringComponents object <see cref="Dtos.AccountingStringComponent"/> in EEDM format</returns>
        [HttpGet]
        public async Task<Dtos.AccountingStringComponent> GetAccountingStringComponentsByGuidAsync(string guid)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                return await _accountingStringService.GetAccountingStringComponentsByGuidAsync(guid);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new accountingStringComponents
        /// </summary>
        /// <param name="accountingStringComponents">DTO of the new accountingStringComponents</param>
        /// <returns>A accountingStringComponents object <see cref="Dtos.AccountingStringComponent"/> in EEDM format</returns>
        [HttpPost]
        public async Task<Dtos.AccountingStringComponent> PostAccountingStringComponentsAsync([FromBody] Dtos.AccountingStringComponent accountingStringComponents)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Update (PUT) an existing accountingStringComponents
        /// </summary>
        /// <param name="guid">GUID of the accountingStringComponents to update</param>
        /// <param name="accountingStringComponents">DTO of the updated accountingStringComponent</param>
        /// <returns>A accountingStringComponents object <see cref="Dtos.AccountingStringComponent"/> in EEDM format</returns>
        [HttpPut]
        public async Task<Dtos.AccountingStringComponent> PutAccountingStringComponentsAsync([FromUri] string guid, [FromBody] Dtos.AccountingStringComponent accountingStringComponents)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Delete (DELETE) a accountingStringComponents
        /// </summary>
        /// <param name="guid">GUID to desired accountingStringComponents</param>
        [HttpDelete]
        public async Task DeleteAccountingStringComponentsAsync(string guid)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }
        #endregion

        #region Accounting String Formats


        /// <summary>
        /// Return all accountingStringFormats
        /// </summary>
        /// <returns>List of AccountingStringFormats <see cref="Dtos.AccountingStringFormats"/> objects representing matching accountingStringFormats</returns>
        [HttpGet]
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.AccountingStringFormats>> GetAccountingStringFormatsAsync()
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                return await _accountingStringService.GetAccountingStringFormatsAsync(bypassCache);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Read (GET) a accountingStringFormats using a GUID
        /// </summary>
        /// <param name="guid">GUID to desired accountingStringFormats</param>
        /// <returns>A accountingStringFormats object <see cref="Dtos.AccountingStringFormats"/> in EEDM format</returns>
        [HttpGet]
        public async Task<Dtos.AccountingStringFormats> GetAccountingStringFormatsByGuidAsync(string guid)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                return await _accountingStringService.GetAccountingStringFormatsByGuidAsync(guid);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new accountingStringFormats
        /// </summary>
        /// <param name="accountingStringFormats">DTO of the new accountingStringFormats</param>
        /// <returns>A accountingStringFormats object <see cref="Dtos.AccountingStringFormats"/> in EEDM format</returns>
        [HttpPost]
        public async Task<Dtos.AccountingStringFormats> PostAccountingStringFormatsAsync([FromBody] Dtos.AccountingStringFormats accountingStringFormats)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Update (PUT) an existing accountingStringFormats
        /// </summary>
        /// <param name="guid">GUID of the accountingStringFormats to update</param>
        /// <param name="accountingStringFormats">DTO of the updated accountingStringFormats</param>
        /// <returns>A accountingStringFormats object <see cref="Dtos.AccountingStringFormats"/> in EEDM format</returns>
        [HttpPut]
        public async Task<Dtos.AccountingStringFormats> PutAccountingStringFormatsAsync([FromUri] string guid, [FromBody] Dtos.AccountingStringFormats accountingStringFormats)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Delete (DELETE) a accountingStringFormats
        /// </summary>
        /// <param name="guid">GUID to desired accountingStringFormats</param>
        [HttpDelete]
        public async Task DeleteAccountingStringFormatsAsync(string guid)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }
        #endregion

        #region Accounting String Components Values

        /// <summary>
        /// Return all accountingStringComponentValues
        /// </summary>
        /// <returns>List of AccountingStringComponentValues <see cref="Dtos.AccountingStringComponentValues"/> objects representing matching accountingStringComponentValues</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 200)]
        public async Task<IHttpActionResult> GetAccountingStringComponentValuesAsync(Paging page, [FromUri] string criteria = "")
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                if (page == null)
                {
                    page = new Paging(200, 0);
                }
                string component = string.Empty, transactionStatus = string.Empty, typeAccount = string.Empty, typeFund = string.Empty;

                var criteriaValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(criteria);

                if (criteriaValues != null)
                {
                    foreach (var value in criteriaValues)
                    {
                        switch (value.Key.ToLower())
                        {
                            case "component":
                                component = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "transactionstatus":
                                transactionStatus = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                if (transactionStatus != "available" && transactionStatus != "unavailable")
                                { throw new ArgumentException(string.Concat("transactionStatus must be either \"available\" or \"unavailable\". We got: ", transactionStatus)); }
                                break;
                            case "typeaccount":
                                typeAccount = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                if (typeAccount != "asset" && typeAccount != "liability" && typeAccount != "fundBalance" && typeAccount != "revenue" && typeAccount != "personalExpense" && typeAccount != "nonPersonalExpense" && typeAccount != "transfer"
                                    && typeAccount != "expense")
                                { throw new ArgumentException(string.Concat("transactionStatus must be either \"asset\" or \"liability\" or \"fundBalance\" or \"revenue\" or \"expense\" or \"personalExpense\" or \"nonPersonalExpense\" or \"transfer\". We got: ", typeAccount)); }

                                break;
                            case "typefund":
                                typeFund = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            default:
                                throw new ArgumentException(string.Concat("Invalid filter value received: ", value.Key));
                        }
                    }
                }

                var pageOfItems = await _accountingStringService.GetAccountingStringComponentValuesAsync(page.Offset, page.Limit, component, transactionStatus, typeAccount, typeFund, bypassCache);
                return new PagedHttpActionResult<IEnumerable<Dtos.AccountingStringComponentValues>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Read (GET) a accountingStringComponentValues using a GUID
        /// </summary>
        /// <param name="guid">GUID to desired accountingStringComponentValues</param>
        /// <returns>A accountingStringComponentValues object <see cref="Dtos.AccountingStringComponentValues"/> in EEDM format</returns>
        [HttpGet]
        public async Task<Dtos.AccountingStringComponentValues> GetAccountingStringComponentValuesByGuidAsync(string guid)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                return await _accountingStringService.GetAccountingStringComponentValuesByGuidAsync(guid);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new accountingStringComponentValues
        /// </summary>
        /// <param name="accountingStringComponentValues">DTO of the new accountingStringComponentValues</param>
        /// <returns>A accountingStringComponentValues object <see cref="Dtos.AccountingStringComponentValues"/> in EEDM format</returns>
        [HttpPost]
        public async Task<Dtos.AccountingStringComponentValues> PostAccountingStringComponentValuesAsync([FromBody] Dtos.AccountingStringComponentValues accountingStringComponentValues)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Update (PUT) an existing accountingStringComponentValues
        /// </summary>
        /// <param name="guid">GUID of the accountingStringComponentValues to update</param>
        /// <param name="accountingStringComponentValues">DTO of the updated accountingStringComponentValues</param>
        /// <returns>A accountingStringComponentValues object <see cref="Dtos.AccountingStringComponentValues"/> in EEDM format</returns>
        [HttpPut]
        public async Task<Dtos.AccountingStringComponentValues> PutAccountingStringComponentValuesAsync([FromUri] string guid, [FromBody] Dtos.AccountingStringComponentValues accountingStringComponentValues)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Delete (DELETE) a accountingStringComponentValues
        /// </summary>
        /// <param name="guid">GUID to desired accountingStringComponentValues</param>
        [HttpDelete]
        public async Task DeleteAccountingStringComponentValuesAsync(string guid)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        #endregion Accounting String Components Values
    }
}
