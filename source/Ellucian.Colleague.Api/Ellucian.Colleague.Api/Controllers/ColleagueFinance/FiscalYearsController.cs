﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.ColleagueFinance
{
    /// <summary>
    /// Fiscal Years controller.
    /// </summary>
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.ColleagueFinance)]
    [Authorize]
    public class FiscalYearsController : BaseCompressedApiController
    {
        //private IGeneralLedgerConfigurationRepository glConfigurationRepository;
        private readonly ICostCenterService costCenterService;
        private readonly ILogger logger;

        /// <summary>
        /// This constructor initializes the Fiscal Years controller.
        /// </summary>
        /// <param name="costCenterService">GL cost center service object.</param>
        /// <param name="logger">Logger object.</param>
        public FiscalYearsController(ICostCenterService costCenterService, ILogger logger)
        {
            //this.glConfigurationRepository = glConfigurationRepository;
            this.costCenterService = costCenterService;
            this.logger = logger;
        }

        /// <summary>
        /// Retrieves all the GL fiscal years available to the user - these include the
        /// fiscal year for today plus a maximum of 5 previous fiscal years, if available.
        /// </summary>
        /// <returns>List of fiscal years.</returns>
        public async Task<IEnumerable<string>> GetAsync()
        {
            try
            {
                // Call the service method to obtain the list of fiscal years to display in the cost center views.
                return await costCenterService.GetFiscalYearsAsync();
            }
            catch (ConfigurationException ce)
            {
                logger.Error(ce, ce.Message);
                throw CreateHttpResponseException("Invalid configuration.", HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw CreateHttpResponseException();
            }
        }

        /// <summary>
        /// Returns the fiscal year for today's date based on the General Ledger configuration.
        /// </summary>
        /// <returns>The fiscal year for today's date.</returns>
        public async Task<string> GetFiscalYearForTodayAsync()
        {
            try
            {
                // Call the service method to obtain today's fiscal year to apply in the cost centers view.
                return await costCenterService.GetFiscalYearForTodayAsync();
            }
            catch (ConfigurationException ce)
            {
                logger.Error(ce, ce.Message);
                throw CreateHttpResponseException("Invalid configuration.", HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw CreateHttpResponseException();
            }
        }
    }
}
