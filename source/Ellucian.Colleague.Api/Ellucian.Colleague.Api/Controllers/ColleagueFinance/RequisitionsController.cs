﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;

namespace Ellucian.Colleague.Api.Controllers.ColleagueFinance
{
    /// <summary>
    /// The controller for requisitions
    /// </summary>
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.ColleagueFinance)]
    [Authorize]
    public class RequisitionsController : BaseCompressedApiController
    {
        private readonly IRequisitionService requisitionService;
        private readonly ILogger logger;

        /// <summary>
        /// This constructor initializes the RequisitionsController object
        /// </summary>
        /// <param name="requisitionService">Requisition service object</param>
        /// <param name="logger">Logger object</param>
        public RequisitionsController(IRequisitionService requisitionService, ILogger logger)
        {
            this.requisitionService = requisitionService;
            this.logger = logger;
        }

        /// <summary>
        /// Retrieves a specified requisition
        /// </summary>
        /// <param name="requisitionId">ID of the requested requisition</param>
        /// <returns>Requisition DTO</returns>
        public async Task<Requisition> GetRequisitionAsync(string requisitionId)
        {
            if (string.IsNullOrEmpty(requisitionId))
            {
                string message = "A Requisition ID must be specified.";
                logger.Error(message);
                throw CreateHttpResponseException(message, HttpStatusCode.BadRequest);
            }

            try
            {
                var requisition = await requisitionService.GetRequisitionAsync(requisitionId);
                return requisition;
            }
            catch (ArgumentNullException anex)
            {
                logger.Error(anex, anex.Message);
                throw CreateHttpResponseException("Invalid argument.", HttpStatusCode.BadRequest);
            }
            catch (KeyNotFoundException knfex)
            {
                logger.Error(knfex, knfex.Message);
                throw CreateHttpResponseException("Record not found.", HttpStatusCode.NotFound);
            }
            catch (ApplicationException aex)
            {
                logger.Error(aex, aex.Message);
                throw CreateHttpResponseException("Invalid data in record.", HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw CreateHttpResponseException();
            }
        }
    }
}