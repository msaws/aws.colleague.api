﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;
using Ellucian.Web.Security;
using System;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using System.Net;
using Newtonsoft.Json;

namespace Ellucian.Colleague.Api.Controllers.ColleagueFinance
{
    /// <summary>
    /// Provides access to Accounting Strings
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.ColleagueFinance)]
    public class AccountFundsAvailableController : BaseCompressedApiController
    {
        private readonly IAccountFundsAvailableService _accountingFundsAvailableService;
        private readonly ILogger _logger;

        /// <summary>
        /// The primary use of the Account Funds Available entity is to validate the status of funds (available, not available, and not available, but may be overridden) 
        /// for an accounting string as of a given date. Initializes a new instance of the AccountFundsAvailableController class.
        /// </summary>
        /// <param name="accountingFundsAvailableService">Service of type <see cref="IAccountFundsAvailableService">IAccountFundsAvailableService</see></param>
        /// <param name="logger">Interface to logger</param>
        public AccountFundsAvailableController(IAccountFundsAvailableService accountingFundsAvailableService, ILogger logger)
        {
            _accountingFundsAvailableService = accountingFundsAvailableService;
            _logger = logger;
        }

        #region accounting funds available

        /// <summary>
        /// 
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<Dtos.AccountFundsAvailable> GetAccountFundsAvailableByFilterAsync([FromUri] string criteria = "")
        {
            if (string.IsNullOrEmpty(criteria))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null criteria argument",
                    IntegrationApiUtility.GetDefaultApiError("The criteria is required when a GET operation is requested.")));
            }

            try
            {
                string accountingString = string.Empty, amount = string.Empty, balanceOn = string.Empty, submittedBy = string.Empty;
                IDictionary<string, string> criteriaValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(criteria);

                foreach (var value in criteriaValues)
                {
                    switch (value.Key.ToLower())
                    {
                        case "accountingstring":
                            accountingString = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value.ToString();
                            break;
                        case "amount":
                            amount = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value.ToString();
                            break;
                        case "balanceon":
                            balanceOn = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value.ToString();
                            break;
                        case "submittedby":
                            submittedBy = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value.ToString();
                            break;
                    }
                }

                //values passed to the service
                decimal amountValue;
                DateTime? balanceOnDateValue;
                string submittedByValue = string.Empty;

                //validate before calling the service
                Validate(accountingString, amount, balanceOn, submittedBy, out balanceOnDateValue, out amountValue, out submittedByValue);

                return await _accountingFundsAvailableService.GetAccountFundsAvailableByFilterCriteriaAsync(accountingString, amountValue, balanceOnDateValue, submittedByValue);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }

            catch (JsonSerializationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Parses and validates the input parameters
        /// </summary>
        /// <param name="accountingString"></param>
        /// <param name="amount"></param>
        /// <param name="balanceOn"></param>
        /// <param name="submittedBy"></param>
        /// <param name="balanceOnDateValue"></param>
        /// <param name="amountValue"></param>
        /// <param name="submittedByValue"></param>
        private void Validate(string accountingString, string amount, string balanceOn, string submittedBy, out DateTime? balanceOnDateValue, out decimal amountValue, out string submittedByValue)
        {
            if (string.IsNullOrEmpty(accountingString))
            {
                throw new ArgumentNullException("Null accountingString argument", "Accounting string is required when a GET operation is requested.");
            }
            if (string.IsNullOrEmpty(amount))
            {
                throw new ArgumentNullException("Null amount argument", "Amount is required when a GET operation is requested.");
            }

            balanceOnDateValue = null;
            submittedByValue = string.Empty;

            if (!string.IsNullOrEmpty(balanceOn))
            {
                DateTime outDate;
                if (DateTime.TryParse(balanceOn, out outDate))
                {
                    balanceOnDateValue = outDate;
                }
                else
                {
                    throw new ArgumentException(
                        "The value provided for balanceOn filter could not be converted into a date.");
                }
            }

            decimal outAmount = 0;
            if (decimal.TryParse(amount, out outAmount))
            {
                if (outAmount == 0)
                {
                    throw new ArgumentException("Amount is required when a GET operation is requested.");
                }
                amountValue = outAmount;
            }
            else
            {
                throw new ArgumentException(
                    "The value provided for amount filter could not be converted into a number.");
            }

            Guid outGuid;
            if (!string.IsNullOrEmpty(submittedBy))
            {
                if (Guid.TryParse(submittedBy, out outGuid))
                {
                    if (outGuid.Equals(Guid.Empty))
                    {
                        throw new ArgumentException("The empty guid is not a valid search criteria parameter.", "submittedBy");
                    }
                    submittedByValue = outGuid.ToString();
                }
                else
                {
                    throw new ArgumentException(
                        "The value provided for submittedBy filter could not be converted into a guid.", "submittedBy");
                }
            }
        }

        /// <summary>
        /// AccountFundsAvailable Get all
        /// </summary>
        /// <returns>MethodNotAllowed error</returns>
        [HttpGet]
        public async Task<IEnumerable<Dtos.AccountFundsAvailable>> GetAccountFundsAvailableAsync()
        {
            //Get all is not supported for Colleague but Data Model requires full crud support.
            throw CreateHttpResponseException(
                new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage,
                    IntegrationApiUtility.DefaultNotSupportedApiError), HttpStatusCode.MethodNotAllowed);
        }

        /// <summary>
        /// AccountFundsAvailable Post
        /// </summary>
        /// <returns>MethodNotAllowed error</returns>
        [HttpPost]
        public async Task<Dtos.AccountFundsAvailable> PostAccountFundsAvailableAsync([FromBody] Dtos.AccountFundsAvailable accountingString)
        {
            //Post is not supported for Colleague but Data Model requires full crud support.
            throw CreateHttpResponseException(
                new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage,
                    IntegrationApiUtility.DefaultNotSupportedApiError), HttpStatusCode.MethodNotAllowed);
        }

        /// <summary>
        /// Update (PUT) AccountFundsAvailable
        /// </summary>
        /// <param name="guid"></param>
        /// <param name="accountFundsAvailable"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<Dtos.AccountFundsAvailable> PutAccountFundsAvailableAsync([FromUri] string guid, [FromBody] Dtos.AccountFundsAvailable accountFundsAvailable)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Delete (DELETE) AccountFundsAvailable
        /// </summary>
        /// <param name="guid">GUID to desired vendor</param>
        [HttpDelete]
        public async Task DeleteAccountFundsAvailableAsync(string guid)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        #endregion accounting funds available

        #region Account funds available transactions

        /// <summary>
        /// Return all accountFundsAvailable_Transactions
        /// </summary>
        /// <returns>List of AccountFundsAvailable_Transactions <see cref="Dtos.AccountFundsAvailable_Transactions"/> objects representing matching accountFundsAvailable_Transactions</returns>
        [HttpGet]
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.AccountFundsAvailable_Transactions>> GetAccountFundsAvailable_TransactionsAsync()
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Read (GET) a accountFundsAvailable_Transactions using a GUID
        /// </summary>
        /// <param name="guid">GUID to desired accountFundsAvailable_Transactions</param>
        /// <returns>A accountFundsAvailable_Transactions object <see cref="Dtos.AccountFundsAvailable_Transactions"/> in EEDM format</returns>
        [HttpGet]
        public async Task<Dtos.AccountFundsAvailable_Transactions> GetAccountFundsAvailable_TransactionsByGuidAsync(string guid)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Create (POST) a new accountFundsAvailable_Transactions
        /// </summary>
        /// <param name="accountFundsAvailable_Transactions">DTO of the new accountFundsAvailable_Transactions</param>
        /// <returns>A accountFundsAvailable_Transactions object <see cref="Dtos.AccountFundsAvailable_Transactions"/> in EEDM format</returns>
        [HttpPost]
        public async Task<Dtos.AccountFundsAvailable_Transactions> PostAccountFundsAvailable_TransactionsAsync([FromBody] Dtos.AccountFundsAvailable_Transactions accountFundsAvailable_Transactions)
        {

            //Create is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Update (PUT) an existing accountFundsAvailable_Transactions
        /// </summary>
        /// <param name="guid">GUID of the accountFundsAvailable_Transactions to update</param>
        /// <param name="accountFundsAvailable_Transactions">DTO of the updated accountFundsAvailable_Transactions</param>
        /// <returns>A accountFundsAvailable_Transactions object <see cref="Dtos.AccountFundsAvailable_Transactions"/> in EEDM format</returns>
        [HttpPut]
        public async Task<Dtos.AccountFundsAvailable_Transactions> PutAccountFundsAvailable_TransactionsAsync([FromUri] string guid, [FromBody] Dtos.AccountFundsAvailable_Transactions accountFundsAvailable_Transactions)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Delete (DELETE) a accountFundsAvailable_Transactions
        /// </summary>
        /// <param name="guid">GUID to desired accountFundsAvailable_Transactions</param>
        [HttpDelete]
        public async Task DeleteAccountFundsAvailable_TransactionsAsync(string guid)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Create (POST) a new accountFundsAvailable_Transactions
        /// </summary>
        /// <param name="accountFundsAvailable_Transactions">DTO of the new accountFundsAvailable_Transactions</param>
        /// <returns>A accountFundsAvailable_Transactions object <see cref="Dtos.AccountFundsAvailable_Transactions"/> in EEDM format</returns>
        [HttpPost]
        public async Task<Dtos.AccountFundsAvailable_Transactions> QueryAccountFundsAvailable_TransactionsAsync([FromBody] Dtos.AccountFundsAvailable_Transactions accountFundsAvailable_Transactions)
        {
            try
            {
                return await _accountingFundsAvailableService.CheckAccountFundsAvailable_Transactions2Async(accountFundsAvailable_Transactions);
            }
          
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Ellucian.Colleague.Domain.Exceptions.RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException aex)
            {
                _logger.Error(aex.ToString());
                throw CreateHttpResponseException(aex.Message, HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }


        }

        #endregion account funds available transactions
    }
}
