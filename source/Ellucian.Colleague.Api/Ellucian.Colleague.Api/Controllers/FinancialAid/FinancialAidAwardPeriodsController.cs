﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Web.Http.Controllers;
using System.Web.Http;
using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using System.Threading.Tasks;
using System;
using Ellucian.Colleague.Coordination.FinancialAid.Services;
using slf4net;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Dtos;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http;
using System.Net;

namespace Ellucian.Colleague.Api.Controllers.FinancialAid
{
    /// <summary>
    /// Provides access to FinancialAidAwardPeriods data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof (EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.FinancialAid)]
    public class FinancialAidAwardPeriodsController : BaseCompressedApiController
    {
        private readonly IFinancialAidAwardPeriodService _financialAidAwardPeriodService;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the FinancialAidAwardPeriodsController class.
        /// </summary>
        /// <param name="financialAidAwardPeriodService">Repository of type <see cref="IFinancialAidAwardPeriodService">IFinancialAidAwardPeriodService</see></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public FinancialAidAwardPeriodsController(IFinancialAidAwardPeriodService financialAidAwardPeriodService, ILogger logger)
        {
            _financialAidAwardPeriodService = financialAidAwardPeriodService;
            _logger = logger;
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Retrieves all financial aid award periods.
        /// If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <returns>All FinancialAidAwardPeriod objects.</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.FinancialAidAwardPeriod>> GetFinancialAidAwardPeriodsAsync()
        {
            bool bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                return await _financialAidAwardPeriodService.GetFinancialAidAwardPeriodsAsync(bypassCache);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Retrieves an Financial Aid Award Periods by ID.
        /// </summary>
        /// <returns>An <see cref="Dtos.FinancialAidAwardPeriod">FinancialAidAwardPeriod</see>object.</returns>
        [HttpGet]
        public async Task<FinancialAidAwardPeriod> GetFinancialAidAwardPeriodByIdAsync(string id)
        {
            try
            {
                return await _financialAidAwardPeriodService.GetFinancialAidAwardPeriodByGuidAsync(id);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Creates a Financial Aid Award Period.
        /// </summary>
        /// <param name="financialAidAwardPeriod"><see cref="Dtos.FinancialAidAwardPeriod">FinancialAidAwardPeriod</see> to create</param>
        /// <returns>Newly created <see cref="Dtos.FinancialAidAwardPeriod">FinancialAidAwardPeriod</see></returns>
        [HttpPost]
        public async Task<Dtos.FinancialAidAwardPeriod> PostFinancialAidAwardPeriodAsync([FromBody] Dtos.FinancialAidAwardPeriod financialAidAwardPeriod)
        {
            //Create is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Updates a Financial Aid Award Period.
        /// </summary>
        /// <param name="id">Id of the Financial Aid Award Period to update</param>
        /// <param name="financialAidAwardPeriod"><see cref="Dtos.FinancialAidAwardPeriod">FinancialAidAwardPeriod</see> to create</param>
        /// <returns>Updated <see cref="Dtos.FinancialAidAwardPeriod">FinancialAidAwardPeriod</see></returns>
        [HttpPut]
        public async Task<Dtos.FinancialAidAwardPeriod> PutFinancialAidAwardPeriodAsync([FromUri] string id, [FromBody] Dtos.FinancialAidAwardPeriod financialAidAwardPeriod)
        {

            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Deletes a Financial Aid Award Period.
        /// </summary>
        /// <param name="id">ID of the Financial Aid Award Period to be deleted</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task DeleteFinancialAidAwardPeriodAsync(string id)
        {
            //Delete is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

    }
}