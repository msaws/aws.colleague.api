﻿/*Copyright 2014-2017 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.FinancialAid.Services;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Web.Security;
using Ellucian.Colleague.Api.Utility;
using System.Net;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Exceptions;

namespace Ellucian.Colleague.Api.Controllers.FinancialAid
{
    /// <summary>
    /// Exposes FinancialAidOffice and FinancialAidConfiguration Data
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class FinancialAidOfficesController : BaseCompressedApiController
    {
        private readonly IFinancialAidOfficeService financialAidOfficeService;
        private readonly IAdapterRegistry adapterRegistry;
        private readonly ILogger logger;

        /// <summary>
        /// Constructor for FinancialAidOfficesController
        /// </summary>
        /// <param name="adapterRegistry">AdapterRegistry</param>
        /// <param name="financialAidOfficeService">FinancialAidOfficeService</param>
        /// <param name="logger">Logger</param>
        public FinancialAidOfficesController(IAdapterRegistry adapterRegistry, IFinancialAidOfficeService financialAidOfficeService, ILogger logger)
        {
            this.financialAidOfficeService = financialAidOfficeService;
            this.adapterRegistry = adapterRegistry;
            this.logger = logger;
        }

        /// <summary>
        /// Get a list of Financial Aid Offices and their year-based configurations
        /// </summary>
        /// <returns>A list of FinancialAidOffice3 objects</returns>
        public async Task<IEnumerable<FinancialAidOffice3>> GetFinancialAidOffices3Async()
        {
            try
            {
                return await financialAidOfficeService.GetFinancialAidOffices3Async();
            }
            catch (KeyNotFoundException knfe)
            {
                logger.Error(knfe, knfe.Message);
                throw CreateNotFoundException("FinancialAidOffices", string.Empty);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException("Unknown error occurred getting FinancialAidOffices resource. See log for details." + Environment.NewLine + e.Message);
            }
        }


        /// <summary>
        /// Get a list of Financial Aid Offices and their year-based configurations
        /// </summary>
        /// <returns>A list of FinancialAidOffice2 objects</returns>
        [Obsolete("Obsolete as of Api version 1.15, use version 3 of this API")]
        public IEnumerable<FinancialAidOffice2> GetFinancialAidOffices2()
        {
            try
            {
                return financialAidOfficeService.GetFinancialAidOffices2();
            }
            catch (KeyNotFoundException knfe)
            {
                logger.Error(knfe, knfe.Message);
                throw CreateNotFoundException("FinancialAidOffices", string.Empty);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException("Unknown error occurred getting FinancialAidOffices resource. See log for details." + Environment.NewLine + e.Message);
            }
        }

        /// <summary>
        /// Get a list of Financial Aid Offices and their year-based configurations
        /// </summary>
        /// <returns>A list of FinancialAidOffice objects</returns>
        [Obsolete("Obsolete as of Api version 1.15, use version 3 of this API")]
        public async Task<IEnumerable<FinancialAidOffice2>> GetFinancialAidOffices2Async()
        {
            try
            {
                return await financialAidOfficeService.GetFinancialAidOffices2Async();
            }
            catch (KeyNotFoundException knfe)
            {
                logger.Error(knfe, knfe.Message);
                throw CreateNotFoundException("FinancialAidOffices", string.Empty);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException("Unknown error occurred getting FinancialAidOffices resource. See log for details." + Environment.NewLine + e.Message);
            }
        }

        /// <summary>
        /// Get a list of Financial Aid Offices and their year-based configurations
        /// </summary>
        /// <returns>A list of FinancialAidOffice objects</returns>
        [Obsolete("Obsolete as of Api version 1.14, use version 2 of this API")]
        public IEnumerable<FinancialAidOffice> GetFinancialAidOffices()
        {
            try
            {
                return financialAidOfficeService.GetFinancialAidOffices();
            }
            catch (KeyNotFoundException knfe)
            {
                logger.Error(knfe, knfe.Message);
                throw CreateNotFoundException("FinancialAidOffices", string.Empty);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException("Unknown error occurred getting FinancialAidOffices resource. See log for details." + Environment.NewLine + e.Message);
            }
        }

        /// <summary>
        /// Get a list of Financial Aid Offices and their year-based configurations
        /// </summary>
        /// <returns>A list of FinancialAidOffice objects</returns>
        [Obsolete("Obsolete as of Api version 1.14, use version 2 of this API")]
        public async Task<IEnumerable<FinancialAidOffice>> GetFinancialAidOfficesAsync()
        {
            try
            {
                return await financialAidOfficeService.GetFinancialAidOfficesAsync();
            }
            catch (KeyNotFoundException knfe)
            {
                logger.Error(knfe, knfe.Message);
                throw CreateNotFoundException("FinancialAidOffices", string.Empty);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException("Unknown error occurred getting FinancialAidOffices resource. See log for details." + Environment.NewLine + e.Message);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Retrieves all financial aid offices.
        /// If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <returns>All FinancialAidOffice objects.</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.FinancialAidOffice>> GetEedmFinancialAidOfficesAsync()
        {
            bool bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                return await financialAidOfficeService.GetFinancialAidOfficesAsync(bypassCache);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentNullException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Retrieves an Financial Aid Offices by ID.
        /// </summary>
        /// <returns>An <see cref="Dtos.FinancialAidOffice">FinancialAidOffice</see>object.</returns>
        [HttpGet]
        public async Task<Ellucian.Colleague.Dtos.FinancialAidOffice> GetFinancialAidOfficeByGuidAsync(string guid)
        {
            try
            {
                return await financialAidOfficeService.GetFinancialAidOfficeByGuidAsync(guid);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentNullException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Creates a Financial Aid Office.
        /// </summary>
        /// <param name="financialAidOffice"><see cref="Dtos.FinancialAidOffice">FinancialAidOffice</see> to create</param>
        /// <returns>Newly created <see cref="Dtos.FinancialAidOffice">FinancialAidOffice</see></returns>
        [HttpPost]
        public async Task<Dtos.FinancialAidOffice> PostFinancialAidOfficeAsync([FromBody] Dtos.FinancialAidOffice financialAidOffice)
        {
            //Create is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Updates a Financial Aid Office.
        /// </summary>
        /// <param name="guid">Id of the Financial Aid Office to update</param>
        /// <param name="financialAidOffice"><see cref="Dtos.FinancialAidOffice">FinancialAidOffice</see> to create</param>
        /// <returns>Updated <see cref="Dtos.FinancialAidOffice">FinancialAidOffice</see></returns>
        [HttpPut]
        public async Task<Dtos.FinancialAidOffice> PutFinancialAidOfficeAsync([FromUri] string guid, [FromBody] Dtos.FinancialAidOffice financialAidOffice)
        {

            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Deletes a Financial Aid Office.
        /// </summary>
        /// <param name="guid">ID of the Financial Aid Office to be deleted</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task DeleteFinancialAidOfficeAsync(string guid)
        {
            //Delete is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }
    }
}