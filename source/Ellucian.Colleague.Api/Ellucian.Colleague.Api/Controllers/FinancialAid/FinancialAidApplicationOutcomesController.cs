﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Web.Http.Controllers;
using System.Web.Http;
using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using slf4net;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Colleague.Coordination.FinancialAid.Services;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Adapters;

namespace Ellucian.Colleague.Api.Controllers.FinancialAid
{
    /// <summary>
    /// Provides access to FinancialAidApplicationOutcomes
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.FinancialAid)]
    public class FinancialAidApplicationOutcomesController : BaseCompressedApiController
    {
        private readonly IFinancialAidApplicationOutcomeService financialAidApplicationOutcomeService;
        //private readonly IAdapterRegistry adapterRegistry;
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the FinancialAidApplicationOutcomesController class.
        /// </summary>
        /// <param name="financialAidApplicationOutcomeService">FinancialAidApplicationOutcomeService</param>
        /// <param name="logger">Interface to logger</param>
        //public FinancialAidApplicationOutcomesController(IAdapterRegistry adapterRegistry,
        //    IFinancialAidApplicationOutcomeService financialAidApplicationOutcomeService,
        //    ILogger logger)
        public FinancialAidApplicationOutcomesController(IFinancialAidApplicationOutcomeService financialAidApplicationOutcomeService,
            ILogger logger)
        {
            //this.adapterRegistry = adapterRegistry;
            this.financialAidApplicationOutcomeService = financialAidApplicationOutcomeService;
            this.logger = logger;
        }

        /// <summary>
        /// Return all financialAidApplicationOutcomes
        /// </summary>
        /// <returns>List of FinancialAidApplicationOutcomes</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetFinancialAidApplicationOutcomesAsync(Paging page)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            if (page == null)
            {
                page = new Paging(100, 0);
            }
            try
            {
                AddDataPrivacyContextProperty((await financialAidApplicationOutcomeService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await financialAidApplicationOutcomeService.GetAsync(page.Offset, page.Limit, bypassCache);
                return new PagedHttpActionResult<IEnumerable<Dtos.FinancialAidApplicationOutcome>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (KeyNotFoundException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Read (GET) a financialAidApplicationOutcomes using a GUID
        /// </summary>
        /// <param name="id">GUID to desired financialAidApplicationOutcomes</param>
        /// <returns>A single financialAidApplicationOutcomes object</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.FinancialAidApplicationOutcome> GetFinancialAidApplicationOutcomesByGuidAsync(string id)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                AddDataPrivacyContextProperty((await financialAidApplicationOutcomeService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await financialAidApplicationOutcomeService.GetByIdAsync(id);
            }
            catch (KeyNotFoundException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new financialAidApplicationOutcomes
        /// </summary>
        /// <param name="financialAidApplicationOutcomes">DTO of the new financialAidApplicationOutcomes</param>
        /// <returns>A single financialAidApplicationOutcomes object</returns>
        [HttpPost]
        public async Task<Dtos.FinancialAidApplicationOutcome> CreateAsync([FromBody] Dtos.FinancialAidApplicationOutcome financialAidApplicationOutcomes)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Update (PUT) an existing financialAidApplicationOutcomes
        /// </summary>
        /// <param name="id">GUID of the financialAidApplicationOutcomes to update</param>
        /// <param name="financialAidApplicationOutcomes">DTO of the updated financialAidApplicationOutcomes</param>
        /// <returns>A single financialAidApplicationOutcomes object</returns>
        [HttpPut]
        public async Task<Dtos.FinancialAidApplicationOutcome> UpdateAsync([FromUri] string id, [FromBody] Dtos.FinancialAidApplicationOutcome financialAidApplicationOutcomes)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Delete (DELETE) a financialAidApplicationOutcomes
        /// </summary>
        /// <param name="id">GUID to desired financialAidApplicationOutcomes</param>
        [HttpDelete]
        public async Task DeleteAsync(string id)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }
    }
}
