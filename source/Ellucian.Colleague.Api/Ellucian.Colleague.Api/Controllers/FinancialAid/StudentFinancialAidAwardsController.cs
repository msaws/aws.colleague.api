﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.FinancialAid.Services;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Web.Security;
using System.Net.Http;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.FinancialAid
{
    /// <summary>
    /// The controller for student financial aid awards for the Ellucian Data Model.
    /// </summary>
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.FinancialAid)]
    [Authorize]
    public class StudentFinancialAidAwardsController : BaseCompressedApiController
    {
        private readonly IStudentFinancialAidAwardService studentFinancialAidAwardService;
        private readonly ILogger logger;

        /// <summary>
        /// This constructor initializes the StudentFinancialAidAwardController object
        /// </summary>
        /// <param name="studentFinancialAidAwardService">student financial aid awards service object</param>
        /// <param name="logger">Logger object</param>
        public StudentFinancialAidAwardsController(IStudentFinancialAidAwardService studentFinancialAidAwardService, ILogger logger)
        {
            this.studentFinancialAidAwardService = studentFinancialAidAwardService;
            this.logger = logger;
        }

        /// <summary>
        /// Retrieves a specified student financial aid award for the data model version 7
        /// There is a restricted and a non-restricted view of financial aid awards.  This
        /// is the non-restricted version using student-financial-aid-awards.
        /// </summary>
        /// <param name="id">The requested student financial aid award GUID</param>
        /// <returns>A StudentFinancialAidAward DTO</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.StudentFinancialAidAward> GetByIdAsync([FromUri] string id)
        {
            bool bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    throw new ArgumentNullException("id", "id is required.");
                }
                AddDataPrivacyContextProperty((await studentFinancialAidAwardService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var studentFinancialAidAward = await studentFinancialAidAwardService.GetByIdAsync(id, false);
                return studentFinancialAidAward;
            }
            catch (KeyNotFoundException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error getting student financial aid award");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves all student financial aid awards for the data model version 7
        /// There is a restricted and a non-restricted view of financial aid awards.  This
        /// is the non-restricted version using student-financial-aid-awards.
        /// </summary>
        /// <returns>A Collection of StudentFinancialAidAwards</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 200), EedmResponseFilter]
        public async Task<IHttpActionResult> GetAsync(Paging page)
        {
            try
            {
                bool bypassCache = false;
                if (Request.Headers.CacheControl != null)
                {
                    if (Request.Headers.CacheControl.NoCache)
                    {
                        bypassCache = true;
                    }
                }
                if (page == null)
                {
                    page = new Paging(200, 0);
                }

                AddDataPrivacyContextProperty((await studentFinancialAidAwardService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await studentFinancialAidAwardService.GetAsync(page.Offset, page.Limit, bypassCache, false);
                return new PagedHttpActionResult<IEnumerable<Dtos.StudentFinancialAidAward>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error getting student financial aid award");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves a specified student financial aid award for the data model version 7
        /// There is a restricted and a non-restricted view of financial aid awards.  This
        /// is the restricted version using restricted-student-financial-aid-awards.
        /// </summary>
        /// <param name="id">The requested student financial aid award GUID</param>
        /// <returns>A StudentFinancialAidAward DTO</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.StudentFinancialAidAward> GetRestrictedByIdAsync([FromUri] string id)
        {
            bool bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    throw new ArgumentNullException("id", "id is required.");
                }
                AddDataPrivacyContextProperty((await studentFinancialAidAwardService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var studentFinancialAidAward = await studentFinancialAidAwardService.GetByIdAsync(id, true);
                return studentFinancialAidAward;
            }
            catch (KeyNotFoundException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error getting student financial aid award");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves all student financial aid awards for the data model version 7
        /// There is a restricted and a non-restricted view of financial aid awards.  This
        /// is the restricted version using restricted-student-financial-aid-awards.
        /// </summary>
        /// <returns>A Collection of StudentFinancialAidAwards</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 200), EedmResponseFilter]
        public async Task<IHttpActionResult> GetRestrictedAsync(Paging page)
        {
            try
            {
                bool bypassCache = false;
                if (Request.Headers.CacheControl != null)
                {
                    if (Request.Headers.CacheControl.NoCache)
                    {
                        bypassCache = true;
                    }
                }
                if (page == null)
                {
                    page = new Paging(200, 0);
                }
                AddDataPrivacyContextProperty((await studentFinancialAidAwardService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await studentFinancialAidAwardService.GetAsync(page.Offset, page.Limit, bypassCache, true);
                return new PagedHttpActionResult<IEnumerable<Dtos.StudentFinancialAidAward>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error getting student financial aid award");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Update a single student financial aid award for the data model version 7
        /// </summary>
        /// <param name="id">The requested student financial aid award GUID</param>
        /// <param name="studentFinancialAidAwardDto">General Ledger DTO from Body of request</param>
        /// <returns>A single StudentFinancialAidAward</returns>
        [HttpPut]
        public async Task<Dtos.StudentFinancialAidAward> UpdateAsync([FromUri] string id, [FromBody] Dtos.StudentFinancialAidAward studentFinancialAidAwardDto)
        {
            //PUT is not supported for Colleague but Hedm requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Create a single student financial aid award for the data model version 7
        /// </summary>
        /// <param name="studentFinancialAidAwardDto">Student Financial Aid Award DTO from Body of request</param>
        /// <returns>A single StudentFinancialAidAward</returns>
        [HttpPost]
        public async Task<Dtos.StudentFinancialAidAward> CreateAsync([FromBody] Dtos.StudentFinancialAidAward studentFinancialAidAwardDto)
        {
            //POST is not supported for Colleague but Hedm requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Delete a single student financial aid award for the data model version 6
        /// </summary>
        /// <param name="id">The requested student financial aid award GUID</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<HttpResponseMessage> DeleteAsync([FromUri] string id)
        {
            //Delete is not supported for Colleague but Hedm requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }
    }
}