﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Web.Http.Controllers;
using System.Web.Http;
using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using slf4net;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Colleague.Coordination.FinancialAid.Services;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;

namespace Ellucian.Colleague.Api.Controllers.FinancialAid
{
    /// <summary>
    /// Provides access to FinancialAidApplications
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.FinancialAid)]
    public class FinancialAidApplicationsController : BaseCompressedApiController
    {
        private readonly IFinancialAidApplicationService financialAidApplicationService;
        private readonly IFinancialAidApplicationService2 financialAidApplicationService2;
        private readonly IAdapterRegistry adapterRegistry;
        private readonly ILogger logger;

        /// <summary>
        /// Initializes a new instance of the FinancialAidApplicationsController class.
        /// </summary>
        /// <param name="adapterRegistry">AdapterRegistry</param>   
        /// <param name="financialAidApplicationService">FinancialAidApplicationService</param>
        /// <param name="financialAidApplicationService2">FinancialAidApplicationsService2</param>
        /// <param name="logger">Interface to logger</param>
        public FinancialAidApplicationsController(IAdapterRegistry adapterRegistry,
            IFinancialAidApplicationService financialAidApplicationService,
            IFinancialAidApplicationService2 financialAidApplicationService2,
            ILogger logger)
        {
            this.adapterRegistry = adapterRegistry;
            this.financialAidApplicationService = financialAidApplicationService;
            this.financialAidApplicationService2 = financialAidApplicationService2;
            this.logger = logger;
        }

        /// <summary>
        /// Return all financialAidApplications
        /// </summary>
        /// <returns>List of FinancialAidApplications <see cref="Dtos.FinancialAidApplications"/> objects representing matching financialAidApplications</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetAsync(Paging page)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            if (page == null)
            {
                page = new Paging(100, 0);
            }
            try
            {
                AddDataPrivacyContextProperty((await financialAidApplicationService2.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await financialAidApplicationService2.GetAsync(page.Offset, page.Limit, bypassCache);
                return new PagedHttpActionResult<IEnumerable<Dtos.FinancialAidApplication>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (KeyNotFoundException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Read (GET) a financialAidApplications using a GUID
        /// </summary>
        /// <param name="id">GUID to desired financialAidApplications</param>
        /// <returns>A financialAidApplications object <see cref="Dtos.FinancialAidApplications"/> in EEDM format</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.FinancialAidApplication> GetByIdAsync(string id)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                AddDataPrivacyContextProperty((await financialAidApplicationService2.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await financialAidApplicationService2.GetByIdAsync(id);
            }
            catch (KeyNotFoundException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new financialAidApplications
        /// </summary>
        /// <param name="financialAidApplications">DTO of the new financialAidApplications</param>
        /// <returns>A financialAidApplications object <see cref="Dtos.FinancialAidApplications"/> in EEDM format</returns>
        [HttpPost]
        public async Task<Dtos.FinancialAidApplication> CreateAsync([FromBody] Dtos.FinancialAidApplication financialAidApplications)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Update (PUT) an existing financialAidApplications
        /// </summary>
        /// <param name="id">GUID of the financialAidApplications to update</param>
        /// <param name="financialAidApplications">DTO of the updated financialAidApplications</param>
        /// <returns>A financialAidApplications object <see cref="Dtos.FinancialAidApplications"/> in EEDM format</returns>
        [HttpPut]
        public async Task<Dtos.FinancialAidApplication> UpdateAsync([FromUri] string id, [FromBody] Dtos.FinancialAidApplication financialAidApplications)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Delete (DELETE) a financialAidApplications
        /// </summary>
        /// <param name="id">GUID to desired financialAidApplications</param>
        [HttpDelete]
        public async Task DeleteAsync(string id)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Obsolete as of API version 1.7. Deprecated. Use FAFSA and ProfileApplication endpoints instead.
        /// Get a list of FinancialAidApplication objects for all years the student has application data in Colleague.
        /// </summary>
        /// <param name="studentId">The studentId for which to get the award data</param>
        /// <returns>A list of FinancialAidApplication DTOs</returns>
        [Obsolete("Obsolete as of API version 1.7. Deprecated. Get Financial Aid Applications using GET /students/{studentId}/fafsas and GET /students/{studentId}/profile-applications")]
        public IEnumerable<FinancialAidApplication> GetFinancialAidApplications(string studentId)
        {
            if (string.IsNullOrEmpty(studentId))
            {
                throw CreateHttpResponseException("studentId cannot be null");
            }
            try
            {
                return financialAidApplicationService.GetFinancialAidApplications(studentId);
            }
            catch (PermissionsException pe)
            {
                logger.Error(pe, pe.Message);
                throw CreateHttpResponseException("Access to financial aid applications resource forbidden. See log for details.", System.Net.HttpStatusCode.Forbidden);
            }
            catch (KeyNotFoundException knfe)
            {
                logger.Error(knfe, knfe.Message);
                throw CreateHttpResponseException("Unable to find financial aid applications resource. See log for details.", System.Net.HttpStatusCode.NotFound);
            }
            catch (Exception e)
            {
                logger.Error(e, e.Message);
                throw CreateHttpResponseException("Unknown error occurred getting financial aid applications. See log for details.", System.Net.HttpStatusCode.BadRequest);
            }
        }

    }
}
