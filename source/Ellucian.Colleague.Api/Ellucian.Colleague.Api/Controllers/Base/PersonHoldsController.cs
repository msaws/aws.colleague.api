﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Colleague.Api.Utility;
using System.Net.Http;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.Base
{
    /// <summary>
    /// Provides person holds data
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Base)]
    public class PersonHoldsController : BaseCompressedApiController
    {
        private readonly IPersonHoldsService _personHoldsService;
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly ILogger _logger;        

       /// <summary>
       /// Person holds constructor
       /// </summary>
       /// <param name="adapterRegistry"></param>
       /// <param name="personHoldsService"></param>
       /// <param name="logger"></param>
        public PersonHoldsController(IAdapterRegistry adapterRegistry, IPersonHoldsService personHoldsService, ILogger logger)
        {
            _adapterRegistry = adapterRegistry;
            _personHoldsService = personHoldsService;
            _logger = logger;
        }

        #region GET Methods
        /// <summary>
        /// Returns a list of all active restrictions recorded for any person in the database
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 200), EedmResponseFilter]
        public async Task<IHttpActionResult> GetPersonsActiveHoldsAsync(Paging page)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                if (page == null)
                {
                    page = new Paging(200, 0);
                }

                AddDataPrivacyContextProperty((await _personHoldsService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await _personHoldsService.GetPersonHoldsAsync(page.Offset, page.Limit);
                return new PagedHttpActionResult<IEnumerable<Dtos.PersonHold>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(ex));
            }
        }
        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Retrieves all active person holds for hold id
        /// </summary>
        /// <returns>PersonHold object for a person.</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.PersonHold> GetPersonsActiveHoldAsync([FromUri] string id)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _personHoldsService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _personHoldsService.GetPersonHoldAsync(id);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Retrieves all active person holds for person id
        /// </summary>
        /// <returns>PersonHold object for a person.</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<IEnumerable<Dtos.PersonHold>> GetPersonsActiveHoldsByPersonIdAsync([FromUri] string person)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _personHoldsService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _personHoldsService.GetPersonHoldsAsync(person);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        #endregion

        #region PUT method
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="personHold"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<Dtos.PersonHold> PutPersonHoldAsync([FromUri] string id, [FromBody] Dtos.PersonHold personHold)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    throw new ArgumentNullException("Person hold id cannot be null or empty");
                }

                if (personHold == null)
                {
                    throw new ArgumentNullException("personHold cannot be null or empty");
                }

                if (!id.Equals(personHold.Id, StringComparison.OrdinalIgnoreCase))
                {
                    throw new InvalidOperationException("id does not match id in personHold.");
                }

                if (id.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    throw new InvalidOperationException("Nil GUID cannot be used in PUT operation.");
                }

                if (string.IsNullOrEmpty(personHold.Id))
                {
                    personHold.Id = id.ToUpperInvariant();
                }

                await _personHoldsService.DoesUpdateViolateDataPrivacySettings(GetRouteResourceName(), personHold);
                return await _personHoldsService.UpdatePersonHoldAsync(id, personHold);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (InvalidOperationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }
        #endregion

        #region POST method
        /// <summary>
        /// Create new person hold
        /// </summary>
        /// <param name="personHold">personHold</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<Dtos.PersonHold> PostPersonHoldAsync([FromBody] Dtos.PersonHold personHold)
        {
            try
            {
                if (personHold == null)
                {
                    throw new ArgumentNullException("personHold cannot be null or empty");
                }
                if (string.IsNullOrEmpty(personHold.Id))
                {
                    throw new ArgumentNullException("Null personHold id", "Id is a required property.");
                }
                return await _personHoldsService.CreatePersonHoldAsync(personHold);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (InvalidOperationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }
        #endregion
        
        #region DELETE method
        /// <summary>
        /// Deletes person hold based on id
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<HttpResponseMessage> DeletePersonHoldAsync(string id)
        {
            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    throw new ArgumentNullException("Person hold id cannot be null or empty");
                }
                await _personHoldsService.DeletePersonHoldAsync(id);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
        #endregion
    }
}