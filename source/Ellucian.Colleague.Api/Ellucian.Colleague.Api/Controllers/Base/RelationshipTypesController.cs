﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Ellucian.Web.Http.Controllers;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Dtos.Base;
using System.Web.Http;
using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Adapters;
using System.Threading.Tasks;
namespace Ellucian.Colleague.Api.Controllers
{
    /// <summary>
    /// Provides access to Relationship Type data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Base)]
    public class RelationshipTypesController : BaseCompressedApiController
    {
        private readonly IReferenceDataRepository _referenceDataRepository;
        private readonly IAdapterRegistry _adapterRegistry;

        /// <summary>
        /// Initializes a new instance of the RelationshipTypesController class.
        /// </summary>
        /// <param name="adapterRegistry">Adapter registry of type <see cref="IAdapterRegistry">IAdapterRegistry</see></param>
        /// <param name="referenceDataRepository">Repository of type <see cref="IReferenceDataRepository">IReferenceDataRepository</see></param>
        public RelationshipTypesController(IAdapterRegistry adapterRegistry, IReferenceDataRepository referenceDataRepository)
        {
            _adapterRegistry = adapterRegistry;
            _referenceDataRepository = referenceDataRepository;
        }

        /// <summary>
        /// Retrieves all Relationship Types.
        /// </summary>
        /// <returns>All <see cref="RelationshipType">relationship types.</see></returns>
        public async Task<IEnumerable<RelationshipType>> GetAsync()
        {
            var relationshipTypeCollection = await _referenceDataRepository.GetRelationshipTypesAsync();

            // Get the right adapter for the type mapping
            var relationshipTypeDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Base.Entities.RelationshipType, RelationshipType>();

            // Map the RelationshipType entity to the program DTO
            var relationshipTypeDtoCollection = new List<RelationshipType>();
            foreach (var relationshipType in relationshipTypeCollection)
            {
                relationshipTypeDtoCollection.Add(relationshipTypeDtoAdapter.MapToType(relationshipType));
            }

            return relationshipTypeDtoCollection;
        }
    }
}
