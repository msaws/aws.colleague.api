﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Web.Http.Controllers;
using System.Web.Http;
using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Adapters;
using slf4net;
using System;
using System.Threading.Tasks;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.Base.Exceptions;
using System.Net;

namespace Ellucian.Colleague.Api.Controllers
{
    /// <summary>
    /// Provides access to Educational Institution Units
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Base)]
    public class EducationalInstitutionUnitsController : BaseCompressedApiController
    {
        private readonly IEducationalInstitutionUnitsService _educationalInstitutionUnitsService;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the EducationalInstitutionUnitsController class.
        /// </summary>
        /// <param name="educationalInstitutionUnitsService">Service of type <see cref="IEducationalInstitutionUnitsService">IEducationalInstitutionUnitsService</see></param>
        /// <param name="logger">Interface to logger</param>
        public EducationalInstitutionUnitsController(IEducationalInstitutionUnitsService educationalInstitutionUnitsService, ILogger logger)
        {
            _educationalInstitutionUnitsService = educationalInstitutionUnitsService;
            _logger = logger;
        }

        /// <summary>
        /// Return all Educational-Institution-Units
        /// including department, division, or school code
        /// </summary>
        /// <returns>List of EducationalInstitutionUnits <see cref="Dtos.EducationalInstitutionUnits"/> objects representing matching educationalInstitutionUnits</returns>
        [HttpGet]
        public async Task<IEnumerable<Dtos.EducationalInstitutionUnits2>> GetEducationalInstitutionUnits2Async()
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                return await _educationalInstitutionUnitsService.GetEducationalInstitutionUnits2Async(bypassCache);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Return all Educational-Institution-Units
        /// </summary>
        /// <returns>List of EducationalInstitutionUnits <see cref="Dtos.EducationalInstitutionUnits"/> objects representing matching educationalInstitutionUnits</returns>
        [HttpGet]
        public async Task<IEnumerable<Dtos.EducationalInstitutionUnits>> GetEducationalInstitutionUnitsAsync()
        {          
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            } 
            try
            {
                return await _educationalInstitutionUnitsService.GetEducationalInstitutionUnitsAsync(bypassCache);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Return Educational-Institution-Unit using type filter
        /// </summary>
        /// <param name="type">Type of Educational-Institution-Unit ex:"school", "division", "department"</param>
        /// <returns>List of EducationalInstitutionUnits <see cref="Dtos.EducationalInstitutionUnits"/> objects representing matching educationalInstitutionUnits</returns>
        [HttpGet]
        public async Task<IEnumerable<Dtos.EducationalInstitutionUnits2>> GetEducationalInstitutionUnitsByType2Async([FromUri] string type = "")
        {
            if (string.IsNullOrEmpty(type))
            {
                throw new ArgumentNullException("type", "No criteria specified for selection of educationalInstitutionUnits");
            }
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                return await _educationalInstitutionUnitsService.GetEducationalInstitutionUnitsByType2Async(type, bypassCache);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }
        
        /// <summary>
        /// Return Educational-Institution-Unit using type filter
        /// </summary>
        /// <param name="type">Type of Educational-Institution-Unit ex:"school", "division", "department"</param>
       /// <returns>List of EducationalInstitutionUnits <see cref="Dtos.EducationalInstitutionUnits"/> objects representing matching educationalInstitutionUnits</returns>
        [HttpGet]
        public async Task<IEnumerable<Dtos.EducationalInstitutionUnits>> GetEducationalInstitutionUnitsByTypeAsync([FromUri] string type = "")
        {      
            if (string.IsNullOrEmpty(type))
            {
                throw new ArgumentNullException("type", "No criteria specified for selection of educationalInstitutionUnits");
            }
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            } 
            try
            {
                return await _educationalInstitutionUnitsService.GetEducationalInstitutionUnitsByTypeAsync(type, bypassCache);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Read (GET) an Educational-Institution-Unit using a GUID
        /// </summary>
        /// <param name="id">GUID to desired educationalInstitutionUnits</param>
        /// <returns>An EducationalInstitutionUnits object <see cref="Dtos.EducationalInstitutionUnits"/> in HEDM format</returns>
        [HttpGet]
        public async Task<Dtos.EducationalInstitutionUnits2> GetEducationalInstitutionUnitsByGuid2Async(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                return await _educationalInstitutionUnitsService.GetEducationalInstitutionUnitsByGuid2Async(id);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }
        
        /// <summary>
        /// Read (GET) an Educational-Institution-Unit using a GUID
        /// </summary>
        /// <param name="id">GUID to desired educationalInstitutionUnits</param>
        /// <returns>An EducationalInstitutionUnits object <see cref="Dtos.EducationalInstitutionUnits"/> in HEDM format</returns>
        [HttpGet]
        public async Task<Dtos.EducationalInstitutionUnits> GetEducationalInstitutionUnitsByGuidAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                return await _educationalInstitutionUnitsService.GetEducationalInstitutionUnitsByGuidAsync(id);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new Educational-Institution-Unit
        /// </summary>
        /// <param name="educationalInstitutionUnits">DTO of the new educationalInstitutionUnits</param>
        /// <returns>A educationalInstitutionUnits object <see cref="Dtos.EducationalInstitutionUnits"/> in HEDM format</returns>
        [HttpPost]
        public async Task<Dtos.EducationalInstitutionUnits> PostEducationalInstitutionUnitsAsync([FromBody] Dtos.EducationalInstitutionUnits educationalInstitutionUnits)
        {
            //Post is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
    
        }

        /// <summary>
        /// Update (PUT) an existing Educational-Institution-Unit
        /// </summary>
        /// <param name="id">GUID of the EducationalInstitutionUnits to update</param>
        /// <param name="educationalInstitutionUnits">DTO of the updated EducationalInstitutionUnits</param>
        /// <returns>A EducationalInstitutionUnits object <see cref="Dtos.EducationalInstitutionUnits"/> in HEDM format</returns>
        [HttpPut]
        public async Task<Dtos.EducationalInstitutionUnits> PutEducationalInstitutionUnitsAsync([FromUri] string id, [FromBody] Dtos.EducationalInstitutionUnits educationalInstitutionUnits)
        {
            //Put is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
    
        }

        /// <summary>
        /// Delete (DELETE) a Educational-Institution-Unit
        /// </summary>
        /// <param name="id">GUID to desired EducationalInstitutionUnits</param>
        [HttpDelete]
        public async Task DeleteEducationalInstitutionUnitsByGuidAsync(string id)
        {
            //Delete is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
    
        }
    }
}