﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Dtos;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Results;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Ellucian.Colleague.Api.Controllers.Base
{
    /// <summary>
    /// Provides access to person data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Base)]
    public class PersonsController : BaseCompressedApiController
    {
        private readonly IPersonRestrictionTypeService _personRestrictionTypeService;
        private readonly IPersonService _personService;
        private readonly IEmergencyInformationService _emergencyInformationService;
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly ILogger _logger;        

        /// <summary>
        /// Initializes a new instance of the PersonsController class.
        /// </summary>
        /// <param name="adapterRegistry">Adapter registry of type <see cref="IAdapterRegistry">IAdapterRegistry</see></param>
        /// <param name="personService">Service of type <see cref="IPersonService">IPersonService</see></param>
        /// <param name="personRestrictionTypeService">Service of type <see cref="IPersonRestrictionTypeService">IPersonRestrictionTypeService</see></param>
        /// <param name="emergencyInformationService">Service of type <see cref="IEmergencyInformationService">IEmergencyInformationService</see></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public PersonsController(IAdapterRegistry adapterRegistry, IPersonService personService, IPersonRestrictionTypeService personRestrictionTypeService, IEmergencyInformationService emergencyInformationService, ILogger logger)
        {
            _adapterRegistry = adapterRegistry;
            _personService = personService;
            _personRestrictionTypeService = personRestrictionTypeService;
            _emergencyInformationService = emergencyInformationService;
            _logger = logger;
        }

        #region Get Methods

        /// <summary>
        /// Get a person.
        /// </summary>
        /// <param name="guid">Guid of the person to get</param>
        /// <returns>The requested <see cref="Person">Person</see></returns>
        public async Task<Person> GetAsync(string guid)
        {
            try
            {
                return await _personService.GetPersonByGuidNonCachedAsync(guid);
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateNotFoundException("person", guid);
            }
        }

        /// <summary>
        /// Get all person credentials not supported in V5
        /// </summary>
        /// <returns></returns>
        public async Task<Dtos.PersonCredential> GetPersonCredentialsV5Async()
        {
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Gets a subset of person credentials.
        /// </summary>
        /// <returns>The requested <see cref="Dtos.PersonCredential">PersonsCredentials</see></returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 200), EedmResponseFilter]
        public async Task<IHttpActionResult> GetPersonCredentialsAsync(Paging page)
        {
            try
            {
                bool bypassCache = false;
                if (Request.Headers.CacheControl != null)
                {
                    if (Request.Headers.CacheControl.NoCache)
                    {
                        bypassCache = true;
                    }
                }
                if (page == null)
                {
                    page = new Paging(200, 0);
                }

                AddDataPrivacyContextProperty((await _personService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await _personService.GetAllPersonCredentialsAsync(page.Offset, page.Limit, bypassCache);
                return new PagedHttpActionResult<IEnumerable<Dtos.PersonCredential>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Gets a subset of person credentials.
        /// </summary>
        /// <returns>The requested <see cref="Dtos.PersonCredential">PersonsCredentials</see></returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 200), EedmResponseFilter]
        public async Task<IHttpActionResult> GetPersonCredentials2Async(Paging page)
        {
            try
            {
                bool bypassCache = false;
                if (Request.Headers.CacheControl != null)
                {
                    if (Request.Headers.CacheControl.NoCache)
                    {
                        bypassCache = true;
                    }
                }
                if (page == null)
                {
                    page = new Paging(200, 0);
                }

                AddDataPrivacyContextProperty((await _personService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await _personService.GetAllPersonCredentials2Async(page.Offset, page.Limit, bypassCache);
                return new PagedHttpActionResult<IEnumerable<Dtos.PersonCredential2>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (RepositoryException rex)
            {
                _logger.Error(rex.Message);
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(rex));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Get a subset of person's data, including only their credentials.
        /// </summary>
        /// <param name="id">A global identifier of a person.</param>
        /// <returns>The requested <see cref="Dtos.PersonCredential">PersonsCredentials</see></returns>
        [EedmResponseFilter]
        public async Task<Dtos.PersonCredential> GetPersonCredentialByGuidAsync(string id)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _personService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _personService.GetPersonCredentialByGuidAsync(id);
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateNotFoundException("person", id);
            }
        }

        /// <summary>
        /// Get a subset of person's data, including only their credentials.
        /// </summary>
        /// <param name="id">A global identifier of a person.</param>
        /// <returns>The requested <see cref="Dtos.PersonCredential2">PersonsCredentials</see></returns>
        [EedmResponseFilter]
        public async Task<Dtos.PersonCredential2> GetPersonCredential2ByGuidAsync(string id)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _personService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _personService.GetPersonCredential2ByGuidAsync(id);
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateNotFoundException("person", id);
            }
        }
        
        /// <summary>
        /// Get person data associated with a particular role.
        /// </summary>
        /// <returns>List of <see cref="Dtos.Person">persons</see> associated with faculty</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100)]
        public async Task<IHttpActionResult> GetPersonsByRoleAsync(Paging page, string role)
        {
            try
            {
                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                var pageOfItems = await _personService.GetPersonsByRoleNonCachedAsync(page.Offset, page.Limit, role);
                return new PagedHttpActionResult<IEnumerable<Dtos.Person>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN CDM</remarks>
        /// <summary>
        /// Retrieves all active person restriction types for a student
        /// </summary>
        /// <returns>PersonRestrictionType object for a student.</returns>
        [Obsolete("Obsolete as of HeDM Version 4.5, use person-holds API instead.")]
        public async Task<List<Dtos.GuidObject>> GetActivePersonRestrictionTypesAsync(string guid)
        {
            try
            {
                return await _personRestrictionTypeService.GetActivePersonRestrictionTypesAsync(guid);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(ex));
            }
        }

        /// <summary>
        /// Get a person's profile information.
        /// </summary>
        /// <param name="personId">Id of the person to get</param>
        /// <returns>The requested <see cref="Person">Profile</see></returns>
        public async Task<Dtos.Base.Profile> GetProfileAsync(string personId)
        {
            try
            {
                bool useCache = true;
                if (Request != null && Request.Headers != null && Request.Headers.CacheControl != null)
                {
                    if (Request.Headers.CacheControl.NoCache)
                    {
                        useCache = false;
                    }
                }
                return await _personService.GetProfileAsync(personId, useCache);
            }
            catch (ArgumentNullException anex)
            {
                _logger.Error(anex.ToString());
                throw CreateHttpResponseException(anex.Message, HttpStatusCode.BadRequest);
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateNotFoundException("person", personId);
            }
        }

        /// <summary>
        /// Get all the emergency information for a person.
        /// </summary>
        /// <param name="personId">Pass in a person ID</param>
        /// <returns>Returns all the emergency information for the specified person</returns>
        [Obsolete("Obsolete as of API 1.16. Use GetEmergencyInformation2Async instead.")]
        public async Task<Ellucian.Colleague.Dtos.Base.EmergencyInformation> GetEmergencyInformationAsync(string personId)
        {
            if (string.IsNullOrEmpty(personId))
            {
                throw CreateHttpResponseException("Invalid person ID", HttpStatusCode.BadRequest);
            }

            try
            {
                return await _emergencyInformationService.GetEmergencyInformationAsync(personId);
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception)
            {
                throw CreateNotFoundException("person", personId);
            }
        }

        /// <summary>
        /// Get all the emergency information for a person.
        /// </summary>
        /// <param name="personId">Pass in a person ID</param>
        /// <returns>Returns all the emergency information for the specified person</returns>
        public async Task<Ellucian.Colleague.Dtos.Base.EmergencyInformation> GetEmergencyInformation2Async(string personId)
        {
            if (string.IsNullOrEmpty(personId))
            {
                throw CreateHttpResponseException("Invalid person ID", HttpStatusCode.BadRequest);
            }

            try
            {
                var privacyWrappedEmerInfo = await _emergencyInformationService.GetEmergencyInformation2Async(personId);
                var emergencyInformation = privacyWrappedEmerInfo.Dto;
                if (privacyWrappedEmerInfo.HasPrivacyRestrictions)
                {
                    SetContentRestrictedHeader("partial");
                }
                return emergencyInformation;
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception)
            {
                throw CreateNotFoundException("person", personId);
            }
        }

        #endregion

        #region Get Methods for HEDM v6

        /// <summary>
        /// Get a person.
        ///  If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <param name="guid">Guid of the person to get</param>
        /// <returns>The requested <see cref="Person">Person</see></returns>
        [EedmResponseFilter]
        public async Task<Person2> GetPerson2Async(string guid)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                AddDataPrivacyContextProperty((await _personService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _personService.GetPerson2ByGuidAsync(guid, bypassCache);
            }
            catch (PermissionsException e)
            {
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (KeyNotFoundException e)
            {
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Get person data associated with a particular role.
        /// </summary>
        /// <returns>List of <see cref="Dtos.Person">persons</see> associated with faculty</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetPersons2ByRoleAsync(Paging page, string role)
        {
            var bypassCache = false;

            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                if (page == null)
                {
                    page = new Paging(100, 0);
                }
                AddDataPrivacyContextProperty((await _personService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await _personService.GetPersons2ByRoleNonCachedAsync(page.Offset, page.Limit, role);
                return new PagedHttpActionResult<IEnumerable<Dtos.Person2>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Return a list of Person objects based on selection criteria.
        /// </summary>
        /// <param name="page">Person page to retrieve</param>
        /// <param name="title">Person Title Contains ...title...</param>
        /// <param name="firstName">Person First Name equal to</param>
        /// <param name="middleName">Person Middle Name equal to</param>
        /// <param name="lastNamePrefix">Person Last Name begins with 'code...'</param>
        /// <param name="lastName">Person Last Name equal to</param>
        /// <param name="pedigree">Person Suffixe Contains ...pedigree... (guid)</param>
        /// <param name="preferredName">Person Preferred Name equal to (guid)</param>
        /// <param name="role">Person Role equal to (guid)</param>
        /// <param name="credentialType">Person Credential Type (colleagueId or ssn)</param>
        /// <param name="credentialValue">Person Credential equal to</param>
        /// <param name="personFilter">Selection from SaveListParms definition or person-filters</param>
        /// <returns>List of Person2 <see cref="Dtos.Person2"/> objects representing matching persons</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetPerson2Async(Paging page, [FromUri] string title = "", [FromUri] string firstName = "", [FromUri] string middleName = "",
            [FromUri] string lastNamePrefix = "", [FromUri] string lastName = "", [FromUri] string pedigree = "", [FromUri] string preferredName = "",
            [FromUri] string role = "", [FromUri] string credentialType = "", [FromUri] string credentialValue = "", [FromUri] string personFilter = "")
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            if (!string.IsNullOrEmpty(credentialType) && string.IsNullOrEmpty(credentialValue))
            {
                throw new ArgumentException("credentialValue", "credentialValue is required when requesting a credentialType");
            }
            if (string.IsNullOrEmpty(credentialType) && !string.IsNullOrEmpty(credentialValue))
            {
                throw new ArgumentException("credentialType", "credentialType is required when requesting a credentialValue");
            }

            // Disallow filtering by SSN/SIN for now
            if (!string.IsNullOrEmpty(credentialType))
            {
                if (credentialType.ToLower() == "ssn")
                {
                    throw new ArgumentException("credentialType", "Filtering by SSN is not supported at this time.");
                }
                if (credentialType.ToLower() == "sin")
                {
                    throw new ArgumentException("credentialType", "Filtering by SIN is not supported at this time.");
                }
            }

            try
            {
                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                AddDataPrivacyContextProperty((await _personService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());

                var pageOfItems = await _personService.GetPerson2NonCachedAsync(page.Offset, page.Limit, bypassCache,
                    title, firstName, middleName, lastNamePrefix, lastName, pedigree, preferredName,
                    role, credentialType, credentialValue, personFilter);

                return new PagedHttpActionResult<IEnumerable<Dtos.Person2>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        #endregion

        #region Get Methods for HEDM v8

        /// <summary>
        /// Get a person.
        ///  If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <param name="guid">Guid of the person to get</param>
        /// <returns>The requested <see cref="Person">Person</see></returns>
        [EedmResponseFilter]
        public async Task<Dtos.Person3> GetPerson3Async(string guid)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                AddDataPrivacyContextProperty((await _personService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _personService.GetPerson3ByGuidAsync(guid, bypassCache);
            }
            catch (PermissionsException e)
            {
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (KeyNotFoundException e)
            {
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Get person data associated with a particular role.
        /// </summary>
        /// <returns>List of <see cref="Dtos.Person3">persons</see> associated with faculty</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetPersons3ByRoleAsync(Paging page, string role)
        {
            var bypassCache = false;

            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _personService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());

                if (page == null)
                {
                    page = new Paging(100, 0);
                }
                var pageOfItems = await _personService.GetPersons3ByRoleNonCachedAsync(page.Offset, page.Limit, role);
                return new PagedHttpActionResult<IEnumerable<Dtos.Person3>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Return a list of Person objects based on selection criteria.
        /// </summary>
        /// <param name="page">Person page to retrieve</param>
        /// <param name="criteria">Person search criteria in JSON format
        /// Can contain:
        /// title - Person title equal to
        /// firstName - Person First Name equal to
        /// middleName - Person Middle Name equal to
        /// lastNamePrefix - Person Last Name begins with 'code...'
        /// lastName- -Person Last Name equal to
        /// pedigree- -Person Suffix Contains pedigree (guid)
        /// preferredName - Person Preferred Name equal to (guid)
        /// role - Person Role equal to (guid)
        /// credentialType - Person Credential Type (colleagueId - SSN/SIN not supported here)
        /// credentialValue - Person Credential equal to
        /// personFilter - Selection from SaveListParms definition or person-filters</param>
        /// <returns>List of Person2 <see cref="Dtos.Person3"/> objects representing matching persons</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetPerson3Async(Paging page, [FromUri] string criteria = "")
        {
            var bypassCache = false;

            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                AddDataPrivacyContextProperty((await _personService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());

                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                string title = string.Empty, firstName = string.Empty, middleName = string.Empty, lastNamePrefix = string.Empty,
                    lastName = string.Empty, pedigree = string.Empty, preferredName = string.Empty, role = string.Empty, 
                    credentialType = string.Empty, credentialValue = string.Empty, personFilter = string.Empty;

                var criteriaValues = JsonConvert.DeserializeObject<Dictionary<string, object>>(criteria);

                if (criteriaValues != null)
                {
                    foreach (var value in criteriaValues)
                    {
                        switch (value.Key.ToLower())
                        {
                            case "title":
                                title = string.IsNullOrWhiteSpace(value.Value.ToString()) ? string.Empty : value.Value.ToString();
                                break;
                            case "firstname":
                                firstName = string.IsNullOrWhiteSpace(value.Value.ToString()) ? string.Empty : value.Value.ToString();
                                break;
                            case "middlename":
                                middleName = string.IsNullOrWhiteSpace(value.Value.ToString()) ? string.Empty : value.Value.ToString();
                                break;
                            case "lastnameprefix":
                                lastNamePrefix = string.IsNullOrWhiteSpace(value.Value.ToString()) ? string.Empty : value.Value.ToString();
                                break;
                            case "lastname":
                                lastName = string.IsNullOrWhiteSpace(value.Value.ToString()) ? string.Empty : value.Value.ToString();
                                break;
                            case "pedigree":
                                pedigree = string.IsNullOrWhiteSpace(value.Value.ToString()) ? string.Empty : value.Value.ToString();
                                break;
                            case "preferredname":
                                preferredName = string.IsNullOrWhiteSpace(value.Value.ToString()) ? string.Empty : value.Value.ToString();
                                break;
                            case "role":
                                role = string.IsNullOrWhiteSpace(value.Value.ToString()) ? string.Empty : value.Value.ToString();
                                break;
                            /*
                                Also, The case for credentialtype & credentialvalue needs to be removed from API version 10 and above,
                                it was added back to support workflow team for V8.
                            */
                            case "credentialtype":
                                credentialType = string.IsNullOrWhiteSpace(value.Value.ToString()) ? string.Empty : value.Value.ToString();
                                break;
                            case "credentialvalue":
                                credentialValue = string.IsNullOrWhiteSpace(value.Value.ToString()) ? string.Empty : value.Value.ToString();
                                break;
                            case "credential":
                                var credType = (JObject) value.Value;
                                credentialType = GetValueFromJsonObject("type", credType);
                                credentialValue = GetValueFromJsonObject("value", credType);
                                break;
                            case "personfilter":
                                personFilter = string.IsNullOrWhiteSpace(value.Value.ToString()) ? string.Empty : value.Value.ToString();
                                break;
                            default:
                                throw new ArgumentException(string.Concat("Invalid filter value received: ", value.Key));
                        }
                    }
                }

                if (!string.IsNullOrEmpty(credentialType) && string.IsNullOrEmpty(credentialValue))
                {
                    throw new ArgumentException("credentialValue", "credentialValue is required when requesting a credentialType");
                }
                if (string.IsNullOrEmpty(credentialType) && !string.IsNullOrEmpty(credentialValue))
                {
                    throw new ArgumentException("credentialType", "credentialType is required when requesting a credentialValue");
                }
                
                // Disallow filtering by SSN/SIN for now
                if (!string.IsNullOrEmpty(credentialType))
                {
                    if (credentialType.ToLower() == "ssn")
                    {
                        throw new ArgumentException("credentialType", "Filtering by SSN is not supported at this time.");
                    }
                    if (credentialType.ToLower() == "sin") 
                    {
                        throw new ArgumentException("credentialType", "Filtering by SIN is not supported at this time.");
                    }
                }

                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                var pageOfItems = await _personService.GetPerson3NonCachedAsync(page.Offset, page.Limit, bypassCache,
                    title, firstName, middleName, lastNamePrefix, lastName, pedigree, preferredName,
                    role, credentialType, credentialValue, personFilter);

                return new PagedHttpActionResult<IEnumerable<Dtos.Person3>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);

            }
            catch (JsonReaderException e)
            {
                _logger.Error(e.ToString());
                
                throw CreateHttpResponseException(new IntegrationApiException("Deserialization Error",
                   IntegrationApiUtility.GetDefaultApiError("Error parsing JSON person search request.")));
            }
            catch
                (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        #endregion

        #region Post Methods

        /// <summary>
        /// Creates a person.
        /// </summary>
        /// <param name="person"><see cref="Person">Person</see> to create</param>
        /// <returns>Newly created <see cref="Person">Person</see></returns>
        [HttpPost]
        public async Task<Person> PostAsync([FromBody] Person person)
        {
            try
            {
                return await _personService.CreatePersonAsync(person);
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Create (POST) a new person
        /// </summary>
        /// <param name="person">DTO of the new person</param>
        /// <returns>A person object <see cref="Dtos.Person2"/> in HeDM format</returns>
        [HttpPost]
        public async Task<Dtos.Person2> PostPerson2Async(Dtos.Person2 person)
        {
            if (person == null)
            {
                throw CreateHttpResponseException("Request body must contain a valid Person.", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrEmpty(person.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null person id",
                    IntegrationApiUtility.GetDefaultApiError("Id is a required property.")));
            }

            try
            {
                return await _personService.CreatePerson2Async(person);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new person
        /// </summary>
        /// <param name="person">DTO of the new person</param>
        /// <returns>A person object <see cref="Dtos.Person3"/> in HeDM format</returns>
        [HttpPost]
        public async Task<Dtos.Person3> PostPerson3Async(Dtos.Person3 person)
        {
            if (person == null)
            {
                throw CreateHttpResponseException("Request body must contain a valid Person.", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrEmpty(person.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null person id",
                    IntegrationApiUtility.GetDefaultApiError("Id is a required property.")));
            }

            try
            {
                return await _personService.CreatePerson3Async(person);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create person credentials.
        /// </summary>
        /// <param name="personCredential"><see cref="Dtos.PersonCredential">PersonCredential</see> to update</param>
        /// <returns>Newly created <see cref="Dtos.PersonCredential">PersonCredential</see></returns>
        [HttpPost]
        public async Task<Dtos.PersonCredential> PostPersonCredentialAsync([FromBody] Dtos.PersonCredential personCredential)
        {
            //Create is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        #endregion

        #region Put Methods

        /// <summary>
        /// Updates a person.
        /// </summary>
        /// <param name="guid">GUID of the person to update</param>
        /// <param name="person"><see cref="Person">Person</see> to update</param>
        /// <returns>Newly updated <see cref="Person">person</see></returns>
        [HttpPut]
        public async Task<Person> PutAsync([FromUri] string guid, [FromBody] Person person)
        {
            if (string.IsNullOrEmpty(guid))
                throw new ArgumentNullException("guid", "Must provide a person guid for update");
            if (person == null)
                throw new ArgumentNullException("person", "Must provide a person for update");
            if (string.IsNullOrEmpty(person.Guid))
                throw new ArgumentNullException("personDto", "Must provide a guid for person update");
            if (guid != person.Guid)
                throw CreateHttpResponseException("GUID in URL is not the same as in request body.", HttpStatusCode.BadRequest);

            try
            {
                return await _personService.UpdatePersonAsync(person);
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }


        /// <summary>
        /// Update (PUT) an existing person
        /// </summary>
        /// <param name="guid">GUID of the person to update</param>
        /// <param name="person">DTO of the updated person</param>
        /// <returns>A Person2 object <see cref="Dtos.Person2"/> in HeDM format</returns>
        [HttpPut]
        public async Task<Dtos.Person2> PutPerson2Async([FromUri] string guid, [FromBody] Dtos.Person2 person)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null guid argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            if (person == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null person argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }
            if (guid.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                throw CreateHttpResponseException("Nil GUID cannot be used in PUT operation.", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrEmpty(person.Id))
            {
                person.Id = guid.ToLowerInvariant();
            }
            else if (!string.Equals(guid, person.Id, StringComparison.InvariantCultureIgnoreCase))
            {
                throw CreateHttpResponseException(new IntegrationApiException("GUID mismatch",
                    IntegrationApiUtility.GetDefaultApiError("GUID not the same as in request body.")));
            }

            try
            {
                await _personService.DoesUpdateViolateDataPrivacySettings(GetRouteResourceName(), person);
                return await _personService.UpdatePerson2Async(person);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Update (PUT) an existing person
        /// </summary>
        /// <param name="guid">GUID of the person to update</param>
        /// <param name="person">DTO of the updated person</param>
        /// <returns>A Person2 object <see cref="Dtos.Person3"/> in HeDM format</returns>
        [HttpPut]
        public async Task<Dtos.Person3> PutPerson3Async([FromUri] string guid, [FromBody] Dtos.Person3 person)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null guid argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            if (person == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null person argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }
            if (guid.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                throw CreateHttpResponseException("Nil GUID cannot be used in PUT operation.", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrEmpty(person.Id))
            {
                person.Id = guid.ToLowerInvariant();
            }
            else if (!string.Equals(guid, person.Id, StringComparison.InvariantCultureIgnoreCase))
            {
                throw CreateHttpResponseException(new IntegrationApiException("GUID mismatch",
                    IntegrationApiUtility.GetDefaultApiError("GUID not the same as in request body.")));
            }

            try
            {
                await _personService.DoesUpdateViolateDataPrivacySettings(GetRouteResourceName(), person);
                return await _personService.UpdatePerson3Async(person);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Updates certain person profile information: AddressConfirmationDateTime, EmailAddressConfirmationDateTime,
        /// PhoneConfirmationDateTime, EmailAddresses, Personal Phones and Addresses. LastChangedDateTime must match the last changed timestamp on the database
        /// Person record to ensure updates not occurring from two different sources at the same time. If no changes are found, a NotModified Http status code
        /// is returned. If required by configuration, users must be set up with permissions to perform these updates: UPDATE.OWN.EMAIL, UPDATE.OWN.PHONE, and 
        /// UPDATE.OWN.ADDRESS. 
        /// </summary>
        /// <param name="personId">The ID of the person profile to update.</param>
        /// <param name="profile"><see cref="Dtos.Base.Profile">Profile</see> to use to update</param>
        /// <returns>Newly updated <see cref="Dtos.Base.Profile">Profile</see></returns>
        [HttpPut]
        [Obsolete("Obsolete as of API 1.16. Use version 2 of this action instead.")]
        public async Task<Dtos.Base.Profile> PutProfileAsync([FromUri] string personId, [FromBody] Dtos.Base.Profile profile)
        {
            try
            {
                if (string.IsNullOrEmpty(personId))
                {
                    _logger.Error("PersonsController-PutProfileAsync: Must provide a person id in the request uri");
                    throw new Exception();
                }
                if (profile == null)
                {
                    _logger.Error("PersonsController-PutProfileAsync: Must provide a profile in the request body");
                    throw new Exception();
                }
                if (string.IsNullOrEmpty(profile.Id))
                {
                    _logger.Error("PersonsController-PutProfileAsync: Must provide a person Id in the request body");
                    throw new Exception();
                }
                if (personId != profile.Id)
                {
                    _logger.Error("PersonsController-PutProfileAsync: PersonID in URL is not the same as in request body");
                    throw new Exception();
                }

                return await _personService.UpdateProfileAsync(profile);
            }
            catch (PermissionsException permissionException)
            {
                throw CreateHttpResponseException(permissionException.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception)
            {
                throw CreateHttpResponseException("Unable to update profile information", HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Updates certain person profile information: AddressConfirmationDateTime, EmailAddressConfirmationDateTime,
        /// PhoneConfirmationDateTime, EmailAddresses, Personal Phones and Addresses. LastChangedDateTime must match the last changed timestamp on the database
        /// Person record to ensure updates not occurring from two different sources at the same time. If no changes are found, a NotModified Http status code
        /// is returned. If required by configuration, users must be set up with permissions to perform these updates: UPDATE.OWN.EMAIL, UPDATE.OWN.PHONE, and 
        /// UPDATE.OWN.ADDRESS. 
        /// </summary>
        /// <param name="personId">The ID of the person profile to update.</param>
        /// <param name="profile"><see cref="Dtos.Base.Profile">Profile</see> to use to update</param>
        /// <returns>Newly updated <see cref="Dtos.Base.Profile">Profile</see></returns>
        [HttpPut]
        public async Task<Dtos.Base.Profile> PutProfile2Async([FromUri] string personId, [FromBody] Dtos.Base.Profile profile)
        {
            try
            {
                if (string.IsNullOrEmpty(personId))
                {
                    _logger.Error("PersonsController-PutProfile2Async: Must provide a person id in the request uri");
                    throw new Exception();
                }
                if (profile == null)
                {
                    _logger.Error("PersonsController-PutProfile2Async: Must provide a profile in the request body");
                    throw new Exception();
                }
                if (string.IsNullOrEmpty(profile.Id))
                {
                    _logger.Error("PersonsController-PutProfile2Async: Must provide a person Id in the request body");
                    throw new Exception();
                }
                if (personId != profile.Id)
                {
                    _logger.Error("PersonsController-PutProfile2Async: PersonID in URL is not the same as in request body");
                    throw new Exception();
                }

                return await _personService.UpdateProfile2Async(profile);
            }
            catch (PermissionsException permissionException)
            {
                throw CreateHttpResponseException(permissionException.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception)
            {
                throw CreateHttpResponseException("Unable to update profile information", HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Creates a PersonCredential.
        /// </summary>
        /// <param name="personCredential"><see cref="Dtos.PersonCredential">PersonCredential</see> to create</param>
        /// <returns>Newly created <see cref="Dtos.PersonCredential">PersonCredential</see></returns>
        [HttpPut]
        public async Task<Dtos.PersonCredential> PutPersonCredentialAsync([FromBody] Dtos.PersonCredential personCredential)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Update a person's emergency information.
        /// </summary>
        /// <param name="emergencyInformation">An emergency information object</param>
        /// <returns>The updated emergency information object</returns>
        [HttpPut]
        public Dtos.Base.EmergencyInformation PutEmergencyInformation(Dtos.Base.EmergencyInformation emergencyInformation)
        {
            if (emergencyInformation == null)
            {
                throw CreateHttpResponseException("Request missing emergency information", HttpStatusCode.BadRequest);
            }
            try
            {
                var updatedEmergencyInformation = _emergencyInformationService.UpdateEmergencyInformation(emergencyInformation);

                return updatedEmergencyInformation;
            }
            catch (PermissionsException permissionException)
            {
                throw CreateHttpResponseException(permissionException.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception)
            {
                throw CreateHttpResponseException("Unable to update emergency information", HttpStatusCode.BadRequest);
            }
        }


        #endregion

        #region Delete Methods

        /// <summary>
        /// Delete (DELETE) an existing Person
        /// </summary>
        /// <param name="id">Id of the Person to delete</param>
        [HttpDelete]
        public async Task DeletePersonAsync(string id)
        {
            //Delete is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Delete (DELETE) an existing PersonCredential
        /// </summary>
        /// <param name="id">Id of the PersonCredential to delete</param>
        [HttpDelete]
        public async Task DeletePersonCredentialAsync(string id)
        {
            //Delete is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }
        #endregion

        #region Query Methods

        /// <summary>
        /// Queries a person by post.
        /// </summary>
        /// <param name="person"><see cref="Person">Person</see> to use for querying</param>
        /// <returns>List of matching <see cref="Person">persons</see></returns>
        [HttpPost]
        public async Task<IEnumerable<Person>> QueryPersonByPostAsync([FromBody] Person person)
        {
            try
            {
                return await _personService.QueryPersonByPostAsync(person);
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Queries a person by post.
        /// </summary>
        /// <param name="person"><see cref="Person2">Person</see> to use for querying</param>
        /// <returns>List of matching <see cref="Person2">persons</see></returns>
        [HttpPost, EedmResponseFilter]
        public async Task<IEnumerable<Person2>> QueryPerson2ByPostAsync([FromBody] Person2 person)
        {
            var bypassCache = false;

            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _personService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _personService.QueryPerson2ByPostAsync(person);
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Queries a person by post.
        /// </summary>
        /// <param name="person"><see cref="Person3">Person</see> to use for querying</param>
        /// <returns>List of matching <see cref="Person3">persons</see></returns>
        [HttpPost, EedmResponseFilter]
        public async Task<IEnumerable<Person3>> QueryPerson3ByPostAsync([FromBody] Person3 person)
        {
            var bypassCache = false;

            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _personService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _personService.QueryPerson3ByPostAsync(person);
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Query person by criteria and return the results of the matching algorithm
        /// </summary>
        /// <param name="criteria">The <see cref="Dtos.Base.PersonMatchCriteria">criteria</see> to query by.</param>
        /// <returns>List of matching <see cref="Dtos.Base.PersonMatchResult">results</see></returns>
        [HttpPost]
        public async Task<IEnumerable<Dtos.Base.PersonMatchResult>> QueryPersonMatchResultsByPostAsync([FromBody] Dtos.Base.PersonMatchCriteria criteria)
        {
            try
            {
                return await _personService.QueryPersonMatchResultsByPostAsync(criteria);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }


        /// <summary>
        /// Retrieves the matching Persons for the ids provided or searches keyword
        /// for the matching Persons if a first and last name are provided.  
        /// In the latter case, a middle name is optional.
        /// Matching is done by partial name; i.e., 'Bro' will match 'Brown' or 'Brodie'. 
        /// Capitalization is ignored.
        /// </summary>
        /// <remarks>the following input is legal
        /// <list type="bullet">
        /// <item>a Colleague id.  Short ids will be zero-padded.</item>
        /// <item>First Last</item>
        /// <item>First Middle Last</item>
        /// <item>Last, First</item>
        /// <item>Last, First Middle</item>
        /// </list>
        /// </remarks>
        /// <param name="criteria">Keyword can be either a Person ID or a first and last name.  A middle name is optional.</param>
        /// <returns>An enumeration of <see cref="Dtos.Base.Person">Person</see> with populated ID and first, middle and last names</returns>
        [HttpPost]
        public async Task<IEnumerable<Dtos.Base.Person>> QueryPersonNamesByPostAsync([FromBody] Dtos.Base.PersonNameQueryCriteria criteria)
        {
            try
            {
                return await _personService.QueryPersonNamesByPostAsync(criteria);
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
            
        }

        #endregion
    }
}