﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;
using System.Web.Http;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using System.Threading.Tasks;
using Ellucian.Colleague.Configuration.Licensing;
using System.Collections.Generic;

namespace Ellucian.Colleague.Api.Controllers
{
    /// <summary>
    /// Controller for Contact Types
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Base)]
    public class ContactTypesController : BaseCompressedApiController
    {
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the  Contact Types Controller class.
        /// </summary>
        /// <param name="logger">Interface to Logger</param>
        public ContactTypesController(ILogger logger)
        {
            _logger = logger;
        }

        /// <summary>        
        ///  Get a Contact-Type.
        /// </summary>
        /// <returns><see cref="Dtos.ContactType">ContactType</see></returns>
        [HttpGet]
        public async Task<Dtos.ContactType> GetContactTypesAsync()
        {
            //Get is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>        
        ///  Get a Contact-Type.
        /// </summary>
        /// <returns>Collection of <see cref="Dtos.ContactType">ContactTypes</see></returns>
        [HttpGet]
        public async Task<IEnumerable<Dtos.ContactType>> GetContactTypes2Async()
        {
            return new List<Dtos.ContactType>();
        }

        /// <summary>        
        ///  Get a Contact-Type.
        /// </summary>
        /// <param name="id">Id of the Contact Type to retrieve</param>
        /// <returns><see cref="Dtos.ContactType">ContactType</see></returns>
        [HttpGet]
        public async Task<Dtos.ContactType> GetContactTypesByIdAsync(string id)
        {
            //Get is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>        
        ///  Get a Contact-Type.
        /// </summary>
        /// <param name="id">Id of the Contact Type to retrieve</param>
        /// <returns><see cref="Dtos.ContactType">ContactType</see></returns>
        [HttpGet]
        public async Task<Dtos.ContactType> GetContactTypesById2Async(string id)
        {
            try
            {
                throw new System.Exception(string.Format("No contact type was found for guid {0}.", id));
            }
            catch (System.Exception e)
            {
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), System.Net.HttpStatusCode.NotFound);
            }
        }


        /// <summary>        
        /// Creates an Contact Type
        /// </summary>
        /// <param name="contactType"><see cref="Dtos.ContactType">ContactType</see> to create</param>
        /// <returns>Newly created <see cref="Dtos.ContactType">ContactType</see></returns>
        [HttpPost]
        public async Task <Dtos.ContactType> PostContactTypesAsync([FromBody] Dtos.ContactType contactType)
        {
            //Create is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>        
        /// Updates an ContactType.
        /// </summary>
        /// <param name="id">Id of the Contact Type to update</param>
        /// <param name="contactType"><see cref="Dtos.ContactType">ContactType</see> to update</param>
        /// <returns>Updated <see cref="Dtos.ContactType">ContactType</see></returns>
        [HttpPut]
        public async Task<Dtos.ContactType> PutContactTypesAsync([FromUri] string id, [FromBody] Dtos.ContactType contactType)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Delete (DELETE) an existing Contact Type
        /// </summary>
        /// <param name="id">Id of the  Contact Type to delete</param>
        [HttpDelete]
        public async Task DeleteContactTypesAsync([FromUri] string id)
        {
            //Delete is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }
    }
}