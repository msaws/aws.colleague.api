﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Web.Http.Exceptions;

namespace Ellucian.Colleague.Api.Controllers.Base
{
    /// <summary>
    /// Provides access to Ethnicity data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Base)]
    public class EthnicitiesController : BaseCompressedApiController
    {
        private readonly IReferenceDataRepository _referenceDataRepository;
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly IDemographicService _demographicService;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the EthnicitiesController class.
        /// </summary>
        /// <param name="adapterRegistry">Adapter registry of type <see cref="IAdapterRegistry">IAdapterRegistry</see></param>
        /// <param name="referenceDataRepository">Repository of type <see cref="IReferenceDataRepository">IReferenceDataRepository</see></param>
        /// <param name="demographicService">Service of type <see cref="IDemographicService">IDemographicService</see></param>
        /// <param name="logger">Interface to Logger</param>
        public EthnicitiesController(IAdapterRegistry adapterRegistry, IReferenceDataRepository referenceDataRepository, IDemographicService demographicService, ILogger logger)
        {
            _adapterRegistry = adapterRegistry;
            _referenceDataRepository = referenceDataRepository;
            _demographicService = demographicService;
            _logger = logger;
        }

        /// <summary>
        /// Gets all of the ethnicities.
        /// </summary>
        /// <returns>All <see cref="Ethnicity">Ethnicities</see></returns>
        public async Task<IEnumerable<Ethnicity>> GetAsync()
        {
            var ethnicityCollection = await _referenceDataRepository.EthnicitiesAsync();

            // Get the right adapter for the type mapping
            var ethnicityDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Base.Entities.Ethnicity, Ethnicity>();

            // Map the ethnicity entity to the program DTO
            var ethnicityDtoCollection = new List<Ethnicity>();
            foreach (var ethnicity in ethnicityCollection)
            {
                ethnicityDtoCollection.Add(ethnicityDtoAdapter.MapToType(ethnicity));
            }

            return ethnicityDtoCollection;
        }

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Retrieves all ethnicities. If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <returns>All Ethnicity objects.</returns>
        [Obsolete("Obsolete as of HeDM Version 4, use Accept Header Version 4 instead.")]
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.Ethnicity>> GetEthnicitiesAsync()
        {
            bool bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                return await _demographicService.GetEthnicitiesAsync(bypassCache);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(ex));
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Retrieves an ethnicity by GUID.
        /// </summary>
        /// <returns>An Ethnicity</returns>
        [Obsolete("Obsolete as of HeDM Version 4, use Accept Header Version 4 instead.")]
        public async Task<Ellucian.Colleague.Dtos.Ethnicity> GetEthnicityByGuidAsync(string guid)
        {
            try
            {
                return await _demographicService.GetEthnicityByGuidAsync(guid);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }
        }

        /// <remarks>For use with Ellucian HeDM Version 4</remarks>
        /// <summary>
        /// Retrieves all ethnicities. If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <returns>All Ethnicity objects.</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.Ethnicity2>> GetEthnicities2Async()
        {
            bool bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                return await _demographicService.GetEthnicities2Async(bypassCache);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(ex));
            }
        }

        /// <remarks>For use with Ellucian HeDM Version 4</remarks>
        /// <summary>
        /// Retrieves an ethnicity by ID.
        /// </summary>
        /// <returns>An Ethnicity</returns>
        public async Task<Ellucian.Colleague.Dtos.Ethnicity2> GetEthnicityById2Async(string id)
        {
            try
            {
                return await _demographicService.GetEthnicityById2Async(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }
        }

        /// <summary>
        /// Updates a Ethnicity.
        /// </summary>
        /// <param name="ethnicity"><see cref="Ethnicity2">Ethnicity</see> to update</param>
        /// <returns>Newly updated <see cref="Ethnicity2">Ethnicity</see></returns>
        [HttpPut]
        public async Task<Dtos.Ethnicity2> PutEthnicitiesAsync([FromBody] Dtos.Ethnicity2 ethnicity)
        {
            //Create is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Creates a Ethnicity.
        /// </summary>
        /// <param name="ethnicity"><see cref="Ethnicity2">Ethnicity</see> to create</param>
        /// <returns>Newly created <see cref="Ethnicity2">Ethnicity</see></returns>
        [HttpPost]
        public async Task<Dtos.Ethnicity2> PostEthnicitiesAsync([FromBody] Dtos.Ethnicity2 ethnicity)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Delete (DELETE) an existing Ethnicity
        /// </summary>
        /// <param name="id">Id of the Ethnicity to delete</param>
        [HttpDelete]
        public async Task DeleteEthnicitiesAsync(string id)
        {
            //Delete is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }
    }
}
