﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Web.Http.Exceptions;

namespace Ellucian.Colleague.Api.Controllers.Base
{
    /// <summary>
    /// Provides access to Bargaining Units data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Base)]
    public class BargainingUnitsController : BaseCompressedApiController
    {
        private readonly IDemographicService _demographicService;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the BargainingUnitsController class.
        /// </summary>
        /// <param name="demographicService">Service of type <see cref="IDemographicService">IDemographicService</see></param>
        /// <param name="logger">Interface to Logger</param>
        public BargainingUnitsController(IDemographicService demographicService, ILogger logger)
        {
            _demographicService = demographicService;
            _logger = logger;
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM VERSION 7</remarks>
        /// <summary>
        /// Retrieves all bargaining units.
        /// </summary>
        /// <returns>All BargainingUnit objects.</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.BargainingUnit>> GetBargainingUnitsAsync()
        {
            try
            {
                bool bypassCache = false;
                if (Request.Headers.CacheControl != null)
                {
                    if (Request.Headers.CacheControl.NoCache)
                    {
                        bypassCache = true;
                    }
                }
                return await _demographicService.GetBargainingUnitsAsync(bypassCache);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(ex));
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM VERSION 7</remarks>
        /// <summary>
        /// Retrieves a bargaining unit by ID.
        /// </summary>
        /// <returns>A <see cref="Ellucian.Colleague.Dtos.BargainingUnit">BargainingUnit.</see></returns>
        public async Task<Ellucian.Colleague.Dtos.BargainingUnit> GetBargainingUnitByIdAsync(string id)
        {
            try
            {
                return await _demographicService.GetBargainingUnitByGuidAsync(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(ex));
            }
        }

        /// <summary>
        /// Updates a BargainingUnit.
        /// </summary>
        /// <param name="bargainingUnit"><see cref="BargainingUnit">BargainingUnit</see> to update</param>
        /// <returns>Newly updated <see cref="BargainingUnit">BargainingUnit</see></returns>
        [HttpPut]
        public async Task<Dtos.BargainingUnit> PutBargainingUnitAsync([FromBody] Dtos.BargainingUnit bargainingUnit)
        {
            //Create is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Creates a BargainingUnit.
        /// </summary>
        /// <param name="bargainingUnit"><see cref="BargainingUnit">BargainingUnit</see> to create</param>
        /// <returns>Newly created <see cref="BargainingUnit">BargainingUnit</see></returns>
        [HttpPost]
        public async Task<Dtos.BargainingUnit> PostBargainingUnitAsync([FromBody] Dtos.BargainingUnit bargainingUnit)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Delete (DELETE) an existing BargainingUnit
        /// </summary>
        /// <param name="id">Id of the BargainingUnit to delete</param>
        [HttpDelete]
        public async Task DeleteBargainingUnitAsync(string id)
        {
            //Delete is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }
    }
}
