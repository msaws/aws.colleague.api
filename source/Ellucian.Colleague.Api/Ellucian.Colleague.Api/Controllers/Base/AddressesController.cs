﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using Ellucian.Web.Http.Controllers;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Web.Http;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Api.Licensing;
using slf4net;

using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.Base
{
    /// <summary>
    /// Provides access to Address data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Base)]
    public class AddressesController : BaseCompressedApiController
    {
        private readonly IAddressRepository _addressRepository;
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly ILogger _logger;
        private readonly IAddressService _addressService;

        /// <summary>
        /// Initializes a new instance of the AddressesController class.
        /// </summary>
        /// <param name="adapterRegistry">Adapter registry of type <see cref="IAdapterRegistry">IAdapterRegistry</see></param>
        /// <param name="addressService">Service of type <see cref="IAddressService">IAddressService</see></param>
        /// <param name="addressRepository">Repository of type <see cref="IAddressRepository">IAddressRepository</see></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public AddressesController(IAdapterRegistry adapterRegistry, IAddressService addressService, IAddressRepository addressRepository, ILogger logger)
        {
            _adapterRegistry = adapterRegistry;
            _addressService = addressService;
            _addressRepository = addressRepository;
            _logger = logger;
        }

        #region Get Methods
        /// <summary>
        /// Get all current addresses for a person
        /// </summary>
        /// <param name="personId">Person to get addresses for</param>
        /// <returns>List of Address Objects <see cref="Ellucian.Colleague.Dtos.Base.Address">Address</see></returns>
        public IEnumerable<Ellucian.Colleague.Dtos.Base.Address> GetPersonAddresses(string personId)
        {
            if (string.IsNullOrEmpty(personId))
            {
                _logger.Error("Invalid personId parameter");
                throw CreateHttpResponseException("The personId is required.", HttpStatusCode.BadRequest);
            }
            try
            {
                var addressDtoCollection = new List<Ellucian.Colleague.Dtos.Base.Address>();
                var addressCollection = _addressRepository.GetPersonAddresses(personId);
                // Get the right adapter for the type mapping
                var addressDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Base.Entities.Address, Ellucian.Colleague.Dtos.Base.Address>();
                // Map the Address entity to the Address DTO
                foreach (var address in addressCollection)
                {
                    addressDtoCollection.Add(addressDtoAdapter.MapToType(address));
                }

                return addressDtoCollection;
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                throw CreateHttpResponseException(e.Message);
            }
        }

        /// <summary>
        /// Get a list of Addresses from a list of Person keys
        /// </summary>
        /// <param name="criteria">Address Query Criteria including PersonIds list.</param>
        /// <returns>List of Address Objects <see cref="Ellucian.Colleague.Dtos.Base.Address">Address</see></returns>
        public IEnumerable<Ellucian.Colleague.Dtos.Base.Address> QueryAddresses(AddressQueryCriteria criteria)
        {
            if (criteria.PersonIds == null || criteria.PersonIds.Count() <= 0)
            {
                _logger.Error("Invalid personIds parameter: null or empty.");
                throw CreateHttpResponseException("No person IDs provided.", HttpStatusCode.BadRequest);
            }
            try
            {
                var addressDtoCollection = new List<Ellucian.Colleague.Dtos.Base.Address>();
                var addressCollection = _addressRepository.GetPersonAddressesByIds(criteria.PersonIds.ToList());
                // Get the right adapter for the type mapping
                var addressDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Base.Entities.Address, Ellucian.Colleague.Dtos.Base.Address>();
                // Map the Address entity to the Address DTO
                foreach (var address in addressCollection)
                {
                    addressDtoCollection.Add(addressDtoAdapter.MapToType(address));
                }

                return addressDtoCollection;
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                _logger.Error(e, "QueryAddresses error");
                throw CreateHttpResponseException(e.Message);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Read (GET) a Address using a GUID
        /// </summary>
        /// <param name="guid">GUID to desired Address</param>
        /// <returns>An address object <see cref="Dtos.Addresses"/> in HeDM format</returns>
        [EedmResponseFilter]
        public async Task<Dtos.Addresses> GetAddressByGuidAsync(string guid)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            if (string.IsNullOrEmpty(guid))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                AddDataPrivacyContextProperty((await _addressService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _addressService.GetAddressesByGuidAsync(guid);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }
        
        /// <summary>
        /// Get all addresses with paging
        /// </summary>
        /// <param name="page"></param>
        /// <returns></returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 200), EedmResponseFilter]
        public async Task<IHttpActionResult> GetAddressesAsync(Paging page)
        {
            try
            {
                var bypassCache = false;
                if (Request.Headers.CacheControl != null)
                {
                    if (Request.Headers.CacheControl.NoCache)
                    {
                        bypassCache = true;
                    }
                }
                if (page == null)
                {
                    page = new Paging(200, 0);
                }

                AddDataPrivacyContextProperty((await _addressService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await _addressService.GetAddressesAsync(page.Offset, page.Limit, bypassCache);
                return new PagedHttpActionResult<IEnumerable<Dtos.Addresses>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        #endregion

        #region Put Methods
        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Update a Address Record in Colleague (Not Supported)
        /// </summary>
        /// <param name="id">Guid to Address in Colleague</param>
        /// <param name="address"><see cref="Dtos.Addresses">Address</see> to update</param>
        /// <returns>An address object <see cref="Dtos.Addresses"/> in HeDM format</returns>
        [HttpPut]
        public async Task<Dtos.Addresses> PutAddressAsync([FromUri] string id, [FromBody] Dtos.Addresses address)
        {
            if (address == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null request body",
                    IntegrationApiUtility.GetDefaultApiError("The request body must be specified in the request.")));
            }
            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            if (id != address.Id)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Incorrect id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID in the URL doesn't match the GUID in the body of the request.")));
            }
            try
            {
                await _addressService.DoesUpdateViolateDataPrivacySettings(GetRouteResourceName(), address);
                return await _addressService.PutAddressesAsync(id, address);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }
        #endregion

        #region Post Methods
        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Create a Address Record in Colleague (Not Supported)
        /// </summary>
        /// <param name="address"><see cref="Dtos.Addresses">Address</see> to create</param>
        /// <returns>An address object <see cref="Dtos.Addresses"/> in HeDM format</returns>
        [HttpPost]
        public async Task<Dtos.Addresses> PostAddressAsync([FromBody] Dtos.Addresses address)
        {
            // We will be rejecting a POST on Addresses at this time.  I am not removing the code yet in case
            // we need to implement this in the future.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
            //    if (address == null)
            //    {
            //        throw CreateHttpResponseException(new IntegrationApiException("Null request body",
            //            IntegrationApiUtility.GetDefaultApiError("The request body must be specified in the request.")));
            //    }
            //    try
            //    {
            //        return await _addressService.PostAddressesAsync(address);
            //    }
            //    catch (ArgumentException e)
            //    {
            //        _logger.Error(e.ToString());
            //        throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            //    }
            //    catch (RepositoryException e)
            //    {
            //        _logger.Error(e.ToString());
            //        throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            //    }
            //    catch (KeyNotFoundException e)
            //    {
            //        _logger.Error(e.ToString());
            //        throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            //    }
            //    catch (Exception e)
            //    {
            //        _logger.Error(e.ToString());
            //        throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            //    }
        }
        #endregion

        #region Delete Methods
        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Delete an existing Address in Colleague (Not Supported)
        /// </summary>
        /// <param name="id">Unique ID representing the Address to delete</param>
        [HttpDelete]
        public async Task DeleteAddressAsync(string id)
        {
            try
            {
                await _addressService.DeleteAddressesAsync(id);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        #endregion
    }
}