﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.ComponentModel;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;

namespace Ellucian.Colleague.Api.Controllers
{
    /// <summary>
    /// Provides access to Integration Configuration data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Base)]
    public class ConfigurationController : BaseCompressedApiController
    {
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly IConfigurationService _configurationService;
        private readonly IProxyService _proxyService;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the ConfigurationController class.
        /// </summary>
        /// <param name="adapterRegistry">Adapter registry of type <see cref="IAdapterRegistry">IAdapterRegistry</see></param>
        /// <param name="configurationService">Service of type <see cref="IConfigurationService">IConfigurationService</see></param>
        /// <param name="proxyService">Service of type <see cref="IProxyService">IProxyService</see></param>
        /// <param name="logger">Interface to Logger</param>
        public ConfigurationController(IAdapterRegistry adapterRegistry, IConfigurationService configurationService, IProxyService proxyService, ILogger logger)
        {
            _adapterRegistry = adapterRegistry;
            _configurationService = configurationService;
            _proxyService = proxyService;
            _logger = logger;
        }

        /// <remarks>FOR USE WITH ELLUCIAN CDM</remarks>
        /// <summary>
        /// Retrieves an integration configuration.
        /// </summary>
        /// <returns>An <see cref="IntegrationConfiguration">IntegrationConfiguration</see> information</returns>
        public Ellucian.Colleague.Dtos.Base.IntegrationConfiguration Get(string configId)
        {
            try
            {
                return _configurationService.GetIntegrationConfiguration(configId);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// This method gets Tax Form Configuration for the tax form passed in.
        /// </summary>
        /// <param name="taxFormId">The tax form (W-2, 1095-C, 1098-T, etc.)</param>
        /// <returns>Tax Form Configuration for the type of tax form.</returns>
        public async Task<TaxFormConfiguration> GetTaxFormConfigurationAsync(TaxForms taxFormId)
        {
            var taxFormConfiguration = await this._configurationService.GetTaxFormConsentConfigurationAsync(taxFormId);

            return taxFormConfiguration;
        }

        /// <summary>
        /// Gets the proxy configuration
        /// </summary>
        /// <returns>Proxy configuration information.</returns>
        public async Task<ProxyConfiguration> GetProxyConfigurationAsync()
        {
            try
            {
                return await _proxyService.GetProxyConfigurationAsync();
            }
            catch (Exception e)
            {
                _logger.Info(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Gets the User Profile Configuration
        /// </summary>
        /// <returns><see cref="UserProfileConfiguration">User Profile Configuration</see></returns>
        [Obsolete("Obsolete as of API 1.16. Use version 2 of this API instead.")]
        public async Task<UserProfileConfiguration> GetUserProfileConfigurationAsync()
        {
            try
            {
                return await _configurationService.GetUserProfileConfigurationAsync();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while retrieving User Profile Configuration: " + ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Gets the User Profile Configuration
        /// </summary>
        /// <returns><see cref="UserProfileConfiguration2">User Profile Configuration</see></returns>
        public async Task<UserProfileConfiguration2> GetUserProfileConfiguration2Async()
        {
            try
            {
                return await _configurationService.GetUserProfileConfiguration2Async();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while retrieving User Profile Configuration: " + ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Gets the Emergency Information Configuration
        /// </summary>
        /// <returns><see cref="EmergencyInformationConfiguration">Emergency Information Configuration</see></returns>
        [Obsolete("Obsolete as of API 1.16. Use version 2 of this API instead.")]
        public async Task<EmergencyInformationConfiguration> GetEmergencyInformationConfigurationAsync()
        {
            try
            {
                return await _configurationService.GetEmergencyInformationConfigurationAsync();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while retrieving Emergency Information Configuration: " + ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Gets the Emergency Information Configuration
        /// </summary>
        /// <returns><see cref="EmergencyInformationConfiguration2">Emergency Information Configuration</see></returns>
        public async Task<EmergencyInformationConfiguration2> GetEmergencyInformationConfiguration2Async()
        {
            try
            {
                return await _configurationService.GetEmergencyInformationConfiguration2Async();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while retrieving Emergency Information Configuration: " + ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Gets the Restriction Configuration
        /// </summary>
        /// <returns><see cref="RestrictionConfiguration">Restriction Configuration</see></returns>
        public async Task<RestrictionConfiguration> GetRestrictionConfigurationAsync()
        {
            try
            {
                return await _configurationService.GetRestrictionConfigurationAsync();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while retrieving Restriction Configuration: ", ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Gets the Privacy Configuration
        /// </summary>
        /// <returns><see cref="PrivacyConfiguration">Privacy Configuration</see></returns>
        public async Task<PrivacyConfiguration> GetPrivacyConfigurationAsync()
        {
            try
            {
                return await _configurationService.GetPrivacyConfigurationAsync();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while retrieving Privacy Configuration: ", ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Gets the Organizational Relationship Configuration
        /// </summary>
        /// <returns><see cref="OrganizationalRelationshipConfiguration">OrganizationalRelationship Configuration</see></returns>
        public async Task<OrganizationalRelationshipConfiguration> GetOrganizationalRelationshipConfigurationAsync()
        {
            try
            {
                return await _configurationService.GetOrganizationalRelationshipConfigurationAsync();
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while retrieving OrganizationalRelationship Configuration: ", ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }
    }
}
