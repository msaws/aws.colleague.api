﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Web.Http.Controllers;
using System.Web.Http;
using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using slf4net;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.Student
{
    /// <summary>
    /// Provides access to StudentAptitudeAssessments
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class StudentAptitudeAssessmentsController : BaseCompressedApiController
    {
        private readonly IStudentAptitudeAssessmentsService _studentAptitudeAssessmentsService;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the StudentAptitudeAssessmentsController class.
        /// </summary>
        /// <param name="studentAptitudeAssessmentsService">Service of type <see cref="IStudentAptitudeAssessmentsService">IStudentAptitudeAssessmentsService</see></param>
        /// <param name="logger">Interface to logger</param>
        public StudentAptitudeAssessmentsController(IStudentAptitudeAssessmentsService studentAptitudeAssessmentsService, ILogger logger)
        {
            _studentAptitudeAssessmentsService = studentAptitudeAssessmentsService;
            _logger = logger;
        }

        /// <summary>
        /// Return all studentAptitudeAssessments
        /// </summary>
        /// <returns>List of StudentAptitudeAssessments <see cref="Dtos.StudentAptitudeAssessments"/> objects representing matching studentAptitudeAssessments</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 200), EedmResponseFilter]
        public async Task<IHttpActionResult> GetStudentAptitudeAssessmentsAsync(Paging page)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                if (page == null)
                {
                    page = new Paging(200, 0);
                }

                AddDataPrivacyContextProperty((await _studentAptitudeAssessmentsService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await _studentAptitudeAssessmentsService.GetStudentAptitudeAssessmentsAsync(page.Offset, page.Limit, bypassCache);
                return new PagedHttpActionResult<IEnumerable<Dtos.StudentAptitudeAssessments>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Read (GET) a studentAptitudeAssessments using a GUID
        /// </summary>
        /// <param name="guid">GUID to desired studentAptitudeAssessments</param>
        /// <returns>A studentAptitudeAssessments object <see cref="Dtos.StudentAptitudeAssessments"/> in EEDM format</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.StudentAptitudeAssessments> GetStudentAptitudeAssessmentsByGuidAsync(string guid)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            if (string.IsNullOrEmpty(guid))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                AddDataPrivacyContextProperty((await _studentAptitudeAssessmentsService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _studentAptitudeAssessmentsService.GetStudentAptitudeAssessmentsByGuidAsync(guid);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new studentAptitudeAssessments
        /// </summary>
        /// <param name="studentAptitudeAssessments">DTO of the new studentAptitudeAssessments</param>
        /// <returns>A studentAptitudeAssessments object <see cref="Dtos.StudentAptitudeAssessments"/> in EEDM format</returns>
        [HttpPost]
        public async Task<Dtos.StudentAptitudeAssessments> PostStudentAptitudeAssessmentsAsync([FromBody] Dtos.StudentAptitudeAssessments studentAptitudeAssessments)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Update (PUT) an existing studentAptitudeAssessments
        /// </summary>
        /// <param name="guid">GUID of the studentAptitudeAssessments to update</param>
        /// <param name="studentAptitudeAssessments">DTO of the updated studentAptitudeAssessments</param>
        /// <returns>A studentAptitudeAssessments object <see cref="Dtos.StudentAptitudeAssessments"/> in EEDM format</returns>
        [HttpPut]
        public async Task<Dtos.StudentAptitudeAssessments> PutStudentAptitudeAssessmentsAsync([FromUri] string guid, [FromBody] Dtos.StudentAptitudeAssessments studentAptitudeAssessments)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Delete (DELETE) a studentAptitudeAssessments
        /// </summary>
        /// <param name="guid">GUID to desired studentAptitudeAssessments</param>
        [HttpDelete]
        public async Task DeleteStudentAptitudeAssessmentsAsync(string guid)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }
    }
}