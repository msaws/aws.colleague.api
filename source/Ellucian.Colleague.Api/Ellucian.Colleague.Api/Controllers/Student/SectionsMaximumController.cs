﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http;
using Newtonsoft.Json;

namespace Ellucian.Colleague.Api.Controllers.Student
{
    /// <summary>
    /// Provides access to course Section data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class SectionsMaximumController : BaseCompressedApiController
    {
        private readonly ISectionCoordinationService _sectionCoordinationService;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the SectionsController class.
        /// </summary>
        /// <param name="sectionCoordinationService">Service of type <see cref="ISectionCoordinationService">ISectionCoordinationService</see></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public SectionsMaximumController(ISectionCoordinationService sectionCoordinationService, ILogger logger)
        {
            _sectionCoordinationService = sectionCoordinationService;
            _logger = logger;
        }

        #region HeDM Methods
        
        /// <summary>
        /// Return a list of SectionMaximum objects based on selection criteria.
        /// </summary>
        /// <param name="page">Section page Contains ...page...</param>
        /// <param name="title">Section Title Contains ...title...</param>
        /// <param name="startOn">Section starts on or after this date</param>
        /// <param name="endOn">Section ends on or before this date</param>
        /// <param name="code">Section Name Contains ...code...</param>
        /// <param name="number">Section Number equal to</param>
        /// <param name="instructionalPlatform">Learning Platform equal to (guid)</param>
        /// <param name="academicPeriod">Section Term equal to (guid)</param>
        /// <param name="academicLevels">Section Academic Level equal to (guid)</param>
        /// <param name="course">Section Course equal to (guid)</param>
        /// <param name="site">Section Location equal to (guid)</param>
        /// <param name="status">Section Status matches closed, open, pending, or cancelled</param>
        /// <param name="owningOrganizations">Section Department equal to (guid)</param>
        /// <returns>List of SectionMaximum <see cref="Dtos.SectionMaximum"/> objects representing matching SectionMaximum</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100)]
        public async Task<IHttpActionResult> GetHedmSectionsMaximumAsync(Paging page, [FromUri] string title = "", [FromUri] string startOn = "", [FromUri] string endOn = "",
            [FromUri] string code = "", [FromUri] string number = "", [FromUri] string instructionalPlatform = "", [FromUri] string academicPeriod = "",
            [FromUri] string academicLevels = "", [FromUri] string course = "", [FromUri] string site = "", [FromUri] string status = "", [FromUri] string owningOrganizations = "")
        {
            string criteria = title + startOn + endOn + code + number + instructionalPlatform + academicPeriod + academicLevels + course + site + status + owningOrganizations;
            if (criteria == string.Empty)
            {
                throw new ArgumentNullException("title, startOn, endOn, code, number, instructionalPlatform, academicPeriod, academicLevels, course, site, status, owningOrganizations","No criteria specified for selection of sections");
            }

            try
            {
                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                var pageOfItems = await _sectionCoordinationService.GetSectionsMaximumAsync(page.Offset, page.Limit, title, startOn, endOn, code, number, instructionalPlatform, academicPeriod, academicLevels, course, site, status, owningOrganizations);

                return new PagedHttpActionResult<IEnumerable<Dtos.SectionMaximum>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Return a list of SectionMaximum objects based on selection criteria.
        /// </summary>
        /// <param name="page">Section page Contains ...page...</param>
        /// <param name="title">Section Title Contains ...title...</param>
        /// <param name="startOn">Section starts on or after this date</param>
        /// <param name="endOn">Section ends on or before this date</param>
        /// <param name="code">Section Name Contains ...code...</param>
        /// <param name="number">Section Number equal to</param>
        /// <param name="instructionalPlatform">Learning Platform equal to (guid)</param>
        /// <param name="academicPeriod">Section Term equal to (guid)</param>
        /// <param name="academicLevels">Section Academic Level equal to (guid)</param>
        /// <param name="course">Section Course equal to (guid)</param>
        /// <param name="site">Section Location equal to (guid)</param>
        /// <param name="status">Section Status matches closed, open, pending, or cancelled</param>
        /// <param name="owningInstitutionUnits">Section Department equal to (guid)</param>
        /// <returns>List of SectionMaximum <see cref="Dtos.SectionMaximum2"/> objects representing matching SectionMaximum</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetHedmSectionsMaximum2Async(Paging page, [FromUri] string title = "", [FromUri] string startOn = "", [FromUri] string endOn = "",
            [FromUri] string code = "", [FromUri] string number = "", [FromUri] string instructionalPlatform = "", [FromUri] string academicPeriod = "",
            [FromUri] string academicLevels = "", [FromUri] string course = "", [FromUri] string site = "", [FromUri] string status = "", [FromUri] string owningInstitutionUnits = "")
        {
            string criteria = title + startOn + endOn + code + number + instructionalPlatform + academicPeriod + academicLevels + course + site + status + owningInstitutionUnits;
            if (criteria == string.Empty)
            {
                throw new ArgumentNullException("title, startOn, endOn, code, number, instructionalPlatform, academicPeriod, academicLevels, course, site, status, owningInstitutionUnits", "No criteria specified for selection of sections");
            }

            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                AddDataPrivacyContextProperty((await _sectionCoordinationService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await _sectionCoordinationService.GetSectionsMaximum2Async(page.Offset, page.Limit, title, startOn, endOn, code, number, instructionalPlatform, academicPeriod, academicLevels, course, site, status, owningInstitutionUnits);

                return new PagedHttpActionResult<IEnumerable<Dtos.SectionMaximum2>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Return a list of SectionMaximum objects based on selection criteria.
        /// </summary>
        /// <param name="page">Section page Contains ...page...</param>
        /// <param name="criteria"> filter criteria</param>
        /// <returns>List of SectionMaximum <see cref="Dtos.SectionMaximum3"/> objects representing matching SectionMaximum</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetHedmSectionsMaximum3Async(Paging page, [FromUri] string criteria = "")
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                string title = string.Empty;
                string startOn = string.Empty;
                string endOn = string.Empty;
                string code = string.Empty;
                string number = string.Empty;
                string instructionalPlatform = string.Empty;
                string academicPeriod = string.Empty;
                string academicLevels = string.Empty;
                string course = string.Empty;
                string site = string.Empty;
                string status = string.Empty;
                string owningInstitutionUnits = string.Empty;

                var criteriaValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(criteria);

                if (criteriaValues != null)
                {
                    foreach (var value in criteriaValues)
                    {

                        switch (value.Key.ToLower())
                        {
                            case "title":
                                title = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "starton":
                                startOn = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "endon":
                                endOn = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "code":
                                code = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "number":
                                number = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "instructionalplatform":
                                instructionalPlatform = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "academicperiod":
                                academicPeriod = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "academiclevels":
                                academicLevels = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "course":
                                course = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "site":
                                site = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "status":
                                status = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "owninginstitutionunits":
                                owningInstitutionUnits = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            default:
                                throw new ArgumentException(string.Concat("Invalid filter value received: ", value.Key));
                        }
                    }
                }

                AddDataPrivacyContextProperty((await _sectionCoordinationService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await _sectionCoordinationService.GetSectionsMaximum3Async(page.Offset, page.Limit, title, startOn, endOn, code, number, instructionalPlatform, academicPeriod, academicLevels, course, site, status, owningInstitutionUnits);

                return new PagedHttpActionResult<IEnumerable<Dtos.SectionMaximum3>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }


        /// <summary>
        /// Read (GET) a section using a GUID
        /// </summary>
        /// <param name="id">GUID to desired section</param>
        /// <returns>A SectionMaximum object <see cref="Dtos.SectionMaximum"/> in HeDM format</returns>
        [HttpGet]
        public async Task<Dtos.SectionMaximum> GetHedmSectionMaximumByGuidAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                return await _sectionCoordinationService.GetSectionMaximumByGuidAsync(id);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Read (GET) a section using a GUID
        /// </summary>
        /// <param name="id">GUID to desired section</param>
        /// <returns>A SectionMaximum object <see cref="Dtos.SectionMaximum"/> in HeDM format</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.SectionMaximum2> GetHedmSectionMaximumByGuid2Async(string id)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                AddDataPrivacyContextProperty((await _sectionCoordinationService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _sectionCoordinationService.GetSectionMaximumByGuid2Async(id);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Read (GET) a section using a GUID
        /// </summary>
        /// <param name="id">GUID to desired section</param>
        /// <returns>A SectionMaximum object <see cref="Dtos.SectionMaximum3"/> in HeDM format</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.SectionMaximum3> GetHedmSectionMaximumByGuid3Async(string id)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                AddDataPrivacyContextProperty((await _sectionCoordinationService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _sectionCoordinationService.GetSectionMaximumByGuid3Async(id);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }


        /// <summary>
        /// Create (POST) a new SectionMaximum
        /// </summary>
        /// <param name="sectionMaximum">DTO of the new section</param>
        /// <returns>A SectionMaximum object <see cref="Dtos.SectionMaximum"/> in HeDM format</returns>
        [HttpPost]
        public async Task<Dtos.SectionMaximum> PostHedmSectionMaximumAsync(Dtos.SectionMaximum sectionMaximum)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Update (PUT) an existing section
        /// </summary>
        /// <param name="id">GUID of the section to update</param>
        /// <param name="sectionMaximum">DTO of the updated section</param>
        /// <returns>A SectionMaximum object <see cref="Dtos.SectionMaximum"/> in HeDM format</returns>
        [HttpPut]
        public async Task<Dtos.SectionMaximum> PutHedmSectionMaximumAsync([FromUri] string id, [FromBody] Dtos.SectionMaximum sectionMaximum)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Delete (DELETE) a section
        /// </summary>
        /// <param name="id">GUID to desired section</param>
        /// <returns>A section object <see cref="Dtos.Section"/> in HeDM format</returns>
        [HttpDelete]
        public async Task DeleteHedmSectionMaximumByGuidAsync(string id)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }
        
        #endregion

    }
}
