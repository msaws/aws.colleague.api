﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Linq;
using System.ComponentModel;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;
using System.Collections.Generic;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http;
using Newtonsoft.Json;

namespace Ellucian.Colleague.Api.Controllers
{
    /// <summary>
    /// Provides access to course Section data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class InstructionalEventsController : BaseCompressedApiController
    {
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly ILogger _logger;
        private readonly ISectionCoordinationService _sectionCoordinationService;

        /// <summary>
        /// Initializes a new instance of the SectionsController class.
        /// </summary>
        /// <param name="adapterRegistry">Adapter registry of type <see cref="IAdapterRegistry">IAdapterRegistry</see></param>
        /// <param name="sectionCoordinationService">Coordination service interface for sections</param>
        /// <param name="logger"> <see cref="ILogger">ILogger</see></param>
        public InstructionalEventsController(IAdapterRegistry adapterRegistry, ISectionCoordinationService sectionCoordinationService, ILogger logger)
        {
            _sectionCoordinationService = sectionCoordinationService;
            _adapterRegistry = adapterRegistry;
            _logger = logger;
        }

        #region HeDM Version 1-3 Methods

        /// <summary>
        /// Read (GET) a section
        /// </summary>
        /// <param name="guid">GUID to desired section</param>
        /// <returns>A DTO in the format of the sections LDM</returns>
        [Obsolete("Obsolete as of HeDM Version 4, use version 4 Accept Header instead.")]
        [HttpGet]
        public async Task<Dtos.InstructionalEvent> GetAsync(string guid)
        {
            try
            {
                return await _sectionCoordinationService.GetInstructionalEventAsync(guid);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new section
        /// </summary>
        /// <param name="meeting">DTO of the new section</param>
        /// <returns>A DTO in the format of the updated section's LDM</returns>
        [Obsolete("Obsolete as of HeDM Version 4, use version 4 Accept Header instead.")]
        [HttpPost]
        public async Task<Dtos.InstructionalEvent> PostAsync([FromBody] Dtos.InstructionalEvent meeting)
        {
            try
            {
                return await _sectionCoordinationService.CreateInstructionalEventAsync(meeting);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e, "Permissions exception");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e, "Argument exception");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e, "Repository exception");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e, "Integration API exception");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e, "Exception occurred");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Update (PUT) an existing section
        /// </summary>
        /// <param name="guid">GUID of the instructional event</param>
        /// <param name="meeting">DTO of the updated instructional event</param>
        /// <returns>A DTO in the format of the CDM sections</returns>
        [Obsolete("Obsolete as of HeDM Version 4, use version 4 Accept Header instead.")]
        [HttpPut]
        public async Task<Dtos.InstructionalEvent> PutAsync([FromUri]string guid, [FromBody] Dtos.InstructionalEvent meeting)
        {
            if (!string.IsNullOrEmpty(guid))
            {
                if (string.IsNullOrEmpty(meeting.Guid))
                {
                    meeting.Guid = guid;
                }
                else if (guid != meeting.Guid)
                {
                    throw CreateHttpResponseException(new IntegrationApiException("GUID mismatch", 
                        IntegrationApiUtility.GetDefaultApiError("GUID not the same as in request body.")));
                }
            }

            try
            {
                return await _sectionCoordinationService.UpdateInstructionalEventAsync(meeting);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Delete (DELETE) an existing section meeting
        /// </summary>
        /// <param name="guid">Uniquie ID of the Instructional Event to delete</param>
        [Obsolete("Obsolete as of HeDM Version 3, use version 3 Accept Header instead.")]
        [HttpDelete]
        public async Task DeleteAsync(string guid)
        {
            try
            {
                await _sectionCoordinationService.DeleteInstructionalEventAsync(guid);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Delete (DELETE) an existing section meeting
        /// </summary>
        /// <param name="guid">Unique ID of the Instructional Event to delete</param>
        [Obsolete("Obsolete as of HeDM Version 4, use version 4 Accept Header instead.")]
        [HttpDelete]
        public async Task<HttpResponseMessage> Delete2Async(string guid)
        {
            try
            {
                await _sectionCoordinationService.DeleteInstructionalEventAsync(guid);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
        #endregion

        #region HeDM version 4 methods
        /// <summary>
        /// Return a list of InstructionalEvents objects based on selection criteria.
        /// </summary>
        /// <param name="page">page</param>
        /// <param name="section">Section Id</param>
        /// <param name="startOn">Start Date and Time</param>
        /// <param name="endOn">End Date and Time</param>
        /// <param name="room">Room where class is being held</param>
        /// <param name="instructor">Instructor ID</param>
        /// <returns>List of InstructionalEvent2 <see cref="Dtos.InstructionalEvent2"/> objects representing matching InstructionalEvents</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 200), EedmResponseFilter]
        public async Task<IHttpActionResult> GetHedmInstructionalEventsAsync(Paging page, [FromUri] string section = "",
            [FromUri] string startOn = "", [FromUri] string endOn = "",
            [FromUri] string room = "", [FromUri] string instructor = "")
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                if (page == null)
                {
                    page = new Paging(200, 0);
                }

                AddDataPrivacyContextProperty((await _sectionCoordinationService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());

                var pageOfItems = await _sectionCoordinationService.GetInstructionalEvent2Async(page.Offset, page.Limit, section, startOn, endOn, room, instructor);
                return new PagedHttpActionResult<IEnumerable<Dtos.InstructionalEvent2>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }
        /// <summary>
        /// Read (GET) a section
        /// </summary>
        /// <param name="id">GUID to desired section</param>
        /// <returns>A DTO in the format of the sections LDM</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.InstructionalEvent2> GetHedmAsync(string id)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _sectionCoordinationService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _sectionCoordinationService.GetInstructionalEvent2Async(id);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new section
        /// </summary>
        /// <param name="meeting">DTO of the new section</param>
        /// <returns>A DTO in the format of the updated section's LDM</returns>
        [HttpPost]
        public async Task<Dtos.InstructionalEvent2> PostHedmAsync([FromBody] Dtos.InstructionalEvent2 meeting)
        {
            if (meeting == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null instructionalEvent argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }

            if (string.IsNullOrEmpty(meeting.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null instructionalEvent id",
                    IntegrationApiUtility.GetDefaultApiError("Id is a required property.")));
            }

            try
            {               
                return await _sectionCoordinationService.CreateInstructionalEvent2Async(meeting);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e, "Permissions exception");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e, "Argument exception");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e, "Repository exception");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e, "Integration API exception");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e, "Exception occurred");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Update (PUT) an existing section
        /// </summary>
        /// <param name="id">GUID of the instructional event</param>
        /// <param name="meeting">DTO of the updated instructional event</param>
        /// <returns>A DTO in the format of the CDM sections</returns>
        [HttpPut]
        public async Task<Dtos.InstructionalEvent2> PutHedmAsync([FromUri]string id, [FromBody] Dtos.InstructionalEvent2 meeting)
        {
            if (!string.IsNullOrEmpty(id))
            {
                if (string.IsNullOrEmpty(meeting.Id))
                {
                    meeting.Id = id;
                }
                else if (id != meeting.Id)
                {
                    throw CreateHttpResponseException(new IntegrationApiException("ID mismatch",
                        IntegrationApiUtility.GetDefaultApiError("ID not the same as in request body.")));
                }
            }

            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The ID must be specified in the request URL.")));
            }
            if (meeting == null)
            {
                throw new ArgumentNullException("Null instructionalEvent argument", "The request body is required.");
            }

            if (id.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException("Nil GUID cannot be used in PUT operation.");
            }

            if (string.IsNullOrEmpty(meeting.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("ID mismatch",
                    IntegrationApiUtility.GetDefaultApiError("Meeting Id must be provided.")));
            }

            // Compare uri value to body value for section Id
            if (!id.Equals(meeting.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("ID mismatch",
                    IntegrationApiUtility.GetDefaultApiError("ID not the same as in request body.")));
            }

            try
            {
                await _sectionCoordinationService.DoesUpdateViolateDataPrivacySettings(GetRouteResourceName(), meeting);
                return await _sectionCoordinationService.UpdateInstructionalEvent2Async(meeting);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }
        #endregion

        #region HeDM version 8 methods
        /// <summary>
        /// Return a list of InstructionalEvents objects based on selection criteria.
        /// </summary>
        /// <param name="page">page</param>
        /// <param name="criteria">Filter criteria</param>
        /// <returns>List of InstructionalEvent3 <see cref="Dtos.InstructionalEvent3"/> objects representing matching InstructionalEvents</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 200), EedmResponseFilter]
        public async Task<IHttpActionResult> GetInstructionalEvents3Async(Paging page, [FromUri] string criteria = "")
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                if (page == null)
                {
                    page = new Paging(200, 0);
                }
                string section = string.Empty, startOn = string.Empty, endOn = string.Empty, room = string.Empty, instructor = string.Empty;

                var criteriaValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(criteria);

                if (criteriaValues != null)
                {
                    foreach (var value in criteriaValues)
                    {
                        switch (value.Key.ToLower())
                        {
                            case "section":
                                section = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "starton":
                                startOn = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "endon":
                                endOn = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "room":
                                room = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            case "instructor":
                                instructor = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                break;
                            default:
                                throw new ArgumentException(string.Concat("Invalid filter value received: ", value.Key));
                        }
                    }
                }

                AddDataPrivacyContextProperty((await _sectionCoordinationService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await _sectionCoordinationService.GetInstructionalEvent3Async(page.Offset, page.Limit, section, startOn, endOn, room, instructor);
                return new PagedHttpActionResult<IEnumerable<Dtos.InstructionalEvent3>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }
        /// <summary>
        /// Read (GET) a section
        /// </summary>
        /// <param name="id">GUID to desired section</param>
        /// <returns>A DTO in the format of the sections LDM</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.InstructionalEvent3> GetInstructionalEvent3Async(string id)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _sectionCoordinationService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _sectionCoordinationService.GetInstructionalEvent3Async(id);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new section
        /// </summary>
        /// <param name="meeting">DTO of the new section</param>
        /// <returns>A DTO in the format of the updated section's LDM</returns>
        [HttpPost]
        public async Task<Dtos.InstructionalEvent3> PostInstructionalEvent3Async([FromBody] Dtos.InstructionalEvent3 meeting)
        {
            if (meeting == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null instructionalEvent argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }

            if (string.IsNullOrEmpty(meeting.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null instructionalEvent id",
                    IntegrationApiUtility.GetDefaultApiError("Id is a required property.")));
            }

            try
            {
                return await _sectionCoordinationService.CreateInstructionalEvent3Async(meeting);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e, "Permissions exception");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e, "Argument exception");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e, "Repository exception");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e, "Integration API exception");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e, "Exception occurred");
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Update (PUT) an existing section
        /// </summary>
        /// <param name="id">GUID of the instructional event</param>
        /// <param name="meeting">DTO of the updated instructional event</param>
        /// <returns>A DTO in the format of the CDM sections</returns>
        [HttpPut]
        public async Task<Dtos.InstructionalEvent3> PutInstructionalEvent3Async([FromUri]string id, [FromBody] Dtos.InstructionalEvent3 meeting)
        {
            if (!string.IsNullOrEmpty(id))
            {
                if (string.IsNullOrEmpty(meeting.Id))
                {
                    meeting.Id = id;
                }
                else if (id != meeting.Id)
                {
                    throw CreateHttpResponseException(new IntegrationApiException("ID mismatch",
                        IntegrationApiUtility.GetDefaultApiError("ID not the same as in request body.")));
                }
            }

            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The ID must be specified in the request URL.")));
            }
            if (meeting == null)
            {
                throw new ArgumentNullException("Null instructionalEvent argument", "The request body is required.");
            }

            if (id.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                throw new InvalidOperationException("Nil GUID cannot be used in PUT operation.");
            }

            if (string.IsNullOrEmpty(meeting.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("ID mismatch",
                    IntegrationApiUtility.GetDefaultApiError("Meeting Id must be provided.")));
            }

            // Compare uri value to body value for section Id
            if (!id.Equals(meeting.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("ID mismatch",
                    IntegrationApiUtility.GetDefaultApiError("ID not the same as in request body.")));
            }

            try
            {
                await _sectionCoordinationService.DoesUpdateViolateDataPrivacySettings(GetRouteResourceName(), meeting);
                return await _sectionCoordinationService.UpdateInstructionalEvent3Async(meeting);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }
        #endregion

        /// <summary>
        /// Delete (DELETE) an existing section meeting
        /// </summary>
        /// <param name="id">Unique ID of the Instructional Event to delete</param>
        [HttpDelete]
        public async Task<HttpResponseMessage> DeleteHedmAsync(string id)
        {
            try
            {
                await _sectionCoordinationService.DeleteInstructionalEventAsync(id);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}
