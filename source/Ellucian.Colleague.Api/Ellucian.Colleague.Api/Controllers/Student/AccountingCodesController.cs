﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Web.License;
using slf4net;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Web.Http;

namespace Ellucian.Colleague.Api.Controllers
{
    /// <summary>
    /// Provides access to AccountingCodes data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class AccountingCodesController : BaseCompressedApiController
    {
        private readonly IAccountingCodesService _accountingCodesService;
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the AccountingCodesController class.
        /// </summary>
        /// <param name="adapterRegistry">Adapter registry of type <see cref="IAdapterRegistry">IAdapterRegistry</see></param>
        /// <param name="accountingCodesService">Service of type <see cref="IAccountingCodesService">IAccountingCodesService</see></param>
        /// <param name="logger">Interface to logger</param>
        public AccountingCodesController(IAdapterRegistry adapterRegistry, IAccountingCodesService accountingCodesService, ILogger logger)
        {
            _accountingCodesService = accountingCodesService;
            _adapterRegistry = adapterRegistry;
            _logger = logger;
        }

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Retrieves all accounting codes.
        /// If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <returns>All accounting codes objects.</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.AccountingCode>> GetAccountingCodesAsync()
        {
            bool bypassCache = false; 
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                return await _accountingCodesService.GetAccountingCodesAsync(bypassCache);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Retrieves a accounting code by ID.
        /// </summary>
        /// <param name="id">Id of accounting code to retrieve</param>
        /// <returns>A <see cref="Ellucian.Colleague.Dtos.AccountingCode">accounting code.</see></returns>
        public async Task<Ellucian.Colleague.Dtos.AccountingCode> GetAccountingCodeByIdAsync(string id)
        {
            try
            {
                return await _accountingCodesService.GetAccountingCodeByIdAsync(id);
            }
            catch (KeyNotFoundException ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Creates a AccountingCode.
        /// </summary>
        /// <param name="accountingCode"><see cref="Dtos.AccountingCode">AccountingCode</see> to create</param>
        /// <returns>Newly created <see cref="Dtos.AccountingCode">AccountingCode</see></returns>
        [HttpPost]
        public async Task<Dtos.AccountingCode> PostAccountingCode([FromBody] Dtos.AccountingCode accountingCode)
        {
            //Create is not supported for Colleague but HEDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Updates a accounting code.
        /// </summary>
        /// <param name="id">Id of the AccountingCode to update</param>
        /// <param name="accountingCode"><see cref="Dtos.AccountingCode">AccountingCode</see> to create</param>
        /// <returns>Updated <see cref="Dtos.AccountingCode">AccountingCode</see></returns>
        [HttpPut]
        public async Task<Dtos.AccountingCode> PutAccountingCode([FromUri] string id, [FromBody] Dtos.AccountingCode accountingCode)
        {
            //Update is not supported for Colleague but HEDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Delete (DELETE) an existing accountingCode
        /// </summary>
        /// <param name="id">Id of the accountingCode to delete</param>
        [HttpDelete]
        public async Task DeleteAccountingCode([FromUri] string id)
        {
            //Delete is not supported for Colleague but HEDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }
    }
}