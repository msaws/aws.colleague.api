﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.Http.Filters;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Domain.Student.Repositories;
using System.Web.Http;
using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using slf4net;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Adapters;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Api.Controllers
{
    /// <summary>
    /// Provides access to Book information
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class BooksController : BaseCompressedApiController
    {
        private readonly IBookRepository _bookRepository;
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly ILogger _logger;
        /// <summary>
        /// BooksController constructor
        /// </summary>
        /// <param name="adapterRegistry">Adapter registry of type <see cref="IAdapterRegistry">IAdapterRegistry</see></param>
        /// <param name="bookRepository"></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public BooksController(IAdapterRegistry adapterRegistry, IBookRepository bookRepository, ILogger logger)
        {
            _adapterRegistry = adapterRegistry;
            _bookRepository = bookRepository;
            _logger = logger;
        }

        /// <summary>
        /// Retrieves information about a specific book.
        /// </summary>
        /// <param name="id">Unique system Id of the book</param>
        /// <returns>Information about a specific <see cref="Book">Book</see></returns>
        // [CacheControlFilter(Public = true, MaxAgeHours = 1, Revalidate = true)]
        public async Task<Book> GetAsync(string id)
        {
            var book = await _bookRepository.GetAsync(id);
            if (book == null)
            {
                throw CreateNotFoundException("book", id);
            }
            // Get the right adapter for the type mapping
            var bookDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Book, Book>();

            // Map the book entity to the book DTO
            return bookDtoAdapter.MapToType(book);
        }

    }
}
