﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Web.Security;
using System.Net.Http;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.Student
{
    /// <summary>
    /// The controller for student payments for the Ellucian Data Model.
    /// </summary>
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    [Authorize]
    public class StudentPaymentsController : BaseCompressedApiController
    {
        private readonly IStudentPaymentService studentPaymentService;
        private readonly ILogger logger;

        /// <summary>
        /// This constructor initializes the StudentPaymentController object
        /// </summary>
        /// <param name="studentPaymentService">student payments service object</param>
        /// <param name="logger">Logger object</param>
        public StudentPaymentsController(IStudentPaymentService studentPaymentService, ILogger logger)
        {
            this.studentPaymentService = studentPaymentService;
            this.logger = logger;
        }

        /// <summary>
        /// Retrieves a specified student payment for the data model version 6
        /// </summary>
        /// <param name="id">The requested student payment GUID</param>
        /// <returns>A StudentPayment DTO</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.StudentPayment> GetByIdAsync([FromUri] string id)
        {
            bool bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                if (string.IsNullOrEmpty(id))
                {
                    throw new ArgumentNullException("id", "id is required.");
                }
                AddDataPrivacyContextProperty((await studentPaymentService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var studentPayment = await studentPaymentService.GetByIdAsync(id);
                return studentPayment;
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (KeyNotFoundException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error getting student payment");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves all student payments for the data model version 6
        /// </summary>
        /// <returns>A Collection of StudentPayments</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetAsync(Paging page, [FromUri] string student = "", string academicPeriod = "", string accountingCode = "", string paymentType = "")
        {
            try
            {
                bool bypassCache = false;
                if (Request.Headers.CacheControl != null)
                {
                    if (Request.Headers.CacheControl.NoCache)
                    {
                        bypassCache = true;
                    }
                }
                if (page == null)
                {
                    page = new Paging(200, 0);
                }

                AddDataPrivacyContextProperty((await studentPaymentService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await studentPaymentService.GetAsync(page.Offset, page.Limit, bypassCache, student, academicPeriod, accountingCode, paymentType);
                return new PagedHttpActionResult<IEnumerable<Dtos.StudentPayment>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error getting student payment");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Update a single student payment for the data model version 6
        /// </summary>
        /// <param name="id">The requested student payment GUID</param>
        /// <param name="studentPaymentDto">General Ledger DTO from Body of request</param>
        /// <returns>A single StudentPayment</returns>
        [HttpPut]
        public async Task<Dtos.StudentPayment> UpdateAsync([FromUri] string id, [FromBody] Dtos.StudentPayment studentPaymentDto)
        {
            // The code is in the service and repository to perform this function but at this time, we
            // are not allowing an update or a delete.  Just throw unsupported error instead.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
            //try
            //{
            //    if (string.IsNullOrEmpty(id))
            //    {
            //        throw new ArgumentNullException("id", "id is a required for update");
            //    }
            //    if (studentPaymentDto == null)
            //    {
            //        throw new ArgumentNullException("studentPaymentDto", "The request body is required.");
            //    }
            //    if (string.IsNullOrEmpty(studentPaymentDto.Id))
            //    {
            //        studentPaymentDto.Id = id.ToUpperInvariant();
            //    }
            //    var studentPaymentTransaction = await studentPaymentService.UpdateAsync(id, studentPaymentDto);
            //    return studentPaymentTransaction;
            //}
            //catch (PermissionsException e)
            //{
            //    logger.Error(e.ToString());
            //    throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            //}
            //catch (ArgumentException e)
            //{
            //    logger.Error(e.ToString());
            //    throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            //}
            //catch (RepositoryException e)
            //{
            //    logger.Error(e.ToString());
            //    throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            //}
            //catch (Exception e)
            //{
            //    logger.Error(e, "Unknown error getting student payment");
            //    throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            //}
        }

        /// <summary>
        /// Create a single student payment for the data model version 6
        /// </summary>
        /// <param name="studentPaymentDto">General Ledger DTO from Body of request</param>
        /// <returns>A single StudentPayment</returns>
        [HttpPost]
        public async Task<Dtos.StudentPayment> CreateAsync([FromBody] Dtos.StudentPayment studentPaymentDto)
        {
            try
            {
                if (studentPaymentDto == null)
                {
                    throw new ArgumentNullException("studentPaymentDto", "The request body is required.");
                }
                ValidateStudentPayments(studentPaymentDto);

                var studentPayment = await studentPaymentService.CreateAsync(studentPaymentDto);
                return studentPayment;
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error getting student payment");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Delete a single student payment for the data model version 6
        /// </summary>
        /// <param name="id">The requested student payment GUID</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<HttpResponseMessage> DeleteAsync([FromUri] string id)
        {
            // The code is in the service and repository to perform this function but at this time, we
            // are not allowing an update or a delete.  Just throw unsupported error instead.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
            //try
            //{
            //    if (string.IsNullOrEmpty(id))
            //    {
            //        throw new ArgumentNullException("id", "guid is a required for delete");
            //    }
            //    await studentPaymentService.DeleteAsync(id);
            //}
            //catch (PermissionsException e)
            //{
            //    logger.Error(e.ToString());
            //    throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            //}
            //catch (ArgumentException e)
            //{
            //    logger.Error(e.ToString());
            //    throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            //}
            //catch (RepositoryException e)
            //{
            //    logger.Error(e.ToString());
            //    throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            //}
            //catch (Exception e)
            //{
            //    logger.Error(e, "Unknown error getting student payment");
            //    throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            //}
            //return new HttpResponseMessage(HttpStatusCode.OK);
        }

        /// <summary>
        /// Helper method to validate Student Payments.
        /// </summary>
        /// <param name="studentPayment">student payment DTO object of type <see cref="Dtos.StudentPayment"/></param>
        private void ValidateStudentPayments(Dtos.StudentPayment studentPayment)
        {
            if (studentPayment.AcademicPeriod == null)
            {
                throw new ArgumentNullException("studentPayments.academicPeriod", "The academic period is required when submitting a student payment. ");
            }
            if (studentPayment.AcademicPeriod != null && string.IsNullOrEmpty(studentPayment.AcademicPeriod.Id))
            {
                throw new ArgumentNullException("studentPayments.academicPeriod", "The academic period id is required when submitting a student payment. ");
            }
            if (studentPayment.Amount == null)
            {
                throw new ArgumentNullException("studentPayments.paymentAmount", "The payment amount cannot be null when submitting a student payment. ");
            }
            if (studentPayment.Amount != null && (studentPayment.Amount.Value == 0 || studentPayment.Amount.Value == null))
            {
                throw new ArgumentNullException("studentPayments.paymentAmount.value", "A student-payments in the amount of zero dollars is not permitted. ");
            }
            if (studentPayment.Amount != null && studentPayment.Amount.Currency != Dtos.EnumProperties.CurrencyCodes.USD && studentPayment.Amount.Currency != Dtos.EnumProperties.CurrencyCodes.CAD)
            {
                throw new ArgumentException("The currency code must be set to either 'USD' or 'CAD'. ", "studentPayments.amount.currency");
            }
            if (studentPayment.PaymentType == Dtos.EnumProperties.StudentPaymentTypes.notset)
            {
                throw new ArgumentException("The paymentType is either invalid or empty and is required when submitting a student payment. ", "studentPayments.paymentType");
            }
            if (studentPayment.Person == null || string.IsNullOrEmpty(studentPayment.Person.Id))
            {
                throw new ArgumentNullException("studentPayments.student.id", "The student id is required when submitting a student payment. ");
            }
            if (studentPayment.AccountingCode != null && string.IsNullOrEmpty(studentPayment.AccountingCode.Id))
            {
                throw new ArgumentException("The accountingCode requires an id when submitting student payments. ","studentPayments.accountingCode.id");
            }
            if (studentPayment.PaymentType == Dtos.EnumProperties.StudentPaymentTypes.sponsor && studentPayment.AccountingCode == null)
            {
                throw new ArgumentNullException("studentPayments.accountingCode", "The accountingCode is required when submitting sponsor payments. ");
            }
        }
    }
}