﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.ComponentModel;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;

namespace Ellucian.Colleague.Api.Controllers.Student
{
    /// <summary>
    /// This is the controller for tax form pdf.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class StudentTaxFormPdfsController : BaseCompressedApiController
    {
        private readonly IAdapterRegistry adapterRegistry;
        private readonly ILogger logger;
        private readonly IStudentTaxFormPdfService taxFormPdfService;

        /// <summary>
        /// Initialize the Tax Form pdf controller.
        /// </summary>
        public StudentTaxFormPdfsController(IAdapterRegistry adapterRegistry, ILogger logger, IStudentTaxFormPdfService taxFormPdfService)
        {
            this.adapterRegistry = adapterRegistry;
            this.logger = logger;
            this.taxFormPdfService = taxFormPdfService;
        }

        /// <summary>
        /// Returns the data to be printed on the pdf for the 1098 tax form.
        /// </summary>
        /// <param name="personId">ID of the person assigned to and requesting the 1098.</param>
        /// <param name="recordId">The record ID where the 1098 pdf data is stored</param>
        /// <returns>HttpResponseMessage</returns>
        public async Task<HttpResponseMessage> Get1098TaxFormPdf(string personId, string recordId)
        {
            if (string.IsNullOrEmpty(personId))
                throw CreateHttpResponseException("Person ID must be specified.", HttpStatusCode.BadRequest);

            if (string.IsNullOrEmpty(recordId))
                throw CreateHttpResponseException("Record ID must be specified.", HttpStatusCode.BadRequest);

            string pdfTemplatePath = string.Empty;
            try
            {
                var pdfData = await taxFormPdfService.Get1098TaxFormData(personId, recordId);

                bool useRdlc = false;
                // Determine which PDF template to use.
                switch (pdfData.TaxYear)
                {
                    case "2016":
                        useRdlc = false;
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/Student/2016-1098T.pdf");
                        break;
                    case "2015":
                        useRdlc = false;
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/Student/2015-1098T.pdf");
                        break;
                    case "2014":
                        useRdlc = false;
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/Student/2014-1098T.pdf");
                        break;
                    case "2013":
                        useRdlc = false;
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/Student/2013-1098T.pdf");
                        break;
                    case "2012":
                        useRdlc = false;
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/Student/2012-1098T.pdf");
                        break;
                    case "2011":
                        useRdlc = false;
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/Student/2011-1098T.pdf");
                        break;
                    case "2010":
                        useRdlc = false;
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/Student/2010-1098T.pdf");
                        break;
                    case "2009":
                        useRdlc = false;
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/Student/2009-1098T.pdf");
                        break;
                    case "2008":
                        useRdlc = false;
                        pdfTemplatePath = HttpContext.Current.Server.MapPath("~/Reports/Student/2008-1098T.pdf");
                        break;
                    default:
                        var message = string.Format("Incorrect Tax Year {0}", pdfData.TaxYear);
                        logger.Error(message);
                        throw new ApplicationException(message);
                }

                var pdfBytes = new byte[0];

                if(useRdlc)
                {
                    pdfBytes = taxFormPdfService.Populate1098tReport(pdfData, pdfTemplatePath);
                }
                else
                {
                    pdfBytes = taxFormPdfService.Populate1098Pdf(pdfData, pdfTemplatePath);
                }

                // Create and return the HTTP response object
                var response = new HttpResponseMessage();
                response.Content = new ByteArrayContent(pdfBytes);

                var fileNameString = "TaxForm1098" + "_" + recordId;
                response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/pdf");
                response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = fileNameString + ".pdf"
                };
                response.Content.Headers.ContentLength = pdfBytes.Length;
                return response;
            }
            catch (Exception e)
            {
                logger.Error(e.Message);
                throw CreateHttpResponseException("Error retrieving 1098-T PDF data.", HttpStatusCode.BadRequest);
            }
        }

    }
}
