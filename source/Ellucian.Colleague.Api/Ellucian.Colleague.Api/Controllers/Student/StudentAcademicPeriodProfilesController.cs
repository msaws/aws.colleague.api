﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Web.Http.Controllers;
using System.Web.Http;
using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using System.Threading.Tasks;
using System;
using System.Linq;
using Ellucian.Colleague.Coordination.Student.Services;
using slf4net;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Dtos;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http;
using System.Net;

namespace Ellucian.Colleague.Api.Controllers.Student
{
    /// <summary>
    /// Provides access to StudentAcademicPeriodProfiles data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof (EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class StudentAcademicPeriodProfilesController : BaseCompressedApiController
    {
        private readonly IStudentAcademicPeriodProfilesService _studentAcademicPeriodProfilesService;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the StudentAcademicPeriodProfilesController class.
        /// </summary>
        /// <param name="studentAcademicPeriodProfilesService">Repository of type <see cref="IStudentAcademicPeriodProfilesService">IStudentAcademicPeriodProfilesService</see></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public StudentAcademicPeriodProfilesController(IStudentAcademicPeriodProfilesService studentAcademicPeriodProfilesService, ILogger logger)
        {
            _studentAcademicPeriodProfilesService = studentAcademicPeriodProfilesService;
            _logger = logger;
        }

        /// <summary>
        /// Retrieves an Student Academic Period Profiles by ID.
        /// </summary>
        /// <returns>An <see cref="Dtos.StudentAcademicPeriodProfiles">StudentAcademicPeriodProfiles</see>object.</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<StudentAcademicPeriodProfiles> GetStudentAcademicPeriodProfileByGuidAsync(string id)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _studentAcademicPeriodProfilesService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _studentAcademicPeriodProfilesService.GetStudentAcademicPeriodProfileByGuidAsync(id);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Return a list of StudentAcademicPeriodProfiles objects based on selection criteria.
        /// </summary>
        ///  <param name="page">page</param>
        /// <param name="person">Id (GUID) A reference to link a student to the common HEDM persons entity</param>     
        /// <param name="academicPeriod">Id (GUID) A term within an academic year (for example, Semester).</param>
        /// <returns>List of StudentAcademicPeriodProfiles <see cref="Dtos.StudentAcademicPeriodProfiles"/> objects representing matching Student Academic Period Profiles</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 50), EedmResponseFilter]
        public async Task<IHttpActionResult> GetStudentAcademicPeriodProfilesAsync(Paging page, [FromUri] string person = "", [FromUri] string academicPeriod = "")
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                if (page == null)
                {
                    page = new Paging(50, 0);
                }

                AddDataPrivacyContextProperty((await _studentAcademicPeriodProfilesService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await _studentAcademicPeriodProfilesService.GetStudentAcademicPeriodProfilesAsync(page.Offset, page.Limit, bypassCache, person, academicPeriod);
                return new PagedHttpActionResult<IEnumerable<StudentAcademicPeriodProfiles>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            { 
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Creates a Student Academic Period Profile.
        /// </summary>
        /// <param name="studentAcademicPeriodProfiles"><see cref="Dtos.StudentAcademicPeriodProfiles">StudentAcademicPeriodProfiles</see> to create</param>
        /// <returns>Newly created <see cref="Dtos.StudentAcademicPeriodProfiles">StudentAcademicPeriodProfiles</see></returns>
        [HttpPost]
        public async Task<Dtos.StudentAcademicPeriodProfiles> CreateStudentAcademicPeriodProfilesAsync([FromBody] Dtos.StudentAcademicPeriodProfiles studentAcademicPeriodProfiles)
        {
            //Create is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Updates a Student Academic Period Profile.
        /// </summary>
        /// <param name="id">Id of the Student Academic Period Profile to update</param>
        /// <param name="studentAcademicPeriodProfiles"><see cref="Dtos.StudentAcademicPeriodProfiles">StudentAcademicPeriodProfiles</see> to create</param>
        /// <returns>Updated <see cref="Dtos.StudentAcademicPeriodProfiles">StudentAcademicPeriodProfiles</see></returns>
        [HttpPut]
        public async Task<Dtos.StudentAcademicPeriodProfiles> UpdateStudentAcademicPeriodProfilesAsync([FromUri] string id, [FromBody] Dtos.StudentAcademicPeriodProfiles studentAcademicPeriodProfiles)
        {

            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

        /// <summary>
        /// Deletes a Student Academic Period Profiles.
        /// </summary>
        /// <param name="id">ID of the Student Academic Period Profile to be deleted</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task DeleteStudentAcademicPeriodProfilesAsync(string id)
        {
            //Delete is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));

        }

    }
}