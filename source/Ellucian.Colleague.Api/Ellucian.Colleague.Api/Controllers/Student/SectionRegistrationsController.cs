﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using System.Collections.Generic;
using System.Web.Http;
using Ellucian.Web.Http.Controllers;
using Ellucian.Colleague.Domain.Student.Repositories;
using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Adapters;
using Ellucian.Colleague.Coordination.Student.Services;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.Student
{
    /// <summary>
    /// Provides access to SectionRegistration
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class SectionRegistrationsController : BaseCompressedApiController
    {
        private readonly IStudentReferenceDataRepository _studentReferenceDataRepository;
        private readonly ISectionRegistrationService _sectionRegistrationService;
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly ILogger _logger;

        /// <summary>
        /// SectionRegistrationStatusesController constructor
        /// </summary>
        /// <param name="adapterRegistry">Adapter registry of type <see cref="IAdapterRegistry">IAdapterRegistry</see></param>
        /// <param name="studentReferenceDataRepository">Repository of type <see cref="IStudentReferenceDataRepository">IStudentReferenceDataRepository</see></param>
        /// <param name="sectionRegistrationService">Service of type <see cref="ICurriculumService">ISectionRegistrationService</see></param>
        /// <param name="logger">Interface to Logger</param>
        public SectionRegistrationsController(IAdapterRegistry adapterRegistry, IStudentReferenceDataRepository studentReferenceDataRepository, ISectionRegistrationService sectionRegistrationService, ILogger logger)
        {
            _adapterRegistry = adapterRegistry;
            _studentReferenceDataRepository = studentReferenceDataRepository;
            _sectionRegistrationService = sectionRegistrationService;
            _logger = logger;
        }

        #region Get Methods

        /// <summary>
        /// Get section registration
        /// </summary>
        /// <param name="guid">Id of the SectionRegistration</param>
        /// <returns>A SectionRegistration <see cref="Dtos.SectionRegistration2"/> object</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.SectionRegistration2> GetSectionRegistrationAsync([FromUri] string guid)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _sectionRegistrationService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _sectionRegistrationService.GetSectionRegistrationAsync(guid);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentOutOfRangeException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Get section registration V7
        /// </summary>
        /// <param name="guid">Id of the SectionRegistration</param>
        /// <returns>A SectionRegistration <see cref="Dtos.SectionRegistration2"/> object</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.SectionRegistration3> GetSectionRegistration2Async([FromUri] string guid)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _sectionRegistrationService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _sectionRegistrationService.GetSectionRegistration2Async(guid);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentOutOfRangeException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }


        /// <summary>
        /// Gets section registrations with filter
        /// </summary>
        /// <param name="page"></param>
        /// <param name="section"></param>
        /// <param name="registrant"></param>
        /// <returns></returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetSectionRegistrationsAsync(Paging page, [FromUri] string section = "",
            [FromUri] string registrant = "")
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                AddDataPrivacyContextProperty((await _sectionRegistrationService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());

                var pageOfItems = await
                        _sectionRegistrationService.GetSectionRegistrationsAsync(page.Offset, page.Limit, section, registrant);
                return new PagedHttpActionResult<IEnumerable<Dtos.SectionRegistration2>>(pageOfItems.Item1, page,  pageOfItems.Item2, Request);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }

        }

        /// <summary>
        /// Gets section registrations with filter V7
        /// </summary>
        /// <param name="page"></param>
        /// <param name="section"></param>
        /// <param name="registrant"></param>
        /// <returns></returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetSectionRegistrations2Async(Paging page, [FromUri] string section = "",
            [FromUri] string registrant = "")
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                AddDataPrivacyContextProperty((await _sectionRegistrationService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());

                var pageOfItems = await _sectionRegistrationService.GetSectionRegistrations2Async(page.Offset, page.Limit, section, registrant);
                return new PagedHttpActionResult<IEnumerable<Dtos.SectionRegistration3>>(pageOfItems.Item1, page, pageOfItems.Item2, Request);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }

        }

        #endregion

        #region Put Methods

        /// <summary>
        /// Update (PUT) section registrations
        /// </summary>
        /// <param name="guid">Id of the SectionRegistration</param>
        /// <param name="sectionRegistration">DTO of the SectionRegistration</param>
        /// <returns>A SectionRegistration <see cref="Dtos.SectionRegistration2"/> object</returns>
        [HttpPut]
        public async Task<Dtos.SectionRegistration2> PutSectionRegistrationAsync([FromUri] string guid, [FromBody] Dtos.SectionRegistration2 sectionRegistration)
        {
            try
            {
                if (string.IsNullOrEmpty(guid))
                {
                    throw new ArgumentNullException("Null sectionRegistration guid", "guid is a required property.");
                }
                if (sectionRegistration == null)
                {
                    throw new ArgumentNullException("Null sectionRegistration argument", "The request body is required.");
                }
                if (guid.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    throw new InvalidOperationException("Nil GUID cannot be used in PUT operation.");
                }

                if (string.IsNullOrEmpty(sectionRegistration.Id))
                {
                    sectionRegistration.Id = guid.ToUpperInvariant();
                }

                await _sectionRegistrationService.DoesUpdateViolateDataPrivacySettings(GetRouteResourceName(), sectionRegistration);
                return await _sectionRegistrationService.UpdateSectionRegistrationAsync(guid, sectionRegistration);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (InvalidOperationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentOutOfRangeException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Update (PUT) section registrations
        /// </summary>
        /// <param name="guid">Id of the SectionRegistration</param>
        /// <param name="sectionRegistration">DTO of the SectionRegistration</param>
        /// <returns>A SectionRegistration <see cref="Dtos.SectionRegistration3"/> object</returns>
        [HttpPut]
        public async Task<Dtos.SectionRegistration3> PutSectionRegistration2Async([FromUri] string guid, [FromBody] Dtos.SectionRegistration3 sectionRegistration)
        {
            try
            {
                if (string.IsNullOrEmpty(guid))
                {
                    throw new ArgumentNullException("Null sectionRegistration guid", "guid is a required property.");
                }
                if (sectionRegistration == null)
                {
                    throw new ArgumentNullException("Null sectionRegistration argument", "The request body is required.");
                }
                if (guid.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    throw new InvalidOperationException("Nil GUID cannot be used in PUT operation.");
                }
                if (!guid.Equals(sectionRegistration.Id, StringComparison.OrdinalIgnoreCase))
                {
                    throw CreateHttpResponseException(new IntegrationApiException("GUID mismatch", IntegrationApiUtility.GetDefaultApiError("GUID not the same as in request body.")));
                }
                if (string.IsNullOrEmpty(sectionRegistration.Id))
                {
                    sectionRegistration.Id = guid.ToUpperInvariant();
                }

                await _sectionRegistrationService.DoesUpdateViolateDataPrivacySettings(GetRouteResourceName(), sectionRegistration);
                return await _sectionRegistrationService.UpdateSectionRegistration2Async(guid, sectionRegistration);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (InvalidOperationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentOutOfRangeException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        #endregion

        #region Post Methods

        /// <summary>
        /// Create (POST) section registrations
        /// </summary>
        /// <param name="sectionRegistration">A SectionRegistration <see cref="Dtos.SectionRegistration2"/> object</param>
        /// <returns>A SectionRegistration <see cref="Dtos.SectionRegistration2"/> object</returns>
        [HttpPost]
        public async Task<Dtos.SectionRegistration2> PostSectionRegistrationAsync([FromBody] Dtos.SectionRegistration2 sectionRegistration)
        {
            try
            {
                if (sectionRegistration == null)
                {
                    throw new ArgumentNullException("Null sectionRegistration argument", "The request body is required.");
                }
                if (string.IsNullOrEmpty(sectionRegistration.Id))
                {
                    throw new ArgumentNullException("Null sectionRegistration id", "Id is a required property.");
                }
                return await _sectionRegistrationService.CreateSectionRegistrationAsync(sectionRegistration);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentOutOfRangeException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (InvalidOperationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (FormatException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }


        /// <summary>
        /// Create (POST) section registrations
        /// </summary>
        /// <param name="sectionRegistration">A SectionRegistration <see cref="Dtos.SectionRegistration3"/> object</param>
        /// <returns>A SectionRegistration <see cref="Dtos.SectionRegistration3"/> object</returns>
        [HttpPost]
        public async Task<Dtos.SectionRegistration3> PostSectionRegistration2Async([FromBody] Dtos.SectionRegistration3 sectionRegistration)
        {
            try
            {
                if (sectionRegistration == null)
                {
                    throw new ArgumentNullException("Null sectionRegistration argument", "The request body is required.");
                }
                if (string.IsNullOrEmpty(sectionRegistration.Id))
                {
                    throw new ArgumentNullException("Null sectionRegistration id", "Id is a required property.");
                }
                return await _sectionRegistrationService.CreateSectionRegistration2Async(sectionRegistration);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentOutOfRangeException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (InvalidOperationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (FormatException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        #endregion

        #region Delete Methods
      
        /// <summary>
        /// Delete (DELETE) an existing section-registrations
        /// </summary>
        /// <param name="guid">Id of the section-registration to delete</param>
        [HttpDelete]
        public async Task DeleteSectionRegistrationAsync([FromUri] string guid)
        {
            //Delete is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        #endregion
    }
}
