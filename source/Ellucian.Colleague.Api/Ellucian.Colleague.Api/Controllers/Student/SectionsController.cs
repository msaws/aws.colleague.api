﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Dtos;
using Ellucian.Colleague.Dtos.EnumProperties;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;
using Ellucian.Web.Http;
using Ellucian.Web.Http.Models;
using Newtonsoft.Json;
using Section = Ellucian.Colleague.Dtos.Student.Section;
using Section2 = Ellucian.Colleague.Dtos.Student.Section2;
using Section3 = Ellucian.Colleague.Dtos.Student.Section3;

namespace Ellucian.Colleague.Api.Controllers.Student
{
    /// <summary>
    /// Provides access to course Section data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof (EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class SectionsController : BaseCompressedApiController
    {
        private readonly ISectionCoordinationService _sectionCoordinationService;
        private readonly ISectionRegistrationService _sectionRegistrationService;
        private readonly IRegistrationGroupService _registrationGroupService;
        private readonly ISectionRepository _sectionRepository;
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the SectionsController class.
        /// </summary>
        /// <param name="adapterRegistry">Adapter registry of type <see cref="IAdapterRegistry">IAdapterRegistry</see></param>
        /// <param name="sectionRepository">Repository of type <see cref="ISectionRepository">ISectionRepository</see></param>
        /// <param name="sectionCoordinationService">Service of type <see cref="ISectionCoordinationService">ISectionCoordinationService</see></param>
        /// <param name="sectionRegistrationService">Service of type <see cref="ISectionRegistrationService">ISectionRegistrationService</see></param>
        /// <param name="registrationGroupService">Service of type <see cref="IRegistrationGroupService">IRegistrationGroupService</see></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public SectionsController(IAdapterRegistry adapterRegistry,
            ISectionRepository sectionRepository,
            ISectionCoordinationService sectionCoordinationService,
            ISectionRegistrationService sectionRegistrationService,
            IRegistrationGroupService registrationGroupService,
            ILogger logger)
        {
            _adapterRegistry = adapterRegistry;
            _sectionRepository = sectionRepository;
            _sectionCoordinationService = sectionCoordinationService;
            _sectionRegistrationService = sectionRegistrationService;
            _registrationGroupService = registrationGroupService;
            _logger = logger;
        }

        /// <summary>
        /// Retrieves information about a specific course section. 
        /// If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <param name="sectionId">Id of the section desired</param>
        /// <returns>The requested <see cref="Dtos.Student.Section">Section</see></returns>
        ///[CacheControlFilter(Public = true, MaxAgeHours = 1, Revalidate = true)]
        [Obsolete("Obsolete as of Api version 1.3, use version 2 of this API")]
        [ParameterSubstitutionFilter]
        public async Task<Section> GetSectionAsync(string sectionId)
        {
            IEnumerable<string> listOfIds = new List<string>() {sectionId};
            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntities = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
            bool useCache = true;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    useCache = false;
                }
            }
            if (useCache)
            {
                sectionEntities = await _sectionRepository.GetCachedSectionsAsync(listOfIds);
            }
            else
            {
                sectionEntities = await _sectionRepository.GetNonCachedSectionsAsync(listOfIds);
            }
            Ellucian.Colleague.Domain.Student.Entities.Section sectionEntity = sectionEntities.Where(s => s.Id == sectionId).FirstOrDefault();
            if (sectionEntity == null)
            {
                throw CreateNotFoundException("section", sectionId);
            }
            // Get the right adapter for the type mapping
            var sectionDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>();

            // Map the section entity to the section Dto then set up response and cache for the single item.
            Section sectionDto = sectionDtoAdapter.MapToType(sectionEntity);

            return sectionDto;
        }

        /// <summary>
        /// Retrieves information about a specific course section. 
        /// If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <param name="sectionId">Id of the section desired</param>
        /// <returns>The requested <see cref="Dtos.Student.Section2">Section</see></returns>
        ///[CacheControlFilter(Public = true, MaxAgeHours = 1, Revalidate = true)]
        [Obsolete("Obsolete as of Api version 1.5, use version 3 of this API")]
        [ParameterSubstitutionFilter]
        public async Task<Section2> GetSection2Async(string sectionId)
        {
            IEnumerable<string> listOfIds = new List<string>() {sectionId};
            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntities = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
            bool useCache = true;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    useCache = false;
                }
            }
            if (useCache)
            {
                sectionEntities = await _sectionRepository.GetCachedSectionsAsync(listOfIds);
            }
            else
            {
                sectionEntities = await _sectionRepository.GetNonCachedSectionsAsync(listOfIds);
            }
            Ellucian.Colleague.Domain.Student.Entities.Section sectionEntity = sectionEntities.Where(s => s.Id == sectionId).FirstOrDefault();
            if (sectionEntity == null)
            {
                throw CreateNotFoundException("section", sectionId);
            }
            // Get the right adapter for the type mapping
            var sectionDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section2>();

            // Map the section entity to the section Dto then set up response and cache for the single item.
            Section2 sectionDto = sectionDtoAdapter.MapToType(sectionEntity);

            return sectionDto;
        }


        /// <summary>
        /// Retrieves information about a specific course section. 
        /// If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <param name="sectionId">Id of the section desired</param>
        /// <returns>The requested <see cref="Dtos.Student.Section3">Section3</see></returns>
        [ParameterSubstitutionFilter]
        public async Task<Section3> GetSection3Async(string sectionId)
        {
            IEnumerable<string> listOfIds = new List<string>() {sectionId};
            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntities = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
            bool useCache = true;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    useCache = false;
                }
            }
            if (useCache)
            {
                sectionEntities = await _sectionRepository.GetCachedSectionsAsync(listOfIds);
            }
            else
            {
                sectionEntities = await _sectionRepository.GetNonCachedSectionsAsync(listOfIds);
            }
            Ellucian.Colleague.Domain.Student.Entities.Section sectionEntity = sectionEntities.Where(s => s.Id == sectionId).FirstOrDefault();
            if (sectionEntity == null)
            {
                throw CreateNotFoundException("section", sectionId);
            }
            // Get the right adapter for the type mapping
            var sectionDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section3>();

            // Map the section entity to the section Dto then set up response and cache for the single item.
            Section3 sectionDto = sectionDtoAdapter.MapToType(sectionEntity);

            return sectionDto;
        }

        /// <summary>
        /// Retrieves the class roster for a section.
        /// </summary>
        /// <param name="sectionId">The section Id.</param>
        /// <returns>All <see cref="RosterStudent">Students</see> in the Section</returns>
        [ParameterSubstitutionFilter]
        public async Task<IEnumerable<RosterStudent>> GetSectionRosterAsync(string sectionId)
        {
            try
            {
                return await _sectionCoordinationService.GetSectionRosterAsync(sectionId);
            }
            catch (ArgumentNullException e)
            {
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
            catch (ApplicationException e)
            {
                throw CreateHttpResponseException(e.Message, HttpStatusCode.NotFound);
            }
                //catch (RepositoryException e)
                //{
                //    _logger.Error(e.ToString());
                //    throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiExcecption(e));
                //}
            catch (Exception e)
            {
                throw CreateHttpResponseException(e.Message, HttpStatusCode.Forbidden);
            }
        }

        /// <summary>
        /// Retrieves the sections for the given section Ids. 
        /// If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <param name="sectionIds">comma delimited list of section IDs</param>
        /// <returns>The requested <see cref="Section">Sections</see></returns>
        [Obsolete("Obsolete as of Api version 1.3, use version 2 of this API")]
        public async Task<IEnumerable<Section>> GetSectionsAsync(string sectionIds)
        {
            if (string.IsNullOrEmpty(sectionIds))
            {
                return new List<Section>();
            }
            var idList = sectionIds.Trim().Split(',');
            List<string> list = new List<string>(idList);

            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntities = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
            bool useCache = true;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    useCache = false;
                }
            }
            if (useCache)
            {
                sectionEntities = await _sectionRepository.GetCachedSectionsAsync(list);
            }
            else
            {
                sectionEntities = await _sectionRepository.GetNonCachedSectionsAsync(list);
            }

            List<Section> sectionDtos = new List<Section>();
            var sectionDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section>();
            foreach (var section in sectionEntities)
            {
                Section sectionDto = sectionDtoAdapter.MapToType(section);
                sectionDtos.Add(sectionDto);
            }
            return sectionDtos;
        }

        /// <summary>
        /// Retrieves the sections for the given section Ids. 
        /// If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <param name="sectionIds">comma delimited list of section IDs</param>
        /// <returns>The requested <see cref="Section2">Sections</see></returns>
        [Obsolete("Obsolete as of Api version 1.4, use endpoint POST qapi/sections")]
        public async Task<IEnumerable<Section2>> GetSections2Async(string sectionIds)
        {
            if (string.IsNullOrEmpty(sectionIds))
            {
                return new List<Section2>();
            }
            var idList = sectionIds.Trim().Split(',');
            List<string> list = new List<string>(idList);

            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntities = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
            bool useCache = true;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    useCache = false;
                }
            }
            if (useCache)
            {
                sectionEntities = await _sectionRepository.GetCachedSectionsAsync(list);
            }
            else
            {
                sectionEntities = await _sectionRepository.GetNonCachedSectionsAsync(list);
            }

            List<Section2> sectionDtos = new List<Section2>();
            var sectionDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section2>();
            foreach (var section in sectionEntities)
            {
                Section2 sectionDto = sectionDtoAdapter.MapToType(section);
                sectionDtos.Add(sectionDto);
            }
            return sectionDtos;
        }

        /// <summary>
        /// Query by post method used to get the sections for the given section Ids. 
        /// If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <param name="sectionIds">list of section IDs</param>
        /// <returns>The requested <see cref="Section2">Sections</see></returns>
        [Obsolete("Obsolete as of Api version 1.5, use version 2 of this API")]
        [HttpPost]
        public async Task<IEnumerable<Section2>> QuerySectionsByPostAsync([FromBody] IEnumerable<string> sectionIds)
        {
            if (sectionIds == null || sectionIds.Count() == 0)
            {
                string errorText = "At least one item in list of sectionIds must be provided.";
                _logger.Error(errorText);
                throw CreateHttpResponseException(errorText, HttpStatusCode.BadRequest);
            }

            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntities = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
            bool useCache = true;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    useCache = false;
                }
            }
            if (useCache)
            {
                sectionEntities = await _sectionRepository.GetCachedSectionsAsync(sectionIds);
            }
            else
            {
                sectionEntities = await _sectionRepository.GetNonCachedSectionsAsync(sectionIds);
            }

            List<Section2> sectionDtos = new List<Section2>();
            var sectionDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section2>();
            foreach (var section in sectionEntities)
            {
                Section2 sectionDto = sectionDtoAdapter.MapToType(section);
                sectionDtos.Add(sectionDto);
            }
            return sectionDtos;
        }

        /// <summary>
        /// Query by post method used to get the sections for the given section Ids. 
        /// If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <param name="sectionIds">list of section IDs</param>
        /// <returns>The requested <see cref="Section3">Sections</see></returns>
        [Obsolete("Obsolete as of Api version 1.6, use version 3 of this API")]
        [HttpPost]
        public async Task<IEnumerable<Section3>> QuerySectionsByPost2Async([FromBody] IEnumerable<string> sectionIds)
        {
            if (sectionIds == null || sectionIds.Count() == 0)
            {
                string errorText = "At least one item in list of sectionIds must be provided.";
                _logger.Error(errorText);
                throw CreateHttpResponseException(errorText, HttpStatusCode.BadRequest);
            }

            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntities = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
            bool useCache = true;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    useCache = false;
                }
            }
            if (useCache)
            {
                sectionEntities = await _sectionRepository.GetCachedSectionsAsync(sectionIds);
            }
            else
            {
                sectionEntities = await _sectionRepository.GetNonCachedSectionsAsync(sectionIds);
            }

            List<Section3> sectionDtos = new List<Section3>();
            var sectionDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section3>();
            foreach (var section in sectionEntities)
            {
                Section3 sectionDto = sectionDtoAdapter.MapToType(section);
                sectionDtos.Add(sectionDto);
            }
            return sectionDtos;
        }

        #region HeDM Methods

        /// <summary>
        /// Read (GET) a section using a GUID
        /// </summary>
        /// <param name="guid">GUID to desired section</param>
        /// <returns>A section DTO in the CDM format</returns>
        [Obsolete("Obsolete as of HeDM Version 4, use Accept Header Version 4 instead.")]
        [HttpGet]
        public async Task<Dtos.Section> GetSectionByGuidAsync(string guid)
        {
            try
            {
                return await _sectionCoordinationService.GetSectionByGuidAsync(guid);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new section
        /// </summary>
        /// <param name="section">DTO of the new section</param>
        /// <returns>A section DTO in the CDM format</returns>
        [Obsolete("Obsolete as of HeDM Version 4, use Accept Header Version 4 instead.")]
        [HttpPost]
        public async Task<Dtos.Section> PostSectionAsync(Dtos.Section section)
        {
            try
            {
                return await _sectionCoordinationService.PostSectionAsync(section);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Update (PUT) an existing section
        /// </summary>
        /// <param name="guid">GUID of the section to update</param>
        /// <param name="section">DTO of the updated section</param>
        /// <returns>A section DTO in the CDM format</returns>
        [Obsolete("Obsolete as of HeDM Version 4, use Accept Header Version 4 instead.")]
        [HttpPut]
        public async Task<Dtos.Section> PutSectionAsync([FromUri] string guid, [FromBody] Dtos.Section section)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null guid argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            if (section == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null section argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }
            if (string.IsNullOrEmpty(section.Guid))
            {
                section.Guid = guid.ToLowerInvariant();
            }
            else if (guid.ToLowerInvariant() != section.Guid.ToLowerInvariant())
            {
                throw CreateHttpResponseException(new IntegrationApiException("GUID mismatch",
                    IntegrationApiUtility.GetDefaultApiError("GUID not the same as in request body.")));
            }

            try
            {
                return await _sectionCoordinationService.PutSectionAsync(section);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Return a list of Sections objects based on selection criteria.
        /// </summary>
        /// <param name="page">Section page Contains ...page...</param>
        /// <param name="title">Section Title Contains ...title...</param>
        /// <param name="startOn">Section starts on or after this date</param>
        /// <param name="endOn">Section ends on or before this date</param>
        /// <param name="code">Section Name Contains ...code...</param>
        /// <param name="number">Section Number equal to</param>
        /// <param name="instructionalPlatform">Learning Platform equal to (guid)</param>
        /// <param name="academicPeriod">Section Term equal to (guid)</param>
        /// <param name="academicLevels">Section Academic Level equal to (guid)</param>
        /// <param name="course">Section Course equal to (guid)</param>
        /// <param name="site">Section Location equal to (guid)</param>
        /// <param name="status">Section Status matches closed, open, pending, or cancelled</param>
        /// <param name="owningOrganizations">Section Department equal to (guid)</param>
        /// <returns>List of Section2 <see cref="Dtos.Section2"/> objects representing matching sections</returns>
        [Obsolete("Obsolete as of HeDM Version 6, use Accept Header Version 6 instead.")]
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 200)]
        public async Task<IHttpActionResult> GetHedmSectionsAsync(Paging page, [FromUri] string title = "", [FromUri] string startOn = "", [FromUri] string endOn = "",
            [FromUri] string code = "", [FromUri] string number = "", [FromUri] string instructionalPlatform = "", [FromUri] string academicPeriod = "",
            [FromUri] string academicLevels = "", [FromUri] string course = "", [FromUri] string site = "", [FromUri] string status = "", [FromUri] string owningOrganizations = "")
        {
            string criteria = title + startOn + endOn + code + number + instructionalPlatform + academicPeriod + academicLevels + course + site + status + owningOrganizations;
            if (criteria == string.Empty)
            {
                throw new ArgumentNullException("title, startOn, endOn, code, number, instructionalPlatform, academicPeriod, academicLevels, course, site, status, owningOrganizations", "No criteria specified for selection of sections");
            }

            try
            {
                if (page == null)
                {
                    page = new Paging(200, 0);
                }

                var pageOfItems = await _sectionCoordinationService.GetSections2Async(page.Offset, page.Limit, title, startOn, endOn, code, number, instructionalPlatform, academicPeriod, academicLevels, course, site, status, owningOrganizations);

                return new PagedHttpActionResult<IEnumerable<Dtos.Section2>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Read (GET) a section using a GUID
        /// </summary>
        /// <param name="id">GUID to desired section</param>
        /// <returns>A section object <see cref="Dtos.Section"/> in HeDM format</returns>
        [Obsolete("Obsolete as of HeDM Version 6, use Accept Header Version 6 instead.")]
        [HttpGet]
        public async Task<Dtos.Section2> GetHedmSectionByGuidAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                return await _sectionCoordinationService.GetSection2ByGuidAsync(id);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new section
        /// </summary>
        /// <param name="section">DTO of the new section</param>
        /// <returns>A section object <see cref="Dtos.Section"/> in HeDM format</returns>
        [Obsolete("Obsolete as of HeDM Version 6, use Accept Header Version 6 instead.")]
        [HttpPost]
        public async Task<Dtos.Section2> PostHedmSectionAsync(Dtos.Section2 section)
        {
            if (section == null)
            {
                throw CreateHttpResponseException("Request body must contain a valid Section.", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrEmpty(section.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null section id",
                    IntegrationApiUtility.GetDefaultApiError("Id is a required property.")));
            }

            try
            {
                return await _sectionCoordinationService.PostSection2Async(section);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Update (PUT) an existing section
        /// </summary>
        /// <param name="id">GUID of the section to update</param>
        /// <param name="section">DTO of the updated section</param>
        /// <returns>A section object <see cref="Dtos.Section"/> in HeDM format</returns>
        [Obsolete("Obsolete as of HeDM Version 6, use Accept Header Version 6 instead.")]
        [HttpPut]
        public async Task<Dtos.Section2> PutHedmSectionAsync([FromUri] string id, [FromBody] Dtos.Section2 section)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            if (section == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null section argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }
            if (id.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                throw CreateHttpResponseException("Nil GUID cannot be used in PUT operation.", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrEmpty(section.Id))
            {
                section.Id = id.ToLowerInvariant();
            }
            else if (id.ToLowerInvariant() != section.Id.ToLowerInvariant())
            {
                throw CreateHttpResponseException(new IntegrationApiException("GUID mismatch",
                    IntegrationApiUtility.GetDefaultApiError("GUID not the same as in request body.")));
            }

            try
            {
                return await _sectionCoordinationService.PutSection2Async(section);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Delete (DELETE) a section
        /// </summary>
        /// <param name="id">GUID to desired section</param>
        /// <returns>A section object <see cref="Dtos.Section"/> in HeDM format</returns>
        [Obsolete("Obsolete as of HeDM Version 6, use Accept Header Version 6 instead.")]
        [HttpDelete]
        public async Task DeleteHedmSectionByGuidAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                var section = await _sectionCoordinationService.GetSection2ByGuidAsync(id);
                if (section == null)
                {
                    throw CreateHttpResponseException(new IntegrationApiException("Invalid section argument",
                        IntegrationApiUtility.GetDefaultApiError("The requested section could not be found.")));
                }
                if (string.IsNullOrEmpty(section.Id))
                {
                    section.Id = id.ToLowerInvariant();
                }
                else if (id.ToLowerInvariant() != section.Id.ToLowerInvariant())
                {
                    throw CreateHttpResponseException(new IntegrationApiException("GUID mismatch",
                        IntegrationApiUtility.GetDefaultApiError("GUID in request URL is not the same as the section GUID.")));
                }
                // Change status to Cancelled instead of actually deleting the section.
                section.Status = Dtos.SectionStatus2.Cancelled;
                var responseDto = await _sectionCoordinationService.PutSection2Async(section);
                // On delete, just return nothing.
                return;
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Update (PUT) section registrations
        /// </summary>
        /// <param name="guid">GUID of the Section</param>
        /// <param name="sectionRegistration">DTO of the SectionRegistration</param>
        /// <returns>A registration response object</returns>
        [Obsolete("Obsolete as of HeDM Version 4, use Accept Header Version 4 instead.")]
        [HttpPut]
        public async Task<Dtos.SectionRegistration> PutSectionRegistrationAsync([FromUri] string guid, [FromBody] Dtos.SectionRegistration sectionRegistration)
        {
            if (sectionRegistration == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null section argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }
            if (string.IsNullOrEmpty(sectionRegistration.Guid))
            {
                sectionRegistration.Guid = sectionRegistration.Section.Guid.ToLowerInvariant();
            }
            else if (guid.ToLowerInvariant() != sectionRegistration.Guid.ToLowerInvariant())
            {
                throw CreateHttpResponseException(new IntegrationApiException("GUID mismatch",
                    IntegrationApiUtility.GetDefaultApiError("GUID not the same as in request body.")));
            }

            try
            {
                return await _sectionRegistrationService.UpdateRegistrationAsync(sectionRegistration);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        #endregion

        #region EEDM V6 Methods

        /// <summary>
        /// Return a list of Sections objects based on selection criteria.
        /// </summary>
        /// <param name="page">Section page Contains ...page...</param>
        /// <param name="title">Section Title Contains ...title...</param>
        /// <param name="startOn">Section starts on or after this date</param>
        /// <param name="endOn">Section ends on or before this date</param>
        /// <param name="code">Section Name Contains ...code...</param>
        /// <param name="number">Section Number equal to</param>
        /// <param name="instructionalPlatform">Learning Platform equal to (guid)</param>
        /// <param name="academicPeriod">Section Term equal to (guid)</param>
        /// <param name="academicLevels">Section Academic Level equal to (guid)</param>
        /// <param name="course">Section Course equal to (guid)</param>
        /// <param name="site">Section Location equal to (guid)</param>
        /// <param name="status">Section Status matches closed, open, pending, or cancelled</param>
        /// <param name="owningOrganizations">Section Department equal to (guid)</param>
        /// <returns>List of Section2 <see cref="Dtos.Section3"/> objects representing matching sections</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetHedmSections2Async(Paging page, [FromUri] string title = "", [FromUri] string startOn = "", [FromUri] string endOn = "",
            [FromUri] string code = "", [FromUri] string number = "", [FromUri] string instructionalPlatform = "", [FromUri] string academicPeriod = "",
            [FromUri] string academicLevels = "", [FromUri] string course = "", [FromUri] string site = "", [FromUri] string status = "", [FromUri] string owningOrganizations = "")
        {
            string criteria = title + startOn + endOn + code + number + instructionalPlatform + academicPeriod + academicLevels + course + site + status + owningOrganizations;

            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                if (page == null)
                {
                    page = new Paging(100, 0);
                }
                AddDataPrivacyContextProperty((await _sectionCoordinationService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());

                var pageOfItems = await _sectionCoordinationService.GetSections3Async(page.Offset, page.Limit, title, startOn, endOn, code, number, instructionalPlatform, academicPeriod, academicLevels, course, site, status, owningOrganizations);

                return new PagedHttpActionResult<IEnumerable<Dtos.Section3>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Read (GET) a section using a GUID
        /// </summary>
        /// <param name="id">GUID to desired section</param>
        /// <returns>A section object <see cref="Dtos.Section3"/> in HeDM format</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.Section3> GetHedmSectionByGuid2Async(string id)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                AddDataPrivacyContextProperty((await _sectionCoordinationService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _sectionCoordinationService.GetSection3ByGuidAsync(id);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new section
        /// </summary>
        /// <param name="section">DTO of the new section</param>
        /// <returns>A section object <see cref="Dtos.Section3"/> in HeDM format</returns>
        [HttpPost]
        public async Task<Dtos.Section3> PostHedmSection2Async(Dtos.Section3 section)
        {
            if (section == null)
            {
                throw CreateHttpResponseException("Request body must contain a valid Section.", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrEmpty(section.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null section id",
                    IntegrationApiUtility.GetDefaultApiError("Id is a required property.")));
            }

            try
            {
                return await _sectionCoordinationService.PostSection3Async(section);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Update (PUT) an existing section
        /// </summary>
        /// <param name="id">GUID of the section to update</param>
        /// <param name="section">DTO of the updated section</param>
        /// <returns>A section object <see cref="Dtos.Section3"/> in HeDM format</returns>
        [HttpPut]
        public async Task<Dtos.Section3> PutHedmSection2Async([FromUri] string id, [FromBody] Dtos.Section3 section)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            if (section == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null section argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }
            if (id.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                throw CreateHttpResponseException("Nil GUID cannot be used in PUT operation.", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrEmpty(section.Id))
            {
                section.Id = id.ToLowerInvariant();
            }
            else if (id.ToLowerInvariant() != section.Id.ToLowerInvariant())
            {
                throw CreateHttpResponseException(new IntegrationApiException("GUID mismatch",
                    IntegrationApiUtility.GetDefaultApiError("GUID not the same as in request body.")));
            }

            try
            {
                await _sectionCoordinationService.DoesUpdateViolateDataPrivacySettings(GetRouteResourceName(), section);
                return await _sectionCoordinationService.PutSection3Async(section);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Delete (DELETE) a section
        /// </summary>
        /// <param name="id">GUID to desired section</param>
        /// <returns>Nothing</returns>
        [HttpDelete]
        public async Task DeleteHedmSectionByGuid2Async(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                var section = await _sectionCoordinationService.GetSection3ByGuidAsync(id);
                if (section == null)
                {
                    throw CreateHttpResponseException(new IntegrationApiException("Invalid section argument",
                        IntegrationApiUtility.GetDefaultApiError("The requested section could not be found.")));
                }
                if (string.IsNullOrEmpty(section.Id))
                {
                    section.Id = id.ToLowerInvariant();
                }
                else if (id.ToLowerInvariant() != section.Id.ToLowerInvariant())
                {
                    throw CreateHttpResponseException(new IntegrationApiException("GUID mismatch",
                        IntegrationApiUtility.GetDefaultApiError("GUID in request URL is not the same as the section GUID.")));
                }
                // Change status to Cancelled instead of actually deleting the section.
                section.Status = Dtos.SectionStatus2.Cancelled;
                var responseDto = await _sectionCoordinationService.PutSection3Async(section);
                // On delete, just return nothing.
                return;
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        #endregion


        #region EEDM V8 Methods

        /// <summary>
        /// Return a list of Sections objects based on selection criteria.
        /// </summary>
        /// <param name="page"> - Section page Contains ...page...</param>
        /// <param name="criteria"> - JSON formatted selection criteria.  Can contain:</param>
        /// <param name="searchable"></param>
        /// <param name="keywordSearch"></param>
        /// "title" - Section Title Contains ...title...
        /// "startOn" - Section starts on or after this date
        /// "endOn" - Section ends on or before this date
        /// "code" - Section Name Contains ...code...
        /// "number" - Section Number equal to
        /// "instructionalPlatform" - Learning Platform equal to (guid)
        /// "academicPeriod" - Section Term equal to (guid)
        /// "academicLevels" - Section Academic Level equal to (guid)
        /// "course" - Section Course equal to (guid)
        /// "site" - Section Location equal to (guid)
        /// "status" - Section Status matches closed, open, pending, or cancelled
        /// "owningInstitutionUnits" - Section Department equal to (guid) [renamed from owningOrganizations in v8]
        /// <returns>List of Section4 <see cref="Dtos.Section4"/> objects representing matching sections</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetHedmSections4Async(Paging page, [FromUri] string criteria = "",
            [FromUri] string searchable = "", [FromUri] string keywordSearch = "")
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                string title = string.Empty, startOn = string.Empty, endOn = string.Empty, code = string.Empty,
                    number = string.Empty, instructionalPlatform = string.Empty, academicPeriod = string.Empty, academicLevels = string.Empty,
                    course = string.Empty, site = string.Empty, status = string.Empty, owningOrganizations = string.Empty,
                    instructor = string.Empty, subject = string.Empty, keyword = string.Empty;

                SectionsSearchable search = SectionsSearchable.NotSet;

                if (!string.IsNullOrEmpty(searchable))
                {

                    var searchString = Regex.Replace(searchable, "[^0-9a-zA-Z]+", "");
                    search = GetEnumFromEnumMemberAttribute<SectionsSearchable>(searchString, SectionsSearchable.NotSet);

                    if (search == SectionsSearchable.NotSet)
                    {
                        throw new ArgumentException(string.Concat("Invalid 'searchable' value received: ", searchable));
                    }

                }

                if (!string.IsNullOrEmpty(keywordSearch))
                {
                    keyword = Regex.Replace(keywordSearch, "[^0-9a-zA-Z]+", "");
                }


                if (!string.IsNullOrEmpty(criteria))
                {
                    var criteriaValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(criteria);

                    if (criteriaValues != null)
                    {
                        foreach (var value in criteriaValues)
                        {
                            switch (value.Key.ToLower())
                            {
                                case "title":
                                    title = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "starton":
                                    startOn = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "endon":
                                    endOn = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "code":
                                    code = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "number":
                                    number = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "instructionalplatform":
                                    instructionalPlatform = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "academicperiod":
                                    academicPeriod = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "academiclevels":
                                    academicLevels = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "course":
                                    course = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "site":
                                    site = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "status":
                                    status = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "owninginstitutionunits":
                                    owningOrganizations = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "subject":
                                    subject = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "instructor":
                                    instructor = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                default:
                                    throw new ArgumentException(string.Concat("Invalid filter value received: ", value.Key));
                            }
                        }
                    }
                }

                AddDataPrivacyContextProperty((await _sectionCoordinationService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());

                var pageOfItems = await _sectionCoordinationService.GetSections4Async(page.Offset, page.Limit,
                    title, startOn, endOn, code, number, instructionalPlatform, academicPeriod, academicLevels,
                    course, site, status, owningOrganizations, subject, instructor, search, keyword);

                return new PagedHttpActionResult<IEnumerable<Dtos.Section4>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (JsonReaderException e)
            {
                _logger.Error(e.ToString());

                throw CreateHttpResponseException(new IntegrationApiException("Deserialization Error",
                    IntegrationApiUtility.GetDefaultApiError("Error parsing JSON section search request.")));
            }
            catch (JsonSerializationException e)
            {
                _logger.Error(e.ToString());

                throw CreateHttpResponseException(new IntegrationApiException("Deserialization Error",
                    IntegrationApiUtility.GetDefaultApiError("Error parsing JSON section search request.")));
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Read (GET) a section using a GUID
        /// </summary>
        /// <param name="id">GUID to desired section</param>
        /// <returns>A section object <see cref="Dtos.Section4"/> in HeDM format</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.Section4> GetHedmSectionByGuid3Async(string id)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                AddDataPrivacyContextProperty((await _sectionCoordinationService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _sectionCoordinationService.GetSection4ByGuidAsync(id);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Create (POST) a new section
        /// </summary>
        /// <param name="section">DTO of the new section</param>
        /// <returns>A section object <see cref="Dtos.Section4"/> in HeDM format</returns>
        [HttpPost]
        public async Task<Dtos.Section4> PostHedmSection4Async(Dtos.Section4 section)
        {
            if (section == null)
            {
                throw CreateHttpResponseException("Request body must contain a valid Section.", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrEmpty(section.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null section id",
                    IntegrationApiUtility.GetDefaultApiError("Id is a required property.")));
            }

            try
            {
                return await _sectionCoordinationService.PostSection4Async(section);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Update (PUT) an existing section
        /// </summary>
        /// <param name="id">GUID of the section to update</param>
        /// <param name="section">DTO of the updated section</param>
        /// <returns>A section object <see cref="Dtos.Section4"/> in HeDM format</returns>
        [HttpPut]
        public async Task<Dtos.Section4> PutHedmSection4Async([FromUri] string id, [FromBody] Dtos.Section4 section)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            if (section == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null section argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }
            if (id.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                throw CreateHttpResponseException("Nil GUID cannot be used in PUT operation.", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrEmpty(section.Id))
            {
                section.Id = id.ToLowerInvariant();
            }
            else if (id.ToLowerInvariant() != section.Id.ToLowerInvariant())
            {
                throw CreateHttpResponseException(new IntegrationApiException("GUID mismatch",
                    IntegrationApiUtility.GetDefaultApiError("GUID not the same as in request body.")));
            }

            try
            {
                await _sectionCoordinationService.DoesUpdateViolateDataPrivacySettings(GetRouteResourceName(), section);
                return await _sectionCoordinationService.PutSection4Async(section);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Delete (DELETE) a section
        /// </summary>
        /// <param name="id">GUID to desired section</param>
        /// <returns>A section object <see cref="Dtos.Section4"/> in HeDM format</returns>
        [HttpDelete]
        public async Task DeleteHedmSectionByGuid4Async(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            try
            {
                var section = await _sectionCoordinationService.GetSection4ByGuidAsync(id);
                if (section == null)
                {
                    throw CreateHttpResponseException(new IntegrationApiException("Invalid section argument",
                        IntegrationApiUtility.GetDefaultApiError("The requested section could not be found.")));
                }
                if (string.IsNullOrEmpty(section.Id))
                {
                    section.Id = id.ToLowerInvariant();
                }
                else if (id.ToLowerInvariant() != section.Id.ToLowerInvariant())
                {
                    throw CreateHttpResponseException(new IntegrationApiException("GUID mismatch",
                        IntegrationApiUtility.GetDefaultApiError("GUID in request URL is not the same as the section GUID.")));
                }
                // Change status to Cancelled instead of actually deleting the section.
                section.Status = Dtos.SectionStatus2.Cancelled;
                var responseDto = await _sectionCoordinationService.PutSection4Async(section);
                // On delete, just return nothing.
                return;
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        #endregion


        /// <summary>
        /// Query by post method used to get the sections for the given section Ids. 
        /// If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <param name="criteria">DTO Object with a list of Section keys</param>
        /// <returns>The requested <see cref="Section3">Sections</see></returns>
        [HttpPost]
        public async Task<IEnumerable<Section3>> QuerySectionsByPost3Async([FromBody] SectionsQueryCriteria criteria)
        {
            IEnumerable<string> sectionIds = criteria.SectionIds;
            bool bestFit = criteria.BestFit;

            if (sectionIds == null || sectionIds.Count() == 0)
            {
                string errorText = "At least one item in list of sectionIds must be provided.";
                _logger.Error(errorText);
                throw CreateHttpResponseException(errorText, HttpStatusCode.BadRequest);
            }

            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section> sectionEntities = new List<Ellucian.Colleague.Domain.Student.Entities.Section>();
            bool useCache = true;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    useCache = false;
                }
            }
            if (useCache)
            {
                sectionEntities = await _sectionRepository.GetCachedSectionsAsync(sectionIds, bestFit);
            }
            else
            {
                sectionEntities = await _sectionRepository.GetNonCachedSectionsAsync(sectionIds, bestFit);
            }

            List<Section3> sectionDtos = new List<Section3>();
            var sectionDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Section, Section3>();
            foreach (var section in sectionEntities)
            {
                Section3 sectionDto = sectionDtoAdapter.MapToType(section);
                sectionDtos.Add(sectionDto);
            }
            return sectionDtos;
        }

       

        /// <summary>
        /// Puts a collection of student section grades.
        /// </summary>
        /// <returns><see cref="Dtos.Student.Grade">StudentSectionGradeResponse</see></returns>
        [Obsolete("Obsolete , use version 2 of this API")]
        public async Task<IEnumerable<SectionGradeResponse>> PutCollectionOfStudentGradesAsync([FromUri] string sectionId, [FromBody] SectionGrades sectionGrades)
        {
            try
            {
                if (ModelState != null && !ModelState.IsValid)
                {
                    var modelErrors = ModelState.Values.SelectMany(v => v.Errors).ToList();
                    if (modelErrors != null && modelErrors.Count() > 0)
                    {
                        var formatExceptions = modelErrors.Where(x => x.Exception is System.FormatException).Select(x => x.Exception as System.FormatException).ToList();

                        if (formatExceptions != null && formatExceptions.Count() > 0)
                        {
                            throw formatExceptions.First();
                        }
                    }
                }

                if (string.IsNullOrEmpty(sectionGrades.SectionId))
                {
                    throw new ArgumentException("SectionId", "Section Id must be provided.");
                }

                // Compare uri value to body value for section Id
                if (!sectionId.Equals(sectionGrades.SectionId))
                {
                    throw new ArgumentException("sectionId", "Section Ids do not match in the request.");
                }

                if (sectionGrades.StudentGrades == null || sectionGrades.StudentGrades.Count() == 0)
                {
                    throw new ArgumentException("StudentGrades", "At least one student grade must be provided.");
                }

                var returnDto = await _sectionCoordinationService.ImportGradesAsync(sectionGrades);
                return returnDto;
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.Message);
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }


        /// <summary>
        /// Puts a collection of student section grades.
        /// </summary>
        /// <returns><see cref="Dtos.Student.Grade">StudentSectionGradeResponse</see></returns>
        [Obsolete("Obsolete as of Api version 1.12, use version 3 of this API")]
        public async Task<IEnumerable<SectionGradeResponse>> PutCollectionOfStudentGrades2Async([FromUri] string sectionId, [FromBody] SectionGrades2 sectionGrades)
        {
            try
            {
                if (ModelState != null && !ModelState.IsValid)
                {
                    var modelErrors = ModelState.Values.SelectMany(v => v.Errors).ToList();
                    if (modelErrors != null && modelErrors.Count() > 0)
                    {
                        var formatExceptions = modelErrors.Where(x => x.Exception is System.FormatException).Select(x => x.Exception as System.FormatException).ToList();

                        if (formatExceptions != null && formatExceptions.Count() > 0)
                        {
                            throw formatExceptions.First();
                        }
                    }
                }

                if (string.IsNullOrEmpty(sectionGrades.SectionId))
                {
                    throw new ArgumentException("SectionId", "Section Id must be provided.");
                }

                // Compare uri value to body value for section Id
                if (!sectionId.Equals(sectionGrades.SectionId))
                {
                    throw new ArgumentException("sectionId", "Section Ids do not match in the request.");
                }

                if (sectionGrades.StudentGrades == null || sectionGrades.StudentGrades.Count() == 0)
                {
                    throw new ArgumentException("StudentGrades", "At least one student grade must be provided.");
                }

                var returnDto = await _sectionCoordinationService.ImportGrades2Async(sectionGrades);
                return returnDto;
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.Message);
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Puts a collection of student section grades.
        /// </summary>
        /// <param name="sectionId">Section ID</param>
        /// <param name="sectionGrades">DTO of section grade information</param>
        /// <returns><see cref="Dtos.Student.Grade">StudentSectionGradeResponse</see></returns>
        [Obsolete("Obsolete as of Api version 1.13, use version 4 for non-ILP callers, or version 1 of the json ILP header for ILP callers")]
        public async Task<IEnumerable<SectionGradeResponse>> PutCollectionOfStudentGrades3Async([FromUri] string sectionId, [FromBody] SectionGrades3 sectionGrades)
        {
            try
            {
                if (ModelState != null && !ModelState.IsValid)
                {
                    var modelErrors = ModelState.Values.SelectMany(v => v.Errors).ToList();
                    if (modelErrors != null && modelErrors.Count() > 0)
                    {
                        var formatExceptions = modelErrors.Where(x => x.Exception is System.FormatException).Select(x => x.Exception as System.FormatException).ToList();

                        if (formatExceptions != null && formatExceptions.Count() > 0)
                        {
                            throw formatExceptions.First();
                        }
                    }
                }

                if (string.IsNullOrEmpty(sectionGrades.SectionId))
                {
                    throw new ArgumentException("SectionId", "Section Id must be provided.");
                }

                // Compare uri value to body value for section Id
                if (!sectionId.Equals(sectionGrades.SectionId))
                {
                    throw new ArgumentException("sectionId", "Section Ids do not match in the request.");
                }

                if (sectionGrades.StudentGrades == null || sectionGrades.StudentGrades.Count() == 0)
                {
                    throw new ArgumentException("StudentGrades", "At least one student grade must be provided.");
                }

                var returnDto = await _sectionCoordinationService.ImportGrades3Async(sectionGrades);
                return returnDto;
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.Message);
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Puts a collection of student section grades from a standard non-ILP caller.
        /// </summary>
        /// <param name="sectionId">Section ID</param>
        /// <param name="sectionGrades">DTO of section grade information</param>
        /// <returns><see cref="Dtos.Student.Grade">StudentSectionGradeResponse</see></returns>
        public async Task<IEnumerable<SectionGradeResponse>> PutCollectionOfStudentGrades4Async([FromUri] string sectionId, [FromBody] SectionGrades3 sectionGrades)
        {
            try
            {
                if (ModelState != null && !ModelState.IsValid)
                {
                    var modelErrors = ModelState.Values.SelectMany(v => v.Errors).ToList();
                    if (modelErrors != null && modelErrors.Count() > 0)
                    {
                        var formatExceptions = modelErrors.Where(x => x.Exception is System.FormatException).Select(x => x.Exception as System.FormatException).ToList();

                        if (formatExceptions != null && formatExceptions.Count() > 0)
                        {
                            throw formatExceptions.First();
                        }
                    }
                }

                if (string.IsNullOrEmpty(sectionGrades.SectionId))
                {
                    throw new ArgumentException("SectionId", "Section Id must be provided.");
                }

                // Compare uri value to body value for section Id
                if (!sectionId.Equals(sectionGrades.SectionId))
                {
                    throw new ArgumentException("sectionId", "Section Ids do not match in the request.");
                }

                if (sectionGrades.StudentGrades == null || sectionGrades.StudentGrades.Count() == 0)
                {
                    throw new ArgumentException("StudentGrades", "At least one student grade must be provided.");
                }

                var returnDto = await _sectionCoordinationService.ImportGrades4Async(sectionGrades);
                return returnDto;
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.Message);
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Puts a collection of student section grades from an ILP caller.
        /// </summary>
        /// <param name="sectionId">Section ID</param>
        /// <param name="sectionGrades">DTO of section grade information</param>
        /// <returns><see cref="Dtos.Student.Grade">StudentSectionGradeResponse</see></returns>
        public async Task<IEnumerable<SectionGradeResponse>> PutIlpCollectionOfStudentGrades1Async([FromUri] string sectionId, [FromBody] SectionGrades3 sectionGrades)
        {
            try
            {
                if (ModelState != null && !ModelState.IsValid)
                {
                    var modelErrors = ModelState.Values.SelectMany(v => v.Errors).ToList();
                    if (modelErrors != null && modelErrors.Count() > 0)
                    {
                        var formatExceptions = modelErrors.Where(x => x.Exception is System.FormatException).Select(x => x.Exception as System.FormatException).ToList();

                        if (formatExceptions != null && formatExceptions.Count() > 0)
                        {
                            throw formatExceptions.First();
                        }
                    }
                }

                if (string.IsNullOrEmpty(sectionGrades.SectionId))
                {
                    throw new ArgumentException("SectionId", "Section Id must be provided.");
                }

                // Compare uri value to body value for section Id
                if (!sectionId.Equals(sectionGrades.SectionId))
                {
                    throw new ArgumentException("sectionId", "Section Ids do not match in the request.");
                }

                if (sectionGrades.StudentGrades == null || sectionGrades.StudentGrades.Count() == 0)
                {
                    throw new ArgumentException("StudentGrades", "At least one student grade must be provided.");
                }

                var returnDto = await _sectionCoordinationService.ImportIlpGrades1Async(sectionGrades);
                return returnDto;
            }
            catch (PermissionsException pex)
            {
                _logger.Error(pex.Message);
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Query by post method used to get the section registration date overrides for any of the specified section Ids based on the registration group of the person making the request. 
        /// </summary>
        /// <param name="criteria">DTO Object that contains the list of Section ids for which registration dates are requested.</param>
        /// <returns><see cref="SectionRegistrationDate">SectionRegistrationDate</see> DTOs.</returns>
        [HttpPost]
        public async Task<IEnumerable<SectionRegistrationDate>> QuerySectionRegistrationDatesAsync([FromBody] SectionDateQueryCriteria criteria)
        {
            IEnumerable<string> sectionIds = criteria.SectionIds;

            if (sectionIds == null || sectionIds.Count() == 0)
            {
                string errorText = "At least one item in list of sectionIds must be provided.";
                _logger.Error(errorText);
                throw CreateHttpResponseException(errorText, HttpStatusCode.BadRequest);
            }
            try
            {
                return await _registrationGroupService.GetSectionRegistrationDatesAsync(sectionIds);

            }
            catch (Exception e)
            {
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }

        }

        private static TEnum GetEnumFromEnumMemberAttribute<TEnum>(string value, TEnum defaultValue) where TEnum : struct
        {
            if (string.IsNullOrEmpty(value))
            {
                return defaultValue;
            }

            TEnum result;
            var enumType = typeof (TEnum);
            foreach (var enumName in Enum.GetNames(enumType))
            {
                var fieldInfo = enumType.GetField(enumName);
                var enumMemberAttribute = ((EnumMemberAttribute[]) fieldInfo.GetCustomAttributes(typeof (EnumMemberAttribute), true)).FirstOrDefault();
                if (enumMemberAttribute != null && enumMemberAttribute.Value == value)
                {
                    return Enum.TryParse(enumName, true, out result) ? result : defaultValue;
                }
            }
            //return Enum.TryParse(value, true, out result) ? result : defaultValue;
            return defaultValue;
        }
    }
}
