﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Net;
using System.Net.Http.Headers;
using Ellucian.Web.Http.Controllers;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Dtos.Student;
using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Adapters;
using Ellucian.Colleague.Coordination.Student.Services;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Dtos.Resources;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http;
using Ellucian.Colleague.Dtos;

namespace Ellucian.Colleague.Api.Controllers.Student
{
    /// <summary>
    /// Controller for Admission Decision Types
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class AdmissionDecisionTypesController : BaseCompressedApiController
    {
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the AdmissionDecisionTypeController class.
        /// </summary>
        /// <param name="logger">Interface to Logger</param>
        public AdmissionDecisionTypesController(ILogger logger)
        {
            _logger = logger;
        }


        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Retrieves all Admission Decision Types
        /// </summary>
        /// <returns>All <see cref="Dtos.AdmissionDecisionTypes">AdmissionDecisionTypes.</see></returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.AdmissionDecisionTypes>> GetAdmissionDecisionTypesAsync()
        {
            return new List<AdmissionDecisionTypes>();
        }

        /// <summary>
        /// Retrieve (GET) an existing Admission Decision Type
        /// </summary>
        /// <param name="guid">GUID of the admissionDecisionType to get</param>
        /// <returns>A admissionDecisionType object <see cref="Dtos.AdmissionDecisionTypes"/> in EEDM format</returns>
        [HttpGet]
        public async Task<Dtos.AdmissionDecisionTypes> GetAdmissionDecisionTypeByGuidAsync([FromUri] string guid)
        {
            try
            {
                throw new Exception(string.Format("No admission decision type was found for guid {0}.", guid));
            }
            catch (Exception e)
            {
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
        }

        /// <summary>
        /// Updates an AdmissionDecisionTypes.
        /// </summary>
        /// <param name="admissionDecisionTypes"><see cref="Dtos.AdmissionDecisionTypes">AdmissionDecisionTypes</see> to update</param>
        /// <returns>Newly updated <see cref="Dtos.AdmissionDecisionTypes">AdmissionDecisionTypes</see></returns>
        [HttpPut]
        public async Task<Dtos.AdmissionDecisionTypes> PutAdmissionDecisionTypesAsync([FromBody] Dtos.AdmissionDecisionTypes admissionDecisionTypes)
        {
            //Create is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Creates a AdmissionDecisionTypes.
        /// </summary>
        /// <param name="admissionDecisionTypes"><see cref="Dtos.AdmissionDecisionTypes">AdmissionDecisionTypes</see> to create</param>
        /// <returns>Newly created <see cref="Dtos.AdmissionDecisionTypes">AdmissionDecisionTypes</see></returns>
        [HttpPost]
        public async Task<Dtos.AdmissionDecisionTypes> PostAdmissionDecisionTypesAsync([FromBody] Dtos.AdmissionDecisionTypes admissionDecisionTypes)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Delete (DELETE) an existing AdmissionDecisionTypes
        /// </summary>
        /// <param name="guid">Id of the AdmissionDecisionTypes to delete</param>
        [HttpDelete]
        public async Task<Dtos.AdmissionDecisionTypes> DeleteAdmissionDecisionTypesAsync(string guid)
        {
            //Delete is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }
    }
}
