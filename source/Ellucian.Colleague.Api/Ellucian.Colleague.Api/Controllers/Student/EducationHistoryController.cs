﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using Ellucian.Web.Http.Controllers;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Web.Http;
using Ellucian.Web.Http.Filters;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Api.Licensing;
using slf4net;

using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Api.Controllers
{
    /// <summary>
    /// Provides access to EducationHistory data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class EducationHistoryController : BaseCompressedApiController
    {
        private readonly IEducationHistoryRepository _educationHistoryRepository;
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the EducationHistoryController class.
        /// </summary>
        /// <param name="adapterRegistry">Adapter registry of type <see cref="IAdapterRegistry">IAdapterRegistry</see></param>
        /// <param name="educationHistoryRepository">Repository of type <see cref="IEducationHistoryRepository">IEducationHistoryRepository</see></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public EducationHistoryController(IAdapterRegistry adapterRegistry, IEducationHistoryRepository educationHistoryRepository, ILogger logger)
        {
            _adapterRegistry = adapterRegistry;
            _educationHistoryRepository = educationHistoryRepository;
            _logger = logger;
        }

        /// <summary>
        /// Get Education History from a list of Student keys
        /// </summary>
        /// <param name="criteria">DTO Object containing a list of Student Keys for selection</param>
        /// <returns>List of EducationHistory Objects <see cref="Ellucian.Colleague.Dtos.Student.EducationHistory">EducationHistory</see></returns>
        [HttpPost]
        public  async Task<IEnumerable<Ellucian.Colleague.Dtos.Student.EducationHistory>> QueryEducationHistoryAsync([FromBody] EducationHistoryQueryCriteria criteria)
        {
            IEnumerable<string> studentIds = criteria.StudentIds;

            if (studentIds.Count() <= 0)
            {
                _logger.Error("Invalid studentIds parameter");
                throw CreateHttpResponseException("The studentIds are required.", HttpStatusCode.BadRequest);
            }
            try
            {
                var educationHistoryDtoCollection = new List<Ellucian.Colleague.Dtos.Student.EducationHistory>();
                var educationHistoryCollection = await _educationHistoryRepository.GetAsync(studentIds);
                // Get the right adapter for the type mapping
                var educationHistoryDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.EducationHistory, Ellucian.Colleague.Dtos.Student.EducationHistory>();
                // Map the Address entity to the Address DTO
                foreach (var address in educationHistoryCollection)
                {
                    educationHistoryDtoCollection.Add(educationHistoryDtoAdapter.MapToType(address));
                }

                return educationHistoryDtoCollection;
            }
            catch (PermissionsException pex)
            {
                throw CreateHttpResponseException(pex.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                throw CreateHttpResponseException(e.Message);
            }
        }
    }
}
