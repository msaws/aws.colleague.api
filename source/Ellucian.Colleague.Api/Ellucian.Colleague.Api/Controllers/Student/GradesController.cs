﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.Http.Filters;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Dtos.Student;
using System.Web.Http;
using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using slf4net;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Adapters;
using System.Threading.Tasks;
using System;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;

namespace Ellucian.Colleague.Api.Controllers
{
    /// <summary>
    /// Provides access to Grade data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class GradesController : BaseCompressedApiController
    {
        private readonly IGradeRepository _gradeRepository;
        private readonly IGradeService _gradeService;
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the GradesController class.
        /// </summary>
        /// <param name="adapterRegistry">Adapter registry of type <see cref="IAdapterRegistry">IAdapterRegistry</see></param>
        /// <param name="gradeRepository">Repository of type <see cref="IGradeRepository">IGradeRepository</see></param>
        /// <param name="gradeService">Service of type <see cref="IGradeService">IGradeService</see></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public GradesController(IAdapterRegistry adapterRegistry, IGradeRepository gradeRepository, IGradeService gradeService, ILogger logger)
        {
            _adapterRegistry = adapterRegistry;
            _gradeRepository = gradeRepository;
            _gradeService = gradeService;
            _logger = logger;
        }

        #region Grades

        /// <summary>
        /// Retrieves information for all Grades.
        /// </summary>
        /// <returns>All <see cref="Grade">Grades</see></returns>
        /// [CacheControlFilter(Public = true, MaxAgeHours = 1, Revalidate = true)]
        public async Task<IEnumerable<Grade>> GetAsync()
        {
            var gradeDtoCollection = new List<Grade>();
            var gradeCollection = await _gradeRepository.GetAsync();
            // Get the right adapter for the type mapping
            var gradeDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.Grade, Grade>();
            // Map the grade entity to the grade DTO
            foreach (var grade in gradeCollection)
            {
                gradeDtoCollection.Add(gradeDtoAdapter.MapToType(grade));
            }

            return gradeDtoCollection;

        }

        /// <summary>
        /// Gets PilotGrades based on query criteria such as studentIds and term.
        /// </summary>
        /// <param name="criteria">A <see cref="GradeQueryCriteria"/> grade criteria object</param>
        /// <returns><see cref="PilotGrade"/>PilotGrades that match the criteria parameters.</returns>
        public async Task<IEnumerable<PilotGrade>> QueryPilotGradesAsync([FromBody] GradeQueryCriteria criteria)
        {
            var gradeDtoCollection = new List<Dtos.Student.PilotGrade>();
            var studentIds = criteria.StudentIds;
            var term = criteria.Term;
            var gradeCollection = await _gradeService.GetPilotGradesAsync(studentIds, term);
            // Get the right adapter for the type mapping
            var gradeDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.PilotGrade, PilotGrade>();
            // Map the grade entity to the grade DTO
            foreach (var grade in gradeCollection)
            {
                gradeDtoCollection.Add(gradeDtoAdapter.MapToType(grade));
            }

            return gradeDtoCollection;
        }
        
        #endregion

        #region grade-definitions

        /// <summary>
        /// Retrieves information for all Grades.
        /// If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <returns>All <see cref="Grade">Grades</see></returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.Grade>> GetHedmAsync()
        {
            bool bypassCache = false;
            if(Request.Headers.CacheControl != null)
            {
                if(Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                return await _gradeService.GetAsync(bypassCache);
            }
            catch(Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }
        }
        /// <summary>
        /// Retrieves grade by id
        /// </summary>
        /// <param name="id">The Id of the grade</param>
        /// <returns>The requested <see cref="Grade">Grade</see></returns>
        public async Task<Ellucian.Colleague.Dtos.Grade> GetByIdHedmAsync(string id)
        {
            try
            {
                return await _gradeService.GetGradeByIdAsync(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }
        }

        /// <summary>
        /// Create a grade
        /// </summary>
        /// <param name="grade">grade</param>
        /// <returns>A section object <see cref="Dtos.Grade"/> in HeDM format</returns>
        public async Task<Dtos.Grade> PostGradeAsync([FromBody] Ellucian.Colleague.Dtos.Grade grade)
        {
            //POST is not supported for Colleague but Hedm requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Update a grade
        /// </summary>
        /// <param name="id">desired id for a grade</param>
        /// <param name="grade">grade</param>
        /// <returns>A section object <see cref="Dtos.Grade"/> in HeDM format</returns>
        public async Task<Dtos.Grade> PutGradeAsync([FromUri] string id, [FromBody] Dtos.Grade grade)
        {
            //POST is not supported for Colleague but Hedm requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Delete (DELETE) a grade
        /// </summary>
        /// <param name="id">id to desired grade</param>
        /// <returns>A section object <see cref="Dtos.Grade"/> in HeDM format</returns>
        [HttpDelete]
        public async Task<Ellucian.Colleague.Dtos.Grade> DeleteGradeByIdAsync(string id)
        {
            //Delete is not supported for Colleague but Hedm requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        #endregion

        #region grade-definitions-maximum

        /// <summary>
        /// Retrieves information for all Grades.
        /// If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned.
        /// </summary>
        /// <returns>All <see cref="GradeDefinitionsMaximum">GradeDefinitionsMaximum</see></returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.GradeDefinitionsMaximum>> GetGradeDefinitionsMaximumAsync()
        {
            bool bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                return await _gradeService.GetGradesDefinitionsMaximumAsync(bypassCache);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }
        }
        /// <summary>
        /// Retrieves grade by id
        /// </summary>
        /// <param name="id">The Id of the grade</param>
        /// <returns>The requested <see cref="GradeDefinitionsMaximum">GradeDefinitionsMaximum</see></returns>                            
        public async Task<Ellucian.Colleague.Dtos.GradeDefinitionsMaximum> GetGradeDefinitionsMaximumByIdAsync(string id)
        {
            return await _gradeService.GetGradesDefinitionsMaximumIdAsync(id);
        }

        /// <summary>
        /// Create a GradeDefinitionsMaximum
        /// </summary>
        /// <param name="grade">grade</param>
        /// <returns>A GradeDefinitionsMaximum object <see cref="Dtos.GradeDefinitionsMaximum"/> in HeDM format</returns>
        public async Task<Dtos.GradeDefinitionsMaximum> PostGradeDefinitionsMaximumAsync([FromBody] Ellucian.Colleague.Dtos.GradeDefinitionsMaximum grade)
        {
            //POST is not supported for Colleague but Hedm requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Update a GradeDefinitionsMaximum
        /// </summary>
        /// <param name="id">desired id for a grade</param>
        /// <param name="grade">grade</param>
        /// <returns>A GradeDefinitionsMaximum object <see cref="Dtos.GradeDefinitionsMaximum"/> in HeDM format</returns>
        public async Task<Dtos.Grade> PutGradeDefinitionsMaximumAsync([FromUri] string id, [FromBody] Dtos.GradeDefinitionsMaximum grade)
        {
            //POST is not supported for Colleague but Hedm requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Delete (DELETE) a GradeDefinitionsMaximum
        /// </summary>
        /// <param name="id">id to desired grade</param>
        /// <returns>A GradeDefinitionsMaximum object <see cref="Dtos.GradeDefinitionsMaximum"/> in HeDM format</returns>
        [HttpDelete]
        public async Task<Ellucian.Colleague.Dtos.GradeDefinitionsMaximum> DeleteGradeDefinitionsMaximumByIdAsync(string id)
        {
            //Delete is not supported for Colleague but Hedm requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        #endregion

    }
}
