﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using Ellucian.Web.Http.Controllers;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Web.Http.Filters;
using System.Web.Http;
using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using slf4net;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using System.Linq;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Base.Exceptions;
using System.Threading.Tasks;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http;
using Ellucian.Colleague.Domain.Exceptions;
using Newtonsoft.Json;

namespace Ellucian.Colleague.Api.Controllers
{
    /// <summary>
    /// Provides access to Course data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class CoursesController : BaseCompressedApiController
    {
        private readonly ICourseService _courseService;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the CoursesController class.
        /// </summary>
        /// <param name="service">Service of type <see cref="ICourseService">ICourseService</see></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public CoursesController(ICourseService service, ILogger logger)
        {
            _courseService = service;
            _logger = logger;
        }


        /// <summary>
        /// Performs a search of courses in Colleague that are available for registration. 
        /// The criteria supplies a keyword, requirement, and various filters which may be used to search and narrow a list of courses.
        /// </summary>
        /// <param name="criteria"><see cref="CourseSearchCriteria">Course search criteria</see></param>
        /// <param name="pageSize">integer page size</param>
        /// <param name="pageIndex">integer page index</param>
        /// <returns>A <see cref="CoursePage">page</see> of courses matching criteria with totals and filter information</returns>
        // [CacheControlFilter(MaxAgeHours = 1, Public = true, Revalidate = true)]
        [Obsolete("Obsolete as of API version 1.3, use version 2 of this API")]
        public async Task<CoursePage> PostSearchAsync([FromBody]CourseSearchCriteria criteria, int pageSize, int pageIndex)
        {
            criteria.Keyword = criteria.Keyword != null ? criteria.Keyword.Replace("_~", "/") : null;
            if (criteria.RequirementGroup != null)
            {
                if (!string.IsNullOrEmpty(criteria.RequirementGroup.RequirementCode))
                {
                    criteria.RequirementGroup.RequirementCode = criteria.RequirementGroup.RequirementCode.Replace("_~", "/");
                }
            }
            if (!string.IsNullOrEmpty(criteria.RequirementCode))
            {
                criteria.RequirementCode = criteria.RequirementCode.Replace("_~", "/");
            }

            try
            {
                // Logging the timings for monitoring
                _logger.Info("Call Course Search Service from Courses controller... ");
                var watch = new Stopwatch();
                watch.Start();
                CoursePage coursesPage = await _courseService.SearchAsync(criteria, pageSize, pageIndex);
                watch.Stop();
                _logger.Info("Course search returned in: " + watch.ElapsedMilliseconds.ToString());
                return coursesPage;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Performs a search of courses in Colleague that are available for registration. 
        /// The criteria supplies a keyword, requirement, and various filters which may be used to search and narrow a list of courses.
        /// </summary>
        /// <param name="criteria"><see cref="CourseSearchCriteria">Course search criteria</see></param>
        /// <param name="pageSize">integer page size</param>
        /// <param name="pageIndex">integer page index</param>
        /// <returns>A <see cref="CoursePage">page</see> of courses matching criteria with totals and filter information.</returns>
        public async Task<CoursePage2> PostSearch2Async([FromBody]CourseSearchCriteria criteria, int pageSize, int pageIndex)
        {
            criteria.Keyword = criteria.Keyword != null ? criteria.Keyword.Replace("_~", "/") : null;
            if (criteria.RequirementGroup != null)
            {
                if (!string.IsNullOrEmpty(criteria.RequirementGroup.RequirementCode))
                {
                    criteria.RequirementGroup.RequirementCode = criteria.RequirementGroup.RequirementCode.Replace("_~", "/");
                }
            }
            if (!string.IsNullOrEmpty(criteria.RequirementCode))
            {
                criteria.RequirementCode = criteria.RequirementCode.Replace("_~", "/");
            }

            try
            {
                // Logging the timings for monitoring
                _logger.Info("Call Course Search Service from Courses controller... ");
                var watch = new Stopwatch();
                watch.Start();
                CoursePage2 coursesPage = await _courseService.Search2Async(criteria, pageSize, pageIndex);
                watch.Stop();
                _logger.Info("Course search returned in: " + watch.ElapsedMilliseconds.ToString());
                return coursesPage;
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString() + ex.StackTrace);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// For each course Id specified, this API will return the sections for this course that are offered in terms "open for registration". 
        /// If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned from the repository.
        /// </summary>
        /// <param name="courseIds">a string of course Ids separated by commas</param>
        /// <returns>A list of <see cref="Section">Sections</see></returns>
        //[CacheControlFilter(MaxAgeHours = 1, Public = true, Revalidate = true)]
        [Obsolete("Obsolete as of API version 1.3, use version 3 of this API")]
        public async Task< IEnumerable<Section>> GetCourseSectionsAsync(string courseIds)
        {
            List<string> courseList = new List<string>();
            if (!String.IsNullOrEmpty(courseIds))
            {
                string[] courseArray = courseIds.Split(new char[] { ',' });
                foreach (var crs in courseArray)
                {
                    courseList.Add(crs);
                }
            }
            bool useCache = true;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    useCache = false;
                }
            }

            return await _courseService.GetSectionsAsync(courseList, useCache);
        }

        /// <summary>
        /// For each course Id specified, this API will return the sections for this course that are offered in terms "open for registration". 
        /// If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned from the repository.
        /// </summary>
        /// <param name="courseIds">a string of course Ids separated by commas</param>
        /// <returns>A list of <see cref="Section2">Sections</see></returns>
        //[CacheControlFilter(MaxAgeHours = 1, Public = true, Revalidate = true)]
        [Obsolete("Obsolete as of API version 1.5, use version 3 of this API")]
        public async Task<IEnumerable<Section2>> GetCourseSections2Async(string courseIds)
        {
            List<string> courseList = new List<string>();
            if (!String.IsNullOrEmpty(courseIds))
            {
                string[] courseArray = courseIds.Split(new char[] { ',' });
                foreach (var crs in courseArray)
                {
                    courseList.Add(crs);
                }
            }
            bool useCache = true;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    useCache = false;
                }
            }

            return await _courseService.GetSections2Async(courseList, useCache);
        }

        /// <summary>
        /// For each course Id specified, this API will return the sections for this course that are offered in terms "open for registration". 
        /// If the request header "Cache-Control" attribute is set to "no-cache" the data returned will be pulled fresh from the database, otherwise cached data is returned from the repository.
        /// </summary>
        /// <param name="courseIds">a string of course Ids separated by commas</param>
        /// <returns>A list of <see cref="Section3">Sections</see></returns>
        //[CacheControlFilter(MaxAgeHours = 1, Public = true, Revalidate = true)]
        public async Task<IEnumerable<Section3>> GetCourseSections3Async(string courseIds)
        {
            List<string> courseList = new List<string>();
            if (!String.IsNullOrEmpty(courseIds))
            {
                string[] courseArray = courseIds.Split(new char[] { ',' });
                foreach (var crs in courseArray)
                {
                    courseList.Add(crs);
                }
            }
            bool useCache = true;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    useCache = false;
                }
            }

            return await _courseService.GetSections3Async(courseList, useCache);
        }

        /// <summary>
        /// Returns courses given the list of course ids.
        /// </summary>
        /// <param name="id">course ID</param>
        /// <returns>A list of <see cref="Course">Courses</see></returns>
        [Obsolete("Obsolete as of API version 1.2, use version 2 of this API")]
        public async Task<List<Course>> GetAsync(string id)
        {

            List<Course> courseList = new List<Course>();
            if (!String.IsNullOrEmpty(id))
            {
                string[] courseIds = id.Split(new char[] { ',' });
                foreach (var crs in courseIds)
                {
                    if (!string.IsNullOrEmpty(crs))
                    {
                        courseList.Add(await _courseService.GetCourseByIdAsync(crs));
                    }
                }
            }
            return courseList;
        }

        /// <summary>
        /// Returns a course for the provided course id.
        /// </summary>
        /// <param name="courseId">course ID</param>
        /// <returns>The requested <see cref="Course">Course</see></returns>
        [Obsolete("Obsolete as of API version 1.3, use version 3 of this API")]
        public async Task<Course> GetCourseAsync(string courseId)
        {
            return await _courseService.GetCourseByIdAsync(courseId);
        }

        /// <summary>
        /// 
        /// Returns a course for the provided course id.
        /// </summary>
        /// <param name="courseId">course ID</param>
        /// <returns>The requested <see cref="Course">Course</see></returns>
        public async Task< Course2> GetCourse2Async(string courseId)
        {
            return await _courseService.GetCourseById2Async(courseId);
        }

        /// <remarks>FOR USE WITH ELLUCIAN CDM</remarks>
        /// <summary>
        /// Retrieves a course for the provided GUID.
        /// </summary>
        /// <param name="guid">The GUID of the course</param>
        /// <returns>The requested <see cref="Course">Course</see></returns>
        [Obsolete("Obsolete as of HeDM Version 4, use Accept Header Version 4 instead.")]
        public async Task<Ellucian.Colleague.Dtos.Course> GetCourseByGuidAsync(string guid)
        {
            return await _courseService.GetCourseByGuidAsync(guid);
        }

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Retrieves a course for the provided Id.
        /// </summary>
        /// <param name="id">The Id of the course</param>
        /// <returns>The requested <see cref="Course">Course</see></returns>
        public async Task<Ellucian.Colleague.Dtos.Course2> GetHedmCourseByIdAsync(string id)
        {
            if(string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException(id, "Course id cannot be null or empty");
            }
            return await _courseService.GetCourseByGuid2Async(id);
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Retrieves a course for the provided Id.
        /// </summary>
        /// <param name="id">The Id of the course</param>
        /// <returns>The requested <see cref="Course3">Course.</see></returns>
        [EedmResponseFilter]
        public async Task<Ellucian.Colleague.Dtos.Course3> GetHedmCourse3ByIdAsync(string id)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException(id, "Course id cannot be null or empty");
            }
            try
            {
                AddDataPrivacyContextProperty((await _courseService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _courseService.GetCourseByGuid3Async(id);
            }
            catch (PermissionsException e)
            {
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (KeyNotFoundException e)
            {
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Retrieves a course for the provided Id.
        /// </summary>
        /// <param name="id">The GUID of the course</param>
        /// <returns>The requested <see cref="Course4">Course.</see></returns>
        [EedmResponseFilter]
        public async Task<Ellucian.Colleague.Dtos.Course4> GetHedmCourse4ByIdAsync(string id)
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException(id, "Course id cannot be null or empty");
            }
            
            try
            {
                AddDataPrivacyContextProperty((await _courseService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _courseService.GetCourseByGuid4Async(id);
            }
            catch (PermissionsException e)
            {
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (KeyNotFoundException e)
            {
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN CDM</remarks>
        /// <summary>
        /// Retrieves all courses.
        /// </summary>
        /// <returns>All <see cref="Course">Courses.</see></returns>
        [Obsolete("Obsolete as of HeDM Version 4, use Accept Header Version 4 instead.")]
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.Course>> GetCdmCoursesAsync()
        {
            try
            {
                return await _courseService.GetCoursesAsync();
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN Hedm</remarks>
        /// <summary>
        /// Retrieves all courses.
        /// </summary>
        /// <returns>All <see cref="Course2">Courses.</see></returns>
        [PagingFilter(IgnorePaging = false, DefaultLimit = 200)]
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.Course2>> GetHedmCoursesAsync()
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                return await _courseService.GetCourses2Async(bypassCache);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }
        /// <summary>
        /// Returns all or filtered list of courses
        /// </summary>
        /// <param name="page"></param>
        /// <param name="subject"></param>
        /// <param name="number"></param>
        /// <param name="academicLevels"></param>
        /// <param name="owningInstitutionUnits"></param>
        /// <param name="title"></param>
        /// <param name="instructionalMethods"></param>
        /// <param name="schedulingStartOn"></param>
        /// <param name="schedulingEndOn"></param>
        /// <returns>Filtered <see cref="Dtos.Course3">Courses.</see></returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 200), EedmResponseFilter]
        public async Task<IHttpActionResult> GetAllAndFilteredCourses3Async(Paging page, [FromUri] string subject = "", [FromUri] string number = "", [FromUri] string academicLevels = "", 
            [FromUri] string owningInstitutionUnits = "", [FromUri] string title = "", [FromUri] string instructionalMethods = "", [FromUri] string schedulingStartOn = "", [FromUri] string schedulingEndOn = "")
        {
            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                if (page == null)
                {
                    page = new Paging(200, 0);
                }

                AddDataPrivacyContextProperty((await _courseService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await _courseService.GetCourses3Async(page.Offset, page.Limit, bypassCache, subject, number, academicLevels, owningInstitutionUnits, title, instructionalMethods, schedulingStartOn, schedulingEndOn);
                return new PagedHttpActionResult<IEnumerable<Dtos.Course3>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }
        /// <summary>
        /// Returns all or filtered list of courses
        /// </summary>
        /// <param name="page"></param>
        /// <param name="criteria"> Can contain: </param>
        /// subject, number, academicLevels, owningInstitutionUnits, title, instructionalMethods
        /// schedulingStartOn, schedulingEndOn

        /// <returns>Filtered <see cref="Dtos.Course4">Courses.</see></returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 200), EedmResponseFilter]
        public async Task<IHttpActionResult> GetAllAndFilteredCourses4Async(Paging page, [FromUri] string criteria = "")
        {

            var bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                if (page == null)
                {
                    page = new Paging(200, 0);
                }


                string subject = string.Empty, number = string.Empty, academicLevels = string.Empty, owningInstitutionUnits = string.Empty, 
                       title = string.Empty, instructionalMethods = string.Empty, schedulingStartOn = string.Empty, schedulingEndOn = string.Empty;


                if (!string.IsNullOrEmpty(criteria))
                {
                    var criteriaValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(criteria);

                    if (criteriaValues != null)
                    {
                        foreach (var value in criteriaValues)
                        {
                            switch (value.Key.ToLower())
                            {
                                case "subject":
                                    subject = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "number":
                                    number = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "academiclevels":
                                    academicLevels = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "owninginstitutionunits":
                                    owningInstitutionUnits = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "title":
                                    title = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "instructionalmethods":
                                    instructionalMethods = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "schedulingstarton":
                                    schedulingStartOn = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                case "schedulingendon":
                                    schedulingEndOn = string.IsNullOrWhiteSpace(value.Value) ? string.Empty : value.Value;
                                    break;
                                default:
                                    throw new ArgumentException(string.Concat("Invalid filter value received: ", value.Key));
                            }
                        }
                    }
                }

                AddDataPrivacyContextProperty((await _courseService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await _courseService.GetCourses4Async(page.Offset, page.Limit, bypassCache, subject, number, academicLevels, owningInstitutionUnits, title, instructionalMethods, schedulingStartOn, schedulingEndOn);
                return new PagedHttpActionResult<IEnumerable<Dtos.Course4>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (JsonReaderException e)
            {
                _logger.Error(e.ToString());

                throw CreateHttpResponseException(new IntegrationApiException("Deserialization Error",
                   IntegrationApiUtility.GetDefaultApiError("Error parsing JSON course search request.")));
            }
            catch (JsonSerializationException e)
            {
                _logger.Error(e.ToString());

                throw CreateHttpResponseException(new IntegrationApiException("Deserialization Error",
                   IntegrationApiUtility.GetDefaultApiError("Error parsing JSON course search request.")));
            }
 
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }


        /// <summary>
        /// Post the course
        /// </summary>
        /// <param name="course">The course to be created</param>
        /// <returns>The created <see cref="Course">Course</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to create the course</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if the course is not provided.</exception>
        [Obsolete("Obsolete as of HeDM Version 4, use Accept Header Version 4 instead.")]
        public async Task<Ellucian.Colleague.Dtos.Course> PostAsync([FromBody] Ellucian.Colleague.Dtos.Course course)
        {
            try
            {
                return await _courseService.CreateCourseAsync(course);
            }
            catch (InvalidOperationException inex)
            {
                _logger.Error(inex.ToString());
                throw CreateHttpResponseException(inex.Message, HttpStatusCode.BadRequest);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (ConfigurationException e)
            {
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Post the course
        /// </summary>
        /// <param name="course">The course to be created</param>
        /// <returns>The created <see cref="Dtos.Course">Course</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to create the course</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if the course is not provided.</exception>
        public async Task<Ellucian.Colleague.Dtos.Course2> Post2Async([FromBody] Ellucian.Colleague.Dtos.Course2 course)
        {
            if (course == null)
            {
                throw CreateHttpResponseException("Request body must contain a valid Course.", HttpStatusCode.BadRequest);
            }

            if (string.IsNullOrEmpty(course.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null course id",
                    IntegrationApiUtility.GetDefaultApiError("Id is a required property.")));
            }

            try
            {
                return await _courseService.CreateCourse2Async(course);
            }
            catch(InvalidOperationException inex)
            {
                _logger.Error(inex.ToString());
                throw CreateHttpResponseException(inex.Message, HttpStatusCode.BadRequest);
            }
            catch(PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch(ConfigurationException e)
            {
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
            catch(Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Post the course
        /// </summary>
        /// <param name="course">The course to be created</param>
        /// <returns>The created <see cref="Dtos.Course3">Course</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to create the course</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if the course is not provided.</exception>
        public async Task<Ellucian.Colleague.Dtos.Course3> PostCourse3Async([FromBody] Ellucian.Colleague.Dtos.Course3 course)
        {
            if (course == null)
            {
                throw CreateHttpResponseException("Request body must contain a valid Course.", HttpStatusCode.BadRequest);
            }

            if (string.IsNullOrEmpty(course.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null course id",
                    IntegrationApiUtility.GetDefaultApiError("Id is a required property.")));
            }

            try
            {
                return await _courseService.CreateCourse3Async(course);
            }
            catch (InvalidOperationException inex)
            {
                _logger.Error(inex.ToString());
                throw CreateHttpResponseException(inex.Message, HttpStatusCode.BadRequest);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (ConfigurationException e)
            {
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Post the course
        /// </summary>
        /// <param name="course">The course to be created</param>
        /// <returns>The created <see cref="Dtos.Course4">Course</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to create the course</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if the course is not provided.</exception>
        public async Task<Ellucian.Colleague.Dtos.Course4> PostCourse4Async([FromBody] Ellucian.Colleague.Dtos.Course4 course)
        {
            if (course == null)
            {
                throw CreateHttpResponseException("Request body must contain a valid Course.", HttpStatusCode.BadRequest);
            }

            if (string.IsNullOrEmpty(course.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null course id",
                    IntegrationApiUtility.GetDefaultApiError("Id is a required property.")));
            }

            try
            {
                return await _courseService.CreateCourse4Async(course);
            }
            catch (InvalidOperationException inex)
            {
                _logger.Error(inex.ToString());
                throw CreateHttpResponseException(inex.Message, HttpStatusCode.BadRequest);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
            catch (ConfigurationException e)
            {
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Update the course
        /// </summary>
        /// <param name="id">Id for the course to be updated</param>
        /// <param name="course">The course to be updated</param>
        /// <returns>The updated <see cref="Course">Course</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to update the course</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if the course is not provided.</exception>
        [Obsolete("Obsolete as of HeDM Version 4, use Accept Header Version 4 instead.")]
        public async Task<Ellucian.Colleague.Dtos.Course> PutAsync([FromUri] string id, [FromBody] Dtos.Course course)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException("GUID in URL was not supplied.", HttpStatusCode.BadRequest);
            }
            if (course == null)
            {
                throw CreateHttpResponseException("Request body must contain a valid Course.", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrEmpty(course.Guid))
            {
                course.Guid = id.ToLowerInvariant();
            }
            else if (id.ToLowerInvariant() != course.Guid.ToLowerInvariant())
            {
                throw CreateHttpResponseException("GUID in URL is not the same as in request body.", HttpStatusCode.BadRequest);
            }

            try
            {
                return await _courseService.UpdateCourseAsync(course);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.Forbidden);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
            catch (ApplicationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.NotFound);
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.Forbidden);
            }
        }

        /// <summary>
        /// Update the course
        /// </summary>
        /// <param name="id">Id for the course to be updated</param>
        /// <param name="course">The course to be updated</param>
        /// <returns>The updated <see cref="Dtos.Course2">Course</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to update the course</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if the course is not provided.</exception>
        public async Task<Ellucian.Colleague.Dtos.Course2> Put2Async([FromUri] string id, [FromBody] Dtos.Course2 course)
        {
            if(string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException("id in URL was not supplied.", HttpStatusCode.BadRequest);
            }
            if(course == null)
            {
                throw CreateHttpResponseException("Request body must contain a valid Course.", HttpStatusCode.BadRequest);
            }
            if (id.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                throw CreateHttpResponseException("Nil GUID cannot be used in PUT operation.", HttpStatusCode.BadRequest);
            }
            if(string.IsNullOrEmpty(course.Id))
            {
                course.Id = id.ToLowerInvariant();
            }
            else if(id.ToLowerInvariant() != course.Id.ToLowerInvariant())
            {
                throw CreateHttpResponseException("id in URL is not the same as in request body.", HttpStatusCode.BadRequest);
            }

            try
            {
                return await _courseService.UpdateCourse2Async(course);
            }
            catch(PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.Forbidden);
            }
            catch(ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
            catch(ApplicationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.NotFound);
            }
            catch(ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.Forbidden);
            }
            catch(Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.Forbidden);
            }
        }

        /// <summary>
        /// Update the course
        /// </summary>
        /// <param name="id">Id for the course to be updated</param>
        /// <param name="course">The course to be updated</param>
        /// <returns>The updated <see cref="Dtos.Course3">Course</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to update the course</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if the course is not provided.</exception>
        public async Task<Ellucian.Colleague.Dtos.Course3> PutCourse3Async([FromUri] string id, [FromBody] Dtos.Course3 course)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException("id in URL was not supplied.", HttpStatusCode.BadRequest);
            }
            if (course == null)
            {
                throw CreateHttpResponseException("Request body must contain a valid Course.", HttpStatusCode.BadRequest);
            }
            if (id.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                throw CreateHttpResponseException("Nil GUID cannot be used in PUT operation.", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrEmpty(course.Id))
            {
                course.Id = id.ToLowerInvariant();
            }
            else if (id.ToLowerInvariant() != course.Id.ToLowerInvariant())
            {
                throw CreateHttpResponseException("id in URL is not the same as in request body.", HttpStatusCode.BadRequest);
            }

            try
            {
                await _courseService.DoesUpdateViolateDataPrivacySettings(GetRouteResourceName(), course);
                return await _courseService.UpdateCourse3Async(course);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.Forbidden);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
            catch (ApplicationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.NotFound);
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.Forbidden);
            }
        }

        /// <summary>
        /// Update the course
        /// </summary>
        /// <param name="id">Id for the course to be updated</param>
        /// <param name="course">The course to be updated</param>
        /// <returns>The updated <see cref="Dtos.Course3">Course</see></returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to update the course</exception>
        /// <exception> <see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.BadRequest returned if the course is not provided.</exception>
        public async Task<Ellucian.Colleague.Dtos.Course4> PutCourse4Async([FromUri] string id, [FromBody] Dtos.Course4 course)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException("id in URL was not supplied.", HttpStatusCode.BadRequest);
            }
            if (course == null)
            {
                throw CreateHttpResponseException("Request body must contain a valid Course.", HttpStatusCode.BadRequest);
            }
            if (id.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                throw CreateHttpResponseException("Nil GUID cannot be used in PUT operation.", HttpStatusCode.BadRequest);
            }
            if (string.IsNullOrEmpty(course.Id))
            {
                course.Id = id.ToLowerInvariant();
            }
            else if (id.ToLowerInvariant() != course.Id.ToLowerInvariant())
            {
                throw CreateHttpResponseException("id in URL is not the same as in request body.", HttpStatusCode.BadRequest);
            }

            try
            {
                await _courseService.DoesUpdateViolateDataPrivacySettings(GetRouteResourceName(), course);
                return await _courseService.UpdateCourse4Async(course);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.Forbidden);
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
            catch (ApplicationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.NotFound);
            }
            catch (ConfigurationException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.Forbidden);
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(e.Message, HttpStatusCode.Forbidden);
            }
        }

        /// <summary>
        /// Accepts a list of course Id strings to post a query against courses.
        /// </summary>
        /// <param name="criteria"><see cref="CourseQueryCriteria">Query Criteria</see> including the list of Course Ids to use to retrieve courses.</param>
        /// <returns>List of <see cref="Course2">Course2</see> objects. </returns>
        [HttpPost]
        public async Task<IEnumerable<Course2>> QueryCoursesByPost2Async([FromBody]CourseQueryCriteria criteria)
        {
            try
            {
                return await _courseService.GetCourses2Async(criteria);
            }
            catch (ArgumentNullException ex)
            {
                _logger.Error(ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.BadRequest);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.Message);
                throw CreateHttpResponseException(ex.Message, HttpStatusCode.InternalServerError);
            }
        }

        /// <summary>
        /// Delete (DELETE) a course
        /// </summary>
        /// <param name="id">Id to desired course</param>
        /// <returns>A course object <see cref="Dtos.Course2"/> in HeDM format</returns>
        [HttpDelete]
        public async Task<Ellucian.Colleague.Dtos.Course2> DeleteHedmCourseByIdAsync(string id)
        {
            //Delete is not supported for Colleague but Hedm requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Delete (DELETE) a course
        /// </summary>
        /// <param name="id">Id to desired course</param>
        /// <returns>A course object <see cref="Dtos.Course3"/> in EEDM format</returns>
        [HttpDelete]
        public async Task<Ellucian.Colleague.Dtos.Course3> DeleteHedmCourse3ByIdAsync(string id)
        {
            //Delete is not supported for Colleague but Hedm requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }
    }
}