﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Web.Security;
using System.Net.Http;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http;

namespace Ellucian.Colleague.Api.Controllers.Student
{
    /// <summary>
    /// The controller for student charges for the Ellucian Data Model.
    /// </summary>
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    [Authorize]
    public class StudentChargesController : BaseCompressedApiController
    {
        private readonly IStudentChargeService studentChargeService;
        private readonly ILogger logger;

        /// <summary>
        /// This constructor initializes the StudentChargeController object
        /// </summary>
        /// <param name="studentChargeService">student charges service object</param>
        /// <param name="logger">Logger object</param>
        public StudentChargesController(IStudentChargeService studentChargeService, ILogger logger)
        {
            this.studentChargeService = studentChargeService;
            this.logger = logger;
        }

        /// <summary>
        /// Retrieves a specified student charge for the data model version 6
        /// </summary>
        /// <param name="id">The requested student charge GUID</param>
        /// <returns>A StudentCharge DTO</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.StudentCharge> GetByIdAsync([FromUri] string id)
        {
            try
            {
                bool bypassCache = false;
                if (Request.Headers.CacheControl != null)
                {
                    if (Request.Headers.CacheControl.NoCache)
                    {
                        bypassCache = true;
                    }
                }

                if (string.IsNullOrEmpty(id))
                {
                    throw new ArgumentNullException("id", "id is required.");
                }
                AddDataPrivacyContextProperty((await studentChargeService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var studentCharge = await studentChargeService.GetByIdAsync(id);
                return studentCharge;
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error getting student charge");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Retrieves all student charges for the data model version 6
        /// </summary>
        /// <returns>A Collection of StudentCharges</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetAsync(Paging page, [FromUri] string student = "", string academicPeriod = "", string accountingCode = "", string chargeType = "")
        {
            try
            {
                bool bypassCache = false;
                if (Request.Headers.CacheControl != null)
                {
                    if (Request.Headers.CacheControl.NoCache)
                    {
                        bypassCache = true;
                    }
                }
                if (page == null)
                {
                    page = new Paging(200, 0);
                }

                AddDataPrivacyContextProperty((await studentChargeService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                var pageOfItems = await studentChargeService.GetAsync(page.Offset, page.Limit, bypassCache, student, academicPeriod, accountingCode, chargeType);
                return new PagedHttpActionResult<IEnumerable<Dtos.StudentCharge>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error getting student charge");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Update a single student charge for the data model version 6
        /// </summary>
        /// <param name="id">The requested student charge GUID</param>
        /// <param name="studentChargeDto">General Ledger DTO from Body of request</param>
        /// <returns>A single StudentCharge</returns>
        [HttpPut]
        public async Task<Dtos.StudentCharge> UpdateAsync([FromUri] string id, [FromBody] Dtos.StudentCharge studentChargeDto)
        {
            // The code is in the service and repository to perform this function but at this time, we
            // are not allowing an update or a delete.  Just throw unsupported error instead.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
            //try
            //{
            //    if (string.IsNullOrEmpty(id))
            //    {
            //        throw new ArgumentNullException("id", "id is a required for update");
            //    }
            //    if (studentChargeDto == null)
            //    {
            //        throw new ArgumentNullException("studentChargeDto", "The request body is required.");
            //    }
            //    if (string.IsNullOrEmpty(studentChargeDto.Id))
            //    {
            //        studentChargeDto.Id = id.ToUpperInvariant();
            //    }
            //    var studentChargeTransaction = await studentChargeService.UpdateAsync(id, studentChargeDto);
            //    return studentChargeTransaction;
            //}
            //catch (PermissionsException e)
            //{
            //    logger.Error(e.ToString());
            //    throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            //}
            //catch (ArgumentException e)
            //{
            //    logger.Error(e.ToString());
            //    throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            //}
            //catch (RepositoryException e)
            //{
            //    logger.Error(e.ToString());
            //    throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            //}
            //catch (Exception e)
            //{
            //    logger.Error(e, "Unknown error getting student charge");
            //    throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            //}
        }

        /// <summary>
        /// Create a single student charge for the data model version 6
        /// </summary>
        /// <param name="studentChargeDto">General Ledger DTO from Body of request</param>
        /// <returns>A single StudentCharge</returns>
        [HttpPost]
        public async Task<Dtos.StudentCharge> CreateAsync([FromBody] Dtos.StudentCharge studentChargeDto)
        {
            try
            {
                if (studentChargeDto == null)
                {
                    throw new ArgumentNullException("studentChargeDto", "The request body is required.");
                }
                var studentChargeTransaction = await studentChargeService.CreateAsync(studentChargeDto);
                return studentChargeTransaction;
            }
            catch (PermissionsException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            }
            catch (ArgumentException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                logger.Error(e, "Unknown error getting student charge");
                throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            }
        }

        /// <summary>
        /// Delete a single student charge for the data model version 6
        /// </summary>
        /// <param name="id">The requested student charge GUID</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task<HttpResponseMessage> DeleteAsync([FromUri] string id)
        {
            // The code is in the service and repository to perform this function but at this time, we
            // are not allowing an update or a delete.  Just throw unsupported error instead.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
            //try
            //{
            //    if (string.IsNullOrEmpty(id))
            //    {
            //        throw new ArgumentNullException("id", "guid is a required for delete");
            //    }
            //    await studentChargeService.DeleteAsync(id);
            //}
            //catch (PermissionsException e)
            //{
            //    logger.Error(e.ToString());
            //    throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.Unauthorized);
            //}
            //catch (ArgumentException e)
            //{
            //    logger.Error(e.ToString());
            //    throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            //}
            //catch (RepositoryException e)
            //{
            //    logger.Error(e.ToString());
            //    throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            //}
            //catch (Exception e)
            //{
            //    logger.Error(e, "Unknown error getting student charge");
            //    throw CreateHttpResponseException(e.Message, HttpStatusCode.BadRequest);
            //}
            //return new HttpResponseMessage(HttpStatusCode.OK);
        }
    }
}