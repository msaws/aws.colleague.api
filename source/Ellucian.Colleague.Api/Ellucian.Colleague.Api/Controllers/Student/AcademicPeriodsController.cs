﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using Ellucian.Web.Http.Controllers;
using System.ComponentModel;
using System.Net;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using slf4net;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Adapters;
using Ellucian.Colleague.Coordination.Student.Services;
using System.Threading.Tasks;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;

namespace Ellucian.Colleague.Api.Controllers
{
    /// <summary>
    /// Provides access to Academic Period data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class AcademicPeriodsController : BaseCompressedApiController
    {
        private readonly IAcademicPeriodService _academicPeriodService;
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the AcademicPeriodsController class.
        /// </summary>
        /// <param name="adapterRegistry">Adapter registry of type <see cref="IAdapterRegistry">IAdapterRegistry</see></param>
        /// <param name="academicPeriodService">Repository of type <see cref="IAcademicPeriodService">IAcademicPeriodService</see></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public AcademicPeriodsController(IAdapterRegistry adapterRegistry, IAcademicPeriodService academicPeriodService, ILogger logger)
        {
            _adapterRegistry = adapterRegistry;
            _academicPeriodService = academicPeriodService;
            _logger = logger;
        }

        ///WARNING: This route is obsolete as of HEDM Version 4. Replaced by GetAcademicPeriods2Async
        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Retrieves all Academic Periods.
        /// </summary>
        /// <returns>All <see cref="Dtos.AcademicPeriod">AcademicPeriod</see>objects.</returns>
        public async Task<IEnumerable<Dtos.AcademicPeriod>> GetAcademicPeriodsAsync()
        {
            try
            {
                return await _academicPeriodService.GetAcademicPeriodsAsync();
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }
        }

        //WARNING: This route is obsolete as of HEDM Version 4. Replaced by GetAcademicPeriodByGuid2Async
        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Retrieves an academic period by GUID.
        /// </summary>
        /// <returns>An <see cref="Dtos.AcademicPeriod">AcademicPeriod</see>object.</returns>
        public async Task<Dtos.AcademicPeriod> GetAcademicPeriodByGuidAsync(string guid)
        {
            try
            {
                return await _academicPeriodService.GetAcademicPeriodByGuidAsync(guid);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Retrieves all Academic Periods
        /// including census dates and registration status.
        /// </summary>
        /// <returns>All <see cref="Dtos.AcademicPeriod">AcademicPeriod</see>objects.</returns>
        public async Task<IEnumerable<Dtos.AcademicPeriod3>> GetAcademicPeriods3Async()
        {
            try
            {
                bool bypassCache = false;
                if (Request.Headers.CacheControl != null)
                {
                    if (Request.Headers.CacheControl.NoCache)
                    {
                        bypassCache = true;
                    }
                }
                return await _academicPeriodService.GetAcademicPeriods3Async(bypassCache);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Retrieves all Academic Periods.
        /// </summary>
        /// <returns>All <see cref="Dtos.AcademicPeriod">AcademicPeriod</see>objects.</returns>
        public async Task<IEnumerable<Dtos.AcademicPeriod2>> GetAcademicPeriods2Async()
        {
            try
            {
                bool bypassCache = false;
                if (Request.Headers.CacheControl != null)
                {
                    if (Request.Headers.CacheControl.NoCache)
                    {
                        bypassCache = true;
                    }
                }
                return await _academicPeriodService.GetAcademicPeriods2Async(bypassCache);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Retrieves an academic period by id
        /// including census dates and registration status.
        /// </summary>
        /// <returns>An <see cref="Dtos.AcademicPeriod">AcademicPeriod</see>object.</returns>
        public async Task<Dtos.AcademicPeriod3> GetAcademicPeriodByGuid3Async(string id)
        {
            try
            {
                return await _academicPeriodService.GetAcademicPeriodByGuid3Async(id);
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e), HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Retrieves an academic period by id.
        /// </summary>
        /// <returns>An <see cref="Dtos.AcademicPeriod">AcademicPeriod</see>object.</returns>
        public async Task<Dtos.AcademicPeriod2> GetAcademicPeriodByGuid2Async(string id)
        {
            try
            {
                return await _academicPeriodService.GetAcademicPeriodByGuid2Async(id);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }
        }

        /// <summary>
        /// Create an academic period
        /// </summary>
        /// <param name="academicPeriod">academicPeriod</param>
        /// <returns>A section object <see cref="Dtos.AcademicPeriod2"/> in HEDM format</returns>
        public async Task<Dtos.AcademicPeriod2> PostAcademicPeriodAsync([FromBody] Ellucian.Colleague.Dtos.AcademicPeriod2 academicPeriod)
        {
            //POST is not supported for Colleague but Hedm requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Update an academic period
        /// </summary>
        /// <param name="id">desired id for an academic period</param>
        /// <param name="academicPeriod">academicPeriod</param>
        /// <returns>A section object <see cref="Dtos.AcademicPeriod2"/> in HEDM format</returns>
        public async Task<Dtos.AcademicPeriod2> PutAcademicPeriodAsync([FromUri] string id, [FromBody] Dtos.AcademicPeriod2 academicPeriod)
        {
            //POST is not supported for Colleague but Hedm requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Delete (DELETE) an academic period
        /// </summary>
        /// <param name="id">id to desired academic period</param>
        /// <returns>A section object <see cref="Dtos.AcademicPeriod2"/> in HEDM format</returns>
        [HttpDelete]
        public async Task<Ellucian.Colleague.Dtos.AcademicPeriod2> DeleteAcademicPeriodByIdAsync(string id)
        {
            //Delete is not supported for Colleague but Hedm requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }
    }
}