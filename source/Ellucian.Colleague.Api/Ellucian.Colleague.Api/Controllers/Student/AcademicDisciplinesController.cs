﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using slf4net;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Student.Services;

namespace Ellucian.Colleague.Api.Controllers.Student
{
    /// <summary>
    /// Provides access to Academic Disciplines data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class AcademicDisciplinesController : BaseCompressedApiController
    {
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly IAcademicDisciplineService _academicDisciplineService;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the AcademicDisciplinesController class.
        /// </summary>
        /// <param name="adapterRegistry">Adapter registry of type <see cref="IAdapterRegistry">IAdapterRegistry</see></param>
        /// <param name="academicDisciplineService">Service of type <see cref="IAcademicDisciplineService">IAcademicDisciplineService</see></param>
        /// <param name="logger">Interface to Logger</param>
        public AcademicDisciplinesController(IAdapterRegistry adapterRegistry, IAcademicDisciplineService academicDisciplineService, ILogger logger)
        {
            _adapterRegistry = adapterRegistry;
            _academicDisciplineService = academicDisciplineService;
            _logger = logger;
        }

        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Retrieves all Academic Disciplines.
        /// </summary>
        /// <returns>All <see cref="Dtos.AcademicDiscipline">AcademicDiscipline </see>objects.</returns>
        [HttpGet]
        public async Task<IEnumerable<Dtos.AcademicDiscipline2>> GetAcademicDisciplines2Async()
        {

            bool bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                return await _academicDisciplineService.GetAcademicDisciplines2Async(bypassCache);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(ex));
            }
        }
        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Retrieves all Academic Disciplines.
        /// </summary>
        /// <returns>All <see cref="Dtos.AcademicDiscipline">AcademicDiscipline </see>objects.</returns>
        /// 
        [HttpGet]
        public async Task<IEnumerable<Dtos.AcademicDiscipline>> GetAcademicDisciplinesAsync()
        {

            bool bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                return await _academicDisciplineService.GetAcademicDisciplinesAsync(bypassCache);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(ex));
            }
        }


        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Retrieves an academic discipline by ID.
        /// </summary>
        /// <returns>An <see cref="Dtos.AcademicDiscipline2">AcademicDiscipline2 </see>object.</returns>
        /// 
        [HttpGet]
        public async Task<Dtos.AcademicDiscipline2> GetAcademicDiscipline2ByIdAsync(string id)
        {
            try
            {
                return await _academicDisciplineService.GetAcademicDiscipline2ByGuidAsync(id);
            }
            catch (KeyNotFoundException ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(ex), HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Retrieves an academic discipline by ID.
        /// </summary>
        /// <returns>An <see cref="Dtos.AcademicDiscipline">AcademicDiscipline </see>object.</returns>
        /// 
        [HttpGet]
        public async Task<Dtos.AcademicDiscipline> GetAcademicDisciplineByIdAsync(string id)
        {
            try
            {
                return await _academicDisciplineService.GetAcademicDisciplineByGuidAsync(id);
            }
            catch (KeyNotFoundException ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(ex), HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                _logger.Error(ex.ToString());
                throw CreateHttpResponseException(ex.Message);
            }
        }



        /// <summary>        
        /// Creates a AcademicDiscipline.
        /// </summary>
        /// <param name="academicDiscipline"><see cref="Dtos.AcademicDiscipline">AcademicDiscipline</see> to create</param>
        /// <returns>Newly created <see cref="Dtos.AcademicDiscipline">AcademicDiscipline</see></returns>
        [HttpPost]
        public Dtos.AcademicDiscipline PostAcademicDisciplines([FromBody] Dtos.AcademicDiscipline academicDiscipline)
        {
            //Create is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Updates a AcademicDiscipline.
        /// </summary>
        /// <param name="id">Id of the AcademicDiscipline to update</param>
        /// <param name="academicDiscipline"><see cref="Dtos.AcademicDiscipline">AcademicDiscipline</see> to create</param>
        /// <returns>Updated <see cref="Dtos.AcademicDiscipline">AcademicDiscipline</see></returns>
        [HttpPut]
        public Dtos.AcademicDiscipline PutAcademicDisciplines([FromUri] string id, [FromBody] Dtos.AcademicDiscipline academicDiscipline)
        {
            //Update is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }

        /// <summary>
        /// Delete (DELETE) an existing AcademicDiscipline
        /// </summary>
        /// <param name="id">Id of the AcademicDiscipline to delete</param>
        [HttpDelete]
        public void DeleteAcademicDisciplines([FromUri] string id)
        {
            //Delete is not supported for Colleague but HeDM requires full crud support.
            throw CreateHttpResponseException(new IntegrationApiException(IntegrationApiUtility.DefaultNotSupportedApiErrorMessage, IntegrationApiUtility.DefaultNotSupportedApiError));
        }
    }
}
