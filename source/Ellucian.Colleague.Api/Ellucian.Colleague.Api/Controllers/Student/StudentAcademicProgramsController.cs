﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using Ellucian.Web.Http.Controllers;
using System.Web.Http;
using System.ComponentModel;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Adapters;
using System.Threading.Tasks;
using System;
using System.Linq;
using Ellucian.Colleague.Coordination.Student.Services;
using slf4net;
using Ellucian.Web.Http.Exceptions;
using Ellucian.Colleague.Api.Utility;
using Ellucian.Web.Security;
using System.Configuration;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Dtos;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Http.Models;
using Ellucian.Web.Http;

namespace Ellucian.Colleague.Api.Controllers
{
    /// <summary>
    /// Provides access to StudentAcademicPrograms data.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Student)]
    public class StudentAcademicProgramsController : BaseCompressedApiController
    {
        private readonly IStudentAcademicProgramService _StudentAcademicProgramService;
        private readonly IAdapterRegistry _adapterRegistry;
        private readonly ILogger _logger;

        /// <summary>
        /// Initializes a new instance of the StudentAcademicProgramsController class.
        /// </summary>
        /// <param name="adapterRegistry">Adapter registry of type <see cref="IAdapterRegistry">IAdapterRegistry</see></param>
        /// <param name="StudentAcademicProgramService">Repository of type <see cref="IStudentAcademicProgramService">IStudentAcademicProgramService</see></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public StudentAcademicProgramsController(IAdapterRegistry adapterRegistry, IStudentAcademicProgramService StudentAcademicProgramService, ILogger logger)
        {
            _adapterRegistry = adapterRegistry;
            _StudentAcademicProgramService = StudentAcademicProgramService;
            _logger = logger;
        }

        /// <summary>
        /// Retrieves an Student Academic Program by ID.
        /// </summary>
        /// <returns>An <see cref="Dtos.StudentAcademicPrograms">StudentAcademicPrograms</see>object.</returns>
        [HttpGet, EedmResponseFilter]
        public async Task<Dtos.StudentAcademicPrograms> GetStudentAcademicProgramsByGuidAsync(string id)
        {
            bool bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }

            try
            {
                AddDataPrivacyContextProperty((await _StudentAcademicProgramService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());
                return await _StudentAcademicProgramService.GetStudentAcademicProgramByGuidAsync(id);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Return a list of StudentAcademicPrograms objects based on selection criteria.
        /// </summary>
        ///  <param name="page">page</param>
        /// <param name="student">Id (GUID) of the student enrolled on the academic program</param>
        /// <param name="startOn">Student Academic Programs that starts on or after this date</param>
        /// <param name="endOn">Student Academic Programs that ends on or before this date</param>
        /// <param name="program">academic program Name Contains ...program...</param>
        /// <param name="catalog">Student Academic Program catalog  equal to</param>
        /// <param name="enrollmentStatus">Student Academic Program status equals to </param>
        /// <param name="programOwner">The owner of the academic program. This property represents the global identifier for the Program Owner.</param>
        /// <param name="site">	The site (campus) the student enrolls for the program at</param>
        /// <param name="academicLevel">The academic level associated with the enrollment of the student in the academic program</param>
        /// <param name="graduatedOn">The date the student graduate from the program.</param>
        /// <param name="credentials">The academic credentials that can be awarded for completing an academic program</param>
        /// <param name="graduatedAcademicPeriod">Filter to provide the academic period the student graduated in.</param>
        /// <returns>List of StudentAcademicPrograms <see cref="Dtos.StudentAcademicPrograms"/> objects representing matching Student Academic Programs</returns>
        [HttpGet]
        [PagingFilter(IgnorePaging = true, DefaultLimit = 100), EedmResponseFilter]
        public async Task<IHttpActionResult> GetStudentAcademicProgramsAsync(Paging page, [FromUri] string student = "", [FromUri] string startOn = "", [FromUri] string endOn = "",
            [FromUri] string program = "", [FromUri] string catalog = "", [FromUri] string enrollmentStatus = "", [FromUri] string programOwner = "", [FromUri] string site = "", [FromUri] string academicLevel = "",
            [FromUri] string graduatedOn = "", [FromUri] string credentials = "", [FromUri] string graduatedAcademicPeriod = "")
        {
            //string criteria = student + startOn + endOn + program + catalog + enrollmentStatus ;
            bool bypassCache = false;
            if (Request.Headers.CacheControl != null)
            {
                if (Request.Headers.CacheControl.NoCache)
                {
                    bypassCache = true;
                }
            }
            try
            {
                if (page == null)
                {
                    page = new Paging(100, 0);
                }

                AddDataPrivacyContextProperty((await _StudentAcademicProgramService.GetDataPrivacyListByApi(GetRouteResourceName(), bypassCache)).ToList());

                var pageOfItems = await _StudentAcademicProgramService.GetStudentAcademicProgramsAsync(page.Offset, page.Limit, bypassCache, student, startOn, endOn, program,
                    catalog, enrollmentStatus, programOwner, site, academicLevel, graduatedOn, credentials,graduatedAcademicPeriod);
                return new PagedHttpActionResult<IEnumerable<Dtos.StudentAcademicPrograms>>(pageOfItems.Item1, page, pageOfItems.Item2, this.Request);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }


        /// <summary>
        /// Creates an Student Academic Program.
        /// </summary>
        /// <param name="StudentAcademicPrograms"><see cref="Dtos.StudentAcademicPrograms">StudentAcademicPrograms</see> to create</param>
        /// <returns>Newly created <see cref="Dtos.StudentAcademicPrograms">StudentAcademicPrograms</see></returns>
        [HttpPost]
        public async Task<Dtos.StudentAcademicPrograms> CreateStudentAcademicProgramsAsync([FromBody] Dtos.StudentAcademicPrograms StudentAcademicPrograms)
        {
            if (StudentAcademicPrograms == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null StudentAcademicPrograms argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }
            if (string.IsNullOrEmpty(StudentAcademicPrograms.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null GUID ",
                    IntegrationApiUtility.GetDefaultApiError("GUID is required in request body.")));
            }
            await ValidateStudentAcademicPrograms(StudentAcademicPrograms);
            try
            {
                return await _StudentAcademicProgramService.CreateStudentAcademicProgramAsync(StudentAcademicPrograms);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Updates an Student Academic Program.
        /// </summary>
        /// <param name="id">Id of the Student Academic Program to update</param>
        /// <param name="StudentAcademicPrograms"><see cref="Dtos.StudentAcademicPrograms">StudentAcademicPrograms</see> to create</param>
        /// <returns>Updated <see cref="Dtos.StudentAcademicPrograms">StudentAcademicPrograms</see></returns>
        [HttpPut]
        public async Task<Dtos.StudentAcademicPrograms> UpdateStudentAcademicProgramsAsync([FromUri] string id, [FromBody] Dtos.StudentAcademicPrograms StudentAcademicPrograms)
        {

            if (string.IsNullOrEmpty(id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null id argument",
                    IntegrationApiUtility.GetDefaultApiError("The GUID must be specified in the request URL.")));
            }
            if (StudentAcademicPrograms == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null StudentAcademicPrograms argument",
                    IntegrationApiUtility.GetDefaultApiError("The request body is required.")));
            }
            if (string.IsNullOrEmpty(StudentAcademicPrograms.Id))
            {
                StudentAcademicPrograms.Id = id.ToLowerInvariant();
            }
            if (!id.Equals(StudentAcademicPrograms.Id, StringComparison.InvariantCultureIgnoreCase))
            {
                throw CreateHttpResponseException(new IntegrationApiException("GUID mismatch",
                    IntegrationApiUtility.GetDefaultApiError("GUID not the same as in request body.")));
            }
            if (string.Equals(StudentAcademicPrograms.Id, Guid.Empty.ToString()))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Invalid GUID ",
              IntegrationApiUtility.GetDefaultApiError("The GUID is not valid")));
            }
            await ValidateStudentAcademicPrograms(StudentAcademicPrograms);
            try
            {
                await _StudentAcademicProgramService.DoesUpdateViolateDataPrivacySettings(GetRouteResourceName(), StudentAcademicPrograms);
                return await _StudentAcademicProgramService.UpdateStudentAcademicProgramAsync(StudentAcademicPrograms);
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentNullException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }

            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Delete (DELETE) an Student Academic Program. Doing so, the record in Colleague is not deleted
        /// but updated with an inactive status and end date of today's date unless the program starts in the future. 
        /// In such case, the end date will be set as future start date minus one day. 
        /// </summary>
        /// <param name="id">ID of the Student Academic Program to be deleted</param>
        /// <returns></returns>
        [HttpDelete]
        public async Task DeleteStudentAcademicProgramsAsync(string id)
        {

            try
            {
                await _StudentAcademicProgramService.DeleteStudentAcademicProgramAsync(id);
                // On delete, just return nothing.
            }
            catch (PermissionsException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (ArgumentException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (RepositoryException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (KeyNotFoundException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (IntegrationApiException e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
            catch (Exception e)
            {
                _logger.Error(e.ToString());
                throw CreateHttpResponseException(IntegrationApiUtility.ConvertToIntegrationApiException(e));
            }
        }

        /// <summary>
        /// Validates the data in the StudentAcademicPrograms object
        /// </summary>
        /// <param name="stuAcadProg">StudentAcademicPrograms from the request</param>
        private async Task ValidateStudentAcademicPrograms(StudentAcademicPrograms stuAcadProg)
        {


            if (stuAcadProg.Program == null || string.IsNullOrEmpty(stuAcadProg.Program.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null program argument",
                    IntegrationApiUtility.GetDefaultApiError("The program ID is required.")));
            }
            if (stuAcadProg.Student == null || string.IsNullOrEmpty(stuAcadProg.Student.Id))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null student argument",
                IntegrationApiUtility.GetDefaultApiError("The Student ID is required.")));
            }
            //if (stuAcadProg.StartDate == default(DateTime))
            if (stuAcadProg.StartDate == null)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null start date argument",
                    IntegrationApiUtility.GetDefaultApiError("The Start Date is required.")));
            }
            if (stuAcadProg.EnrollmentStatus != null && string.IsNullOrEmpty(stuAcadProg.EnrollmentStatus.EnrollStatus.ToString()))
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null enrollment status",
                    IntegrationApiUtility.GetDefaultApiError("The enrollment status is required.")));
            }
            //check end date is not before start date
            if (stuAcadProg.EndDate != null && stuAcadProg.EndDate < stuAcadProg.StartDate)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Invalid End Date",
                    IntegrationApiUtility.GetDefaultApiError("Student Academic Program end date cannot be before start date.")));
            }
            //check graduation date is not before start date
            if (stuAcadProg.GraduatedOn != null && stuAcadProg.GraduatedOn < stuAcadProg.StartDate)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Invalid Graduation Date",
                    IntegrationApiUtility.GetDefaultApiError("Student Academic Program graduation date cannot be before start date.")));
            }
            //check credentials is not before start date
            if (stuAcadProg.CredentialsDate != null && stuAcadProg.CredentialsDate < stuAcadProg.StartDate)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Invalid Credentials Date",
                    IntegrationApiUtility.GetDefaultApiError("Student Academic Program credential date cannot be before start date.")));
            }
            //if the enrollment status is inactive, then the end date is required.
            if (stuAcadProg.EndDate == null && stuAcadProg.EnrollmentStatus.EnrollStatus == Dtos.EnrollmentStatusType.Inactive)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Null End Date",
                    IntegrationApiUtility.GetDefaultApiError("End date is required for the enrollment status of inactive.")));
            }
            // the status of complete is not valid for PUT/POST
            if (stuAcadProg.EnrollmentStatus.EnrollStatus == Dtos.EnrollmentStatusType.Complete)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Incorrect EnrollmentStatus",
                    IntegrationApiUtility.GetDefaultApiError("The enrollment status of complete is not supported.")));
            }

            //the status of active cannot have end date
            if (stuAcadProg.EndDate != null && stuAcadProg.EnrollmentStatus.EnrollStatus == Dtos.EnrollmentStatusType.Active)
            {
                throw CreateHttpResponseException(new IntegrationApiException("Invalid End Date",
                    IntegrationApiUtility.GetDefaultApiError("End date is not valid for the enrollment status of active.")));
            }
            //check the credentials body is good.
            if (stuAcadProg.Credentials != null && stuAcadProg.Credentials.Count > 0)
            {
                foreach (var cred in stuAcadProg.Credentials)
                {
                    if (cred == null || string.IsNullOrEmpty(cred.Id))
                    {
                        throw CreateHttpResponseException(new IntegrationApiException("Invalid credentials",
                            IntegrationApiUtility.GetDefaultApiError("Credential id is a required field when credentials are in the message body.")));
                    }
                }
            }
            //check the recognitions body is good.
            if (stuAcadProg.Recognitions != null && stuAcadProg.Recognitions.Count > 0)
            {
                foreach (var honor in stuAcadProg.Recognitions)
                {
                    if (honor == null || string.IsNullOrEmpty(honor.Id))
                    {
                        throw CreateHttpResponseException(new IntegrationApiException("Invalid recognitions",
                            IntegrationApiUtility.GetDefaultApiError("Recognition id is a required field when recognitions are in the message body.")));
                    }
                }
            }

            //check displines body is good.
            if (stuAcadProg.Disciplines != null && stuAcadProg.Disciplines.Count > 0)
            {
                foreach (var dis in stuAcadProg.Disciplines)
                {
                    if (dis.Discipline == null)
                    {
                        throw CreateHttpResponseException(new IntegrationApiException("Invalid disciplines",
                           IntegrationApiUtility.GetDefaultApiError("Discipline is a required field when disciplines are in the message body.")));
                    }
                    else if (string.IsNullOrEmpty(dis.Discipline.Id))
                    {
                        throw CreateHttpResponseException(new IntegrationApiException("Invalid discipline",
                           IntegrationApiUtility.GetDefaultApiError("Discipline id is a required field when discipline is in the message body.")));
                    }
                    else if (string.IsNullOrEmpty(dis.AdministeringInstitutionUnit.Id))
                    {
                        throw CreateHttpResponseException(new IntegrationApiException("Invalid discipline",
                           IntegrationApiUtility.GetDefaultApiError("Administering Institution Unit id is a required field when Administering Institution Unit is in the message body.")));
                    }
                    else if (dis.SubDisciplines != null)
                    {
                        throw CreateHttpResponseException(new IntegrationApiException("Invalid Subdisciplines",
                           IntegrationApiUtility.GetDefaultApiError("Subdisciplines are not supported.")));
                    }
                }
            }


        }
    }


}
