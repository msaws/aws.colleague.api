﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using System.ComponentModel;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Coordination.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Http.Controllers;
using slf4net;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using System;

namespace Ellucian.Colleague.Api.Controllers.Finance
{
    /// <summary>
    /// Provides access to get student financial account activity.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Finance)]
    public class AccountActivityController : BaseCompressedApiController
    {
        private readonly IAccountActivityService _service;
        private readonly ILogger _logger;

        /// <summary>
        /// AccountActivityController class constructor
        /// </summary>
        /// <param name="service">Service of type <see cref="IAccountActivityService">IAccountActivityService</see></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public AccountActivityController(IAccountActivityService service, ILogger logger)
        {
            _service = service;
            _logger = logger;
        }

        /// <summary>
        /// Retrieves the account period data for a student.
        /// </summary>
        /// <param name="studentId">Student ID</param>
        /// <returns>The student's <see cref="AccountActivityPeriods">account activity periods</see> data</returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to access this information</exception>
        public AccountActivityPeriods GetAccountActivityPeriodsForStudent(string studentId)
        {
            try
            {
                return _service.GetAccountActivityPeriodsForStudent(studentId);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
        }

        /// <summary>
        /// Retrieves the account activity data for a student for a specified term.
        /// </summary>
        /// <param name="termId">Term ID</param>
        /// <param name="studentId">Student ID</param>
        /// <returns>The <see cref="DetailedAccountPeriod">detailed account period</see> for the specified student and term.</returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to access this information</exception>
        [Obsolete("Obsolete as of API version 1.8, use GetAccountActivityByTermForStudent2 instead")]      
        public DetailedAccountPeriod GetAccountActivityByTermForStudent(string termId, string studentId)
        {
            try
            {
                return _service.GetAccountActivityByTermForStudent(termId, studentId);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
        }

        /// <summary>
        /// Retrieves the account activity data for a student for a specified term.
        /// </summary>
        /// <param name="termId">Term ID</param>
        /// <param name="studentId">Student ID</param>
        /// <returns>The <see cref="DetailedAccountPeriod">detailed account period</see> for the specified student and term.</returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to access this information</exception>
        public DetailedAccountPeriod GetAccountActivityByTermForStudent2(string termId, string studentId)
        {
            try
            {
                return _service.GetAccountActivityByTermForStudent2(termId, studentId);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
        }

        /// <summary>
        /// Retrieves the account activity data for a student for a specified period.
        /// </summary>
        /// <param name="arguments">The <see cref="AccountActivityPeriodArguments">AccountActivityPeriodArguments</see> for the desired period</param>
        /// <param name="studentId">Student ID</param>
        /// <returns>The <see cref="DetailedAccountPeriod">Detailed Account Period</see>/> for the specified student and period.</returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to access this information</exception>
        [Obsolete("Obsolete as of API version 1.8, use PostAccountActivityByPeriodForStudent2 instead")]      
        public DetailedAccountPeriod PostAccountActivityByPeriodForStudent(AccountActivityPeriodArguments arguments, [FromUri]string studentId)
        {
            try
            {
                return _service.PostAccountActivityByPeriodForStudent(arguments.AssociatedPeriods, arguments.StartDate, arguments.EndDate, studentId);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
        }

        /// <summary>
        /// Retrieves the account activity data for a student for a specified period.
        /// </summary>
        /// <param name="arguments">The <see cref="AccountActivityPeriodArguments">AccountActivityPeriodArguments</see> for the desired period</param>
        /// <param name="studentId">Student ID</param>
        /// <returns>The <see cref="DetailedAccountPeriod">Detailed Account Period</see>/> for the specified student and period.</returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to access this information</exception>
        public DetailedAccountPeriod PostAccountActivityByPeriodForStudent2(AccountActivityPeriodArguments arguments, [FromUri]string studentId)
        {
            try
            {
                return _service.PostAccountActivityByPeriodForStudent2(arguments.AssociatedPeriods, arguments.StartDate, arguments.EndDate, studentId);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
        }
    }
}
