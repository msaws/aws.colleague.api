﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.Finance;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;

namespace Ellucian.Colleague.Api.Controllers.Finance
{
    /// <summary>
    /// Provides access to get and update Accounts Receivable information.
    /// </summary>
    [Authorize]
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.Finance)]
    public class DepositsController : BaseCompressedApiController
    {
        private readonly IAccountsReceivableService _service;
        private readonly ILogger _logger;

        /// <summary>
        /// AccountsReceivableController class constructor
        /// </summary>
        /// <param name="service">Service of type <see cref="IAccountsReceivableService">IAccountsReceivableService</see></param>
        /// <param name="logger">Logger of type <see cref="ILogger">ILogger</see></param>
        public DepositsController(IAccountsReceivableService service, ILogger logger)
        {
            _service = service;
            _logger = logger;
        }

        /// <summary>
        /// Get the deposits due for a specified student
        /// </summary>
        /// <param name="studentId">Student ID</param>
        /// <returns>A list of <see cref="DepositDue">deposits due</see> for the student</returns>
        /// <exception><see cref="HttpResponseException">HttpResponseException</see> with <see cref="HttpResponseMessage">HttpResponseMessage</see> containing <see cref="HttpStatusCode">HttpStatusCode</see>.Forbidden returned if user does not have the required role and permissions to access this information</exception>
        public IEnumerable<DepositDue> GetDepositsDue(string studentId)
        {
            try
            {
                return _service.GetDepositsDue(studentId);
            }
            catch (PermissionsException peex)
            {
                _logger.Info(peex.ToString());
                throw CreateHttpResponseException(peex.Message, HttpStatusCode.Forbidden);
            }
        }

        /// <summary>
        /// Retrieves all Deposit Types
        /// </summary>
        /// <returns>All <see cref="DepositType">deposit types</see> codes and descriptions.</returns>
        public IEnumerable<DepositType> GetDepositTypes()
        {
            return _service.GetDepositTypes();
        }
    }
}
