﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.ProjectsAccounting.Services;
using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;

namespace Ellucian.Colleague.Api.Controllers.ProjectsAccounting
{
    /// <summary>
    /// Provides access to Item Code information
    /// </summary>
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.ProjectsAccounting)]
    [Authorize]
    public class ProjectItemCodesController : BaseCompressedApiController
    {
        private readonly IProjectItemCodeService projectItemCodeService;

        /// <summary>
        /// Constructor to initialize ProjectItemCodesController object.
        /// </summary>
        public ProjectItemCodesController(IProjectItemCodeService projectItemCodeService)
        {
            this.projectItemCodeService = projectItemCodeService;
        }

        /// <summary>
        /// Get all of the item codes.
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<ProjectItemCode>> GetProjectItemCodesAsync()
        {
            var itemCodes = await projectItemCodeService.GetProjectItemCodesAsync();
            return itemCodes;
        }
    }
}
