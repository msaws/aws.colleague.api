﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Ellucian.Colleague.Api.Licensing;
using Ellucian.Colleague.Configuration.Licensing;
using Ellucian.Colleague.Coordination.ProjectsAccounting.Services;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Http.Controllers;
using Ellucian.Web.License;
using Ellucian.Web.Security;
using slf4net;

namespace Ellucian.Colleague.Api.Controllers.ProjectsAccounting
{
    /// <summary>
    /// This is the controller for projects accounting.
    /// </summary>
    [LicenseProvider(typeof(EllucianLicenseProvider))]
    [EllucianLicenseModule(ModuleConstants.ProjectsAccounting)]
    [Authorize]
    public class ProjectsController : BaseCompressedApiController
    {
        private readonly IProjectService projectService;
        private readonly ILogger logger;

        /// <summary>
        /// This constructor initializes the ProjectsController object.
        /// </summary>
        /// <param name="projectService">Project service object</param>
        /// <param name="logger">Logger object</param>
        public ProjectsController(IProjectService projectService, ILogger logger)
        {
            this.projectService = projectService;
            this.logger = logger;
        }

        /// <summary>
        /// Retrieves all projects assigned to the user.
        /// </summary>
        /// <param name="filter">Filter criteria used to reduce the number of projects returned.</param>
        /// <param name="summaryOnly">Specify whether or not detailed information will be returned with the project.</param>
        /// <returns>List of project DTOs.</returns>
        public async Task<IEnumerable<Project>> GetAsync([FromUri] IEnumerable<UrlFilter> filter, bool summaryOnly = true)
        {
            try
            {
                var projects = await projectService.GetProjectsAsync(summaryOnly, filter);
                return projects;
            }
            catch (ArgumentNullException anex)
            {
                logger.Error(anex, anex.Message);
                throw CreateHttpResponseException("Invalid argument.", HttpStatusCode.BadRequest);
            }
            catch (ConfigurationException ce)
            {
                logger.Error(ce, ce.Message);
                throw CreateHttpResponseException("Invalid configuration.", HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw CreateHttpResponseException();
            }
        }

        /// <summary>
        /// Retrieves the project the user selected
        /// </summary>
        /// <param name="projectId">ID of the project requested.</param>
        /// <param name="summaryOnly">Specify whether or not detailed information will be returned with the project.</param>
        /// <param name="sequenceNumber">Specify a sequence number to return project totals for a single budget period.</param>
        /// <returns>Project DTO</returns>
        public async Task<Project> GetProjectAsync(string projectId, bool summaryOnly = false, string sequenceNumber = null)
        {
            try
            {
                var project = await projectService.GetProjectAsync(projectId, summaryOnly, sequenceNumber);
                return project;
            }
            catch (PermissionsException peex)
            {
                logger.Info(peex.ToString());
                throw CreateHttpResponseException("Access denied.", HttpStatusCode.Forbidden);
            }
            catch (ArgumentNullException anex)
            {
                logger.Error(anex, anex.Message);
                throw CreateHttpResponseException("Invalid argument.", HttpStatusCode.BadRequest);
            }
            catch (ConfigurationException ce)
            {
                logger.Error(ce, ce.Message);
                throw CreateHttpResponseException("Invalid configuration.", HttpStatusCode.NotFound);
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
                throw CreateHttpResponseException();
            }
        }
    }
}
