﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.Student.DataContracts;
using Ellucian.Colleague.Data.Student.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Ellucian.Colleague.Data.Student.Tests.Repositories
{
    [TestClass]
    public class StudentTaxFormStatementRepositoryTests : BaseRepositorySetup
    {
        private StudentTaxFormStatementRepository actualRepository;
        private Collection<TaxForm1098Forms> form1098contracts = new Collection<TaxForm1098Forms>();
        private Parm1098 parm1098Contract;

        #region Initialize and Cleanup
        [TestInitialize]
        public void Initialize()
        {
            MockInitialize();
            actualRepository =  new StudentTaxFormStatementRepository(apiSettings, cacheProviderMock.Object,
                transFactoryMock.Object, loggerMock.Object);

            Build1098contracts();
            dataReaderMock.Setup(x => x.BulkReadRecordAsync<TaxForm1098Forms>(It.IsAny<string>(), true)).Returns(() =>
                {
                    return Task.FromResult(form1098contracts);
                });

            parm1098Contract = new Parm1098() { P1098TTaxForm = "1098T" };
            dataReaderMock.Setup(x => x.ReadRecordAsync<Parm1098>(It.IsAny<string>(), It.IsAny<string>(), true)).Returns(() =>
                {
                    return Task.FromResult(parm1098Contract);
                });
        }

        [TestCleanup]
        public void Cleanup()
        {
            actualRepository = null;
        }
        #endregion

        #region GetAsync for 1098s
        [TestMethod]
        public async Task GetAsync_1098_NullPersonId()
        {
            var expectedParam = "personid";
            var actualParam = "";
            try
            {
                await actualRepository.GetAsync(null, Domain.Base.Entities.TaxForms.Form1098);
            }
            catch (ArgumentNullException anex)
            {
                actualParam = anex.ParamName.ToLower();
            }

            Assert.AreEqual(expectedParam, actualParam);
        }

        [TestMethod]
        public async Task GetAsync_1098_EmptyPersonId()
        {
            var expectedParam = "personid";
            var actualParam = "";
            try
            {
                await actualRepository.GetAsync("", Domain.Base.Entities.TaxForms.Form1098);
            }
            catch (ArgumentNullException anex)
            {
                actualParam = anex.ParamName.ToLower();
            }

            Assert.AreEqual(expectedParam, actualParam);
        }

        [TestMethod]
        public async Task GetAsync_1098_InvalidTaxFormId()
        {
            var expectedParam = "taxform";
            var actualParam = "";
            try
            {
                await actualRepository.GetAsync("1", Domain.Base.Entities.TaxForms.Form1095C);
            }
            catch (ArgumentException anex)
            {
                actualParam = anex.ParamName.ToLower();
            }

            Assert.AreEqual(expectedParam, actualParam);
        }

        [TestMethod]
        public async Task GetAsync_1098_NullParm1098Record()
        {
            var expectedMessage = "PARM.1098 cannot be null.";
            var actualMessage = "";
            try
            {
                parm1098Contract = null;
                await actualRepository.GetAsync("1", Domain.Base.Entities.TaxForms.Form1098);
            }
            catch (NullReferenceException nrex)
            {
                actualMessage = nrex.Message;
            }

            Assert.AreEqual(expectedMessage, actualMessage);
        }

        [TestMethod]
        public async Task GetAsync_1098_NullTaxForm1098FormsRecord()
        {
            form1098contracts[0] = null;
            var actual1098Entities = await actualRepository.GetAsync("1", Domain.Base.Entities.TaxForms.Form1098);

            // The null record should be excluded from the returned list.
            Assert.AreEqual(form1098contracts.Count, actual1098Entities.Count() + 1);
        }

        [TestMethod]
        public async Task GetAsync_1098_NullTaxYear()
        {
            form1098contracts[0].Tf98fTaxYear = null;
            var actualStatements = await actualRepository.GetAsync("1", Domain.Base.Entities.TaxForms.Form1098);

            Assert.AreEqual(form1098contracts.Count - 1, actualStatements.Count());
        }

        [TestMethod]
        public async Task GetAsync_1098_NullRecordKey()
        {
            form1098contracts[0].Recordkey = null;
            var actualStatements = await actualRepository.GetAsync("1", Domain.Base.Entities.TaxForms.Form1098);

            Assert.AreEqual(form1098contracts.Count - 1, actualStatements.Count());
        }

        [TestMethod]
        public async Task GetAsync_1098_EmptyRecordKey()
        {
            form1098contracts[0].Recordkey = "";
            var actualStatements = await actualRepository.GetAsync("1", Domain.Base.Entities.TaxForms.Form1098);

            Assert.AreEqual(form1098contracts.Count - 1, actualStatements.Count());
        }

        [TestMethod]
        public async Task GetAsync_1098_NullStudentId()
        {
            form1098contracts[0].Tf98fStudent = null;
            var actualStatements = await actualRepository.GetAsync("1", Domain.Base.Entities.TaxForms.Form1098);

            Assert.AreEqual(form1098contracts.Count - 1, actualStatements.Count());
        }

        [TestMethod]
        public async Task GetAsync_1098_EmptyStudentId()
        {
            form1098contracts[0].Tf98fStudent = "";
            var actualStatements = await actualRepository.GetAsync("1", Domain.Base.Entities.TaxForms.Form1098);

            Assert.AreEqual(form1098contracts.Count - 1, actualStatements.Count());
        }

        [TestMethod]
        public async Task GetAsync_1098_OneDataContractIsNull()
        {
            form1098contracts[0] = null;
            var actualStatements = await actualRepository.GetAsync("1", Domain.Base.Entities.TaxForms.Form1098);

            Assert.AreEqual(form1098contracts.Count - 1, actualStatements.Count());
        }

        [TestMethod]
        public async Task GetAsync_1098_Success()
        {
            var actualStatements = await actualRepository.GetAsync("1", Domain.Base.Entities.TaxForms.Form1098);

            foreach (var dataContract in form1098contracts.Where(x => x.Tf98fTaxForm == "1098T").ToList())
            {
                var selectedEntities = actualStatements.Where(x =>
                    x.PdfRecordId == dataContract.Recordkey
                    && x.PersonId == dataContract.Tf98fStudent
                    && x.TaxForm.ToString() + "T" == "Form" + dataContract.Tf98fTaxForm
                    && x.TaxYear == dataContract.Tf98fTaxYear.ToString()).ToList();
                Assert.AreEqual(1, selectedEntities.Count);
            }
        }
        #endregion

        #region Private methods
        private void Build1098contracts()
        {
            form1098contracts.Add(new TaxForm1098Forms()
            {
                Recordkey = "1",
                Tf98fStudent = "0003946",
                Tf98fTaxYear = 2016,
                Tf98fTaxForm = "1098T"
            });

            form1098contracts.Add(new TaxForm1098Forms()
            {
                Recordkey = "2",
                Tf98fStudent = "0003946",
                Tf98fTaxYear = 2015,
                Tf98fTaxForm = "1098T"
            });
        }
        #endregion
    }
}