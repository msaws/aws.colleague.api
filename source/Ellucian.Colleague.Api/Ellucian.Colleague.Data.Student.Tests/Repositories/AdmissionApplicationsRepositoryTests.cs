﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.Student.DataContracts;
using Ellucian.Colleague.Data.Student.Repositories;
using Ellucian.Colleague.Domain.Student;
using Ellucian.Colleague.Domain.Student.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Data.Colleague;
using Ellucian.Web.Cache;
using slf4net;
using Ellucian.Web.Http.Configuration;
using System.Threading;

namespace Ellucian.Colleague.Data.Student.Tests.Repositories
{
    [TestClass]
    public class AdmissionApplicationsRepositoryTests : BaseRepositorySetup
    {
        [TestClass]
        public class GetAdmissionApplicationsTests
        {
            Mock<IColleagueTransactionFactory> transFactoryMock;
            Mock<ICacheProvider> cacheProviderMock;
            Mock<IColleagueDataReader> dataReaderMock;
            Mock<ILogger> loggerMock;
            ApiSettings apiSettingsMock;

            //Data contract objects returned as responses by the mocked datareaders
            private Applications applicationResponseData;
            private Collection<Applications> applicationResponseCollection;

            //TestRepositories
            private TestAdmissionApplicationsRepository expectedRepository;
            private AdmissionApplicationsRepository actualRepository;

            //Test data
            private AdmissionApplication expectedApplication;
            private AdmissionApplication actualApplication;
            private List<AdmissionApplication> expectedApplications;

            //used throughout
            private string applicationId;
            private string[] applicationIds;
            private string applicationGuid;
            int offset = 0;
            int limit = 200;


            [TestInitialize]
            public async void Initialize()
            {
                expectedRepository = new TestAdmissionApplicationsRepository();

                //setup the expected applications
                var pageOfApplications = await expectedRepository.GetAdmissionApplicationsAsync(offset, limit, false);
                expectedApplications = pageOfApplications.Item1.ToList();

                applicationId = expectedApplications.First().ApplicantRecordKey;
                applicationGuid = expectedApplications.First().Guid;
                expectedApplication = await expectedRepository.GetAdmissionApplicationByIdAsync(applicationGuid);

                //set the response data objects
                applicationIds = expectedApplications.Select(ap => ap.ApplicantRecordKey).ToArray();
                applicationResponseCollection = BuildResponseData(expectedApplications);
                applicationResponseData = applicationResponseCollection.FirstOrDefault(ap => ap.RecordGuid == applicationGuid);

                //build the repository
                actualRepository = BuildRepository();
            }

            [TestCleanup]
            public void Cleanup()
            {
                cacheProviderMock = null;
                dataReaderMock = null;
                loggerMock = null;
                transFactoryMock = null;

                applicationResponseData = null;
                expectedRepository = null;
                actualRepository = null;
                expectedApplications = null;
                expectedApplication = null;
                actualApplication = null;
                applicationId = null;
                applicationIds = null;
            }

            private AdmissionApplicationsRepository BuildRepository()
            {
                // transaction factory mock
                transFactoryMock = new Mock<IColleagueTransactionFactory>();
                // Cache Provider Mock
                cacheProviderMock = new Mock<ICacheProvider>();
                // Logger Mock
                loggerMock = new Mock<ILogger>();

                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
                    x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                    .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

                // Set up data accessor for mocking 
                dataReaderMock = new Mock<IColleagueDataReader>();
                apiSettingsMock = new ApiSettings("TEST");

                // Set up dataAccessorMock as the object for the DataAccessor
                transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(dataReaderMock.Object);

                // Single Application
                dataReaderMock.Setup(acc => acc.SelectAsync(It.IsAny<GuidLookup[]>())).Returns<GuidLookup[]>(gla =>
                {
                    var result = new Dictionary<string, GuidLookupResult>();
                    foreach (var gl in gla)
                    {
                        var appl = applicationResponseCollection.FirstOrDefault(x => x.RecordGuid == gl.Guid);
                        result.Add(gl.Guid, appl == null ? null : new GuidLookupResult() { Entity = "APPLICATIONS", PrimaryKey = appl.Recordkey });
                    }
                    return Task.FromResult(result);
                });
                dataReaderMock.Setup(dr => dr.ReadRecordAsync<Applications>("APPLICATIONS", applicationId, true)).ReturnsAsync(applicationResponseData);
                dataReaderMock.Setup<Task<Collection<StudentPrograms>>>(dr => dr.BulkReadRecordAsync<StudentPrograms>(It.IsAny<string[]>(), true)).ReturnsAsync(new Collection<StudentPrograms>());
                
                // Multiple Applications
                dataReaderMock.Setup(dr => dr.SelectAsync("APPLICATIONS", It.IsAny<string>())).ReturnsAsync(applicationIds);
                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<Applications>("APPLICATIONS", It.IsAny<string[]>(), true)).ReturnsAsync(applicationResponseCollection);
                return new AdmissionApplicationsRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);
            }

            private Collection<Applications> BuildResponseData(List<AdmissionApplication> applData)
            {
                var collection = new Collection<Applications>();
                foreach (var appl in applData)
                {
                    var applicationRecord = new Applications()
                    {
                        Recordkey = appl.ApplicantRecordKey,
                        RecordGuid = appl.Guid,
                        ApplAcadProgram = appl.ApplicationAcadProgram,
                        ApplAdmissionsRep = appl.ApplicationAdmissionsRep,
                        ApplAdmitStatus = appl.ApplicationAdmitStatus,
                        ApplApplicant = appl.ApplicantPersonId,
                        ApplAttendedInstead = appl.ApplicationAttendedInstead,
                        ApplComments = appl.ApplicationComments,
                        ApplLocations = appl.ApplicationLocations,
                        ApplNo = appl.ApplicationNo,
                        ApplStartTerm = appl.ApplicationStartTerm,
                        ApplStudentLoadIntent = appl.ApplicationStudentLoadIntent,
                        ApplSource = appl.ApplicationSource,
                        ApplWithdrawReason = appl.ApplicationWithdrawReason,
                        ApplResidencyStatus = appl.ApplicationResidencyStatus,

                        ApplStatus = new List<string>(),
                        ApplStatusDate = new List<DateTime?>(),
                        ApplStatusTime = new List<DateTime?>(),
                        ApplStatusesEntityAssociation = new List<ApplicationsApplStatuses>(),
                        ApplDecisionBy = new List<string>(),
                        ApplLocationDates = new List<DateTime?>(),
                        ApplLocationChangeReasons = new List<string>(),
                        ApplLocationInfoEntityAssociation = new List<ApplicationsApplLocationInfo>()
                    };
                    foreach (var status in appl.AdmissionApplicationStatuses)
                    {
                        applicationRecord.ApplStatus.Add(status.ApplicationStatus);
                        applicationRecord.ApplStatusDate.Add(status.ApplicationStatusDate);
                        applicationRecord.ApplStatusTime.Add(status.ApplicationStatusTime);
                        applicationRecord.ApplDecisionBy.Add(string.Empty);
                    }
                    applicationRecord.buildAssociations();

                    collection.Add(applicationRecord);
                }
                return collection;
            }

            [TestMethod]
            public async Task GetAdmissionApplicationByIdAsync()
            {
                actualApplication = await actualRepository.GetAdmissionApplicationByIdAsync(applicationGuid);
                Assert.AreEqual(expectedApplication.ApplicantPersonId, actualApplication.ApplicantPersonId);
                Assert.AreEqual(expectedApplication.ApplicantRecordKey, actualApplication.ApplicantRecordKey);
                Assert.AreEqual(expectedApplication.ApplicationAdmissionsRep, actualApplication.ApplicationAdmissionsRep);
                Assert.AreEqual(expectedApplication.ApplicationAdmitStatus, actualApplication.ApplicationAdmitStatus);
                Assert.AreEqual(expectedApplication.ApplicationAttendedInstead, actualApplication.ApplicationAttendedInstead);
                Assert.AreEqual(expectedApplication.ApplicationComments, actualApplication.ApplicationComments);
                Assert.AreEqual(expectedApplication.ApplicationLocations, actualApplication.ApplicationLocations);
                Assert.AreEqual(expectedApplication.ApplicationNo, actualApplication.ApplicationNo);
                Assert.AreEqual(expectedApplication.ApplicationOwnerId, actualApplication.ApplicationOwnerId);
                Assert.AreEqual(expectedApplication.ApplicationResidencyStatus, actualApplication.ApplicationResidencyStatus);
                Assert.AreEqual(expectedApplication.ApplicationSource, actualApplication.ApplicationSource);
                Assert.AreEqual(expectedApplication.ApplicationStartTerm, actualApplication.ApplicationStartTerm);
                Assert.AreEqual(expectedApplication.ApplicationStudentLoadIntent, actualApplication.ApplicationStudentLoadIntent);
                Assert.AreEqual(expectedApplication.ApplicationWithdrawReason, actualApplication.ApplicationWithdrawReason);
            }

            [TestMethod]
            public async Task GetAdmissionApplicationsAsync()
            {
                var actuals = await actualRepository.GetAdmissionApplicationsAsync(offset, limit, false);

                Assert.AreEqual(expectedApplications.Count(), actuals.Item1.Count());

                foreach (var actual in actuals.Item1)
                {
                    var expected = expectedApplications.FirstOrDefault(i => i.ApplicantRecordKey.Equals(actual.ApplicantRecordKey, StringComparison.OrdinalIgnoreCase));
                    Assert.IsNotNull(expected);
                    Assert.AreEqual(expected.ApplicantPersonId, actual.ApplicantPersonId);
                    Assert.AreEqual(expected.ApplicantRecordKey, actual.ApplicantRecordKey);
                    Assert.AreEqual(expected.ApplicationAdmissionsRep, actual.ApplicationAdmissionsRep);
                    Assert.AreEqual(expected.ApplicationAdmitStatus, actual.ApplicationAdmitStatus);
                    Assert.AreEqual(expected.ApplicationAttendedInstead, actual.ApplicationAttendedInstead);
                    Assert.AreEqual(expected.ApplicationComments, actual.ApplicationComments);
                    Assert.AreEqual(expected.ApplicationLocations, actual.ApplicationLocations);
                    Assert.AreEqual(expected.ApplicationNo, actual.ApplicationNo);
                    Assert.AreEqual(expected.ApplicationOwnerId, actual.ApplicationOwnerId);
                    Assert.AreEqual(expected.ApplicationResidencyStatus, actual.ApplicationResidencyStatus);
                    Assert.AreEqual(expected.ApplicationSource, actual.ApplicationSource);
                    Assert.AreEqual(expected.ApplicationStartTerm, actual.ApplicationStartTerm);
                    Assert.AreEqual(expected.ApplicationStudentLoadIntent, actual.ApplicationStudentLoadIntent);
                    Assert.AreEqual(expected.ApplicationWithdrawReason, actual.ApplicationWithdrawReason);
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task ApplicationIdRequiredTest()
            {
                await actualRepository.GetAdmissionApplicationByIdAsync(null);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task NullApplicationRecord_ExceptionTest()
            {
                //set the response data object to null and the dataReaderMock
                applicationResponseData = null;
                dataReaderMock.Setup(dr => dr.ReadRecordAsync<Applications>("APPLICATIONS", applicationId, true)).ReturnsAsync(applicationResponseData);

                actualApplication = await actualRepository.GetAdmissionApplicationByIdAsync(applicationGuid);
            }

            [TestMethod]
            public async Task NullApplicationRecord_LogsErrorTest()
            {
                var exceptionCaught = false;
                try
                {
                    //set the response data object to null and the dataReaderMock
                    applicationResponseData = null;
                    dataReaderMock.Setup(dr => dr.ReadRecordAsync<Applications>("APPLICATIONS", applicationId, true)).ReturnsAsync(applicationResponseData);

                    actualApplication = await actualRepository.GetAdmissionApplicationByIdAsync(applicationGuid);
                }
                catch
                {
                    exceptionCaught = true;
                }

                Assert.IsTrue(exceptionCaught);
                // loggerMock.Verify(l => l.Error(It.IsAny<string>()));
            }
        }
    }
}
