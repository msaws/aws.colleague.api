﻿//// Copyright 2017 Ellucian Company L.P. and its affiliates.

//using Ellucian.Colleague.Data.Base.DataContracts;
//using Ellucian.Colleague.Data.Base.Tests.Repositories;
//using Ellucian.Colleague.Data.Student.DataContracts;
//using Ellucian.Colleague.Data.Student.Repositories;
//using Ellucian.Colleague.Data.Student.Transactions;
//using Ellucian.Colleague.Domain.Base.Tests;
//using Ellucian.Colleague.Domain.Student.Entities;
//using Ellucian.Colleague.Domain.Student.Repositories;
//using Ellucian.Colleague.Domain.Student.Tests;
//using Ellucian.Data.Colleague;
//using Ellucian.Data.Colleague.DataContracts;
//using Ellucian.Web.Cache;
//using Ellucian.Web.Http.Configuration;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Moq;
//using slf4net;
//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Linq;
//using System.Threading;
//using System.Threading.Tasks;


//namespace Ellucian.Colleague.Data.Student.Tests.Repositories
//{
//    [TestClass]
//    public class StudentAcademicProgramRepositoryTests
//    {
//        #region HEDMGet

//        [TestClass]
//        public class StudentAcademicProgramRepository_GetHeDM : BaseRepositorySetup
//        {
//            private ApiSettings apiSettingsMock;
//            private Mock<IStudentReferenceDataRepository> studentReferenceDataRepositoryMock;
//            IEnumerable<StudentAcademicProgram> allStuProgs;
//            IEnumerable<AcademicProgram> allAcadProgs;
//            Collection<AcadPrograms> acadProgResponseData;
//            Collection<Person> studentResponseData;
//            Collection<StudentProgramsStprMajorList> majors;
//            Collection<StudentProgramsStprMinorList> minors;
//            Collection<StudentProgramsStprCcdList> ccds;
//            Collection<StudentProgramsStprSpecialties> sps;
//            Collection<StudentPrograms> stuProgResponseData;

//            Collection<AcadCredentials> acadCredentialsResponseData;
//            protected Dictionary<string, AcadCredentials> acadCredentialsRecords;
//            protected List<string> acadCredentialIds;


//            Dictionary<string, StudentAcademicProgram> allStuProgDict;
//            private IStudentAcademicProgramRepository stuProgRepo;
//            private IStudentReferenceDataRepository stuRefData;
//            private string defaultInstitution = "0000043";

//            private string[,] _acadCredentialsData = {
//                                       {"100", "12345678", "0000604", "", "", "", "06/15/2014", "", "", "","","", "Honors"},
//                                       {"200", "0000304", "0000704", "BA", "2013-05-23", "2013-07-01", "2014-06-30", "ACL", "ENGL,HIST", "MATH", "EMT", "CCL,EML", "Honors"},
//                                       {"300", "0000304", "0000704", "BS", "2013-05-23", "", "", "", "", "", "", "", ""},
//                                       {"400", "0000404", "0000704", "BA", "", "", "", "", "", "", "", "", "National Science"}
//                                   };

//            [TestInitialize]
//            public async void Initialize()
//            {
//                MockInitialize();
//                studentReferenceDataRepositoryMock = new Mock<IStudentReferenceDataRepository>();
//                allStuProgs = await new TestStudentAcademicProgramRepository().GetStudentAcademicProgramsAsync(false);
//                allAcadProgs = await new TestAcademicProgramRepository().GetAsync();
//                // Build studentprograms dict, response from cache
//                allStuProgDict = new Dictionary<string, StudentAcademicProgram>();
//                acadCredentialsRecords = SetupAcadCredentials(out acadCredentialIds);

//                foreach (var prog in allStuProgs)
//                {
//                    allStuProgDict[prog.Guid] = prog;
//                }


//                // Repository response data
//                var startDates = new List<DateTime?>() { DateTime.Now.Subtract(new TimeSpan(72, 00, 00)), DateTime.Now.Subtract(new TimeSpan(72, 00, 00)), DateTime.Now.AddDays(2), new DateTime(2013, 12, 31) };
//                var endDates = new List<DateTime?>() { null, DateTime.Now.AddDays(3), null, DateTime.Now };
//                majors = BuildAdditionalMajorsResponse(startDates, endDates);
//                minors = BuildAdditionalMinorsResponse(startDates, endDates);
//                ccds = BuildAdditionalCcdsResponse(startDates, endDates);
//                sps = BuildAdditionalSpecialtiesResponse(startDates, endDates);
//                stuProgResponseData = BuildStudentProgramResponse(allStuProgs);
//                acadProgResponseData = BuildAcadProgResponse(allAcadProgs);
//                studentResponseData = BuildStudentResponse();
//                stuProgRepo = BuildValidStudentAcademicProgramRepository();
//                stuRefData = new StudentReferenceDataRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettingsMock);



//            }


//            [TestCleanup]
//            public void Cleanup()
//            {
//                transFactoryMock = null;
//                cacheProviderMock = null;
//                stuProgResponseData = null;
//                allStuProgs = null;
//                stuProgRepo = null;
//            }

//            private Dictionary<string, AcadCredentials> SetupAcadCredentials(out List<string> acadCredentialsIds)
//            {
//                string[,] recordData = _acadCredentialsData;

//                acadCredentialsIds = new List<string>();
//                int institutionsCount = recordData.Length / 13;
//                Dictionary<string, AcadCredentials> records = new Dictionary<string, AcadCredentials>();
//                for (int i = 0; i < institutionsCount; i++)
//                {
//                    string key = recordData[i, 0].TrimEnd();
//                    string personId = recordData[i, 1].TrimEnd();
//                    string instId = recordData[i, 2].TrimEnd();
//                    string degree = (recordData[i, 3] == null) ? String.Empty : recordData[i, 3].TrimEnd();
//                    DateTime? degreeDate = (string.IsNullOrEmpty(recordData[i, 4])) ? new DateTime?() : DateTime.Parse(recordData[i, 4].TrimEnd());
//                    DateTime? startDate = (string.IsNullOrEmpty(recordData[i, 5])) ? new DateTime?() : DateTime.Parse(recordData[i, 5].TrimEnd());
//                    DateTime? endDate = (string.IsNullOrEmpty(recordData[i, 6])) ? new DateTime?() : DateTime.Parse(recordData[i, 6].TrimEnd());
//                    List<string> ccds = (string.IsNullOrEmpty(recordData[i, 7])) ? new List<string>() : recordData[i, 7].TrimEnd().Split(',').ToList();
//                    List<string> majors = (string.IsNullOrEmpty(recordData[i, 8])) ? new List<string>() : recordData[i, 8].TrimEnd().Split(',').ToList();
//                    List<string> minors = (string.IsNullOrEmpty(recordData[i, 9])) ? new List<string>() : recordData[i, 9].TrimEnd().Split(',').ToList();
//                    List<string> specializations = (string.IsNullOrEmpty(recordData[i, 10])) ? new List<string>() : recordData[i, 10].TrimEnd().Split(',').ToList();
//                    List<string> honors = (string.IsNullOrEmpty(recordData[i, 11])) ? new List<string>() : recordData[i, 11].TrimEnd().Split(',').ToList();
//                    List<string> awards = (string.IsNullOrEmpty(recordData[i, 12])) ? new List<string>() : recordData[i, 12].TrimEnd().Split(',').ToList();

//                    AcadCredentials record = new AcadCredentials();
//                    record.Recordkey = key;
//                    record.AcadAcadProgram = "BA-MATH";
//                    record.AcadPersonId = personId;
//                    record.AcadDegree = degree;
//                    record.AcadDegreeDate = degreeDate;
//                    record.AcadStartDate = startDate;
//                    record.AcadEndDate = endDate;
//                    record.AcadCcd = ccds;
//                    record.AcadMajors = majors;
//                    record.AcadMinors = minors;
//                    record.AcadSpecialization = specializations;
//                    record.AcadHonors = honors;
//                    record.AcadAwards = awards;

//                    if (acadCredentialsIds.Where(id => id.Equals(key)).Count() == 0)
//                    {
//                        acadCredentialsIds.Add(key);
//                    }
//                    records.Add(key, record);
//                }
//                return records;
//            }

//            private Collection<StudentPrograms> BuildStudentProgramResponse(IEnumerable<StudentAcademicProgram> stuProg)
//            {

//                Collection<StudentPrograms> repoStuProgs = new Collection<StudentPrograms>();
//                foreach (var prog in stuProg)
//                {
//                    var repoStuProg = new StudentPrograms();
//                    repoStuProg.Recordkey = string.Concat(prog.StudentId.ToString(), "*", prog.ProgramCode);
//                    repoStuProg.RecordGuid = prog.Guid;
//                    repoStuProg.StprCatalog = prog.CatalogCode;
//                    repoStuProg.StprStatus = new List<string>() { prog.Status };
//                    repoStuProg.StprStartDate = new List<DateTime?>() { prog.StartDate };
//                    repoStuProg.StprEndDate = new List<DateTime?>() { prog.EndDate };

//                    // Build additional majors, minors, ccds, specializations responses, each one has four items with each of the above dates to verify proper date checking and inclusion
//                    repoStuProg.StprMajorListEntityAssociation = majors.ToList();
//                    repoStuProg.StprMinorListEntityAssociation = minors.ToList();
//                    repoStuProg.StprCcdListEntityAssociation = ccds.ToList();
//                    repoStuProg.StprSpecialtiesEntityAssociation = sps.ToList();

//                    repoStuProg.StprDept = prog.DepartmentCode;
//                    repoStuProg.StprLocation = prog.Location;                    
//                    repoStuProgs.Add(repoStuProg);
//                    //repoStuProg.StprAddnlMajors = prog.StudentProgramMajors;
//                }

//                return repoStuProgs;
//            }

//            private Collection<AcadPrograms> BuildAcadProgResponse(IEnumerable<AcademicProgram> acadProg)
//            {
//                Collection<AcadPrograms> repoAcadProgs = new Collection<AcadPrograms>();
//                foreach (var prog in acadProg)
//                {
//                    var repoStuProg = new AcadPrograms();
//                    repoStuProg.Recordkey = prog.Code;
//                    repoStuProg.RecordGuid = prog.Guid;
//                    repoStuProg.AcpgMajors = prog.MajorCodes;
//                    repoStuProg.AcpgMinors = prog.MinorCodes;
//                    repoStuProg.AcpgSpecializations = prog.SpecializationCodes;
//                    repoStuProg.AcpgCcds = new List<string>();
//                    repoStuProg.AcpgAcadLevel = prog.AcadLevelCode;
//                    repoAcadProgs.Add(repoStuProg);
//                }

//                return repoAcadProgs;
//            }

//            private Collection<Person> BuildStudentResponse()
//            {
//                Collection<Person> stuCollection = new Collection<Person>();
//                var stu = new Person();
//                stu.Recordkey = "12345678";
//                stuCollection.Add(stu);
//                return stuCollection;
//            }

//            private Collection<StudentProgramsStprMajorList> BuildAdditionalMajorsResponse(List<DateTime?> startDates, List<DateTime?> endDates)
//            {
//                var addlMajors = new Collection<StudentProgramsStprMajorList>();
//                var majors = new TestAcademicDisciplineRepository().GetOtherMajors();
//                for (int i = 0; i < majors.Count(); i++)
//                {
//                    var addlMajor = new StudentProgramsStprMajorList()
//                    {
//                        StprAddnlMajorsAssocMember = majors.ElementAt(i).Code,
//                        StprAddnlMajorReqmtsAssocMember = "req" + i.ToString(),
//                        StprAddnlMajorStartDateAssocMember = startDates.ElementAt(i),
//                        StprAddnlMajorEndDateAssocMember = endDates.ElementAt(i)
//                    };
//                    addlMajors.Add(addlMajor);
//                }
//                return addlMajors;
//            }

//            private Collection<StudentProgramsStprMinorList> BuildAdditionalMinorsResponse(List<DateTime?> startDates, List<DateTime?> endDates)
//            {
//                var addlMinors = new Collection<StudentProgramsStprMinorList>();
//                var minors = new TestAcademicDisciplineRepository().GetOtherMinors();
//                for (int i = 0; i < minors.Count(); i++)
//                {
//                    var addlMinor = new StudentProgramsStprMinorList()
//                    {
//                        StprMinorsAssocMember = minors.ElementAt(i).Code,
//                        StprMinorReqmtsAssocMember = "req" + i.ToString(),
//                        StprMinorStartDateAssocMember = startDates.ElementAt(i),
//                        StprMinorEndDateAssocMember = endDates.ElementAt(i)
//                    };
//                    addlMinors.Add(addlMinor);
//                }
//                return addlMinors;
//            }

//            private Collection<StudentProgramsStprCcdList> BuildAdditionalCcdsResponse(List<DateTime?> startDates, List<DateTime?> endDates)
//            {
//                var addlCcds = new Collection<StudentProgramsStprCcdList>();
//                var ccds = new TestAcademicCredentialsRepository().GetOtherCcds();
//                for (int i = 0; i < ccds.Count(); i++)
//                {
//                    var addlCcd = new StudentProgramsStprCcdList()
//                    {
//                        StprCcdsAssocMember = ccds.ElementAt(i).Code,
//                        StprCcdsReqmtsAssocMember = "req" + i.ToString(),
//                        StprCcdsStartDateAssocMember = startDates.ElementAt(i),
//                        StprCcdsEndDateAssocMember = endDates.ElementAt(i)
//                    };
//                    addlCcds.Add(addlCcd);
//                }
//                return addlCcds;
//            }

//            private Collection<StudentProgramsStprSpecialties> BuildAdditionalSpecialtiesResponse(List<DateTime?> startDates, List<DateTime?> endDates)
//            {
//                var addlSpecialtizations = new Collection<StudentProgramsStprSpecialties>();
//                var specializations = new TestAcademicDisciplineRepository().GetOtherSpecials();
//                for (int i = 0; i < specializations.Count(); i++)
//                {
//                    var addlSpecialty = new StudentProgramsStprSpecialties()
//                    {
//                        StprSpecializationsAssocMember = specializations.ElementAt(i).Code,
//                        StprSpecializationReqmtsAssocMember = "eq" + i.ToString(),
//                        StprSpecializationStartAssocMember = startDates.ElementAt(i),
//                        StprSpecializationEndAssocMember = endDates.ElementAt(i)
//                    };
//                    addlSpecialtizations.Add(addlSpecialty);
//                }
//                return addlSpecialtizations;
//            }

//            public IStudentAcademicProgramRepository BuildValidStudentAcademicProgramRepository()
//            {
//                // Set up dataAccessorMock as the object for the DataAccessor
//                transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(dataReaderMock.Object);

//                // Set up response for grade Get request
//                dataReaderMock.Setup<Task<Collection<StudentPrograms>>>(acc => acc.BulkReadRecordAsync<StudentPrograms>("", true)).Returns(Task.FromResult(stuProgResponseData));

//                //dataAccessorMock.Setup(acc => acc.BulkReadRecordAsync<DataContracts.AcadPrograms>("ACAD.PROGRAMS", It.IsAny<string[]>(), true)).ReturnsAsync(acadProgResponseData);
//                dataReaderMock.Setup(acc => acc.BulkReadRecordAsync<DataContracts.AcadPrograms>(It.IsAny<string[]>(), true)).ReturnsAsync(acadProgResponseData);

//                dataReaderMock.Setup(acc => acc.BulkReadRecordAsync<Person>(It.IsAny<string[]>(), true)).ReturnsAsync(studentResponseData);
//                //dataReaderMock.Setup<Task<Collection<AcadPrograms>>>(acc => acc.BulkReadRecordAsync<AcadPrograms>("ACAD.PROGRAMS",It.IsAny<string[]>(), true)).Returns(Task.FromResult(acadProgResponseData));
//                // get one student program
//                dataReaderMock.Setup<Task<StudentPrograms>>(acc => acc.ReadRecordAsync<StudentPrograms>(It.IsAny<string>(), It.IsAny<bool>())).Returns(Task.FromResult(stuProgResponseData[0]));

//                dataReaderMock.Setup(acc => acc.SelectAsync("STUDENT.PROGRAMS", "")).Returns(Task.FromResult(new string[] { "1", "2", "3" }));
//                dataReaderMock.Setup(acc => acc.SelectAsync("STUDENT.PROGRAMS", It.IsAny<string>())).Returns(Task.FromResult(new string[] { "12345678" }));

//                dataReaderMock.Setup(acc => acc.SelectAsync(It.IsAny<GuidLookup[]>())).Returns<GuidLookup[]>(gla =>
//                {
//                    var result = new Dictionary<string, GuidLookupResult>();
//                    foreach (var gl in gla)
//                    {
//                        var stuprog = stuProgResponseData.FirstOrDefault(x => x.RecordGuid == gl.Guid);
//                        result.Add(gl.Guid, stuprog == null ? null : new GuidLookupResult() { Entity = "STUDENT.PROGRAMS", PrimaryKey = stuprog.Recordkey });
//                    }
//                    return Task.FromResult(result);
//                });

//                dataReaderMock.Setup(acc => acc.ReadRecordAsync<StudentPrograms>(It.IsAny<string>(), It.IsAny<bool>())).Returns<string, bool>(
//                    (id, repl) => Task.FromResult(stuProgResponseData.FirstOrDefault(c => c.Recordkey == id)));
//                dataReaderMock.Setup(acc => acc.ReadRecordAsync<StudentPrograms>("STUDENT.PROGRAMS", It.IsAny<string>(), true)).Returns<string>(
//                    id => Task.FromResult(stuProgResponseData.FirstOrDefault(c => c.Recordkey == id)));

//                // mock data accessor STUDENT.PROGRAM.STATUSES
//                dataReaderMock.Setup<Task<ApplValcodes>>(a =>
//                    a.ReadRecordAsync<ApplValcodes>("ST.VALCODES", "STUDENT.PROGRAM.STATUSES", true))
//                    .ReturnsAsync(new ApplValcodes()
//                    {
//                        ValInternalCode = new List<string>() { "A", "C" },
//                        ValExternalRepresentation = new List<string>() { "Active", "Changed" },
//                        ValActionCode1 = new List<string>() { "2", "4" },
//                        ValsEntityAssociation = new List<ApplValcodesVals>()
//                        {
//                            new ApplValcodesVals() 
//                            {
//                                ValInternalCodeAssocMember = "A",
//                                ValExternalRepresentationAssocMember = "Active",
//                                ValActionCode1AssocMember = "2"
//                            },
//                            new ApplValcodesVals() 
//                            {
//                                ValInternalCodeAssocMember = "C",
//                                ValExternalRepresentationAssocMember = "Changed",
//                                ValActionCode1AssocMember = "4"
//                            }
//                        }
//                    });
//                //mock for majors
//                var majs = new List<Major>()
//                {
//                    new Major("ENGL", "English"),
//                    new Major("MATH", "Mathematics")
//                };
//                studentReferenceDataRepositoryMock.Setup(srdr => srdr.GetMajorsAsync(It.IsAny<bool>())).ReturnsAsync(majs);

//                //mock for minors
//                var minors = new List<Minor>()
//                {
//                    new Minor("HIST", "History"),
//                    new Minor("ACCT", "Accounting")
//                };
//                studentReferenceDataRepositoryMock.Setup(srdr => srdr.GetMinorsAsync(It.IsAny<bool>())).ReturnsAsync(minors);

//                //mock for specializations
//                var sps = new List<Specialization>()
//                {
//                    new Specialization("CERT", "Certification"),
//                    new Specialization("SCIE", "Sciences")
//                };
//                studentReferenceDataRepositoryMock.Setup(srdr => srdr.GetSpecializationsAsync()).ReturnsAsync(sps);

//                //mock for ccds
//                var ccd = new List<Ccd>()
//                {
//                    new Ccd("ELE", "Elementary Education"),
//                    new Ccd( "DI", "Diploma")
//                };
//                studentReferenceDataRepositoryMock.Setup(srdr => srdr.GetCcdsAsync()).ReturnsAsync(ccd);

//                stuRefData = studentReferenceDataRepositoryMock.Object;

//                // Construct student program repository
//                stuProgRepo = new StudentAptitudeAssessmentsRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettingsMock);

//                acadCredentialsResponseData = BuildAcadCredentialsResponseData(acadCredentialsRecords);
//                return stuProgRepo;
//            }

//            private Collection<AcadCredentials> BuildAcadCredentialsResponseData(Dictionary<string, AcadCredentials> acadCredentialsRecords)
//            {
//                Collection<AcadCredentials> acadCredentialsContracts = new Collection<AcadCredentials>();
//                foreach (var institutionItem in acadCredentialsRecords)
//                {
//                    acadCredentialsContracts.Add(institutionItem.Value);
//                }
//                return acadCredentialsContracts;
//            }

//            [TestMethod]
//            public async Task StudentAcademicProgramRepository_GetAcademicProgramEnrollmentByGuidAsync()
//            {
//                string stuProgId = "12345678*BA-MATH";
//                var stuprog = stuProgResponseData.FirstOrDefault(c => c.Recordkey == stuProgId);
//                string guid = stuprog.RecordGuid;
//                StudentAcademicProgram result = await stuProgRepo.GetStudentAcademicProgramByGuidAsync(guid, defaultInstitution);
//                Assert.AreEqual(stuprog.Recordkey, string.Concat(result.StudentId, "*", result.ProgramCode));
//                Assert.AreEqual(stuprog.RecordGuid, result.Guid);
//                Assert.AreEqual(stuprog.StprCatalog, result.CatalogCode);
//                Assert.AreEqual(stuprog.StprStartDate.FirstOrDefault(), result.StartDate);
//                Assert.AreEqual(stuprog.StprLocation, result.Location);
//                Assert.AreEqual(stuprog.StprIntgStartTerm, result.StartTerm);
//                Assert.AreEqual(stuprog.StprEndDate.FirstOrDefault(), result.EndDate);
//                Assert.AreEqual(stuprog.StprStatus.FirstOrDefault(), result.Status);


//            }

//            [TestMethod]
//            public async Task StudentAcademicProgramRepository_FilterAsync_Program()
//            {
//                dataReaderMock.Setup<Task<Collection<StudentPrograms>>>(acc => acc.BulkReadRecordAsync<StudentPrograms>("STUDENT.PROGRAMS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(stuProgResponseData));

//                IEnumerable<StudentAcademicProgram> results = (await stuProgRepo.GetStudentAcademicProgramsAsync(defaultInstitution, 0, 2, false, "BA-MATH")).Item1;
//                Assert.IsNotNull(results);
//                Assert.AreEqual(stuProgResponseData.Count(), results.Count());

//                foreach (var expected in stuProgResponseData)
//                {
//                    var result = results.FirstOrDefault(i => i.Guid.Equals(expected.RecordGuid));
//                    Assert.IsNotNull(result);

//                    Assert.AreEqual(expected.Recordkey, string.Concat(result.StudentId, "*", result.ProgramCode));
//                    Assert.AreEqual(expected.RecordGuid, result.Guid);
//                    Assert.AreEqual(expected.StprCatalog, result.CatalogCode);
//                    Assert.AreEqual(expected.StprStartDate.FirstOrDefault(), result.StartDate);
//                    Assert.AreEqual(expected.StprLocation, result.Location);
//                    Assert.AreEqual(expected.StprIntgStartTerm, result.StartTerm);
//                    Assert.AreEqual(expected.StprEndDate.FirstOrDefault(), result.EndDate);
//                    Assert.AreEqual(expected.StprStatus.FirstOrDefault(), result.Status);
//                }              
//            }
            
//            [TestMethod]
//            public async Task StudentAcademicProgramRepository_FilterAsync_StartOn()
//            {
//                DateTime? date = new DateTime(2001, 1, 1);
//                Collection<StudentPrograms> stuProgExpected = new Collection<StudentPrograms>( stuProgResponseData.Where(i => i.StprStartDate.Contains(date.Value)).ToList());
//                dataReaderMock.Setup<Task<Collection<StudentPrograms>>>(acc => acc.BulkReadRecordAsync<StudentPrograms>("STUDENT.PROGRAMS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(stuProgExpected));

//                IEnumerable<StudentAcademicProgram> results = (await stuProgRepo.GetStudentAcademicProgramsAsync(defaultInstitution, 0, 2, false, "", date.Value.ToString())).Item1;
                
//                Assert.IsNotNull(results);
//                Assert.AreEqual(stuProgExpected.Count(), results.Count());

//                foreach (var expected in stuProgExpected)
//                {
//                    var result = results.FirstOrDefault(i => i.Guid.Equals(expected.RecordGuid));
//                    Assert.IsNotNull(result);

//                    Assert.AreEqual(expected.Recordkey, string.Concat(result.StudentId, "*", result.ProgramCode));
//                    Assert.AreEqual(expected.RecordGuid, result.Guid);
//                    Assert.AreEqual(expected.StprCatalog, result.CatalogCode);
//                    Assert.AreEqual(expected.StprStartDate.FirstOrDefault(), result.StartDate);
//                    Assert.AreEqual(expected.StprLocation, result.Location);
//                    Assert.AreEqual(expected.StprIntgStartTerm, result.StartTerm);
//                    Assert.AreEqual(expected.StprEndDate.FirstOrDefault(), result.EndDate);
//                    Assert.AreEqual(expected.StprStatus.FirstOrDefault(), result.Status);
//                }
//            }

//            [TestMethod]
//            public async Task StudentAcademicProgramRepository_FilterAsync_EndtOn()
//            {
//                DateTime? date = new DateTime(2001, 12, 31);
//                Collection<StudentPrograms> stuProgExpected = new Collection<StudentPrograms>(stuProgResponseData.Where(i => i.StprEndDate.Contains(date.Value)).ToList());
//                dataReaderMock.Setup<Task<Collection<StudentPrograms>>>(acc => acc.BulkReadRecordAsync<StudentPrograms>("STUDENT.PROGRAMS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(stuProgExpected));

//                IEnumerable<StudentAcademicProgram> results = (await stuProgRepo.GetStudentAcademicProgramsAsync(defaultInstitution, 0, 2, false, "", "", date.Value.ToString())).Item1;

//                Assert.IsNotNull(results);
//                Assert.AreEqual(stuProgExpected.Count(), results.Count());
//                Assert.AreEqual(stuProgExpected.First().StprEndDate.First().Value, results.First().EndDate.Value);
//            }

//            [TestMethod]
//            public async Task StudentAcademicProgramRepository_FilterAsync_Student()
//            {
//                string student = "12345678";
//                char[] seperators = new char[] { '*' };
//                Collection<StudentPrograms> stuProgExpected = new Collection<StudentPrograms>(stuProgResponseData.Where(i => student.Equals(i.Recordkey.Split(seperators)[0])).ToList());
//                dataReaderMock.Setup<Task<Collection<StudentPrograms>>>(acc => acc.BulkReadRecordAsync<StudentPrograms>("STUDENT.PROGRAMS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(stuProgExpected));

//                IEnumerable<StudentAcademicProgram> results = (await stuProgRepo.GetStudentAcademicProgramsAsync(defaultInstitution, 0, 2, false, "", "", "", student)).Item1;

//                Assert.IsNotNull(results);
//                Assert.AreEqual(stuProgExpected.Count(), results.Count());

//                foreach (var expected in stuProgExpected)
//                {
//                    var result = results.FirstOrDefault(i => i.Guid.Equals(expected.RecordGuid));
//                    Assert.IsNotNull(result);

//                    Assert.AreEqual(expected.Recordkey, string.Concat(result.StudentId, "*", result.ProgramCode));
//                    Assert.AreEqual(expected.RecordGuid, result.Guid);
//                    Assert.AreEqual(expected.StprCatalog, result.CatalogCode);
//                    Assert.AreEqual(expected.StprStartDate.FirstOrDefault(), result.StartDate);
//                    Assert.AreEqual(expected.StprLocation, result.Location);
//                    Assert.AreEqual(expected.StprIntgStartTerm, result.StartTerm);
//                    Assert.AreEqual(expected.StprEndDate.FirstOrDefault(), result.EndDate);
//                    Assert.AreEqual(expected.StprStatus.FirstOrDefault(), result.Status);
//                }
//            }

//            [TestMethod]
//            public async Task StudentAcademicProgramRepository_FilterAsync_Catalog()
//            {
//                string catalog = "2012";
//                Collection<StudentPrograms> stuProgExpected = new Collection<StudentPrograms>(stuProgResponseData.Where(i => i.StprCatalog.Equals(catalog, StringComparison.OrdinalIgnoreCase)).ToList());
//                dataReaderMock.Setup<Task<Collection<StudentPrograms>>>(acc => acc.BulkReadRecordAsync<StudentPrograms>("STUDENT.PROGRAMS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(stuProgExpected));

//                IEnumerable<StudentAcademicProgram> results = (await stuProgRepo.GetStudentAcademicProgramsAsync(defaultInstitution, 0, 2, false, "", "", "", "", catalog)).Item1;

//                Assert.IsNotNull(results);
//                Assert.AreEqual(stuProgExpected.Count(), results.Count());

//                foreach (var expected in stuProgExpected)
//                {
//                    var result = results.FirstOrDefault(i => i.Guid.Equals(expected.RecordGuid));
//                    Assert.IsNotNull(result);

//                    Assert.AreEqual(expected.Recordkey, string.Concat(result.StudentId, "*", result.ProgramCode));
//                    Assert.AreEqual(expected.RecordGuid, result.Guid);
//                    Assert.AreEqual(expected.StprCatalog, result.CatalogCode);
//                    Assert.AreEqual(expected.StprStartDate.FirstOrDefault(), result.StartDate);
//                    Assert.AreEqual(expected.StprLocation, result.Location);
//                    Assert.AreEqual(expected.StprIntgStartTerm, result.StartTerm);
//                    Assert.AreEqual(expected.StprEndDate.FirstOrDefault(), result.EndDate);
//                    Assert.AreEqual(expected.StprStatus.FirstOrDefault(), result.Status);
//                }
//            }

//            [TestMethod]
//            public async Task StudentAcademicProgramRepository_FilterAsync_Status()
//            {
//                string status = "A";
//                Collection<StudentPrograms> stuProgExpected = new Collection<StudentPrograms>(stuProgResponseData.Where(i => i.StprStatus.Contains(status)).ToList());
//                dataReaderMock.Setup<Task<Collection<StudentPrograms>>>(acc => acc.BulkReadRecordAsync<StudentPrograms>("STUDENT.PROGRAMS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(stuProgExpected));

//                IEnumerable<StudentAcademicProgram> results = (await stuProgRepo.GetStudentAcademicProgramsAsync(defaultInstitution, 0, 2, false, "", "", "", "", "", status)).Item1;

//                Assert.IsNotNull(results);
//                Assert.AreEqual(stuProgExpected.Count(), results.Count());

//                foreach (var expected in stuProgExpected)
//                {
//                    var result = results.FirstOrDefault(i => i.Guid.Equals(expected.RecordGuid));
//                    Assert.IsNotNull(result);

//                    Assert.AreEqual(expected.Recordkey, string.Concat(result.StudentId, "*", result.ProgramCode));
//                    Assert.AreEqual(expected.RecordGuid, result.Guid);
//                    Assert.AreEqual(expected.StprCatalog, result.CatalogCode);
//                    Assert.AreEqual(expected.StprStartDate.FirstOrDefault(), result.StartDate);
//                    Assert.AreEqual(expected.StprLocation, result.Location);
//                    Assert.AreEqual(expected.StprIntgStartTerm, result.StartTerm);
//                    Assert.AreEqual(expected.StprEndDate.FirstOrDefault(), result.EndDate);
//                    Assert.AreEqual(expected.StprStatus.FirstOrDefault(), result.Status);
//                }
//            }

//            [TestMethod]
//            public async Task StudentAcademicProgramRepository_FilterAsync_ProgOwner()
//            {
//                string progOwner = "MATH";
//                Collection<StudentPrograms> stuProgExpected = new Collection<StudentPrograms>(stuProgResponseData.Where(i => i.StprDept.Equals(progOwner, StringComparison.OrdinalIgnoreCase)).ToList());
//                dataReaderMock.Setup<Task<Collection<StudentPrograms>>>(acc => acc.BulkReadRecordAsync<StudentPrograms>("STUDENT.PROGRAMS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(stuProgExpected));

//                IEnumerable<StudentAcademicProgram> results = (await stuProgRepo.GetStudentAcademicProgramsAsync(defaultInstitution, 0, 2, false, "", "", "", "", "", "", progOwner)).Item1;

//                Assert.IsNotNull(results);
//                Assert.AreEqual(stuProgExpected.Count(), results.Count());

//                foreach (var expected in stuProgExpected)
//                {
//                    var result = results.FirstOrDefault(i => i.Guid.Equals(expected.RecordGuid));
//                    Assert.IsNotNull(result);

//                    Assert.AreEqual(expected.StprDept, result.DepartmentCode);
//                    Assert.AreEqual(expected.Recordkey, string.Concat(result.StudentId, "*", result.ProgramCode));
//                    Assert.AreEqual(expected.RecordGuid, result.Guid);
//                    Assert.AreEqual(expected.StprCatalog, result.CatalogCode);
//                    Assert.AreEqual(expected.StprStartDate.FirstOrDefault(), result.StartDate);
//                    Assert.AreEqual(expected.StprLocation, result.Location);
//                    Assert.AreEqual(expected.StprIntgStartTerm, result.StartTerm);
//                    Assert.AreEqual(expected.StprEndDate.FirstOrDefault(), result.EndDate);
//                    Assert.AreEqual(expected.StprStatus.FirstOrDefault(), result.Status);
//                }
//            }

//            [TestMethod]
//            public async Task StudentAcademicProgramRepository_FilterAsync_Site()
//            {
//                string site = "MC";
//                Collection<StudentPrograms> stuProgExpected = new Collection<StudentPrograms>(stuProgResponseData.Where(i => i.StprLocation.Equals(site, StringComparison.OrdinalIgnoreCase)).ToList());
//                dataReaderMock.Setup<Task<Collection<StudentPrograms>>>(acc => acc.BulkReadRecordAsync<StudentPrograms>("STUDENT.PROGRAMS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(stuProgExpected));

//                IEnumerable<StudentAcademicProgram> results = (await stuProgRepo.GetStudentAcademicProgramsAsync(defaultInstitution, 0, 2, false, "", "", "", "", "", "", "", site)).Item1;

//                Assert.IsNotNull(results);
//                Assert.AreEqual(stuProgExpected.Count(), results.Count());

//                foreach (var expected in stuProgExpected)
//                {
//                    var result = results.FirstOrDefault(i => i.Guid.Equals(expected.RecordGuid));
//                    Assert.IsNotNull(result);

//                    Assert.AreEqual(expected.StprLocation, result.Location);
//                    Assert.AreEqual(expected.Recordkey, string.Concat(result.StudentId, "*", result.ProgramCode));
//                    Assert.AreEqual(expected.RecordGuid, result.Guid);
//                    Assert.AreEqual(expected.StprCatalog, result.CatalogCode);
//                    Assert.AreEqual(expected.StprStartDate.FirstOrDefault(), result.StartDate);
//                    Assert.AreEqual(expected.StprIntgStartTerm, result.StartTerm);
//                    Assert.AreEqual(expected.StprEndDate.FirstOrDefault(), result.EndDate);
//                    Assert.AreEqual(expected.StprStatus.FirstOrDefault(), result.Status);
//                }
//            }

//            [TestMethod]
//            public async Task StudentAcademicProgramRepository_FilterAsync_AcademicLevel()
//            {
//                string academicLevel = "UG";
//                string[] progs = new[] { "BA-MATH", "AA-NURS" };
//                char[] seperators = new char[] { '*' };
//                Collection<StudentPrograms> stuProgExpected = new Collection<StudentPrograms>(stuProgResponseData.Where(i => progs.Contains(i.Recordkey.Split(seperators)[1])).ToList());
//                dataReaderMock.Setup<Task<Collection<StudentPrograms>>>(acc => acc.BulkReadRecordAsync<StudentPrograms>("STUDENT.PROGRAMS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(stuProgExpected));
//                //allAcadProgs
//                Collection<AcadPrograms> programs = new Collection<AcadPrograms>(acadProgResponseData.Where(i => progs.Contains(i.Recordkey)).ToList());
//                dataReaderMock.Setup(acc => acc.BulkReadRecordAsync<DataContracts.AcadPrograms>(progs, true)).ReturnsAsync(programs);

//                IEnumerable<StudentAcademicProgram> results = (await stuProgRepo.GetStudentAcademicProgramsAsync(defaultInstitution, 0, 2, false, "", "", "", "", "", "", "", "", academicLevel)).Item1;

//                Assert.IsNotNull(results);
//                Assert.AreEqual(stuProgExpected.Count(), results.Count());

//                foreach (var expected in stuProgExpected)
//                {
//                    var result = results.FirstOrDefault(i => i.Guid.Equals(expected.RecordGuid));
//                    Assert.IsNotNull(result);

//                    Assert.AreEqual(academicLevel, result.AcademicLevelCode);
//                }
//            }

//            [TestMethod]
//            public async Task StudentAcademicProgramRepository_FilterAsync_GraduatedOn()
//            {
//                DateTime? date = new DateTime(2014, 6, 15);
//                string studentcriteria = "WITH ACAD.PERSON.ID EQ '?'";
//                string programcriteria = "WITH ACAD.ACAD.PROGRAM EQ '?'";
//                string defaultInstCriteria = "WITH ACAD.INSTITUTIONS.ID EQ '0000043'";


//                Collection<StudentPrograms> stuProgExpected = new Collection<StudentPrograms>(stuProgResponseData.Where(i => i.RecordGuid.Equals("bfde7c40-f27b-4747-bbd1-aab4b3b77bb9")).ToList());

//                dataReaderMock.Setup<Task<Collection<StudentPrograms>>>(acc => acc.BulkReadRecordAsync<StudentPrograms>("STUDENT.PROGRAMS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(stuProgExpected));
//                dataReaderMock.Setup(acc => acc.SelectAsync("ACAD.CREDENTIALS", studentcriteria, It.IsAny<string[]>(), "?", true, It.IsAny<int>())).ReturnsAsync(new string[] { "100" });
//                dataReaderMock.Setup(acc => acc.SelectAsync("ACAD.CREDENTIALS", programcriteria, It.IsAny<string[]>(), "?", true, It.IsAny<int>())).ReturnsAsync(new string[] { "100" });
//                dataReaderMock.Setup(acc => acc.SelectAsync("ACAD.CREDENTIALS", It.IsAny<string[]>(), defaultInstCriteria)).ReturnsAsync(new string[] { "100" });
//                dataReaderMock.Setup<Task<Collection<AcadCredentials>>>(acc => acc.BulkReadRecordAsync<AcadCredentials>("ACAD.CREDENTIALS", It.IsAny<string[]>(), true)).ReturnsAsync(acadCredentialsResponseData);

//                IEnumerable<StudentAcademicProgram> results = (await stuProgRepo.GetStudentAcademicProgramsAsync(defaultInstitution, 0, 2, false, "", "", "", "", "", "", "", "", "", date.Value.ToString())).Item1;

//                Assert.IsNotNull(results);
//                Assert.AreEqual(stuProgExpected.Count(), results.Count());

//                foreach (var expected in stuProgExpected)
//                {
//                    var result = results.FirstOrDefault(i => i.Guid.Equals(expected.RecordGuid));
//                    Assert.IsNotNull(result);

//                    Assert.AreEqual(date.Value, result.GraduationDate.Value);
//                }
//            }

//            [TestMethod]
//            public async Task StudentAcademicProgramRepository_FilterAsync_CCD()
//            {
//                DateTime? date = new DateTime(2014, 6, 15);
//                string studentcriteria = "WITH ACAD.PERSON.ID EQ '?'";
//                string programcriteria = "WITH ACAD.ACAD.PROGRAM EQ '?'";
//                string defaultInstCriteria = "WITH ACAD.INSTITUTIONS.ID EQ '0000043'";


//                Collection<StudentPrograms> stuProgExpected = new Collection<StudentPrograms>(stuProgResponseData.Where(i => i.RecordGuid.Equals("bfde7c40-f27b-4747-bbd1-aab4b3b77bb9")).ToList());

//                dataReaderMock.Setup<Task<Collection<StudentPrograms>>>(acc => acc.BulkReadRecordAsync<StudentPrograms>("STUDENT.PROGRAMS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(stuProgExpected));
//                dataReaderMock.Setup(acc => acc.SelectAsync("ACAD.CREDENTIALS", studentcriteria, It.IsAny<string[]>(), "?", true, It.IsAny<int>())).ReturnsAsync(new string[] { "100" });
//                dataReaderMock.Setup(acc => acc.SelectAsync("ACAD.CREDENTIALS", programcriteria, It.IsAny<string[]>(), "?", true, It.IsAny<int>())).ReturnsAsync(new string[] { "100" });
//                dataReaderMock.Setup(acc => acc.SelectAsync("ACAD.CREDENTIALS", It.IsAny<string[]>(), defaultInstCriteria)).ReturnsAsync(new string[] { "100" });
//                dataReaderMock.Setup(acc => acc.SelectAsync("ACAD.CREDENTIALS", It.IsAny<string>())).ReturnsAsync(new string[] { "100" });
//                dataReaderMock.Setup(acc => acc.SelectAsync("ACAD.PROGRAMS", It.IsAny<string>())).ReturnsAsync(new string[] { "100" });

//                dataReaderMock.Setup<Task<Collection<AcadCredentials>>>(acc => acc.BulkReadRecordAsync<AcadCredentials>("ACAD.CREDENTIALS", It.IsAny<string[]>(), true)).ReturnsAsync(acadCredentialsResponseData);

//                IEnumerable<StudentAcademicProgram> results = (await stuProgRepo.GetStudentAcademicProgramsAsync(defaultInstitution, 0, 2, false, "", "", "", "", "", "", "", "", "", "", "BA", "Degree", "Fall")).Item1;

//                Assert.IsNotNull(results);
//                Assert.AreEqual(stuProgExpected.Count(), results.Count());

//                foreach (var expected in stuProgExpected)
//                {
//                    var result = results.FirstOrDefault(i => i.Guid.Equals(expected.RecordGuid));
//                    Assert.IsNotNull(result);

//                    Assert.IsNotNull(result.StudentProgramCcds);
//                }
//            }

//            [TestMethod]
//            [ExpectedException(typeof(ArgumentNullException))]
//            public async Task StudentAcademicProgramRepository_GetAcademicProgramEnrollmentByGuidAsync_ArgumentNullException_NoGuid()
//            {
//                //Arrange
//                StudentAcademicProgram result = await stuProgRepo.GetStudentAcademicProgramByGuidAsync(null, null);

//            }

//            [TestMethod]
//            [ExpectedException(typeof(KeyNotFoundException))]
//            public async Task StudentAcademicProgramRepository_GetAcademicProgramEnrollmentByGuidAsync_NullPrimaryKey()
//            {
//                var stuProg = allStuProgs.FirstOrDefault();
//                StudentPrograms stProg = stuProgResponseData.FirstOrDefault();

//                var guid = stProg.RecordGuid;
//                var id = stProg.Recordkey;
//                var guidLookupResult = new GuidLookupResult() { Entity = "STUDENT.PROGRAMS", PrimaryKey = null };
//                var guidLookupDict = new Dictionary<string, GuidLookupResult>();
//                dataReaderMock.Setup(dr => dr.SelectAsync(It.IsAny<GuidLookup[]>())).Returns<GuidLookup[]>(gla =>
//                {
//                    if (gla.Any(gl => gl.Guid == guid))
//                    {
//                        guidLookupDict.Add(guid, guidLookupResult);
//                    }
//                    return Task.FromResult(guidLookupDict);
//                });

//                dataReaderMock.Setup(acc => acc.ReadRecordAsync<DataContracts.StudentPrograms>(stProg.Recordkey, true)).ReturnsAsync(stProg);


//                // Verify that the GetById returns a grade in the test repository
//                // with fields properly initialized
//                await stuProgRepo.GetStudentAcademicProgramByGuidAsync(guid, defaultInstitution);
//            }

//        }
//        #endregion


//    }
//}