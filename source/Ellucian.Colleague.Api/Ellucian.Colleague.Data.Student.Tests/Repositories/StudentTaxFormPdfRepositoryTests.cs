﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.Student.DataContracts;
using Ellucian.Colleague.Data.Student.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Ellucian.Colleague.Data.Student.Tests.Repositories
{
    [TestClass]
    public class StudentTaxFormPdfRepositoryTests : BaseRepositorySetup
    {
        private StudentTaxFormPdfDataRepository actualRepository;
        private Collection<TaxForm1098Forms> form1098contracts = new Collection<TaxForm1098Forms>();
        private Collection<TaxForm1098Boxes> taxForm1098Boxes = new Collection<TaxForm1098Boxes>();
        private Collection<BoxCodes> boxCodes = new Collection<BoxCodes>();
        private TaxForm1098Forms form1098contract;
        private Person personContract;
        private Corp corpContract;
        private CorpFounds corpFoundsContract;
        private Parm1098 parm1098Contract;

        #region Initialize and Cleanup
        [TestInitialize]
        public void Initialize()
        {
            MockInitialize();
            actualRepository = new StudentTaxFormPdfDataRepository(cacheProviderMock.Object,
                transFactoryMock.Object, loggerMock.Object);

            Build1098contracts();
            dataReaderMock.Setup(x => x.ReadRecordAsync<TaxForm1098Forms>(It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(form1098contract);
            });
            dataReaderMock.Setup(x => x.ReadRecordAsync<Person>(It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(personContract);
            });
            dataReaderMock.Setup(x => x.ReadRecordAsync<Corp>(It.IsAny<string>(), It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(corpContract);
            });
            dataReaderMock.Setup(x => x.ReadRecordAsync<CorpFounds>(It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(corpFoundsContract);
            });
            dataReaderMock.Setup(x => x.ReadRecordAsync<Parm1098>(It.IsAny<string>(), It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(parm1098Contract);
            });
            dataReaderMock.Setup(x => x.BulkReadRecordAsync<TaxForm1098Boxes>(It.IsAny<string[]>(), true)).Returns(() =>
            {
                return Task.FromResult(taxForm1098Boxes);
            });
            dataReaderMock.Setup(x => x.BulkReadRecordAsync<BoxCodes>(It.IsAny<string[]>(), true)).Returns(() =>
            {
                return Task.FromResult(boxCodes);
            });
        }

        [TestCleanup]
        public void Cleanup()
        {
            actualRepository = null;
        }
        #endregion

        [TestMethod]
        public async Task GetAsync_1098_Success()
        {
            var pdfData = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(form1098contract.Tf98fStudent, pdfData.StudentId);
            Assert.AreEqual(form1098contract.Tf98fName, pdfData.StudentName);
            Assert.AreEqual(form1098contract.Tf98fName2, pdfData.StudentName2);
            Assert.AreEqual(form1098contract.Tf98fTaxYear.ToString(), pdfData.TaxYear);
            Assert.AreEqual(form1098contract.Tf98fAddress, pdfData.StudentAddressLine1);
            var line2 = form1098contract.Tf98fCity + ", " + form1098contract.Tf98fState + " " + form1098contract.Tf98fZip;
            Assert.AreEqual(line2, pdfData.StudentAddressLine2);
            Assert.AreEqual(form1098contract.Tf98fCorrectionInd.ToUpper() == "Y", pdfData.Correction);
            Assert.AreEqual(form1098contract.Tf98fInstitution, pdfData.InstitutionId);
            Assert.AreEqual(personContract.Ssn, pdfData.SSN);
        }

        #region Tests
        [TestMethod]
        public async Task GetAsync_1098_NullTf98fTaxForm1098Boxes()
        {
            form1098contract.Tf98fTaxForm1098Boxes = null;
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(null, actualStatements);
        }

        [TestMethod]
        public async Task GetAsync_1098_NullPdfDataContract()
        {
            form1098contract = null;
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(null, actualStatements);
        }

        [TestMethod]
        public async Task GetAsync_1098_NullCorpFoundsContract()
        {
            corpFoundsContract = null;
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(null, actualStatements);
        }

        [TestMethod]
        public async Task GetAsync_1098_NullTf98fTaxYear()
        {
            form1098contract.Tf98fTaxYear = null;
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(null, actualStatements);
        }

        [TestMethod]
        public async Task GetAsync_1098_NullCorpTaxId()
        {
            corpFoundsContract.CorpTaxId = null;
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(null, actualStatements);
        }

        [TestMethod]
        public async Task GetAsync_1098_NullCorpName()
        {
            corpContract.CorpName = null;
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(null, actualStatements);
        }

        [TestMethod]
        public async Task GetAsync_1098_NullStudentAddress()
        {
            form1098contract.Tf98fAddress = null;
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(null, actualStatements.StudentAddressLine1);
        }

        [TestMethod]
        public async Task GetAsync_1098_EmptyStudentAddress()
        {
            form1098contract.Tf98fAddress = "";
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual("", actualStatements.StudentAddressLine1);
        }

        [TestMethod]
        public async Task GetAsync_1098_NonUSAAddress()
        {
            form1098contract.Tf98fCountry = "CA";
            var line2 = form1098contract.Tf98fCity + ", " + form1098contract.Tf98fState + " " + form1098contract.Tf98fZip + " " + form1098contract.Tf98fCountry;
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(line2, actualStatements.StudentAddressLine2);
        }

        [TestMethod]
        public async Task GetAsync_1098_NullStudentCity()
        {
            form1098contract.Tf98fCity = null;
            var line2 = form1098contract.Tf98fCity + ", " + form1098contract.Tf98fState + " " + form1098contract.Tf98fZip;
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(line2, actualStatements.StudentAddressLine2);
        }

        [TestMethod]
        public async Task GetAsync_1098_EmptyCity()
        {
            form1098contract.Tf98fCity = "";
            var line2 = form1098contract.Tf98fCity + ", " + form1098contract.Tf98fState + " " + form1098contract.Tf98fZip;
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(line2, actualStatements.StudentAddressLine2);
        }

        [TestMethod]
        public async Task GetAsync_1098_NullInstitutionId()
        {
            form1098contract.Tf98fInstitution = null;
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(null, actualStatements.InstitutionId);
        }

        [TestMethod]
        public async Task GetAsync_1098_EmptyInstitutionId()
        {
            form1098contract.Tf98fInstitution = "";
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual("", actualStatements.InstitutionId);
        }

        [TestMethod]
        public async Task GetAsync_1098_NullName()
        {
            form1098contract.Tf98fName = null;
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(null, actualStatements.StudentName);
        }

        [TestMethod]
        public async Task GetAsync_1098_EmptyName()
        {
            form1098contract.Tf98fName = "";
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual("", actualStatements.StudentName);
        }

        [TestMethod]
        public async Task GetAsync_1098_NullState()
        {
            form1098contract.Tf98fState = null;
            var line2 = form1098contract.Tf98fCity + ", " + form1098contract.Tf98fState + " " + form1098contract.Tf98fZip;
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(line2, actualStatements.StudentAddressLine2);
        }

        [TestMethod]
        public async Task GetAsync_1098_EmptyState()
        {
            form1098contract.Tf98fState = "";
            var line2 = form1098contract.Tf98fCity + ", " + form1098contract.Tf98fState + " " + form1098contract.Tf98fZip;
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(line2, actualStatements.StudentAddressLine2);
        }


        [TestMethod]
        public async Task GetAsync_1098_NullStudentZip()
        {
            form1098contract.Tf98fZip = null;
            var line2 = form1098contract.Tf98fCity + ", " + form1098contract.Tf98fState + " " + form1098contract.Tf98fZip;
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(line2, actualStatements.StudentAddressLine2);
        }

        [TestMethod]
        public async Task GetAsync_1098_EmptyStudentZip()
        {
            form1098contract.Tf98fZip = null;
            var line2 = form1098contract.Tf98fCity + ", " + form1098contract.Tf98fState + " " + form1098contract.Tf98fZip;
            var actualStatements = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(line2, actualStatements.StudentAddressLine2);
        }
        #endregion

        #region SSN scenarios
        [TestMethod]
        public async Task Get1098TPdfAsync_DefaultSsn()
        {
            var pdfData = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(personContract.Ssn, pdfData.SSN);
        }

        [TestMethod]
        public async Task Get1098TPdfAsync_NullPersonContract()
        {
            personContract = null;
            var actualStatement = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual("", actualStatement.SSN);
        }

        [TestMethod]
        public async Task Get1098TPdfAsync_NullSsn()
        {
            personContract.Ssn = null;
            var actualStatement = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual("", actualStatement.SSN);
        }

        [TestMethod]
        public async Task Get1098TPdfAsync_EmptySsn()
        {
            personContract.Ssn = "";
            var actualStatement = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual("", actualStatement.SSN);
        }

        [TestMethod]
        public async Task Get1098TPdfAsync_SsnNotProperLength()
        {
            personContract.Ssn = "000-0";
            var actualStatement = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(personContract.Ssn, actualStatement.SSN);
        }
        #endregion

        #region Masking scenarios
        [TestMethod]
        public async Task Get1098TPdfAsync_MaskedSsn1()
        {
            parm1098Contract.P1098MaskSsn = "y";
            var pdfData = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual("XXX-XX-" + personContract.Ssn.Substring(personContract.Ssn.Length - 4), pdfData.SSN);
        }

        [TestMethod]
        public async Task Get1098TPdfAsync_MaskedSsn2()
        {
            parm1098Contract.P1098MaskSsn = "Y";
            var pdfData = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual("XXX-XX-" + personContract.Ssn.Substring(personContract.Ssn.Length - 4), pdfData.SSN);
        }

        [TestMethod]
        public async Task Get1098TPdfAsync_NullParm1098Contract()
        {
            parm1098Contract = null;
            var pdfData = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(personContract.Ssn, pdfData.SSN);
        }

        [TestMethod]
        public async Task Get1098TPdfAsync_NullMaskParameter()
        {
            parm1098Contract.P1098MaskSsn = null;
            var pdfData = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(personContract.Ssn, pdfData.SSN);
        }

        [TestMethod]
        public async Task Get1098TPdfAsync_EmptyMaskParameter()
        {
            parm1098Contract.P1098MaskSsn = "";
            var pdfData = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual(personContract.Ssn, pdfData.SSN);
        }

        [TestMethod]
        public async Task Get1098TPdfAsync_SsnNotFullLength()
        {
            personContract.Ssn = "000-0";
            parm1098Contract.P1098MaskSsn = "Y";
            var pdfData = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual("XXX-XX-" + personContract.Ssn.Substring(personContract.Ssn.Length - 4), pdfData.SSN);
        }

        [TestMethod]
        public async Task Get1098TPdfAsync_SsnExactly4Digits()
        {
            personContract.Ssn = "000-";
            parm1098Contract.P1098MaskSsn = "Y";
            var pdfData = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual("XXX-XX-" + personContract.Ssn.Substring(personContract.Ssn.Length - 4), pdfData.SSN);
        }

        [TestMethod]
        public async Task Get1098TPdfAsync_SsnLessThan4Digits()
        {
            personContract.Ssn = "0";
            parm1098Contract.P1098MaskSsn = "Y";
            var pdfData = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual("XXX-XX-" + personContract.Ssn, pdfData.SSN);
        }

        [TestMethod]
        public async Task Get1098TPdfAsync_NullSsn_Masked()
        {
            personContract.Ssn = null;
            parm1098Contract.P1098MaskSsn = "Y";
            var pdfData = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual("", pdfData.SSN);
        }

        [TestMethod]
        public async Task Get1098TPdfAsync_EmptySsn_Masked()
        {
            personContract.Ssn = "";
            parm1098Contract.P1098MaskSsn = "Y";
            var pdfData = await actualRepository.Get1098TPdfAsync("0003946", "1");

            Assert.AreEqual("", pdfData.SSN);
        }

        #endregion

        #region Private methods
        private void Build1098contracts()
        {
            form1098contract = new TaxForm1098Forms()
            {
                Recordkey = "1",
                TaxForm1098FormsAdddate = new DateTime(2015, 05, 10, 5, 5, 5),
                TaxForm1098FormsAddtime = new DateTime(2015, 05, 10, 5, 5, 5),
                Tf98fAddress = "1234 Main St.",
                Tf98fCity = "Fairfax",
                Tf98fState = "VA",
                Tf98fZip = "22033",
                Tf98fCountry = "USA",
                Tf98fCorrectionInd = "Y",
                Tf98fInstitution = "0001234",
                Tf98fName = "Andrew Kleehammer",
                Tf98fName2 = "",
                Tf98fStudent = "0003946",
                Tf98fTaxYear = 2016,
                Tf98fTaxForm = "1098T",
                Tf98fTaxForm1098Boxes = new List<string>() { "1", "2", "3", "4" }
            };
            form1098contract.Tf98fTaxForm1098Boxes.Add("Box1");

            personContract = new Person()
            {
                Recordkey = "0003946",
                Ssn = "000-00-0001"
            };

            corpContract = new Corp()
            {
                Recordkey = "0001234",
                CorpName = new List<string>()
            };
            corpContract.CorpName.Add("0001234");

            corpFoundsContract = new CorpFounds()
            {
                CorpTaxId = "0001234",
                Recordkey = "0001234"
            };

            parm1098Contract = new Parm1098()
            {
                P1098TRefundBoxCode = "TUI",
                P1098TInstPhone = "703-259-9000",
                P1098TInstPhoneExt = "1009",
                P1098TFaBoxCode = "",
                P1098TLoadBoxCode = "",
                P1098TGradBoxCode = "",
                P1098TFaRefBoxCode = "",
                P1098TNewYrBoxCode = "",
                P1098TYears = new List<int?>() { 2016, 2015 },
                P1098TYrChgRptMeths = new List<string>() { "N", "Y" },
            };
            parm1098Contract.buildAssociations();

            taxForm1098Boxes.Add(new TaxForm1098Boxes()
            {
                Recordkey = "1",
                Tf98bAmt = 980,
                Tf98bBoxCode = "TUI",
                Tf98bValue = ""
            });

            taxForm1098Boxes.Add(new TaxForm1098Boxes()
            {
                Recordkey = "2",
                Tf98bAmt = 1000,
                Tf98bBoxCode = "CNY",
                Tf98bValue = ""
            });

            taxForm1098Boxes.Add(new TaxForm1098Boxes()
            {
                Recordkey = "3",
                Tf98bAmt = 150,
                Tf98bBoxCode = "LOD",
                Tf98bValue = ""
            });

            taxForm1098Boxes.Add(new TaxForm1098Boxes()
            {
                Recordkey = "4",
                Tf98bAmt = 275,
                Tf98bBoxCode = "GRD",
                Tf98bValue = ""
            });

            boxCodes.Add(new BoxCodes()
            {
                BxcBoxNumber = "2",
                Recordkey = "TUI"
            });

            boxCodes.Add(new BoxCodes()
            {
                BxcBoxNumber = "7",
                Recordkey = "CNY"
            });

            boxCodes.Add(new BoxCodes()
            {
                BxcBoxNumber = "8",
                Recordkey = "LOD"
            });

            boxCodes.Add(new BoxCodes()
            {
                BxcBoxNumber = "9",
                Recordkey = "GRD"
            });
        }
        #endregion
    }
}