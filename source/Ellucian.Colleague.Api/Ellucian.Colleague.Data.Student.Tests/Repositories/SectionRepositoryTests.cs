﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Caching;
using System.Threading;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.Student.DataContracts;
using Ellucian.Colleague.Data.Student.Repositories;
using Ellucian.Colleague.Data.Student.Transactions;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.DataContracts;
using Ellucian.Dmi.Runtime;
using Ellucian.Web.Cache;
using Ellucian.Web.Http.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;

namespace Ellucian.Colleague.Data.Student.Tests.Repositories
{
    [TestClass]
    public class SectionRepositoryTests : BaseRepositorySetup
    {
        protected void MainInitialize()
        {
            base.MockInitialize();

        }

        [TestClass]
        public class SectionRepository_TestAllFields : SectionRepositoryTests
        {
            /// <summary>
            /// Essentially this is a test of GetSectionAsync (which uses GetNonCachedSectionsAsync)
            /// </summary>
            
            SectionRepository sectionRepo;
            Mock<IStudentRepositoryHelper> stuRepoHelperMock;
            IStudentRepositoryHelper stuRepoHelper;
            CourseSections cs;
            CourseSecMeeting csm;
            CourseSecFaculty csf;
            Section result;
            CdDefaults cdDefaults;
            PortalSites ps;
            string csId;

            [TestInitialize]
            public async void Initialize()
           {
                MainInitialize();
                stuRepoHelperMock = new Mock<IStudentRepositoryHelper>();
                stuRepoHelper = stuRepoHelperMock.Object;
                csId = "12345";

                cs = new CourseSections()
                {
                    RecordGuid = Guid.NewGuid().ToString().ToLowerInvariant(),
                    Recordkey = csId,
                    RecordModelName = "sections",
                    SecAcadLevel = "UG",
                    SecActiveStudents = new List<string>(),
                    SecAllowAuditFlag = "N",
                    SecAllowPassNopassFlag = "N",
                    SecAllowWaitlistFlag = "Y",
                    SecBookOptions = new List<string>() { "R", "O" },
                    SecBooks = new List<string>() { "Book 1", "Book 2" },
                    SecCapacity = 30,
                    SecCeus = null,
                    SecCloseWaitlistFlag = "Y",
                    SecCourse = "210",
                    SecCourseLevels = new List<string>() { "100" },
                    SecCourseTypes = new List<string>() { "STND", "HONOR" },
                    SecCredType = "IN",
                    SecEndDate = new DateTime(2014, 12, 15),
                    SecFaculty = new List<string>(),
                    SecFacultyConsentFlag = "Y",
                    SecGradeScheme = "UGR",
                    SecInstrMethods = new List<string>() { "LEC", "LAB" },
                    SecLocation = "MAIN",
                    SecMaxCred = 6m,
                    SecMeeting = new List<string>(),
                    SecMinCred = 3m,
                    SecName = "MATH-4350-01",
                    SecNo = "01",
                    SecNoWeeks = 10,
                    SecOnlyPassNopassFlag = "N",
                    SecPortalSite = csId,
                    SecShortTitle = "Statistics",
                    SecStartDate = DateTime.Today.AddDays(-10),
                    SecTerm = "2014/FA",
                    SecTopicCode = "ABC",
                    SecVarCredIncrement = 1m,
                    SecWaitlistMax = 10,
                    SecWaitlistRating = "SR",
                    SecXlist = null, 
                    SecHideInCatalog = "Y"
                };
                cs.SecEndDate = cs.SecStartDate.Value.AddDays(69);
                cs.SecContactEntityAssociation = new List<CourseSectionsSecContact>();
                cs.SecContactEntityAssociation.Add(new CourseSectionsSecContact("LEC", 20.00m, 45.00m, "T", 37.50m));
                cs.SecContactEntityAssociation.Add(new CourseSectionsSecContact("LAB", 10.00m, 15.00m, "T", 45.00m));
                cs.SecDepartmentsEntityAssociation = new List<CourseSectionsSecDepartments>();
                cs.SecDepartmentsEntityAssociation.Add(new CourseSectionsSecDepartments("MATH", 75m));
                cs.SecDepartmentsEntityAssociation.Add(new CourseSectionsSecDepartments("PSYC", 25m));
                cs.SecStatusesEntityAssociation = new List<CourseSectionsSecStatuses>();
                cs.SecStatusesEntityAssociation.Add(new CourseSectionsSecStatuses(new DateTime(2001, 5, 15), "A"));
                // Instr methods association - instructional method and load
                cs.SecContactEntityAssociation = new List<CourseSectionsSecContact>();
                cs.SecContactEntityAssociation.Add(new CourseSectionsSecContact("LEC", 20.00m, 0m, "", 0m));
                cs.SecContactEntityAssociation.Add(new CourseSectionsSecContact("LAB", 10.00m, 0m, "", 0m));
                // Pointer to CourseSecFaculty
                cs.SecFaculty.Add("1");
                // Pointer to CourseSecMeeting
                cs.SecMeeting.Add("1");

                BuildLdmConfiguration(dataReaderMock, out cdDefaults);

                MockRecordAsync<CourseSections>("COURSE.SECTIONS", cs, cs.RecordGuid);

                // Set up repo response for course.sec.meeting
                csm = new CourseSecMeeting()
                    {
                        Recordkey = "1",
                        CsmInstrMethod = "LEC",
                        CsmCourseSection = "12345",
                        CsmStartDate = DateTime.Today,
                        CsmEndDate = DateTime.Today.AddDays(27),
                        CsmStartTime = (new DateTime(1, 1, 1, 10, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone).ToLocalDateTime(colleagueTimeZone),
                        CsmEndTime = (new DateTime(1, 1, 1, 11, 20, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone).ToLocalDateTime(colleagueTimeZone),
                        CsmMonday = "Y"
                    };
                MockRecordAsync<CourseSecMeeting>("COURSE.SEC.MEETING", csm);

                // Set up repo response for course.sec.faculty
                csf = new CourseSecFaculty()
                    {
                        Recordkey = "1",
                        CsfInstrMethod = "LEC",
                        CsfCourseSection = "12345",
                        CsfFaculty = "FAC1",
                        CsfFacultyPct = 100m,
                        CsfStartDate = cs.SecStartDate,
                        CsfEndDate = cs.SecEndDate,
                    };
                MockRecordAsync<CourseSecFaculty>("COURSE.SEC.FACULTY", csf);

                MockRecordsAsync<CourseSecXlists>("COURSE.SEC.XLISTS", new Collection<CourseSecXlists>());
                MockRecordsAsync<CourseSecPending>("COURSE.SEC.PENDING", new Collection<CourseSecPending>());
                ps = new PortalSites() { Recordkey = csId, PsLearningProvider = "MOODLE", PsPrtlSiteGuid = csId };
                MockRecordsAsync<PortalSites>("PORTAL.SITES", new Collection<PortalSites>() { ps });
                MockRecordsAsync<WaitList>("WAIT.LIST", new Collection<WaitList>());
                MockRecordsAsync<AcadReqmts>("ACAD.REQMTS", new Collection<AcadReqmts>());

                MockRecordAsync<Dflts>("CORE.PARMS", new Dflts() { Recordkey = "DEFAULTS", DfltsCampusCalendar = "CAL" });
                // Mock data needed to read campus calendar
                var startTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 06, 00, 00);
                var endTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 23, 30, 00);
                MockRecordAsync<Data.Base.DataContracts.CampusCalendar>("CAL", new Data.Base.DataContracts.CampusCalendar() { Recordkey = "CAL", CmpcDesc = "Calendar", CmpcDayStartTime = startTime, CmpcDayEndTime = endTime, CmpcBookPastNoDays = "30" });
                // Set up response for instructional methods and ST web defaults
                MockRecordsAsync<InstrMethods>("INSTR.METHODS", BuildValidInstrMethodResponse());

                // Set up repo response for section statuses
                var sectionStatuses = new ApplValcodes();
                sectionStatuses.ValsEntityAssociation = new List<ApplValcodesVals>();
                sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("A", "Active", "1", "A", "", "", ""));
                sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("I", "Inactive", "2", "I", "", "", ""));
                sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("C", "Cancelled", "", "C", "", "", ""));
                dataReaderMock.Setup<Task<ApplValcodes>>(cacc => cacc.ReadRecordAsync<ApplValcodes>("ST.VALCODES", "SECTION.STATUSES", true)).ReturnsAsync(sectionStatuses);

                //setup mocking for Stweb Defaults
                var stWebDflt = BuildStwebDefaults(); ;
                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>("ST.PARMS", It.IsAny<string>(), It.IsAny<bool>())).Returns<string, string, bool>(
                    (param, id, repl) => Task.FromResult((stWebDflt.Recordkey == id) ? stWebDflt : null)
                    );

                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>(It.IsAny<string>(), It.IsAny<bool>())).Returns<string, bool>(
                   (id, repl) => Task.FromResult((stWebDflt.Recordkey == id) ? stWebDflt : null)
                   );

                // Mock the data readers needed for the hiding sections tests
                var regUser = new RegUsers();
                regUser.Recordkey = "REGUSERID";
                regUser.RguRegControls = new List<string>() { "REGCTLID", "OTHER" };
                dataReaderMock.Setup<Task<RegUsers>>(cacc => cacc.ReadRecordAsync<RegUsers>("REG.USERS", "REGUSERID", false)).ReturnsAsync(regUser);
                var regCtl = new RegControls();
                regCtl.Recordkey = "REGCTLID";
                regCtl.RgcSectionLookupCriteria = new List<string>() { "WITH CRS.EXTERNAL.SOURCE=''", "AND WITH SEC.COURSE.TYPES NE 'PSE'" };
                dataReaderMock.Setup<Task<RegControls>>(cacc => cacc.ReadRecordAsync<RegControls>("REG.CONTROLS", "REGCTLID", false)).ReturnsAsync(regCtl);
                // The following id is viewable in the catalog - the rest aren't
                var viewableIds = new string[] { "1" };
                dataReaderMock.Setup(acc => acc.SelectAsync("COURSE.SECTIONS", It.IsAny<string[]>(), It.IsAny<string>())).ReturnsAsync(viewableIds);

                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
                x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                .ReturnsAsync(new Tuple<object, SemaphoreSlim>(
                    null,
                    new SemaphoreSlim(1, 1)
                    ));
                sectionRepo = new SectionRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettings);
                result = await sectionRepo.GetSectionAsync(csId);
                result.NumberOnWaitlist = 10;
                result.PermittedToRegisterOnWaitlist = 3;
                result.ReservedSeats = 5;
                result.GlobalCapacity = 40;
                result.GlobalWaitlistMaximum = 15;
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Id()
            {
                Assert.AreEqual(cs.Recordkey, result.Id);
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidOperationException))]
            public void SectionRepository_TestAllFields_IdChange()
            {
                result.Id = "2";
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Guid()
            {
                Assert.AreEqual(cs.RecordGuid, result.Guid);
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidOperationException))]
            public void SectionRepository_TestAllFields_GuidChange()
            {
                result.Guid = Guid.NewGuid().ToString().ToLowerInvariant();
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_CourseId()
            {
                Assert.AreEqual(cs.SecCourse, result.CourseId);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Number()
            {
                Assert.AreEqual(cs.SecNo, result.Number);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_StartDate()
            {
                Assert.AreEqual(cs.SecStartDate, result.StartDate);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_MinimumCredits()
            {
                Assert.AreEqual(cs.SecMinCred, result.MinimumCredits);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Ceus()
            {
                Assert.AreEqual(cs.SecCeus, result.Ceus);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Title()
            {
                Assert.AreEqual(cs.SecShortTitle, result.Title);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Departments_Count()
            {
                Assert.AreEqual(cs.SecDepartmentsEntityAssociation.Count, result.Departments.Count);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Departments_Type()
            {
                CollectionAssert.AllItemsAreInstancesOfType(result.Departments, typeof(OfferingDepartment));
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Departments_AcademicDepartmentCode()
            {
                CollectionAssert.AreEqual(cs.SecDepartmentsEntityAssociation.Select(x => x.SecDeptsAssocMember).ToList(),
                    result.Departments.Select(x => x.AcademicDepartmentCode).ToList());
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Departments_ResponsibilityPercentage()
            {
                Assert.AreEqual(100m, result.Departments.Sum(x => x.ResponsibilityPercentage));
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_CourseLevelCodes_Count()
            {
                Assert.AreEqual(cs.SecCourseLevels.Count, result.CourseLevelCodes.Count);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_CourseLevelCodes_Type()
            {
                CollectionAssert.AllItemsAreInstancesOfType(result.CourseLevelCodes, cs.SecCourseLevels.AsQueryable().ElementType);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_CourseLevelCodes()
            {
                CollectionAssert.AreEqual(cs.SecCourseLevels, result.CourseLevelCodes);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_AcademicLevelCode()
            {
                Assert.AreEqual(cs.SecAcadLevel, result.AcademicLevelCode);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_AllowPassNoPass()
            {
                Assert.AreEqual(cs.SecAllowPassNopassFlag == "Y", result.AllowPassNoPass);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_AllowAudit()
            {
                Assert.AreEqual(cs.SecAllowAuditFlag == "Y", result.AllowAudit);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_OnlyPassNopass()
            {
                Assert.AreEqual(cs.SecOnlyPassNopassFlag == "Y", result.OnlyPassNoPass);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_AllowWaitlist()
            {
                Assert.AreEqual(cs.SecAllowWaitlistFlag == "Y", result.AllowWaitlist);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_IsInstructorConsentRequired()
            {
                Assert.AreEqual(cs.SecFacultyConsentFlag == "Y", result.IsInstructorConsentRequired);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_WaitlistClosed()
            {
                Assert.AreEqual(cs.SecCloseWaitlistFlag == "Y", result.WaitlistClosed);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_CreditTypeCode()
            {
                Assert.AreEqual(cs.SecCredType, result.CreditTypeCode);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Statuses_Count()
            {
                Assert.AreEqual(cs.SecStatusesEntityAssociation.Count, result.Statuses.Count);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Statuses_Type()
            {
                CollectionAssert.AllItemsAreInstancesOfType(result.Statuses, typeof(SectionStatusItem));
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Statuses_Code()
            {
                CollectionAssert.AreEqual(cs.SecStatusesEntityAssociation.Select(x => x.SecStatusAssocMember).ToList(),
                    result.Statuses.Select(s => s.StatusCode).ToList());
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Statuses_StatusDate()
            {
                CollectionAssert.AreEqual(cs.SecStatusesEntityAssociation.Select(x => x.SecStatusDateAssocMember).ToList(),
                    result.Statuses.Select(s => s.Date).ToList());
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_GradeSchemeCode()
            {
                Assert.AreEqual(cs.SecGradeScheme, result.GradeSchemeCode);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_TermId()
            {
                Assert.AreEqual(cs.SecTerm, result.TermId);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_MaximumCredits()
            {
                Assert.AreEqual(cs.SecMaxCred, result.MaximumCredits);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_VariableCreditIncrement()
            {
                Assert.AreEqual(cs.SecVarCredIncrement, result.VariableCreditIncrement);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Location()
            {
                Assert.AreEqual(cs.SecLocation, result.Location);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_EndDate()
            {
                Assert.AreEqual(cs.SecEndDate, result.EndDate);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_FirstMeetingDate()
            {
                Assert.AreEqual(cs.SecFirstMeetingDate ?? cs.SecStartDate, result.FirstMeetingDate);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_LastMeetingDate()
            {
                Assert.AreEqual(cs.SecLastMeetingDate ?? cs.SecEndDate, result.LastMeetingDate);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_WaitlistRatingCode()
            {
                Assert.AreEqual(cs.SecWaitlistRating, result.WaitlistRatingCode);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Name()
            {
                Assert.AreEqual(cs.SecName, result.Name);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_FacultyIds_Count()
            {
                Assert.AreEqual(cs.SecFaculty.Count, result.FacultyIds.Count);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_FacultyIds_Type()
            {
                CollectionAssert.AllItemsAreInstancesOfType(result.FacultyIds, typeof(string));
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Faculty_Count()
            {
                Assert.AreEqual(cs.SecFaculty.Count, result.Faculty.Count);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Faculty_Type()
            {
                CollectionAssert.AllItemsAreInstancesOfType(result.Faculty, typeof(SectionFaculty));
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Faculty_Contents()
            {
                CollectionAssert.AreEqual(cs.SecFaculty, result.Faculty.Select(f => f.Id).ToList());
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_SectionBook_Count()
            {
                Assert.AreEqual(cs.SecBooks.Count, result.Books.Count);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_SectionBook_Type()
            {
                CollectionAssert.AllItemsAreInstancesOfType(result.Books, typeof(SectionBook));
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_SectionBook_BookId()
            {
                CollectionAssert.AreEqual(cs.SecBooks, result.Books.Select(x => x.BookId).ToList());
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_SectionBook_IsRequired()
            {
                CollectionAssert.AreEqual(cs.SecBookOptions.ConvertAll(b => b == "R"), result.Books.Select(x => x.IsRequired).ToList());
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_CourseTypeCodes_Count()
            {
                Assert.AreEqual(cs.SecCourseTypes.Count, result.CourseTypeCodes.Count);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_CourseTypeCodes_Type()
            {
                CollectionAssert.AllItemsAreInstancesOfType(result.CourseTypeCodes, cs.SecCourseTypes.AsQueryable().ElementType);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_CourseTypeCodes_Content()
            {
                CollectionAssert.AreEqual(cs.SecCourseTypes, result.CourseTypeCodes.ToList());
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_TopicCode()
            {
                Assert.AreEqual(cs.SecTopicCode, result.TopicCode);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_NumberOfWeeks()
            {
                Assert.AreEqual(cs.SecNoWeeks, result.NumberOfWeeks);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_OnlineCategory()
            {
                Assert.AreEqual(OnlineCategory.NotOnline, result.OnlineCategory);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_ActiveStudentIds_Count()
            {
                Assert.AreEqual(cs.SecActiveStudents.Count, result.ActiveStudentIds.Count);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_ActiveStudentIds_Type()
            {
                CollectionAssert.AllItemsAreInstancesOfType(result.ActiveStudentIds, cs.SecActiveStudents.AsQueryable().ElementType);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_ActiveStudentIds_Content()
            {
                CollectionAssert.AreEqual(cs.SecActiveStudents, result.ActiveStudentIds.ToList());
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_LearningProvider()
            {
                Assert.AreEqual(ps.PsLearningProvider, result.LearningProvider);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_LearningProviderSiteId()
            {
                Assert.AreEqual(ps.PsPrtlSiteGuid, result.LearningProviderSiteId);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_NumberOnWaitList()
            {
                Assert.AreEqual(10, result.NumberOnWaitlist);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_PermittedToRegisterOnWaitlist()
            {
                Assert.AreEqual(3, result.PermittedToRegisterOnWaitlist);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_ReservedSeats()
            {
                Assert.AreEqual(5, result.ReservedSeats);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_SectionCapacity()
            {
                Assert.AreEqual(cs.SecCapacity, result.SectionCapacity);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_WaitlistMaximum()
            {
                Assert.AreEqual(cs.SecWaitlistMax, result.WaitlistMaximum);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_GlobalCapacity()
            {
                Assert.AreEqual(40, result.GlobalCapacity);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_GlobalWaitlistMaximum()
            {
                Assert.AreEqual(15, result.GlobalWaitlistMaximum);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Capacity()
            {
                Assert.AreEqual(40, result.Capacity);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Available()
            {
                Assert.AreEqual(cs.SecCapacity - cs.SecActiveStudents.Count - 5 - 3, result.Available);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_WaitlistAvailable()
            {
                Assert.IsFalse(result.WaitlistAvailable);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_Waitlisted()
            {
                Assert.AreEqual(10, result.Waitlisted);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_CurrentStatus()
            {
                Assert.AreEqual(SectionStatus.Active, result.CurrentStatus);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_IsActive()
            {
                Assert.IsTrue(result.IsActive);
            }

            [TestMethod]
            public void SectionRepository_TestAllFields_HideInCatalog_True()
            {
                Assert.IsTrue(result.HideInCatalog);
            }

            [TestMethod]
            public void SectionRepository_MeetingLoadFactor_MeetingsCount()
            {
                Assert.AreEqual(cs.SecMeeting.Count, result.Meetings.Count);
            }

            [TestMethod]
            public void SectionRepository_MeetingLoadFactor_FacultyCount()
            {
                Assert.AreEqual(cs.SecFaculty.Count, result.Faculty.Count);
            }

            [TestMethod]
            public void SectionRepository_MeetingLoadFactor_Updated()
            {
                // Act - get the section
                //result = await sectionRepo.GetSectionAsync("12345");

                // Assert - verify load properly calculated and updated
                Assert.IsTrue(result.Meetings.Count() == 1);
                Assert.AreEqual(cs.SecContactEntityAssociation[0].SecInstrMethodsAssocMember, result.Meetings[0].FacultyRoster[0].InstructionalMethodCode);
                Assert.AreEqual(cs.SecContactEntityAssociation[0].SecLoadAssocMember.GetValueOrDefault(), result.Meetings[0].FacultyRoster[0].MeetingLoadFactor);
            }

            [TestMethod]
            public void SectionRepository_TotalMeetingMinutes_Updated()
            {
                // Act - get the section
                //result = await sectionRepo.GetSectionAsync("12345");

                // Assert - verify load properly calculated and updated
                int meetingTime = (int)(csm.CsmEndTime.Value - csm.CsmStartTime.Value).TotalMinutes;
                int numDays = (int)(csm.CsmEndDate.Value - csm.CsmStartDate.Value).TotalDays + 1;
                int time = meetingTime * numDays * CountMeetingDaysPerWeek(csm) / 7;
                Assert.IsTrue(result.Meetings.Count == 1);
                Assert.AreEqual(cs.SecContactEntityAssociation[0].SecInstrMethodsAssocMember, result.Meetings[0].FacultyRoster[0].InstructionalMethodCode);
                Assert.AreEqual(cs.SecContactEntityAssociation[0].SecLoadAssocMember.GetValueOrDefault(), result.Meetings[0].FacultyRoster[0].MeetingLoadFactor);
                Assert.AreEqual(time, result.Meetings[0].TotalMeetingMinutes);
            }
        }

        [TestClass]
        public class SectionRepository_TestGuidLookups : SectionRepositoryTests
        {
            string id, guid, id2, guid2, id3, guid3;
            GuidLookup guidLookup;
            GuidLookupResult guidLookupResult;
            Dictionary<string, GuidLookupResult> guidLookupDict;
            RecordKeyLookup recordLookup;
            RecordKeyLookupResult recordLookupResult;
            Dictionary<string, RecordKeyLookupResult> recordLookupDict;
            SectionRepository sectionRepo;

            [TestInitialize]
            public void Initialize()
            {
                base.MainInitialize();

                // Set up for GUID lookups
                id = "12345";
                id2 = "9876";
                id3 = "0012345";

                guid = "F5FC5310-17F1-49FC-926D-CC6E3DA6DAEA".ToLowerInvariant();
                guid2 = "5B35075D-14FB-45F7-858A-83F4174B76EA".ToLowerInvariant();
                guid3 = "246E16D9-8790-4D7E-ACA1-D5B1CB9D4A24".ToLowerInvariant();

                guidLookup = new GuidLookup(guid);
                guidLookupResult = new GuidLookupResult() { Entity = "COURSE.SECTIONS", PrimaryKey = id };
                guidLookupDict = new Dictionary<string, GuidLookupResult>();
                recordLookup = new RecordKeyLookup("COURSE.SECTIONS", id, false);
                recordLookupResult = new RecordKeyLookupResult() { Guid = guid };
                recordLookupDict = new Dictionary<string, RecordKeyLookupResult>();

                dataReaderMock.Setup(dr => dr.SelectAsync(It.IsAny<GuidLookup[]>())).Returns<GuidLookup[]>(gla =>
                    {
                        if (gla.Any(gl => gl.Guid == guid))
                        {
                            guidLookupDict.Add(guid, guidLookupResult);
                        }
                        if (gla.Any(gl => gl.Guid == guid2))
                        {
                            guidLookupDict.Add(guid2, null);
                        }
                        if (gla.Any(gl => gl.Guid == guid3))
                        {
                            guidLookupDict.Add(guid3, new GuidLookupResult() { Entity = "PERSON", PrimaryKey = id3 });
                        }
                        return Task.FromResult(guidLookupDict);
                    });
                dataReaderMock.Setup(dr => dr.SelectAsync(It.IsAny<RecordKeyLookup[]>())).Returns<RecordKeyLookup[]>(rkla =>
                    {
                        if (rkla.Any(rkl => rkl.PrimaryKey == id))
                        {
                            recordLookupDict.Add(recordLookup.ResultKey, recordLookupResult);
                        }
                        if (rkla.Any(rkl => rkl.PrimaryKey == id2))
                        {
                            recordLookupDict.Add("COURSE.SECTIONS+" + id2, null);
                        }
                        if (rkla.Any(rkl => rkl.PrimaryKey == id3))
                        {
                            recordLookupDict.Add("PERSON+" + id3, new RecordKeyLookupResult() { Guid = guid3 });
                        }
                        return Task.FromResult(recordLookupDict);
                    });

                //setup mocking for Stweb Defaults
                var stWebDflt = BuildStwebDefaults(); ;
                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>("ST.PARMS", It.IsAny<string>(), It.IsAny<bool>())).Returns<string, string, bool>(
                    (param, idd, repl) => Task.FromResult((stWebDflt.Recordkey == idd) ? stWebDflt : null)
                    );

                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>(It.IsAny<string>(), It.IsAny<bool>())).Returns<string, bool>(
                   (idd, repl) => Task.FromResult((stWebDflt.Recordkey == idd) ? stWebDflt : null)
                   );
                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
            x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
            .ReturnsAsync(new Tuple<object, SemaphoreSlim>(
                null,
                new SemaphoreSlim(1, 1)
                ));
                sectionRepo = new SectionRepository(cacheProvider, transFactory, logger, apiSettings);
            }

            [TestMethod]
            public async Task SectionRepository_TestGuidLookups_GuidLookupSuccess()
            {
                var result = await sectionRepo.GetSectionIdFromGuidAsync(guid);
                Assert.AreEqual(id, result);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionRepository_TestGuidLookups_GuidNull()
            {
                var result = await sectionRepo.GetSectionIdFromGuidAsync(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionRepository_TestGuidLookups_GuidEmpty()
            {
                var result = await sectionRepo.GetSectionIdFromGuidAsync(string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionRepository_TestGuidLookups_GuidLookupFailure()
            {
                var result = await sectionRepo.GetSectionIdFromGuidAsync(guid2);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionRepository_TestGuidLookups_GuidLookup_KeyNotFound()
            {
                var result = await sectionRepo.GetSectionIdFromGuidAsync(guid2);
            }

            [TestMethod]
            [ExpectedException(typeof(RepositoryException))]
            public async Task SectionRepository_TestGuidLookups_GuidLookupWrongFile()
            {
                var result = await sectionRepo.GetSectionIdFromGuidAsync(guid3);
            }

            [TestMethod]
            public async Task SectionRepository_TestGuidLookups_RecordKeyLookupSuccess()
            {
                var result = await sectionRepo.GetSectionGuidFromIdAsync(id);
                Assert.AreEqual(guid, result);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionRepository_TestGuidLookups_RecordKeyLookupFailure_IdNull()
            {
                var result = await sectionRepo.GetSectionGuidFromIdAsync(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionRepository_TestGuidLookups_RecordKeyLookupFailure_IdEmpty()
            {
                var result = await sectionRepo.GetSectionGuidFromIdAsync(string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(RepositoryException))]
            public async Task SectionRepository_TestGuidLookups_RecordKeyLookupFailure()
            {
                var result = await sectionRepo.GetSectionGuidFromIdAsync(id2);
            }
        }

        [TestClass]
        public class SectionRepository_GeneralTests : SectionRepositoryTests
        {
            IEnumerable<Section> regSections;
            Dictionary<string, Section> regSectionsDict;
            List<Term> registrationTerms = new List<Term>();
            Collection<CourseSections> sectionsResponseData;
            Collection<CourseSecMeeting> sectionMeetingResponseData;
            Collection<CourseSecFaculty> sectionFacultyResponseData;
            GetStudentCourseSecStudentsResponse studentCourseSecResponseData;
            DeleteInstructionalEventRequest request;
            DeleteInstructionalEventResponse response;
            Collection<PortalSites> portalSitesResponseData;
            Collection<CourseSecXlists> crosslistResponseData;
            Collection<CourseSecPending> pendingResponseData;
            Collection<WaitList> waitlistResponseData;
            GetStudentCourseSecStudentsRequest updateRequest;
            //   ApiSettings apiSettingsMock;
            CdDefaults cdDefaults;
            String guid;
            String id;

            SectionRepository sectionRepo;

            [TestInitialize]
            public async void Initialize()
            {
                base.MainInitialize();
                Term term1 = new Term("2012/FA", "Fall 2012", new DateTime(2012, 9, 1), new DateTime(2012, 12, 15), 2012, 1, true, true, "2012/FA", true);
                Term term2 = new Term("2013/SP", "Spring 2013", new DateTime(2013, 1, 1), new DateTime(2013, 5, 15), 2012, 2, true, true, "2013/SP", true);
                registrationTerms.Add(term1);
                registrationTerms.Add(term2);

                // Set up for GUID lookups
                id = "12345";
                guid = "F5FC5310-17F1-49FC-926D-CC6E3DA6DAEA".ToLowerInvariant();
                

                // Build Section responses used for mocking
                regSections = await new TestSectionRepository().GetRegistrationSectionsAsync(registrationTerms);
                regSectionsDict = new Dictionary<string, Section>();
                foreach (var sec in regSections)
                {
                    regSectionsDict[sec.Id] = sec;
                }
                sectionsResponseData = BuildSectionsResponse(regSections);
                sectionMeetingResponseData = BuildSectionMeetingsResponse(regSections);
                sectionFacultyResponseData = BuildSectionFacultyResponse(regSections);
                studentCourseSecResponseData = BuildStudentCourseSecResponse(regSections);
                portalSitesResponseData = BuildPortalSitesResponse(regSections);
                crosslistResponseData = BuildCrosslistResponse(regSections);
                pendingResponseData = BuildPendingSectionResponse(regSections);
                waitlistResponseData = BuildWaitlistResponse(regSections);

                request = new DeleteInstructionalEventRequest();
                response = new DeleteInstructionalEventResponse()
                {
                    DeleteInstructionalEventErrors = new List<DeleteInstructionalEventErrors>(),
                    DeleteInstructionalEventWarnings = new List<DeleteInstructionalEventWarnings>()
                };

                // Build section repository
                sectionRepo = BuildValidSectionRepository();
            }

            [TestCleanup]
            public void Cleanup()
            {
                transFactoryMock = null;
                dataReaderMock = null;
                cacheProviderMock = null;
                //localCacheMock = null;
                sectionsResponseData = null;
                regSections = null;
                sectionRepo = null;
            }

            [TestMethod]
            public async Task SectionRepository_GetRegistrationSections_ReturnsAll()
            {
                var sections = await sectionRepo.GetRegistrationSectionsAsync(registrationTerms);
                Assert.AreEqual(regSections.Count(), sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetRegistrationSections_HideInCatalog_Sections()
            {
                var expectedHiddenSections = regSections.Where(s => s.HideInCatalog);
                var sections = await sectionRepo.GetRegistrationSectionsAsync(registrationTerms);
                var actualHiddenSections = sections.Where(s => s.HideInCatalog);
                Assert.AreEqual(expectedHiddenSections.Count(), actualHiddenSections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetRegistrationSections_ReturnsNone()
            {
                var sections = await sectionRepo.GetRegistrationSectionsAsync(new List<Term>());
                Assert.AreEqual(0, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetRegistrationSections_TestProperties()
            {
                IEnumerable<Section> sections = await sectionRepo.GetRegistrationSectionsAsync(registrationTerms);
                Section testSection = sections.ElementAt(0);
                Section sec = regSections.Where(s => s.Id == testSection.Id).First();
                Assert.AreEqual(testSection.AcademicLevelCode, sec.AcademicLevelCode);
                Assert.AreEqual(testSection.Ceus, sec.Ceus);
                Assert.AreEqual(testSection.CourseId, sec.CourseId);
                Assert.AreEqual(testSection.CourseLevelCodes.Count(), sec.CourseLevelCodes.Count());
                Assert.AreEqual(testSection.Departments.Count(), sec.Departments.Count());
                Assert.AreEqual(testSection.MinimumCredits, sec.MinimumCredits);
                Assert.AreEqual(testSection.MaximumCredits, sec.MaximumCredits);
                Assert.AreEqual(testSection.VariableCreditIncrement, sec.VariableCreditIncrement);
                Assert.AreEqual(testSection.Number, sec.Number);
                Assert.AreEqual(testSection.TermId, sec.TermId);
                Assert.AreEqual(testSection.StartDate, sec.StartDate);
                Assert.AreEqual(testSection.Location, sec.Location);
                Assert.AreEqual(testSection.Title, sec.Title);
                Assert.AreEqual(testSection.EndDate, sec.EndDate);
                Assert.AreEqual(testSection.AllowWaitlist, sec.AllowWaitlist);
                Assert.AreEqual(testSection.WaitlistAvailable, sec.WaitlistAvailable);
            }


            [TestMethod]
            public async Task SectionRepository_Get_CachedRegistrationSections()
            {
                // Set up local cache mock to respond to cache request:
                //  -to "Contains" request, return "true" to indicate item is in cache
                //  -to "Get" request, return the cache item (in this case the "AllRegistrationSections" cache item)
                string cacheKey = sectionRepo.BuildFullCacheKey("AllRegistrationSections");
                cacheProviderMock.Setup(x => x.Contains(cacheKey, null)).Returns(true);
                cacheProviderMock.Setup(x => x.Get(cacheKey, null)).Returns(regSectionsDict).Verifiable();

                // return null for request, so that if we have a result, it wasn't the data accessor that returned it.
                dataReaderMock.Setup(acc => acc.SelectAsync("COURSE.SECTIONS", "")).Returns(Task.FromResult(new string[] { "1", "2" }));
                dataReaderMock.Setup<Task<Collection<CourseSections>>>(acc => acc.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string>(), true)).Returns(Task.FromResult<Collection<CourseSections>>(new Collection<CourseSections>()));

                // Assert that proper course was returned
                var sections = await sectionRepo.GetRegistrationSectionsAsync(registrationTerms);
                Assert.IsTrue(sections.Count() >= 40);
                // Verify that Get was called to get the courses from cache
                cacheProviderMock.Verify(m => m.Get(cacheKey, null));
            }

            [TestMethod]
            public async Task SectionRepository_GetSingleSection_TestProperties()
            {
                List<string> courseIds = new List<string>() { "139", "42" };
                IEnumerable<Section> sections = await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms);
                Section section = sections.Where(s => s.Id == "1").FirstOrDefault();
                Assert.AreEqual("1", section.Id);
                Assert.AreEqual("2012/FA", section.TermId);
                Assert.IsTrue(section.IsActive);
                Assert.IsTrue(section.AllowPassNoPass);
                Assert.IsTrue(section.AllowAudit);
                Assert.IsFalse(section.OnlyPassNoPass);
                Assert.IsFalse(section.AllowWaitlist);
                Assert.IsFalse(section.IsInstructorConsentRequired);
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionsCached_NoSectionsFound()
            {
                IEnumerable<string> courseIds = new List<string>() { "99999", "JUNK" };
                IEnumerable<Section> sections = await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms);
                Assert.AreEqual(0, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionCached_ZeroCourseIds()
            {
                IEnumerable<string> courseIds = new List<string>();
                IEnumerable<Section> sections = await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms);
                Assert.AreEqual(0, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionsCached_NullCourseIds()
            {
                IEnumerable<Section> sections = await sectionRepo.GetCourseSectionsCachedAsync(null, registrationTerms);
                Assert.AreEqual(0, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionsCached_SectionsReturned()
            {
                List<string> courseIds = new List<string>() { "139", "42" };
                IEnumerable<Section> sections = await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms);
                Assert.AreEqual(18, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionsCached_SectionMeetings()
            {
                List<string> courseIds = new List<string>() { "42" };
                IEnumerable<Section> sections = await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms);
                var section = sections.ElementAt(0);
                var mt = section.Meetings.ElementAt(0);
                var time = mt.StartTime;
                Assert.AreEqual(1, section.Meetings.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionsCached_NoAudit_NoPassFail()
            {
                List<string> courseIds = new List<string>() { "7272" };
                var section = (await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms)).Where(s => s.Number == "99").FirstOrDefault();
                Assert.IsFalse(section.AllowAudit);
                Assert.IsFalse(section.AllowPassNoPass);
                Assert.IsFalse(section.OnlyPassNoPass);
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionsCached_CourseCoreqs_PreConversion()
            {
                // Check course requisites on the Section. This scenario will only happen preconversion
                // because course coreqs on the section level will be represented by requisite codes once converted.
                BuildCourseParametersUnConvertedResponse(cdDefaults);
                dataReaderMock.Setup<Task<CdDefaults>>(acc => acc.ReadRecordAsync<CdDefaults>("ST.PARMS", "CD.DEFAULTS", true)).ReturnsAsync(cdDefaults);

                List<string> courseIds = new List<string>() { "7703" };
                // Get the first section from the repo for course "7703" (they all have the same coreq setup)
                var section = (await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms)).First();
                // Check that the expected course corequisites are found in the section's list of requisites
                var req1 = section.Requisites.Where(r => r.CorequisiteCourseId == "42").FirstOrDefault();
                Assert.AreEqual(true, req1.IsRequired);
                Assert.AreEqual(RequisiteCompletionOrder.PreviousOrConcurrent, req1.CompletionOrder);
                var req2 = section.Requisites.Where(r => r.CorequisiteCourseId == "21").FirstOrDefault();
                Assert.AreEqual(false, req2.IsRequired);
                Assert.AreEqual(RequisiteCompletionOrder.PreviousOrConcurrent, req2.CompletionOrder);
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionsCached_SectionCoreqs_PostConversion()
            {
                List<string> courseIds = new List<string>() { "7703" };
                // Get the first section from the repo for course "7703" (they all have the same coreq setup)
                List<Term> terms = new List<Term>() { registrationTerms.ElementAt(0) };
                // Get a section for this course for any term
                var section = (await sectionRepo.GetCourseSectionsCachedAsync(courseIds, terms)).First();

                // multi-section requisite has all sections and number needed
                var req1 = section.SectionRequisites.Where(r => r.CorequisiteSectionIds.Count() > 1).FirstOrDefault();
                Assert.IsNotNull(req1);
                Assert.AreEqual(2, req1.NumberNeeded);
                Assert.IsTrue(req1.IsRequired);
                Assert.AreEqual(3, req1.CorequisiteSectionIds.Count());
                // See TestSectionRepository, scan for "7703" to find the requisites built for all sections of this course
                var sec = (await sectionRepo.GetCourseSectionsCachedAsync(new List<string>() { "87" }, terms)).Where(s => s.Number == "02").First(); // HIST-400-02
                Assert.IsTrue(req1.CorequisiteSectionIds.Contains(sec.Id));
                sec = (await sectionRepo.GetCourseSectionsCachedAsync(new List<string>() { "154" }, terms)).Where(s => s.Number == "01").First(); // PHYS-100-01
                Assert.IsTrue(req1.CorequisiteSectionIds.Contains(sec.Id));
                sec = (await sectionRepo.GetCourseSectionsCachedAsync(new List<string>() { "333" }, terms)).Where(s => s.Number == "03").First(); // MATH-152-03
                Assert.IsTrue(req1.CorequisiteSectionIds.Contains(sec.Id));

                // Verify that the recommended section requisite has the expected data
                var reqs = section.SectionRequisites.Where(r => r.CorequisiteSectionIds.Count() == 1);
                Assert.AreEqual(2, reqs.Count());

                // Get the section cited in this requisite, verify that it is the correct section
                sec = (await sectionRepo.GetCourseSectionsCachedAsync(new List<string>() { "91" }, terms)).Where(s => s.Number == "02").First(); // MATH-400-02
                var req2 = reqs.Where(r => r.CorequisiteSectionIds.ElementAt(0) == sec.Id).FirstOrDefault();
                Assert.IsFalse(req2.IsRequired);
                Assert.AreEqual(1, req2.NumberNeeded);
                sec = (await sectionRepo.GetCourseSectionsCachedAsync(new List<string>() { "159" }, terms)).Where(s => s.Number == "01").First(); // MATH-152-03
                req2 = reqs.Where(r => r.CorequisiteSectionIds.ElementAt(0) == sec.Id).FirstOrDefault();
                Assert.IsFalse(req2.IsRequired);
                Assert.AreEqual(1, req2.NumberNeeded);

            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionsCached_SectionCoreqs_PreConversion()
            {
                BuildCourseParametersUnConvertedResponse(cdDefaults);
                dataReaderMock.Setup<Task<CdDefaults>>(acc => acc.ReadRecordAsync<CdDefaults>("ST.PARMS", "CD.DEFAULTS", true)).ReturnsAsync(cdDefaults);

                List<string> courseIds = new List<string>() { "7703" };
                // Get the first section from the repo for course "7703" (they all have the same coreq setup)
                List<Term> terms = new List<Term>() { registrationTerms.ElementAt(0) };
                // Get a section for this course for any term
                var section = (await sectionRepo.GetCourseSectionsCachedAsync(courseIds, terms)).First();

                // Preconversion, an individual section requisite is created for each item, because there is no such concept as "2 out of 3" yet.
                // verify that the required section requisite has the expected data
                var secReqs = section.SectionRequisites.Where(r => r.CorequisiteSectionIds != null);

                // Three individual required section requisites
                var sec = (await sectionRepo.GetCourseSectionsCachedAsync(new List<string>() { "87" }, terms)).Where(s => s.Number == "02").First(); // HIST-400-02
                var req1 = secReqs.Where(r => r.CorequisiteSectionIds.Contains(sec.Id)).FirstOrDefault();
                Assert.IsTrue(req1.IsRequired);
                Assert.AreEqual(1, req1.NumberNeeded);

                sec = (await sectionRepo.GetCourseSectionsCachedAsync(new List<string>() { "154" }, terms)).Where(s => s.Number == "01").First(); // PHYS-100-01
                req1 = secReqs.Where(r => r.CorequisiteSectionIds.Contains(sec.Id)).FirstOrDefault();
                Assert.IsTrue(req1.IsRequired);
                Assert.AreEqual(1, req1.NumberNeeded);

                sec = (await sectionRepo.GetCourseSectionsCachedAsync(new List<string>() { "333" }, terms)).Where(s => s.Number == "03").First(); // MATH-152-03
                req1 = secReqs.Where(r => r.CorequisiteSectionIds.Contains(sec.Id)).FirstOrDefault();
                Assert.IsTrue(req1.IsRequired);
                Assert.AreEqual(1, req1.NumberNeeded);

                // Two Individual recommended section requisites
                sec = (await sectionRepo.GetCourseSectionsCachedAsync(new List<string>() { "91" }, terms)).Where(s => s.Number == "02").First(); // MATH-400-02
                req1 = secReqs.Where(r => r.CorequisiteSectionIds.ElementAt(0) == sec.Id).FirstOrDefault();
                Assert.IsFalse(req1.IsRequired);
                Assert.AreEqual(1, req1.NumberNeeded);

                sec = (await sectionRepo.GetCourseSectionsCachedAsync(new List<string>() { "159" }, terms)).Where(s => s.Number == "01").First(); // SOCI-100-01
                req1 = secReqs.Where(r => r.CorequisiteSectionIds.ElementAt(0) == sec.Id).FirstOrDefault();
                Assert.IsFalse(req1.IsRequired);
                Assert.AreEqual(1, req1.NumberNeeded);
            }

            [TestMethod]
            public async Task SectionRepository_IncludesAcadReqmtReqsWhenOverrideIsTrue()
            {
                // This section set up with a requisite with a requirement code and override set to true. Verify requisite is included
                List<string> courseIds = new List<string>() { "7706" };
                var section = (await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms)).Where(s => s.Number == "04").First();
                // Check that the completion order and required flags are converted properly, based on the ACAD.REQMTS response.
                var req = section.Requisites.Where(r => r.RequirementCode == "PREREQ1").FirstOrDefault();
                Assert.AreEqual(true, req.IsRequired);
                Assert.AreEqual(RequisiteCompletionOrder.Previous, req.CompletionOrder);
                req = section.Requisites.Where(r => r.RequirementCode == "COREQ2").FirstOrDefault();
                Assert.AreEqual(false, req.IsRequired);
                Assert.AreEqual(RequisiteCompletionOrder.Concurrent, req.CompletionOrder);
                req = section.Requisites.Where(r => r.RequirementCode == "REQ1").FirstOrDefault();
                Assert.AreEqual(true, req.IsRequired);
                Assert.AreEqual(RequisiteCompletionOrder.PreviousOrConcurrent, req.CompletionOrder);
            }

            [TestMethod]
            public async Task SectionRepository_IgnoresAcadReqmtReqsWhenOverrideIsFalse()
            {
                // This section set up with a requisite with a requirement code and override set to false. Verify requisite is ignored
                List<string> courseIds = new List<string>() { "7706" };
                var section = (await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms)).Where(s => s.Number == "05").First();
                var req = section.Requisites.Where(r => !string.IsNullOrEmpty(r.RequirementCode)).FirstOrDefault();
                Assert.IsNull(req);
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionCached_Books()
            {
                List<string> courseIds = new List<string>() { "7702" };
                var section = (await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms)).First();
                Assert.AreEqual(2, section.Books.Count());
                Assert.AreEqual("111", section.Books.ElementAt(0).BookId);
                Assert.AreEqual("222", section.Books.ElementAt(1).BookId);
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionsCached_CourseTypes()
            {
                List<string> courseIds = new List<string>() { "42" };
                IEnumerable<Section> sections = await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms);
                var section = sections.ElementAt(0);
                Assert.AreEqual("STND", section.CourseTypeCodes.ElementAt(0));
                Assert.AreEqual("WR", section.CourseTypeCodes.ElementAt(1));
            }

            [TestMethod]
            public async Task SectionRepository_GetRegistrationSections_ActiveStudents()
            {
                IEnumerable<Section> sections = await sectionRepo.GetRegistrationSectionsAsync(null);
                foreach (Section section in sections)
                {
                    if (Int16.Parse(section.Id) % 2 == 0)
                    {
                        Assert.AreEqual(section.ActiveStudentIds.Count(), 1);
                    }
                    else
                    {
                        Assert.AreEqual(section.ActiveStudentIds.Count(), 0);
                    }
                }
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionCached_NoPtlSite()
            {
                List<string> courseIds = new List<string>() { "7272" };
                Section section = (await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms)).Where(s => s.Number == "99").First();
                Assert.IsNull(section.LearningProviderSiteId);
                Assert.IsNull(section.LearningProvider);
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionCached_PtlSiteNoLearningProvider()
            {
                List<string> courseIds = new List<string>() { "7272" };
                Section section = (await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms)).Where(s => s.Number == "98").First();
                Assert.AreEqual("SHAREPOINT", section.LearningProvider);
                Assert.AreEqual(section.Id, section.LearningProviderSiteId);
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionCached_PtlSiteWithLearningProvider()
            {
                List<string> courseIds = new List<string>() { "7272" };
                Section section = (await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms)).Where(s => s.Number == "97").First();
                Assert.AreEqual("MOODLE", section.LearningProvider);
                Assert.AreEqual(section.Id, section.LearningProviderSiteId);
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionCached_NoXlist()
            {
                List<string> courseIds = new List<string>() { "7702" };
                Section section = (await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms)).First();
                Assert.IsNull(section.PrimarySectionId);
                Assert.AreEqual(0, section.CrossListedSections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionCached_PrimaryInXlist()
            {
                List<string> courseIds = new List<string>() { "7272" };
                Section section = (await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms)).Where(s => s.Number == "98").First();
                Assert.AreEqual(section.PrimarySectionId, section.Id);
                Assert.AreEqual(2, section.CrossListedSections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_HandlesMissingINTERNATIONALParam()
            {
                // Should return sections without error.
                var param = new Data.Base.DataContracts.IntlParams();
                param = null;
                dataReaderMock.Setup<Task<Data.Base.DataContracts.IntlParams>>(iacc => iacc.ReadRecordAsync<Data.Base.DataContracts.IntlParams>("INTL.PARAMS", "INTERNATIONAL", true)).ReturnsAsync(param);
                List<string> courseIds = new List<string>() { "42" };
                IEnumerable<Section> sections = await sectionRepo.GetCourseSectionsCachedAsync(courseIds, registrationTerms);
                Assert.AreEqual(9, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetCachedSections_SectionIdsNull()
            {
                IEnumerable<Section> sections = await sectionRepo.GetCachedSectionsAsync(null);
                Assert.AreEqual(0, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetCachedSections_SectionIdsZero()
            {
                IEnumerable<string> sectionIds = new List<string>();
                IEnumerable<Section> sections = await sectionRepo.GetCachedSectionsAsync(sectionIds);
                Assert.AreEqual(0, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetCachedSections_Successful()
            {
                // Set up local cache mock to respond to cache request:
                //  -to "Contains" request, return "true" to indicate item is in cache
                //  -to "Get" request, return the cache item (in this case the "AllRegistrationSections" cache item)
                string cacheKey = sectionRepo.BuildFullCacheKey("AllRegistrationSections");
                cacheProviderMock.Setup(x => x.Contains(cacheKey, null)).Returns(true);
                cacheProviderMock.Setup(x => x.Get(cacheKey, null)).Returns(regSectionsDict).Verifiable();

                // return null for request, so that if we have a result, it wasn't the data accessor that returned it.
                dataReaderMock.Setup(acc => acc.SelectAsync("COURSE.SECTIONS", "")).Returns(Task.FromResult(new string[] { "1", "2" }));
                dataReaderMock.Setup<Task<Collection<CourseSections>>>(acc => acc.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string>(), true)).Returns(Task.FromResult<Collection<CourseSections>>(new Collection<CourseSections>()));

                // Assert sections are returned
                IEnumerable<string> sectionIds = new List<string> { "1", "2" };
                var sections = await sectionRepo.GetCachedSectionsAsync(sectionIds);
                Assert.AreEqual(2, sections.Count());
                // Verify that Get was called to get the courses from cache
                cacheProviderMock.Verify(m => m.Get(cacheKey, null));
            }

            [TestMethod]
            public async Task SectionRepository_GetCachedSections_GetsSectionFromArchivedSectionCache()
            {
                // Set up local cache mock to respond to cache request:
                //  -to "Contains" request, return "true" to indicate item is in cache
                //  -to "Get" request, return the cache item from the archive cache
                var recordKey = "99999";
                string cacheKey = "ArchivedSection" + recordKey;
                string fullCacheKey = sectionRepo.BuildFullCacheKey(cacheKey);
                cacheProviderMock.Setup(x => x.Contains(fullCacheKey, null)).Returns(true);
                var sec = new Section(recordKey, "1", "01", new DateTime(2012, 1, 1), 3m, 0m, "course1", "IN", new List<OfferingDepartment>() { new OfferingDepartment("HIST", 100m) }, new List<string>() { "100" }, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddDays(-60)) });
                cacheProviderMock.Setup(x => x.Get(fullCacheKey, null)).Returns(sec).Verifiable();
                cacheProviderMock.Setup(x => x.Add(fullCacheKey, It.IsAny<Object>(), It.IsAny<CacheItemPolicy>(), null)).Verifiable();

                // Act--Execute repo method to get cached section. We know this one is not a registration section.
                IEnumerable<string> sectionIds = new List<string> { recordKey };
                var sections = await sectionRepo.GetCachedSectionsAsync(sectionIds);

                // Assert--the requested section is returned
                Assert.AreEqual(1, sections.Count());
                Assert.AreEqual(recordKey, sections.ElementAt(0).Id);
                // Verify that Get was called with the section ID to get the section from the archive cache
                cacheProviderMock.Verify(x => x.Get(fullCacheKey, null));
                // Verify that the Add was called with the section to update the cache
                cacheProviderMock.Verify(x => x.Add(fullCacheKey, It.IsAny<Object>(), It.IsAny<CacheItemPolicy>(), null));
            }

            [TestMethod]
            public async Task SectionRepository_GetCachedSections_GetsSectionFromDatabaseIfNotInArchivedSectionCache()
            {
                // Set up local cache mock to respond to cache request:
                //  -to "Contains" request, return "false" to indicate item is not in cache
                var recordKey = "99999";
                string fullCacheKey = sectionRepo.BuildFullCacheKey("ArchivedSection" + recordKey);
                cacheProviderMock.Setup(x => x.Contains(fullCacheKey, null)).Returns(false);

                // Mock return of a DataContract CourseSection from bulk read 
                CourseSections cs = sectionsResponseData.Where(s => s.Recordkey == "1").First();
                cs.Recordkey = recordKey; // Insert the recordkey we want
                dataReaderMock.Setup<Task<Collection<CourseSections>>>(acc => acc.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(new Collection<CourseSections>() { cs }));

                // Set up mocking to verify the section was written to the archive cache
                cacheProviderMock.Setup(x => x.Add(fullCacheKey, It.IsAny<Section>(), It.IsAny<CacheItemPolicy>(), null)).Verifiable();

                // Act--Execute repo method to get cached section. We know this one is not a registration section.
                IEnumerable<string> sectionIds = new List<string> { recordKey };
                var sections = await sectionRepo.GetCachedSectionsAsync(sectionIds);

                // Assert--the requested section is returned
                Assert.AreEqual(1, sections.Count());
                Assert.AreEqual(recordKey, sections.ElementAt(0).Id);

                // Verify that the section was also added to the cache
                cacheProviderMock.Verify(m => m.Add(fullCacheKey, It.IsAny<Section>(), It.IsAny<CacheItemPolicy>(), null));
            }

            // TODO: SSS This test broken by Async changes, needs to be resolved
            [TestMethod]
            public async Task SectionRepository_GetCachedSections_GetsSectionFromDatabase_OnlyOnline()
            {
                // Set up local cache mock to respond to cache request:
                //  -to "Contains" request, return "false" to indicate item is not in cache
                var recordKey = "99999";
                string fullCacheKey = sectionRepo.BuildFullCacheKey("ArchivedSection" + recordKey);
                cacheProviderMock.Setup(x => x.Contains(fullCacheKey, null)).Returns(false);

                // Mock return of a DataContract CourseSection from bulk read 
                CourseSections cs = sectionsResponseData.Where(s => s.Recordkey == "1").First();
                cs.Recordkey = recordKey; // Insert the recordkey we want
                dataReaderMock.Setup<Task<Collection<CourseSections>>>(acc => acc.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(new Collection<CourseSections>() { cs }));

                // Mock up appropriate SectionMeetings
                Collection<CourseSecMeeting> courseSecMeetings = new Collection<CourseSecMeeting>();
                courseSecMeetings.Add(new CourseSecMeeting() { Recordkey = "1", CsmInstrMethod = "ONL", CsmFriday = "Y", CsmCourseSection = recordKey, CsmFrequency = "W", CsmStartDate = new DateTime(2012, 9, 1), CsmEndDate = new DateTime(2012, 12, 12) });
                dataReaderMock.Setup<Task<Collection<CourseSecMeeting>>>(acc => acc.BulkReadRecordAsync<CourseSecMeeting>("COURSE.SEC.MEETING", It.IsAny<string[]>(), true)).Returns(Task.FromResult(courseSecMeetings));


                // Set up mocking to verify the section was written to the archive cache
                cacheProviderMock.Setup(x => x.Add(fullCacheKey, It.IsAny<Section>(), It.IsAny<CacheItemPolicy>(), null)).Verifiable();

                // Act--Execute repo method to get cached section. We know this one is not a registration section.
                IEnumerable<string> sectionIds = new List<string> { recordKey };
                var sections = await sectionRepo.GetCachedSectionsAsync(sectionIds);

                // Assert--the requested section is online
                Assert.AreEqual(1, sections.Count());
                var section = sections.ElementAt(0);
                Assert.AreEqual(OnlineCategory.Online, section.OnlineCategory);
                Assert.IsTrue(section.Meetings[0].IsOnline);
            }

            [TestMethod]
            public async Task SectionRepository_GetRegistrationSections_NullFacultyDataResponse_SectionsReturned()
            {
                // Mock null faculty repo response
                sectionFacultyResponseData = null;
                dataReaderMock.Setup<Task<Collection<CourseSecFaculty>>>(facc => facc.BulkReadRecordAsync<CourseSecFaculty>("COURSE.SEC.FACULTY", It.IsAny<string[]>(), true)).Returns(Task.FromResult(sectionFacultyResponseData));
                IEnumerable<Section> sections = await sectionRepo.GetRegistrationSectionsAsync(registrationTerms);
                Section testSection = sections.ElementAt(0);
                Assert.IsNotNull(testSection);
            }

            [TestMethod]
            public async Task SectionRepository_GetRegistrationSections_NullRosterDataResponse_SectionsReturned()
            {
                // Mock null roster response
                studentCourseSecResponseData = null;
                transManagerMock.Setup(mgr => mgr.ExecuteAsync<GetStudentCourseSecStudentsRequest, GetStudentCourseSecStudentsResponse>(It.IsAny<GetStudentCourseSecStudentsRequest>())).ReturnsAsync(studentCourseSecResponseData);
                IEnumerable<Section> sections = await sectionRepo.GetRegistrationSectionsAsync(registrationTerms);
                Section testSection = sections.ElementAt(0);
                Assert.IsNotNull(testSection);
            }

            [TestMethod]
            public async Task SectionRepository_GetRegistrationSections_NullPendingDataResponse_SectionsReturned()
            {
                // Mock null pending data response
                pendingResponseData = null;
                dataReaderMock.Setup<Task<Collection<CourseSecPending>>>(csp => csp.BulkReadRecordAsync<CourseSecPending>("COURSE.SEC.PENDING", It.IsAny<string[]>(), true)).Returns(Task.FromResult(pendingResponseData));
                IEnumerable<Section> sections = await sectionRepo.GetRegistrationSectionsAsync(registrationTerms);
                Section testSection = sections.ElementAt(0);
                Assert.IsNotNull(testSection);
            }

            [TestMethod]
            public async Task SectionRepository_GetRegistrationSections_NullWaitlistDataReponse_SectionsReturned()
            {
                // Mock null waitlist data response
                waitlistResponseData = null;
                dataReaderMock.Setup<Task<Collection<WaitList>>>(wl => wl.BulkReadRecordAsync<WaitList>("WAIT.LIST", It.IsAny<string>(), true)).Returns(Task.FromResult(waitlistResponseData));
                IEnumerable<Section> sections = await sectionRepo.GetRegistrationSectionsAsync(registrationTerms);
                Section testSection = sections.ElementAt(0);
                Assert.IsNotNull(testSection);
            }

            [TestMethod]
            public async Task SectionRepository_GetRegistrationSections_NullPortalDataResponse_SectionsReturned()
            {
                // Mock null portal site response
                portalSitesResponseData = null;
                dataReaderMock.Setup<Task<Collection<PortalSites>>>(ps => ps.BulkReadRecordAsync<PortalSites>("PORTAL.SITES", It.IsAny<string[]>(), true)).Returns(Task.FromResult(portalSitesResponseData));
                IEnumerable<Section> sections = await sectionRepo.GetRegistrationSectionsAsync(registrationTerms);
                Section testSection = sections.ElementAt(0);
                Assert.IsNotNull(testSection);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionRepository_GetSectionMeetingAsync_NullException()
            {
                await sectionRepo.GetSectionMeetingAsync("");          
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionRepository_GetSectionMeetingAsync_KeyNotFoundException()
            {
                await sectionRepo.GetSectionMeetingAsync("dsdsa");
            }

            [TestMethod]
            public async Task GetCachedSections_GetsChangedRegistrationSections()
            {
                // Get the list of registration sections from cache
                var allCacheKey = sectionRepo.BuildFullCacheKey("AllRegistrationSections");
                var cacheDateKey = sectionRepo.BuildFullCacheKey("AllRegistrationSectionsCacheDate");
                // Create a small dict to be returned as "AllRegistrationSections".
                var sectionsDict = new Dictionary<string, Section>();
                sectionsDict["2"] = regSections.Where(s => s.Id == "2").First();
                sectionsDict["3"] = regSections.Where(s => s.Id == "3").First();
                sectionsDict["4"] = regSections.Where(s => s.Id == "4").First();
                var testLocation = "SOUTH";
                sectionsDict["4"].Location = testLocation; // Override original name so we can make sure it was overlaid.
                // Set up the response from the all registration sections cache
                cacheProviderMock.Setup(x => x.Contains(allCacheKey, null)).Returns(true);
                cacheProviderMock.Setup(x => x.Get(allCacheKey, null)).Returns(sectionsDict);
                // Set up the response of the cache date
                cacheProviderMock.Setup(x => x.Contains(cacheDateKey, null)).Returns(true);
                cacheProviderMock.Setup(x => x.Get(cacheDateKey, null)).Returns(DateTime.Now);
                // Set up ChangedRegistrationsCache to respond that it does ont exist so it will have to be built.
                string changedCacheKey = sectionRepo.BuildFullCacheKey("ChangedRegistrationSections");
                cacheProviderMock.Setup(x => x.Contains(changedCacheKey, null)).Returns(false);
                // Mock return of a course section Id from the select for changed course sections
                dataReaderMock.Setup(acc => acc.SelectAsync("COURSE.SECTIONS", It.IsAny<string>())).Returns(Task.FromResult(new List<string>() { "4", "9" }.ToArray()));
                // Then noncached sections is called and should take care of getting sections "4" and "9".
                sectionsResponseData = new Collection<CourseSections>(sectionsResponseData.Where(s => s.Recordkey == "4" || s.Recordkey == "9").ToList());
                dataReaderMock.Setup<Task<Collection<CourseSections>>>(acc => acc.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(sectionsResponseData));

                // Get the current DateTime, for use in later Asserts
                var beforeUpdate = DateTime.Now;
                Thread.Sleep(2000);

                // Act--Execute repo method to get registration sections
                var sections = await sectionRepo.GetRegistrationSectionsAsync(registrationTerms);

                // Assert--the original 3 sections, one overlaid (4) and one new one(9).
                Assert.AreEqual(4, sections.Count());
                Assert.IsNotNull(sections.Where(s => s.Id == "2").First());
                Assert.IsNotNull(sections.Where(s => s.Id == "3").First());
                var section4 = sections.Where(s => s.Id == "4").First();
                Assert.IsNotNull(section4);
                Assert.AreEqual("MAIN", section4.Location);
                Assert.IsNotNull(sections.Where(s => s.Id == "9").First());

                // Assert that the DateTime the changedRegistrationCache was updated is more recent than beforeUpdate
                var afterUpdate = sectionRepo.GetChangedRegistrationSectionsCacheBuildTime();
                Assert.IsTrue(beforeUpdate < afterUpdate);
            }

            [TestMethod]
            public async Task SectionRepository_TestGuidLookupsforSectionMeeting_GuidLookupSuccess()
            {
                var result = await sectionRepo.GetSectionMeetingByGuidAsync(guid);
                Assert.AreEqual(id, result.Id);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionRepository_TestGuidLookupsforSectionMeeting_RecordKeyLookupFailure_IdNull()
            {
                var result = await sectionRepo.GetSectionMeetingByGuidAsync(null);
            }

            [TestMethod]
            public async Task SectionRepository_GetSectionMeeting()
            {
                CourseSecMeeting csm = sectionMeetingResponseData.FirstOrDefault();
                CourseSecFaculty csf = sectionFacultyResponseData.FirstOrDefault();
                
                var tuple = await sectionRepo.GetSectionMeetingAsync(10, 10, csm.CsmCourseSection, csm.CsmStartDate.ToString(), csm.CsmEndDate.ToString(), csm.CsmStartTime.ToString(), csm.CsmEndTime.ToString(), csm.CsmBldg, csm.CsmRoom, csf.CsfFaculty);
                IEnumerable<SectionMeeting> secMeets = tuple.Item1;
                Assert.AreEqual(csm.CsmCourseSection, secMeets.FirstOrDefault().SectionId);
            }

            private SectionRepository BuildValidSectionRepository()
            {
               
                BuildLdmConfiguration(dataReaderMock, out cdDefaults);

                // Set up repo response for "all" section requests
                dataReaderMock.Setup(acc => acc.SelectAsync("COURSE.SECTIONS", It.IsAny<string>())).Returns(Task.FromResult(sectionsResponseData.Select(c => c.Recordkey).ToArray()));
                dataReaderMock.Setup<Task<Collection<CourseSections>>>(acc => acc.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(sectionsResponseData));

                // Set up repo response for single section request
                CourseSections cs = sectionsResponseData.Where(s => s.Recordkey == "1").First();
                dataReaderMock.Setup<Task<CourseSections>>(acc => acc.ReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string>(), true)).Returns(Task.FromResult(cs));
                CourseSecXlists csxl = crosslistResponseData.Where(s => s.CsxlCourseSections.Contains("1")).First();
                dataReaderMock.Setup<Task<CourseSecXlists>>(acc => acc.ReadRecordAsync<CourseSecXlists>("COURSE.SEC.XLISTS", It.IsAny<string>(), true)).Returns(Task.FromResult(csxl));

                // Set up repo response for "all" meeting requests
                dataReaderMock.Setup(acc => acc.SelectAsync("COURSE.SEC.MEETING", It.IsAny<string>())).Returns(Task.FromResult(sectionMeetingResponseData.Select(c => c.Recordkey).ToArray()));
                dataReaderMock.Setup<Task<Collection<CourseSecMeeting>>>(macc => macc.BulkReadRecordAsync<CourseSecMeeting>("COURSE.SEC.MEETING", It.IsAny<string[]>(), true)).Returns(Task.FromResult(sectionMeetingResponseData));

                // Set up repo response for "all" faculty
                dataReaderMock.Setup<Task<Collection<CourseSecFaculty>>>(facc => facc.BulkReadRecordAsync<CourseSecFaculty>("COURSE.SEC.FACULTY", It.IsAny<string[]>(), true)).Returns(Task.FromResult(sectionFacultyResponseData));

                transManagerMock.Setup(mgr => mgr.ExecuteAsync<GetStudentCourseSecStudentsRequest, GetStudentCourseSecStudentsResponse>(It.IsAny<GetStudentCourseSecStudentsRequest>())).Returns(Task.FromResult(studentCourseSecResponseData)).Callback<GetStudentCourseSecStudentsRequest>(req => updateRequest = req);
                transManagerMock.Setup(t => t.ExecuteAsync<DeleteInstructionalEventRequest, DeleteInstructionalEventResponse>(It.IsAny<DeleteInstructionalEventRequest>())).Returns(Task.FromResult(response));

                dataReaderMock.Setup<Task<Collection<PortalSites>>>(ps => ps.BulkReadRecordAsync<PortalSites>("PORTAL.SITES", It.IsAny<string[]>(), true)).Returns(Task.FromResult(portalSitesResponseData));
                dataReaderMock.Setup<Task<Collection<CourseSecXlists>>>(sxl => sxl.BulkReadRecordAsync<CourseSecXlists>("COURSE.SEC.XLISTS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(crosslistResponseData));
                dataReaderMock.Setup<Task<Collection<CourseSecPending>>>(csp => csp.BulkReadRecordAsync<CourseSecPending>("COURSE.SEC.PENDING", It.IsAny<string[]>(), true)).Returns(Task.FromResult(pendingResponseData));
                dataReaderMock.Setup<Task<Collection<WaitList>>>(wl => wl.BulkReadRecordAsync<WaitList>("WAIT.LIST", It.IsAny<string>(), true)).Returns(Task.FromResult(waitlistResponseData));

                // Set up repo response for the temporary international parameter item
                Data.Base.DataContracts.IntlParams intlParams = new Data.Base.DataContracts.IntlParams();
                intlParams.HostDateDelimiter = "/";
                intlParams.HostShortDateFormat = "MDY";
                dataReaderMock.Setup<Task<Data.Base.DataContracts.IntlParams>>(iacc => iacc.ReadRecordAsync<Data.Base.DataContracts.IntlParams>("INTL.PARAMS", "INTERNATIONAL", true)).ReturnsAsync(intlParams);
                // Setup localCacheMock as the object for the CacheProvider
                //cacheProviderMock.Setup(provider => provider.GetCache(It.IsAny<string>())).Returns(localCacheMock.Object);

                // Set up course defaults response (indicates if coreq conversion has taken place)
                BuildCourseParametersConvertedResponse(cdDefaults);
                dataReaderMock.Setup<Task<CdDefaults>>(acc => acc.ReadRecordAsync<CdDefaults>("ST.PARMS", "CD.DEFAULTS", true)).ReturnsAsync(cdDefaults);

                // Set up acad reqmts response (for section requisite codes)
                var acadReqmtsResponse = BuildAcadReqmtsResponse();
                dataReaderMock.Setup(acc => acc.BulkReadRecordAsync<AcadReqmts>("ACAD.REQMTS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(acadReqmtsResponse));

                //// Set up response for instructional methods
                //dataAccessorMock.Setup(acc => acc.BulkReadRecordAsync<InstrMethods>("INSTR.METHODS", "", true)).ReturnsAsync(BuildValidInstrMethodResponse());

                // Set up dataAccessorMock as the object for the DataAccessor
                transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(dataReaderMock.Object);

                // Mock data needed to read campus calendar
                var startTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 06, 00, 00);
                var endTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 23, 30, 00);
                dataReaderMock.Setup(r => r.ReadRecordAsync<Dflts>("CORE.PARMS", "DEFAULTS", true))
                    .ReturnsAsync(new Dflts() { DfltsCampusCalendar = "CAL" });
                dataReaderMock.Setup(r => r.ReadRecordAsync<Data.Base.DataContracts.CampusCalendar>("CAL", true))
                    .ReturnsAsync(new Data.Base.DataContracts.CampusCalendar() { Recordkey = "CAL", CmpcDesc = "Calendar", CmpcDayStartTime = startTime, CmpcDayEndTime = endTime, CmpcBookPastNoDays = "30" });
                // Set up repo response for section statuses
                var sectionStatuses = new ApplValcodes();
                sectionStatuses.ValsEntityAssociation = new List<ApplValcodesVals>();
                sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("A", "Active", "1", "A", "", "", ""));
                sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("I", "Inactive", "2", "I", "", "", ""));
                sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("C", "Cancelled", "", "C", "", "", ""));
                dataReaderMock.Setup<Task<ApplValcodes>>(cacc => cacc.ReadRecordAsync<ApplValcodes>("ST.VALCODES", "SECTION.STATUSES", true)).ReturnsAsync(sectionStatuses);

                //setup mocking for CourseSecMeeting
                var crsSecMeet = BuildCourseSecMeetingDefaults();
                dataReaderMock.Setup<Task<CourseSecMeeting>>(dr => dr.ReadRecordAsync<CourseSecMeeting>(It.IsAny<GuidLookup>(), true)).Returns(Task.FromResult(crsSecMeet));

                //setup mocking for Stweb Defaults
                var stWebDflt = BuildStwebDefaults(); ;
                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>("ST.PARMS", It.IsAny<string>(), It.IsAny<bool>())).Returns<string, string, bool>(
                    (param, id, repl) => Task.FromResult((stWebDflt.Recordkey == id) ? stWebDflt : null)
                    );

                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>(It.IsAny<string>(), It.IsAny<bool>())).Returns<string, bool>(
                   (id, repl) => Task.FromResult((stWebDflt.Recordkey == id) ? stWebDflt : null)
                   );
                MockRecordsAsync<InstrMethods>("INSTR.METHODS", BuildValidInstrMethodResponse());
                // Construct section repository
                sectionRepo = new SectionRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettings);

                return sectionRepo;
            }

            private SectionRepository BuildInvalidSectionRepository()
            {
                var transFactoryMock = new Mock<IColleagueTransactionFactory>();
                apiSettings = new ApiSettings("null");

                // Set up data accessor for mocking 
                var dataAccessorMock = new Mock<IColleagueDataReader>();
                transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(dataAccessorMock.Object);

                // Set up repo response for "all" section requests
                Exception expectedFailure = new Exception("fail");
                dataAccessorMock.Setup<Task<Collection<CourseSections>>>(acc => acc.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string[]>(), true)).Throws(expectedFailure);

                // Cache Mock
                var localCacheMock = new Mock<ObjectCache>();
                // Cache Provider Mock
                var cacheProviderMock = new Mock<ICacheProvider>();
                //cacheProviderMock.Setup(provider => provider.GetCache(It.IsAny<string>())).Returns(localCacheMock.Object);
                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
                  x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                  .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));
                // Construct section repository
                sectionRepo = new SectionRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettings);

                return sectionRepo;
            }

            private Collection<CourseSections> BuildSectionsResponse(IEnumerable<Section> sections)
            {
                Collection<CourseSections> repoSections = new Collection<CourseSections>();
                foreach (var section in sections)
                {
                    var crsSec = new CourseSections();
                    crsSec.RecordGuid = section.Guid;
                    crsSec.Recordkey = section.Id.ToString();
                    crsSec.SecAcadLevel = section.AcademicLevelCode;
                    crsSec.SecCeus = section.Ceus;
                    crsSec.SecCourse = section.CourseId.ToString();
                    crsSec.SecCourseLevels = section.CourseLevelCodes.ToList();
                    crsSec.SecDepts = section.Departments.Select(x => x.AcademicDepartmentCode).ToList();
                    crsSec.SecLocation = section.Location;
                    crsSec.SecMaxCred = section.MaximumCredits;
                    crsSec.SecVarCredIncrement = section.VariableCreditIncrement;
                    crsSec.SecMinCred = section.MinimumCredits;
                    crsSec.SecNo = section.Number;
                    crsSec.SecShortTitle = section.Title;
                    crsSec.SecDepartmentsEntityAssociation = section.Departments.Select(x => new CourseSectionsSecDepartments(x.AcademicDepartmentCode, x.ResponsibilityPercentage)).ToList();
                    crsSec.SecCredType = section.CreditTypeCode;
                    crsSec.SecStatusesEntityAssociation = section.Statuses.Select(x => new CourseSectionsSecStatuses(x.Date, ConvertSectionStatusToCode(x.Status))).ToList();
                    crsSec.SecStartDate = section.StartDate;
                    crsSec.SecEndDate = section.EndDate;
                    crsSec.SecTerm = section.TermId;
                    crsSec.SecOnlyPassNopassFlag = section.OnlyPassNoPass == true ? "Y" : "N";
                    crsSec.SecAllowPassNopassFlag = section.AllowPassNoPass == true ? "Y" : "N";
                    crsSec.SecAllowAuditFlag = section.AllowAudit == true ? "Y" : "N";
                    var csm = new List<string>() { "1", "2" };
                    crsSec.SecMeeting = csm;
                    var csf = new List<string>() { "1", "2" };
                    crsSec.SecFaculty = csf;
                    var sas = new List<string>() { "1", "2" };
                    crsSec.SecActiveStudents = sas;
                    crsSec.SecCourseTypes = section.CourseTypeCodes.ToList();
                    crsSec.SecAllowWaitlistFlag = section.AllowWaitlist == true ? "Y" : "N";
                    crsSec.SecFacultyConsentFlag = section.IsInstructorConsentRequired ? "Y" : "N";
                    crsSec.SecName = section.Name;
                    crsSec.SecNoWeeks = section.NumberOfWeeks;
                    crsSec.SecWaitlistRating = section.WaitlistRatingCode;
                    crsSec.SecHideInCatalog = section.HideInCatalog ? "Y" : string.Empty;

                    // Reconstruct all Colleague requisite and corequisite fields from the data in the Requisites 
                    // post-conversion fields
                    crsSec.SecReqs = new List<string>();
                    crsSec.SecRecommendedSecs = new List<string>();
                    crsSec.SecCoreqSecs = new List<string>();
                    crsSec.SecMinNoCoreqSecs = null;
                    crsSec.SecOverrideCrsReqsFlag = section.OverridesCourseRequisites == true ? "Y" : "";
                    // pre-conversion fields
                    crsSec.SecCourseCoreqsEntityAssociation = new List<CourseSectionsSecCourseCoreqs>();
                    crsSec.SecCoreqsEntityAssociation = new List<CourseSectionsSecCoreqs>();

                    foreach (var req in section.Requisites)
                    {
                        if (!string.IsNullOrEmpty(req.RequirementCode))
                        {
                            // post-conversion
                            crsSec.SecReqs.Add(req.RequirementCode);
                            // pre-conversion -- this does not convert
                        }
                        else if (!string.IsNullOrEmpty(req.CorequisiteCourseId))
                        {
                            // post-conversion -- this does not convert
                            // pre-conversion
                            crsSec.SecCourseCoreqsEntityAssociation.Add(
                                new CourseSectionsSecCourseCoreqs(req.CorequisiteCourseId, (req.IsRequired == true) ? "Y" : "")
                            );
                        }

                    }

                    foreach (var req in section.SectionRequisites)
                    {
                        if (req.CorequisiteSectionIds != null && req.CorequisiteSectionIds.Count() > 1)
                        {
                            // these are all required
                            foreach (var secId in req.CorequisiteSectionIds)
                            {
                                // postconversion
                                crsSec.SecCoreqSecs.Add(secId);
                                // pre-conversion. number needed does not convert
                                crsSec.SecCoreqsEntityAssociation.Add(
                                    new CourseSectionsSecCoreqs(secId, "Y")
                                    );
                            }
                            // The minimum number is associated with the entire list of SecCoreqSecs
                            // postconversion
                            var numNeeded = (crsSec.SecMinNoCoreqSecs.HasValue && crsSec.SecMinNoCoreqSecs > 0 ? crsSec.SecMinNoCoreqSecs : 0);
                            numNeeded += req.NumberNeeded;
                            crsSec.SecMinNoCoreqSecs = numNeeded;
                        }
                        else
                        {
                            if (req.IsRequired)
                            {
                                // pre-conversion--each requisite section has a required flag
                                crsSec.SecCoreqsEntityAssociation.Add(
                                    new CourseSectionsSecCoreqs(req.CorequisiteSectionIds.ElementAt(0), "Y")
                                    );
                                // post-conversion--all required sections--add to list and increment counter
                                crsSec.SecCoreqSecs.Add(req.CorequisiteSectionIds.ElementAt(0));
                                var numNeeded = crsSec.SecMinNoCoreqSecs.HasValue && crsSec.SecMinNoCoreqSecs > 0 ? crsSec.SecMinNoCoreqSecs : 0;
                                numNeeded += 1;
                                crsSec.SecMinNoCoreqSecs = numNeeded;
                            }
                            else
                            {
                                // pre-conversion
                                crsSec.SecCoreqsEntityAssociation.Add(
                                    new CourseSectionsSecCoreqs(req.CorequisiteSectionIds.ElementAt(0), "N")
                                    );
                                // post-conversion
                                crsSec.SecRecommendedSecs.Add(req.CorequisiteSectionIds.ElementAt(0));
                            }
                        }
                    }

                    if (section.Books != null)
                    {
                        crsSec.SecBooks = new List<string>();
                        crsSec.SecBookOptions = new List<string>();
                        foreach (var book in section.Books)
                        {
                            crsSec.SecBooks.Add(book.BookId);
                            if (book.IsRequired == true)
                            {
                                crsSec.SecBookOptions.Add("R");
                            }
                        }
                    }
                    crsSec.SecPortalSite = ""; // (!string.IsNullOrEmpty(section.LearningProvider) ? crsSec.Recordkey : "");
                    if (!string.IsNullOrEmpty(section.LearningProvider))
                    {
                        crsSec.SecPortalSite = section.Id;
                    }
                    else
                    {
                        crsSec.SecPortalSite = "";
                    }
                    crsSec.SecXlist = ""; // (!string.IsNullOrEmpty(section.PrimarySectionId) && section.PrimarySectionId.Equals(section.Id)) ? "" : crsSec.Recordkey;
                    if (!string.IsNullOrEmpty(section.PrimarySectionId))
                    {
                        crsSec.SecXlist = section.Id;
                    }
                    else
                    {
                        crsSec.SecXlist = "";
                    }

                    repoSections.Add(crsSec);
                }
                return repoSections;
            }

            private Collection<CourseSecMeeting> BuildSectionMeetingsResponse(IEnumerable<Section> sections)
            {
                Collection<CourseSecMeeting> repoSecMeetings = new Collection<CourseSecMeeting>();
                int crsSecMId = 0;
                foreach (var section in sections)
                {
                    foreach (var mt in section.Meetings)
                    {
                        var crsSecM = new CourseSecMeeting();
                        crsSecMId += 1;
                        crsSecM.Recordkey = crsSecMId.ToString();
                        if (!string.IsNullOrEmpty(mt.Room))
                        {
                            crsSecM.CsmBldg = "ABLE";
                            crsSecM.CsmRoom = "A100";
                        }
                        crsSecM.CsmCourseSection = section.Id;
                        crsSecM.CsmInstrMethod = mt.InstructionalMethodCode;
                        crsSecM.CsmStartTime = mt.StartTime.HasValue ? mt.StartTime.Value.DateTime : (DateTime?)null;
                        crsSecM.CsmEndTime = mt.EndTime.HasValue ? mt.EndTime.Value.DateTime : (DateTime?)null;
                        crsSecM.CsmStartDate = mt.StartDate;
                        crsSecM.CsmEndDate = mt.EndDate;
                        crsSecM.CsmFrequency = mt.Frequency;
                        foreach (var d in mt.Days)
                        {
                            switch (d)
                            {
                                case DayOfWeek.Friday:
                                    crsSecM.CsmFriday = "Y";
                                    break;
                                case DayOfWeek.Monday:
                                    crsSecM.CsmMonday = "Y";
                                    break;
                                case DayOfWeek.Saturday:
                                    crsSecM.CsmSaturday = "Y";
                                    break;
                                case DayOfWeek.Sunday:
                                    crsSecM.CsmSunday = "Y";
                                    break;
                                case DayOfWeek.Thursday:
                                    crsSecM.CsmThursday = "Y";
                                    break;
                                case DayOfWeek.Tuesday:
                                    crsSecM.CsmTuesday = "Y";
                                    break;
                                case DayOfWeek.Wednesday:
                                    crsSecM.CsmWednesday = "Y";
                                    break;
                                default:
                                    break;
                            }
                        }
                        repoSecMeetings.Add(crsSecM);
                    }

                }
                return repoSecMeetings;
            }

            private Collection<CourseSecFaculty> BuildSectionFacultyResponse(IEnumerable<Section> sections)
            {
                Collection<CourseSecFaculty> repoSecFaculty = new Collection<CourseSecFaculty>();
                int crsSecFId = 0;
                foreach (var section in sections)
                {
                    foreach (var fac in section.FacultyIds)
                    {
                        var crsSecF = new CourseSecFaculty();
                        crsSecFId += 1;
                        crsSecF.Recordkey = crsSecFId.ToString();
                        crsSecF.CsfCourseSection = section.Id;
                        crsSecF.CsfFaculty = fac;
                        repoSecFaculty.Add(crsSecF);
                    }

                }
                return repoSecFaculty;
            }

            private GetStudentCourseSecStudentsResponse BuildStudentCourseSecResponse(IEnumerable<Section> sections)
            {
                GetStudentCourseSecStudentsResponse scssr = new GetStudentCourseSecStudentsResponse();
                scssr.StudentCourseSectionStudents = new List<StudentCourseSectionStudents>();
                Collection<StudentCourseSec> repoSCS = new Collection<StudentCourseSec>();
                foreach (var section in sections)
                {
                    foreach (var stu in section.ActiveStudentIds)
                    {
                        StudentCourseSectionStudents scss = new StudentCourseSectionStudents();
                        scss.CourseSectionIds = section.Id;
                        scss.StudentIds = stu;
                        scssr.StudentCourseSectionStudents.Add(scss);
                    }
                }
                return scssr;
            }

            private Collection<PortalSites> BuildPortalSitesResponse(IEnumerable<Section> sections)
            {
                Collection<PortalSites> repoPS = new Collection<PortalSites>();
                foreach (var section in sections)
                {
                    if (section.CourseId == "7272")
                    {
                        var ps = new PortalSites();
                        // normally some thing like "HIST-190-001-cs11347", but mock portal site Id with section ID
                        ps.Recordkey = section.Id;
                        if (section.Number == "98")
                        {
                            ps.PsLearningProvider = "";
                            ps.PsPrtlSiteGuid = section.Id;
                        }
                        if (section.Number == "97")
                        {
                            ps.PsLearningProvider = "MOODLE";
                            ps.PsPrtlSiteGuid = section.Id;
                        }
                        repoPS.Add(ps);
                    }
                }
                return repoPS;
            }

            private Collection<CourseSecXlists> BuildCrosslistResponse(IEnumerable<Section> sections)
            {
                // currently built only for ILP testing
                Collection<CourseSecXlists> repoXL = new Collection<CourseSecXlists>();
                foreach (var section in sections)
                {
                    if (section.CourseId == "7272" && (section.Number == "98" || section.Number == "97"))
                    {
                        var xl = new CourseSecXlists();
                        xl.Recordkey = section.Id;
                        if (section.Number == "98")
                        {
                            xl.CsxlPrimarySection = section.Id;
                            xl.CsxlCourseSections = new List<string>() { section.Id, "1", "5" };
                            xl.CsxlCapacity = 20;
                        }
                        if (section.Number == "97")
                        {
                            xl.CsxlPrimarySection = section.Id;
                            xl.CsxlCourseSections = new List<string>() { "2", section.Id, "6" };
                            xl.CsxlCapacity = 100;
                        }
                        repoXL.Add(xl);
                    }
                }
                return repoXL;
            }

            private Collection<CourseSecPending> BuildPendingSectionResponse(IEnumerable<Section> sections)
            {
                Collection<CourseSecPending> repoPending = new Collection<CourseSecPending>();
                foreach (var section in sections)
                {
                    if (section.CourseId == "7272" && (section.Number == "98" || section.Number == "97"))
                    {
                        var cp = new CourseSecPending();
                        cp.Recordkey = section.Id;
                        if (section.Number == "98")
                        {
                            cp.CspReservedSeats = 1;
                        }
                        if (section.Number == "97")
                        {
                            cp.CspReservedSeats = 2;
                        }
                        repoPending.Add(cp);
                    }
                }
                return repoPending;
            }

            private Collection<WaitList> BuildWaitlistResponse(IEnumerable<Section> sections)
            {
                Collection<WaitList> repoWaitlist = new Collection<WaitList>();
                foreach (var section in sections)
                {
                    if (section.CourseId == "7272" && (section.Number == "98" || section.Number == "97"))
                    {
                        var wl = new WaitList();
                        wl.Recordkey = section.Id;
                        if (section.Number == "98")
                        {
                            wl.WaitCourseSection = section.Id;
                            wl.WaitStatus = "P";
                            wl.WaitStudent = "111111";
                        }
                        if (section.Number == "97")
                        {
                            wl.WaitCourseSection = section.Id;
                            wl.WaitStatus = "P";
                            wl.WaitStudent = "22222";
                        }
                        repoWaitlist.Add(wl);
                    }
                }
                return repoWaitlist;
            }

            private Collection<AcadReqmts> BuildAcadReqmtsResponse()
            {
                Collection<AcadReqmts> acadReqmtsResponse = new Collection<AcadReqmts>();
                // Previous, Required
                var acadReqmts1 = new AcadReqmts() { Recordkey = "PREREQ1", AcrReqsTiming = "P", AcrReqsEnforcement = "RQ" };
                acadReqmtsResponse.Add(acadReqmts1);
                // Previous, Recommended
                var acadReqmts2 = new AcadReqmts() { Recordkey = "PREREQ2", AcrReqsTiming = "P", AcrReqsEnforcement = "RM" };
                acadReqmtsResponse.Add(acadReqmts2);
                // Concurrent, Required
                var acadReqmts3 = new AcadReqmts() { Recordkey = "COREQ1", AcrReqsTiming = "C", AcrReqsEnforcement = "RQ" };
                acadReqmtsResponse.Add(acadReqmts3);
                // Concurrent, Recommended
                var acadReqmts4 = new AcadReqmts() { Recordkey = "COREQ2", AcrReqsTiming = "C", AcrReqsEnforcement = "RM" };
                acadReqmtsResponse.Add(acadReqmts4);
                //  Previous or Concurrent, Required
                var acadReqmts5 = new AcadReqmts() { Recordkey = "REQ1", AcrReqsTiming = "E", AcrReqsEnforcement = "RQ" };
                acadReqmtsResponse.Add(acadReqmts5);
                return acadReqmtsResponse;
            }

            private CdDefaults BuildCourseParametersConvertedResponse(CdDefaults defaults)
            {
                if (defaults == null)
                    defaults = BuildCdDefaults();
                // Converted Response
                defaults.CdReqsConvertedFlag = "Y";
                return defaults;
            }

            private CdDefaults BuildCourseParametersUnConvertedResponse(CdDefaults defaults)
            {
                if (defaults == null)
                    defaults = BuildCdDefaults();
                defaults.CdReqsConvertedFlag = "";
                return defaults;
            }

        }

        [TestClass]
        public class SectionRepository_GetNonCachedSectionsTests : SectionRepositoryTests
        {
            // Broke out these tests so I could better control the subset of records and the data.
            Collection<CourseSections> sectionsResponseData;
            Collection<CourseSecMeeting> sectionMeetingResponseData;
            Collection<CourseSecFaculty> sectionFacultyResponseData;
            GetStudentCourseSecStudentsResponse studentCourseSecResponseData;
            Collection<PortalSites> portalSitesResponseData;
            Collection<CourseSecXlists> crosslistResponseData;
            Collection<CourseSecPending> pendingResponseData;
            Collection<WaitList> waitlistResponseData;
            IEnumerable<Section> reqSections;
            List<Term> registrationTerms = new List<Term>();
            GetStudentCourseSecStudentsRequest updateRequest;
            CdDefaults cdDefaults;

            SectionRepository sectionRepo;

            [TestInitialize]
            public async void Initialize()
            {
                base.MainInitialize();
                // Build Section responses used for mocking
                IEnumerable<string> sectionIdsRequested = new List<string>() { "1", "2", "3", "5" };
                reqSections = await new TestSectionRepository().GetNonCachedSectionsAsync(sectionIdsRequested);
                Term term1 = new Term("2012/FA", "Fall 2012", new DateTime(2012, 9, 1), new DateTime(2012, 12, 15), 2012, 1, true, true, "2012/FA", true);
                Term term2 = new Term("2013/SP", "Spring 2013", new DateTime(2013, 1, 1), new DateTime(2013, 5, 15), 2012, 2, true, true, "2013/SP", true);
                registrationTerms.Add(term1);
                registrationTerms.Add(term2);
                sectionsResponseData = BuildSectionsResponse(reqSections);
                sectionMeetingResponseData = BuildSectionMeetingsResponse(reqSections);
                sectionFacultyResponseData = BuildSectionFacultyResponse(reqSections);
                studentCourseSecResponseData = BuildStudentCourseSecResponse(reqSections);
                portalSitesResponseData = BuildPortalSitesResponse(reqSections);
                crosslistResponseData = BuildCrosslistResponse(reqSections);
                pendingResponseData = BuildPendingSectionResponse(reqSections);
                waitlistResponseData = BuildWaitlistResponse(reqSections);

                // Build section repository
                sectionRepo = BuildValidSectionRepository();

                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
                   x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                   .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

            }

            [TestCleanup]
            public void Cleanup()
            {
                transFactoryMock = null;
                dataReaderMock = null;
                cacheProviderMock = null;
                sectionsResponseData = null;
                sectionRepo = null;
            }

            [TestMethod]
            public async Task SectionRepository_GetNonCachedSections_NullSectionIds()
            {
                IEnumerable<Section> sections = await sectionRepo.GetNonCachedSectionsAsync(null);
                Assert.AreEqual(0, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetNonCachedSections_NoSectionsFound()
            {
                IEnumerable<string> sectionIds = new List<string>() { "99999", "JUNK" };
                IEnumerable<Section> sections = await sectionRepo.GetNonCachedSectionsAsync(sectionIds);
                Assert.AreEqual(0, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetNonCachedSections_ZeroSectionIds()
            {
                IEnumerable<string> sectionIds = new List<string>();
                IEnumerable<Section> sections = await sectionRepo.GetNonCachedSectionsAsync(sectionIds);
                Assert.AreEqual(0, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetNonCachedSections_SectionsReturned()
            {
                List<string> sectionIds = new List<string>() { "1", "2", "3" };
                IEnumerable<Section> sections = await sectionRepo.GetNonCachedSectionsAsync(sectionIds);
                Assert.AreEqual(3, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetNonCachedSections_TestProperties()
            {
                List<string> sectionIds = new List<string>() { "1", "2", "3" };
                IEnumerable<Section> sections = await sectionRepo.GetNonCachedSectionsAsync(sectionIds);
                Section section = sections.Where(s => s.Id == "1").FirstOrDefault();
                Assert.AreEqual("1", section.Id);
                Assert.AreEqual("2012/FA", section.TermId);
                Assert.IsTrue(section.IsActive);
                Assert.IsTrue(section.AllowPassNoPass);
                Assert.IsTrue(section.AllowAudit);
                Assert.IsFalse(section.OnlyPassNoPass);
                Assert.IsFalse(section.AllowWaitlist);
                Assert.AreEqual(20, section.Capacity);
            }

            // TODO: SSS This test broken by Async changes, needs to be resolved
            [TestMethod]
            public async Task SectionRepository_GetNonCachedSections_OnlineIndicators()
            {
                // Section 1 will be Online entirely
                // Section 2 will be Hybrid
                // Section 3 will not be online line (one non online meeting)
                // Section 5 has no meetings - not online

                List<string> sectionIds = new List<string>() { "1", "2", "3", "5" };

                // Set up the section meetings we want to go with these sections.
                Collection<CourseSecMeeting> sectionMeetings = new Collection<CourseSecMeeting>();
                sectionMeetings.Add(new CourseSecMeeting() { Recordkey = "11", RecordGuid = Guid.NewGuid().ToString(), CsmCourseSection = "1", CsmInstrMethod = "ONL", CsmFriday = "Y", CsmStartDate = new DateTime(2012, 9, 11), CsmEndDate = new DateTime(2012, 12, 12), CsmFrequency = "W" });
                sectionMeetings.Add(new CourseSecMeeting() { Recordkey = "22", RecordGuid = Guid.NewGuid().ToString(), CsmCourseSection = "2", CsmInstrMethod = "LEC", CsmFriday = "Y", CsmStartDate = new DateTime(2012, 9, 11), CsmEndDate = new DateTime(2012, 12, 12), CsmFrequency = "W" });
                sectionMeetings.Add(new CourseSecMeeting() { Recordkey = "23", RecordGuid = Guid.NewGuid().ToString(), CsmCourseSection = "2", CsmInstrMethod = "ONL", CsmFriday = "Y", CsmStartDate = new DateTime(2012, 9, 11), CsmEndDate = new DateTime(2012, 12, 12), CsmFrequency = "W" });
                sectionMeetings.Add(new CourseSecMeeting() { Recordkey = "33", RecordGuid = Guid.NewGuid().ToString(), CsmCourseSection = "3", CsmInstrMethod = "LEC", CsmFriday = "Y", CsmStartDate = new DateTime(2012, 9, 11), CsmEndDate = new DateTime(2012, 12, 12), CsmFrequency = "W" });
                dataReaderMock.Setup<Task<Collection<CourseSecMeeting>>>(macc => macc.BulkReadRecordAsync<CourseSecMeeting>("COURSE.SEC.MEETING", It.IsAny<string[]>(), true)).Returns(Task.FromResult(sectionMeetings));
                IEnumerable<Section> sections = await sectionRepo.GetNonCachedSectionsAsync(sectionIds);
                Section section1 = sections.Where(s => s.Id == "1").FirstOrDefault();
                Assert.AreEqual(OnlineCategory.Online, section1.OnlineCategory);
                Assert.IsTrue(section1.Meetings.ElementAt(0).IsOnline);
                Section section2 = sections.Where(s => s.Id == "2").FirstOrDefault();
                Assert.AreEqual(OnlineCategory.Hybrid, section2.OnlineCategory);
                Section section3 = sections.Where(s => s.Id == "3").FirstOrDefault();
                Assert.AreEqual(OnlineCategory.NotOnline, section3.OnlineCategory);
                Section section5 = sections.Where(s => s.Id == "5").FirstOrDefault();
                Assert.AreEqual(OnlineCategory.NotOnline, section5.OnlineCategory);
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionsNonCached_CourseIdsNull()
            {
                IEnumerable<Section> sections = await sectionRepo.GetCourseSectionsNonCachedAsync(null, registrationTerms);
                Assert.AreEqual(0, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionsNonCached_CourseIdsZero()
            {
                IEnumerable<string> courseIds = new List<string>();
                IEnumerable<Section> sections = await sectionRepo.GetCourseSectionsNonCachedAsync(courseIds, registrationTerms);
                Assert.AreEqual(0, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionsNonCached_TermsNull()
            {
                IEnumerable<string> courseIds = new List<string>() { "123" };
                IEnumerable<Section> sections = await sectionRepo.GetCourseSectionsNonCachedAsync(courseIds, null);
                Assert.AreEqual(0, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionsNonCached_TermsZero()
            {
                IEnumerable<Term> termIds = new List<Term>();
                IEnumerable<string> courseIds = new List<string>() { "123" };
                IEnumerable<Section> sections = await sectionRepo.GetCourseSectionsNonCachedAsync(courseIds, termIds);
                Assert.AreEqual(0, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetCourseSectionsNonCached_ReturnsResults()
            {
                IEnumerable<string> courseIds = new List<string>() { "123" };
                IEnumerable<Section> sections = await sectionRepo.GetCourseSectionsNonCachedAsync(courseIds, registrationTerms);
                Assert.AreEqual(4, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetNoncachedSections_NullFacultyDataResponse_SectionsReturned()
            {
                // Mock null faculty repo response
                sectionFacultyResponseData = null;
                dataReaderMock.Setup<Task<Collection<CourseSecFaculty>>>(facc => facc.BulkReadRecordAsync<CourseSecFaculty>("COURSE.SEC.FACULTY", It.IsAny<string[]>(), true)).Returns(Task.FromResult(sectionFacultyResponseData));
                List<string> sectionIds = new List<string>() { "1", "2", "3" };
                IEnumerable<Section> sections = await sectionRepo.GetNonCachedSectionsAsync(sectionIds);
                Assert.AreEqual(3, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetNoncachedSections_NullRosterDataResponse_SectionsReturned()
            {
                // Mock null roster response
                studentCourseSecResponseData = null;
                transManagerMock.Setup(mgr => mgr.ExecuteAsync<GetStudentCourseSecStudentsRequest, GetStudentCourseSecStudentsResponse>(It.IsAny<GetStudentCourseSecStudentsRequest>())).Returns(Task.FromResult(studentCourseSecResponseData));
                List<string> sectionIds = new List<string>() { "1", "2", "3" };
                IEnumerable<Section> sections = await sectionRepo.GetNonCachedSectionsAsync(sectionIds);
                Assert.AreEqual(3, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetNoncachedSections_NullPendingDataResponse_SectionsReturned()
            {
                // Mock null pending data response
                pendingResponseData = null;
                dataReaderMock.Setup<Task<Collection<CourseSecPending>>>(csp => csp.BulkReadRecordAsync<CourseSecPending>("COURSE.SEC.PENDING", It.IsAny<string[]>(), true)).Returns(Task.FromResult(pendingResponseData));
                List<string> sectionIds = new List<string>() { "1", "2", "3" };
                IEnumerable<Section> sections = await sectionRepo.GetNonCachedSectionsAsync(sectionIds);
                Assert.AreEqual(3, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetNoncachedSections_NullWaitlistDataReponse_SectionsReturned()
            {
                // Mock null waitlist data response
                waitlistResponseData = null;
                dataReaderMock.Setup<Task<Collection<WaitList>>>(wl => wl.BulkReadRecordAsync<WaitList>("WAIT.LIST", It.IsAny<string>(), true)).Returns(Task.FromResult(waitlistResponseData));
                List<string> sectionIds = new List<string>() { "1", "2", "3" };
                IEnumerable<Section> sections = await sectionRepo.GetNonCachedSectionsAsync(sectionIds);
                Assert.AreEqual(3, sections.Count());
            }

            [TestMethod]
            public async Task SectionRepository_GetNoncachedSections_NullPortalDataResponse_SectionsReturned()
            {
                // Mock null portal site response
                portalSitesResponseData = null;
                dataReaderMock.Setup<Task<Collection<PortalSites>>>(ps => ps.BulkReadRecordAsync<PortalSites>("PORTAL.SITES", It.IsAny<string[]>(), true)).Returns(Task.FromResult(portalSitesResponseData));
                List<string> sectionIds = new List<string>() { "1", "2", "3" };
                IEnumerable<Section> sections = await sectionRepo.GetNonCachedSectionsAsync(sectionIds);
                Assert.AreEqual(3, sections.Count());
            }

            [TestMethod]
            public async Task StWebDefaultsNotFound()
            {
                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>("ST.PARMS", It.IsAny<string>(), It.IsAny<bool>())).Returns<string, string, bool>(
                (param, id, repl) => Task.FromResult(new StwebDefaults())
                );

                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>(It.IsAny<string>(), It.IsAny<bool>())).Returns<string, bool>(
                   (id, repl) => Task.FromResult(new StwebDefaults())
                   );
                List<string> sectionIds = new List<string>() { "1", "2", "3" };
                IEnumerable<Section> sections = await sectionRepo.GetNonCachedSectionsAsync(sectionIds);
                Section sec = sections.Where(s => s.Id == "1").FirstOrDefault();
                Assert.AreEqual(null, sec.BookstoreURL);
            }

            [TestMethod]
            public async Task NoBookstoreTemplateDefined()
            {
                StwebDefaults stwebDefaults = new StwebDefaults();
                dataReaderMock.Setup<Task<StwebDefaults>>(ps => ps.ReadRecordAsync<Ellucian.Colleague.Data.Student.DataContracts.StwebDefaults>("ST.PARMS", "STWEB.DEFAULTS", false)).Returns(Task.FromResult(stwebDefaults));
                List<string> sectionIds = new List<string>() { "1", "2", "3" };
                IEnumerable<Section> sections = await sectionRepo.GetNonCachedSectionsAsync(sectionIds);
                Section sec = sections.Where(s => s.Id == "1").FirstOrDefault();
                Assert.AreEqual(null, sec.BookstoreURL);
            }

            [TestMethod]
            public async Task BookstoreTemplate_RemovedValueMarks()
            {
                char _VM = Convert.ToChar(DynamicArray.VM);
                StwebDefaults stwebDefaults = new StwebDefaults();
                stwebDefaults.StwebBookstoreUrlTemplate = "abc" + _VM + "zyx";
                dataReaderMock.Setup<Task<StwebDefaults>>(ps => ps.ReadRecordAsync<Ellucian.Colleague.Data.Student.DataContracts.StwebDefaults>("ST.PARMS", "STWEB.DEFAULTS", false)).Returns(Task.FromResult(stwebDefaults));
                List<string> sectionIds = new List<string>() { "1", "2", "3" };
                IEnumerable<Section> sections = await sectionRepo.GetNonCachedSectionsAsync(sectionIds);
                Section sec = sections.Where(s => s.Id == "1").FirstOrDefault();
                Assert.AreEqual("abczyx", sec.BookstoreURL);
            }

            [TestMethod]
            public async Task TemplateHasNoQueryParmeters()
            {
                StwebDefaults stwebDefaults = new StwebDefaults();
                stwebDefaults.StwebBookstoreUrlTemplate = "abc";
                dataReaderMock.Setup<Task<StwebDefaults>>(ps => ps.ReadRecordAsync<Ellucian.Colleague.Data.Student.DataContracts.StwebDefaults>("ST.PARMS", "STWEB.DEFAULTS", false)).Returns(Task.FromResult(stwebDefaults));
                List<string> sectionIds = new List<string>() { "1", "2", "3" };
                IEnumerable<Section> sections = await sectionRepo.GetNonCachedSectionsAsync(sectionIds);
                Section sec = sections.Where(s => s.Id == "1").FirstOrDefault();
                Assert.AreEqual("abc", sec.BookstoreURL);
            }

            [TestMethod]
            public async Task MultipleSubstitutions_Successful()
            {
                string template = "abc?a={4}&b={5}&c={0}&d={1}&e={3}&f={2}&a={4}&b={5}&c={0}&d={1}&e={3}&f={2}"; StwebDefaults stwebDefaults = new StwebDefaults();
                stwebDefaults.StwebBookstoreUrlTemplate = template;
                dataReaderMock.Setup<Task<StwebDefaults>>(ps => ps.ReadRecordAsync<Ellucian.Colleague.Data.Student.DataContracts.StwebDefaults>("ST.PARMS", "STWEB.DEFAULTS", false)).Returns(Task.FromResult(stwebDefaults));
                List<string> sectionIds = new List<string>() { "1", "2", "3" };
                IEnumerable<Section> sections = await sectionRepo.GetNonCachedSectionsAsync(sectionIds);
                Section sec = sections.Where(s => s.Id == "1").FirstOrDefault();
                Assert.AreEqual("abc?a=1&b=139&c=2012%2fFA&d=MATH&e=01&f=1000&a=1&b=139&c=2012%2fFA&d=MATH&e=01&f=1000", sec.BookstoreURL);
            }

            private SectionRepository BuildValidSectionRepository()
            {
                apiSettings = new ApiSettings("null");

                BuildLdmConfiguration(dataReaderMock, out cdDefaults);

                // Set up repo response for initial section request (1, 2, 3)
                dataReaderMock.Setup(acc => acc.SelectAsync("COURSE.SECTIONS", It.IsAny<string>())).Returns(Task.FromResult(sectionsResponseData.Select(c => c.Recordkey).ToArray()));
                dataReaderMock.Setup<Task<Collection<CourseSections>>>(acc => acc.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string[]>(), true))
                    .Returns<string, string[], bool>((file, ids, flag) => Task.FromResult(new Collection<CourseSections>(sectionsResponseData.Where(x => ids.Contains(x.Recordkey)).ToList())));

                // Set up repo response for "all" meeting requests
                dataReaderMock.Setup<Task<Collection<CourseSecMeeting>>>(macc => macc.BulkReadRecordAsync<CourseSecMeeting>("COURSE.SEC.MEETING", It.IsAny<string[]>(), true)).Returns(Task.FromResult(sectionMeetingResponseData));

                // Set up repo response for "all" faculty
                dataReaderMock.Setup<Task<Collection<CourseSecFaculty>>>(facc => facc.BulkReadRecordAsync<CourseSecFaculty>("COURSE.SEC.FACULTY", It.IsAny<string[]>(), true)).Returns(Task.FromResult(sectionFacultyResponseData));

                transManagerMock.Setup(mgr => mgr.ExecuteAsync<GetStudentCourseSecStudentsRequest, GetStudentCourseSecStudentsResponse>(It.IsAny<GetStudentCourseSecStudentsRequest>())).Returns(Task.FromResult(studentCourseSecResponseData)).Callback<GetStudentCourseSecStudentsRequest>(req => updateRequest = req);
                //dataAccessorMock.Setup<Collection<StudentCourseSec>>(scsc => scsc.BulkReadRecordAsync<StudentCourseSec>("STUDENT.COURSE.SEC", It.IsAny<string[]>())).Returns(studentCourseSecResponseData);

                dataReaderMock.Setup<Task<Collection<PortalSites>>>(ps => ps.BulkReadRecordAsync<PortalSites>("PORTAL.SITES", It.IsAny<string[]>(), true)).Returns(Task.FromResult(portalSitesResponseData));
                dataReaderMock.Setup<Task<Collection<CourseSecXlists>>>(sxl => sxl.BulkReadRecordAsync<CourseSecXlists>("COURSE.SEC.XLISTS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(crosslistResponseData));
                dataReaderMock.Setup<Task<Collection<CourseSecPending>>>(csp => csp.BulkReadRecordAsync<CourseSecPending>("COURSE.SEC.PENDING", It.IsAny<string[]>(), true)).Returns(Task.FromResult(pendingResponseData));
                dataReaderMock.Setup<Task<Collection<WaitList>>>(wl => wl.BulkReadRecordAsync<WaitList>("WAIT.LIST", It.IsAny<string>(), true)).Returns(Task.FromResult(waitlistResponseData));

                // Set up repo response for waitlist statuses
                //ApplValcodes waitlistCodeResponse = new ApplValcodes()
                //{
                //    ValsEntityAssociation = new List<ApplValcodesVals>() {new ApplValcodesVals() { ValInternalCodeAssocMember = "A", ValActionCode1AssocMember = "1" },
                //                                                       new ApplValcodesVals() { ValInternalCodeAssocMember = "E", ValActionCode1AssocMember = "2"},
                //                                                       new ApplValcodesVals() { ValInternalCodeAssocMember = "D", ValActionCode1AssocMember = "3"},
                //                                                       new ApplValcodesVals() { ValInternalCodeAssocMember = "P", ValActionCode1AssocMember = "4"},
                //                                                       new ApplValcodesVals() { ValInternalCodeAssocMember = "X", ValActionCode1AssocMember = "5"}}
                //};
                //dataAccessorMock.Setup<ApplValcodes>(cacc => cacc.ReadRecord<ApplValcodes>("ST.VALCODES", "WAIT.LIST.STATUSES", true)).Returns(waitlistCodeResponse);

                // Set up repo response for section statuses
                var sectionStatuses = new ApplValcodes();
                sectionStatuses.ValsEntityAssociation = new List<ApplValcodesVals>();
                sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("A", "Active", "1", "A", "", "", ""));
                sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("I", "Inactive", "2", "I", "", "", ""));
                sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("C", "Cancelled", "", "C", "", "", ""));
                dataReaderMock.Setup<Task<ApplValcodes>>(cacc => cacc.ReadRecordAsync<ApplValcodes>("ST.VALCODES", "SECTION.STATUSES", true)).ReturnsAsync(sectionStatuses);

                // Set up repo response for the temporary international parameter item
                Data.Base.DataContracts.IntlParams intlParams = new Data.Base.DataContracts.IntlParams();
                intlParams.HostDateDelimiter = "/";
                intlParams.HostShortDateFormat = "MDY";
                dataReaderMock.Setup<Task<Data.Base.DataContracts.IntlParams>>(iacc => iacc.ReadRecordAsync<Data.Base.DataContracts.IntlParams>("INTL.PARAMS", "INTERNATIONAL", true)).ReturnsAsync(intlParams);
                // Setup localCacheMock as the object for the CacheProvider
                //cacheProviderMock.Setup(provider => provider.GetCache(It.IsAny<string>())).Returns(localCacheMock.Object);

                // Set up course defaults response (indicates if coreq conversion has taken place)
                BuildCourseParametersConvertedResponse(cdDefaults);
                dataReaderMock.Setup<Task<CdDefaults>>(acc => acc.ReadRecordAsync<CdDefaults>("ST.PARMS", "CD.DEFAULTS", true)).ReturnsAsync(cdDefaults);

                // Set up instructional method response (indicates if sections are online)
                MockRecordsAsync("INSTR.METHODS", BuildValidInstrMethodResponse());
                //var instrMethods = BuildValidInstrMethodResponse();
                //dataReaderMock.Setup(acc => acc.BulkReadRecordAsync<InstrMethods>("INSTR.METHODS", "", true)).ReturnsAsync(instrMethods);
                //var instrMethodLookup = instrMethods.Select(x => new RecordKeyLookup("INSTR.METHODS", x.Recordkey, string.Empty, string.Empty, false));
                //var instrMethodResult = instrMethods.ToDictionary<InstrMethods, string, RecordKeyLookupResult>(im => "INSTR.METHODS+" + im.Recordkey, im => new RecordKeyLookupResult() { Guid = im.RecordGuid });
                //dataReaderMock.Setup(reader => reader.SelectAsync(instrMethodLookup.ToArray())).ReturnsAsync(instrMethodResult);

                //setup mocking for Stweb Defaults
                var stWebDflt = BuildStwebDefaults(); ;
                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>("ST.PARMS", It.IsAny<string>(), It.IsAny<bool>())).Returns<string, string, bool>(
                    (param, id, repl) => Task.FromResult((stWebDflt.Recordkey == id) ? stWebDflt : null)
                    );

                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>(It.IsAny<string>(), It.IsAny<bool>())).Returns<string, bool>(
                   (id, repl) => Task.FromResult((stWebDflt.Recordkey == id) ? stWebDflt : null)
                   );

                // Construct section repository
                sectionRepo = new SectionRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettings);

                return sectionRepo;
            }

            private SectionRepository BuildInvalidSectionRepository()
            {
                apiSettings = new ApiSettings("null");

                // Set up repo response for "all" section requests
                Exception expectedFailure = new Exception("fail");
                dataReaderMock.Setup<Task<Collection<CourseSections>>>(acc => acc.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string[]>(), true)).Throws(expectedFailure);

                // Construct section repository
                sectionRepo = new SectionRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettings);

                return sectionRepo;
            }

            private Collection<CourseSections> BuildSectionsResponse(IEnumerable<Section> sections)
            {
                var repoSections = new Collection<CourseSections>();
                foreach (var section in sections)
                {
                    var crsSec = new CourseSections();
                    crsSec.RecordGuid = section.Guid;
                    crsSec.Recordkey = section.Id;
                    crsSec.SecAcadLevel = section.AcademicLevelCode;
                    crsSec.SecCeus = section.Ceus;
                    crsSec.SecCourse = section.CourseId;
                    crsSec.SecCourseLevels = section.CourseLevelCodes.ToList();
                    crsSec.SecDepartmentsEntityAssociation = section.Departments.Select(x => new CourseSectionsSecDepartments(x.AcademicDepartmentCode, x.ResponsibilityPercentage)).ToList();
                    crsSec.SecLocation = section.Location;
                    crsSec.SecMaxCred = section.MaximumCredits;
                    crsSec.SecVarCredIncrement = section.VariableCreditIncrement;
                    crsSec.SecMinCred = section.MinimumCredits;
                    crsSec.SecNo = section.Number;
                    crsSec.SecShortTitle = section.Title;
                    crsSec.SecCredType = section.CreditTypeCode;
                    crsSec.SecStartDate = section.StartDate;
                    crsSec.SecEndDate = section.EndDate;
                    crsSec.SecTerm = section.TermId;
                    crsSec.SecOnlyPassNopassFlag = section.OnlyPassNoPass ? "Y" : "N";
                    crsSec.SecAllowPassNopassFlag = section.AllowPassNoPass ? "Y" : "N";
                    crsSec.SecAllowAuditFlag = section.AllowAudit ? "Y" : "N";
                    var csm = new List<string>() { "1", "2" };
                    crsSec.SecMeeting = csm;
                    var csf = new List<string>() { "1", "2" };
                    crsSec.SecFaculty = csf;
                    var sas = new List<string>() { "1", "2" };
                    crsSec.SecActiveStudents = sas;
                    crsSec.SecStatusesEntityAssociation = section.Statuses.Select(x => new CourseSectionsSecStatuses(x.Date, ConvertSectionStatusToCode(x.Status))).ToList();
                    crsSec.SecCourseTypes = section.CourseTypeCodes.ToList();
                    crsSec.SecAllowWaitlistFlag = section.AllowWaitlist ? "Y" : "N";

                    // Reconstruct all Colleague requisite and corequisite fields from the data in the Requisites 
                    // post-conversion fields
                    crsSec.SecReqs = new List<string>();
                    crsSec.SecRecommendedSecs = new List<string>();
                    crsSec.SecCoreqSecs = new List<string>();
                    crsSec.SecMinNoCoreqSecs = null;
                    crsSec.SecOverrideCrsReqsFlag = section.OverridesCourseRequisites ? "Y" : "";
                    // pre-conversion fields
                    crsSec.SecCourseCoreqsEntityAssociation = new List<CourseSectionsSecCourseCoreqs>();
                    crsSec.SecCoreqsEntityAssociation = new List<CourseSectionsSecCoreqs>();

                    foreach (var req in section.Requisites)
                    {
                        if (!string.IsNullOrEmpty(req.RequirementCode))
                        {
                            // post-conversion
                            crsSec.SecReqs.Add(req.RequirementCode);
                            // pre-conversion -- this does not convert
                        }
                        else if (!string.IsNullOrEmpty(req.CorequisiteCourseId))
                        {
                            // post-conversion -- this does not convert
                            // pre-conversion
                            crsSec.SecCourseCoreqsEntityAssociation.Add(
                                new CourseSectionsSecCourseCoreqs(req.CorequisiteCourseId, (req.IsRequired) ? "Y" : "")
                            );
                        }

                    }

                    foreach (var req in section.SectionRequisites)
                    {
                        if (req.CorequisiteSectionIds != null && req.CorequisiteSectionIds.Count() > 0)
                        {
                            foreach (var secId in req.CorequisiteSectionIds)
                            {
                                // post-conversion--put required and recommended into two separate lists
                                if (req.IsRequired)
                                {
                                    crsSec.SecCoreqSecs.Add(secId);
                                    // The minimum number is associated with the entire list of SecCoreqSecs
                                    crsSec.SecMinNoCoreqSecs = req.NumberNeeded;
                                }
                                else
                                {
                                    crsSec.SecRecommendedSecs.Add(secId);
                                }
                                // pre-conversion--each requisite section has a required flag
                                crsSec.SecCoreqsEntityAssociation.Add(
                                    new CourseSectionsSecCoreqs(secId, (req.IsRequired) ? "Y" : "")
                                    );
                            }
                        }
                    }

                    if (section.Books != null)
                    {
                        crsSec.SecBooks = new List<string>();
                        crsSec.SecBookOptions = new List<string>();
                        foreach (var book in section.Books)
                        {
                            crsSec.SecBooks.Add(book.BookId);
                            if (book.IsRequired)
                            {
                                crsSec.SecBookOptions.Add("R");
                            }
                        }
                    }
                    crsSec.SecPortalSite = ""; // (!string.IsNullOrEmpty(section.LearningProvider) ? crsSec.Recordkey : "");
                    if (!string.IsNullOrEmpty(section.LearningProvider))
                    {
                        crsSec.SecPortalSite = section.Id;
                    }
                    else
                    {
                        crsSec.SecPortalSite = "";
                    }
                    if (section.Id == "1")
                    {
                        crsSec.SecXlist = "234";
                        crsSec.SecCapacity = 10;
                        crsSec.SecSubject = "MATH";
                        crsSec.SecCourseNo = "1000";
                    }
                    repoSections.Add(crsSec);
                }
                return repoSections;
            }

            private Collection<CourseSecMeeting> BuildSectionMeetingsResponse(IEnumerable<Section> sections)
            {
                Collection<CourseSecMeeting> repoSecMeetings = new Collection<CourseSecMeeting>();
                int crsSecMId = 0;
                foreach (var section in sections)
                {
                    foreach (var mt in section.Meetings)
                    {
                        var crsSecM = new CourseSecMeeting();
                        crsSecMId += 1;
                        crsSecM.Recordkey = crsSecMId.ToString();
                        if (!string.IsNullOrEmpty(mt.Room))
                        {
                            crsSecM.CsmBldg = "ABLE";
                            crsSecM.CsmRoom = "A100";
                        }
                        crsSecM.CsmCourseSection = section.Id;
                        crsSecM.CsmInstrMethod = mt.InstructionalMethodCode;
                        crsSecM.CsmStartTime = mt.StartTime.HasValue ? mt.StartTime.Value.DateTime : (DateTime?)null;
                        crsSecM.CsmEndTime = mt.EndTime.HasValue ? mt.EndTime.Value.DateTime : (DateTime?)null;
                        foreach (var d in mt.Days)
                        {
                            switch (d)
                            {
                                case DayOfWeek.Friday:
                                    crsSecM.CsmFriday = "Y";
                                    break;
                                case DayOfWeek.Monday:
                                    crsSecM.CsmMonday = "Y";
                                    break;
                                case DayOfWeek.Saturday:
                                    crsSecM.CsmSaturday = "Y";
                                    break;
                                case DayOfWeek.Sunday:
                                    crsSecM.CsmSunday = "Y";
                                    break;
                                case DayOfWeek.Thursday:
                                    crsSecM.CsmThursday = "Y";
                                    break;
                                case DayOfWeek.Tuesday:
                                    crsSecM.CsmTuesday = "Y";
                                    break;
                                case DayOfWeek.Wednesday:
                                    crsSecM.CsmWednesday = "Y";
                                    break;
                                default:
                                    break;
                            }
                        }
                        repoSecMeetings.Add(crsSecM);
                    }

                }
                return repoSecMeetings;
            }

            private Collection<CourseSecFaculty> BuildSectionFacultyResponse(IEnumerable<Section> sections)
            {
                Collection<CourseSecFaculty> repoSecFaculty = new Collection<CourseSecFaculty>();
                int crsSecFId = 0;
                foreach (var section in sections)
                {
                    foreach (var fac in section.FacultyIds)
                    {
                        var crsSecF = new CourseSecFaculty();
                        crsSecFId += 1;
                        crsSecF.Recordkey = crsSecFId.ToString();
                        crsSecF.CsfCourseSection = section.Id;
                        crsSecF.CsfFaculty = fac;
                        repoSecFaculty.Add(crsSecF);
                    }

                }
                return repoSecFaculty;
            }

            private GetStudentCourseSecStudentsResponse BuildStudentCourseSecResponse(IEnumerable<Section> sections)
            {
                var scssr = new GetStudentCourseSecStudentsResponse();
                scssr.StudentCourseSectionStudents = new List<StudentCourseSectionStudents>();
                foreach (var section in sections)
                {
                    foreach (var stu in section.ActiveStudentIds)
                    {
                        var scss = new StudentCourseSectionStudents();
                        scss.CourseSectionIds = section.Id;
                        scss.StudentIds = stu;
                        scssr.StudentCourseSectionStudents.Add(scss);
                    }
                }
                return scssr;
            }

            private Collection<PortalSites> BuildPortalSitesResponse(IEnumerable<Section> sections)
            {
                var repoPS = new Collection<PortalSites>();
                foreach (var section in sections)
                {
                    if (section.CourseId == "7272")
                    {
                        var ps = new PortalSites();
                        // normally some thing like "HIST-190-001-cs11347", but mock portal site Id with section ID
                        ps.Recordkey = section.Id;
                        if (section.Number == "98")
                        {
                            ps.PsLearningProvider = "";
                            ps.PsPrtlSiteGuid = section.Id;
                        }
                        if (section.Number == "97")
                        {
                            ps.PsLearningProvider = "MOODLE";
                            ps.PsPrtlSiteGuid = section.Id;
                        }
                        repoPS.Add(ps);
                    }
                }
                return repoPS;
            }

            private Collection<CourseSecXlists> BuildCrosslistResponse(IEnumerable<Section> sections)
            {
                // currently built only for ILP testing
                var repoXL = new Collection<CourseSecXlists>();
                foreach (var section in sections)
                {
                    if (section.Id == "1")
                    {
                        var xl = new CourseSecXlists();
                        xl.Recordkey = "232";
                        xl.CsxlPrimarySection = section.Id;
                        xl.CsxlCourseSections = new List<string>() { section.Id, "5" };
                        xl.CsxlCapacity = 20;
                        xl.CsxlWaitlistMax = 5;
                        xl.CsxlWaitlistFlag = "Y";
                        repoXL.Add(xl);
                    }
                }
                return repoXL;
            }

            private Collection<CourseSecPending> BuildPendingSectionResponse(IEnumerable<Section> sections)
            {
                var repoPending = new Collection<CourseSecPending>();
                foreach (var section in sections)
                {
                    if (section.CourseId == "7272" && (section.Number == "98" || section.Number == "97"))
                    {
                        var cp = new CourseSecPending();
                        cp.Recordkey = section.Id;
                        if (section.Number == "98")
                        {
                            cp.CspReservedSeats = 1;
                        }
                        if (section.Number == "97")
                        {
                            cp.CspReservedSeats = 2;
                        }
                        repoPending.Add(cp);
                    }
                }
                return repoPending;
            }

            private Collection<WaitList> BuildWaitlistResponse(IEnumerable<Section> sections)
            {
                var repoWaitlist = new Collection<WaitList>();
                foreach (var section in sections)
                {
                    if (section.CourseId == "7272" && (section.Number == "98" || section.Number == "97"))
                    {
                        var wl = new WaitList();
                        wl.Recordkey = section.Id;
                        if (section.Number == "98")
                        {
                            wl.WaitCourseSection = section.Id;
                            wl.WaitStatus = "P";
                            wl.WaitStudent = "111111";
                        }
                        if (section.Number == "97")
                        {
                            wl.WaitCourseSection = section.Id;
                            wl.WaitStatus = "P";
                            wl.WaitStudent = "22222";
                        }
                        repoWaitlist.Add(wl);
                    }
                }
                return repoWaitlist;
            }

            private void BuildCourseParametersConvertedResponse(CdDefaults defaults)
            {
                // Converted Response
                defaults.CdReqsConvertedFlag = "Y";
            }
        }

        [TestClass]
        public class SectionRepository_PostSectionMeeting : SectionRepositoryTests
        {
            string secGuid, meet1Guid, meet2Guid, meet3Guid;
            Section section;
            SectionMeeting meeting1, meeting2, meeting3, secMeet;
            CourseSecMeeting csm;
            CourseSecFaculty csf;
            string faculty1, faculty2, faculty3;
            SectionFaculty lecFaculty, labFaculty1, labFaculty2, semFaculty;
            DateTime secStartDate, secEndDate, semStartDate, semEndDate;
            DateTimeOffset? lecStartTime, lecEndTime, labStartTime, labEndTime, semStartTime, semEndTime;
            List<OfferingDepartment> depts = new List<OfferingDepartment>();
            List<SectionStatusItem> statuses = new List<SectionStatusItem>();
            List<DayOfWeek> MWF = new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday };
            List<DayOfWeek> TTh = new List<DayOfWeek>() { DayOfWeek.Tuesday, DayOfWeek.Thursday };
            List<DayOfWeek> Wed = new List<DayOfWeek>() { DayOfWeek.Wednesday };
            List<SectionMeeting> meetings = new List<SectionMeeting>();
            List<SectionFaculty> faculty = new List<SectionFaculty>();

            UpdateInstructionalEventRequest request;
            UpdateInstructionalEventResponse response;
            SectionRepository repository;

            [TestInitialize]
            public void Initialize()
            {
                MainInitialize();

                secGuid = Guid.NewGuid().ToString();
                secStartDate = new DateTime(2014, 9, 2);
                secEndDate = new DateTime(2014, 12, 5);
                depts.Add(new OfferingDepartment("RECR", 75m));
                depts.Add(new OfferingDepartment("CECR", 25m));
                var courseLevelCodes = new List<string>() { "100", "CE" };
                statuses.Add(new SectionStatusItem(SectionStatus.Active, "A", new DateTime(2011, 9, 28)));
                section = new Section("1", "1", "01", secStartDate, 3.00m, null, "Underwater Basketweaving", "IN", depts, courseLevelCodes, "CE", statuses) { Guid = secGuid };

                faculty1 = "1234567";
                faculty2 = "2345678";
                faculty3 = "3456789";
                meet1Guid = Guid.NewGuid().ToString();
                meet2Guid = Guid.NewGuid().ToString();
                meet3Guid = Guid.NewGuid().ToString();
                lecStartTime = (new DateTime(1, 1, 1, 9, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                lecEndTime = (new DateTime(1, 1, 1, 9, 50, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                labStartTime = (new DateTime(1, 1, 1, 13, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                labEndTime = (new DateTime(1, 1, 1, 16, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                meeting1 = new SectionMeeting("1", "1", "LEC", secStartDate, secEndDate, "W") { Days = MWF, StartTime = lecStartTime, EndTime = lecEndTime, Room = "ARM*240", Load = 20m, IsOnline = false, Guid = meet1Guid };
                meeting1.AddFacultyId(faculty1);
                meeting2 = new SectionMeeting("2", "1", "LAB", secStartDate, secEndDate, "W") { Days = TTh, StartTime = labStartTime, EndTime = labEndTime, Room = "ARM*131", Load = 10m, IsOnline = false, Guid = meet2Guid };
                meeting2.AddFacultyId(faculty1);
                meeting2.AddFacultyId(faculty2);

                semStartDate = new DateTime(2014, 10, 1);
                semEndDate = new DateTime(2014, 10, 29);
                semStartTime = (new DateTime(1, 1, 1, 19, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                semEndTime = (new DateTime(1, 1, 1, 22, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                meeting3 = new SectionMeeting(null, "1", "SEM", semStartDate, semEndDate, "W") { Days = Wed, StartTime = semStartTime, EndTime = semEndTime, Room = "JFK*200", Load = 5m, IsOnline = false, Guid = meet3Guid };
                meeting3.AddFacultyId(faculty3);

                lecFaculty = new SectionFaculty("1", "1", faculty1, "LEC", secStartDate, secEndDate, 100m) { LoadFactor = 20m };
                labFaculty1 = new SectionFaculty("2", "1", faculty1, "LAB", secStartDate, secEndDate, 50m) { LoadFactor = 5m };
                labFaculty2 = new SectionFaculty("3", "1", faculty2, "LAB", secStartDate, secEndDate, 50m) { LoadFactor = 5m };
                semFaculty = new SectionFaculty(null, "1", faculty3, "SEM", semStartDate, semEndDate, 100m) { LoadFactor = 5m };

                meeting1.AddSectionFaculty(lecFaculty);
                meeting2.AddSectionFaculty(labFaculty1);
                meeting2.AddSectionFaculty(labFaculty2);
                meeting3.AddSectionFaculty(semFaculty);

                section.AddSectionMeeting(meeting1);
                section.AddSectionMeeting(meeting2);
                section.AddSectionMeeting(meeting3);
                section.AddSectionFaculty(lecFaculty);
                section.AddSectionFaculty(labFaculty1);
                section.AddSectionFaculty(labFaculty2);
                section.AddSectionFaculty(semFaculty);

                meetings.Add(meeting1);
                meetings.Add(meeting2);
                meetings.Add(meeting3);
                faculty.Add(lecFaculty);
                faculty.Add(labFaculty1);
                faculty.Add(labFaculty2);
                faculty.Add(semFaculty);

                request = new UpdateInstructionalEventRequest();
                response = new UpdateInstructionalEventResponse()
                    {
                        UpdateInstructionalEventErrors = new List<UpdateInstructionalEventErrors>(),
                        UpdateInstructionalEventWarnings = new List<UpdateInstructionalEventWarnings>()
                    };

                //newMeeting = new SectionMeeting("5", "1", "SEM", semStartDate, semEndDate, "W") { Days = Wed, StartTime = semStartTime, EndTime = semEndTime, Room = "JFK*200", Load = 5m, IsOnline = false, Guid = Guid.NewGuid().ToString().ToLowerInvariant() };
                csm = new CourseSecMeeting()
                    {
                        CsmBldg = "JFK",
                        CsmCourseSection = "1",
                        CsmEndDate = semEndDate,
                        CsmEndTime = semEndTime.Value.DateTime,
                        CsmFaculty = new List<string>() { faculty3 },
                        CsmFrequency = "W",
                        CsmFriday = "N",
                        CsmInstrMethod = "SEM",
                        CsmLoad = 5m,
                        CsmMonday = "N",
                        CsmRoom = "200",
                        CsmSaturday = "N",
                        CsmStartDate = semStartDate,
                        CsmStartTime = semStartTime.Value.DateTime,
                        CsmSunday = "N",
                        CsmThursday = "N",
                        CsmTuesday = "N",
                        CsmWednesday = "Y",
                        RecordGuid = Guid.NewGuid().ToString().ToLowerInvariant(),
                        Recordkey = "4"
                    };
                csf = new CourseSecFaculty()
                    {
                        CsfCourseSection = "1",
                        CsfEndDate = semEndDate,
                        CsfFaculty = faculty3,
                        CsfFacultyLoad = 5m,
                        CsfFacultyPct = 100m,
                        CsfInstrMethod = "SEM",
                        CsfStartDate = semStartDate,
                        Recordkey = "5"
                    };

                CdDefaults cdDefaults;
                BuildLdmConfiguration(dataReaderMock, out cdDefaults);

                dataReaderMock.Setup(r => r.ReadRecordAsync<CourseSecMeeting>(It.IsAny<string>(), true)).Returns<string, bool>((id, flag) =>
                    {
                        if (id == "4")
                        {
                            return Task.FromResult(csm);
                        }
                        var mtg = meetings.FirstOrDefault(x => x.Id == id);
                        if (mtg == null) return Task.FromResult(new CourseSecMeeting());

                        var room = mtg.Room.Contains('*') ? mtg.Room.Split('*') : new string[2] { mtg.Room, string.Empty };
                        return Task.FromResult(new CourseSecMeeting()
                            {
                                Recordkey = mtg.Id,
                                RecordGuid = mtg.Guid,
                                CsmBldg = room[0],
                                CsmCourseSection = mtg.SectionId,
                                CsmEndDate = mtg.EndDate,
                                CsmEndTime = mtg.EndTime.Value.DateTime,
                                CsmFaculty = mtg.FacultyIds.ToList(),
                                CsmFrequency = mtg.Frequency,
                                CsmInstrMethod = mtg.InstructionalMethodCode,
                                CsmLoad = mtg.Load,
                                CsmRoom = room[1],
                                CsmStartDate = mtg.StartDate,
                                CsmStartTime = mtg.StartTime.Value.DateTime,
                                CsmSunday = mtg.Days.Contains(DayOfWeek.Sunday) ? "Y" : "N",
                                CsmMonday = mtg.Days.Contains(DayOfWeek.Monday) ? "Y" : "N",
                                CsmTuesday = mtg.Days.Contains(DayOfWeek.Tuesday) ? "Y" : "N",
                                CsmWednesday = mtg.Days.Contains(DayOfWeek.Wednesday) ? "Y" : "N",
                                CsmThursday = mtg.Days.Contains(DayOfWeek.Thursday) ? "Y" : "N",
                                CsmFriday = mtg.Days.Contains(DayOfWeek.Friday) ? "Y" : "N",
                                CsmSaturday = mtg.Days.Contains(DayOfWeek.Saturday) ? "Y" : "N"
                            });
                    });
                dataReaderMock.Setup(r => r.BulkReadRecordAsync<CourseSecFaculty>(It.IsAny<string>(), true)).Returns<string, bool>((id, flag) =>
                    {
                        var results = new Collection<CourseSecFaculty>() { csf };
                        foreach (var fac in faculty)
                        {
                            results.Add(new CourseSecFaculty()
                                {
                                    CsfCourseSection = request.CsmCourseSection,
                                    CsfEndDate = fac.EndDate,
                                    CsfFaculty = fac.FacultyId,
                                    CsfFacultyLoad = fac.LoadFactor,
                                    CsfFacultyPct = fac.ResponsibilityPercentage,
                                    CsfInstrMethod = fac.InstructionalMethodCode,
                                    CsfStartDate = fac.StartDate,
                                    CsfPacLpAsgmt = fac.ContractAssignment,
                                    CsfTeachingArrangement = fac.TeachingArrangementCode,
                                    Recordkey = fac.Id
                                });
                        }
                        return Task.FromResult(results);
                    });
                transManagerMock.Setup(t => t.ExecuteAsync<UpdateInstructionalEventRequest, UpdateInstructionalEventResponse>(It.IsAny<UpdateInstructionalEventRequest>())).Returns(Task.FromResult(response));

                // Set up response for instructional methods
                var instrMethods = BuildValidInstrMethodResponse();
                dataReaderMock.Setup<Task<Collection<InstrMethods>>>(acc => acc.BulkReadRecordAsync<InstrMethods>("INSTR.METHODS", "", true)).ReturnsAsync(instrMethods);

                //setup mocking for Stweb Defaults
                var stWebDflt = BuildStwebDefaults(); ;
                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>("ST.PARMS", It.IsAny<string>(), It.IsAny<bool>())).Returns<string, string, bool>(
                    (param, id, repl) => Task.FromResult((stWebDflt.Recordkey == id) ? stWebDflt : null)
                    );

                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>(It.IsAny<string>(), It.IsAny<bool>())).Returns<string, bool>(
                   (id, repl) => Task.FromResult((stWebDflt.Recordkey == id) ? stWebDflt : null)
                   );
                repository = new SectionRepository(cacheProvider, transFactory, logger, apiSettings);



            }

            [TestMethod]
            public async Task SectionRepository_PostSectionMeeting_AddNewMeeting()
            {
                response.CourseSecMeetingId = "4";

                secMeet = await repository.PostSectionMeetingAsync(section, meet3Guid);
                Assert.AreEqual("4", secMeet.Id);
            }

            [TestMethod]
            public async Task SectionRepository_PostSectionMeeting_ChangeMeetingDates()
            {
                response.CourseSecMeetingId = "1";

                var start = secStartDate.AddDays(30);
                var end = secEndDate.AddDays(30);
                section.Meetings[0].StartDate = start;
                section.Meetings[0].EndDate = end;
                secMeet = await repository.PostSectionMeetingAsync(section, meet1Guid);
                Assert.AreEqual(start, secMeet.StartDate);
                Assert.AreEqual(end, secMeet.EndDate);
            }

            [TestMethod]
            public async Task SectionRepository_PostSectionMeeting_ChangeMeetingTimes()
            {
                response.CourseSecMeetingId = "1";

                var start = lecStartTime.Value.AddHours(1);
                var end = lecEndTime.Value.AddHours(1);
                section.Meetings[0].StartTime = start;
                section.Meetings[0].EndTime = end;
                secMeet = await repository.PostSectionMeetingAsync(section, meet1Guid);
                Assert.AreEqual(start.ToLocalTime(), secMeet.StartTime.Value.ToLocalTime());
                Assert.AreEqual(end.ToLocalTime(), secMeet.EndTime.Value.ToLocalTime());
            }

            [TestMethod]
            public async Task SectionRepository_PostSectionMeeting_ChangeMeetingRoom()
            {
                response.CourseSecMeetingId = "1";

                var room = "ARM*140";
                section.Meetings[0].Room = room;
                secMeet = await repository.PostSectionMeetingAsync(section, meet1Guid);
                Assert.AreEqual(room, secMeet.Room);
            }

            [TestMethod]
            public async Task SectionRepository_PostSectionMeeting_ChangeMeetingFaculty()
            {
                response.CourseSecMeetingId = "1";

                section.Meetings[0].RemoveFacultyId(faculty1);
                section.Meetings[0].AddFacultyId(faculty3);
                secMeet = await repository.PostSectionMeetingAsync(section, meet1Guid);
                Assert.AreEqual(1, secMeet.FacultyIds.Count);
                Assert.AreEqual(faculty3, secMeet.FacultyIds[0]);
            }
        }

        [TestClass]
        public class SectionRepository_PutSectionMeeting : SectionRepositoryTests
        {
            string secGuid, meet1Guid, meet2Guid, meet3Guid;
            Section section;
            SectionMeeting meeting1, meeting2, meeting3, secMeet;
            IEnumerable<SectionMeeting> secMeets;
            CourseSecMeeting csm;
            CourseSecFaculty csf;
            string faculty1, faculty2, faculty3;
            SectionFaculty lecFaculty, labFaculty1, labFaculty2, semFaculty;
            DateTime secStartDate, secEndDate, semStartDate, semEndDate;
            DateTimeOffset? lecStartTime, lecEndTime, labStartTime, labEndTime, semStartTime, semEndTime;
            List<OfferingDepartment> depts = new List<OfferingDepartment>();
            List<SectionStatusItem> statuses = new List<SectionStatusItem>();
            List<DayOfWeek> MWF = new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday };
            List<DayOfWeek> TTh = new List<DayOfWeek>() { DayOfWeek.Tuesday, DayOfWeek.Thursday };
            List<DayOfWeek> Wed = new List<DayOfWeek>() { DayOfWeek.Wednesday };
            List<SectionMeeting> meetings = new List<SectionMeeting>();
            List<SectionFaculty> faculty = new List<SectionFaculty>();

            UpdateInstructionalEventRequest request;
            UpdateInstructionalEventResponse response;
            SectionRepository repository;

            [TestInitialize]
            public void Initialize()
            {
                MainInitialize();

                secGuid = Guid.NewGuid().ToString();
                secStartDate = new DateTime(2014, 9, 2);
                secEndDate = new DateTime(2014, 12, 5);
                depts.Add(new OfferingDepartment("RECR", 75m));
                depts.Add(new OfferingDepartment("CECR", 25m));
                var courseLevelCodes = new List<string>() { "100", "CE" };
                statuses.Add(new SectionStatusItem(SectionStatus.Active, "A", new DateTime(2011, 9, 28)));
                section = new Section("1", "1", "01", secStartDate, 3.00m, null, "Underwater Basketweaving", "IN", depts, courseLevelCodes, "CE", statuses) { Guid = secGuid };

                faculty1 = "1234567";
                faculty2 = "2345678";
                faculty3 = "3456789";
                meet1Guid = Guid.NewGuid().ToString();
                meet2Guid = Guid.NewGuid().ToString();
                meet3Guid = Guid.NewGuid().ToString();
                lecStartTime = (new DateTime(1, 1, 1, 9, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                lecEndTime = (new DateTime(1, 1, 1, 9, 50, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                labStartTime = (new DateTime(1, 1, 1, 13, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                labEndTime = (new DateTime(1, 1, 1, 16, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                meeting1 = new SectionMeeting("1", "1", "LEC", secStartDate, secEndDate, "W") { Days = MWF, StartTime = lecStartTime, EndTime = lecEndTime, Room = "ARM*240", Load = 20m, IsOnline = false, Guid = meet1Guid };
                meeting1.AddFacultyId(faculty1);
                meeting2 = new SectionMeeting("2", "1", "LAB", secStartDate, secEndDate, "W") { Days = TTh, StartTime = labStartTime, EndTime = labEndTime, Room = "ARM*131", Load = 10m, IsOnline = false, Guid = meet2Guid };
                meeting2.AddFacultyId(faculty1);
                meeting2.AddFacultyId(faculty2);

                semStartDate = new DateTime(2014, 10, 1);
                semEndDate = new DateTime(2014, 10, 29);
                semStartTime = (new DateTime(1, 1, 1, 19, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                semEndTime = (new DateTime(1, 1, 1, 22, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                meeting3 = new SectionMeeting(null, "1", "SEM", semStartDate, semEndDate, "W") { Days = Wed, StartTime = semStartTime, EndTime = semEndTime, Room = "JFK*200", Load = 5m, IsOnline = false, Guid = meet3Guid };
                meeting3.AddFacultyId(faculty3);

                lecFaculty = new SectionFaculty("1", "1", faculty1, "LEC", secStartDate, secEndDate, 100m) { LoadFactor = 20m };
                labFaculty1 = new SectionFaculty("2", "1", faculty1, "LAB", secStartDate, secEndDate, 50m) { LoadFactor = 5m };
                labFaculty2 = new SectionFaculty("3", "1", faculty2, "LAB", secStartDate, secEndDate, 50m) { LoadFactor = 5m };
                semFaculty = new SectionFaculty(null, "1", faculty3, "SEM", semStartDate, semEndDate, 100m) { LoadFactor = 5m };

                meeting1.AddSectionFaculty(lecFaculty);
                meeting2.AddSectionFaculty(labFaculty1);
                meeting2.AddSectionFaculty(labFaculty2);
                meeting3.AddSectionFaculty(semFaculty);

                section.AddSectionMeeting(meeting1);
                section.AddSectionMeeting(meeting2);
                section.AddSectionMeeting(meeting3);
                section.AddSectionFaculty(lecFaculty);
                section.AddSectionFaculty(labFaculty1);
                section.AddSectionFaculty(labFaculty2);
                section.AddSectionFaculty(semFaculty);

                meetings.Add(meeting1);
                meetings.Add(meeting2);
                meetings.Add(meeting3);
                faculty.Add(lecFaculty);
                faculty.Add(labFaculty1);
                faculty.Add(labFaculty2);
                faculty.Add(semFaculty);

                request = new UpdateInstructionalEventRequest();
                response = new UpdateInstructionalEventResponse()
                {
                    UpdateInstructionalEventErrors = new List<UpdateInstructionalEventErrors>(),
                    UpdateInstructionalEventWarnings = new List<UpdateInstructionalEventWarnings>()
                };

                //newMeeting = new SectionMeeting("5", "1", "SEM", semStartDate, semEndDate, "W") { Days = Wed, StartTime = semStartTime, EndTime = semEndTime, Room = "JFK*200", Load = 5m, IsOnline = false, Guid = Guid.NewGuid().ToString().ToLowerInvariant() };
                csm = new CourseSecMeeting()
                {
                    CsmBldg = "JFK",
                    CsmCourseSection = "1",
                    CsmEndDate = semEndDate,
                    CsmEndTime = semEndTime.Value.DateTime,
                    CsmFaculty = new List<string>() { faculty3 },
                    CsmFrequency = "W",
                    CsmFriday = "N",
                    CsmInstrMethod = "SEM",
                    CsmLoad = 5m,
                    CsmMonday = "N",
                    CsmRoom = "200",
                    CsmSaturday = "N",
                    CsmStartDate = semStartDate,
                    CsmStartTime = semStartTime.Value.DateTime,
                    CsmSunday = "N",
                    CsmThursday = "N",
                    CsmTuesday = "N",
                    CsmWednesday = "Y",
                    RecordGuid = Guid.NewGuid().ToString().ToLowerInvariant(),
                    Recordkey = "4"
                };
                csf = new CourseSecFaculty()
                {
                    CsfCourseSection = "1",
                    CsfEndDate = semEndDate,
                    CsfFaculty = faculty3,
                    CsfFacultyLoad = 5m,
                    CsfFacultyPct = 100m,
                    CsfInstrMethod = "SEM",
                    CsfStartDate = semStartDate,
                    Recordkey = "5"
                };

                CdDefaults cdDefaults;
                BuildLdmConfiguration(dataReaderMock, out cdDefaults);

                dataReaderMock.Setup(r => r.ReadRecordAsync<CourseSecMeeting>(It.IsAny<string>(), true)).Returns<string, bool>((id, flag) =>
                {
                    if (id == "4")
                    {
                        return Task.FromResult(csm);
                    }
                    var mtg = meetings.FirstOrDefault(x => x.Id == id);
                    if (mtg == null) return Task.FromResult(new CourseSecMeeting());

                    var room = mtg.Room.Contains('*') ? mtg.Room.Split('*') : new string[2] { mtg.Room, string.Empty };
                    return Task.FromResult(new CourseSecMeeting()
                    {
                        Recordkey = mtg.Id,
                        RecordGuid = mtg.Guid,
                        CsmBldg = room[0],
                        CsmCourseSection = mtg.SectionId,
                        CsmEndDate = mtg.EndDate,
                        CsmEndTime = mtg.EndTime.Value.DateTime,
                        CsmFaculty = mtg.FacultyIds.ToList(),
                        CsmFrequency = mtg.Frequency,
                        CsmInstrMethod = mtg.InstructionalMethodCode,
                        CsmLoad = mtg.Load,
                        CsmRoom = room[1],
                        CsmStartDate = mtg.StartDate,
                        CsmStartTime = mtg.StartTime.Value.DateTime,
                        CsmSunday = mtg.Days.Contains(DayOfWeek.Sunday) ? "Y" : "N",
                        CsmMonday = mtg.Days.Contains(DayOfWeek.Monday) ? "Y" : "N",
                        CsmTuesday = mtg.Days.Contains(DayOfWeek.Tuesday) ? "Y" : "N",
                        CsmWednesday = mtg.Days.Contains(DayOfWeek.Wednesday) ? "Y" : "N",
                        CsmThursday = mtg.Days.Contains(DayOfWeek.Thursday) ? "Y" : "N",
                        CsmFriday = mtg.Days.Contains(DayOfWeek.Friday) ? "Y" : "N",
                        CsmSaturday = mtg.Days.Contains(DayOfWeek.Saturday) ? "Y" : "N"
                    });
                });
                dataReaderMock.Setup(r => r.BulkReadRecordAsync<CourseSecFaculty>(It.IsAny<string>(), true)).Returns<string, bool>((id, flag) =>
                {
                    var results = new Collection<CourseSecFaculty>() { csf };
                    foreach (var fac in faculty)
                    {
                        results.Add(new CourseSecFaculty()
                        {
                            CsfCourseSection = request.CsmCourseSection,
                            CsfEndDate = fac.EndDate,
                            CsfFaculty = fac.FacultyId,
                            CsfFacultyLoad = fac.LoadFactor,
                            CsfFacultyPct = fac.ResponsibilityPercentage,
                            CsfInstrMethod = fac.InstructionalMethodCode,
                            CsfStartDate = fac.StartDate,
                            CsfPacLpAsgmt = fac.ContractAssignment,
                            CsfTeachingArrangement = fac.TeachingArrangementCode,
                            Recordkey = fac.Id
                        });
                    }
                    return Task.FromResult(results);
                });
                transManagerMock.Setup(t => t.ExecuteAsync<UpdateInstructionalEventRequest, UpdateInstructionalEventResponse>(It.IsAny<UpdateInstructionalEventRequest>())).Returns(Task.FromResult(response));

                // Set up response for instructional methods
                var instrMethods = BuildValidInstrMethodResponse();
                dataReaderMock.Setup<Task<Collection<InstrMethods>>>(acc => acc.BulkReadRecordAsync<InstrMethods>("INSTR.METHODS", "", true)).ReturnsAsync(instrMethods);

                //setup mocking for Stweb Defaults
                var stWebDflt = BuildStwebDefaults(); ;
                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>("ST.PARMS", It.IsAny<string>(), It.IsAny<bool>())).Returns<string, string, bool>(
                    (param, id, repl) => Task.FromResult((stWebDflt.Recordkey == id) ? stWebDflt : null)
                    );

                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>(It.IsAny<string>(), It.IsAny<bool>())).Returns<string, bool>(
                   (id, repl) => Task.FromResult((stWebDflt.Recordkey == id) ? stWebDflt : null)
                   );
                repository = new SectionRepository(cacheProvider, transFactory, logger, apiSettings);



            }

            [TestMethod]
            public async Task SectionRepository_PutSectionMeeting_AddNewMeeting()
            {
                response.CourseSecMeetingId = "4";

                secMeet = await repository.PutSectionMeetingAsync(section, meet3Guid);
                Assert.AreEqual("4", secMeet.Id);
            }

            [TestMethod]
            public async Task SectionRepository_PutSectionMeeting_ChangeMeetingDates()
            {
                response.CourseSecMeetingId = "1";

                var start = secStartDate.AddDays(30);
                var end = secEndDate.AddDays(30);
                section.Meetings[0].StartDate = start;
                section.Meetings[0].EndDate = end;
                secMeet = await repository.PutSectionMeetingAsync(section, meet1Guid);
                Assert.AreEqual(start, secMeet.StartDate);
                Assert.AreEqual(end, secMeet.EndDate);
            }

            [TestMethod]
            public async Task SectionRepository_PutSectionMeeting_ChangeMeetingTimes()
            {
                response.CourseSecMeetingId = "1";

                var start = lecStartTime.Value.AddHours(1);
                var end = lecEndTime.Value.AddHours(1);
                section.Meetings[0].StartTime = start;
                section.Meetings[0].EndTime = end;
                secMeet = await repository.PutSectionMeetingAsync(section, meet1Guid);
                Assert.AreEqual(start.ToLocalTime(), secMeet.StartTime.Value.ToLocalTime());
                Assert.AreEqual(end.ToLocalTime(), secMeet.EndTime.Value.ToLocalTime());
            }

            [TestMethod]
            public async Task SectionRepository_PutSectionMeeting_ChangeMeetingRoom()
            {
                response.CourseSecMeetingId = "1";

                var room = "ARM*140";
                section.Meetings[0].Room = room;
                secMeet = await repository.PutSectionMeetingAsync(section, meet1Guid);
                Assert.AreEqual(room, secMeet.Room);
            }

            [TestMethod]
            public async Task SectionRepository_PutSectionMeeting_ChangeMeetingFaculty()
            {
                response.CourseSecMeetingId = "1";

                section.Meetings[0].RemoveFacultyId(faculty1);
                section.Meetings[0].AddFacultyId(faculty3);
                secMeet = await repository.PutSectionMeetingAsync(section, meet1Guid);
                Assert.AreEqual(1, secMeet.FacultyIds.Count);
                Assert.AreEqual(faculty3, secMeet.FacultyIds[0]);
            }
        }

        [TestClass]
        public class SectionRepository_DeleteSectionMeeting : SectionRepositoryTests
        {
            string secGuid, meet1Guid, meet2Guid, meet3Guid;
            Section section;
            SectionMeeting meeting1, meeting2, meeting3, secMeet;
            IEnumerable<SectionMeeting> secMeets;
            CourseSecMeeting csm;
            CourseSecFaculty csf;
            string faculty1, faculty2, faculty3;
            SectionFaculty lecFaculty, labFaculty1, labFaculty2, semFaculty;
            DateTime secStartDate, secEndDate, semStartDate, semEndDate;
            DateTimeOffset? lecStartTime, lecEndTime, labStartTime, labEndTime, semStartTime, semEndTime;
            List<OfferingDepartment> depts = new List<OfferingDepartment>();
            List<SectionStatusItem> statuses = new List<SectionStatusItem>();
            List<DayOfWeek> MWF = new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Wednesday, DayOfWeek.Friday };
            List<DayOfWeek> TTh = new List<DayOfWeek>() { DayOfWeek.Tuesday, DayOfWeek.Thursday };
            List<DayOfWeek> Wed = new List<DayOfWeek>() { DayOfWeek.Wednesday };
            List<SectionMeeting> meetings = new List<SectionMeeting>();
            List<SectionFaculty> faculty = new List<SectionFaculty>();

            DeleteInstructionalEventRequest request;
            DeleteInstructionalEventResponse response;
            SectionRepository repository;

            [TestInitialize]
            public void Initialize()
            {
                MainInitialize();

                secGuid = Guid.NewGuid().ToString();
                secStartDate = new DateTime(2014, 9, 2);
                secEndDate = new DateTime(2014, 12, 5);
                depts.Add(new OfferingDepartment("RECR", 75m));
                depts.Add(new OfferingDepartment("CECR", 25m));
                var courseLevelCodes = new List<string>() { "100", "CE" };
                statuses.Add(new SectionStatusItem(SectionStatus.Active, "A", new DateTime(2011, 9, 28)));
                section = new Section("1", "1", "01", secStartDate, 3.00m, null, "Underwater Basketweaving", "IN", depts, courseLevelCodes, "CE", statuses) { Guid = secGuid };

                faculty1 = "1234567";
                faculty2 = "2345678";
                faculty3 = "3456789";
                meet1Guid = Guid.NewGuid().ToString();
                meet2Guid = Guid.NewGuid().ToString();
                meet3Guid = Guid.NewGuid().ToString();
                lecStartTime = (new DateTime(1, 1, 1, 9, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                lecEndTime = (new DateTime(1, 1, 1, 9, 50, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                labStartTime = (new DateTime(1, 1, 1, 13, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                labEndTime = (new DateTime(1, 1, 1, 16, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                meeting1 = new SectionMeeting("1", "1", "LEC", secStartDate, secEndDate, "W") { Days = MWF, StartTime = lecStartTime, EndTime = lecEndTime, Room = "ARM*240", Load = 20m, IsOnline = false, Guid = meet1Guid };
                meeting1.AddFacultyId(faculty1);
                meeting2 = new SectionMeeting("2", "1", "LAB", secStartDate, secEndDate, "W") { Days = TTh, StartTime = labStartTime, EndTime = labEndTime, Room = "ARM*131", Load = 10m, IsOnline = false, Guid = meet2Guid };
                meeting2.AddFacultyId(faculty1);
                meeting2.AddFacultyId(faculty2);

                semStartDate = new DateTime(2014, 10, 1);
                semEndDate = new DateTime(2014, 10, 29);
                semStartTime = (new DateTime(1, 1, 1, 19, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                semEndTime = (new DateTime(1, 1, 1, 22, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone);
                meeting3 = new SectionMeeting(null, "1", "SEM", semStartDate, semEndDate, "W") { Days = Wed, StartTime = semStartTime, EndTime = semEndTime, Room = "JFK*200", Load = 5m, IsOnline = false, Guid = meet3Guid };
                meeting3.AddFacultyId(faculty3);

                lecFaculty = new SectionFaculty("1", "1", faculty1, "LEC", secStartDate, secEndDate, 100m) { LoadFactor = 20m };
                labFaculty1 = new SectionFaculty("2", "1", faculty1, "LAB", secStartDate, secEndDate, 50m) { LoadFactor = 5m };
                labFaculty2 = new SectionFaculty("3", "1", faculty2, "LAB", secStartDate, secEndDate, 50m) { LoadFactor = 5m };
                semFaculty = new SectionFaculty(null, "1", faculty3, "SEM", semStartDate, semEndDate, 100m) { LoadFactor = 5m };

                meeting1.AddSectionFaculty(lecFaculty);
                meeting2.AddSectionFaculty(labFaculty1);
                meeting2.AddSectionFaculty(labFaculty2);
                meeting3.AddSectionFaculty(semFaculty);

                section.AddSectionMeeting(meeting1);
                section.AddSectionMeeting(meeting2);
                section.AddSectionMeeting(meeting3);
                section.AddSectionFaculty(lecFaculty);
                section.AddSectionFaculty(labFaculty1);
                section.AddSectionFaculty(labFaculty2);
                section.AddSectionFaculty(semFaculty);

                meetings.Add(meeting1);
                meetings.Add(meeting2);
                meetings.Add(meeting3);
                faculty.Add(lecFaculty);
                faculty.Add(labFaculty1);
                faculty.Add(labFaculty2);
                faculty.Add(semFaculty);

                request = new DeleteInstructionalEventRequest();
                response = new DeleteInstructionalEventResponse()
                {
                    DeleteInstructionalEventErrors = new List<DeleteInstructionalEventErrors>(),
                    DeleteInstructionalEventWarnings = new List<DeleteInstructionalEventWarnings>()
                };

                //newMeeting = new SectionMeeting("5", "1", "SEM", semStartDate, semEndDate, "W") { Days = Wed, StartTime = semStartTime, EndTime = semEndTime, Room = "JFK*200", Load = 5m, IsOnline = false, Guid = Guid.NewGuid().ToString().ToLowerInvariant() };
                csm = new CourseSecMeeting()
                {
                    CsmBldg = "JFK",
                    CsmCourseSection = "1",
                    CsmEndDate = semEndDate,
                    CsmEndTime = semEndTime.Value.DateTime,
                    CsmFaculty = new List<string>() { faculty3 },
                    CsmFrequency = "W",
                    CsmFriday = "N",
                    CsmInstrMethod = "SEM",
                    CsmLoad = 5m,
                    CsmMonday = "N",
                    CsmRoom = "200",
                    CsmSaturday = "N",
                    CsmStartDate = semStartDate,
                    CsmStartTime = semStartTime.Value.DateTime,
                    CsmSunday = "N",
                    CsmThursday = "N",
                    CsmTuesday = "N",
                    CsmWednesday = "Y",
                    RecordGuid = Guid.NewGuid().ToString().ToLowerInvariant(),
                    Recordkey = "4"
                };
                csf = new CourseSecFaculty()
                {
                    CsfCourseSection = "1",
                    CsfEndDate = semEndDate,
                    CsfFaculty = faculty3,
                    CsfFacultyLoad = 5m,
                    CsfFacultyPct = 100m,
                    CsfInstrMethod = "SEM",
                    CsfStartDate = semStartDate,
                    Recordkey = "5"
                };

                CdDefaults cdDefaults;
                BuildLdmConfiguration(dataReaderMock, out cdDefaults);

                dataReaderMock.Setup(r => r.ReadRecordAsync<CourseSecMeeting>(It.IsAny<string>(), true)).Returns<string, bool>((id, flag) =>
                {
                    if (id == "4")
                    {
                        return Task.FromResult(csm);
                    }
                    var mtg = meetings.FirstOrDefault(x => x.Id == id);
                    if (mtg == null) return Task.FromResult(new CourseSecMeeting());

                    var room = mtg.Room.Contains('*') ? mtg.Room.Split('*') : new string[2] { mtg.Room, string.Empty };
                    return Task.FromResult(new CourseSecMeeting()
                    {
                        Recordkey = mtg.Id,
                        RecordGuid = mtg.Guid,
                        CsmBldg = room[0],
                        CsmCourseSection = mtg.SectionId,
                        CsmEndDate = mtg.EndDate,
                        CsmEndTime = mtg.EndTime.Value.DateTime,
                        CsmFaculty = mtg.FacultyIds.ToList(),
                        CsmFrequency = mtg.Frequency,
                        CsmInstrMethod = mtg.InstructionalMethodCode,
                        CsmLoad = mtg.Load,
                        CsmRoom = room[1],
                        CsmStartDate = mtg.StartDate,
                        CsmStartTime = mtg.StartTime.Value.DateTime,
                        CsmSunday = mtg.Days.Contains(DayOfWeek.Sunday) ? "Y" : "N",
                        CsmMonday = mtg.Days.Contains(DayOfWeek.Monday) ? "Y" : "N",
                        CsmTuesday = mtg.Days.Contains(DayOfWeek.Tuesday) ? "Y" : "N",
                        CsmWednesday = mtg.Days.Contains(DayOfWeek.Wednesday) ? "Y" : "N",
                        CsmThursday = mtg.Days.Contains(DayOfWeek.Thursday) ? "Y" : "N",
                        CsmFriday = mtg.Days.Contains(DayOfWeek.Friday) ? "Y" : "N",
                        CsmSaturday = mtg.Days.Contains(DayOfWeek.Saturday) ? "Y" : "N"
                    });
                });
                dataReaderMock.Setup(r => r.BulkReadRecordAsync<CourseSecFaculty>(It.IsAny<string>(), true)).Returns<string, bool>((id, flag) =>
                {
                    var results = new Collection<CourseSecFaculty>() { csf };
                    foreach (var fac in faculty)
                    {
                        results.Add(new CourseSecFaculty()
                        {
                            //CsfCourseSection = request.CsmCourseSection,
                            CsfEndDate = fac.EndDate,
                            CsfFaculty = fac.FacultyId,
                            CsfFacultyLoad = fac.LoadFactor,
                            CsfFacultyPct = fac.ResponsibilityPercentage,
                            CsfInstrMethod = fac.InstructionalMethodCode,
                            CsfStartDate = fac.StartDate,
                            CsfPacLpAsgmt = fac.ContractAssignment,
                            CsfTeachingArrangement = fac.TeachingArrangementCode,
                            Recordkey = fac.Id
                        });
                    }
                    return Task.FromResult(results);
                });
                transManagerMock.Setup(t => t.ExecuteAsync<DeleteInstructionalEventRequest, DeleteInstructionalEventResponse>(It.IsAny<DeleteInstructionalEventRequest>())).Returns(Task.FromResult(response));

                // Set up response for instructional methods
                var instrMethods = BuildValidInstrMethodResponse();
                dataReaderMock.Setup<Task<Collection<InstrMethods>>>(acc => acc.BulkReadRecordAsync<InstrMethods>("INSTR.METHODS", "", true)).ReturnsAsync(instrMethods);

                //setup mocking for Stweb Defaults
                var stWebDflt = BuildStwebDefaults(); ;
                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>("ST.PARMS", It.IsAny<string>(), It.IsAny<bool>())).Returns<string, string, bool>(
                    (param, id, repl) => Task.FromResult((stWebDflt.Recordkey == id) ? stWebDflt : null)
                    );

                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>(It.IsAny<string>(), It.IsAny<bool>())).Returns<string, bool>(
                   (id, repl) => Task.FromResult((stWebDflt.Recordkey == id) ? stWebDflt : null)
                   );
                repository = new SectionRepository(cacheProvider, transFactory, logger, apiSettings);



            }

            [TestMethod]
            public async Task SectionRepository_DeleteSectionMeeting_AddNewMeeting()
            {
                await repository.DeleteSectionMeetingAsync(meet3Guid, faculty);
            }
        }

        [TestClass]
        public class SectionRepository_GradeImportTests
        {
            Mock<IColleagueTransactionFactory> transFactoryMock;
            Mock<IColleagueTransactionInvoker> transManagerMock;
            Mock<ICacheProvider> cacheProviderMock;
            Mock<ILogger> loggerMock;
            ApiSettings apiSettingsMock;
            SectionRepository sectionRepo;

            [TestInitialize]
            public void Initialize()
            {
                loggerMock = new Mock<ILogger>();
                cacheProviderMock = new Mock<ICacheProvider>();
                transFactoryMock = new Mock<IColleagueTransactionFactory>();
                transManagerMock = new Mock<IColleagueTransactionInvoker>();
                apiSettingsMock = new ApiSettings("null");

                var transactionResponse = GetTransactionResponse();
                var grades = GetSectionGrades();
                // Identify the request based on the SectionGrades entity returned by GetSectionGrades by having 9 items to post and the sectionID.
                // Return GetTransactionResponse
                transManagerMock.Setup<Task<ImportGradesFromILPResponse>>(mgr => mgr.ExecuteAsync<ImportGradesFromILPRequest, ImportGradesFromILPResponse>(
                    It.Is<ImportGradesFromILPRequest>(x => x.ItemsToPostInput.Count() == 9 &&
                        x.SectionId == grades.SectionId)
                    )).Returns(Task.FromResult(transactionResponse));

                // When the SectionId is "TrueForceVerify", and 8 items to post, ForceNoVerify is true, and CheckForLocks is true, 
                // return an error message "WasTrue"
                ItemsToPostOutput trueOutput = new ItemsToPostOutput { ItemOutPerson = "Person1", ItemErrorMsg = "WasTrue", ItemOutStatus = "failure"};
                ImportGradesFromILPResponse trueResponse = 
                    new ImportGradesFromILPResponse { SectionId = "TrueForceVerify", ItemsToPostOutput = new List<ItemsToPostOutput>() {trueOutput} };
                transManagerMock.Setup<Task<ImportGradesFromILPResponse>>(mgr => mgr.ExecuteAsync<ImportGradesFromILPRequest, ImportGradesFromILPResponse>(
                    It.Is<ImportGradesFromILPRequest>(x => x.ItemsToPostInput.Count() == 8 &&
                        x.SectionId == "TrueForceVerify" && x.ForceNoVerify == true && x.CheckForLocks == true)                         
                    )).Returns(Task.FromResult(trueResponse));

                // Same but return "WasFalse" when SectionId is "FalseForceVerify", ForceNoVerify is false, and CheckForLocks is false
                ItemsToPostOutput falseOutput = new ItemsToPostOutput { ItemOutPerson = "Person1", ItemErrorMsg = "WasFalse", ItemOutStatus = "failure" };
                ImportGradesFromILPResponse falseResponse =
                    new ImportGradesFromILPResponse { SectionId = "FalseForceVerify", ItemsToPostOutput = new List<ItemsToPostOutput>() { falseOutput } };
                transManagerMock.Setup<Task<ImportGradesFromILPResponse>>(mgr => mgr.ExecuteAsync<ImportGradesFromILPRequest, ImportGradesFromILPResponse>(
                    It.Is<ImportGradesFromILPRequest>(x => x.ItemsToPostInput.Count() == 8 &&
                        x.SectionId == "FalseForceVerify" && x.ForceNoVerify == false && x.CheckForLocks == false)
                    )).Returns(Task.FromResult(falseResponse));

                // When the SectionId is "TestCallerType", and 8 items to post, ForceNoVerify is true, and CheckForLocks is true, and CallerType is "ILP",
                // return an error message "ILPCaller"
                ItemsToPostOutput ilpOutput = new ItemsToPostOutput { ItemOutPerson = "Person1", ItemErrorMsg = "IlpCaller", ItemOutStatus = "failure" };
                ImportGradesFromILPResponse ilpResponse =
                    new ImportGradesFromILPResponse { SectionId = "TestCallerType", ItemsToPostOutput = new List<ItemsToPostOutput>() { ilpOutput } };
                transManagerMock.Setup<Task<ImportGradesFromILPResponse>>(mgr => mgr.ExecuteAsync<ImportGradesFromILPRequest, ImportGradesFromILPResponse>(
                    It.Is<ImportGradesFromILPRequest>(x => x.ItemsToPostInput.Count() == 8 &&
                        x.SectionId == "TestCallerType" && x.ForceNoVerify == true && x.CheckForLocks == true && x.CallerType == "ILP")
                    )).Returns(Task.FromResult(ilpResponse));

                // When the SectionId is "TestCallerType", and 8 items to post, ForceNoVerify is true, and CheckForLocks is true, and CallerType is "Standard"                ,
                // return an error message "StandardCaller"
                ItemsToPostOutput standardCallerOutput = new ItemsToPostOutput { ItemOutPerson = "Person1", ItemErrorMsg = "StandardCaller", ItemOutStatus = "failure" };
                ImportGradesFromILPResponse standardResponse =
                    new ImportGradesFromILPResponse { SectionId = "TestCallerType", ItemsToPostOutput = new List<ItemsToPostOutput>() { standardCallerOutput } };
                transManagerMock.Setup<Task<ImportGradesFromILPResponse>>(mgr => mgr.ExecuteAsync<ImportGradesFromILPRequest, ImportGradesFromILPResponse>(
                    It.Is<ImportGradesFromILPRequest>(x => x.ItemsToPostInput.Count() == 8 &&
                        x.SectionId == "TestCallerType" && x.ForceNoVerify == true && x.CheckForLocks == true && x.CallerType == "Standard")
                    )).Returns(Task.FromResult(standardResponse));

                
                transFactoryMock.Setup(transFac => transFac.GetTransactionInvoker()).Returns(transManagerMock.Object);

                sectionRepo = new SectionRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettingsMock);

                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
                   x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                   .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

            }

            [TestCleanup]
            public void Cleanup()
            {
                transFactoryMock = null;
                cacheProviderMock = null;
                transManagerMock = null;
                sectionRepo = null;
                apiSettingsMock = null;
            }

            [TestMethod]
            public async Task SectionRepository_ImportGrades()
            {
                // Test basic execution. Mocked ImportGradesAsync should return a response with the same
                // student ID as GetSectionGrades that is passed in as the request.
                var grades = GetSectionGrades();
                var response = await sectionRepo.ImportGradesAsync(grades, false, true, GradesPutCallerTypes.ILP);

                foreach (var r in response)
                {
                    Assert.AreEqual(GetSectionGrades().StudentGrades[0].StudentId, r.StudentId);
                }
            }
            
            [TestMethod]
            public async Task SectionRepository_GradesForceNoVerifyTrueFlagAndCheckForLocksTrueFlagPassedSuccessfully()
            {
                var grades = GetSectionGrades();
                // Remove a midterm grade to get the total items to post down to 8
                grades.StudentGrades[0].MidtermGrade1 = null;
                grades.SectionId = "TrueForceVerify";
                // Tests that ForceNoVerify is passed to the CTX when true
                IEnumerable<SectionGradeResponse> response = await sectionRepo.ImportGradesAsync(grades, true, true, GradesPutCallerTypes.ILP);
                Assert.AreEqual(response.First().Errors[0].Message, "WasTrue");
            }

            [TestMethod]
            public async Task SectionRepository_GradesForceNoVerifyFalseFlagAndCheckForLocksFalseFlagPassedSuccessfully()
            {
                var grades = GetSectionGrades();
                // Remove a midterm grade to get the total items to post down to 8
                grades.StudentGrades[0].MidtermGrade1 = null;
                grades.SectionId = "FalseForceVerify";
                // Tests that ForceNoVerify is passed to the CTX when false
                IEnumerable<SectionGradeResponse> response = await sectionRepo.ImportGradesAsync(grades, false, false, GradesPutCallerTypes.ILP);
                Assert.AreEqual(response.First().Errors[0].Message, "WasFalse");
            }

            [TestMethod]
            public async Task SectionRepository_GradesIlpCallerPassedSuccessfully()
            {
                var grades = GetSectionGrades();
                // Remove a midterm grade to get the total items to post down to 8
                grades.StudentGrades[0].MidtermGrade1 = null;
                grades.SectionId = "TestCallerType";
                // Tests that ForceNoVerify is passed to the CTX when false
                IEnumerable<SectionGradeResponse> response = await sectionRepo.ImportGradesAsync(grades, true, true, GradesPutCallerTypes.ILP);
                Assert.AreEqual(response.First().Errors[0].Message, "IlpCaller");
            }

            [TestMethod]
            public async Task SectionRepository_GradesStandardCallerPassedSuccessfully()
            {
                var grades = GetSectionGrades();
                // Remove a midterm grade to get the total items to post down to 8
                grades.StudentGrades[0].MidtermGrade1 = null;
                grades.SectionId = "TestCallerType";
                // Tests that ForceNoVerify is passed to the CTX when false
                IEnumerable<SectionGradeResponse> response = await sectionRepo.ImportGradesAsync(grades, true, true, GradesPutCallerTypes.Standard);
                Assert.AreEqual(response.First().Errors[0].Message, "StandardCaller");
            }


            private SectionGrades GetSectionGrades()
            {
                var grades = new SectionGrades();
                grades.SectionId = "123";
                var grade = new StudentGrade();
                grade.StudentId = "101";
                grade.MidtermGrade1 = "A";
                grade.MidtermGrade2 = "B";
                grade.MidtermGrade3 = "C";
                grade.MidtermGrade4 = "D";
                grade.MidtermGrade5 = "E";
                grade.MidtermGrade6 = "F";
                grade.FinalGrade = "G";
                grade.FinalGradeExpirationDate = DateTime.Now;
                grade.LastAttendanceDate = DateTime.Now;
                grade.NeverAttended = true;
                grades.StudentGrades = new List<StudentGrade>();
                grades.StudentGrades.Add(grade);

                return grades;
            }

            private ImportGradesFromILPResponse GetTransactionResponse()
            {
                ImportGradesFromILPResponse response = new ImportGradesFromILPResponse();
                response.SectionId = GetSectionGrades().SectionId;
                response.ErrorCode = null; // success

                response.ItemsToPostOutput.Add(new ItemsToPostOutput() { ItemOutStatus = "success", ItemOutPerson = "101" });

                return response;
            }
        }

        [TestClass]
        public class SectionRepository_GradesBuildImportPostItemsTests
        {
            Mock<IColleagueTransactionFactory> transFactoryMock;
            Mock<IColleagueTransactionInvoker> transManagerMock;
            Mock<ICacheProvider> cacheProviderMock;
            Mock<ILogger> loggerMock;
            ApiSettings apiSettingsMock;
            PrivateObject sectionRepo;

            [TestInitialize]
            public void Initialize()
            {
                loggerMock = new Mock<ILogger>();
                cacheProviderMock = new Mock<ICacheProvider>();
                transFactoryMock = new Mock<IColleagueTransactionFactory>();
                transManagerMock = new Mock<IColleagueTransactionInvoker>();
                apiSettingsMock = new ApiSettings("null");

                //sectionRepo = new SectionRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettingsMock);
                sectionRepo = new PrivateObject(typeof(SectionRepository), cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettingsMock);

                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
                       x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                       .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

            }

            [TestCleanup]
            public void Cleanup()
            {
                transFactoryMock = null;
                cacheProviderMock = null;
                transManagerMock = null;
                sectionRepo = null;
                apiSettingsMock = null;
            }

            [TestMethod]
            public void SectionRepository_GradesBuildImportPostItems()
            {
                var grade = GetStudentGrade();
                var postItems = (List<ItemsToPostInput>)sectionRepo.Invoke("BuildImportPostItems", grade);
                Assert.IsTrue(postItems.All(x => x.ItemPerson == grade.StudentId));
                Assert.IsTrue(postItems.Any(x => x.ItemCode == "MidtermGrade1" && x.ItemValue == grade.MidtermGrade1));
                Assert.IsTrue(postItems.Any(x => x.ItemCode == "MidtermGrade2" && x.ItemValue == grade.MidtermGrade2));
                Assert.IsTrue(postItems.Any(x => x.ItemCode == "MidtermGrade3" && x.ItemValue == grade.MidtermGrade3));
                Assert.IsTrue(postItems.Any(x => x.ItemCode == "MidtermGrade4" && x.ItemValue == grade.MidtermGrade4));
                Assert.IsTrue(postItems.Any(x => x.ItemCode == "MidtermGrade5" && x.ItemValue == grade.MidtermGrade5));
                Assert.IsTrue(postItems.Any(x => x.ItemCode == "MidtermGrade6" && x.ItemValue == grade.MidtermGrade6));
                Assert.IsTrue(postItems.Any(x => x.ItemCode == "FinalGrade" && x.ItemValue == grade.FinalGrade + "|" + grade.FinalGradeExpirationDate.Value.ToString("yyyy/MM/dd")));
                Assert.IsTrue(postItems.Any(x => x.ItemCode == "LastAttendanceDate" && x.ItemValue == grade.LastAttendanceDate.Value.ToString("yyyy/MM/dd")));
                Assert.IsTrue(postItems.Any(x => x.ItemCode == "NeverAttended" && x.ItemValue == "1"));

                grade = GetMinimumStudentGrade();
                postItems = (List<ItemsToPostInput>)sectionRepo.Invoke("BuildImportPostItems", grade);
                Assert.IsTrue(postItems.All(x => x.ItemPerson == grade.StudentId));
                Assert.IsTrue(postItems.Any(x => x.ItemCode == "MidtermGrade1" && x.ItemValue == grade.MidtermGrade1));
                Assert.IsTrue(postItems.Any(x => x.ItemCode == "NeverAttended" && x.ItemValue == "0"));

                grade = GetStudentGradeWithClearFlags();
                postItems = (List<ItemsToPostInput>)sectionRepo.Invoke("BuildImportPostItems", grade);
                Assert.IsTrue(postItems.All(x => x.ItemPerson == grade.StudentId));
                Assert.IsTrue(postItems.Any(x => x.ItemCode == "FinalGrade" && x.ItemValue == grade.FinalGrade + "|"));
                Assert.IsTrue(postItems.Any(x => x.ItemCode == "LastAttendanceDate" && x.ItemValue == ""));

                grade = GetStudentGradeOnlyExpireDate();
                postItems = (List<ItemsToPostInput>)sectionRepo.Invoke("BuildImportPostItems", grade);
                Assert.IsTrue(postItems.All(x => x.ItemPerson == grade.StudentId));
                Assert.IsTrue(postItems.Any(x => x.ItemCode == "FinalGradeExpirationDate" && x.ItemValue == grade.FinalGradeExpirationDate.Value.ToString("yyyy/MM/dd")));

                grade = GetStudentGradeOnlyClearExpireFlag();
                postItems = (List<ItemsToPostInput>)sectionRepo.Invoke("BuildImportPostItems", grade);
                Assert.IsTrue(postItems.All(x => x.ItemPerson == grade.StudentId));
                Assert.IsTrue(postItems.Any(x => x.ItemCode == "FinalGradeExpirationDate" && x.ItemValue == ""));
            }

            [TestMethod]
            public void SectionRepository_GradesConvertTransactionOutputToDomainEntities()
            {
                ImportGradesFromILPResponse transactionOutput = new ImportGradesFromILPResponse();

                // empty input and output
                var sectionGradeResponses = (List<SectionGradeResponse>)sectionRepo.Invoke("ConvertImportOutputToDomainEntities", transactionOutput);
                Assert.AreEqual(sectionGradeResponses.Count(), 0);

                // single success record
                transactionOutput = GetSingleSuccessResponse_ForSingleStudent();
                sectionGradeResponses = (List<SectionGradeResponse>)sectionRepo.Invoke("ConvertImportOutputToDomainEntities", transactionOutput);
                Assert.AreEqual(transactionOutput.ItemsToPostOutput[0].ItemOutPerson, sectionGradeResponses[0].StudentId);
                Assert.AreEqual("success", sectionGradeResponses[0].Status);

                // single failure record
                transactionOutput = GetSingleFailureResponse_ForSingleStudent();
                sectionGradeResponses = (List<SectionGradeResponse>)sectionRepo.Invoke("ConvertImportOutputToDomainEntities", transactionOutput);
                Assert.AreEqual(transactionOutput.ItemsToPostOutput[0].ItemOutPerson, sectionGradeResponses[0].StudentId);
                Assert.AreEqual("failure", sectionGradeResponses[0].Status);
                Assert.IsTrue(sectionGradeResponses[0].Errors.Count() == 1);
                Assert.AreEqual(transactionOutput.ItemsToPostOutput[0].ItemOutCode, sectionGradeResponses[0].Errors[0].Property);
                Assert.AreEqual(transactionOutput.ItemsToPostOutput[0].ItemErrorMsg, sectionGradeResponses[0].Errors[0].Message);

                // mixed response for single person
                transactionOutput = GetMixedResponse_ForSingleStudent();
                sectionGradeResponses = (List<SectionGradeResponse>)sectionRepo.Invoke("ConvertImportOutputToDomainEntities", transactionOutput);
                Assert.AreEqual(transactionOutput.ItemsToPostOutput[0].ItemOutPerson, sectionGradeResponses[0].StudentId);
                Assert.AreEqual("failure", sectionGradeResponses[0].Status);
                Assert.IsTrue(sectionGradeResponses[0].Errors.Count() == 1);
                Assert.AreEqual(transactionOutput.ItemsToPostOutput.First(x => x.ItemOutStatus == "failure").ItemOutCode, sectionGradeResponses[0].Errors[0].Property);
                Assert.AreEqual(transactionOutput.ItemsToPostOutput.First(x => x.ItemOutStatus == "failure").ItemErrorMsg, sectionGradeResponses[0].Errors[0].Message);

                // multiple success for single person
                transactionOutput = GetMultiSuccessResponse_ForSingleStudent();
                sectionGradeResponses = (List<SectionGradeResponse>)sectionRepo.Invoke("ConvertImportOutputToDomainEntities", transactionOutput);
                Assert.AreEqual(transactionOutput.ItemsToPostOutput[0].ItemOutPerson, sectionGradeResponses[0].StudentId);
                Assert.AreEqual("success", sectionGradeResponses[0].Status);

                // multiple failures for single person
                transactionOutput = GetMultiFailureResponse_ForSingleStudent();
                sectionGradeResponses = (List<SectionGradeResponse>)sectionRepo.Invoke("ConvertImportOutputToDomainEntities", transactionOutput);
                Assert.AreEqual(transactionOutput.ItemsToPostOutput[0].ItemOutPerson, sectionGradeResponses[0].StudentId);
                Assert.AreEqual("failure", sectionGradeResponses[0].Status);
                Assert.IsTrue(sectionGradeResponses[0].Errors.Count() > 1);
                transactionOutput.ItemsToPostOutput.ForEach(x =>
                {
                    Assert.IsTrue(sectionGradeResponses[0].Errors.Any(z => z.Property == x.ItemOutCode));
                    Assert.IsTrue(sectionGradeResponses[0].Errors.Any(z => z.Message == x.ItemErrorMsg));
                });

                // two people, both success
                transactionOutput = GetSuccessResponse_ForMultiStudent();
                sectionGradeResponses = (List<SectionGradeResponse>)sectionRepo.Invoke("ConvertImportOutputToDomainEntities", transactionOutput);
                transactionOutput.ItemsToPostOutput.ForEach(x =>
                {
                    Assert.IsTrue(sectionGradeResponses.Any(z => z.StudentId == x.ItemOutPerson && z.Status == "success"));
                });

                // two people, both failure
                transactionOutput = GetFailureResponse_ForMultiStudent();
                sectionGradeResponses = (List<SectionGradeResponse>)sectionRepo.Invoke("ConvertImportOutputToDomainEntities", transactionOutput);
                transactionOutput.ItemsToPostOutput.ForEach(x =>
                {
                    Assert.IsTrue(sectionGradeResponses.Any(z => z.StudentId == x.ItemOutPerson && z.Status == "failure"));
                    Assert.IsTrue(sectionGradeResponses.Any(z => z.StudentId == x.ItemOutPerson && z.Errors.Any(y => y.Property == x.ItemOutCode)));
                    Assert.IsTrue(sectionGradeResponses.Any(z => z.StudentId == x.ItemOutPerson && z.Errors.Any(y => y.Message == x.ItemErrorMsg)));
                });

                // two people, one success, one failure
                transactionOutput = GetFailureResponse_ForMultiStudent();
                sectionGradeResponses = (List<SectionGradeResponse>)sectionRepo.Invoke("ConvertImportOutputToDomainEntities", transactionOutput);
                transactionOutput.ItemsToPostOutput.ForEach(x =>
                {
                    Assert.IsTrue(sectionGradeResponses.Any(z => z.StudentId == x.ItemOutPerson && z.Status == x.ItemOutStatus));

                    if (x.ItemOutStatus == "success")
                    {
                        Assert.IsTrue(sectionGradeResponses.First(z => z.StudentId == x.ItemOutStatus).Errors.Count() == 0);
                    }

                    if (x.ItemOutStatus == "failure")
                    {
                        Assert.IsTrue(sectionGradeResponses.Any(z => z.StudentId == x.ItemOutPerson && z.Errors.Any(y => y.Property == x.ItemOutCode)));
                        Assert.IsTrue(sectionGradeResponses.Any(z => z.StudentId == x.ItemOutPerson && z.Errors.Any(y => y.Message == x.ItemErrorMsg)));
                    }
                });
            }

            private StudentGrade GetStudentGrade()
            {
                var grade = new StudentGrade();
                grade.StudentId = "101";
                grade.MidtermGrade1 = "A";
                grade.MidtermGrade2 = "B";
                grade.MidtermGrade3 = "C";
                grade.MidtermGrade4 = "D";
                grade.MidtermGrade5 = "E";
                grade.MidtermGrade6 = "F";
                grade.FinalGrade = "G";
                grade.FinalGradeExpirationDate = DateTime.Now;
                grade.LastAttendanceDate = DateTime.Now;
                grade.NeverAttended = true;
                return grade;
            }

            private StudentGrade GetMinimumStudentGrade()
            {
                var grade = new StudentGrade();
                grade.StudentId = "102";
                grade.MidtermGrade1 = "A";
                grade.FinalGradeExpirationDate = null;
                grade.LastAttendanceDate = null;
                grade.NeverAttended = false;
                return grade;
            }

            private StudentGrade GetStudentGradeWithClearFlags()
            {
                var grade = new StudentGrade();
                grade.StudentId = "105";
                grade.FinalGrade = "I";
                grade.ClearLastAttendanceDateFlag = true;
                grade.ClearFinalGradeExpirationDateFlag = true;
                return grade;
            }

            private StudentGrade GetStudentGradeOnlyExpireDate()
            {
                var grade = new StudentGrade();
                grade.StudentId = "12345";
                grade.FinalGradeExpirationDate = DateTime.Now;
                return grade;
            }

            private StudentGrade GetStudentGradeOnlyClearExpireFlag()
            {
                var grade = new StudentGrade();
                grade.StudentId = "23456";
                grade.ClearFinalGradeExpirationDateFlag = true;
                return grade;
            }

            private ImportGradesFromILPResponse GetSingleSuccessResponse_ForSingleStudent()
            {
                ImportGradesFromILPResponse response = new ImportGradesFromILPResponse();
                ItemsToPostOutput outputItem = new ItemsToPostOutput();
                outputItem.ItemOutPerson = "1";
                outputItem.ItemOutStatus = "success";
                response.ItemsToPostOutput.Add(outputItem);
                return response;
            }

            private ImportGradesFromILPResponse GetSingleFailureResponse_ForSingleStudent()
            {
                ImportGradesFromILPResponse response = new ImportGradesFromILPResponse();
                ItemsToPostOutput outputItem = new ItemsToPostOutput();
                outputItem.ItemOutPerson = "1";
                outputItem.ItemOutStatus = "failure";
                outputItem.ItemErrorMsg = "some error message";
                outputItem.ItemOutCode = "some output code";
                response.ItemsToPostOutput.Add(outputItem);
                return response;
            }

            private ImportGradesFromILPResponse GetMixedResponse_ForSingleStudent()
            {
                ImportGradesFromILPResponse response = new ImportGradesFromILPResponse();

                ItemsToPostOutput outputItem = new ItemsToPostOutput();
                outputItem.ItemOutPerson = "1";
                outputItem.ItemOutStatus = "success";
                response.ItemsToPostOutput.Add(outputItem);

                outputItem = new ItemsToPostOutput();
                outputItem.ItemOutPerson = "1";
                outputItem.ItemOutStatus = "failure";
                outputItem.ItemErrorMsg = "some error message";
                outputItem.ItemOutCode = "some output code";
                response.ItemsToPostOutput.Add(outputItem);

                return response;
            }

            private ImportGradesFromILPResponse GetMultiSuccessResponse_ForSingleStudent()
            {
                ImportGradesFromILPResponse response = new ImportGradesFromILPResponse();
                ItemsToPostOutput outputItem = new ItemsToPostOutput();
                outputItem.ItemOutPerson = "1";
                outputItem.ItemOutStatus = "success";

                outputItem = new ItemsToPostOutput();
                outputItem.ItemOutPerson = "1";
                outputItem.ItemOutStatus = "success";

                response.ItemsToPostOutput.Add(outputItem);
                return response;
            }

            private ImportGradesFromILPResponse GetMultiFailureResponse_ForSingleStudent()
            {
                ImportGradesFromILPResponse response = new ImportGradesFromILPResponse();
                ItemsToPostOutput outputItem = new ItemsToPostOutput();
                outputItem.ItemOutPerson = "1";
                outputItem.ItemOutStatus = "failure";
                outputItem.ItemErrorMsg = "some error message";
                outputItem.ItemOutCode = "some output code";
                response.ItemsToPostOutput.Add(outputItem);

                outputItem = new ItemsToPostOutput();
                outputItem.ItemOutPerson = "1";
                outputItem.ItemOutStatus = "failure";
                outputItem.ItemErrorMsg = "some other error message";
                outputItem.ItemOutCode = "some other output code";
                response.ItemsToPostOutput.Add(outputItem);

                return response;
            }

            private ImportGradesFromILPResponse GetSuccessResponse_ForMultiStudent()
            {
                ImportGradesFromILPResponse response = new ImportGradesFromILPResponse();
                ItemsToPostOutput outputItem = new ItemsToPostOutput();
                outputItem.ItemOutPerson = "1";
                outputItem.ItemOutStatus = "success";
                response.ItemsToPostOutput.Add(outputItem);

                outputItem = new ItemsToPostOutput();
                outputItem.ItemOutPerson = "2";
                outputItem.ItemOutStatus = "success";
                response.ItemsToPostOutput.Add(outputItem);

                return response;
            }

            private ImportGradesFromILPResponse GetFailureResponse_ForMultiStudent()
            {
                ImportGradesFromILPResponse response = new ImportGradesFromILPResponse();
                ItemsToPostOutput outputItem = new ItemsToPostOutput();
                outputItem.ItemOutPerson = "1";
                outputItem.ItemOutStatus = "failure";
                outputItem.ItemErrorMsg = "some error message";
                outputItem.ItemOutCode = "some output code";
                response.ItemsToPostOutput.Add(outputItem);

                outputItem = new ItemsToPostOutput();
                outputItem.ItemOutPerson = "2";
                outputItem.ItemOutStatus = "failure";
                outputItem.ItemErrorMsg = "some other error message";
                outputItem.ItemOutCode = "some other output code";
                response.ItemsToPostOutput.Add(outputItem);

                return response;
            }

            private ImportGradesFromILPResponse GetMixedResponse_ForMultiStudent()
            {
                ImportGradesFromILPResponse response = new ImportGradesFromILPResponse();
                ItemsToPostOutput outputItem = new ItemsToPostOutput();
                outputItem.ItemOutPerson = "1";
                outputItem.ItemOutStatus = "failure";
                outputItem.ItemErrorMsg = "some error message";
                outputItem.ItemOutCode = "some output code";
                response.ItemsToPostOutput.Add(outputItem);

                outputItem = new ItemsToPostOutput();
                outputItem.ItemOutPerson = "2";
                outputItem.ItemOutStatus = "success";
                response.ItemsToPostOutput.Add(outputItem);

                return response;
            }
        }

        [TestClass]
        public class SectionRepository_GradeImportFailureTests
        {
            Mock<IColleagueTransactionFactory> transFactoryMock;
            Mock<IColleagueTransactionInvoker> transManagerMock;
            Mock<ICacheProvider> cacheProviderMock;
            Mock<ILogger> loggerMock;
            ApiSettings apiSettingsMock;
            SectionRepository sectionRepo;

            [TestInitialize]
            public void Initialize()
            {
                loggerMock = new Mock<ILogger>();
                cacheProviderMock = new Mock<ICacheProvider>();
                transFactoryMock = new Mock<IColleagueTransactionFactory>();
                transManagerMock = new Mock<IColleagueTransactionInvoker>();
                apiSettingsMock = new ApiSettings("null");

                var transactionResponse = GetTransactionResponse();
                var grades = GetSectionGrades();
                // Identify transaction request based on GetSectionGrades by having 9 items and the section ID. Return GetTransactionResponse which contains
                // failures.
                transManagerMock.Setup<Task<ImportGradesFromILPResponse>>(mgr => mgr.ExecuteAsync<ImportGradesFromILPRequest, ImportGradesFromILPResponse>(
                    It.Is<ImportGradesFromILPRequest>(x => x.ItemsToPostInput.Count() == 9 &&
                        x.SectionId == grades.SectionId)
                    )).Returns(Task.FromResult(transactionResponse));

                transFactoryMock.Setup(transFac => transFac.GetTransactionInvoker()).Returns(transManagerMock.Object);

                sectionRepo = new SectionRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettingsMock);

                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
                   x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                   .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

            }

            [TestCleanup]
            public void Cleanup()
            {
                transFactoryMock = null;
                cacheProviderMock = null;
                transManagerMock = null;
                sectionRepo = null;
                apiSettingsMock = null;
            }

            [TestMethod]
            public async Task SectionRepository_ImportGradesFailure()
            {
                var grades = GetSectionGrades();
                var response = await sectionRepo.ImportGradesAsync(grades, false, true, GradesPutCallerTypes.ILP);
                var expectedData = GetTransactionResponse();

                foreach (var r in response)
                {
                    Assert.AreEqual(GetSectionGrades().StudentGrades[0].StudentId, r.StudentId);
                    Assert.AreEqual("failure", r.Status);
                    Assert.IsTrue(r.Errors.Count() > 0);
                }
            }

            private SectionGrades GetSectionGrades()
            {
                var grades = new SectionGrades();
                grades.SectionId = "123";
                var grade = new StudentGrade();
                grade.StudentId = "101";
                grade.MidtermGrade1 = "A";
                grade.MidtermGrade2 = "B";
                grade.MidtermGrade3 = "C";
                grade.MidtermGrade4 = "D";
                grade.MidtermGrade5 = "E";
                grade.MidtermGrade6 = "F";
                grade.FinalGrade = "G";
                grade.FinalGradeExpirationDate = DateTime.Now;
                grade.ClearFinalGradeExpirationDateFlag = false;
                grade.LastAttendanceDate = DateTime.Now;
                grade.ClearLastAttendanceDateFlag = false;
                grade.NeverAttended = true;
                grades.StudentGrades = new List<StudentGrade>();
                grades.StudentGrades.Add(grade);

                return grades;
            }

            private ImportGradesFromILPResponse GetTransactionResponse()
            {
                ImportGradesFromILPResponse response = new ImportGradesFromILPResponse();
                response.SectionId = GetSectionGrades().SectionId;

                response.ItemsToPostOutput.Add(new ItemsToPostOutput() { ItemOutStatus = "failure", ItemOutCode = "Midterm1", ItemOutPerson = GetSectionGrades().StudentGrades[0].StudentId, ItemErrorMsg = "some error message" });
                response.ItemsToPostOutput.Add(new ItemsToPostOutput() { ItemOutStatus = "failure", ItemOutCode = "Midterm2", ItemOutPerson = GetSectionGrades().StudentGrades[0].StudentId, ItemErrorMsg = "some error message" });
                response.ItemsToPostOutput.Add(new ItemsToPostOutput() { ItemOutStatus = "failure", ItemOutCode = "Midterm3", ItemOutPerson = GetSectionGrades().StudentGrades[0].StudentId, ItemErrorMsg = "some error message" });
                response.ItemsToPostOutput.Add(new ItemsToPostOutput() { ItemOutStatus = "failure", ItemOutCode = "Midterm4", ItemOutPerson = GetSectionGrades().StudentGrades[0].StudentId, ItemErrorMsg = "some error message" });
                response.ItemsToPostOutput.Add(new ItemsToPostOutput() { ItemOutStatus = "failure", ItemOutCode = "Midterm5", ItemOutPerson = GetSectionGrades().StudentGrades[0].StudentId, ItemErrorMsg = "some error message" });
                response.ItemsToPostOutput.Add(new ItemsToPostOutput() { ItemOutStatus = "failure", ItemOutCode = "SOME-OTHER-CODE", ItemOutPerson = GetSectionGrades().StudentGrades[0].StudentId, ItemErrorMsg = "some error message" });

                return response;
            }
        }

        [TestClass]
        public class SectionRepository_GradeImportExceptionTests
        {
            Mock<IColleagueTransactionFactory> transFactoryMock;
            Mock<ICacheProvider> cacheProviderMock;
            Mock<ILogger> loggerMock;
            ApiSettings apiSettingsMock;
            SectionRepository sectionRepo;
            Mock<IColleagueTransactionInvoker> transManagerMock;

            [TestInitialize]
            public void Initialize()
            {
                loggerMock = new Mock<ILogger>();
                cacheProviderMock = new Mock<ICacheProvider>();
                transFactoryMock = new Mock<IColleagueTransactionFactory>();
                apiSettingsMock = new ApiSettings("null");
                transManagerMock = new Mock<IColleagueTransactionInvoker>();
                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
                  x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                  .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));
                var transactionResponse = GetTransactionErrorResponse();
                // Mock grade import CTX to return an error code, which should cause the repository to throw an exception.
                transManagerMock.Setup<Task<ImportGradesFromILPResponse>>(mgr => mgr.ExecuteAsync<ImportGradesFromILPRequest, ImportGradesFromILPResponse>(
                    It.IsAny<ImportGradesFromILPRequest>()
                    )).Returns(Task.FromResult(transactionResponse));
                transFactoryMock.Setup(transFac => transFac.GetTransactionInvoker()).Returns(transManagerMock.Object);
                sectionRepo = new SectionRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettingsMock);
            }

            [TestCleanup]
            public void Cleanup()
            {
                transFactoryMock = null;
                cacheProviderMock = null;
                sectionRepo = null;
                apiSettingsMock = null;
            }

            [TestClass]
            public class GetNonCachedFacultySectionsTests
            {
                // Broke out these tests to control the subset of records and the data. Not as detailed
                // as GetNonCachedSectionsTests as the GetNonCachedFacultySections being tested selects
                // sectionIds and defers to GetNonCachedSections to marshal the details of the sections 
                // returned (that repo method has its own set of tests)
                Mock<IColleagueTransactionFactory> transFactoryMock;
                Mock<IColleagueTransactionInvoker> mockManager;
                Mock<ICacheProvider> cacheProviderMock;
                Mock<IColleagueDataReader> dataAccessorMock;
                Mock<ILogger> loggerMock;
                Collection<CourseSections> sectionsResponseData;
                Collection<CourseSecMeeting> sectionMeetingResponseData;
                Collection<CourseSecFaculty> sectionFacultyResponseData;
                Collection<CourseSecFaculty> sectionFaculty0000049Term2012FAResponseData;
                Collection<CourseSecFaculty> sectionFaculty0000049Term2013SPResponseData;
                Collection<CourseSecFaculty> sectionFaculty0000049MultiTermResponseData;
                GetStudentCourseSecStudentsResponse studentCourseSecResponseData;
                Collection<PortalSites> portalSitesResponseData;
                Collection<CourseSecXlists> crosslistResponseData;
                Collection<CourseSecPending> pendingResponseData;
                Collection<WaitList> waitlistResponseData;
                IEnumerable<Section> reqSections;
                List<Term> registrationTerms = new List<Term>();
                GetStudentCourseSecStudentsRequest updateRequest;
                ApiSettings apiSettingsMock;
                string noDataFacId = "9999999";
                string goodFacId = "0000049";
                Term term0;
                Term term1;
                Term term2;

                SectionRepository sectionRepo;

                [TestInitialize]
                public void Initialize()
                {
                    loggerMock = new Mock<ILogger>();
                    mockManager = new Mock<IColleagueTransactionInvoker>();
                    term0 = new Term("2012/S2", "Summer 2012-2", new DateTime(2012, 7, 5), new DateTime(2012, 8, 15), 2012, 0, true, true, "2012/S2", true);
                    term1 = new Term("2012/FA", "Fall 2012", new DateTime(2012, 9, 1), new DateTime(2012, 12, 15), 2012, 1, true, true, "2012/FA", true);
                    term2 = new Term("2013/SP", "Spring 2013", new DateTime(2013, 1, 1), new DateTime(2013, 5, 15), 2012, 2, true, true, "2013/SP", true);

                }

                [TestCleanup]
                public void Cleanup()
                {
                    transFactoryMock = null;
                    dataAccessorMock = null;
                    cacheProviderMock = null;
                    sectionsResponseData = null;
                    sectionMeetingResponseData = null;
                    sectionFacultyResponseData = null;
                    sectionFaculty0000049Term2012FAResponseData = null;
                    sectionFaculty0000049Term2013SPResponseData = null;
                    sectionFaculty0000049MultiTermResponseData = null;
                    studentCourseSecResponseData = null;
                    portalSitesResponseData = null;
                    crosslistResponseData = null;
                    pendingResponseData = null;
                    waitlistResponseData = null;
                    reqSections = null;
                    registrationTerms = null;

                    sectionRepo = null;
                }

                private async Task SetupGetNonCachedFacultySectionsData(List<string> sectionIdsToRetrieve)
                {
                    reqSections = await new TestSectionRepository().GetNonCachedSectionsAsync(sectionIdsToRetrieve);
                    registrationTerms.Add(term1);
                    registrationTerms.Add(term2);
                    sectionsResponseData = BuildSectionsResponse(reqSections);
                    sectionMeetingResponseData = BuildSectionMeetingsResponse(reqSections);
                    sectionFacultyResponseData = BuildSectionFacultyResponse(reqSections);
                    sectionFaculty0000049Term2012FAResponseData = BuildSectionFaculty0000049Term2012FAResponse(reqSections);
                    sectionFaculty0000049Term2013SPResponseData = BuildSectionFaculty0000049Term2013SPResponse(reqSections);
                    sectionFaculty0000049MultiTermResponseData = BuildSectionFaculty0000049MultiTermResponse(reqSections);
                    sectionFacultyResponseData = BuildSectionFacultyResponse(reqSections);
                    studentCourseSecResponseData = BuildStudentCourseSecResponse(reqSections);
                    portalSitesResponseData = BuildPortalSitesResponse(reqSections);
                    crosslistResponseData = BuildCrosslistResponse(reqSections);
                    pendingResponseData = BuildPendingSectionResponse(reqSections);
                    waitlistResponseData = BuildWaitlistResponse(reqSections);

                    // Build section repository
                    sectionRepo = BuildValidSectionRepository();
                }

                [TestMethod]
                public async Task GetNonCachedFacultySections_NullTermList()
                {
                    SetupGetNonCachedFacultySectionsData(new List<string>() { "1" });
                    IEnumerable<Section> sections = await sectionRepo.GetNonCachedFacultySectionsAsync(null, "1");
                    Assert.AreEqual(0, sections.Count());
                }

                [TestMethod]
                public async Task GetNonCachedFacultySections_EmptyTermList()
                {
                    SetupGetNonCachedFacultySectionsData(new List<string>() { "1" });
                    List<Term> terms = new List<Term>();
                    IEnumerable<Section> sections = await sectionRepo.GetNonCachedFacultySectionsAsync(terms, "1");
                    Assert.AreEqual(0, sections.Count());
                }

                [TestMethod]
                public async Task GetNonCachedFacultySections_NullFacultyId()
                {
                    SetupGetNonCachedFacultySectionsData(new List<string>() { "1" });
                    List<Term> terms = new List<Term>() { term0 };
                    IEnumerable<Section> sections = await sectionRepo.GetNonCachedFacultySectionsAsync(terms, null);
                    Assert.AreEqual(0, sections.Count());
                }

                [TestMethod]
                public async Task GetNonCachedFacultySections_EmptyFacultyId()
                {
                    SetupGetNonCachedFacultySectionsData(new List<string>() { "1" });
                    List<Term> terms = new List<Term>() { term0 };
                    IEnumerable<Section> sections = await sectionRepo.GetNonCachedFacultySectionsAsync(terms, "");
                    Assert.AreEqual(0, sections.Count());
                }

                [TestMethod]
                public async Task GetNonCachedFacultySections_NoSectionsFound()
                {
                    await SetupGetNonCachedFacultySectionsData(new List<string>() { "1" });
                    List<Term> terms = new List<Term>() { term0 };
                    Collection<CourseSecFaculty> noCSF = new Collection<CourseSecFaculty>();
                    string criteria = "WITH CSF.FACULTY EQ '" + noDataFacId + "' AND CSF.START.DATE GE '07/05/2012' AND CSF.END.DATE LE '08/15/2012' AND CSF.SECTION.TERM EQ '2012/S2'''";
                    dataAccessorMock.Setup<Task<Collection<CourseSecFaculty>>>(csf => csf.BulkReadRecordAsync<CourseSecFaculty>(criteria, true)).Returns(Task.FromResult<Collection<CourseSecFaculty>>(noCSF));
                    IEnumerable<Section> sections = await sectionRepo.GetNonCachedFacultySectionsAsync(terms, noDataFacId);
                    Assert.AreEqual(0, sections.Count());
                }

                [TestMethod]
                public async Task GetNonCachedFacultySections_2012FASectionsReturned()
                {
                    var sectionIdsRequested = (await new TestSectionRepository().GetAsync()).
                        Where(s => s.TermId == "2012/FA" && s.FacultyIds.Contains(goodFacId)).Select(s => s.Id).ToList();
                    await SetupGetNonCachedFacultySectionsData(sectionIdsRequested);
                    List<Term> terms = new List<Term>() { term1 };
                    IEnumerable<Section> sections = await sectionRepo.GetNonCachedFacultySectionsAsync(terms, goodFacId);
                    Assert.AreEqual(sectionFaculty0000049Term2012FAResponseData.Count(), sections.Count());
                }

                [TestMethod]
                public async Task GetNonCachedFacultySections_2013SPSectionsReturned()
                {
                    var sectionIdsRequested = (await new TestSectionRepository().GetAsync()).
                        Where(s => s.TermId == "2013/SP" && s.FacultyIds.Contains(goodFacId)).Select(s => s.Id).ToList();
                    SetupGetNonCachedFacultySectionsData(sectionIdsRequested);
                    List<Term> terms = new List<Term>() { term2 };
                    IEnumerable<Section> sections = await sectionRepo.GetNonCachedFacultySectionsAsync(terms, goodFacId);
                    Assert.AreEqual(sectionFaculty0000049Term2013SPResponseData.Count(), sections.Count());
                }

                [TestMethod]
                public async Task GetNonCachedFacultySections_MultiTermSectionsReturned()
                {
                    var sectionIdsRequested = (await new TestSectionRepository().GetAsync()).
                        Where(s => (s.TermId == "2013/SP" || s.TermId == "2012/FA") && s.FacultyIds.Contains(goodFacId)).Select(s => s.Id).ToList();
                    SetupGetNonCachedFacultySectionsData(sectionIdsRequested);
                    List<Term> terms = new List<Term>() { term1, term2 };
                    IEnumerable<Section> sections = await sectionRepo.GetNonCachedFacultySectionsAsync(terms, goodFacId);
                    int multiCount = sectionFaculty0000049Term2012FAResponseData.Count() + sectionFaculty0000049Term2013SPResponseData.Count();
                    Assert.AreEqual(multiCount, sections.Count());
                }

                private SectionRepository BuildValidSectionRepository()
                {
                    // transaction factory mock
                    transFactoryMock = new Mock<IColleagueTransactionFactory>();

                    apiSettingsMock = new ApiSettings("null");

                    transFactoryMock.Setup(transFac => transFac.GetTransactionInvoker()).Returns(mockManager.Object);
                    // Cache Mock
                    //localCacheMock = new Mock<ObjectCache>();
                    // Cache Provider Mock
                    cacheProviderMock = new Mock<ICacheProvider>();
                    // Set up data accessor for mocking 
                    dataAccessorMock = new Mock<IColleagueDataReader>();
                    cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
                    x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                    .ReturnsAsync(new Tuple<object, SemaphoreSlim>(
                        null,
                        new SemaphoreSlim(1, 1)
                        ));

                    // Set up repo response for initial section request (1, 2, 3)
                    dataAccessorMock.Setup(acc => acc.SelectAsync("COURSE.SECTIONS", It.IsAny<string>())).Returns(Task.FromResult(sectionsResponseData.Select(c => c.Recordkey).ToArray()));
                    dataAccessorMock.Setup<Task<Collection<CourseSections>>>(acc => acc.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(sectionsResponseData));

                    // Set up repo response for "all" meeting requests
                    dataAccessorMock.Setup<Task<Collection<CourseSecMeeting>>>(macc => macc.BulkReadRecordAsync<CourseSecMeeting>("COURSE.SEC.MEETING", It.IsAny<string[]>(), true)).Returns(Task.FromResult(sectionMeetingResponseData));

                    // Set up repo response for "all" faculty
                    dataAccessorMock.Setup<Task<Collection<CourseSecFaculty>>>(facc => facc.BulkReadRecordAsync<CourseSecFaculty>("COURSE.SEC.FACULTY", It.IsAny<string[]>(), true)).Returns(Task.FromResult(sectionFacultyResponseData));
                    string criteria0 = "WITH CSF.FACULTY EQ '0000049' AND CSF.START.DATE GE '07/05/2012' AND CSF.END.DATE LE '08/15/2012'";
                    dataAccessorMock.Setup<Task<Collection<CourseSecFaculty>>>(csf => csf.BulkReadRecordAsync<CourseSecFaculty>(criteria0, true)).Returns(Task.FromResult(new Collection<CourseSecFaculty>()));
                    string criteria1 = "WITH CSF.FACULTY EQ '0000049' AND CSF.START.DATE GE '09/01/2012' AND CSF.END.DATE LE '12/15/2012' AND CSF.SECTION.TERM EQ '2012/FA'''";
                    dataAccessorMock.Setup<Task<Collection<CourseSecFaculty>>>(csf => csf.BulkReadRecordAsync<CourseSecFaculty>(criteria1, true)).Returns(Task.FromResult(sectionFaculty0000049Term2012FAResponseData));
                    string criteria2 = "WITH CSF.FACULTY EQ '0000049' AND CSF.START.DATE GE '01/01/2013' AND CSF.END.DATE LE '05/15/2013' AND CSF.SECTION.TERM EQ '2013/SP'''";
                    dataAccessorMock.Setup<Task<Collection<CourseSecFaculty>>>(csf => csf.BulkReadRecordAsync<CourseSecFaculty>(criteria2, true)).Returns(Task.FromResult(sectionFaculty0000049Term2013SPResponseData));
                    string criteria3 = "WITH CSF.FACULTY EQ '0000049' AND CSF.START.DATE GE '09/01/2012' AND CSF.END.DATE LE '05/15/2013' AND CSF.SECTION.TERM EQ '2012/FA' '2013/SP'''";
                    dataAccessorMock.Setup<Task<Collection<CourseSecFaculty>>>(csf => csf.BulkReadRecordAsync<CourseSecFaculty>(criteria3, true)).Returns(Task.FromResult(sectionFaculty0000049MultiTermResponseData));

                    mockManager.Setup(mgr => mgr.ExecuteAsync<GetStudentCourseSecStudentsRequest, GetStudentCourseSecStudentsResponse>(It.IsAny<GetStudentCourseSecStudentsRequest>())).Returns(Task.FromResult(studentCourseSecResponseData)).Callback<GetStudentCourseSecStudentsRequest>(req => updateRequest = req);

                    dataAccessorMock.Setup<Task<Collection<PortalSites>>>(ps => ps.BulkReadRecordAsync<PortalSites>("PORTAL.SITES", It.IsAny<string[]>(), true)).Returns(Task.FromResult(portalSitesResponseData));
                    dataAccessorMock.Setup<Task<Collection<CourseSecXlists>>>(sxl => sxl.BulkReadRecordAsync<CourseSecXlists>("COURSE.SEC.XLISTS", It.IsAny<string[]>(), true)).Returns(Task.FromResult(crosslistResponseData));
                    dataAccessorMock.Setup<Task<Collection<CourseSecPending>>>(csp => csp.BulkReadRecordAsync<CourseSecPending>("COURSE.SEC.PENDING", It.IsAny<string[]>(), true)).Returns(Task.FromResult(pendingResponseData));
                    dataAccessorMock.Setup<Task<Collection<WaitList>>>(wl => wl.BulkReadRecordAsync<WaitList>("WAIT.LIST", It.IsAny<string>(), true)).Returns(Task.FromResult(waitlistResponseData));

                    var stWebDflt = BuildStwebDefaults(); ;
                    dataAccessorMock.Setup(r => r.ReadRecordAsync<StwebDefaults>("ST.PARMS", It.IsAny<string>(), It.IsAny<bool>())).Returns<string, string, bool>(
                        (param, id, repl) => Task.FromResult((stWebDflt.Recordkey == id) ? stWebDflt : null)
                        );

                    dataAccessorMock.Setup(r => r.ReadRecordAsync<StwebDefaults>(It.IsAny<string>(), It.IsAny<bool>())).Returns<string, bool>(
                       (id, repl) => Task.FromResult((stWebDflt.Recordkey == id) ? stWebDflt : null)
                       );


                    // Set up repo response for section statuses
                    var sectionStatuses = new ApplValcodes();
                    sectionStatuses.ValsEntityAssociation = new List<ApplValcodesVals>();
                    sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("A", "Active", "1", "A", "", "", ""));
                    sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("I", "Inactive", "2", "I", "", "", ""));
                    sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("C", "Cancelled", "", "C", "", "", ""));
                    dataAccessorMock.Setup<Task<ApplValcodes>>(cacc => cacc.ReadRecordAsync<ApplValcodes>("ST.VALCODES", "SECTION.STATUSES", true)).Returns(Task.FromResult(sectionStatuses));

                    // Set up repo response for waitlist statuses
                    ApplValcodes waitlistCodeResponse = new ApplValcodes()
                    {
                        ValsEntityAssociation = new List<ApplValcodesVals>() {new ApplValcodesVals() { ValInternalCodeAssocMember = "A", ValActionCode1AssocMember = "1" },
                                                                   new ApplValcodesVals() { ValInternalCodeAssocMember = "E", ValActionCode1AssocMember = "2"},
                                                                   new ApplValcodesVals() { ValInternalCodeAssocMember = "D", ValActionCode1AssocMember = "3"},
                                                                   new ApplValcodesVals() { ValInternalCodeAssocMember = "P", ValActionCode1AssocMember = "4"},
                                                                   new ApplValcodesVals() { ValInternalCodeAssocMember = "X", ValActionCode1AssocMember = "5"}}
                    };
                    dataAccessorMock.Setup<Task<ApplValcodes>>(cacc => cacc.ReadRecordAsync<ApplValcodes>("ST.VALCODES", "WAIT.LIST.STATUSES", true)).Returns(Task.FromResult(waitlistCodeResponse));

                    // Set up repo response for the temporary international parameter item
                    Data.Base.DataContracts.IntlParams intlParams = new Data.Base.DataContracts.IntlParams();
                    intlParams.HostDateDelimiter = "/";
                    intlParams.HostShortDateFormat = "MDY";
                    dataAccessorMock.Setup<Task<Data.Base.DataContracts.IntlParams>>(iacc => iacc.ReadRecordAsync<Data.Base.DataContracts.IntlParams>("INTL.PARAMS", "INTERNATIONAL", true)).ReturnsAsync(intlParams);

                    // Set up course defaults response (indicates if coreq conversion has taken place)
                    var courseParameters = BuildCourseParametersConvertedResponse();
                    dataAccessorMock.Setup<Task<CdDefaults>>(acc => acc.ReadRecordAsync<CdDefaults>("ST.PARMS", "CD.DEFAULTS", true)).ReturnsAsync(courseParameters);

                    // Set up instructional method response (indicates if sections are online)
                    var instrMethods = BuildValidInstrMethodResponse();
                    dataAccessorMock.Setup(acc => acc.BulkReadRecordAsync<InstrMethods>("INSTR.METHODS", "", true)).ReturnsAsync(instrMethods);

                    // Set up dataAccessorMock as the object for the DataAccessor
                    transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(dataAccessorMock.Object);

                    // Construct section repository
                    sectionRepo = new SectionRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettingsMock);

                    return sectionRepo;
                }

                private SectionRepository BuildInvalidSectionRepository()
                {
                    var transFactoryMock = new Mock<IColleagueTransactionFactory>();
                    apiSettingsMock = new ApiSettings("null");

                    // Set up data accessor for mocking 
                    var dataAccessorMock = new Mock<IColleagueDataReader>();
                    transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(dataAccessorMock.Object);

                    // Set up repo response for "all" section requests
                    Exception expectedFailure = new Exception("fail");
                    dataAccessorMock.Setup<Task<Collection<CourseSections>>>(acc => acc.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string[]>(), true)).Throws(expectedFailure);

                    // Cache Mock
                    var localCacheMock = new Mock<ObjectCache>();
                    // Cache Provider Mock
                    var cacheProviderMock = new Mock<ICacheProvider>();
                    //cacheProviderMock.Setup(provider => provider.GetCache(It.IsAny<string>())).Returns(localCacheMock.Object);

                    // Construct section repository
                    sectionRepo = new SectionRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettingsMock);

                    return sectionRepo;
                }

                private Collection<CourseSections> BuildSectionsResponse(IEnumerable<Section> sections)
                {
                    Collection<CourseSections> repoSections = new Collection<CourseSections>();
                    foreach (var section in sections)
                    {
                        var crsSec = new CourseSections();
                        crsSec.Recordkey = section.Id.ToString();
                        crsSec.SecAcadLevel = section.AcademicLevelCode;
                        crsSec.SecCeus = section.Ceus;
                        crsSec.SecCourse = section.CourseId.ToString();
                        crsSec.SecCourseLevels = section.CourseLevelCodes.ToList();
                        crsSec.SecDepts = section.Departments.Select(x => x.AcademicDepartmentCode).ToList();
                        crsSec.SecLocation = section.Location;
                        crsSec.SecMaxCred = section.MaximumCredits;
                        crsSec.SecVarCredIncrement = section.VariableCreditIncrement;
                        crsSec.SecMinCred = section.MinimumCredits;
                        crsSec.SecNo = section.Number;
                        crsSec.SecShortTitle = section.Title;
                        crsSec.SecStartDate = section.StartDate;
                        crsSec.SecEndDate = section.EndDate;
                        crsSec.SecTerm = section.TermId;
                        crsSec.SecOnlyPassNopassFlag = section.OnlyPassNoPass == true ? "Y" : "N";
                        crsSec.SecAllowPassNopassFlag = section.AllowPassNoPass == true ? "Y" : "N";
                        crsSec.SecAllowAuditFlag = section.AllowAudit == true ? "Y" : "N";
                        var csm = new List<string>() { "1", "2" };
                        crsSec.SecMeeting = csm;
                        var csf = new List<string>() { "1", "2" };
                        crsSec.SecFaculty = csf;
                        var sas = new List<string>() { "1", "2" };
                        crsSec.SecActiveStudents = sas;
                        crsSec.SecStatus = new List<string>() { "A", "P" };
                        crsSec.SecCourseTypes = section.CourseTypeCodes.ToList();
                        crsSec.SecAllowWaitlistFlag = section.AllowWaitlist == true ? "Y" : "N";
                        crsSec.SecCredType = section.CreditTypeCode;

                        // Reconstruct all Colleague requisite and corequisite fields from the data in the Requisites 
                        // post-conversion fields
                        crsSec.SecReqs = new List<string>();
                        crsSec.SecRecommendedSecs = new List<string>();
                        crsSec.SecCoreqSecs = new List<string>();
                        crsSec.SecMinNoCoreqSecs = null;
                        crsSec.SecOverrideCrsReqsFlag = section.OverridesCourseRequisites == true ? "Y" : "";
                        // pre-conversion fields
                        crsSec.SecCourseCoreqsEntityAssociation = new List<CourseSectionsSecCourseCoreqs>();
                        crsSec.SecCoreqsEntityAssociation = new List<CourseSectionsSecCoreqs>();
                        crsSec.SecStatusesEntityAssociation = section.Statuses.Select(x => new CourseSectionsSecStatuses(x.Date, ConvertSectionStatusToCode(x.Status))).ToList();
                        crsSec.SecDepartmentsEntityAssociation = new List<CourseSectionsSecDepartments>();
                        crsSec.SecDepartmentsEntityAssociation.Add(new CourseSectionsSecDepartments("MATH", 75m));
                        crsSec.SecDepartmentsEntityAssociation.Add(new CourseSectionsSecDepartments("PSYC", 25m));

                        foreach (var req in section.Requisites)
                        {
                            if (!string.IsNullOrEmpty(req.RequirementCode))
                            {
                                // post-conversion
                                crsSec.SecReqs.Add(req.RequirementCode);
                                // pre-conversion -- this does not convert
                            }
                            else if (!string.IsNullOrEmpty(req.CorequisiteCourseId))
                            {
                                // post-conversion -- this does not convert
                                // pre-conversion
                                crsSec.SecCourseCoreqsEntityAssociation.Add(
                                    new CourseSectionsSecCourseCoreqs(req.CorequisiteCourseId, (req.IsRequired == true) ? "Y" : "")
                                );
                            }

                        }

                        foreach (var req in section.SectionRequisites)
                        {
                            if (req.CorequisiteSectionIds != null && req.CorequisiteSectionIds.Count() > 0)
                            {
                                foreach (var secId in req.CorequisiteSectionIds)
                                {
                                    // post-conversion--put required and recommended into two separate lists
                                    if (req.IsRequired)
                                    {
                                        crsSec.SecCoreqSecs.Add(secId);
                                        // The minimum number is associated with the entire list of SecCoreqSecs
                                        crsSec.SecMinNoCoreqSecs = req.NumberNeeded;
                                    }
                                    else
                                    {
                                        crsSec.SecRecommendedSecs.Add(secId);
                                    }
                                    // pre-conversion--each requisite section has a required flag
                                    crsSec.SecCoreqsEntityAssociation.Add(
                                        new CourseSectionsSecCoreqs(secId, (req.IsRequired == true) ? "Y" : "")
                                        );
                                }
                            }
                        }

                        if (section.Books != null)
                        {
                            crsSec.SecBooks = new List<string>();
                            crsSec.SecBookOptions = new List<string>();
                            foreach (var book in section.Books)
                            {
                                crsSec.SecBooks.Add(book.BookId);
                                if (book.IsRequired == true)
                                {
                                    crsSec.SecBookOptions.Add("R");
                                }
                            }
                        }
                        crsSec.SecPortalSite = ""; // (!string.IsNullOrEmpty(section.LearningProvider) ? crsSec.Recordkey : "");
                        if (!string.IsNullOrEmpty(section.LearningProvider))
                        {
                            crsSec.SecPortalSite = section.Id;
                        }
                        else
                        {
                            crsSec.SecPortalSite = "";
                        }
                        if (section.Id == "1")
                        {
                            crsSec.SecXlist = "234";
                            crsSec.SecCapacity = 10;
                            crsSec.SecSubject = "MATH";
                            crsSec.SecCourseNo = "1000";
                        }
                        repoSections.Add(crsSec);
                    }
                    return repoSections;
                }

                private Collection<CourseSecMeeting> BuildSectionMeetingsResponse(IEnumerable<Section> sections)
                {
                    Collection<CourseSecMeeting> repoSecMeetings = new Collection<CourseSecMeeting>();
                    int crsSecMId = 0;
                    foreach (var section in sections)
                    {
                        foreach (var mt in section.Meetings)
                        {
                            var crsSecM = new CourseSecMeeting();
                            crsSecMId += 1;
                            crsSecM.Recordkey = crsSecMId.ToString();
                            if (!string.IsNullOrEmpty(mt.Room))
                            {
                                crsSecM.CsmBldg = "ABLE";
                                crsSecM.CsmRoom = "A100";
                            }
                            crsSecM.CsmCourseSection = section.Id;
                            crsSecM.CsmInstrMethod = mt.InstructionalMethodCode;
                            crsSecM.CsmStartTime = mt.StartTime.HasValue ? mt.StartTime.Value.DateTime : (DateTime?)null;
                            crsSecM.CsmEndTime = mt.EndTime.HasValue ? mt.EndTime.Value.DateTime : (DateTime?)null;
                            foreach (var d in mt.Days)
                            {
                                switch (d)
                                {
                                    case DayOfWeek.Friday:
                                        crsSecM.CsmFriday = "Y";
                                        break;
                                    case DayOfWeek.Monday:
                                        crsSecM.CsmMonday = "Y";
                                        break;
                                    case DayOfWeek.Saturday:
                                        crsSecM.CsmSaturday = "Y";
                                        break;
                                    case DayOfWeek.Sunday:
                                        crsSecM.CsmSunday = "Y";
                                        break;
                                    case DayOfWeek.Thursday:
                                        crsSecM.CsmThursday = "Y";
                                        break;
                                    case DayOfWeek.Tuesday:
                                        crsSecM.CsmTuesday = "Y";
                                        break;
                                    case DayOfWeek.Wednesday:
                                        crsSecM.CsmWednesday = "Y";
                                        break;
                                    default:
                                        break;
                                }
                            }
                            repoSecMeetings.Add(crsSecM);
                        }

                    }
                    return repoSecMeetings;
                }

                private Collection<CourseSecFaculty> BuildSectionFacultyResponse(IEnumerable<Section> sections)
                {
                    Collection<CourseSecFaculty> repoSecFaculty = new Collection<CourseSecFaculty>();
                    int crsSecFId = 0;
                    foreach (var section in sections)
                    {
                        foreach (var fac in section.FacultyIds)
                        {
                            var crsSecF = new CourseSecFaculty();
                            crsSecFId += 1;
                            crsSecF.Recordkey = crsSecFId.ToString();
                            crsSecF.CsfCourseSection = section.Id;
                            crsSecF.CsfFaculty = fac;
                            repoSecFaculty.Add(crsSecF);
                        }

                    }
                    return repoSecFaculty;
                }

                private Collection<CourseSecFaculty> BuildSectionFaculty0000049Term2012FAResponse(IEnumerable<Section> sections)
                {
                    Collection<CourseSecFaculty> repoSecFaculty = new Collection<CourseSecFaculty>();
                    int crsSecFId = 0;
                    foreach (var section in sections)
                    {
                        foreach (var fac in section.FacultyIds)
                        {
                            if (section.TermId.Equals("2012/FA") && fac.Equals("0000049"))
                            {
                                var crsSecF = new CourseSecFaculty();
                                crsSecFId += 1;
                                crsSecF.Recordkey = crsSecFId.ToString();
                                crsSecF.CsfCourseSection = section.Id;
                                crsSecF.CsfFaculty = fac;
                                repoSecFaculty.Add(crsSecF);
                            }
                        }

                    }
                    return repoSecFaculty;
                }

                private Collection<CourseSecFaculty> BuildSectionFaculty0000049Term2013SPResponse(IEnumerable<Section> sections)
                {
                    Collection<CourseSecFaculty> repoSecFaculty = new Collection<CourseSecFaculty>();
                    int crsSecFId = 0;
                    foreach (var section in sections)
                    {
                        foreach (var fac in section.FacultyIds)
                        {
                            if (section.TermId.Equals("2013/SP") && fac.Equals("0000049"))
                            {
                                var crsSecF = new CourseSecFaculty();
                                crsSecFId += 1;
                                crsSecF.Recordkey = crsSecFId.ToString();
                                crsSecF.CsfCourseSection = section.Id;
                                crsSecF.CsfFaculty = fac;
                                repoSecFaculty.Add(crsSecF);
                            }
                        }

                    }
                    return repoSecFaculty;
                }

                private Collection<CourseSecFaculty> BuildSectionFaculty0000049MultiTermResponse(IEnumerable<Section> sections)
                {
                    Collection<CourseSecFaculty> repoSecFaculty = new Collection<CourseSecFaculty>();
                    int crsSecFId = 0;
                    foreach (var section in sections)
                    {
                        foreach (var fac in section.FacultyIds)
                        {
                            if ((section.TermId.Equals("2012/FA") || section.TermId.Equals("2013/SP")) && fac.Equals("0000049"))
                            {
                                var crsSecF = new CourseSecFaculty();
                                crsSecFId += 1;
                                crsSecF.Recordkey = crsSecFId.ToString();
                                crsSecF.CsfCourseSection = section.Id;
                                crsSecF.CsfFaculty = fac;
                                repoSecFaculty.Add(crsSecF);
                            }
                        }

                    }
                    return repoSecFaculty;
                }

                private GetStudentCourseSecStudentsResponse BuildStudentCourseSecResponse(IEnumerable<Section> sections)
                {
                    GetStudentCourseSecStudentsResponse scssr = new GetStudentCourseSecStudentsResponse();
                    scssr.StudentCourseSectionStudents = new List<StudentCourseSectionStudents>();
                    Collection<StudentCourseSec> repoSCS = new Collection<StudentCourseSec>();
                    foreach (var section in sections)
                    {
                        foreach (var stu in section.ActiveStudentIds)
                        {
                            StudentCourseSectionStudents scss = new StudentCourseSectionStudents();
                            scss.CourseSectionIds = section.Id;
                            scss.StudentIds = stu;
                            scssr.StudentCourseSectionStudents.Add(scss);
                        }
                    }
                    return scssr;
                }

                private Collection<PortalSites> BuildPortalSitesResponse(IEnumerable<Section> sections)
                {
                    Collection<PortalSites> repoPS = new Collection<PortalSites>();
                    foreach (var section in sections)
                    {
                        if (section.CourseId == "7272")
                        {
                            var ps = new PortalSites();
                            // normally some thing like "HIST-190-001-cs11347", but mock portal site Id with section ID
                            ps.Recordkey = section.Id;
                            if (section.Number == "98")
                            {
                                ps.PsLearningProvider = "";
                                ps.PsPrtlSiteGuid = section.Id;
                            }
                            if (section.Number == "97")
                            {
                                ps.PsLearningProvider = "MOODLE";
                                ps.PsPrtlSiteGuid = section.Id;
                            }
                            repoPS.Add(ps);
                        }
                    }
                    return repoPS;
                }

                private Collection<CourseSecXlists> BuildCrosslistResponse(IEnumerable<Section> sections)
                {
                    // currently built only for ILP testing
                    Collection<CourseSecXlists> repoXL = new Collection<CourseSecXlists>();
                    foreach (var section in sections)
                    {
                        if (section.Id == "1")
                        {
                            var xl = new CourseSecXlists();
                            xl.Recordkey = "232";
                            xl.CsxlPrimarySection = section.Id;
                            xl.CsxlCourseSections = new List<string>() { section.Id, "1", "5" };
                            xl.CsxlCapacity = 20;
                            xl.CsxlWaitlistMax = 5;
                            xl.CsxlWaitlistFlag = "Y";
                            repoXL.Add(xl);
                        }
                    }
                    return repoXL;
                }

                private Collection<CourseSecPending> BuildPendingSectionResponse(IEnumerable<Section> sections)
                {
                    Collection<CourseSecPending> repoPending = new Collection<CourseSecPending>();
                    foreach (var section in sections)
                    {
                        if (section.CourseId == "7272" && (section.Number == "98" || section.Number == "97"))
                        {
                            var cp = new CourseSecPending();
                            cp.Recordkey = section.Id;
                            if (section.Number == "98")
                            {
                                cp.CspReservedSeats = 1;
                            }
                            if (section.Number == "97")
                            {
                                cp.CspReservedSeats = 2;
                            }
                            repoPending.Add(cp);
                        }
                    }
                    return repoPending;
                }

                private Collection<WaitList> BuildWaitlistResponse(IEnumerable<Section> sections)
                {
                    Collection<WaitList> repoWaitlist = new Collection<WaitList>();
                    foreach (var section in sections)
                    {
                        if (section.CourseId == "7272" && (section.Number == "98" || section.Number == "97"))
                        {
                            var wl = new WaitList();
                            wl.Recordkey = section.Id;
                            if (section.Number == "98")
                            {
                                wl.WaitCourseSection = section.Id;
                                wl.WaitStatus = "P";
                                wl.WaitStudent = "111111";
                            }
                            if (section.Number == "97")
                            {
                                wl.WaitCourseSection = section.Id;
                                wl.WaitStatus = "P";
                                wl.WaitStudent = "22222";
                            }
                            repoWaitlist.Add(wl);
                        }
                    }
                    return repoWaitlist;
                }

                private CdDefaults BuildCourseParametersConvertedResponse()
                {
                    // Converted Response
                    CdDefaults defaults = new CdDefaults();
                    defaults.Recordkey = "CD.DEFAULTS";
                    defaults.CdReqsConvertedFlag = "Y";
                    return defaults;
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionRepository_ImportGradesSectionNull()
            {
                await sectionRepo.ImportGradesAsync(null, false, true, GradesPutCallerTypes.ILP);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionRepository_ImportGradesStudentGradesNull()
            {
                var grades = new SectionGrades();
                grades.StudentGrades = null;
                await sectionRepo.ImportGradesAsync(grades, false, true, GradesPutCallerTypes.ILP);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionRepository_ImportGradesStudentGradesEmpty()
            {
                var grades = new SectionGrades();
                grades.StudentGrades = new List<StudentGrade>();
                await sectionRepo.ImportGradesAsync(grades, false, true, GradesPutCallerTypes.ILP);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task SectionRepository_ImportGradesTransactionException()
            {
                var grades = GetSectionGrades();
                await sectionRepo.ImportGradesAsync(grades, false, true, GradesPutCallerTypes.ILP);
            }

            private SectionGrades GetSectionGrades()
            {
                var grades = new SectionGrades();
                grades.SectionId = "123";
                var grade = new StudentGrade();
                grade.StudentId = "101";
                grade.MidtermGrade1 = "A";
                grade.MidtermGrade2 = "B";
                grade.MidtermGrade3 = "C";
                grade.MidtermGrade4 = "D";
                grade.MidtermGrade5 = "E";
                grade.MidtermGrade6 = "F";
                grade.FinalGrade = "G";
                grade.FinalGradeExpirationDate = DateTime.Now;
                grade.LastAttendanceDate = DateTime.Now;
                grade.NeverAttended = true;
                grade.EffectiveStartDate = DateTime.Now;
                grade.EffectiveStartDate = DateTime.Now.AddDays(30);
                grades.StudentGrades = new List<StudentGrade>();
                grades.StudentGrades.Add(grade);

                return grades;
            }

            private ImportGradesFromILPResponse GetTransactionErrorResponse()
            {
                ImportGradesFromILPResponse response = new ImportGradesFromILPResponse();
                response.SectionId = GetSectionGrades().SectionId;
                response.ErrorCode = "MD-TID";

                return response;
            }
        }


        [TestClass]
        public class SectionRepository_EedmTests : SectionRepositoryTests
        {
            SectionRepository sectionRepo;
            Mock<IStudentRepositoryHelper> stuRepoHelperMock;
            IStudentRepositoryHelper stuRepoHelper;
            CourseSections cs;
            Collection<CourseSections> courseSectionCollection = new Collection<CourseSections>();
            Collection<CourseSecXlists> crosslistResponseData;
            CourseSecMeeting csm;
            CourseSecFaculty csf;
            Section result;
            CdDefaults cdDefaults;
            PortalSites ps;
            string csId;

            [TestInitialize]
            public void Initialize()
            {
                MainInitialize();
                //stuRepoHelperMock = new Mock<IStudentRepositoryHelper>();
                //stuRepoHelper = stuRepoHelperMock.Object;
                csId = "12345";

                cs = new CourseSections()
                {
                    RecordGuid = "23033dc3-06fc-4111-b910-77050b45cbe1",
                    Recordkey = csId,
                    RecordModelName = "sections",
                    SecAcadLevel = "UG",
                    SecActiveStudents = new List<string>(),
                    SecAllowAuditFlag = "N",
                    SecAllowPassNopassFlag = "N",
                    SecAllowWaitlistFlag = "Y",
                    SecBookOptions = new List<string>() { "R", "O" },
                    SecBooks = new List<string>() { "Book 1", "Book 2" },
                    SecCapacity = 30,
                    SecCeus = null,
                    SecCloseWaitlistFlag = "Y",
                    SecCourse = "210",
                    SecCourseLevels = new List<string>() { "100" },
                    SecCourseTypes = new List<string>() { "STND", "HONOR" },
                    SecCredType = "IN",
                    SecEndDate = new DateTime(2014, 12, 15),
                    SecFaculty = new List<string>(),
                    SecFacultyConsentFlag = "Y",
                    SecGradeScheme = "UGR",
                    SecInstrMethods = new List<string>() { "LEC", "LAB" },
                    SecLocation = "MAIN",
                    SecMaxCred = 6m,
                    SecMeeting = new List<string>(),
                    SecMinCred = 3m,
                    SecName = "MATH-4350-01",
                    SecNo = "01",
                    SecNoWeeks = 10,
                    SecOnlyPassNopassFlag = "N",
                    SecPortalSite = csId,
                    SecShortTitle = "Statistics",
                    SecStartDate = DateTime.Today.AddDays(-10),
                    SecTerm = "2014/FA",
                    SecTopicCode = "ABC",
                    SecVarCredIncrement = 1m,
                    SecWaitlistMax = 10,
                    SecWaitlistRating = "SR",
                    SecXlist = null,
                    SecFirstMeetingDate = new DateTime(2015, 10, 25),
                    SecLastMeetingDate = new DateTime(2017, 01, 02),
                    SecLearningProvider = "MOODLE"
                };
                cs.SecEndDate = cs.SecStartDate.Value.AddDays(69);
                cs.SecContactEntityAssociation = new List<CourseSectionsSecContact>();
                cs.SecContactEntityAssociation.Add(new CourseSectionsSecContact("LEC", 20.00m, 45.00m, "T", 37.50m));
                cs.SecContactEntityAssociation.Add(new CourseSectionsSecContact("LAB", 10.00m, 15.00m, "T", 45.00m));
                cs.SecDepartmentsEntityAssociation = new List<CourseSectionsSecDepartments>();
                cs.SecDepartmentsEntityAssociation.Add(new CourseSectionsSecDepartments("MATH", 75m));
                cs.SecDepartmentsEntityAssociation.Add(new CourseSectionsSecDepartments("PSYC", 25m));
                cs.SecStatusesEntityAssociation = new List<CourseSectionsSecStatuses>();
                cs.SecStatusesEntityAssociation.Add(new CourseSectionsSecStatuses(new DateTime(2001, 5, 15), "A"));
                // Instr methods association - instructional method and load
                cs.SecContactEntityAssociation = new List<CourseSectionsSecContact>();
                cs.SecContactEntityAssociation.Add(new CourseSectionsSecContact("LEC", 20.00m, 0m, "", 0m));
                cs.SecContactEntityAssociation.Add(new CourseSectionsSecContact("LAB", 10.00m, 0m, "", 0m));
                // Pointer to CourseSecFaculty
                cs.SecFaculty.Add("1");
                // Pointer to CourseSecMeeting
                cs.SecMeeting.Add("1");

                //crosslistResponseData = BuildCrosslistResponse()

                BuildLdmConfiguration(dataReaderMock, out cdDefaults);

                MockRecordAsync<CourseSections>("COURSE.SECTIONS", cs, cs.RecordGuid);

                // Set up repo response for course.sec.meeting
                csm = new CourseSecMeeting()
                {
                    Recordkey = "1",
                    CsmInstrMethod = "LEC",
                    CsmCourseSection = "12345",
                    CsmStartDate = DateTime.Today,
                    CsmEndDate = DateTime.Today.AddDays(27),
                    CsmStartTime = (new DateTime(1, 1, 1, 10, 0, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone).ToLocalDateTime(colleagueTimeZone),
                    CsmEndTime = (new DateTime(1, 1, 1, 11, 20, 0) as DateTime?).ToTimeOfDayDateTimeOffset(colleagueTimeZone).ToLocalDateTime(colleagueTimeZone),
                    CsmMonday = "Y"
                };
                MockRecordAsync<CourseSecMeeting>("COURSE.SEC.MEETING", csm);

                // Set up repo response for course.sec.faculty
                csf = new CourseSecFaculty()
                {
                    Recordkey = "1",
                    CsfInstrMethod = "LEC",
                    CsfCourseSection = "12345",
                    CsfFaculty = "FAC1",
                    CsfFacultyPct = 100m,
                    CsfStartDate = cs.SecStartDate,
                    CsfEndDate = cs.SecEndDate,
                };
                MockRecordAsync<CourseSecFaculty>("COURSE.SEC.FACULTY", csf);

                MockRecordsAsync<CourseSecXlists>("COURSE.SEC.XLISTS", new Collection<CourseSecXlists>());
                MockRecordsAsync<CourseSecPending>("COURSE.SEC.PENDING", new Collection<CourseSecPending>());
                ps = new PortalSites() { Recordkey = csId, PsLearningProvider = "MOODLE", PsPrtlSiteGuid = csId };
                MockRecordsAsync<PortalSites>("PORTAL.SITES", new Collection<PortalSites>() { ps });
                MockRecordsAsync<WaitList>("WAIT.LIST", new Collection<WaitList>());
                MockRecordsAsync<AcadReqmts>("ACAD.REQMTS", new Collection<AcadReqmts>());

                MockRecordAsync<Dflts>("CORE.PARMS", new Dflts() { Recordkey = "DEFAULTS", DfltsCampusCalendar = "CAL" });
                // Mock data needed to read campus calendar
                var startTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 06, 00, 00);
                var endTime = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, 23, 30, 00);
                MockRecordAsync<Data.Base.DataContracts.CampusCalendar>("CAL", new Data.Base.DataContracts.CampusCalendar() { Recordkey = "CAL", CmpcDesc = "Calendar", CmpcDayStartTime = startTime, CmpcDayEndTime = endTime, CmpcBookPastNoDays = "30" });
                // Set up response for instructional methods and ST web defaults
                MockRecordsAsync<InstrMethods>("INSTR.METHODS", BuildValidInstrMethodResponse());

                // Set up repo response for section statuses
                var sectionStatuses = new ApplValcodes();
                sectionStatuses.ValsEntityAssociation = new List<ApplValcodesVals>();
                sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("A", "Active", "1", "A", "", "", ""));
                sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("I", "Inactive", "2", "I", "", "", ""));
                sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("C", "Cancelled", "", "C", "", "", ""));
                dataReaderMock.Setup<Task<ApplValcodes>>(cacc => cacc.ReadRecordAsync<ApplValcodes>("ST.VALCODES", "SECTION.STATUSES", true)).ReturnsAsync(sectionStatuses);

                //setup mocking for Stweb Defaults
                var stWebDflt = BuildStwebDefaults(); ;
                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>("ST.PARMS", It.IsAny<string>(), It.IsAny<bool>())).Returns<string, string, bool>(
                    (param, id, repl) => Task.FromResult((stWebDflt.Recordkey == id) ? stWebDflt : null)
                    );

                dataReaderMock.Setup(r => r.ReadRecordAsync<StwebDefaults>(It.IsAny<string>(), It.IsAny<bool>())).Returns<string, bool>(
                   (id, repl) => Task.FromResult((stWebDflt.Recordkey == id) ? stWebDflt : null)
                   );

                cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
                x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                .ReturnsAsync(new Tuple<object, SemaphoreSlim>(
                    null,
                    new SemaphoreSlim(1, 1)
                    ));
                sectionRepo = new SectionRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object, apiSettings);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionRepo = null;
                stuRepoHelperMock = null;
                stuRepoHelper = null;
                cs = null;
                courseSectionCollection = null;
                crosslistResponseData = null;
                csm = null;
                csf = null;
                result = null;
                cdDefaults = null;
                ps = null;
                csId = null;
            }

            [TestMethod]
            public async Task SectionRepository_GetSectionsAsync()
            {
                string[] sublist = new string[] { "1" };
                courseSectionCollection.Add(cs);
                dataReaderMock.Setup(dr => dr.SelectAsync("COURSE.SECTIONS", It.IsAny<string>())).ReturnsAsync(sublist);
                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", sublist, It.IsAny<bool>())).ReturnsAsync(courseSectionCollection);

                dataReaderMock.Setup(dr => dr.SelectAsync("COURSE.SEC.FACULTY", It.IsAny<string>())).ReturnsAsync(sublist);
                var regSections = await new TestSectionRepository().GetRegistrationSectionsAsync(new List<Term>());
                var sectionFacultyResponseData = BuildSectionFacultyResponse(regSections);
                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSecFaculty>("COURSE.SEC.FACULTY", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(sectionFacultyResponseData);


                var results = await sectionRepo.GetSectionsAsync(0, 3, "Title", "2016/01/01", "2016/12/31", "code", "number", "learningProvider", "termId", "academicLevel", "course", "location",
                                                                       "status", "department", "", "");
                Assert.IsNotNull(results);
                var actuals = results.Item1;

                for (int i = 0; i < actuals.Count(); i++)
                {
                    var actual = actuals.ToList()[i];
                    var expected = courseSectionCollection.ToList()[i];
                    Assert.IsNotNull(actual);

                    Assert.AreEqual(expected.RecordGuid, actual.Guid);
                    Assert.AreEqual(expected.Recordkey, actual.Id);
                    Assert.AreEqual(expected.SecAcadLevel, actual.AcademicLevelCode);
                    Assert.AreEqual(expected.SecCapacity, actual.Capacity);
                    Assert.AreEqual(expected.SecCeus, actual.Ceus);
                    Assert.AreEqual(expected.SecCourse, actual.CourseId);
                    Assert.AreEqual(expected.SecCredType, actual.CreditTypeCode);
                    Assert.AreEqual(expected.SecEndDate, actual.EndDate);
                    Assert.AreEqual(expected.SecFacultyConsentFlag.Equals("Y", StringComparison.OrdinalIgnoreCase) ? true : false, actual.IsInstructorConsentRequired);
                    Assert.AreEqual(expected.SecFirstMeetingDate, actual.FirstMeetingDate);
                    Assert.AreEqual(expected.SecLastMeetingDate, actual.LastMeetingDate);
                    Assert.AreEqual(expected.SecLearningProvider, actual.LearningProvider);
                    Assert.AreEqual(expected.SecLocation, actual.Location);
                    Assert.AreEqual(expected.SecMaxCred, actual.MaximumCredits);
                    Assert.AreEqual(expected.SecMinCred, actual.MinimumCredits);
                    Assert.AreEqual(expected.SecName, actual.Name);
                    Assert.AreEqual(expected.SecNo, actual.Number);
                }
            }

            [TestMethod]
            public async Task SectionRepository_GetSectionByGuidAsync()
            {
                string id = "23033dc3-06fc-4111-b910-77050b45cbe1";
                var guidLookUp = new GuidLookup[] { new GuidLookup(id) };
                GuidLookupResult guidLookupResult = new GuidLookupResult() { Entity = "COURSE.SECTIONS", PrimaryKey = csId };
                Dictionary<string, GuidLookupResult> guidLookupDict = new Dictionary<string, GuidLookupResult>();
                guidLookupDict.Add("COURSE.SECTIONS", guidLookupResult);
                dataReaderMock.Setup(dr => dr.SelectAsync(It.IsAny<GuidLookup[]>())).ReturnsAsync(guidLookupDict);

                var results = await sectionRepo.GetSectionByGuidAsync(id);
                Assert.IsNotNull(results);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionRepository_GetSectionAsync_ArgumentNullException()
            {
                var results = await sectionRepo.GetSectionAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionRepository_GetSectionAsync_CourseSection_Null_KeyNotFoundException()
            {
                var results = await sectionRepo.GetSectionAsync("BadKey");
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionRepository_GetSectionByGuidAsync_Dictionary_Null_KeyNotFoundException()
            {
                string id = "23033dc3-06fc-4111-b910-77050b45cbe1";
                var guidLookUp = new GuidLookup[] { new GuidLookup(id) };
                GuidLookupResult guidLookupResult = null;
                Dictionary<string, GuidLookupResult> guidLookupDict = new Dictionary<string, GuidLookupResult>();
                guidLookupDict.Add("COURSE.SECTIONS", guidLookupResult);
                dataReaderMock.Setup(dr => dr.SelectAsync(guidLookUp)).ReturnsAsync(guidLookupDict);

                var results = await sectionRepo.GetSectionByGuidAsync(id);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionRepository_GetSectionByGuidAsync_NullFoundEntry_KeyNotFoundException()
            {
                string id = "23033dc3-06fc-4111-b910-77050b45cbe1";
                var guidLookUp = new GuidLookup[] { new GuidLookup(id) };
                GuidLookupResult guidLookupResult = new GuidLookupResult() { Entity = "COURSE.SECTIONS", PrimaryKey = csId };
                Dictionary<string, GuidLookupResult> guidLookupDict = new Dictionary<string, GuidLookupResult>();
                guidLookupDict.Add("COURSE.SECTIONS", null);
                dataReaderMock.Setup(dr => dr.SelectAsync(It.IsAny<GuidLookup[]>())).ReturnsAsync(guidLookupDict);

                var results = await sectionRepo.GetSectionByGuidAsync(id);
            }

            private Collection<CourseSecFaculty> BuildSectionFacultyResponse(IEnumerable<Section> sections)
            {
                Collection<CourseSecFaculty> repoSecFaculty = new Collection<CourseSecFaculty>();
                int crsSecFId = 0;
                foreach (var section in sections)
                {
                    foreach (var fac in section.FacultyIds)
                    {
                        var crsSecF = new CourseSecFaculty();
                        //crsSecFId += 1;
                        crsSecF.Recordkey = section.Id;
                        crsSecF.CsfCourseSection = section.Id;
                        crsSecF.CsfFaculty = fac;
                        crsSecF.CsfInstrMethod = "ONL";
                        crsSecF.CsfStartDate = section.StartDate;
                        crsSecF.CsfEndDate = section.EndDate;
                        crsSecF.RecordGuid = Guid.NewGuid().ToString();
                        repoSecFaculty.Add(crsSecF);
                    }

                }
                return repoSecFaculty;
            }


            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionRepository_PostSectionFacultyAsync_NullSection()
            {
                var actual = await sectionRepo.PostSectionFacultyAsync(null, "92364642-4CF0-4640-B657-DC76CB7E289B");
            }


            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionRepository_DeleteSectionFacultyAsync_NullGuid()
            {
                var guid = "92364642-4CF0-4640-B657-DC76CB7E289B";
                var sectionFaculty = new SectionFaculty(guid, "1", "12345", "12345", "OLN", new DateTime(2016, 9, 1), new DateTime(2017, 9, 1), 0);

                await sectionRepo.DeleteSectionFacultyAsync(sectionFaculty, null);
            }

            [TestMethod]
            [ExpectedException(typeof(RepositoryException))]
            public async Task SectionRepository_DeleteSectionFacultyAsync_RepositoryException()
            {
                var guid = "92364642-4CF0-4640-B657-DC76CB7E289B";

                var sectionFaculty = new SectionFaculty(guid, "1", "12345", "12345", "OLN", new DateTime(2016, 9, 1), new DateTime(2017, 9, 1), 0);

                var deleteSectionFacultyErrors = new DeleteSectionInstructorsErrors() { ErrorCodes = "1", ErrorMessages = "Error" };
                var deleteSectionFacultyWarnings = new DeleteSectionInstructorsWarnings { WarningCodes = "2", WarningMessages = "Warning" };

                var deleteResponse = new DeleteSectionInstructorsResponse()
                {
                   
                   DeleteSectionInstructorsErrors = new List<DeleteSectionInstructorsErrors>( ) {  deleteSectionFacultyErrors },
                   DeleteSectionInstructorsWarnings = new List<DeleteSectionInstructorsWarnings> {  deleteSectionFacultyWarnings }
                };
                transManagerMock.Setup(i => i.ExecuteAsync<DeleteSectionInstructorsRequest, DeleteSectionInstructorsResponse>(It.IsAny<DeleteSectionInstructorsRequest>())).ReturnsAsync(deleteResponse);
                await sectionRepo.DeleteSectionFacultyAsync(sectionFaculty, guid);

            }

            [TestMethod]
            public async Task SectionRepository_DeleteSectionFacultyAsync()
            {
                var guid = "92364642-4CF0-4640-B657-DC76CB7E289B";

                var sectionFaculty = new SectionFaculty(guid, "1", "12345", "12345", "OLN", new DateTime(2016, 9, 1), new DateTime(2017, 9, 1), 0);

                var deleteSectionFacultyErrors = new DeleteSectionInstructorsErrors() { ErrorCodes = "1", ErrorMessages = "Error" };
                var deleteSectionFacultyWarnings = new DeleteSectionInstructorsWarnings { WarningCodes = "2", WarningMessages = "Warning" };

                var deleteResponse = new DeleteSectionInstructorsResponse(){};

                transManagerMock.Setup(i => i.ExecuteAsync<DeleteSectionInstructorsRequest, DeleteSectionInstructorsResponse>(It.IsAny<DeleteSectionInstructorsRequest>())).ReturnsAsync(deleteResponse);
                await sectionRepo.DeleteSectionFacultyAsync(sectionFaculty, guid);
            }


            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionRepository_PostSectionFacultyAsync_NullGuid()
            {
                var guid = "92364642-4CF0-4640-B657-DC76CB7E289B";
                var sectionFaculty = new SectionFaculty(guid, "1", "12345", "12345", "OLN", new DateTime(2016, 9, 1), new DateTime(2017, 9, 1), 0);

                var actual = await sectionRepo.PostSectionFacultyAsync(sectionFaculty, null);
            }

            [TestMethod]
            [ExpectedException(typeof(RepositoryException))]
            public async Task SectionRepository_PostSectionFacultyAsync_RepositoryException()
            {
                var guid = "92364642-4CF0-4640-B657-DC76CB7E289B";
                
                var sectionFaculty = new SectionFaculty(guid, "1", "12345", "12345", "OLN", new DateTime(2016, 9, 1), new DateTime(2017, 9, 1), 0);

                var updateSectionFacultyErrors = new UpdateSectionFacultyErrors() { ErrorCodes = "1", ErrorMessages = "Error" };
                var updateSectionFacultyWarnings = new UpdateSectionFacultyWarnings { WarningCodes = "2", WarningMessages = "Warning" };
               
                var updateResponse = new UpdateSectionFacultyResponse()
                {
                    CourseSecFacultyId = "12345",
                    CsfGuid = guid,
                    UpdateSectionFacultyWarnings = new List<UpdateSectionFacultyWarnings> { updateSectionFacultyWarnings },
                    UpdateSectionFacultyErrors = new List<UpdateSectionFacultyErrors>() { updateSectionFacultyErrors }
                    
                };
                transManagerMock.Setup(i => i.ExecuteAsync<UpdateSectionFacultyRequest, UpdateSectionFacultyResponse>(It.IsAny<UpdateSectionFacultyRequest>())).ReturnsAsync(updateResponse);
                await sectionRepo.PostSectionFacultyAsync(sectionFaculty, guid);
               
            }

            [TestMethod]
            public async Task SectionRepository_PostSectionFacultyAsync()
            {
                var guid = "92364642-4CF0-4640-B657-DC76CB7E289B";
                string[] sublist = new string[] { "1" };
                courseSectionCollection.Add(cs);
                dataReaderMock.Setup(dr => dr.SelectAsync("COURSE.SECTIONS", It.IsAny<string>())).ReturnsAsync(sublist);
                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(courseSectionCollection);

                dataReaderMock.Setup(dr => dr.SelectAsync("COURSE.SEC.FACULTY", It.IsAny<string>())).ReturnsAsync(sublist);

                var sectionFacultyResponseData = new CourseSecFaculty()
                {
                    Recordkey = "1",
                    RecordGuid = guid,
                    CsfInstrMethod = "ONL",
                    CsfStartDate = new DateTime(2016, 9, 1),
                    CsfEndDate = new DateTime(2017, 9, 1),
                    CsfFaculty = "1",
                    CsfCourseSection = "12345"

                };

                dataReaderMock.Setup(dr => dr.ReadRecordAsync<CourseSecFaculty>(It.IsAny<GuidLookup>(), It.IsAny<bool>())).ReturnsAsync(sectionFacultyResponseData);
                var courseSecMeetings = new Collection<CourseSecMeeting>();
                courseSecMeetings.Add(new CourseSecMeeting() { Recordkey = "1", CsmInstrMethod = "ONL", CsmFriday = "Y", CsmCourseSection = "12345", CsmFrequency = "W", CsmStartDate = new DateTime(2016, 9, 1), CsmEndDate = new DateTime(2017, 12, 12) });

                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSecMeeting>("COURSE.SEC.MEETING", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(courseSecMeetings);

                var sectionFaculty = new SectionFaculty(guid,  "1", "12345", "12345",  "OLN", new DateTime(2016, 9, 1), new DateTime(2017, 9, 1), 0);

                var updateResponse = new UpdateSectionFacultyResponse()
                {
                    CourseSecFacultyId = "12345",
                    CsfGuid = guid,

                };
                transManagerMock.Setup(i => i.ExecuteAsync<UpdateSectionFacultyRequest, UpdateSectionFacultyResponse>(It.IsAny<UpdateSectionFacultyRequest>())).ReturnsAsync(updateResponse);
                var actual  = await sectionRepo.PostSectionFacultyAsync(sectionFaculty, guid);
                Assert.IsNotNull(actual);

                Assert.AreEqual(guid, actual.Guid);
                Assert.AreEqual("1", actual.Id);
                Assert.AreEqual(new DateTime(2017, 9, 1), actual.EndDate);
                Assert.AreEqual("1", actual.FacultyId);
                Assert.AreEqual("ONL", actual.InstructionalMethodCode);
                Assert.AreEqual(0, actual.MeetingLoadFactor);
                Assert.AreEqual(true, actual.PrimaryIndicator);
                Assert.AreEqual(0, actual.ResponsibilityPercentage);
                Assert.AreEqual("12345", actual.SectionId);
                Assert.AreEqual(new DateTime(2016, 9, 1), actual.StartDate);

            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionRepository_PutSectionFacultyAsync_NullSection()
            {
                var actual = await sectionRepo.PutSectionFacultyAsync(null, "92364642-4CF0-4640-B657-DC76CB7E289B");
            }


            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionRepository_PutSectionFacultyAsync_NullGuid()
            {
                var guid = "92364642-4CF0-4640-B657-DC76CB7E289B";
                var sectionFaculty = new SectionFaculty(guid, "1", "12345", "12345", "OLN", new DateTime(2016, 9, 1), new DateTime(2017, 9, 1), 0);

                var actual = await sectionRepo.PutSectionFacultyAsync(sectionFaculty, null);
            }

            [TestMethod]
            [ExpectedException(typeof(RepositoryException))]
            public async Task SectionRepository_PutSectionFacultyAsync_RepositoryException()
            {
                var guid = "92364642-4CF0-4640-B657-DC76CB7E289B";

                var sectionFaculty = new SectionFaculty(guid, "1", "12345", "12345", "OLN", new DateTime(2016, 9, 1), new DateTime(2017, 9, 1), 0);

                var updateSectionFacultyErrors = new UpdateSectionFacultyErrors() { ErrorCodes = "1", ErrorMessages = "Error" };
                var updateSectionFacultyWarnings = new UpdateSectionFacultyWarnings { WarningCodes = "2", WarningMessages = "Warning" };

                var updateResponse = new UpdateSectionFacultyResponse()
                {
                    CourseSecFacultyId = "12345",
                    CsfGuid = guid,
                    UpdateSectionFacultyWarnings = new List<UpdateSectionFacultyWarnings> { updateSectionFacultyWarnings },
                    UpdateSectionFacultyErrors = new List<UpdateSectionFacultyErrors>() { updateSectionFacultyErrors }

                };
                transManagerMock.Setup(i => i.ExecuteAsync<UpdateSectionFacultyRequest, UpdateSectionFacultyResponse>(It.IsAny<UpdateSectionFacultyRequest>())).ReturnsAsync(updateResponse);
                await sectionRepo.PutSectionFacultyAsync(sectionFaculty, guid);

            }
            [TestMethod]
            public async Task SectionRepository_PutSectionFacultyAsync()
            {
                var guid = "92364642-4CF0-4640-B657-DC76CB7E289B";
                string[] sublist = new string[] { "1" };
                courseSectionCollection.Add(cs);
                dataReaderMock.Setup(dr => dr.SelectAsync("COURSE.SECTIONS", It.IsAny<string>())).ReturnsAsync(sublist);
                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(courseSectionCollection);

                dataReaderMock.Setup(dr => dr.SelectAsync("COURSE.SEC.FACULTY", It.IsAny<string>())).ReturnsAsync(sublist);

                var sectionFacultyResponseData = new CourseSecFaculty()
                {
                    Recordkey = "1",
                    RecordGuid = guid,
                    CsfInstrMethod = "ONL",
                    CsfStartDate = new DateTime(2016, 9, 1),
                    CsfEndDate = new DateTime(2017, 9, 1),
                    CsfFaculty = "1",
                    CsfCourseSection = "12345"

                };

                dataReaderMock.Setup(dr => dr.ReadRecordAsync<CourseSecFaculty>(It.IsAny<GuidLookup>(), It.IsAny<bool>())).ReturnsAsync(sectionFacultyResponseData);
                var courseSecMeetings = new Collection<CourseSecMeeting>();
                courseSecMeetings.Add(new CourseSecMeeting() { Recordkey = "1", CsmInstrMethod = "ONL", CsmFriday = "Y", CsmCourseSection = "12345", CsmFrequency = "W", CsmStartDate = new DateTime(2016, 9, 1), CsmEndDate = new DateTime(2017, 12, 12) });

                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSecMeeting>("COURSE.SEC.MEETING", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(courseSecMeetings);

                var sectionFaculty = new SectionFaculty(guid, "1", "12345", "12345", "OLN", new DateTime(2016, 9, 1), new DateTime(2017, 9, 1), 0);

                var updateResponse = new UpdateSectionFacultyResponse()
                {
                    CourseSecFacultyId = "12345",
                    CsfGuid = guid,

                };
                transManagerMock.Setup(i => i.ExecuteAsync<UpdateSectionFacultyRequest, UpdateSectionFacultyResponse>(It.IsAny<UpdateSectionFacultyRequest>())).ReturnsAsync(updateResponse);
                var actual = await sectionRepo.PutSectionFacultyAsync(sectionFaculty, guid);
                Assert.IsNotNull(actual);

                Assert.AreEqual(guid, actual.Guid);
                Assert.AreEqual("1", actual.Id);
                Assert.AreEqual(new DateTime(2017, 9, 1), actual.EndDate);
                Assert.AreEqual("1", actual.FacultyId);
                Assert.AreEqual("ONL", actual.InstructionalMethodCode);
                Assert.AreEqual(0, actual.MeetingLoadFactor);
                Assert.AreEqual(true, actual.PrimaryIndicator);
                Assert.AreEqual(0, actual.ResponsibilityPercentage);
                Assert.AreEqual("12345", actual.SectionId);
                Assert.AreEqual(new DateTime(2016, 9, 1), actual.StartDate);

            }


            [TestMethod]
            public async Task SectionRepository_GetSectionFaculty()
            {

                string[] sublist = new string[] { "1" };
                courseSectionCollection.Add(cs);
                dataReaderMock.Setup(dr => dr.SelectAsync("COURSE.SECTIONS", It.IsAny<string>())).ReturnsAsync(sublist);
                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(courseSectionCollection);

                dataReaderMock.Setup(dr => dr.SelectAsync("COURSE.SEC.FACULTY", It.IsAny<string>())).ReturnsAsync(sublist);

                var sectionFacultyResponseData = new Collection<CourseSecFaculty>();
                sectionFacultyResponseData.Add(new CourseSecFaculty()
                {
                    Recordkey = "1",
                    RecordGuid = "92364642-4CF0-4640-B657-DC76CB7E289B",
                    CsfInstrMethod = "ONL",
                    CsfStartDate = new DateTime(2016, 9, 1),
                    CsfEndDate = new DateTime(2017, 9, 1),
                    CsfFaculty = "1",
                    CsfCourseSection = "12345",
                    CsfFacultyPct = 10

                });

                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSecFaculty>("COURSE.SEC.FACULTY", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(sectionFacultyResponseData);

                var courseSecMeetings = new Collection<CourseSecMeeting>();
                courseSecMeetings.Add(new CourseSecMeeting() { Recordkey = "1", CsmInstrMethod = "ONL", CsmFriday = "Y", CsmCourseSection = "12345", CsmFrequency = "W", CsmStartDate = new DateTime(2016, 9, 1), CsmEndDate = new DateTime(2017, 12, 12) });

                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSecMeeting>("COURSE.SEC.MEETING", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(courseSecMeetings);

                var results = await sectionRepo.GetSectionFacultyAsync(0, 1, "", "", "");
                Assert.IsNotNull(results);
                var actuals = results.Item1;

                for (int i = 0; i < actuals.Count(); i++)
                {
                    var actual = actuals.ToList()[i];
                    var expected = courseSectionCollection.ToList()[i];
                    Assert.IsNotNull(actual);

                    Assert.AreEqual("92364642-4CF0-4640-B657-DC76CB7E289B", actual.Guid);
                    Assert.AreEqual("1", actual.Id);
                    Assert.AreEqual(new DateTime(2017, 9, 1), actual.EndDate);
                    Assert.AreEqual("1", actual.FacultyId);
                    Assert.AreEqual("ONL", actual.InstructionalMethodCode);
                    Assert.AreEqual(0, actual.MeetingLoadFactor);
                    Assert.AreEqual(true, actual.PrimaryIndicator);
                    Assert.AreEqual(10, actual.ResponsibilityPercentage);
                    Assert.AreEqual("12345", actual.SectionId);
                    Assert.AreEqual(new DateTime(2016, 9, 1), actual.StartDate);

                }
            }


            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionRepository_GetSectionFacultyByGuidAsync_Null()
            {
                var actual = await sectionRepo.GetSectionFacultyByGuidAsync(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionRepository_GetSectionFacultyByGuidAsync_Empty()
            {
                var actual = await sectionRepo.GetSectionFacultyByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionRepository_GetSectionFacultyByGuidAsync_CourseSecFaculty_Null()
            {
                dataReaderMock.Setup(dr => dr.ReadRecordAsync<CourseSecFaculty>(It.IsAny<GuidLookup>(), It.IsAny<bool>())).ReturnsAsync(null);

                var actual = await sectionRepo.GetSectionFacultyByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionRepository_GetSectionFacultyByGuidAsync_Invalid()
            {
                var guid = "92364642-4CF0-4640-B657-DC76CB7E289B";
                string[] sublist = new string[] { "1" };
                courseSectionCollection.Add(cs);
                dataReaderMock.Setup(dr => dr.SelectAsync("COURSE.SECTIONS", It.IsAny<string>())).ReturnsAsync(sublist);
                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(courseSectionCollection);

                dataReaderMock.Setup(dr => dr.SelectAsync("COURSE.SEC.FACULTY", It.IsAny<string>())).ReturnsAsync(sublist);

                var sectionFacultyResponseData = new CourseSecFaculty()
                {
                    Recordkey = "1",
                    RecordGuid = guid,
                    CsfInstrMethod = "ONL",
                    CsfStartDate = new DateTime(2016, 9, 1),
                    CsfEndDate = new DateTime(2017, 9, 1),
                    CsfFaculty = "1",
                    CsfCourseSection = "12345"

                };
                dataReaderMock.Setup(dr => dr.ReadRecordAsync<CourseSecFaculty>(It.IsAny<GuidLookup>(), It.IsAny<bool>())).ReturnsAsync(sectionFacultyResponseData);

                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSecMeeting>("COURSE.SEC.MEETING", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(null);

                 await sectionRepo.GetSectionFacultyByGuidAsync(guid);
                
            }

            [TestMethod]
            public async Task SectionRepository_GetSectionFacultyByGuidAsync()
            {
                var guid = "92364642-4CF0-4640-B657-DC76CB7E289B";
                string[] sublist = new string[] { "1" };
                courseSectionCollection.Add(cs);
                dataReaderMock.Setup(dr => dr.SelectAsync("COURSE.SECTIONS", It.IsAny<string>())).ReturnsAsync(sublist);
                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(courseSectionCollection);

                dataReaderMock.Setup(dr => dr.SelectAsync("COURSE.SEC.FACULTY", It.IsAny<string>())).ReturnsAsync(sublist);

                var sectionFacultyResponseData = new CourseSecFaculty()
                {
                    Recordkey = "1",
                    RecordGuid = guid,
                    CsfInstrMethod = "ONL",
                    CsfStartDate = new DateTime(2016, 9, 1),
                    CsfEndDate = new DateTime(2017, 9, 1),
                    CsfFaculty = "1",
                    CsfCourseSection = "12345"

                };

               // dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSecFaculty>("COURSE.SEC.FACULTY", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(sectionFacultyResponseData);
                //faculty =  await DataReader.ReadRecordAsync<CourseSecFaculty>(new GuidLookup(guid, null));
                dataReaderMock.Setup(dr => dr.ReadRecordAsync<CourseSecFaculty>(It.IsAny<GuidLookup>(), It.IsAny<bool>())).ReturnsAsync(sectionFacultyResponseData);


                var courseSecMeetings = new Collection<CourseSecMeeting>();
                courseSecMeetings.Add(new CourseSecMeeting() { Recordkey = "1", CsmInstrMethod = "ONL", CsmFriday = "Y", CsmCourseSection = "12345", CsmFrequency = "W", CsmStartDate = new DateTime(2016, 9, 1), CsmEndDate = new DateTime(2017, 12, 12) });

                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSecMeeting>("COURSE.SEC.MEETING", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(courseSecMeetings);

                var actual = await sectionRepo.GetSectionFacultyByGuidAsync(guid);
                var expected = courseSectionCollection.FirstOrDefault(x => x.RecordGuid == guid);
                Assert.IsNotNull(actual);

                Assert.AreEqual("92364642-4CF0-4640-B657-DC76CB7E289B", actual.Guid);
                Assert.AreEqual("1", actual.Id);
                Assert.AreEqual(new DateTime(2017, 9, 1), actual.EndDate);
                Assert.AreEqual("1", actual.FacultyId);
                Assert.AreEqual("ONL", actual.InstructionalMethodCode);
                Assert.AreEqual(0, actual.MeetingLoadFactor);
                Assert.AreEqual(true, actual.PrimaryIndicator);
                Assert.AreEqual(0, actual.ResponsibilityPercentage);
                Assert.AreEqual("12345", actual.SectionId);
                Assert.AreEqual(new DateTime(2016, 9, 1), actual.StartDate);
            }

            [TestMethod]
            public async Task SectionRepository_GetSectionFaculty_EmptyLimitOffset()
            {

                string[] sublist = new string[] { "1" };
                courseSectionCollection.Add(cs);
                dataReaderMock.Setup(dr => dr.SelectAsync("COURSE.SECTIONS", It.IsAny<string>())).ReturnsAsync(sublist);
                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(courseSectionCollection);

                dataReaderMock.Setup(dr => dr.SelectAsync("COURSE.SEC.FACULTY", It.IsAny<string>())).ReturnsAsync(sublist);

                var sectionFacultyResponseData = new Collection<CourseSecFaculty>();
                sectionFacultyResponseData.Add(new CourseSecFaculty()
                {
                    Recordkey = "1",
                    RecordGuid = "92364642-4CF0-4640-B657-DC76CB7E289B",
                    CsfInstrMethod = "ONL",
                    CsfStartDate = new DateTime(2016, 9, 1),
                    CsfEndDate = new DateTime(2017, 9, 1),
                    CsfFaculty = "1",
                    CsfCourseSection = "12345"

                });

                //dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSecFaculty>("COURSE.SEC.FACULTY", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(sectionFacultyResponseData);
                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSecFaculty>( It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(sectionFacultyResponseData);

                var courseSecMeetings = new Collection<CourseSecMeeting>();
                courseSecMeetings.Add(new CourseSecMeeting() { Recordkey = "1", CsmInstrMethod = "ONL", CsmFriday = "Y", CsmCourseSection = "12345", CsmFrequency = "W", CsmStartDate = new DateTime(2016, 9, 1), CsmEndDate = new DateTime(2017, 12, 12) });

                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSecMeeting>("COURSE.SEC.MEETING", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(courseSecMeetings);

                var results = await sectionRepo.GetSectionFacultyAsync(0, 0, "", "", "");
                Assert.IsNotNull(results);
                var actuals = results.Item1;

                for (int i = 0; i < actuals.Count(); i++)
                {
                    var actual = actuals.ToList()[i];
                    var expected = courseSectionCollection.ToList()[i];
                    Assert.IsNotNull(actual);

                    Assert.AreEqual("92364642-4CF0-4640-B657-DC76CB7E289B", actual.Guid);
                    Assert.AreEqual("1", actual.Id);
                    Assert.AreEqual(new DateTime(2017, 9, 1), actual.EndDate);
                    Assert.AreEqual("1", actual.FacultyId);
                    Assert.AreEqual("ONL", actual.InstructionalMethodCode);
                    Assert.AreEqual(0, actual.MeetingLoadFactor);
                    Assert.AreEqual(true, actual.PrimaryIndicator);
                    Assert.AreEqual(0, actual.ResponsibilityPercentage);
                    Assert.AreEqual("12345", actual.SectionId);
                    Assert.AreEqual(new DateTime(2016, 9, 1), actual.StartDate);

                }
            }

            [TestMethod]
            public async Task SectionRepository_GetSectionMeetingIdFromGuid_GuidLookupSuccess()
            {

                // Set up for GUID lookups
                var id = "12345";
                var id2 = "9876";
                var id3 = "0012345";

                var guid = "F5FC5310-17F1-49FC-926D-CC6E3DA6DAEA".ToLowerInvariant();
                var guid2 = "5B35075D-14FB-45F7-858A-83F4174B76EA".ToLowerInvariant();
                var guid3 = "246E16D9-8790-4D7E-ACA1-D5B1CB9D4A24".ToLowerInvariant();

                var guidLookup = new GuidLookup(guid);
                var guidLookupResult = new GuidLookupResult() { Entity = "COURSE.SEC.MEETING", PrimaryKey = id };
                var guidLookupDict = new Dictionary<string, GuidLookupResult>();
                var recordLookup = new RecordKeyLookup("COURSE.SEC.MEETING", id, false);
                var recordLookupResult = new RecordKeyLookupResult() { Guid = guid };
                var recordLookupDict = new Dictionary<string, RecordKeyLookupResult>();

                dataReaderMock.Setup(dr => dr.SelectAsync(It.IsAny<GuidLookup[]>())).Returns<GuidLookup[]>(gla =>
                {
                    if (gla.Any(gl => gl.Guid == guid))
                    {
                        guidLookupDict.Add(guid, guidLookupResult);
                    }
                    if (gla.Any(gl => gl.Guid == guid2))
                    {
                        guidLookupDict.Add(guid2, null);
                    }
                    if (gla.Any(gl => gl.Guid == guid3))
                    {
                        guidLookupDict.Add(guid3, new GuidLookupResult() { Entity = "COURSE.SEC.MEETING", PrimaryKey = id3 });
                    }
                    return Task.FromResult(guidLookupDict);
                });
                
                var result = await sectionRepo.GetSectionMeetingIdFromGuidAsync(guid);
                Assert.AreEqual(id, result);
            }

            [TestMethod]
            public async Task SectionRepository_GetSectionFacultyIdFromGuid_GuidLookupSuccess()
            {

                // Set up for GUID lookups
                var id = "12345";
                var id2 = "9876";
                var id3 = "0012345";

                var guid = "F5FC5310-17F1-49FC-926D-CC6E3DA6DAEA".ToLowerInvariant();
                var guid2 = "5B35075D-14FB-45F7-858A-83F4174B76EA".ToLowerInvariant();
                var guid3 = "246E16D9-8790-4D7E-ACA1-D5B1CB9D4A24".ToLowerInvariant();

                var guidLookup = new GuidLookup(guid);
                var guidLookupResult = new GuidLookupResult() { Entity = "COURSE.SEC.FACULTY", PrimaryKey = id };
                var guidLookupDict = new Dictionary<string, GuidLookupResult>();
                var recordLookup = new RecordKeyLookup("COURSE.SEC.FACULTY", id, false);
                var recordLookupResult = new RecordKeyLookupResult() { Guid = guid };
                var recordLookupDict = new Dictionary<string, RecordKeyLookupResult>();

                dataReaderMock.Setup(dr => dr.SelectAsync(It.IsAny<GuidLookup[]>())).Returns<GuidLookup[]>(gla =>
                {
                    if (gla.Any(gl => gl.Guid == guid))
                    {
                        guidLookupDict.Add(guid, guidLookupResult);
                    }
                    if (gla.Any(gl => gl.Guid == guid2))
                    {
                        guidLookupDict.Add(guid2, null);
                    }
                    if (gla.Any(gl => gl.Guid == guid3))
                    {
                        guidLookupDict.Add(guid3, new GuidLookupResult() { Entity = "COURSE.SEC.FACULTY", PrimaryKey = id3 });
                    }
                    return Task.FromResult(guidLookupDict);
                });

                var result = await sectionRepo.GetSectionFacultyIdFromGuidAsync(guid);
                Assert.AreEqual(id, result);
            }

            [TestMethod]
            public async Task SectionRepository_GetSectionFaculty_withFilters()
            {

                string[] sublist = new string[] { "1" };
                courseSectionCollection.Add(cs);
                dataReaderMock.Setup(dr => dr.SelectAsync("COURSE.SECTIONS", It.IsAny<string>())).ReturnsAsync(sublist);
                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSections>("COURSE.SECTIONS", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(courseSectionCollection);

                dataReaderMock.Setup(dr => dr.SelectAsync("COURSE.SEC.FACULTY", It.IsAny<string>())).ReturnsAsync(sublist);

                var sectionFacultyResponseData = new Collection<CourseSecFaculty>();
                sectionFacultyResponseData.Add(new CourseSecFaculty()
                {
                    Recordkey = "1",
                    RecordGuid = "92364642-4CF0-4640-B657-DC76CB7E289B",
                    CsfInstrMethod = "ONL",
                    CsfStartDate = new DateTime(2016, 9, 1),
                    CsfEndDate = new DateTime(2017, 9, 1),
                    CsfFaculty = "1",
                    CsfCourseSection = "12345"

                });

                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSecFaculty>("COURSE.SEC.FACULTY", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(sectionFacultyResponseData);

                var courseSecMeetings = new Collection<CourseSecMeeting>();
                courseSecMeetings.Add(new CourseSecMeeting() { Recordkey = "1", CsmInstrMethod = "ONL", CsmFriday = "Y", CsmCourseSection = "12345", CsmFrequency = "W", CsmStartDate = new DateTime(2016, 9, 1), CsmEndDate = new DateTime(2017, 12, 12) });

                dataReaderMock.Setup(dr => dr.BulkReadRecordAsync<CourseSecMeeting>("COURSE.SEC.MEETING", It.IsAny<string[]>(), It.IsAny<bool>())).ReturnsAsync(courseSecMeetings);

                var results = await sectionRepo.GetSectionFacultyAsync(0, 1, "12345", "1", "1");
                Assert.IsNotNull(results);
                var actuals = results.Item1;

                for (int i = 0; i < actuals.Count(); i++)
                {
                    var actual = actuals.ToList()[i];
                    var expected = courseSectionCollection.ToList()[i];
                    Assert.IsNotNull(actual);

                    Assert.AreEqual("92364642-4CF0-4640-B657-DC76CB7E289B", actual.Guid);
                    Assert.AreEqual("1", actual.Id);
                    Assert.AreEqual(new DateTime(2017, 9, 1), actual.EndDate);
                    Assert.AreEqual("1", actual.FacultyId);
                    Assert.AreEqual("ONL", actual.InstructionalMethodCode);
                    Assert.AreEqual(0, actual.MeetingLoadFactor);
                    Assert.AreEqual(true, actual.PrimaryIndicator);
                    Assert.AreEqual(0, actual.ResponsibilityPercentage);
                    Assert.AreEqual("12345", actual.SectionId);
                    Assert.AreEqual(new DateTime(2016, 9, 1), actual.StartDate);

                }
            }
        }


    #region Private helper methods - static so they can be used in any of the above subclasses

    private static void BuildLdmConfiguration(Mock<IColleagueDataReader> dataReaderMock, out CdDefaults cdDefaults)
        {
            cdDefaults = BuildCdDefaults();

            var sectionStatuses = new ApplValcodes();
            sectionStatuses.ValsEntityAssociation = new List<ApplValcodesVals>();
            sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("A", "Active", "1", "A", "", "", ""));
            sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("I", "Inactive", "2", "I", "", "", ""));
            sectionStatuses.ValsEntityAssociation.Add(new ApplValcodesVals("C", "Cancelled", "", "C", "", "", ""));

            // Set up repo response for waitlist statuses
            var waitlistCodeResponse = new ApplValcodes()
            {
                ValsEntityAssociation = new List<ApplValcodesVals>() {new ApplValcodesVals() { ValInternalCodeAssocMember = "A", ValExternalRepresentationAssocMember = "Active", ValActionCode1AssocMember = "1" },
                                                                   new ApplValcodesVals() { ValInternalCodeAssocMember = "E", ValExternalRepresentationAssocMember = "Enrolled", ValActionCode1AssocMember = "2"},
                                                                   new ApplValcodesVals() { ValInternalCodeAssocMember = "D", ValExternalRepresentationAssocMember = "Dropped", ValActionCode1AssocMember = "3"},
                                                                   new ApplValcodesVals() { ValInternalCodeAssocMember = "P", ValExternalRepresentationAssocMember = "Permission to Register", ValActionCode1AssocMember = "4"},
                                                                   new ApplValcodesVals() { ValInternalCodeAssocMember = "X", ValExternalRepresentationAssocMember = "Expired", ValActionCode1AssocMember = "5"},
                                                                   new ApplValcodesVals() { ValInternalCodeAssocMember = "C", ValExternalRepresentationAssocMember = "Cancelled", ValActionCode1AssocMember = "6"},
                                                                   new ApplValcodesVals() { ValInternalCodeAssocMember = "L", ValExternalRepresentationAssocMember = "Closed", ValActionCode1AssocMember = "7"},
                                                                   new ApplValcodesVals() { ValInternalCodeAssocMember = "OS", ValExternalRepresentationAssocMember = "Other Section Enrollment", ValActionCode1AssocMember = "8"}
                }
            };

            dataReaderMock.Setup(r => r.ReadRecordAsync<ApplValcodes>("ST.VALCODES", "SECTION.STATUSES", true)).Returns(Task.FromResult(sectionStatuses));
            dataReaderMock.Setup(r => r.ReadRecordAsync<ApplValcodes>("ST.VALCODES", "WAIT.LIST.STATUSES", true)).Returns(Task.FromResult(waitlistCodeResponse));

            dataReaderMock.Setup(r => r.SelectAsync(It.IsAny<RecordKeyLookup[]>())).Returns<RecordKeyLookup[]>(rkla =>
                {
                    var result = new Dictionary<string, RecordKeyLookupResult>();
                    foreach (var rkl in rkla)
                    {
                        result.Add(rkl.ResultKey, new RecordKeyLookupResult() { Guid = Guid.NewGuid().ToString().ToLowerInvariant() });
                    }
                    return Task.FromResult(result);
                });
        }

        private static CdDefaults BuildCdDefaults()
        {
            var cdDefaults = new CdDefaults()
            {
                CdAllowAuditFlag = "Y",
                CdAllowPassNopassFlag = "Y",
                CdAllowWaitlistFlag = "Y",
                CdCourseDelimiter = "-",
                CdFacultyConsentFlag = "N",
                CdInstrMethods = "LEC",
                CdOnlyPassNopassFlag = "N",
                CdReqsConvertedFlag = "N",
                CdWaitlistRating = "SR"
            };
            return cdDefaults;
        }

        private static StwebDefaults BuildStwebDefaults()
        {
            string template = "abc?a={4}&b={5}&c={0}&d={1}&e={3}&f={2}&a={4}&b={5}&c={0}&d={1}&e={3}&f={2}";
            StwebDefaults stwebDefaults = new StwebDefaults();
            stwebDefaults.Recordkey = "STWEB.DEFAULTS";
            stwebDefaults.StwebBookstoreUrlTemplate = template;
            stwebDefaults.StwebRegUsersId = "REGUSERID";
            return stwebDefaults;
        }

        private static CourseSecMeeting BuildCourseSecMeetingDefaults()
        {
            CourseSecMeeting courseSecMeet = new CourseSecMeeting();
            courseSecMeet.Recordkey = "12345";
            courseSecMeet.CsmInstrMethod = "LAB";
            return courseSecMeet;
        }

        private static Collection<InstrMethods> BuildValidInstrMethodResponse()
        {
            var instructionalMethods = new Collection<InstrMethods>();
            instructionalMethods.Add(new InstrMethods() { RecordGuid = Guid.NewGuid().ToString(), Recordkey = "LEC", InmDesc = "Lecture", InmOnline = "" });
            instructionalMethods.Add(new InstrMethods() { RecordGuid = Guid.NewGuid().ToString(), Recordkey = "LAB", InmDesc = "Lab", InmOnline = "N" });
            instructionalMethods.Add(new InstrMethods() { RecordGuid = Guid.NewGuid().ToString(), Recordkey = "ONL", InmDesc = "Online", InmOnline = "Y" });
            return instructionalMethods;
        }

        private static string ConvertSectionStatusToCode(SectionStatus sectionStatus)
        {
            switch (sectionStatus)
            {
                case SectionStatus.Active:
                    return "A";
                case SectionStatus.Cancelled:
                    return "C";
                case SectionStatus.Inactive:
                default:
                    return "I";
            }
        }

        private static int CountMeetingDaysPerWeek(CourseSecMeeting meeting)
        {
            int count = 0;
            if (meeting.CsmSunday == "Y") count += 1;
            if (meeting.CsmMonday == "Y") count += 1;
            if (meeting.CsmTuesday == "Y") count += 1;
            if (meeting.CsmWednesday == "Y") count += 1;
            if (meeting.CsmThursday == "Y") count += 1;
            if (meeting.CsmFriday == "Y") count += 1;
            if (meeting.CsmSaturday == "Y") count += 1;
            return count;
        }

        #endregion
    }
}