﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.Caching;
using Ellucian.Colleague.Data.Student.DataContracts;
using Ellucian.Colleague.Data.Student.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Web.Cache;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System.Threading.Tasks;
using System.Threading;
using System;

namespace Ellucian.Colleague.Data.Student.Tests.Repositories
{
    [TestClass]
    public class BookRepositoryTests
    {
        BookRepository bookRepo;

        [TestInitialize]
        public void Initialize()
        {
            bookRepo = BuildValidRepository();
        }

        [TestCleanup]
        public void Cleanup()
        {
            bookRepo = null;
        }

        [TestMethod]
        public async Task BookCount_Valid()
        {
            var allBooks =await bookRepo.GetAsync();
            Assert.AreEqual(6, allBooks.Count());
        }

        [TestMethod]
        public async Task CheckBookProperties_Valid()
        {
            var book =await bookRepo.GetAsync("333");
            Assert.AreEqual("333", book.Id);
            Assert.AreEqual(120.00m, book.Price);
        }

        [TestMethod]
        public async Task CheckBookProperties_NullPrice()
        {
            var book = await bookRepo.GetAsync("666");
            Assert.IsNull(book.Price);
        }

        private BookRepository BuildValidRepository()
        {
            var transFactoryMock = new Mock<IColleagueTransactionFactory>();

            var loggerMock = new Mock<ILogger>();

            // Cache mocking
            var cacheProviderMock = new Mock<ICacheProvider>();
            var localCacheMock = new Mock<ObjectCache>();
            //cacheProviderMock.Setup(provider => provider.GetCache(It.IsAny<string>())).Returns(localCacheMock.Object);
            cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
            x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
            .ReturnsAsync(new Tuple<object, SemaphoreSlim>(
                null,
                new SemaphoreSlim(1, 1)
                ));


            // Set up data accessor for mocking 
            var dataAccessorMock = new Mock<IColleagueDataReader>();
            transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(dataAccessorMock.Object);

            // Set up Book Response
            var bookResponseData = BuildBookResponse();

            // Set up term response for "all" book requests
            dataAccessorMock.Setup<Task<Collection<Books>>>(acc => acc.BulkReadRecordAsync<Books>("BOOKS", "", true)).Returns(Task.FromResult(bookResponseData));

            // Set up term response for "one" book requests
            Books bookItem = bookResponseData.Where(b => b.Recordkey == "333").FirstOrDefault();
            dataAccessorMock.Setup<Task<Books>>(acc => acc.ReadRecordAsync<Books>("BOOKS", "333", true)).Returns(Task.FromResult(bookItem));

            Books bookItem2 = bookResponseData.Where(b => b.Recordkey == "666").FirstOrDefault();
            dataAccessorMock.Setup<Task<Books>>(acc => acc.ReadRecordAsync<Books>("BOOKS", "666", true)).Returns(Task.FromResult(bookItem2));

            // Construct referenceData repository
            bookRepo = new BookRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);

            return bookRepo;
        }

        private Collection<Books> BuildBookResponse()
        {
            Collection<Books> bookData = new Collection<Books>();
            List<string> recordData = new List<string>() { "111", "222", "333", "444", "555" };
            decimal setPrice = 100.00m;
            foreach (var bk in recordData)
            {
                Books book = new Books();
                book.Recordkey = bk.ToString();
                book.BookPrice = setPrice;
                setPrice += 10.00m;
                bookData.Add(book);
            }
            // Add one book with no price.
            Books priceless = new Books();
            priceless.Recordkey = "666";
            bookData.Add(priceless);
            return bookData;
        }
    }
}
