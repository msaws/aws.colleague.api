﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Tests
{
    public class TestTimeEntryCommentsRepository : ITimeEntryCommentsRepository
    {
        public class TimecardCommentRecord
        {
            public string id;
            public string employeeID;
            public string timecardId;
            public string positionId;
            public string payCycleId;
            public DateTime? payPeriodEndDate;
            public string addopr;
            public DateTime? addDate;
            public DateTime? addTime;
            public string chgOpr;
            public DateTime? chgDate;
            public DateTime? chgTime;
            public string comments;
        }

        public List<TimecardCommentRecord> timecardCommentRecords = new List<TimecardCommentRecord>() 
        {
            new TimecardCommentRecord() {
                id = "1",
                employeeID = "24601",
                timecardId = "1",
                payCycleId = "BW",
                payPeriodEndDate = new DateTime(2016, 7, 4),
                positionId = "ZDFA12345MUSIC",
                addopr = "24601",
                addDate = new DateTime(2016,01,01),
                addTime = new DateTime(2016,01,01, 7, 0, 0),
                chgOpr = "24601",
                chgDate = new DateTime(2016,01,01),
                chgTime = new DateTime(2016,01,01, 7, 0, 0),              
            },
            new TimecardCommentRecord() {
                id = "1",
                employeeID = "24601",
                addopr = "24601",
                payCycleId = "BW",
                payPeriodEndDate = new DateTime(2016, 7, 4),
                positionId = "ZDFA12345MUSIC",
                addDate = new DateTime(2016,01,01),
                addTime = new DateTime(2016,01,01, 7, 0, 0),
                chgOpr = "24601",
                chgDate = new DateTime(2016,01,01),
                chgTime = new DateTime(2016,01,01, 7, 0, 0),              
            },
            new TimecardCommentRecord() {
                id = "1",
                employeeID = "24601",
                payCycleId = "BW",
                payPeriodEndDate = new DateTime(2016, 7, 4),
                positionId = "ZDFA12345MUSIC",
                addopr = "24601",
                addDate = new DateTime(2016,01,01),
                addTime = new DateTime(2016,01,01, 7, 0, 0),
                chgOpr = "24601",
                chgDate = new DateTime(2016,01,01),
                chgTime = new DateTime(2016,01,01, 7, 0, 0),              
            }
        };


        public async Task<IEnumerable<TimeEntryComment>> GetCommentsAsync(IEnumerable<string> employeeIds)
        {
            return await Task.FromResult(
                timecardCommentRecords
                    .Where(r => employeeIds.Contains(r.employeeID))
                    .Select(dbObj => BuildCommentsEntity(dbObj)).ToList());
        }

        public Task<TimeEntryComment> CreateCommentsAsync(TimeEntryComment timecardComments)
        {
            throw new NotImplementedException();
        }

        public TimeEntryComment BuildCommentsEntity(TimecardCommentRecord dbCommments)
        {
            return new TimeEntryComment(
                dbCommments.id,
                dbCommments.employeeID,
                dbCommments.timecardId,
                dbCommments.positionId,
                dbCommments.payCycleId,
                dbCommments.payPeriodEndDate.Value,
                dbCommments.comments,
                new Timestamp(dbCommments.addopr,
                    dbCommments.addDate.Value.Add(dbCommments.addTime.Value.TimeOfDay),
                    dbCommments.chgOpr,
                    dbCommments.chgDate.Value.Add(dbCommments.chgTime.Value.TimeOfDay))
             );
        }

        public string AddCommentRecord(TimecardCommentRecord record)
        {
            var newId = timecardCommentRecords.Select(r => r.id).Aggregate((x, y) => x + y).GetHashCode().ToString();
            record.id = newId;
            record.addDate = DateTime.Today;
            record.addTime = DateTime.Now;
            record.addopr = "YOU";
            record.chgDate = DateTime.Today;
            record.chgTime = DateTime.Now;
            record.chgOpr = "YOU";
            timecardCommentRecords.Add(record);
            return newId;
        }

    }
}
