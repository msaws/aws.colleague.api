﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Tests
{
    public class TestOvertimeCalculationRepository : IOvertimeCalculationRepository
    {
        public class OvertimeCalculationResponse
        {
            public List<ThresholdResponse> thresholds;
            public List<DailyAllocationResponse> dailyAllocations;
        }
        public class ThresholdResponse
        {
            public string earningsTypeId;
            public decimal? earningsFactor;
            public int? totalOvertimeMinutes;
        }
        public class DailyAllocationResponse
        {
            public int? thresholdPosition;
            public int? overtimeMinutes;
            public DateTime? date;
        }

        public OvertimeCalculationResponse testResponse = new OvertimeCalculationResponse()
        {
            thresholds = new List<ThresholdResponse>() 
            {
                new ThresholdResponse() {
                    earningsTypeId = "OVT",
                    earningsFactor = 1.5005m,
                    totalOvertimeMinutes = 480
                },
                new ThresholdResponse() 
                {
                    earningsTypeId = "WKND",
                    earningsFactor = 2,
                    totalOvertimeMinutes = 480,
                }
            },
            dailyAllocations = new List<DailyAllocationResponse>() 
            {
                new DailyAllocationResponse() 
                {
                    date = new DateTime(2016, 1, 1),
                    overtimeMinutes = 240,
                    thresholdPosition = 1
                },
                new DailyAllocationResponse()
                {
                    date = new DateTime(2016, 1, 2),
                    overtimeMinutes = 240,
                    thresholdPosition = 1
                }
            }
        };

        //TODO: write domain tests, modify coord service to get timecards, 
        // maybe modify timecard repository to build timecards for specified date range
        public async Task<OvertimeCalculationResult> CalculateOvertime(string personId, DateTime startDate, DateTime endDate, string payCycleId)
        {
            return await Task.FromResult(
                new OvertimeCalculationResult(personId, payCycleId, startDate, endDate, DateTime.Now)
                {
                    Thresholds = testResponse.thresholds.Select(t =>
                        new OvertimeThresholdResult(t.earningsTypeId, t.earningsFactor.Value, TimeSpan.FromMinutes(t.totalOvertimeMinutes.Value))
                        {
                            DailyAllocations = testResponse.dailyAllocations
                                .Where(d => d.thresholdPosition == testResponse.thresholds.IndexOf(t))
                                .Select(d => new DailyOvertimeAllocation(d.date.Value, TimeSpan.FromMinutes(d.overtimeMinutes.Value))).ToList()
                        }).ToList()
                });
        }
    }
}
