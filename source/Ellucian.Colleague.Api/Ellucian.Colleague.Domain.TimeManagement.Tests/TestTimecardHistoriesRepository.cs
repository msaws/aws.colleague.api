﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Data.Colleague;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Tests
{
    public class TestTimecardHistoriesRepository : ITimecardHistoriesRepository
    {

        public List<string> timecardHistoryIds = new List<string>();
        private string colleagueTimeZone = "Eastern Standard Time";
        public async Task<string> CreateTimecardHistoryAsync(Timecard timecard, TimecardStatus timecardStatus, IEnumerable<TimeEntryComment> timecardComments)
        {
            return await Task.FromResult(CreateTimecardHistoryRecordHelper());
        }

        public async Task<IEnumerable<TimecardHistory>> GetTimecardHistoriesAsync(DateTime startDate, DateTime endDate, IEnumerable<string> employeeIds)
        {
            var dbTimecardHistories = this.DatabaseTimecardHistories.Where(t => employeeIds.Any(i => i == t.TcdhEmployeeId) && t.TcdhEndDate >= startDate && t.TcdhStartDate <= endDate);
            var dbTimeEntryHistories = this.DatabaseTimeEntryHistories.Where(e => dbTimecardHistories.Any(t => t.Recordkey == e.TehTimecardHistoryId));

            var domainTimecardHistories = new List<TimecardHistory>();

            var timeEntryHistoryLookup = dbTimeEntryHistories.ToLookup(te => te.TehTimecardHistoryId);
            foreach (var tc in dbTimecardHistories)
            {
                var timecardHistory = this.MapToDomainTimecardHistory(tc);

                foreach (var te in timeEntryHistoryLookup[tc.Recordkey])
                {
                    timecardHistory.TimeEntryHistories.Add(this.MapToDomainTimeEntryHistory(te));
                }

                domainTimecardHistories.Add(timecardHistory);
            }
            return await Task.FromResult(domainTimecardHistories);
        }

        public async Task<string> CreateTimecardHistory2Async(Timecard2 timecard, TimecardStatus timecardStatus, IEnumerable<TimeEntryComment> timecardComments)
        {
            return await Task.FromResult(CreateTimecardHistoryRecordHelper());
        }

        public async Task<IEnumerable<TimecardHistory2>> GetTimecardHistories2Async(DateTime startDate, DateTime endDate, IEnumerable<string> employeeIds)
        {
            var dbTimecardHistories = this.DatabaseTimecardHistories.Where(t => employeeIds.Any(i => i == t.TcdhEmployeeId) && t.TcdhEndDate >= startDate && t.TcdhStartDate <= endDate);
            var dbTimeEntryHistories = this.DatabaseTimeEntryHistories.Where(e => dbTimecardHistories.Any(t => t.Recordkey == e.TehTimecardHistoryId));

            var domainTimecardHistories = new List<TimecardHistory2>();

            var timeEntryHistoryLookup = dbTimeEntryHistories.ToLookup(te => te.TehTimecardHistoryId);
            foreach (var tc in dbTimecardHistories)
            {
                var timecardHistory = this.MapToDomainTimecardHistory(tc);

                foreach (var te in timeEntryHistoryLookup[tc.Recordkey])
                {
                    timecardHistory.TimeEntryHistories.Add(this.MapToDomainTimeEntryHistory(te));
                }

                //domainTimecardHistories.Add(timecardHistory);
            }
            return await Task.FromResult(domainTimecardHistories);
        }



        private Random randomHistoryIdGenerator = new Random();
        public string CreateTimecardHistoryRecordHelper()
        {
            var id = randomHistoryIdGenerator.Next().ToString();
            timecardHistoryIds.Add(id);
            return id;
        }

        public IEnumerable<TimecardHistoryRecord> DatabaseTimecardHistories = new List<TimecardHistoryRecord>()
        {          
            new TimecardHistoryRecord()
            {
                Recordkey = "001",
                TcdhStartDate = new DateTime(2016,09,01),
                TcdhEndDate = new DateTime(2016,09,07),
                TcdhPaycycleId= "TMA-0Ɩ",
                TcdhPeriodStartDate = new DateTime(2016,09,01),
                TcdhPeriodEndDate = new DateTime(2016,09,14),
                TcdhPositionId = "CLVS",
                TimecardHistoryAdddate = new DateTime(2016,01,01),
                TimecardHistoryAddtime =new DateTime(2016,01,01),
                TimecardHistoryChgdate = new DateTime(2016,01,01),
                TimecardHistoryChgtime = new DateTime(2016,01,01),
                TimecardHistoryAddopr = "Sasquatch",
                TimecardHistoryChgopr = "ɥɔʇɐnbsɐS",
                TcdhEmployeeId = "24601",
                TcdhStatusActionType = "Paid"
                
            },
            new TimecardHistoryRecord()
            {
                Recordkey = "002",
                TcdhStartDate = new DateTime(2016,09,07),
                TcdhEndDate = new DateTime(2016,09,14),
                TcdhPaycycleId= "TMA-01",
                TcdhPeriodStartDate = new DateTime(2016,09,01),
                TcdhPeriodEndDate = new DateTime(2016,09,14),
                TcdhPositionId = "CLVS",
                TimecardHistoryAdddate = new DateTime(2016,01,01),
                TimecardHistoryAddtime =new DateTime(2016,01,01),
                TimecardHistoryChgdate = new DateTime(2016,01,01),
                TimecardHistoryChgtime = new DateTime(2016,01,01),
                TimecardHistoryAddopr = "Sasquatch",
                TimecardHistoryChgopr = "ɥɔʇɐnbsɐS",
                TcdhEmployeeId = "24601",
                TcdhStatusActionType = "Submitted"
            },
        };

        public IEnumerable<TimeEntryHistoryRecord> DatabaseTimeEntryHistories = new List<TimeEntryHistoryRecord>()
        {          
            new TimeEntryHistoryRecord()
            {
                Recordkey = "001",
                TehEarntypeId  = "Q",
                TehInDate = new DateTime(2016,09,01),
                TehInTime = null,
                TehOutDate = new DateTime(2016,09,01),
                TehOutTime = null,
                TehPerleaveId= "R",
                TehProjectId  = "S",
                TehTimecardHistoryId = "001",
                TehWorkedDate = new DateTime(2016,09,01),
                TehWorkedMinutes  = 42,
                TimeEntryHistoryAdddate = new DateTime(2016,09,01),
                TimeEntryHistoryChgdate = new DateTime(2016,09,01),
                TimeEntryHistoryChgtime = new DateTime(2016,09,01),
                TimeEntryHistoryChgopr = "Sasquatch",
                TimeEntryHistoryAddtime = new DateTime(2016,09,01),
                TimeEntryHistoryAddopr = "Sasquatch",

            },
            new TimeEntryHistoryRecord()
            {
                Recordkey = "002",
                TehEarntypeId  = "Q",
                TehInDate = new DateTime(2016,09,03),
                TehInTime = null,
                TehOutDate = new DateTime(2016,09,03),
                TehOutTime = null,
                TehPerleaveId= "R",
                TehProjectId  = "S",
                TehTimecardHistoryId = "001",
                TehWorkedDate = new DateTime(2016,09,01),
                TehWorkedMinutes  = 96,
                TimeEntryHistoryAdddate = new DateTime(2016,09,01),
                TimeEntryHistoryChgdate = new DateTime(2016,09,01),
                TimeEntryHistoryChgtime = new DateTime(2016,09,01),
                TimeEntryHistoryChgopr = "Sasquatch",
                TimeEntryHistoryAddtime = new DateTime(2016,09,01),
                TimeEntryHistoryAddopr = "Sasquatch",

            },
            new TimeEntryHistoryRecord()
            {
                Recordkey = "003",
                TehEarntypeId  = "Q",
                TehInDate = new DateTime(2016,09,09),
                TehInTime = null,
                TehOutDate = new DateTime(2016,09,09),
                TehOutTime = null,
                TehPerleaveId= "R",
                TehProjectId  = "S",
                TehTimecardHistoryId = "002",
                TehWorkedDate = new DateTime(2016,09,09),
                TehWorkedMinutes  = 48,
                TimeEntryHistoryAdddate = new DateTime(2016,09,09),
                TimeEntryHistoryChgdate = new DateTime(2016,09,09),
                TimeEntryHistoryChgtime = new DateTime(2016,09,09),
                TimeEntryHistoryChgopr = "Sasquatch",
                TimeEntryHistoryAddtime = new DateTime(2016,09,09),
                TimeEntryHistoryAddopr = "Sasquatch",

            },
        };

        private TimecardHistory MapToDomainTimecardHistory(TimecardHistoryRecord tc)
        {
            StatusAction statusAction = (StatusAction)Enum.Parse(typeof(StatusAction), tc.TcdhStatusActionType, true);

            return new TimecardHistory(
                tc.Recordkey,
                tc.TcdhEmployeeId,
                tc.TcdhPaycycleId,
                tc.TcdhPositionId,
                tc.TcdhPeriodStartDate.Value,
                tc.TcdhPeriodEndDate.Value,
                tc.TcdhStartDate.Value,
                tc.TcdhEndDate.Value,
                statusAction,
                new Timestamp(tc.TimecardHistoryAddopr,
                    tc.TimecardHistoryAddtime.ToPointInTimeDateTimeOffset(tc.TimecardHistoryAdddate, colleagueTimeZone).Value,
                    tc.TimecardHistoryChgopr,
                    tc.TimecardHistoryChgtime.ToPointInTimeDateTimeOffset(tc.TimecardHistoryChgdate, colleagueTimeZone).Value)
            ) { };
        }

        private TimeEntryHistory MapToDomainTimeEntryHistory(TimeEntryHistoryRecord te)
        {
            return new TimeEntryHistory(
                te.Recordkey,
                te.TehTimecardHistoryId,
                te.TehEarntypeId,
                new TimeSpan(0, te.TehWorkedMinutes.Value, 0),
                te.TehWorkedDate.Value,
                te.TehPerleaveId,
                te.TehProjectId,
                new Timestamp(te.TimeEntryHistoryAddopr,
                    te.TimeEntryHistoryAddtime.ToPointInTimeDateTimeOffset(te.TimeEntryHistoryAdddate, colleagueTimeZone).Value,
                    te.TimeEntryHistoryChgopr,
                    te.TimeEntryHistoryChgtime.ToPointInTimeDateTimeOffset(te.TimeEntryHistoryChgdate, colleagueTimeZone).Value)
            ) { };
        }
    }

    public class TimecardHistoryRecord
    {
        public string Recordkey { get; set; }
        public DateTime? TcdhStartDate { get; set; }
        public DateTime? TcdhEndDate { get; set; }
        public string TcdhPaycycleId { get; set; }
        public DateTime? TcdhPeriodStartDate { get; set; }
        public DateTime? TcdhPeriodEndDate { get; set; }
        public string TcdhPositionId { get; set; }
        public DateTime? TimecardHistoryAdddate { get; set; }
        public DateTime? TimecardHistoryAddtime { get; set; }
        public DateTime? TimecardHistoryChgdate { get; set; }
        public DateTime? TimecardHistoryChgtime { get; set; }
        public string TimecardHistoryAddopr { get; set; }
        public string TimecardHistoryChgopr { get; set; }
        public string TcdhEmployeeId { get; set; }
        public string TcdhStatusActionType;

    }

    public class TimeEntryHistoryRecord
    {
        public string Recordkey { get; set; }
        public string TehEarntypeId { get; set; }
        public DateTime? TehInDate { get; set; }
        public DateTime? TehInTime { get; set; }
        public DateTime? TehOutDate { get; set; }
        public DateTime? TehOutTime { get; set; }
        public string TehPerleaveId { get; set; }
        public string TehProjectId { get; set; }
        public string TehTimecardHistoryId { get; set; }
        public DateTime? TehWorkedDate { get; set; }
        public int? TehWorkedMinutes { get; set; }
        public DateTime? TimeEntryHistoryAdddate { get; set; }
        public DateTime? TimeEntryHistoryChgdate { get; set; }
        public DateTime? TimeEntryHistoryChgtime { get; set; }
        public string TimeEntryHistoryChgopr { get; set; }
        public DateTime? TimeEntryHistoryAddtime { get; set; }
        public string TimeEntryHistoryAddopr { get; set; }
    }
}
