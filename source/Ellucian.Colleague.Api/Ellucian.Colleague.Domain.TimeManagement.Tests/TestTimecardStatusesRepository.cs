﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Domain.Base.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;

namespace Ellucian.Colleague.Domain.TimeManagement.Tests
{
    public class TestTimecardStatusesRepository : ITimecardStatusesRepository
    {
        public const string colleagueTimeZone = "Eastern Standard Time";

        public class TimecardStatusRecord
        {
            public string id;
            public string actionerId;
            public string actionType;
            public string comments;
            public string timecardId;
            public DateTime? addDate;
            public string addOpr;
            public DateTime? addTime;
            public DateTime? chgDate;
            public string chgOpr;
            public DateTime? chgTime;
        }

        public List<TimecardStatusRecord> timecardStatusRecords = new List<TimecardStatusRecord>()
        {
            new TimecardStatusRecord()
            {
                id = "001",
                actionerId = "24601",
                actionType = "Submitted",
                comments = "this is a comment",
                timecardId = "5",
                addDate = new DateTime(2016, 6, 14),
                addOpr = "0003914",
                addTime = new DateTime(2016, 6, 14, 5, 46, 1),
                chgDate = new DateTime(2016, 6, 14),
                chgOpr = "0003914",
                chgTime = new DateTime(2016, 6, 14, 5, 46, 1),
            },
            new TimecardStatusRecord()
            {
                id = "002",
                actionerId = "24601",
                actionType = "Unsubmitted",
                comments = "this is a comment",
                timecardId = "5",
                addDate = new DateTime(2016, 6, 14),
                addOpr = "0003914",
                addTime = new DateTime(2016, 6, 14, 5, 46, 1),
                chgDate = new DateTime(2016, 6, 14),
                chgOpr = "0003914",
                chgTime = new DateTime(2016, 6, 14, 5, 46, 1),
            }
        };



        public async Task<IEnumerable<TimecardStatus>> GetTimecardStatusesByTimecardIdAsync(string timecardId)
        {
            return await Task.FromResult(timecardStatusRecords
                .Where(rec => rec.timecardId == timecardId)
                .Select(rec => new TimecardStatus(rec.timecardId, rec.actionerId, ConvertDatabaseStringToStatusAction(rec.actionType), rec.id)
                {                           
                    Timestamp = new Timestamp(rec.addOpr,
                        rec.addDate.Value.AddHours(rec.addTime.Value.Hour).AddMinutes(rec.addTime.Value.Minute).AddSeconds(rec.addTime.Value.Second),
                        rec.chgOpr,
                        rec.chgDate.Value.AddHours(rec.chgTime.Value.Hour).AddMinutes(rec.chgTime.Value.Minute).AddSeconds(rec.chgTime.Value.Second))
       
                }));
        }

        public async Task<IEnumerable<TimecardStatus>> CreateTimecardStatusesAsync(List<TimecardStatus> statuses)
        {
            List<TimecardStatusRecord> records = new List<TimecardStatusRecord>();
            List<TimecardStatus> newTimecardStatuses = new List<TimecardStatus>();
            foreach (var status in statuses)
            {
                var record = CreateTimecardStatusRecordHelper(status.ActionerId, ConvertStatusActionToDatabaseString(status.ActionType), null, status.TimecardId);
                var timecardStatuses = await GetTimecardStatusesByTimecardIdAsync(status.TimecardId);
                newTimecardStatuses.Add(timecardStatuses.Where(t=>t.Id == record.id).FirstOrDefault());
            }
            return newTimecardStatuses;
        }

        public TimecardStatusRecord CreateTimecardStatusRecordHelper(string actionerId, string actionType, TimeEntryComment comments, string timecardId)
        {
            var idGen = new Random().Next();

            var record = new TimecardStatusRecord()
            {
                id = idGen.ToString(),
                actionerId = actionerId,
                actionType = actionType,
                timecardId = timecardId,
                addDate = DateTime.Now,
                addOpr = "0003914",
                addTime = DateTime.Now,
                chgDate = DateTime.Now,
                chgOpr = "0003914",
                chgTime = DateTime.Now
            };
            timecardStatusRecords.Add(record);
            return record;
            
        }

        private StatusAction ConvertDatabaseStringToStatusAction(string actionType)
        {
            switch (actionType.ToUpperInvariant())
            {
                case "UNSUBMITTED":
                    return StatusAction.Unsubmitted;
                case "SUBMITTED":
                    return StatusAction.Submitted;
                default:
                    throw new Exception("unknown action type");
            }
        }

        private string ConvertStatusActionToDatabaseString(StatusAction action)
        {
            switch (action)
            {
                case StatusAction.Submitted:
                    return "Submitted";
                case StatusAction.Unsubmitted:
                    return "Unsubmitted";
                default:
                    throw new Exception("Unknown status action");
            }
        }


        public async Task<IEnumerable<TimecardStatus>> GetTimecardStatusesByPersonIdsAsync(IEnumerable<string> personIds)
        {
            return await Task.FromResult(timecardStatusRecords
                .Select(rec => new TimecardStatus(rec.timecardId, rec.actionerId, ConvertDatabaseStringToStatusAction(rec.actionType), rec.id)
                    {
                    Timestamp = new Timestamp( rec.addOpr,
                         rec.addDate.Value.AddHours(rec.addTime.Value.Hour).AddMinutes(rec.addTime.Value.Minute).AddSeconds(rec.addTime.Value.Second),
                         rec.chgOpr,
                         rec.chgDate.Value.AddHours(rec.chgTime.Value.Hour).AddMinutes(rec.chgTime.Value.Minute).AddSeconds(rec.chgTime.Value.Second))
                   
                }));
        }
    }
}
