﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.Base.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Colleague.Domain.TimeManagement.Entities;


namespace Ellucian.Colleague.Domain.TimeManagement.Tests
{
    public class TestTimecardsRepository : ITimecardsRepository
    {
        public const string colleagueTimeZone = "Eastern Standard Time";

        public class TimecardRecord
        {
            public string id;
            public DateTime? addDate;
            public string addOpr;
            public DateTime? addTime;
            public DateTime? chgDate;
            public string chgOpr;
            public DateTime? chgTime;
            public string comments;
            public string employeeId;
            public string payCycleId;
            public DateTime? periodStartDate;
            public DateTime? periodEndDate;
            public DateTime? startDate;
            public DateTime? endDate;
            public string positionId;
        }

        public class TimeEntryRecord
        {
            public string id;
            public DateTime? addDate;
            public string addOpr;
            public DateTime? addTime;
            public DateTime? chgDate;
            public string chgOpr;
            public DateTime? chgTime;
            public string earnTypeId;
            public DateTime? inDate;
            public DateTime? inTime;
            public DateTime? outDate;
            public DateTime? outTime;
            public string personLeaveId;
            public string projectId;
            public string timecardId;
            public int? workedMinutes;
            public DateTime? workedDate;
        }

        public List<TimecardRecord> timecardRecords = new List<TimecardRecord>()
        {
            new TimecardRecord()
            {
                id = "1",
                addDate = new DateTime(2016, 6, 14),
                addOpr = "0003914",
                addTime = new DateTime(2016, 6, 14, 8, 30, 0),
                chgDate = new DateTime(2016, 6, 14),
                chgOpr = "0003914",
                chgTime = new DateTime(2016, 6, 14, 8, 30, 0),
                comments = "a bb ccc dddd eeeee ffff ggg hh i",
                employeeId = "0003914",
                payCycleId = "52",
                periodEndDate = new DateTime(2016, 6, 30),
                periodStartDate = new DateTime(2016, 6, 1),
                startDate = new DateTime(2016, 6, 11),
                endDate = new DateTime(2016, 6, 17),
                positionId = "ZMUSIC",
            },
            new TimecardRecord()
            {
                id = "2", //this is a timein/timeout timecard
                addDate = new DateTime(2016, 6, 14),
                addOpr = "0003914",
                addTime = new DateTime(2016, 6, 14, 8, 30, 0),
                chgDate = new DateTime(2016, 6, 14),
                chgOpr = "0003914",
                chgTime = new DateTime(2016, 6, 14, 8, 30, 0),
                comments = null,
                employeeId = "0003914",
                payCycleId = "52",
                periodEndDate = new DateTime(2016, 6, 30),
                periodStartDate = new DateTime(2016, 6, 1),
                startDate = new DateTime(2016, 6, 11),
                endDate = new DateTime(2016, 6, 17),
                positionId = "ZCOMPSCI",              
            },
            new TimecardRecord()
            {
                id = "3",
                addDate = new DateTime(2016, 6, 14),
                addOpr = "0003914",
                addTime = new DateTime(2016, 6, 14, 8, 30, 0),
                chgDate = new DateTime(2016, 6, 14),
                chgOpr = "0003914",
                chgTime = new DateTime(2016, 6, 14, 8, 30, 0),
                comments = null,
                employeeId = "0003914",
                payCycleId = "52",
                periodEndDate = new DateTime(2016, 6, 30),
                periodStartDate = new DateTime(2016, 6, 1),
                startDate = new DateTime(2016, 6, 11),
                endDate = new DateTime(2016, 6, 17),
                positionId = "ZPERF",  
            },
        };

        public List<TimeEntryRecord> timeEntryRecords = new List<TimeEntryRecord>()
        {
            new TimeEntryRecord() 
            {
                id = "1",
                addDate = new DateTime(2016, 6, 14),
                addOpr = "0003914",
                addTime = new DateTime(2016, 6, 14, 8, 30, 0),
                chgDate = new DateTime(2016, 6, 14),
                chgOpr = "0003914",
                chgTime = new DateTime(2016, 6, 14, 8, 30, 0),
                earnTypeId = "WORK",
                inDate = null,
                inTime = null,
                outDate = null,
                outTime = null,
                personLeaveId = null,
                projectId = null,
                timecardId = "1",
                workedDate = new DateTime(2016, 6, 11),
                workedMinutes = 480,
            },
            new TimeEntryRecord() 
            {
                id = "2",
                addDate = new DateTime(2016, 6, 14),
                addOpr = "0003914",
                addTime = new DateTime(2016, 6, 14, 8, 30, 0),
                chgDate = new DateTime(2016, 6, 14),
                chgOpr = "0003914",
                chgTime = new DateTime(2016, 6, 14, 8, 30, 0),
                earnTypeId = "WORK",
                inDate = null,
                inTime = null,
                outDate = null,
                outTime = null,
                personLeaveId = null,
                projectId = null,
                timecardId = "1",
                workedDate = new DateTime(2016, 6, 12),
                workedMinutes = 480,
            },
            new TimeEntryRecord() 
            {
                id = "3",
                addDate = new DateTime(2016, 6, 14),
                addOpr = "0003914",
                addTime = new DateTime(2016, 6, 14, 8, 30, 0),
                chgDate = new DateTime(2016, 6, 14),
                chgOpr = "0003914",
                chgTime = new DateTime(2016, 6, 14, 8, 30, 0),
                earnTypeId = "COMP",
                inDate = new DateTime(2016, 6, 11),
                inTime = new DateTime(2016, 6, 11, 8, 0, 0),
                outDate = new DateTime(2016, 6, 11),
                outTime = new DateTime(2016, 6, 11, 17, 0, 0),
                personLeaveId = "12",
                timecardId = "2",
                workedDate = new DateTime(2016, 6, 11),
                workedMinutes = 480,
            },
            new TimeEntryRecord() 
            {
                id = "4",
                addDate = new DateTime(2016, 6, 14),
                addOpr = "0003914",
                addTime = new DateTime(2016, 6, 14, 8, 30, 0),
                chgDate = new DateTime(2016, 6, 14),
                chgOpr = "0003914",
                chgTime = new DateTime(2016, 6, 14, 8, 30, 0),
                earnTypeId = "COMP",
                inDate = new DateTime(2016, 6, 12),
                inTime = new DateTime(2016, 6, 12, 8, 0, 0),
                outDate = new DateTime(2016, 6, 12),
                outTime = new DateTime(2016, 6, 12, 17, 0, 0),
                personLeaveId = "12",
                timecardId = "2",
                workedDate = new DateTime(2016, 6, 12),
                workedMinutes = 480,
            }
        };

        public TimecardRecord AddTimecardRecordHelper(string id, string personId)
        {
            var record = new TimecardRecord()
                {
                    id = id,
                    employeeId = personId,
                    endDate = new DateTime(2016, 6, 7),
                    startDate = new DateTime(2016, 6, 1),
                    periodStartDate = new DateTime(2016, 6, 1),
                    periodEndDate = new DateTime(2016, 6, 30),
                    payCycleId = "M1",
                    positionId = "ZMUSIC",
                    addDate = new DateTime(2016, 6, 14),
                    addOpr = "0003914",
                    addTime = new DateTime(2016, 6, 14, 8, 30, 0),
                    chgDate = new DateTime(2016, 6, 14),
                    chgOpr = "0003914",
                    chgTime = new DateTime(2016, 6, 14, 8, 30, 0),
                };

            timecardRecords.Add(record);
            return record;
            
        }


        public async Task<Domain.TimeManagement.Entities.Timecard> GetTimecardAsync(string timecardId)
        {

            var record = timecardRecords.FirstOrDefault(r => r.id == timecardId);
            return await Task.FromResult(BuildTimecardEntity(record));
        }

        public async Task<IEnumerable<Timecard>> GetTimecardsAsync(List<string> employeeIds)
        {
            var entities = timecardRecords.Where(tcd => employeeIds.Contains(tcd.employeeId))
                .Select(tcd => BuildTimecardEntity(tcd));

            return await Task.FromResult(entities);
        }

        public async Task<Domain.TimeManagement.Entities.Timecard> UpdateTimecardAsync(Domain.TimeManagement.Entities.TimecardHelper helper)
        {
            var record = timecardRecords.FirstOrDefault(r => r.id == helper.Timecard.Id);
            record.chgDate = DateTime.Now;
            record.chgTime = DateTime.Now;
            record.chgOpr = "0003914";
            
            var idGen = new Random();
            helper.TimeEntriesToCreate.ForEach(te => 
                timeEntryRecords.Add(new TimeEntryRecord()
                {
                    id = idGen.Next().ToString(),
                    addDate = DateTime.Now,
                    addOpr = "0003914",
                    addTime = DateTime.Now,
                    chgDate = DateTime.Now,
                    chgOpr = "0003914",
                    chgTime = DateTime.Now,
                    earnTypeId = te.EarningsTypeId,
                    inDate = te.InDateTime.Value.DateTime,
                    inTime = te.InDateTime.Value.DateTime,
                    outDate = te.OutDateTime.Value.DateTime,
                    outTime = te.OutDateTime.Value.DateTime,
                    personLeaveId = te.PersonLeaveId,
                    projectId = te.ProjectId,
                    timecardId = te.TimecardId,
                    workedDate = te.WorkedDate,
                    workedMinutes = (int?)te.WorkedTime.Value.TotalMinutes
                }));

            helper.TimeEntriesToUpdate.ForEach(te =>
                {
                    var entry = timeEntryRecords.FirstOrDefault(rec => rec.id == te.Id);
                    if (entry != null) 
                    {
                        entry.chgDate = DateTime.Now;
                        entry.chgOpr = "0003914";
                        entry.chgTime = DateTime.Now;
                        entry.earnTypeId = te.EarningsTypeId;
                        entry.inDate = te.InDateTime.Value.DateTime;
                        entry.inTime = te.InDateTime.Value.DateTime;
                        entry.outDate = te.OutDateTime.Value.DateTime;
                        entry.outTime = te.OutDateTime.Value.DateTime;
                        entry.personLeaveId = te.PersonLeaveId;
                        entry.projectId = te.ProjectId;
                        entry.timecardId = te.TimecardId;
                        entry.workedDate = te.WorkedDate;
                        entry.workedMinutes = (int?)te.WorkedTime.Value.TotalMinutes;
                    }
                });

            helper.TimeEntriesToDelete.ForEach(te => 
                {
                    var entry = timeEntryRecords.FirstOrDefault(rec => rec.id == te.Id);
                    if (entry != null)
                    {
                        timeEntryRecords.Remove(entry);
                    }
                });

            return await GetTimecardAsync(helper.Timecard.Id);

        }



        public async Task<Domain.TimeManagement.Entities.Timecard2> GetTimecard2Async(string timecardId)
        {

            var record = timecardRecords.FirstOrDefault(r => r.id == timecardId);
            return await Task.FromResult(BuildTimecard2Entity(record));
        }

        public async Task<IEnumerable<Timecard2>> GetTimecards2Async(List<string> employeeIds)
        {
            var entities = timecardRecords.Where(tcd => employeeIds.Contains(tcd.employeeId))
                .Select(tcd => BuildTimecard2Entity(tcd));

            return await Task.FromResult(entities);
            //return await Task.FromResult(new List<Timecard2>());
        }

        public async Task<Domain.TimeManagement.Entities.Timecard2> UpdateTimecard2Async(Domain.TimeManagement.Entities.Timecard2Helper helper)
        {
            var record = timecardRecords.FirstOrDefault(r => r.id == helper.Timecard.Id);
            record.chgDate = DateTime.Now;
            record.chgTime = DateTime.Now;
            record.chgOpr = "0003914";

            var idGen = new Random();
            helper.TimeEntrysToCreate.ForEach(te =>
                timeEntryRecords.Add(new TimeEntryRecord()
                {
                    id = idGen.Next().ToString(),
                    addDate = DateTime.Now,
                    addOpr = "0003914",
                    addTime = DateTime.Now,
                    chgDate = DateTime.Now,
                    chgOpr = "0003914",
                    chgTime = DateTime.Now,
                    earnTypeId = te.EarningsTypeId,
                    inDate = te.InDateTime.Value,
                    inTime = te.InDateTime.Value,
                    outDate = te.OutDateTime.Value,
                    outTime = te.OutDateTime.Value,
                    personLeaveId = te.PersonLeaveId,
                    projectId = te.ProjectId,
                    timecardId = te.TimecardId,
                    workedDate = te.WorkedDate,
                    workedMinutes = (int?)te.WorkedTime.Value.TotalMinutes
                }));

            helper.TimeEntrysToUpdate.ForEach(te =>
            {
                var entry = timeEntryRecords.FirstOrDefault(rec => rec.id == te.Id);
                if (entry != null)
                {
                    entry.chgDate = DateTime.Now;
                    entry.chgOpr = "0003914";
                    entry.chgTime = DateTime.Now;
                    entry.earnTypeId = te.EarningsTypeId;
                    entry.inDate = te.InDateTime.Value;
                    entry.inTime = te.InDateTime.Value;
                    entry.outDate = te.OutDateTime.Value;
                    entry.outTime = te.OutDateTime.Value;
                    entry.personLeaveId = te.PersonLeaveId;
                    entry.projectId = te.ProjectId;
                    entry.timecardId = te.TimecardId;
                    entry.workedDate = te.WorkedDate;
                    entry.workedMinutes = (int?)te.WorkedTime.Value.TotalMinutes;
                }
            });

            helper.TimeEntrysToDelete.ForEach(te =>
            {
                var entry = timeEntryRecords.FirstOrDefault(rec => rec.id == te.Id);
                if (entry != null)
                {
                    timeEntryRecords.Remove(entry);
                }
            });

            return await GetTimecard2Async(helper.Timecard.Id);

        }


        //public Domain.TimeManagement.Entities.TimecardHelper CreateTimecardHelper(Domain.TimeManagement.Entities.Timecard timecardToUpdate, Domain.TimeManagement.Entities.Timecard existingTimecard)
        //{
        //    Domain.TimeManagement.Entities.TimecardHelper helper = new Domain.TimeManagement.Entities.TimecardHelper(timecardToUpdate);

        //    foreach (var timeEntryToUpdate in timecardToUpdate.TimeEntries)
        //    {
        //        // no Id means this is a new time entry...
        //        if (string.IsNullOrEmpty(timeEntryToUpdate.Id))
        //        {
        //            helper.TimeEntriesToCreate.Add(timeEntryToUpdate);
        //        }
        //        else
        //        {
        //            // find the matching exisitng time entry...
        //            var existingTimeEntry = existingTimecard.TimeEntries.FirstOrDefault(existing => existing.Equals(timeEntryToUpdate));
        //            if (existingTimeEntry != null)
        //            {
        //                // check for a difference and if found add to the list of time entries to update...
        //                if (!timeEntryToUpdate.CompareTimeEntry(existingTimeEntry))
        //                {
        //                    helper.TimeEntriesToUpdate.Add(timeEntryToUpdate);
        //                }
        //            }
        //        }
        //    }

        //    // any time entry found in the db that are not in this timecards time entries should be deleted...
        //    helper.TimeEntriesToDelete = existingTimecard.TimeEntries.Where(e => !timecardToUpdate.TimeEntries.Contains(e)).ToList();
        //    return helper;
        //}

        public async Task<Timecard> CreateTimecardAsync(Timecard timecard)
        {
            var recordId = CreateTimecardRecordHelper(timecard.EmployeeId, timecard.PayCycleId, timecard.PeriodStartDate, timecard.PeriodEndDate, timecard.StartDate, timecard.EndDate, timecard.PositionId, null);

            return await GetTimecardAsync(timecard.Id);
        }

        public async Task<Timecard2> CreateTimecard2Async(Timecard2 timecard)
        {
            var recordId = CreateTimecardRecordHelper(timecard.EmployeeId, timecard.PayCycleId, timecard.PeriodStartDate, timecard.PeriodEndDate, timecard.StartDate, timecard.EndDate, timecard.PositionId, null);

            return await GetTimecard2Async(timecard.Id);
        }

        public string CreateTimecardRecordHelper(
            string employeeId, 
            string payCycleId, 
            DateTime? periodStartDate, 
            DateTime? periodEndDate, 
            DateTime? startDate, 
            DateTime? endDate, 
            string positionId, 
            string comments)
        {            
            var idGen = new Random();
            var record = new TimecardRecord()
            {
                id = idGen.Next().ToString(),
                addDate = DateTime.Now,
                addOpr = "0003914",
                addTime = DateTime.Now,
                chgDate = DateTime.Now,
                chgOpr = "0003914",
                chgTime = DateTime.Now,
                comments = comments,
                employeeId = employeeId,
                endDate = endDate,
                payCycleId = payCycleId,
                periodEndDate = periodEndDate,
                periodStartDate = periodStartDate,
                positionId = positionId,
                startDate = startDate,

            };

            timecardRecords.Add(record);

            return record.id;
        }

        private Timecard BuildTimecardEntity(TimecardRecord tcd)
        {
            return new Timecard(tcd.employeeId, tcd.payCycleId, tcd.positionId, tcd.periodStartDate.Value, tcd.periodEndDate.Value, tcd.startDate.Value, tcd.endDate.Value, tcd.id)
                    {
                
                        Timestamp = new Timestamp(tcd.addOpr,
                            tcd.addDate.Value.AddHours(tcd.addTime.Value.Hour).AddMinutes(tcd.addTime.Value.Minute).AddSeconds(tcd.addTime.Value.Second),
                            tcd.chgOpr,
                            tcd.chgDate.Value.AddHours(tcd.chgTime.Value.Hour).AddMinutes(tcd.chgTime.Value.Minute).AddSeconds(tcd.chgTime.Value.Second)),

                        TimeEntries = timeEntryRecords
                            .Where(te => te.timecardId == tcd.id)
                            .Select(te => BuildTimeEntryEntity(te)).ToList()
                    };
        }

        private Timecard2 BuildTimecard2Entity(TimecardRecord tcd)
        {
            return new Timecard2(tcd.employeeId, tcd.payCycleId, tcd.positionId, tcd.periodStartDate.Value, tcd.periodEndDate.Value, tcd.startDate.Value, tcd.endDate.Value, tcd.id)
            {

                Timestamp = new Timestamp(tcd.addOpr,
                    tcd.addDate.Value.AddHours(tcd.addTime.Value.Hour).AddMinutes(tcd.addTime.Value.Minute).AddSeconds(tcd.addTime.Value.Second),
                    tcd.chgOpr,
                    tcd.chgDate.Value.AddHours(tcd.chgTime.Value.Hour).AddMinutes(tcd.chgTime.Value.Minute).AddSeconds(tcd.chgTime.Value.Second)),

                TimeEntries = timeEntryRecords
                    .Where(te => te.timecardId == tcd.id)
                    .Select(te => BuildTimeEntry2Entity(te)).ToList()
            };
        }

        private TimeEntry BuildTimeEntryEntity(TimeEntryRecord te)
        {
            var addDateTime = te.addDate.Value.AddHours(te.addTime.Value.Hour).AddMinutes(te.addTime.Value.Minute).AddSeconds(te.addTime.Value.Second);            
            var changeDateTime = te.chgDate.Value.AddHours(te.chgTime.Value.Hour).AddMinutes(te.chgTime.Value.Minute).AddSeconds(te.chgTime.Value.Second);

            return new TimeEntry(te.timecardId, te.earnTypeId, TimeSpan.FromMinutes(te.workedMinutes.Value), te.workedDate.Value, te.id)
            {
                ProjectId = te.projectId,
                InDateTime = (!te.inDate.HasValue) ? new Nullable<DateTimeOffset>() : te.inDate.Value.AddHours(te.inTime.Value.Hour).AddMinutes(te.inTime.Value.Minute).AddSeconds(te.inTime.Value.Second),
                OutDateTime = (!te.outDate.HasValue) ? new Nullable<DateTimeOffset>() : te.outDate.Value.AddHours(te.outTime.Value.Hour).AddMinutes(te.outTime.Value.Minute).AddSeconds(te.outTime.Value.Second),
                Timestamp = new Timestamp(te.addOpr,
                    addDateTime,
                    te.chgOpr,
                    changeDateTime)
     
            };
        }

        private TimeEntry2 BuildTimeEntry2Entity(TimeEntryRecord te)
        {
            var addDateTime = te.addDate.Value.AddHours(te.addTime.Value.Hour).AddMinutes(te.addTime.Value.Minute).AddSeconds(te.addTime.Value.Second);
            var changeDateTime = te.chgDate.Value.AddHours(te.chgTime.Value.Hour).AddMinutes(te.chgTime.Value.Minute).AddSeconds(te.chgTime.Value.Second);

            var inDateTime = TimeMangementUtility.SetTimeOfDate(te.inDate, te.inTime);
            var outDateTime = TimeMangementUtility.SetTimeOfDate(te.outDate, te.outTime);

            return new TimeEntry2(te.timecardId, te.earnTypeId, TimeSpan.FromMinutes(te.workedMinutes.Value), te.workedDate.Value, te.id)
            {
                ProjectId = te.projectId,
                InDateTime = inDateTime,
                OutDateTime = outDateTime,
                Timestamp = new Timestamp(te.addOpr,
                    addDateTime,
                    te.chgOpr,
                    changeDateTime)

            };
        }
    }
}
