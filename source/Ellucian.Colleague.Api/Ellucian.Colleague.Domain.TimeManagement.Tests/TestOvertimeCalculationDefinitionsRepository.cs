﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Tests
{
    public class TestOvertimeCalculationDefinitionsRepository
    {
        public class OtCalcDefinition
        {
            public string Recordkey;
            public string OtDescription;
            public string OtCalculationMethod;
        }
        public class OtcdWeeklyParms
        {
            public string Recordkey;
            public int OtcdWeeklyHours;
            public string OtcdWeeklyEarntype;
            public string OtcdWeeklyAltEarntype;
            public string OtcdWeeklyOtCalcDefId;
        }
        public class OtcdDailyParms
        {
            public string Recordkey;
            public int OtcdDailyHours;
            public string OtcdDailyEarntype;
            public string OtcdDailyAltEarntype;
            public string OtcdDailyOtCalcDefId;
        }
        public class OtcdInclEarntypes
        {
            public string Recordkey;
            public string OtcdInclEarntype;
            public string OtcdUseAltOtEarntype;
            public string OtcdOvertimeCalcDefId;
        }
        public class Earntype
        {
            public string Recordkey;
            public decimal? EtpOtFactor;
        }


        //Data Contracts
        public Collection<OtCalcDefinition> OtCalcDefinitionRecords = new Collection<OtCalcDefinition>(){
               new OtCalcDefinition(){
                    Recordkey = "OTCD1",
                    OtDescription = "First OTCD",
                    OtCalculationMethod = "WTDAVG"
               },
               new OtCalcDefinition(){
                    Recordkey = "OTCD2",
                    OtDescription = "Second OTCD",
                    OtCalculationMethod = "WTDAVG"
               }
          };

        public Collection<OtcdWeeklyParms> OtcdWeeklyParmsRecords = new Collection<OtcdWeeklyParms>(){
               new OtcdWeeklyParms(){
                    Recordkey = "1",
                    OtcdWeeklyHours = 35,
                    OtcdWeeklyEarntype = "OT1",
                    OtcdWeeklyAltEarntype = "OT2",
                    OtcdWeeklyOtCalcDefId = "OTCD1"
               },
               new OtcdWeeklyParms(){
                    Recordkey = "2",
                    OtcdWeeklyHours = 40,
                    OtcdWeeklyEarntype = "OT3",
                    OtcdWeeklyAltEarntype = "OT4",
                    OtcdWeeklyOtCalcDefId = "OTCD1"
               },
               new OtcdWeeklyParms(){
                    Recordkey = "3",
                    OtcdWeeklyHours = 36,
                    OtcdWeeklyEarntype = "OT1",
                    OtcdWeeklyAltEarntype = "OT2",
                    OtcdWeeklyOtCalcDefId = "OTCD2"
               },
               new OtcdWeeklyParms(){
                    Recordkey = "4",
                    OtcdWeeklyHours = 41,
                    OtcdWeeklyEarntype = "OT3",
                    OtcdWeeklyAltEarntype = "OT4",
                    OtcdWeeklyOtCalcDefId = "OTCD2"
               }
          };

        public Collection<OtcdDailyParms> OtcdDailyParmsRecords = new Collection<OtcdDailyParms>(){
               new OtcdDailyParms(){
                    Recordkey = "1",
                    OtcdDailyHours = 8,
                    OtcdDailyEarntype = "OT1",
                    OtcdDailyAltEarntype = "OT2",
                    OtcdDailyOtCalcDefId = "OTCD1"
               },
               new OtcdDailyParms(){
                    Recordkey = "2",
                    OtcdDailyHours = 10,
                    OtcdDailyEarntype = "OT3",
                    OtcdDailyAltEarntype = "OT4",
                    OtcdDailyOtCalcDefId = "OTCD1"
               },
               new OtcdDailyParms(){
                    Recordkey = "3",
                    OtcdDailyHours = 9,
                    OtcdDailyEarntype = "OT1",
                    OtcdDailyAltEarntype = "OT2",
                    OtcdDailyOtCalcDefId = "OTCD2"
               },
               new OtcdDailyParms(){
                    Recordkey = "4",
                    OtcdDailyHours = 11,
                    OtcdDailyEarntype = "OT3",
                    OtcdDailyAltEarntype = "OT4",
                    OtcdDailyOtCalcDefId = "OTCD2"
               }
          };

        public Collection<OtcdInclEarntypes> OtcdInclEarntypesRecords = new Collection<OtcdInclEarntypes>()
          {

               new OtcdInclEarntypes(){
                    Recordkey = "1",
                    OtcdInclEarntype = "OT2",
                    OtcdUseAltOtEarntype = "Y",
                    OtcdOvertimeCalcDefId = "OTCD1"
               },
               new OtcdInclEarntypes(){
                    Recordkey = "2",
                    OtcdInclEarntype = "OT4",
                    OtcdUseAltOtEarntype = "Y",
                    OtcdOvertimeCalcDefId = "OTCD1"
               } ,
               new OtcdInclEarntypes(){
                    Recordkey = "3",
                    OtcdInclEarntype = "OT5",
                    OtcdUseAltOtEarntype = "N",
                    OtcdOvertimeCalcDefId = "OTCD1"
               } ,
               new OtcdInclEarntypes(){
                    Recordkey = "4",
                    OtcdInclEarntype = "OT2",
                    OtcdUseAltOtEarntype = "Y",
                    OtcdOvertimeCalcDefId = "OTCD2"
               },
               new OtcdInclEarntypes(){
                    Recordkey = "5",
                    OtcdInclEarntype = "OT4",
                    OtcdUseAltOtEarntype = "Y",
                    OtcdOvertimeCalcDefId = "OTCD2"
               } ,
               new OtcdInclEarntypes(){
                    Recordkey = "6",
                    OtcdInclEarntype = "OT5",
                    OtcdUseAltOtEarntype = "N",
                    OtcdOvertimeCalcDefId = "OTCD2"
               } 
          };

        public Collection<Earntype> Earntypes = new Collection<Earntype>(){
               new Earntype(){
                    Recordkey = "OT1",
                    EtpOtFactor = 1.5m
               },
               new Earntype(){
                    Recordkey = "OT2",
                    EtpOtFactor = 1.75m
               },
               new Earntype(){
                    Recordkey = "OT3",
                    EtpOtFactor = 2.0m
               },
               new Earntype(){
                    Recordkey = "OT4",
                    EtpOtFactor = 2.25m
               },
               new Earntype(){
                    Recordkey = "OT5",
                    EtpOtFactor = 2.50m
               }
          };


        //Entities
        public async Task<List<OvertimeCalculationDefinition>> GetOvertimeCalculationDefinitionsAsync()
        {
            var returnList = new List<OvertimeCalculationDefinition>();
            var ef1 = new EarningsFactor("OT1", 1.5m);
            var ef2 = new EarningsFactor("OT2", 1.75m);
            var ef3 = new EarningsFactor("OT3", 2.0m);
            var ef4 = new EarningsFactor("OT4", 2.25m);

            var otcd1Id = "OTCD1";
            var otcd1Description = "First OTCD";
            var otcd1Method = OvertimePayCalculationMethod.WeightedAverage;
            var otcd1 = new OvertimeCalculationDefinition(otcd1Id, otcd1Description, otcd1Method);
            otcd1.WeeklyHoursThreshold = new List<HoursThreshold>(){
                    new HoursThreshold(35, ef1, ef2),
                    new HoursThreshold(40, ef3, ef4)
               };
            otcd1.DailyHoursThreshold = new List<HoursThreshold>(){
                    new HoursThreshold(8, ef1, ef2),
                    new HoursThreshold(10, ef3, ef4)
               };
            otcd1.IncludedEarningsTypes = new List<IncludedEarningsType>(){
                    new IncludedEarningsType("OT2", true),
                    new IncludedEarningsType("OT4", true),
                    new IncludedEarningsType("OT5", false)
               };


            var otcd2Id = "OTCD2";
            var otcd2Description = "Second OTCD";
            var otcd2Method = OvertimePayCalculationMethod.WeightedAverage;
            var otcd2 = new OvertimeCalculationDefinition(otcd2Id, otcd2Description, otcd2Method);

            otcd2.WeeklyHoursThreshold = new List<HoursThreshold>(){
                    new HoursThreshold(36, ef1, ef2),
                    new HoursThreshold(41, ef3, ef4)
               };
            otcd2.DailyHoursThreshold = new List<HoursThreshold>(){
                    new HoursThreshold(9, ef1, ef2),
                    new HoursThreshold(11, ef3, ef4)
               };
            otcd2.IncludedEarningsTypes = new List<IncludedEarningsType>(){
                    new IncludedEarningsType("OT2", true),
                    new IncludedEarningsType("OT4", true),
                    new IncludedEarningsType("OT5", false)
               };

            returnList.Add(otcd1);
            returnList.Add(otcd2);

            return await Task.FromResult(returnList);
        }



    }
}
