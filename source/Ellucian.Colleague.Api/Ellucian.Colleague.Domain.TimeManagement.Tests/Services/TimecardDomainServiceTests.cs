﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Colleague.Domain.TimeManagement.Services;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using slf4net;

namespace Ellucian.Colleague.Domain.TimeManagement.Tests.Services
{
    [TestClass]
    public class TimecardDomainServiceTests
    {
        public TestTimecardsRepository testTimecardsRepository;
        public TimecardDomainService domainServiceUnderTest;

        public Mock<ILogger> loggerMock;
        public Timecard timeCardExisting;
        public Timecard timeCardUpdate;
        public TimecardHelper helper;

        public void TimecardDomainServiceTestsInitialize()
        {
            testTimecardsRepository = new TestTimecardsRepository();
            loggerMock = new Mock<ILogger>();
            domainServiceUnderTest = new TimecardDomainService(loggerMock.Object);
        }

        [TestClass]
        public class VerifyStuffTests : TimecardDomainServiceTests
        {
            public List<string> availableTimecardIds;

            [TestInitialize]
            public void Initialize()
            {
                TimecardDomainServiceTestsInitialize();
                availableTimecardIds = testTimecardsRepository.timecardRecords.Select(t => t.id).ToList();
            }

            [TestMethod]
            public async Task NoChangesResultsInEmptyListsTest()
            {
                timeCardExisting = await testTimecardsRepository.GetTimecardAsync(availableTimecardIds[0]);
                timeCardUpdate = await testTimecardsRepository.GetTimecardAsync(availableTimecardIds[0]);

                helper = domainServiceUnderTest.CreateTimecardHelper(timeCardUpdate, timeCardExisting);
                Assert.IsFalse(helper.TimeEntriesToCreate.Any());
                Assert.IsFalse(helper.TimeEntriesToDelete.Any());
                Assert.IsFalse(helper.TimeEntriesToUpdate.Any());
            }

            [TestMethod]
            public async Task ChangesResultsInUpdateListsTest()
            {
                timeCardExisting = await testTimecardsRepository.GetTimecardAsync(availableTimecardIds[0]);
                timeCardUpdate = await testTimecardsRepository.GetTimecardAsync(availableTimecardIds[0]);

                timeCardUpdate.TimeEntries[0].EarningsTypeId = "foo";

                helper = domainServiceUnderTest.CreateTimecardHelper(timeCardUpdate, timeCardExisting);
                Assert.IsTrue(helper.TimeEntriesToUpdate[0].Id == timeCardUpdate.TimeEntries[0].Id);
            }

            [TestMethod]
            public async Task CreateResultsInCreateListsTest()
            {
                timeCardExisting = await testTimecardsRepository.GetTimecardAsync(availableTimecardIds[0]);
                timeCardUpdate = await testTimecardsRepository.GetTimecardAsync(availableTimecardIds[0]);

                // create a new time entry...
                var timeEntry = timeCardUpdate.TimeEntries[0];

                //set the new time entry to null to indicate to the helper that this is a new entry...
                
                timeCardUpdate.TimeEntries.Add(new TimeEntry(timeEntry.TimecardId, timeEntry.EarningsTypeId, timeEntry.WorkedTime, timeEntry.WorkedDate));

                helper = domainServiceUnderTest.CreateTimecardHelper(timeCardUpdate, timeCardExisting);
                Assert.IsNotNull(helper.TimeEntriesToUpdate.Select(te => te.Id == null));
            }

            [TestMethod]
            public async Task DeleteResultsInDeleteListsTest()
            {
                timeCardExisting = await testTimecardsRepository.GetTimecardAsync(availableTimecardIds[0]);
                timeCardUpdate = await testTimecardsRepository.GetTimecardAsync(availableTimecardIds[0]);

                timeCardUpdate.TimeEntries.Remove(timeCardUpdate.TimeEntries[0]);

                helper = domainServiceUnderTest.CreateTimecardHelper(timeCardUpdate, timeCardExisting);
                Assert.IsTrue(helper.TimeEntriesToDelete[0].Id == timeCardExisting.TimeEntries[0].Id);
            }
        }
    }
}