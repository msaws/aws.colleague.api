﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.Base.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;


namespace Ellucian.Colleague.Domain.TimeManagement.Tests.Entities
{
    [TestClass]
    public class TimecardStatusTests
    {
        public string id;
        public string timecardId;
        public string actionerId;
        public StatusAction actionType;
        public Timestamp timestamp;

        public void TestInitialize()
        {
            this.id = "001";
            this.timecardId = "002";
            this.actionerId = "24601";
            this.actionType = StatusAction.Submitted;
            this.timestamp = new Timestamp(actionerId, DateTime.Today, actionerId, DateTime.Today);
        }

        [TestClass]
        public class StatusTests : TimecardStatusTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TestInitialize();
            }

            [TestMethod]
            public void PropertiesAreSet()
            {
                var status = new TimecardStatus(timecardId, actionerId, actionType, id) { Timestamp = timestamp };
                Assert.AreEqual(id, status.Id);
                Assert.AreEqual(timecardId, status.TimecardId);
                Assert.AreEqual(actionerId, status.ActionerId);
                Assert.AreEqual(actionType, status.ActionType);
                Assert.AreEqual(timestamp, status.Timestamp);
            }
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullTimecardIdException()
            {
                new TimecardStatus(null, base.actionerId, base.actionType);
            }
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullActioner()
            {
                new TimecardStatus(base.timecardId, null, base.actionType);
            }
            }
        }
}
