﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Tests.Entities
{
    [TestClass]
    public class OvertimeCalculationResultTests
    {
        

        public OvertimeCalculationResult resultUnderTest
        {
            get
            {
                return new OvertimeCalculationResult(personId, payCycleId, startDate, endDate, resultDateTime);
            }
        }

        public string personId;
        public string payCycleId;
        public DateTime startDate;
        public DateTime endDate;
        public DateTimeOffset resultDateTime;
        public List<OvertimeThresholdResult> thresholds;

        [TestClass]
        public class ConstructorTests : OvertimeCalculationResultTests
        {
            
            [TestInitialize]
            public void Initialize()
            {
                personId = "0003914";
                payCycleId = "BW";
                startDate = new DateTime(2016, 6, 4);
                endDate = new DateTime(2016, 6, 10);
                resultDateTime = new DateTimeOffset(new DateTime(2016, 6, 5));
                thresholds = new List<OvertimeThresholdResult>() {
                    new OvertimeThresholdResult("foo", 1.5m, TimeSpan.FromHours(4))
                };
            }

            [TestMethod]
            public void PersonIdTest()
            {
                Assert.AreEqual(personId, resultUnderTest.PersonId);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonIdRequiredTest()
            {
                personId = null;
                var fail = resultUnderTest;
            }

            [TestMethod]
            public void PayCycleIdTest()
            {
                Assert.AreEqual(payCycleId, resultUnderTest.PayCycleId);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PayCycleIdRequiredTest()
            {
                payCycleId = "";
                var fail = resultUnderTest;
            }

            [TestMethod]
            public void StartDateTest()
            {
                Assert.AreEqual(startDate, resultUnderTest.StartDate);
            }

            [TestMethod]
            public void EndDateTest()
            {
                Assert.AreEqual(endDate, resultUnderTest.EndDate);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void StartDateRequiredBeforeEndDateTest()
            {
                startDate = endDate.AddDays(1);
                var fail = resultUnderTest;
            }

            [TestMethod]
            public void ResultDateTimeTest()
            {
                Assert.AreEqual(resultDateTime, resultUnderTest.ResultDateTime);
            }

            [TestMethod]
            public void ThresholdsInitializedTest()
            {
                Assert.IsNotNull(resultUnderTest.Thresholds);
            }
        }

        [TestClass]
        public class EqualsTests : OvertimeCalculationResultTests
        {
            [TestInitialize]
            public void Initialize()
            {
                personId = "0003914";
                startDate = new DateTime(2016, 6, 4);
                payCycleId = "BW";
                endDate = new DateTime(2016, 6, 10);
                resultDateTime = new DateTimeOffset(new DateTime(2016, 6, 5));
                thresholds = new List<OvertimeThresholdResult>() {
                    new OvertimeThresholdResult("foo", 1.5m, TimeSpan.FromHours(4))
                };
            }

            [TestMethod]
            public void AreEqualTest()
            {
                var obj1 = resultUnderTest;
                var obj2 = resultUnderTest;
                Assert.IsTrue(obj1.Equals(obj2));
            }

            [TestMethod]
            public void PersonIdsMustEqualTest()
            {
                var obj1 = resultUnderTest;
                personId = "foobar";
                var obj2 = resultUnderTest;
                Assert.IsFalse(obj1.Equals(obj2));
            }

            [TestMethod]
            public void PayCycleIdsMustEqualTest()
            {
                var obj1 = resultUnderTest;
                payCycleId = "foobar";
                var obj2 = resultUnderTest;
                Assert.IsFalse(obj1.Equals(obj2));
            }

            [TestMethod]
            public void StartDatesMustEqualTest()
            {
                var obj1 = resultUnderTest;
                startDate = startDate.AddDays(-1);
                var obj2 = resultUnderTest;
                Assert.IsFalse(obj1.Equals(obj2));
            }

            [TestMethod]
            public void EndDatesMustEqualTest()
            {
                var obj1 = resultUnderTest;
                endDate = endDate.AddDays(1);
                var obj2 = resultUnderTest;
                Assert.IsFalse(obj1.Equals(obj2));
            }

            [TestMethod]
            public void ThresholdsMustEqualTest()
            {
                var obj1 = resultUnderTest;
                var obj2 = resultUnderTest;
                obj2.Thresholds = thresholds;
                Assert.IsFalse(obj1.Equals(obj2));
            }

            [TestMethod]
            public void NullIsNotEqualTest()
            {
                Assert.IsFalse(resultUnderTest.Equals(null));
            }

            [TestMethod]
            public void OtherObjectIsNotEqualTest()
            {
                Assert.IsFalse(resultUnderTest.Equals(thresholds));
            }
        }


    }
}
