﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Tests.Entities
{
    [TestClass]
    public class TimecardHistory2Tests
    {

        #region fields'nProps
        // timecard History2
        public string Id;
        public string EmployeeId;
        public string PositionId;
        public string PayCycleId;
        public DateTime PeriodStartDate;
        public DateTime PeriodEndDate;
        public DateTime WeekStartDate;
        public DateTime WeekEndDate;
        public List<TimeManagement.Entities.TimeEntryHistory2> TimeEntryHistories2;
        TimeManagement.Entities.TimeEntryHistory2 TimeEntryHistory2;
        public Timestamp Timestamp;
        public StatusAction statusAction;

        // time entry History2
        public string teId;
        public string earnTypeId;
        public string personLeaveId;
        public string projectId;
        public TimeSpan workedTime;
        public DateTime workedDate;
        public DateTime? inDateTime;
        public DateTime? outDateTime;
        #endregion

        #region ini
        public void TimeCardTestInitialize()
        {
            // time entry
            teId = "100";
            earnTypeId = "C";
            personLeaveId = "D";
            workedTime = new TimeSpan(08, 00, 00);
            workedDate = new DateTime(2000, 08, 28);
            inDateTime =  workedDate.AddHours(9);
            outDateTime = workedDate.AddHours(17);
            // timecard
            Id = "001";
            EmployeeId = "24601";
            PositionId = "A";
            PayCycleId = "B";
            PeriodStartDate = new DateTime(2000, 1, 1);
            PeriodEndDate = new DateTime(2000, 2, 2);
            WeekStartDate = new DateTime(2000, 1, 1);
            WeekEndDate = new DateTime(2000, 2, 2);
            TimeEntryHistory2 = new TimeEntryHistory2(teId, Id, earnTypeId, inDateTime, outDateTime, workedTime, workedDate, personLeaveId, projectId, Timestamp);
            TimeEntryHistories2 = new List<TimeManagement.Entities.TimeEntryHistory2>() { TimeEntryHistory2 };
            Timestamp = new Timestamp("MCD", DateTime.Today, "MCD", DateTime.Today);
            statusAction = StatusAction.Approved;
        }
        #endregion

        #region tests

        [TestClass]
        public class TimecardHistory2ConstructorTests : TimecardHistory2Tests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeCardTestInitialize();
            }

            public TimecardHistory2 Get()
            {
                return new TimecardHistory2(Id, EmployeeId, PayCycleId, PositionId, PeriodStartDate, PeriodEndDate, WeekStartDate, WeekEndDate, statusAction, Timestamp);
            }

            [TestMethod]
            public void TimecardHistory2CorrectlyConstructedTest()
            {
                var tc = Get();
                Assert.AreEqual(EmployeeId, tc.EmployeeId);
                Assert.AreEqual(PayCycleId, tc.PayCycleId);
                Assert.AreEqual(PositionId, tc.PositionId);
                Assert.AreEqual(PeriodStartDate, tc.PeriodStartDate);
                Assert.AreEqual(PeriodEndDate, tc.PeriodEndDate);
                Assert.AreEqual(Id, tc.Id);
                Assert.AreEqual(statusAction, tc.StatusAction);
                
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespaceEmployeeIdThrowsArgExceptionTest()
            {
                EmployeeId = null;
                Get();
                //new TimecardHistory2(string.Empty, PayCycleId, PositionId, PeriodStartDate, PeriodEndDate, WeekStartDate, WeekEndDate, st);
            }
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespacePayCycleIdThrowsArgExceptionTest()
            {
                PayCycleId = string.Empty;
                Get();
                //new TimecardHistory2(EmployeeId, string.Empty, PositionId, PeriodStartDate, PeriodEndDate, WeekStartDate, WeekEndDate);
            }
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespacePositionIdThrowsArgExceptionTest()
            {
                PositionId = "";
                Get();
                //new TimecardHistory2(EmployeeId, PayCycleId, string.Empty, PeriodStartDate, PeriodEndDate, WeekStartDate, WeekEndDate);
            }
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullTimestampThrowsExceptionTest()
            {
                Timestamp = null;
                Get();
            }
            [TestMethod]
            public void TimecardHistory2EqualsTest()
            {
                var tc1 = Get();
                var tc2 = Get();

                Assert.IsTrue(tc1.Equals(tc2));
            }
            [TestMethod]
            public void TimecardHistory2NotEqualTest()
            {
                var tc1 = Get();
                Id = "foobar";
                var tc2 = Get();
                Assert.IsFalse(tc1.Equals(tc2));
            }
        }

        #endregion
    }
}
