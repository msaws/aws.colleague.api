﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Tests.Entities
{
    [TestClass]
    public class OvertimeThresholdResultTests
    {

        public string earningsTypeId;
        public decimal factor;
        public TimeSpan totalOvertime;
        public List<DailyOvertimeAllocation> dailyAllocations;

        [TestClass]
        public class ConstructorTests : OvertimeThresholdResultTests
        {
            public OvertimeThresholdResult resultUnderTest
            {
                get
                {
                    return new OvertimeThresholdResult(earningsTypeId, factor, totalOvertime);
                }
            }

            [TestInitialize]
            public void Initialize()
            {
                earningsTypeId = "OVT";
                factor = 1.5m;
                totalOvertime = TimeSpan.FromHours(4);
                dailyAllocations = new List<DailyOvertimeAllocation>()
                {
                    new DailyOvertimeAllocation(new DateTime(2016, 6, 14), TimeSpan.FromHours(2)),
                    new DailyOvertimeAllocation(new DateTime(2016, 6, 15), TimeSpan.FromHours(2))
                };
            }

            [TestMethod]
            public void EarningsTypeTest()
            {
                Assert.AreEqual(earningsTypeId, resultUnderTest.EarningsTypeId);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void EarningsTypeRequiredTest()
            {
                earningsTypeId = null;
                var fail = resultUnderTest;
            }

            [TestMethod]
            public void FactorTest()
            {
                Assert.AreEqual(factor, resultUnderTest.Factor);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void FactorMustBePositiveTest()
            {
                factor = -0.1m;
                var fail = resultUnderTest;
            }

            [TestMethod]
            public void TotalOvertimeTest()
            {
                Assert.AreEqual(totalOvertime, resultUnderTest.TotalOvertime);
            }

            [TestMethod]
            public void DailyAllocationsInitializedTest()
            {
                Assert.IsNotNull(resultUnderTest.DailyAllocations);
            }
        }

        [TestClass]
        public class EqualsTest : OvertimeThresholdResultTests
        {
            public OvertimeThresholdResult resultUnderTest
            {
                get
                {
                    return new OvertimeThresholdResult(earningsTypeId, factor, totalOvertime)
                    {
                        DailyAllocations = dailyAllocations
                    };
                }
            }
            [TestInitialize]
            public void Initialize()
            {
                earningsTypeId = "OVT";
                factor = 1.5m;
                totalOvertime = TimeSpan.FromHours(4);
                dailyAllocations = new List<DailyOvertimeAllocation>()
                {
                    new DailyOvertimeAllocation(new DateTime(2016, 6, 14), TimeSpan.FromHours(2)),
                    new DailyOvertimeAllocation(new DateTime(2016, 6, 15), TimeSpan.FromHours(2))
                };
            }

            [TestMethod]
            public void AreEqualTest()
            {
                Assert.IsTrue(resultUnderTest.Equals(resultUnderTest));
            }

            [TestMethod]
            public void EarningsTypeMustEqualTest()
            {
                var obj1 = resultUnderTest;
                earningsTypeId = "FOO";
                var obj2 = resultUnderTest;
                Assert.IsFalse(obj1.Equals(obj2));
            }

            [TestMethod]
            public void FactorMustEqualTest()
            {
                var obj1 = resultUnderTest;
                factor = 10m;
                var obj2 = resultUnderTest;
                Assert.IsFalse(obj1.Equals(obj2));
            }

            [TestMethod]
            public void TotalOvertimeMustEqualTest()
            {
                var obj1 = resultUnderTest;
                totalOvertime = TimeSpan.FromHours(100);
                var obj2 = resultUnderTest;
                Assert.IsFalse(obj1.Equals(obj2));
            }

            [TestMethod]
            public void DailyAllocationsMustEqualTest()
            {
                var obj1 = resultUnderTest;
                dailyAllocations = new List<DailyOvertimeAllocation>();
                var obj2 = resultUnderTest;
                Assert.IsFalse(obj1.Equals(obj2));
            }
        }
    }
}
