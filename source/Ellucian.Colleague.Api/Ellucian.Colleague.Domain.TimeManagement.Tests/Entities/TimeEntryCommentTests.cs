﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Tests.Entities
{
    [TestClass]
    public class TimeEntryCommentTests
    {
        public string id;
        public string employeeId;
        public string timecardId;
        public string payCycleId;
        public DateTime payPeriodEndDate;
        public string positionId;
        public string comments;
        public Timestamp timestamp;

        public TimeEntryComment comment;

        public void TimeEntryCommentTestsInitialize()
        {
            id = "5";
            employeeId = "0003914";
            timecardId = "1234";
            payCycleId = "BW";
            payPeriodEndDate = new DateTime(2016, 8, 26);
            positionId = "POSITIONID";
            comments = "This is the comment string";
            timestamp = new Timestamp("MCD", DateTime.Today, "MCD", DateTime.Today);
        }

        [TestClass]
        public class TimeEntryCommentConstructor_ExistingRecordTests : TimeEntryCommentTests
        {
            public TimeEntryComment createComment()
            {
                return new TimeEntryComment(id, employeeId, timecardId, positionId, payCycleId, payPeriodEndDate, comments, timestamp);
            }
            [TestInitialize]
            public void Initialize()
            {
                TimeEntryCommentTestsInitialize();

            }

            [TestMethod]
            public void IdTest()
            {
                Assert.AreEqual(id, createComment().Id);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void IdRequiredTest()
            {
                id = null;
                createComment();
            }

            [TestMethod]
            public void EmployeeIdTest()
            {
                Assert.AreEqual(employeeId, createComment().EmployeeId);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void EmployeeIdRequiredTest()
            {
                employeeId = null;
                createComment();
            }


            [TestMethod]
            public void PositionIdTest()
            {
                Assert.AreEqual(positionId, createComment().PositionId);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PositionIdRequiredTest()
            {
                positionId = null;
                createComment();
            }

            [TestMethod]
            public void PayCycleIdTest()
            {
                Assert.AreEqual(payCycleId, createComment().PayCycleId);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PayCycleIdRequiredTest()
            {
                payCycleId = null;
                createComment();
            }

            [TestMethod]
            public void PayPeriodEndDateTest()
            {
                Assert.AreEqual(payPeriodEndDate, createComment().PayPeriodEndDate);
            }

            [TestMethod]
            public void CommentsTest()
            {
                Assert.AreEqual(comments, createComment().Comments);
            }

            [TestMethod]
            public void TimestampTest()
            {
                Assert.AreEqual(timestamp, createComment().CommentsTimestamp);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void TimestampRequiredTest()
            {
                timestamp = null;
                createComment();
            }

        }


    }
}
