﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.Base.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;


namespace Ellucian.Colleague.Domain.TimeManagement.Tests.Entities
{
    [TestClass]
    public class TimeEntry2Tests
    {


        public string id;
        public string Timecard2Id;
        public string projectId;
        public string earnTypeId;
        public string personLeaveId;
        public DateTime? inDateTime;
        public DateTime? outDateTime;
        public TimeSpan? workedTime;
        public DateTime workedDate;
        public Timestamp timestamp;

        public TimeEntry2 TimeEntry2;

        public void TimeEntry2TestInitialize()
        {
            id = "jygr";
            Timecard2Id = "sdfgfgd";
            projectId = "ghgghfd";
            earnTypeId = "hgfd";
            personLeaveId = "gfgfdd";
            inDateTime = new DateTime(2011, 12, 13, 07, 00, 00, 00) as DateTime?; //7am
            outDateTime = new DateTime(2011, 12, 13, 14, 00, 00, 00) as DateTime?; //2pm
            workedDate = new DateTime(2011, 12, 13);
            workedTime = new TimeSpan(12, 5, 0);
            timestamp = new Timestamp("MCD", DateTime.Today, "MCD", DateTime.Today);
        }

        [TestClass]
        public class TimeEntry2Constructor_ExistingTotalTime : TimeEntry2Tests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntry2TestInitialize();
            }

            [TestMethod]
            public void TimeEntry2CorrectlyConstructedWithSpanTest()
            {
                var te = new TimeManagement.Entities.TimeEntry2(Timecard2Id, earnTypeId, workedTime, workedDate, id);
                Assert.AreEqual(id, te.Id);
                Assert.AreEqual(earnTypeId, te.EarningsTypeId);
                Assert.AreEqual(Timecard2Id, te.TimecardId);
                Assert.AreEqual(workedTime, te.WorkedTime);
                Assert.AreEqual(workedDate, te.WorkedDate);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void IdRequiredTest()
            {
                new TimeEntry2(Timecard2Id, earnTypeId, workedTime, workedDate, string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespaceEarnTypeIdThrowsArgExceptionTest()
            {
                new TimeEntry2(Timecard2Id, string.Empty, workedTime, workedDate, id);
            }

            [TestMethod]
            public void CanBeChangedToDetailTimeTest()
            {
                var te = new TimeManagement.Entities.TimeEntry2(Timecard2Id, earnTypeId, workedTime, workedDate, id)
                    {
                        InDateTime = inDateTime,
                        OutDateTime = outDateTime
                    };

                Assert.AreNotEqual(workedTime, te.WorkedTime);
                Assert.AreEqual(outDateTime - inDateTime, te.WorkedTime);
            }
        }

        [TestClass]
        public class TimeEntry2Constructor_ExistingDetailTime : TimeEntry2Tests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntry2TestInitialize();
            }

            [TestMethod]
            public void TimeEntry2CorrectlyConstructedWithTimeTest()
            {
                var te = new TimeManagement.Entities.TimeEntry2(Timecard2Id, earnTypeId, inDateTime, outDateTime, workedDate, id);
                Assert.AreEqual(id, te.Id);
                Assert.AreEqual(earnTypeId, te.EarningsTypeId);
                Assert.AreEqual(Timecard2Id, te.TimecardId);
                Assert.AreEqual(inDateTime, te.InDateTime);
                Assert.AreEqual(outDateTime, outDateTime);
                Assert.AreEqual(workedDate, te.WorkedDate);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void IdRequiredTest()
            {

                new TimeEntry2(Timecard2Id, earnTypeId, inDateTime, outDateTime, workedDate, string.Empty);

            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespaceEarnTypeIdThrowsArgExceptionTest()
            {

                new TimeEntry2(Timecard2Id, string.Empty, inDateTime, outDateTime, workedDate, id);

            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void InDateMustBeSameAsWorkedDateTest()
            {
                var newWorkedDate = workedDate.AddDays(1);
                new TimeEntry2(Timecard2Id, earnTypeId, null, outDateTime, newWorkedDate, id);//this is ok when in date is null
                new TimeEntry2(Timecard2Id, earnTypeId, inDateTime, outDateTime, workedDate.AddDays(1), id);//this is not ok
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void EarlyOutDateTimeThrowsExceptionTest()
            {
                new TimeEntry2(Timecard2Id, earnTypeId, inDateTime, inDateTime.Value.AddDays(-1), workedDate, id);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void LateInDateTimeThrowsExceptionTest()
            {
                new TimeEntry2(Timecard2Id, earnTypeId, outDateTime.Value.AddSeconds(1), outDateTime, workedDate, id);
            }

            [TestMethod]
            public void CanBeChangedToSummaryTest()
            {
                var te = new TimeManagement.Entities.TimeEntry2(Timecard2Id, earnTypeId, inDateTime, outDateTime, workedDate, id) 
                { 
                    InDateTime = null,
                    OutDateTime = null,
                    WorkedTime = workedTime
                };

                Assert.AreNotEqual(outDateTime - inDateTime, te.WorkedTime);
                Assert.AreEqual(workedTime, te.WorkedTime);
            }
        }

        [TestClass]
        public class TimeEntry2Constructor_NewTotalTime : TimeEntry2Tests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntry2TestInitialize();
            }

            [TestMethod]
            public void TimeEntry2CorrectlyConstructedWithSpanTest()
            {
                var te = new TimeManagement.Entities.TimeEntry2(Timecard2Id, earnTypeId, workedTime, workedDate);
                Assert.IsNull(te.Id);
                Assert.AreEqual(earnTypeId, te.EarningsTypeId);
                Assert.AreEqual(Timecard2Id, te.TimecardId);
                Assert.AreEqual(workedTime, te.WorkedTime);
                Assert.AreEqual(workedDate, te.WorkedDate);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespaceEarnTypeIdThrowsArgExceptionTest()
            {
                new TimeEntry2(Timecard2Id, string.Empty, workedTime, workedDate);
            }
        }

        [TestClass]
        public class TimeEntry2Constructor_NewDetailTime : TimeEntry2Tests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntry2TestInitialize();
            }

            [TestMethod]
            public void TimeEntry2CorrectlyConstructedWithTimeTest()
            {
                var te = new TimeManagement.Entities.TimeEntry2(Timecard2Id, earnTypeId, inDateTime, outDateTime, workedDate);
                Assert.IsNull(te.Id);
                Assert.AreEqual(earnTypeId, te.EarningsTypeId);
                Assert.AreEqual(Timecard2Id, te.TimecardId);
                Assert.AreEqual(inDateTime, te.InDateTime);
                Assert.AreEqual(outDateTime, outDateTime);
                Assert.AreEqual(workedDate, te.WorkedDate);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespaceEarnTypeIdThrowsArgExceptionTest()
            {

                new TimeEntry2(Timecard2Id, string.Empty, inDateTime, outDateTime, workedDate);

            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void InDateMustBeSameAsWorkedDateTest()
            {
                var newWorkedDate = workedDate.AddDays(1);
                new TimeEntry2(Timecard2Id, earnTypeId, null, outDateTime, newWorkedDate);//this is ok when in date is null
                new TimeEntry2(Timecard2Id, earnTypeId, inDateTime, outDateTime, workedDate.AddDays(1));//this is not ok
            }

        }

        [TestClass]
        public class TimeEntry2EqualityTests : TimeEntry2Tests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntry2TestInitialize();
            }

            [TestMethod]
            public void ExitingRecordEqualityTest()
            {
                var te1 = new TimeEntry2(Timecard2Id, earnTypeId, workedTime, workedDate, id);
                var te2 = new TimeEntry2("foo", "bar", null, workedDate.AddDays(1), id);

                //this is a contrived scenario, but ids are the same so they're equal.
                Assert.IsTrue(te1.Equals(te2));
                Assert.AreEqual(te1.GetHashCode(), te2.GetHashCode());
            }

            [TestMethod]
            public void ExistingRecordInequalityTest()
            {
                var te1 = new TimeEntry2(Timecard2Id, earnTypeId, workedTime, workedDate, id);
                var te2 = new TimeEntry2(Timecard2Id, earnTypeId, workedTime, workedDate, "foobar");

                //this is a contrived scenario, but ids are the same so they're equal.
                Assert.IsFalse(te1.Equals(te2));
                Assert.AreNotEqual(te1.GetHashCode(), te2.GetHashCode());
            }

            [TestMethod]
            public void NewRecordEqualityTest()
            {
                var te1 = new TimeEntry2(Timecard2Id, earnTypeId, inDateTime, outDateTime, workedDate)
                    {
                        PersonLeaveId = personLeaveId,
                        ProjectId = projectId
                    };
                var te2 = new TimeEntry2(Timecard2Id, earnTypeId, inDateTime, outDateTime, workedDate)
                {
                    PersonLeaveId = personLeaveId,
                    ProjectId = projectId
                };

                //properties are the same so they're equal
                Assert.IsTrue(te1.Equals(te2));
                Assert.AreEqual(te1.GetHashCode(), te2.GetHashCode());
            }

            [TestMethod]
            public void NewRecordInequalityTest()
            {
                var te1 = new TimeEntry2("foo", earnTypeId, inDateTime, outDateTime, workedDate)
                {
                    PersonLeaveId = personLeaveId,
                    ProjectId = projectId
                };
                var te2 = new TimeEntry2("bar", earnTypeId, inDateTime, outDateTime, workedDate)
                {
                    PersonLeaveId = personLeaveId,
                    ProjectId = projectId
                };

                //properties are not the same so they're equal
                Assert.IsFalse(te1.Equals(te2));
                Assert.AreNotEqual(te1.GetHashCode(), te2.GetHashCode());
            }

            [TestMethod]
            public void NullObjectEqualityTest()
            {
                var te1 = new TimeEntry2(Timecard2Id, earnTypeId, workedTime, workedDate, id);
                Assert.IsFalse(te1.Equals(null));
            }

            [TestMethod]
            public void DifferentObjectTypeTest()
            {
                var te1 = new TimeEntry2(Timecard2Id, earnTypeId, workedTime, workedDate, id);
                Assert.IsFalse(te1.Equals(DateTime.Today));
            }

            


            [TestMethod]
            public void CompareTimeEntry2WorksWhenSameTest()
            {
                var te1 = new TimeEntry2(Timecard2Id, earnTypeId, inDateTime, outDateTime, workedDate, id);
                var te2 = new TimeEntry2(Timecard2Id, earnTypeId, inDateTime, outDateTime, workedDate, id);

                Assert.IsTrue(te1.CompareTimeEntry(te2));
            }
            [TestMethod]
            public void CompareTimeEntry2WorksWhenDifferentTest()
            {
                var te1 = new TimeEntry2(Timecard2Id, earnTypeId, inDateTime, outDateTime, workedDate, id);
                Timecard2Id = "454544";
                var te2 = new TimeEntry2(Timecard2Id, earnTypeId, inDateTime, outDateTime, workedDate, id);

                Assert.IsFalse(te1.CompareTimeEntry(te2));
            }
        }

        [TestClass]
        public class EarningsTypeIdTests : TimeEntry2Tests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntry2TestInitialize();

                TimeEntry2 = new TimeEntry2(Timecard2Id, earnTypeId, workedTime, workedDate);
            }

            [TestMethod]
            public void SetAndGetTest()
            {
                TimeEntry2.EarningsTypeId = "foobar";
                Assert.AreEqual("foobar", TimeEntry2.EarningsTypeId);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void CannotSetToNullTest()
            {
                TimeEntry2.EarningsTypeId = null;
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void CannotSetToEmptyTest()
            {
                TimeEntry2.EarningsTypeId = "";
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void CannotSetToWhitespaceTest()
            {
                TimeEntry2.EarningsTypeId = " ";
            }
        }

        [TestClass]
        public class TimeInTimeOutTests : TimeEntry2Tests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntry2TestInitialize();

                TimeEntry2 = new TimeEntry2(Timecard2Id, earnTypeId, inDateTime, outDateTime, workedDate);
            }

            [TestMethod]
            public void SetInTimeTest()
            {
                var value = TimeEntry2.InDateTime.Value.AddSeconds(-1);
                TimeEntry2.InDateTime = value;
                Assert.IsTrue(value.Equals(TimeEntry2.InDateTime.Value));
            }

            [TestMethod]
            public void SetOutTimeTest()
            {
                var value = TimeEntry2.OutDateTime.Value.AddSeconds(1);
                TimeEntry2.OutDateTime = value;
                Assert.IsTrue(value.Equals(TimeEntry2.OutDateTime.Value));
            }


            [TestMethod]
            public void SetInTimeAndOutTimeNullTest()
            {
                TimeEntry2.InDateTime = null;
                TimeEntry2.OutDateTime = null;
                Assert.IsNull(TimeEntry2.InDateTime);

                TimeEntry2.InDateTime = inDateTime;
                Assert.IsNull(TimeEntry2.OutDateTime);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void InTimeMustBeBeforeOrSameAsOutTimeTest()
            {
                TimeEntry2.InDateTime = null;
                TimeEntry2.OutDateTime = inDateTime;
                TimeEntry2.InDateTime = inDateTime.Value.AddSeconds(1);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void OutTimeMustBeAfterOrSameAsInTimeTest()
            {
                TimeEntry2.OutDateTime = null;
                TimeEntry2.InDateTime = outDateTime;
                TimeEntry2.OutDateTime = outDateTime.Value.AddSeconds(-1);
            }
        }

        [TestClass]
        public class WorkedTimeTests : TimeEntry2Tests
        {
            public TimeEntry2 summaryEntry;
            public TimeEntry2 detailEntry;

            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntry2TestInitialize();

                summaryEntry = new TimeEntry2(Timecard2Id, earnTypeId, workedTime, workedDate);
                detailEntry = new TimeEntry2(Timecard2Id, earnTypeId, inDateTime, outDateTime, workedDate);
            }

            [TestMethod]
            public void GetSetWorkedTimeOnSummaryEntryTest()
            {
                var value = new TimeSpan(14, 1, 2);

                summaryEntry.WorkedTime = value;
                Assert.IsTrue(value.Equals(summaryEntry.WorkedTime));
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidOperationException))]
            public void SetWorkedTimeOnDetailEntryTest()
            {
                detailEntry.WorkedTime = workedTime;
            }


            [TestMethod]
            [ExpectedException(typeof(InvalidOperationException))]
            public void SetWorkedTimeOnDetailWithInTimeTest()
            {
                detailEntry.OutDateTime = null;
                detailEntry.WorkedTime = workedTime;
            }


            [TestMethod]
            [ExpectedException(typeof(InvalidOperationException))]
            public void SetWorkedTimeOnDetailWithOutTimeTest()
            {
                detailEntry.InDateTime = null;
                detailEntry.WorkedTime = workedTime;
            }

            [TestMethod]
            public void CalculateWorkedTimeOnDetailTest()
            {
                var time = outDateTime - inDateTime;
                Assert.AreEqual(time, detailEntry.WorkedTime.Value);
            }

            [TestMethod]
            public void NullWorkedTimeOnDetailNullOutTimeTest()
            {
                detailEntry.OutDateTime = null;
                Assert.IsNull(detailEntry.WorkedTime);
            }

            [TestMethod]
            public void NullWorkedTimeOnDetailNullInTimeTest()
            {
                detailEntry.InDateTime = null;
                Assert.IsNull(detailEntry.WorkedTime);
            }
        }

    }
}
