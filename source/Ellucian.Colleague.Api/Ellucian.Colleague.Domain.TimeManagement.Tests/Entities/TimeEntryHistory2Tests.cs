﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.Base.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;


namespace Ellucian.Colleague.Domain.TimeManagement.Tests.Entities
{
    [TestClass]
    public class TimeEntry2History2Tests
    {

        #region fields'nProps
        public string Id;
        public string TimecardHistory2Id;
        public string ProjectId;
        public string EarnTypeId;
        public string PersonLeaveId;
        public DateTime? InDateTime;
        public DateTime? OutDateTime;
        public TimeSpan WorkedTime;
        public DateTime WorkedDate;
        public Timestamp Timestamp;
        #endregion

        #region ini
        public void TimeEntry2TestInitialize()
        {
            Id = "jygr";
            TimecardHistory2Id = "sdfgfgd";
            ProjectId = "ghgghfd";
            EarnTypeId = "hgfd";
            PersonLeaveId = "gfgfdd";
            InDateTime = new DateTime(2011, 12, 13, 07, 00, 00, 00) as DateTime?;
            OutDateTime = new DateTime(2011, 12, 13, 14, 00, 00, 00) as DateTime?;
            WorkedDate = new DateTime(2011, 12, 15);
            WorkedTime = new TimeSpan(07, 00, 00);
            Timestamp = new Timestamp("MCD", DateTime.Today, "MCD", DateTime.Today);
        }
        #endregion

        #region tests

        [TestClass]
        public class TimeEntry2History2ConstructorTests : TimeEntry2History2Tests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntry2TestInitialize();
            }

            [TestMethod]
            public void TimeEntry2History2CorrectlyConstructedWithTimeTest()
            {
                var te = new TimeEntryHistory2(Id, TimecardHistory2Id, EarnTypeId, InDateTime, OutDateTime, WorkedTime, WorkedDate, PersonLeaveId, ProjectId, Timestamp);
                Assert.AreEqual(Id, te.Id);
                Assert.AreEqual(EarnTypeId, te.EarningsTypeId);
                Assert.AreEqual(TimecardHistory2Id, te.TimecardHistoryId);
                Assert.AreEqual(PersonLeaveId, te.PersonLeaveId);
                Assert.AreEqual(InDateTime, te.InDateTime);
                Assert.AreEqual(OutDateTime, OutDateTime);
                Assert.AreEqual(WorkedDate, te.WorkedDate);
            }

            [TestMethod]
            public void TimeEntry2History2CorrectlyConstructedWithSpanTest()
            {
                var te = new TimeEntryHistory2(Id, TimecardHistory2Id, EarnTypeId, InDateTime, OutDateTime, WorkedTime, WorkedDate, PersonLeaveId, ProjectId, Timestamp);
                Assert.AreEqual(Id, te.Id);
                Assert.AreEqual(EarnTypeId, te.EarningsTypeId);
                Assert.AreEqual(TimecardHistory2Id, te.TimecardHistoryId);
                Assert.AreEqual(PersonLeaveId, te.PersonLeaveId);
                Assert.AreEqual(WorkedTime, te.WorkedTime);
                Assert.AreEqual(WorkedDate, te.WorkedDate);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespaceIdThrowsArgExceptionTest()
            {
                try
                {
                    Id = null;
                    new TimeEntryHistory2(Id, TimecardHistory2Id, EarnTypeId, InDateTime, OutDateTime, WorkedTime, WorkedDate, PersonLeaveId, ProjectId, Timestamp);
                }
                catch (ArgumentNullException)
                {
                    try
                    {
                        new TimeEntryHistory2(Id, TimecardHistory2Id, EarnTypeId, InDateTime, OutDateTime, WorkedTime, WorkedDate, PersonLeaveId, ProjectId, Timestamp);
                    }
                    catch (ArgumentNullException e)
                    {
                        throw e;
                    }
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespaceTimecardHistory2IdThrowsArgExceptionTest()
            {
                try
                {
                    Id = null;
                    new TimeEntryHistory2(Id, TimecardHistory2Id, EarnTypeId, InDateTime, OutDateTime, WorkedTime, WorkedDate, PersonLeaveId, ProjectId, Timestamp);
                }
                catch (ArgumentNullException)
                {
                    try
                    {
                        new TimeEntryHistory2(Id, TimecardHistory2Id, EarnTypeId, InDateTime, OutDateTime, WorkedTime, WorkedDate, PersonLeaveId, ProjectId, Timestamp);
                    }
                    catch (ArgumentNullException e)
                    {
                        throw e;
                    }
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespaceEarnTypeIdThrowsArgExceptionTest()
            {
                try
                {
                    EarnTypeId = null;
                    new TimeEntryHistory2(Id, TimecardHistory2Id, EarnTypeId, InDateTime, OutDateTime, WorkedTime, WorkedDate, PersonLeaveId, ProjectId, Timestamp);
                }
                catch (ArgumentNullException)
                {
                    try
                    {
                        new TimeEntryHistory2(Id, TimecardHistory2Id, EarnTypeId, InDateTime, OutDateTime, WorkedTime, WorkedDate, PersonLeaveId, ProjectId, Timestamp);
                    }
                    catch (ArgumentNullException e)
                    {
                        throw e;
                    }
                }
            }

            [TestMethod]
            public void CompareTimeEntry2History2WorksWhenSameTest()
            {
                var te1 = new TimeEntryHistory2(Id, TimecardHistory2Id, EarnTypeId, InDateTime, OutDateTime, WorkedTime, WorkedDate, PersonLeaveId, ProjectId, Timestamp);
                var te2 = new TimeEntryHistory2(Id, TimecardHistory2Id, EarnTypeId, InDateTime, OutDateTime, WorkedTime, WorkedDate, PersonLeaveId, ProjectId, Timestamp);

                Assert.IsTrue(te1.CompareTimeEntry(te2));
            }
            [TestMethod]
            public void CompareTimeEntry2History2WorksWhenDifferentTest()
            {
                var te1 = new TimeEntryHistory2(Id, TimecardHistory2Id, EarnTypeId, InDateTime, OutDateTime, WorkedTime, WorkedDate, PersonLeaveId, ProjectId, Timestamp);
                TimecardHistory2Id = "454544";
                var te2 = new TimeEntryHistory2(Id, TimecardHistory2Id, EarnTypeId, InDateTime, OutDateTime, WorkedTime, WorkedDate, PersonLeaveId, ProjectId, Timestamp);

                Assert.IsFalse(te1.CompareTimeEntry(te2));
            }
        }

        #endregion
    }
}
