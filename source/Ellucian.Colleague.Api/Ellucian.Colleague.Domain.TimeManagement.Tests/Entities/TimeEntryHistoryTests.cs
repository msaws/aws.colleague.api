﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.Base.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;


namespace Ellucian.Colleague.Domain.TimeManagement.Tests.Entities
{
    [TestClass]
    public class TimeEntryHistoryTests
    {

        #region fields'nProps
        public string Id;
        public string TimecardHistoryId;
        public string ProjectId;
        public string EarnTypeId;
        public string PersonLeaveId;
        public DateTimeOffset? InDateTime;
        public DateTimeOffset? OutDateTime;
        public TimeSpan WorkedTime;
        public DateTime WorkedDate;
        public Timestamp Timestamp;
        #endregion

        #region ini
        public void TimeEntryTestInitialize()
        {
            Id = "jygr";
            TimecardHistoryId = "sdfgfgd";
            ProjectId = "ghgghfd";
            EarnTypeId = "hgfd";
            PersonLeaveId = "gfgfdd";
            InDateTime = new DateTimeOffset(2011, 12, 13, 07, 00, 00, 00, new TimeSpan(0, 0, 0)) as DateTimeOffset?;
            OutDateTime = new DateTimeOffset(2011, 12, 13, 14, 00, 00, 00, new TimeSpan(0, 0, 0)) as DateTimeOffset?;
            WorkedDate = new DateTime(2011, 12, 15);
            WorkedTime = new TimeSpan(07, 00, 00);
            Timestamp = new Timestamp("MCD", DateTime.Today, "MCD", DateTime.Today);
        }
        #endregion

        #region tests

        [TestClass]
        public class TimeEntryHistoryConstructorTests : TimeEntryHistoryTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntryTestInitialize();
            }

            [TestMethod]
            public void TimeEntryHistoryCorrectlyConstructedWithTimeTest()
            {
                var te = new TimeEntryHistory(Id, TimecardHistoryId, EarnTypeId, InDateTime, OutDateTime, WorkedDate, PersonLeaveId);
                Assert.AreEqual(Id, te.Id);
                Assert.AreEqual(EarnTypeId, te.EarningsTypeId);
                Assert.AreEqual(TimecardHistoryId, te.TimecardHistoryId);
                Assert.AreEqual(PersonLeaveId, te.PersonLeaveId);
                Assert.AreEqual(InDateTime, te.InDateTime);
                Assert.AreEqual(OutDateTime, OutDateTime);
                Assert.AreEqual(WorkedDate, te.WorkedDate);
            }

            [TestMethod]
            public void TimeEntryHistoryCorrectlyConstructedWithSpanTest()
            {
                var te = new TimeEntryHistory(Id, TimecardHistoryId, EarnTypeId, WorkedTime, WorkedDate, PersonLeaveId, ProjectId, Timestamp);
                Assert.AreEqual(Id, te.Id);
                Assert.AreEqual(EarnTypeId, te.EarningsTypeId);
                Assert.AreEqual(TimecardHistoryId, te.TimecardHistoryId);
                Assert.AreEqual(PersonLeaveId, te.PersonLeaveId);
                Assert.AreEqual(WorkedTime, te.WorkedTime);
                Assert.AreEqual(WorkedDate, te.WorkedDate);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespaceIdThrowsArgExceptionTest()
            {
                try
                {
                    new TimeEntryHistory(string.Empty, TimecardHistoryId, EarnTypeId, InDateTime, OutDateTime, WorkedDate, PersonLeaveId);
                }
                catch (ArgumentNullException)
                {
                    try
                    {
                        new TimeEntryHistory(string.Empty, TimecardHistoryId, EarnTypeId, WorkedTime, WorkedDate, PersonLeaveId, ProjectId, Timestamp);
                    }
                    catch (ArgumentNullException e)
                    {
                        throw e;
                    }
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespaceTimecardHistoryIdThrowsArgExceptionTest()
            {
                try
                {
                    new TimeEntryHistory(Id, string.Empty, EarnTypeId, InDateTime, OutDateTime, WorkedDate, PersonLeaveId);
                }
                catch (ArgumentNullException)
                {
                    try
                    {
                        new TimeEntryHistory(Id, string.Empty, EarnTypeId, WorkedTime, WorkedDate, PersonLeaveId, ProjectId, Timestamp);
                    }
                    catch (ArgumentNullException e)
                    {
                        throw e;
                    }
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespaceEarnTypeIdThrowsArgExceptionTest()
            {
                try
                {
                    new TimeEntryHistory(Id, TimecardHistoryId, string.Empty, InDateTime, OutDateTime, WorkedDate, PersonLeaveId);
                }
                catch (ArgumentNullException)
                {
                    try
                    {
                        new TimeEntryHistory(Id, TimecardHistoryId, string.Empty, WorkedTime, WorkedDate, PersonLeaveId, ProjectId, Timestamp);
                    }
                    catch (ArgumentNullException e)
                    {
                        throw e;
                    }
                }
            }

            [TestMethod]
            public void CompareTimeEntryHistoryWorksWhenSameTest()
            {
                var te1 = new TimeEntryHistory(Id, TimecardHistoryId, EarnTypeId, InDateTime, OutDateTime, WorkedDate, PersonLeaveId);
                var te2 = new TimeEntryHistory(Id, TimecardHistoryId, EarnTypeId, InDateTime, OutDateTime, WorkedDate, PersonLeaveId);

                Assert.IsTrue(te1.CompareTimeEntry(te2));
            }
            [TestMethod]
            public void CompareTimeEntryHistoryWorksWhenDifferentTest()
            {
                var te1 = new TimeEntryHistory(Id, TimecardHistoryId, EarnTypeId, InDateTime, OutDateTime, WorkedDate, PersonLeaveId);
                TimecardHistoryId = "454544";
                var te2 = new TimeEntryHistory(Id, TimecardHistoryId, EarnTypeId, InDateTime, OutDateTime, WorkedDate, PersonLeaveId);

                Assert.IsFalse(te1.CompareTimeEntry(te2));
            }
        }

        #endregion
    }
}
