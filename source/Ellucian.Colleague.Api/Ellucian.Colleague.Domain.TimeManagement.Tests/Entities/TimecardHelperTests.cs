﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.Base.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace Ellucian.Colleague.Domain.TimeManagement.Tests.Entities
{
    [TestClass]
    public class TimecardHelperTests
    {
        // timecard
        public string Id;
        public string EmployeeId;
        public string PositionId;
        public string PayCycleId;
        public string Comments;
        public DateTime PeriodStartDate;
        public DateTime PeriodEndDate;
        public DateTime WeekStartDate;
        public DateTime WeekEndDate;
        public List<TimeManagement.Entities.TimeEntry> TimeEntries;
        TimeManagement.Entities.TimeEntry TimeEntry;
        public Timestamp Timestamp;

        // timeentry
        public string teId;
        public string earnTypeId;
        public string personLeaveId;
        public TimeSpan? workedTime;
        public DateTime workedDate;

        public void TimeCardHelperTestInitialize()
        {
            // timecard
            Id = "001";
            EmployeeId = "24601";
            PositionId = "A";
            PayCycleId = "B";
            Comments = "The fear is excruciating, but therein lies the answer";
            PeriodStartDate = new DateTime(2000, 1, 1);
            PeriodEndDate = new DateTime(2000, 2, 2);
            WeekStartDate = new DateTime(2000, 1, 1);
            WeekEndDate = new DateTime(2000, 2, 2);

            // time entries
            TimeEntries = new List<TimeManagement.Entities.TimeEntry>() { };

            teId = "100";
            earnTypeId = "C";
            personLeaveId = "D";
            workedTime = new TimeSpan(08, 00, 00);
            workedDate = new DateTime(2000, 08, 28);

            TimeEntry = new TimeManagement.Entities.TimeEntry(Id, earnTypeId, workedTime, workedDate, teId);
            TimeEntries.Add(TimeEntry);

            Timestamp = new Timestamp("MCD", DateTime.Today, "MCD", DateTime.Today);
        }

        [TestClass]
        public class TimecardHelperConstructorTests : TimecardHelperTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeCardHelperTestInitialize();
            }

            [TestMethod]
            public void TimecardHelperCorrectlyConstructedTest()
            {
                var tc = new Timecard(EmployeeId, PayCycleId, PositionId, PeriodStartDate, PeriodEndDate, WeekStartDate, WeekEndDate, Id);
                var tch = new TimecardHelper(tc);

                Assert.AreEqual(tc, tch.Timecard);
            }

        }
    }
}
