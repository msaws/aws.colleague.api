﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.Base.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;


namespace Ellucian.Colleague.Domain.TimeManagement.Tests
{
    [TestClass]
    public class TimecardTests
    {

        #region fields'nProps
        // timecard
        public string Id;
        public string EmployeeId;
        public string PositionId;
        public string PayCycleId;
        public string Comments;
        public DateTime PeriodStartDate;
        public DateTime PeriodEndDate;
        public DateTime WeekStartDate;
        public DateTime WeekEndDate;
        public List<TimeManagement.Entities.TimeEntry> TimeEntries;
        TimeManagement.Entities.TimeEntry TimeEntry;
        public Timestamp Timestamp;

        // timeentry
        public string teId;
        public string earnTypeId;
        public string personLeaveId;
        public TimeSpan? workedTime;
        public DateTime workedDate;

        #endregion

        #region ini
        public void TimeCardTestInitialize()
        {
            // time entry
            teId = "100";
            earnTypeId = "C";
            personLeaveId = "D";
            workedTime = new TimeSpan(08, 00, 00);
            workedDate = new DateTime(2000, 08, 28);

            // timecard
            Id = "001";
            EmployeeId = "24601";
            PositionId = "A";
            PayCycleId = "B";
            Comments = "The fear is excruciating, but therein lies the answer";
            PeriodStartDate = new DateTime(2000, 1, 1);
            PeriodEndDate = new DateTime(2000, 2, 2);
            WeekStartDate = new DateTime(2000, 1, 1);
            WeekEndDate = new DateTime(2000, 2, 2);
            TimeEntry = new TimeManagement.Entities.TimeEntry(Id, earnTypeId, workedTime, workedDate, teId);
            TimeEntries = new List<TimeManagement.Entities.TimeEntry>() { TimeEntry };
            Timestamp = new Timestamp("MCD", DateTime.Today, "MCD", DateTime.Today);
        }
        #endregion

        #region tests

        [TestClass]
        public class TimecardConstructorTests : TimecardTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeCardTestInitialize();
            }

            [TestMethod]
            public void TimeCardCorrectlyConstructedTest()
            {
                var tc = new Timecard(EmployeeId, PayCycleId, PositionId, PeriodStartDate, PeriodEndDate, WeekStartDate, WeekEndDate, Id);
                Assert.AreEqual(EmployeeId, tc.EmployeeId);
                Assert.AreEqual(PayCycleId, tc.PayCycleId);
                Assert.AreEqual(PositionId, tc.PositionId);
                Assert.AreEqual(PeriodStartDate, tc.PeriodStartDate);
                Assert.AreEqual(PeriodEndDate, tc.PeriodEndDate);
                Assert.AreEqual(Id, tc.Id);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespaceEmployeeIdThrowsArgExceptionTest()
            {
                new Timecard(string.Empty, PayCycleId, PositionId, PeriodStartDate, PeriodEndDate, WeekStartDate, WeekEndDate);
            }
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespacePayCycleIdThrowsArgExceptionTest()
            {
                new Timecard(EmployeeId, string.Empty, PositionId, PeriodStartDate, PeriodEndDate, WeekStartDate, WeekEndDate);
            }
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespacePositionIdThrowsArgExceptionTest()
            {
                new Timecard(EmployeeId, PayCycleId, string.Empty, PeriodStartDate, PeriodEndDate, WeekStartDate, WeekEndDate);
            }
            [TestMethod]
            public void TimecardCompareWorksWhenSameTest()
            {
                var tc1 = new Timecard(EmployeeId, PayCycleId, PositionId, PeriodStartDate, PeriodEndDate, WeekStartDate, WeekEndDate, Id);
                var tc2 = new Timecard(EmployeeId, PayCycleId, PositionId, PeriodStartDate, PeriodEndDate, WeekStartDate, WeekEndDate, Id);

                Assert.IsTrue(tc1.CompareTimecard(tc2));
            }
            [TestMethod]
            public void TimecardCompareWorksWhenDifferentTest()
            {
                var tc1 = new Timecard(EmployeeId, PayCycleId, PositionId, PeriodStartDate, PeriodEndDate, WeekStartDate, WeekEndDate, Id);
                EmployeeId = "9999";
                var tc2 = new Timecard(EmployeeId, PayCycleId, PositionId, PeriodStartDate, PeriodEndDate, WeekStartDate, WeekEndDate, Id);

                Assert.IsFalse(tc1.CompareTimecard(tc2));
            }
        }

        #endregion

    }
}
