﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Tests.Entities
{
    [TestClass]
    public class DailyOvertimeAllocationTests
    {

        public DateTime date;
        public TimeSpan overtime;

        [TestClass]
        public class ConstructorTests : DailyOvertimeAllocationTests
        {
            public DailyOvertimeAllocation allocationUnderTest
            {
                get
                {
                    return new DailyOvertimeAllocation(date, overtime);
                }
            }

            [TestInitialize]
            public void Initialize()
            {
                date = new DateTime(2016, 6, 14);
                overtime = TimeSpan.FromHours(2);
            }

            [TestMethod]
            public void DateTest()
            {
                Assert.AreEqual(date, allocationUnderTest.Date);
            }

            [TestMethod]
            public void OvertimeTest()
            {
                Assert.AreEqual(overtime, allocationUnderTest.Overtime);
            }
        }

        [TestClass]
        public class EqualsTests : DailyOvertimeAllocationTests
        {
            public DailyOvertimeAllocation allocationUnderTest
            {
                get
                {
                    return new DailyOvertimeAllocation(date, overtime);
                }
            }

            [TestInitialize]
            public void Initialize()
            {
                date = new DateTime(2016, 6, 14);
                overtime = TimeSpan.FromHours(2);
            }

            [TestMethod]
            public void AreEqualTests()
            {
                Assert.IsTrue(allocationUnderTest.Equals(allocationUnderTest));
            }

            [TestMethod]
            public void DateMustEqualTest()
            {
                var obj1 = allocationUnderTest;
                date = new DateTime(2014, 6, 14);
                var obj2 = allocationUnderTest;
                Assert.IsFalse(obj1.Equals(obj2));
            }

            [TestMethod]
            public void OvertimeMustEqualTest()
            {
                var obj1 = allocationUnderTest;
                overtime = TimeSpan.FromHours(100);
                var obj2 = allocationUnderTest;
                Assert.IsFalse(obj1.Equals(obj2));
            }
        }
    }
}
