﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.Base.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;


namespace Ellucian.Colleague.Domain.TimeManagement.Tests.Entities
{
    [TestClass]
    public class TimeEntryTests
    {


        public string id;
        public string timecardId;
        public string projectId;
        public string earnTypeId;
        public string personLeaveId;
        public DateTimeOffset? inDateTime;
        public DateTimeOffset? outDateTime;
        public TimeSpan? workedTime;
        public DateTime workedDate;
        public Timestamp timestamp;

        public TimeEntry timeEntry;

        public void TimeEntryTestInitialize()
        {
            id = "jygr";
            timecardId = "sdfgfgd";
            projectId = "ghgghfd";
            earnTypeId = "hgfd";
            personLeaveId = "gfgfdd";
            inDateTime = new DateTimeOffset(2011, 12, 13, 07, 00, 00, 00, new TimeSpan(0, 0, 0)) as DateTimeOffset?; //7am
            outDateTime = new DateTimeOffset(2011, 12, 13, 14, 00, 00, 00, new TimeSpan(0, 0, 0)) as DateTimeOffset?; //2pm
            workedDate = new DateTime(2011, 12, 13);
            workedTime = new TimeSpan(12, 5, 0);
            timestamp = new Timestamp("MCD", DateTime.Today, "MCD", DateTime.Today);
        }

        [TestClass]
        public class TimeEntryConstructor_ExistingTotalTime : TimeEntryTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntryTestInitialize();
            }

            [TestMethod]
            public void TimeEntryCorrectlyConstructedWithSpanTest()
            {
                var te = new TimeManagement.Entities.TimeEntry(timecardId, earnTypeId, workedTime, workedDate, id);
                Assert.AreEqual(id, te.Id);
                Assert.AreEqual(earnTypeId, te.EarningsTypeId);
                Assert.AreEqual(timecardId, te.TimecardId);
                Assert.AreEqual(workedTime, te.WorkedTime);
                Assert.AreEqual(workedDate, te.WorkedDate);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void IdRequiredTest()
            {
                new TimeEntry(timecardId, earnTypeId, workedTime, workedDate, string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespaceEarnTypeIdThrowsArgExceptionTest()
            {
                new TimeEntry(timecardId, string.Empty, workedTime, workedDate, id);
            }

            [TestMethod]
            public void CanBeChangedToDetailTimeTest()
            {
                var te = new TimeManagement.Entities.TimeEntry(timecardId, earnTypeId, workedTime, workedDate, id)
                    {
                        InDateTime = inDateTime,
                        OutDateTime = outDateTime
                    };

                Assert.AreNotEqual(workedTime, te.WorkedTime);
                Assert.AreEqual(outDateTime - inDateTime, te.WorkedTime);
            }
        }

        [TestClass]
        public class TimeEntryConstructor_ExistingDetailTime : TimeEntryTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntryTestInitialize();
            }

            [TestMethod]
            public void TimeEntryCorrectlyConstructedWithTimeTest()
            {
                var te = new TimeManagement.Entities.TimeEntry(timecardId, earnTypeId, inDateTime, outDateTime, workedDate, id);
                Assert.AreEqual(id, te.Id);
                Assert.AreEqual(earnTypeId, te.EarningsTypeId);
                Assert.AreEqual(timecardId, te.TimecardId);
                Assert.AreEqual(inDateTime, te.InDateTime);
                Assert.AreEqual(outDateTime, outDateTime);
                Assert.AreEqual(workedDate, te.WorkedDate);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void IdRequiredTest()
            {

                new TimeEntry(timecardId, earnTypeId, inDateTime, outDateTime, workedDate, string.Empty);

            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespaceEarnTypeIdThrowsArgExceptionTest()
            {

                new TimeEntry(timecardId, string.Empty, inDateTime, outDateTime, workedDate, id);

            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void InDateMustBeSameAsWorkedDateTest()
            {
                var newWorkedDate = workedDate.AddDays(1);
                new TimeEntry(timecardId, earnTypeId, null, outDateTime, newWorkedDate, id);//this is ok when in date is null
                new TimeEntry(timecardId, earnTypeId, inDateTime, outDateTime, workedDate.AddDays(1), id);//this is not ok
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void EarlyOutDateTimeThrowsExceptionTest()
            {
                new TimeEntry(timecardId, earnTypeId, inDateTime, inDateTime.Value.AddDays(-1), workedDate, id);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void LateInDateTimeThrowsExceptionTest()
            {
                new TimeEntry(timecardId, earnTypeId, outDateTime.Value.AddSeconds(1), outDateTime, workedDate, id);
            }

            [TestMethod]
            public void CanBeChangedToSummaryTest()
            {
                var te = new TimeManagement.Entities.TimeEntry(timecardId, earnTypeId, inDateTime, outDateTime, workedDate, id) 
                { 
                    InDateTime = null,
                    OutDateTime = null,
                    WorkedTime = workedTime
                };

                Assert.AreNotEqual(outDateTime - inDateTime, te.WorkedTime);
                Assert.AreEqual(workedTime, te.WorkedTime);
            }

            [TestMethod]
            public void InDateOffsetAppliedWhenComparingWorkDateTest()
            {
                inDateTime = new DateTimeOffset(2011, 12, 12, 22, 0, 0, TimeSpan.FromHours(-4));
                new TimeManagement.Entities.TimeEntry(timecardId, earnTypeId, inDateTime, outDateTime, workedDate, id);

                inDateTime = new DateTimeOffset(2011, 12, 14, 2, 0, 0, TimeSpan.FromHours(4));
                outDateTime = inDateTime.Value.AddHours(8);
                new TimeManagement.Entities.TimeEntry(timecardId, earnTypeId, inDateTime, outDateTime, workedDate, id);
            }
        }

        [TestClass]
        public class TimeEntryConstructor_NewTotalTime : TimeEntryTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntryTestInitialize();
            }

            [TestMethod]
            public void TimeEntryCorrectlyConstructedWithSpanTest()
            {
                var te = new TimeManagement.Entities.TimeEntry(timecardId, earnTypeId, workedTime, workedDate);
                Assert.IsNull(te.Id);
                Assert.AreEqual(earnTypeId, te.EarningsTypeId);
                Assert.AreEqual(timecardId, te.TimecardId);
                Assert.AreEqual(workedTime, te.WorkedTime);
                Assert.AreEqual(workedDate, te.WorkedDate);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespaceEarnTypeIdThrowsArgExceptionTest()
            {
                new TimeEntry(timecardId, string.Empty, workedTime, workedDate);
            }
        }

        [TestClass]
        public class TimeEntryConstructor_NewDetailTime : TimeEntryTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntryTestInitialize();
            }

            [TestMethod]
            public void TimeEntryCorrectlyConstructedWithTimeTest()
            {
                var te = new TimeManagement.Entities.TimeEntry(timecardId, earnTypeId, inDateTime, outDateTime, workedDate);
                Assert.IsNull(te.Id);
                Assert.AreEqual(earnTypeId, te.EarningsTypeId);
                Assert.AreEqual(timecardId, te.TimecardId);
                Assert.AreEqual(inDateTime, te.InDateTime);
                Assert.AreEqual(outDateTime, outDateTime);
                Assert.AreEqual(workedDate, te.WorkedDate);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOrWhitespaceEarnTypeIdThrowsArgExceptionTest()
            {

                new TimeEntry(timecardId, string.Empty, inDateTime, outDateTime, workedDate);

            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void InDateMustBeSameAsWorkedDateTest()
            {
                var newWorkedDate = workedDate.AddDays(1);
                new TimeEntry(timecardId, earnTypeId, null, outDateTime, newWorkedDate);//this is ok when in date is null
                new TimeEntry(timecardId, earnTypeId, inDateTime, outDateTime, workedDate.AddDays(1));//this is not ok
            }

        }

        [TestClass]
        public class TimeEntryEqualityTests : TimeEntryTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntryTestInitialize();
            }

            [TestMethod]
            public void ExitingRecordEqualityTest()
            {
                var te1 = new TimeEntry(timecardId, earnTypeId, workedTime, workedDate, id);
                var te2 = new TimeEntry("foo", "bar", null, workedDate.AddDays(1), id);

                //this is a contrived scenario, but ids are the same so they're equal.
                Assert.IsTrue(te1.Equals(te2));
                Assert.AreEqual(te1.GetHashCode(), te2.GetHashCode());
            }

            [TestMethod]
            public void ExistingRecordInequalityTest()
            {
                var te1 = new TimeEntry(timecardId, earnTypeId, workedTime, workedDate, id);
                var te2 = new TimeEntry(timecardId, earnTypeId, workedTime, workedDate, "foobar");

                //this is a contrived scenario, but ids are the same so they're equal.
                Assert.IsFalse(te1.Equals(te2));
                Assert.AreNotEqual(te1.GetHashCode(), te2.GetHashCode());
            }

            [TestMethod]
            public void NewRecordEqualityTest()
            {
                var te1 = new TimeEntry(timecardId, earnTypeId, inDateTime, outDateTime, workedDate)
                    {
                        PersonLeaveId = personLeaveId,
                        ProjectId = projectId
                    };
                var te2 = new TimeEntry(timecardId, earnTypeId, inDateTime, outDateTime, workedDate)
                {
                    PersonLeaveId = personLeaveId,
                    ProjectId = projectId
                };

                //properties are the same so they're equal
                Assert.IsTrue(te1.Equals(te2));
                Assert.AreEqual(te1.GetHashCode(), te2.GetHashCode());
            }

            [TestMethod]
            public void NewRecordInequalityTest()
            {
                var te1 = new TimeEntry("foo", earnTypeId, inDateTime, outDateTime, workedDate)
                {
                    PersonLeaveId = personLeaveId,
                    ProjectId = projectId
                };
                var te2 = new TimeEntry("bar", earnTypeId, inDateTime, outDateTime, workedDate)
                {
                    PersonLeaveId = personLeaveId,
                    ProjectId = projectId
                };

                //properties are not the same so they're equal
                Assert.IsFalse(te1.Equals(te2));
                Assert.AreNotEqual(te1.GetHashCode(), te2.GetHashCode());
            }

            [TestMethod]
            public void NullObjectEqualityTest()
            {
                var te1 = new TimeEntry(timecardId, earnTypeId, workedTime, workedDate, id);
                Assert.IsFalse(te1.Equals(null));
            }

            [TestMethod]
            public void DifferentObjectTypeTest()
            {
                var te1 = new TimeEntry(timecardId, earnTypeId, workedTime, workedDate, id);
                Assert.IsFalse(te1.Equals(DateTime.Today));
            }

            


            [TestMethod]
            public void CompareTimeEntryWorksWhenSameTest()
            {
                var te1 = new TimeEntry(timecardId, earnTypeId, inDateTime, outDateTime, workedDate, id);
                var te2 = new TimeEntry(timecardId, earnTypeId, inDateTime, outDateTime, workedDate, id);

                Assert.IsTrue(te1.CompareTimeEntry(te2));
            }
            [TestMethod]
            public void CompareTimeEntryWorksWhenDifferentTest()
            {
                var te1 = new TimeEntry(timecardId, earnTypeId, inDateTime, outDateTime, workedDate, id);
                timecardId = "454544";
                var te2 = new TimeEntry(timecardId, earnTypeId, inDateTime, outDateTime, workedDate, id);

                Assert.IsFalse(te1.CompareTimeEntry(te2));
            }
        }

        [TestClass]
        public class EarningsTypeIdTests : TimeEntryTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntryTestInitialize();

                timeEntry = new TimeEntry(timecardId, earnTypeId, workedTime, workedDate);
            }

            [TestMethod]
            public void SetAndGetTest()
            {
                timeEntry.EarningsTypeId = "foobar";
                Assert.AreEqual("foobar", timeEntry.EarningsTypeId);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void CannotSetToNullTest()
            {
                timeEntry.EarningsTypeId = null;
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void CannotSetToEmptyTest()
            {
                timeEntry.EarningsTypeId = "";
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void CannotSetToWhitespaceTest()
            {
                timeEntry.EarningsTypeId = " ";
            }
        }

        [TestClass]
        public class TimeInTimeOutTests : TimeEntryTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntryTestInitialize();

                timeEntry = new TimeEntry(timecardId, earnTypeId, inDateTime, outDateTime, workedDate);
            }

            [TestMethod]
            public void SetInTimeTest()
            {
                var value = timeEntry.InDateTime.Value.AddSeconds(-1);
                timeEntry.InDateTime = value;
                Assert.IsTrue(value.EqualsExact(timeEntry.InDateTime.Value));
            }

            [TestMethod]
            public void SetOutTimeTest()
            {
                var value = timeEntry.OutDateTime.Value.AddSeconds(1);
                timeEntry.OutDateTime = value;
                Assert.IsTrue(value.EqualsExact(timeEntry.OutDateTime.Value));
            }


            [TestMethod]
            public void SetInTimeAndOutTimeNullTest()
            {
                timeEntry.InDateTime = null;
                timeEntry.OutDateTime = null;
                Assert.IsNull(timeEntry.InDateTime);

                timeEntry.InDateTime = inDateTime;
                Assert.IsNull(timeEntry.OutDateTime);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void InTimeMustBeBeforeOrSameAsOutTimeTest()
            {
                timeEntry.InDateTime = null;
                timeEntry.OutDateTime = inDateTime;
                timeEntry.InDateTime = inDateTime.Value.AddSeconds(1);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void OutTimeMustBeAfterOrSameAsInTimeTest()
            {
                timeEntry.OutDateTime = null;
                timeEntry.InDateTime = outDateTime;
                timeEntry.OutDateTime = outDateTime.Value.AddSeconds(-1);
            }
        }

        [TestClass]
        public class WorkedTimeTests : TimeEntryTests
        {
            public TimeEntry summaryEntry;
            public TimeEntry detailEntry;

            [TestInitialize]
            public void Initialize()
            {
                base.TimeEntryTestInitialize();

                summaryEntry = new TimeEntry(timecardId, earnTypeId, workedTime, workedDate);
                detailEntry = new TimeEntry(timecardId, earnTypeId, inDateTime, outDateTime, workedDate);
            }

            [TestMethod]
            public void GetSetWorkedTimeOnSummaryEntryTest()
            {
                var value = new TimeSpan(14, 1, 2);

                summaryEntry.WorkedTime = value;
                Assert.IsTrue(value.Equals(summaryEntry.WorkedTime));
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidOperationException))]
            public void SetWorkedTimeOnDetailEntryTest()
            {
                detailEntry.WorkedTime = workedTime;
            }


            [TestMethod]
            [ExpectedException(typeof(InvalidOperationException))]
            public void SetWorkedTimeOnDetailWithInTimeTest()
            {
                detailEntry.OutDateTime = null;
                detailEntry.WorkedTime = workedTime;
            }


            [TestMethod]
            [ExpectedException(typeof(InvalidOperationException))]
            public void SetWorkedTimeOnDetailWithOutTimeTest()
            {
                detailEntry.InDateTime = null;
                detailEntry.WorkedTime = workedTime;
            }

            [TestMethod]
            public void CalculateWorkedTimeOnDetailTest()
            {
                var time = outDateTime - inDateTime;
                Assert.AreEqual(time, detailEntry.WorkedTime.Value);
            }

            [TestMethod]
            public void NullWorkedTimeOnDetailNullOutTimeTest()
            {
                detailEntry.OutDateTime = null;
                Assert.IsNull(detailEntry.WorkedTime);
            }

            [TestMethod]
            public void NullWorkedTimeOnDetailNullInTimeTest()
            {
                detailEntry.InDateTime = null;
                Assert.IsNull(detailEntry.WorkedTime);
            }
        }

    }
}
