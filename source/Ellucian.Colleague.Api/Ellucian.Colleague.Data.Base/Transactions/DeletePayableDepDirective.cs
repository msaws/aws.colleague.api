//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 3/22/2017 4:56:43 PM by user apikopstein
//
//     Type: CTX
//     Transaction ID: DELETE.PAYABLE.DEP.DIRECTIVE
//     Application: CORE
//     Environment: dvcoll
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;

namespace Ellucian.Colleague.Data.Base.Transactions
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract]
	[ColleagueDataContract(ColleagueId = "DELETE.PAYABLE.DEP.DIRECTIVE", GeneratedDateTime = "3/22/2017 4:56:43 PM", User = "apikopstein")]
	[SctrqDataContract(Application = "CORE", DataContractVersion = 1)]
	public class DeletePayableDepDirectiveRequest
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		[DataMember(IsRequired = true)]
		[SctrqDataMember(AppServerName = "IN.PERSON.ADDR.BNK.INFO.ID", InBoundData = true)]        
		public string PersonAddrBnkInfoId { get; set; }

		public DeletePayableDepDirectiveRequest()
		{	
		}
	}

	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract]
	[ColleagueDataContract(ColleagueId = "DELETE.PAYABLE.DEP.DIRECTIVE", GeneratedDateTime = "3/22/2017 4:56:43 PM", User = "apikopstein")]
	[SctrqDataContract(Application = "CORE", DataContractVersion = 1)]
	public class DeletePayableDepDirectiveResponse
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "ERROR.MESSAGE", OutBoundData = true)]        
		public string ErrorMessage { get; set; }

		public DeletePayableDepDirectiveResponse()
		{	
		}
	}
}
