//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 8/19/2016 9:23:04 AM by user coles
//
//     Type: CTX
//     Transaction ID: UPDATE.SELFSERVICE.PREFERENCE
//     Application: CORE
//     Environment: dvcoll_wstst01
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;

namespace Ellucian.Colleague.Data.Base.Transactions
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract]
	[ColleagueDataContract(ColleagueId = "UPDATE.SELFSERVICE.PREFERENCE", GeneratedDateTime = "8/19/2016 9:23:04 AM", User = "coles")]
	[SctrqDataContract(Application = "CORE", DataContractVersion = 1)]
	public class UpdateSelfservicePreferencesRequest
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.PERSON.ID", InBoundData = true)]        
		public string PersonId { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.SELFSRVPREFS.PREFKEY", InBoundData = true)]        
		public string SelfsrvPrefKey { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.SELFSRVPREFS.PREFDATA", InBoundData = true)]        
		public string SelfsrvPrefData { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.SELFSERVICE.PREFERENCES.ID", InBoundData = true)]        
		public string SelfservicePreferencesId { get; set; }

		public UpdateSelfservicePreferencesRequest()
		{	
		}
	}

	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract]
	[ColleagueDataContract(ColleagueId = "UPDATE.SELFSERVICE.PREFERENCE", GeneratedDateTime = "8/19/2016 9:23:04 AM", User = "coles")]
	[SctrqDataContract(Application = "CORE", DataContractVersion = 1)]
	public class UpdateSelfservicePreferencesResponse
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.ERROR.OCCURRED", OutBoundData = true)]        
		public string ErrorOccurred { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "A.ERROR.MESSAGE", OutBoundData = true)]        
		public string ErrorMessage { get; set; }

		public UpdateSelfservicePreferencesResponse()
		{	
		}
	}
}
