//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 3/22/2017 3:57:17 PM by user mdediana
//
//     Type: CTX
//     Transaction ID: DELETE.BANKING.AUTH.CLAIMS
//     Application: CORE
//     Environment: colldv
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;

namespace Ellucian.Colleague.Data.Base.Transactions
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract]
	[ColleagueDataContract(ColleagueId = "DELETE.BANKING.AUTH.CLAIMS", GeneratedDateTime = "3/22/2017 3:57:17 PM", User = "mdediana")]
	[SctrqDataContract(Application = "CORE", DataContractVersion = 1)]
	public class DeleteBankingAuthClaimsRequest
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		[DataMember]
		[SctrqDataMember(AppServerName = "RECORD.IDS", InBoundData = true)]        
		public List<string> RecordIds { get; set; }

		public DeleteBankingAuthClaimsRequest()
		{	
			RecordIds = new List<string>();
		}
	}

	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract]
	[ColleagueDataContract(ColleagueId = "DELETE.BANKING.AUTH.CLAIMS", GeneratedDateTime = "3/22/2017 3:57:17 PM", User = "mdediana")]
	[SctrqDataContract(Application = "CORE", DataContractVersion = 1)]
	public class DeleteBankingAuthClaimsResponse
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }


		public DeleteBankingAuthClaimsResponse()
		{	
		}
	}
}
