﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Http.Configuration;
using slf4net;
using Ellucian.Web.Dependency;
using Ellucian.Colleague.Domain.Exceptions;

namespace Ellucian.Colleague.Data.Base.Repositories
{
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class ExternalEducationRepository : BaseColleagueRepository, IExternalEducationRepository
    {
        private readonly int _readSize;

        public ExternalEducationRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings apiSettings)
            : base(cacheProvider, transactionFactory, logger)
        {

            CacheTimeout = Level1CacheTimeoutValue;

            this._readSize = ((apiSettings != null) && (apiSettings.BulkReadSize > 0)) ? apiSettings.BulkReadSize : 5000;

        }

        /// <summary>
        ///  Get all external education data consistng of academic credential data not from home insitution,
        /// along with some data from institutions attended
        /// </summary>
        /// <param name="limit"></param>
        /// <param name="bypassCache"></param>
        /// <param name="personId">person id</param>
        /// <param name="offset"></param>
        /// <returns>Collection of ExternalEducation domain entities</returns>
        public async Task<Tuple<IEnumerable<ExternalEducation>, int>> GetExternalEducationAsync(int offset, int limit, bool bypassCache = false, string personId = "")
        {
            var criteria = string.Empty;
            var coreDefaultData = GetDefaults();

            var hostInstitutionId = coreDefaultData.DefaultHostCorpId;

            if (!(string.IsNullOrEmpty(hostInstitutionId)))
            {
                criteria += "WITH ACAD.INSTITUTIONS.ID NE '" + hostInstitutionId + "'";
            }

            if (!string.IsNullOrEmpty(personId))
            {
                if (criteria != "")
                {
                    criteria += " AND ";
                }

                criteria += "WITH ACAD.PERSON.ID EQ '" + personId + "'";
            }

            //if ((!bypassCache) && (offset == 0) && (limit == 0))
            if (bypassCache)
            {
                var acadCredentialsIds = await DataReader.SelectAsync("ACAD.CREDENTIALS", criteria);
                var totalCount = acadCredentialsIds.Count();
                Array.Sort(acadCredentialsIds);
                var subList = acadCredentialsIds.Skip(offset).Take(limit).ToArray();
                var externalEducationData = await DataReader.BulkReadRecordAsync<AcadCredentials>("ACAD.CREDENTIALS", subList);
                var externalEducationEntities = await BuildExternalEducationsAsync(externalEducationData);
                return new Tuple<IEnumerable<ExternalEducation>, int>(externalEducationEntities, totalCount);

            }
            else
            {
                return await GetOrAddToCacheAsync<Tuple<IEnumerable<ExternalEducation>, int>>("ExternalEducation",
                    async () =>
                    {
                        var acadCredentialIds = await DataReader.SelectAsync("ACAD.CREDENTIALS", criteria);
                        var totalCount = acadCredentialIds.Count();
                        Array.Sort(acadCredentialIds);
                        var externalEducationData = new Collection<AcadCredentials>();
                        var subList = acadCredentialIds.Skip(offset).Take(limit).ToArray();
                        var bulkData = await DataReader.BulkReadRecordAsync<AcadCredentials>("ACAD.CREDENTIALS", subList);
                        foreach (var prog in bulkData)
                            externalEducationData.Add(prog);
                        var studentProgEntities = await BuildExternalEducationsAsync(externalEducationData);
                        return new Tuple<IEnumerable<ExternalEducation>, int>(studentProgEntities, totalCount);

                    }
                    );
            }
        }

        /// <summary>
        /// Get a single External Education domain entity from an academic credential guid.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public async Task<ExternalEducation> GetExternalEducationByGuidAsync(string guid)
        {
            return await GetExternalEducationByIdAsync(await GetExternalEducationIdFromGuidAsync(guid));
        }


        /// <summary>
        /// Get the record key from a GUID
        /// </summary>
        /// <param name="guid">The GUID</param>
        /// <returns>Primary key</returns>
        public async Task<string> GetExternalEducationIdFromGuidAsync(string guid)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw new ArgumentNullException("guid");
            }

            var idDict = await DataReader.SelectAsync(new GuidLookup[] {new GuidLookup(guid)});
            if (idDict == null || idDict.Count == 0)
            {
                throw new KeyNotFoundException("AcadCredentials GUID " + guid + " not found.");
            }

            var foundEntry = idDict.FirstOrDefault();
            if (foundEntry.Value == null)
            {
                throw new KeyNotFoundException("AcadCredentials GUID " + guid + " lookup failed.");
            }

            if (foundEntry.Value.Entity != "ACAD.CREDENTIALS")
            {
                throw new RepositoryException("GUID " + guid + " has different entity, " + foundEntry.Value.Entity + ", than expected, REMARKS");
            }

            return foundEntry.Value.PrimaryKey;
        }


        /// <summary>
        /// Get a single External Education domain entity from an academic credential id.
        /// </summary>
        /// <param name="id">The academic credential id</param>
        /// <returns>External Education domain entity object</returns>
        public async Task<ExternalEducation> GetExternalEducationByIdAsync(string id)
        {
            ExternalEducation acadCredential = null;

            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id", "ID is required to get a acadCredential.");
            }

            // Now we have an ID, so we can read the record
            var record = await DataReader.ReadRecordAsync<AcadCredentials>(id);
            if (record == null)
            {
                throw new KeyNotFoundException(string.Concat("Record not found, or acadCredential with ID ", id, "invalid."));
            }

            var defaults = GetDefaults();
            if (record.AcadInstitutionsId == defaults.DefaultHostCorpId)
                throw new ArgumentException("id", "Record associated with home institution can not be returned.");

            acadCredential = await BuildExternalEducationAsync(record);

            return acadCredential;
        }


        public async Task<IEnumerable<ExternalEducation>> BuildExternalEducationsAsync(Collection<AcadCredentials> sources)
        {
            var acadCredentialCollection = new List<ExternalEducation>();
            foreach (var source in sources)
            {
                acadCredentialCollection.Add(await BuildExternalEducationAsync(source));
            }
            return acadCredentialCollection.AsEnumerable();
        }

        public async Task<ExternalEducation> BuildExternalEducationAsync(AcadCredentials source)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source", "source required to build acadCredential.");
            }

            ExternalEducation externalEducation = null;
            var instAttendData = new Collection<Base.DataContracts.InstitutionsAttend>();
            var instAttendIds = new List<string>();

            externalEducation = new ExternalEducation(source.RecordGuid)
            {
                AcadPersonId = source.AcadPersonId,
                AcadInstitutionsId = source.AcadInstitutionsId,
                AcadDegree = source.AcadDegree,
                AcadMajors = source.AcadMajors,
                AcadMinors = source.AcadMinors,
                AcadSpecialization = source.AcadSpecialization,
                AcadStartDate = source.AcadStartDate,
                AcadEndDate = source.AcadEndDate,
                AcadGpa = source.AcadGpa,
                AcadHonors = source.AcadHonors,
                AcadCommencementDate = source.AcadCommencementDate,
                AcadDegreeDate = source.AcadDegreeDate,
                AcadThesis = source.AcadThesis,
                AcadRankDenominator = source.AcadRankDenominator,
                AcadRankPercent = source.AcadRankPercent,
                AcadRankNumerator = source.AcadRankNumerator,

            };

            if ((!string.IsNullOrEmpty(source.AcadPersonId)) && (!string.IsNullOrEmpty(source.AcadInstitutionsId)))
            {

                instAttendIds.Add(source.AcadPersonId + "*" + source.AcadInstitutionsId);

                instAttendData = await DataReader.BulkReadRecordAsync<Base.DataContracts.InstitutionsAttend>(instAttendIds.ToArray());

                var instAttend = instAttendData.FirstOrDefault();
                if (instAttend != null)
                {
                    externalEducation.InstExtCredits = instAttend.InstaExtCredits;
                    externalEducation.InstTransciptDate = instAttend.InstaTranscriptDate;
                }
            }
            return externalEducation;
        }

        /// <summary>
        /// Get the Defaults from CORE to compare default institution Id
        /// </summary>
        /// <returns>Core Defaults</returns>
        private Base.DataContracts.Defaults GetDefaults()
        {
            return GetOrAddToCache<Data.Base.DataContracts.Defaults>("CoreDefaults",
                () =>
                {
                    var coreDefaults = DataReader.ReadRecord<Data.Base.DataContracts.Defaults>("CORE.PARMS", "DEFAULTS");
                    if (coreDefaults == null)
                    {
                        logger.Info("Unable to access DEFAULTS from CORE.PARMS table.");
                        coreDefaults = new Defaults();
                    }
                    return coreDefaults;
                }, Level1CacheTimeoutValue);
        }
    }
}