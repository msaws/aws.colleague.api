﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Data.Base.Transactions;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Services;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.DataContracts;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Http.Configuration;
using Ellucian.Web.Dependency;
using slf4net;
using Ellucian.Dmi.Runtime;
using Ellucian.Colleague.Domain.Exceptions;
using Microsoft.Practices.EnterpriseLibrary.Common.Utility;

namespace Ellucian.Colleague.Data.Base.Repositories
{
     [RegisterType]
     public class PersonBaseRepository : BaseColleagueRepository, IPersonBaseRepository
     {
        private const string _preferredHierarchyCode = "PREFERRED";
          private ApplValcodes PersonEthnics;
          private ApplValcodes MaritalStatuses;
          private ApplValcodes PersonRaces;
          protected string Quote = '"'.ToString();
          protected const int PersonCacheTimeout = 120;
          protected const int AddressCacheTimeout = 120;
          protected const string PersonContractCachePrefix = "PersonContract";
          protected const string AddressContractCachePrefix = "AddressContract";
          private readonly string colleagueTimeZone;
          public static char _SM = Convert.ToChar(DynamicArray.SM);

          /// <summary>
          /// Initializes a new instance of the <see cref="PersonRepository"/> class.
          /// </summary>
          /// <param name="cacheProvider">The cache provider.</param>
          /// <param name="transactionFactory">The transaction factory.</param>
          /// <param name="logger">The logger.</param>
          public PersonBaseRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings settings)
               : base(cacheProvider, transactionFactory, logger)
          {
               colleagueTimeZone = settings.ColleagueTimeZone;
          }

          #region Valcodes
          private async Task<ApplValcodes> GetEthnicitiesAsync()
          {
               if (PersonEthnics != null)
               {
                    return PersonEthnics;
               }

               PersonEthnics = await GetOrAddToCacheAsync<ApplValcodes>("PersonEthnics",
                   async () =>
                   {
                        ApplValcodes ethnicsTable = await DataReader.ReadRecordAsync<ApplValcodes>("CORE.VALCODES", "PERSON.ETHNICS");
                        if (ethnicsTable == null)
                        {
                             var errorMessage = "Unable to access PERSON.ETHNICS valcode table.";
                             logger.Info(errorMessage);
                             throw new Exception(errorMessage);
                        }
                        return ethnicsTable;
                   }, Level1CacheTimeoutValue);
               return PersonEthnics;
          }
          private async Task<ApplValcodes> GetMaritalStatusesAsync()
          {
               if (MaritalStatuses != null)
               {
                    return MaritalStatuses;
               }

               MaritalStatuses = await GetOrAddToCacheAsync<ApplValcodes>("MaritalStatuses",
                   async () =>
                   {
                        ApplValcodes statusesTable = await DataReader.ReadRecordAsync<ApplValcodes>("CORE.VALCODES", "MARITAL.STATUSES");
                        if (statusesTable == null)
                        {
                             var errorMessage = "Unable to access MARITAL.STATUSES valcode table.";
                             logger.Info(errorMessage);
                             throw new Exception(errorMessage);
                        }
                        return statusesTable;
                   }, Level1CacheTimeoutValue);
               return MaritalStatuses;
          }
          private async Task<ApplValcodes> GetRacesAsync()
          {
               if (PersonRaces != null)
               {
                    return PersonRaces;
               }

               PersonRaces = await GetOrAddToCacheAsync<ApplValcodes>("PersonRaces",
                   async () =>
                   {
                        ApplValcodes racesTable = await DataReader.ReadRecordAsync<ApplValcodes>("CORE.VALCODES", "PERSON.RACES");
                        if (racesTable == null)
                        {
                             var errorMessage = "Unable to access PERSON.RACES valcode table.";
                             logger.Info(errorMessage);
                             throw new Exception(errorMessage);
                        }
                        return racesTable;
                   }, Level1CacheTimeoutValue);
               return PersonRaces;
          }

          /// <summary>
        /// Returns the NameAddrHierarchy record.
          /// </summary>
          /// <returns></returns>
        public async Task<NameAddressHierarchy> GetCachedNameAddressHierarchyAsync(string nameAddressHierarchyCode = _preferredHierarchyCode)
          {
            NameAddressHierarchy requestedNameAddressHierarchy = await GetOrAddToCacheAsync<NameAddressHierarchy>(nameAddressHierarchyCode + "_NameAddressHierarchy",
               async () =>
               {
                NameAddrHierarchy nameAddrHierarchy = await DataReader.ReadRecordAsync<NameAddrHierarchy>("NAME.ADDR.HIERARCHY", nameAddressHierarchyCode);
                if (nameAddrHierarchy == null)
                    {
                    if (nameAddressHierarchyCode != _preferredHierarchyCode)
                    {
                        // for all hierarchies except PREFERRED throw an error - this means nothing will be cached 
                        var errorMessage = "Unable to find NAME.ADDR.HIERARCHY record with Id " + nameAddressHierarchyCode + ". Cache not built.";
                         logger.Info(errorMessage);
                        throw new KeyNotFoundException(errorMessage);
                    }
                    else
                    {
                        // All clients are expected to have a PREFERRED name address hierarchy. If they don't, report it but also default one so that it is cached.
                        var errorMessage = "Unable to find NAME.ADDR.HIERARCHY record with Id " + nameAddressHierarchyCode + ". Creating a basic preferred hierarchy with PF name type.";
                        logger.Info(errorMessage);
                        // Construct a default one with the desired name type values.
                        nameAddrHierarchy = new NameAddrHierarchy();
                        nameAddrHierarchy.Recordkey = _preferredHierarchyCode;
                        nameAddrHierarchy.NahNameHierarchy = new List<string>() { "PF" };
                    }
                    
                }
                // Build the NameAddressHierarchy Entity and cache that.
                NameAddressHierarchy newHierarchy = new NameAddressHierarchy(nameAddrHierarchy.Recordkey);
                if (nameAddrHierarchy.NahNameHierarchy != null && nameAddrHierarchy.NahNameHierarchy.Any())
                {
                    foreach (var nameType in nameAddrHierarchy.NahNameHierarchy)
                    {
                        newHierarchy.AddNameTypeHierarchy(nameType);
                    }
                }

                return newHierarchy;
               }, Level1CacheTimeoutValue);
            return requestedNameAddressHierarchy;
          }
          #endregion

          #region PersonBase Get methods

          /// <summary>
          /// Gets PersonBase entity (Same as person but without Preferred Address)
          /// </summary>
          /// <param name="personId">Id of base person to retrieve</param>
          /// <param name="useCache">Indicates whether to retrieve from cache if available</param>
          /// <returns></returns>
          public async Task<PersonBase> GetPersonBaseAsync(string personId, bool useCache = true)
          {
               Domain.Base.Entities.PersonBase personBaseEntity = await GetBaseAsync<Domain.Base.Entities.PersonBase>(personId,
                    person =>
                    {
                         Domain.Base.Entities.PersonBase entity = new Domain.Base.Entities.Person(person.Recordkey, person.LastName, person.PrivacyFlag);
                         return entity;
                    });
               return personBaseEntity;
          }

          /// <summary>
          /// Gets PersonBase entities (Same as person but without Preferred Address)
          /// </summary>
          /// <param name="personIds">Ids of base persons to retrieve</param>
          /// <param name="useCache">Indicates whether to retrieve from cache if available</param>
          /// <returns>Collection of <see cref="PersonBase"/>PersonBase entities</returns>
          public async Task<IEnumerable<PersonBase>> GetPersonsBaseAsync(IEnumerable<string> personIds)
          {
               List<PersonBase> personEntities = new List<PersonBase>();
               try
               {
                    personEntities = await GetBaseAsync<Domain.Base.Entities.PersonBase>(personIds, person =>
                    {
                         List<Domain.Base.Entities.PersonBase> entities = new List<Domain.Base.Entities.PersonBase>();
                         foreach (var p in person)
                         {
                              entities.Add(new PersonBase(p.Recordkey, p.LastName, p.PrivacyFlag));
                         }
                         return entities;
                    });
               }
               catch (ArgumentOutOfRangeException aoore)
               {
                    var message = "One or more IDs did not return any data. Person records may be corrupt.";
                    logger.Info(message);
                    throw new ApplicationException(message, aoore);
               }
               catch (ArgumentNullException ane)
               {
                    var message = "One or more IDs was null.";
                    logger.Info(message);
                    throw new ApplicationException(message, ane);
               }

               return personEntities;
          }

          /// <summary>
          /// Builds a base person entity.
          /// </summary>
          /// <typeparam name="TDomain"></typeparam>
          /// <param name="id"></param>
          /// <param name="objectBuilder"></param>
          /// <param name="useCache"></param>
          /// <returns></returns>
          public async Task<TDomain> GetBaseAsync<TDomain>(string id, Func<DataContracts.Person, TDomain> objectBuilder, bool useCache = true)
              where TDomain : Domain.Base.Entities.PersonBase
          {
               if (string.IsNullOrEmpty(id))
               {
                    throw new ArgumentNullException("id", "Must provide an ID to get a record.");
               }
               DataContracts.Person record = await GetPersonContractAsync(id, useCache);
               if (record == null)
               {
                    throw new ArgumentOutOfRangeException("id", "No person record for ID " + id);
               }
               // Create the specified domain object
               TDomain person = objectBuilder.Invoke(record);
               var personBaseEntities = await BuildBasePersonsAsync<TDomain>(new List<string>() { id },
                   new Collection<DataContracts.Person>() { record }, new Collection<TDomain>() { person });
               return personBaseEntities.FirstOrDefault();
          }

          /// <summary>
          /// Builds base person entities.
          /// </summary>
          /// <typeparam name="TDomain"></typeparam>
          /// <param name="id"></param>
          /// <param name="objectBuilder"></param>
          /// <param name="useCache"></param>
          /// <returns></returns>
          public async Task<List<TDomain>> GetBaseAsync<TDomain>(IEnumerable<string> ids, Func<IEnumerable<DataContracts.Person>, IEnumerable<TDomain>> objectBuilder, bool useCache = true)
              where TDomain : Domain.Base.Entities.PersonBase
          {
               if (ids == null || !ids.Any())
               {
                    throw new ArgumentNullException("ids", "Must provide IDs to get records.");
               }
               IEnumerable<DataContracts.Person> records = await GetPersonContractsAsync(ids);
               if (records == null || !records.Any())
               {
                    throw new ArgumentOutOfRangeException("ids", "No person records for IDs " + string.Join(",", ids));
               }
               // Create the specified domain object
               IEnumerable<TDomain> persons = objectBuilder.Invoke(records);
               var personBaseEntities = await BuildBasePersonsAsync<TDomain>(ids, records, persons);
               return personBaseEntities.ToList();
          }

          public async Task<Data.Base.DataContracts.Person> GetPersonContractAsync(string personId, bool useCache = true)
          {
               Data.Base.DataContracts.Person personData = null;
               if (useCache)
               {
                    personData = await GetOrAddToCacheAsync<Data.Base.DataContracts.Person>(PersonContractCachePrefix + personId,
                        async () =>
                        {
                             var personRecord = await DataReader.ReadRecordAsync<Data.Base.DataContracts.Person>("PERSON", personId);
                             if (personRecord == null)
                             {
                                  throw new ArgumentOutOfRangeException("Person Id " + personId + " is not returning any data. Person may be corrupted.");
                             }
                             return personRecord;
                        }, PersonCacheTimeout);
               }
               else
               {
                    personData = await DataReader.ReadRecordAsync<Data.Base.DataContracts.Person>("PERSON", personId);
                    await AddOrUpdateCacheAsync<Data.Base.DataContracts.Person>(PersonContractCachePrefix + personId, personData, PersonCacheTimeout);
               }
               return personData;
          }

          public async Task<IEnumerable<Data.Base.DataContracts.Person>> GetPersonContractsAsync(IEnumerable<string> personIds)
          {
               IEnumerable<Data.Base.DataContracts.Person> personData = null;
               personData = await DataReader.BulkReadRecordAsync<Data.Base.DataContracts.Person>("PERSON", personIds.ToArray());
               if (personData == null)
               {
                    throw new ArgumentOutOfRangeException("Person Ids " + string.Join(",", personIds) + " are not returning any data. Person records may be corrupted.");
               }
               return personData;
          }

          public async Task<Data.Base.DataContracts.Address> GetPersonAddressContractAsync(string addressId, bool useCache = true)
          {
               Data.Base.DataContracts.Address addressData = null;
               if (useCache)
               {
                    addressData = await GetOrAddToCacheAsync<Data.Base.DataContracts.Address>(AddressContractCachePrefix + addressId,
                        async () =>
                        {
                             var addressRecord = await DataReader.ReadRecordAsync<Data.Base.DataContracts.Address>("ADDRESS", addressId);
                             if (addressRecord == null)
                             {
                                  throw new ArgumentOutOfRangeException("Address Id " + addressId + " is not returning any data. Address may be corrupted.");
                             }
                             return addressRecord;
                        }, AddressCacheTimeout);
               }
               else
               {
                    addressData = await DataReader.ReadRecordAsync<Data.Base.DataContracts.Address>("ADDRESS", addressId);
                    await AddOrUpdateCacheAsync<Data.Base.DataContracts.Address>(AddressContractCachePrefix + addressId, addressData, AddressCacheTimeout);
               }
               return addressData;
          }

          public async Task<IEnumerable<Data.Base.DataContracts.Address>> GetPersonAddressContractsAsync(IEnumerable<string> addressIds)
          {
               IEnumerable<Data.Base.DataContracts.Address> addressData = null;
               addressData = await DataReader.BulkReadRecordAsync<Data.Base.DataContracts.Address>("ADDRESS", addressIds.ToArray());
               if (addressData == null)
               {
                    throw new ArgumentOutOfRangeException("Address Ids " + string.Join(",", addressIds) + " are not returning any data. Address records may be corrupted.");
               }
               return addressData;
          }

          #endregion

          #region Build Person Methods

          /// <summary>
          /// Builds PersonBase objects 
          /// </summary>
          /// <typeparam name="TDomain"></typeparam>
          /// <param name="personIds"></param>
          /// <param name="records"></param>
          /// <param name="personBasedObjects"></param>
          /// <returns></returns>
          private async Task<IEnumerable<TDomain>> BuildBasePersonsAsync<TDomain>(IEnumerable<string> personIds, IEnumerable<DataContracts.Person> records, IEnumerable<TDomain> personBasedObjects)
          where TDomain : Domain.Base.Entities.PersonBase
          {
               var personResults = new List<TDomain>();
               List<string> personIdsNotFound = new List<string>();

               foreach (string personId in personIds)
               {
                    DataContracts.Person record = records.Where(p => p.Recordkey == personId).FirstOrDefault();
                    TDomain tDomainObject = personBasedObjects.Where(p => p.Id == personId).FirstOrDefault();
                    if (record != null && tDomainObject != null)
                    {
                         TDomain personBasedObject = await BuildBasePersonAsync<TDomain>(personId, record, tDomainObject);

                         personResults.Add(personBasedObject);
                    }
                    else
                    {
                         personIdsNotFound.Add(personId);
                    }
               }
               if (personIdsNotFound.Count() > 0)
               {
                    // log any ids that were not found.
                    var errorMessage = "The following person Ids were requested but not found: " + string.Join(",", personIdsNotFound.ToArray());
                    logger.Info(errorMessage);
               }
               return personResults;
          }

          /// <summary>
          /// Build PersonBase object, which is the base class of the Person Object.
          /// </summary>
          /// <typeparam name="TDomain"></typeparam>
          /// <param name="personId"></param>
          /// <param name="record"></param>
          /// <param name="tDomainObject"></param>
          /// <returns></returns>
          protected async Task<TDomain> BuildBasePersonAsync<TDomain>(string personId, Base.DataContracts.Person record, TDomain tDomainObject)
          where TDomain : Domain.Base.Entities.PersonBase
          {
               TDomain personBasedObject = tDomainObject;

               if (record != null && personBasedObject != null)
               {
                    // Populate additional Person fields
                    personBasedObject.Guid = record.RecordGuid;
                    personBasedObject.FirstName = record.FirstName;
                    personBasedObject.MiddleName = record.MiddleName;
                    personBasedObject.BirthNameFirst = record.BirthNameFirst;
                    personBasedObject.BirthNameMiddle = record.BirthNameMiddle;
                personBasedObject.BirthNameLast = record.BirthNameLast;
                    personBasedObject.Prefix = record.Prefix;
                    personBasedObject.Suffix = record.Suffix;
                    personBasedObject.Nickname = record.Nickname;
                    personBasedObject.GovernmentId = record.Ssn;
                    personBasedObject.BirthDate = record.BirthDate;
                    personBasedObject.MaritalStatusCode = record.MaritalStatus;
                    personBasedObject.RaceCodes = record.PerRaces;
                    personBasedObject.EthnicCodes = record.PerEthnics;
                    personBasedObject.DeceasedDate = record.DeceasedDate;
                    personBasedObject.Gender = record.Gender;
                    personBasedObject.PersonCorpIndicator = record.PersonCorpIndicator;
                personBasedObject.ChosenLastName = record.PersonChosenLastName;
                personBasedObject.ChosenFirstName = record.PersonChosenFirstName;
                personBasedObject.ChosenMiddleName = record.PersonChosenMiddleName;
                personBasedObject.PersonalPronounCode = record.PersonalPronoun;
                // Take the mail label name or preferred name override values from the data contract (which could be either a name or a coded entry) and 
                // convert it into simply a name override - where the coded entries are convered into their actual results.
                // In case of mail label name, it defaults to the preferred name override information unless it has its own.
                string mailLabelOverride = record.PersonMailLabel != null && record.PersonMailLabel.Any() ? string.Join(" ", record.PersonMailLabel.ToArray()) : record.PreferredName;
                personBasedObject.MailLabelNameOverride = FormalNameFormat(mailLabelOverride, record.Prefix, record.FirstName, record.MiddleName, record.LastName, record.Suffix);
                personBasedObject.PreferredNameOverride = FormalNameFormat(record.PreferredName, record.Prefix, record.FirstName, record.MiddleName, record.LastName, record.Suffix);
                // Add any special formatted names provided for this person.
                if (record.PFormatEntityAssociation != null && record.PFormatEntityAssociation.Any())
                {
                    foreach (var pFormat in record.PFormatEntityAssociation)
                    {
                        try
                        {
                            personBasedObject.AddFormattedName(pFormat.PersonFormattedNameTypesAssocMember, pFormat.PersonFormattedNamesAssocMember);
                        }
                        catch (Exception)
                        {
                            logger.Info("Unable to add formatted name to person " + record.Recordkey + " with type " + pFormat.PersonFormattedNameTypesAssocMember + " and name " + pFormat.PersonFormattedNamesAssocMember);
                        }
                    }
                }
                    // If the change time is null, use the change date for the time (which will be zero). A null date causes
                    // the resulting datetimeoffset to be 1/1/0001 00.00.000.
                    if (record.PersonChgtime == null)
                    {
                         personBasedObject.LastChangedDateTime = record.PersonChangeDate.ToPointInTimeDateTimeOffset(
                                     record.PersonChangeDate, colleagueTimeZone) ?? new DateTimeOffset();
                    }
                    else
                    {
                         personBasedObject.LastChangedDateTime = record.PersonChgtime.ToPointInTimeDateTimeOffset(
                                         record.PersonChangeDate, colleagueTimeZone) ?? new DateTimeOffset();
                    }

                    // Build Ethnicities Data element from Ethnic Codes and Race Codes
                    var ethnicOrigins = new List<EthnicOrigin>();
                    if (record.PerEthnics != null && record.PerEthnics.Count > 0)
                    {
                         foreach (var ethnicCode in record.PerEthnics)
                         {
                              var ethnicOrigin = EthnicOrigin.Unknown;
                              var codeAssoc = (await GetEthnicitiesAsync()).ValsEntityAssociation.Where(v => v.ValInternalCodeAssocMember == ethnicCode).FirstOrDefault();
                              if (codeAssoc != null && codeAssoc.ValActionCode1AssocMember == "H")
                              {
                                   ethnicOrigin = EthnicOrigin.HispanicOrLatino;
                                   ethnicOrigins.Add(ethnicOrigin);
                              }
                         }
                         foreach (var raceCode in record.PerRaces)
                         {
                              var ethnicOrigin = EthnicOrigin.Unknown;
                              var codeAssoc = (await GetRacesAsync()).ValsEntityAssociation.Where(v => v.ValInternalCodeAssocMember == raceCode).FirstOrDefault();
                              if (codeAssoc != null)
                              {
                                   switch (codeAssoc.ValActionCode1AssocMember)
                                   {
                                        case ("1"): { ethnicOrigin = EthnicOrigin.AmericanIndianOrAlaskanNative; break; }
                                        case ("2"): { ethnicOrigin = EthnicOrigin.Asian; break; }
                                        case ("3"): { ethnicOrigin = EthnicOrigin.BlackOrAfricanAmerican; break; }
                                        case ("4"): { ethnicOrigin = EthnicOrigin.NativeHawaiianOrOtherPacificIslander; break; }
                                        case ("5"): { ethnicOrigin = EthnicOrigin.White; break; }
                                   }
                                   if (ethnicOrigin != EthnicOrigin.Unknown)
                                   {
                                        ethnicOrigins.Add(ethnicOrigin);
                                   }
                              }
                         }
                    }
                    personBasedObject.Ethnicities = ethnicOrigins;

                    // Build Marital Status field
                    personBasedObject.MaritalStatus = new MaritalState();
                    if (!string.IsNullOrEmpty(record.MaritalStatus))
                    {
                         var maritalStatusCode = (await GetMaritalStatusesAsync()).ValsEntityAssociation.Where(v => v.ValInternalCodeAssocMember == record.MaritalStatus).FirstOrDefault();
                         if (maritalStatusCode != null)
                         {
                              switch (maritalStatusCode.ValActionCode1AssocMember)
                              {
                                   case ("1"): { personBasedObject.MaritalStatus = MaritalState.Single; break; }
                                   case ("2"): { personBasedObject.MaritalStatus = MaritalState.Married; break; }
                                   case ("3"): { personBasedObject.MaritalStatus = MaritalState.Divorced; break; }
                                   case ("4"): { personBasedObject.MaritalStatus = MaritalState.Widowed; break; }
                              }
                         }
                    }

                // Get preferred name using name hierarchy PREFERRED.
                NameAddressHierarchy hierarchy = await GetCachedNameAddressHierarchyAsync(_preferredHierarchyCode);
                if (hierarchy != null)
                {
                    var personNameHierarchy = PersonNameService.GetHierarchyName(personBasedObject, hierarchy);
                    personBasedObject.PreferredName = personNameHierarchy != null ? personNameHierarchy.FullName : string.Empty;
                }

                    // person email addresses
                    if (record.PeopleEmailEntityAssociation != null && record.PeopleEmailEntityAssociation.Count > 0)
                    {
                         foreach (var emailData in record.PeopleEmailEntityAssociation)
                         {
                              try
                              {
                                   EmailAddress eAddress = new EmailAddress(emailData.PersonEmailAddressesAssocMember, emailData.PersonEmailTypesAssocMember);
                                   eAddress.IsPreferred = emailData.PersonPreferredEmailAssocMember == "Y";
                                   personBasedObject.AddEmailAddress(eAddress);
                              }
                              catch (Exception ex)
                              {
                                   // Log the original exception and a serialized version of the email
                                   LogDataError("Person email address", personId, emailData, ex);
                              }
                         }
                    }

                    // person alternate Ids
                    if (record.PersonAltEntityAssociation != null && record.PersonAltEntityAssociation.Count > 0)
                    {
                         foreach (var personAltData in record.PersonAltEntityAssociation)
                         {
                              try
                              {
                                   var personAlt = new PersonAlt(personAltData.PersonAltIdsAssocMember, personAltData.PersonAltIdTypesAssocMember);
                                   personBasedObject.AddPersonAlt(personAlt);
                              }
                              catch (Exception ex)
                              {
                                   // Log the original exception and a serialized version of the person alt Id
                                   LogDataError("Person alternate ID", personId, personAltData, ex);
                              }
                         }
                    }

               }
               return personBasedObject;
          }


          #endregion

          #region Name methods

          /// <summary>
        /// Builds a formal mail name override or preferred name override based on the specific coded name override in data contract (could be a code) and all the person's current name parts.
          /// </summary>
          /// <param name="nameValue">Either the mail label or preferred name override - which can include special codes</param>
          /// <param name="prefix">Name prefix</param>
          /// <param name="firstName">First Name</param>
          /// <param name="middleName">Middle Name</param>
          /// <param name="lastName">Last Name</param>
          /// <param name="suffix">Suffix</param>
          /// <returns></returns>
        public static string FormalNameFormat(string nameValue, string prefix, string firstName, string middleName, string lastName, string suffix)
          {
               string firstInitial = !string.IsNullOrEmpty(firstName) ? firstName.Trim().Substring(0, 1) + "." : string.Empty;
               string middleInitial = !string.IsNullOrEmpty(middleName) ? middleName.Trim().Substring(0, 1) + "." : string.Empty;
               if (!string.IsNullOrEmpty(nameValue))
               {
                    string shortCutCode = nameValue.ToUpper();
                    switch (shortCutCode)
                    {
                         case "IM":
                        return PersonNameService.FormatName(prefix, firstInitial, middleName, lastName, suffix);
                         case "II":
                        return PersonNameService.FormatName(prefix, firstInitial, middleInitial, lastName, suffix);
                         case "FM":
                        return PersonNameService.FormatName(prefix, firstName, middleName, lastName, suffix);
                         case "FI":
                        return PersonNameService.FormatName(prefix, firstName, middleInitial, lastName, suffix);
                         default:
                              return nameValue;
                    }
               }
               // If no name value supplied default to prefix first middle initial last suffix.
            return PersonNameService.FormatName(prefix, firstName, middleInitial, lastName, suffix);
          }

          #endregion

          #region Get Persona Methods

          /// <summary>
          /// Determine if the identifier represents a valid person
          /// </summary>
          /// <param name="personId">Identifier to test</param>
          /// <returns>True if the identifier represents a person in the system</returns>
          public async Task<bool> IsPersonAsync(string personId)
          {
               if (string.IsNullOrEmpty(personId))
                    throw new ArgumentNullException("personId", "Person ID is required for determining existence.");

               var result = (await DataReader.SelectAsync("PERSON", new string[] { personId }, string.Empty)).FirstOrDefault();
               return result != null && result.Any() ? true : false;
        }

        /// <summary>
        /// Determine if the identifier represents a valid corporation
        /// </summary>
        /// <param name="personId">Identifier to test</param>
        /// <returns>True if the identifier represents a person in the system</returns>
        public async Task<bool> IsCorpAsync(string personId)
        {
            if (string.IsNullOrEmpty(personId))
                throw new ArgumentNullException("personId", "Person ID is required for determining existence.");

            var result = await DataReader.ReadRecordAsync<DataContracts.Person>("PERSON", personId);
            return result != null && result.PersonCorpIndicator == "Y" ? true : false;
        }

        /// <summary>
        /// Determine if the person is a faculty member
        /// </summary>
        /// <param name="personId">Person ID</param>
        /// <returns>True if the person is a faculty member, false otherwise</returns>
        public async Task<bool> IsFacultyAsync(string personId)
          {
               if (string.IsNullOrEmpty(personId))
                    throw new ArgumentNullException("personId", "Person ID is required for determining faculty role");

               var result = (await DataReader.SelectAsync("FACULTY", new string[] { personId }, string.Empty)).FirstOrDefault();
               return result != null && result.Count() > 0 ? true : false;
          }

          /// <summary>
          /// Determine if the person is a faculty advisor
          /// </summary>
          /// <param name="personId">Person ID</param>
          /// <returns>True if the person is a faculty advisor, false otherwise</returns>
          public async Task<bool> IsAdvisorAsync(string personId)
          {
              if (string.IsNullOrEmpty(personId))
                  throw new ArgumentNullException("personId", "Person ID is required for determining faculty role");

              var result = (await DataReader.SelectAsync("PERSON", new string[] { personId }, "WITH FAC.ADVISOR.FLAG = 'Y'")).FirstOrDefault();
              return result != null && result.Count() > 0 ? true : false;
          }

          /// <summary>
          /// Determine if the person is a student
          /// </summary>
          /// <param name="personId">Person ID</param>
          /// <returns>True if the person is a student, false otherwise</returns>
          public async Task<bool> IsStudentAsync(string personId)
          {
               if (string.IsNullOrEmpty(personId))
                    throw new ArgumentNullException("personId", "Person ID is required for determining student role");

               var result = (await DataReader.SelectAsync("STUDENTS", new string[] { personId }, string.Empty)).FirstOrDefault();
               return result != null && result.Count() > 0 ? true : false;
          }

        /// <summary>
        /// Determines if the person with the specified id is an applicant 
        /// </summary>
        /// <param name="personId">person id</param>
        /// <returns>boolean value indicating whether the person is an applicant</returns>
        public async Task<bool> IsApplicantAsync(string personId)
        {
            if (string.IsNullOrEmpty(personId))
            {
                throw new ArgumentNullException("personId", "Person ID is required for determining applicant role");
            }

            var result = (await DataReader.SelectAsync("APPLICANTS", new string[] { personId }, string.Empty)).FirstOrDefault();
            return result != null && result.Any() ? true : false;
        }

          #endregion

        #region Get Person/Org Guids
          /// <summary>
          /// Gets person guid from opers
          /// </summary>
          /// <param name="opersKey">opersKey</param>
          /// <returns></returns>
          public async Task<string> GetPersonGuidFromOpersAsync(string opersKey)
          {
               if (string.IsNullOrEmpty(opersKey))
                    throw new ArgumentNullException("opersKey", "Opers ID is a required argument.");

               string personId = await GetPersonIdFromOpersAsync(opersKey);
               if (string.IsNullOrEmpty(personId))
                    throw new ArgumentNullException("opersKey", "No ID found for person ID " + opersKey);

               string personGuid = await GetPersonGuidFromIdAsync(personId);
               if (!string.IsNullOrEmpty(personGuid))
                    return personGuid;

               return string.Empty;
          }

          /// <summary>
          /// Gets person id from opers
          /// </summary>
          /// <param name="opersKey">opersKey</param>
          /// <returns></returns>
          public async Task<string> GetPersonIdFromOpersAsync(string opersKey)
          {
               if (string.IsNullOrEmpty(opersKey))
                    throw new ArgumentNullException("opersKey", "Opers ID is a required argument.");

               int id;
               bool isNumber = int.TryParse(opersKey, out id);
               if (isNumber && opersKey.Length == 7)
                    return opersKey;
               //else get the recordId and return
               DataContracts.Opers record = await DataReader.ReadRecordAsync<DataContracts.Opers>("UT.OPERS", opersKey);

               if (record != null)
               {
                    return record.SysPersonId;
               }
               return string.Empty;
          }

          /// <summary>
          /// Get the GUID from a person ID
          /// </summary>
          /// <param name="personId">The person ID</param>
          /// <returns>The GUID</returns>
          public async Task<string> GetPersonGuidFromIdAsync(string personId)
          {
               if (string.IsNullOrEmpty(personId))
                    throw new ArgumentNullException("personId", "Person ID is a required argument.");

               var lookup = new RecordKeyLookup("PERSON", personId, false);
               var result = await DataReader.SelectAsync(new RecordKeyLookup[] { lookup });
               if (result != null && result.Count > 0)
               {
                    RecordKeyLookupResult lookupResult = null;
                    if (result.TryGetValue(lookup.ResultKey, out lookupResult))
                    {
                         if (lookupResult != null)
                         {
                              return lookupResult.Guid;
                         }
                    }
               }

               throw new ArgumentOutOfRangeException("personId", "No GUID found for person ID " + personId);
          }

          /// <summary>
        /// Gets all the person guids
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        public async Task<Tuple<IEnumerable<string>, int>> GetPersonGuidsAsync(int offset, int limit, bool bypassCache)
        {
            //Dont return Orgs & Institutions
            string criteria = "WITH PERSON.CORP.INDICATOR NE 'Y'";

            var personIds = await DataReader.SelectAsync("PERSON", criteria);
            if ((personIds != null && !personIds.Any()))
            {
                return new Tuple<IEnumerable<string>, int>(null, 0);
            }

            var totalCount = personIds.Count();

            Array.Sort(personIds);

            var sublist = personIds.Skip(offset).Take(limit);

            var personGuids = new List<string>();

            if (sublist != null && sublist.Any())
            {
                try
                {
                    // convert the person keys to person guids
                    var personGuidLookup = sublist.ToList().ConvertAll(p => new RecordKeyLookup("PERSON", p, false)).ToArray();
                    personGuids = (await DataReader.SelectAsync(personGuidLookup)).ToList().ConvertAll(p => p.Value.Guid);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error occured while getting person guids.", ex); ;
                }
            }

            return (personGuids != null && personGuids.Any()) ? new Tuple<IEnumerable<string>, int>(personGuids, totalCount) : null;
        }

        /// <summary>
          /// Get a list of guids associated with faculty
          /// </summary>
          /// <returns>List of person guids associated with faculty</returns>
          public async Task<Tuple<IEnumerable<string>, int>> GetFacultyPersonGuidsAsync(int offset, int limit)
          {
               var facultyPersonGuids = new List<string>();
               // get the faculty keys
               var facultyIds = await DataReader.SelectAsync("FACULTY", "");
            // check HR database to see if we have faculty setup there.
            // the best we can do is check for termination date.  If we have no ID's
            // returned, then they aren't using HR to track faculty contracts.
            var hrperIds = (await DataReader.SelectAsync("HRPER", facultyIds.ToArray(), "WITH HRP.EFFECT.TERM.DATE = ''"));
            if (hrperIds != null && hrperIds.Any())
            {
                facultyIds = hrperIds;
            }
               var totalCount = facultyIds.Count();
               Array.Sort(facultyIds);
               var sublist = facultyIds.Skip(offset).Take(limit);
               var newFacultyIds = sublist.ToList();
               if (newFacultyIds != null && newFacultyIds.Any())
               {
                    // convert the faculty keys to person guids
                    var facultyPersonGuidLookup = newFacultyIds.ConvertAll(f => new RecordKeyLookup("PERSON", f, false)).ToArray();
                    facultyPersonGuids = (await DataReader.SelectAsync(facultyPersonGuidLookup)).ToList().ConvertAll(p => p.Value.Guid);
               }
               return new Tuple<IEnumerable<string>, int>(facultyPersonGuids, totalCount);
          }

          /// <summary>
          /// Get a list of guids associated with faculty
          /// </summary>
          /// <param name="Offset">Paging offset</param>
          /// <param name="Limit">Paging limit</param>
          /// <param name="bypassCache">Flag to bypass cache</param>
          /// <param name="title">Specific title</param>
          /// <param name="firstName">Specific first name</param>
          /// <param name="middleName">Specific middle name</param>
          /// <param name="lastNamePrefix">Last name beings with</param>
          /// <param name="lastName">Specific last name</param>
          /// <param name="pedigree">Specific suffix</param>
          /// <param name="preferredName">Specific preferred name</param>
          /// <param name="role">Specific role of a person</param>
          /// <param name="credentialType">Credential type of either colleagueId or ssn</param>
          /// <param name="credentialValue">Specific value of the credential to be evaluated</param>
          /// <param name="personFilter">Person Saved List selection or list name from person-filters</param>
          /// <returns>List of person guids associated with faculty</returns>
          public async Task<Tuple<IEnumerable<string>, int>> GetFilteredPersonGuidsAsync(int offset, int limit, bool bypassCache,
              string title, string firstName, string middleName, string lastNamePrefix, string lastName, string pedigree,
              string preferredName, string role, string credentialType, string credentialValue, string personFilter)
          {
            var ctxFilters = string.Concat(firstName, middleName, lastNamePrefix, lastName, preferredName, personFilter);

            int totalCount = 0;
            var personGuids = new List<string>();
            if (!string.IsNullOrEmpty(ctxFilters))
            {
                var request = new GetPersonFilterResultsRequest()
                {
                    Title = title,
                    FirstName = firstName,
                    MiddleName = middleName,
                    LastNamePrefix = lastNamePrefix,
                    LastName = lastName,
                    Pedigree = pedigree,
                    PreferredName = preferredName,
                    Role = role,
                    CredentialType = credentialType,
                    CredentialValue = credentialValue,
                    Guid = personFilter,
                    Offset = offset,
                    Limit = limit
                };


                // Check for invalid PERSON.FILTERS guid, return 200 and empty set if filter doesn't exist

                if (!String.IsNullOrEmpty(personFilter))
                {
                    IEnumerable<PersonFilter> allFilters = null;

                    allFilters = await GetGuidCodeItemAsync<SaveListParms, PersonFilter>("AllPersonFilters", "SAVE.LIST.PARMS",
                   (c, g) => new PersonFilter(g, c.Recordkey, c.SlpDescription), CacheTimeout, this.DataReader.IsAnonymous, false);



                    if (allFilters == null || allFilters.Count() == 0 || allFilters.FirstOrDefault(af => af.Guid == personFilter) == null)
                    {
                        return new Tuple<IEnumerable<string>, int>(personGuids, totalCount);
                    }
                }

                // Execute request


                var response = await transactionInvoker.ExecuteAsync<GetPersonFilterResultsRequest, GetPersonFilterResultsResponse>(request);


                if (response.ErrorMessages.Any())
                {
                    var errorMessage = "Error(s) occurred retrieving person data: ";
                    var exception = new RepositoryException(errorMessage);
                    foreach (var errMsg in response.ErrorMessages)
                    {
                        exception.AddError(new Domain.Entities.RepositoryError("person.filter", errMsg));
                        errorMessage += string.Join(Environment.NewLine, errMsg);
                    }
                    logger.Error(errorMessage.ToString());
                    throw exception;
                }

                var personIds = response.PersonIds;
                var filteredPersonIds = personIds.ToArray();

                var subList = filteredPersonIds.Skip(offset).Take(limit).ToArray();

                var idLookUpList = new List<RecordKeyLookup>();
                subList.ForEach(s => idLookUpList.Add(new RecordKeyLookup("PERSON", s, false)));

                var personGuidLookupList = await DataReader.SelectAsync(idLookUpList.ToArray());
                foreach (var o in personGuidLookupList)
                {
                    if (o.Value != null && !string.IsNullOrEmpty(o.Value.Guid))
                    {
                        personGuids.Add(o.Value.Guid);
                    }
                    else
                    {
                        var ex = new RepositoryException();
                        ex.AddError(new Domain.Entities.RepositoryError(string.Format("PERSON record '{0}' is missing a guid.", o.Key.Split('+')[1])));
                        throw ex;
                    }
                }

                totalCount = response.TotalRecords.Value;
            }
            else
            {
                List<string> personIds = new List<string>();
                List<string> rolePersonList = new List<string>();
                List<string> credentialPersonList = new List<string>();
                string[] limitKeys = null;
                string criteria = "WITH PERSON.CORP.INDICATOR NE 'Y'";
                if (!string.IsNullOrEmpty(title))
                {
                    criteria = string.Concat(criteria, " AND WITH PREFIX LIKE '...", title, "...'");
                }
                if (!string.IsNullOrEmpty(pedigree))
                {
                    criteria = string.Concat(criteria, " AND WITH SUFFIX LIKE '...", pedigree, "...'");
                }
                if (!string.IsNullOrEmpty(credentialValue))
                {
                    // Only Colleague Person ID or SSN/SIN is allowed.
                    if (credentialType.ToLowerInvariant() == "colleaguepersonid")
                    {
                        criteria = string.Concat(criteria, " AND WITH ID = '", credentialValue, "'");
                    }
                    else if (credentialType.ToLowerInvariant() != "colleagueusername")
                    {
                        criteria = string.Concat(criteria, " AND WITH SSN = '", credentialValue, "'");
                    }
                    else
                    {
                        if (credentialValue.Contains("'"))
                        {
                            credentialValue = credentialValue.Replace("'", "");
                        }
                        credentialPersonList = (await DataReader.SelectAsync("PERSON.PIN", string.Format("WITH PERSON.PIN.USER.ID = '{0}'", credentialValue))).ToList();
                    }
                }

                //time to check for other role filters
                //build the orgId list for the role in the filter
                if (!string.IsNullOrEmpty(role))
                {
                    string roleCriteria = string.Empty;
                    string[] hrperIds = null;

                    switch (role.ToLower())
                    {
                        case "student":
                            roleCriteria = "WITH PER.INTG.ROLE = 'Student'";
                            rolePersonList = (await DataReader.SelectAsync("STUDENTS", "")).ToList();
                            break;
                        case "instructor":
                            roleCriteria = "WITH PER.INTG.ROLE = 'Instructor'";
                            rolePersonList = (await DataReader.SelectAsync("FACULTY", "")).ToList();
                            // check HR database to see if we have faculty setup there.
                            // the best we can do is check for termination date.  If we have no ID's
                            // returned, then they aren't using HR to track faculty contracts.
                            hrperIds = (await DataReader.SelectAsync("HRPER", rolePersonList.ToArray(), "WITH HRP.EFFECT.TERM.DATE = ''"));
                            if (hrperIds != null && hrperIds.Any())
                            {
                                rolePersonList = hrperIds.ToList();
                            }
                            break;
                        case "employee":
                            roleCriteria = "WITH PER.INTG.ROLE = 'Employee'";
                            var employeeKeys = (await DataReader.SelectAsync("EMPLOYES", "")).ToList();
                            rolePersonList.AddRange(employeeKeys);
                            var hrperKeys = (await DataReader.SelectAsync("HRPER", "")).ToList();
                            rolePersonList.AddRange(hrperKeys);
                            break;
                        case "prospectivestudent":
                            roleCriteria = "WITH PER.INTG.ROLE = 'ProspectiveStudent'";
                            rolePersonList = (await DataReader.SelectAsync("APPLICANTS", "WITH APP.PROSPECT.STATUS NE ''")).ToList();
                            break;
                        case "advisor":
                            roleCriteria = "WITH PER.INTG.ROLE = 'Advisor'";
                            rolePersonList = (await DataReader.SelectAsync("FACULTY", "WITH FAC.ADVISE.FLAG = 'Y'")).ToList();
                            // check HR database to see if we have faculty setup there.
                            // the best we can do is check for termination date.  If we have no ID's
                            // returned, then they aren't using HR to track faculty contracts.
                            hrperIds = (await DataReader.SelectAsync("HRPER", rolePersonList.ToArray(), "WITH HRP.EFFECT.TERM.DATE = ''"));
                            if (hrperIds != null && hrperIds.Any())
                            {
                                rolePersonList = hrperIds.ToList();
                            }
                            break;
                        case "alumni":
                            roleCriteria = "WITH PER.INTG.ROLE = 'Alumni'";
                            break;
                        case "vendor":
                            roleCriteria = "WITH PER.INTG.ROLE = 'Vendor'";
                            rolePersonList = (await DataReader.SelectAsync("VENDORS", "")).ToList();
                            break;
                        default:
                            throw new ArgumentException(string.Format("Unsupported role of '{0}' used in filter. ", role), "role");
                    }

                    if (!string.IsNullOrEmpty(roleCriteria))
                    {
                        var personIntgKeys = (await DataReader.SelectAsync("PERSON.INTG", roleCriteria)).ToList();
                        rolePersonList.AddRange(personIntgKeys);
                    }
                    if (rolePersonList.Any())
                    {
                        if (credentialPersonList.Any())
                        {
                            var personsWithCredentialAndRole = credentialPersonList.Intersect(rolePersonList);
                            rolePersonList = personsWithCredentialAndRole.ToList();
                        }
                        if (!string.IsNullOrEmpty(credentialValue) && credentialType.ToLowerInvariant() == "colleagueusername" && !credentialPersonList.Any())
                        {
                            // Do not return personIds.  No records match criteria.
                        }
                        else
                        { 
                            limitKeys = rolePersonList.Distinct().ToArray();
                            personIds = (await DataReader.SelectAsync("PERSON", limitKeys, criteria)).ToList();
                        }
                    }
                }
                else
                {
                    if (credentialPersonList.Any())
                    {
                        limitKeys = credentialPersonList.Distinct().ToArray();
                        personIds = (await DataReader.SelectAsync("PERSON", limitKeys, criteria)).ToList();
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(credentialValue) && credentialType.ToLowerInvariant() == "colleagueusername" && !credentialPersonList.Any())
                        {
                            // Do not return personIds.  No records match criteria.
                        }
                        else
                        {
                            personIds = (await DataReader.SelectAsync("PERSON", criteria)).ToList();
                        }
                    }
                }

                totalCount = personIds.Count();
                var filteredPersonIds = personIds.ToArray();
                Array.Sort(filteredPersonIds);

                var subList = filteredPersonIds.Skip(offset).Take(limit).ToArray();

                var idLookUpList = new List<RecordKeyLookup>();
                subList.ForEach(s => idLookUpList.Add(new RecordKeyLookup("PERSON", s, false)));

                var personGuidLookupList = await DataReader.SelectAsync(idLookUpList.ToArray());
                foreach (var o in personGuidLookupList)
                {
                    if (o.Value != null && !string.IsNullOrEmpty(o.Value.Guid))
                    {
                        personGuids.Add(o.Value.Guid);
                    }
                    else
                    {
                        var ex = new RepositoryException();
                        ex.AddError(new Domain.Entities.RepositoryError(string.Format("PERSON record '{0}' is missing a guid.", o.Key.Split('+')[1])));
                        throw ex;
                    }
                }
              }
               return new Tuple<IEnumerable<string>, int>(personGuids, totalCount);
          }

        /// <summary>
        /// Get a list of guids associated with organizations
        /// </summary>
        /// <param name="Offset">Paging offset</param>
        /// <param name="Limit">Paging limit</param>
        /// <param name="role">Specific role of a organization</param>
        /// <param name="credentialType">Credential type of colleagueId</param>
        /// <param name="credentialValue">Specific value of the credential to be evaluated</param>
        /// <returns>List of organization guids associated with faculty</returns>
        public async Task<Tuple<IEnumerable<string>, int>> GetFilteredOrganizationGuidsAsync(int offset, int limit,
            string role, string credentialType, string credentialValue)
        {
            var orgGuids = new List<string>();
            string criteria = "WITH PERSON.CORP.INDICATOR = 'Y' ";
            if (!string.IsNullOrEmpty(credentialValue))
            {
                criteria = string.Concat(criteria, " AND WITH ID = '", credentialValue, "'");
            }

            int totalCount = 0;
            //this is all the ids from Person that could be orgs, will also include institutions
            var orgIds = (await DataReader.SelectAsync("PERSON", criteria)).ToList();

            //list of institutions
            var institutionIds = (await DataReader.SelectAsync("INSTITUTIONS", string.Empty)).ToList();

            //remove the existing institutions
            orgIds.RemoveAll(o => institutionIds.Exists(i => i.Equals(o, StringComparison.OrdinalIgnoreCase)));

            List<string> roleOrgList = new List<string>();

            //time to check for other role filters
            //build the orgId list for the role in the filter
            if (!string.IsNullOrEmpty(role) &&
                (role.Equals("partner", StringComparison.OrdinalIgnoreCase) ||
                role.Equals("affiliate", StringComparison.OrdinalIgnoreCase) ||
                role.Equals("constituent", StringComparison.OrdinalIgnoreCase) ||
                role.Equals("vendor", StringComparison.OrdinalIgnoreCase)))
            {
                string roleCriteria = string.Empty;

                switch (role.ToLower())
                {
                    case "partner":
                        roleCriteria = "WITH PER.INTG.ROLE = 'Partner'";
                        break;
                    case "affiliate":
                        roleCriteria = "WITH PER.INTG.ROLE = 'Affiliate'";
                        break;
                    case "constituent":
                        roleCriteria = "WITH PER.INTG.ROLE = 'Constituent'";
                        break;
                    case "vendor":
                        roleCriteria = "WITH PER.INTG.ROLE = 'Vendor'";
                        break;
                }

                roleOrgList = (await DataReader.SelectAsync("PERSON.INTG", roleCriteria)).ToList();
            }

            //we have a list of orgs without institutions
            //we have a list or per intg ids with vendor role
            //we have to get a list or orgs without insitutions that has whre.used vendor
            //take those three list and find the correct intersections and we now have a full list of orgs with vendor where.used or per.intg role
            if (role.Equals("vendor", StringComparison.OrdinalIgnoreCase))
            {
                criteria = string.Concat(criteria, " AND WITH WHERE.USED = 'VENDORS'");
                
                //get orgs that have vendor where used and remove institutions
                var orgIdsUsedVendor = (await DataReader.SelectAsync("PERSON", criteria)).ToList();
                orgIdsUsedVendor.RemoveAll(o => institutionIds.Exists(i => i.Equals(o, StringComparison.OrdinalIgnoreCase)));

                //orgs that are vendors from either per.intg or where.sed will get added in this list
                var vendorOrgResultList = new List<string>();

                if (roleOrgList.Any())
                {
                    //intersect the list of orgs and perintg with role of vendor
                    var orgsWithVendorPerIntgRole = orgIds.Intersect(roleOrgList);

                    //add intersect result to result list
                    vendorOrgResultList.AddRange(orgsWithVendorPerIntgRole);
                }

                //add list of orgs that have where.used vendor
                vendorOrgResultList.AddRange(orgIdsUsedVendor);
                //select distinct from the combinded list, repeats could happen
                orgIds = vendorOrgResultList.Distinct().ToList();
            }
            else if (roleOrgList.Any())  
            {
                //this means it was something other than the vendor role so just remove anything from the filtered list 
                //if it isnt in the list of roleOrgids
                orgIds.RemoveAll(o => !roleOrgList.Exists(r => r.Equals(o, StringComparison.OrdinalIgnoreCase)));
            }
            else if (!roleOrgList.Any() && (role.Equals("partner", StringComparison.OrdinalIgnoreCase) ||
                role.Equals("affiliate", StringComparison.OrdinalIgnoreCase) ||
                role.Equals("constituent", StringComparison.OrdinalIgnoreCase)))
            {
                //this means nothing matched the roles so return nothing
                orgIds = new List<string>();
            }

            //return to array form the list now that insitutions have been removed
            var filteredOrgIds = orgIds.ToArray();

            totalCount = filteredOrgIds.Count();

            Array.Sort(filteredOrgIds);

            var subList = filteredOrgIds.Skip(offset).Take(limit).ToArray();

            var idLookUpList = new List<RecordKeyLookup>();
            subList.ForEach(s => idLookUpList.Add(new RecordKeyLookup("PERSON", s, false)));

            var orgGuidLookupList = await DataReader.SelectAsync(idLookUpList.ToArray());

            orgGuidLookupList.ForEach(o => orgGuids.Add(o.Value.Guid));
            
            return new Tuple<IEnumerable<string>, int>(orgGuids, totalCount);
        }

          #endregion

          #region Search by Name
          /// <summary>
          /// We are querying the SORT.NAME, a computed column on PERSON, an upper-cased field which appends last first middle name
          /// Second query against partial last name and nickname, in the format entered by the user.
          /// </summary>
          /// <param name="firstName"></param>
          /// <param name="middleName"></param>
          /// <param name="lastName"></param>
          /// <returns>list of Ids</returns>
          public async Task<IEnumerable<string>> SearchByNameAsync(string lastName, string firstName = null, string middleName = null)
          {
               var watch = new Stopwatch();
               watch.Start();

               if (lastName == null || lastName.Trim().Count() < 2)
               {
                    throw new ArgumentNullException("lastName", "Supplied last name must be at least two characters");
               }

               // Trim spaces from each name part
               lastName = lastName.Trim();
               firstName = (firstName != null) ? firstName = firstName.Trim() : null;
               middleName = (middleName != null) ? middleName = middleName.Trim() : null;

               // Call transaction that returns search string
               var lookupStringRequest = new GetPersonLookupStringRequest();
               lookupStringRequest.SearchString = lastName + "," + firstName + " " + middleName;
               var response = await transactionInvoker.ExecuteAsync<GetPersonLookupStringRequest, GetPersonLookupStringResponse>(lookupStringRequest);

               var persons = new string[] { };
               string searchString = string.Empty;

               if (string.IsNullOrEmpty(response.ErrorMessage))
               {
                    // Transaction returns something like ;PARTIAL.NAME.INDEX SMITH_BL. Parse out into valid query clause: WITH PARTIAL.NAME.INDEX EQ SMITH_BL
                    var searchArray = (response.IndexString.Replace(";", string.Empty)).Split(' ');
                    searchString = "WITH " + searchArray.ElementAt(0) + " EQ " + searchArray.ElementAt(1);

                    // Select on person
                    persons = await DataReader.SelectAsync("PERSON", searchString);
               }

               watch.Stop();
               logger.Info("    STEPX.1.1 Select PERSON " + searchString + " ... completed in " + watch.ElapsedMilliseconds.ToString());
               if (persons != null)
               {
                    logger.Info("    STEPX.1.1 Select found " + persons.Count() + " PERSONS with search string " + searchString);
               }

               return persons;
          }

        /// <summary>
        /// Retrieves the information for PersonBase for ids provided,
        /// and the matching PersonBases if a first and last name are provided.  
        /// In the latter case, a middle name is optional.
        /// Matching is done by partial name; i.e., 'Bro' will match 'Brown' or 'Brodie'. 
        /// Capitalization is ignored.
        /// </summary>
        /// <remarks>the following keyword input is legal
        /// <list type="bullet">
        /// <item>a Colleague id.  Short ids will be zero-padded.</item>
        /// <item>First Last</item>
        /// <item>First Middle Last</item>
        /// <item>Last, First</item>
        /// <item>Last, First Middle</item>
        /// </list>
        /// </remarks>
        /// <param name="ids">Enumeration of ids for which to retrieve records</param>
        /// <param name="keyword">either a Person ID or a first and last name.  A middle name is optional.</param>
        /// <param name="useCache">True if you want to use cached data</param>
        /// <returns>An enumeration of <see cref="PersonBase">PersonBase</see> information</returns>
        public async Task<IEnumerable<PersonBase>> SearchByIdsOrNamesAsync(IEnumerable<string> ids, string keyword, bool useCache = true)
        {
            if (!ids.Any() && string.IsNullOrEmpty(keyword))
            {
                throw new ArgumentNullException("keyword");
            }

            List<PersonBase> personBases = new List<PersonBase>();
            List<string> personIds = new List<string>();

            personIds.AddRange(ids);

            // If search string is a numeric ID, add only that ID to the list 
            int personId;
            bool isId = int.TryParse(keyword, out personId);
            if (isId)
            {
                string id = keyword.PadLeft(7, '0');
                personIds.Add(id);
            }
            // If search string is alphanumeric, parse names from query and add matching persons to list
            else
            {
                string lastName = "";
                string firstName = "";
                string middleName = "";
                ParseNames(keyword, ref lastName, ref firstName, ref middleName);
                if (string.IsNullOrEmpty(firstName))
                {
                    throw new ArgumentException("Either an id or a first and last name must be supplied.");
                }
                personIds.AddRange(await SearchByNameAsync(lastName, firstName, middleName));
            }
            // If there are no persons, return the empty set
            if (!personIds.Any()) return personBases;

            // Filter out corporations from the set of persons
            personIds = (await FilterOutCorporations(personIds)).ToList();

            // Build PersonBase objects with full names
            personBases = await GetBaseAsync<PersonBase>(personIds,
                persons =>
                {
                    var personBaseList = new List<PersonBase>();
                    foreach (var person in persons)
                    {

                        personBaseList.Add(new PersonBase(person.Recordkey, person.LastName, person.PrivacyFlag)
                        {
                            MiddleName = person.MiddleName,
                            FirstName = person.FirstName
                        });

                    }
                    return personBaseList;
                }, useCache);

            return personBases;
        }

        private void ParseNames(string criteria, ref string lastName, ref string firstName, ref string middleName)
        {
            // Regular expression for all punctuation and numbers to remove from name string
            Regex regexNotPunc = new Regex(@"[!-&(-,.-@[-`{-~]");
            Regex regexNotSpace = new Regex(@"\s");

            var nameStrings = criteria.Split(',');
            // If there was a comma, set the first item to last name
            if (nameStrings.Count() > 1)
            {
                lastName = nameStrings.ElementAt(0).Trim();
                if (nameStrings.Count() >= 2)
                {
                    // parse the two items after the comma using a space. Ignore anything else
                    var nameStrings2 = nameStrings.ElementAt(1).Trim().Split(' ');
                    if (nameStrings2.Count() >= 1) { firstName = nameStrings2.ElementAt(0).Trim(); }
                    if (nameStrings2.Count() >= 2) { middleName = nameStrings2.ElementAt(1).Trim(); }
                }
            }
            else
            {
                // Parse entry using spaces, assume entered (last) or (first last) or (first middle last). 
                // Blank values don't hurt anything.
                nameStrings = criteria.Split(' ');
                switch (nameStrings.Count())
                {
                    case 1:
                        lastName = nameStrings.ElementAt(0).Trim();
                        break;
                    case 2:
                        firstName = nameStrings.ElementAt(0).Trim();
                        lastName = nameStrings.ElementAt(1).Trim();
                        break;
                    default:
                        firstName = nameStrings.ElementAt(0).Trim();
                        middleName = nameStrings.ElementAt(1).Trim();
                        lastName = nameStrings.ElementAt(2).Trim();
                        break;
                }
            }
            // Remove characters that won't make sense for each name part, including all punctuation and numbers 
            if (lastName != null)
            {
                lastName = regexNotPunc.Replace(lastName, "");
                lastName = regexNotSpace.Replace(lastName, "");
            }
            if (firstName != null)
            {
                firstName = regexNotPunc.Replace(firstName, "");
                firstName = regexNotSpace.Replace(firstName, "");
            }
            if (middleName != null)
            {
                middleName = regexNotPunc.Replace(middleName, "");
                middleName = regexNotSpace.Replace(middleName, "");
            }

        }

          /// <summary>
          /// Takes a colleague entity name and a list of Ids and filters the list of Ids
          /// by selecting against the specified entity.
          /// </summary>
          /// <param name="entityName"></param>
          /// <param name="ids"></param>
          /// <returns></returns>
          public async Task<IEnumerable<string>> FilterByEntityAsync(string entityName, IEnumerable<string> ids, string limitingCriteria = null)
          {
               // If none were found on person, return an empty list
               if (ids == null || ids.Count() == 0)
               {
                    return new List<string>();
               }

               // If Ids WERE found on person, limit the list by those that exist on entityName
               var limitedIds = await DataReader.SelectAsync(entityName, ids.ToArray(), limitingCriteria);

               if (limitedIds == null) { return new List<string>(); }

               return limitedIds.ToList();
          }

        /// <summary>
        /// Filters corporations out of a list of person IDs
        /// </summary>
        /// <param name="personIds"></param>
        /// <returns></returns>
        private async Task<IEnumerable<string>> FilterOutCorporations(IEnumerable<string> personIds)
        {
            if (personIds == null || !personIds.Any()) return personIds;

            var corpIds = new List<string>(personIds);
            corpIds = (await FilterByEntityAsync("CORP.FOUNDS", corpIds)).ToList();
            if (corpIds != null && corpIds.Any())
            {
                personIds = personIds.Where(p => !corpIds.Contains(p));
            }
            return personIds;
        }

          private string QuoteDelimitIds(IEnumerable<string> stringList)
          {
               if (stringList == null || stringList.Select(i => (!string.IsNullOrEmpty(i))).Count() == 0)
               {
                    return null;
               }
               else
               {
                    return Quote + (string.Join(" ", stringList.ToArray())).Replace(" ", Quote + " " + Quote) + Quote;
               }
          }

          #endregion

          #region Get Person Integration Data

          /// <summary>
          /// Get person addresses, email addresses and phones used for integration.
          /// </summary>
          /// <param name="personId">Person's Colleague ID</param>
          /// <param name="emailAddresses">List of <see cref="EmailAddress"> email addresses</see></param>
          /// <param name="phones">List of <see cref="Phone"> phones</see></param>
          /// <param name="addresses">List of <see cref="Address">addresses</see></param>
          /// <returns>Boolean where true is success and false otherwise</returns>
          public async Task<Tuple<List<EmailAddress>, List<Phone>, List<Domain.Base.Entities.Address>, bool>> GetPersonIntegrationDataAsync(string personId)
          {
               List<Domain.Base.Entities.Address> addresses = new List<Domain.Base.Entities.Address>();
               List<EmailAddress> emailAddresses = new List<EmailAddress>();
               List<Phone> phones = new List<Phone>();

               var response = await transactionInvoker.ExecuteAsync<GetPersonAddressesRequest, GetPersonAddressesResponse>(
                   new GetPersonAddressesRequest() { PersonId = personId });

               if (response.ErrorMessages.Count() > 0)
               {
                    var errorMessage = "Error(s) occurred retrieving person data:";
                    errorMessage += string.Join(Environment.NewLine, response.ErrorMessages);
                    logger.Error(errorMessage.ToString());
                    throw new InvalidOperationException("Error occurred retrieving person data");
               }
               else
               {
                    if (response.PersonPhones != null && response.PersonPhones.Count() > 0)
                    {
                         // create the phone entities
                         foreach (var phone in response.PersonPhones)
                         {
                              phones.Add(new Phone(phone.PhoneNumber, phone.PhoneType, phone.PhoneExtension));
                         }
                    }

                    if (response.PersonEmailAddresses != null && response.PersonEmailAddresses.Count() > 0)
                    {
                         // create the email address entities
                         foreach (var emailAddress in response.PersonEmailAddresses)
                         {
                              emailAddresses.Add(new EmailAddress(emailAddress.EmailAddressValue, emailAddress.EmailAddressType));
                         }
                    }

                    if (response.PersonAddresses != null && response.PersonAddresses.Count() > 0)
                    {
                         // create the address entities
                         foreach (var address in response.PersonAddresses)
                         {
                              var addressEntity = new Domain.Base.Entities.Address();
                              addressEntity.Type = address.AddressType;
                              addressEntity.City = address.AddressCity;
                              addressEntity.State = address.AddressRegion;
                              addressEntity.PostalCode = address.AddressPostalCode;
                              addressEntity.Country = address.AddressCountry;
                              addressEntity.County = address.AddressCounty;
                              var addressLines = new List<string>();
                              addressLines.Add(address.AddressStreet1);
                              if (!string.IsNullOrEmpty(address.AddressStreet2)) addressLines.Add(address.AddressStreet2);
                              if (!string.IsNullOrEmpty(address.AddressStreet3)) addressLines.Add(address.AddressStreet3);
                              addressEntity.AddressLines = addressLines;
                              addresses.Add(addressEntity);
                         }
                    }
               }

               return new Tuple<List<EmailAddress>, List<Phone>, List<Domain.Base.Entities.Address>, bool>(emailAddresses, phones, addresses, true);
          }

          /// <summary>
          /// Get person addresses, email addresses and phones used for integration.
          /// </summary>
          /// <param name="personId">Person's Colleague ID</param>
          /// <param name="emailAddresses">List of <see cref="EmailAddress"> email addresses</see></param>
          /// <param name="phones">List of <see cref="Phone"> phones</see></param>
          /// <param name="addresses">List of <see cref="Address">addresses</see></param>
          /// <returns>Boolean where true is success and false otherwise</returns>
          public async Task<Tuple<List<EmailAddress>, List<Phone>, List<Domain.Base.Entities.Address>, List<Domain.Base.Entities.SocialMedia>, bool>> GetPersonIntegrationData2Async(string personId)
          {
               List<Domain.Base.Entities.Address> addresses = new List<Domain.Base.Entities.Address>();
               List<EmailAddress> emailAddresses = new List<EmailAddress>();
               List<Phone> phones = new List<Phone>();
               List<SocialMedia> socialMedias = new List<SocialMedia>();

               var response = await transactionInvoker.ExecuteAsync<GetPersonContactInfoRequest, GetPersonContactInfoResponse>(
                   new GetPersonContactInfoRequest() { PersonId = personId });

               if (response.PersonErrorInfo != null && response.PersonErrorInfo.Any())
               {
                    var errorMessage = "Error(s) occurred retrieving person data:";
                    foreach (var message in response.PersonErrorInfo)
                    {
                         errorMessage += string.Join(Environment.NewLine, message.ErrorMessages);
                    }
                    logger.Error(errorMessage.ToString());
                    throw new InvalidOperationException("Error occurred retrieving person data");
               }
               else
               {
                    if (response.PersonPhoneInfo != null && response.PersonPhoneInfo.Any())
                    {
                         // create the phone entities
                         foreach (var phone in response.PersonPhoneInfo)
                         {
                              phones.Add(new Phone(phone.PhoneNumber, phone.PhoneType, phone.PhoneExtension)
                              {
                                   CountryCallingCode = phone.PhoneCallingCode,
                            IsPreferred = (!string.IsNullOrEmpty(phone.PhonePref) ? phone.PhonePref.Equals("Y", StringComparison.OrdinalIgnoreCase) : false)
                              });
                         }
                    }

                    if (response.PersonSocialMediaInfo != null && response.PersonSocialMediaInfo.Any())
                    {
                         // create the email address entities
                         foreach (var socialMedia in response.PersonSocialMediaInfo)
                         {
                              var preferred = false;
                              if (!string.IsNullOrEmpty(socialMedia.SocialMediaPref))
                              {
                                   preferred = socialMedia.SocialMediaPref.Equals("y", StringComparison.OrdinalIgnoreCase);
                              }

                              socialMedias.Add(new SocialMedia(socialMedia.SocialMediaType, socialMedia.SocialMediaHandle, preferred));
                         }
                    }

                    var personDataContract = await GetPersonContractAsync(personId, false);

                    if (personDataContract.PersonEmailAddresses != null && personDataContract.PersonEmailAddresses.Any())
                    {
                         for (int i = 0; i < personDataContract.PersonEmailAddresses.Count; i++)
                         {
                              try
                              {
                                   var emailAddress = personDataContract.PersonEmailAddresses[i];
                                   var emailAddressType = personDataContract.PersonEmailTypes.Count > i
                                       ? personDataContract.PersonEmailTypes[i]
                                       : null;
                                   var emailAddressPreferred = personDataContract.PersonPreferredEmail.Count > i
                                       ? personDataContract.PersonPreferredEmail[i]
                                       : string.Empty;

                                   var emailToAdd = new EmailAddress(emailAddress, emailAddressType)
                                   {
                                        IsPreferred = emailAddressPreferred.Equals("y", StringComparison.OrdinalIgnoreCase)
                                   };

                                   emailAddresses.Add(emailToAdd);
                              }
                              catch (Exception exception)
                              {
                                   logger.Error(exception, string.Concat("Could not load email address for person id", personDataContract.RecordGuid));
                              }
                         }
                    }

                    var addressIds = personDataContract.PersonAddresses;

                    // Current Addresses First
                    if (addressIds.Any())
                    {
                         var addressDataContracts = await GetPersonAddressContractsAsync(addressIds);
                         if (addressDataContracts.Any())
                         {
                              // create the address entities
                              foreach (var address in addressDataContracts)
                              {
                                   var addressEntity = new Domain.Base.Entities.Address();
                                   addressEntity.Guid = address.RecordGuid;
                                   addressEntity.City = address.City;
                                   addressEntity.State = address.State;
                                   addressEntity.PostalCode = address.Zip;
                                   addressEntity.Country = address.Country;
                                   addressEntity.County = address.County;
                                   addressEntity.AddressLines = address.AddressLines;
                                   // Find Addrel Association in Person contract
                                   var assocEntity = personDataContract.PseasonEntityAssociation.FirstOrDefault(pa => address.Recordkey == pa.PersonAddressesAssocMember);
                                   if (assocEntity != null)
                                   {
                                //addressEntity.TypeCode = assocEntity.AddrTypeAssocMember.Split(_SM).FirstOrDefault();
                                addressEntity.TypeCode = assocEntity.AddrTypeAssocMember;
                                        addressEntity.EffectiveStartDate = assocEntity.AddrEffectiveStartAssocMember;
                                        addressEntity.EffectiveEndDate = assocEntity.AddrEffectiveEndAssocMember;
                                        addressEntity.SeasonalDates = new List<AddressSeasonalDates>();
                                        if (!string.IsNullOrEmpty(assocEntity.AddrSeasonalStartAssocMember) && !string.IsNullOrEmpty(assocEntity.AddrSeasonalEndAssocMember))
                                        {
                                             // This could be subvalued so need to split on subvalue mark ASCII 252.
                                             string[] startDate = assocEntity.AddrSeasonalStartAssocMember.Split(_SM);
                                             string[] endDate = assocEntity.AddrSeasonalEndAssocMember.Split(_SM);
                                             for (int i = 0; i < startDate.Length; i++)
                                             {
                                                  try
                                                  {
                                                       // add in the address override phones into the person's list of phones
                                                       AddressSeasonalDates seasonalDates = new AddressSeasonalDates(startDate[i], endDate[i]);
                                                       addressEntity.SeasonalDates.Add(seasonalDates);
                                                  }
                                                  catch (Exception ex)
                                                  {
                                                       var error = "Person address seasonal start/end information is invalid. PersonId: " + personId;

                                                       // Log the original exception
                                                       logger.Error(ex.ToString());
                                                       logger.Info(error);
                                                  }
                                             }
                                        }
                                   }
                                   addressEntity.IsPreferredAddress = (address.Recordkey == personDataContract.PreferredAddress);
                                   addressEntity.IsPreferredResidence = (address.Recordkey == personDataContract.PreferredResidence);
                                   addressEntity.Status = "Current";
                                   addresses.Add(addressEntity);
                              }
                         }
                    }
               }

               return new Tuple<List<EmailAddress>, List<Phone>, List<Domain.Base.Entities.Address>, List<Domain.Base.Entities.SocialMedia>, bool>(emailAddresses, phones, addresses, socialMedias, true);
          }

          #endregion

     }
}
