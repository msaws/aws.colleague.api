﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.DataContracts;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Web.Http.Configuration;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.Base.Repositories
{
    /// <summary>
    /// Accesses Colleague for a person's contact information: Name, address, phones, emails.
    /// </summary>
    [RegisterType]
    public class OrganizationalPersonPositionRepository : BaseColleagueRepository, IOrganizationalPersonPositionRepository
    {
        private readonly int _readSize;
        private readonly string _orgIndicator;

        /// <summary>
        /// Constructor for person profile repository.
        /// </summary>
        /// <param name="cacheProvider"></param>
        /// <param name="transactionFactory"></param>
        /// <param name="logger"></param>
        public OrganizationalPersonPositionRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings apiSettings)
            : base(cacheProvider, transactionFactory, logger)
        {
            // Using level 1 cache time out value for data that rarely changes.
            CacheTimeout = Level1CacheTimeoutValue;
            _readSize = apiSettings != null && apiSettings.BulkReadSize > 0 ? apiSettings.BulkReadSize : 5000;
            _orgIndicator = "ORG";
        }

        /// <summary>
        /// Get the position assignments and relevant relationships for the given IDs
        /// </summary>
        /// <param name="ids">Organizational Person Position IDs</param>
        /// <returns>The Collection of Organizational Person Positions with the given IDs</returns>
        public async Task<IEnumerable<OrganizationalPersonPosition>> GetOrganizationalPersonPositionsByIdsAsync(IEnumerable<string> ids)
        {
            if (ids == null)
            {
                throw new ArgumentNullException("ids", "At least one id is required to get organizational person positions");
            }

            // Org Roles representing person positions
            var orgRoleRecords = await GetOrganizationalRoleRecordsForPersonPositionsAsync();

            //Read all OrgEntityRole (person positions at the UT level). Later will pare down to include only those with allowable ORG.ROLES based on role type
            var orgEntityRoleIds = ids.ToArray();
            List<OrgEntityRolePosition> orgEntityRoleRecords = await GetOrgEntityRoleRecordByIdsAsync(orgEntityRoleIds);

            var orgEntityRoleRelRecords = await GetOrgEntityRoleRelRecordsByOrgEntityRoleIdsAsync(orgEntityRoleIds);

            // Read ORG.ENTITY.ROLE one more time now that we have the related org entity role ids from the relationships. 
            // Keep these in a separate list since they are not the primary ones we are building.
            var relatedIds = orgEntityRoleRelRecords.Select(oerr => oerr.OerrelRelatedOerId).ToList();
            relatedIds.AddRange(orgEntityRoleRelRecords.Select(oerr => oerr.OerrelOerId));

            List<OrgEntityRolePosition> relatedOrgEntityRoleRecords = await GetOrgEntityRoleRecordByIdsAsync(relatedIds.Distinct().ToArray());

            return BuildOrganizationalPersonPosition(orgRoleRecords, orgEntityRoleRecords, orgEntityRoleRelRecords, relatedOrgEntityRoleRecords);
        }

        /// <summary>
        /// Get the position assignments for the specified person and relevant relationships
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<OrganizationalPersonPosition>> GetOrganizationalPersonPositionAsync(IEnumerable<string> personIds, IEnumerable<string> orgPersonPositionIds)
        {
            if (personIds == null && personIds.Count() == 0 && orgPersonPositionIds == null && orgPersonPositionIds.Count() == 0)
            {
                throw new ArgumentException("At least one piece of criteria is required to search for organizational person positions");
            }
            // Get the IDs for the ORG.ENTITY.ROLE with the given Person IDs
            string[] orgEntityRoleIds = new List<string>().ToArray();
            if (personIds != null && personIds.Count() > 0)
            {
                var orgEntityRoleCriteria = "WITH OER.PERSON EQ '?'";
                orgEntityRoleIds = await DataReader.SelectAsync("ORG.ENTITY.ROLE", orgEntityRoleCriteria, personIds.ToArray());
            }
            // Combine the list of person position IDs to create distinct list of person postion ids to get
            var orgPersonPositionIdsToGet = new List<string>();
            if (orgPersonPositionIds != null)
            {
                orgPersonPositionIdsToGet = orgPersonPositionIds.ToList();
            }
            if (orgEntityRoleIds != null)
            {
                orgPersonPositionIdsToGet.AddRange(orgEntityRoleIds.ToList());
            }
            if (orgPersonPositionIdsToGet == null || orgPersonPositionIdsToGet.Count() == 0)
            {
                // No person positions match this criteria
                return new List<OrganizationalPersonPosition>();
            }
            return await GetOrganizationalPersonPositionsByIdsAsync(orgPersonPositionIdsToGet.Distinct());
        }

        private IEnumerable<OrganizationalPersonPosition> BuildOrganizationalPersonPosition(IEnumerable<OrgRole> orgRoleRecords, IEnumerable<OrgEntityRolePosition> orgEntityRoleRecords, IEnumerable<OrgEntityRoleRel> orgEntityRoleRelRecords, IEnumerable<OrgEntityRolePosition> relatedOrgEntityRoleRecords)
        {
            var orgPersonPositions = new List<OrganizationalPersonPosition>();
            // Create a dictionary of the organizational roles, selected using valid role types
            var orgRoleRecordDict = orgRoleRecords.ToDictionary(r => r.Recordkey, r => r);
            // Process the selected orgEntityRole
            foreach (var orgEntityRoleRecord in orgEntityRoleRecords)
            {
                if (orgRoleRecordDict.ContainsKey(orgEntityRoleRecord.OerOrgRole))
                {
                    // Create the organizational person position
                    var orgPersonPosition = new OrganizationalPersonPosition(orgEntityRoleRecord.Recordkey, orgEntityRoleRecord.OerPerson, orgEntityRoleRecord.OerOrgRole, orgRoleRecordDict[orgEntityRoleRecord.OerOrgRole].OroleTitle);
                    // Add any relationships where this orgEntityRole is a participant
                    var orgEntityRoleRelationships = orgEntityRoleRelRecords
                        .Where(orrr => orrr.OerrelOerId == orgEntityRoleRecord.Recordkey || orrr.OerrelRelatedOerId == orgEntityRoleRecord.Recordkey)
                        .ToList();
                    if (orgEntityRoleRelationships != null)
                    {
                        foreach (var relation in orgEntityRoleRelationships)
                        {
                            try
                            {
                                var primaryOrgEntityRoleRecord = relatedOrgEntityRoleRecords.Where(roerr => roerr.Recordkey == relation.OerrelOerId).First();
                                var relatedOrgEntityRoleRecord = relatedOrgEntityRoleRecords.Where(roerr => roerr.Recordkey == relation.OerrelRelatedOerId).First();
                                if (orgRoleRecordDict.ContainsKey(relatedOrgEntityRoleRecord.OerOrgRole))
                                {
                                    if (orgRoleRecordDict.ContainsKey(primaryOrgEntityRoleRecord.OerOrgRole))
                                    {
                                        var relationshipToAdd = new OrganizationalRelationship(
                                            relation.Recordkey,
                                            relation.OerrelOerId,
                                            primaryOrgEntityRoleRecord.OerPerson,
                                            primaryOrgEntityRoleRecord.OerOrgRole,
                                            orgRoleRecordDict[primaryOrgEntityRoleRecord.OerOrgRole].OroleTitle,
                                            relation.OerrelRelatedOerId,
                                            relatedOrgEntityRoleRecord.OerPerson,
                                            relatedOrgEntityRoleRecord.OerOrgRole,
                                            orgRoleRecordDict[relatedOrgEntityRoleRecord.OerOrgRole].OroleTitle,
                                            relation.OerrelRelationshipCategory
                                            );
                                        orgPersonPosition.AddRelationship(relationshipToAdd);
                                    }
                                    else
                                    {
                                        LogDataError("ORG.ENTITY.ROLE.REL", relation.Recordkey, relation, null, "Could not build organizational relationship " + relation.Recordkey + " with person " + primaryOrgEntityRoleRecord.OerPerson + " because primary role " + primaryOrgEntityRoleRecord.OerOrgRole + " does not have special processing code ORG.");
                                    }
                                }
                                else
                                {
                                    LogDataError("ORG.ENTITY.ROLE.REL", relation.Recordkey, relation, null, "Could not build organizational relationship " + relation.Recordkey + " with person " + relatedOrgEntityRoleRecord.OerPerson + " because related role " + relatedOrgEntityRoleRecord.OerOrgRole + " does not have special processing code ORG.");
                                }
                            }
                            catch (Exception ex)
                            {
                                LogDataError("ORG.ENTITY.ROLE.REL", relation.Recordkey, relation, ex, "Could not build organizational relationship " + relation.Recordkey + " for person " + orgEntityRoleRecord.OerPerson + ".");
                            }
                        }
                        orgPersonPositions.Add(orgPersonPosition);
                    }
                }
            }
            return orgPersonPositions.AsEnumerable();
        }

        /// <summary>
        /// Return only the list of roles types relevant to Organizational Members
        /// </summary>
        /// <returns></returns>
        private async Task<IEnumerable<string>> GetOrganizationalRoleTypes()
        {
            var roleTypes = await GetOrAddToCacheAsync<ApplValcodes>("OrganizationalRoleTypes",
                async () =>
                {
                    ApplValcodes roleTypesTable = await DataReader.ReadRecordAsync<ApplValcodes>("UT.VALCODES", "ORG.ROLE.TYPES");
                    if (roleTypesTable == null)
                    {
                        var errorMessage = "Unable to access ORG.ROLE.TYPES valcode table.";
                        logger.Error(errorMessage);
                        throw new Exception(errorMessage);
                    }
                    return roleTypesTable;
                }, Level1CacheTimeoutValue);

            var orgRoleTypes = roleTypes.ValsEntityAssociation.Where(v => v.ValActionCode1AssocMember == _orgIndicator).Select(v => v.ValInternalCodeAssocMember).ToList();
            if (orgRoleTypes.Count() == 0)
            {
                logger.Error("No ORG.ROLE.TYPES found with Action Code 1 of ORG, Cannot build organizational relationships.");
            }
            return orgRoleTypes;
        }

        /// <summary>
        /// Gets the OrgRole records that represent person positions
        /// </summary>
        /// <returns>OrgRole records representing person positions</returns>
        private async Task<IEnumerable<OrgRole>> GetOrganizationalRoleRecordsForPersonPositionsAsync()
        {

            //Read async org role types
            var roleTypes = await GetOrganizationalRoleTypes();

            //Read roles with any "ORG" role type
            var criteria = "WITH OROLE.TYPE EQ '?'";
            var selectedIds = await DataReader.SelectAsync("ORG.ROLE", criteria, roleTypes.ToArray());
            List<OrgRole> orgRoleRecords = new List<OrgRole>();
            if (selectedIds != null && selectedIds.Count() > 0)
            {
                for (int i = 0; i < selectedIds.Count(); i += _readSize)
                {
                    var subList = selectedIds.Skip(i).Take(_readSize).ToArray();
                    var bulkRecords = await DataReader.BulkReadRecordAsync<OrgRole>(subList);
                    if (bulkRecords != null)
                    {
                        orgRoleRecords.AddRange(bulkRecords);
                    }
                }
            }
            return orgRoleRecords;
        }

        /// <summary>
        /// Gets OrgEntityRoleRel records by OrgEntityRole ID on either side of the relationship.
        /// </summary>
        /// <param name="orgEntityRoleIds">OrgEntityRole IDs to retrieve relationships for</param>
        /// <returns>OrgEntityRoleRel records with the given OrgEntityRole IDs</returns>
        private async Task<IEnumerable<OrgEntityRoleRel>> GetOrgEntityRoleRelRecordsByOrgEntityRoleIdsAsync(string[] orgEntityRoleIds)
        {
            //read async OrgEntityRoleRel (org entity role relationships for primary relationship participant)
            var criteria = "WITH OERREL.OER.ID EQ '?'";
            var selectedPrimaryIds = new List<string>().ToArray();
            selectedPrimaryIds = await DataReader.SelectAsync("ORG.ENTITY.ROLE.REL", criteria, orgEntityRoleIds);
            var relatedCriteria = "WITH OERREL.RELATED.OER.ID EQ '?'";
            var selectedRelatedIds = new List<string>().ToArray();
            selectedRelatedIds = await DataReader.SelectAsync("ORG.ENTITY.ROLE.REL", relatedCriteria, orgEntityRoleIds);
            var combinedIds = selectedPrimaryIds.Concat(selectedRelatedIds).Distinct();
            List<OrgEntityRoleRel> orgEntityRoleRelRecords = new List<OrgEntityRoleRel>();
            if (combinedIds != null && combinedIds.Count() > 0)
            {
                for (int i = 0; i < combinedIds.Count(); i += _readSize)
                {
                    var subList = combinedIds.Skip(i).Take(_readSize).ToArray();
                    var bulkRecords = await DataReader.BulkReadRecordAsync<OrgEntityRoleRel>(subList);
                    if (bulkRecords != null)
                    {
                        orgEntityRoleRelRecords.AddRange(bulkRecords);
                    }
                }
            }

            return orgEntityRoleRelRecords;

        }

        /// <summary>
        /// Get OrgEntityRole records by IDs
        /// </summary>
        /// <param name="ids">OrgEntityRole record keys</param>
        /// <returns>Last of OrgEntityRole records matching the IDs given</returns>
        private async Task<List<OrgEntityRolePosition>> GetOrgEntityRoleRecordByIdsAsync(string[] ids)
        {
            List<OrgEntityRolePosition> relatedOrgEntityRoleRecords = new List<OrgEntityRolePosition>();
            if (ids != null && ids.Count() > 0)
            {
                for (int i = 0; i < ids.Count(); i += _readSize)
                {
                    var subList = ids.Skip(i).Take(_readSize).ToArray();
                    var bulkRecords = await DataReader.BulkReadRecordAsync<OrgEntityRolePosition>(subList);
                    if (bulkRecords != null)
                    {
                        relatedOrgEntityRoleRecords.AddRange(bulkRecords);
                    }
                }
            }

            return relatedOrgEntityRoleRecords;
        }

    }
}
