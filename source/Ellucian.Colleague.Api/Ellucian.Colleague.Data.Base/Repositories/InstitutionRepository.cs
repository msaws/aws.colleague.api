﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Data.Base.Transactions;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.DataContracts;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Dmi.Runtime;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Web.Http.Configuration;
using Ellucian.Web.Utility;
using slf4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.Base.Repositories
{
    [RegisterType]
    public class InstitutionRepository : BaseColleagueRepository, IInstitutionRepository
    {
        // Sets the maximum number of records to bulk read at one time
        readonly int readSize;

        public InstitutionRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings apiSettings)
            : base(cacheProvider, transactionFactory, logger)
        {
            // Using level 1 cache time out value for data that rarely changes.
            CacheTimeout = Level1CacheTimeoutValue;

            this.readSize = ((apiSettings != null) && (apiSettings.BulkReadSize > 0)) ? apiSettings.BulkReadSize : 5000;
        }
        /// <summary>
        /// Get a list of all institutions
        /// </summary>
        /// <returns>Institution Objects</returns>
        public IEnumerable<Institution> Get()
        {
            var institutions = GetOrAddToCache<IEnumerable<Institution>>("AllInstitutions",
                () =>
                {
                    // Get Special Processing on Institution Type
                    var types = GetValcode<InstitutionType>("CORE", "INST.TYPES", ii => new InstitutionType(ii.ValInternalCodeAssocMember, ii.ValExternalRepresentationAssocMember, ii.ValActionCode1AssocMember));

                    // Get Institutions if not in cache
                    var instList = new List<Institution>();
                    var institutionData = new List<Institutions>();
                    var personData = new List<Ellucian.Colleague.Data.Base.DataContracts.Person>();
                    var addressesData = new List<Ellucian.Colleague.Data.Base.DataContracts.Address>();
                    var coreDefaultData = GetDefaults();
                    var faSystemParams = GetSystemParameters();
                    var institutionIds = DataReader.Select("INSTITUTIONS", "WITH INST.TYPE NE ''");
                    for (int x = 0; x < institutionIds.Count(); x += readSize)
                    {
                        var subList = institutionIds.Skip(x).Take(readSize).ToArray();
                        var bulkInstitutionData = DataReader.BulkReadRecord<Institutions>("INSTITUTIONS", subList);
                        var bulkPersonData = DataReader.BulkReadRecord<Ellucian.Colleague.Data.Base.DataContracts.Person>("PERSON", bulkInstitutionData.Select(p => p.Recordkey).Distinct().ToArray());
                        var bulkAddressesData = DataReader.BulkReadRecord<Ellucian.Colleague.Data.Base.DataContracts.Address>("ADDRESS", bulkPersonData.Select(p => p.PreferredAddress).Distinct().ToArray());
                        institutionData.AddRange(bulkInstitutionData);
                        personData.AddRange(bulkPersonData);
                        addressesData.AddRange(bulkAddressesData);
                    }

                    var personLookup = personData.ToLookup(x => x.Recordkey);
                    var addressLookup = addressesData.ToLookup(x => x.Recordkey);

                    foreach (var i in institutionData)
                    {
                        try
                        {
                            InstType instType;
                            var codeAssoc = "";
                            foreach (var type in types)
                            {
                                if (type.Code == i.InstType)
                                {
                                    codeAssoc = type.Category;
                                }
                            }
                            if (!string.IsNullOrEmpty(codeAssoc))
                            {
                                switch (codeAssoc)
                                {
                                    case ("C"):
                                        instType = InstType.College;
                                        break;
                                    case ("H"):
                                        instType = InstType.HighSchool;
                                        break;
                                    default:
                                        instType = InstType.Unknown;
                                        break;
                                }
                                Institution inst = new Institution(i.Recordkey, instType);
                                inst.Ceeb = i.InstCeeb;
                            
                                // Update the Preferred Name and Address Data from Preferred Address
                                var person = personLookup[i.Recordkey].FirstOrDefault();
                                if (person != null)
                                {
                                    inst.Name = person.PreferredName;
                                    if (string.IsNullOrEmpty(inst.Name))
                                    {
                                        inst.Name = person.LastName;
                                    }

                                    var addressData = addressLookup[person.PreferredAddress].FirstOrDefault();
                                    if (addressData != null)
                                    {
                                        inst.City = addressData.City;
                                        inst.State = addressData.State;
                                    }
                                }
                                inst.IsHostInstitution = (i.Recordkey == coreDefaultData.DefaultHostCorpId);
                                if (inst.IsHostInstitution && faSystemParams != null)
                                {
                                    inst.FinancialAidInstitutionName = faSystemParams.FspInstitutionName;
                                }

                                instList.Add(inst);
                            }
                        }
                        catch (Exception ex)
                        {
                            LogDataError("Institutions", i.Recordkey, i, ex);
                        }
                    }
                    return instList;
                }
            );
            return institutions;
        }

        /// <summary>
        /// Gets all institutions
        /// </summary>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Institution>> GetAllInstitutionsAsync(bool bypassCache = false)
        {
            List<Institution> institutions = new List<Institution>();

            if (bypassCache)
            {
                institutions = (await GetInstitutionsAsync()).ToList();
                await AddOrUpdateCacheAsync<IEnumerable<Institution>>("AllInstitutions", institutions);
            }
            else
            {
                institutions = (await GetOrAddToCacheAsync<IEnumerable<Institution>>("AllInstitutions", 
                    async () => 
                    {
                        return await GetInstitutionsAsync();
                    })).ToList();
            }
            return institutions;
        }        

        /// <summary>
        /// Gets institutions
        /// </summary>
        /// <returns></returns>
        private async Task<IEnumerable<Institution>> GetInstitutionsAsync()
        {
            // Get Special Processing on Institution Type
            var types = GetValcode<InstitutionType>("CORE", "INST.TYPES", ii => new InstitutionType(ii.ValInternalCodeAssocMember, ii.ValExternalRepresentationAssocMember, ii.ValActionCode1AssocMember));

            // Get Institutions if not in cache
            var instList = new List<Institution>();
            var institutionData = new List<Institutions>();
            var personData = new List<Ellucian.Colleague.Data.Base.DataContracts.Person>();
            var addressesData = new List<Ellucian.Colleague.Data.Base.DataContracts.Address>();
            var coreDefaultData = GetDefaults();
            var faSystemParams = GetSystemParameters();
            var institutionIds = await DataReader.SelectAsync("INSTITUTIONS", "WITH INST.TYPE NE ''");
            for (int x = 0; x < institutionIds.Count(); x += readSize)
            {
                var subList = institutionIds.Skip(x).Take(readSize).ToArray();
                var bulkInstitutionData = await DataReader.BulkReadRecordAsync<Institutions>("INSTITUTIONS", subList);
                var bulkPersonData = await DataReader.BulkReadRecordAsync<Ellucian.Colleague.Data.Base.DataContracts.Person>("PERSON", bulkInstitutionData.Select(p => p.Recordkey).Distinct().ToArray());
                var bulkAddressesData = await DataReader.BulkReadRecordAsync<Ellucian.Colleague.Data.Base.DataContracts.Address>("ADDRESS", bulkPersonData.Select(p => p.PreferredAddress).Distinct().ToArray());
                institutionData.AddRange(bulkInstitutionData);
                personData.AddRange(bulkPersonData);
                addressesData.AddRange(bulkAddressesData);
            }

            var personLookup = personData.ToLookup(x => x.Recordkey);
            var addressLookup = addressesData.ToLookup(x => x.Recordkey);

            foreach (var i in institutionData)
            {
                try
                {
                    InstType instType;
                    var codeAssoc = "";
                    foreach (var type in types)
                    {
                        if (type.Code == i.InstType)
                        {
                            codeAssoc = type.Category;
                        }
                    }
                    if (!string.IsNullOrEmpty(codeAssoc))
                    {
                        switch (codeAssoc)
                        {
                            case ("C"):
                                instType = InstType.College;
                                break;
                            case ("H"):
                                instType = InstType.HighSchool;
                                break;
                            default:
                                instType = InstType.Unknown;
                                break;
                        }
                        Institution inst = new Institution(i.Recordkey, instType);
                        inst.Ceeb = i.InstCeeb;

                        // Update the Preferred Name and Address Data from Preferred Address
                        var person = personLookup[i.Recordkey].FirstOrDefault();
                        if (person != null)
                        {
                            inst.Name = person.PreferredName;
                            if (string.IsNullOrEmpty(inst.Name))
                            {
                                inst.Name = person.LastName;
                            }

                            var addressData = addressLookup[person.PreferredAddress].FirstOrDefault();
                            if (addressData != null)
                            {
                                inst.City = addressData.City;
                                inst.State = addressData.State;
                            }
                        }
                        inst.IsHostInstitution = (i.Recordkey == coreDefaultData.DefaultHostCorpId);
                        if (inst.IsHostInstitution && faSystemParams != null)
                        {
                            inst.FinancialAidInstitutionName = faSystemParams.FspInstitutionName;
                        }

                        instList.Add(inst);
                    }
                }
                catch (Exception ex)
                {
                    LogDataError("Institutions", i.Recordkey, i, ex);
                }
            }
            return instList;
        }

        /// <summary>
        /// Get the Defaults from CORE to compare default institution Id
        /// </summary>
        /// <returns>Core Defaults</returns>
        private Base.DataContracts.Defaults GetDefaults()
        {
            return GetOrAddToCache<Data.Base.DataContracts.Defaults>("CoreDefaults",
                () =>
                {
                    Data.Base.DataContracts.Defaults coreDefaults = DataReader.ReadRecord<Data.Base.DataContracts.Defaults>("CORE.PARMS", "DEFAULTS");
                    if (coreDefaults == null)
                    {
                        logger.Info("Unable to access DEFAULTS from CORE.PARMS table.");
                        coreDefaults = new Defaults();
                    }
                    return coreDefaults;
                }, Level1CacheTimeoutValue);
        }

        /// <summary>
        /// Get and Cache the FaSysParams record
        /// </summary>
        /// <returns>FaSysParams DataContract</returns>
        private FaSysParams GetSystemParameters()
        {
            return GetOrAddToCache<FaSysParams>("FinancialAidSystemParameters",
                            () =>
                            {
                                var sysParams = DataReader.ReadRecord<FaSysParams>("ST.PARMS", "FA.SYS.PARAMS");
                                if (sysParams == null)
                                {
                                    logger.Info("Unable to read FA.SYS.PARAMS from database");
                                    sysParams = new FaSysParams();
                                }
                                return sysParams;
                            });
        }
    }
}