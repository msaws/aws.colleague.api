﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Data.Base.Transactions;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Web.Http.Configuration;
using slf4net;

namespace Ellucian.Colleague.Data.Base.Repositories
{
    /// <summary>
    /// Repository for tax form consents.
    /// </summary>
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class TaxFormConsentRepository : BaseColleagueRepository, ITaxFormConsentRepository
    {
        private readonly string colleagueTimeZone;

        /// <summary>
        /// Tax form consent repository constructor.
        /// </summary>
        /// <param name="settings">Settings</param>
        /// <param name="cacheProvider">Cache Provider</param>
        /// <param name="transactionFactory">Transaction factory</param>
        /// <param name="logger">Logger</param>
        public TaxFormConsentRepository(ApiSettings settings, ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            colleagueTimeZone = settings.ColleagueTimeZone;
        }

        /// <summary>
        /// Get a set of consent records.
        /// </summary>
        /// <param name="personId">Person ID</param>
        /// <param name="taxForm">Type of tax form</param>
        /// <returns>Set of tax form consent records</returns>
        public async Task<IEnumerable<TaxFormConsent>> GetAsync(string personId, TaxForms taxForm)
        {
            if (string.IsNullOrEmpty(personId))
                throw new ArgumentNullException("personId", "Person ID must be specified.");

            // Based on the type of tax form, we will obtain the consents from different entities.
            var taxFormConsentEntities = new List<TaxFormConsent>();
            switch (taxForm)
            {
                case TaxForms.FormW2:

                    // W2 consents are stored in W2.CONSENT.HISTORY
                    var w2Criteria = "W2CH.HRPER.ID EQ '" + personId + "'";
                    var w2docConsentHistoryRecords = await DataReader.BulkReadRecordAsync<W2ConsentHistory>(w2Criteria);

                    foreach (var record in w2docConsentHistoryRecords)
                    {
                        try
                        {
                            var status = false;
                            // "C" indicates consented, "W" indicates withhold consent
                            if (record.W2chNewStatus.ToUpper() == "C")
                            {
                                status = true;
                            }

                            var taxForms = TaxForms.FormW2;
                            var timeStamp = record.W2ConsentHistoryAddtime.ToPointInTimeDateTimeOffset(record.W2ConsentHistoryAdddate, colleagueTimeZone);

                            if (timeStamp == null || !timeStamp.HasValue)
                                throw new ApplicationException("Invalid timestamp for W2.CONSENT.HISTORY record: " + record.Recordkey);

                            var taxFormConsent = new TaxFormConsent(personId, taxForms, status, timeStamp.Value);
                            taxFormConsentEntities.Add(taxFormConsent);
                        }
                        catch (Exception e)
                        {
                            LogDataError("TaxFormConsent", personId, new Object(), e, e.Message);
                        }
                    }
                    break;

                case TaxForms.Form1095C:
                    // 1095-C consents are stored in DOC.CONSENT.HISTORY
                    var criteria = "DCHIST.PERSON.ID EQ '" + personId + "' AND WITH DCHIST.DOCUMENT EQ '1095C'";
                    var docConsentHistoryRecords = await DataReader.BulkReadRecordAsync<DocConsentHistory>(criteria);

                    foreach (var record in docConsentHistoryRecords)
                    {
                        try
                        {
                            var status = false;
                            // "C" indicates consented, "W" indicates withhold consent
                            if (record.DchistStatus.ToUpper() == "C")
                            {
                                status = true;
                            }

                            // Make sure the document associated to the consent history record is 1095C
                            if (record.DchistDocument != "1095C")
                                throw new ApplicationException("DOC.CONSENT.HISTORY record " + record.Recordkey + " should have a document type of 1095C, but does not.");

                            var taxForms = TaxForms.Form1095C;
                            var timeStamp = record.DchistStatusTime.ToPointInTimeDateTimeOffset(record.DchistStatusDate, colleagueTimeZone);

                            if (timeStamp == null || !timeStamp.HasValue)
                                throw new ApplicationException("Invalid timestamp for DOC.CONSENT.HISTORY record: " + record.Recordkey);

                            var taxFormConsent = new TaxFormConsent(personId, taxForms, status, timeStamp.Value);
                            taxFormConsentEntities.Add(taxFormConsent);
                        }
                        catch (Exception e)
                        {
                            LogDataError("TaxFormConsent", personId, record, e, e.Message);
                        }
                    }
                    break;
                case TaxForms.Form1098:

                    // 1095-C consents are stored in DOC.CONSENT.HISTORY
                    var form1098Criteria = "DCHIST.PERSON.ID EQ '" + personId + "' AND WITH DCHIST.DOCUMENT EQ '1098'";
                    var form1098DocConsentHistoryRecords = await DataReader.BulkReadRecordAsync<DocConsentHistory>(form1098Criteria);

                    foreach (var record in form1098DocConsentHistoryRecords)
                    {
                        try
                        {
                            var status = false;
                            // "C" indicates consented, "W" indicates withhold consent
                            if (record.DchistStatus.ToUpper() == "C")
                            {
                                status = true;
                            }

                            // Make sure the document associated to the consent history record is 1095C
                            if (record.DchistDocument != "1098")
                                throw new ApplicationException("DOC.CONSENT.HISTORY record " + record.Recordkey + " should have a document type of 1098, but does not.");

                            var taxForms = TaxForms.Form1098;
                            var timeStamp = record.DchistStatusTime.ToPointInTimeDateTimeOffset(record.DchistStatusDate, colleagueTimeZone);

                            if (timeStamp == null || !timeStamp.HasValue)
                                throw new ApplicationException("Invalid timestamp for DOC.CONSENT.HISTORY record: " + record.Recordkey);

                            var taxFormConsent = new TaxFormConsent(personId, taxForms, status, timeStamp.Value);
                            taxFormConsentEntities.Add(taxFormConsent);
                        }
                        catch (Exception e)
                        {
                            LogDataError("TaxFormConsent", personId, record, e, e.Message);
                        }
                    }
                    break;
            }

            return taxFormConsentEntities;
        }

        /// <summary>
        /// Create a new tax form consent record.
        /// </summary>
        /// <param name="newTaxFormConsent">TaxFormConsent object</param>
        /// <returns>New TaxFormConsent record.</returns>
        public async Task<TaxFormConsent> PostAsync(TaxFormConsent newTaxFormConsent)
        {
            // Converting form names from enumerable to string equivalents used in Colleague.
            var document = string.Empty;
            switch (newTaxFormConsent.TaxForm)
            {
                case TaxForms.FormW2:
                    document = "W2";
                    break;
                case TaxForms.Form1095C:
                    document = "1095C";
                    break;
                case TaxForms.Form1098:
                    document = "1098";
                    break;
            }

            // Boolean true corresponds to "C" for "Consent" while false corresponds to "W" for "Withheld Consent."
            var consent = newTaxFormConsent.HasConsented ? "C" : "W";
            var timeStamp = ColleagueTimeZoneUtility.ToLocalDateTime(newTaxFormConsent.TimeStamp, colleagueTimeZone);

            CreateDocConsentHistoryRequest request = new CreateDocConsentHistoryRequest()
            {
                APersonId = newTaxFormConsent.PersonId,
                ADocument = document,
                AConsent = consent,
                ADate = timeStamp,
                ATime = timeStamp
            };
            CreateDocConsentHistoryResponse response = await transactionInvoker.ExecuteAsync<CreateDocConsentHistoryRequest, CreateDocConsentHistoryResponse>(request);

            return newTaxFormConsent;
        }
    }
}
