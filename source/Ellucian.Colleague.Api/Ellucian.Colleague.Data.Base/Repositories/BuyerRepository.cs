﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.DataContracts;
using Ellucian.Web.Cache;
using slf4net;
using System.Threading.Tasks;
using System.Collections.Generic;
using Ellucian.Web.Http.Configuration;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.Entities;

namespace Ellucian.Colleague.Data.Base.Repositories
{
    public class BuyerRepository: PersonRepository, IBuyerRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="BuyerRepository"/> class.
        /// </summary>
        /// <param name="cacheProvider">The cache provider.</param>
        /// <param name="transactionFactory">The transaction factory.</param>
        /// <param name="logger">The logger.</param>
        public BuyerRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings apiSettings)
            : base(cacheProvider, transactionFactory, logger, apiSettings)
        {
            CacheTimeout = Level1CacheTimeoutValue;
        }

        /// <summary>
        /// Get all staff records and convert it to the Buyer Entity
        /// </summary>
        /// <typeparam name="Buyer"></typeparam>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        public async Task<Tuple<IEnumerable<Buyer>, int>> GetBuyersAsync(int offset, int limit, bool bypassCache = false)
        {
            List<Buyer> buyers = new List<Buyer>();

            var staffIds = await DataReader.SelectAsync("STAFF", string.Empty);
            if ((staffIds == null || !staffIds.Any()))
            {
                return new Tuple<IEnumerable<Buyer>, int>(null, 0);
            }

            int totalRecords = staffIds.Count();

            Array.Sort(staffIds);

            var subList = staffIds.Skip(offset).Take(limit);

            if (subList != null && subList.Any())
            {
                try
                {
                    var personRecords = await DataReader.BulkReadRecordAsync<DataContracts.Person>(subList.ToArray(), bypassCache);

                    var staffStatusesValidationTable = await GetStaffStatusesValcode();

                    var staffData =await DataReader.BulkReadRecordAsync<DataContracts.Staff>(subList.ToArray(), bypassCache);
                    foreach (var staff in staffData)
                    {
                        Buyer buyerEntity = await GetBuyerEnity(staff, personRecords.ToList(), staffStatusesValidationTable);
                        buyers.Add(buyerEntity);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error occurred while getting staff records.", ex);
                }
            }

            return new Tuple<IEnumerable<Buyer>, int>(buyers, totalRecords);
        }

        public async Task<Buyer> GetBuyerAsync(string guid)
        {
            var entity = await this.GetRecordInfoFromGuidAsync(guid);
            if (entity == null || entity.Entity != "STAFF")
            {
                throw new KeyNotFoundException(string.Format("Buyer not found for GUID {0}", guid));
            }

            var staffId = entity.PrimaryKey;
            if (string.IsNullOrWhiteSpace(staffId))
            {
                throw new KeyNotFoundException("Guid " + guid + "does not exist");
            }
            var staffRecord = await DataReader.ReadRecordAsync<DataContracts.Staff>("STAFF", staffId);
            if (staffRecord == null)
            {
                throw new KeyNotFoundException("Buyer not found with GUID " + guid);
            }
            var personRecord = await DataReader.ReadRecordAsync<DataContracts.Person>("PERSON", staffId);
            List<DataContracts.Person> personRecords = new List<DataContracts.Person>();
            personRecords.Add(personRecord);
            var staffStatusesValidationTable = await GetStaffStatusesValcode();
            return await GetBuyerEnity(staffRecord, personRecords, staffStatusesValidationTable);
        }

        private async Task<Buyer> GetBuyerEnity(DataContracts.Staff staff, List<DataContracts.Person> StaffEntities, ApplValcodes staffStatusesValidationTable)
        {
            Buyer BuyerEntity = new Buyer()
            {
                RecordKey = staff.Recordkey,
                Guid = staff.RecordGuid,
                StartOn = staff.StaffAddDate,
            };
            
            // update IsActive flag on staff record if status indicates Active
            ApplValcodesVals staffStatus = null;
            if (staffStatusesValidationTable != null && staffStatusesValidationTable.ValsEntityAssociation != null && !string.IsNullOrEmpty(staff.StaffStatus))
            {
                staffStatus = staffStatusesValidationTable.ValsEntityAssociation.FirstOrDefault(v => v.ValInternalCodeAssocMember == staff.StaffStatus);
            }

            // Status must have a special processing code of "A" to be considered "Active"
            if (staffStatus != null && staffStatus.ValActionCode1AssocMember == "A")
            {
                BuyerEntity.PersonGuid = await GetGuidFromRecordInfoAsync("PERSON", staff.Recordkey);
                BuyerEntity.Status = "active";
            }
            else
            {
                BuyerEntity.Status = "inactive";
            }

            DataContracts.Person StaffEntity = StaffEntities.FirstOrDefault(x => x.Recordkey == BuyerEntity.RecordKey);

            if (StaffEntity != null)
            {
                BuyerEntity.Name = string.Concat(StaffEntity.FirstName, " ", StaffEntity.LastName);
            }

            return BuyerEntity;
        }

        /// <summary>
        /// Gets Staff Statuses Valcodes
        /// </summary>
        /// <returns>STAFF.STATUSES Valcodes</returns>
        private async Task<ApplValcodes> GetStaffStatusesValcode()
        {
            // Get the STAFF.STATUSES valcode 
            var staffStatusesValidationTable = new ApplValcodes();
            try
            {
                staffStatusesValidationTable = await GetOrAddToCacheAsync<ApplValcodes>("StaffStatuses",
                    async () =>
                    {
                        ApplValcodes staffStatusesValTable = await DataReader.ReadRecordAsync<ApplValcodes>("CORE.VALCODES", "STAFF.STATUSES");
                        return staffStatusesValTable;
                    }, Level1CacheTimeoutValue);
            }
            catch (Exception)
            {
                // log the issue and move on. Not likely to happen...
                var errorMessage = "Unable to retrieve STAFF.STATUSES validation table from Colleague.";
                logger.Info(errorMessage);
            }
            return staffStatusesValidationTable;
        }

        /// <summary>
        /// Get the GUID for a buyer using its ID
        /// </summary>
        /// <param name="id">Buyer ID</param>
        /// <returns>Buyer GUID</returns>
        public async Task<string> GetBuyerGuidFromIdAsync(string id)
        {
            try
            {
                return await GetGuidFromRecordInfoAsync("PERSON", id);
            }
            catch (ArgumentNullException)
            {
                throw;
            }
            catch (RepositoryException ex)
            {
                ex.AddError(new RepositoryError("Guid NotFound", "GUID not found for buyer " + id));
                throw ex;
            }
        }
    }
}
