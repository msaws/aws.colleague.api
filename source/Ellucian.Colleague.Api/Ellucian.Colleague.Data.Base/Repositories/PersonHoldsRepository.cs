﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;
using Ellucian.Colleague.Data.Base.Transactions;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.Entities;
using Ellucian.Web.Http.Configuration;

namespace Ellucian.Colleague.Data.Base.Repositories
{
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class PersonHoldsRepository : BaseColleagueRepository, IPersonHoldsRepository
    {
        readonly int readSize;

        /// <summary>
        /// Contructor
        /// </summary>
        /// <param name="cacheProvider"></param>
        /// <param name="transactionFactory"></param>
        /// <param name="logger"></param>
        public PersonHoldsRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings apiSettings)
            : base(cacheProvider, transactionFactory, logger)
        {
            this.readSize = ((apiSettings != null) && (apiSettings.BulkReadSize > 0)) ? apiSettings.BulkReadSize : 5000;
        }
        #region GET Methods

        /// <summary>
        /// Returns a list of all active holds recorded for any person in the database
        /// </summary>
        /// <returns></returns>
        public async Task<Tuple<IEnumerable<PersonRestriction>, int>> GetPersonHoldsAsync(int offset, int limit)
        {
            var criteria =
                string.Format("WITH STR.STUDENT NE '' AND WITH STR.RESTRICTION NE '' AND WITH STR.END.DATE EQ '' OR STR.END.DATE GE '{0}' AND WITH STR.CORP.INDICATOR NE 'Y'",
                    DateTime.Today.Date.ToString("d"));
        
            var personHoldsIds = await DataReader.SelectAsync("STUDENT.RESTRICTIONS", criteria);
            var totalCount = personHoldsIds.Count();
            var studentHolds = new List<StudentRestrictions>();

            Array.Sort(personHoldsIds);
            var sublist = personHoldsIds.Skip(offset).Take(limit);
            var newPersonHoldsIds = sublist.ToArray();
            if (newPersonHoldsIds.Any())
            {
                var bulkData =
                    await DataReader.BulkReadRecordAsync<StudentRestrictions>("STUDENT.RESTRICTIONS", newPersonHoldsIds);
                studentHolds.AddRange(bulkData);
            }
            var personHoldsList = BuildPersonHolds(studentHolds);

            return new Tuple<IEnumerable<PersonRestriction>, int>(personHoldsList, totalCount);
        }

        /// <summary>
        /// Returns a hold for a specified Student Restrictions key.
        /// </summary>
        /// <param name="ids">Key to Student Restrictions to be returned</param>
        /// <returns>List of PersonRestrictions Objects</returns>
        public async Task<PersonRestriction> GetPersonHoldByIdAsync(string id)
        {
            var personHoldId = await GetRecordKeyFromGuidAsync(id);
            var studentHold = await DataReader.ReadRecordAsync<StudentRestrictions>("STUDENT.RESTRICTIONS", personHoldId);
            var personHold = BuildPersonHold(studentHold);

            return personHold;
        }

        /// <summary>
        /// Returns all restrictions for a specified person Id
        /// </summary>
        /// <param name="personId">Person Id for whom restrictions are requested</param>
        /// <returns>List of PersonRestrictions</returns>
        public async Task<IEnumerable<PersonRestriction>> GetPersonHoldsByPersonIdAsync(string personId)
        {
            if (string.IsNullOrEmpty(personId))
            {
                throw new ArgumentNullException("personId", "Must provide a person Id");
            }

            IEnumerable<PersonRestriction> holdsList = new List<PersonRestriction>();

            var personKey = await GetRecordKeyFromGuidAsync(personId);
            if (string.IsNullOrEmpty(personKey))
            {
                throw new ArgumentNullException("personKey", "No person record with person Id: " + personId);
            }

            var criteria = string.Format("WITH STR.STUDENT EQ '{0}'", personKey);

            // If there is no STUDENT.RESTRICTIONS record for this person in Colleague returns no restrictions.
            Collection<StudentRestrictions> personHolds = await DataReader.BulkReadRecordAsync<StudentRestrictions>(criteria);
            holdsList = BuildPersonHolds(personHolds);

            return holdsList;
        }
       
        #endregion

        #region DELETE Method
        /// <summary>
        /// Delete a person hold based on person hold id
        /// </summary>
        /// <param name="personHoldsId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<PersonHoldResponse>> DeletePersonHoldsAsync(string personHoldsId)
        {
            var request = new DeleteRestrictionRequest()
            {
                StudentRestrictionsId = personHoldsId
            };
            
            //Delete
            var response = await transactionInvoker.ExecuteAsync<DeleteRestrictionRequest, DeleteRestrictionResponse>(request);
            
            //if there are any errors throw
            if (response.DeleteRestrictionErrors.Any())
            {
                var exception = new RepositoryException("Errors encountered while deleting person hold: " + personHoldsId);
                response.DeleteRestrictionErrors.ForEach(e => exception.AddError(new RepositoryError(e.ErrorCodes, e.ErrorMessages)));
                throw exception;
            }

            //log any warnings
            List<PersonHoldResponse> warningList = new List<PersonHoldResponse>();
            if (response.DeleteRestrictionWarnings.Any())
            {
                foreach (var restrictionWarning in response.DeleteRestrictionWarnings)
                {
                    PersonHoldResponse restrictionResponse = new PersonHoldResponse();
                    restrictionResponse.WarningCode = restrictionWarning.WarningCodes;
                    restrictionResponse.WarningMessage = restrictionWarning.WarningMessages;

                    warningList.Add(restrictionResponse);
                }
            }
            return warningList;
        }

        #endregion

        #region Updates

        /// <summary>
        /// Updates or creates the person hold
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        public async Task<PersonHoldResponse> UpdatePersonHoldAsync(PersonHoldRequest request)
        {
            UpdateRestrictionRequest updateRequest = new UpdateRestrictionRequest();

            updateRequest.StudentRestrictionsId = request.Id;            
            updateRequest.StrComments = string.IsNullOrEmpty(request.Comments) ? string.Empty : request.Comments;
            updateRequest.StrEndDate = request.EndOn.HasValue ? request.EndOn.Value.Date : default(DateTime?);
            updateRequest.StrGuid = request.PersonHoldGuid;
            updateRequest.StrNotify = request.NotificationIndicator.Equals("Y", StringComparison.InvariantCultureIgnoreCase) ? true : false;
            updateRequest.StrRestriction = request.RestrictionType;
            updateRequest.StrStartDate = request.StartOn.Value.Date;
            updateRequest.StrStudent = request.PersonId;

            UpdateRestrictionResponse updateResponse = await transactionInvoker.ExecuteAsync<UpdateRestrictionRequest, UpdateRestrictionResponse>(updateRequest);

            if (updateResponse.RestrictionErrorMessages.Any())
            {
                var errorMessage = string.Empty;
                foreach (var message in updateResponse.RestrictionErrorMessages)
                {
                    errorMessage = string.Format("Error occurred updating person hold '{0} {1}'", request.Id, request.PersonHoldGuid);
                    errorMessage += string.Join(Environment.NewLine, message.ErrorMsg);
                    logger.Error(errorMessage.ToString());
                }
                throw new InvalidOperationException(errorMessage);
            }

            PersonHoldResponse response = new PersonHoldResponse();
            response.PersonHoldGuid = updateResponse.StrGuid;
            response.PersonHoldId = updateResponse.StudentRestrictionsId;
            
            return response;
        }

        #endregion

        #region Helper Methods
        /// <summary>
        /// Builds many person holds
        /// </summary>
        /// <param name="studentHolds"></param>
        /// <returns></returns>
        private List<PersonRestriction> BuildPersonHolds(IEnumerable<StudentRestrictions> studentHolds)
        {
            List<PersonRestriction> personHolds = new List<PersonRestriction>();
            var filteredHolList = studentHolds.Where(h => h.StrEndDate > DateTime.Today.Date || h.StrEndDate == null);

            foreach (var hold in filteredHolList)
            {
                PersonRestriction personHold = BuildPersonHold(hold);
                personHolds.Add(personHold);
            }
            return personHolds;
        }

        /// <summary>
        /// Builds single person hold
        /// </summary>
        /// <param name="studentHold"></param>
        /// <returns></returns>
        private PersonRestriction BuildPersonHold(StudentRestrictions studentHold)
        {
            PersonRestriction stuRestriction = null;
            if (studentHold != null)
            {

                try
                {
                    stuRestriction = new PersonRestriction(studentHold.Recordkey, studentHold.StrStudent, studentHold.StrRestriction, studentHold.StrStartDate, studentHold.StrEndDate, studentHold.StrSeverity, studentHold.StrPrtlDisplayFlag);
                    stuRestriction.Comment = string.IsNullOrEmpty(studentHold.StrComments) ? string.Empty : studentHold.StrComments;
                    stuRestriction.NotificationIndicator = studentHold.StrPrtlDisplayFlag;
                    return stuRestriction;
                }
                catch (Exception e)
                {
                    var inString = "Student Restriction Id: " + studentHold.Recordkey + ", Student Id: " + studentHold.StrStudent + ", Restriction Id: " + studentHold.StrRestriction;
                    LogDataError("Student Restriction", studentHold.Recordkey, studentHold, e, inString);
                }
            }
            return stuRestriction;
        }

        /// <summary>
        /// Get the GUID for a section using its ID
        /// </summary>
        /// <param name="id">Section ID</param>
        /// <returns>Section GUID</returns>
        public async Task<string> GetStudentHoldGuidFromIdAsync(string id)
        {
            try
            {
                return await GetGuidFromRecordInfoAsync("STUDENT.RESTRICTIONS", id);
            }
            catch (ArgumentNullException)
            {
                throw;
            }
            catch (RepositoryException ex)
            {
                ex.AddError(new RepositoryError("student.restriction.guid.NotFound", "GUID not found for student restriction " + id));
                throw ex;
            }
        }

        /// <summary>
        /// Gets id from guid input
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<string> GetStudentHoldIdFromGuidAsync(string id)
        {
            try
            {
                return await GetRecordKeyFromGuidAsync(id);
            }
            catch (ArgumentNullException)
            {
                throw;
            }
            catch (RepositoryException ex)
            {
                ex.AddError(new RepositoryError("student.restriction.guid.NotFound", "GUID not found for student restriction " + id));
                throw ex;
            }
        }
        #endregion
       
    }
}
