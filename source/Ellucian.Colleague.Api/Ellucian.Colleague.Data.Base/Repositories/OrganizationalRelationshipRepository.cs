﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Dependency;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Data.Colleague;
using Ellucian.Web.Cache;
using slf4net;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Data.Base.Transactions;
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Web.Http.Configuration;

namespace Ellucian.Colleague.Data.Base.Repositories
{
    /// <summary>
    /// Repository for organizational relationships
    /// </summary>
    [RegisterType]
    public class OrganizationalRelationshipRepository : BaseColleagueRepository, IOrganizationalRelationshipRepository
    {
        private readonly int _readSize;

        /// <summary>
        /// Constructor for organizational relationship repository.
        /// </summary>
        /// <param name="cacheProvider"></param>
        /// <param name="transactionFactory"></param>
        /// <param name="logger"></param>
        /// <param name="apiSettings"></param>
        public OrganizationalRelationshipRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings apiSettings)
            : base(cacheProvider, transactionFactory, logger)
        {
            _readSize = apiSettings != null && apiSettings.BulkReadSize > 0 ? apiSettings.BulkReadSize : 5000;
        }

        /// <summary>
        /// Returns the organizational relationships for the given IDs
        /// </summary>
        /// <param name="ids">Organizational relationship IDs to retrieve</param>
        /// <returns>The organizational relationships</returns>
        public async Task<IEnumerable<OrganizationalRelationship>> GetAsync(List<string> ids)
        {
            if (ids == null)
            {
                throw new ArgumentNullException("ids");
            }
            try
            {
                List<OrgEntityRoleRel> orgEntityRoleRelRecords = new List<OrgEntityRoleRel>();
                for (int i = 0; i < ids.Count(); i += _readSize)
                {
                    var subList = ids.Skip(i).Take(_readSize).ToArray();
                    var bulkRecords = await DataReader.BulkReadRecordAsync<OrgEntityRoleRel>(subList);
                    if (bulkRecords != null)
                    {
                        orgEntityRoleRelRecords.AddRange(bulkRecords);
                    }
                }

                var organizationalRelationships = new List<OrganizationalRelationship>();

                foreach (var orgEntityRoleRel in orgEntityRoleRelRecords)
                {
                    organizationalRelationships.Add(new OrganizationalRelationship(orgEntityRoleRel.Recordkey, orgEntityRoleRel.OerrelOerId, orgEntityRoleRel.OerrelRelatedOerId, orgEntityRoleRel.OerrelRelationshipCategory));
                }

                return organizationalRelationships;
            }
            catch (Exception e)
            {
                logger.Error(e, "Error reading IDs");
                throw;
            }
        }

        /// <summary>
        /// Adds an organizational relationship
        /// </summary>
        /// <param name="organizationalRelationshipEntity">The organizational relationship to add</param>
        /// <returns>The new organizational relationship</returns>
        public async Task<OrganizationalRelationship> AddAsync(OrganizationalRelationship organizationalRelationshipEntity)
        {
            if (organizationalRelationshipEntity == null)
            {
                throw new ArgumentNullException("organizationalRelationshipEntity");
            }

            var organizationalRelationshipAddRequest = new UpdateOrgEntityRoleRelMultiRequest()
            {
                Action = "A",
                OrgEntityRoleRelId = "",
                OrgEntityRoleId = organizationalRelationshipEntity.OrganizationalPersonPositionId,
                RelatedOrgEntityRoleId = organizationalRelationshipEntity.RelatedOrganizationalPersonPositionId,
                RelationshipCategory = organizationalRelationshipEntity.Category
            };

            var organizationalRelationshipAddResponse = await transactionInvoker.ExecuteAsync<UpdateOrgEntityRoleRelMultiRequest, UpdateOrgEntityRoleRelMultiResponse>(organizationalRelationshipAddRequest);

            // Check response for errors
            //ErrorOccurred: OUT-- Code representing error that occurred
            //           1 = ORG.ENTITY.ROLE record does not exist for a value in OrgEntityRole
            //           2 = ORG.ENTITY.ROLE record does not exist for a value in RelatedOrgEntityRole
            //           3 = The two ORG.ENTITY.ROLE records reference the same person and
            //                  therefore cannot be related
            //           4 = ORG.ENTITY.ROLE.REL record does not exist for a value in OrgEntityRoleRel
            //           5 = An existing relationship for OrgEntityRel already exists for the specified category
            //           6 = Requested relationship would cause a circular relationship with an existing one
            //           7 = Invalid argument (see message)
            //  Message: OUT-- Detailed message describing the error
            if (!string.IsNullOrEmpty(organizationalRelationshipAddResponse.ErrorOccurred) && organizationalRelationshipAddResponse.ErrorOccurred != "0")
            {
                var errorMessage = "Error(s) occurred adding relationships '" + organizationalRelationshipAddRequest.OrgEntityRoleRelId + "':";
                errorMessage += organizationalRelationshipAddResponse.Message;
                logger.Error(errorMessage.ToString());
                throw new InvalidOperationException("Error occurred adding relationships");
            }

            var newId = organizationalRelationshipAddResponse.OrgEntityRoleRelId;
            var newOrganizationalRelationships = await GetAsync(new List<string> { newId });

            if (newOrganizationalRelationships.Any())
            {
                return newOrganizationalRelationships.First();
            }
            else
            {
                throw new InvalidOperationException("Error occurred retrieving relationship after addition.");
            }
        }

        /// <summary>
        /// Deletes an organizational relationship with the given ID
        /// </summary>
        /// <param name="id">The organizational relationship ID to delete</param>
        public async Task DeleteAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }
            var organizationalRelationshipDeleteRequest = new UpdateOrgEntityRoleRelMultiRequest()
            {
                Action = "D",
                OrgEntityRoleRelId = id,
            };

            var organizationalRelationshipDeleteResponse = await transactionInvoker.ExecuteAsync<UpdateOrgEntityRoleRelMultiRequest, UpdateOrgEntityRoleRelMultiResponse>(organizationalRelationshipDeleteRequest);

            // Check response for errors
            //ErrorOccurred: OUT-- Code representing error that occurred
            //           1 = ORG.ENTITY.ROLE record does not exist for a value in OrgEntityRole
            //           2 = ORG.ENTITY.ROLE record does not exist for a value in RelatedOrgEntityRole
            //           3 = The two ORG.ENTITY.ROLE records reference the same person and
            //                  therefore cannot be related
            //           4 = ORG.ENTITY.ROLE.REL record does not exist for a value in OrgEntityRoleRel
            //           5 = An existing relationship for OrgEntityRel already exists for the specified category
            //           6 = Requested relationship would cause a circular relationship with an existing one
            //           7 = Invalid argument (see message)
            //  Message: OUT-- Detailed message describing the error
            if (!string.IsNullOrEmpty(organizationalRelationshipDeleteResponse.ErrorOccurred) && organizationalRelationshipDeleteResponse.ErrorOccurred != "0")
            {
                var errorMessage = "Error(s) occurred updating relationships '" + organizationalRelationshipDeleteRequest.OrgEntityRoleRelId + "':";
                errorMessage += organizationalRelationshipDeleteResponse.Message;
                logger.Error(errorMessage.ToString());
                throw new InvalidOperationException("Error occurred deleting relationships");
            }

            return;
        }

        /// <summary>
        /// Updates an organizational relationship
        /// </summary>
        /// <param name="organizationalRelationshipEntity">The organizational relationship to delete</param>
        /// <returns>The updated organizational relationship</returns>
        public async Task<OrganizationalRelationship> UpdateAsync(OrganizationalRelationship organizationalRelationshipEntity)
        {
            if (organizationalRelationshipEntity == null)
            {
                throw new ArgumentNullException("organizationalRelationshipEntity");
            }

            var organizationalRelationshipUpdateRequest = new UpdateOrgEntityRoleRelMultiRequest()
            {
                Action = "U",
                OrgEntityRoleRelId = organizationalRelationshipEntity.Id,
                OrgEntityRoleId = organizationalRelationshipEntity.OrganizationalPersonPositionId,
                RelatedOrgEntityRoleId = organizationalRelationshipEntity.RelatedOrganizationalPersonPositionId,
                RelationshipCategory = organizationalRelationshipEntity.Category
            };

            var organizationalRelationshipUpdateResponse = await transactionInvoker.ExecuteAsync<UpdateOrgEntityRoleRelMultiRequest, UpdateOrgEntityRoleRelMultiResponse>(organizationalRelationshipUpdateRequest);

            // Check response for errors
            //ErrorOccurred: OUT-- Code representing error that occurred
            //           1 = ORG.ENTITY.ROLE record does not exist for a value in OrgEntityRole
            //           2 = ORG.ENTITY.ROLE record does not exist for a value in RelatedOrgEntityRole
            //           3 = The two ORG.ENTITY.ROLE records reference the same person and
            //                  therefore cannot be related
            //           4 = ORG.ENTITY.ROLE.REL record does not exist for a value in OrgEntityRoleRel
            //           5 = An existing relationship for OrgEntityRel already exists for the specified category
            //           6 = Requested relationship would cause a circular relationship with an existing one
            //           7 = Invalid argument (see message)
            //  Message: OUT-- Detailed message describing the error
            if (!string.IsNullOrEmpty(organizationalRelationshipUpdateResponse.ErrorOccurred) && organizationalRelationshipUpdateResponse.ErrorOccurred != "0")
            {
                var errorMessage = "Error(s) occurred updating relationships '" + organizationalRelationshipUpdateRequest.OrgEntityRoleRelId + "':";
                errorMessage += organizationalRelationshipUpdateResponse.Message;
                logger.Error(errorMessage.ToString());
                throw new InvalidOperationException("Error occurred updating relationships");
            }

            var organizationalRelationships = await GetAsync(new List<string> { organizationalRelationshipUpdateRequest.OrgEntityRoleRelId });
            return organizationalRelationships.First();
        }
    }
}
