﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Data.Base.Transactions;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;
using Ellucian.Data.Colleague.DataContracts;
using System.Collections.ObjectModel;
using Ellucian.Web.License;
using Microsoft.IdentityModel.Protocols.WSFederation.Metadata;
using Microsoft.Practices.EnterpriseLibrary.Common.Utility;

namespace Ellucian.Colleague.Data.Base.Repositories
{
    [RegisterType]
    public class ConfigurationRepository : BaseColleagueRepository, IConfigurationRepository
    {
        private readonly string _orgIndicator;

        /// <summary>
        /// Initializes a new instance of the <see cref="ConfigurationRepository"/> class.
        /// </summary>
        /// <param name="cacheProvider">The cache provider.</param>
        /// <param name="transactionFactory">The transaction factory.</param>
        /// <param name="logger">The logger.</param>
        public ConfigurationRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            // Using level 1 cache time out value for data that rarely changes.
            CacheTimeout = Level1CacheTimeoutValue;
            _orgIndicator = "ORG";
        }

        #region Public Methods

        /// <summary>
        /// Gets all of the EthosDataPrivacy settings stored on ETHOS.SECURITY accessed on form EDPS
        /// </summary>
        /// <param name="bypassCache">bool to determine if cache should be bypassed</param>
        /// <returns>List of Ellucian.Colleague.Domain.Base.Entities.EthosSecurity</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Domain.Base.Entities.EthosSecurity>> GetEthosDataPrivacyConfiguration(bool bypassCache)
        {
            const string ethosDataPrivacyCacheKey = "AllEthosDataPrivacySettings";

            if (bypassCache && ContainsKey(BuildFullCacheKey(ethosDataPrivacyCacheKey)))
            {
                ClearCache(new List<string> { ethosDataPrivacyCacheKey });
            }

            var ethosDataPrivacySettingsList = await GetOrAddToCacheAsync<List<Ellucian.Colleague.Domain.Base.Entities.EthosSecurity>>(ethosDataPrivacyCacheKey,
                async () =>
                {
                    var ethosDpSettings = await DataReader.BulkReadRecordAsync<DataContracts.EthosSecurity>("ETHOS.SECURITY", "");
                    return (await BuildEthosSecurityList(ethosDpSettings)).ToList();
                }
            );
            return ethosDataPrivacySettingsList;
        }

        /// <summary>
        /// Checks if the user making the API call is the EMA user based on the user settings on the EMA configuration
        /// </summary>
        /// <param name="userName">userName to check</param>
        /// <param name="bypassCache">bool to determine if the cache should be bypassed</param>
        /// <returns>True if EMA user, false if not</returns>
        public async Task<bool> IsThisTheEmaUser(string userName, bool bypassCache)
        {
            const string hubCincRecord = "HubRecordFromCINC";
            
            if (bypassCache && ContainsKey(BuildFullCacheKey(hubCincRecord)))
            {
                ClearCache(new List<string> { hubCincRecord });
            }

            var hubRecordToCheck = await GetOrAddToCacheAsync<DataContracts.CdmIntegration>(hubCincRecord,
                async () =>
                {
                    return (await DataReader.BulkReadRecordAsync<DataContracts.CdmIntegration>("CDM.INTEGRATION", "WITH CDM.INTEGRATION.ID EQ 'HUB'")).FirstOrDefault();
                }
            );

            if (hubRecordToCheck == null)
            {
                return false;
            }
            else
            {
                return hubRecordToCheck.CintApiUsername.Equals(userName, StringComparison.OrdinalIgnoreCase);
            }

            //var test = await DataReader.BulkReadRecordAsync<DataContracts.CdmIntegration>("CDM.INTEGRATION", "WITH CDM.INTEGRATION.ID EQ 'HUB'");
            //return false;
        }

        /// <summary>
        /// Get an external mapping
        /// </summary>
        /// <param name="id">External Mapping ID</param>
        /// <returns>The external mapping info</returns>
        public ExternalMapping GetExternalMapping(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id", "External Mapping ID must be specified.");
            }

            ElfTranslateTables elfTranslateTable = DataReader.ReadRecord<ElfTranslateTables>(id);
            if (elfTranslateTable == null)
            {
                throw new KeyNotFoundException("External Mapping ID " + id + " is not valid.");
            }

            return BuildExternalMapping(elfTranslateTable);
        }

        /// <summary>
        /// Get the defaults configuration.
        /// </summary>
        /// <returns>The <see cref="DefaultsConfiguration">defaults configuration</see></returns>
        public DefaultsConfiguration GetDefaultsConfiguration()
        {
            return GetOrAddToCache<DefaultsConfiguration>("DefaultsConfiguration",
                () => { return BuildDefaultsConfiguration(); });
        }

        /// <summary>
        /// Gets an integration configuration
        /// </summary>
        /// <param name="integrationConfigurationId">Integration Configuration ID</param>
        /// <returns>An <see cref="IntegrationConfiguration">integration configuration</see></returns>
        public IntegrationConfiguration GetIntegrationConfiguration(string integrationConfigurationId)
        {
            if (string.IsNullOrEmpty(integrationConfigurationId))
            {
                throw new ArgumentNullException(integrationConfigurationId);
            }
            return BuildIntegrationConfiguration(integrationConfigurationId);
        }

        #region Tax form consent paragraphs
        /// <summary>
        /// Gets the tax form configuration parameters for the specific tax form passed in.
        /// </summary>
        /// <param name="taxFormId">The tax form (W-2, 1095-C, 1098-T, etc.)</param>
        /// <returns>Consent and withheld paragraphs for the specific tax form</returns>
        public async Task<TaxFormConfiguration> GetTaxFormConsentConfigurationAsync(TaxForms taxFormId)
        {
            var configuration = new TaxFormConfiguration(taxFormId);

            switch (taxFormId)
            {
                case TaxForms.FormW2:
                    configuration.ConsentParagraphs = await GetOrAddToCacheAsync<TaxFormConsentParagraph>("W2ConsentParagraphs",
                     async () =>
                     {
                         var paragraph = new TaxFormConsentParagraph();

                         // Get tax form parameters from HRWEB.DEFAULTS
                         var hrWebDefaults = await DataReader.ReadRecordAsync<HrwebDefaults>("HR.PARMS", "HRWEB.DEFAULTS");
                         if (hrWebDefaults != null)
                         {

                             // Get the tax form consent paragraphs for W-2
                             paragraph.ConsentText = hrWebDefaults.HrwebW2oConText;
                             paragraph.ConsentWithheldText = hrWebDefaults.HrwebW2oWhldText;
                         }
                         return paragraph;
                     });
                    break;

                case TaxForms.Form1095C:
                    configuration.ConsentParagraphs = await GetOrAddToCacheAsync<TaxFormConsentParagraph>("1095cConsentParagraphs",
                    async () =>
                    {
                        var paragraph = new TaxFormConsentParagraph();

                        // Get tax form parameters from HRWEB.DEFAULTS
                        var hrWebDefaults = await DataReader.ReadRecordAsync<HrwebDefaults>("HR.PARMS", "HRWEB.DEFAULTS");
                        if (hrWebDefaults != null)
                        {
                            // Get the tax form consent paragraphs for 1095-C
                            paragraph.ConsentText = hrWebDefaults.Hrweb1095cConText;
                            paragraph.ConsentWithheldText = hrWebDefaults.Hrweb1095cWhldText;
                        }
                        return paragraph;
                    });
                    break;
                case TaxForms.Form1098:
                    configuration = await Get1098TaxFormConsentParagraphsAsync();
                    break;
            }

            return configuration;
        }

        private async Task<TaxFormConfiguration> Get1098TaxFormConsentParagraphsAsync()
        {
            TaxFormConfiguration configuration = new TaxFormConfiguration(TaxForms.Form1098);
            configuration.ConsentParagraphs = await GetOrAddToCacheAsync<TaxFormConsentParagraph>("1098ConsentParagraphs",
                async () =>
                {
                    var paragraph = new TaxFormConsentParagraph();

                    // Get tax form parameters from HRWEB.DEFAULTS
                    var parm1098Contract = await DataReader.ReadRecordAsync<Parm1098>("ST.PARMS", "PARM.1098");
                    if (parm1098Contract != null)
                    {
                        // Get the tax form consent paragraphs for 1095-C
                        paragraph.ConsentText = parm1098Contract.P1098ConsentText;
                        paragraph.ConsentWithheldText = parm1098Contract.P1098WhldConsentText;
                    }
                    return paragraph;
                });

            return configuration;
        }
        #endregion

        #region Tax form availability
        /// <summary>
        /// Gets the tax form configuration parameters for the specific tax form passed in.
        /// </summary>
        /// <param name="taxFormId">The tax form (W-2, 1095-C, 1098-T, etc.)</param>
        /// <returns>Availability dates for the specific tax form</returns>
        public async Task<TaxFormConfiguration> GetTaxFormAvailabilityConfigurationAsync(TaxForms taxFormId)
        {
            var configuration = new TaxFormConfiguration(taxFormId);

            switch (taxFormId)
            {
                case TaxForms.FormW2:
                    // Obtain the availability dates for W-2.
                    var qtdYtdParameterW2 = await DataReader.ReadRecordAsync<QtdYtdParameterW2>("HR.PARMS", "QTD.YTD.PARAMETER");
                    if (qtdYtdParameterW2 != null)
                    {
                        // Validate the availability dates for W-2.
                        if (qtdYtdParameterW2.WebW2ParametersEntityAssociation != null)
                        {
                            foreach (var dataContract in qtdYtdParameterW2.WebW2ParametersEntityAssociation)
                            {
                                try
                                {
                                    if (string.IsNullOrEmpty(dataContract.QypWebW2YearsAssocMember))
                                        throw new ArgumentNullException("QypWebW2YearsAssocMember", "QypWebW2YearsAssocMember is required.");

                                    if (!dataContract.QypWebW2AvailableDatesAssocMember.HasValue)
                                        throw new ArgumentNullException("QypWebW2AvailableDatesAssocMember", "QypWebW2AvailableDatesAssocMember is required.");

                                    configuration.AddAvailability(new TaxFormAvailability(dataContract.QypWebW2YearsAssocMember, dataContract.QypWebW2AvailableDatesAssocMember.Value));
                                }
                                catch (Exception e)
                                {
                                    LogDataError("QypWebW2YearsAssocMember", "HR.PARMS - QTD.YTD.PARAMETER", dataContract, e, e.Message);
                                }
                            }
                        }
                    }
                    break;

                case TaxForms.Form1095C:
                    // Obtain the availability dates for 1095-C.
                    var qtdYtdParameter1095C = await DataReader.ReadRecordAsync<QtdYtdParameter1095C>("HR.PARMS", "QTD.YTD.PARAMETER");
                    if (qtdYtdParameter1095C != null)
                    {
                        // Validate the availability dates for 1095-C.
                        if (qtdYtdParameter1095C.Qyp1095cParametersEntityAssociation != null)
                        {
                            foreach (var contract in qtdYtdParameter1095C.Qyp1095cParametersEntityAssociation)
                            {
                                try
                                {
                                    if (string.IsNullOrEmpty(contract.QypWeb1095cYearsAssocMember))
                                        throw new ArgumentNullException("QypWeb1095cYearsAssocMember", "QypWeb1095cYearsAssocMember is required.");

                                    if (!contract.QypWeb1095cAvailDatesAssocMember.HasValue)
                                        throw new ArgumentNullException("QypWeb1095cAvailDatesAssocMember", "QypWeb1095cAvailDatesAssocMember is required.");

                                    configuration.AddAvailability(new TaxFormAvailability(contract.QypWeb1095cYearsAssocMember, contract.QypWeb1095cAvailDatesAssocMember.Value));
                                }
                                catch (Exception e)
                                {
                                    LogDataError("Qyp1095cParametersEntityAssociation", "HR.PARMS - QTD.YTD.PARAMETER", contract, e, e.Message);
                                }
                            }
                        }
                    }
                    break;
                case TaxForms.Form1098:
                    configuration = await Get1098TaxFormAvailabilityAsync();
                    break;

            }
            return configuration;
        }

        private async Task<TaxFormConfiguration> Get1098TaxFormAvailabilityAsync()
        {
            TaxFormConfiguration configuration = new TaxFormConfiguration(TaxForms.Form1098);

            // Read the PARM.1098 record so we can use the tax form specified as the 1098 tax form in Colleague.
            var parm1098Contract = await DataReader.ReadRecordAsync<Parm1098>("ST.PARMS", "PARM.1098");
            if (parm1098Contract == null)
            {
                LogDataError("ST.PARMS", "PARM.1098", "", null, "PARM.1098 cannot be null.");
                return configuration;
            }

            if (string.IsNullOrEmpty(parm1098Contract.P1098TTaxForm))
            {
                LogDataError("ST.PARMS", "PARM.1098", "", null, "No 1098-T form specified.");
                return configuration;
            }

            // Obtain the availability dates for 1098.
            var taxForm1098Years = await DataReader.BulkReadRecordAsync<TaxForm1098Years>("WITH TF98Y.TAX.FORM EQ '" + parm1098Contract.P1098TTaxForm + "'");
            if (taxForm1098Years != null)
            {
                var taxForm1098Status = await DataReader.ReadRecordAsync<TaxFormStatus>(parm1098Contract.P1098TTaxForm);
                // Validate the availability dates for 1098.
                foreach (var taxFormYear in taxForm1098Years)
                {
                    try
                    {
                        if (taxFormYear.Tf98yTaxYear == null)
                        {
                            throw new NullReferenceException("Tf98yTaxYear cannot be null.");
                        }
                        if (taxFormYear.Tf98yWebEnabled == null)
                        {
                            throw new NullReferenceException("Tf98yWebEnabled cannot be null.");
                        }
                        var available = true;
                        if (taxFormYear.Tf98yWebEnabled.ToUpper().Equals("Y"))
                        {
                            // If the tax form status doesn't exist, then the tax form is not available.
                            if (taxForm1098Status == null)
                            {
                                LogDataError("TaxFormStatus", "taxForm1098Status", taxForm1098Status);
                                available = false;
                            }
                            // If the tax form status year exists and is equal to the tax form year from taxForm1098Years, then we have additional evaluation.
                            if (taxForm1098Status.TfsTaxYear != null && taxForm1098Status.TfsTaxYear == taxFormYear.Tf98yTaxYear.ToString())
                            {
                                // If there is no gen date, the tax form is not available.
                                if (taxForm1098Status.TfsGenDate == null)
                                {
                                    available = false;
                                }
                                // If the tax form status exists and is "GEN", "MOD", or "UNF" then the tax form is not available.
                                if (taxForm1098Status.TfsStatus != null &&
                                    (taxForm1098Status.TfsStatus.ToUpper() == "GEN" ||
                                    taxForm1098Status.TfsStatus.ToUpper() == "MOD" ||
                                    taxForm1098Status.TfsStatus.ToUpper() == "UNF"))
                                {
                                    available = false;
                                }
                            }
                        }
                        else
                        {
                            // If the tax form year is specified as not web enabled, the tax form is not available.
                            available = false;
                        }
                        // Create a tax form availability object and add it to the tax form configuration.
                        configuration.AddAvailability(new TaxFormAvailability(taxFormYear.Tf98yTaxYear.ToString(), available));
                    }
                    catch (NullReferenceException e)
                    {
                        LogDataError("TaxForm1098Years", "contract", taxFormYear, e, e.Message);
                    }
                }
            }
            return configuration;
        }
        #endregion

        /// <summary>
        /// Retrieve user profile configuration servicing old versions of the API.
        /// </summary>
        /// <returns>User profile configuration</returns>
        [Obsolete("This method services version of the API prior to 1.16.")]
        public async Task<UserProfileConfiguration> GetUserProfileConfigurationAsync()
        {
            UserProfileConfiguration configuration = new UserProfileConfiguration();

            Dflts dfltsRecord = null;
            try
            {
                dfltsRecord = GetDefaults();
            }
            catch (Exception e)
            {
                logger.Info(e, "Error retrieving DFLTS record");
                return configuration;
            }

            CorewebDefaults corewebDefaultsRecord = null;
            try
            {
                corewebDefaultsRecord = await GetCorewebDefaultsAsync();
            }
            catch (Exception e)
            {
                logger.Info(e, "Error retrieving COREWEB.DEFAULTS record");
                return configuration;
            }
            if (corewebDefaultsRecord != null && dfltsRecord != null)
            {
                configuration.UpdateAddressTypeConfiguration(string.Equals(corewebDefaultsRecord.CorewebAllAddrViewable, "Y", StringComparison.OrdinalIgnoreCase),
                    corewebDefaultsRecord.CorewebAddressViewTypes,
                    string.Equals(corewebDefaultsRecord.CorewebAddrUpdatable, "Y", StringComparison.OrdinalIgnoreCase),
                    dfltsRecord.DfltsWebAdrelType);

                configuration.UpdateEmailTypeConfiguration(string.Equals(corewebDefaultsRecord.CorewebAllEmailViewable, "Y", StringComparison.OrdinalIgnoreCase),
                    corewebDefaultsRecord.CorewebEmailViewTypes,
                    string.Equals(corewebDefaultsRecord.CorewebAllEmailUpdatable, "Y", StringComparison.OrdinalIgnoreCase),
                    corewebDefaultsRecord.CorewebEmailUpdtTypes);

                configuration.UpdatePhoneTypeConfiguration(string.Equals(corewebDefaultsRecord.CorewebAllPhoneViewable, "Y", StringComparison.OrdinalIgnoreCase),
                    corewebDefaultsRecord.CorewebPhoneViewTypes,
                    corewebDefaultsRecord.CorewebPhoneUpdtTypes);

                if (corewebDefaultsRecord.CorewebUserProfileText != null)
                {
                    configuration.Text = corewebDefaultsRecord.CorewebUserProfileText;
                }

                configuration.CanUpdateAddressWithoutPermission = string.Equals(corewebDefaultsRecord.CorewebAddrUpdtNoPerm, "Y", StringComparison.OrdinalIgnoreCase);
                configuration.CanUpdateEmailWithoutPermission = string.Equals(corewebDefaultsRecord.CorewebEmailUpdtNoPerm, "Y", StringComparison.OrdinalIgnoreCase);
                configuration.CanUpdatePhoneWithoutPermission = string.Equals(corewebDefaultsRecord.CorewebPhoneUpdtNoPerm, "Y", StringComparison.OrdinalIgnoreCase);
            }
            return configuration;
        }

        /// <summary>
        /// Retrieve user profile configuration.
        /// </summary>
        /// <returns>User profile configuration</returns>
        public async Task<UserProfileConfiguration2> GetUserProfileConfiguration2Async()
        {
            UserProfileConfiguration2 configuration = new UserProfileConfiguration2();

            CorewebDefaults corewebDefaultsRecord = null;
            try
            {
                corewebDefaultsRecord = await GetCorewebDefaultsAsync();
            }
            catch (Exception e)
            {
                logger.Info(e, "Error retrieving COREWEB.DEFAULTS record");
                return configuration;
            }
            if (corewebDefaultsRecord != null)
            {
                configuration.UpdateAddressTypeConfiguration(string.Equals(corewebDefaultsRecord.CorewebAllAddrViewable, "Y", StringComparison.OrdinalIgnoreCase),
                    corewebDefaultsRecord.CorewebAddressViewTypes,
                    corewebDefaultsRecord.CorewebAddressUpdtTypes);

                configuration.UpdateEmailTypeConfiguration(string.Equals(corewebDefaultsRecord.CorewebAllEmailViewable, "Y", StringComparison.OrdinalIgnoreCase),
                    corewebDefaultsRecord.CorewebEmailViewTypes,
                    string.Equals(corewebDefaultsRecord.CorewebAllEmailUpdatable, "Y", StringComparison.OrdinalIgnoreCase),
                    corewebDefaultsRecord.CorewebEmailUpdtTypes);

                configuration.UpdatePhoneTypeConfiguration(string.Equals(corewebDefaultsRecord.CorewebAllPhoneViewable, "Y", StringComparison.OrdinalIgnoreCase),
                    corewebDefaultsRecord.CorewebPhoneViewTypes,
                    corewebDefaultsRecord.CorewebPhoneUpdtTypes);

                if (corewebDefaultsRecord.CorewebUserProfileText != null)
                {
                    configuration.Text = corewebDefaultsRecord.CorewebUserProfileText;
                }

                configuration.CanUpdateAddressWithoutPermission = string.Equals(corewebDefaultsRecord.CorewebAddrUpdtNoPerm, "Y", StringComparison.OrdinalIgnoreCase);
                configuration.CanUpdateEmailWithoutPermission = string.Equals(corewebDefaultsRecord.CorewebEmailUpdtNoPerm, "Y", StringComparison.OrdinalIgnoreCase);
                configuration.CanUpdatePhoneWithoutPermission = string.Equals(corewebDefaultsRecord.CorewebPhoneUpdtNoPerm, "Y", StringComparison.OrdinalIgnoreCase);
            }
            return configuration;
        }

        /// <summary>
        /// Gets the Emergency Information Configuration from Colleague.
        /// </summary>
        /// <returns>EmergencyInformationConfiguration</returns>
        public async Task<EmergencyInformationConfiguration> GetEmergencyInformationConfigurationAsync()
        {
            try
            {
                var corewebDefaults = await GetCorewebDefaultsAsync();
                var hideHealthConditions = string.Equals(corewebDefaults.CorewebEmerHideHlthCond, "Y", StringComparison.OrdinalIgnoreCase);
                var hideOtherInformation = string.Equals(corewebDefaults.CorewebEmerHideOtherInfo, "Y", StringComparison.OrdinalIgnoreCase);
                var requireContact = string.Equals(corewebDefaults.CorewebEmerRequireContact, "Y", StringComparison.OrdinalIgnoreCase);
                var allowOptOut = string.Equals(corewebDefaults.CorewebEmerAllowOptout, "Y", StringComparison.OrdinalIgnoreCase);
                EmergencyInformationConfiguration configuration = new EmergencyInformationConfiguration(hideHealthConditions, hideOtherInformation, requireContact, allowOptOut);
                return configuration;
            }
            catch (Exception e)
            {
                logger.Info(e, "Error retrieving COREWEB.DEFAULTS record for EmergencyInformationConfiguration");
                throw;
            }
        }

        /// <summary>
        /// Return a RestrictionConfiguration entity that contains a list of entities (SeverityStyleMapping).
        /// </summary>
        /// <returns>awaitable Restriction Configuration</returns>
        public async Task<RestrictionConfiguration> GetRestrictionConfigurationAsync()
        {
            RestrictionConfiguration configuration = new RestrictionConfiguration();

            CorewebDefaults corewebDefaultsRecord = null;
            try
            {
                corewebDefaultsRecord = await GetCorewebDefaultsAsync();
            }
            catch (Exception e)
            {
                logger.Info(e, "Error retrieving COREWEB.DEFAULTS record");
                return configuration;
            }

            if ((corewebDefaultsRecord.CorewebSeverityStart != null) &&
                (corewebDefaultsRecord.CorewebSeverityEnd != null) &&
                (corewebDefaultsRecord.CorewebStyle != null))
            {
                for (int i = 0; i < corewebDefaultsRecord.CorewebSeverityStart.Count; i++)
                {
                    AlertStyle style;

                    switch (corewebDefaultsRecord.CorewebStyle[i])
                    {
                        case "C":
                            style = AlertStyle.Critical;
                            break;
                        case "W":
                            style = AlertStyle.Warning;
                            break;
                        case "I":
                            style = AlertStyle.Information;
                            break;
                        default:
                            throw new InvalidCastException("Unknown");
                    }

                    var start = corewebDefaultsRecord.CorewebSeverityStart[i];
                    var end = corewebDefaultsRecord.CorewebSeverityEnd[i];
                    configuration.AddItem(new SeverityStyleMapping(start, end, style));
                }
            }
            return configuration;
        }
        /// <summary>
        /// Get Pilot configuration
        /// </summary>
        /// <returns>Pilot configuration</returns>
        public async Task<PilotConfiguration> GetPilotConfigurationAsync()
        {
            PilotParms pilotParms = await GetPilotParmsAsync();
            PilotConfiguration pilotConfiguration = new PilotConfiguration();
            pilotConfiguration.PrimaryPhoneTypes = pilotParms.PilHomePhoneTypes;
            pilotConfiguration.SmsPhoneTypes = pilotParms.PilCellPhoneTypes;
            return pilotConfiguration;
        }

        /// <summary>
        /// Retrieves privacy configuration
        /// </summary>
        /// <returns>Privacy configuration</returns>
        public async Task<PrivacyConfiguration> GetPrivacyConfigurationAsync()
        {
            var defaults = await GetDefaultsAsync();
            return new PrivacyConfiguration(defaults.DfltsRecordDenialMsg);
        }

        /// <summary>
        /// Retrieves organizational relationship configuration
        /// </summary>
        /// <returns>Organizational relationship configuration</returns>
        public async Task<OrganizationalRelationshipConfiguration> GetOrganizationalRelationshipConfigurationAsync()
        {
            var relationshipCategories = await GetRelationshipCategoriesAsync();
            var organizationRelationshipConfiguration = new OrganizationalRelationshipConfiguration();
            // Populate type mapping with all special types
            foreach (OrganizationalRelationshipType orgRelType in Enum.GetValues(typeof(OrganizationalRelationshipType)))
            {
                organizationRelationshipConfiguration.RelationshipTypeCodeMapping.Add(orgRelType, new List<string>());
            }

            // Map type codes to special types
            foreach (var relationshipCategory in relationshipCategories.ValsEntityAssociation)
            {
                if (relationshipCategory.ValActionCode1AssocMember == _orgIndicator)
                {
                    organizationRelationshipConfiguration.RelationshipTypeCodeMapping[OrganizationalRelationshipType.Manager].Add(relationshipCategory.ValInternalCodeAssocMember);
                }

            }
            return organizationRelationshipConfiguration;
        }

        #endregion

        #region Private Methods

        private ExternalMapping BuildExternalMapping(ElfTranslateTables table)
        {
            if (table != null)
            {
                try
                {
                    ExternalMapping externalMapping = new ExternalMapping(table.Recordkey, table.ElftDesc)
                    {
                        OriginalCodeValidationField = table.ElftOrigCodeField,
                        NewCodeValidationField = table.ElftNewCodeField
                    };

                    if (table.ElftblEntityAssociation != null && table.ElftblEntityAssociation.Count > 0)
                    {
                        foreach (var item in table.ElftblEntityAssociation)
                        {
                            externalMapping.AddItem(new ExternalMappingItem(item.ElftOrigCodesAssocMember)
                            {
                                NewCode = item.ElftNewCodesAssocMember,
                                ActionCode1 = item.ElftActionCodes1AssocMember,
                                ActionCode2 = item.ElftActionCodes2AssocMember,
                                ActionCode3 = item.ElftActionCodes3AssocMember,
                                ActionCode4 = item.ElftActionCodes4AssocMember
                            });
                        }
                    }

                    return externalMapping;
                }
                catch (Exception ex)
                {
                    string inError = "External mapping '" + table.Recordkey + "' corrupt";
                    LogDataError("External mapping", table.Recordkey, table, ex, inError);
                    throw new ConfigurationException(inError);
                }
            }
            return null;
        }

        private DefaultsConfiguration BuildDefaultsConfiguration()
        {
            DefaultsConfiguration configuration = new DefaultsConfiguration();
            var ldmDefaults = GetLdmDefaults();
            var defaults = GetDefaults();

            if (ldmDefaults != null && defaults != null)
            {
                configuration.AddressDuplicateCriteriaId = ldmDefaults.LdmdAddrDuplCriteria;
                configuration.AddressTypeMappingId = ldmDefaults.LdmdAddrTypeMapping;
                configuration.EmailAddressTypeMappingId = ldmDefaults.LdmdEmailTypeMapping;
                configuration.PersonDuplicateCriteriaId = ldmDefaults.LdmdPersonDuplCriteria;
                configuration.SubjectDepartmentMappingId = ldmDefaults.LdmdSubjDeptMapping;

                if (ldmDefaults.LdmdCollDefaultsEntityAssociation != null && ldmDefaults.LdmdCollDefaultsEntityAssociation.Count() > 0)
                {
                    foreach (var defaultMapping in ldmDefaults.LdmdCollDefaultsEntityAssociation)
                    {
                        if (defaultMapping != null && !string.IsNullOrEmpty(defaultMapping.LdmdCollFieldNameAssocMember) && !string.IsNullOrEmpty(defaultMapping.LdmdCollDefaultValueAssocMember))
                        {
                            configuration.AddDefaultMapping(defaultMapping.LdmdCollFieldNameAssocMember, defaultMapping.LdmdCollDefaultValueAssocMember);
                        }
                        else
                        {
                            string inError = "Defaults configuration mapping";
                            LogDataError(inError, ldmDefaults.Recordkey, ldmDefaults);
                            throw new ConfigurationException(inError);
                        }
                    }
                }
                configuration.CampusCalendarId = defaults.DfltsCampusCalendar;
                configuration.HostInstitutionCodeId = defaults.DfltsHostInstitutionCode;
            }
            return configuration;
        }

        private LdmDefaults GetLdmDefaults()
        {
            var ldmDefaults = DataReader.ReadRecord<LdmDefaults>("CORE.PARMS", "LDM.DEFAULTS");
            if (ldmDefaults == null)
            {
                throw new ConfigurationException("CDM configuration setup not complete.");
            }
            return ldmDefaults;
        }

        private Dflts GetDefaults()
        {
            var defaults = DataReader.ReadRecord<Dflts>("CORE.PARMS", "DEFAULTS");
            if (defaults == null)
            {
                throw new ConfigurationException("Default configuration setup not complete.");
            }
            return defaults;
        }

        /// <summary>
        /// Gets 
        /// </summary>
        /// <returns></returns>
        private async Task<Data.Base.DataContracts.Dflts> GetDefaultsAsync()
        {
            var defaults = await GetOrAddToCacheAsync<Data.Base.DataContracts.Dflts>("Dflts",
                async () =>
                {
                    var dflts = await DataReader.ReadRecordAsync<Data.Base.DataContracts.Dflts>("CORE.PARMS", "DEFAULTS");
                    if (dflts == null)
                    {
                        throw new ConfigurationException("Default configuration setup not complete.");
                    }
                    return dflts;
                }
            );
            return defaults;
        }

        /// <summary>
        /// Builds a list of EthosSecurity Domain entities from a collection of Colleague DataContract ethossecurity objects
        /// </summary>
        /// <param name="ethosSecurities"></param>
        /// <returns>List of EthosSecurity Domain entities</returns>
        private async Task<IEnumerable<Ellucian.Colleague.Domain.Base.Entities.EthosSecurity>> BuildEthosSecurityList(Collection<DataContracts.EthosSecurity> ethosSecurities)
        {
            var returnList = new List<Ellucian.Colleague.Domain.Base.Entities.EthosSecurity>();
            ethosSecurities.ForEach(e =>
            {
                var secDefList = new List<EthosSecurityDefinitions>();
                e.EthsSecDefEntityAssociation.ForEach(s =>
                {
                    bool required = s.EthsSecurityLevelAssocMember.Equals("P");
                    secDefList.Add(new EthosSecurityDefinitions(s.EthsPropertiesAssocMember, s.EthsUsersAssocMember, s.EthsRolesAssocMember, s.EthsPermissionsAssocMember, required));
                });
                returnList.Add(new Ellucian.Colleague.Domain.Base.Entities.EthosSecurity(e.EthsApiName, secDefList));
            });

            return returnList;
        }

        /// <summary>
        /// Builds an integration configuration
        /// </summary>
        /// <param name="integrationConfigurationId">Integration configuration ID</param>
        /// <returns>An <see cref="IntegrationConfiguration">integration configuration</see></returns>
        private IntegrationConfiguration BuildIntegrationConfiguration(string integrationConfigurationId)
        {
            IntegrationConfiguration configuration = null;
            var request = new GetIntegrationConfigRequest()
            {
                CdmIntegrationId = integrationConfigurationId.ToUpperInvariant()
            };
            GetIntegrationConfigResponse response = transactionInvoker.Execute<GetIntegrationConfigRequest, GetIntegrationConfigResponse>(request);

            if (response.ErrorMsgs != null && response.ErrorMsgs.Count > 0)
            {
                var message = string.Join(";", response.ErrorMsgs);
                throw new InvalidOperationException(message);
            }

            List<ResourceBusinessEventMapping> mappings = new List<ResourceBusinessEventMapping>();
            if (response.ApiBusEventMap != null && response.ApiBusEventMap.Count() > 0)
            {
                foreach (var map in response.ApiBusEventMap)
                {
                    mappings.Add(new ResourceBusinessEventMapping(map.CintApiResource, map.CintApiRsrcSchemaVer.GetValueOrDefault(),
                        map.CintApiPath, map.CintApiBusEvents));
                }
            }

            System.Uri uri = new System.Uri(response.CintServerBaseUrl);

            int guidLifespan = response.CintGuidLifespan != null ? response.CintGuidLifespan.GetValueOrDefault() : 30;

            configuration = new IntegrationConfiguration(response.CdmIntegrationId, response.CintDesc, uri.Host, (uri.Scheme == "https"), uri.Port,
                response.CintServerUsername, response.CintServerPassword, response.CintBusEventExchange,
                response.CintBusEventQueue, response.CintOutboundExchange, response.CintInboundExchange,
                response.CintInboundQueue, response.CintApiUsername, response.CintApiPassword,
                response.CintApiErp, ConvertDebugLevelStringToDebugLevelEnumValue(response.CintDebugLevel), guidLifespan, mappings)
            {
                AmqpMessageServerVirtualHost = response.CintServerVirtHost,
                AmqpMessageServerConnectionTimeout = response.CintServerTimeout.GetValueOrDefault(),
                AutomaticallyRecoverAmqpMessages = response.CintServerAutorecoverFlag.GetValueOrDefault(),
                AmqpMessageServerHeartbeat = response.CintServerHeartbeat.GetValueOrDefault(),
                UseIntegrationHub = response.CintUseIntegrationHub ?? false,
                ApiKey = response.CintHubApiKey,
                TokenUrl = response.CintHubTokenUrl,
                PublishUrl = response.CintHubPublishUrl,
                SubscribeUrl = response.CintHubSubscribeUrl,
                ErrorUrl = response.CintHubErrorUrl,
                HubMediaType = response.CintHubMediaType
            };

            if (response.CintBusEventRoutingKeys != null && response.CintBusEventRoutingKeys.Count > 0)
            {
                configuration.BusinessEventRoutingKeys.AddRange(response.CintBusEventRoutingKeys);
            }

            if (response.CintInboundRoutingKeys != null && response.CintInboundRoutingKeys.Count > 0)
            {
                configuration.InboundExchangeRoutingKeys.AddRange(response.CintInboundRoutingKeys);
            }

            return configuration;
        }

        /// <summary>
        /// Converts a Debug Level string to a corresponding AdapterDebugLevel enumeration value
        /// </summary>
        /// <param name="debugLevel">Debug Level</param>
        /// <returns>AdapterDebugLevel enumeration value</returns>
        private AdapterDebugLevel ConvertDebugLevelStringToDebugLevelEnumValue(string debugLevel)
        {
            AdapterDebugLevel level;
            switch (debugLevel)
            {
                case "FATAL":
                    level = AdapterDebugLevel.Fatal;
                    break;
                case "ERROR":
                default:
                    level = AdapterDebugLevel.Error;
                    break;
                case "WARN":
                    level = AdapterDebugLevel.Warning;
                    break;
                case "DEBUG":
                    level = AdapterDebugLevel.Debug;
                    break;
                case "TRACE":
                    level = AdapterDebugLevel.Trace;
                    break;
                case "INFO":
                    level = AdapterDebugLevel.Information;
                    break;
            }
            return level;
        }

        /// <summary>
        /// Gets a corewebdefault data contract
        /// </summary>
        /// <returns>CorewebDefaults data contract object</returns>
        private async Task<CorewebDefaults> GetCorewebDefaultsAsync()
        {
            return await GetOrAddToCacheAsync<CorewebDefaults>("CorewebDefaults", async () =>
            {
                var corewebDefaults = await DataReader.ReadRecordAsync<CorewebDefaults>("CORE.PARMS", "COREWEB.DEFAULTS");
                if (corewebDefaults != null)
                {
                    return corewebDefaults;
                }
                else
                {
                    logger.Info("Null CorewebDefaults record returned from database");
                    return new CorewebDefaults();
                }
            });
        }

        private async Task<PilotParms> GetPilotParmsAsync()
        {
            PilotParms pilotParms = await DataReader.ReadRecordAsync<PilotParms>("ST.PARMS", "PILOT.DEFAULTS");

            if (pilotParms == null)
            {
                // PILOT.PARMS must exist for Colleague/Pilot integration to function properly
                throw new ConfigurationException("Pilot Parameters setup not complete on PIPA form in Colleague.");
            }
            return pilotParms;
        }

        private async Task<ApplValcodes> GetRelationshipCategoriesAsync()
        {

            return await GetOrAddToCacheAsync<ApplValcodes>("RelationshipCategories",
                async () =>
                {
                    ApplValcodes relationshipTable = await DataReader.ReadRecordAsync<ApplValcodes>("UT.VALCODES", "RELATIONSHIP.CATEGORIES");
                    if (relationshipTable == null)
                    {
                        var errorMessage = "Unable to access RELATIONSHIP.CATEGORIES valcode table.";
                        logger.Info(errorMessage);
                        throw new Exception(errorMessage);
                    }
                    return relationshipTable;
                }, Level1CacheTimeoutValue);
        }
        #endregion


        
    }
}