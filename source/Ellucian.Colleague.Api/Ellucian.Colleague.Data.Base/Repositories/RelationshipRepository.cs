﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;


namespace Ellucian.Colleague.Data.Base.Repositories
{
    /// <summary>
    /// Accesses Colleague for a person's relationship information.
    /// </summary>
    [RegisterType]
    public class RelationshipRepository : BaseColleagueRepository, IRelationshipRepository
    {
        private List<Domain.Base.Entities.Relationship> _relationships;

        #region Non EEDM

        /// <summary>
        /// Initializes a new instance of the <see cref="RelationshipRepository"/> class.
        /// </summary>
        /// <param name="cacheProvider">The cache provider.</param>
        /// <param name="transactionFactory">The transaction factory.</param>
        /// <param name="logger">The logger.</param>
        public RelationshipRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            :base(cacheProvider, transactionFactory, logger)
        {
        }

        /// <summary>
        /// Gets the relationships between one entity (person or organization) and another entity
        /// </summary>
        /// <param name="id">The identifier of the entity of interest</param>
        /// <returns>An enumeration of <see cref="Domain.Base.Entities.Relationship"/></returns>
        public async Task<IEnumerable<Domain.Base.Entities.Relationship>> GetPersonRelationshipsAsync(string id)
        {
            if (_relationships != null)
            {
                return _relationships;
            }
            else
            {
                if (string.IsNullOrEmpty(id))
                {
                    throw new ArgumentNullException("id");
                }

                _relationships = new List<Domain.Base.Entities.Relationship>();
                // explicit ordering required by the business rules that determine the implicit primary relationship when
                // there is no explicit primary elationship defined.
                var rels = await DataReader.BulkReadRecordAsync<Data.Base.DataContracts.Relationship>("RELATIONSHIP", string.Format("RS.ID1 = {0} OR RS.ID2 = {1} BY RS.START.DATE BY RELATIONSHIP.ID", id, id));
                if (rels != null)
                {
//                    _relationships.AddRange(rels.Select(x => new Domain.Base.Entities.Relationship(x.RsId1, x.RsId2, x.RsRelationType, x.RsPrimaryRelationshipFlag.ToUpper() == "Y", x.RsStartDate, x.RsEndDate)));
                    _relationships.AddRange(rels.Select(x => BuildRelationship(x)));
                }
                return _relationships;    
            }
        }

        /// <summary>
        /// Gets a collection of unique IDs for persons and organizations with whom the supplied person has a relationship
        /// </summary>
        /// <param name="id">The identifier of the entity of interest</param>
        /// <returns>Collection of unique IDs for persons and organizations with whom the supplied person has a relationship</returns>
        public async Task<IEnumerable<string>> GetRelatedPersonIdsAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }

            List<string> personIds = new List<string>();
            var relRecords = await this.DataReader.BulkReadRecordAsync<Data.Base.DataContracts.Relationship>("RELATIONSHIP", string.Format("RS.ID1 = {0} OR RS.ID2 = {1} BY RS.START.DATE BY RELATIONSHIP.ID", id, id));
            if (relRecords == null || !relRecords.Any())
            {
                return personIds;
            }

            personIds.AddRange(relRecords.Select(r => r.RsId1));
            personIds.AddRange(relRecords.Select(r => r.RsId2));
            personIds = personIds.Distinct().ToList();

            return personIds;
        }

        /// <summary>
        /// Creates the given relationship between the two given entities
        /// </summary>
        /// <param name="isTheId">P1, in the phrase 'P1 is the "relationship type" of P2'</param>
        /// <param name="relationshipType">The relationship in the phrase 'P1 is the "relationship type" of P2'</param>
        /// <param name="ofTheId">P2, in the phrase 'P1 is the "relationship type" of P2 </param>
        /// <returns>the created <see cref="Domain.Base.Entities.Relationship"/></returns>
        public async Task<Domain.Base.Entities.Relationship> PostRelationshipAsync(string isTheId, string relationshipType, string ofTheId)
        {
            if (string.IsNullOrEmpty(isTheId))
            {
                throw new ArgumentNullException("isTheId");
            }
            if (string.IsNullOrEmpty(relationshipType))
            {
                throw new ArgumentNullException("relationshipType");
            }
            if (string.IsNullOrEmpty(ofTheId))
            {
                throw new ArgumentNullException("ofTheId");
            }

            var request = new CreateRelationshipsRequest();
            request.RelationsToCreate = new List<RelationsToCreate>() {new RelationsToCreate() { IsTheIds = isTheId,RelationTypes = relationshipType, OfTheIds = ofTheId } };

            var response = new CreateRelationshipsResponse();
            try
            {
                response = await transactionInvoker.ExecuteAsync<CreateRelationshipsRequest, CreateRelationshipsResponse>(request);
                if (response.ErrorInd || response.RelationshipIdentifiers == null || response.RelationshipIdentifiers.Count != 1)
                {
                    foreach (var msg in response.Messages)
                    {
                        logger.Error(msg);
                    }

                    var message = "Error failure encountered by Colleague Transaction CREATE.RELATIONSHIPS.";
                    logger.Error(message);
                    throw new RepositoryException(message);
                }
                var data = await DataReader.ReadRecordAsync<Relationship>(response.RelationshipIdentifiers[0]);
                if (data == null)
                {
                    var message = "Could not read relationship record.";
                    logger.Error(message);
                    throw new RepositoryException(message);
                }

                return BuildRelationship (data);
            }
            catch (Exception ex)
                {
                var message = "Error processing Colleague Transaction CREATE.RELATIONSHIPS.";
                logger.Error(ex, message);
                throw new RepositoryException(message, ex);
            }
        }

        private Domain.Base.Entities.Relationship BuildRelationship(Relationship data)
        {
            return new Domain.Base.Entities.Relationship(data.RsId1, data.RsId2, data.RsRelationType, data.RsPrimaryRelationshipFlag.ToUpper() == "Y", data.RsStartDate, data.RsEndDate) { Guid = data.RecordGuid };
        }

        #endregion

        #region EEDM Changes
        /// <summary>
        /// Returns all relationships
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        public async Task<Tuple<IEnumerable<Domain.Base.Entities.Relationship>, int>> GetAllAsync(int offset, int limit, bool bypassCache, List<string> guardianRelTypesWithInverse)
        {
            string criteria = "WITH RS.ID1.CORP.INDICATOR NE 'Y' AND RS.ID2.CORP.INDICATOR NE 'Y'";

            if (guardianRelTypesWithInverse != null && guardianRelTypesWithInverse.Any())
            {
                if (!string.IsNullOrEmpty(criteria))
                {
                    string guardianRelTypeCriteria = "";
                    foreach (var guardianRelType in guardianRelTypesWithInverse)
                    {
                        guardianRelTypeCriteria += string.Format(" AND WITH RS.RELATION.TYPE NE '{0}'", guardianRelType);
                    }
                    criteria = string.Format("{0}{1}", criteria, guardianRelTypeCriteria);
                }
            }

            string[] relationshipIds = null;
            if ((guardianRelTypesWithInverse == null || !guardianRelTypesWithInverse.Any()))
            {
                relationshipIds = await DataReader.SelectAsync("RELATIONSHIP", criteria);
            }
            else
            {
                relationshipIds = await DataReader.SelectAsync("RELATIONSHIP", criteria, guardianRelTypesWithInverse.ToArray());
            }

            var totalCount = relationshipIds.Count();

            Array.Sort(relationshipIds);

            var subList = relationshipIds.Skip(offset).Take(limit).ToArray();

            //Get paged data for relationship
            var pageData = await DataReader.BulkReadRecordAsync<Relationship>("RELATIONSHIP", subList);

            var combinedList = pageData.Select(i => i.RsId1).Union(pageData.Select(i => i.RsId2));

            var personCollection = await DataReader.BulkReadRecordAsync<Person>(combinedList.ToArray());

            //Get comment data
            var commentData = await DataReader.BulkReadRecordAsync<Relation>("RELATION", "WITH RELATION.COMMENTS NE ''");

            List<Domain.Base.Entities.Relationship> relList = new List<Domain.Base.Entities.Relationship>();

            foreach (var item in pageData)
            {
                Domain.Base.Entities.Relationship relationship = BuildRelationship(item);
                relationship.Status = item.RsStatus;
                relationship.Comment = BuildComment(item.RsId1 + "*" + item.RsId2, commentData);
                relationship.SubjectPersonGuid = GetGuid(item.RsId1, personCollection);
                relationship.RelationPersonGuid = GetGuid(item.RsId2, personCollection);
                relationship.SubjectPersonGender = GetGender(item.RsId1, personCollection);
                relationship.RelationPersonGender = GetGender(item.RsId2, personCollection);
                relList.Add(relationship);
            }
            return new Tuple<IEnumerable<Domain.Base.Entities.Relationship>,int>(relList, totalCount);
        }
       
        /// <summary>
        /// Get a list of personal relationships using criteria
        /// </summary>
        /// <returns>A list of personal relationships Entities</returns>
        public async Task<Tuple<IEnumerable<Domain.Base.Entities.Relationship>, int>> GetRelationshipsAsync(int offset, int limit, List<string> guardianRelTypesWithInverse, string subjectPerson = "", string relatedPerson = "",
            string directRelationshipType = "", string directRelationshipDetailId = "")
        {
            List<Domain.Base.Entities.Relationship> personalRelationships = new List<Domain.Base.Entities.Relationship>();
            string criteria = "WITH RS.ID1.CORP.INDICATOR NE 'Y' AND RS.ID2.CORP.INDICATOR NE 'Y'";
            if (!string.IsNullOrEmpty(subjectPerson))
            {
                criteria += " AND WITH RS.ID1 = '" + subjectPerson + "'";
            }
            if (!string.IsNullOrEmpty(relatedPerson))
            {
                if (!string.IsNullOrEmpty(criteria))
                {
                    criteria += " AND ";
                }
                criteria += "WITH RS.ID2 = '" + relatedPerson + "'";
            }
            if (!string.IsNullOrEmpty(directRelationshipType))
            {
                if (!string.IsNullOrEmpty(criteria))
                {
                    criteria += " AND ";
                }
                criteria += "WITH RS.RELATION.TYPE = '" + directRelationshipType + "'";
            }
            if (!string.IsNullOrEmpty(directRelationshipDetailId) && !directRelationshipDetailId.Equals(directRelationshipType))
            {
                if (!string.IsNullOrEmpty(criteria) && !string.IsNullOrEmpty(directRelationshipType))
                {
                    criteria += " OR ";
                }
                else if (!string.IsNullOrEmpty(criteria))
                {
                    criteria += " AND ";
                }
                criteria += "WITH RS.RELATION.TYPE = '" + directRelationshipDetailId + "'";
            }

            if (guardianRelTypesWithInverse != null && guardianRelTypesWithInverse.Any())
            {
                if (!string.IsNullOrEmpty(criteria))
                {
                    string guardianRelTypeCriteria = string.Empty;
                    foreach (var guardianRelType in guardianRelTypesWithInverse)
                    {
                        guardianRelTypeCriteria += string.Format(" AND WITH RS.RELATION.TYPE NE '{0}'", guardianRelType);
                    }
                    criteria = string.Format("{0}{1}", criteria, guardianRelTypeCriteria);
                }
            }

            int totalCount = 0;
            string[] relationshipIds = null;
            if (!string.IsNullOrEmpty(criteria))
            {
                if ((guardianRelTypesWithInverse == null || !guardianRelTypesWithInverse.Any()))
                {
                    relationshipIds = await DataReader.SelectAsync("RELATIONSHIP", criteria);
                }
                else
                {
                    relationshipIds = await DataReader.SelectAsync("RELATIONSHIP", criteria, guardianRelTypesWithInverse.ToArray());
                }

                totalCount = relationshipIds.Count();

                Array.Sort(relationshipIds);

                var subList = relationshipIds.Skip(offset).Take(limit).ToArray();
                var bulkData = await DataReader.BulkReadRecordAsync<Relationship>("RELATIONSHIP", subList);

                var relationshipData = new List<Relationship>();
                relationshipData.AddRange(bulkData);

                var combinedList = relationshipData.Select(i => i.RsId1).Union(relationshipData.Select(i => i.RsId2));

                //Get comment data
                var commentData = await DataReader.BulkReadRecordAsync<Relation>("RELATION", "WITH RELATION.COMMENTS NE ''");

                var personCollection = await DataReader.BulkReadRecordAsync<Person>(combinedList.ToArray());

                foreach (var item in relationshipData) {

                    Domain.Base.Entities.Relationship relationship = BuildRelationship(item);
                    //relationship.Guid = item.RecordGuid;
                    relationship.Status = item.RsStatus;
                    relationship.Comment = BuildComment(item.RsId1 + "*" + item.RsId2, commentData);
                    relationship.SubjectPersonGuid = GetGuid(item.RsId1, personCollection);
                    relationship.RelationPersonGuid = GetGuid(item.RsId2, personCollection);
                    relationship.SubjectPersonGender = GetGender(item.RsId1, personCollection);
                    relationship.RelationPersonGender = GetGender(item.RsId2, personCollection);
                    personalRelationships.Add(relationship);
                }
            }

            return new Tuple<IEnumerable<Domain.Base.Entities.Relationship>, int>(personalRelationships, totalCount);
        }

        /// <summary>
        /// Return relationship entity based on guid
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Domain.Base.Entities.Relationship> GetPersonRelationshipByIdAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("Relationship id is required.");
            }

            var relationshipId = await GetRecordKeyFromGuidAsync(id);

            if (string.IsNullOrEmpty(relationshipId))
            {
                throw new KeyNotFoundException("Personal relationship record not found for id: " + id);
            }

            var relationshipContract = await DataReader.ReadRecordAsync<Relationship>("RELATIONSHIP", relationshipId);
            if (relationshipContract == null)
            {
                throw new KeyNotFoundException("Did not find personal relationship for id: " + id);
            }
            var combinedList = new string[] { relationshipContract.RsId1, relationshipContract.RsId2 };
            var personCollection = await DataReader.BulkReadRecordAsync<Person>(combinedList.ToArray());

            var isCorporation = personCollection.Any(i => !string.IsNullOrEmpty(i.PersonCorpIndicator) && i.PersonCorpIndicator.Equals("Y", StringComparison.OrdinalIgnoreCase));
            if (isCorporation)
            {
                throw new InvalidOperationException("Personal relationship record not found for id: " + id);
            }

            var relationshipEnity = BuildRelationship(relationshipContract);
            relationshipEnity.Status = relationshipContract.RsStatus;
            relationshipEnity.SubjectPersonGuid = GetGuid(relationshipContract.RsId1, personCollection);
            relationshipEnity.RelationPersonGuid = GetGuid(relationshipContract.RsId2, personCollection);
            relationshipEnity.SubjectPersonGender = GetGender(relationshipContract.RsId1, personCollection);
            relationshipEnity.RelationPersonGender = GetGender(relationshipContract.RsId2, personCollection);

            var commentData = await DataReader.BulkReadRecordAsync<Relation>("RELATION", "WITH RELATION.COMMENTS NE ''");   //Get comment data
            relationshipEnity.Comment = BuildComment(relationshipContract.RsId1 + "*" + relationshipContract.RsId2, commentData);

            return relationshipEnity;
        }

        /// <summary>
        /// Returns all relationships
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        public async Task<Tuple<IEnumerable<Domain.Base.Entities.Relationship>, int>> GetAllGuardiansAsync(int offset, int limit, string person, List<string> guardianRelTypesWithInverse)
        {
            /*
                 var date = await this.GetUnidataFormattedDate(DateTime.Today.ToString());
            
                 Leaving it in here because in future there is a chance that these dates will be addded
                 string.Format("WITH RS.ID1.CORP.INDICATOR NE 'Y' AND RS.ID2.CORP.INDICATOR NE 'Y' AND (RS.END.DATE EQ '' OR RS.END.DATE GT '{0}')", date);
            */
            string criteria = "WITH RS.ID1.CORP.INDICATOR NE 'Y' AND RS.ID2.CORP.INDICATOR NE 'Y'";

            if (!string.IsNullOrEmpty(person))
            {
                string rsIdCriteria = "{0} AND (RS.ID1 EQ '{1}' OR RS.ID2 EQ '{1}')";
                criteria = string.Format(rsIdCriteria, criteria, person);
            }

            if (guardianRelTypesWithInverse != null && guardianRelTypesWithInverse.Any())
            {
                criteria = string.Format("{0} AND RS.RELATION.TYPE EQ '?'", criteria);
            }
           
            string[] relationshipIds = null;

            relationshipIds = await DataReader.SelectAsync("RELATIONSHIP", criteria, guardianRelTypesWithInverse.ToArray());

            var totalCount = relationshipIds.Count();

            Array.Sort(relationshipIds);

            var subList = relationshipIds.Skip(offset).Take(limit).ToArray();

            //Get paged data for relationship
            var pageData = await DataReader.BulkReadRecordAsync<Relationship>("RELATIONSHIP", subList);

            var combinedList = pageData.Select(i => i.RsId1).Union(pageData.Select(i => i.RsId2));

            var personCollection = await DataReader.BulkReadRecordAsync<Person>(combinedList.ToArray());

            List<Domain.Base.Entities.Relationship> relList = new List<Domain.Base.Entities.Relationship>();

            foreach (var item in pageData)
            {
                Domain.Base.Entities.Relationship relationship = BuildRelationship(item);
                relationship.Guid = item.RecordGuid;
                relationship.Status = item.RsStatus;
                relationship.SubjectPersonGuid = GetGuid(item.RsId1, personCollection);
                relationship.RelationPersonGuid = GetGuid(item.RsId2, personCollection);
                relList.Add(relationship);
            }
            return new Tuple<IEnumerable<Domain.Base.Entities.Relationship>, int>(relList, totalCount);
        }

        /// <summary>
        /// Gets person guardians based on id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Domain.Base.Entities.Relationship> GetPersonGuardianRelationshipByIdAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("Person guardian relationship id is required.");
            }
            var relationshipId = await GetRecordKeyFromGuidAsync(id);
            if (string.IsNullOrEmpty(relationshipId))
            {
                throw new KeyNotFoundException("Person guardian relationship record not found for id: " + id);
            }

            var relationshipContract = await DataReader.ReadRecordAsync<Relationship>("RELATIONSHIP", relationshipId);

            if (relationshipContract == null)
            {
                throw new KeyNotFoundException("Did not find personal relationship for id: " + id);
            }

            var combinedList = new string[] { relationshipContract.RsId1, relationshipContract.RsId2 };
            var personCollection = await DataReader.BulkReadRecordAsync<Person>(combinedList.ToArray());

            var relationshipEnity = BuildRelationship(relationshipContract);
            relationshipEnity.Guid = relationshipContract.RecordGuid;
            relationshipEnity.Status = relationshipContract.RsStatus;
            relationshipEnity.SubjectPersonGuid = GetGuid(relationshipContract.RsId1, personCollection);
            relationshipEnity.RelationPersonGuid = GetGuid(relationshipContract.RsId2, personCollection);

            return relationshipEnity;
        }

        /// <summary>
        /// Gets guid from person collection
        /// </summary>
        /// <param name="recordKey"></param>
        /// <param name="personCollection"></param>
        /// <returns></returns>
        private string GetGuid(string recordKey, Collection<Person> personCollection)
        {
            string guid = string.Empty;
            var record = personCollection.FirstOrDefault(i => i.Recordkey.Equals(recordKey, StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(i.RecordGuid));

            if (record != null && string.IsNullOrEmpty(record.RecordGuid))
            {
                throw new KeyNotFoundException("No guid found for record key: " + recordKey);
            }
            guid = record.RecordGuid;
            return guid;
        }

        /// <summary>
        /// Gets gender from person collection
        /// </summary>
        /// <param name="recordKey"></param>
        /// <param name="personCollection"></param>
        /// <returns></returns>
        private string GetGender(string recordKey, Collection<Person> personCollection)
        {
            string gender = string.Empty;
            var record = personCollection.FirstOrDefault(i => i.Recordkey.Equals(recordKey, StringComparison.OrdinalIgnoreCase) && !string.IsNullOrEmpty(i.Gender));
            if (record != null)
            {
                gender = record.Gender;
            }
            return gender;
        }

        /// <summary>
        /// Returns default guardian relationship types set up in CDHP
        /// </summary>
        /// <returns></returns>
        public async Task<List<string>> GetDefaultGuardianRelationshipTypesAsync(bool bypassCache)
        {
            if (bypassCache)
            {

                return await BuildDefaultGuardianRelationshipTypes();
            }
            else
            {
                return await GetOrAddToCacheAsync<List<string>>("ALLGUARDIANRELATIONTYPES", async () =>
                {
                    return await BuildDefaultGuardianRelationshipTypes();
                });
            }
        }

        /// <summary>
        /// Builds the list of guardian relationships strings
        /// </summary>
        /// <returns></returns>
        private async Task<List<string>> BuildDefaultGuardianRelationshipTypes()
        {
            List<string> defaultGuardianRelations = new List<string>();
            var ldmDefaults = await DataReader.ReadRecordAsync<LdmDefaults>("CORE.PARMS", "LDM.DEFAULTS");

            if (ldmDefaults.LdmdGuardianRelTypes.Any())
            {
                defaultGuardianRelations.AddRange(ldmDefaults.LdmdGuardianRelTypes.ToList());
            }

            return defaultGuardianRelations.Any() ? defaultGuardianRelations : null;
        }

        /// <summary>
        /// Builds comments
        /// </summary>
        /// <param name="id"></param>
        /// <param name="commentData"></param>
        /// <returns></returns>
        private string BuildComment(string id, IEnumerable<Relation> commentData)
        {
            string comment = null;
            var relation = commentData.FirstOrDefault(x => x.Recordkey.Equals(id, StringComparison.OrdinalIgnoreCase));
            if (relation != null)
            {
                comment = relation.RelationComments;
            }
            return comment;
        }

        /// <summary>
        /// Return a Unidata Formatted Date string from an input argument of string type
        /// </summary>
        /// <param name="date">String representing a Date</param>
        /// <returns>Unidata formatted Date string for use in Colleague Selection.</returns>
        public async Task<string> GetUnidataFormattedDate(string date)
        {
            var internationalParameters = await InternationalParametersAsync();
            var newDate = DateTime.Parse(date).Date;
            return UniDataFormatter.UnidataFormatDate(newDate, internationalParameters.HostShortDateFormat, internationalParameters.HostDateDelimiter);
        }

        //private ApplValcodes waitlistStatuses;
        private Ellucian.Colleague.Data.Base.DataContracts.IntlParams _internationalParameters;

        private async Task<Ellucian.Colleague.Data.Base.DataContracts.IntlParams> InternationalParametersAsync()
        {

            if (_internationalParameters == null)
            {
                // Overriding cache timeout to be Level1 Cache time out for data that rarely changes.
                _internationalParameters = await GetOrAddToCacheAsync<Ellucian.Colleague.Data.Base.DataContracts.IntlParams>("InternationalParameters",
                     async () =>
                     {
                         Data.Base.DataContracts.IntlParams intlParams = await DataReader.ReadRecordAsync<Data.Base.DataContracts.IntlParams>("INTL.PARAMS", "INTERNATIONAL");
                         if (intlParams == null)
                         {
                             var errorMessage = "Unable to access international parameters INTL.PARAMS INTERNATIONAL.";
                             logger.Info(errorMessage);
                             // If we cannot read the international parameters default to US with a / delimiter.
                             // throw new Exception(errorMessage);
                             Data.Base.DataContracts.IntlParams newIntlParams = new Data.Base.DataContracts.IntlParams();
                             newIntlParams.HostShortDateFormat = "MDY";
                             newIntlParams.HostDateDelimiter = "/";
                             intlParams = newIntlParams;
                         }
                         return intlParams;
                     }, Level1CacheTimeoutValue);
            }
            return _internationalParameters;

        }
        #endregion        
    }
}
