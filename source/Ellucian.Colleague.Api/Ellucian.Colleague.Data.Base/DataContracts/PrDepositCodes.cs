//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 3/13/2017 11:16:15 AM by user mdediana
//
//     Type: ENTITY
//     Entity: PR.DEPOSIT.CODES
//     Application: HR
//     Environment: colldv
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;
using Ellucian.Data.Colleague;

namespace Ellucian.Colleague.Data.Base.DataContracts
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract(Name = "PrDepositCodes")]
	[ColleagueDataContract(GeneratedDateTime = "3/13/2017 11:16:15 AM", User = "mdediana")]
	[EntityDataContract(EntityName = "PR.DEPOSIT.CODES", EntityType = "PHYS")]
	public class PrDepositCodes : IColleagueEntity
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		/// <summary>
		/// Record Key
		/// </summary>
		[DataMember]
		public string Recordkey { get; set; }
		
		public void setKey(string key)
		{
			Recordkey = key;
		}
		
		/// <summary>
		/// CDD Name: DDC.DESCRIPTION
		/// </summary>
		[DataMember(Order = 0, Name = "DDC.DESCRIPTION")]
		public string DdcDescription { get; set; }
		
		/// <summary>
		/// CDD Name: DDC.TRANSIT.NO
		/// </summary>
		[DataMember(Order = 1, Name = "DDC.TRANSIT.NO")]
		public string DdcTransitNo { get; set; }
		
		/// <summary>
		/// CDD Name: DDC.FIN.INST.NUMBER
		/// </summary>
		[DataMember(Order = 2, Name = "DDC.FIN.INST.NUMBER")]
		public string DdcFinInstNumber { get; set; }
		
		/// <summary>
		/// CDD Name: DDC.BR.TRANSIT.NUMBER
		/// </summary>
		[DataMember(Order = 3, Name = "DDC.BR.TRANSIT.NUMBER")]
		public string DdcBrTransitNumber { get; set; }
		
		/// <summary>
		/// CDD Name: DDC.IS.ARCHIVED
		/// </summary>
		[DataMember(Order = 12, Name = "DDC.IS.ARCHIVED")]
		public string DdcIsArchived { get; set; }
		
	
		// build up all the Associated objects and add them to the properties
		public void buildAssociations()
		{	
			   
		}
	}
	
	// EntityAssociation classes
}