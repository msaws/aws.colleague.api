//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 11/25/2014 12:48:15 PM by user cindystair
//
//     Type: ENTITY
//     Entity: NAME.ADDR.HIERARCHY
//     Application: CORE
//     Environment: dvcoll_wstst01
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;
using Ellucian.Data.Colleague;

namespace Ellucian.Colleague.Data.Base.DataContracts
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract(Name = "NameAddrHierarchy")]
	[ColleagueDataContract(GeneratedDateTime = "11/25/2014 12:48:15 PM", User = "cindystair")]
	[EntityDataContract(EntityName = "NAME.ADDR.HIERARCHY", EntityType = "PHYS")]
	public class NameAddrHierarchy : IColleagueEntity
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		/// <summary>
		/// Record Key
		/// </summary>
		[DataMember]
		public string Recordkey { get; set; }
		
		public void setKey(string key)
		{
			Recordkey = key;
		}
		
		/// <summary>
		/// CDD Name: NAH.NAME.HIERARCHY
		/// </summary>
		[DataMember(Order = 2, Name = "NAH.NAME.HIERARCHY")]
		public List<string> NahNameHierarchy { get; set; }
		
	
		// build up all the Associated objects and add them to the properties
		public void buildAssociations()
		{	
			   
		}
	}
	
	// EntityAssociation classes
}