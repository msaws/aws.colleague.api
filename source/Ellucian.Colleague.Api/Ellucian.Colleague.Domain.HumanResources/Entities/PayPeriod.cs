﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.HumanResources.Entities
{
    [Serializable]
    public class PayPeriod
    {
        /// <summary>
        /// The pay period start date
        /// </summary>
        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                if (value > endDate)
                {
                    throw new ArgumentException("StartDate cannot be later than EndDate");
                }
                startDate = value;
            }
        }
        private DateTime startDate;

        /// <summary>
        /// The pay period end date
        /// </summary>
        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                if (startDate > value)
                {
                    throw new ArgumentException("StartDate cannot be later than EndDate");
                }
                endDate = value;
            }
        }
        private DateTime endDate;


        public DateTimeOffset? EmployeeTimecardCutoffDateTime { get; set; }
        public DateTimeOffset? SupervisorTimecardCutoffDateTime { get; set; }

        public PayPeriodStatus Status { get { return status; } }
        private PayPeriodStatus status;
        
        /// <summary>
        /// Constructor for a PayPeriod object
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public PayPeriod(DateTime startDate, DateTime endDate)
        {
            if (startDate > endDate)
            {
                throw new ArgumentException("StartDate cannot be later than EndDate");
            }

            this.EndDate = endDate; 
            this.StartDate = startDate;
        }

        /// <summary>
        /// Constructor for a PayPeriod object
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="status"></param>
        public PayPeriod(DateTime startDate, DateTime endDate, DateTimeOffset? employeeTimecardCutoffDateTime, DateTimeOffset? supervisorTimecardCutoffDateTime, PayPeriodStatus status)
        {
            if (startDate > endDate)
            {
                throw new ArgumentException("StartDate cannot be later than EndDate");
            }

            this.EndDate = endDate;
            this.StartDate = startDate;
            this.EmployeeTimecardCutoffDateTime = employeeTimecardCutoffDateTime;
            this.SupervisorTimecardCutoffDateTime = supervisorTimecardCutoffDateTime;
            this.status = status;
        }

        /// <summary>
        /// Two pay period date ranges are equal when their start and end dates are equal
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var payPeriod = obj as PayPeriod;
            return payPeriod.StartDate == this.StartDate && payPeriod.EndDate == this.EndDate;
        }

        /// <summary>
        /// Hashcode representation of PayPeriod (StartDate & EndDate)
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return StartDate.GetHashCode() ^ EndDate.GetHashCode();
        }
    }
}
