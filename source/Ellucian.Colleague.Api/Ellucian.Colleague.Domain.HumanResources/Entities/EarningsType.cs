﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.HumanResources.Entities
{
    [Serializable]
    public class EarningsType
    {
        /// <summary>
        /// The database Id
        /// </summary>
        public string Id { get { return id; } }
        private readonly string id;

        /// <summary>
        /// The earnings type description
        /// </summary>
        public string Description { 
            get { return description; } 
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentNullException("value");
                }
                description = value;

            }
        }
        private string description;

        /// <summary>
        /// Is this earnings type active or not?
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// The category specifies whether earnings are regular, overtime, leave, college work study or miscellaneous
        /// </summary>
        public EarningsCategory Category { get { return category; } }
        private EarningsCategory category;

        /// <summary>
        /// The method specifies whether earnings are for leave accrued, leave taken, or time not paid. Can be null.
        /// </summary>
        public EarningsMethod Method { get { return method; } }
        private EarningsMethod method;

        /// <summary>
        /// Instantiate an EarningsType object
        /// </summary>
        /// <param name="id">The Id of the earnings type.  Can be null or empty if creating a new earnings type.</param>
        /// <param name="description">The description of the earnings type.  Required.</param>
        /// <param name="isActive">True: earnings type is active; False: earnings type is inactive.</param>
        /// <param name="category">The earnings type category (Regular, Overtime, Leave, College Work Study, Miscellaneous)</param>
        public EarningsType(string id, string description, bool isActive, EarningsCategory category, EarningsMethod method)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullException("id");
            }
            
            if (string.IsNullOrWhiteSpace(description))
            {
                throw new ArgumentNullException("description");
            }

            this.id = id;
            this.description = description;
            this.IsActive = isActive;
            this.category = category;
            this.method = method;
        }

        /// <summary>
        /// Two earnings types are equal when their ids are equal
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals (object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var earningsType = obj as EarningsType;
            return earningsType.Id == this.Id;
        }
        
        /// <summary>
        /// Hascode representation of EarningsType (Id)
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }

        public override string ToString()
        {
            return Description + "-" + Id;
        }
    }
}
