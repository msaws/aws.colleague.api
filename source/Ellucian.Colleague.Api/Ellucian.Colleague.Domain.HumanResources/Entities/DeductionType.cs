﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Domain.Entities;
using System;

namespace Ellucian.Colleague.Domain.HumanResources.Entities
{
    /// <summary>
    /// Deduction type.
    /// </summary>
    [Serializable]
    public class DeductionType: GuidCodeItem
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DeductionType"/> class.
        /// </summary>
        public DeductionType(string guid, string code, string description)
            : base(guid, code, description)
        {
        }
    }
}
