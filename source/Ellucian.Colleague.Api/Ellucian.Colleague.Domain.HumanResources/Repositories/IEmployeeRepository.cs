﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Domain.HumanResources.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.HumanResources.Repositories
{
    public interface IEmployeeRepository
    {
        /// <summary>
        /// Get Employees objects for all employees bypassing cache and reading directly from the database.
        /// </summary>
        /// <param name="offset">Offset for record index on page reads.</param>
        /// <param name="limit">Take number of records on page reads.</param>
        /// <param name="person">Person id filter.</param>
        /// <param name="campus">Primary campus or location filter.</param>
        /// <param name="status">Status ("active", "terminated", or "leave") filter.</param>
        /// <param name="startOn">Start on a specific date filter.</param>
        /// <param name="endOn">End on a specific date filter.</param>
        /// <param name="rehireableStatus">Rehireable status ("eligible" or "ineligible") filter.</param>
        /// <returns>Tuple of Employee Entity objects <see cref="Employee"/> and a count for paging.</returns>
        Task<Tuple<IEnumerable<Employee>, int>> GetEmployeesAsync(int offset, int limit, string person = "",
            string campus = "", string status = "", string startOn = "", string endOn = "", string rehireableStatusEligibility = "", string rehireableStatusType = "");

        /// <summary>
        /// Get Employees objects for all employees.
        /// </summary>   
        /// <param name="id">guid of the employees record.</param>
        /// <returns>Employee Entity <see cref="Employee"./></returns>
        Task<Employee> GetEmployeeByIdAsync(string id);

        /// <summary>
        /// Get Unidata formatted date for filters.
        /// </summary>   
        /// <param name="date">date </param>
        /// <returns>date in undiata format</returns>
        Task<string> GetUnidataFormattedDate(string date);
    }
}
