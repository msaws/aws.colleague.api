﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System.Threading.Tasks;
using Ellucian.Colleague.Domain.HumanResources.Entities;

namespace Ellucian.Colleague.Domain.HumanResources.Repositories
{
    /// <summary>
    ///  Define the method signatures for a TaxFormPdfDataRepository
    /// </summary>
    public interface IHumanResourcesTaxFormPdfDataRepository
    {
        /// <summary>
        /// Get the W-2 data for a PDF.
        /// </summary>
        /// <param name="personId">ID of the person assigned to and requesting the W-2.</param>
        /// <param name="recordId">ID of the record containing the pdf data for a W-2 tax form</param>
        /// <returns>W-2 data for a pdf</returns>
        Task<FormW2PdfData> GetW2PdfAsync(string personId, string recordId);

        /// <summary>
        /// Get the 1095-C data for a PDF.
        /// </summary>
        /// <param name="personId">ID of the person assigned to and requesting the 1095-C.</param>
        /// <param name="recordId">ID of the 1095-C record.</param>
        /// <returns>1095-C data for a pdf</returns>
        Task<Form1095cPdfData> Get1095cPdfAsync(string personId, string recordId);
    }
}
