﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Domain.HumanResources.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.HumanResources.Repositories
{
    public interface IHumanResourcesReferenceDataRepository
    {
        /// <summary>
        /// Get a collection of employee classifications
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of employee classifications</returns>
        Task<IEnumerable<EmploymentClassification>> GetEmploymentClassificationsAsync(bool ignoreCache);

        /// <summary>
        /// Get a collection of employee proficiencies
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of employee proficiencies</returns>
        Task<IEnumerable<EmploymentProficiency>> GetEmploymentProficienciesAsync(bool ignoreCache);
        
        /// <summary>
        /// Get all deduction types
        /// </summary>
        /// <param name="bypassCache">Boolean to bypass use of cached data and go directly to disk.</param>
        /// <returns>Collection of <see cref="DeductionType"/></returns>
        Task<IEnumerable<DeductionType>> GetDeductionTypesAsync(bool bypassCache);

        /// <summary>
        /// Get all employment status ending reasons
        /// </summary>
        /// <param name="bypassCache">Boolean to bypass use of cached data and go directly to disk.</param>
        /// <returns>Collection of <see cref="EmploymentStatusEndingReason"/></returns>
        Task<IEnumerable<EmploymentStatusEndingReason>> GetEmploymentStatusEndingReasonsAsync(bool bypassCache);

        ///// <summary>
        ///// Get a collection of institution job supervisors
        ///// </summary>
        ///// <param name="ignoreCache">Bypass cache flag</param>
        ///// <returns>Collection of institution job supervisors</returns>
        //Task<IEnumerable<InstitutionJobSupervisor>> GetInstitutionJobSupervisorsAsync(bool ignoreCache);

        /// <summary>
        /// Get all payroll deduction arrangement change reasons
        /// </summary>
        /// <param name="bypassCache">Boolean to bypass use of cached data and go directly to disk.</param>
        /// <returns>Collection of <see cref="PayrollDeductionArrangementChangeReason"/></returns>
        Task<IEnumerable<PayrollDeductionArrangementChangeReason>> GetPayrollDeductionArrangementChangeReasonsAsync(bool bypassCache);
        
        /// <summary>
        /// Get all HR person statuses used in PERSTAT in Colleague
        /// </summary>
        /// <param name="bypassCache">Boolean to bypass use of cached data and go directly to disk.</param>
        /// <returns>Collection of <see cref="PersonStatuses"/></returns>
        Task<IEnumerable<PersonStatuses>> GetPersonStatusesAsync(bool bypassCache);

        /// <summary>
        /// Get a collection of job change reasons
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of job change reasons</returns>
        Task<IEnumerable<JobChangeReason>> GetJobChangeReasonsAsync(bool ignoreCache);
        
        /// <summary>
        /// Get a collection of rehire types
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of rehire types</returns>
        Task<IEnumerable<RehireType>> GetRehireTypesAsync(bool ignoreCache);
    }
}
