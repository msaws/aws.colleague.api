﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.HumanResources
{
    [Serializable]
    public static class HumanResourcesPermissionCodes
    {
        // Enables an employee to create, update, or delete payroll banking information 
        public const string EditPayrollBankingInformation = "EDIT.PAYROLL.BANKING.INFORMATION";

        /// <summary>
        /// In terms of Human Resources, this permission enables supervisers to view data for their supervisees.
        /// Enables a supervisor to approve and reject timecards over in the Time Management module, 
        /// </summary>
        public const string ViewSuperviseeData = "APPROVE.REJECT.TIME.ENTRY";

        /// <summary>
        /// In terms of Human Resources, this permission enables users to view data for employees. 
        /// </summary>
        public const string ViewEmployeeData = "VIEW.EMPLOYEE.DATA";

        /// <summary>
        /// In terms of Human Resources, this permission enables users to view institution position data. 
        /// </summary>
        public const string ViewInstitutionPosition = "VIEW.INSTITUTION.POSITION";

        /// <summary>
        /// In terms of Human Resources, this permission enables users to create/update institution jobs. 
        /// </summary>
        public const string CreateInstitutionJob = "CREATE.UPDATE.INSTITUTION.JOB";

        /// <summary>
        /// In terms of Human Resources, this permission enables users to view institution jobs data. 
        /// </summary>
        public const string ViewInstitutionJob = "VIEW.INSTITUTION.JOB";

        /// <summary>
        /// In terms of Human Resources, this permission enables users to view institution job supervisors data. 
        /// </summary>
        public const string ViewInstitutionJobSupervisor = "VIEW.INSTITUTION.JOB.SUPERVISORS";

        /// <summary>
        /// In terms of Human Resources, this permission enables users to create payroll deduction arrangements data. 
        /// </summary>
        public const string CreatePayrollDeductionArrangements = "CREATE.PAYROLL.DEDUCTION.ARRANGEMENTS";

        /// <summary>
        /// In terms of Human Resources, this permission enables users to view contribution-payroll-deductions. 
        /// </summary>
        public const string ViewContributionPayrollDeductions = "VIEW.CONTR.PAYROLL.DEDUCTIONS";
    }
}
