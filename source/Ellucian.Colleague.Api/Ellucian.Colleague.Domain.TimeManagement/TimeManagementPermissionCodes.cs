﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement
{
    /// <summary>
    /// Defines Permission codes for Time Management as constants
    /// </summary>
    public static class TimeManagementPermissionCodes
    {
        /// <summary>
        /// This permits a role to view, approve, and reject timecards. More specifically, 
        /// a user with a role associated to this permission can view all data associated to a timecard for all employees the user 
        /// supervises, and can create new timecard statuses for those timecards.
        /// </summary>
        public const string ApproveRejectEmployeeTimecard = "APPROVE.REJECT.TIME.ENTRY";
    }
}
