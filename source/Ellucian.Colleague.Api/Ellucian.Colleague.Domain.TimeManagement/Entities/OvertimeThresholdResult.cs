﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
        /// <summary>
    /// Represents part of the overtime calculation result that indicates 
    /// the earnings factor applied to overtime above a particular threshold.
    /// </summary>    
    [Serializable]
    public class OvertimeThresholdResult
    {
        /// <summary>
        /// The total overtime for the specified threshold
        /// </summary>
        public TimeSpan TotalOvertime { get { return totalOvertime; } }
        private readonly TimeSpan totalOvertime;

        /// <summary>
        /// The Id representing the EarningsType
        /// </summary>
        public string EarningsTypeId { get { return earningsTypeId; } }
        private readonly string earningsTypeId;

        /// <summary>
        /// The factor (pay multiplier) associated this threshold
        /// </summary>
        public decimal Factor { get { return factor; } }
        private readonly decimal factor;

        /// <summary>
        /// A list of daily overtime allocations for daily thresholds.
        /// If null or empty, the threshold is for the entire time frame
        /// </summary>
        public List<DailyOvertimeAllocation> DailyAllocations { get; set; }

        /// <summary>
        /// Create an OvertimeThresholdResult object
        /// </summary>
        /// <param name="earningsTypeId"></param>
        /// <param name="factor"></param>
        /// <param name="totalOvertime"></param>
        public OvertimeThresholdResult(string earningsTypeId, decimal factor, TimeSpan totalOvertime)
        {
            if (string.IsNullOrEmpty(earningsTypeId))
            {
                throw new ArgumentNullException("earningsTypeId");
            }
            if (factor < 0)
            {
                throw new ArgumentOutOfRangeException("factor", "factor must be greater than or equal to zero");
            }

            this.totalOvertime = totalOvertime;
            this.earningsTypeId = earningsTypeId;
            this.factor = factor;

            DailyAllocations = new List<DailyOvertimeAllocation>();
        }

        /// <summary>
        /// Two OvertimeThresholdResult objects are equal when their EarningsTypeIds, Factors, TotalOvertimes
        /// and DailyAllocations are equal.
        /// Equality of OvertimeThresholdResults should be considered in the context of their parent
        /// OvertimeCalculationResults
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var result = obj as OvertimeThresholdResult;

            return result.EarningsTypeId == this.EarningsTypeId &&
                result.Factor == this.Factor &&
                result.TotalOvertime == this.TotalOvertime &&
                result.DailyAllocations.SequenceEqual(this.DailyAllocations);
        }

        /// <summary>
        /// Compute the hashcode of this OvertimeThresholdResult
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return EarningsTypeId.GetHashCode() ^ Factor.GetHashCode() ^ TotalOvertime.GetHashCode() ^ DailyAllocations.GetHashCode();
        }
    }
}
