﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
    /// <summary>
    /// This class is used to help the TimecardRepository understand how to update
    /// a Timecard and its associated Time Entries. 
    /// </summary>
    [Serializable]
    public class Timecard2Helper
    {
        /// <summary>
        /// The timecard to be updated
        /// </summary>
        public Timecard2 Timecard { get; set; }
        /// <summary>
        /// List of time entries to update
        /// </summary>
        public List<TimeEntry2> TimeEntrysToUpdate { get; set; }
        /// <summary>
        /// List of time entries to create
        /// </summary>
        public List<TimeEntry2> TimeEntrysToCreate { get; set; }
        /// <summary>
        /// List of time entries to delete
        /// </summary>
        public List<TimeEntry2> TimeEntrysToDelete { get; set; }

        /// <summary>
        /// Constructor for helper
        /// </summary>
        /// <param name="timecard"></param>
        public Timecard2Helper(Timecard2 timecard)
        {
            if (timecard == null)
            {
                throw new ArgumentNullException("Timecard is a required argument in Timecard2Helper entity contstructor");
            }

            this.Timecard = timecard;
            this.TimeEntrysToUpdate = new List<TimeEntry2>();
            this.TimeEntrysToCreate = new List<TimeEntry2>();
            this.TimeEntrysToDelete = new List<TimeEntry2>();
        }
    }
}
