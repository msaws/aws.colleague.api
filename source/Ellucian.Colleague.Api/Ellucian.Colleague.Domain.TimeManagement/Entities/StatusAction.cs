﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
    /// <summary>
    /// The action performed on a timecard
    /// </summary>
    [Serializable]
    public enum StatusAction
    {
        /// <summary>
        /// Indicates the timecard was submitted for approval
        /// </summary>
        Submitted,
        /// <summary>
        /// Indiciates the timecard was unsubmitted after previously being submitted
        /// </summary>
        Unsubmitted,

        /// <summary>
        /// Indicates the timecard was rejected
        /// </summary>
        Rejected,

        /// <summary>
        /// Indicates the timecard was approved
        /// </summary>
        Approved,

        /// <summary>
        /// Indicates the timecard was processed through payroll
        /// </summary>
        Paid
    }
}
