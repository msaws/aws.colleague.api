﻿using Ellucian.Colleague.Domain.Base.Entities;
/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
    /// <summary>
    /// Timecard History Entity
    /// </summary>
    [Serializable]
    public class TimecardHistory
    {
        /// <summary>
        /// The Identifier of this timecard history
        /// </summary>
        public string Id { get; private set; }
        /// <summary>
        /// The identification of this timecard history's associated employee
        /// </summary>
        public string EmployeeId { get; private set; }
        /// <summary>
        /// The identification this timecard history's associated position
        /// </summary>
        public string PositionId { get; private set; }
        /// <summary>
        /// The idenitification of this timecard history's associated pay cycle
        /// </summary>
        public string PayCycleId { get; private set; }
        /// <summary>
        /// Start date for this timecard history's associated period
        /// </summary>
        public DateTime PeriodStartDate { get; private set; }
        /// <summary>
        /// End date for this timecard history's associated period
        /// </summary>
        public DateTime PeriodEndDate { get; private set; }
        /// <summary>
        /// Start date for this timecard history's associated week
        /// </summary>
        public DateTime StartDate { get; private set; }
        /// <summary>
        /// End date for this timecard history's associated week
        /// </summary>
        public DateTime EndDate { get; private set; }
        /// <summary>
        /// The StatusAction that created this history record
        /// </summary>
        public StatusAction StatusAction { get; private set; }
        /// <summary>
        /// A list of time entries associated to this timecard history
        /// </summary>
        public List<TimeEntryHistory> TimeEntryHistories { get; set; }
        /// <summary>
        /// This record's timestamp
        /// </summary>
        public Timestamp Timestamp { get; private set; }

        #region CONSTRUCTORS

        /// <summary>
        /// Timecard  history entity constructor
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="payCycleId"></param>
        /// <param name="positionId"></param>
        /// <param name="periodStartDate"></param>
        /// <param name="periodEndDate"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="id"></param>
        /// <param name="statusAction">The Status Action that created this history object</param>
        /// <param name="timestamp"></param>
        public TimecardHistory(
            string id,
            string employeeId,
            string payCycleId,
            string positionId,
            DateTime periodStartDate,
            DateTime periodEndDate,
            DateTime startDate,
            DateTime endDate,
            StatusAction statusAction,
            Timestamp timestamp
            )
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }
            if (string.IsNullOrWhiteSpace(employeeId))
            {
                throw new ArgumentNullException("employeeId");
            }
            if (string.IsNullOrWhiteSpace(payCycleId))
            {
                throw new ArgumentNullException("payCycleId");
            }
            if (string.IsNullOrWhiteSpace(positionId))
            {
                throw new ArgumentNullException("positionId");
            }
            if (timestamp == null)
            {
                throw new ArgumentNullException("timestamp");
            }

            this.Id = id;
            this.EmployeeId = employeeId;
            this.PayCycleId = payCycleId;
            this.PositionId = positionId;
            this.PeriodStartDate = periodStartDate;
            this.PeriodEndDate = periodEndDate;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.Timestamp = timestamp;
            this.StatusAction = statusAction;

            this.TimeEntryHistories = new List<TimeEntryHistory>();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var timecardHistory = obj as TimecardHistory;

            return this.Id == timecardHistory.Id;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
        #endregion
    }
}
