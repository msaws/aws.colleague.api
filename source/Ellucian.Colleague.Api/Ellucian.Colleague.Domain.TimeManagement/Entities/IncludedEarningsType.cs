﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
     [Serializable]
     public class IncludedEarningsType
     {
          /// <summary>
          /// The ID associated with the earnings type
          /// </summary>
          public string EarningsTypeId { get { return earningsTypeId; } }
          private readonly string earningsTypeId;

          /// <summary>
          /// Boolean that states whether to use the alternate earntype or not
          /// </summary>
          public bool UseAlternate { get { return useAlternate; } }
          private readonly bool useAlternate;

          public IncludedEarningsType() { }

          public IncludedEarningsType(string id, bool useAlternate)
          {
               this.earningsTypeId = id;
               this.useAlternate = useAlternate;
          }
     }
}
