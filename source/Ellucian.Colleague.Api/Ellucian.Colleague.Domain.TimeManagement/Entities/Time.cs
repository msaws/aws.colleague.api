﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
    public class Time
    {
        public int Hours
        {
            get
            {
                return hours;
            }
            set
            {
                hours = value;
            }
        }
        private int hours;
        public int Minutes { get; set; }
        private int minutes { get; set; }
        public int Seconds { get; set; }
        private int seconds { get; set; }
    }
}
