﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
     [Serializable]
     public class HoursThreshold
     {
          /// <summary>
          /// The number of hours associated with this threshold
          /// </summary>
          public decimal Threshold { get { return threshold; } }
          private readonly decimal threshold;

          /// <summary>
          /// The default EarningsType ID for overtime hours
          /// </summary>
          public EarningsFactor DefaultEarningsType { get { return defaultEarningsType; } }
          private readonly EarningsFactor defaultEarningsType;
          
          /// <summary>
          /// The alternate EarningsType ID for overtime hours
          /// </summary>
          public EarningsFactor AlternateEarningsType { get { return alternateEarningsType; } }
          private readonly EarningsFactor alternateEarningsType;


          public HoursThreshold(decimal hours, EarningsFactor defaultFactor, EarningsFactor alternateFactor)
          {
               if (hours == 0)
               {
                    throw new ArgumentNullException("hours");
               }

               threshold = hours;
               defaultEarningsType = defaultFactor;
               alternateEarningsType = alternateFactor;

          }
     }
}
