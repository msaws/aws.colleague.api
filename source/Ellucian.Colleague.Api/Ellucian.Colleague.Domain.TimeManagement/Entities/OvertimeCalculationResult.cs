﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
    /// <summary>
    /// The result of an overtime calculation.
    /// </summary>
    [Serializable]
    public class OvertimeCalculationResult
    {
        /// <summary>
        /// The Id of the person for whom this overtime result applies
        /// </summary>
        public string PersonId { get { return personId; } }
        private readonly string personId;

        /// <summary>
        /// The Id of the PayCycle for which this overtime result applies.
        /// </summary>
        public string PayCycleId { get { return payCycleId; } }
        private readonly string payCycleId;

        /// <summary>
        /// The start of the date range for which this overtime result applies.
        /// </summary>
        public DateTime StartDate { get { return startDate; } }
        private readonly DateTime startDate;


        /// <summary>
        /// The end of the date range for which the overtime result applies.
        /// </summary>
        public DateTime EndDate { get { return endDate; } }
        private readonly DateTime endDate;

        /// <summary>
        /// The date and time this result was calculated.
        /// </summary>
        public DateTimeOffset ResultDateTime { get { return resultDateTime; } }
        private readonly DateTimeOffset resultDateTime;

        /// <summary>
        /// A list of the overtime thresholds reached by the person for the given timeframe
        /// </summary>
        public List<OvertimeThresholdResult> Thresholds { get; set; }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public OvertimeCalculationResult(string personId, string payCycleId, DateTime startDate, DateTime endDate, DateTimeOffset resultDateTime)
        {
            if (string.IsNullOrEmpty(personId))
            {
                throw new ArgumentNullException("personId");
            }
            if (string.IsNullOrEmpty(payCycleId))
            {
                throw new ArgumentNullException("payCycleId");
            }
            if (startDate > endDate)
            {
                throw new ArgumentException("start date must be before end date");
            }

            this.personId = personId;
            this.payCycleId = payCycleId;
            this.startDate = startDate;
            this.endDate = endDate;
            this.resultDateTime = resultDateTime;

            Thresholds = new List<OvertimeThresholdResult>();
        }

        /// <summary>
        /// Two objects are equal when the personId, startdate, enddate are equal
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var result = obj as OvertimeCalculationResult;

            return result.PersonId == this.PersonId &&
                result.PayCycleId == this.PayCycleId &&
                result.StartDate == this.StartDate &&
                result.EndDate == this.EndDate &&
                result.Thresholds.SequenceEqual(this.Thresholds);
        }

        public override int GetHashCode()
        {
            return PersonId.GetHashCode() ^ StartDate.GetHashCode() ^ EndDate.GetHashCode() ^ Thresholds.GetHashCode();
        }
    }
}
