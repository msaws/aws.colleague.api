﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.Base.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
    /// <summary>
    /// A TimeEntry object defines a single unit of work on a Timecard.
    /// </summary>
    [Serializable]
    public class TimeEntry
    {
        #region PROPERTIES

        /// <summary>
        /// The identifier of this time entry
        /// </summary>
        public string Id { get; private set; }

        /// <summary>
        /// This time entry's associated timecard identifier
        /// </summary>
        public string TimecardId { get; private set; }

        /// <summary>
        /// This time entry's associated project identifier
        /// </summary>
        public string ProjectId { get; set; }

        /// <summary>
        /// This time entry's associated earn type identifier. EarningsTypeId must have value. It cannot be null, empty or whitespace.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if set to null, empty or whitespace string.</exception>
        public string EarningsTypeId
        {
            get
            {
                return earningsTypeId;
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    throw new ArgumentOutOfRangeException("EarningsTypeId must have value. It cannot be null, empty or whitespace");
                }
                earningsTypeId = value;
            }
        }
        private string earningsTypeId;

        /// <summary>
        /// This time entry's associated person leave identifier
        /// </summary>
        public string PersonLeaveId { get; set; }

        /// <summary>
        /// The time of this entry's commencement. Must be an earlier DateTimeOffset than OutDateTime.
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if value is after OutDateTime</exception>
        public DateTimeOffset? InDateTime
        {
            get
            {
                return inDateTime;
            }
            set
            {
                if (outDateTime.HasValue && value.HasValue)
                {
                    if (outDateTime.Value < value.Value)
                    {
                        throw new ArgumentOutOfRangeException("OutDateTime cannot be earlier than InDateTime");
                    }
                }

                inDateTime = value;
                
            }
        }
        private DateTimeOffset? inDateTime;

        /// <summary>
        /// The time of this entry's termination. Must be a later DateTimeOffset than InDateTime
        /// </summary>
        /// <exception cref="ArgumentOutOfRangeException">Thrown if value is before InDateTime</exception>
        public DateTimeOffset? OutDateTime
        {
            get
            {
                return outDateTime;
            }
            set
            {
                if (inDateTime.HasValue && value.HasValue)
                {
                    if (inDateTime.Value > value.Value)
                    {
                        throw new ArgumentOutOfRangeException("OutDateTime cannot be earlier than InDateTime");
                    }
                }
                outDateTime = value;
            }
        }
        private DateTimeOffset? outDateTime;


        /// <summary>
        /// The total time amount for this entry, either entered directly or derived from InDateTime/OutDateTime
        /// Null when InDateTime has value but OutDateTime does not, and visa versa.
        /// </summary>
        public TimeSpan? WorkedTime
        {
            get
            {

                //not good practice to change the state of the object in a getter, so calculate the value here
                if (!inDateTime.HasValue && !outDateTime.HasValue) //if both in/out times are null, return worked time
                {
                    return workedTime;
                }
                else if (inDateTime.HasValue && outDateTime.HasValue) //if both in/out times have value, calculate worked time
                {
                    return outDateTime.Value - inDateTime.Value;
                }
                else //one of in/out is null, so return null since we don't know the worked time
                {
                    return null;
                }
            }
            set
            {
                if (!inDateTime.HasValue && !outDateTime.HasValue)
                {
                    workedTime = value;
                }
                else
                {
                    throw new InvalidOperationException("WorkedTime cannot be set if InDateTime or OutDateTime are present");
                }
            }
        }
        private TimeSpan? workedTime;

        /// <summary>
        /// The date associated with commencement of this time entry.
        /// </summary>
        public DateTime WorkedDate { get; set; } 

        /// <summary>
        /// The record timestamp
        /// </summary>
        public Timestamp Timestamp { get; set; }

        #endregion

        #region CONSTRUCTORS
        /// <summary>
        /// Time entry constructor for an existing time entry use with time in and time out
        /// </summary>
        /// <param name="timecardId"></param>
        /// <param name="earningsTypeId"></param>
        /// <param name="inDateTime"></param>
        /// <param name="outDateTime"></param> 
        /// <param name="workedDate"></param>
        /// <param name="personLeaveId"></param>
        /// <param name="id"></param>
        public TimeEntry(string timecardId, string earningsTypeId, DateTimeOffset? inDateTime, DateTimeOffset? outDateTime, DateTime workedDate, string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullException("id");
            }
            if (string.IsNullOrWhiteSpace(earningsTypeId))
            {
                throw new ArgumentNullException("EarningsTypeId");
            }
            if (inDateTime.HasValue && inDateTime.Value.UtcDateTime.Date != workedDate)
            {
                var message = string.Format("Date of inDateTime {0} does not match workedDate {1}", inDateTime.Value, workedDate);
                throw new ArgumentException(message, "inDateTime");
            }

            this.Id = id;
            this.TimecardId = timecardId;
            this.EarningsTypeId = earningsTypeId;
            this.InDateTime = inDateTime;
            this.OutDateTime = outDateTime;
            this.WorkedDate = workedDate;
        }

        /// <summary>
        /// Time entry constructor for an existing time entry for use with total time
        /// </summary>
        /// <param name="timecardId"></param>
        /// <param name="earningsTypeId"></param>
        /// <param name="workedDate"></param>
        /// <param name="workedTime"></param>
        /// <param name="personLeaveId"></param>
        /// <param name="id"></param>
        public TimeEntry(string timecardId, string earningsTypeId, TimeSpan? workedTime, DateTime workedDate, string id)
        {
            if(string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullException("id");
            }
            if (string.IsNullOrWhiteSpace(earningsTypeId))
            {
                throw new ArgumentNullException("EarningsTypeId");
            }
            this.Id = id;
            this.TimecardId = timecardId;
            this.EarningsTypeId = earningsTypeId;
            this.WorkedTime = workedTime;
            this.WorkedDate = workedDate;
        }

        /// <summary>
        /// Time entry constructor for a new time entry use with time in and time out
        /// </summary>
        /// <param name="timecardId"></param>
        /// <param name="earningsTypeId"></param>
        /// <param name="inDateTime"></param>
        /// <param name="outDateTime"></param> 
        /// <param name="workedDate"></param>
        /// <param name="personLeaveId"></param>
        /// <param name="id"></param>
        public TimeEntry(string timecardId, string earningsTypeId, DateTimeOffset? inDateTime, DateTimeOffset? outDateTime, DateTime workedDate)
        {
            if (string.IsNullOrWhiteSpace(earningsTypeId))
            {
                throw new ArgumentNullException("EarningsTypeId is a required argument in TimeEntry entity constructor");
            }
            if (inDateTime.HasValue && inDateTime.Value.Date != workedDate)
            {
                var message = string.Format("Date of inDateTime {0} does not match workedDate {1}", inDateTime.Value, workedDate);
                throw new ArgumentException(message, "inDateTime");
            }

            this.TimecardId = timecardId;
            this.EarningsTypeId = earningsTypeId;
            this.InDateTime = inDateTime;
            this.OutDateTime = outDateTime;
            this.WorkedDate = workedDate;
        }

        /// <summary>
        /// Time entry constructor for a new time entry for use with total time
        /// </summary>
        /// <param name="timecardId"></param>
        /// <param name="earningsTypeId"></param>
        /// <param name="workedDate"></param>
        /// <param name="workedTime"></param>
        /// <param name="personLeaveId"></param>
        /// <param name="id"></param>
        public TimeEntry(string timecardId, string earningsTypeId, TimeSpan? workedTime, DateTime workedDate)
        {
            if (string.IsNullOrWhiteSpace(earningsTypeId))
            {
                throw new ArgumentNullException("EarningsTypeId is a required argument in TimeEntry entity constructor");
            }
            this.TimecardId = timecardId;
            this.EarningsTypeId = earningsTypeId;
            this.WorkedTime = workedTime;
            this.WorkedDate = workedDate;
        }

        #endregion

        #region METHODS
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var timeEntry = obj as TimeEntry;

            if (string.IsNullOrEmpty(this.Id) && string.IsNullOrEmpty(timeEntry.Id))
            {
                return CompareTimeEntry(timeEntry);
            }

            if (timeEntry.Id == this.Id)
            {
                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            if (string.IsNullOrEmpty(this.Id))
            {
                return TimecardId.GetHashCode() ^
                    ProjectId.GetHashCode() ^
                    EarningsTypeId.GetHashCode() ^
                    PersonLeaveId.GetHashCode() ^
                    InDateTime.GetHashCode() ^
                    OutDateTime.GetHashCode() ^
                    WorkedDate.GetHashCode() ^
                    WorkedTime.GetHashCode();

            }
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Used to determine if all properties of object are equal...
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool CompareTimeEntry(TimeEntry timeEntry)
        {

            if (TimeMangementUtility.AreEqual(timeEntry.Id, this.Id) &&
                TimeMangementUtility.AreEqual(timeEntry.TimecardId, this.TimecardId) &&
                TimeMangementUtility.AreEqual(timeEntry.ProjectId, this.ProjectId) &&
                TimeMangementUtility.AreEqual(timeEntry.EarningsTypeId, this.EarningsTypeId) &&
                TimeMangementUtility.AreEqual(timeEntry.PersonLeaveId, this.PersonLeaveId) &&
                timeEntry.InDateTime == this.InDateTime &&
                timeEntry.OutDateTime == this.OutDateTime &&
                timeEntry.WorkedTime == this.WorkedTime &&
                timeEntry.WorkedDate == this.WorkedDate
                )
            {
                return true;
            }

            return false;
        }
        #endregion
    }
}
