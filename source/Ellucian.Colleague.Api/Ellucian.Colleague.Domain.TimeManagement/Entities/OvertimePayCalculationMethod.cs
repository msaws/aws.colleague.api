﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
     [Serializable]
     public enum OvertimePayCalculationMethod
     {
          WeightedAverage,
          PositionRate
     } 
}
