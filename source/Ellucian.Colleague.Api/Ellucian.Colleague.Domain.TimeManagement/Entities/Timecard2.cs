﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.Base.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.TimeManagement;
using System.Collections.ObjectModel;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
    /// <summary>
    /// A timecard domain model
    /// </summary>
    [Serializable]
    public class Timecard2
    {
        #region PROPERTIES
        /// <summary>
        /// The Identifier of this timecard
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// The identification of this timecard's associated employee
        /// </summary>
        public string EmployeeId { get; set; }
        /// <summary>
        /// The identification of this timecard's associated position
        /// </summary>
        public string PositionId { get; set; }
        /// <summary>
        /// The idenitification of this timecard's associated pay cycle
        /// </summary>
        public string PayCycleId { get; set; }
        /// <summary>
        /// Start date for this timecard's associated period
        /// </summary>
        public DateTime PeriodStartDate { get; set; }
        /// <summary>
        /// End date for this timecard's associated period
        /// </summary>
        public DateTime PeriodEndDate { get; set; }
        /// <summary>
        /// Start date for this timecard's associated week
        /// </summary>
        public DateTime StartDate { get; set; }
        /// <summary>
        /// End date for this timecard's associated week
        /// </summary>
        public DateTime EndDate { get; set; }
        /// <summary>
        /// A list of time entries associated to this timecard
        /// </summary>
        public List<TimeEntry2> TimeEntries { get; set; }

        /// <summary>
        /// This record's timestamp
        /// </summary>
        public Timestamp Timestamp { get; set; }

        #endregion

        #region CONSTRUCTORS

        /// <summary>
        /// Timecard entity constructor for an existing timecard
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="payCycleId"></param>
        /// <param name="positionId"></param>
        /// <param name="periodStartDate"></param>
        /// <param name="periodEndDate"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="id"></param>
        public Timecard2(
            string employeeId,
            string payCycleId,
            string positionId,
            DateTime periodStartDate,
            DateTime periodEndDate,
            DateTime startDate,
            DateTime endDate,
            string id
            )
        {
            if (string.IsNullOrWhiteSpace(employeeId))
            {
                throw new ArgumentNullException("EmployeeId is a required argument in the Timecard entity constructor");
            }
            if (string.IsNullOrWhiteSpace(payCycleId))
            {
                throw new ArgumentNullException("PayCycleId is a required argument in the Timecard entity constructor");
            }
            if (string.IsNullOrWhiteSpace(positionId))
            {
                throw new ArgumentNullException("positionid is a required argument in the Timecard entity constructor");
            }

            this.Id = id;
            this.EmployeeId = employeeId;
            this.PayCycleId = payCycleId;
            this.PositionId = positionId;
            this.PeriodStartDate = periodStartDate;
            this.PeriodEndDate = periodEndDate;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.TimeEntries = new List<TimeEntry2>();
        }

        /// <summary>
        /// Timecard entity constructor for a new timecard
        /// </summary>
        /// <param name="employeeId"></param>
        /// <param name="payCycleId"></param>
        /// <param name="positionId"></param>
        /// <param name="periodStartDate"></param>
        /// <param name="periodEndDate"></param>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        public Timecard2(
            string employeeId,
            string payCycleId,
            string positionId,
            DateTime periodStartDate,
            DateTime periodEndDate,
            DateTime startDate,
            DateTime endDate
        )
        {
            if (string.IsNullOrWhiteSpace(employeeId))
            {
                throw new ArgumentNullException("EmployeeId is a required argument in the Timecard entity constructor");
            }
            if (string.IsNullOrWhiteSpace(payCycleId))
            {
                throw new ArgumentNullException("PayCycleId is a required argument in the Timecard entity constructor");
            }
            if (string.IsNullOrWhiteSpace(positionId))
            {
                throw new ArgumentNullException("positionid is a required argument in the Timecard entity constructor");
            }
            this.EmployeeId = employeeId;
            this.PayCycleId = payCycleId;
            this.PositionId = positionId;
            this.PeriodStartDate = periodStartDate;
            this.PeriodEndDate = periodEndDate;
            this.StartDate = startDate;
            this.EndDate = endDate;
            this.TimeEntries = new List<TimeEntry2>();
        }

        /// <summary>
        /// Used to determine if all properties of object are equal...
        /// </summary>
        /// <param name="timecard"></param>
        /// <returns></returns>
        public bool CompareTimecard(Timecard2 timecard)
        {
            if (timecard == null || timecard.GetType() != GetType())
            {
                return false;
            }

            if (TimeMangementUtility.AreEqual(timecard.PayCycleId, this.PayCycleId) &&
                TimeMangementUtility.AreEqual(timecard.PositionId, this.PositionId) &&
                TimeMangementUtility.AreEqual(timecard.EmployeeId, this.EmployeeId) &&
                timecard.PeriodEndDate == this.PeriodEndDate &&
                timecard.PeriodStartDate == this.PeriodStartDate &&
                timecard.StartDate == this.StartDate &&
                timecard.EndDate == this.EndDate
                )
            {
                return true;
            }

            return false;
        }
        #endregion
    }
}
