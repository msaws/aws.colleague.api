﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.Base.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.TimeManagement;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
    /// <summary>
    /// A representation of a timecard status instance
    /// </summary>
    [Serializable]
    public class TimecardStatus
    {
        /// <summary>
        /// This identifier of this status
        /// </summary>
        public string Id { get; private set; }
        
        /// <summary>
        /// The associated timecard
        /// </summary>
        public string TimecardId { get; private set; }

        /// <summary>
        /// The person involved with the status action
        /// </summary>
        public string ActionerId { get; private set; }

        
        
        /// <summary>
        /// The type of action to be performed
        /// </summary>
        public StatusAction ActionType { get; private set; }

        /// <summary>
        /// Timestamp metadata
        /// </summary>
        public Timestamp Timestamp { get; set; }

        /// <summary>
        /// constructor for new timecard
        /// </summary>
        public TimecardStatus(string timecardId, string actionerId, StatusAction actionType)
        {
            if (string.IsNullOrWhiteSpace(timecardId))
            {
                throw new ArgumentNullException("timecardId");
            }
            if (string.IsNullOrWhiteSpace(actionerId))
            {
                throw new ArgumentNullException("actionerId");
            }


            this.TimecardId = timecardId;
            this.ActionerId = actionerId;
            this.ActionType = actionType;
        }

        /// <summary>
        /// constructor for existing timecard status 
        /// </summary>
        public TimecardStatus(string timecardId, string actionerId, StatusAction actionType, string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentException("Id");
            }
            if (string.IsNullOrWhiteSpace(timecardId))
            {
                throw new ArgumentNullException("timecardId");
            }
            if (string.IsNullOrWhiteSpace(actionerId))
            {
                throw new ArgumentNullException("actionerId");
            }
            
            this.Id = id;
            this.TimecardId = timecardId;
            this.ActionerId = actionerId;
            this.ActionType = actionType;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var status = obj as TimecardStatus;

          
                return status.Id == this.Id;
                       
        }

        public override int GetHashCode()
        {
   
               return this.Id.GetHashCode();
                     
        }
    }
}
