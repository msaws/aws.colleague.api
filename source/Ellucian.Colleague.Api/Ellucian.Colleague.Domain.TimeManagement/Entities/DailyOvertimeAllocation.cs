﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
    /// <summary>
    /// Part of an OvertimeCalculationResult that indicates overtime
    /// for a specific date when a daily overtime threshold has been reached.
    /// </summary>
    [Serializable]
    public class DailyOvertimeAllocation
    {
        /// <summary>
        /// The date this overtime applies to
        /// </summary>
        public DateTime Date { get { return date; } }
        private readonly DateTime date;

        /// <summary>
        /// The overtime for the date
        /// </summary>
        public TimeSpan Overtime { get { return overtime; } }
        private readonly TimeSpan overtime;

        /// <summary>
        /// Create a DailyOvertimeAllocation object
        /// </summary>
        /// <param name="date">the date this overtime applies to</param>
        /// <param name="overtime">the amount of overtime worked on the date</param>
        public DailyOvertimeAllocation(DateTime date, TimeSpan overtime)
        {
            this.date = date;
            this.overtime = overtime;
        }

        /// <summary>
        /// Two daily allocation objects are equal when their dates and overtimes are equal.
        /// Equality of daily allocations should be considered in the context of their parent
        /// Overtime Calculation Results
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var allocation = obj as DailyOvertimeAllocation;

            return allocation.Date == this.Date &&
                allocation.Overtime == this.Overtime;
        }

        /// <summary>
        /// Compute the hashcode of the daily allocation
        /// </summary>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return Date.GetHashCode() ^ Overtime.GetHashCode();
        }
    }
}
