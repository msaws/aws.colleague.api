﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
     [Serializable] 
     public class OvertimeCalculationDefinition
     {
          public string Id { get; set; }

          public string Description { get; set; }

          public OvertimePayCalculationMethod OvertimePayCalculationMethod { get; set; }

          public List<HoursThreshold> WeeklyHoursThreshold { get; set; }

          public List<HoursThreshold> DailyHoursThreshold { get; set; }

          public List<IncludedEarningsType> IncludedEarningsTypes { get; set; }

          public OvertimeCalculationDefinition(string id, string description, OvertimePayCalculationMethod overtimePayCalculationMethod) 
          {
               // required elements
               if (String.IsNullOrWhiteSpace(id)) throw new ArgumentNullException("id");
               if (string.IsNullOrWhiteSpace(description)) throw new ArgumentNullException("description");
               Id = id;
               Description = description;
               OvertimePayCalculationMethod = overtimePayCalculationMethod;

               WeeklyHoursThreshold = new List<HoursThreshold>();
               DailyHoursThreshold = new List<HoursThreshold>();
               IncludedEarningsTypes = new List<IncludedEarningsType>();
          
          }

     }

}
