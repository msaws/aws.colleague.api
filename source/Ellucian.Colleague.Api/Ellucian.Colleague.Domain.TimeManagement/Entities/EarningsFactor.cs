﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
     [Serializable]
     public class EarningsFactor
     {


        public string PersonId { get { return personId; } }
        private readonly string personId;

          /// <summary>
          /// The Id representing the EarningsType
          /// </summary>
        public string EarningsTypeId { get { return earningsTypeId; } }
        private readonly string earningsTypeId;

          /// <summary>
          /// The factor associated with the EarningsType. The field here is a Nullable Decimal, however, due to the constructor, it will never be null
          /// </summary>
        public decimal? Factor { get { return factor; } }
        private readonly decimal? factor;


          public EarningsFactor(string id, decimal? factor)
          {
               if (string.IsNullOrWhiteSpace(id))
               {
                    throw new ArgumentNullException("id");
               }
               earningsTypeId = id;
               this.factor = (factor == null) ? 1.0m : factor;
          }

     }
}
