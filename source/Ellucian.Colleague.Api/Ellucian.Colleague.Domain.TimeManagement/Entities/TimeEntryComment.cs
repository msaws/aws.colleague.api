﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.Base.Entities;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
    /// <summary>
    /// Comments applied to a timecard or timecardStatus
    /// </summary>
    [Serializable]
    public class TimeEntryComment
    {
        /// <summary>
        /// The database ID of the Comments object
        /// </summary>
        public string Id { get; private set; }

        /// <summary>
        /// the ID of the employee this Comment is associated to
        /// </summary>
        public string EmployeeId { get; private set; }

        /// <summary>
        /// The ID of the timecard this Comment is associated to. Not required if the comment is assigned to
        /// a timecardStatusId, or to a PositionId + PayCycleId + PayPeriodEndDate grouping
        /// </summary>
        public string TimecardId { get; private set; }

        ///// <summary>
        ///// The ID of the timecardStatus this Comment is associated to. Not required if the comment is assigned to
        ///// a timecardId,  or to a PositionId + PayCycleId + PayPeriodEndDate grouping.
        ///// </summary>
        //public string TimecardStatusId { get; private set; }

        /// <summary>
        /// The ID of the PayCycle this Comment is associated to. Required if the comment is assigned to a
        /// PositionId + PayCycleId + PayPeriodEndDate grouping. Not Required if the comment is assigned to a TimecardId or
        /// timecardStatusId
        /// </summary>
        public string PayCycleId { get; private set; }

        /// <summary>
        /// The end date of the pay period this Comment is associated to. Required if the comment is assigned to a
        /// PositionId + PayCycleId + PayPeriodEndDate grouping. Not Required if the comment is assigned to a TimecardId or
        /// timecardStatusId
        /// </summary>
        public DateTime PayPeriodEndDate { get; private set; }

        /// <summary>
        /// The ID of the Position this Comment is associated to. Required if the comment is assigned to a
        /// PositionId + PayCycleId + PayPeriodEndDate grouping. Not Required if the comment is assigned to a TimecardId or
        /// timecardStatusId
        /// </summary>
        public string PositionId { get; private set; }

        /// <summary>
        /// The actual Comment content
        /// </summary>
        public string Comments { get; private set; }

        /// <summary>
        /// A timestamp of when this Comment object was created and changed in the database. Required if the Comment database record
        /// this object represents already exists. Not required if this is a new Comment
        /// </summary>
        public Timestamp CommentsTimestamp { get; private set; }

        /// <summary>
        /// Returns true or false whether this comment belongs to the given timecard
        /// </summary>
        /// <param name="timecard"></param>
        /// <returns></returns>
        public bool IsLinkedToTimecard(dynamic timecard)
        {
            if (!string.IsNullOrEmpty(this.TimecardId))
            {
                return this.TimecardId == timecard.Id;
            }
            return this.PayCycleId == timecard.PayCycleId &&
                this.PayPeriodEndDate == timecard.PeriodEndDate &&
                this.PositionId == timecard.PositionId &&
                this.EmployeeId == timecard.EmployeeId;
        }

        /// <summary>
        /// Constructor for a Comments object with a database identifier.
        /// Comments must have an assigned timecardId or a timecardStatusId, or be associated 
        /// to a PositionId + PayCycleId + PayPeriodEndDate
        /// </summary>
        /// <param name="id"></param>
        /// <param name="employeeId"></param>
        /// <param name="timecardId"></param>
        /// <param name="timecardStatusId"></param>
        /// <param name="payCycleId"></param>
        /// <param name="payPeriodEndDate"></param>
        /// <param name="comments"></param>
        /// <param name="timestamp"></param>
        public TimeEntryComment(
            string id,
            string employeeId,
            string timecardId,
            string positionId,
            string payCycleId,
            DateTime payPeriodEndDate,
            string comments,
            Timestamp timestamp)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullException("id");
            }

            if (string.IsNullOrWhiteSpace(employeeId))
            {
                throw new ArgumentNullException("employeeId");
            }

            if (string.IsNullOrWhiteSpace(positionId))
            {
                throw new ArgumentNullException("positionId");
            }

            if (string.IsNullOrWhiteSpace(payCycleId))
            {
                throw new ArgumentNullException("payCycleId");
            }

            if (timestamp == null)
            {
                throw new ArgumentNullException("timestamp");
            }

            this.Id = id;
            this.EmployeeId = employeeId;
            this.TimecardId = timecardId;
            this.PositionId = positionId;
            this.PayCycleId = payCycleId;
            this.PayPeriodEndDate = payPeriodEndDate;
            this.Comments = comments;
            this.CommentsTimestamp = timestamp;


        }

        /// <summary>
        /// Constructor for a Comments object without a database identifier.
        /// Comments must have an assigned timecardId or a timecardStatusId, or be associated 
        /// to a PositionId + PayCycleId + PayPeriodEndDate
        /// </summary>
        /// <param name="timecardId"></param>
        /// <param name="employeeId"></param>
        /// <param name="timecardStatusId"></param>
        /// <param name="positionId"></param>
        /// <param name="payCycleId"></param>
        /// <param name="payPeriodEndDate"></param>
        /// <param name="comments"></param>
        public TimeEntryComment(
            string timecardId,
            string employeeId,
            string positionId,
            string payCycleId,
            DateTime payPeriodEndDate,
            string comments)
        {
            if (string.IsNullOrWhiteSpace(employeeId))
            {
                throw new ArgumentNullException("employeeId");
            }

            if (string.IsNullOrWhiteSpace(positionId))
            {
                throw new ArgumentNullException("positionId");
            }

            if (string.IsNullOrWhiteSpace(payCycleId))
            {
                throw new ArgumentNullException("payCycleId");
            }

            this.EmployeeId = employeeId;
            this.TimecardId = timecardId;
            this.PositionId = positionId;
            this.PayCycleId = payCycleId;
            this.PayPeriodEndDate = payPeriodEndDate;
            this.Comments = comments;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var commentObj = obj as TimeEntryComment;

            if (!string.IsNullOrEmpty(Id) && !string.IsNullOrEmpty(commentObj.Id))
            {
                //if both objs have Ids, check equality.
                return Id == commentObj.Id;
            }
            else if((!string.IsNullOrEmpty(Id) && string.IsNullOrEmpty(commentObj.Id)) ||
                string.IsNullOrEmpty(Id) && !string.IsNullOrEmpty(commentObj.Id))
            {
                //if only one obj has Id, they're not equal
                return false;
            }

            //both objects are "new" check properties
            return EmployeeId == commentObj.EmployeeId &&
                TimecardId == commentObj.TimecardId &&
                PayCycleId == commentObj.PayCycleId &&
                PositionId == commentObj.PositionId &&
                PayPeriodEndDate == commentObj.PayPeriodEndDate &&
                Comments == commentObj.Comments;
                
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode() ^
                EmployeeId.GetHashCode() ^
                TimecardId.GetHashCode() ^
                PayCycleId.GetHashCode() ^
                PositionId.GetHashCode() ^
                PayPeriodEndDate.GetHashCode() ^
                Comments.GetHashCode();
        }
    }
}
