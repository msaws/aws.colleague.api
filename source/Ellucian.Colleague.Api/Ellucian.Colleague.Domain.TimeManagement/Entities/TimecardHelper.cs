﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
    /// <summary>
    /// This class is used to help the TimecardRepository understand how to update
    /// a Timecard and its associated Time Entries. 
    /// </summary>
    [Serializable]
    public class TimecardHelper
    {
        /// <summary>
        /// The timecard to be updated
        /// </summary>
        public Timecard Timecard { get; set; }
        /// <summary>
        /// List of time entries to update
        /// </summary>
        public List<TimeEntry> TimeEntriesToUpdate { get; set; }
        /// <summary>
        /// List of time entries to create
        /// </summary>
        public List<TimeEntry> TimeEntriesToCreate { get; set; }
        /// <summary>
        /// List of time entries to delete
        /// </summary>
        public List<TimeEntry> TimeEntriesToDelete { get; set; }

        /// <summary>
        /// Constructor for helper
        /// </summary>
        /// <param name="timecard"></param>
        public TimecardHelper(Timecard timecard)
        {
            if (timecard == null)
            {
                throw new ArgumentNullException("Timecard is a required argument in TimecardHelper entity contstructor");
            }

            this.Timecard = timecard;
            this.TimeEntriesToUpdate = new List<TimeEntry>();
            this.TimeEntriesToCreate = new List<TimeEntry>();
            this.TimeEntriesToDelete = new List<TimeEntry>();
        }
    }
}
