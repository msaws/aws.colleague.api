﻿using Ellucian.Colleague.Domain.Base.Entities;
/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Entities
{
    /// <summary>
    /// Time entry history entity
    /// </summary>
    [Serializable]
    public class TimeEntryHistory2
    {
        /// <summary>
        /// The identifier of this time entry history
        /// </summary>
        public string Id { get; private set; }
        /// <summary>
        /// This time entry's associated timecard history identifier
        /// </summary>
        public string TimecardHistoryId { get; private set; }
        /// <summary>
        /// This time entry history's associated project identifier
        /// </summary>
        public string ProjectId { get; private set; }
        /// <summary>
        /// This time entry history's associated earn type identifier
        /// </summary>
        public string EarningsTypeId { get; private set; }
        /// <summary>
        /// This time entry history's associated person leave identifier
        /// </summary>
        public string PersonLeaveId { get; private set; }
        /// <summary>
        /// The time of this entry history's commencement
        /// </summary>
        public DateTime? InDateTime { get; private set; }
        /// <summary>
        /// The time of this entry history's termination
        /// </summary>
        public DateTime? OutDateTime { get; private set; }
        /// <summary>
        /// The total time amount for this entry history
        /// </summary>
        public TimeSpan WorkedTime { get; private set; }   // todo cstrctr1
        /// <summary>
        /// The date associated with commencement of this time entry
        /// </summary>
        public DateTime WorkedDate { get; private set; }  // todo cstrctr1
        /// <summary>
        /// The record timestamp
        /// </summary>
        public Timestamp Timestamp { get; private set; }

        #region CONSTRUCTOR(S)

        /// <summary>
        /// Time entry history constructor for both summary and detail time entries
        /// </summary>
        /// <param name="id"></param>
        /// <param name="timecardHistory2Id"></param>
        /// <param name="earningsTypeId"></param>
        /// <param name="inDateTime"></param>
        /// <param name="outDateTime"></param>
        /// <param name="workedTime"></param>
        /// <param name="workedDate"></param>
        /// <param name="personLeaveId"></param>
        /// <param name="projectId"></param>
        /// <param name="timestamp"></param>
        public TimeEntryHistory2(string id, string timecardHistory2Id, string earningsTypeId, DateTime? inDateTime, DateTime? outDateTime, TimeSpan workedTime, DateTime workedDate, string personLeaveId, string projectId, Timestamp timestamp)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                throw new ArgumentNullException("id");
            }
            else if (string.IsNullOrWhiteSpace(timecardHistory2Id))
            {
                throw new ArgumentNullException("timecardHistoryId");
            }
            else if (string.IsNullOrWhiteSpace(earningsTypeId))
            {
                throw new ArgumentNullException("earningsTypeId");
            }
            this.Id = id;
            this.TimecardHistoryId = timecardHistory2Id;
            this.EarningsTypeId = earningsTypeId;
            this.InDateTime = inDateTime;
            this.OutDateTime = outDateTime;
            this.WorkedDate = workedDate;
            this.WorkedTime = workedTime;
            this.PersonLeaveId = personLeaveId;
            this.ProjectId = projectId;
            this.Timestamp = timestamp;
        }
        #endregion

        #region METHODS
        public override bool Equals(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var timeEntry = obj as TimeEntry2;

            if (timeEntry.Id == this.Id)
            {
                return true;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        /// <summary>
        /// Used to determine if all properties of object are equal...
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool CompareTimeEntry(object obj)
        {
            if (obj == null || obj.GetType() != GetType())
            {
                return false;
            }

            var timeEntryHistory = obj as TimeEntryHistory2;

            if (timeEntryHistory.Id == this.Id &&
                TimeMangementUtility.AreEqual(timeEntryHistory.TimecardHistoryId, this.TimecardHistoryId) &&
                TimeMangementUtility.AreEqual(timeEntryHistory.ProjectId, this.ProjectId) &&
                TimeMangementUtility.AreEqual(timeEntryHistory.EarningsTypeId, this.EarningsTypeId) &&
                TimeMangementUtility.AreEqual(timeEntryHistory.PersonLeaveId, this.PersonLeaveId) &&
                timeEntryHistory.InDateTime == this.InDateTime &&
                timeEntryHistory.OutDateTime == this.OutDateTime &&
                timeEntryHistory.WorkedTime == this.WorkedTime &&
                timeEntryHistory.WorkedDate == this.WorkedDate
                )
            {
                return true;
            }

            return false;
        }
        #endregion

    }
}
