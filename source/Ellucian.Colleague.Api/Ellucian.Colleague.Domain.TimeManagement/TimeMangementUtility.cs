﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement
{
    /// <summary>
    /// Utility methods for TimeManagement
    /// </summary>
    public static class TimeMangementUtility
    {
        /// <summary>
        /// Helper compares two strings, considering null/empty to be the same thing
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool AreEqual(string a, string b)
        {
            if (string.IsNullOrEmpty(a))
            {
                return string.IsNullOrEmpty(b);
            }
            return string.Equals(a, b, StringComparison.InvariantCultureIgnoreCase);
        }
        // todo bhr: look for existing helper
        /// <summary>
        /// Sets the time of a date to create a relative dateTime
        /// </summary>
        /// <param name="date"></param>
        /// <param name="time"></param>
        /// <returns>DateTime?</returns>
        public static DateTime? SetTimeOfDate(DateTime? date, DateTime? time)
        {
            if (!date.HasValue) return null;

            // strip any time already applied to the date
            var existingTime = date.Value.TimeOfDay;
            date = date.Value.Subtract(existingTime);

            if (!time.HasValue) return date;

            // add the supplied time argument
            var newTime = time.Value.TimeOfDay;
            date = date.Value.Add(newTime);

            return date;
        }
    }
}
