﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Services
{
    public interface ITimecardDomainService
    {
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        TimecardHelper CreateTimecardHelper(Timecard timecard, Timecard existingTimecard);

        Timecard2Helper CreateTimecard2Helper(Timecard2 timecard2, Timecard2 existingTimecard2);
    }
}