﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Web.Dependency;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Services
{
    [RegisterType]
    public class TimecardDomainService : ITimecardDomainService
    {
        private readonly ILogger logger;
        public TimecardDomainService(ILogger logger)
        {
            this.logger = logger;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="timecardToUpdate"></param>
        /// <param name="existingTimecard"></param>
        /// <returns></returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        public TimecardHelper CreateTimecardHelper(Timecard timecardToUpdate, Timecard existingTimecard)
        {
            TimecardHelper helper = new TimecardHelper(timecardToUpdate);

            foreach (var timeEntryToUpdate in timecardToUpdate.TimeEntries)
            {
                // no Id means this is a new time entry...
                if (string.IsNullOrEmpty(timeEntryToUpdate.Id))
                {
                    helper.TimeEntriesToCreate.Add(timeEntryToUpdate);
                }
                else
                {
                    // find the matching exisitng time entry...
                    var existingTimeEntry = existingTimecard.TimeEntries.FirstOrDefault(existing => existing.Equals(timeEntryToUpdate));
                    if (existingTimeEntry != null)
                    {
                        // check for a difference and if found add to the list of time entries to update...
                        if (!timeEntryToUpdate.CompareTimeEntry(existingTimeEntry))
                        {
                            helper.TimeEntriesToUpdate.Add(timeEntryToUpdate);
                        }
                    } 
                }
            }

            // any time entry found in the db that are not in this timecards time entries should be deleted...
            helper.TimeEntriesToDelete = existingTimecard.TimeEntries.Where(e => !timecardToUpdate.TimeEntries.Contains(e)).ToList();
            return helper;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="timecard2ToUpdate"></param>
        /// <param name="existingTimecard2"></param>
        /// <returns></returns>
        public Timecard2Helper CreateTimecard2Helper(Timecard2 timecard2ToUpdate, Timecard2 existingTimecard2)
        {
            Timecard2Helper helper = new Timecard2Helper(timecard2ToUpdate);

            foreach (var timeEntryToUpdate in timecard2ToUpdate.TimeEntries)
            {
                // no Id means this is a new time entry...
                if (string.IsNullOrEmpty(timeEntryToUpdate.Id))
                {
                    helper.TimeEntrysToCreate.Add(timeEntryToUpdate);
                }
                else
                {
                    // find the matching exisitng time entry...
                    var existingTimeEntry = existingTimecard2.TimeEntries.FirstOrDefault(existing => existing.Equals(timeEntryToUpdate));
                    if (existingTimeEntry != null)
                    {
                        // check for a difference and if found add to the list of time entries to update...
                        if (!timeEntryToUpdate.CompareTimeEntry(existingTimeEntry))
                        {
                            helper.TimeEntrysToUpdate.Add(timeEntryToUpdate);
                        }
                    }
                }
            }

            // any time entry found in the db that are not in this timecards time entries should be deleted...
            helper.TimeEntrysToDelete = existingTimecard2.TimeEntries.Where(e => !timecard2ToUpdate.TimeEntries.Contains(e)).ToList();
            return helper;
        }
    }
}
