﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.TimeManagement.Entities;

namespace Ellucian.Colleague.Domain.TimeManagement.Repositories
{
    public interface ITimecardsRepository
    {
        /// <summary>
        /// Gets employee timecards asynchronously
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns>A list of timecards</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use GetTimecards2Async instead")]
        Task<IEnumerable<Timecard>> GetTimecardsAsync(List<string> employeeIds);

        /// <summary>
        /// Gets a single Timecard
        /// </summary>
        /// <param name="timeCardId"></param>
        /// <returns>A timecard task</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use GetTimecard2Async instead")]
        Task<Timecard> GetTimecardAsync(string timecardId);

        /// <summary>
        /// Updates a single Timecard
        /// </summary>
        /// <param name="timeCard"></param>
        /// <returns>A timecard task</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use UpdateTimecard2Async instead")]
        Task<Timecard> UpdateTimecardAsync(TimecardHelper timecardHelper);

        /// <summary>
        /// Create a Timecard
        /// </summary>
        /// <param name="timecard"></param>
        /// <returns>A timecard task</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use CreateTimecard2Async instead")]
        Task<Timecard> CreateTimecardAsync(Timecard timecard);

        /// <summary>
        /// Gets employee timecards asynchronously
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns>A list of timecards</returns>
        Task<IEnumerable<Timecard2>> GetTimecards2Async(List<string> employeeIds);

        /// <summary>
        /// Gets a single Timecard
        /// </summary>
        /// <param name="timeCardId"></param>
        /// <returns>A timecard task</returns>
        Task<Timecard2> GetTimecard2Async(string timecard2Id);

        /// <summary>
        /// Updates a single Timecard
        /// </summary>
        /// <param name="timecard"></param>
        /// <returns>A timecard task</returns>
        Task<Timecard2> UpdateTimecard2Async(Timecard2Helper timecard2Helper);

        /// <summary>
        /// Create a Timecard
        /// </summary>
        /// <param name="timecard2"></param>
        /// <returns>A timecard task</returns>
        Task<Timecard2> CreateTimecard2Async(Timecard2 timecard2);
    }
}
