﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Repositories
{
    public interface ITimecardStatusesRepository
    {

        /// <summary>
        /// Create timecard status
        /// </summary>
        /// <param name="statuses"></param>
        /// <returns></returns>
        Task<IEnumerable<TimecardStatus>> CreateTimecardStatusesAsync(List<TimecardStatus> statuses);

        /// <summary>
        /// Gets all timecard statues for a given timecard
        /// </summary>
        /// <param name="timecardId"></param>
        /// <returns></returns>
        Task<IEnumerable<TimecardStatus>> GetTimecardStatusesByTimecardIdAsync(string timecardId);

        Task<IEnumerable<TimecardStatus>> GetTimecardStatusesByPersonIdsAsync(IEnumerable<string> personIds);
    }
}
