﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Repositories
{
    public interface ITimeEntryCommentsRepository
    {
        Task<IEnumerable<TimeEntryComment>> GetCommentsAsync(IEnumerable<string> employeeIds);

        Task<TimeEntryComment> CreateCommentsAsync(TimeEntryComment timecardComments);
    }
}
