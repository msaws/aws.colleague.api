﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.TimeManagement.Repositories
{
    public interface ITimecardHistoriesRepository
    {
        /// <summary>
        /// Creates a timecard history record
        /// </summary>
        /// <param name="timecard"></param>
        /// <returns></returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        Task<string> CreateTimecardHistoryAsync(Timecard timecard, TimecardStatus timecardStatus, IEnumerable<TimeEntryComment> timeEntryComments);

        /// <summary>
        /// Gets a list of timecardHistory records for a specified date range
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="SupervisorAndEmployeeIds"></param>
        /// <returns></returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        Task<IEnumerable<TimecardHistory>> GetTimecardHistoriesAsync(DateTime startDate, DateTime endDate, IEnumerable<string> SupervisorAndEmployeeIds);

        /// <summary>
        /// Creates a timecard history record
        /// </summary>
        /// <param name="timecard2"></param>
        /// <returns></returns>
        Task<string> CreateTimecardHistory2Async(Timecard2 timecard2, TimecardStatus timecardStatus, IEnumerable<TimeEntryComment> timeEntryComments);

        /// <summary>
        /// Gets a list of timecardHistory records for a specified date range
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="SupervisorAndEmployeeIds"></param>
        /// <returns></returns>
        Task<IEnumerable<TimecardHistory2>> GetTimecardHistories2Async(DateTime startDate, DateTime endDate, IEnumerable<string> SupervisorAndEmployeeIds);
    }
}
