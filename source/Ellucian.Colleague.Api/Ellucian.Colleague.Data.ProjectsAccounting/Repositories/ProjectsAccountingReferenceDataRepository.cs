﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.ColleagueFinance.DataContracts;
using Ellucian.Colleague.Data.ProjectsAccounting.DataContracts;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;

namespace Ellucian.Colleague.Data.ProjectsAccounting.Repositories
{
    /// <summary>
    /// Repository for Colleague Finance reference data
    /// </summary>
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class ProjectsAccountingReferenceDataRepository : BaseColleagueRepository, IProjectsAccountingReferenceDataRepository
    {
        public ProjectsAccountingReferenceDataRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            CacheTimeout = Level1CacheTimeoutValue;
        }

        /// <summary>
        /// Return a list of project line item codes.
        /// </summary>
        public async Task<IEnumerable<ProjectItemCode>> GetLineItemCodesAsync()
        {
            return await GetCodeItemAsync<ProjectsItemCodes, ProjectItemCode>("AllProjectItemCodes", "PROJECTS.ITEM.CODES",
                itemCode => new ProjectItemCode(itemCode.Recordkey, itemCode.PrjicDesc));
        }

        /// <summary>
        /// Return a list of project types.
        /// </summary>
        public async Task<IEnumerable<ProjectType>> GetProjectTypesAsync()
        {
            return await GetValcodeAsync<ProjectType>("CORE", "PROJECT.TYPES",
                projectType =>
                {
                    var x = projectType;
                    return new ProjectType(projectType.ValInternalCodeAssocMember, projectType.ValExternalRepresentationAssocMember);
                }
            );
        }
    }
}
