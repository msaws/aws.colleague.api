﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.ColleagueFinance.DataContracts;
using Ellucian.Colleague.Data.ColleagueFinance.Transactions;
using Ellucian.Colleague.Data.ProjectsAccounting.DataContracts;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.DataContracts;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;

namespace Ellucian.Colleague.Data.ProjectsAccounting.Repositories
{
    /// <summary>
    /// This class implements the IProjectRepository interface.
    /// </summary>
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class ProjectRepository : BaseColleagueRepository, IProjectRepository
    {
        /// <summary>
        /// This constructor allows us to instantiate a project repository object.
        /// </summary>
        /// <param name="cacheProvider">Pass in an ICacheProvider object.</param>
        /// <param name="transactionFactory">Pass in an IColleagueTransactionFactory object.</param>
        /// <param name="logger">Pass in an ILogger object.</param>
        public ProjectRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            // Nothing to do here.
        }

        /// <summary>
        /// Get a list of projects based on the list of IDs supplied.
        /// </summary>
        /// <param name="projectIds">Specify a list of project IDs to determine which projects will be selected.</param>
        /// <param name="summaryOnly">Specify whether or not detailed information will be returned with the project.</param>
        /// <param name="sequenceNumber">Specify a sequence number to return project totals for a single budget period.</param>
        /// <returns>List of project domain entities.</returns>
        public async Task<IEnumerable<Project>> GetProjectsAsync(IEnumerable<string> projectIds, bool summaryOnly, string sequenceNumber)
        {
            if (projectIds == null || !projectIds.Any())
            {
                logger.Warn("Project IDs should not be null or empty.");
                return new List<Project>();
            }

            // Always get the current project and project line item information from the repository.
            // We are not going to cache the project/project line item data. So first we got the 
            // list of project ids assigned to the user and then we get the project and project
            // line item information for those ids.
            List<Project> projectDomainEntities = new List<Project>();
            var projectContracts = await DataReader.BulkReadRecordAsync<Projects>(projectIds.ToArray(), true);

            if (projectContracts == null)
            {
                return new List<Project>();
            }

            // Remove any null projects from the list.
            var workingProjectsContractsList = projectContracts.Where(x => x != null).ToList();

            var criteria = "PRJLN.PROJECTS.CF EQ '?'";
            var lineItemIds = await DataReader.SelectAsync("PROJECTS.LINE.ITEMS", criteria, workingProjectsContractsList.Select(x => x.Recordkey).ToArray());
            var lineItems = await DataReader.BulkReadRecordAsync<ProjectsLineItems>(lineItemIds, true);

            // Initialize a master list for all the GL accounts from all the line item for a project.
            var glNumbersForAllProjects = new List<string>();
            if (lineItems != null)
            {
                glNumbersForAllProjects = lineItems.Where(x => x != null).SelectMany(x => x.PrjlnGlAccts).Distinct().ToList();
            }

            // Obtain the descriptions for all the GL accounts in the line item.
            // Create GL account domain entities for each one and then add them to the project line item domain entity.
            GetGlAccountDescriptionRequest glAccountDescriptionRequest = new GetGlAccountDescriptionRequest()
            {
                GlAccountIds = glNumbersForAllProjects,
                Module = "SS"
            };
            GetGlAccountDescriptionResponse glAccountDescriptionResponse = null;
            Collection<Ellucian.Colleague.Data.ProjectsAccounting.DataContracts.ProjectsCf> projectsCfList = null;
            if (!summaryOnly)
            {
                projectsCfList = await DataReader.BulkReadRecordAsync<Ellucian.Colleague.Data.ProjectsAccounting.DataContracts.ProjectsCf>(
                    projectContracts.Select(x => x.Recordkey).ToArray(), true);
                glAccountDescriptionResponse = await transactionInvoker.ExecuteAsync<GetGlAccountDescriptionRequest, GetGlAccountDescriptionResponse>(glAccountDescriptionRequest);
            }

            foreach (var projectContract in workingProjectsContractsList)
            {
                try
                {
                    #region Build the project entities
                    // Check for data corruption in the Project Status.
                    // Project Status cannot be null.
                    if (string.IsNullOrEmpty(projectContract.PrjCurrentStatus))
                    {
                        throw new ArgumentNullException("PrjCurrentStatus", "PrjCurrentStatus must have a value");
                    }

                    // Translate the status code into a ProjectStatus enumeration value
                    ProjectStatus projectStatus = new ProjectStatus();
                    switch (projectContract.PrjCurrentStatus.ToUpper())
                    {
                        case "A":
                            projectStatus = ProjectStatus.Active;
                            break;
                        case "I":
                            projectStatus = ProjectStatus.Inactive;
                            break;
                        case "X":
                            projectStatus = ProjectStatus.Closed;
                            break;
                        default:
                            // if we get here, we have corrupt data.
                            throw new ApplicationException("Invalid project status for project: " + projectContract.Recordkey);
                    }

                    var projectDomainEntity = new Project(projectContract.Recordkey, projectContract.PrjRefNo, projectContract.PrjTitle, projectStatus, projectContract.PrjType);
                    #endregion

                    #region Build project budget periods
                    // Only include budget periods if we're in detail mode
                    if (!summaryOnly && projectsCfList != null)
                    {
                        // Read the PROJECTS.CF record, loop through the project budget periods, and add them to the project domain entity.
                        // If there is no PROJECTS.CF record then log a warning and skip the budget periods.
                        // If there are any budget periods that do not have a start date and an end date, throw an application exception.  
                        var projectsCf = projectsCfList.FirstOrDefault(x => x.Recordkey == projectContract.Recordkey);
                        if (projectsCf == null)
                        {
                            logger.Warn("projectsCf cannot be null. The PROJECTS.CF record for project " + projectContract.Recordkey + " appears to be missing.");
                        }
                        else
                        {
                            if (projectsCf.PrjcfPeriodsEntityAssociation != null)
                            {
                                foreach (var budgetPeriodAssoc in projectsCf.PrjcfPeriodsEntityAssociation)
                                {
                                    if (budgetPeriodAssoc.PrjcfPeriodStartDatesAssocMember.HasValue && budgetPeriodAssoc.PrjcfPeriodEndDatesAssocMember.HasValue)
                                    {
                                        string budgetPeriodSequenceNumber = budgetPeriodAssoc.PrjcfPeriodSeqNoAssocMember;
                                        DateTime budgetPeriodStartDate = budgetPeriodAssoc.PrjcfPeriodStartDatesAssocMember.Value;
                                        DateTime budgetPeriodEndDate = budgetPeriodAssoc.PrjcfPeriodEndDatesAssocMember.Value;
                                        ProjectBudgetPeriod bdgtPeriod = new ProjectBudgetPeriod(budgetPeriodSequenceNumber, budgetPeriodStartDate, budgetPeriodEndDate);
                                        projectDomainEntity.AddBudgetPeriod(bdgtPeriod);
                                    }
                                    else
                                    {
                                        // if we get here, we have corrupt data.
                                        throw new ApplicationException("Both Budget Period Start Date and Budget Period End Date are required");
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                    #region Build project line items
                    if (lineItems != null)
                    {
                        var projectLineItems = lineItems.Where(x => x != null && x.PrjlnProjectsCf == projectContract.Recordkey).ToList();
                        foreach (var lineItem in projectLineItems)
                        {
                            GlClass glClass = new GlClass();
                            switch (lineItem.PrjlnGlClassType.ToUpper())
                            {
                                case "E":
                                    glClass = GlClass.Expense;
                                    break;
                                case "A":
                                    glClass = GlClass.Asset;
                                    break;
                                case "R":
                                    glClass = GlClass.Revenue;
                                    break;
                                case "F":
                                    glClass = GlClass.FundBalance;
                                    break;
                                case "L":
                                    glClass = GlClass.Liability;
                                    break;
                                default:
                                    // if we get here, we have corrupt data.
                                    throw new ApplicationException("Invalid GL class for project line item: " + lineItem.Recordkey);
                            }

                            // Include all types of project line items.
                            var lineItemDomainEntity = new ProjectLineItem(lineItem.Recordkey, glClass, lineItem.PrjlnProjectItemCode);

                            // Inlcude GL account number and descriptions only if we're in "detail" mode.
                            if (summaryOnly == false)
                            {
                                // The transaction returns the description for each GL account number.
                                if (glAccountDescriptionResponse != null &&
                                    glAccountDescriptionResponse.GlAccountIds != null &&
                                    glAccountDescriptionResponse.GlDescriptions != null)
                                {
                                    foreach (var glNumber in lineItem.PrjlnGlAccts)
                                    {
                                        var glAccountIndex = glAccountDescriptionResponse.GlAccountIds.IndexOf(glNumber);
                                        ProjectLineItemGlAccount glAccount = new ProjectLineItemGlAccount(glAccountDescriptionResponse.GlAccountIds[glAccountIndex]);
                                        glAccount.GlAccountDescription = glAccountDescriptionResponse.GlDescriptions[glAccountIndex];
                                        lineItemDomainEntity.AddGlAccount(glAccount);
                                    }
                                }
                            }

                            foreach (var budgetPeriodAmounts in lineItem.PrjlnBudgetEntityAssociation)
                            {
                                BudgetPeriodAmount budgetPeriodAmount = new BudgetPeriodAmount(budgetPeriodAmounts.PrjlnPeriodSeqNosAssocMember,
                                    budgetPeriodAmounts.PrjlnBudgetAmtsAssocMember ?? 0,
                                    budgetPeriodAmounts.PrjlnRequisitionMemosAssocMember ?? 0,
                                    budgetPeriodAmounts.PrjlnEncumbranceMemosAssocMember ?? 0,
                                    budgetPeriodAmounts.PrjlnEncumbrancePostedAssocMember ?? 0,
                                    budgetPeriodAmounts.PrjlnActualMemosAssocMember ?? 0,
                                    budgetPeriodAmounts.PrjlnActualPostedAssocMember ?? 0);

                                lineItemDomainEntity.AddBudgetPeriodAmount(budgetPeriodAmount);
                            }
                            projectDomainEntity.AddLineItem(lineItemDomainEntity);
                        }
                    }
                    #endregion

                    projectDomainEntities.Add(projectDomainEntity);
                }
                catch (ArgumentNullException anex)
                {
                    LogDataError("Project", projectContract.Recordkey, projectContract, anex, anex.Message);
                }
                catch (ApplicationException ae)
                {
                    LogDataError("Project", projectContract.Recordkey, projectContract, ae, ae.Message);
                }
            }

            try
            {
                // Only include PA.GLA information if we're in detail mode.
                // Detail means we are looking at only one project.
                if (summaryOnly == false && projectIds.Count() == 1)
                {
                    // At this point we are only getting detail for one project.
                    // Get all of the project transactions from PA.GLA.

                    // Read the transactions associated to the project GL accounts and prepare the
                    // select query to specify the project ID so we don't get unneeded transactions.
                    string projectId = projectIds.First();
                    var searchString = "PA.GLA.GL.NO EQ '?' AND WITH PA.GLA.PROJECT.ID EQ '" + projectId + "'";

                    // If we're supposed to look at a specific budget period then add it to the query
                    if (!string.IsNullOrEmpty(sequenceNumber))
                    {
                        searchString += " AND WITH PA.GLA.BUDGET.PERIOD EQ '" + sequenceNumber + "'";
                    }

                    // Sort the selected PA.GLA records by descendent transaction date.
                    var sortString = " BY.DSND PA.GLA.TR.DATE";
                    searchString += sortString;

                    var transactionIds = await DataReader.SelectAsync("PA.GLA", searchString, glNumbersForAllProjects.ToArray());
                    var projectTransactions = await DataReader.BulkReadRecordAsync<PaGla>(transactionIds, true);

                    // Get the GL.SOURCE.CODES valcode table
                    var GlSourceCodesValidationTable = await GetGlSourceCodesAsync();

                    ApplValcodesVals GLSourceCode = new ApplValcodesVals();
                    var projectTransactionDomainEntities = new List<ProjectTransaction>();
                    foreach (var transaction in projectTransactions)
                    {
                        try
                        {
                            #region Determine whether this is an Actual, Encumbrance, or Requisition transaction
                            GlTransactionType type = GlTransactionType.Actual;
                            GLSourceCode = GlSourceCodesValidationTable.ValsEntityAssociation.Where(x =>
                                x.ValInternalCodeAssocMember == transaction.PaGlaSource).FirstOrDefault();

                            switch (GLSourceCode.ValActionCode2AssocMember)
                            {
                                case "1":
                                    type = GlTransactionType.Actual;
                                    break;
                                case "3":
                                    type = GlTransactionType.Encumbrance;
                                    break;
                                case "4":
                                    type = GlTransactionType.Requisition;
                                    break;
                                default:
                                    // Log an error for bad data
                                    // There should be any budget or wrong source codes
                                    throw new ApplicationException("Unrecognizable GL Source Code: " + transaction.PaGlaSource);
                            }
                            #endregion

                            // Calculate the transaction amount.
                            decimal amount = (transaction.PaGlaDebit ?? 0) - (transaction.PaGlaCredit ?? 0);

                            // Check that the transaction has a date; otherwise throw an exception 
                            // and do not add this transaction to the domain.
                            DateTime transactionDate;
                            if (transaction.PaGlaTrDate.HasValue)
                            {
                                transactionDate = transaction.PaGlaTrDate.Value;
                            }
                            else
                            {
                                throw new ApplicationException("Missing transaction date for transaction: " + transaction.Recordkey);
                            }

                            var projectTransaction = new ProjectTransaction(transaction.Recordkey, type, transaction.PaGlaSource, transaction.PaGlaGlNo, amount, transaction.PaGlaRefNo, transactionDate, transaction.PaGlaDescription, transaction.PaGlaPrjItemId);
                            projectTransactionDomainEntities.Add(projectTransaction);
                        }
                        catch (ApplicationException ae)
                        {
                            logger.Info(ae.Message);
                        }
                        catch (Exception ex)
                        {
                            logger.Info(ex.Message);
                        }
                    }

                    // We only execute this code when dealing with one project, but projectDomainEntities is a list.
                    // So we still have to loop through our list of one.
                    foreach (var project in projectDomainEntities)
                    {
                        foreach (var projectLineItem in project.LineItems)
                        {
                            foreach (var glAccount in projectLineItem.GlAccounts)
                            {
                                // Get the transactions for this GL number
                                var transactions = projectTransactionDomainEntities.Where(x =>
                                    x.GlAccount == glAccount.GlAccountNumber
                                    && x.ProjectLineItemId == projectLineItem.Id).ToList();
                                foreach (var transaction in transactions)
                                {
                                    // Add each transaction to the object consolidating the encumbrance and requisition ones.
                                    glAccount.AddProjectTransaction(transaction);
                                }

                                // Remove those encumbrance or Requisition transactions that have a $0.00 amount.
                                glAccount.RemoveZeroEncumbranceTransactions();
                            }
                        }

                        #region Assign REQ/PO/BPO IDs
                        // Get the encumbrance and requisition transactions in the project.
                        List<string> requisitionsReferenceNumbers = new List<string>();
                        List<string> purchaseOrdersReferenceNumbers = new List<string>();
                        List<string> blanketPurchaseOrdersReferenceNumbers = new List<string>();

                        #region Get Reference Numbers
                        foreach (var projectLineItem in project.LineItems)
                        {
                            foreach (var glAccount in projectLineItem.GlAccounts)
                            {
                                var encumbranceTransactions = glAccount.ProjectTransactions.Where(x =>
                                    x.GlTransactionType == GlTransactionType.Encumbrance || x.GlTransactionType == GlTransactionType.Requisition);

                                // Put together a list of requisition, purchase order and bpo numbers to pass to the transaction.
                                foreach (var encumbranceTransaction in encumbranceTransactions)
                                {
                                    // If the transaction is a requisition one, add the reference number
                                    // to the list if it is not in there already.
                                    if (encumbranceTransaction.GlTransactionType == GlTransactionType.Requisition)
                                    {
                                        if (!requisitionsReferenceNumbers.Contains(encumbranceTransaction.ReferenceNumber))
                                        {
                                            requisitionsReferenceNumbers.Add(encumbranceTransaction.ReferenceNumber);
                                        }
                                    }

                                    else
                                    {
                                        // Get the PO/BPO id for the document number. The PO/BPO
                                        // numbers are prefixed by P or B respectively.
                                        if (!string.IsNullOrEmpty(encumbranceTransaction.ReferenceNumber) &&
                                            encumbranceTransaction.ReferenceNumber.Any())
                                        {
                                            char prefix = encumbranceTransaction.ReferenceNumber[0];
                                            if (prefix.ToString().ToUpper() == "P")
                                            {
                                                if (!purchaseOrdersReferenceNumbers.Contains(encumbranceTransaction.ReferenceNumber))
                                                {
                                                    purchaseOrdersReferenceNumbers.Add(encumbranceTransaction.ReferenceNumber);
                                                }
                                            }
                                            else
                                            {
                                                if (prefix.ToString().ToUpper() == "B")
                                                {
                                                    if (!blanketPurchaseOrdersReferenceNumbers.Contains(encumbranceTransaction.ReferenceNumber))
                                                    {
                                                        blanketPurchaseOrdersReferenceNumbers.Add(encumbranceTransaction.ReferenceNumber);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion

                        #region Get Document IDs
                        // Call a Colleague Transaction to obtain the document ID for each reference number
                        GetPurchasingDocumentIdsRequest request = new GetPurchasingDocumentIdsRequest()
                        {
                            ReqNumbers = requisitionsReferenceNumbers,
                            PoNumbers = purchaseOrdersReferenceNumbers,
                            BpoNumbers = blanketPurchaseOrdersReferenceNumbers
                        };
                        GetPurchasingDocumentIdsResponse response = await transactionInvoker.ExecuteAsync<GetPurchasingDocumentIdsRequest, GetPurchasingDocumentIdsResponse>(request);

                        // If there are any ids to assign to the numbers, then process them.
                        if (response != null)
                        {
                            if ((response.ReqNumbers != null) || (response.PoNumbers != null) || (response.BpoNumbers != null))
                            {
                                foreach (var projectLineItem in project.LineItems)
                                {
                                    foreach (var glAccount in projectLineItem.GlAccounts)
                                    {
                                        if ((response.ReqNumbers != null) && (response.ReqNumbers.Count() != 0))
                                        {
                                            // Select only the requisition transactions.
                                            var requisitionTransactions = glAccount.ProjectTransactions.Where(x => x.GlTransactionType == GlTransactionType.Requisition).ToList();

                                            // Loop through each requisition transaction to match the id to the number.
                                            if (requisitionTransactions != null)
                                            {
                                                foreach (var requisitionTransaction in requisitionTransactions)
                                                {
                                                    // Find the requisition number and assign its ID.
                                                    for (int x = 0; x < response.ReqIds.Count(); x++)
                                                    {
                                                        if (requisitionTransaction.ReferenceNumber == response.ReqNumbers[x])
                                                        {
                                                            requisitionTransaction.DocumentId = response.ReqIds[x];
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        if (((response.PoNumbers != null) && (response.PoNumbers.Count() != 0)) || ((response.BpoNumbers != null) && (response.BpoNumbers.Count() != 0)))
                                        {
                                            // Get the PO/BPO id for the document number. The PO/BPO
                                            // number are 7 digits long prefixed by P or B respectively.

                                            // Select only the encumbrance transactions.
                                            var encumbranceTransactions = glAccount.ProjectTransactions.Where(x => x.GlTransactionType == GlTransactionType.Encumbrance);

                                            // Loop through the encumbrance transactions and determine which ones are PO or BPO ones.
                                            if (encumbranceTransactions != null)
                                            {
                                                foreach (var encumbranceTransaction in encumbranceTransactions)
                                                {
                                                    // Get the PO/BPO id for the document number. The PO/BPO
                                                    // numbers are prefixed by P or B respectively.
                                                    if (!string.IsNullOrEmpty(encumbranceTransaction.ReferenceNumber) &&
                                                        encumbranceTransaction.ReferenceNumber.Any())
                                                    {
                                                        char prefix = encumbranceTransaction.ReferenceNumber[0];
                                                        if (prefix.ToString().ToUpper() == "P")
                                                        {
                                                            // Find the purchase order number and assign its ID.
                                                            if ((response.PoNumbers != null) && (response.PoNumbers.Count() != 0))
                                                            {
                                                                for (int x = 0; x < response.PoIds.Count(); x++)
                                                                {
                                                                    if (encumbranceTransaction.ReferenceNumber == response.PoNumbers[x])
                                                                    {
                                                                        encumbranceTransaction.DocumentId = response.PoIds[x];
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else
                                                        {
                                                            if (prefix.ToString().ToUpper() == "B")
                                                            {
                                                                // Find the blanket purchase order number and assign its ID.
                                                                if ((response.BpoNumbers != null) && (response.BpoNumbers.Count() != 0))
                                                                {
                                                                    for (int x = 0; x < response.BpoIds.Count(); x++)
                                                                    {
                                                                        if (encumbranceTransaction.ReferenceNumber == response.BpoNumbers[x])
                                                                        {
                                                                            encumbranceTransaction.DocumentId = response.BpoIds[x];
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                // If any of the required project information is null, log an error.
                LogDataError("Projects", "no ID to report", new Object(), ex, ex.Message);
            }

            return projectDomainEntities;
        }

        /// <summary>
        /// Get a single project.
        /// </summary>
        /// <param name="projectId">Specify a project ID to determine which project to select.</param>
        /// <param name="summaryOnly">Specify whether or not detailed information will be returned with the project.</param>
        /// <param name="sequenceNumber">Specify a sequence number to return project totals for a single budget period.</param>
        /// <returns>Project domain entity.</returns>
        public async Task<Project> GetProjectAsync(string projectId, bool summaryOnly, string sequenceNumber)
        {
            if (string.IsNullOrEmpty(projectId))
            {
                throw new ArgumentNullException("projectId", "Parameter projectId may not be null or empty.");
            }

            // We are dealing only with one project but GetProjects needs
            // to be passded in a list so we declare our variable as such.
            var project = new List<string>();
            project.Add(projectId);
            var projects = await GetProjectsAsync(project, summaryOnly, sequenceNumber);

            // Only one project is returned but projects is cast as a list.
            return projects.FirstOrDefault();
        }

        /// <summary>
        /// Get the GL Source Codes from Colleague.
        /// </summary>
        /// <returns>ApplValcodes association of GL Source Codes data.</returns>
        private async Task<ApplValcodes> GetGlSourceCodesAsync()
        {
            var GlSourceCodesValidationTable = new ApplValcodes();
            try
            {
                // Verify that it is populated. If not, throw an error.
                GlSourceCodesValidationTable = await GetOrAddToCacheAsync<ApplValcodes>("GlSourceCodes",
                    async () =>
                    {
                        ApplValcodes GlSourceCodesValTable = await DataReader.ReadRecordAsync<ApplValcodes>("CF.VALCODES", "GL.SOURCE.CODES");
                        if (GlSourceCodesValTable == null)
                        {
                            logger.Info("GL.SOURCE.CODES validation table data is null.");
                            throw new Exception();
                        }
                        return GlSourceCodesValTable;
                    }, Level1CacheTimeoutValue);

                return GlSourceCodesValidationTable;
            }
            catch (Exception)
            {
                var errorMessage = "Unable to retrieve GL.SOURCE.CODES validation table from Colleague.";
                logger.Info(errorMessage);
                throw new Exception(errorMessage);
            }
        }
    }
}