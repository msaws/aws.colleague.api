﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Data.Base.Repositories;
using Ellucian.Colleague.Data.ColleagueFinance.DataContracts;
using Ellucian.Colleague.Data.ColleagueFinance.Transactions;
using Ellucian.Colleague.Data.ProjectsAccounting.Transactions;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Web.Http.Configuration;
using slf4net;

namespace Ellucian.Colleague.Data.ProjectsAccounting.Repositories
{
    /// <summary>
    /// This class implements the IProjectsAccountingUserRepository interface to extract project user information from Colleague.
    /// </summary>
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class ProjectsAccountingUserRepository : PersonRepository, IProjectsAccountingUserRepository
    {
        /// <summary>
        /// This constructor allows us to instantiate a project user repository object.
        /// </summary>
        /// <param name="cacheProvider">Pass in an ICacheProvider object.</param>
        /// <param name="transactionFactory">Pass in an IColleagueTransactionFactory object.</param>
        /// <param name="logger">Pass in an ILogger object.</param>
        public ProjectsAccountingUserRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings apiSettings)
            : base(cacheProvider, transactionFactory, logger, apiSettings)
        {
            // Nothing to do here.
        }
        /// <summary>
        /// This method gets the projects that are assigned to the user.
        /// </summary>
        /// <param name="id">This is the ID of the user logged in.</param>
        /// <returns>Returns the list of project IDs that are assigned to the user.</returns>
        public async Task<ProjectsAccountingUser> GetProjectsAccountingUserAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id", "ID may not be null or empty");
            }

            // Get PERSON data contract.
            Person personContract = await DataReader.ReadRecordAsync<Person>("PERSON", id);

            GetProjectsForPaUserRequest request = new GetProjectsForPaUserRequest()
            {
                PersonId = id
            };
            GetProjectsForPaUserResponse response = transactionInvoker.Execute<GetProjectsForPaUserRequest, GetProjectsForPaUserResponse>(request);
            var projectsAccountingUserEntity = BuildProjectsAccountingUser(personContract, response.UserProjects);
            if (!string.IsNullOrEmpty(response.Msg))
            {
                if (response.Msg.Length > 0)
                {
                    logger.Info(response.Msg);
                }
            }
            return projectsAccountingUserEntity;
        }

        /// <summary>
        /// This method builds a ProjectAccountingUser object that contains the user ID, and the list of assigned project IDs.
        /// </summary>
        /// <param name="id">This is the user ID.</param>
        /// <param name="personContract">This is the data contract for the PERSON entity.</param>
        /// <param name="projectIds">This is the list of project IDs associated with the user.</param>
        /// <param name="message">This is response message from the Colleague transaction.</param>
        /// <returns>Returns the ProjectsAccountingUser object.</returns>
        private ProjectsAccountingUser BuildProjectsAccountingUser(Person personContract, IEnumerable<string> projectIds)
        {
            // Build the Person object.
            var projectsAccountingUserEntity = Get<ProjectsAccountingUser>(personContract.Recordkey,
                person => new ProjectsAccountingUser(person.Recordkey, person.LastName));

            // Initialize the list of project IDs and then add project IDs to the list.
            projectsAccountingUserEntity.RemoveAll();
            projectsAccountingUserEntity.AddProjects(projectIds);
            return projectsAccountingUserEntity;
        }
    }
}
