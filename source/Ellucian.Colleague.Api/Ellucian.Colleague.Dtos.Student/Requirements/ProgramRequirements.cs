﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.Student.Requirements
{
    /// <summary>
    /// The requirements and rules that must be evaluated against coursework to determine if
    /// a student has completed a Program, or what remains to be done.
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Ellucian.StyleCop.WebApi.EllucianWebApiDtoAnalyzer", "EL1000:NoPublicFieldsOnDtos", Justification = "Already released. Risk of breaking change.")] 
    public class ProgramRequirements
    {
        /// <summary>
        /// The code (unique Id) of the academic program
        /// </summary>
        public string ProgramCode;
        /// <summary>
        /// The code (unique Id) of the catalog
        /// </summary>
        public string CatalogCode;
        /// <summary>
        /// Minimum number of credits required
        /// </summary>
        public decimal MinimumCredits;
        /// <summary>
        /// The maximum number of credits allowed for program completion. Maximum Credits
        /// are used by Financial Aid Academic Progress Evaluations
        /// </summary>
        public decimal? MaximumCredits { get; set; }
        /// <summary>
        /// Minimum number of institutional credits required
        /// </summary>
        public decimal MinimumInstitutionalCredits;
        /// <summary>
        /// Minimum required overall GPA.
        /// </summary>
        public decimal MinOverallGpa;
        /// <summary>
        /// Minimum required institutional GPA.
        /// </summary>
        public decimal MinInstGpa;
        /// <summary>
        /// Grade scheme used for calculating GPA and credits
        /// </summary>
        public string GradeSchemeCode;
        /// <summary>
        /// Body of requirements, subrequirements and groups that must be satisfied to complete this program
        /// <see cref="Requirement"/>
        /// </summary>
        public List<Requirement> Requirements;

        /// <summary>
        /// Override method for ToString 
        /// </summary>
        /// <returns>Program code followed by an asterisk followed by the catalog year</returns>
        public override string ToString()
        {
            return ProgramCode + "*" + CatalogCode;
        }

    }
   
}
