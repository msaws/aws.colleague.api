﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.Student
{
    /// <summary>
    /// Enumeration of possible values for person type
    /// </summary>
    public enum PersonType
    {
        /// <summary>
        /// Student
        /// </summary>
        Student,
        /// <summary>
        /// Advisor
        /// </summary>
        Advisor
    }
}
