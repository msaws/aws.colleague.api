﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Ellucian.Colleague.Dtos.Student
{
    /// <summary>
    /// Section registration action request
    /// </summary>
    public class SectionRegistration
    {
        /// <summary>
        /// Id of section for registration request
        /// </summary>
        public string SectionId { get; set; }
        /// <summary>
        /// <see cref="RegistrationAction">RegistrationAction</see> to take (e.g., Add, Drop, Audit, etc)
        /// </summary>
        public RegistrationAction Action { get; set; }
        /// <summary>
        /// Decimal credits to register, only for variable credit sections
        /// </summary>
        public decimal? Credits { get; set; }
    }

}
