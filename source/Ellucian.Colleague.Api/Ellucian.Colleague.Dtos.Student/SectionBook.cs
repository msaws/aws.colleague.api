﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.Student
{
    /// <summary>
    /// A book that is listed in the list of books for a section
    /// </summary>
    public class SectionBook
    {
        /// <summary>
        /// The unique Id of the book
        /// </summary>
        public string BookId { get; set; }
        /// <summary>
        /// Indicates whether this book is required
        /// </summary>
        public bool IsRequired { get; set; }
    }
}
