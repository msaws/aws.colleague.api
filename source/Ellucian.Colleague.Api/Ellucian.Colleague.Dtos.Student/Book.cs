﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.Student
{
    /// <summary>
    /// The Id and price for a book
    /// </summary>
    public class Book
    {
        /// <summary>
        /// Unique system Id for the book
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Decimal price this book
        /// </summary>
        public decimal? Price { get; set; }
    }
}
