﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.ProjectsAccounting
{
    /// <summary>
    /// Project query input criteria
    /// </summary>
    public class ProjectQueryCriteria
    {
        /// <summary>
        /// Default constructor
        /// </summary>
        public ProjectQueryCriteria()
        {

        }

        /// <summary>
        /// List of status codes to use as query criteria
        /// </summary>
        public IEnumerable<string> StatusCodes { get; set; }

        /// <summary>
        /// List of type codes to use as query criteria
        /// </summary>
        public IEnumerable<string> TypeCodes { get; set; }
    }
}
