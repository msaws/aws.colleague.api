﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.ProjectsAccounting
{
    /// <summary>
    /// Project line item code
    /// </summary>
    public class ProjectItemCode
    {
        /// <summary>
        /// Item code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Item code description
        /// </summary>
        public string Description { get; set; }
    }
}
