﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.ProjectsAccounting
{
    /// <summary>
    /// Type of the project
    /// </summary>
    public class ProjectType
    {
        /// <summary>
        /// Project type code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Project type description
        /// </summary>
        public string Description { get; set; }
    }
}