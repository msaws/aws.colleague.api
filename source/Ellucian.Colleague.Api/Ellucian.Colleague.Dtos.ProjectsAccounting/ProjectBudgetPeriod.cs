﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.ProjectsAccounting
{
    /// <summary>
    /// This is the project budget period.
    /// </summary>
    public class ProjectBudgetPeriod
    {
        /// <summary>
        /// This is the unique budget period identifier for the project
        /// </summary>
        public string SequenceNumber { get; set; }

        /// <summary>
        /// This is the project budget period start date
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// This is the project budget period end date
        /// </summary>
        public DateTime EndDate { get; set; }

    }
}
