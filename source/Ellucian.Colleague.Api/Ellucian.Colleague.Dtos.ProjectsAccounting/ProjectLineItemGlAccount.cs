﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ColleagueFinance;


namespace Ellucian.Colleague.Dtos.ProjectsAccounting
{
    /// <summary>
    /// This is the project line item GL Account
    /// </summary>
    public class ProjectLineItemGlAccount
    {
        /// <summary>
        /// This is the GL account for the project line item
        /// </summary>
        public string GlAccount { get; set; }

        /// <summary>
        /// This is the GL account for the project line item formatted for display.
        /// </summary>
        public string FormattedGlAccount { get; set; }

        /// <summary>
        /// This the GL account description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The total actuals amount for this GL account
        /// </summary>
        public decimal Actuals { get; set; }

        /// <summary>
        /// The total encumbrances amount for this GL account
        /// </summary>
        public decimal Encumbrances { get; set; }

        /// <summary>
        /// List of encumbrance and requisition transactions for this GL Account
        /// </summary>
        public List<GlTransaction> EncumbranceGlTransactions { get; set; }

        /// <summary>
        /// List of actuals transactions for this GL Account
        /// </summary>
        public List<GlTransaction> ActualsGlTransactions { get; set; }
    }
}
