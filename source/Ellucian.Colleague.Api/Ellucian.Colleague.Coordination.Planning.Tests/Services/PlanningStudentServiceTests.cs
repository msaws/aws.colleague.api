﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Http.TestUtil;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Collections;
using System;
using System.Linq;
using Ellucian.Colleague.Coordination.Planning.Services;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Domain.Planning.Repositories;
using Ellucian.Colleague.Domain.Planning.Entities;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Colleague.Domain.Planning.Tests;
using Ellucian.Colleague.Domain.Student.Entities.Requirements;
using Ellucian.Colleague.Domain.Entities;
using slf4net;
using Ellucian.Web.Adapters;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Domain.Student;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Planning.Tests.Services
{

    [TestClass]
    public class PlanningStudentServiceTests
    {

        [TestClass]
        public class PlanningStudentServiceTests_GetPlanningStudent : CurrentUserSetup
        {
            private IAdapterRegistry adapterRegistry;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private Mock<IPlanningStudentRepository> planningStudentRepoMock;
            private IPlanningStudentRepository planningStudentRepo;
            private IRoleRepository roleRepository;
            private Mock<IRoleRepository> roleRepoMock;
            private PlanningStudentService planningStudentService;
            private Dumper dumper;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;
            private IStudentRepository studentRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IConfigurationRepository baseConfigurationRepository;
            private Mock<IConfigurationRepository> baseConfigurationRepositoryMock;


            string studentId;
            Domain.Planning.Entities.PlanningStudent planningStudent;


            [TestInitialize]
            public void Initialize()
            {
                planningStudentRepoMock = new Mock<IPlanningStudentRepository>();
                planningStudentRepo = planningStudentRepoMock.Object;
                dumper = new Dumper();
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                logger = new Mock<ILogger>().Object;
                currentUserFactory = new StudentUserFactory();
                roleRepoMock = new Mock<IRoleRepository>();
                roleRepository = roleRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                baseConfigurationRepositoryMock = new Mock<IConfigurationRepository>();
                baseConfigurationRepository = baseConfigurationRepositoryMock.Object;

                var planningStudentAdapter = new AutoMapperAdapter<Domain.Planning.Entities.PlanningStudent, Ellucian.Colleague.Dtos.Planning.PlanningStudent>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Domain.Planning.Entities.PlanningStudent, Ellucian.Colleague.Dtos.Planning.PlanningStudent>()).Returns(planningStudentAdapter);
                var advisementAdapter = new AutoMapperAdapter<Domain.Student.Entities.Advisement, Ellucian.Colleague.Dtos.Student.Advisement>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Domain.Student.Entities.Advisement, Ellucian.Colleague.Dtos.Student.Advisement>()).Returns(advisementAdapter);

                studentId = "0000894";
                planningStudent = new Domain.Planning.Entities.PlanningStudent(studentId, "brown", 3, new List<string>() { "ENGL.BA", "MATH.BS" });
                // This will be the response regardless of the student requested--it's ok.
                planningStudentRepoMock.Setup(repo => repo.GetAsync(It.IsAny<string>())).Returns(Task.FromResult(planningStudent));

                planningStudentService = new PlanningStudentService(adapterRegistry, planningStudentRepo, currentUserFactory, roleRepository, logger, studentRepo, baseConfigurationRepository);
            }

            [TestMethod]
            public async Task UserIsSelf_ReturnsPlanningStudent()
            {
                // Act
                var planningStudentResponse = await planningStudentService.GetPlanningStudentAsync(studentId);

                // Assert - spot-check the response
                Assert.AreEqual(planningStudent.Id, planningStudentResponse.Id);
                Assert.AreEqual(planningStudent.LastName, planningStudentResponse.LastName);
                Assert.AreEqual(planningStudent.ProgramIds.Count(), planningStudentResponse.ProgramIds.Count());
            }

            [TestMethod]
            public async Task PlanningStudent_With_PrivacyStatusCode()
            {
                planningStudent = new Domain.Planning.Entities.PlanningStudent(studentId, "brown", 3, new List<string>() { "ENGL.BA", "MATH.BS" }, "S");
                planningStudentRepoMock.Setup(repo => repo.GetAsync(It.IsAny<string>())).Returns(Task.FromResult(planningStudent));
                planningStudentService = new PlanningStudentService(adapterRegistry, planningStudentRepo, currentUserFactory, roleRepository, logger, studentRepo, baseConfigurationRepository);

                var planningStudentResponse = await planningStudentService.GetPlanningStudentAsync(studentId);
                Assert.AreEqual(planningStudent.PrivacyStatusCode, planningStudentResponse.PrivacyStatusCode);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task AdvisorWithViewAssignedAccess_NotAssigned_ThrowsException()
            {
                // Arrange
                currentUserFactory = new AdvisorUserFactory();
                planningStudentService = new PlanningStudentService(adapterRegistry, planningStudentRepo, currentUserFactory, roleRepository, logger, studentRepo, baseConfigurationRepository);

                // Set up view assigned advisee permissions on advisor's role--so that advisor cannot access this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.ViewAssignedAdvisees));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                // Act
                var result = await planningStudentService.GetPlanningStudentAsync("0000894");
            }

            [TestMethod]
            public async Task AdvisorWithViewAssignedAccess_AssignedToStudent_ReturnsResult()
            {
                // Arrange
                // In TestStudentRepository, Student 0004012 has advisor 0000111 (Id for advisor from AdvisorUserFactory) set as current advisor
                studentId = "00004012";
                planningStudent = new Domain.Planning.Entities.PlanningStudent(studentId, "brown", 3, new List<string>() { "ENGL.BA", "MATH.BS" });
                planningStudent.AddAdvisement("0000111", null, null, null);
                planningStudentRepoMock.Setup(repo => repo.GetAsync(It.IsAny<string>())).Returns(Task.FromResult(planningStudent));

                // View Assigned advisees permissions
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.ViewAssignedAdvisees));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });
                currentUserFactory = new AdvisorUserFactory();
                planningStudentService = new PlanningStudentService(adapterRegistry, planningStudentRepo, currentUserFactory, roleRepository, logger, studentRepo, baseConfigurationRepository);

                // Act
                var planningStudentResponse =await planningStudentService.GetPlanningStudentAsync(studentId);

                // Assert - spot-check the response
                Assert.AreEqual(planningStudent.Id, planningStudentResponse.Id);
                Assert.AreEqual(planningStudent.LastName, planningStudentResponse.LastName);
                Assert.AreEqual(planningStudent.ProgramIds.Count(), planningStudentResponse.ProgramIds.Count());
                Assert.AreEqual(planningStudent.AdvisorIds.ElementAt(0), planningStudentResponse.AdvisorIds.ElementAt(0));
            }

            [TestMethod]
            public async Task AdvisorWithViewAssignedAccess_AssignedToStudent_With_PrivacyCode()
            {
                // Arrange
                // In TestStudentRepository, Student 0004012 has advisor 0000111 (Id for advisor from AdvisorUserFactory) set as current advisor
                studentId = "00004012";
                planningStudent = new Domain.Planning.Entities.PlanningStudent(studentId, "brown", 3, new List<string>() { "ENGL.BA", "MATH.BS" }, "S");
                planningStudent.AddAdvisement("0000111", null, null, null);
                planningStudentRepoMock.Setup(repo => repo.GetAsync(It.IsAny<string>())).Returns(Task.FromResult(planningStudent));

                // View Assigned advisees permissions
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.ViewAssignedAdvisees));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });
                currentUserFactory = new AdvisorUserFactory();
                planningStudentService = new PlanningStudentService(adapterRegistry, planningStudentRepo, currentUserFactory, roleRepository, logger, studentRepo, baseConfigurationRepository);

                // Act
                var planningStudentResponse = await planningStudentService.GetPlanningStudentAsync(studentId);

                // Assert - spot-check the response
                Assert.AreEqual(planningStudent.PrivacyStatusCode, planningStudentResponse.PrivacyStatusCode);
            }

            [TestMethod]
            public async Task QueryPlanningStudents_WithPermission()
            {
                List<string> planningStudents = new List<string>() { "0004723", "0011902" };
                var planningStudentsEntity = new List<Domain.Planning.Entities.PlanningStudent>() {
                new Domain.Planning.Entities.PlanningStudent("0004723","last name",null,null),
                new Domain.Planning.Entities.PlanningStudent("0011902","last name",null,null)
                };
                planningStudentRepoMock.Setup(repo => repo.GetAsync(It.IsAny<List<string>>())).Returns(Task.FromResult(planningStudentsEntity.AsEnumerable()));
                currentUserFactory = new FacultyUserFactory();
                planningStudentService = new PlanningStudentService(adapterRegistry, planningStudentRepo, currentUserFactory, roleRepository, logger, studentRepo, baseConfigurationRepository);
                // Set up view assigned advisee permissions on advisor's role--so that advisor cannot access this student
                facultyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(StudentPermissionCodes.ViewStudentInformation));
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Ellucian.Colleague.Domain.Entities.Role>() { facultyRole });
                var result = await planningStudentService.QueryPlanningStudentsAsync(planningStudents);
                Assert.IsTrue(result is List<Ellucian.Colleague.Dtos.Planning.PlanningStudent>);
                Assert.AreEqual(2, result.Count());
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task  QueryPlanningStudents_NoPermission_ThrowsException()
            {
                List<string> planningStudents = new List<string>() { "0004723", "0011902" };
                var planningStudentsEntity = new List<Domain.Planning.Entities.PlanningStudent>() {
                new Domain.Planning.Entities.PlanningStudent("0004723","last name",null,null),
                new Domain.Planning.Entities.PlanningStudent("0011902","last name",null,null)
                };
                planningStudentRepoMock.Setup(repo => repo.GetAsync(It.IsAny<List<string>>())).Returns(Task.FromResult(planningStudentsEntity.AsEnumerable()));
                currentUserFactory = new FacultyUserFactory();
                planningStudentService = new PlanningStudentService(adapterRegistry, planningStudentRepo, currentUserFactory, roleRepository, logger, studentRepo, baseConfigurationRepository);
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { facultyRole });
                var result = await planningStudentService.QueryPlanningStudentsAsync(planningStudents);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task QueryPlanningStudents_EmptyIds()
            {
                List<string> planningStudents = new List<string>();
                var planningStudentsEntity = new List<Domain.Planning.Entities.PlanningStudent>() {
                new Domain.Planning.Entities.PlanningStudent("0004723","last name",null,null),
                new Domain.Planning.Entities.PlanningStudent("0011902","last name",null,null)
                };
                planningStudentRepoMock.Setup(repo => repo.GetAsync(It.IsAny<List<string>>())).Returns(Task.FromResult(planningStudentsEntity.AsEnumerable()));
                currentUserFactory = new FacultyUserFactory();
                planningStudentService = new PlanningStudentService(adapterRegistry, planningStudentRepo, currentUserFactory, roleRepository, logger, studentRepo, baseConfigurationRepository);
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { facultyRole });
                var result = await planningStudentService.QueryPlanningStudentsAsync(planningStudents);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task QueryPlanningStudents_NullIds()
            {
                List<string> planningStudents = null;
                var planningStudentsEntity = new List<Domain.Planning.Entities.PlanningStudent>() {
                new Domain.Planning.Entities.PlanningStudent("0004723","last name",null,null),
                new Domain.Planning.Entities.PlanningStudent("0011902","last name",null,null)
                };
                planningStudentRepoMock.Setup(repo => repo.GetAsync(It.IsAny<List<string>>())).Returns(Task.FromResult(planningStudentsEntity.AsEnumerable()));
                currentUserFactory = new FacultyUserFactory();
                planningStudentService = new PlanningStudentService(adapterRegistry, planningStudentRepo, currentUserFactory, roleRepository, logger, studentRepo, baseConfigurationRepository);
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { facultyRole });
                var result = await planningStudentService.QueryPlanningStudentsAsync(planningStudents);
            }
        }
    }
}
