﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Http.TestUtil;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Collections;
using System;
using System.Linq;
using Ellucian.Colleague.Coordination.Planning.Services;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Domain.Planning.Repositories;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Colleague.Domain.Planning.Tests;
using Ellucian.Colleague.Domain.Student.Entities.Requirements;
using Ellucian.Colleague.Domain.Entities;
using slf4net;
using Ellucian.Web.Adapters;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Domain.Student;
using Ellucian.Colleague.Domain.Student.Entities;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Planning.Tests.Services
{

    [TestClass]
    public class ProgramEvaluationServiceTests
    {

        [TestClass]
        public class ProgramEvaluationServiceTests_Evaluate : CurrentUserSetup
        {
            private IAdapterRegistry adapterRegistry;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IProgramRequirementsRepository programRequirementsRepo;
            private IStudentRepository studentRepo;
            private Mock<IPlanningStudentRepository> planningStudentRepoMock;
            private IPlanningStudentRepository planningStudentRepo;
            private Mock<IStudentProgramRepository> studentProgramRepoMock;
            private IStudentProgramRepository studentProgramRepo;
            private Mock<IAcademicCreditRepository> academicCreditRepoMock;
            private IAcademicCreditRepository academicCreditRepo;
            private IAcademicCreditRepository academicCreditRepoInstance = new TestAcademicCreditRepository();
            private Mock<IDegreePlanRepository> degreePlanRepoMock;
            private IDegreePlanRepository degreePlanRepo;
            private ICourseRepository courseRepo;
            private IRequirementRepository requirementRepo;
            private ITermRepository termRepo;
            private IRuleRepository ruleRepo;
            private IProgramRepository programRepo;
            private ICatalogRepository catalogRepo;
            private IPlanningConfigurationRepository planningConfigRepo;
            private IReferenceDataRepository referenceDataRepo;
            private IRoleRepository roleRepository;
            private Mock<IRoleRepository> roleRepoMock;
            private ProgramEvaluationService programEvaluationService;
            private Dumper dumper;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;
            private IConfigurationRepository baseConfigurationRepository;
            private Mock<IConfigurationRepository> baseConfigurationRepositoryMock;


            private Dictionary<string, List<AcademicCredit>> GetCredits(IEnumerable<string> studentIds)
            {
                var creditDictionary = new Dictionary<string, List<AcademicCredit>>();
                foreach (var id in studentIds)
                {
                    var student = studentRepo.Get(id);
                    var acadCreditIds = student.AcademicCreditIds;

                    var credits = academicCreditRepoInstance.GetAsync(acadCreditIds).Result.ToList();

                    creditDictionary.Add(id, credits);
                }

                return creditDictionary;
            }

            [TestInitialize]
            public void Initialize()
            {
                programRequirementsRepo = new TestProgramRequirementsRepository();
                studentRepo = new TestStudentRepository();
                planningStudentRepoMock = new Mock<IPlanningStudentRepository>();
                planningStudentRepo = planningStudentRepoMock.Object;
                studentProgramRepo = new TestStudentProgramRepository();
                
                
                academicCreditRepoMock = new Mock<IAcademicCreditRepository>();
                academicCreditRepo = academicCreditRepoMock.Object;
                referenceDataRepo = new Mock<IReferenceDataRepository>().Object;
                academicCreditRepoMock.Setup(repo => repo.GetAsync(It.IsAny<string[]>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns((string[] s, bool b1, bool b2) => Task.FromResult(academicCreditRepoInstance.GetAsync(s, b1, b2).Result));
                academicCreditRepoMock.Setup(repo => repo.GetAcademicCreditByStudentIdsAsync(It.IsAny<IEnumerable<string>>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns((IEnumerable<string> s, bool b1, bool b2) =>Task.FromResult( GetCredits(s)));

                degreePlanRepoMock = new Mock<IDegreePlanRepository>();
                studentProgramRepoMock = new Mock<IStudentProgramRepository>();

                degreePlanRepo = new TestDegreePlanRepository();
                courseRepo = new TestCourseRepository();
                requirementRepo = new TestRequirementRepository();
                ruleRepo = new TestRuleRepository2();
                programRepo = new TestProgramRepository();
                catalogRepo = new TestCatalogRepository();
                planningConfigRepo = new TestPlanningConfigurationRepository();
                termRepo = new TestTermRepository();
                dumper = new Dumper();
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                logger = new Mock<ILogger>().Object;
                currentUserFactory = new StudentUserFactory();
                roleRepoMock = new Mock<IRoleRepository>();
                roleRepository = roleRepoMock.Object;
                baseConfigurationRepositoryMock = new Mock<IConfigurationRepository>();
                baseConfigurationRepository = baseConfigurationRepositoryMock.Object;
            }

            [TestMethod]
            public async Task UserIsSelf_ReturnsEvaluations()
            {
                // Arrange
                string studentid = "0000894";
                var programCodes = new List<string>() { "MATH.BS.BLANKTAKEONE", "MATH.BS.TOBEOPTIMIZED2" };

                var student = studentRepo.Get("0000894");
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                // reinitialize the service to effect the advisor as the current user
                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                // Act
                var evaluation = await programEvaluationService.EvaluateAsync(studentid, programCodes);

                // Assert
                Assert.IsTrue(evaluation is IEnumerable<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation>);
                Assert.AreEqual(programCodes.ElementAt(0), evaluation.ElementAt(0).ProgramCode);
                Assert.IsTrue(evaluation.ElementAt(0).RequirementResults.Count() > 0);
                Assert.AreEqual(programCodes.ElementAt(1), evaluation.ElementAt(1).ProgramCode);
                Assert.IsTrue(evaluation.ElementAt(0).RequirementResults.Count() > 0);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task NonExistent_Student_ThrowsException()
            {
                // Arrange
                string studentid = "0000894";
                var programCodes = new List<string>() { "MATH.BS.BLANKTAKEONE", "MATH.BS.TOBEOPTIMIZED2" };

                var student = studentRepo.Get("0000894");
                Domain.Planning.Entities.PlanningStudent planningStudent = null;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                // reinitialize the service to effect the advisor as the current user
                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                // Act
                var evaluation = await programEvaluationService.EvaluateAsync(studentid, programCodes);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task Student_Invalid_DegreePlan_ThrowsException()
            {
                // Arrange
                string studentid = "0000894";
                var programCodes = new List<string>() { "MATH.BS.BLANKTAKEONE", "MATH.BS.TOBEOPTIMIZED2" };

                var student = studentRepo.Get("0000894");
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, 123, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(student.Id)).Returns(Task.FromResult(planningStudent));
                Domain.Planning.Entities.DegreePlan dp = null;
                degreePlanRepoMock.Setup(dpr => dpr.GetAsync(It.IsAny<int>())).ReturnsAsync(dp);
                degreePlanRepo = degreePlanRepoMock.Object;

                // reinitialize the service to effect the advisor as the current user
                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                // Act
                var evaluation = await programEvaluationService.EvaluateAsync(studentid, programCodes);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task Student_Null_StudentProgram_ThrowsException()
            {
                // Arrange
                string studentid = "0000894";
                var programCodes = new List<string>() { "MATH.BS.BLANKTAKEONE", "MATH.BS.TOBEOPTIMIZED2" };

                var student = studentRepo.Get("0000894");
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(student.Id)).Returns(Task.FromResult(planningStudent));
                List<StudentProgram> programs = null;
                studentProgramRepoMock.Setup(spr => spr.GetStudentProgramsByIdsAsync(It.IsAny<IEnumerable<string>>(), It.IsAny<bool>(), It.IsAny<Term>(), It.IsAny<bool>())).ReturnsAsync(programs);
                studentProgramRepo = studentProgramRepoMock.Object;

                // reinitialize the service to effect the advisor as the current user
                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                // Act
                var evaluation = await programEvaluationService.EvaluateAsync(studentid, programCodes);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task Student_Empty_StudentProgram_ThrowsException()
            {
                // Arrange
                string studentid = "0000894";
                var programCodes = new List<string>() { "MATH.BS.BLANKTAKEONE", "MATH.BS.TOBEOPTIMIZED2" };

                var student = studentRepo.Get("0000894");
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(student.Id)).Returns(Task.FromResult(planningStudent));
                List<StudentProgram> programs = new List<StudentProgram>();
                studentProgramRepoMock.Setup(spr => spr.GetStudentProgramsByIdsAsync(It.IsAny<IEnumerable<string>>(), It.IsAny<bool>(), It.IsAny<Term>(), It.IsAny<bool>())).ReturnsAsync(programs);
                studentProgramRepo = studentProgramRepoMock.Object;

                // reinitialize the service to effect the advisor as the current user
                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                // Act
                var evaluation = await programEvaluationService.EvaluateAsync(studentid, programCodes);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task AdvisorWithViewAssignedAccess_NotAssigned_ThrowsException()
            {
                // Arrange
                var studentId = "00004013";
                var student = studentRepo.Get(studentId);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentId)).Returns(Task.FromResult(planningStudent));

                currentUserFactory = new AdvisorUserFactory();
                // Set up view assigned advisee permissions on advisor's role--so that advisor cannot access this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.ViewAssignedAdvisees));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });
                // reinitialize the service to effect the advisor as the current user
                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                // Act
                var evaluation = (await programEvaluationService.EvaluateAsync("00004013", new List<string>() { "ENGL.BA", "MATH.BS.BLANKTAKEONE" }));
            }

            [TestMethod]
            public void AdvisorWithViewAssignedAccess_AssignedToStudent_ReturnsEvaluation()
            {
                // Arrange
                // In TestStudentRepository, Student 0004012 has advisor 0000111 (Id for advisor from AdvisorUserFactory) set as current advisor
                string studentid = "00004012";
                var programCodes = new List<string>() { "MATH.BS.BLANKTAKEONE", "MATH.BS.TOBEOPTIMIZED2" };
                var student = studentRepo.Get(studentid);
                
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                currentUserFactory = new AdvisorUserFactory();
                // View Assigned advisees permissions
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.ViewAssignedAdvisees));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });
                // reinitialize the service to effect the advisor as the current user
                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                // Act
                var evaluation = programEvaluationService.EvaluateAsync(studentid, programCodes).Result;

                // Assert
                Assert.IsTrue(evaluation is IEnumerable<Ellucian.Colleague.Domain.Planning.Entities.ProgramEvaluation>);
                Assert.AreEqual(programCodes.ElementAt(0), evaluation.ElementAt(0).ProgramCode);
                Assert.IsTrue(evaluation.ElementAt(0).RequirementResults.Count() > 0);
                Assert.AreEqual(programCodes.ElementAt(1), evaluation.ElementAt(1).ProgramCode);
                Assert.IsTrue(evaluation.ElementAt(0).RequirementResults.Count() > 0);
            }

            [TestMethod]
            public async Task Evaluate_Credits()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004001"; // has acad cred ids 1,2,and 3 - three credits each, #3 is transfer
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                
                string programid = "MATH.BS";

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var progresult =(await  programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();
                Assert.AreEqual(9, progresult.Credits);
                //dumper.Dump(progresult);
            }


            [TestMethod]
            public async Task Evaluate_InstitutionalCredits()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004001"; // has acad cred ids 1,2,and 3 - three credits each, #3 is transfer
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                string programid = "MATH.BS";

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var progresult = (await programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();
                Assert.AreEqual(6, progresult.InstitutionalCredits);
            }


            [TestMethod]
            public void HawkingComparison()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "1143352"; // Stephen Hawking
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                string programid = "STSS.MATH.BS*2010";

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var pe = programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid }).Result.First();

                dumper.Dump(pe, "Verbosely, he spaketh");

                Assert.AreEqual(12m, pe.InstitutionalCredits);
                Assert.AreEqual(12m, pe.Credits);
                Assert.AreEqual(3, pe.RequirementResults.Count);

            }

            [TestMethod]
            public async Task Evaluate_WithNonCourseCreditsAndPlannedCourses()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004010"; // has a plan, pluse has a few acad creds including one noncourse
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                string programid = "MATH.BS";

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo,  referenceDataRepo, currentUserFactory,  roleRepository, logger, baseConfigurationRepository);

                var progresult = (await programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();

                dumper.Dump(progresult, "verbose");
            }

            [TestMethod]
            public async Task Evaluate_WithGroupLevelCourseRule()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004002"; //planned MATH-100, MATH-200       ENGL-102, ENGL-101
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                string programid = "MATH.BS.WITHGROUPRULE";

                // the test repo will return for this programid a program with a group that requires one course, from rule "MATH100"
                // if the rule evaluates correctly, only the planned course for math 100 will satisfy it.  If it fails to do so,
                // the english 100 will because credit > plannedcourse.

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var progresult = (await programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();

                dumper.Dump(progresult, "verbose");

                var gr = progresult.RequirementResults.First().SubRequirementResults.First().GroupResults.First(gg => gg.Group.Id == "10056");
                var c = gr.GetPlannedApplied().First().GetCourse();
                Assert.AreEqual("MATH", c.SubjectCode);
                Assert.AreEqual("100", c.Number);
            }

            [TestMethod]
            public async Task Evaluate_WithSubrequirementLevelCourseRule()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004002"; //planned MATH-100, MATH-200       ENGL-102, ENGL-101
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                string programid = "MATH.BS.WITHSUBRULE";

                // the test repo will return for this programid a program with a group that requires one course, from rule "MATH100"
                // if the rule evaluates correctly, only the planned course for math 100 will satisfy it.  If it fails to do so,
                // the english 100 will because credit > plannedcourse.

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var progresult =(await  programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();

                dumper.Dump(progresult, "verbose");

                var gr = progresult.RequirementResults.First().SubRequirementResults.First().GroupResults.First(gg => gg.Group.Id == "10056");
                var c = gr.GetPlannedApplied().First().GetCourse();
                Assert.AreEqual("MATH", c.SubjectCode);
                Assert.AreEqual("100", c.Number);
            }

            [TestMethod]
            public async Task Evaluate_WithRequirementLevelCourseRule()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004002"; //planned MATH-100, MATH-200       ENGL-102, ENGL-101
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                string programid = "MATH.BS.WITHREQRULE";

                // the test repo will return for this programid a program with a group that requires one course, from rule "MATH100"
                // if the rule evaluates correctly, only the planned course for math 100 will satisfy it.  If it fails to do so,
                // the english 100 will because credit > plannedcourse.

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var progresult = (await programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();

                dumper.Dump(progresult, "verbose");

                var gr = progresult.RequirementResults.First().SubRequirementResults.First().GroupResults.First(gg => gg.Group.Id == "10056");
                var c = gr.GetPlannedApplied().First().GetCourse();
                Assert.AreEqual("MATH", c.SubjectCode);
                Assert.AreEqual("100", c.Number);
            }

            [TestMethod]
            public async Task Evaluate_WithProgramLevelCourseRule()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004002"; //planned MATH-100, MATH-200       ENGL-102, ENGL-101
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                string programid = "MATH.BS.WITHPROGRULE";

                // the test repo will return for this programid a program with a group that requires one course, from rule "MATH100"
                // if the rule evaluates correctly, only the planned course for math 100 will satisfy it.  If it fails to do so,
                // the english 100 will because credit > plannedcourse.

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var progresult = (await programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();

                dumper.Dump(progresult, "verbose");

                var gr = progresult.RequirementResults.First().SubRequirementResults.First().GroupResults.First(gg => gg.Group.Id == "10056");
                var c = gr.GetPlannedApplied().First().GetCourse();
                Assert.AreEqual("MATH", c.SubjectCode);
                Assert.AreEqual("100", c.Number);
            }

            [TestMethod]
            public async Task Evaluate_WithGroupLevelCreditRule()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004001"; // none                     HIST-100,HIST-200,BIOL-100 (bio 100 has "A" status, which will fail rule NEWSTAT)
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                string programid = "MATH.BS.WITHGROUPCREDRULE";

                // the test repo will return for this programid a program with a group that requires one course, from rule "NEWSTAT"
                // and from subjects "BIOL".  The BIOL-100 credit does not have a new status, so it should not satisfy the requirement

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var progresult = (await programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();

                dumper.Dump(progresult, "verbose");

                var gr = progresult.RequirementResults.First().SubRequirementResults.First().GroupResults.First(gg => gg.Group.Id == "10056");
                Assert.IsFalse(gr.IsSatisfied());
                Assert.AreEqual(Ellucian.Colleague.Domain.Student.Entities.Requirements.Result.RuleFailed, gr.Results.First(cr => cr.GetAcadCredId() == "3").Result);
            }

            [TestMethod]
            public async Task Evaluate_WithSubrequirementLevelCreditRule()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004001"; // none                     HIST-100,HIST-200,BIOL-100 (bio 100 has "A" status, which will fail rule NEWSTAT)
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                string programid = "MATH.BS.WITHSUBCREDRULE";

                // the test repo will return for this programid a program with a group that requires one course, from rule "NEWSTAT"
                // and from subjects "BIOL".  The BIOL-100 credit does not have a new status, so it should not satisfy the requirement

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var progresult = (await programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();

                dumper.Dump(progresult, "verbose");

                var gr = progresult.RequirementResults.First().SubRequirementResults.First().GroupResults.First(gg => gg.Group.Id == "10056");
                Assert.IsFalse(gr.IsSatisfied());
                Assert.AreEqual(Ellucian.Colleague.Domain.Student.Entities.Requirements.Result.RuleFailed, gr.Results.First(cr => cr.GetAcadCredId() == "3").Result);
            }

            [TestMethod]
            public async Task Evaluate_WithRequirementLevelCreditRule()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004001"; // none                     HIST-100,HIST-200,BIOL-100 (bio 100 has "A" status, which will fail rule NEWSTAT)
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                string programid = "MATH.BS.WITHREQCREDRULE";

                // the test repo will return for this programid a program with a group that requires one course, from rule "NEWSTAT"
                // and from subjects "BIOL".  The BIOL-100 credit does not have a new status, so it should not satisfy the requirement

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var progresult = (await programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();

                dumper.Dump(progresult, "verbose");

                var gr = progresult.RequirementResults.First().SubRequirementResults.First().GroupResults.First(gg => gg.Group.Id == "10056");
                Assert.IsFalse(gr.IsSatisfied());
                Assert.AreEqual(Ellucian.Colleague.Domain.Student.Entities.Requirements.Result.RuleFailed, gr.Results.First(cr => cr.GetAcadCredId() == "3").Result);
            }

            [TestMethod]
            public async Task Evaluate_WithProgramLevelCreditRule()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004001"; // none                     HIST-100,HIST-200,BIOL-100 (bio 100 has "A" status, which will fail rule NEWSTAT)
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                string programid = "MATH.BS.WITHPROGCREDRULE";

                // the test repo will return for this programid a program with a group that requires one course, from rule "NEWSTAT"
                // and from subjects "BIOL".  The BIOL-100 credit does not have a new status, so it should not satisfy the requirement

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var progresult = (await programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();

                dumper.Dump(progresult, "verbose");

                var gr = progresult.RequirementResults.First().SubRequirementResults.First().GroupResults.First(gg => gg.Group.Id == "10056");
                Assert.IsFalse(gr.IsSatisfied());
                Assert.AreEqual(Ellucian.Colleague.Domain.Student.Entities.Requirements.Result.RuleFailed, gr.Results.First(cr => cr.GetAcadCredId() == "3").Result);
            }

            [TestMethod]
            public async Task Evaluate_IgnoresWithdrawnCredits()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004011"; // none                     MATH*4003 & MATH*151 - both withdrawn (one with a grade and one without)
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                string programid = "MATH.BS.BLANKTAKEONE";

                // the test repo will return for this programid a program with a group that requires one course, no other requirement modifiers.
                // The student has only one acad cred, for a withdrawn course.  It should not apply.

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var progresult = (await programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();

                dumper.Dump(progresult, "verbose");

                var gr = progresult.RequirementResults.First().SubRequirementResults.First().GroupResults.First(gg => gg.Group.Id == "10056");
                Assert.IsFalse(gr.IsSatisfied());
                Assert.AreEqual(0, gr.Results.Count);  // no results because the evaluator doesn't even pass the withdrawn credit to the group
            }

            [TestMethod]
            public async Task Evaluate_IgnoresCreditsInWrongTranscriptGrouping()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004012"; // none                     MATH-502 - graduate course
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                string programid = "MATH.BS.BLANKTAKEONE";

                // the test repo will return for this programid a program with a group that requires one course, no other requirement modifiers.
                // The student has only one acad cred, for a withdrawn course.  It should not apply.

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var progresult =(await  programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();

                dumper.Dump(progresult, "verbose");

                var gr = progresult.RequirementResults.First().SubRequirementResults.First().GroupResults.First(gg => gg.Group.Id == "10056");
                Assert.IsFalse(gr.IsSatisfied());
                Assert.AreEqual(0, gr.Results.Count);   // no results because the Evaluation service applies the program's credit filter 
                // and doesn't pass the credit to the evaluator
                Assert.AreEqual(0, progresult.AllCredit.Count);
            }


            [TestMethod]
            public void SatisfiedSubrequirementTriggersOptimizerForPartialGroups()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004013";  //no plan, credits ENGL-102
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                string programid = "MATH.BS.TOBEOPTIMIZED";


                // Test3 is a "Take all" which should evaluate before Test32.  The Evaluator should see that 
                // test3 is NOT satisfied and that therefore will evaluate Test32 should eval and be satisfied.  
                // The optimizer should see that Test3 is partially satisfied and hanging out to dry in a satisfied
                // subrequriement and rerun the eval without evaluating Test3.

                // It would have been extra cool to 


                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                      requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var eval = programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid }).Result.First();

                dumper.Dump(eval, "verbose");

                // get the group and Subrequirement results
                GroupResult gr1 = eval.RequirementResults.First().SubRequirementResults.First().GroupResults.First(gr => gr.Group.Code == "Test3");
                GroupResult gr2 = eval.RequirementResults.First().SubRequirementResults.First().GroupResults.First(gr => gr.Group.Code == "Test32");
                SubrequirementResult sr1 = eval.RequirementResults.First().SubRequirementResults.First();

                // check group
                Assert.IsTrue(gr1.CompletionStatus == CompletionStatus.NotStarted);
                Assert.IsTrue(gr2.CompletionStatus == CompletionStatus.Completed);


                // Check Acad Cred results
                Assert.IsTrue(gr1.Results.Where(x => x.Result == Result.Applied).Count() == 0);
                Assert.IsTrue(gr2.Results.Where(x => x.Result == Result.Applied).Count() == 1);

                // Check Subrequirement
                Assert.IsTrue(eval.RequirementResults.First().SubRequirementResults.First().CompletionStatus == CompletionStatus.Completed);


            }

            [TestMethod]
            public void SatisfiedRequirementTriggersOptimizerForPartialSubrequirements()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004013";  //no plan, credits ENGL-102
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                string programid = "MATH.BS.TOBEOPTIMIZED2";


                // Test3 is a "Take all" which should evaluate before Test32.  The Evaluator should see that 
                // test3 is NOT satisfied and that therefore will evaluate Test32 should eval and be satisfied.  
                // The optimizer should see that Test3 is partially satisfied and hanging out to dry in a satisfied
                // subrequriement and rerun the eval without evaluating Test3.

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                      requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var eval = programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid }).Result.First();

                dumper.Dump(eval, "verbose");

                // get the group and Subrequirement results
                GroupResult gr1 = eval.RequirementResults.First().SubRequirementResults.First().GroupResults.First(gr => gr.Group.Code == "Test3");
                GroupResult gr2 = eval.RequirementResults.First().SubRequirementResults.Last().GroupResults.First(gr => gr.Group.Code == "Test32");
                SubrequirementResult sr1 = eval.RequirementResults.First().SubRequirementResults.First();

                // check group
                Assert.IsTrue(gr1.CompletionStatus == CompletionStatus.NotStarted);
                Assert.IsTrue(gr2.CompletionStatus == CompletionStatus.Completed);


                // Check Acad Cred results
                Assert.IsTrue(gr1.Results.Where(x => x.Result == Result.Applied).Count() == 0);
                Assert.IsTrue(gr2.Results.Where(x => x.Result == Result.Applied).Count() == 1);

                // Check Subrequirement
                Assert.IsTrue(eval.RequirementResults.First().SubRequirementResults.Last().CompletionStatus == CompletionStatus.Completed);

                // Check Requirement
                Assert.IsTrue(eval.RequirementResults.First().CompletionStatus == CompletionStatus.Completed);

            }
    
            [TestMethod]
            public async Task Evaluate_AppliesCreditsByTypeThenByDateThenByReverseAcademicCreditId()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004014";
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));
                // Student has academic credits in the following sequence on student record with the following id and course name, type (all graded, but some institutional and some transfer) 
                // and start date (same as term start) and expected applied position
                // 13 MUSC-100 TR 2010SP expected sort pos: 5
                // 16 MATH-362 IN 2010SP expected sort pos: 3
                // 14 MUSC-207 TR 2010SP expected sort pos: 4
                //  8 MATH-100 IN 2009FA expected sort pos: 1
                // 63 MUSC-209 IN 2010SP expected sort pos: 2
                // 11 MATH-150 IN 2009FA expected sort pos: 0
                string programid = "SIMPLE";
                // The SIMPLE program will allow up to 10 courses of any course level to be applied.

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var progresult = (await programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();

                var gr = progresult.RequirementResults.First().SubRequirementResults.First().GroupResults.First();
                Assert.AreEqual(6, gr.Results.Count());
                Assert.AreEqual("11", gr.Results.ElementAt(0).GetAcadCredId()); // MATH-150
                Assert.AreEqual("8", gr.Results.ElementAt(1).GetAcadCredId());  // MATH-100
                Assert.AreEqual("63", gr.Results.ElementAt(2).GetAcadCredId()); // MUSC-209
                Assert.AreEqual("16", gr.Results.ElementAt(3).GetAcadCredId()); // MATH-362
                Assert.AreEqual("14", gr.Results.ElementAt(4).GetAcadCredId()); // MUSC-207
                Assert.AreEqual("13", gr.Results.ElementAt(5).GetAcadCredId()); // MUSC-100
            }

            [TestMethod]
            public async Task Evaluate_ReturnsEvaluationIfStudentHasNoAcademicCredits()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004014";
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));
                // set up academic credit repo response with no academic credits.
                academicCreditRepoMock.Setup(repo => repo.GetAcademicCreditByStudentIdsAsync(It.IsAny<IEnumerable<string>>(), It.IsAny<bool>(), It.IsAny<bool>())).ReturnsAsync((new Dictionary<string, List<AcademicCredit>>()));
                string programid = "SIMPLE";
                // The SIMPLE program will allow up to 10 courses of any course level to be applied.

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var progresult = (await programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();
                Assert.IsNotNull(progresult);
            }

            [TestMethod]
            public async Task Evaluate_ExcludesWithdrawnCreditsWithNoGrade()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });

                string studentid = "00004011"; // none                     2 withdrawn classes MATH*4003 id 56 with grade, MATH-151 id 74 without grade
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));

                string programid = "SIMPLE";

                // the test repo will return for this programid a program with a group that is take any 10 courses.
                // The student has 2 acad cred, one for a withdrawn course with a grade and one for a course with no grade.  Only 1 should be used.

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                var progresult = (await programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();

                dumper.Dump(progresult, "verbose");

                var gr = progresult.RequirementResults.First().SubRequirementResults.First().GroupResults.First(gg => gg.Group.Id == "SIMPLE.ANY");
                Assert.IsFalse(gr.IsSatisfied());       // Neither of the withdrawns are applied.
                Assert.AreEqual(0, gr.Results.Count);   
                Assert.AreEqual(1, progresult.OtherAcademicCredits.Count);    // Only the one withdrawn class is listed in the other count and it is 56.
                var otherCreditId = progresult.OtherAcademicCredits.Where(c => c == "56").FirstOrDefault();
                Assert.AreEqual("56", otherCreditId);
            }
        }

        [TestClass]
        public class ProgramEvaluationServiceTests_GetEvaluationNotices : CurrentUserSetup
        {
            private IAdapterRegistry adapterRegistry;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IProgramRequirementsRepository programRequirementsRepo;
            private IStudentRepository studentRepo;
            private Mock<IPlanningStudentRepository> planningStudentRepoMock;
            private IPlanningStudentRepository planningStudentRepo;
            private IStudentProgramRepository studentProgramRepo;
            private Mock<IStudentProgramRepository> studentProgramRepoMock;
            private IAcademicCreditRepository academicCreditRepo;            
            private IDegreePlanRepository degreePlanRepo;
            private ICourseRepository courseRepo;
            private IRequirementRepository requirementRepo;
            private ITermRepository termRepo;
            private IRuleRepository ruleRepo;
            private IProgramRepository programRepo;
            private ICatalogRepository catalogRepo;
            private IPlanningConfigurationRepository planningConfigRepo;
            private ProgramEvaluationService programEvaluationService;
            private Dumper dumper;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;
            private IRoleRepository roleRepository;
            private Mock<IRoleRepository> roleRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private string studentId;
            private string programCode;
            private IEnumerable<Colleague.Domain.Student.Entities.EvaluationNotice> evaluationNoticeEntities;
            private IConfigurationRepository baseConfigurationRepository;
            private Mock<IConfigurationRepository> baseConfigurationRepositoryMock;


            [TestInitialize]
            public void Initialize()
            {
                programRequirementsRepo = new TestProgramRequirementsRepository();
                planningStudentRepoMock = new Mock<IPlanningStudentRepository>();
                planningStudentRepo = planningStudentRepoMock.Object;
                studentProgramRepoMock = new Mock<IStudentProgramRepository>();
                studentProgramRepo = studentProgramRepoMock.Object;
                studentRepo = new TestStudentRepository();
                academicCreditRepo = new TestAcademicCreditRepository();
                degreePlanRepo = new TestDegreePlanRepository();
                courseRepo = new TestCourseRepository();
                requirementRepo = new TestRequirementRepository();
                ruleRepo = new TestRuleRepository2();
                programRepo = new TestProgramRepository();
                catalogRepo = new TestCatalogRepository();
                planningConfigRepo = null;
                termRepo = new TestTermRepository();
                dumper = new Dumper();
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                logger = new Mock<ILogger>().Object;
                currentUserFactory = new StudentUserFactory();
                roleRepoMock = new Mock<IRoleRepository>();
                roleRepository = roleRepoMock.Object;
                referenceDataRepo = new Mock<IReferenceDataRepository>().Object;
                baseConfigurationRepositoryMock = new Mock<IConfigurationRepository>();
                baseConfigurationRepository = baseConfigurationRepositoryMock.Object;

                studentId = "0000894";
                programCode = "ENGL.BA";

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo,  referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                // Mock adapter
                var evaluationNoticeAdapter = new AutoMapperAdapter<Colleague.Domain.Student.Entities.EvaluationNotice, Colleague.Dtos.Planning.EvaluationNotice>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Colleague.Domain.Student.Entities.EvaluationNotice, Colleague.Dtos.Planning.EvaluationNotice>()).Returns(evaluationNoticeAdapter);

                // Mock object returned from call to GetStudentProgramEvaluationNotices
                evaluationNoticeEntities = new List<Colleague.Domain.Student.Entities.EvaluationNotice>() 
                {
                    new Colleague.Domain.Student.Entities.EvaluationNotice(studentId, programCode, new List<string> {"line1","line2"}, Domain.Student.Entities.EvaluationNoticeType.Program),
                    new Colleague.Domain.Student.Entities.EvaluationNotice(studentId, programCode, new List<string> {"line3","line4"}, Domain.Student.Entities.EvaluationNoticeType.StudentProgram)
                };
                studentProgramRepoMock.Setup(repo => repo.GetStudentProgramEvaluationNoticesAsync(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(evaluationNoticeEntities));
            }

            [TestMethod]
            public async Task Returns_EvaluationNoticeDtos()
            {
                // Act
                var notices =await programEvaluationService.GetEvaluationNoticesAsync("0000894", "ENGL.BA");
                
                // Assert
                Assert.IsTrue(notices is List<Ellucian.Colleague.Dtos.Planning.EvaluationNotice>);
                Assert.AreEqual(2, notices.Count());
                for (int i = 0; i < evaluationNoticeEntities.Count(); i++)
                {
                    Assert.AreEqual(evaluationNoticeEntities.ElementAt(i).ProgramCode, notices.ElementAt(i).ProgramCode);
                    Assert.AreEqual(evaluationNoticeEntities.ElementAt(i).StudentId, notices.ElementAt(i).StudentId);
                    Assert.AreEqual(evaluationNoticeEntities.ElementAt(i).Type.ToString(), notices.ElementAt(i).Type.ToString());
                    for (int j = 0; j < evaluationNoticeEntities.ElementAt(i).Text.Count(); j++)
                    {
                        Assert.AreEqual(evaluationNoticeEntities.ElementAt(i).Text.ElementAt(j), notices.ElementAt(i).Text.ElementAt(j));    
                    }
                }
            }

            [TestMethod]
            public async Task NullNoticesReturned_ReturnsEmptyDto()
            {
                IEnumerable<Ellucian.Colleague.Domain.Student.Entities.EvaluationNotice> nullNotices = null;
                studentProgramRepoMock.Setup(spr => spr.GetStudentProgramEvaluationNoticesAsync(It.IsAny<string>(), It.IsAny<string>())).Returns(Task.FromResult(nullNotices));
                var notices = await programEvaluationService.GetEvaluationNoticesAsync("0000894", "ENGL.BA");
            }

            [TestMethod]
            public async Task AdvisorWithAccess_ReturnsEvaluationNoticeDtos()
            {
                // Arrange
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.ViewAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });
                // reinitialize the service to effect the advisor as the current user
                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);

                // Act
                var notices = await programEvaluationService.GetEvaluationNoticesAsync("00004013", "ENGL.BA");
                
                // Assert
                Assert.IsTrue(notices is List<Ellucian.Colleague.Dtos.Planning.EvaluationNotice>);
                Assert.AreEqual(2, notices.Count());
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task AdvisorWithoutAccess_ThrowsException()
            {
                // Arrange
                currentUserFactory = new AdvisorUserFactory(); 
                // Set up view assigned advisee permissions on advisor's role--so that advisor cannot access this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.ViewAssignedAdvisees));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });
                // reinitialize the service to effect the advisor as the current user
                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, baseConfigurationRepository);
                
                // Act
                var notices = await programEvaluationService.GetEvaluationNoticesAsync("00004013", "ENGL.BA");
            }
        }


        [TestClass]
        public class ProgramEvaluationServiceTests_Evaluate_SortOrder : CurrentUserSetup
        {
            private IAdapterRegistry adapterRegistry;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IProgramRequirementsRepository programRequirementsRepo;
            private IStudentRepository studentRepo;
            private Mock<IPlanningStudentRepository> planningStudentRepoMock;
            private IPlanningStudentRepository planningStudentRepo;
            private Mock<IStudentProgramRepository> studentProgramRepoMock;
            private IStudentProgramRepository studentProgramRepo;
            private Mock<IAcademicCreditRepository> academicCreditRepoMock;
            private IAcademicCreditRepository academicCreditRepo;
            private IAcademicCreditRepository academicCreditRepoInstance = new TestAcademicCreditRepository();
            private Mock<IDegreePlanRepository> degreePlanRepoMock;
            private IDegreePlanRepository degreePlanRepo;
            private ICourseRepository courseRepo;
            private IRequirementRepository requirementRepo;
            private Mock<IRequirementRepository> requirementRepoMock;
            private IRequirementRepository requirementRepoInstance = new TestRequirementRepository();
            private ITermRepository termRepo;
            private IRuleRepository ruleRepo;
            private IProgramRepository programRepo;
            private ICatalogRepository catalogRepo;
            private IPlanningConfigurationRepository planningConfigRepo;
            private IReferenceDataRepository referenceDataRepo;
            private IRoleRepository roleRepository;
            private Mock<IRoleRepository> roleRepoMock;
            private ProgramEvaluationService programEvaluationService;
            private Dumper dumper;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;


            private Dictionary<string, List<AcademicCredit>> GetCredits(IEnumerable<string> studentIds)
            {
                var creditDictionary = new Dictionary<string, List<AcademicCredit>>();
                foreach (var id in studentIds)
                {
                    var student = studentRepo.Get(id);
                    var acadCreditIds = student.AcademicCreditIds;

                    var credits = academicCreditRepoInstance.GetAsync(acadCreditIds).Result.ToList();

                    creditDictionary.Add(id, credits);
                }

                return creditDictionary;
            }

            [TestInitialize]
            public void Initialize()
            {
                programRequirementsRepo = new TestProgramRequirementsRepository();
                studentRepo = new TestStudentRepository();
                planningStudentRepoMock = new Mock<IPlanningStudentRepository>();
                planningStudentRepo = planningStudentRepoMock.Object;
                studentProgramRepo = new TestStudentProgramRepository();


                academicCreditRepoMock = new Mock<IAcademicCreditRepository>();
                academicCreditRepo = academicCreditRepoMock.Object;
                referenceDataRepo = new Mock<IReferenceDataRepository>().Object;
                academicCreditRepoMock.Setup(repo => repo.GetAsync(It.IsAny<string[]>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns((string[] s, bool b1, bool b2) => Task.FromResult(academicCreditRepoInstance.GetAsync(s, b1, b2).Result));
                academicCreditRepoMock.Setup(repo => repo.GetAcademicCreditByStudentIdsAsync(It.IsAny<IEnumerable<string>>(), It.IsAny<bool>(), It.IsAny<bool>())).Returns((IEnumerable<string> s, bool b1, bool b2) => Task.FromResult(GetCredits(s)));

                degreePlanRepoMock = new Mock<IDegreePlanRepository>();
                studentProgramRepoMock = new Mock<IStudentProgramRepository>();

                degreePlanRepo = new TestDegreePlanRepository();
                courseRepo = new TestCourseRepository();
                //requirementRepo = new TestRequirementRepository();
                requirementRepoMock = new Mock<IRequirementRepository>();
                requirementRepo = requirementRepoMock.Object;
                ruleRepo = new TestRuleRepository2();
                programRepo = new TestProgramRepository();
                catalogRepo = new TestCatalogRepository();
                planningConfigRepo = new TestPlanningConfigurationRepository();
                termRepo = new TestTermRepository();
                dumper = new Dumper();
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                logger = new Mock<ILogger>().Object;
                currentUserFactory = new StudentUserFactory();
                roleRepoMock = new Mock<IRoleRepository>();
                roleRepository = roleRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
            }

            [TestMethod]
            public async Task Evaluate_AppliesCreditsByDefaultSortSpec()
            {
                // Set up user as "AllAccessAnyAdvisee" advisor so that evaluation is permitted
                currentUserFactory = new AdvisorUserFactory();
                // Set up view any advisee permissions on advisor's role--so that advisor has access to this student
                advisorRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(PlanningPermissionCodes.AllAccessAnyAdvisee));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Ellucian.Colleague.Domain.Entities.Role>() { advisorRole });
                // Mock the parameters where the modified sort is true.
                requirementRepoMock.Setup(repo => repo.GetDegreeAuditParametersAsync()).Returns(Task.FromResult(new DegreeAuditParameters(ExtraCourses.Apply, true, true)));
                // Build a filterCreditDictionary
                Dictionary<string, List<AcademicCredit>> filteredCreditsDict = new Dictionary<string, List<AcademicCredit>>();
                IEnumerable<string> students = new List<string>() { "00004014" };
                List<AcademicCredit> sortedCredits = GetCredits(students)["00004014"].OrderBy(ac => ac.CourseName).ToList();
                filteredCreditsDict.Add("DEFAULT", sortedCredits);
                academicCreditRepoMock.Setup(repo => repo.GetSortedAcademicCreditsBySortSpecificationIdAsync(It.IsAny<IEnumerable<AcademicCredit>>(), It.IsAny<List<string>>())).Returns(Task.FromResult(filteredCreditsDict));
                string studentid = "00004014";
                var student = studentRepo.Get(studentid);
                var planningStudent = new Domain.Planning.Entities.PlanningStudent(student.Id, student.LastName, student.DegreePlanId, student.ProgramIds);
                planningStudent.Advisements = student.Advisements;
                planningStudentRepoMock.Setup(psr => psr.GetAsync(studentid)).Returns(Task.FromResult(planningStudent));



                // Student has academic credits in the following sequence on student record with the following id and course name, type (all graded, but some institutional and some transfer) 
                // and start date (same as term start) and expected applied position
                // 13 MUSC-100 TR 2010SP expected sort pos: 5
                // 16 MATH-362 IN 2010SP expected sort pos: 3
                // 14 MUSC-207 TR 2010SP expected sort pos: 4
                //  8 MATH-100 IN 2009FA expected sort pos: 1
                // 63 MUSC-209 IN 2010SP expected sort pos: 2
                // 11 MATH-150 IN 2009FA expected sort pos: 0
                string programid = "SIMPLE";
                // The SIMPLE program will allow up to 10 courses of any course level to be applied.

                programEvaluationService = new ProgramEvaluationService(adapterRegistry, programRequirementsRepo, studentRepo, planningStudentRepo, studentProgramRepo,
                                                                        requirementRepo, academicCreditRepo, degreePlanRepo, courseRepo, termRepo, ruleRepo, programRepo, catalogRepo, planningConfigRepo, referenceDataRepo, currentUserFactory, roleRepository, logger, configRepo);

                var progresult = (await programEvaluationService.EvaluateAsync(studentid, new List<string>() { programid })).First();

                var gr = progresult.RequirementResults.First().SubRequirementResults.First().GroupResults.First();
                Assert.AreEqual(6, gr.Results.Count());
                // Item will be sorted in title order because that is what was returned by the sort spec DEFAULT.
                Assert.AreEqual("8", gr.Results.ElementAt(0).GetAcadCredId());  // MATH-100
                Assert.AreEqual("11", gr.Results.ElementAt(1).GetAcadCredId()); // MATH-150
                Assert.AreEqual("16", gr.Results.ElementAt(2).GetAcadCredId()); // MATH-362
                Assert.AreEqual("13", gr.Results.ElementAt(3).GetAcadCredId()); // MUSC-100
                Assert.AreEqual("14", gr.Results.ElementAt(4).GetAcadCredId()); // MUSC-207
                Assert.AreEqual("63", gr.Results.ElementAt(5).GetAcadCredId()); // MUSC-209

            }
        }
    }
}
