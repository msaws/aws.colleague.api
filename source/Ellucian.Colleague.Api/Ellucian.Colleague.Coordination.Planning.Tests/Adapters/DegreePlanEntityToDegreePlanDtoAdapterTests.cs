﻿// Copyright 2012-2013 Ellucian Company L.P. and its affiliates.
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Colleague.Dtos.Planning;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Coordination.Planning.Adapters;
using slf4net;
using Ellucian.Web.Adapters;
using Ellucian.Colleague.Coordination.Base.Adapters;

namespace Ellucian.Colleague.Coordination.Planning.Tests.Adapters
{
    [TestClass]
    public class DegreePlanEntityToDegreePlanDtoAdapterTests
    {
        int planId = 1;
        string personId = "0000001";
        int version = 2;
        Ellucian.Colleague.Domain.Planning.Entities.PlannedCourse plannedCourse1;
        Ellucian.Colleague.Domain.Planning.Entities.PlannedCourse plannedCourse2;
        Ellucian.Colleague.Domain.Planning.Entities.PlannedCourse plannedCourse3;
        Ellucian.Colleague.Domain.Planning.Entities.PlannedCourseWarning warning1;
        Ellucian.Colleague.Domain.Planning.Entities.PlannedCourseWarning warning2;
        Ellucian.Colleague.Domain.Planning.Entities.PlannedCourseWarning warning3;
        Ellucian.Colleague.Domain.Student.Entities.Requisite coreq;
        Ellucian.Colleague.Domain.Student.Entities.Requisite prereq;
        Ellucian.Colleague.Domain.Student.Entities.SectionRequisite sectionreq;
        DegreePlan degreePlanDto;
        DateTime date;

        [TestInitialize]
        public void Initialize()
        {
            var adapterRegistryMock = new Mock<IAdapterRegistry>();
            var loggerMock = new Mock<ILogger>();

            var adapter = new DegreePlanEntityAdapter(adapterRegistryMock.Object, loggerMock.Object);

            var datetimeOffsetAdapter = new DateTimeOffsetToDateTimeAdapter(adapterRegistryMock.Object, loggerMock.Object);
            adapterRegistryMock.Setup(reg => reg.GetAdapter<System.DateTimeOffset, System.DateTime>()).Returns(datetimeOffsetAdapter);            
            
            var degreePlanWarningDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Planning.Entities.PlannedCourseWarning, DegreePlanWarning>(adapterRegistryMock.Object, loggerMock.Object);
            adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.PlannedCourseWarning, DegreePlanWarning>()).Returns(degreePlanWarningDtoAdapter);
            //var degreePlanApprovalDtoAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApproval, DegreePlanApproval>(adapterRegistryMock.Object, loggerMock.Object);
            //adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApproval, DegreePlanApproval>()).Returns(degreePlanApprovalDtoAdapter);
            var degreePlanApprovalEntityAdapter = new DegreePlanApprovalEntityAdapter(adapterRegistryMock.Object, loggerMock.Object);
            adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApproval, DegreePlanApproval>()).Returns(degreePlanApprovalEntityAdapter);

            // Create the entity to convert
            var degreePlanEntity = new Ellucian.Colleague.Domain.Planning.Entities.DegreePlan(planId, personId, version);
            // Add term course with message
            prereq = new Ellucian.Colleague.Domain.Student.Entities.Requisite("PREREQ", true, Domain.Student.Entities.RequisiteCompletionOrder.Previous, false);
            warning1 = new Ellucian.Colleague.Domain.Planning.Entities.PlannedCourseWarning(Ellucian.Colleague.Domain.Planning.Entities.PlannedCourseWarningType.UnmetRequisite) { Requisite = prereq, SectionId = "112" };
            coreq = new Ellucian.Colleague.Domain.Student.Entities.Requisite("999", false);
            warning3 = new Ellucian.Colleague.Domain.Planning.Entities.PlannedCourseWarning(Ellucian.Colleague.Domain.Planning.Entities.PlannedCourseWarningType.UnmetRequisite) { Requisite = coreq, SectionId = "112" };
            plannedCourse1 = new Ellucian.Colleague.Domain.Planning.Entities.PlannedCourse("111", null) { Credits = 3.0m };
            plannedCourse1.AddWarning(warning1);
            plannedCourse1.AddWarning(warning3);
            degreePlanEntity.AddCourse(plannedCourse1, "2012/FA");
            // Add term course and section
            plannedCourse2 = new Ellucian.Colleague.Domain.Planning.Entities.PlannedCourse("222", "10002") { Credits = 4.0m };
            degreePlanEntity.AddCourse(plannedCourse2, "2012/FA");
            sectionreq = new Ellucian.Colleague.Domain.Student.Entities.SectionRequisite("221",true);
            warning2 = new Ellucian.Colleague.Domain.Planning.Entities.PlannedCourseWarning(Ellucian.Colleague.Domain.Planning.Entities.PlannedCourseWarningType.UnmetRequisite) { SectionRequisite = sectionreq };
            // Add nonterm course section with message
            plannedCourse3 = new Ellucian.Colleague.Domain.Planning.Entities.PlannedCourse("333", "10003") { Credits = 2.0m };
            plannedCourse3.AddWarning(warning2);
            degreePlanEntity.AddCourse(plannedCourse3, "");

            // Add approval statuses
            var approvals = new List<Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApproval>();
            date = new DateTime(2012, 10, 1, 10, 15, 00);
            var approval1 = new Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApproval(personId, Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApprovalStatus.Approved, date, "111", "2012/FA");
            approvals.Add(approval1);
            var approval2 = new Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApproval("1234567", Ellucian.Colleague.Domain.Planning.Entities.DegreePlanApprovalStatus.Denied, date.AddDays(1), "222", "2012/FA");
            approvals.Add(approval2);
            degreePlanEntity.Approvals = approvals;

            // Add notes
            var notes = new List<Domain.Planning.Entities.DegreePlanNote>() {
                new Domain.Planning.Entities.DegreePlanNote(1, "0000001", date, "note1"),
                new Domain.Planning.Entities.DegreePlanNote(2, "0000456", date, "note2")
            };
            degreePlanEntity.Notes = notes;

            // Convert to DTO
            degreePlanDto = adapter.MapToType(degreePlanEntity);

        }

        [TestMethod]
        public void DegreePlanDtoAdapter_Id()
        {
            Assert.AreEqual(planId, degreePlanDto.Id);

        }

        [TestMethod]
        public void DegreePlanDtoAdapter_PersonId()
        {
            Assert.AreEqual(personId, degreePlanDto.PersonId);
        }

        [TestMethod]
        public void DegreePlanDtoAdapter_Version()
        {
            Assert.AreEqual(2, degreePlanDto.Version);
        }

        [TestMethod]
        public void DegreePlanDtoAdapter_TermCourses()
        {
            Assert.AreEqual(1, degreePlanDto.Terms.Count());
        }

        [TestMethod]
        public void DegreePlanDtoAdapter_CoursesInATerm()
        {
            var tc = degreePlanDto.Terms.ElementAt(0);
            Assert.AreEqual(2, tc.GetPlannedCourseIds().Count());
        }

        [TestMethod]
        public void DegreePlanDtoAdapter_PlannedCourseProperties()
        {
            var tc = degreePlanDto.Terms.ElementAt(0);
            var pc = tc.PlannedCourses.ElementAt(1);
            Assert.AreEqual("10002", pc.SectionId);
            Assert.AreEqual("222", pc.CourseId);
            Assert.AreEqual(4.0m, pc.Credits);
        }

        [TestMethod]
        public void DegreePlanDtoAdapter_DegreePlanWarnings()
        {
            var tc = degreePlanDto.Terms.ElementAt(0);
            var pc = tc.PlannedCourses.ElementAt(0);
            Assert.AreEqual(2, pc.Warnings.Count());
            var pcWarning1 = pc.Warnings.ElementAt(0);
            Assert.AreEqual(warning1.Requisite.RequirementCode, pcWarning1.RequirementCode);
            Assert.AreEqual(DegreePlanWarningType.PrerequisiteUnsatisfied, pcWarning1.Type);
            var pcWarning2 = pc.Warnings.ElementAt(1);
            Assert.AreEqual(warning3.SectionId, pcWarning2.SectionId);
            Assert.AreEqual(DegreePlanWarningType.CorequisiteOptionalCourse, pcWarning2.Type);
            Assert.AreEqual(warning3.Requisite.CorequisiteCourseId, pcWarning2.CourseId);
        }

        [TestMethod]
        public void DegreePlanDtoAdapter_NonTermPlannedCourse()
        {
            var ntpc = degreePlanDto.NonTermPlannedCourses.ElementAt(0);
            Assert.AreEqual(plannedCourse3.CourseId, ntpc.CourseId);
            Assert.AreEqual(plannedCourse3.SectionId, ntpc.SectionId);
            Assert.AreEqual(plannedCourse3.Credits, ntpc.Credits);
            Assert.AreEqual(DegreePlanWarningType.CorequisiteRequiredSection, ntpc.Warnings.ElementAt(0).Type);
            Assert.AreEqual(plannedCourse3.Warnings.Count(), ntpc.Warnings.Count());
        }

        [TestMethod]
        public void DegreePlanDtoAdapter_Approvals()
        {
            Assert.AreEqual(2, degreePlanDto.Approvals.Count());
            var approval = degreePlanDto.Approvals.Where(a => a.PersonId == personId).First();
            Assert.AreEqual(personId, approval.PersonId);
            Assert.AreEqual(date, approval.Date);
            Assert.AreEqual("2012/FA", approval.TermCode);
        }

        [TestMethod]
        public void DegreePlanDtoAdapter_Notes()
        {
            Assert.AreEqual(2, degreePlanDto.Notes.Count());
            var note = degreePlanDto.Notes.ElementAt(0);
            Assert.AreEqual(1, note.Id);
            Assert.AreEqual("0000001", note.PersonId);
            Assert.AreEqual(date, note.Date);
            Assert.AreEqual("note1", note.Text);
            Assert.AreEqual(PersonType.Student, note.PersonType);
            note = degreePlanDto.Notes.ElementAt(1);
            Assert.AreEqual(2, note.Id);
            Assert.AreEqual("0000456", note.PersonId);
            Assert.AreEqual(date, note.Date);
            Assert.AreEqual("note2", note.Text);
            Assert.AreEqual(PersonType.Advisor, note.PersonType);
        }
    }
}
