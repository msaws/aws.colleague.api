﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Coordination.Student.Adapters;
using slf4net;
using Ellucian.Web.Adapters;
using Ellucian.Colleague.Domain.Student.Entities;

namespace Ellucian.Colleague.Coordination.Student.Adapters
{
    [TestClass]
    public class AcademicCreditEntityToAcademicCredit2DtoAdapterTests
    {
        Ellucian.Colleague.Domain.Student.Entities.AcademicCredit academicCreditEntity;
        AcademicCredit2 academicCreditDto;
        AcademicCreditEntityToAcademicCredit2DtoAdapter adapter;
        IAdapterRegistry adapterRegistry;

        [TestInitialize]
        public void Initialize()
        {
            var adapterRegistryMock = new Mock<IAdapterRegistry>();
            adapterRegistry = adapterRegistryMock.Object;
            var loggerMock = new Mock<ILogger>();

            // Data required to create an academic credit domain entity
            string id = "1";
            string courseId = "111";
            string sectionId = "11111";
            var verifiedGrade = new Ellucian.Colleague.Domain.Student.Entities.Grade("B", "B", "UG");
            var course = new Ellucian.Colleague.Domain.Student.Entities.Course(courseId, "Introduction to Art", null, new List<OfferingDepartment>() { new OfferingDepartment("ART", 100m) }, "ART", "100", "UG", new List<string>() { "Type" }, 3.0m, null, new List<CourseApproval>() { new CourseApproval("A", DateTime.Today, "0000043", "0003315", DateTime.Today) { Status = Ellucian.Colleague.Domain.Student.Entities.CourseStatus.Active } });

            // Create the domain entity to be converted
            academicCreditEntity = new Ellucian.Colleague.Domain.Student.Entities.AcademicCredit(id, course, sectionId);
            academicCreditEntity.AdjustedCredit = .5m;
            academicCreditEntity.CompletedCredit = 1.0m;
            academicCreditEntity.ContinuingEducationUnits = 3;
            academicCreditEntity.GradingType = Ellucian.Colleague.Domain.Student.Entities.GradingType.PassFail;
            academicCreditEntity.CourseName = "ART*100";
            academicCreditEntity.Title = "Title Override";
            academicCreditEntity.Credit = 3.0m;
            academicCreditEntity.GpaCredit = 3.0m;
            academicCreditEntity.GradePoints = 9.0m;
            academicCreditEntity.ContinuingEducationUnits = 2.0m;
            academicCreditEntity.Status = Ellucian.Colleague.Domain.Student.Entities.CreditStatus.Add;
            academicCreditEntity.SectionNumber = "99";
            academicCreditEntity.AdjustedCredit = .9m;
            academicCreditEntity.VerifiedGrade = verifiedGrade;
            academicCreditEntity.IsNonCourse = true;
            academicCreditEntity.AddMidTermGrade(new Domain.Student.Entities.MidTermGrade(1, "1", DateTimeOffset.Now));
            academicCreditEntity.AddMidTermGrade(new Domain.Student.Entities.MidTermGrade(2, "2", DateTimeOffset.Now));

            // Map the domain entity to DTO
            adapter = new AcademicCreditEntityToAcademicCredit2DtoAdapter(adapterRegistry, loggerMock.Object);
            academicCreditDto = adapter.MapToType(academicCreditEntity);
        }


        [TestMethod]
        public void AcademicCreditDtoAdapter_CourseId()
        {
            Assert.AreEqual(academicCreditEntity.Course.Id, academicCreditDto.CourseId);
        }

        [TestMethod]
        public void AcademicCreditDtoAdapter_VerifiedGradeId()
        {
            Assert.AreEqual(academicCreditEntity.VerifiedGrade.Id, academicCreditDto.VerifiedGradeId);
        }

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_AdjustedCredit()
        //{
        //    Assert.AreEqual(academicCreditEntity.AdjustedCredit, academicCreditDto.AdjustedCredit);
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_CompletedCredit()
        //{
        //    Assert.AreEqual(academicCreditEntity.CompletedCredit, academicCreditDto.CompletedCredit);
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_ContinuingEducationUnits()
        //{
        //    Assert.AreEqual(academicCreditEntity.ContinuingEducationUnits, academicCreditDto.ContinuingEducationUnits);
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_CourseName()
        //{
        //    Assert.AreEqual(academicCreditEntity.CourseName, academicCreditDto.CourseName);
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_MidTermGrades()
        //{
        //    Assert.AreEqual(2, academicCreditDto.MidTermGrades.Count());
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_SectionId()
        //{
        //    Assert.AreEqual(academicCreditEntity.SectionId, academicCreditDto.SectionId);
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_SectionNumber()
        //{
        //    Assert.AreEqual(academicCreditEntity.SectionNumber, academicCreditDto.SectionNumber);
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_Title()
        //{
        //    Assert.AreEqual(academicCreditEntity.Title, academicCreditDto.Title);
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_Credit()
        //{
        //    Assert.AreEqual(academicCreditEntity.Credit, academicCreditDto.Credit);
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_GpaCredit()
        //{
        //    Assert.AreEqual(academicCreditEntity.GpaCredit, academicCreditDto.GpaCredit);
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_GradePoints()
        //{
        //    Assert.AreEqual(academicCreditEntity.GradePoints, academicCreditDto.GradePoints);
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_Status()
        //{
        //    Assert.AreEqual(academicCreditEntity.Status.ToString(), academicCreditDto.Status);
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_IsNonCourse()
        //{
        //    Assert.AreEqual(academicCreditEntity.IsNonCourse, academicCreditDto.IsNonCourse);
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_IsCompletedCredit()
        //{
        //    academicCreditEntity.HasVerifiedGrade = true;
        //    academicCreditDto = adapter.MapToType(academicCreditEntity);
        //    Assert.IsTrue(academicCreditEntity.IsCompletedCredit);
        //    Assert.AreEqual(academicCreditEntity.IsCompletedCredit, academicCreditDto.IsCompletedCredit);
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_VerifiedGradeTimeStamp()
        //{
        //    academicCreditEntity.HasVerifiedGrade = true;
        //    academicCreditEntity.VerifiedGradeTimestamp = new DateTime(2014, 06, 12, 08, 50, 22);
        //    academicCreditDto = adapter.MapToType(academicCreditEntity);
        //    Assert.IsTrue(academicCreditEntity.IsCompletedCredit);
        //    Assert.AreEqual(academicCreditEntity.IsCompletedCredit, academicCreditDto.IsCompletedCredit);
        //    Assert.IsNotNull(academicCreditDto.VerifiedGradeTimestamp);
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_GradingType()
        //{
        //    Assert.AreEqual(academicCreditEntity.GradingType.ToString(), academicCreditDto.GradingType.ToString());
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_HasVerifiedGrade()
        //{
        //    Assert.AreEqual(academicCreditEntity.HasVerifiedGrade, academicCreditDto.HasVerifiedGrade);
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_Id()
        //{
        //    Assert.AreEqual(academicCreditEntity.Id, academicCreditDto.Id);
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_ReplacedStatus()
        //{
        //    Assert.AreEqual(academicCreditEntity.ReplacedStatus.ToString(), academicCreditDto.ReplacedStatus.ToString());
        //}

        //[TestMethod]
        //public void AcademicCreditDtoAdapter_TermCode()
        //{
        //    Assert.AreEqual(academicCreditEntity.TermCode, academicCreditDto.TermCode);
        //}
    }
}

