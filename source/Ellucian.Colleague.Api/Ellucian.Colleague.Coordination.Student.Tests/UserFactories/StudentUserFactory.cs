﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Web.Security;

namespace Ellucian.Colleague.Coordination.Student.Tests.UserFactories
{
    /// <summary>
    /// Define a user factory to simulate the user
    /// </summary>
    public abstract class StudentUserFactory
    {
        public class UserFactory : ICurrentUserFactory
        {
            public ICurrentUser CurrentUser
            {
                get
                {
                    return new CurrentUser(new Claims()
                    {
                        PersonId = "0000001",
                        ControlId = "123",
                        Name = "Johnny",
                        SecurityToken = "321",
                        SessionTimeout = 30,
                        UserName = "Student",
                        Roles = new List<string>() { "Student" },
                        SessionFixationId = "abc123"
                    });
                }
            }
        }

        public class HousingAssignmentUser : ICurrentUserFactory
        {
            public ICurrentUser CurrentUser
            {
                get
                {
                    return new CurrentUser(new Claims()
                    {
                        PersonId = "0000004",
                        ControlId = "123",
                        Name = "Johnny",
                        SecurityToken = "321",
                        SessionTimeout = 30,
                        UserName = "StudentHA",
                        Roles = new List<string>() { "VIEW.ROOM.ASSIGNMENT", "UPDATE.ROOM.ASSIGNMENT" },
                        SessionFixationId = "abc123"
                    });
                }
            }
        }

        public class SectionInstructorsUser : ICurrentUserFactory
        {
            public ICurrentUser CurrentUser
            {
                get
                {
                    return new CurrentUser(new Claims()
                    {
                        PersonId = "0000004",
                        ControlId = "123",
                        Name = "Johnny",
                        SecurityToken = "321",
                        SessionTimeout = 30,
                        UserName = "StudentPO",
                        Roles = new List<string>() { "VIEW.SECTION.INSTRUCTORS", "UPDATE.SECTION.INSTRUCTORS", "DELETE.SECTION.INSTRUCTORS" },
                        SessionFixationId = "abc123"
                    });
                }
            }
        }
    }
}
