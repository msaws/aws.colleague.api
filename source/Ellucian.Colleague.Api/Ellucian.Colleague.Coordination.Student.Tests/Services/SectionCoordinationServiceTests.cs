﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Base.Tests;
using Ellucian.Colleague.Domain.Entities;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Student;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.EnumProperties;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Web.Http.Exceptions;
using RegistrationDate = Ellucian.Colleague.Domain.Student.Entities.RegistrationDate;
using Section = Ellucian.Colleague.Domain.Student.Entities.Section;
using Term = Ellucian.Colleague.Domain.Student.Entities.Term;

namespace Ellucian.Colleague.Coordination.Student.Tests.Services
{
    [TestClass]
    public class SectionCoordinationServiceTests
    {
       public abstract class CurrentUserSetup
        {
            protected Role thirdPartyRole = new Role(1, "ThirdPartyCanUpdateGrades");

            public class StudentUserFactory : ICurrentUserFactory
            {
                public ICurrentUser CurrentUser
                {
                    get
                    {
                        return new CurrentUser(new Claims()
                        {
                            ControlId = "123",
                            Name = "Samwise",
                            PersonId = "STU1",
                            SecurityToken = "321",
                            SessionTimeout = 30,
                            UserName = "Samwise",
                            Roles = new List<string>() { },
                            SessionFixationId = "abc123"
                        });
                    }
                }
            }

            // Represents a third party system like ILP
            public class ThirdPartyUserFactory : ICurrentUserFactory
            {
                public ICurrentUser CurrentUser
                {
                    get
                    {
                        return new CurrentUser(new Claims()
                        {
                            ControlId = "123",
                            Name = "ILP",
                            PersonId = "ILP",
                            SecurityToken = "321",
                            SessionTimeout = 30,
                            UserName = "ILP",
                            Roles = new List<string>() { "ThirdPartyCanUpdateGrades",
                                                     "CreateAndUpdateRoomBooking",
                                                     "CreateAndUpdateFacultyBooking"},
                            SessionFixationId = "abc123"
                        });
                    }
                }
            }
        }
        

        [TestClass]
        public class GetSectionRoster : CurrentUserSetup
        {
            private SectionCoordinationService sectionCoordinationService;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepo;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private IStudentReferenceDataRepository studentReferenceDataRepo;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private Mock<ICourseRepository> courseRepoMock;
            private ICourseRepository courseRepo;
            private Mock<ITermRepository> termRepoMock;
            private ITermRepository termRepo;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private IStudentConfigurationRepository studentConfigRepo;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IRoomRepository> roomRepoMock;
            private IRoomRepository roomRepo;
            private Mock<IEventRepository> eventRepoMock;
            private IEventRepository eventRepo;


            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;

            private Domain.Student.Entities.Student student1;
            private Domain.Student.Entities.Student student2;
            private Domain.Student.Entities.Student student3;
            private Domain.Student.Entities.Section section1;
            private Domain.Student.Entities.Section section2;
            private Domain.Student.Entities.Section section3;
            private List<OfferingDepartment> dpts = new List<OfferingDepartment>() { new OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>() { "Whatever" };
            private List<SectionStatusItem> statuses = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddDays(-60)) };

            [TestInitialize]
            public void Initialize()
            {
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepo = sectionRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                roomRepoMock = new Mock<IRoomRepository>();
                roomRepo = roomRepoMock.Object;
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                studentReferenceDataRepo = stuRefDataRepoMock.Object;
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                referenceDataRepo = referenceDataRepoMock.Object;
                courseRepoMock = new Mock<ICourseRepository>();
                courseRepo = courseRepoMock.Object;
                termRepoMock = new Mock<ITermRepository>();
                termRepo = termRepoMock.Object;
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                studentConfigRepo = studentConfigRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
                eventRepoMock = new Mock<IEventRepository>();
                eventRepo = eventRepoMock.Object;


                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;

                // "STU1" is the current user
                currentUserFactory = new CurrentUserSetup.StudentUserFactory();

                logger = new Mock<ILogger>().Object;

                // Mock the student repo response
                student1 = new Domain.Student.Entities.Student("STU1", "Gamgee", null, new List<string>(), new List<string>());
                student1.FirstName = "Samwise";

                student2 = new Domain.Student.Entities.Student("STU2", "Took", null, new List<string>(), new List<string>());
                student2.FirstName = "Peregrin";

                student3 = new Domain.Student.Entities.Student("STU3", "Baggins", null, new List<string>(), new List<string>());
                student3.FirstName = "Frodo"; student3.MiddleName = "Ring-bearer";

                studentRepoMock.Setup(repo => repo.Get("STU1")).Returns(student1);
                studentRepoMock.Setup(repo => repo.Get("STU2")).Returns(student2);
                studentRepoMock.Setup(repo => repo.Get("STU3")).Returns(student3);

                // Mock the section repo response
                section1 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section1.TermId = "2012/FA";
                section1.EndDate = new DateTime(2012, 12, 21); section1.AddActiveStudent("STU1");

                section2 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section2.TermId = "2012/FA"; section2.AddActiveStudent("STU2");

                section3 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section3.TermId = "2011/FA"; section3.AddActiveStudent("STU1"); section3.AddActiveStudent("STU2"); section3.AddActiveStudent("STU3");
                section3.EndDate = new DateTime(2011, 12, 21);

                List<string> sec1List = new List<string>() { "SEC1" };
                List<Domain.Student.Entities.Section> sec1Resp = new List<Domain.Student.Entities.Section>() { section1 };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec1List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sec1Resp));

                List<string> sec2List = new List<string>() { "SEC2" };
                List<Domain.Student.Entities.Section> sec2Resp = new List<Domain.Student.Entities.Section>() { section2 };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec2List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sec2Resp));

                List<string> sec3List = new List<string>() { "SEC3" };
                List<Domain.Student.Entities.Section> sec3Resp = new List<Domain.Student.Entities.Section>() { section3 };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec3List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sec3Resp));


                List<string> sec4List = new List<string>() { "9999999" };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec4List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(new List<Domain.Student.Entities.Section>()));

                var rosterStudentAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.RosterStudent, Ellucian.Colleague.Dtos.Student.RosterStudent>(adapterRegistry, logger);
                adapterRegistryMock.Setup(x => x.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.RosterStudent, Ellucian.Colleague.Dtos.Student.RosterStudent>()).Returns(rosterStudentAdapter);

                Domain.Student.Entities.RosterStudent rs1 = new Domain.Student.Entities.RosterStudent(student1.Id, student1.LastName) { FirstName = student1.FirstName, MiddleName = student1.MiddleName };
                Domain.Student.Entities.RosterStudent rs2 = new Domain.Student.Entities.RosterStudent(student2.Id, student2.LastName) { FirstName = student2.FirstName, MiddleName = student2.MiddleName };
                Domain.Student.Entities.RosterStudent rs3 = new Domain.Student.Entities.RosterStudent(student3.Id, student3.LastName) { FirstName = student3.FirstName, MiddleName = student3.MiddleName };

                studentRepoMock.Setup(repo => repo.GetRosterStudentsAsync(new List<string>() { "STU1" })).Returns(Task.FromResult(new List<Domain.Student.Entities.RosterStudent>() { rs1 }.AsEnumerable()));
                studentRepoMock.Setup(repo => repo.GetRosterStudentsAsync(new List<string>() { "STU1", "STU2", "STU3" })).Returns(Task.FromResult(new List<Domain.Student.Entities.RosterStudent>() { rs1, rs2, rs3 }.AsEnumerable()));

                // Mock the section service
                sectionCoordinationService = new SectionCoordinationService(adapterRegistry, sectionRepo, courseRepo, studentRepo, studentReferenceDataRepo,
                    referenceDataRepo, termRepo, studentConfigRepo, configRepo, personRepo, roomRepo, eventRepo, currentUserFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionRepo = null;
                studentRepo = null;
                roleRepo = null;
                adapterRegistry = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_RosterEmptySectionId()
            {
                var r = await sectionCoordinationService.GetSectionRosterAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_RosterUserNotInSection()
            {
                var r = await sectionCoordinationService.GetSectionRosterAsync("SEC2");
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task SectionService_RosterInvalidSectionId()
            {
                var r = await sectionCoordinationService.GetSectionRosterAsync("9999999");
            }

            [TestMethod]
            public async Task SectionService_RosterOneStudent()
            {
                var res = await sectionCoordinationService.GetSectionRosterAsync("SEC1");
                Assert.AreEqual(1, res.Count());
            }

            [TestMethod]
            public async Task SectionService_RosterMultipleStudents()
            {
                var res = await sectionCoordinationService.GetSectionRosterAsync("SEC3");
                Assert.AreEqual(3, res.Count());
            }

            [TestMethod]
            public async Task SectionService_RosterRosterStudentId()
            {
                var res = await sectionCoordinationService.GetSectionRosterAsync("SEC3");
                Dtos.Student.RosterStudent rStu1 = res.ElementAt(0);
                Assert.AreEqual(student1.Id, rStu1.Id);
                Dtos.Student.RosterStudent rStu2 = res.ElementAt(1);
                Assert.AreEqual(student2.Id, rStu2.Id);
                Dtos.Student.RosterStudent rStu3 = res.ElementAt(2);
                Assert.AreEqual(student3.Id, rStu3.Id);
            }

            [TestMethod]
            public async Task SectionService_RosterRosterStudentFirstName()
            {
                var res = await sectionCoordinationService.GetSectionRosterAsync("SEC3");
                Dtos.Student.RosterStudent rStu1 = res.ElementAt(0);
                Assert.AreEqual(student1.FirstName, rStu1.FirstName);
                Dtos.Student.RosterStudent rStu2 = res.ElementAt(1);
                Assert.AreEqual(student2.FirstName, rStu2.FirstName);
                Dtos.Student.RosterStudent rStu3 = res.ElementAt(2);
                Assert.AreEqual(student3.FirstName, rStu3.FirstName);
            }

            [TestMethod]
            public async Task SectionService_RosterRosterStudentMiddleName()
            {
                var res = await sectionCoordinationService.GetSectionRosterAsync("SEC3");
                Dtos.Student.RosterStudent rStu1 = res.ElementAt(0);
                Assert.AreEqual(student1.MiddleName, rStu1.MiddleName);
                Dtos.Student.RosterStudent rStu2 = res.ElementAt(1);
                Assert.AreEqual(student2.MiddleName, rStu2.MiddleName);
                Dtos.Student.RosterStudent rStu3 = res.ElementAt(2);
                Assert.AreEqual(student3.MiddleName, rStu3.MiddleName);
            }

            [TestMethod]
            public async Task SectionService_RosterRosterStudentLastName()
            {
                var res = await sectionCoordinationService.GetSectionRosterAsync("SEC3");
                Dtos.Student.RosterStudent rStu1 = res.ElementAt(0);
                Assert.AreEqual(student1.LastName, rStu1.LastName);
                Dtos.Student.RosterStudent rStu2 = res.ElementAt(1);
                Assert.AreEqual(student2.LastName, rStu2.LastName);
                Dtos.Student.RosterStudent rStu3 = res.ElementAt(2);
                Assert.AreEqual(student3.LastName, rStu3.LastName);
            }

        }

        [TestClass]
        public class UserCanUpdateGrades : CurrentUserSetup
        {
            private SectionCoordinationService sectionCoordinationService;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepo;
            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IRoomRepository> roomRepoMock;
            private IRoomRepository roomRepo;
            private Mock<IEventRepository> eventRepoMock;
            private IEventRepository eventRepo;


            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private IStudentReferenceDataRepository studentReferenceDataRepo;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private Mock<ICourseRepository> courseRepoMock;
            private ICourseRepository courseRepo;
            private Mock<ITermRepository> termRepoMock;
            private ITermRepository termRepo;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private IStudentConfigurationRepository studentConfigRepo;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;

            private Dtos.Student.SectionGrades3 sectionGrades3Dto;
            private List<Domain.Student.Entities.SectionGradeResponse> sectionGradeResponse;
            private List<Domain.Student.Entities.SectionGradeResponse> sectionGradeResponseNonIlp;

            [TestInitialize]
            public void Initialize()
            {
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepo = sectionRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                logger = new Mock<ILogger>().Object;
                currentUserFactory = new CurrentUserSetup.ThirdPartyUserFactory();
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                roomRepoMock = new Mock<IRoomRepository>();
                roomRepo = roomRepoMock.Object;
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                studentReferenceDataRepo = stuRefDataRepoMock.Object;
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                referenceDataRepo = referenceDataRepoMock.Object;
                courseRepoMock = new Mock<ICourseRepository>();
                courseRepo = courseRepoMock.Object;
                termRepoMock = new Mock<ITermRepository>();
                termRepo = termRepoMock.Object;
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                studentConfigRepo = studentConfigRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
                eventRepoMock = new Mock<IEventRepository>();
                eventRepo = eventRepoMock.Object;

                var gradeAdapter = new Ellucian.Colleague.Coordination.Student.Adapters.SectionGradesAdapter(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Dtos.Student.SectionGrades, Ellucian.Colleague.Domain.Student.Entities.SectionGrades>()).Returns(gradeAdapter);

                var grade2Adapter = new Ellucian.Colleague.Coordination.Student.Adapters.SectionGrades2Adapter(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Dtos.Student.SectionGrades2, Ellucian.Colleague.Domain.Student.Entities.SectionGrades>()).Returns(grade2Adapter);

                var grade3Adapter = new Ellucian.Web.Adapters.AutoMapperAdapter<Ellucian.Colleague.Dtos.Student.SectionGrades3, Ellucian.Colleague.Domain.Student.Entities.SectionGrades>(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Dtos.Student.SectionGrades3, Ellucian.Colleague.Domain.Student.Entities.SectionGrades>()).Returns(grade3Adapter);

                var responseAdapter = new Ellucian.Colleague.Coordination.Student.Adapters.SectionGradeResponseAdapter(adapterRegistry, logger);
                adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.SectionGradeResponse, Ellucian.Colleague.Dtos.Student.SectionGradeResponse>()).Returns(responseAdapter);

                // For testing the v1 and v2 put grades services.
                // Mock repository ImportGradesAsync to return GetSectionGradeResponse if called with any SectionGrades, false for forceNoVerifyFlag, 
                // false for checkForLocksFlag, and ILP for callerType. The v1 and v2 services will always pass those values for the last three arguments.
                sectionRepoMock.Setup(r => r.ImportGradesAsync(It.IsAny<Ellucian.Colleague.Domain.Student.Entities.SectionGrades>(), It.Is<bool>(b => b == false),
                        It.Is<bool>(b => b == false), It.Is<GradesPutCallerTypes>(ct => ct == GradesPutCallerTypes.ILP))).Returns
                        (Task.FromResult<IEnumerable<Domain.Student.Entities.SectionGradeResponse>>(GetSectionGradeResponse()));

                // Instantiate a SectionGrades3 dto with particular values that will invoke a particular ImportGradesAsync repository mock.
                // Set forceNoVerify true here, which will eventually pass true in forceNoVerify to the ImportGradesAsync repository method.
                sectionGrades3Dto = new Dtos.Student.SectionGrades3()
                {
                    SectionId = "Section3",
                    ForceNoVerifyFlag = true,
                    StudentGrades = new List<StudentGrade2>() { new StudentGrade2() { FinalGrade = "C" } }
                };

                // Instantiate a response from repository ImportGradesAsync
                sectionGradeResponse =
                new List<Domain.Student.Entities.SectionGradeResponse>() 
                { 
                    new Domain.Student.Entities.SectionGradeResponse ()
                    {
                        Status = "Failure",
                        Errors = new List<Domain.Student.Entities.SectionGradeResponseError>() { 
                            new Domain.Student.Entities.SectionGradeResponseError() { Message = "Grades3TestError" } }
                    }
                };

                // Instantiate an alternative repository response when called for a non-ILP caller type
                sectionGradeResponseNonIlp =
                new List<Domain.Student.Entities.SectionGradeResponse>() 
                { 
                    new Domain.Student.Entities.SectionGradeResponse ()
                    {
                        Status = "Failure",
                        Errors = new List<Domain.Student.Entities.SectionGradeResponseError>() { 
                            new Domain.Student.Entities.SectionGradeResponseError() { Message = "NonIlpPutGradesTestError" } }
                    }
                };


                // For testing the v3 put grades service and the v1 ilp put grades service which are essentially identical.
                // Mock the repository ImportGradesAsync to return sectionGradeResponse when called with the data in sectionGrades3Dto, true for forceNoVerifyFlag (
                // from the dto), true for checkForLocksFlag (which will always be true from the v3 service), and ILP for callerType (which is always the value
                // from the v3 service or the v1 ilp service.)
                sectionRepoMock.Setup(r =>
                    r.ImportGradesAsync(
                        It.Is<Ellucian.Colleague.Domain.Student.Entities.SectionGrades>(sg => sg.SectionId == sectionGrades3Dto.SectionId &&
                            sg.StudentGrades[0].FinalGrade == sectionGrades3Dto.StudentGrades[0].FinalGrade), It.Is<bool>(b => b == true),
                            It.Is<bool>(b => b == true), It.Is<GradesPutCallerTypes>(ct => ct == GradesPutCallerTypes.ILP))).Returns(
                        Task.FromResult<IEnumerable<Domain.Student.Entities.SectionGradeResponse>>(sectionGradeResponse));

                // For testing the v43 put grades service.
                // Mock the repository ImportGradesAsync to return sectionGradeResponseNonIlp when called with the data in sectionGrades3Dto, true for 
                // forceNoVerifyFlag (from the dto), true for checkForLocksFlag (which will always be true from the v3 service), and Standard for callerType 
                // (which is always the value from the v4 service.)
                sectionRepoMock.Setup(r =>
                    r.ImportGradesAsync(
                        It.Is<Ellucian.Colleague.Domain.Student.Entities.SectionGrades>(sg => sg.SectionId == sectionGrades3Dto.SectionId &&
                            sg.StudentGrades[0].FinalGrade == sectionGrades3Dto.StudentGrades[0].FinalGrade), It.Is<bool>(b => b == true),
                            It.Is<bool>(b => b == true), It.Is<GradesPutCallerTypes>(ct => ct == GradesPutCallerTypes.Standard))).Returns(
                        Task.FromResult<IEnumerable<Domain.Student.Entities.SectionGradeResponse>>(sectionGradeResponseNonIlp));

                // Mock repository GetCachedSectionsAsync to return the section in sectionGrades3Dto with CurrentUser.PersonId as a faculty
                // member. This will be used to test the permission which allows faculty members to grade sections they teach.
                // Only the sectionId and FacultyIds need values for the test.
                var section = new Ellucian.Colleague.Domain.Student.Entities.Section(sectionGrades3Dto.SectionId, "courseId", "number", new DateTime(2016, 1, 1),
                       3M, null, "title", "credittypecode", new List<OfferingDepartment>() { new OfferingDepartment("deptA", 100M) },
                       new List<string>() { "courselevel" }, "acadlevelcode",
                       new List<SectionStatusItem>() { new SectionStatusItem(new SectionStatus(), "code", new DateTime(2016, 1, 1)) });
                section.AddFaculty(currentUserFactory.CurrentUser.PersonId);
                sectionRepoMock.Setup(r =>
                    r.GetCachedSectionsAsync(It.Is<IEnumerable<string>>(s => s.ElementAt(0) == sectionGrades3Dto.SectionId),
                        It.IsAny<bool>())).Returns(
                        Task.FromResult<IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Section>>(
                            new List<Ellucian.Colleague.Domain.Student.Entities.Section>() { section })
                    );

                sectionCoordinationService = new SectionCoordinationService(adapterRegistry, sectionRepo, courseRepo, studentRepo, studentReferenceDataRepo,
                    referenceDataRepo, termRepo, studentConfigRepo, configRepo, personRepo, roomRepo, eventRepo, currentUserFactory, roleRepo, logger);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullInputThrowsException()
            {
                var r = await sectionCoordinationService.ImportGradesAsync(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_ImportGrades3Async_NullInputThrowsException()
            {
                var r = await sectionCoordinationService.ImportGrades3Async(null);
            }


            [TestMethod]
            [ExpectedException(typeof(Ellucian.Web.Security.PermissionsException))]
            public async Task SectionService_ImportGradesThrowsPermissionsException()
            {
                thirdPartyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission("some other permission"));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { thirdPartyRole });
                var r = await sectionCoordinationService.ImportGradesAsync(new Dtos.Student.SectionGrades());
            }

            [TestMethod]
            [ExpectedException(typeof(Ellucian.Web.Security.PermissionsException))]
            public async Task SectionService_ImportGrades2ThrowsPermissionsException()
            {
                thirdPartyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission("some other permission"));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { thirdPartyRole });
                var r = await sectionCoordinationService.ImportGrades2Async(new Dtos.Student.SectionGrades2());
            }

            [TestMethod]
            [ExpectedException(typeof(Ellucian.Web.Security.PermissionsException))]
            public async Task SectionService__ImportGrades3Async_NoPermissionsAndNotTeacherOfSection()
            {
                // No UpdateGrades permission
                thirdPartyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission("some other permission"));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { thirdPartyRole });
                // No section passed, so cannot be teacher of section.
                var r = await sectionCoordinationService.ImportGrades3Async(new Dtos.Student.SectionGrades3());
            }

            [TestMethod]
            public async Task SectionService__ImportGrades3Async_UpdateGradesPermissionsWorks()
            {
                thirdPartyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(SectionPermissionCodes.UpdateGrades));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { thirdPartyRole });
                // No section passed, so cannot be teacher of section.
                // If the permissions fail, an exception will be thrown and this test will fail. If the permissions
                // pass as expected, no exception will be thrown and the test will succeed.
                var r = await sectionCoordinationService.ImportGrades3Async(new Dtos.Student.SectionGrades3());
            }

            [TestMethod]
            public async Task SectionService__ImportGrades3Async_TeacherOfSectionIsGrantedPermission()
            {
                // No UpdateGrades permission
                thirdPartyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission("some other permission"));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { thirdPartyRole });

                // sectionGrades3Dto contains a sectionId that will cause the mocked repository to return a section with 
                // CurrentUser.PersonId in FacultyIds. Therefore the permissions should pass and no exception should be thrown.
                var r = await sectionCoordinationService.ImportGrades3Async(sectionGrades3Dto);
            }

            [TestMethod]
            [ExpectedException(typeof(Ellucian.Web.Security.PermissionsException))]
            public async Task SectionService__ImportIlpGrades1Async_NoPermissionsAndNotTeacherOfSection()
            {
                // No UpdateGrades permission
                thirdPartyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission("some other permission"));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { thirdPartyRole });
                // No section passed, so cannot be teacher of section.
                var r = await sectionCoordinationService.ImportIlpGrades1Async(new Dtos.Student.SectionGrades3());
            }

            [TestMethod]
            public async Task SectionService__ImportIlpGrades1Async_UpdateGradesPermissionsWorks()
            {
                thirdPartyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(SectionPermissionCodes.UpdateGrades));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { thirdPartyRole });
                // No section passed, so cannot be teacher of section.
                // If the permissions fail, an exception will be thrown and this test will fail. If the permissions
                // pass as expected, no exception will be thrown and the test will succeed.
                var r = await sectionCoordinationService.ImportIlpGrades1Async(new Dtos.Student.SectionGrades3());
            }

            [TestMethod]
            public async Task SectionService__ImportIlpGrades1Async_TeacherOfSectionIsGrantedPermission()
            {
                // No UpdateGrades permission
                thirdPartyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission("some other permission"));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { thirdPartyRole });

                // sectionGrades3Dto contains a sectionId that will cause the mocked repository to return a section with 
                // CurrentUser.PersonId in FacultyIds. Therefore the permissions should pass and no exception should be thrown.
                var r = await sectionCoordinationService.ImportIlpGrades1Async(sectionGrades3Dto);
            }

            [TestMethod]
            [ExpectedException(typeof(Ellucian.Web.Security.PermissionsException))]
            public async Task SectionService__ImportGrades4Async_NoPermissionsAndNotTeacherOfSection()
            {
                // No UpdateGrades permission
                thirdPartyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission("some other permission"));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { thirdPartyRole });
                // No section passed, so cannot be teacher of section.
                var r = await sectionCoordinationService.ImportGrades4Async(new Dtos.Student.SectionGrades3());
            }

            [TestMethod]
            public async Task SectionService__ImportGrades4Async_UpdateGradesPermissionsWorks()
            {
                thirdPartyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(SectionPermissionCodes.UpdateGrades));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { thirdPartyRole });
                // No section passed, so cannot be teacher of section.
                // If the permissions fail, an exception will be thrown and this test will fail. If the permissions
                // pass as expected, no exception will be thrown and the test will succeed.
                var r = await sectionCoordinationService.ImportGrades4Async(new Dtos.Student.SectionGrades3());
            }

            [TestMethod]
            public async Task SectionService__ImportIlpGrades4Async_TeacherOfSectionIsGrantedPermission()
            {
                // No UpdateGrades permission
                thirdPartyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission("some other permission"));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { thirdPartyRole });

                // sectionGrades3Dto contains a sectionId that will cause the mocked repository to return a section with 
                // CurrentUser.PersonId in FacultyIds. Therefore the permissions should pass and no exception should be thrown.
                var r = await sectionCoordinationService.ImportGrades4Async(sectionGrades3Dto);
            }


            [TestMethod]
            public void SectionService_ImportGrades()
            {
                thirdPartyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(SectionPermissionCodes.UpdateGrades));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { thirdPartyRole });
                // The mock of ImportGradesAsync in the repository expects "false" in forceNoVerify and false in checkForLocksFlag
                // which the v1 service should pass.
                var response = sectionCoordinationService.ImportGradesAsync(new Dtos.Student.SectionGrades()).Result;

                var expected = GetSectionGradeResponse();
                Assert.AreEqual(expected[0].StudentId, response.First().StudentId);
                Assert.AreEqual(expected[0].Status, response.First().Status);
                Assert.AreEqual(expected[0].Errors.Count(), response.First().Errors.Count());
                Assert.AreEqual(expected[0].Errors[0].Message, response.First().Errors[0].Message);
                Assert.AreEqual(expected[0].Errors[0].Property, response.First().Errors[0].Property);
            }

            [TestMethod]
            public void SectionService_ImportGrades2()
            {
                thirdPartyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(SectionPermissionCodes.UpdateGrades));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { thirdPartyRole });
                // The mock of ImportGradesAsync in the repository expects "false" in forceNoVerify and false in checkForLocksFlag
                // which the v2 service should pass.
                var response = sectionCoordinationService.ImportGrades2Async(new Dtos.Student.SectionGrades2()).Result;

                var expected = GetSectionGradeResponse();
                Assert.AreEqual(expected[0].StudentId, response.First().StudentId);
                Assert.AreEqual(expected[0].Status, response.First().Status);
                Assert.AreEqual(expected[0].Errors.Count(), response.First().Errors.Count());
                Assert.AreEqual(expected[0].Errors[0].Message, response.First().Errors[0].Message);
                Assert.AreEqual(expected[0].Errors[0].Property, response.First().Errors[0].Property);
            }

            [TestMethod]
            public void SectionService_ImportGrades3Async_SuccessfulExecution()
            {
                // Give the permission to update all grades.
                thirdPartyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(SectionPermissionCodes.UpdateGrades));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { thirdPartyRole });

                // This will test the mapping of the dto to the domain entity which is passed to the repository ImportGradesAsync.
                // It will also test that the service calls the repository method with the expected arguments.
                // The mock repository will return sectionGradeResponse when passed sectionGrades3Dto, true in forceNoVerifyFlag which comes from
                // sectionGrades3Dto, true in checkForLocks which ImportIlpGrades1Async should always pass, and ILP as caller type which 
                // ImportGrades3Async always passes.
                IEnumerable<Dtos.Student.SectionGradeResponse> response = sectionCoordinationService.ImportGrades3Async(sectionGrades3Dto).Result;

                Assert.AreEqual(sectionGradeResponse[0].Status, response.First().Status);
                Assert.AreEqual(sectionGradeResponse[0].Errors.Count(), response.First().Errors.Count());
                Assert.AreEqual(sectionGradeResponse[0].Errors[0].Message, response.First().Errors[0].Message);
            }

            [TestMethod]
            public void SectionService_ImportIlpGrades1Async_SuccessfulExecution()
            {
                // ImportIlpGrades1Async has the same logic as ImportGrades3Async, so the test is the same.

                // Give the permission to update all grades.
                thirdPartyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(SectionPermissionCodes.UpdateGrades));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { thirdPartyRole });

                // This will test the mapping of the dto to the domain entity which is passed to the repository ImportGradesAsync.
                // It will also test that the service calls the repository method with the expected arguments.
                // The mock repository will return sectionGradeResponse when passed sectionGrades3Dto, true in forceNoVerifyFlag which comes from
                // sectionGrades3Dto, true in checkForLocks which ImportIlpGrades1Async should always pass, and ILP as caller type which 
                // ImportIlpGrades1Async always passes.
                IEnumerable<Dtos.Student.SectionGradeResponse> response = sectionCoordinationService.ImportIlpGrades1Async(sectionGrades3Dto).Result;

                Assert.AreEqual(sectionGradeResponse[0].Status, response.First().Status);
                Assert.AreEqual(sectionGradeResponse[0].Errors.Count(), response.First().Errors.Count());
                Assert.AreEqual(sectionGradeResponse[0].Errors[0].Message, response.First().Errors[0].Message);
            }

            [TestMethod]
            public void SectionService_ImportGrades4Async_SuccessfulExecution()
            {
                // Give the permission to update all grades.
                thirdPartyRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(SectionPermissionCodes.UpdateGrades));
                roleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { thirdPartyRole });

                // This will test the mapping of the dto to the domain entity which is passed to the repository ImportGradesAsync.
                // It will also test that the service calls the repository method with the expected arguments.
                // The mock repository will return sectionGradeResponseNonIlp when passed sectionGrades3Dto, true in forceNoVerifyFlag which comes from
                // sectionGrades3Dto, true in checkForLocks which ImportIlpGrades1Async should always pass, and Standdard as caller type which 
                // ImportGrades4Async always passes.
                IEnumerable<Dtos.Student.SectionGradeResponse> response = sectionCoordinationService.ImportGrades4Async(sectionGrades3Dto).Result;

                Assert.AreEqual(sectionGradeResponseNonIlp[0].Status, response.First().Status);
                Assert.AreEqual(sectionGradeResponseNonIlp[0].Errors.Count(), response.First().Errors.Count());
                Assert.AreEqual(sectionGradeResponseNonIlp[0].Errors[0].Message, response.First().Errors[0].Message);
            }

            private List<Ellucian.Colleague.Domain.Student.Entities.SectionGradeResponse> GetSectionGradeResponse()
            {
                var response = new Ellucian.Colleague.Domain.Student.Entities.SectionGradeResponse();
                response.StudentId = "101";
                response.Status = "status";

                var error = new Ellucian.Colleague.Domain.Student.Entities.SectionGradeResponseError();
                error.Message = "message";
                error.Property = "property";
                response.Errors.Add(error);

                List<Ellucian.Colleague.Domain.Student.Entities.SectionGradeResponse> list = new List<Domain.Student.Entities.SectionGradeResponse>();
                list.Add(response);
                return list;
            }
        }

        [TestClass]
        public class InstructionalEvents_Get : CurrentUserSetup
        {
            private SectionCoordinationService sectionCoordinationService;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepo;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private IStudentReferenceDataRepository studentReferenceDataRepo;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private Mock<ICourseRepository> courseRepoMock;
            private ICourseRepository courseRepo;
            private Mock<ITermRepository> termRepoMock;
            private ITermRepository termRepo;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private IStudentConfigurationRepository studentConfigRepo;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IRoomRepository> roomRepoMock;
            private IRoomRepository roomRepo;
            private Mock<IEventRepository> eventRepoMock;
            private IEventRepository eventRepo;


            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;

            private Domain.Student.Entities.Section section1;
            private Domain.Student.Entities.Section section2;
            private Domain.Student.Entities.Section section3;
            private Domain.Student.Entities.SectionMeeting meeting1;
            private Domain.Student.Entities.SectionMeeting meeting2;
            private Domain.Student.Entities.SectionMeeting meeting3;
            private Domain.Student.Entities.SectionMeeting meeting4;

            private List<OfferingDepartment> dpts = new List<OfferingDepartment>() { new OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>() { "Whatever" };
            private List<SectionStatusItem> statuses = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddDays(-60)) };

            private string meeting1Guid = Guid.NewGuid().ToString();
            private string instructionalMethodGuid = Guid.NewGuid().ToString();
            private string roomGuid = Guid.NewGuid().ToString();

            [TestInitialize]
            public void Initialize()
            {
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepo = sectionRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                roomRepoMock = new Mock<IRoomRepository>();
                roomRepo = roomRepoMock.Object;
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                studentReferenceDataRepo = stuRefDataRepoMock.Object;
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                referenceDataRepo = referenceDataRepoMock.Object;
                courseRepoMock = new Mock<ICourseRepository>();
                courseRepo = courseRepoMock.Object;
                termRepoMock = new Mock<ITermRepository>();
                termRepo = termRepoMock.Object;
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                studentConfigRepo = studentConfigRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
                eventRepoMock = new Mock<IEventRepository>();
                eventRepo = eventRepoMock.Object;


                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;

                // "STU1" is the current user
                currentUserFactory = new CurrentUserSetup.StudentUserFactory();

                logger = new Mock<ILogger>().Object;

                // Mock the section repo response
                section1 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section1.TermId = "2012/FA";
                section1.EndDate = new DateTime(2012, 12, 21);

                section2 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section2.TermId = "2012/FA";

                section3 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section3.TermId = "2011/FA";
                section3.EndDate = new DateTime(2011, 12, 21);

                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LEC", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting1.Guid = meeting1Guid;
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "DAN*102";
                section1.AddSectionMeeting(meeting1);

                meeting2 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting2.Guid = meeting1Guid;

                meeting3 = new Domain.Student.Entities.SectionMeeting("456", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting3.Guid = meeting1Guid;

                meeting4 = new Domain.Student.Entities.SectionMeeting("789", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting4.Guid = meeting1Guid;

                var instructionalMethods = new List<Domain.Student.Entities.InstructionalMethod>();
                var instructionalMethod = new Domain.Student.Entities.InstructionalMethod(instructionalMethodGuid, "LEC", "Lecture", false);
                instructionalMethods.Add(instructionalMethod);

                var rooms = new List<Domain.Base.Entities.Room>();
                var room = new Domain.Base.Entities.Room(roomGuid, "DAN*102", "Danville Hall Room 102");
                rooms.Add(room);

                var scheduleRepeats = new List<Domain.Base.Entities.ScheduleRepeat>();
                var scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("W", "Weekly", "7", FrequencyType.Weekly);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("D", "Daily", "1", FrequencyType.Daily);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("M", "Monthly", "30", FrequencyType.Monthly);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("Y", "Yearly", "365", FrequencyType.Yearly);
                scheduleRepeats.Add(scheduleRepeat);

                List<string> sec1List = new List<string>() { "SEC1" };
                List<Domain.Student.Entities.Section> sec1Resp = new List<Domain.Student.Entities.Section>() { section1 };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec1List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sec1Resp));

                List<string> sec2List = new List<string>() { "SEC2" };
                List<Domain.Student.Entities.Section> sec2Resp = new List<Domain.Student.Entities.Section>() { section2 };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec2List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sec2Resp));

                List<string> sec3List = new List<string>() { "SEC3" };
                List<Domain.Student.Entities.Section> sec3Resp = new List<Domain.Student.Entities.Section>() { section3 };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec3List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sec3Resp));

                List<string> sec4List = new List<string>() { "9999999" };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec4List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(new List<Domain.Student.Entities.Section>()));

                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting1.Guid)).ReturnsAsync(meeting1);
                stuRefDataRepoMock.Setup(repo => repo.GetInstructionalMethodsAsync()).ReturnsAsync(instructionalMethods);
                roomRepoMock.Setup(repo => repo.RoomsAsync()).ReturnsAsync(rooms);
                referenceDataRepoMock.Setup(repo => repo.ScheduleRepeats).Returns(scheduleRepeats);

                // Mock the section service
                sectionCoordinationService = new SectionCoordinationService(adapterRegistry, sectionRepo, courseRepo, studentRepo, studentReferenceDataRepo,
                    referenceDataRepo, termRepo, studentConfigRepo, configRepo, personRepo, roomRepo, eventRepo, currentUserFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionRepo = null;
                studentRepo = null;
                roleRepo = null;
                adapterRegistry = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_MeetingEmptyId_V3()
            {
                var r = await sectionCoordinationService.GetInstructionalEventAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_MeetingEmptyId_V4()
            {
                var r = await sectionCoordinationService.GetInstructionalEvent2Async("");
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionService_MeetingInvalidSectionId_V3()
            {
                var r = await sectionCoordinationService.GetInstructionalEventAsync("9999999");
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionService_MeetingInvalidSectionId_V4()
            {
                var r = await sectionCoordinationService.GetInstructionalEvent2Async("9999999");
            }

            [TestMethod]
            public async Task SectionService_GetByGuid_Weekly_V3()
            {
                var res = await sectionCoordinationService.GetInstructionalEventAsync(meeting1.Guid);
                Assert.AreEqual(meeting1.Guid, res.Guid);
                var classroom = res.Locations.ElementAt(0) as Dtos.InstructionalRoom;
                Assert.AreEqual(roomGuid, classroom.Guid);
                Assert.AreEqual(instructionalMethodGuid, res.InstructionalMethod.Guid);
            }

            [TestMethod]
            public async Task SectionService_GetByGuid_Weekly_V4()
            {
                var res = await sectionCoordinationService.GetInstructionalEvent2Async(meeting1.Guid);
                Assert.AreEqual(meeting1.Guid, res.Id);
                var classroom = res.Locations.ElementAt(0).Locations as Dtos.InstructionalRoom2;
                Assert.AreEqual(roomGuid, classroom.Room.Id);
                Assert.AreEqual(instructionalMethodGuid, res.InstructionalMethod.Id);
            }

            [TestMethod]
            public async Task SectionService_GetByGuid_Daily_V3()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting2.Guid)).ReturnsAsync(meeting2);
                var res = await sectionCoordinationService.GetInstructionalEventAsync(meeting2.Guid);
                Assert.AreEqual(meeting2.Guid, res.Guid);
            }

            [TestMethod]
            public async Task SectionService_GetByGuid_Daily_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting2.Guid)).ReturnsAsync(meeting2);
                var res = await sectionCoordinationService.GetInstructionalEvent2Async(meeting2.Guid);
                Assert.AreEqual(meeting2.Guid, res.Id);
            }

            [TestMethod]
            public async Task SectionService_GetByGuid_Monthly_V3()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting3.Guid)).ReturnsAsync(meeting3);
                var res = await sectionCoordinationService.GetInstructionalEventAsync(meeting3.Guid);
                Assert.AreEqual(meeting2.Guid, res.Guid);
            }

            [TestMethod]
            public async Task SectionService_GetByGuid_Monthly_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting3.Guid)).ReturnsAsync(meeting3);
                var res = await sectionCoordinationService.GetInstructionalEvent2Async(meeting3.Guid);
                Assert.AreEqual(meeting2.Guid, res.Id);
            }

            [TestMethod]
            public async Task SectionService_GetByGuid_Yearly_V3()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting4.Guid)).ReturnsAsync(meeting4);
                var res = await sectionCoordinationService.GetInstructionalEventAsync(meeting4.Guid);
                Assert.AreEqual(meeting2.Guid, res.Guid);
            }

            [TestMethod]
            public async Task SectionService_GetByGuid_Yearly_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting4.Guid)).ReturnsAsync(meeting4);
                var res = await sectionCoordinationService.GetInstructionalEvent2Async(meeting4.Guid);
                Assert.AreEqual(meeting2.Guid, res.Id);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_GetByGuid_Invalid_Freq_V3()
            {
                meeting4 = new Domain.Student.Entities.SectionMeeting("789", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "X");
                meeting4.Guid = Guid.NewGuid().ToString();
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting4.Guid)).ReturnsAsync(meeting4);
                var res = await sectionCoordinationService.GetInstructionalEventAsync(meeting4.Guid);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_GetByGuid_Invalid_Freq_V4()
            {
                meeting4 = new Domain.Student.Entities.SectionMeeting("789", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "X");
                meeting4.Guid = Guid.NewGuid().ToString();
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting4.Guid)).ReturnsAsync(meeting4);
                var res = await sectionCoordinationService.GetInstructionalEvent2Async(meeting4.Guid);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_RoomBookingPermissionsException_V3()
            {
                var instructionalEvent = new Dtos.InstructionalEvent();
                var room = new Dtos.InstructionalRoom() { Guid = Guid.NewGuid().ToString(), LocationType = Dtos.InstructionalLocationType.InstructionalRoom };
                instructionalEvent.Locations.Add(room);
                var r = await sectionCoordinationService.CreateInstructionalEventAsync(instructionalEvent);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_RoomBookingPermissionsException_V4()
            {
                var instructionalEvent = new Dtos.InstructionalEvent2();
                var room = new Dtos.InstructionalRoom2() { Room = new Dtos.GuidObject2() { Id = Guid.NewGuid().ToString() }, LocationType = Dtos.InstructionalLocationType.InstructionalRoom };
                var location = new Dtos.Location() { Locations = room };
                instructionalEvent.Locations = new List<Dtos.Location>();
                instructionalEvent.Locations.Add(location);
                var r = await sectionCoordinationService.CreateInstructionalEvent2Async(instructionalEvent);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_FacultyBookingPermissionsException_V3()
            {
                var instructionalEvent = new Dtos.InstructionalEvent();
                var faculty = new Dtos.InstructionalEventInstructor() { Instructor = new Dtos.GuidObject() { Guid = Guid.NewGuid().ToString() } };
                instructionalEvent.Instructors.Add(faculty);
                var r = await sectionCoordinationService.CreateInstructionalEventAsync(instructionalEvent);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_FacultyBookingPermissionsException_V4()
            {
                var instructionalEvent = new Dtos.InstructionalEvent2();
                var faculty = new Dtos.InstructionalEventInstructor2() { Instructor = new Dtos.GuidObject2() { Id = Guid.NewGuid().ToString() } };
                instructionalEvent.Instructors = new List<Dtos.InstructionalEventInstructor2>();
                instructionalEvent.Instructors.Add(faculty);
                var r = await sectionCoordinationService.CreateInstructionalEvent2Async(instructionalEvent);
            }
        }

        [TestClass]
        public class InstructionalEvents_Put : CurrentUserSetup
        {
            private SectionCoordinationService sectionCoordinationService;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepo;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private IStudentReferenceDataRepository studentReferenceDataRepo;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private Mock<ICourseRepository> courseRepoMock;
            private ICourseRepository courseRepo;
            private Mock<ITermRepository> termRepoMock;
            private ITermRepository termRepo;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private IStudentConfigurationRepository studentConfigRepo;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IRoomRepository> roomRepoMock;
            private IRoomRepository roomRepo;
            private Mock<IEventRepository> eventRepoMock;
            private IEventRepository eventRepo;
            // private Mock<IAcademicPeriodRepository> academicPeriodRepoMock;

            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;

            private Domain.Student.Entities.Section section1;
            private Domain.Student.Entities.Section section2;
            private Domain.Student.Entities.Section section3;
            private Domain.Student.Entities.SectionMeeting meeting1;
            private Domain.Student.Entities.SectionMeeting meeting2;
            private Domain.Student.Entities.SectionMeeting meeting3;
            private Domain.Student.Entities.SectionMeeting meeting4;

            private List<OfferingDepartment> dpts = new List<OfferingDepartment>() { new OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>() { "Whatever" };
            private List<SectionStatusItem> statuses = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddDays(-60)) };

            private string meeting1Guid = Guid.NewGuid().ToString();
            private string instructionalMethodGuid = Guid.NewGuid().ToString();
            private string roomGuid = Guid.NewGuid().ToString();

            [TestInitialize]
            public void Initialize()
            {
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepo = sectionRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                roomRepoMock = new Mock<IRoomRepository>();
                roomRepo = roomRepoMock.Object;
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                studentReferenceDataRepo = stuRefDataRepoMock.Object;
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                referenceDataRepo = referenceDataRepoMock.Object;
                courseRepoMock = new Mock<ICourseRepository>();
                courseRepo = courseRepoMock.Object;
                termRepoMock = new Mock<ITermRepository>();
                termRepo = termRepoMock.Object;
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                studentConfigRepo = studentConfigRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
                eventRepoMock = new Mock<IEventRepository>();
                eventRepo = eventRepoMock.Object;


                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;

                // Set Current User
                currentUserFactory = new CurrentUserSetup.ThirdPartyUserFactory();

                logger = new Mock<ILogger>().Object;

                // Mock the section repo response
                section1 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section1.TermId = "2012/FA";
                section1.EndDate = new DateTime(2012, 12, 21);
                section1.Guid = Guid.NewGuid().ToString();

                section2 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section2.TermId = "2012/FA";
                section2.Guid = Guid.NewGuid().ToString();

                section3 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section3.TermId = "2011/FA";
                section3.EndDate = new DateTime(2011, 12, 21);
                section3.Guid = Guid.NewGuid().ToString();

                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LEC", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting1.Guid = Guid.NewGuid().ToString();
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "DAN*102";
                section1.AddSectionMeeting(meeting1);

                meeting2 = new Domain.Student.Entities.SectionMeeting("456", "SEC2", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting2.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting2.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting2.Guid = Guid.NewGuid().ToString();
                section2.AddSectionMeeting(meeting2);

                meeting3 = new Domain.Student.Entities.SectionMeeting("789", "SEC3", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting3.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting3.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting3.Guid = Guid.NewGuid().ToString();
                section3.AddSectionMeeting(meeting3);

                meeting4 = new Domain.Student.Entities.SectionMeeting("012", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting4.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting4.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting4.Guid = Guid.NewGuid().ToString();
                section1.AddSectionMeeting(meeting4);

                var instructionalMethods = new List<Domain.Student.Entities.InstructionalMethod>();
                var instructionalMethod = new Domain.Student.Entities.InstructionalMethod(instructionalMethodGuid, "LEC", "Lecture", false);
                instructionalMethods.Add(instructionalMethod);

                var rooms = new List<Domain.Base.Entities.Room>();
                var room = new Domain.Base.Entities.Room(roomGuid, "DAN*102", "Danville Hall Room 102");
                rooms.Add(room);

                var scheduleRepeats = new List<Domain.Base.Entities.ScheduleRepeat>();
                var scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("W", "Weekly", "7", FrequencyType.Weekly);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("D", "Daily", "1", FrequencyType.Daily);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("M", "Monthly", "30", FrequencyType.Monthly);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("Y", "Yearly", "365", FrequencyType.Yearly);
                scheduleRepeats.Add(scheduleRepeat);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).ReturnsAsync(section1);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section2.Guid)).ReturnsAsync(section2);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section3.Guid)).ReturnsAsync(section3);
                sectionRepoMock.Setup(repo => repo.PutSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting1.Guid)).ReturnsAsync(meeting1);
                sectionRepoMock.Setup(repo => repo.PutSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting2.Guid)).ReturnsAsync(meeting2);
                sectionRepoMock.Setup(repo => repo.PutSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting3.Guid)).ReturnsAsync(meeting3);
                sectionRepoMock.Setup(repo => repo.PutSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting4.Guid)).ReturnsAsync(meeting4);

                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting1.Guid)).ReturnsAsync(meeting1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingIdFromGuidAsync(meeting1.Guid)).ReturnsAsync(meeting1.Id);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting2.Guid)).ReturnsAsync(meeting2);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingIdFromGuidAsync(meeting2.Guid)).ReturnsAsync(meeting2.Id);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting3.Guid)).ReturnsAsync(meeting3);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingIdFromGuidAsync(meeting3.Guid)).ReturnsAsync(meeting3.Id);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting4.Guid)).ReturnsAsync(meeting4);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingIdFromGuidAsync(meeting4.Guid)).ReturnsAsync(meeting4.Id);
                stuRefDataRepoMock.Setup(repo => repo.GetInstructionalMethodsAsync()).ReturnsAsync(instructionalMethods);
                roomRepoMock.Setup(repo => repo.RoomsAsync()).ReturnsAsync(rooms);
                referenceDataRepoMock.Setup(repo => repo.ScheduleRepeats).Returns(scheduleRepeats);

                // Configuration defaults and campus calendar information.
                configRepoMock.Setup(repo => repo.GetDefaultsConfiguration()).Returns(new DefaultsConfiguration() { CampusCalendarId = "MAIN" });
                studentConfigRepoMock.Setup(repo => repo.GetCurriculumConfigurationAsync()).ReturnsAsync(new CurriculumConfiguration() { SectionActiveStatusCode = "A", SectionInactiveStatusCode = "I", DefaultInstructionalMethodCode = "INT" });
                eventRepoMock.Setup(repo => repo.GetCalendar("MAIN")).Returns(new CampusCalendar("MAIN", "Default Calendar", new DateTimeOffset(), new DateTimeOffset()));

                // Mock the section service
                sectionCoordinationService = new SectionCoordinationService(adapterRegistry, sectionRepo, courseRepo, studentRepo, studentReferenceDataRepo,
                    referenceDataRepo, termRepo, studentConfigRepo, configRepo, personRepo, roomRepo, eventRepo, currentUserFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionRepo = null;
                studentRepo = null;
                roleRepo = null;
                adapterRegistry = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(IntegrationApiException))]
            public async Task SectionService_Null_Object_V3()
            {
                var r = await sectionCoordinationService.UpdateInstructionalEventAsync(new Dtos.InstructionalEvent());
            }

            [TestMethod]
            [ExpectedException(typeof(IntegrationApiException))]
            public async Task SectionService_Null_Object_V4()
            {
                var r = await sectionCoordinationService.UpdateInstructionalEvent2Async(new Dtos.InstructionalEvent2());
            }

            [TestMethod]
            [ExpectedException(typeof(IntegrationApiException))]
            public async Task SectionService_InvalidGuid_V3()
            {
                var instrEvent = new Dtos.InstructionalEvent() { Guid = "999999999999999" };
                var r = await sectionCoordinationService.UpdateInstructionalEventAsync(instrEvent);
            }

            [TestMethod]
            [ExpectedException(typeof(IntegrationApiException))]
            public async Task SectionService_InvalidId_V4()
            {
                var instrEvent = new Dtos.InstructionalEvent2() { Id = "999999999999999" };
                var r = await sectionCoordinationService.UpdateInstructionalEvent2Async(instrEvent);
            }

            [TestMethod]
            public async Task SectionService_Daily_V3()
            {
                var instrEvent = new Dtos.InstructionalEvent()
                {
                    Guid = meeting1.Guid,
                    Section = new Dtos.GuidObject() { Guid = section1.Guid },
                    InstructionalMethod = new Dtos.GuidObject() { Guid = instructionalMethodGuid },
                    Workload = 10.5m,
                    Recurrence = new Dtos.Recurrence() { Interval = 1, Frequency = Dtos.FrequencyType.Daily, Days = new List<DayOfWeek>() { DayOfWeek.Monday } },
                    StartDate = new DateTime(2012, 1, 1),
                    EndDate = new DateTime(2012, 12, 21),
                    StartTime = new DateTimeOffset(new DateTime(2012, 1, 1, 8, 30, 0)),
                    EndTime = new DateTimeOffset(new DateTime(2012, 12, 21, 9, 30, 0))
                };
                var r = await sectionCoordinationService.UpdateInstructionalEventAsync(instrEvent);
                Assert.AreEqual(r.Guid, meeting1.Guid);
                Assert.AreEqual(r.InstructionalMethod.Guid, instructionalMethodGuid);
                Assert.AreEqual(r.Recurrence.Frequency.ToString(), FrequencyType.Daily.ToString());
                Assert.AreEqual(r.StartDate, meeting1.StartDate);
                Assert.AreEqual(r.EndDate, meeting1.EndDate);
                Assert.AreEqual(r.StartTime, meeting1.StartTime);
                Assert.AreEqual(r.EndTime, meeting1.EndTime);
            }

            [TestMethod]
            public async Task SectionService_Daily_V4()
            {
                var timePeriod = new Dtos.RepeatTimePeriod2
                {
                    StartOn = new DateTime(2012, 1, 1),
                    EndOn = new DateTime(2012, 12, 21)
                };
                var repeatRuleEnds = new Dtos.RepeatRuleEnds { Date = new DateTime(2012, 12, 21) };
                var repeatRule = new Dtos.RepeatRuleDaily { Type = Dtos.FrequencyType2.Daily, Ends = repeatRuleEnds, Interval = 1 };
                var instrEvent = new Dtos.InstructionalEvent2()
                {
                    Id = meeting1.Guid,
                    Section = new Dtos.GuidObject2() { Id = section1.Guid },
                    InstructionalMethod = new Dtos.GuidObject2() { Id = instructionalMethodGuid },
                    Workload = 10.5m,
                    Recurrence = new Dtos.Recurrence3() { RepeatRule = repeatRule, TimePeriod = timePeriod }
                };
                var r = await sectionCoordinationService.UpdateInstructionalEvent2Async(instrEvent);
                Assert.AreEqual(r.Id, meeting1.Guid);
                Assert.AreEqual(r.InstructionalMethod.Id, instructionalMethodGuid);
                Assert.AreEqual(r.Recurrence.RepeatRule.Type.ToString(), FrequencyType.Daily.ToString());
                Assert.AreEqual(r.Recurrence.TimePeriod.StartOn.Date, timePeriod.StartOn.Date);
                Assert.AreEqual(r.Recurrence.TimePeriod.EndOn.Date, timePeriod.EndOn.Date);
                Assert.AreEqual(r.Recurrence.TimePeriod.StartOn.ToLocalTime().TimeOfDay.ToString(), "08:30:00");
                Assert.AreEqual(r.Recurrence.TimePeriod.EndOn.ToLocalTime().TimeOfDay.ToString(), "09:30:00");
            }

            [TestMethod]
            public async Task SectionService_Weekly_V3()
            {
                var instrEvent = new Dtos.InstructionalEvent()
                {
                    Guid = meeting2.Guid,
                    Section = new Dtos.GuidObject() { Guid = section2.Guid },
                    InstructionalMethod = new Dtos.GuidObject() { Guid = instructionalMethodGuid },
                    Workload = 10.5m,
                    Recurrence = new Dtos.Recurrence() { Interval = 1, Frequency = Dtos.FrequencyType.Weekly, Days = new List<DayOfWeek>() { DayOfWeek.Monday } },
                    StartDate = new DateTime(2012, 1, 1),
                    EndDate = new DateTime(2012, 12, 21),
                    StartTime = new DateTimeOffset(new DateTime(2012, 1, 1, 8, 30, 0)),
                    EndTime = new DateTimeOffset(new DateTime(2012, 12, 21, 9, 30, 0))
                };
                var r = await sectionCoordinationService.UpdateInstructionalEventAsync(instrEvent);
                Assert.AreEqual(r.Guid, meeting2.Guid);
                Assert.AreEqual(r.InstructionalMethod.Guid, instructionalMethodGuid);
                Assert.AreEqual(r.Recurrence.Frequency.ToString(), FrequencyType.Weekly.ToString());
                Assert.AreEqual(r.StartDate, meeting2.StartDate);
                Assert.AreEqual(r.EndDate, meeting2.EndDate);
                Assert.AreEqual(r.StartTime, meeting2.StartTime);
                Assert.AreEqual(r.EndTime, meeting2.EndTime);
            }

            [TestMethod]
            public async Task SectionService_Weekly_V4()
            {
                var timePeriod = new Dtos.RepeatTimePeriod2
                {
                    StartOn = new DateTime(2012, 1, 1, 8, 30, 0),
                    EndOn = new DateTime(2012, 12, 21, 9, 30, 0)
                };
                var repeatRuleEnds = new Dtos.RepeatRuleEnds { Date = new DateTime(2012, 12, 21) };
                var repeatRule = new Dtos.RepeatRuleWeekly { Ends = new Dtos.RepeatRuleEnds() { Date = new DateTime(2012, 12, 21) }, DayOfWeek = new List<HedmDayOfWeek> { HedmDayOfWeek.Monday } };
                var instrEvent = new Dtos.InstructionalEvent2()
                {
                    Id = meeting2.Guid,
                    Section = new Dtos.GuidObject2() { Id = section2.Guid },
                    InstructionalMethod = new Dtos.GuidObject2() { Id = instructionalMethodGuid },
                    Workload = 10.5m,
                    Recurrence = new Dtos.Recurrence3() { RepeatRule = repeatRule, TimePeriod = timePeriod }
                };
                var r = await sectionCoordinationService.UpdateInstructionalEvent2Async(instrEvent);
                Assert.AreEqual(r.Id, meeting2.Guid);
                Assert.AreEqual(r.InstructionalMethod.Id, instructionalMethodGuid);
                Assert.AreEqual(r.Recurrence.RepeatRule.Type.ToString(), FrequencyType.Weekly.ToString());
                Assert.AreEqual(r.Recurrence.TimePeriod.StartOn.Date, timePeriod.StartOn.Date);
                Assert.AreEqual(r.Recurrence.TimePeriod.EndOn.Date, timePeriod.EndOn.Date);
                Assert.AreEqual(r.Recurrence.TimePeriod.StartOn.ToLocalTime().TimeOfDay.ToString(), "08:30:00");
                Assert.AreEqual(r.Recurrence.TimePeriod.EndOn.ToLocalTime().TimeOfDay.ToString(), "09:30:00");
            }

            [TestMethod]
            public async Task SectionService_Monthly_V3()
            {
                var instrEvent = new Dtos.InstructionalEvent()
                {
                    Guid = meeting3.Guid,
                    Section = new Dtos.GuidObject() { Guid = section3.Guid },
                    InstructionalMethod = new Dtos.GuidObject() { Guid = instructionalMethodGuid },
                    Workload = 10.5m,
                    Recurrence = new Dtos.Recurrence() { Interval = 1, Frequency = Dtos.FrequencyType.Monthly, Days = new List<DayOfWeek>() { DayOfWeek.Monday } },
                    StartDate = new DateTime(2012, 1, 1),
                    EndDate = new DateTime(2012, 12, 21),
                    StartTime = new DateTimeOffset(new DateTime(2012, 1, 1, 8, 30, 0)),
                    EndTime = new DateTimeOffset(new DateTime(2012, 12, 21, 9, 30, 0))
                };
                var r = await sectionCoordinationService.UpdateInstructionalEventAsync(instrEvent);
                Assert.AreEqual(r.Guid, meeting3.Guid);
                Assert.AreEqual(r.InstructionalMethod.Guid, instructionalMethodGuid);
                Assert.AreEqual(r.Recurrence.Frequency.ToString(), FrequencyType.Monthly.ToString());
                Assert.AreEqual(r.StartDate, meeting3.StartDate);
                Assert.AreEqual(r.EndDate, meeting3.EndDate);
                Assert.AreEqual(r.StartTime, meeting3.StartTime);
                Assert.AreEqual(r.EndTime, meeting3.EndTime);
            }

            [TestMethod]
            public async Task SectionService_Monthly_V4()
            {
                var timePeriod = new Dtos.RepeatTimePeriod2
                {
                    StartOn = new DateTime(2012, 1, 1, 8, 30, 0),
                    EndOn = new DateTime(2012, 12, 21, 9, 30, 0)
                };
                var repeatRuleEnds = new Dtos.RepeatRuleEnds { Date = new DateTime(2012, 12, 21) };
                var repeatRule = new Dtos.RepeatRuleMonthly { RepeatBy = new Dtos.RepeatRuleRepeatBy() { DayOfMonth = 30 }, Ends = new Dtos.RepeatRuleEnds() { Date = new DateTime(2012, 12, 21) } };
                var instrEvent = new Dtos.InstructionalEvent2()
                {
                    Id = meeting3.Guid,
                    Section = new Dtos.GuidObject2() { Id = section3.Guid },
                    InstructionalMethod = new Dtos.GuidObject2() { Id = instructionalMethodGuid },
                    Workload = 10.5m,
                    Recurrence = new Dtos.Recurrence3() { RepeatRule = repeatRule, TimePeriod = timePeriod }
                };
                var r = await sectionCoordinationService.UpdateInstructionalEvent2Async(instrEvent);
                Assert.AreEqual(r.Id, meeting3.Guid);
                Assert.AreEqual(r.InstructionalMethod.Id, instructionalMethodGuid);
                Assert.AreEqual(r.Recurrence.RepeatRule.Type.ToString(), FrequencyType.Monthly.ToString());
                Assert.AreEqual(r.Recurrence.TimePeriod.StartOn.Date, timePeriod.StartOn.Date);
                Assert.AreEqual(r.Recurrence.TimePeriod.EndOn.Date, timePeriod.EndOn.Date);
                Assert.AreEqual(r.Recurrence.TimePeriod.StartOn.ToLocalTime().TimeOfDay.ToString(), "08:30:00");
                Assert.AreEqual(r.Recurrence.TimePeriod.EndOn.ToLocalTime().TimeOfDay.ToString(), "09:30:00");
            }

            [TestMethod]
            public async Task SectionService_Yearly_V3()
            {
                var instrEvent = new Dtos.InstructionalEvent()
                {
                    Guid = meeting4.Guid,
                    Section = new Dtos.GuidObject() { Guid = section3.Guid },
                    InstructionalMethod = new Dtos.GuidObject() { Guid = instructionalMethodGuid },
                    Workload = 10.5m,
                    Recurrence = new Dtos.Recurrence() { Interval = 1, Frequency = Dtos.FrequencyType.Yearly, Days = new List<DayOfWeek>() { DayOfWeek.Monday } },
                    StartDate = new DateTime(2012, 1, 1),
                    EndDate = new DateTime(2012, 12, 21),
                    StartTime = new DateTimeOffset(new DateTime(2012, 1, 1, 8, 30, 0)),
                    EndTime = new DateTimeOffset(new DateTime(2012, 12, 21, 9, 30, 0))
                };
                var r = await sectionCoordinationService.UpdateInstructionalEventAsync(instrEvent);
                Assert.AreEqual(r.Guid, meeting4.Guid);
                Assert.AreEqual(r.InstructionalMethod.Guid, instructionalMethodGuid);
                Assert.AreEqual(r.Recurrence.Frequency.ToString(), FrequencyType.Yearly.ToString());
                Assert.AreEqual(r.StartDate, meeting4.StartDate);
                Assert.AreEqual(r.EndDate, meeting4.EndDate);
                Assert.AreEqual(r.StartTime, meeting4.StartTime);
                Assert.AreEqual(r.EndTime, meeting4.EndTime);
            }

            [TestMethod]
            public async Task SectionService_Yearly_V4()
            {
                var timePeriod = new Dtos.RepeatTimePeriod2
                {
                    StartOn = new DateTime(2012, 1, 1, 8, 30, 0),
                    EndOn = new DateTime(2012, 12, 21, 9, 30, 0)
                };
                var repeatRuleEnds = new Dtos.RepeatRuleEnds { Date = new DateTime(2012, 12, 21) };
                var repeatRule = new Dtos.RepeatRuleYearly { Ends = new Dtos.RepeatRuleEnds() { Date = new DateTime(2012, 12, 21) } };
                var instrEvent = new Dtos.InstructionalEvent2()
                {
                    Id = meeting4.Guid,
                    Section = new Dtos.GuidObject2() { Id = section3.Guid },
                    InstructionalMethod = new Dtos.GuidObject2() { Id = instructionalMethodGuid },
                    Workload = 10.5m,
                    Recurrence = new Dtos.Recurrence3() { RepeatRule = repeatRule, TimePeriod = timePeriod }
                };
                var r = await sectionCoordinationService.UpdateInstructionalEvent2Async(instrEvent);
                Assert.AreEqual(r.Id, meeting4.Guid);
                Assert.AreEqual(r.InstructionalMethod.Id, instructionalMethodGuid);
                Assert.AreEqual(r.Recurrence.RepeatRule.Type.ToString(), FrequencyType.Yearly.ToString());
                Assert.AreEqual(r.Recurrence.TimePeriod.StartOn.Date, timePeriod.StartOn.Date);
                Assert.AreEqual(r.Recurrence.TimePeriod.EndOn.Date, timePeriod.EndOn.Date);
                Assert.AreEqual(r.Recurrence.TimePeriod.StartOn.ToLocalTime().TimeOfDay.ToString(), "08:30:00");
                Assert.AreEqual(r.Recurrence.TimePeriod.EndOn.ToLocalTime().TimeOfDay.ToString(), "09:30:00");
            }
        }

        [TestClass]
        public class InstructionalEvents_Post : CurrentUserSetup
        {
            private SectionCoordinationService sectionCoordinationService;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepo;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private IStudentReferenceDataRepository studentReferenceDataRepo;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private Mock<ICourseRepository> courseRepoMock;
            private ICourseRepository courseRepo;
            private Mock<ITermRepository> termRepoMock;
            private ITermRepository termRepo;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private IStudentConfigurationRepository studentConfigRepo;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IRoomRepository> roomRepoMock;
            private IRoomRepository roomRepo;
            private Mock<IEventRepository> eventRepoMock;
            private IEventRepository eventRepo;


            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;

            private Domain.Student.Entities.Section section1;
            private Domain.Student.Entities.Section section2;
            private Domain.Student.Entities.Section section3;
            private Domain.Student.Entities.SectionMeeting meeting1;
            private Domain.Student.Entities.SectionMeeting meeting2;
            private Domain.Student.Entities.SectionMeeting meeting3;
            private Domain.Student.Entities.SectionMeeting meeting4;

            private List<OfferingDepartment> dpts = new List<OfferingDepartment>() { new OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>() { "Whatever" };
            private List<SectionStatusItem> statuses = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddDays(-60)) };

            private string meeting1Guid = Guid.NewGuid().ToString();
            private string instructionalMethodGuid = Guid.NewGuid().ToString();
            private string roomGuid = Guid.NewGuid().ToString();

            [TestInitialize]
            public void Initialize()
            {
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepo = sectionRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                roomRepoMock = new Mock<IRoomRepository>();
                roomRepo = roomRepoMock.Object;
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                studentReferenceDataRepo = stuRefDataRepoMock.Object;
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                referenceDataRepo = referenceDataRepoMock.Object;
                courseRepoMock = new Mock<ICourseRepository>();
                courseRepo = courseRepoMock.Object;
                termRepoMock = new Mock<ITermRepository>();
                termRepo = termRepoMock.Object;
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                studentConfigRepo = studentConfigRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
                eventRepoMock = new Mock<IEventRepository>();
                eventRepo = eventRepoMock.Object;


                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;

                // Set Current User
                currentUserFactory = new CurrentUserSetup.ThirdPartyUserFactory();

                logger = new Mock<ILogger>().Object;

                // Mock the section repo response
                section1 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section1.TermId = "2012/FA";
                section1.EndDate = new DateTime(2012, 12, 21);
                section1.Guid = Guid.NewGuid().ToString();

                section2 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section2.TermId = "2012/FA";
                section2.Guid = Guid.NewGuid().ToString();

                section3 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section3.TermId = "2011/FA";
                section3.EndDate = new DateTime(2011, 12, 21);
                section3.Guid = Guid.NewGuid().ToString();

                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LEC", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting1.Guid = meeting1Guid;
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "DAN*102";
                section1.AddSectionMeeting(meeting1);

                meeting2 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting2.Guid = meeting1Guid;

                meeting3 = new Domain.Student.Entities.SectionMeeting("456", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting3.Guid = meeting1Guid;

                meeting4 = new Domain.Student.Entities.SectionMeeting("789", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting4.Guid = meeting1Guid;

                var instructionalMethods = new List<Domain.Student.Entities.InstructionalMethod>();
                var instructionalMethod = new Domain.Student.Entities.InstructionalMethod(instructionalMethodGuid, "LEC", "Lecture", false);
                instructionalMethods.Add(instructionalMethod);

                var rooms = new List<Domain.Base.Entities.Room>();
                var room = new Domain.Base.Entities.Room(roomGuid, "DAN*102", "Danville Hall Room 102");
                rooms.Add(room);

                var scheduleRepeats = new List<Domain.Base.Entities.ScheduleRepeat>();
                var scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("W", "Weekly", "7", FrequencyType.Weekly);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("D", "Daily", "1", FrequencyType.Daily);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("M", "Monthly", "30", FrequencyType.Monthly);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("Y", "Yearly", "365", FrequencyType.Yearly);
                scheduleRepeats.Add(scheduleRepeat);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).ReturnsAsync(section1);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section2.Guid)).ReturnsAsync(section2);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section3.Guid)).ReturnsAsync(section3);
                sectionRepoMock.Setup(repo => repo.PostSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting1.Guid)).ReturnsAsync(meeting1);
                sectionRepoMock.Setup(repo => repo.PostSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting2.Guid)).ReturnsAsync(meeting2);
                sectionRepoMock.Setup(repo => repo.PostSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting3.Guid)).ReturnsAsync(meeting3);

                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting1.Guid)).ReturnsAsync(meeting1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingIdFromGuidAsync(meeting1.Guid)).ReturnsAsync(meeting1.Id);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting2.Guid)).ReturnsAsync(meeting2);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingIdFromGuidAsync(meeting2.Guid)).ReturnsAsync(meeting2.Id);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting3.Guid)).ReturnsAsync(meeting3);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingIdFromGuidAsync(meeting3.Guid)).ReturnsAsync(meeting3.Id);
                stuRefDataRepoMock.Setup(repo => repo.GetInstructionalMethodsAsync()).ReturnsAsync(instructionalMethods);
                roomRepoMock.Setup(repo => repo.RoomsAsync()).ReturnsAsync(rooms);
                referenceDataRepoMock.Setup(repo => repo.ScheduleRepeats).Returns(scheduleRepeats);

                // Configuration defaults and campus calendar information.
                configRepoMock.Setup(repo => repo.GetDefaultsConfiguration()).Returns(new DefaultsConfiguration() { CampusCalendarId = "MAIN" });
                studentConfigRepoMock.Setup(repo => repo.GetCurriculumConfigurationAsync()).ReturnsAsync(new CurriculumConfiguration() { SectionActiveStatusCode = "A", SectionInactiveStatusCode = "I", DefaultInstructionalMethodCode = "INT" });
                eventRepoMock.Setup(repo => repo.GetCalendar("MAIN")).Returns(new CampusCalendar("MAIN", "Default Calendar", new DateTimeOffset(), new DateTimeOffset()));

                // Mock the section service
                sectionCoordinationService = new SectionCoordinationService(adapterRegistry, sectionRepo, courseRepo, studentRepo, studentReferenceDataRepo,
                    referenceDataRepo, termRepo, studentConfigRepo, configRepo, personRepo, roomRepo, eventRepo, currentUserFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionRepo = null;
                studentRepo = null;
                roleRepo = null;
                adapterRegistry = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(IntegrationApiException))]
            public async Task SectionService_Null_Object_V3()
            {
                var r = await sectionCoordinationService.CreateInstructionalEventAsync(new Dtos.InstructionalEvent());
            }

            [TestMethod]
            [ExpectedException(typeof(IntegrationApiException))]
            public async Task SectionService_Null_Object_V4()
            {
                var r = await sectionCoordinationService.CreateInstructionalEvent2Async(new Dtos.InstructionalEvent2());
            }

            [TestMethod]
            [ExpectedException(typeof(IntegrationApiException))]
            public async Task SectionService_SectionKeyNotFound_V3()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).Throws(new KeyNotFoundException());
                var r = await sectionCoordinationService.CreateInstructionalEventAsync(new Dtos.InstructionalEvent()
                {
                    Guid = meeting1.Guid,
                    Section = new Dtos.GuidObject() { Guid = section1.Guid }
                });
            }

            [TestMethod]
            [ExpectedException(typeof(IntegrationApiException))]
            public async Task SectionService_SectionKeyNotFound_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).Throws(new KeyNotFoundException());
                var r = await sectionCoordinationService.CreateInstructionalEvent2Async(new Dtos.InstructionalEvent2()
                {
                    Id = meeting1.Guid,
                    Section = new Dtos.GuidObject2() { Id = section1.Guid }
                });
            }

            [TestMethod]
            [ExpectedException(typeof(IntegrationApiException))]
            public async Task SectionService_InvalidGuid_V3()
            {
                var instrEvent = new Dtos.InstructionalEvent() { Guid = "999999999999999" };
                var r = await sectionCoordinationService.CreateInstructionalEventAsync(instrEvent);
            }

            [TestMethod]
            [ExpectedException(typeof(IntegrationApiException))]
            public async Task SectionService_InvalidId_V4()
            {
                var instrEvent = new Dtos.InstructionalEvent2() { Id = "999999999999999" };
                var r = await sectionCoordinationService.CreateInstructionalEvent2Async(instrEvent);
            }

            [TestMethod]
            public async Task SectionService_OverrideRoomCapacity_V3()
            {
                section1.SectionCapacity = 0;
                var approval = new Dtos.InstructionalEventApproval() { ApprovingEntity = Dtos.InstructionalEventApprovalEntity.User, Type = Dtos.InstructionalEventApprovalType.RoomCapacity };
                var instrEvent = new Dtos.InstructionalEvent()
                {
                    Guid = meeting1.Guid,
                    Section = new Dtos.GuidObject() { Guid = section1.Guid },
                    InstructionalMethod = new Dtos.GuidObject() { Guid = instructionalMethodGuid },
                    Workload = 10.5m,
                    Approvals = new List<Dtos.InstructionalEventApproval>() { approval },
                    Recurrence = new Dtos.Recurrence() { Interval = 1, Frequency = Dtos.FrequencyType.Daily, Days = new List<DayOfWeek>() { DayOfWeek.Monday } },
                    StartDate = new DateTime(2012, 1, 1),
                    EndDate = new DateTime(2012, 12, 21),
                    StartTime = new DateTimeOffset(new DateTime(2012, 1, 1, 8, 30, 0)),
                    EndTime = new DateTimeOffset(new DateTime(2012, 12, 21, 9, 30, 0))
                };
                var r = await sectionCoordinationService.CreateInstructionalEventAsync(instrEvent);
                Assert.AreEqual(r.Guid, meeting1.Guid);
            }

            [TestMethod]
            public async Task SectionService_OverrideRoomCapacity_V4()
            {
                section1.SectionCapacity = 0;
                var approval = new Dtos.InstructionalEventApproval2() { ApprovingEntity = Dtos.InstructionalEventApprovalEntity2.User, Type = Dtos.InstructionalEventApprovalType2.RoomCapacity };
                var timePeriod = new Dtos.RepeatTimePeriod2
                {
                    StartOn = new DateTime(2012, 1, 1),
                    EndOn = new DateTime(2012, 12, 21)
                };
                var repeatRuleEnds = new Dtos.RepeatRuleEnds { Date = new DateTime(2012, 12, 21) };
                var repeatRule = new Dtos.RepeatRuleDaily { Type = Dtos.FrequencyType2.Daily, Ends = repeatRuleEnds, Interval = 1 };
                var instrEvent = new Dtos.InstructionalEvent2()
                {
                    Id = meeting1.Guid,
                    Section = new Dtos.GuidObject2() { Id = section1.Guid },
                    InstructionalMethod = new Dtos.GuidObject2() { Id = instructionalMethodGuid },
                    Workload = 10.5m,
                    Approvals = new List<Dtos.InstructionalEventApproval2>() { approval },
                    Recurrence = new Dtos.Recurrence3() { RepeatRule = repeatRule, TimePeriod = timePeriod }
                };
                var r = await sectionCoordinationService.CreateInstructionalEvent2Async(instrEvent);
                Assert.AreEqual(r.Id, meeting1.Guid);
            }
        }

        [TestClass]
        public class InstructionalEvents_Delete : CurrentUserSetup
        {
            private SectionCoordinationService sectionCoordinationService;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepo;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private IStudentReferenceDataRepository studentReferenceDataRepo;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private Mock<ICourseRepository> courseRepoMock;
            private ICourseRepository courseRepo;
            private Mock<ITermRepository> termRepoMock;
            private ITermRepository termRepo;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private IStudentConfigurationRepository studentConfigRepo;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IRoomRepository> roomRepoMock;
            private IRoomRepository roomRepo;
            private Mock<IEventRepository> eventRepoMock;
            private IEventRepository eventRepo;

            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;

            private Domain.Student.Entities.Section section1;
            private Domain.Student.Entities.Section section2;
            private Domain.Student.Entities.Section section3;
            private Domain.Student.Entities.SectionMeeting meeting1;
            private Domain.Student.Entities.SectionMeeting meeting2;
            private Domain.Student.Entities.SectionMeeting meeting3;
            private Domain.Student.Entities.SectionMeeting meeting4;

            private List<OfferingDepartment> dpts = new List<OfferingDepartment>() { new OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>() { "Whatever" };
            private List<SectionStatusItem> statuses = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddDays(-60)) };

            private string meeting1Guid = Guid.NewGuid().ToString();
            private string instructionalMethodGuid = Guid.NewGuid().ToString();
            private string roomGuid = Guid.NewGuid().ToString();

            [TestInitialize]
            public void Initialize()
            {
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepo = sectionRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                roomRepoMock = new Mock<IRoomRepository>();
                roomRepo = roomRepoMock.Object;
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                studentReferenceDataRepo = stuRefDataRepoMock.Object;
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                referenceDataRepo = referenceDataRepoMock.Object;
                courseRepoMock = new Mock<ICourseRepository>();
                courseRepo = courseRepoMock.Object;
                termRepoMock = new Mock<ITermRepository>();
                termRepo = termRepoMock.Object;
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                studentConfigRepo = studentConfigRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
                eventRepoMock = new Mock<IEventRepository>();
                eventRepo = eventRepoMock.Object;
                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;

                // Set Current User
                currentUserFactory = new CurrentUserSetup.ThirdPartyUserFactory();

                logger = new Mock<ILogger>().Object;

                // Mock the section repo response
                section1 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section1.TermId = "2012/FA";
                section1.EndDate = new DateTime(2012, 12, 21);

                section2 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section2.TermId = "2012/FA";

                section3 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section3.TermId = "2011/FA";
                section3.EndDate = new DateTime(2011, 12, 21);

                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LEC", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting1.Guid = meeting1Guid;
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "DAN*102";
                section1.AddSectionMeeting(meeting1);

                meeting2 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting2.Guid = meeting1Guid;

                meeting3 = new Domain.Student.Entities.SectionMeeting("456", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting3.Guid = meeting1Guid;

                meeting4 = new Domain.Student.Entities.SectionMeeting("789", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting4.Guid = meeting1Guid;

                var instructionalMethods = new List<Domain.Student.Entities.InstructionalMethod>();
                var instructionalMethod = new Domain.Student.Entities.InstructionalMethod(instructionalMethodGuid, "LEC", "Lecture", false);
                instructionalMethods.Add(instructionalMethod);

                var rooms = new List<Domain.Base.Entities.Room>();
                var room = new Domain.Base.Entities.Room(roomGuid, "DAN*102", "Danville Hall Room 102");
                rooms.Add(room);

                var scheduleRepeats = new List<Domain.Base.Entities.ScheduleRepeat>();
                var scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("W", "Weekly", "7", FrequencyType.Weekly);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("D", "Daily", "1", FrequencyType.Daily);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("M", "Monthly", "30", FrequencyType.Monthly);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("Y", "Yearly", "365", FrequencyType.Yearly);
                scheduleRepeats.Add(scheduleRepeat);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).ReturnsAsync(section1);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section2.Guid)).ReturnsAsync(section2);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section3.Guid)).ReturnsAsync(section3);

                sectionRepoMock.Setup(repo => repo.GetSectionMeetingByGuidAsync(meeting1.Guid)).ReturnsAsync(meeting1);
                stuRefDataRepoMock.Setup(repo => repo.GetInstructionalMethodsAsync()).ReturnsAsync(instructionalMethods);
                roomRepoMock.Setup(repo => repo.RoomsAsync()).ReturnsAsync(rooms);
                referenceDataRepoMock.Setup(repo => repo.ScheduleRepeats).Returns(scheduleRepeats);

                // Mock the section service
                sectionCoordinationService = new SectionCoordinationService(adapterRegistry, sectionRepo, courseRepo, studentRepo, studentReferenceDataRepo,
                    referenceDataRepo, termRepo, studentConfigRepo, configRepo, personRepo, roomRepo, eventRepo, currentUserFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionRepo = null;
                studentRepo = null;
                roleRepo = null;
                adapterRegistry = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_Null_Guid()
            {
                await sectionCoordinationService.DeleteInstructionalEventAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionService_InvalidGuid()
            {
                await sectionCoordinationService.DeleteInstructionalEventAsync("999999999999999");
            }

            [TestMethod]
            public async Task SectionService_Delete()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionAsync(meeting1.SectionId)).ReturnsAsync(section1);
                sectionRepoMock.Setup(repo => repo.DeleteSectionMeetingAsync(meeting1.Id, section1.Faculty.ToList()));
                await sectionCoordinationService.DeleteInstructionalEventAsync(meeting1.Guid);
            }
        }

        [TestClass]
        public class InstructionalEvents_Filters : CurrentUserSetup
        {
            private SectionCoordinationService sectionCoordinationService;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepo;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private IStudentReferenceDataRepository studentReferenceDataRepo;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private Mock<ICourseRepository> courseRepoMock;
            private ICourseRepository courseRepo;
            private Mock<ITermRepository> termRepoMock;
            private ITermRepository termRepo;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private IStudentConfigurationRepository studentConfigRepo;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IRoomRepository> roomRepoMock;
            private IRoomRepository roomRepo;
            private Mock<IEventRepository> eventRepoMock;
            private IEventRepository eventRepo;


            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;

            private Domain.Student.Entities.SectionMeeting meeting1;
            private Domain.Student.Entities.SectionMeeting meeting2;
            private Domain.Student.Entities.SectionMeeting meeting3;
            private Domain.Student.Entities.SectionMeeting meeting4;

            private string meeting1Guid = Guid.NewGuid().ToString();
            private string instructionalMethodGuid = Guid.NewGuid().ToString();
            private string roomGuid = Guid.NewGuid().ToString();
            private string sectionGuid = Guid.NewGuid().ToString();
            private string instructorGuid = Guid.NewGuid().ToString();

            [TestInitialize]
            public void Initialize()
            {
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepo = sectionRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                roomRepoMock = new Mock<IRoomRepository>();
                roomRepo = roomRepoMock.Object;
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                studentReferenceDataRepo = stuRefDataRepoMock.Object;
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                referenceDataRepo = referenceDataRepoMock.Object;
                courseRepoMock = new Mock<ICourseRepository>();
                courseRepo = courseRepoMock.Object;
                termRepoMock = new Mock<ITermRepository>();
                termRepo = termRepoMock.Object;
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                studentConfigRepo = studentConfigRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
                eventRepoMock = new Mock<IEventRepository>();
                eventRepo = eventRepoMock.Object;

                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;

                // Set Current User
                currentUserFactory = new CurrentUserSetup.ThirdPartyUserFactory();

                logger = new Mock<ILogger>().Object;

                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LEC", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting1.Guid = meeting1Guid;
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "DAN*102";

                meeting2 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting2.Guid = meeting1Guid;

                meeting3 = new Domain.Student.Entities.SectionMeeting("456", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting3.Guid = meeting1Guid;

                meeting4 = new Domain.Student.Entities.SectionMeeting("789", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting4.Guid = meeting1Guid;

                var instructionalMethods = new List<Domain.Student.Entities.InstructionalMethod>();
                var instructionalMethod = new Domain.Student.Entities.InstructionalMethod(instructionalMethodGuid, "LEC", "Lecture", false);
                instructionalMethods.Add(instructionalMethod);

                var rooms = new List<Domain.Base.Entities.Room>();
                var room = new Domain.Base.Entities.Room(roomGuid, "DAN*102", "Danville Hall Room 102");
                rooms.Add(room);

                var scheduleRepeats = new List<Domain.Base.Entities.ScheduleRepeat>();
                var scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("W", "Weekly", "7", FrequencyType.Weekly);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("D", "Daily", "1", FrequencyType.Daily);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("M", "Monthly", "30", FrequencyType.Monthly);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("Y", "Yearly", "365", FrequencyType.Yearly);
                scheduleRepeats.Add(scheduleRepeat);

                sectionRepoMock.Setup(repo => repo.GetSectionIdFromGuidAsync(sectionGuid)).ReturnsAsync("SEC1");
                sectionRepoMock.Setup(repo => repo.GetSectionGuidFromIdAsync("SEC1")).ReturnsAsync(sectionGuid);
                stuRefDataRepoMock.Setup(repo => repo.GetInstructionalMethodsAsync()).ReturnsAsync(instructionalMethods);
                roomRepoMock.Setup(repo => repo.RoomsAsync()).ReturnsAsync(rooms);
                roomRepoMock.Setup(repo => repo.GetRoomsAsync(false)).ReturnsAsync(rooms);
                referenceDataRepoMock.Setup(repo => repo.ScheduleRepeats).Returns(scheduleRepeats);
                personRepoMock.Setup(repo => repo.GetPersonGuidFromIdAsync("0000678")).ReturnsAsync(instructorGuid);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(It.IsAny<string>())).ReturnsAsync(meeting1);
                personRepoMock.Setup(repo => repo.GetPersonIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("0000678");

                // Mock the section service
                sectionCoordinationService = new SectionCoordinationService(adapterRegistry, sectionRepo, courseRepo, studentRepo, studentReferenceDataRepo,
                    referenceDataRepo, termRepo, studentConfigRepo, configRepo, personRepo, roomRepo, eventRepo, currentUserFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionRepo = null;
                studentRepo = null;
                roleRepo = null;
                adapterRegistry = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_Null_Arguments()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(It.IsAny<int>(), It.IsAny<int>(), "", "", "", "", "", "", "", "")).Throws(new ArgumentException());
                await sectionCoordinationService.GetInstructionalEvent2Async(It.IsAny<int>(), It.IsAny<int>(), "", "", "", "", "");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_InvalidSection()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionIdFromGuidAsync(It.IsAny<string>())).Throws(new ArgumentException());
                await sectionCoordinationService.GetInstructionalEvent2Async(It.IsAny<int>(), It.IsAny<int>(), Guid.NewGuid().ToString(), "", "", "", "");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_InvalidStartOn()
            {
                await sectionCoordinationService.GetInstructionalEvent2Async(It.IsAny<int>(), It.IsAny<int>(), "", "abc", "", "", "");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_InvalidEndOn()
            {
                await sectionCoordinationService.GetInstructionalEvent2Async(It.IsAny<int>(), It.IsAny<int>(), "", "", "abc", "", "");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_InvalidRoom()
            {
                roomRepoMock.Setup(repo => repo.GetRoomsAsync(false)).Throws(new ArgumentException());
                await sectionCoordinationService.GetInstructionalEvent2Async(It.IsAny<int>(), It.IsAny<int>(), "", "", "", "abc", "");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_InvalidInstructor()
            {
                personRepoMock.Setup(repo => repo.GetPersonIdFromGuidAsync(It.IsAny<string>())).Throws(new ArgumentException());
                await sectionCoordinationService.GetInstructionalEvent2Async(It.IsAny<int>(), It.IsAny<int>(), "", "", "", "", "abc");
            }

            [TestMethod]
            public async Task SectionService_Section()
            {
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 1, "SEC1", "", "", "", "", "", "", "")).ReturnsAsync(tuple);
                var r = await sectionCoordinationService.GetInstructionalEvent2Async(0, 1, sectionGuid, "", "", "", "");
                var instrEvent = r.Item1.FirstOrDefault();
                Assert.AreEqual(instrEvent.Section.Id, sectionGuid);
            }

            [TestMethod]
            public async Task SectionService_StartDate()
            {
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 1, "", new DateTime(2012, 01, 01, 0, 0, 0, DateTimeKind.Local).ToShortDateString(), "", "", "", "", "", "")).ReturnsAsync(tuple);
                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate(new DateTime(2012, 01, 01, 0, 0, 0, DateTimeKind.Local).ToShortDateString())).ReturnsAsync(new DateTime(2012, 01, 01).ToShortDateString());
                var r = await sectionCoordinationService.GetInstructionalEvent2Async(0, 1, "", new DateTime(2012, 01, 01, 0, 0, 0, DateTimeKind.Local).ToShortDateString(), "", "", "");
                var instrEvent = r.Item1.FirstOrDefault();
                Assert.AreEqual(instrEvent.Recurrence.TimePeriod.StartOn.Date.ToShortDateString(), new DateTime(2012, 01, 01, 0, 0, 0, DateTimeKind.Local).ToShortDateString());
            }

            [TestMethod]
            public async Task SectionService_EndDate()
            {
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 1, "", "", new DateTime(2012, 12, 21, 0, 0, 0, DateTimeKind.Local).ToShortDateString(), "", "", "", "", "")).ReturnsAsync(tuple);
                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate(new DateTime(2012, 12, 21, 0, 0, 0, DateTimeKind.Local).ToShortDateString())).ReturnsAsync(new DateTime(2012, 12, 21, 0, 0, 0, DateTimeKind.Local).ToShortDateString());
                var r = await sectionCoordinationService.GetInstructionalEvent2Async(0, 1, "", "", new DateTime(2012, 12, 21, 0, 0, 0, DateTimeKind.Local).ToShortDateString(), "", "");
                var instrEvent = r.Item1.FirstOrDefault();
                Assert.AreEqual(instrEvent.Recurrence.TimePeriod.EndOn.Date.ToShortDateString(), new DateTime(2012, 12, 21, 0, 0, 0, DateTimeKind.Local).ToShortDateString());
            }

            [TestMethod]
            public async Task SectionService_StartTime()
            {
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 1, "", new DateTime(2012, 01, 01, 0, 0, 0, DateTimeKind.Local).ToShortDateString(), "", new DateTime(2012, 01, 01, 08, 30, 0, DateTimeKind.Local).ToLocalTime().TimeOfDay.ToString(), "", "", "", "")).ReturnsAsync(tuple);
                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate(new DateTime(2012, 01, 01, 08, 30, 0, DateTimeKind.Local).ToString("yyyy-MM-ddThh:mm:ss"))).ReturnsAsync(new DateTime(2012, 01, 01, 08, 30, 0, DateTimeKind.Local).ToShortDateString());
                var r = await sectionCoordinationService.GetInstructionalEvent2Async(0, 1, "", new DateTime(2012, 01, 01, 08, 30, 0, DateTimeKind.Local).ToString("yyyy-MM-ddThh:mm:ss"), "", "", "");
                var instrEvent = r.Item1.FirstOrDefault();
                Assert.AreEqual(instrEvent.Recurrence.TimePeriod.StartOn.TimeOfDay.ToString(), new DateTime(2012, 01, 01, 08, 30, 0, DateTimeKind.Local).ToLocalTime().TimeOfDay.ToString());
            }

            [TestMethod]
            public async Task SectionService_EndTime()
            {
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 1, "", "", new DateTime(2012, 12, 21, 0, 0, 0, DateTimeKind.Local).ToShortDateString(), "", new DateTime(2012, 12, 21, 09, 30, 0, DateTimeKind.Local).ToLocalTime().TimeOfDay.ToString(), "", "", "")).ReturnsAsync(tuple);
                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate(new DateTime(2012, 12, 21, 09, 30, 0, DateTimeKind.Local).ToString("yyyy-MM-ddThh:mm:ss"))).ReturnsAsync(new DateTime(2012, 12, 21, 0, 0, 0, DateTimeKind.Local).ToShortDateString());
                var r = await sectionCoordinationService.GetInstructionalEvent2Async(0, 1, "", "", new DateTime(2012, 12, 21, 09, 30, 0, DateTimeKind.Local).ToString("yyyy-MM-ddThh:mm:ss"), "", "");
                var instrEvent = r.Item1.FirstOrDefault();
                Assert.AreEqual(instrEvent.Recurrence.TimePeriod.EndOn.TimeOfDay.ToString(), new DateTime(2012, 12, 21, 09, 30, 0, DateTimeKind.Local).ToLocalTime().TimeOfDay.ToString());
            }

            [TestMethod]
            public async Task SectionService_ClassRoom()
            {
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 1, "", "", "", "", "", "DAN", "102", "")).ReturnsAsync(tuple);
                var r = await sectionCoordinationService.GetInstructionalEvent2Async(0, 1, "", "", "", roomGuid, "");
                var instrEvent = r.Item1.FirstOrDefault();
                Assert.AreEqual(((Dtos.InstructionalRoom2)instrEvent.Locations.ElementAt(0).Locations).Room.Id, roomGuid);
            }

            [TestMethod]
            public async Task SectionService_Instructor()
            {
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 1, "", "", "", "", "", "", "", "0000678")).ReturnsAsync(tuple);
                var r = await sectionCoordinationService.GetInstructionalEvent2Async(0, 1, "", "", "", "", instructorGuid);
                var instrEvent = r.Item1.FirstOrDefault();
                Assert.AreEqual(instrEvent.Instructors.FirstOrDefault().Instructor.Id, instructorGuid);
            }
        }
       

        [TestClass]
        public class SectionCoordinationServiceTests_V4 : CurrentUserSetup
        {
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private Mock<ISectionRepository> sectionRepoMock;
            private Mock<IStudentRepository> studentRepoMock;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private Mock<ICourseRepository> courseRepoMock;
            private Mock<ITermRepository> termRepoMock;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private Mock<IConfigurationRepository> configRepoMock;
            private Mock<IPersonRepository> personRepoMock;
            private Mock<IRoomRepository> roomRepoMock;
            private Mock<IEventRepository> eventRepoMock;
            private Mock<IRoleRepository> roleRepoMock;
            private Mock<ILogger> loggerMock;
            private ICurrentUserFactory currentUserFactory;
            

            private SectionCoordinationService sectionCoordinationService;
            Tuple<IEnumerable<Domain.Student.Entities.Section>, int> sectionMaxEntitiesTuple;
            IEnumerable<Domain.Student.Entities.Section> sectionEntities;

            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Course> courseEntities;
            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Subject> subjectEntities;
            IEnumerable<Domain.Student.Entities.CreditCategory> creditCategoryEntities;
            IEnumerable<Domain.Student.Entities.GradeScheme> gradeSchemeEntities;
            IEnumerable<Domain.Student.Entities.CourseLevel> courseLevelEntities;
            IEnumerable<Domain.Student.Entities.AcademicPeriod> acadPeriodsEntities;
            IEnumerable<Domain.Student.Entities.AcademicLevel> academicLevelEntities;
            IEnumerable<Domain.Base.Entities.InstructionalPlatform> instructionalPlatformEntities;
            IEnumerable<Domain.Base.Entities.Location> locationEntities;
            IEnumerable<Domain.Base.Entities.Department> departmentEntities;
            IEnumerable<Domain.Student.Entities.InstructionalMethod> instrMethodEntities;
            IEnumerable<Domain.Base.Entities.Room> roomEntities;

            Domain.Base.Entities.Person person;
            private Domain.Student.Entities.SectionMeeting meeting1;


            [TestInitialize]
            public void Initialize()
            {
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                sectionRepoMock = new Mock<ISectionRepository>();
                courseRepoMock = new Mock<ICourseRepository>();
                studentRepoMock = new Mock<IStudentRepository>();
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                termRepoMock = new Mock<ITermRepository>();
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                configRepoMock = new Mock<IConfigurationRepository>();
                personRepoMock = new Mock<IPersonRepository>();
                roomRepoMock = new Mock<IRoomRepository>();
                eventRepoMock = new Mock<IEventRepository>();
                roleRepoMock = new Mock<IRoleRepository>();
                loggerMock = new Mock<ILogger>();

                currentUserFactory = new CurrentUserSetup.StudentUserFactory();
                BuildData();

                sectionCoordinationService = new SectionCoordinationService(adapterRegistryMock.Object, sectionRepoMock.Object, courseRepoMock.Object, studentRepoMock.Object,
                    stuRefDataRepoMock.Object, referenceDataRepoMock.Object, termRepoMock.Object, studentConfigRepoMock.Object, configRepoMock.Object, personRepoMock.Object,
                    roomRepoMock.Object, eventRepoMock.Object, currentUserFactory, roleRepoMock.Object, loggerMock.Object);

            }           

            [TestCleanup]
            public void Cleanup()
            {
                adapterRegistryMock = null;
                sectionRepoMock = null;
                courseRepoMock = null;
                studentRepoMock = null;
                stuRefDataRepoMock = null;
                referenceDataRepoMock = null;
                termRepoMock = null;
                studentConfigRepoMock = null;
                configRepoMock = null;
                personRepoMock = null;
                roomRepoMock = null;
                eventRepoMock = null;
                roleRepoMock = null;
                sectionCoordinationService = null;
                courseEntities = null;
                subjectEntities = null;
                creditCategoryEntities = null;
                gradeSchemeEntities = null;
                courseLevelEntities = null;
                acadPeriodsEntities = null;
                academicLevelEntities = null;
                instructionalPlatformEntities = null;
                locationEntities = null;
                departmentEntities = null;
                instrMethodEntities = null;
                roomEntities = null;
                person = null;
                meeting1 = null;
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V4_GetSectionsMaximumAsync_WithAllFilters()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);

                var results = await sectionCoordinationService.GetSectionsMaximumAsync(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");

                int count = results.Item1.Count();
                Assert.IsNotNull(results);
                Assert.AreEqual(1, count);

                for (int i = 0; i < count; i++)
                {
                    var actual = results.Item1.ToList()[i];
                    var expected = sectionEntities.ToList()[i];

                    Assert.AreEqual(expected.Guid, actual.Id);
                    Assert.AreEqual(expected.Title, actual.Title);
                    Assert.AreEqual(expected.StartDate, actual.StartOn);
                    Assert.AreEqual(expected.AcademicLevelCode, actual.AcademicLevels.ToList()[0].Code);
                    Assert.AreEqual(expected.Ceus, actual.Credits.ToList()[0].Minimum);
                    Assert.AreEqual(expected.Comments, actual.Description);
                    Assert.AreEqual(expected.CourseLevelCodes.Count(), actual.CourseLevels.Count());
                }
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V4_GetSectionMaximumByGuidAsync()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuidAsync("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V4_GetSectionMaximumByGuidAsync_CE_CreditType()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", "C", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuidAsync("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V4_GetSectionMaximumByGuidAsync_Transfer_CreditType()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", "T", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuidAsync("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V4_GetSectionMaximumByGuidAsync_CreditType_Blank()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", " ", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuidAsync("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V4_GetSectionMaximumByGuidAsync_No_CEUS()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, null, "Title 1", "I", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuidAsync("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V4_GetSectionMaximumByGuidAsync_Freq_Daily()
            {
                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LG", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LG", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting1.Guid = "1f1485cb-2705-4b59-9f51-7aa85c79b377";
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "COE*Room 1";
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 0, "1", "", "", "", "", "", "", "")).ReturnsAsync(tuple);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuidAsync("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result); 
                Assert.AreEqual(Dtos.FrequencyType2.Daily, result.InstructionalEvents.ToList()[0].Recurrence.RepeatRule.Type);

            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V4_GetSectionMaximumByGuidAsync_Freq_Monthly_WithDays()
            {
                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LG", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LG", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting1.Days = new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Wednesday };
                meeting1.Guid = "1f1485cb-2705-4b59-9f51-7aa85c79b377";
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "COE*Room 1";
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 0, "1", "", "", "", "", "", "", "")).ReturnsAsync(tuple);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuidAsync("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
                Assert.AreEqual(Dtos.FrequencyType2.Monthly, result.InstructionalEvents.ToList()[0].Recurrence.RepeatRule.Type);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V4_GetSectionMaximumByGuidAsync_Freq_Monthly_WithoutDays()
            {
                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LG", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LG", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting1.Guid = "1f1485cb-2705-4b59-9f51-7aa85c79b377";
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "COE*Room 1";
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 0, "1", "", "", "", "", "", "", "")).ReturnsAsync(tuple);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuidAsync("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result); 
                Assert.AreEqual(Dtos.FrequencyType2.Monthly, result.InstructionalEvents.ToList()[0].Recurrence.RepeatRule.Type);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V4_GetSectionMaximumByGuidAsync_Freq_Yearly()
            {
                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LG", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LG", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting1.Guid = "1f1485cb-2705-4b59-9f51-7aa85c79b377";
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "COE*Room 1";
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 0, "1", "", "", "", "", "", "", "")).ReturnsAsync(tuple);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuidAsync("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
                Assert.AreEqual(Dtos.FrequencyType2.Yearly, result.InstructionalEvents.ToList()[0].Recurrence.RepeatRule.Type);
            }           

            #region All Exceptions

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionCoordinationServiceTests_V4_GetSectionsMaximumAsync_NullSectionGuid_ArgumentException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);
                sectionRepoMock.Setup(repo => repo.GetSectionGuidFromIdAsync(It.IsAny<string>())).ThrowsAsync(new ArgumentException());

                var results = await sectionCoordinationService.GetSectionsMaximumAsync(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionCoordinationServiceTests_V4_GetSectionsMaximumAsync_NullInstrMethodGuid_ArgumentException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);
                stuRefDataRepoMock.Setup(repo => repo.GetInstructionalMethodsAsync()).ThrowsAsync(new ArgumentException());

                var results = await sectionCoordinationService.GetSectionsMaximumAsync(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionCoordinationServiceTests_V4_GetSectionsMaximumAsync_RepeatCodeNull_ArgumentException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);
                
                meeting1.Frequency = "abc";

                var results = await sectionCoordinationService.GetSectionsMaximumAsync(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionCoordinationServiceTests_V4_GetSectionsMaximumAsync_GetPersonGuidFromIdAsync_Error_ArgumentException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);

                personRepoMock.Setup(repo => repo.GetPersonGuidFromIdAsync(It.IsAny<string>())).ThrowsAsync(new ArgumentException());

                var results = await sectionCoordinationService.GetSectionsMaximumAsync(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
            }          
            
            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionCoordinationServiceTests_V4_GetSectionsMaximumAsync_ArgumentException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);
                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate(It.IsAny<string>())).ThrowsAsync(new ArgumentException());

                var results = await sectionCoordinationService.GetSectionsMaximumAsync(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionCoordinationServiceTests_V4_GetSectionsMaximumAsync_NullInstrPlatList_ArgumentNullException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);
                referenceDataRepoMock.Setup(repo => repo.GetInstructionalPlatformsAsync(It.IsAny<bool>())).ReturnsAsync(null);

                var results = await sectionCoordinationService.GetSectionsMaximumAsync(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
            }

            [TestMethod]
            [ExpectedException(typeof(RepositoryException))]
            public async Task SectionCoordinationServiceTests_V4_GetSectionsMaximumAsync_EmptyCourseGuid_RepositoryException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(string.Empty);

                var results = await sectionCoordinationService.GetSectionsMaximumAsync(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
            }

            [TestMethod]
            [ExpectedException(typeof(RepositoryException))]
            public async Task SectionCoordinationServiceTests_V4_GetSectionsMaximumAsync_NullCourse_RepositoryException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);
                courseRepoMock.Setup(repo => repo.GetCourseByGuidAsync(It.IsAny<string>())).ReturnsAsync(null);

                var results = await sectionCoordinationService.GetSectionsMaximumAsync(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionCoordinationServiceTests_V4_GetSectionMaximumByGuidAsync_WithNullId_ArgumentNullException()
            {
                var result = await sectionCoordinationService.GetSectionMaximumByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionCoordinationServiceTests_V4_GetSectionMaximumByGuidAsync_WithNullEntity_KeyNotFoundException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(null);

                var result = await sectionCoordinationService.GetSectionMaximumByGuidAsync("0b983700-29eb-46ff-8616-21a8c3a48a0c");
            }
            
            #endregion

            private void BuildData()
            {
                //Course entity
                courseEntities = new TestCourseRepository().GetAsync().Result.Take(41);
                var courseEntity = courseEntities.FirstOrDefault(i => i.Id.Equals("180"));
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(courseEntity.Guid);
                courseRepoMock.Setup(repo => repo.GetCourseByGuidAsync(courseEntity.Guid)).ReturnsAsync(courseEntity);
                //Subject entity
                subjectEntities = new TestSubjectRepository().Get();
                stuRefDataRepoMock.Setup(repo => repo.GetSubjectsAsync()).ReturnsAsync(subjectEntities);
                var subjectEntity = subjectEntities.FirstOrDefault(s => s.Code.Equals(courseEntity.SubjectCode));
                //Credit Categories
                creditCategoryEntities = new List<Domain.Student.Entities.CreditCategory>()
                {
                    new Domain.Student.Entities.CreditCategory("9C3B805D-CFE6-483B-86C3-4C20562F8C15", "I", "Institutional", Domain.Student.Entities.CreditType.Institutional),
                    new Domain.Student.Entities.CreditCategory("73244057-D1EC-4094-A0B7-DE602533E3A6", "C", "Continuing Education", Domain.Student.Entities.CreditType.ContinuingEducation),
                    new Domain.Student.Entities.CreditCategory("1df164eb-8178-4321-a9f7-24f12d3991d8", "T", "Transfer Credit", Domain.Student.Entities.CreditType.Transfer)
                };
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(creditCategoryEntities);
                //Grade Schemes
                gradeSchemeEntities = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(gradeSchemeEntities);
                //Course Levels
                courseLevelEntities = new List<Domain.Student.Entities.CourseLevel>()
                {
                    new Domain.Student.Entities.CourseLevel("19f6e2cd-1e5f-4485-9b27-60d4f4e4b1ff", "100", "First Yr"),
                    new Domain.Student.Entities.CourseLevel("73244057-D1EC-4094-A0B7-DE602533E3A6", "200", "Second Year"),
                    new Domain.Student.Entities.CourseLevel("1df164eb-8178-4321-a9f7-24f12d3991d8", "300", "Third Year"),
                    new Domain.Student.Entities.CourseLevel("4aa187ae-6d22-4b10-a2e2-1304ebc18176", "400", "Third Year"),
                    new Domain.Student.Entities.CourseLevel("d9f42a0f-39de-44bc-87af-517619141bde", "500", "Third Year")
                };
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(courseLevelEntities);

                //IEnumerable<SectionMeeting>
                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LG", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LG", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting1.Guid = "1f1485cb-2705-4b59-9f51-7aa85c79b377";
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "COE*Room 1";
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 0, "1", "", "", "", "", "", "", "")).ReturnsAsync(tuple);
                sectionRepoMock.Setup(repo => repo.GetSectionGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("ad7655d0-77e3-4f8a-a07c-5c86f6a6f86a");

                //Instructor Id
                personRepoMock.Setup(repo => repo.GetPersonGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("ebf585ad-cc1f-478f-a7a3-aefae87f873a");
                //Instructional Methods
                instrMethodEntities = new List<Domain.Student.Entities.InstructionalMethod>()
                {
                    new Domain.Student.Entities.InstructionalMethod("9C3B805D-CFE6-483B-86C3-4C20562F8C15", "LG", "LG", false),
                    new Domain.Student.Entities.InstructionalMethod("73244057-D1EC-4094-A0B7-DE602533E3A6", "30", "30", true),
                    new Domain.Student.Entities.InstructionalMethod("1df164eb-8178-4321-a9f7-24f12d3991d8", "04", "04", false)
                };
                stuRefDataRepoMock.Setup(repo => repo.GetInstructionalMethodsAsync()).ReturnsAsync(instrMethodEntities);
                //schedule repeats
                var scheduleRepeats = new List<Domain.Base.Entities.ScheduleRepeat>() 
                { 
                    new Domain.Base.Entities.ScheduleRepeat("W", "Weekly", "7", FrequencyType.Weekly),                
                    new Domain.Base.Entities.ScheduleRepeat("D", "Daily", "1", FrequencyType.Daily),                
                    new Domain.Base.Entities.ScheduleRepeat("M", "Monthly", "30", FrequencyType.Monthly),                
                    new Domain.Base.Entities.ScheduleRepeat("Y", "Yearly", "365", FrequencyType.Yearly)
                };
                referenceDataRepoMock.Setup(repo => repo.ScheduleRepeats).Returns(scheduleRepeats);
                //Rooms entities
                roomEntities = new List<Domain.Base.Entities.Room>
                {
                    new Room("2ae6e009-40ca-4ac0-bb41-c123f7c344e3", "COE*Room 1", "COE")
                    {
                        Capacity = 50,
                        RoomType = "110",
                        Name = "Room 1"
                    },
                    new Room("8c92e963-5f05-45a2-8484-d9ad21e6ab47", "COE*0110", "CEE")
                    {
                        Capacity = 100,
                        RoomType = "111",
                        Name = "Room 2"
                    },
                    new Room("8fdbaec7-4198-4348-b95a-a48a357e67f5", "COE*0120", "CDF")
                    {
                        Capacity = 20,
                        RoomType = "111",
                        Name = "Room 13"
                    },
                    new Room("327a6856-0230-4a6d-82ed-5c99dc1b1862", "COE*0121", "CSD")
                    {
                        Capacity = 50,
                        RoomType = "111",
                        Name = "Room 112"
                    },
                    new Room("cc9aa34c-db5e-46dc-9e5b-ba3f4b2557a8", "EIN*0121", "BSF")
                    {
                        Capacity = 30,
                        RoomType = "111",
                        Name = "Room BSF"                    
                    }
                };
                roomRepoMock.Setup(repo => repo.RoomsAsync()).ReturnsAsync(roomEntities);
                //Person
                person = new Domain.Base.Entities.Person("1", "Brown")
                {
                    Guid = "96cf912f-5e87-4099-96bf-73baac4c7715",
                    Prefix = "Mr.",
                    FirstName = "Ricky",
                    MiddleName = "Lee",
                    Suffix = "Jr.",
                    Nickname = "Rick",
                    BirthDate = new DateTime(1930, 1, 1),
                    DeceasedDate = new DateTime(2014, 5, 12),
                    GovernmentId = "111-11-1111",
                    MaritalStatusCode = "M",
                    EthnicCodes = new List<string> { "H" },
                    RaceCodes = new List<string> { "AS" }
                };
                person.AddEmailAddress(new EmailAddress("xyz@xmail.com", "COL"));
                person.AddPersonAlt(new PersonAlt("1", Domain.Base.Entities.PersonAlt.ElevatePersonAltType.ToString()));
            
                personRepoMock.Setup(repo => repo.GetPersonByGuidNonCachedAsync(It.IsAny<string>())).ReturnsAsync(person);
                // Mock the reference repository for prefix
                referenceDataRepoMock.Setup(repo => repo.Prefixes).Returns(new List<Prefix>()
                { 
                    new Prefix("MR","Mr","Mr."),
                    new Prefix("MS","Ms","Ms."),
                    new Prefix("JR","Jr","Jr."),
                    new Prefix("SR","Sr","Sr.")
                });

                // Mock the reference repository for suffix
                referenceDataRepoMock.Setup(repo => repo.Suffixes).Returns(new List<Suffix>()
                { 
                    new Suffix("JR","Jr","Jr."),
                    new Suffix("SR","Sr","Sr.")
                });

                //Acad Periods
                acadPeriodsEntities = new List<Domain.Student.Entities.AcademicPeriod>() 
                { 
                    new Domain.Student.Entities.AcademicPeriod("8d3fcd4d-cec8-4405-90eb-948392bd0a7e", "2012/FA", "Fall 2014", DateTime.Today.AddDays(-60), DateTime.Today.AddDays(10), DateTime.Today.Year, 4, "Spring", "5f7e7071-5aef-4d22-891f-86ab472a9f15", 
                        "edcfd1ee-4adf-46bc-8b87-8853ae49dbeb", null) 
                };
                termRepoMock.Setup(repo => repo.GetAcademicPeriods(It.IsAny<IEnumerable<Term>>())).Returns(acadPeriodsEntities);

                academicLevelEntities = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(academicLevelEntities);

                locationEntities = new TestLocationRepository().Get();
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns(locationEntities);

                departmentEntities = new TestDepartmentRepository().Get().ToList();
                referenceDataRepoMock.Setup(repo => repo.DepartmentsAsync()).ReturnsAsync(departmentEntities);

                instructionalPlatformEntities = new List<InstructionalPlatform>
                {
                    new InstructionalPlatform("840e72f0-57b9-42a2-ae88-df3c2262fbbc", "CE", "Continuing Education"),
                    new InstructionalPlatform("e986b8a5-25f3-4aa0-bd0e-90982865e749", "D", "Institutional"),
                    new InstructionalPlatform("b5cc288b-8692-474e-91be-bdc55778e2f5", "TR", "Transfer")
                };
                referenceDataRepoMock.Setup(repo => repo.GetInstructionalPlatformsAsync(It.IsAny<bool>())).ReturnsAsync(instructionalPlatformEntities);

                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate(It.IsAny<string>())).ReturnsAsync("2016/01/01");
                sectionRepoMock.Setup(repo => repo.GetCourseIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
                sectionRepoMock.Setup(repo => repo.ConvertStatusToStatusCodeAsync(It.IsAny<string>())).ReturnsAsync("Open");

                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", "I", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
            }
        }

        [TestClass]
        public class SectionCoordinationServiceTests_V6 : CurrentUserSetup
        {
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private Mock<ISectionRepository> sectionRepoMock;
            private Mock<IStudentRepository> studentRepoMock;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private Mock<ICourseRepository> courseRepoMock;
            private Mock<ITermRepository> termRepoMock;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private Mock<IConfigurationRepository> configRepoMock;
            private Mock<IPersonRepository> personRepoMock;
            private Mock<IRoomRepository> roomRepoMock;
            private Mock<IEventRepository> eventRepoMock;
            private Mock<IRoleRepository> roleRepoMock;
            private Mock<ILogger> loggerMock;
            private ICurrentUserFactory currentUserFactory;


            private SectionCoordinationService sectionCoordinationService;
            Tuple<IEnumerable<Domain.Student.Entities.Section>, int> sectionMaxEntitiesTuple;
            IEnumerable<Domain.Student.Entities.Section> sectionEntities;

            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Course> courseEntities;
            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Subject> subjectEntities;
            IEnumerable<Domain.Student.Entities.CreditCategory> creditCategoryEntities;
            IEnumerable<Domain.Student.Entities.GradeScheme> gradeSchemeEntities;
            IEnumerable<Domain.Student.Entities.CourseLevel> courseLevelEntities;
            IEnumerable<Domain.Student.Entities.AcademicPeriod> acadPeriodsEntities;
            IEnumerable<Domain.Student.Entities.AcademicLevel> academicLevelEntities;
            IEnumerable<Domain.Base.Entities.InstructionalPlatform> instructionalPlatformEntities;
            IEnumerable<Domain.Base.Entities.Location> locationEntities;
            IEnumerable<Domain.Base.Entities.Department> departmentEntities;
            IEnumerable<Domain.Student.Entities.InstructionalMethod> instrMethodEntities;
            IEnumerable<Domain.Base.Entities.Room> roomEntities;

            Domain.Base.Entities.Person person;
            private Domain.Student.Entities.SectionMeeting meeting1;


            [TestInitialize]
            public void Initialize()
            {
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                sectionRepoMock = new Mock<ISectionRepository>();
                courseRepoMock = new Mock<ICourseRepository>();
                studentRepoMock = new Mock<IStudentRepository>();
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                termRepoMock = new Mock<ITermRepository>();
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                configRepoMock = new Mock<IConfigurationRepository>();
                personRepoMock = new Mock<IPersonRepository>();
                roomRepoMock = new Mock<IRoomRepository>();
                eventRepoMock = new Mock<IEventRepository>();
                roleRepoMock = new Mock<IRoleRepository>();
                loggerMock = new Mock<ILogger>();

                currentUserFactory = new CurrentUserSetup.StudentUserFactory();
                BuildData();

                sectionCoordinationService = new SectionCoordinationService(adapterRegistryMock.Object, sectionRepoMock.Object, courseRepoMock.Object, studentRepoMock.Object,
                    stuRefDataRepoMock.Object, referenceDataRepoMock.Object, termRepoMock.Object, studentConfigRepoMock.Object, configRepoMock.Object, personRepoMock.Object,
                    roomRepoMock.Object, eventRepoMock.Object, currentUserFactory, roleRepoMock.Object, loggerMock.Object);

            }

            [TestCleanup]
            public void Cleanup()
            {
                adapterRegistryMock = null;
                sectionRepoMock = null;
                courseRepoMock = null;
                studentRepoMock = null;
                stuRefDataRepoMock = null;
                referenceDataRepoMock = null;
                termRepoMock = null;
                studentConfigRepoMock = null;
                configRepoMock = null;
                personRepoMock = null;
                roomRepoMock = null;
                eventRepoMock = null;
                roleRepoMock = null;
                sectionCoordinationService = null;
                courseEntities = null;
                subjectEntities = null;
                creditCategoryEntities = null;
                gradeSchemeEntities = null;
                courseLevelEntities = null;
                acadPeriodsEntities = null;
                academicLevelEntities = null;
                instructionalPlatformEntities = null;
                locationEntities = null;
                departmentEntities = null;
                instrMethodEntities = null;
                roomEntities = null;
                person = null;
                meeting1 = null;
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V6_GetSectionsMaximum2Async_ValidateAllFilters()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);

                var results = await sectionCoordinationService.GetSectionsMaximum2Async(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");

                Assert.IsNotNull(results);
                Assert.AreEqual(1, results.Item1.Count());
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V6_GetSectionMaximumByGuid2Async_CE()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", "C", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid2Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V6_GetSectionMaximumByGuid2Async_Transfer()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", "T", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid2Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V6_GetSectionMaximumByGuid2Async_Exchange()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", "E", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid2Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V6_GetSectionMaximumByGuid2Async_Other()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", "O", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid2Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V6_GetSectionMaximumByGuid2Async_NoCredit()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", "N", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid2Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V6_GetSectionMaximumByGuid2Async_WithId_CreditTypeCode_Blank()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", " ", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid2Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V6_GetSectionMaximumByGuid2Async_WithId_No_CEUS()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, null, "Title 1", "I", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid2Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V6_GetSectionMaximumByGuid2Async_WithId_Freq_Daily()
            {
                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LG", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LG", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting1.Guid = "1f1485cb-2705-4b59-9f51-7aa85c79b377";
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "COE*Room 1";
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 0, "1", "", "", "", "", "", "", "")).ReturnsAsync(tuple);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid2Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
                Assert.AreEqual(Dtos.FrequencyType2.Daily, result.InstructionalEvents.ToList()[0].Recurrence.RepeatRule.Type);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V6_GetSectionMaximumByGuid2Async_WithId_Freq_Monthly_WithDays()
            {
                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LG", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LG", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting1.Days = new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Wednesday };
                meeting1.Guid = "1f1485cb-2705-4b59-9f51-7aa85c79b377";
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "COE*Room 1";
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 0, "1", "", "", "", "", "", "", "")).ReturnsAsync(tuple);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid2Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
                Assert.AreEqual(Dtos.FrequencyType2.Monthly, result.InstructionalEvents.ToList()[0].Recurrence.RepeatRule.Type);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V6_GetSectionMaximumByGuid2Async_WithId_Freq_Monthly_WithoutDays()
            {
                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LG", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LG", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting1.Guid = "1f1485cb-2705-4b59-9f51-7aa85c79b377";
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "COE*Room 1";
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 0, "1", "", "", "", "", "", "", "")).ReturnsAsync(tuple);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid2Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
                Assert.AreEqual(Dtos.FrequencyType2.Monthly, result.InstructionalEvents.ToList()[0].Recurrence.RepeatRule.Type);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V6_GetSectionMaximumByGuid2Async_WithId_Freq_Yearly()
            {
                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LG", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LG", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting1.Guid = "1f1485cb-2705-4b59-9f51-7aa85c79b377";
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "COE*Room 1";
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 0, "1", "", "", "", "", "", "", "")).ReturnsAsync(tuple);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid2Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
                Assert.AreEqual(Dtos.FrequencyType2.Yearly, result.InstructionalEvents.ToList()[0].Recurrence.RepeatRule.Type);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V6_GetSectionMaximumByGuid2Async_WithId()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid2Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            #region All Exceptions

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionCoordinationServiceTests_V6_GetSectionsMaximum2Async_NullSectionGuid_ArgumentException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);
                sectionRepoMock.Setup(repo => repo.GetSectionGuidFromIdAsync(It.IsAny<string>())).ThrowsAsync(new ArgumentException());

                var results = await sectionCoordinationService.GetSectionsMaximum2Async(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionCoordinationServiceTests_V6_GetSectionsMaximum2Async_NullInstrMethodGuid_ArgumentException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);
                stuRefDataRepoMock.Setup(repo => repo.GetInstructionalMethodsAsync()).ThrowsAsync(new ArgumentException());

                var results = await sectionCoordinationService.GetSectionsMaximum2Async(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionCoordinationServiceTests_V6_GetSectionsMaximum2Async_RepeatCodeNull_ArgumentException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);

                meeting1.Frequency = "abc";

                var results = await sectionCoordinationService.GetSectionsMaximum2Async(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionCoordinationServiceTests_V6_GetSectionsMaximum2Async_GetPersonGuidFromIdAsync_Error_ArgumentException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);

                personRepoMock.Setup(repo => repo.GetPersonGuidFromIdAsync(It.IsAny<string>())).ThrowsAsync(new ArgumentException());

                var results = await sectionCoordinationService.GetSectionsMaximum2Async(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionCoordinationServiceTests_V6_GetSectionsMaximum2Async_ArgumentException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);
                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate(It.IsAny<string>())).ThrowsAsync(new ArgumentException());

                var results = await sectionCoordinationService.GetSectionsMaximum2Async(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionCoordinationServiceTests_V6_GetSectionsMaximum2Async_NullInstrPlatList_ArgumentNullException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);
                referenceDataRepoMock.Setup(repo => repo.GetInstructionalPlatformsAsync(It.IsAny<bool>())).ReturnsAsync(null);

                var results = await sectionCoordinationService.GetSectionsMaximum2Async(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
            }

            [TestMethod]
            [ExpectedException(typeof(RepositoryException))]
            public async Task SectionCoordinationServiceTests_V6_GetSectionsMaximum2Async_EmptyCourseGuid_RepositoryException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(string.Empty);

                var results = await sectionCoordinationService.GetSectionsMaximum2Async(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
            }

            [TestMethod]
            [ExpectedException(typeof(RepositoryException))]
            public async Task SectionCoordinationServiceTests_V6_GetSectionsMaximum2Async_NullCourse_RepositoryException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);
                courseRepoMock.Setup(repo => repo.GetCourseByGuidAsync(It.IsAny<string>())).ReturnsAsync(null);

                var results = await sectionCoordinationService.GetSectionsMaximum2Async(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionCoordinationServiceTests_V6_GetSectionMaximumByGuid2Async_WithNullId_ArgumentNullException()
            {
                var result = await sectionCoordinationService.GetSectionMaximumByGuid2Async("");
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionCoordinationServiceTests_V6_GetSectionMaximumByGuid2Async_WithNullEntity_KeyNotFoundException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(null);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid2Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");
            }
            
            #endregion

            private void BuildData()
            {
                //Course entity
                courseEntities = new TestCourseRepository().GetAsync().Result.Take(41);
                var courseEntity = courseEntities.FirstOrDefault(i => i.Id.Equals("180"));
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(courseEntity.Guid);
                courseRepoMock.Setup(repo => repo.GetCourseByGuidAsync(courseEntity.Guid)).ReturnsAsync(courseEntity);
                //Subject entity
                subjectEntities = new TestSubjectRepository().Get();
                stuRefDataRepoMock.Setup(repo => repo.GetSubjectsAsync()).ReturnsAsync(subjectEntities);
                var subjectEntity = subjectEntities.FirstOrDefault(s => s.Code.Equals(courseEntity.SubjectCode));
                //Credit Categories
                creditCategoryEntities = new List<Domain.Student.Entities.CreditCategory>()
                {
                    new Domain.Student.Entities.CreditCategory("9C3B805D-CFE6-483B-86C3-4C20562F8C15", "I", "Institutional", Domain.Student.Entities.CreditType.Institutional),
                    new Domain.Student.Entities.CreditCategory("73244057-D1EC-4094-A0B7-DE602533E3A6", "C", "Continuing Education", Domain.Student.Entities.CreditType.ContinuingEducation),
                    new Domain.Student.Entities.CreditCategory("73244057-D1EC-4094-A0B7-DE602533E3A6", "E", "Exchange", Domain.Student.Entities.CreditType.Exchange),
                    new Domain.Student.Entities.CreditCategory("73244057-D1EC-4094-A0B7-DE602533E3A6", "O", "Other", Domain.Student.Entities.CreditType.Other),
                    new Domain.Student.Entities.CreditCategory("73244057-D1EC-4094-A0B7-DE602533E3A6", "N", "Continuing Education", Domain.Student.Entities.CreditType.None),
                    new Domain.Student.Entities.CreditCategory("1df164eb-8178-4321-a9f7-24f12d3991d8", "T", "Transfer Credit", Domain.Student.Entities.CreditType.Transfer)
                };
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(creditCategoryEntities);
                //Grade Schemes
                gradeSchemeEntities = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(gradeSchemeEntities);
                //Course Levels
                courseLevelEntities = new List<Domain.Student.Entities.CourseLevel>()
                {
                    new Domain.Student.Entities.CourseLevel("19f6e2cd-1e5f-4485-9b27-60d4f4e4b1ff", "100", "First Yr"),
                    new Domain.Student.Entities.CourseLevel("73244057-D1EC-4094-A0B7-DE602533E3A6", "200", "Second Year"),
                    new Domain.Student.Entities.CourseLevel("1df164eb-8178-4321-a9f7-24f12d3991d8", "300", "Third Year"),
                    new Domain.Student.Entities.CourseLevel("4aa187ae-6d22-4b10-a2e2-1304ebc18176", "400", "Third Year"),
                    new Domain.Student.Entities.CourseLevel("d9f42a0f-39de-44bc-87af-517619141bde", "500", "Third Year")
                };
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(courseLevelEntities);

                //IEnumerable<SectionMeeting>
                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LG", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LG", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting1.Guid = "1f1485cb-2705-4b59-9f51-7aa85c79b377";
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "COE*Room 1";
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 0, "1", "", "", "", "", "", "", "")).ReturnsAsync(tuple);
                sectionRepoMock.Setup(repo => repo.GetSectionGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("ad7655d0-77e3-4f8a-a07c-5c86f6a6f86a");

                //Instructor Id
                personRepoMock.Setup(repo => repo.GetPersonGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("ebf585ad-cc1f-478f-a7a3-aefae87f873a");
                //Instructional Methods
                instrMethodEntities = new List<Domain.Student.Entities.InstructionalMethod>()
                {
                    new Domain.Student.Entities.InstructionalMethod("9C3B805D-CFE6-483B-86C3-4C20562F8C15", "LG", "LG", false),
                    new Domain.Student.Entities.InstructionalMethod("73244057-D1EC-4094-A0B7-DE602533E3A6", "30", "30", true),
                    new Domain.Student.Entities.InstructionalMethod("1df164eb-8178-4321-a9f7-24f12d3991d8", "04", "04", false)
                };
                stuRefDataRepoMock.Setup(repo => repo.GetInstructionalMethodsAsync()).ReturnsAsync(instrMethodEntities);
                //schedule repeats
                var scheduleRepeats = new List<Domain.Base.Entities.ScheduleRepeat>() 
                { 
                    new Domain.Base.Entities.ScheduleRepeat("W", "Weekly", "7", FrequencyType.Weekly),                
                    new Domain.Base.Entities.ScheduleRepeat("D", "Daily", "1", FrequencyType.Daily),                
                    new Domain.Base.Entities.ScheduleRepeat("M", "Monthly", "30", FrequencyType.Monthly),                
                    new Domain.Base.Entities.ScheduleRepeat("Y", "Yearly", "365", FrequencyType.Yearly)
                };
                referenceDataRepoMock.Setup(repo => repo.ScheduleRepeats).Returns(scheduleRepeats);
                //Rooms entities
                roomEntities = new List<Domain.Base.Entities.Room>
                {
                    new Room("2ae6e009-40ca-4ac0-bb41-c123f7c344e3", "COE*Room 1", "COE")
                    {
                        Capacity = 50,
                        RoomType = "110",
                        Name = "Room 1"
                    },
                    new Room("8c92e963-5f05-45a2-8484-d9ad21e6ab47", "COE*0110", "CEE")
                    {
                        Capacity = 100,
                        RoomType = "111",
                        Name = "Room 2"
                    },
                    new Room("8fdbaec7-4198-4348-b95a-a48a357e67f5", "COE*0120", "CDF")
                    {
                        Capacity = 20,
                        RoomType = "111",
                        Name = "Room 13"
                    },
                    new Room("327a6856-0230-4a6d-82ed-5c99dc1b1862", "COE*0121", "CSD")
                    {
                        Capacity = 50,
                        RoomType = "111",
                        Name = "Room 112"
                    },
                    new Room("cc9aa34c-db5e-46dc-9e5b-ba3f4b2557a8", "EIN*0121", "BSF")
                    {
                        Capacity = 30,
                        RoomType = "111",
                        Name = "Room BSF"                    
                    }
                };
                roomRepoMock.Setup(repo => repo.RoomsAsync()).ReturnsAsync(roomEntities);
                //Person
                person = new Domain.Base.Entities.Person("1", "Brown")
                {
                    Guid = "96cf912f-5e87-4099-96bf-73baac4c7715",
                    Prefix = "Mr.",
                    FirstName = "Ricky",
                    MiddleName = "Lee",
                    Suffix = "Jr.",
                    Nickname = "Rick",
                    BirthDate = new DateTime(1930, 1, 1),
                    DeceasedDate = new DateTime(2014, 5, 12),
                    GovernmentId = "111-11-1111",
                    MaritalStatusCode = "M",
                    EthnicCodes = new List<string> { "H" },
                    RaceCodes = new List<string> { "AS" }
                };
                person.AddEmailAddress(new EmailAddress("xyz@xmail.com", "COL"));
                person.AddPersonAlt(new PersonAlt("1", Domain.Base.Entities.PersonAlt.ElevatePersonAltType.ToString()));

                personRepoMock.Setup(repo => repo.GetPersonByGuidNonCachedAsync(It.IsAny<string>())).ReturnsAsync(person);
                // Mock the reference repository for prefix
                referenceDataRepoMock.Setup(repo => repo.Prefixes).Returns(new List<Prefix>()
                { 
                    new Prefix("MR","Mr","Mr."),
                    new Prefix("MS","Ms","Ms."),
                    new Prefix("JR","Jr","Jr."),
                    new Prefix("SR","Sr","Sr.")
                });

                // Mock the reference repository for suffix
                referenceDataRepoMock.Setup(repo => repo.Suffixes).Returns(new List<Suffix>()
                { 
                    new Suffix("JR","Jr","Jr."),
                    new Suffix("SR","Sr","Sr.")
                });

                //Acad Periods
                acadPeriodsEntities = new List<Domain.Student.Entities.AcademicPeriod>() 
                { 
                    new Domain.Student.Entities.AcademicPeriod("8d3fcd4d-cec8-4405-90eb-948392bd0a7e", "2012/FA", "Fall 2014", DateTime.Today.AddDays(-60), DateTime.Today.AddDays(10), DateTime.Today.Year, 4, "Spring", "5f7e7071-5aef-4d22-891f-86ab472a9f15", 
                        "edcfd1ee-4adf-46bc-8b87-8853ae49dbeb", null) 
                };
                termRepoMock.Setup(repo => repo.GetAcademicPeriods(It.IsAny<IEnumerable<Term>>())).Returns(acadPeriodsEntities);

                academicLevelEntities = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(academicLevelEntities);

                locationEntities = new TestLocationRepository().Get();
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns(locationEntities);

                departmentEntities = new TestDepartmentRepository().Get().ToList();
                referenceDataRepoMock.Setup(repo => repo.DepartmentsAsync()).ReturnsAsync(departmentEntities);

                instructionalPlatformEntities = new List<InstructionalPlatform>
                {
                    new InstructionalPlatform("840e72f0-57b9-42a2-ae88-df3c2262fbbc", "CE", "Continuing Education"),
                    new InstructionalPlatform("e986b8a5-25f3-4aa0-bd0e-90982865e749", "D", "Institutional"),
                    new InstructionalPlatform("b5cc288b-8692-474e-91be-bdc55778e2f5", "TR", "Transfer")
                };
                referenceDataRepoMock.Setup(repo => repo.GetInstructionalPlatformsAsync(It.IsAny<bool>())).ReturnsAsync(instructionalPlatformEntities);

                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate(It.IsAny<string>())).ReturnsAsync("2016/01/01");
                sectionRepoMock.Setup(repo => repo.GetCourseIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
                sectionRepoMock.Setup(repo => repo.ConvertStatusToStatusCodeAsync(It.IsAny<string>())).ReturnsAsync("Open");

                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", "I", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
            }
        }

        [TestClass]
        public class Sections_Get_V3_V4 : CurrentUserSetup
        {
            private SectionCoordinationService sectionCoordinationService;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepo;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private IStudentReferenceDataRepository studentReferenceDataRepo;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private Mock<ICourseRepository> courseRepoMock;
            private ICourseRepository courseRepo;
            private Mock<ITermRepository> termRepoMock;
            private ITermRepository termRepo;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private IStudentConfigurationRepository studentConfigRepo;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IRoomRepository> roomRepoMock;
            private IRoomRepository roomRepo;
            private Mock<IEventRepository> eventRepoMock;
            private IEventRepository eventRepo;
            // private Mock<IAcademicPeriodRepository> academicPeriodRepoMock;
            // private IAcademicPeriodRepository academicPeriodRepo;
            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;

            private Domain.Student.Entities.Section section1;
            private Domain.Student.Entities.Section section2;
            private Domain.Student.Entities.Section section3;
            private Domain.Student.Entities.Section section4;
            private Domain.Student.Entities.SectionMeeting meeting1;
            private Domain.Student.Entities.SectionMeeting meeting2;
            private Domain.Student.Entities.SectionMeeting meeting3;
            private Domain.Student.Entities.SectionMeeting meeting4;

            private List<Domain.Student.Entities.Section> section;
            private List<OfferingDepartment> dpts = new List<OfferingDepartment>() { new OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>() { "Whatever" };
            private List<SectionStatusItem> statuses = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses2 = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Inactive, "I", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses3 = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Cancelled, "C", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses4 = new List<SectionStatusItem>() { new SectionStatusItem(new SectionStatus(), "N/A", DateTime.Today.AddDays(-60)) };
            private List<CreditCategory> creditCategories = new List<CreditCategory>() { new CreditCategory(Guid.NewGuid().ToString(), "IN", "Regular Credit", CreditType.Institutional) };
            private List<Domain.Student.Entities.AcademicLevel> academicLevels = new List<Domain.Student.Entities.AcademicLevel>() { new Domain.Student.Entities.AcademicLevel(Guid.NewGuid().ToString(), "UG", "Undergraduate") };
            private List<GradeScheme> gradeSchemes = new List<GradeScheme>() { new GradeScheme(Guid.NewGuid().ToString(), "Pass/Fail", "Pass/Fail grade") };
            private List<Domain.Student.Entities.CourseLevel> courseLevels = new List<Domain.Student.Entities.CourseLevel>() { new Domain.Student.Entities.CourseLevel(Guid.NewGuid().ToString(), "100", "Introduction to terms, concepts, and techniques.") };
            private List<Department> departments = new List<Department>() { new Department(Guid.NewGuid().ToString(), "PHYS", "Physics", true) };
            private List<Location> locations = new List<Location>() { new Location(Guid.NewGuid().ToString(), "MD", "Maryland") };
            private List<AcademicPeriod> academicPeriods = new List<AcademicPeriod>() { new AcademicPeriod(Guid.NewGuid().ToString(), "2012/FA", "Fall 2014", DateTime.Today.AddDays(-60), DateTime.Today, 2015, 4, "Spring", Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), null) };
            private List<Domain.Base.Entities.InstructionalPlatform> instructionalPlatforms = new List<Domain.Base.Entities.InstructionalPlatform>() { new Domain.Base.Entities.InstructionalPlatform(Guid.NewGuid().ToString(), "Ellucian", "IP") };

            private string meeting1Guid = Guid.NewGuid().ToString();
            private string sectionGuid = Guid.NewGuid().ToString();
            private string roomGuid = Guid.NewGuid().ToString();
            private string courseGuid = Guid.NewGuid().ToString();

            [TestInitialize]
            public void Initialize()
            {
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepo = sectionRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                roomRepoMock = new Mock<IRoomRepository>();
                roomRepo = roomRepoMock.Object;
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                studentReferenceDataRepo = stuRefDataRepoMock.Object;
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                referenceDataRepo = referenceDataRepoMock.Object;
                courseRepoMock = new Mock<ICourseRepository>();
                courseRepo = courseRepoMock.Object;
                termRepoMock = new Mock<ITermRepository>();
                termRepo = termRepoMock.Object;
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                studentConfigRepo = studentConfigRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
                eventRepoMock = new Mock<IEventRepository>();
                eventRepo = eventRepoMock.Object;

                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;

                // "STU1" is the current user
                currentUserFactory = new CurrentUserSetup.StudentUserFactory();

                logger = new Mock<ILogger>().Object;

                // Mock the section repo response
                section1 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section1.Guid = sectionGuid;
                section1.TermId = "2012/FA";
                section1.EndDate = new DateTime(2012, 12, 21);
                section1.LearningProvider = "Ellucian";

                section2 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses2, true);
                section2.Guid = Guid.NewGuid().ToString();
                section2.TermId = "2012/FA";

                section3 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses3, true);
                section3.Guid = Guid.NewGuid().ToString();
                section3.TermId = "2011/FA";
                section3.EndDate = new DateTime(2011, 12, 21);

                section4 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC4", "1119", "04", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses4, true);
                section4.Guid = Guid.NewGuid().ToString();
                section4.TermId = "2011/FA";
                section4.EndDate = new DateTime(2011, 12, 21);

                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LEC", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting1.Guid = meeting1Guid;
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "DAN*102";
                section1.AddSectionMeeting(meeting1);

                meeting2 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting2.Guid = meeting1Guid;

                meeting3 = new Domain.Student.Entities.SectionMeeting("456", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting3.Guid = meeting1Guid;

                meeting4 = new Domain.Student.Entities.SectionMeeting("789", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting4.Guid = meeting1Guid;

                var sections = new List<Domain.Student.Entities.Section>();
                //var section = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                sections.Add(section1);
                sections.Add(section2);
                sections.Add(section3);
                sections.Add(section4);

                var rooms = new List<Domain.Base.Entities.Room>();
                var room = new Domain.Base.Entities.Room(roomGuid, "DAN*102", "Danville Hall Room 102");
                rooms.Add(room);

                var scheduleRepeats = new List<Domain.Base.Entities.ScheduleRepeat>();
                var scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("W", "Weekly", "7", FrequencyType.Weekly);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("D", "Daily", "1", FrequencyType.Daily);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("M", "Monthly", "30", FrequencyType.Monthly);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("Y", "Yearly", "365", FrequencyType.Yearly);
                scheduleRepeats.Add(scheduleRepeat);

                List<string> sec1List = new List<string>() { "SEC1" };
                List<Domain.Student.Entities.Section> sec1Resp = new List<Domain.Student.Entities.Section>() { section1 };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec1List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sec1Resp));

                List<string> sec2List = new List<string>() { "SEC2" };
                List<Domain.Student.Entities.Section> sec2Resp = new List<Domain.Student.Entities.Section>() { section2 };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec2List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sec2Resp));

                List<string> sec3List = new List<string>() { "SEC3" };
                List<Domain.Student.Entities.Section> sec3Resp = new List<Domain.Student.Entities.Section>() { section3 };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec3List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sec3Resp));

                List<string> sec4List = new List<string>() { "9999999" };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec4List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(new List<Domain.Student.Entities.Section>()));

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).ReturnsAsync(section1);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section2.Guid)).ReturnsAsync(section2);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section3.Guid)).ReturnsAsync(section3);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section4.Guid)).ReturnsAsync(section4);
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(section1.CourseId)).ReturnsAsync(section1.Title);
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(section2.CourseId)).ReturnsAsync(section2.Title);
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(section3.CourseId)).ReturnsAsync(section3.Title);
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(section4.CourseId)).ReturnsAsync(section4.Title);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(creditCategories);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(academicLevels);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(gradeSchemes);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(courseLevels);
                referenceDataRepoMock.Setup(repo => repo.DepartmentsAsync()).ReturnsAsync(departments);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns(locations);
                termRepoMock.Setup(repo => repo.GetAcademicPeriods(It.IsAny<IEnumerable<Domain.Student.Entities.Term>>())).Returns(academicPeriods);
                referenceDataRepoMock.Setup(repo => repo.GetInstructionalPlatformsAsync(It.IsAny<bool>())).ReturnsAsync(instructionalPlatforms);

                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(new Tuple<IEnumerable<Domain.Student.Entities.Section>, int>(sections, 100));

                //roomRepoMock.Setup(repo => repo.RoomsAsync()).ReturnsAsync(rooms);
                //referenceDataRepoMock.Setup(repo => repo.ScheduleRepeats).Returns(scheduleRepeats);

                // Mock the section service
                sectionCoordinationService = new SectionCoordinationService(adapterRegistry, sectionRepo, courseRepo, studentRepo, studentReferenceDataRepo,
                    referenceDataRepo, termRepo, studentConfigRepo, configRepo, personRepo, roomRepo, eventRepo, currentUserFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionRepo = null;
                studentRepo = null;
                roleRepo = null;
                adapterRegistry = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_SectionEmptyId_V3()
            {
                var r = await sectionCoordinationService.GetSectionByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_SectionEmptyId_V4()
            {
                var r = await sectionCoordinationService.GetSection2ByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionService_InvalidSectionId_V3()
            {
                var r = await sectionCoordinationService.GetSectionByGuidAsync("9999999");
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionService_InvalidSectionId_V4()
            {
                var r = await sectionCoordinationService.GetSection2ByGuidAsync("9999999");
            }

            [TestMethod]
            public async Task SectionService_GetByGuid_V3()
            {
                var res = await sectionCoordinationService.GetSectionByGuidAsync(section1.Guid);
                Assert.AreEqual(section1.Guid, res.Guid);
            }

            [TestMethod]
            public async Task SectionService_GetByGuid_V4()
            {
                var res = await sectionCoordinationService.GetSection2ByGuidAsync(section1.Guid);
                Assert.AreEqual(section1.Guid, res.Id);
            }

            [TestMethod]
            public async Task SectionService_GetAll_EmptyArgument_V4()
            {
                var res = await sectionCoordinationService.GetSections2Async(0, 5);
                var sec = res.Item1.Where(r => r.Id == section1.Guid).FirstOrDefault();
                Assert.AreEqual(section1.Guid, sec.Id);
            }

            [TestMethod]
            public async Task SectionService_GetAll_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate("2014-09-01T00:00:00+00:00")).ReturnsAsync("2014-09-01T00:00:00+00:00");
                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate("2015-08-31T23:59:59+00:00")).ReturnsAsync("2015-08-31T23:59:59+00:00");
                sectionRepoMock.Setup(repo => repo.GetCourseIdFromGuidAsync(section1.Guid)).ReturnsAsync(section1.CourseId);
                sectionRepoMock.Setup(repo => repo.ConvertStatusToStatusCodeAsync("open")).ReturnsAsync("'A'");
                var res = await sectionCoordinationService.GetSections2Async(0, 5, "",
                    "2014-09-01T00:00:00+00:00", "2015-08-31T23:59:59+00:00", "", "", "", "", "",
                    section1.Guid, "", "open", "");
                var sec = res.Item1.Where(r => r.Id == section1.Guid).FirstOrDefault();
                Assert.AreEqual(section1.Guid, sec.Id);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_GetAll_WithInvalidDateFormat_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate(It.IsAny<string>())).Throws<Exception>();
                var res = await sectionCoordinationService.GetSections2Async(0, 5, "",
                    "2014-09-01T00:00:00+00:00", "2015-08-31T23:59:59+00:00", "", "", "", "", "",
                    section1.Guid, "", "open", "");
            }

            [TestMethod]
            public async Task SectionService_GetByGuidWithInactiveStatus_V3()
            {
                var res = await sectionCoordinationService.GetSectionByGuidAsync(section2.Guid);
                Assert.AreEqual(section2.Guid, res.Guid);
            }

            [TestMethod]
            public async Task SectionService_GetByGuidWithInactiveStatus_V4()
            {
                var res = await sectionCoordinationService.GetSection2ByGuidAsync(section2.Guid);
                Assert.AreEqual(section2.Guid, res.Id);
            }

            [TestMethod]
            public async Task SectionService_GetByGuidWithCancelledStatus_V3()
            {
                var res = await sectionCoordinationService.GetSectionByGuidAsync(section3.Guid);
                Assert.AreEqual(section3.Guid, res.Guid);
            }

            [TestMethod]
            public async Task SectionService_GetByGuidWithCancelledStatus_V4()
            {
                var res = await sectionCoordinationService.GetSection2ByGuidAsync(section3.Guid);
                Assert.AreEqual(section3.Guid, res.Id);
            }

            [TestMethod]
            public async Task SectionService_GetByGuidWithNoStatus_V3()
            {
                var res = await sectionCoordinationService.GetSectionByGuidAsync(section4.Guid);
                Assert.AreEqual(section4.Guid, res.Guid);
            }

            [TestMethod]
            public async Task SectionService_GetByGuidWithNoStatus_V4()
            {
                var res = await sectionCoordinationService.GetSection2ByGuidAsync(section4.Guid);
                Assert.AreEqual(section4.Guid, res.Id);
            }
        }

        [TestClass]
        public class Sections_Post_V3_V4 : CurrentUserSetup
        {
            private SectionCoordinationService sectionCoordinationService;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepo;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private IStudentReferenceDataRepository studentReferenceDataRepo;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private Mock<ICourseRepository> courseRepoMock;
            private ICourseRepository courseRepo;
            private Mock<ITermRepository> termRepoMock;
            private ITermRepository termRepo;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private IStudentConfigurationRepository studentConfigRepo;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IRoomRepository> roomRepoMock;
            private IRoomRepository roomRepo;
            private Mock<IEventRepository> eventRepoMock;
            private IEventRepository eventRepo;


            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;

            private Domain.Student.Entities.Section section1;
            private Domain.Student.Entities.Section section2;
            private Domain.Student.Entities.Section section3;
            private Domain.Student.Entities.SectionMeeting meeting1;
            private Domain.Student.Entities.SectionMeeting meeting2;
            private Domain.Student.Entities.SectionMeeting meeting3;
            private Domain.Student.Entities.SectionMeeting meeting4;

            private List<OfferingDepartment> dpts = new List<OfferingDepartment>() { new OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>() { "Whatever" };
            private List<SectionStatusItem> statuses = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses2 = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Inactive, "I", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses3 = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Cancelled, "C", DateTime.Today.AddDays(-60)) };
            private IEnumerable<Domain.Student.Entities.Course> allCourses = new TestCourseRepository().GetAsync().Result;

            private List<CreditCategory> creditCategories = new List<CreditCategory>() { new CreditCategory(Guid.NewGuid().ToString(), "RC", "Regular Credit", CreditType.Institutional),
                new CreditCategory(Guid.NewGuid().ToString(), "RC", "Regular Credit", CreditType.Transfer), new CreditCategory(Guid.NewGuid().ToString(), "RC", "Regular Credit", CreditType.ContinuingEducation ) };
            private List<Domain.Student.Entities.AcademicLevel> academicLevels = new List<Domain.Student.Entities.AcademicLevel>() { new Domain.Student.Entities.AcademicLevel(Guid.NewGuid().ToString(), "UG", "Undergraduate") };
            private List<GradeScheme> gradeSchemes = new List<GradeScheme>() { new GradeScheme(Guid.NewGuid().ToString(), "Pass/Fail", "Pass/Fail grade") };
            private List<Domain.Student.Entities.CourseLevel> courseLevels = new List<Domain.Student.Entities.CourseLevel>() { new Domain.Student.Entities.CourseLevel(Guid.NewGuid().ToString(), "100", "Introduction to terms, concepts, and techniques.") };
            private List<Department> departments = new List<Department>() { new Department(Guid.NewGuid().ToString(), "PHYS", "Physics", true) };
            private List<Location> locations = new List<Location>() { new Location(Guid.NewGuid().ToString(), "MD", "Maryland") };

            private string meeting1Guid = Guid.NewGuid().ToString();
            private string instructionalMethodGuid = Guid.NewGuid().ToString();
            private string roomGuid = Guid.NewGuid().ToString();
            private Domain.Entities.Permission permissionCreateandUpdateSections;
            private Domain.Entities.Permission permissionViewAnyProjects;
            private Domain.Entities.Permission permissionViewAnyProjectsLineItems;

            [TestInitialize]
            public void Initialize()
            {
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepo = sectionRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                roomRepoMock = new Mock<IRoomRepository>();
                roomRepo = roomRepoMock.Object;
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                studentReferenceDataRepo = stuRefDataRepoMock.Object;
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                referenceDataRepo = referenceDataRepoMock.Object;
                courseRepoMock = new Mock<ICourseRepository>();
                courseRepo = courseRepoMock.Object;
                termRepoMock = new Mock<ITermRepository>();
                termRepo = termRepoMock.Object;
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                studentConfigRepo = studentConfigRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
                eventRepoMock = new Mock<IEventRepository>();
                eventRepo = eventRepoMock.Object;


                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;

                // Set Current User
                currentUserFactory = new CurrentUserSetup.ThirdPartyUserFactory();

                logger = new Mock<ILogger>().Object;

                // Mock permissions
                permissionCreateandUpdateSections = new Ellucian.Colleague.Domain.Entities.Permission(StudentPermissionCodes.CreateAndUpdateSection);
                thirdPartyRole.AddPermission(permissionCreateandUpdateSections);
                //permissionViewAnyProjects = new Ellucian.Colleague.Domain.Entities.Permission(StudentPermissionCodes.ViewAnyProjects);
                //thirdPartyRole.AddPermission(permissionViewAnyProjects);
                //permissionViewAnyProjectsLineItems = new Ellucian.Colleague.Domain.Entities.Permission(BasePermissionCodes.ViewAnyProjectsLineItems);
                //thirdPartyRole.AddPermission(permissionViewAnyProjectsLineItems);

                // Mock the section repo response
                section1 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section1.TermId = "2012/FA";
                section1.EndDate = new DateTime(2012, 12, 21);
                section1.Guid = Guid.NewGuid().ToString();

                section2 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, null, "Introduction to Art", "IN", dpts, levels, "UG", statuses2, true);
                section2.TermId = "2012/FA";
                section2.Guid = Guid.NewGuid().ToString();

                section3 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses3, true);
                section3.TermId = "2011/FA";
                section3.EndDate = new DateTime(2011, 12, 21);
                section3.Guid = Guid.NewGuid().ToString();

                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LEC", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting1.Guid = meeting1Guid;
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "DAN*102";
                section1.AddSectionMeeting(meeting1);

                meeting2 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting2.Guid = meeting1Guid;

                meeting3 = new Domain.Student.Entities.SectionMeeting("456", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting3.Guid = meeting1Guid;

                meeting4 = new Domain.Student.Entities.SectionMeeting("789", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting4.Guid = meeting1Guid;

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).ReturnsAsync(section1);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section2.Guid)).ReturnsAsync(section2);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section3.Guid)).ReturnsAsync(section3);
                sectionRepoMock.Setup(repo => repo.PostSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting1.Guid)).ReturnsAsync(meeting1);
                sectionRepoMock.Setup(repo => repo.PostSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting2.Guid)).ReturnsAsync(meeting2);
                sectionRepoMock.Setup(repo => repo.PostSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting3.Guid)).ReturnsAsync(meeting3);
                courseRepoMock.Setup(repo => repo.GetAsync()).Returns(Task.FromResult(allCourses));
                var course_temp = allCourses.Where(c => c.Id == "342").FirstOrDefault();
                courseRepoMock.Setup(repo => repo.GetCourseByGuidAsync(It.IsAny<String>())).ReturnsAsync(course_temp);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(creditCategories);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(academicLevels);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(gradeSchemes);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(courseLevels);
                referenceDataRepoMock.Setup(repo => repo.DepartmentsAsync()).ReturnsAsync(departments);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns(locations);

                // Configuration defaults and campus calendar information.
                configRepoMock.Setup(repo => repo.GetDefaultsConfiguration()).Returns(new DefaultsConfiguration() { CampusCalendarId = "MAIN" });
                studentConfigRepoMock.Setup(repo => repo.GetCurriculumConfigurationAsync()).ReturnsAsync(new CurriculumConfiguration() { SectionActiveStatusCode = "A", SectionInactiveStatusCode = "I", DefaultInstructionalMethodCode = "INT" });
                eventRepoMock.Setup(repo => repo.GetCalendar("MAIN")).Returns(new CampusCalendar("MAIN", "Default Calendar", new DateTimeOffset(), new DateTimeOffset()));

                var sectionStatusCodes = new List<SectionStatusCode>()
                {
                    new SectionStatusCode("P", "Pending", SectionStatus.Active, SectionStatusIntegration.Pending),
                    new SectionStatusCode("C", "Closed", SectionStatus.Active, SectionStatusIntegration.Closed),
                    new SectionStatusCode("C", "Cancelled", SectionStatus.Active, SectionStatusIntegration.Cancelled),
                    new SectionStatusCode("O", "Open", SectionStatus.Active, SectionStatusIntegration.Open),
                };

                stuRefDataRepoMock.Setup(repo => repo.GetSectionStatusCodesAsync()).ReturnsAsync(sectionStatusCodes);


                // Mock the section service
                sectionCoordinationService = new SectionCoordinationService(adapterRegistry, sectionRepo, courseRepo, studentRepo, studentReferenceDataRepo,
                    referenceDataRepo, termRepo, studentConfigRepo, configRepo, personRepo, roomRepo, eventRepo, currentUserFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionRepo = null;
                studentRepo = null;
                roleRepo = null;
                adapterRegistry = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_Null_Object_V3()
            {
                var r = await sectionCoordinationService.PostSectionAsync(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_Null_Object_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var r = await sectionCoordinationService.PostSection2Async(null);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionService_Null_Guid_V3()
            {
                var r = await sectionCoordinationService.PostSectionAsync(new Dtos.Section());
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_Null_Guid_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var r = await sectionCoordinationService.PostSection2Async(new Dtos.Section2());
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_SectionKeyNotFound_V3()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).Throws(new KeyNotFoundException());
                var r = await sectionCoordinationService.PostSectionAsync(new Dtos.Section()
                {
                    Guid = meeting1.Guid
                });
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_NoPermission_V3()
            {
                var section = new Dtos.Section() { Guid = "999999999999999" };
                var r = await sectionCoordinationService.PostSectionAsync(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullCourse_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = null,
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection2Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_EmptyTitle_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection2Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullStartOn_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = null,
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection2Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_StartOnGreaterThanEndOn_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2010, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection2Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditMeasure_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection2Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditDetailAndCreditType_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = null }, Measure = new Dtos.CreditMeasure2(), Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection2Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditMinimum_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2010, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection2Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditType_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty(), 
                    Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Status = Dtos.SectionStatus2.Open,
                    Credits = credit,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection2Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullInstructionalPlaforms_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                referenceDataRepoMock.Setup(repo => repo.GetInstructionalPlatformsAsync(It.IsAny<bool>())).ReturnsAsync(null);
                //var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty(), 
                //Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Status = Dtos.SectionStatus2.Open,
                    AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2(academicLevels[0].Guid) },
                    //Credits = credit,
                    InstructionalPlatform = new Dtos.GuidObject2("jfhjdhjs"),
                    OwningOrganizations = new List<Dtos.OfferingOrganization2>() { 
                        new Dtos.OfferingOrganization2() {Organization = new Dtos.GuidObject2(departments[0].Guid) } },
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection2Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditCategoryOfCredit_V3()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() { Measure = Dtos.CreditMeasure.CEU, 
                    Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = "999999999999999",
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    EndDate = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus.Open,
                    CourseLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSectionAsync(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditCategoryOfCredit_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() { Measure = Dtos.CreditMeasure2.CEU, 
                    Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection2Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullCreditCategories_V3()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section2);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() {CreditCategory = new Dtos.GuidObject(creditCategories[0].Guid), 
                    Measure = Dtos.CreditMeasure.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = "999999999999999",
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    EndDate = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus.Open,
                    CourseLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSectionAsync(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditCategories_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section2);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection2Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullGradeSchemes_V3()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section3);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section3);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() {CreditCategory = new Dtos.GuidObject(creditCategories[0].Guid), 
                    Measure = Dtos.CreditMeasure.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = "999999999999999",
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    EndDate = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus.Open,
                    CourseLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSectionAsync(section);
            }

            [TestMethod]
            [ExpectedException(typeof(NullReferenceException))]
            public async Task SectionService_NullGradeSchemes_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section3);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section3);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    /*Detail = new Dtos.GuidObject2(creditCategories[0].Guid),*/ CreditType = Dtos.CreditCategoryType2.Institutional }, Measure = Dtos.CreditMeasure2.Credit, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection2Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullAcademicLevels_V3()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() {CreditCategory = new Dtos.GuidObject(creditCategories[0].Guid), 
                    Measure = Dtos.CreditMeasure.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = "999999999999999",
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    EndDate = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus.Open,
                    CourseLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSectionAsync(section);
            }

            [TestMethod]
            [ExpectedException(typeof(NullReferenceException))]
            public async Task SectionService_NullAcademicLevels_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    /*Detail = new Dtos.GuidObject2(creditCategories[0].Guid),*/ CreditType = Dtos.CreditCategoryType2.Transfer }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection2Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullLocations_V3()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section2);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section2);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns<Location>(null);
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() {CreditCategory = new Dtos.GuidObject(creditCategories[0].Guid), 
                    Measure = Dtos.CreditMeasure.Credit, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = "999999999999999",
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    EndDate = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus.Open,
                    CourseLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSectionAsync(section);
            }

            [TestMethod]
            [ExpectedException(typeof(NullReferenceException))]
            public async Task SectionService_NullLocations_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section2);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section2);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns<Location>(null);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    /*Detail = new Dtos.GuidObject2(creditCategories[0].Guid),*/ CreditType = Dtos.CreditCategoryType2.ContinuingEducation }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection2Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullCourseLevels_V3()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() {CreditCategory = new Dtos.GuidObject(creditCategories[0].Guid), 
                    Measure = Dtos.CreditMeasure.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = "999999999999999",
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus.Open
                };
                var r = await sectionCoordinationService.PostSectionAsync(section);
            }



            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCourseLevels_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection2Async(section2);
            }

            [TestMethod]
            public async Task SectionService_Post_V3()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() {CreditCategory = new Dtos.GuidObject(creditCategories[0].Guid), 
                    Measure = Dtos.CreditMeasure.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = section1.Guid,
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus.Open
                };
                var res = await sectionCoordinationService.PostSectionAsync(section);
                Assert.AreEqual(section1.Guid, res.Guid);
            }

            [TestMethod]
            public async Task SectionService_Post_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid), CreditType = Dtos.CreditCategoryType2.Institutional }, 
                    Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = section1.Guid,
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var res = await sectionCoordinationService.PostSection2Async(section2);
                Assert.AreEqual(section1.Guid, res.Id);
            }
        }

        [TestClass]
        public class Sections_Put_V3_V4 : CurrentUserSetup
        {
            private SectionCoordinationService sectionCoordinationService;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepo;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private IStudentReferenceDataRepository studentReferenceDataRepo;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private Mock<ICourseRepository> courseRepoMock;
            private ICourseRepository courseRepo;
            private Mock<ITermRepository> termRepoMock;
            private ITermRepository termRepo;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private IStudentConfigurationRepository studentConfigRepo;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IRoomRepository> roomRepoMock;
            private IRoomRepository roomRepo;
            private Mock<IEventRepository> eventRepoMock;
            private IEventRepository eventRepo;


            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;

            private Domain.Student.Entities.Section section1;
            private Domain.Student.Entities.Section section2;
            private Domain.Student.Entities.Section section3;
            private Domain.Student.Entities.SectionMeeting meeting1;
            private Domain.Student.Entities.SectionMeeting meeting2;
            private Domain.Student.Entities.SectionMeeting meeting3;
            private Domain.Student.Entities.SectionMeeting meeting4;

            private List<OfferingDepartment> dpts = new List<OfferingDepartment>() { new OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>() { "Whatever" };
            private List<SectionStatusItem> statuses = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddDays(-60)) };
            private IEnumerable<Domain.Student.Entities.Course> allCourses = new TestCourseRepository().GetAsync().Result;

            private List<CreditCategory> creditCategories = new List<CreditCategory>() { new CreditCategory(Guid.NewGuid().ToString(), "RC", "Regular Credit", CreditType.Institutional) };
            private List<Domain.Student.Entities.AcademicLevel> academicLevels = new List<Domain.Student.Entities.AcademicLevel>() { new Domain.Student.Entities.AcademicLevel(Guid.NewGuid().ToString(), "UG", "Undergraduate") };
            private List<GradeScheme> gradeSchemes = new List<GradeScheme>() { new GradeScheme(Guid.NewGuid().ToString(), "Pass/Fail", "Pass/Fail grade") };
            private List<Domain.Student.Entities.CourseLevel> courseLevels = new List<Domain.Student.Entities.CourseLevel>() { new Domain.Student.Entities.CourseLevel(Guid.NewGuid().ToString(), "100", "Introduction to terms, concepts, and techniques.") };
            private List<Department> departments = new List<Department>() { new Department(Guid.NewGuid().ToString(), "PHYS", "Physics", true) };
            private List<Location> locations = new List<Location>() { new Location(Guid.NewGuid().ToString(), "MD", "Maryland") };

            private string meeting1Guid = Guid.NewGuid().ToString();
            private string instructionalMethodGuid = Guid.NewGuid().ToString();
            private string roomGuid = Guid.NewGuid().ToString();
            private Domain.Entities.Permission permissionCreateandUpdateSections;
            private Domain.Entities.Permission permissionViewAnyProjects;
            private Domain.Entities.Permission permissionViewAnyProjectsLineItems;

            [TestInitialize]
            public void Initialize()
            {
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepo = sectionRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                roomRepoMock = new Mock<IRoomRepository>();
                roomRepo = roomRepoMock.Object;
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                studentReferenceDataRepo = stuRefDataRepoMock.Object;
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                referenceDataRepo = referenceDataRepoMock.Object;
                courseRepoMock = new Mock<ICourseRepository>();
                courseRepo = courseRepoMock.Object;
                termRepoMock = new Mock<ITermRepository>();
                termRepo = termRepoMock.Object;
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                studentConfigRepo = studentConfigRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
                eventRepoMock = new Mock<IEventRepository>();
                eventRepo = eventRepoMock.Object;

                // academicPeriodRepo = academicPeriodRepoMock.Object;
                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;

                // Set Current User
                currentUserFactory = new CurrentUserSetup.ThirdPartyUserFactory();

                logger = new Mock<ILogger>().Object;

                // Mock permissions
                permissionCreateandUpdateSections = new Ellucian.Colleague.Domain.Entities.Permission(StudentPermissionCodes.CreateAndUpdateSection);
                thirdPartyRole.AddPermission(permissionCreateandUpdateSections);

                // Mock the section repo response
                section1 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section1.TermId = "2012/FA";
                section1.EndDate = new DateTime(2012, 12, 21);
                section1.Guid = Guid.NewGuid().ToString();

                section2 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section2.TermId = "2012/FA";
                section2.Guid = Guid.NewGuid().ToString();

                section3 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section3.TermId = "2011/FA";
                section3.EndDate = new DateTime(2011, 12, 21);
                section3.Guid = Guid.NewGuid().ToString();

                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LEC", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting1.Guid = meeting1Guid;
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "DAN*102";
                section1.AddSectionMeeting(meeting1);

                meeting2 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting2.Guid = meeting1Guid;

                meeting3 = new Domain.Student.Entities.SectionMeeting("456", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting3.Guid = meeting1Guid;

                meeting4 = new Domain.Student.Entities.SectionMeeting("789", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting4.Guid = meeting1Guid;

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).ReturnsAsync(section1);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section2.Guid)).ReturnsAsync(section2);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section3.Guid)).ReturnsAsync(section3);
                sectionRepoMock.Setup(repo => repo.PutSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting1.Guid)).ReturnsAsync(meeting1);
                sectionRepoMock.Setup(repo => repo.PutSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting2.Guid)).ReturnsAsync(meeting2);
                sectionRepoMock.Setup(repo => repo.PutSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting3.Guid)).ReturnsAsync(meeting3);
                courseRepoMock.Setup(repo => repo.GetAsync()).Returns(Task.FromResult(allCourses));
                var course_temp = allCourses.Where(c => c.Id == "342").FirstOrDefault();
                courseRepoMock.Setup(repo => repo.GetCourseByGuidAsync(It.IsAny<String>())).ReturnsAsync(course_temp);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(creditCategories);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(academicLevels);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(gradeSchemes);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(courseLevels);
                referenceDataRepoMock.Setup(repo => repo.DepartmentsAsync()).ReturnsAsync(departments);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns(locations);

                var sectionStatusCodes = new List<SectionStatusCode>()
                {
                    new SectionStatusCode("P", "Pending", SectionStatus.Active, SectionStatusIntegration.Pending),
                    new SectionStatusCode("C", "Closed", SectionStatus.Active, SectionStatusIntegration.Closed),
                    new SectionStatusCode("C", "Cancelled", SectionStatus.Active, SectionStatusIntegration.Cancelled),
                    new SectionStatusCode("O", "Open", SectionStatus.Active, SectionStatusIntegration.Open),
                };

                stuRefDataRepoMock.Setup(repo => repo.GetSectionStatusCodesAsync()).ReturnsAsync(sectionStatusCodes);


                // Configuration defaults and campus calendar information.
                configRepoMock.Setup(repo => repo.GetDefaultsConfiguration()).Returns(new DefaultsConfiguration() { CampusCalendarId = "MAIN" });
                studentConfigRepoMock.Setup(repo => repo.GetCurriculumConfigurationAsync()).ReturnsAsync(new CurriculumConfiguration() { SectionActiveStatusCode = "A", SectionInactiveStatusCode = "I", DefaultInstructionalMethodCode = "INT" });
                eventRepoMock.Setup(repo => repo.GetCalendar("MAIN")).Returns(new CampusCalendar("MAIN", "Default Calendar", new DateTimeOffset(), new DateTimeOffset()));

                // Mock the section service
                sectionCoordinationService = new SectionCoordinationService(adapterRegistry, sectionRepo, courseRepo, studentRepo, studentReferenceDataRepo,
                    referenceDataRepo, termRepo, studentConfigRepo, configRepo, personRepo, roomRepo, eventRepo, currentUserFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionRepo = null;
                studentRepo = null;
                roleRepo = null;
                adapterRegistry = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_Null_Object_V3()
            {
                var r = await sectionCoordinationService.PutSectionAsync(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_Null_Object_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var r = await sectionCoordinationService.PutSection2Async(null);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionService_Null_Guid_V3()
            {
                var r = await sectionCoordinationService.PutSectionAsync(new Dtos.Section());
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_Null_Guid_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var r = await sectionCoordinationService.PutSection2Async(new Dtos.Section2());
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_SectionKeyNotFound_V3()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).Throws(new KeyNotFoundException());
                var r = await sectionCoordinationService.PutSectionAsync(new Dtos.Section()
                {
                    Guid = meeting1.Guid
                });
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_NoPermission_V3()
            {
                var section = new Dtos.Section() { Guid = "999999999999999" };
                var r = await sectionCoordinationService.PutSectionAsync(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullCourse_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = null,
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection2Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_EmptyTitle_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection2Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullStartOn_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = null,
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection2Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_StartOnGreaterThanEndOn_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2010, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection2Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditMeasure_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection2Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditMinimum_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2010, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection2Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCredit_V3()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() { Measure = Dtos.CreditMeasure.CEU, 
                    Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = "999999999999999",
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    EndDate = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus.Open,
                    CourseLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSectionAsync(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCredit_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() { Measure = Dtos.CreditMeasure2.CEU, 
                    Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection2Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ConfigurationException))]
            public async Task SectionService_NullCurriculumConfig_V3()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                studentConfigRepoMock.Setup(repo => repo.GetCurriculumConfigurationAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() {CreditCategory = new Dtos.GuidObject(creditCategories[0].Guid), 
                    Measure = Dtos.CreditMeasure.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = "999999999999999",
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    EndDate = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus.Open,
                    CourseLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSectionAsync(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCurriculumConfig_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                studentConfigRepoMock.Setup(repo => repo.GetCurriculumConfigurationAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection2Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ConfigurationException))]
            public async Task SectionService_NullInactiveStatusForCurriculumConfig_V3()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                studentConfigRepoMock.Setup(repo => repo.GetCurriculumConfigurationAsync()).ReturnsAsync(new CurriculumConfiguration() { SectionActiveStatusCode = "A", DefaultInstructionalMethodCode = "INT" });
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() {CreditCategory = new Dtos.GuidObject(creditCategories[0].Guid), 
                    Measure = Dtos.CreditMeasure.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = "999999999999999",
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    EndDate = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus.Open,
                    CourseLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSectionAsync(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullInactiveStatusForCurriculumConfig_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                studentConfigRepoMock.Setup(repo => repo.GetCurriculumConfigurationAsync()).ReturnsAsync(new CurriculumConfiguration() { SectionActiveStatusCode = "A", DefaultInstructionalMethodCode = "INT" });
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection2Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ConfigurationException))]
            public async Task SectionService_NullActiveStatusForCurriculumConfig_V3()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                studentConfigRepoMock.Setup(repo => repo.GetCurriculumConfigurationAsync()).ReturnsAsync(new CurriculumConfiguration() { SectionInactiveStatusCode = "I", DefaultInstructionalMethodCode = "INT" });
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() {CreditCategory = new Dtos.GuidObject(creditCategories[0].Guid), 
                    Measure = Dtos.CreditMeasure.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = "999999999999999",
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    EndDate = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus.Open,
                    CourseLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSectionAsync(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullActiveStatusForCurriculumConfig_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                studentConfigRepoMock.Setup(repo => repo.GetCurriculumConfigurationAsync()).ReturnsAsync(new CurriculumConfiguration() { SectionInactiveStatusCode = "I", DefaultInstructionalMethodCode = "INT" });
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection2Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullCreditCategories_V3()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() {CreditCategory = new Dtos.GuidObject(creditCategories[0].Guid), 
                    Measure = Dtos.CreditMeasure.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = "999999999999999",
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    EndDate = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus.Closed,
                    CourseLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSectionAsync(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditCategories_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Closed,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection2Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullGradeSchemes_V3()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() {CreditCategory = new Dtos.GuidObject(creditCategories[0].Guid), 
                    Measure = Dtos.CreditMeasure.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = "999999999999999",
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    EndDate = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus.Cancelled,
                    CourseLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSectionAsync(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullGradeSchemes_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Cancelled,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection2Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullAcademicLevels_V3()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() {CreditCategory = new Dtos.GuidObject(creditCategories[0].Guid), 
                    Measure = Dtos.CreditMeasure.Credit, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = "999999999999999",
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    EndDate = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    //Status = Dtos.SectionStatus.Open,
                    CourseLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSectionAsync(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullAcademicLevels_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    //Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection2Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullLocations_V3()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section2);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section2);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns<Location>(null);
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() {CreditCategory = new Dtos.GuidObject(creditCategories[0].Guid), 
                    Measure = Dtos.CreditMeasure.Credit, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = "999999999999999",
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    EndDate = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus.Open,
                    CourseLevels = new List<Dtos.GuidObject>() { new Dtos.GuidObject("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSectionAsync(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullLocations_V4()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns<Location>(null);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection2Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullCourseLevels_V3()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() {CreditCategory = new Dtos.GuidObject(creditCategories[0].Guid), 
                    Measure = Dtos.CreditMeasure.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = "999999999999999",
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus.Open
                };
                var r = await sectionCoordinationService.PutSectionAsync(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCourseLevels_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection2Async(section2);
            }

            [TestMethod]
            public async Task SectionService_Put_V3()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.Credit>() { new Dtos.Credit() {CreditCategory = new Dtos.GuidObject(creditCategories[0].Guid), 
                    Measure = Dtos.CreditMeasure.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section()
                {
                    Guid = section1.Guid,
                    Number = "01",
                    StartDate = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus.Open
                };
                var res = await sectionCoordinationService.PutSectionAsync(section);
                Assert.AreEqual(section1.Guid, res.Guid);
            }

            [TestMethod]
            public async Task SectionService_Put_V4()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid), CreditType = Dtos.CreditCategoryType2.Institutional }, 
                    Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section2()
                {
                    Id = section1.Guid,
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var res = await sectionCoordinationService.PutSection2Async(section2);
                Assert.AreEqual(section1.Guid, res.Id);
            }
        }

        [TestClass]
        public class Sections_Get_V6 : CurrentUserSetup
        {
            private SectionCoordinationService sectionCoordinationService;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepo;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private IStudentReferenceDataRepository studentReferenceDataRepo;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private Mock<ICourseRepository> courseRepoMock;
            private ICourseRepository courseRepo;
            private Mock<ITermRepository> termRepoMock;
            private ITermRepository termRepo;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private IStudentConfigurationRepository studentConfigRepo;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IRoomRepository> roomRepoMock;
            private IRoomRepository roomRepo;
            private Mock<IEventRepository> eventRepoMock;
            private IEventRepository eventRepo;
            // private Mock<IAcademicPeriodRepository> academicPeriodRepoMock;
            // private IAcademicPeriodRepository academicPeriodRepo;
            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;

            private Domain.Student.Entities.Section section1;
            private Domain.Student.Entities.Section section2;
            private Domain.Student.Entities.Section section3;
            private Domain.Student.Entities.Section section4;
            private Domain.Student.Entities.SectionMeeting meeting1;
            private Domain.Student.Entities.SectionMeeting meeting2;
            private Domain.Student.Entities.SectionMeeting meeting3;
            private Domain.Student.Entities.SectionMeeting meeting4;

            private List<Domain.Student.Entities.Section> section;
            private List<OfferingDepartment> dpts = new List<OfferingDepartment>() { new OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>() { "Whatever" };
            private List<SectionStatusItem> statuses = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses2 = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Inactive, "I", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses3 = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Cancelled, "C", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses4 = new List<SectionStatusItem>() { new SectionStatusItem(new SectionStatus(), "N/A", DateTime.Today.AddDays(-60)) };
            private List<CreditCategory> creditCategories = new List<CreditCategory>() { new CreditCategory(Guid.NewGuid().ToString(), "IN", "Regular Credit", CreditType.Institutional) };
            private List<Domain.Student.Entities.AcademicLevel> academicLevels = new List<Domain.Student.Entities.AcademicLevel>() { new Domain.Student.Entities.AcademicLevel(Guid.NewGuid().ToString(), "UG", "Undergraduate") };
            private List<GradeScheme> gradeSchemes = new List<GradeScheme>() { new GradeScheme(Guid.NewGuid().ToString(), "Pass/Fail", "Pass/Fail grade") };
            private List<Domain.Student.Entities.CourseLevel> courseLevels = new List<Domain.Student.Entities.CourseLevel>() { new Domain.Student.Entities.CourseLevel(Guid.NewGuid().ToString(), "100", "Introduction to terms, concepts, and techniques.") };
            private List<Department> departments = new List<Department>() { new Department(Guid.NewGuid().ToString(), "PHYS", "Physics", true) };
            private List<Location> locations = new List<Location>() { new Location(Guid.NewGuid().ToString(), "MD", "Maryland") };
            private List<AcademicPeriod> academicPeriods = new List<AcademicPeriod>() { new AcademicPeriod(Guid.NewGuid().ToString(), "2012/FA", "Fall 2014", DateTime.Today.AddDays(-60), DateTime.Today, 2015, 4, "Spring", Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), null) };
            private List<Domain.Base.Entities.InstructionalPlatform> instructionalPlatforms = new List<Domain.Base.Entities.InstructionalPlatform>() { new Domain.Base.Entities.InstructionalPlatform(Guid.NewGuid().ToString(), "Ellucian", "IP") };

            private string meeting1Guid = Guid.NewGuid().ToString();
            private string sectionGuid = Guid.NewGuid().ToString();
            private string roomGuid = Guid.NewGuid().ToString();
            private string courseGuid = Guid.NewGuid().ToString();

            [TestInitialize]
            public void Initialize()
            {
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepo = sectionRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                roomRepoMock = new Mock<IRoomRepository>();
                roomRepo = roomRepoMock.Object;
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                studentReferenceDataRepo = stuRefDataRepoMock.Object;
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                referenceDataRepo = referenceDataRepoMock.Object;
                courseRepoMock = new Mock<ICourseRepository>();
                courseRepo = courseRepoMock.Object;
                termRepoMock = new Mock<ITermRepository>();
                termRepo = termRepoMock.Object;
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                studentConfigRepo = studentConfigRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
                eventRepoMock = new Mock<IEventRepository>();
                eventRepo = eventRepoMock.Object;

                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;

                // "STU1" is the current user
                currentUserFactory = new CurrentUserSetup.StudentUserFactory();

                logger = new Mock<ILogger>().Object;

                // Mock the section repo response
                section1 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section1.Guid = sectionGuid;
                section1.TermId = "2012/FA";
                section1.EndDate = new DateTime(2012, 12, 21);
                section1.LearningProvider = "Ellucian";

                section2 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses2, true);
                section2.Guid = Guid.NewGuid().ToString();
                section2.TermId = "2012/FA";

                section3 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses3, true);
                section3.Guid = Guid.NewGuid().ToString();
                section3.TermId = "2011/FA";
                section3.EndDate = new DateTime(2011, 12, 21);

                section4 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC4", "1119", "04", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses4, true);
                section4.Guid = Guid.NewGuid().ToString();
                section4.TermId = "2011/FA";
                section4.EndDate = new DateTime(2011, 12, 21);

                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LEC", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting1.Guid = meeting1Guid;
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "DAN*102";
                section1.AddSectionMeeting(meeting1);

                meeting2 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting2.Guid = meeting1Guid;

                meeting3 = new Domain.Student.Entities.SectionMeeting("456", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting3.Guid = meeting1Guid;

                meeting4 = new Domain.Student.Entities.SectionMeeting("789", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting4.Guid = meeting1Guid;

                var sections = new List<Domain.Student.Entities.Section>();
                //var section = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                sections.Add(section1);
                sections.Add(section2);
                sections.Add(section3);
                sections.Add(section4);

                var rooms = new List<Domain.Base.Entities.Room>();
                var room = new Domain.Base.Entities.Room(roomGuid, "DAN*102", "Danville Hall Room 102");
                rooms.Add(room);

                var scheduleRepeats = new List<Domain.Base.Entities.ScheduleRepeat>();
                var scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("W", "Weekly", "7", FrequencyType.Weekly);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("D", "Daily", "1", FrequencyType.Daily);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("M", "Monthly", "30", FrequencyType.Monthly);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("Y", "Yearly", "365", FrequencyType.Yearly);
                scheduleRepeats.Add(scheduleRepeat);

                List<string> sec1List = new List<string>() { "SEC1" };
                List<Domain.Student.Entities.Section> sec1Resp = new List<Domain.Student.Entities.Section>() { section1 };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec1List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sec1Resp));

                List<string> sec2List = new List<string>() { "SEC2" };
                List<Domain.Student.Entities.Section> sec2Resp = new List<Domain.Student.Entities.Section>() { section2 };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec2List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sec2Resp));

                List<string> sec3List = new List<string>() { "SEC3" };
                List<Domain.Student.Entities.Section> sec3Resp = new List<Domain.Student.Entities.Section>() { section3 };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec3List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sec3Resp));

                List<string> sec4List = new List<string>() { "9999999" };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec4List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(new List<Domain.Student.Entities.Section>()));

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).ReturnsAsync(section1);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section2.Guid)).ReturnsAsync(section2);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section3.Guid)).ReturnsAsync(section3);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section4.Guid)).ReturnsAsync(section4);
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(section1.CourseId)).ReturnsAsync(section1.Title);
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(section2.CourseId)).ReturnsAsync(section2.Title);
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(section3.CourseId)).ReturnsAsync(section3.Title);
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(section4.CourseId)).ReturnsAsync(section4.Title);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(creditCategories);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(academicLevels);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(gradeSchemes);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(courseLevels);
                referenceDataRepoMock.Setup(repo => repo.DepartmentsAsync()).ReturnsAsync(departments);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns(locations);
                termRepoMock.Setup(repo => repo.GetAcademicPeriods(It.IsAny<IEnumerable<Domain.Student.Entities.Term>>())).Returns(academicPeriods);
                referenceDataRepoMock.Setup(repo => repo.GetInstructionalPlatformsAsync(It.IsAny<bool>())).ReturnsAsync(instructionalPlatforms);

                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(new Tuple<IEnumerable<Domain.Student.Entities.Section>, int>(sections, 100));

                //roomRepoMock.Setup(repo => repo.RoomsAsync()).ReturnsAsync(rooms);
                //referenceDataRepoMock.Setup(repo => repo.ScheduleRepeats).Returns(scheduleRepeats);

                // Mock the section service
                sectionCoordinationService = new SectionCoordinationService(adapterRegistry, sectionRepo, courseRepo, studentRepo, studentReferenceDataRepo,
                    referenceDataRepo, termRepo, studentConfigRepo, configRepo, personRepo, roomRepo, eventRepo, currentUserFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionRepo = null;
                studentRepo = null;
                roleRepo = null;
                adapterRegistry = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_SectionEmptyId_V6()
            {
                var r = await sectionCoordinationService.GetSection3ByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionService_InvalidSectionId_V6()
            {
                var r = await sectionCoordinationService.GetSection3ByGuidAsync("9999999");
            }

            [TestMethod]
            public async Task SectionService_GetByGuid_V6()
            {
                var res = await sectionCoordinationService.GetSection3ByGuidAsync(section1.Guid);
                Assert.AreEqual(section1.Guid, res.Id);
            }

            [TestMethod]
            public async Task SectionService_GetAll_EmptyArgument_V6()
            {
                var res = await sectionCoordinationService.GetSections3Async(0, 5);
                var sec = res.Item1.Where(r => r.Id == section1.Guid).FirstOrDefault();
                Assert.AreEqual(section1.Guid, sec.Id);
            }

            [TestMethod]
            public async Task SectionService_GetAll_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate("2014-09-01T00:00:00+00:00")).ReturnsAsync("2014-09-01T00:00:00+00:00");
                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate("2015-08-31T23:59:59+00:00")).ReturnsAsync("2015-08-31T23:59:59+00:00");
                sectionRepoMock.Setup(repo => repo.GetCourseIdFromGuidAsync(section1.Guid)).ReturnsAsync(section1.CourseId);
                sectionRepoMock.Setup(repo => repo.ConvertStatusToStatusCodeAsync("open")).ReturnsAsync("'A'");
                var res = await sectionCoordinationService.GetSections3Async(0, 5, "",
                    "2014-09-01T00:00:00+00:00", "2015-08-31T23:59:59+00:00", "", "", "", "", "",
                    section1.Guid, "", "open", "");
                var sec = res.Item1.Where(r => r.Id == section1.Guid).FirstOrDefault();
                Assert.AreEqual(section1.Guid, sec.Id);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_GetAll_WithInvalidDateFormat_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate(It.IsAny<string>())).Throws<Exception>();
                var res = await sectionCoordinationService.GetSections3Async(0, 5, "",
                    "2014-09-01T00:00:00+00:00", "2015-08-31T23:59:59+00:00", "", "", "", "", "",
                    section1.Guid, "", "open", "");
            }
            [TestMethod]
            public async Task SectionService_GetByGuidWithInactiveStatus_V6()
            {
                var res = await sectionCoordinationService.GetSection3ByGuidAsync(section2.Guid);
                Assert.AreEqual(section2.Guid, res.Id);
            }

            [TestMethod]
            public async Task SectionService_GetByGuidWithCancelledStatus_V6()
            {
                var res = await sectionCoordinationService.GetSection3ByGuidAsync(section3.Guid);
                Assert.AreEqual(section3.Guid, res.Id);
            }
            [TestMethod]
            public async Task SectionService_GetByGuidWithNoStatus_V6()
            {
                var res = await sectionCoordinationService.GetSection3ByGuidAsync(section4.Guid);
                Assert.AreEqual(section4.Guid, res.Id);
            }
        }

        [TestClass]
        public class Sections_Post_V6 : CurrentUserSetup
        {
            private SectionCoordinationService sectionCoordinationService;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepo;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private IStudentReferenceDataRepository studentReferenceDataRepo;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private Mock<ICourseRepository> courseRepoMock;
            private ICourseRepository courseRepo;
            private Mock<ITermRepository> termRepoMock;
            private ITermRepository termRepo;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private IStudentConfigurationRepository studentConfigRepo;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IRoomRepository> roomRepoMock;
            private IRoomRepository roomRepo;
            private Mock<IEventRepository> eventRepoMock;
            private IEventRepository eventRepo;


            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;

            private Domain.Student.Entities.Section section1;
            private Domain.Student.Entities.Section section2;
            private Domain.Student.Entities.Section section3;
            private Domain.Student.Entities.SectionMeeting meeting1;
            private Domain.Student.Entities.SectionMeeting meeting2;
            private Domain.Student.Entities.SectionMeeting meeting3;
            private Domain.Student.Entities.SectionMeeting meeting4;

            private List<OfferingDepartment> dpts = new List<OfferingDepartment>() { new OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>() { "Whatever" };
            private List<SectionStatusItem> statuses = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses2 = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Inactive, "I", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses3 = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Cancelled, "C", DateTime.Today.AddDays(-60)) };
            private IEnumerable<Domain.Student.Entities.Course> allCourses = new TestCourseRepository().GetAsync().Result;

            private List<CreditCategory> creditCategories = new List<CreditCategory>() { new CreditCategory(Guid.NewGuid().ToString(), "RC", "Regular Credit", CreditType.Institutional),
                new CreditCategory(Guid.NewGuid().ToString(), "RC", "Regular Credit", CreditType.Transfer), new CreditCategory(Guid.NewGuid().ToString(), "RC", "Regular Credit", CreditType.ContinuingEducation ) };
            private List<Domain.Student.Entities.AcademicLevel> academicLevels = new List<Domain.Student.Entities.AcademicLevel>() { new Domain.Student.Entities.AcademicLevel(Guid.NewGuid().ToString(), "UG", "Undergraduate") };
            private List<GradeScheme> gradeSchemes = new List<GradeScheme>() { new GradeScheme(Guid.NewGuid().ToString(), "Pass/Fail", "Pass/Fail grade") };
            private List<Domain.Student.Entities.CourseLevel> courseLevels = new List<Domain.Student.Entities.CourseLevel>() { new Domain.Student.Entities.CourseLevel(Guid.NewGuid().ToString(), "100", "Introduction to terms, concepts, and techniques.") };
            private List<Department> departments = new List<Department>() { new Department(Guid.NewGuid().ToString(), "PHYS", "Physics", true) };
            private List<Location> locations = new List<Location>() { new Location(Guid.NewGuid().ToString(), "MD", "Maryland") };

            private string meeting1Guid = Guid.NewGuid().ToString();
            private string instructionalMethodGuid = Guid.NewGuid().ToString();
            private string roomGuid = Guid.NewGuid().ToString();
            private Domain.Entities.Permission permissionCreateandUpdateSections;
            private Domain.Entities.Permission permissionViewAnyProjects;
            private Domain.Entities.Permission permissionViewAnyProjectsLineItems;

            [TestInitialize]
            public void Initialize()
            {
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepo = sectionRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                roomRepoMock = new Mock<IRoomRepository>();
                roomRepo = roomRepoMock.Object;
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                studentReferenceDataRepo = stuRefDataRepoMock.Object;
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                referenceDataRepo = referenceDataRepoMock.Object;
                courseRepoMock = new Mock<ICourseRepository>();
                courseRepo = courseRepoMock.Object;
                termRepoMock = new Mock<ITermRepository>();
                termRepo = termRepoMock.Object;
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                studentConfigRepo = studentConfigRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
                eventRepoMock = new Mock<IEventRepository>();
                eventRepo = eventRepoMock.Object;


                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;

                // Set Current User
                currentUserFactory = new CurrentUserSetup.ThirdPartyUserFactory();

                logger = new Mock<ILogger>().Object;

                // Mock permissions
                permissionCreateandUpdateSections = new Ellucian.Colleague.Domain.Entities.Permission(StudentPermissionCodes.CreateAndUpdateSection);
                thirdPartyRole.AddPermission(permissionCreateandUpdateSections);
                //permissionViewAnyProjects = new Ellucian.Colleague.Domain.Entities.Permission(StudentPermissionCodes.ViewAnyProjects);
                //thirdPartyRole.AddPermission(permissionViewAnyProjects);
                //permissionViewAnyProjectsLineItems = new Ellucian.Colleague.Domain.Entities.Permission(BasePermissionCodes.ViewAnyProjectsLineItems);
                //thirdPartyRole.AddPermission(permissionViewAnyProjectsLineItems);

                // Mock the section repo response
                section1 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section1.TermId = "2012/FA";
                section1.EndDate = new DateTime(2012, 12, 21);
                section1.Guid = Guid.NewGuid().ToString();

                section2 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, null, "Introduction to Art", "IN", dpts, levels, "UG", statuses2, true);
                section2.TermId = "2012/FA";
                section2.Guid = Guid.NewGuid().ToString();

                section3 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses3, true);
                section3.TermId = "2011/FA";
                section3.EndDate = new DateTime(2011, 12, 21);
                section3.Guid = Guid.NewGuid().ToString();

                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LEC", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting1.Guid = meeting1Guid;
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "DAN*102";
                section1.AddSectionMeeting(meeting1);

                meeting2 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting2.Guid = meeting1Guid;

                meeting3 = new Domain.Student.Entities.SectionMeeting("456", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting3.Guid = meeting1Guid;

                meeting4 = new Domain.Student.Entities.SectionMeeting("789", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting4.Guid = meeting1Guid;

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).ReturnsAsync(section1);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section2.Guid)).ReturnsAsync(section2);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section3.Guid)).ReturnsAsync(section3);
                sectionRepoMock.Setup(repo => repo.PostSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting1.Guid)).ReturnsAsync(meeting1);
                sectionRepoMock.Setup(repo => repo.PostSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting2.Guid)).ReturnsAsync(meeting2);
                sectionRepoMock.Setup(repo => repo.PostSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting3.Guid)).ReturnsAsync(meeting3);
                courseRepoMock.Setup(repo => repo.GetAsync()).Returns(Task.FromResult(allCourses));
                var course_temp = allCourses.Where(c => c.Id == "342").FirstOrDefault();
                courseRepoMock.Setup(repo => repo.GetCourseByGuidAsync(It.IsAny<String>())).ReturnsAsync(course_temp);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(creditCategories);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(academicLevels);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(gradeSchemes);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(courseLevels);
                referenceDataRepoMock.Setup(repo => repo.DepartmentsAsync()).ReturnsAsync(departments);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns(locations);

                // Configuration defaults and campus calendar information.
                configRepoMock.Setup(repo => repo.GetDefaultsConfiguration()).Returns(new DefaultsConfiguration() { CampusCalendarId = "MAIN" });
                studentConfigRepoMock.Setup(repo => repo.GetCurriculumConfigurationAsync()).ReturnsAsync(new CurriculumConfiguration() { SectionActiveStatusCode = "A", SectionInactiveStatusCode = "I", DefaultInstructionalMethodCode = "INT" });
                eventRepoMock.Setup(repo => repo.GetCalendar("MAIN")).Returns(new CampusCalendar("MAIN", "Default Calendar", new DateTimeOffset(), new DateTimeOffset()));

                var sectionStatusCodes = new List<SectionStatusCode>()
                {
                    new SectionStatusCode("P", "Pending", SectionStatus.Active, SectionStatusIntegration.Pending),
                    new SectionStatusCode("C", "Closed", SectionStatus.Active, SectionStatusIntegration.Closed),
                    new SectionStatusCode("C", "Cancelled", SectionStatus.Active, SectionStatusIntegration.Cancelled),
                    new SectionStatusCode("O", "Open", SectionStatus.Active, SectionStatusIntegration.Open),
                };

                stuRefDataRepoMock.Setup(repo => repo.GetSectionStatusCodesAsync()).ReturnsAsync(sectionStatusCodes);


                // Mock the section service
                sectionCoordinationService = new SectionCoordinationService(adapterRegistry, sectionRepo, courseRepo, studentRepo, studentReferenceDataRepo,
                    referenceDataRepo, termRepo, studentConfigRepo, configRepo, personRepo, roomRepo, eventRepo, currentUserFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionRepo = null;
                studentRepo = null;
                roleRepo = null;
                adapterRegistry = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_Null_Object_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var r = await sectionCoordinationService.PostSection3Async(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_Null_Guid_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var r = await sectionCoordinationService.PostSection3Async(new Dtos.Section3());
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_SectionKeyNotFound_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).Throws(new KeyNotFoundException());
                var r = await sectionCoordinationService.PostSection3Async(new Dtos.Section3()
                {
                    Id = meeting1.Guid
                });
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_NoPermission_V6()
            {
                var section = new Dtos.Section3() { Id = "999999999999999" };
                var r = await sectionCoordinationService.PostSection3Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullCourse_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                //var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                //    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = null,
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection3Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_EmptyTitle_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection3Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullStartOn_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = null,
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection3Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_StartOnGreaterThanEndOn_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2010, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection3Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditMeasure_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection3Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditDetailAndCreditType_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = null }, Measure = new Dtos.CreditMeasure2(), Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection3Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditMinimum_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2010, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection3Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditType_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2(), 
                    Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Status = Dtos.SectionStatus2.Open,
                    Credits = credit,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection3Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullInstructionalPlaforms_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                referenceDataRepoMock.Setup(repo => repo.GetInstructionalPlatformsAsync(It.IsAny<bool>())).ReturnsAsync(null);
                //var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty(), 
                //Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Status = Dtos.SectionStatus2.Open,
                    AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2(academicLevels[0].Guid) },
                    //Credits = credit,
                    InstructionalPlatform = new Dtos.GuidObject2("jfhjdhjs"),
                    //OwningOrganizations = new List<Dtos.OfferingOrganization2>() { 
                    //    new Dtos.OfferingOrganization2() {Organization = new Dtos.GuidObject2(departments[0].Guid) } },
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection3Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditCategoryOfCredit_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() { Measure = Dtos.CreditMeasure2.CEU, 
                    Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection3Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditCategories_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section2);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection3Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullGradeSchemes_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section3);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section3);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    /*Detail = new Dtos.GuidObject2(creditCategories[0].Guid),*/ CreditType = Dtos.EnumProperties.CreditCategoryType3.Institutional }, Measure = Dtos.CreditMeasure2.Credit, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection3Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullAcademicLevels_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    /*Detail = new Dtos.GuidObject2(creditCategories[0].Guid),*/ CreditType = Dtos.EnumProperties.CreditCategoryType3.Transfer }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection3Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullLocations_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section2);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section2);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns<Location>(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    /*Detail = new Dtos.GuidObject2(creditCategories[0].Guid),*/ CreditType = Dtos.EnumProperties.CreditCategoryType3.ContinuingEducation }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection3Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCourseLevels_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection3Async(section2);
            }

            [TestMethod]
            public async Task SectionService_Post_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid), CreditType = Dtos.EnumProperties.CreditCategoryType3.Institutional }, 
                    Measure = Dtos.CreditMeasure2.Credit, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = section1.Guid,
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var res = await sectionCoordinationService.PostSection3Async(section2);
                Assert.AreEqual(section1.Guid, res.Id);
            }
        }

        [TestClass]
        public class Sections_Put_V6 : CurrentUserSetup
        {
            private SectionCoordinationService sectionCoordinationService;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepo;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private IStudentReferenceDataRepository studentReferenceDataRepo;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private Mock<ICourseRepository> courseRepoMock;
            private ICourseRepository courseRepo;
            private Mock<ITermRepository> termRepoMock;
            private ITermRepository termRepo;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private IStudentConfigurationRepository studentConfigRepo;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IRoomRepository> roomRepoMock;
            private IRoomRepository roomRepo;
            private Mock<IEventRepository> eventRepoMock;
            private IEventRepository eventRepo;


            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;

            private Domain.Student.Entities.Section section1;
            private Domain.Student.Entities.Section section2;
            private Domain.Student.Entities.Section section3;
            private Domain.Student.Entities.SectionMeeting meeting1;
            private Domain.Student.Entities.SectionMeeting meeting2;
            private Domain.Student.Entities.SectionMeeting meeting3;
            private Domain.Student.Entities.SectionMeeting meeting4;

            private List<OfferingDepartment> dpts = new List<OfferingDepartment>() { new OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>() { "Whatever" };
            private List<SectionStatusItem> statuses = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses2 = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Inactive, "I", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses3 = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Cancelled, "C", DateTime.Today.AddDays(-60)) };
            private IEnumerable<Domain.Student.Entities.Course> allCourses = new TestCourseRepository().GetAsync().Result;

            private List<CreditCategory> creditCategories = new List<CreditCategory>() { new CreditCategory(Guid.NewGuid().ToString(), "RC", "Regular Credit", CreditType.Institutional),
                new CreditCategory(Guid.NewGuid().ToString(), "RC", "Regular Credit", CreditType.Transfer), new CreditCategory(Guid.NewGuid().ToString(), "RC", "Regular Credit", CreditType.ContinuingEducation ) };
            private List<Domain.Student.Entities.AcademicLevel> academicLevels = new List<Domain.Student.Entities.AcademicLevel>() { new Domain.Student.Entities.AcademicLevel(Guid.NewGuid().ToString(), "UG", "Undergraduate") };
            private List<GradeScheme> gradeSchemes = new List<GradeScheme>() { new GradeScheme(Guid.NewGuid().ToString(), "Pass/Fail", "Pass/Fail grade") };
            private List<Domain.Student.Entities.CourseLevel> courseLevels = new List<Domain.Student.Entities.CourseLevel>() { new Domain.Student.Entities.CourseLevel(Guid.NewGuid().ToString(), "100", "Introduction to terms, concepts, and techniques.") };
            private List<Department> departments = new List<Department>() { new Department(Guid.NewGuid().ToString(), "PHYS", "Physics", true) };
            private List<Location> locations = new List<Location>() { new Location(Guid.NewGuid().ToString(), "MD", "Maryland") };

            private string meeting1Guid = Guid.NewGuid().ToString();
            private string instructionalMethodGuid = Guid.NewGuid().ToString();
            private string roomGuid = Guid.NewGuid().ToString();
            private Domain.Entities.Permission permissionCreateandUpdateSections;
            private Domain.Entities.Permission permissionViewAnyProjects;
            private Domain.Entities.Permission permissionViewAnyProjectsLineItems;

            [TestInitialize]
            public void Initialize()
            {
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepo = sectionRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                roomRepoMock = new Mock<IRoomRepository>();
                roomRepo = roomRepoMock.Object;
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                studentReferenceDataRepo = stuRefDataRepoMock.Object;
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                referenceDataRepo = referenceDataRepoMock.Object;
                courseRepoMock = new Mock<ICourseRepository>();
                courseRepo = courseRepoMock.Object;
                termRepoMock = new Mock<ITermRepository>();
                termRepo = termRepoMock.Object;
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                studentConfigRepo = studentConfigRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
                eventRepoMock = new Mock<IEventRepository>();
                eventRepo = eventRepoMock.Object;


                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;

                // Set Current User
                currentUserFactory = new CurrentUserSetup.ThirdPartyUserFactory();

                logger = new Mock<ILogger>().Object;

                // Mock permissions
                permissionCreateandUpdateSections = new Ellucian.Colleague.Domain.Entities.Permission(StudentPermissionCodes.CreateAndUpdateSection);
                thirdPartyRole.AddPermission(permissionCreateandUpdateSections);
                //permissionViewAnyProjects = new Ellucian.Colleague.Domain.Entities.Permission(StudentPermissionCodes.ViewAnyProjects);
                //thirdPartyRole.AddPermission(permissionViewAnyProjects);
                //permissionViewAnyProjectsLineItems = new Ellucian.Colleague.Domain.Entities.Permission(BasePermissionCodes.ViewAnyProjectsLineItems);
                //thirdPartyRole.AddPermission(permissionViewAnyProjectsLineItems);

                // Mock the section repo response
                section1 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section1.TermId = "2012/FA";
                section1.EndDate = new DateTime(2012, 12, 21);
                section1.Guid = Guid.NewGuid().ToString();

                section2 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, null, "Introduction to Art", "IN", dpts, levels, "UG", statuses2, true);
                section2.TermId = "2012/FA";
                section2.Guid = Guid.NewGuid().ToString();

                section3 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses3, true);
                section3.TermId = "2011/FA";
                section3.EndDate = new DateTime(2011, 12, 21);
                section3.Guid = Guid.NewGuid().ToString();

                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LEC", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting1.Guid = meeting1Guid;
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "DAN*102";
                section1.AddSectionMeeting(meeting1);

                meeting2 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting2.Guid = meeting1Guid;

                meeting3 = new Domain.Student.Entities.SectionMeeting("456", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting3.Guid = meeting1Guid;

                meeting4 = new Domain.Student.Entities.SectionMeeting("789", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting4.Guid = meeting1Guid;

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).ReturnsAsync(section1);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section2.Guid)).ReturnsAsync(section2);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section3.Guid)).ReturnsAsync(section3);
                sectionRepoMock.Setup(repo => repo.PutSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting1.Guid)).ReturnsAsync(meeting1);
                sectionRepoMock.Setup(repo => repo.PutSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting2.Guid)).ReturnsAsync(meeting2);
                sectionRepoMock.Setup(repo => repo.PutSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting3.Guid)).ReturnsAsync(meeting3);
                courseRepoMock.Setup(repo => repo.GetAsync()).Returns(Task.FromResult(allCourses));
                var course_temp = allCourses.Where(c => c.Id == "342").FirstOrDefault();
                courseRepoMock.Setup(repo => repo.GetCourseByGuidAsync(It.IsAny<String>())).ReturnsAsync(course_temp);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(creditCategories);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(academicLevels);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(gradeSchemes);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(courseLevels);
                referenceDataRepoMock.Setup(repo => repo.DepartmentsAsync()).ReturnsAsync(departments);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns(locations);

                // Configuration defaults and campus calendar information.
                configRepoMock.Setup(repo => repo.GetDefaultsConfiguration()).Returns(new DefaultsConfiguration() { CampusCalendarId = "MAIN" });
                studentConfigRepoMock.Setup(repo => repo.GetCurriculumConfigurationAsync()).ReturnsAsync(new CurriculumConfiguration() { SectionActiveStatusCode = "A", SectionInactiveStatusCode = "I", DefaultInstructionalMethodCode = "INT" });
                eventRepoMock.Setup(repo => repo.GetCalendar("MAIN")).Returns(new CampusCalendar("MAIN", "Default Calendar", new DateTimeOffset(), new DateTimeOffset()));

                var sectionStatusCodes = new List<SectionStatusCode>()
                {
                    new SectionStatusCode("P", "Pending", SectionStatus.Active, SectionStatusIntegration.Pending),
                    new SectionStatusCode("C", "Closed", SectionStatus.Active, SectionStatusIntegration.Closed),
                    new SectionStatusCode("C", "Cancelled", SectionStatus.Active, SectionStatusIntegration.Cancelled),
                    new SectionStatusCode("O", "Open", SectionStatus.Active, SectionStatusIntegration.Open),
                };

                stuRefDataRepoMock.Setup(repo => repo.GetSectionStatusCodesAsync()).ReturnsAsync(sectionStatusCodes);


                // Mock the section service
                sectionCoordinationService = new SectionCoordinationService(adapterRegistry, sectionRepo, courseRepo, studentRepo, studentReferenceDataRepo,
                    referenceDataRepo, termRepo, studentConfigRepo, configRepo, personRepo, roomRepo, eventRepo, currentUserFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionRepo = null;
                studentRepo = null;
                roleRepo = null;
                adapterRegistry = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_Null_Object_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var r = await sectionCoordinationService.PutSection3Async(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_Null_Guid_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var r = await sectionCoordinationService.PutSection3Async(new Dtos.Section3());
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_SectionKeyNotFound_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).Throws(new KeyNotFoundException());
                var r = await sectionCoordinationService.PutSection3Async(new Dtos.Section3()
                {
                    Id = meeting1.Guid
                });
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_NoPermission_V6()
            {
                var section = new Dtos.Section3() { Id = "999999999999999" };
                var r = await sectionCoordinationService.PutSection3Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullCourse_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                //var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                //    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = null,
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection3Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_EmptyTitle_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection3Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullStartOn_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = null,
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection3Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_StartOnGreaterThanEndOn_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2010, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection3Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditMeasure_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection3Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditDetailAndCreditType_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = null }, Measure = new Dtos.CreditMeasure2(), Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection3Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditMinimum_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2010, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection3Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditType_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2(), 
                    Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Status = Dtos.SectionStatus2.Open,
                    Credits = credit,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection3Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullInstructionalPlaforms_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                referenceDataRepoMock.Setup(repo => repo.GetInstructionalPlatformsAsync(It.IsAny<bool>())).ReturnsAsync(null);
                //var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty(), 
                //Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Status = Dtos.SectionStatus2.Open,
                    AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2(academicLevels[0].Guid) },
                    //Credits = credit,
                    InstructionalPlatform = new Dtos.GuidObject2("jfhjdhjs"),
                    //OwningOrganizations = new List<Dtos.OfferingOrganization2>() { 
                    //    new Dtos.OfferingOrganization2() {Organization = new Dtos.GuidObject2(departments[0].Guid) } },
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection3Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditCategoryOfCredit_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() { Measure = Dtos.CreditMeasure2.CEU, 
                    Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection3Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditCategories_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section2);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection3Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullGradeSchemes_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section3);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section3);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    /*Detail = new Dtos.GuidObject2(creditCategories[0].Guid),*/ CreditType = Dtos.EnumProperties.CreditCategoryType3.Institutional }, Measure = Dtos.CreditMeasure2.Credit, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection3Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullAcademicLevels_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    /*Detail = new Dtos.GuidObject2(creditCategories[0].Guid),*/ CreditType = Dtos.EnumProperties.CreditCategoryType3.Transfer }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection3Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullLocations_V6()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section2);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section2);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns<Location>(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    /*Detail = new Dtos.GuidObject2(creditCategories[0].Guid),*/ CreditType = Dtos.EnumProperties.CreditCategoryType3.ContinuingEducation }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection3Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCourseLevels_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection3Async(section2);
            }

            [TestMethod]
            public async Task SectionService_Put_V6()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid), CreditType = Dtos.EnumProperties.CreditCategoryType3.Institutional }, 
                    Measure = Dtos.CreditMeasure2.Credit, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section3()
                {
                    Id = section1.Guid,
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var res = await sectionCoordinationService.PutSection3Async(section2);
                Assert.AreEqual(section1.Guid, res.Id);
            }
        }

        [TestClass]
        public class Sections_Get_V8 : CurrentUserSetup
        {
            private SectionCoordinationService sectionCoordinationService;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepo;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private IStudentReferenceDataRepository studentReferenceDataRepo;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private Mock<ICourseRepository> courseRepoMock;
            private ICourseRepository courseRepo;
            private Mock<ITermRepository> termRepoMock;
            private ITermRepository termRepo;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private IStudentConfigurationRepository studentConfigRepo;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IRoomRepository> roomRepoMock;
            private IRoomRepository roomRepo;
            private Mock<IEventRepository> eventRepoMock;
            private IEventRepository eventRepo;
            // private Mock<IAcademicPeriodRepository> academicPeriodRepoMock;
            // private IAcademicPeriodRepository academicPeriodRepo;
            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;

            private Domain.Student.Entities.Section section1;
            private Domain.Student.Entities.Section section2;
            private Domain.Student.Entities.Section section3;
            private Domain.Student.Entities.Section section4;
            private Domain.Student.Entities.SectionMeeting meeting1;
            private Domain.Student.Entities.SectionMeeting meeting2;
            private Domain.Student.Entities.SectionMeeting meeting3;
            private Domain.Student.Entities.SectionMeeting meeting4;

            private List<Domain.Student.Entities.Section> section;
            private List<OfferingDepartment> dpts = new List<OfferingDepartment>() { new OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>() { "Whatever" };
            private List<SectionStatusItem> statuses = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses2 = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Inactive, "I", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses3 = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Cancelled, "C", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses4 = new List<SectionStatusItem>() { new SectionStatusItem(new SectionStatus(), "N/A", DateTime.Today.AddDays(-60)) };
            private List<CreditCategory> creditCategories = new List<CreditCategory>() { new CreditCategory(Guid.NewGuid().ToString(), "IN", "Regular Credit", CreditType.Institutional) };
            private List<Domain.Student.Entities.AcademicLevel> academicLevels = new List<Domain.Student.Entities.AcademicLevel>() { new Domain.Student.Entities.AcademicLevel(Guid.NewGuid().ToString(), "UG", "Undergraduate") };
            private List<GradeScheme> gradeSchemes = new List<GradeScheme>() { new GradeScheme(Guid.NewGuid().ToString(), "Pass/Fail", "Pass/Fail grade") };
            private List<Domain.Student.Entities.CourseLevel> courseLevels = new List<Domain.Student.Entities.CourseLevel>() { new Domain.Student.Entities.CourseLevel(Guid.NewGuid().ToString(), "100", "Introduction to terms, concepts, and techniques.") };
            private List<Department> departments = new List<Department>() { new Department(Guid.NewGuid().ToString(), "PHYS", "Physics", true) };
            private List<Location> locations = new List<Location>() { new Location(Guid.NewGuid().ToString(), "MD", "Maryland") };
            private List<AcademicPeriod> academicPeriods = new List<AcademicPeriod>() { new AcademicPeriod(Guid.NewGuid().ToString(), "2012/FA", "Fall 2014", DateTime.Today.AddDays(-60), DateTime.Today, 2015, 4, "Spring", Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), null) };
            private List<Domain.Base.Entities.InstructionalPlatform> instructionalPlatforms = new List<Domain.Base.Entities.InstructionalPlatform>() { new Domain.Base.Entities.InstructionalPlatform(Guid.NewGuid().ToString(), "Ellucian", "IP") };

            private string meeting1Guid = Guid.NewGuid().ToString();
            private string sectionGuid = Guid.NewGuid().ToString();
            private string roomGuid = Guid.NewGuid().ToString();
            private string courseGuid = Guid.NewGuid().ToString();

            [TestInitialize]
            public void Initialize()
            {
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepo = sectionRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                roomRepoMock = new Mock<IRoomRepository>();
                roomRepo = roomRepoMock.Object;
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                studentReferenceDataRepo = stuRefDataRepoMock.Object;
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                referenceDataRepo = referenceDataRepoMock.Object;
                courseRepoMock = new Mock<ICourseRepository>();
                courseRepo = courseRepoMock.Object;
                termRepoMock = new Mock<ITermRepository>();
                termRepo = termRepoMock.Object;
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                studentConfigRepo = studentConfigRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
                eventRepoMock = new Mock<IEventRepository>();
                eventRepo = eventRepoMock.Object;

                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;

                // "STU1" is the current user
                currentUserFactory = new CurrentUserSetup.StudentUserFactory();

                logger = new Mock<ILogger>().Object;

                // Mock the section repo response
                section1 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section1.Guid = sectionGuid;
                section1.TermId = "2012/FA";
                section1.EndDate = new DateTime(2012, 12, 21);
                section1.LearningProvider = "Ellucian";
                section1.Location = "Loc1";
                section1.BillingCred = 3;

                section2 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses2, true);
                section2.Guid = Guid.NewGuid().ToString();
                section2.TermId = "2012/FA";
                section1.Location = "Loc2";
                section2.BillingCred = 4;

                section3 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses3, true);
                section3.Guid = Guid.NewGuid().ToString();
                section3.TermId = "2011/FA";
                section3.EndDate = new DateTime(2011, 12, 21);
                section3.BillingCred = 3;
                section3.CensusDates = new List<DateTime?>() { new DateTime(2011, 09, 10) };

                section4 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC4", "1119", "04", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses4, true);
                section4.Guid = Guid.NewGuid().ToString();
                section4.TermId = "2011/FA";
                section4.EndDate = new DateTime(2011, 12, 21);
                section4.Location = "Loc1";

                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LEC", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting1.Guid = meeting1Guid;
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "DAN*102";
                section1.AddSectionMeeting(meeting1);

                meeting2 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting2.Guid = meeting1Guid;

                meeting3 = new Domain.Student.Entities.SectionMeeting("456", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting3.Guid = meeting1Guid;

                meeting4 = new Domain.Student.Entities.SectionMeeting("789", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting4.Guid = meeting1Guid;

                var sections = new List<Domain.Student.Entities.Section>();
                //var section = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                sections.Add(section1);
                sections.Add(section2);
                sections.Add(section3);
                sections.Add(section4);

                var rooms = new List<Domain.Base.Entities.Room>();
                var room = new Domain.Base.Entities.Room(roomGuid, "DAN*102", "Danville Hall Room 102");
                rooms.Add(room);

                var scheduleRepeats = new List<Domain.Base.Entities.ScheduleRepeat>();
                var scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("W", "Weekly", "7", FrequencyType.Weekly);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("D", "Daily", "1", FrequencyType.Daily);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("M", "Monthly", "30", FrequencyType.Monthly);
                scheduleRepeats.Add(scheduleRepeat);
                scheduleRepeat = new Domain.Base.Entities.ScheduleRepeat("Y", "Yearly", "365", FrequencyType.Yearly);
                scheduleRepeats.Add(scheduleRepeat);

                List<string> sec1List = new List<string>() { "SEC1" };
                List<Domain.Student.Entities.Section> sec1Resp = new List<Domain.Student.Entities.Section>() { section1 };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec1List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sec1Resp));

                List<string> sec2List = new List<string>() { "SEC2" };
                List<Domain.Student.Entities.Section> sec2Resp = new List<Domain.Student.Entities.Section>() { section2 };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec2List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sec2Resp));

                List<string> sec3List = new List<string>() { "SEC3" };
                List<Domain.Student.Entities.Section> sec3Resp = new List<Domain.Student.Entities.Section>() { section3 };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec3List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(sec3Resp));

                List<string> sec4List = new List<string>() { "9999999" };
                sectionRepoMock.Setup(repo => repo.GetCachedSectionsAsync(sec4List, false)).Returns(Task.FromResult<IEnumerable<Domain.Student.Entities.Section>>(new List<Domain.Student.Entities.Section>()));

                Term term1 = new Term("termGuid1","2011/FA", "2011 Fall", new DateTime(2011, 09, 01), new DateTime(2011, 12, 15), 2012,
                    0, false, false, "2011/FA", false);
                var date1 = new List<DateTime?>() { new DateTime(2011, 09, 15) };
                var regDate = new RegistrationDate("Loc1", null, null, null, null, null, null, null, null, null, date1);
                term1.AddRegistrationDates(regDate);
                Term term2 = new Term("termGuid2","2012/FA", "2012 Fall", new DateTime(2012, 09, 01), new DateTime(2012, 12, 15), 2013,
                    0, false, false, "2012/FA", false);
                regDate = new RegistrationDate("Loc2", null, null, null, null, null, null, null, null, null, new List<DateTime?>());
                term2.AddRegistrationDates(regDate);
                termRepoMock.Setup(repo => repo.Get("2011/FA")).Returns(term1);
                termRepoMock.Setup(repo => repo.Get("2012/FA")).Returns(term2);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).ReturnsAsync(section1);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section2.Guid)).ReturnsAsync(section2);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section3.Guid)).ReturnsAsync(section3);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section4.Guid)).ReturnsAsync(section4);
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(section1.CourseId)).ReturnsAsync(section1.Title);
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(section2.CourseId)).ReturnsAsync(section2.Title);
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(section3.CourseId)).ReturnsAsync(section3.Title);
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(section4.CourseId)).ReturnsAsync(section4.Title);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(creditCategories);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(academicLevels);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(gradeSchemes);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(courseLevels);
                referenceDataRepoMock.Setup(repo => repo.DepartmentsAsync()).ReturnsAsync(departments);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns(locations);
                termRepoMock.Setup(repo => repo.GetAcademicPeriods(It.IsAny<IEnumerable<Domain.Student.Entities.Term>>())).Returns(academicPeriods);
                referenceDataRepoMock.Setup(repo => repo.GetInstructionalPlatformsAsync(It.IsAny<bool>())).ReturnsAsync(instructionalPlatforms);
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(new Tuple<IEnumerable<Domain.Student.Entities.Section>, int>(sections, 100));

                //roomRepoMock.Setup(repo => repo.RoomsAsync()).ReturnsAsync(rooms);
                //referenceDataRepoMock.Setup(repo => repo.ScheduleRepeats).Returns(scheduleRepeats);

                // Mock the section service
                sectionCoordinationService = new SectionCoordinationService(adapterRegistry, sectionRepo, courseRepo, studentRepo, studentReferenceDataRepo,
                    referenceDataRepo, termRepo, studentConfigRepo, configRepo, personRepo, roomRepo, eventRepo, currentUserFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionRepo = null;
                studentRepo = null;
                roleRepo = null;
                adapterRegistry = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_SectionEmptyId_V8()
            {
                var r = await sectionCoordinationService.GetSection4ByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionService_InvalidSectionId_V8()
            {
                var r = await sectionCoordinationService.GetSection4ByGuidAsync("9999999");
            }

            [TestMethod]
            public async Task SectionService_GetByGuid_V8()
            {
                var res = await sectionCoordinationService.GetSection4ByGuidAsync(section1.Guid);
                Assert.AreEqual(section1.Guid, res.Id);
            }

            [TestMethod]
            public async Task SectionService_GetAll_EmptyArgument_V8()
            {
                var res = await sectionCoordinationService.GetSections4Async(0, 5);
                var sec = res.Item1.Where(r => r.Id == section1.Guid).FirstOrDefault();
                Assert.AreEqual(section1.Guid, sec.Id);
            }

            [TestMethod]
            public async Task SectionService_GetAll_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate("2014-09-01T00:00:00+00:00")).ReturnsAsync("2014-09-01T00:00:00+00:00");
                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate("2015-08-31T23:59:59+00:00")).ReturnsAsync("2015-08-31T23:59:59+00:00");
                sectionRepoMock.Setup(repo => repo.GetCourseIdFromGuidAsync(section1.Guid)).ReturnsAsync(section1.CourseId);
                sectionRepoMock.Setup(repo => repo.ConvertStatusToStatusCodeNoDefaultAsync("open")).ReturnsAsync("'A'");
                var res = await sectionCoordinationService.GetSections4Async(0, 5, "",
                    "2014-09-01T00:00:00+00:00", "2015-08-31T23:59:59+00:00", "", "", "", "", "",
                    section1.Guid, "", "open", "");
                var sec = res.Item1.Where(r => r.Id == section1.Guid).FirstOrDefault();
                Assert.AreEqual(section1.Guid, sec.Id);
            }

          [TestMethod]
            public async Task SectionService_GetByGuidWithInactiveStatus_V8()
            {
                var res = await sectionCoordinationService.GetSection4ByGuidAsync(section2.Guid);
                Assert.AreEqual(section2.Guid, res.Id);
            }

            [TestMethod]
            public async Task SectionService_GetByGuidWithCancelledStatus_V8()
            {
                var res = await sectionCoordinationService.GetSection4ByGuidAsync(section3.Guid);
                Assert.AreEqual(section3.Guid, res.Id);
            }
            [TestMethod]
            public async Task SectionService_GetByGuidWithNoStatus_V8()
            {
                var res = await sectionCoordinationService.GetSection4ByGuidAsync(section4.Guid);
                Assert.AreEqual(section4.Guid, res.Id);
            }
            [TestMethod]
            public async Task SectionService_GetAll_V8_WithBadPeriodFilter_ReturnsEmptySet()
            {
                var emptyResult = new Tuple<IEnumerable<Section>, int>(new List<Domain.Student.Entities.Section>(), 0);
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(1, 1, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emptyResult);
                var result = await sectionCoordinationService.GetSections4Async(1, 1, "", "", "", "", "", "", "badperiod", "", "", "", "", "");
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Item1.Count());
                Assert.AreEqual(0, result.Item2);
            }
            [TestMethod]
            public async Task SectionService_GetAll_V8_WithBadSubjectFilter_ReturnsEmptySet()
            {
                var emptyResult = new Tuple<IEnumerable<Section>, int>(new List<Domain.Student.Entities.Section>(), 0);
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(1, 1, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emptyResult);
                var result = await sectionCoordinationService.GetSections4Async(1, 1, "", "", "", "badsubject", "", "", "", "", "", "", "", "");
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Item1.Count());
                Assert.AreEqual(0, result.Item2);
            }

            [TestMethod]
            public async Task SectionService_GetAll_V8_WithBadPlatformFilter_ReturnsEmptySet()
            {
                var emptyResult = new Tuple<IEnumerable<Section>, int>(new List<Domain.Student.Entities.Section>(), 0);
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(1, 1, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emptyResult);
                var result = await sectionCoordinationService.GetSections4Async(1, 1, "", "", "", "", "", "badplatform", "", "", "", "", "", "");
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Item1.Count());
                Assert.AreEqual(0, result.Item2);
            }
            [TestMethod]
            public async Task SectionService_GetAll_V8_WithBadLevelFilter_ReturnsEmptySet()
            {
                var emptyResult = new Tuple<IEnumerable<Section>, int>(new List<Domain.Student.Entities.Section>(), 0);
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(1, 1, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emptyResult);
                var result = await sectionCoordinationService.GetSections4Async(1, 1, "", "", "", "", "", "", "", "badlevel", "", "", "", "");
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Item1.Count());
                Assert.AreEqual(0, result.Item2);
            }
            [TestMethod]
            public async Task SectionService_GetAll_V8_WithBadSiteFilter_ReturnsEmptySet()
            {
                var emptyResult = new Tuple<IEnumerable<Section>, int>(new List<Domain.Student.Entities.Section>(), 0);
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(1, 1, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emptyResult);
                var result = await sectionCoordinationService.GetSections4Async(1, 1, "", "", "", "", "", "", "", "", "", "badsite", "", "");
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Item1.Count());
                Assert.AreEqual(0, result.Item2);
            }
            [TestMethod]
            public async Task SectionService_GetAll_V8_WithBadStatusFilter_ReturnsEmptySet()
            {
                var emptyResult = new Tuple<IEnumerable<Section>, int>(new List<Domain.Student.Entities.Section>(), 0);
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(1, 1, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emptyResult);
                var result = await sectionCoordinationService.GetSections4Async(1, 1, "", "", "", "", "", "", "", "", "", "", "badstatus", "");
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Item1.Count());
                Assert.AreEqual(0, result.Item2);
            }
            [TestMethod]
            public async Task SectionService_GetAll_V8_WithBadOwningOrganizationFilter_ReturnsEmptySet()
            {
                var emptyResult = new Tuple<IEnumerable<Section>, int>(new List<Domain.Student.Entities.Section>(), 0);
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(1, 1, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emptyResult);
                var result = await sectionCoordinationService.GetSections4Async(1, 1, "", "", "", "", "", "", "", "", "", "", "", "badorg");
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Item1.Count());
                Assert.AreEqual(0, result.Item2);
            }

        }

        [TestClass]
        public class Sections_Post_V8 : CurrentUserSetup
        {
            private SectionCoordinationService sectionCoordinationService;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepo;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private IStudentReferenceDataRepository studentReferenceDataRepo;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private Mock<ICourseRepository> courseRepoMock;
            private ICourseRepository courseRepo;
            private Mock<ITermRepository> termRepoMock;
            private ITermRepository termRepo;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private IStudentConfigurationRepository studentConfigRepo;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IRoomRepository> roomRepoMock;
            private IRoomRepository roomRepo;
            private Mock<IEventRepository> eventRepoMock;
            private IEventRepository eventRepo;


            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;

            private Domain.Student.Entities.Section section1;
            private Domain.Student.Entities.Section section2;
            private Domain.Student.Entities.Section section3;
            private Domain.Student.Entities.SectionMeeting meeting1;
            private Domain.Student.Entities.SectionMeeting meeting2;
            private Domain.Student.Entities.SectionMeeting meeting3;
            private Domain.Student.Entities.SectionMeeting meeting4;

            private List<OfferingDepartment> dpts = new List<OfferingDepartment>() { new OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>() { "Whatever" };
            private List<SectionStatusItem> statuses = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses2 = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Inactive, "I", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses3 = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Cancelled, "C", DateTime.Today.AddDays(-60)) };
            private IEnumerable<Domain.Student.Entities.Course> allCourses = new TestCourseRepository().GetAsync().Result;

            private List<CreditCategory> creditCategories = new List<CreditCategory>() { new CreditCategory(Guid.NewGuid().ToString(), "RC", "Regular Credit", CreditType.Institutional),
                new CreditCategory(Guid.NewGuid().ToString(), "RC", "Regular Credit", CreditType.Transfer), new CreditCategory(Guid.NewGuid().ToString(), "RC", "Regular Credit", CreditType.ContinuingEducation ) };
            private List<Domain.Student.Entities.AcademicLevel> academicLevels = new List<Domain.Student.Entities.AcademicLevel>() { new Domain.Student.Entities.AcademicLevel(Guid.NewGuid().ToString(), "UG", "Undergraduate") };
            private List<GradeScheme> gradeSchemes = new List<GradeScheme>() { new GradeScheme(Guid.NewGuid().ToString(), "Pass/Fail", "Pass/Fail grade") };
            private List<Domain.Student.Entities.CourseLevel> courseLevels = new List<Domain.Student.Entities.CourseLevel>() { new Domain.Student.Entities.CourseLevel(Guid.NewGuid().ToString(), "100", "Introduction to terms, concepts, and techniques.") };
            private List<Department> departments = new List<Department>() { new Department(Guid.NewGuid().ToString(), "PHYS", "Physics", true) };
            private List<Location> locations = new List<Location>() { new Location(Guid.NewGuid().ToString(), "MD", "Maryland") };

            private string meeting1Guid = Guid.NewGuid().ToString();
            private string instructionalMethodGuid = Guid.NewGuid().ToString();
            private string roomGuid = Guid.NewGuid().ToString();
            private Domain.Entities.Permission permissionCreateandUpdateSections;
            private Domain.Entities.Permission permissionViewAnyProjects;
            private Domain.Entities.Permission permissionViewAnyProjectsLineItems;

            [TestInitialize]
            public void Initialize()
            {
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepo = sectionRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                roomRepoMock = new Mock<IRoomRepository>();
                roomRepo = roomRepoMock.Object;
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                studentReferenceDataRepo = stuRefDataRepoMock.Object;
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                referenceDataRepo = referenceDataRepoMock.Object;
                courseRepoMock = new Mock<ICourseRepository>();
                courseRepo = courseRepoMock.Object;
                termRepoMock = new Mock<ITermRepository>();
                termRepo = termRepoMock.Object;
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                studentConfigRepo = studentConfigRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
                eventRepoMock = new Mock<IEventRepository>();
                eventRepo = eventRepoMock.Object;


                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;

                // Set Current User
                currentUserFactory = new CurrentUserSetup.ThirdPartyUserFactory();

                logger = new Mock<ILogger>().Object;

                // Mock permissions
                permissionCreateandUpdateSections = new Ellucian.Colleague.Domain.Entities.Permission(StudentPermissionCodes.CreateAndUpdateSection);
                thirdPartyRole.AddPermission(permissionCreateandUpdateSections);
                //permissionViewAnyProjects = new Ellucian.Colleague.Domain.Entities.Permission(StudentPermissionCodes.ViewAnyProjects);
                //thirdPartyRole.AddPermission(permissionViewAnyProjects);
                //permissionViewAnyProjectsLineItems = new Ellucian.Colleague.Domain.Entities.Permission(BasePermissionCodes.ViewAnyProjectsLineItems);
                //thirdPartyRole.AddPermission(permissionViewAnyProjectsLineItems);

                // Mock the section repo response
                section1 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section1.TermId = "2012/FA";
                section1.EndDate = new DateTime(2012, 12, 21);
                section1.Guid = Guid.NewGuid().ToString();

                section2 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, null, "Introduction to Art", "IN", dpts, levels, "UG", statuses2, true);
                section2.TermId = "2012/FA";
                section2.Guid = Guid.NewGuid().ToString();

                section3 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses3, true);
                section3.TermId = "2011/FA";
                section3.EndDate = new DateTime(2011, 12, 21);
                section3.Guid = Guid.NewGuid().ToString();

                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LEC", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting1.Guid = meeting1Guid;
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "DAN*102";
                section1.AddSectionMeeting(meeting1);

                meeting2 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting2.Guid = meeting1Guid;

                meeting3 = new Domain.Student.Entities.SectionMeeting("456", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting3.Guid = meeting1Guid;

                meeting4 = new Domain.Student.Entities.SectionMeeting("789", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting4.Guid = meeting1Guid;

                Term term1 = new Term("termGuid1", "2011/FA", "2011 Fall", new DateTime(2011, 09, 01), new DateTime(2011, 12, 15), 2012,
                    0, false, false, "2011/FA", false);
                var date1 = new List<DateTime?>() { new DateTime(2011, 09, 15) };
                var regDate = new RegistrationDate("Loc1", null, null, null, null, null, null, null, null, null, date1);
                term1.AddRegistrationDates(regDate);
                Term term2 = new Term("termGuid2", "2012/FA", "2012 Fall", new DateTime(2012, 09, 01), new DateTime(2012, 12, 15), 2013,
                    0, false, false, "2012/FA", false);
                regDate = new RegistrationDate("Loc2", null, null, null, null, null, null, null, null, null, new List<DateTime?>());
                term2.AddRegistrationDates(regDate);
                termRepoMock.Setup(repo => repo.Get("2011/FA")).Returns(term1);
                termRepoMock.Setup(repo => repo.Get("2012/FA")).Returns(term2);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).ReturnsAsync(section1);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section2.Guid)).ReturnsAsync(section2);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section3.Guid)).ReturnsAsync(section3);
                sectionRepoMock.Setup(repo => repo.PostSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting1.Guid)).ReturnsAsync(meeting1);
                sectionRepoMock.Setup(repo => repo.PostSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting2.Guid)).ReturnsAsync(meeting2);
                sectionRepoMock.Setup(repo => repo.PostSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting3.Guid)).ReturnsAsync(meeting3);
                courseRepoMock.Setup(repo => repo.GetAsync()).Returns(Task.FromResult(allCourses));
                var course_temp = allCourses.Where(c => c.Id == "342").FirstOrDefault();
                courseRepoMock.Setup(repo => repo.GetCourseByGuidAsync(It.IsAny<String>())).ReturnsAsync(course_temp);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(creditCategories);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(academicLevels);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(gradeSchemes);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(courseLevels);
                referenceDataRepoMock.Setup(repo => repo.DepartmentsAsync()).ReturnsAsync(departments);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns(locations);

                // Configuration defaults and campus calendar information.
                configRepoMock.Setup(repo => repo.GetDefaultsConfiguration()).Returns(new DefaultsConfiguration() { CampusCalendarId = "MAIN" });
                studentConfigRepoMock.Setup(repo => repo.GetCurriculumConfigurationAsync()).ReturnsAsync(new CurriculumConfiguration() { SectionActiveStatusCode = "A", SectionInactiveStatusCode = "I", DefaultInstructionalMethodCode = "INT" });
                eventRepoMock.Setup(repo => repo.GetCalendar("MAIN")).Returns(new CampusCalendar("MAIN", "Default Calendar", new DateTimeOffset(), new DateTimeOffset()));

                var sectionStatusCodes = new List<SectionStatusCode>()
                {
                    new SectionStatusCode("P", "Pending", SectionStatus.Active, SectionStatusIntegration.Pending),
                    new SectionStatusCode("C", "Closed", SectionStatus.Active, SectionStatusIntegration.Closed),
                    new SectionStatusCode("C", "Cancelled", SectionStatus.Active, SectionStatusIntegration.Cancelled),
                    new SectionStatusCode("O", "Open", SectionStatus.Active, SectionStatusIntegration.Open),
                };

                stuRefDataRepoMock.Setup(repo => repo.GetSectionStatusCodesAsync()).ReturnsAsync(sectionStatusCodes);


                // Mock the section service
                sectionCoordinationService = new SectionCoordinationService(adapterRegistry, sectionRepo, courseRepo, studentRepo, studentReferenceDataRepo,
                    referenceDataRepo, termRepo, studentConfigRepo, configRepo, personRepo, roomRepo, eventRepo, currentUserFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionRepo = null;
                studentRepo = null;
                roleRepo = null;
                adapterRegistry = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_Null_Object_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var r = await sectionCoordinationService.PostSection4Async(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_Null_Guid_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var r = await sectionCoordinationService.PostSection4Async(new Dtos.Section4());
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_SectionKeyNotFound_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).Throws(new KeyNotFoundException());
                var r = await sectionCoordinationService.PostSection4Async(new Dtos.Section4()
                {
                    Id = meeting1.Guid
                });
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_NoPermission_V8()
            {
                var section = new Dtos.Section4() { Id = "999999999999999" };
                var r = await sectionCoordinationService.PostSection4Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullCourse_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                //var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                //    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = null,
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection4Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_EmptyTitle_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection4Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullStartOn_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = null,
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection4Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_StartOnGreaterThanEndOn_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2010, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection4Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditMeasure_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection4Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditDetailAndCreditType_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = null }, Measure = new Dtos.CreditMeasure2(), Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection4Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditMinimum_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2010, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection4Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditType_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2(),
                    Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Status = Dtos.SectionStatus2.Open,
                    Credits = credit,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection4Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullInstructionalPlaforms_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                referenceDataRepoMock.Setup(repo => repo.GetInstructionalPlatformsAsync(It.IsAny<bool>())).ReturnsAsync(null);
                //var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty(), 
                //Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Status = Dtos.SectionStatus2.Open,
                    AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2(academicLevels[0].Guid) },
                    //Credits = credit,
                    InstructionalPlatform = new Dtos.GuidObject2("jfhjdhjs"),
                    //OwningOrganizations = new List<Dtos.OfferingOrganization2>() { 
                    //    new Dtos.OfferingOrganization2() {Organization = new Dtos.GuidObject2(departments[0].Guid) } },
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection4Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditCategoryOfCredit_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() { Measure = Dtos.CreditMeasure2.CEU,
                    Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection4Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditCategories_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section2);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection4Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullGradeSchemes_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section3);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section3);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    /*Detail = new Dtos.GuidObject2(creditCategories[0].Guid),*/ CreditType = Dtos.EnumProperties.CreditCategoryType3.Institutional }, Measure = Dtos.CreditMeasure2.Credit, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection4Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullAcademicLevels_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    /*Detail = new Dtos.GuidObject2(creditCategories[0].Guid),*/ CreditType = Dtos.EnumProperties.CreditCategoryType3.Transfer }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection4Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullLocations_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section2);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section2);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns<Location>(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    /*Detail = new Dtos.GuidObject2(creditCategories[0].Guid),*/ CreditType = Dtos.EnumProperties.CreditCategoryType3.ContinuingEducation }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PostSection4Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCourseLevels_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PostSection4Async(section2);
            }

            [TestMethod]
            public async Task SectionService_Post_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PostSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid), CreditType = Dtos.EnumProperties.CreditCategoryType3.Institutional },
                    Measure = Dtos.CreditMeasure2.Credit, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = section1.Guid,
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var res = await sectionCoordinationService.PostSection4Async(section2);
                Assert.AreEqual(section1.Guid, res.Id);
            }
        }

        [TestClass]
        public class Sections_Put_V8 : CurrentUserSetup
        {
            private SectionCoordinationService sectionCoordinationService;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private Mock<ISectionRepository> sectionRepoMock;
            private ISectionRepository sectionRepo;
            private Mock<IStudentRepository> studentRepoMock;
            private IStudentRepository studentRepo;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private IStudentReferenceDataRepository studentReferenceDataRepo;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private IReferenceDataRepository referenceDataRepo;
            private Mock<ICourseRepository> courseRepoMock;
            private ICourseRepository courseRepo;
            private Mock<ITermRepository> termRepoMock;
            private ITermRepository termRepo;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private IStudentConfigurationRepository studentConfigRepo;
            private Mock<IConfigurationRepository> configRepoMock;
            private IConfigurationRepository configRepo;
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IRoomRepository> roomRepoMock;
            private IRoomRepository roomRepo;
            private Mock<IEventRepository> eventRepoMock;
            private IEventRepository eventRepo;


            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ILogger logger;
            private ICurrentUserFactory currentUserFactory;

            private Domain.Student.Entities.Section section1;
            private Domain.Student.Entities.Section section2;
            private Domain.Student.Entities.Section section3;
            private Domain.Student.Entities.SectionMeeting meeting1;
            private Domain.Student.Entities.SectionMeeting meeting2;
            private Domain.Student.Entities.SectionMeeting meeting3;
            private Domain.Student.Entities.SectionMeeting meeting4;

            private List<OfferingDepartment> dpts = new List<OfferingDepartment>() { new OfferingDepartment("ART", 100m) };
            private List<string> levels = new List<string>() { "Whatever" };
            private List<SectionStatusItem> statuses = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses2 = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Inactive, "I", DateTime.Today.AddDays(-60)) };
            private List<SectionStatusItem> statuses3 = new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Cancelled, "C", DateTime.Today.AddDays(-60)) };
            private IEnumerable<Domain.Student.Entities.Course> allCourses = new TestCourseRepository().GetAsync().Result;

            private List<CreditCategory> creditCategories = new List<CreditCategory>() { new CreditCategory(Guid.NewGuid().ToString(), "RC", "Regular Credit", CreditType.Institutional),
                new CreditCategory(Guid.NewGuid().ToString(), "RC", "Regular Credit", CreditType.Transfer), new CreditCategory(Guid.NewGuid().ToString(), "RC", "Regular Credit", CreditType.ContinuingEducation ) };
            private List<Domain.Student.Entities.AcademicLevel> academicLevels = new List<Domain.Student.Entities.AcademicLevel>() { new Domain.Student.Entities.AcademicLevel(Guid.NewGuid().ToString(), "UG", "Undergraduate") };
            private List<GradeScheme> gradeSchemes = new List<GradeScheme>() { new GradeScheme(Guid.NewGuid().ToString(), "Pass/Fail", "Pass/Fail grade") };
            private List<Domain.Student.Entities.CourseLevel> courseLevels = new List<Domain.Student.Entities.CourseLevel>() { new Domain.Student.Entities.CourseLevel(Guid.NewGuid().ToString(), "100", "Introduction to terms, concepts, and techniques.") };
            private List<Department> departments = new List<Department>() { new Department(Guid.NewGuid().ToString(), "PHYS", "Physics", true) };
            private List<Location> locations = new List<Location>() { new Location(Guid.NewGuid().ToString(), "MD", "Maryland") };

            private string meeting1Guid = Guid.NewGuid().ToString();
            private string instructionalMethodGuid = Guid.NewGuid().ToString();
            private string roomGuid = Guid.NewGuid().ToString();
            private Domain.Entities.Permission permissionCreateandUpdateSections;
            private Domain.Entities.Permission permissionViewAnyProjects;
            private Domain.Entities.Permission permissionViewAnyProjectsLineItems;

            [TestInitialize]
            public void Initialize()
            {
                sectionRepoMock = new Mock<ISectionRepository>();
                sectionRepo = sectionRepoMock.Object;
                studentRepoMock = new Mock<IStudentRepository>();
                studentRepo = studentRepoMock.Object;
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                roomRepoMock = new Mock<IRoomRepository>();
                roomRepo = roomRepoMock.Object;
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                studentReferenceDataRepo = stuRefDataRepoMock.Object;
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                referenceDataRepo = referenceDataRepoMock.Object;
                courseRepoMock = new Mock<ICourseRepository>();
                courseRepo = courseRepoMock.Object;
                termRepoMock = new Mock<ITermRepository>();
                termRepo = termRepoMock.Object;
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                studentConfigRepo = studentConfigRepoMock.Object;
                configRepoMock = new Mock<IConfigurationRepository>();
                configRepo = configRepoMock.Object;
                eventRepoMock = new Mock<IEventRepository>();
                eventRepo = eventRepoMock.Object;


                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;

                // Set Current User
                currentUserFactory = new CurrentUserSetup.ThirdPartyUserFactory();

                logger = new Mock<ILogger>().Object;

                // Mock permissions
                permissionCreateandUpdateSections = new Ellucian.Colleague.Domain.Entities.Permission(StudentPermissionCodes.CreateAndUpdateSection);
                thirdPartyRole.AddPermission(permissionCreateandUpdateSections);
                //permissionViewAnyProjects = new Ellucian.Colleague.Domain.Entities.Permission(StudentPermissionCodes.ViewAnyProjects);
                //thirdPartyRole.AddPermission(permissionViewAnyProjects);
                //permissionViewAnyProjectsLineItems = new Ellucian.Colleague.Domain.Entities.Permission(BasePermissionCodes.ViewAnyProjectsLineItems);
                //thirdPartyRole.AddPermission(permissionViewAnyProjectsLineItems);

                // Mock the section repo response
                section1 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC1", "1119", "01", new DateTime(2012, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses, true);
                section1.TermId = "2012/FA";
                section1.EndDate = new DateTime(2012, 12, 21);
                section1.Guid = Guid.NewGuid().ToString();

                section2 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC2", "1119", "02", new DateTime(2012, 09, 01), 3.0m, null, "Introduction to Art", "IN", dpts, levels, "UG", statuses2, true);
                section2.TermId = "2012/FA";
                section2.Guid = Guid.NewGuid().ToString();

                section3 = new Ellucian.Colleague.Domain.Student.Entities.Section("SEC3", "1119", "03", new DateTime(2011, 09, 01), 3.0m, 0, "Introduction to Art", "IN", dpts, levels, "UG", statuses3, true);
                section3.TermId = "2011/FA";
                section3.EndDate = new DateTime(2011, 12, 21);
                section3.Guid = Guid.NewGuid().ToString();

                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LEC", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting1.Guid = meeting1Guid;
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "DAN*102";
                section1.AddSectionMeeting(meeting1);

                meeting2 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting2.Guid = meeting1Guid;

                meeting3 = new Domain.Student.Entities.SectionMeeting("456", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting3.Guid = meeting1Guid;

                meeting4 = new Domain.Student.Entities.SectionMeeting("789", "SEC1", "LEC", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting4.Guid = meeting1Guid;

                Term term1 = new Term("termGuid1", "2011/FA", "2011 Fall", new DateTime(2011, 09, 01), new DateTime(2011, 12, 15), 2012,
                    0, false, false, "2011/FA", false);
                var date1 = new List<DateTime?>() { new DateTime(2011, 09, 15) };
                var regDate = new RegistrationDate("Loc1", null, null, null, null, null, null, null, null, null, date1);
                term1.AddRegistrationDates(regDate);
                Term term2 = new Term("termGuid2", "2012/FA", "2012 Fall", new DateTime(2012, 09, 01), new DateTime(2012, 12, 15), 2013,
                    0, false, false, "2012/FA", false);
                regDate = new RegistrationDate("Loc2", null, null, null, null, null, null, null, null, null, new List<DateTime?>());
                term2.AddRegistrationDates(regDate);
                termRepoMock.Setup(repo => repo.Get("2011/FA")).Returns(term1);
                termRepoMock.Setup(repo => repo.Get("2012/FA")).Returns(term2);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).ReturnsAsync(section1);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section2.Guid)).ReturnsAsync(section2);
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section3.Guid)).ReturnsAsync(section3);
                sectionRepoMock.Setup(repo => repo.PutSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting1.Guid)).ReturnsAsync(meeting1);
                sectionRepoMock.Setup(repo => repo.PutSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting2.Guid)).ReturnsAsync(meeting2);
                sectionRepoMock.Setup(repo => repo.PutSectionMeetingAsync(It.IsAny<Domain.Student.Entities.Section>(), meeting3.Guid)).ReturnsAsync(meeting3);
                courseRepoMock.Setup(repo => repo.GetAsync()).Returns(Task.FromResult(allCourses));
                var course_temp = allCourses.Where(c => c.Id == "342").FirstOrDefault();
                courseRepoMock.Setup(repo => repo.GetCourseByGuidAsync(It.IsAny<String>())).ReturnsAsync(course_temp);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(creditCategories);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(academicLevels);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(gradeSchemes);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(courseLevels);
                referenceDataRepoMock.Setup(repo => repo.DepartmentsAsync()).ReturnsAsync(departments);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns(locations);

                // Configuration defaults and campus calendar information.
                configRepoMock.Setup(repo => repo.GetDefaultsConfiguration()).Returns(new DefaultsConfiguration() { CampusCalendarId = "MAIN" });
                studentConfigRepoMock.Setup(repo => repo.GetCurriculumConfigurationAsync()).ReturnsAsync(new CurriculumConfiguration() { SectionActiveStatusCode = "A", SectionInactiveStatusCode = "I", DefaultInstructionalMethodCode = "INT" });
                eventRepoMock.Setup(repo => repo.GetCalendar("MAIN")).Returns(new CampusCalendar("MAIN", "Default Calendar", new DateTimeOffset(), new DateTimeOffset()));

                var sectionStatusCodes = new List<SectionStatusCode>()
                {
                    new SectionStatusCode("P", "Pending", SectionStatus.Active, SectionStatusIntegration.Pending),
                    new SectionStatusCode("C", "Closed", SectionStatus.Active, SectionStatusIntegration.Closed),
                    new SectionStatusCode("C", "Cancelled", SectionStatus.Active, SectionStatusIntegration.Cancelled),
                    new SectionStatusCode("O", "Open", SectionStatus.Active, SectionStatusIntegration.Open),
                };

                stuRefDataRepoMock.Setup(repo => repo.GetSectionStatusCodesAsync()).ReturnsAsync(sectionStatusCodes);


                // Mock the section service
                sectionCoordinationService = new SectionCoordinationService(adapterRegistry, sectionRepo, courseRepo, studentRepo, studentReferenceDataRepo,
                    referenceDataRepo, termRepo, studentConfigRepo, configRepo, personRepo, roomRepo, eventRepo, currentUserFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                sectionRepo = null;
                studentRepo = null;
                roleRepo = null;
                adapterRegistry = null;
                sectionCoordinationService = null;
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_Null_Object_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var r = await sectionCoordinationService.PutSection4Async(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_Null_Guid_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var r = await sectionCoordinationService.PutSection4Async(new Dtos.Section4());
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_SectionKeyNotFound_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(section1.Guid)).Throws(new KeyNotFoundException());
                var r = await sectionCoordinationService.PutSection4Async(new Dtos.Section4()
                {
                    Id = meeting1.Guid
                });
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SectionService_NoPermission_V8()
            {
                var section = new Dtos.Section4() { Id = "999999999999999" };
                var r = await sectionCoordinationService.PutSection4Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullCourse_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                //var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty() { 
                //    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = null,
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection4Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_EmptyTitle_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection4Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionService_NullStartOn_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = null,
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection4Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_StartOnGreaterThanEndOn_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2010, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection4Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditMeasure_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection4Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditDetailAndCreditType_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = null }, Measure = new Dtos.CreditMeasure2(), Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection4Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditMinimum_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2010, 09, 01),
                    Title = "MATH",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection4Async(section2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditType_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2(),
                    Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Status = Dtos.SectionStatus2.Open,
                    Credits = credit,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection4Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullInstructionalPlaforms_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                referenceDataRepoMock.Setup(repo => repo.GetInstructionalPlatformsAsync(It.IsAny<bool>())).ReturnsAsync(null);
                //var credit = new List<Dtos.Credit2>() { new Dtos.Credit2() {CreditCategory = new Dtos.CreditIdAndTypeProperty(), 
                //Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Status = Dtos.SectionStatus2.Open,
                    AcademicLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2(academicLevels[0].Guid) },
                    //Credits = credit,
                    InstructionalPlatform = new Dtos.GuidObject2("jfhjdhjs"),
                    //OwningOrganizations = new List<Dtos.OfferingOrganization2>() { 
                    //    new Dtos.OfferingOrganization2() {Organization = new Dtos.GuidObject2(departments[0].Guid) } },
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection4Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditCategoryOfCredit_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() { Measure = Dtos.CreditMeasure2.CEU,
                    Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection4Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCreditCategories_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section2);
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection4Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullGradeSchemes_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section3);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section3);
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    /*Detail = new Dtos.GuidObject2(creditCategories[0].Guid),*/ CreditType = Dtos.EnumProperties.CreditCategoryType3.Institutional }, Measure = Dtos.CreditMeasure2.Credit, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection4Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullAcademicLevels_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section1);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    /*Detail = new Dtos.GuidObject2(creditCategories[0].Guid),*/ CreditType = Dtos.EnumProperties.CreditCategoryType3.Transfer }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection4Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullLocations_V8()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<String>())).ReturnsAsync(section2);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section2);
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns<Location>(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() { 
                    /*Detail = new Dtos.GuidObject2(creditCategories[0].Guid),*/ CreditType = Dtos.EnumProperties.CreditCategoryType3.ContinuingEducation }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    EndOn = new DateTime(2011, 12, 14),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open,
                    CourseLevels = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("sjdhd7392") }
                };
                var r = await sectionCoordinationService.PutSection4Async(section);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task SectionService_NullCourseLevels_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(null);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid) }, Measure = Dtos.CreditMeasure2.CEU, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = "999999999999999",
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var r = await sectionCoordinationService.PutSection4Async(section2);
            }

            [TestMethod]
            public async Task SectionService_Put_V8()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { thirdPartyRole });
                sectionRepoMock.Setup(repo => repo.PutSectionAsync(It.IsAny<Domain.Student.Entities.Section>())).ReturnsAsync(section1);
                var credit = new List<Dtos.DtoProperties.SectionCreditDtoProperty>() { new Dtos.DtoProperties.SectionCreditDtoProperty() {CreditCategory = new Dtos.DtoProperties.CreditIdAndTypeProperty2() {
                    Detail = new Dtos.GuidObject2(creditCategories[0].Guid), CreditType = Dtos.EnumProperties.CreditCategoryType3.Institutional },
                    Measure = Dtos.CreditMeasure2.Credit, Minimum = 1.0m, Maximum = 4.0m, Increment = 1.0m}};
                var section2 = new Dtos.Section4()
                {
                    Id = section1.Guid,
                    Number = "01",
                    StartOn = new DateTime(2011, 09, 01),
                    Title = "Modern Algebra",
                    Course = new Dtos.GuidObject2("342"),
                    Credits = credit,
                    Status = Dtos.SectionStatus2.Open
                };
                var res = await sectionCoordinationService.PutSection4Async(section2);
                Assert.AreEqual(section1.Guid, res.Id);
            }
        }

        [TestClass]
        public class SectionCoordinationServiceTests_V8 : CurrentUserSetup
        {
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private Mock<ISectionRepository> sectionRepoMock;
            private Mock<IStudentRepository> studentRepoMock;
            private Mock<IStudentReferenceDataRepository> stuRefDataRepoMock;
            private Mock<IReferenceDataRepository> referenceDataRepoMock;
            private Mock<ICourseRepository> courseRepoMock;
            private Mock<ITermRepository> termRepoMock;
            private Mock<IStudentConfigurationRepository> studentConfigRepoMock;
            private Mock<IConfigurationRepository> configRepoMock;
            private Mock<IPersonRepository> personRepoMock;
            private Mock<IRoomRepository> roomRepoMock;
            private Mock<IEventRepository> eventRepoMock;
            private Mock<IRoleRepository> roleRepoMock;
            private Mock<ILogger> loggerMock;
            private ICurrentUserFactory currentUserFactory;


            private SectionCoordinationService sectionCoordinationService;
            Tuple<IEnumerable<Domain.Student.Entities.Section>, int> sectionMaxEntitiesTuple;
            IEnumerable<Domain.Student.Entities.Section> sectionEntities;

            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Course> courseEntities;
            IEnumerable<Ellucian.Colleague.Domain.Student.Entities.Subject> subjectEntities;
            IEnumerable<Domain.Student.Entities.CreditCategory> creditCategoryEntities;
            IEnumerable<Domain.Student.Entities.GradeScheme> gradeSchemeEntities;
            IEnumerable<Domain.Student.Entities.CourseLevel> courseLevelEntities;
            IEnumerable<Domain.Student.Entities.AcademicPeriod> acadPeriodsEntities;
            IEnumerable<Domain.Student.Entities.AcademicLevel> academicLevelEntities;
            IEnumerable<Domain.Base.Entities.InstructionalPlatform> instructionalPlatformEntities;
            IEnumerable<Domain.Base.Entities.Location> locationEntities;
            IEnumerable<Domain.Base.Entities.Department> departmentEntities;
            IEnumerable<Domain.Student.Entities.InstructionalMethod> instrMethodEntities;
            IEnumerable<Domain.Base.Entities.Room> roomEntities;

            Domain.Base.Entities.Person person;
            private Domain.Student.Entities.SectionMeeting meeting1;


            [TestInitialize]
            public void Initialize()
            {
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                sectionRepoMock = new Mock<ISectionRepository>();
                courseRepoMock = new Mock<ICourseRepository>();
                studentRepoMock = new Mock<IStudentRepository>();
                stuRefDataRepoMock = new Mock<IStudentReferenceDataRepository>();
                referenceDataRepoMock = new Mock<IReferenceDataRepository>();
                termRepoMock = new Mock<ITermRepository>();
                studentConfigRepoMock = new Mock<IStudentConfigurationRepository>();
                configRepoMock = new Mock<IConfigurationRepository>();
                personRepoMock = new Mock<IPersonRepository>();
                roomRepoMock = new Mock<IRoomRepository>();
                eventRepoMock = new Mock<IEventRepository>();
                roleRepoMock = new Mock<IRoleRepository>();
                loggerMock = new Mock<ILogger>();

                currentUserFactory = new CurrentUserSetup.StudentUserFactory();
                BuildData();

                sectionCoordinationService = new SectionCoordinationService(adapterRegistryMock.Object, sectionRepoMock.Object, courseRepoMock.Object, studentRepoMock.Object,
                    stuRefDataRepoMock.Object, referenceDataRepoMock.Object, termRepoMock.Object, studentConfigRepoMock.Object, configRepoMock.Object, personRepoMock.Object,
                    roomRepoMock.Object, eventRepoMock.Object, currentUserFactory, roleRepoMock.Object, loggerMock.Object);

            }

            [TestCleanup]
            public void Cleanup()
            {
                adapterRegistryMock = null;
                sectionRepoMock = null;
                courseRepoMock = null;
                studentRepoMock = null;
                stuRefDataRepoMock = null;
                referenceDataRepoMock = null;
                termRepoMock = null;
                studentConfigRepoMock = null;
                configRepoMock = null;
                personRepoMock = null;
                roomRepoMock = null;
                eventRepoMock = null;
                roleRepoMock = null;
                sectionCoordinationService = null;
                courseEntities = null;
                subjectEntities = null;
                creditCategoryEntities = null;
                gradeSchemeEntities = null;
                courseLevelEntities = null;
                acadPeriodsEntities = null;
                academicLevelEntities = null;
                instructionalPlatformEntities = null;
                locationEntities = null;
                departmentEntities = null;
                instrMethodEntities = null;
                roomEntities = null;
                person = null;
                meeting1 = null;
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionsMaximum8Async_ValidateAllFilters()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);

                var results = await sectionCoordinationService.GetSectionsMaximum3Async(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");

                Assert.IsNotNull(results);
                Assert.AreEqual(0, results.Item1.Count());
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximumByGuid3Async_CE()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", "C", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid3Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximumByGuid3Async_Transfer()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", "T", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid3Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximumByGuid3Async_Exchange()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", "E", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid3Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximumByGuid3Async_Other()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", "O", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid3Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximumByGuid3Async_NoCredit()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", "N", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid3Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximumByGuid3Async_WithId_CreditTypeCode_Blank()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", " ", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid3Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximumByGuid3Async_WithId_No_CEUS()
            {
                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, null, "Title 1", "I", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG"
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid3Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximumByGuid3Async_WithId_Freq_Daily()
            {
                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LG", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LG", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "D");
                meeting1.Guid = "1f1485cb-2705-4b59-9f51-7aa85c79b377";
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "COE*Room 1";
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 0, "1", "", "", "", "", "", "", "")).ReturnsAsync(tuple);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid3Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
                Assert.AreEqual(Dtos.FrequencyType2.Daily, result.InstructionalEvents.ToList()[0].Recurrence.RepeatRule.Type);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximumByGuid3Async_WithId_Freq_Monthly_WithDays()
            {
                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LG", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LG", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting1.Days = new List<DayOfWeek>() { DayOfWeek.Monday, DayOfWeek.Wednesday };
                meeting1.Guid = "1f1485cb-2705-4b59-9f51-7aa85c79b377";
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "COE*Room 1";
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 0, "1", "", "", "", "", "", "", "")).ReturnsAsync(tuple);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid3Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
                Assert.AreEqual(Dtos.FrequencyType2.Monthly, result.InstructionalEvents.ToList()[0].Recurrence.RepeatRule.Type);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximumByGuid3Async_WithId_Freq_Monthly_WithoutDays()
            {
                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LG", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LG", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "M");
                meeting1.Guid = "1f1485cb-2705-4b59-9f51-7aa85c79b377";
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "COE*Room 1";
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 0, "1", "", "", "", "", "", "", "")).ReturnsAsync(tuple);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid3Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
                Assert.AreEqual(Dtos.FrequencyType2.Monthly, result.InstructionalEvents.ToList()[0].Recurrence.RepeatRule.Type);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximumByGuid3Async_WithId_Freq_Yearly()
            {
                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LG", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LG", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "Y");
                meeting1.Guid = "1f1485cb-2705-4b59-9f51-7aa85c79b377";
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "COE*Room 1";
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 0, "1", "", "", "", "", "", "", "")).ReturnsAsync(tuple);

                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid3Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
                Assert.AreEqual(Dtos.FrequencyType2.Yearly, result.InstructionalEvents.ToList()[0].Recurrence.RepeatRule.Type);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximumByGuid3Async_WithId()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(sectionEntities.ToList()[0]);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid3Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");

                Assert.IsNotNull(result);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximum3Async_WithBadPeriodFilter_ReturnsEmptySet()
            {
                var emptyResult = new Tuple<IEnumerable<Section>, int>(new List<Domain.Student.Entities.Section>(), 0);
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(1, 1, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emptyResult);
                var result = await sectionCoordinationService.GetSectionsMaximum3Async(1, 1, "", "", "", "", "", "", "badperiod", "", "", "", "", "");
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Item1.Count());
                Assert.AreEqual(0, result.Item2);
            }
            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximum3Async_WithBadSubjectFilter_ReturnsEmptySet()
            {
                var emptyResult = new Tuple<IEnumerable<Section>, int>(new List<Domain.Student.Entities.Section>(), 0);
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(1, 1, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emptyResult);
                var result = await sectionCoordinationService.GetSectionsMaximum3Async(1, 1, "", "", "", "badsubject", "", "", "", "", "", "", "", "");
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Item1.Count());
                Assert.AreEqual(0, result.Item2);
            }

            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximum3Async_WithBadPlatformFilter_ReturnsEmptySet()
            {
                var emptyResult= new Tuple<IEnumerable<Section>, int>(new List<Domain.Student.Entities.Section>(),0);
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(1, 1, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emptyResult);
                var result = await sectionCoordinationService.GetSectionsMaximum3Async(1, 1, "", "", "", "", "", "badplatform", "", "", "", "", "", "");
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Item1.Count());
                Assert.AreEqual(0, result.Item2);
            }
            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximum3Async_WithBadLevelFilter_ReturnsEmptySet()
            {
                var emptyResult = new Tuple<IEnumerable<Section>, int>(new List<Domain.Student.Entities.Section>(), 0);
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(1, 1, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emptyResult);
                var result = await sectionCoordinationService.GetSectionsMaximum3Async(1, 1, "", "", "", "", "", "", "", "badlevel", "", "", "", "");
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Item1.Count());
                Assert.AreEqual(0, result.Item2);
            }
            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximum3Async_WithBadSiteFilter_ReturnsEmptySet()
            {
                var emptyResult = new Tuple<IEnumerable<Section>, int>(new List<Domain.Student.Entities.Section>(), 0);
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(1, 1, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emptyResult);
                var result = await sectionCoordinationService.GetSectionsMaximum3Async(1, 1, "", "", "", "", "", "", "", "", "", "badsite", "", "");
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Item1.Count());
                Assert.AreEqual(0, result.Item2);
            }
            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximum3Async_WithBadStatusFilter_ReturnsEmptySet()
            {
                var emptyResult = new Tuple<IEnumerable<Section>, int>(new List<Domain.Student.Entities.Section>(), 0);
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(1, 1, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emptyResult);
                var result = await sectionCoordinationService.GetSectionsMaximum3Async(1, 1, "", "", "", "", "", "", "", "", "", "", "badstatus", "");
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Item1.Count());
                Assert.AreEqual(0, result.Item2);
            }
            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximum3Async_WithBadOwningOrganizationFilter_ReturnsEmptySet()
            {
                var emptyResult = new Tuple<IEnumerable<Section>, int>(new List<Domain.Student.Entities.Section>(), 0);
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(1, 1, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emptyResult);
                var result = await sectionCoordinationService.GetSectionsMaximum3Async(1, 1, "", "", "", "", "", "", "", "", "", "", "", "badorg");
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Item1.Count());
                Assert.AreEqual(0, result.Item2);
            }
            [TestMethod]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximum3Async_WithBadCourseFilter_ReturnsEmptySet()
            {
                var emptyResult = new Tuple<IEnumerable<Section>, int>(new List<Domain.Student.Entities.Section>(), 0);
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(1, 1, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(emptyResult);
                var result = await sectionCoordinationService.GetSectionsMaximum3Async(1, 1, "", "", "", "", "", "", "", "", "badcourse", "", "", "");
                Assert.IsNotNull(result);
                Assert.AreEqual(0, result.Item1.Count());
                Assert.AreEqual(0, result.Item2);
            }
            #region All Exceptions

            [TestMethod]
            //[ExpectedException(typeof(ArgumentException))]
            public async Task SectionCoordinationServiceTests_V8_GetSectionsMaximum3Async_NullSectionGuid_ArgumentException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);
                sectionRepoMock.Setup(repo => repo.GetSectionGuidFromIdAsync(It.IsAny<string>())).ThrowsAsync(new ArgumentException());

                var results = await sectionCoordinationService.GetSectionsMaximum3Async(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
                Assert.AreEqual(0, results.Item1.Count());
                Assert.AreEqual(0, results.Item2);
            }

            [TestMethod]
            //[ExpectedException(typeof(ArgumentException))]
            public async Task SectionCoordinationServiceTests_V8_GetSectionsMaximum3Async_NullInstrMethodGuid_ArgumentException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);
                stuRefDataRepoMock.Setup(repo => repo.GetInstructionalMethodsAsync()).ThrowsAsync(new ArgumentException());

                var results = await sectionCoordinationService.GetSectionsMaximum3Async(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
                Assert.AreEqual(0, results.Item1.Count());
                Assert.AreEqual(0, results.Item2);
            }

            [TestMethod]
            //[ExpectedException(typeof(ArgumentException))]
            public async Task SectionCoordinationServiceTests_V8_GetSectionsMaximum3Async_RepeatCodeNull_ArgumentException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);

                meeting1.Frequency = "abc";

                var results = await sectionCoordinationService.GetSectionsMaximum3Async(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
                Assert.AreEqual(0, results.Item1.Count());
                Assert.AreEqual(0, results.Item2);
            }

            [TestMethod]
            //[ExpectedException(typeof(ArgumentException))]
            public async Task SectionCoordinationServiceTests_V8_GetSectionsMaximum3Async_GetPersonGuidFromIdAsync_Error_ArgumentException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);

                personRepoMock.Setup(repo => repo.GetPersonGuidFromIdAsync(It.IsAny<string>())).ThrowsAsync(new ArgumentException());

                var results = await sectionCoordinationService.GetSectionsMaximum3Async(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
                Assert.IsNotNull(results);
                Assert.AreEqual(0, results.Item2);
            }

            [TestMethod]
            //[ExpectedException(typeof(RepositoryException))]
            public async Task SectionCoordinationServiceTests_V8_GetSectionsMaximum3Async_NullCourse_RepositoryException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionsAsync(0, 3, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(),
                    It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(sectionMaxEntitiesTuple);
                courseRepoMock.Setup(repo => repo.GetCourseByGuidAsync(It.IsAny<string>())).ReturnsAsync(null);

                var results = await sectionCoordinationService.GetSectionsMaximum3Async(0, 3, It.IsAny<string>(), "2016-01-01", "2016-10-01", It.IsAny<string>(), It.IsAny<string>(), "840e72f0-57b9-42a2-ae88-df3c2262fbbc", "8d3fcd4d-cec8-4405-90eb-948392bd0a7e",
                    "558ca14c-718a-4b6e-8d92-77f498034f9f", "38b4330c-befa-435e-81d5-c3ffd52759f2", "b0eba383-5acf-4050-949d-8bb7a17c5012", "open", "1a2e2906-d46b-4698-80f6-af87b8083c64");
                Assert.IsNotNull(results);
                Assert.AreEqual(0, results.Item2);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximumByGuid3Async_WithNullId_ArgumentNullException()
            {
                var result = await sectionCoordinationService.GetSectionMaximumByGuid3Async("");
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task SectionCoordinationServiceTests_V8_GetSectionMaximumByGuid3Async_WithNullEntity_KeyNotFoundException()
            {
                sectionRepoMock.Setup(repo => repo.GetSectionByGuidAsync(It.IsAny<string>())).ReturnsAsync(null);

                var result = await sectionCoordinationService.GetSectionMaximumByGuid3Async("0b983700-29eb-46ff-8616-21a8c3a48a0c");
            }

            

            #endregion

            private void BuildData()
            {
                //Course entity
                courseEntities = new TestCourseRepository().GetAsync().Result.Take(41);
                var courseEntity = courseEntities.FirstOrDefault(i => i.Id.Equals("180"));
                courseRepoMock.Setup(repo => repo.GetCourseGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(courseEntity.Guid);
                courseRepoMock.Setup(repo => repo.GetCourseByGuidAsync(courseEntity.Guid)).ReturnsAsync(courseEntity);
                //Subject entity
                subjectEntities = new TestSubjectRepository().Get();
                stuRefDataRepoMock.Setup(repo => repo.GetSubjectsAsync()).ReturnsAsync(subjectEntities);
                var subjectEntity = subjectEntities.FirstOrDefault(s => s.Code.Equals(courseEntity.SubjectCode));
                //Credit Categories
                creditCategoryEntities = new List<Domain.Student.Entities.CreditCategory>()
                {
                    new Domain.Student.Entities.CreditCategory("9C3B805D-CFE6-483B-86C3-4C20562F8C15", "I", "Institutional", Domain.Student.Entities.CreditType.Institutional),
                    new Domain.Student.Entities.CreditCategory("73244057-D1EC-4094-A0B7-DE602533E3A6", "C", "Continuing Education", Domain.Student.Entities.CreditType.ContinuingEducation),
                    new Domain.Student.Entities.CreditCategory("73244057-D1EC-4094-A0B7-DE602533E3A6", "E", "Exchange", Domain.Student.Entities.CreditType.Exchange),
                    new Domain.Student.Entities.CreditCategory("73244057-D1EC-4094-A0B7-DE602533E3A6", "O", "Other", Domain.Student.Entities.CreditType.Other),
                    new Domain.Student.Entities.CreditCategory("73244057-D1EC-4094-A0B7-DE602533E3A6", "N", "Continuing Education", Domain.Student.Entities.CreditType.None),
                    new Domain.Student.Entities.CreditCategory("1df164eb-8178-4321-a9f7-24f12d3991d8", "T", "Transfer Credit", Domain.Student.Entities.CreditType.Transfer)
                };
                stuRefDataRepoMock.Setup(repo => repo.GetCreditCategoriesAsync()).ReturnsAsync(creditCategoryEntities);
                //Grade Schemes
                gradeSchemeEntities = new TestStudentReferenceDataRepository().GetGradeSchemesAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.GradeScheme>;
                stuRefDataRepoMock.Setup(repo => repo.GetGradeSchemesAsync()).ReturnsAsync(gradeSchemeEntities);
                //Course Levels
                courseLevelEntities = new List<Domain.Student.Entities.CourseLevel>()
                {
                    new Domain.Student.Entities.CourseLevel("19f6e2cd-1e5f-4485-9b27-60d4f4e4b1ff", "100", "First Yr"),
                    new Domain.Student.Entities.CourseLevel("73244057-D1EC-4094-A0B7-DE602533E3A6", "200", "Second Year"),
                    new Domain.Student.Entities.CourseLevel("1df164eb-8178-4321-a9f7-24f12d3991d8", "300", "Third Year"),
                    new Domain.Student.Entities.CourseLevel("4aa187ae-6d22-4b10-a2e2-1304ebc18176", "400", "Third Year"),
                    new Domain.Student.Entities.CourseLevel("d9f42a0f-39de-44bc-87af-517619141bde", "500", "Third Year")
                };
                stuRefDataRepoMock.Setup(repo => repo.GetCourseLevelsAsync()).ReturnsAsync(courseLevelEntities);

                //IEnumerable<SectionMeeting>
                var sectionFaculty = new Domain.Student.Entities.SectionFaculty("1234", "SEC1", "0000678", "LG", new DateTime(2012, 09, 01), new DateTime(2011, 12, 21), 100.0m);

                meeting1 = new Domain.Student.Entities.SectionMeeting("123", "SEC1", "LG", new DateTime(2012, 01, 01), new DateTime(2012, 12, 21), "W");
                meeting1.Guid = "1f1485cb-2705-4b59-9f51-7aa85c79b377";
                meeting1.AddFacultyId("0000678");
                meeting1.AddSectionFaculty(sectionFaculty);
                meeting1.StartTime = new DateTimeOffset(2012, 01, 01, 08, 30, 00, new TimeSpan());
                meeting1.EndTime = new DateTimeOffset(2012, 12, 21, 09, 30, 00, new TimeSpan());
                meeting1.Room = "COE*Room 1";
                var sectionMeetings = new List<Domain.Student.Entities.SectionMeeting>();
                sectionMeetings.Add(meeting1);
                var tuple = new Tuple<IEnumerable<Domain.Student.Entities.SectionMeeting>, int>(sectionMeetings, 1);
                sectionRepoMock.Setup(repo => repo.GetSectionMeetingAsync(0, 0, "1", "", "", "", "", "", "", "")).ReturnsAsync(tuple);
                sectionRepoMock.Setup(repo => repo.GetSectionGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("ad7655d0-77e3-4f8a-a07c-5c86f6a6f86a");

                //Instructor Id
                personRepoMock.Setup(repo => repo.GetPersonGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("ebf585ad-cc1f-478f-a7a3-aefae87f873a");
                //Instructional Methods
                instrMethodEntities = new List<Domain.Student.Entities.InstructionalMethod>()
                {
                    new Domain.Student.Entities.InstructionalMethod("9C3B805D-CFE6-483B-86C3-4C20562F8C15", "LG", "LG", false),
                    new Domain.Student.Entities.InstructionalMethod("73244057-D1EC-4094-A0B7-DE602533E3A6", "30", "30", true),
                    new Domain.Student.Entities.InstructionalMethod("1df164eb-8178-4321-a9f7-24f12d3991d8", "04", "04", false)
                };
                stuRefDataRepoMock.Setup(repo => repo.GetInstructionalMethodsAsync()).ReturnsAsync(instrMethodEntities);
                //schedule repeats
                var scheduleRepeats = new List<Domain.Base.Entities.ScheduleRepeat>() 
                { 
                    new Domain.Base.Entities.ScheduleRepeat("W", "Weekly", "7", FrequencyType.Weekly),                
                    new Domain.Base.Entities.ScheduleRepeat("D", "Daily", "1", FrequencyType.Daily),                
                    new Domain.Base.Entities.ScheduleRepeat("M", "Monthly", "30", FrequencyType.Monthly),                
                    new Domain.Base.Entities.ScheduleRepeat("Y", "Yearly", "365", FrequencyType.Yearly)
                };
                referenceDataRepoMock.Setup(repo => repo.ScheduleRepeats).Returns(scheduleRepeats);
                //Rooms entities
                roomEntities = new List<Domain.Base.Entities.Room>
                {
                    new Room("2ae6e009-40ca-4ac0-bb41-c123f7c344e3", "COE*Room 1", "COE")
                    {
                        Capacity = 50,
                        RoomType = "110",
                        Name = "Room 1"
                    },
                    new Room("8c92e963-5f05-45a2-8484-d9ad21e6ab47", "COE*0110", "CEE")
                    {
                        Capacity = 100,
                        RoomType = "111",
                        Name = "Room 2"
                    },
                    new Room("8fdbaec7-4198-4348-b95a-a48a357e67f5", "COE*0120", "CDF")
                    {
                        Capacity = 20,
                        RoomType = "111",
                        Name = "Room 13"
                    },
                    new Room("327a6856-0230-4a6d-82ed-5c99dc1b1862", "COE*0121", "CSD")
                    {
                        Capacity = 50,
                        RoomType = "111",
                        Name = "Room 112"
                    },
                    new Room("cc9aa34c-db5e-46dc-9e5b-ba3f4b2557a8", "EIN*0121", "BSF")
                    {
                        Capacity = 30,
                        RoomType = "111",
                        Name = "Room BSF"                    
                    }
                };
                roomRepoMock.Setup(repo => repo.RoomsAsync()).ReturnsAsync(roomEntities);
                //Person
                person = new Domain.Base.Entities.Person("1", "Brown")
                {
                    Guid = "96cf912f-5e87-4099-96bf-73baac4c7715",
                    Prefix = "Mr.",
                    FirstName = "Ricky",
                    MiddleName = "Lee",
                    Suffix = "Jr.",
                    Nickname = "Rick",
                    BirthDate = new DateTime(1930, 1, 1),
                    DeceasedDate = new DateTime(2014, 5, 12),
                    GovernmentId = "111-11-1111",
                    MaritalStatusCode = "M",
                    EthnicCodes = new List<string> { "H" },
                    RaceCodes = new List<string> { "AS" }
                };
                person.AddEmailAddress(new EmailAddress("xyz@xmail.com", "COL"));
                person.AddPersonAlt(new PersonAlt("1", Domain.Base.Entities.PersonAlt.ElevatePersonAltType.ToString()));

                personRepoMock.Setup(repo => repo.GetPersonByGuidNonCachedAsync(It.IsAny<string>())).ReturnsAsync(person);
                // Mock the reference repository for prefix
                referenceDataRepoMock.Setup(repo => repo.Prefixes).Returns(new List<Prefix>()
                { 
                    new Prefix("MR","Mr","Mr."),
                    new Prefix("MS","Ms","Ms."),
                    new Prefix("JR","Jr","Jr."),
                    new Prefix("SR","Sr","Sr.")
                });

                // Mock the reference repository for suffix
                referenceDataRepoMock.Setup(repo => repo.Suffixes).Returns(new List<Suffix>()
                { 
                    new Suffix("JR","Jr","Jr."),
                    new Suffix("SR","Sr","Sr.")
                });

                //Acad Periods
                var registrationDates = new RegistrationDate(null, DateTime.Today.AddYears(1), DateTime.Today.AddYears(1), DateTime.Today.AddYears(1), DateTime.Today.AddYears(1),
                    DateTime.Today.AddYears(1), DateTime.Today.AddYears(1), DateTime.Today.AddYears(1), DateTime.Today.AddYears(1), DateTime.Today.AddYears(1), new List<DateTime?>() { DateTime.Today.AddYears(1) });
                 acadPeriodsEntities = new List<Domain.Student.Entities.AcademicPeriod>() 
                { 
                    new Domain.Student.Entities.AcademicPeriod("8d3fcd4d-cec8-4405-90eb-948392bd0a7e", "2012/FA", "Fall 2014", DateTime.Today.AddDays(-60), DateTime.Today.AddDays(10), 
                        DateTime.Today.Year, 4, "2012/FA", "5f7e7071-5aef-4d22-891f-86ab472a9f15", "edcfd1ee-4adf-46bc-8b87-8853ae49dbeb",new List<RegistrationDate>(){registrationDates} )
                };
                termRepoMock.Setup(repo => repo.GetAcademicPeriods(It.IsAny<IEnumerable<Term>>())).Returns(acadPeriodsEntities);

                academicLevelEntities = new TestAcademicLevelRepository().GetAsync().Result as List<Ellucian.Colleague.Domain.Student.Entities.AcademicLevel>;
                stuRefDataRepoMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(academicLevelEntities);

                locationEntities = new TestLocationRepository().Get();
                referenceDataRepoMock.Setup(repo => repo.Locations).Returns(locationEntities);

                departmentEntities = new TestDepartmentRepository().Get().ToList();
                referenceDataRepoMock.Setup(repo => repo.DepartmentsAsync()).ReturnsAsync(departmentEntities);

                instructionalPlatformEntities = new List<InstructionalPlatform>
                {
                    new InstructionalPlatform("840e72f0-57b9-42a2-ae88-df3c2262fbbc", "CE", "Continuing Education"),
                    new InstructionalPlatform("e986b8a5-25f3-4aa0-bd0e-90982865e749", "D", "Institutional"),
                    new InstructionalPlatform("b5cc288b-8692-474e-91be-bdc55778e2f5", "TR", "Transfer")
                };
                referenceDataRepoMock.Setup(repo => repo.GetInstructionalPlatformsAsync(It.IsAny<bool>())).ReturnsAsync(instructionalPlatformEntities);

                sectionRepoMock.Setup(repo => repo.GetUnidataFormattedDate(It.IsAny<string>())).ReturnsAsync("2016/01/01");
                sectionRepoMock.Setup(repo => repo.GetCourseIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
                sectionRepoMock.Setup(repo => repo.ConvertStatusToStatusCodeAsync(It.IsAny<string>())).ReturnsAsync("Open");

                sectionEntities = new List<Domain.Student.Entities.Section>() 
                {
                    new Domain.Student.Entities.Section("1", "180", "111", new DateTime(2016, 01, 01), 3, 3, "Title 1", "I", new Collection<Domain.Student.Entities.OfferingDepartment>(){ new Domain.Student.Entities.OfferingDepartment("MATH") },
                        new Collection<string>(){ "100", "200"}, "UG", new List<SectionStatusItem>() { new SectionStatusItem(SectionStatus.Active, "A", DateTime.Today.AddYears(-6)) })
                        { 
                            Guid = "0b983700-29eb-46ff-8616-21a8c3a48a0c", 
                            LearningProvider = "CE",
                            TermId = "2012/FA",
                            Location = "MAIN",
                            NumberOfWeeks = 2,
                            NumberOnWaitlist = 10,
                            GradeSchemeCode = "UG",
                            BillingCred = 1
                        }
                };
                sectionMaxEntitiesTuple = new Tuple<IEnumerable<Section>, int>(sectionEntities, sectionEntities.Count());
            }
        }
    }
}
