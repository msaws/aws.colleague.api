﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Dtos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using Ellucian.Colleague.Domain.Student.Tests;

namespace Ellucian.Colleague.Coordination.Student.Tests.Services
{
    [TestClass]
    public class AccountingCodesServiceTests
    {
        [TestClass]
        public class GET
        {
            Mock<IStudentReferenceDataRepository> studentReferenceDataRepositoryMock;
            Mock<ILogger> loggerMock;

            AccountingCodesService accountingCodesService;
            List<Ellucian.Colleague.Domain.Student.Entities.AccountingCode> accountingCodes;

            [TestInitialize]
            public void Initialize()
            {
                studentReferenceDataRepositoryMock = new Mock<IStudentReferenceDataRepository>();
                loggerMock = new Mock<ILogger>();

                accountingCodes = new TestAccountingCodesRepository().Get();

                accountingCodesService = new AccountingCodesService(studentReferenceDataRepositoryMock.Object, loggerMock.Object);
            }

            [TestCleanup]
            public void Cleanup()
            {
                accountingCodesService = null;
                accountingCodes = null;
            }

            [TestMethod]
            public async Task AccountingCodesService__GetAllAsync()
            {
                studentReferenceDataRepositoryMock.Setup(i => i.GetAccountingCodesAsync(It.IsAny<bool>())).ReturnsAsync(accountingCodes);

                var results = await accountingCodesService.GetAccountingCodesAsync(It.IsAny<bool>());
                Assert.AreEqual(accountingCodes.Count, (results.Count()));

                foreach (var accountingCode in accountingCodes)
                {
                    var result = results.FirstOrDefault(i => i.Id == accountingCode.Guid);

                    Assert.AreEqual(accountingCode.Code, result.Code);
                    Assert.AreEqual(accountingCode.Description, result.Title);
                    Assert.AreEqual(accountingCode.Guid, result.Id);
                }
            }

            [TestMethod]
            public async Task AccountingCodesService__GetByIdAsync()
            {
                studentReferenceDataRepositoryMock.Setup(i => i.GetAccountingCodesAsync(It.IsAny<bool>())).ReturnsAsync(accountingCodes);

                string id = "a142d78a-b472-45de-8a4b-953258976a0b";
                var accountingCode = accountingCodes.FirstOrDefault(i => i.Guid == id);

                var result = await accountingCodesService.GetAccountingCodeByIdAsync(id);

                Assert.AreEqual(accountingCode.Code, result.Code);
                Assert.AreEqual(accountingCode.Description, result.Title);
                Assert.AreEqual(accountingCode.Guid, result.Id);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task AccountingCodesService__GetByIdAsync_KeyNotFoundException()
            {
                studentReferenceDataRepositoryMock.Setup(i => i.GetAccountingCodesAsync(true)).ReturnsAsync(accountingCodes);
                var result = await accountingCodesService.GetAccountingCodeByIdAsync("123");
            }
        }
    }
}
