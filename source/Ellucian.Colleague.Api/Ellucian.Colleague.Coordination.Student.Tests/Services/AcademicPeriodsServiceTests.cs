﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Colleague.Dtos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Tests;
using Ellucian.Data.Colleague;
using Ellucian.Colleague.Data.Student.Repositories;

namespace Ellucian.Colleague.Coordination.Student.Tests.Services
{
    [TestClass]
    public class AcademicPeriodServiceTests
    {
        private const string _termCode = "2000/S1";  
        private ICollection<Term> _termCollection;
        private ICollection<Ellucian.Colleague.Domain.Student.Entities.AcademicPeriod> _academicPeriodCollection;
        private AcademicPeriodService _academicPeriodService;
        private Mock<ILogger> _loggerMock;
       private Mock<ITermRepository> _termRepositoryMock;
       
        [TestInitialize]//
        public void Initialize()
        {
            _termRepositoryMock = new Mock<ITermRepository>();
            _loggerMock = new Mock<ILogger>();

            _termCollection = new TestTermRepository().Get().ToList();

            _academicPeriodCollection = new TestAcademicPeriodRepository().Get().ToList();
        
            _termRepositoryMock.Setup(repo => repo.GetAsync())
                     .ReturnsAsync(_termCollection);

            _termRepositoryMock.Setup(repo => repo.GetAsync(It.IsAny<bool>()))
                    .ReturnsAsync(_termCollection);

            _academicPeriodService = new AcademicPeriodService(_termRepositoryMock.Object, _loggerMock.Object);
        }

        [TestCleanup]
        public void Cleanup()
        {

            _loggerMock = null;
            _termRepositoryMock = null;
            _academicPeriodService = null;
            _termCollection = null;
            _academicPeriodCollection = null;
  
        }

        [TestMethod]
        public async Task AcademicPeriodService_GetAcademicPeriodsAsync()
        {
            var results = await _academicPeriodService.GetAcademicPeriods2Async(false);
            Assert.IsTrue(results is IEnumerable<Dtos.AcademicPeriod2>);
            Assert.IsNotNull(results);
        }

        [TestMethod]
        public async Task AcademicPeriodService_GetAcademicPeriods3Async()
        {
            var results = await _academicPeriodService.GetAcademicPeriods3Async(false);
            Assert.IsTrue(results is IEnumerable<Dtos.AcademicPeriod3>);
            Assert.IsNotNull(results);
        }

        [TestMethod]
        public async Task AcademicPeriodService_GetAcademicPeriodsAsync_Count()
        {
            var term = _termCollection.FirstOrDefault(x => x.Code == _termCode);

            _termRepositoryMock.Setup(repo => repo.GetAsync(It.IsAny<string>()))
                    .ReturnsAsync(term);
            _termRepositoryMock.Setup(repo => repo.GetAcademicPeriods(_termCollection))
              .Returns(_academicPeriodCollection);

            var results = await _academicPeriodService.GetAcademicPeriods2Async(false);
            Assert.AreEqual(_academicPeriodCollection.Count, results.Count());
        }

        [TestMethod]
        public async Task AcademicPeriodService_GetAcademicPeriods3Async_Count()
        {
            var term = _termCollection.FirstOrDefault(x => x.Code == _termCode);

            _termRepositoryMock.Setup(repo => repo.GetAsync(It.IsAny<string>()))
                    .ReturnsAsync(term);
            _termRepositoryMock.Setup(repo => repo.GetAcademicPeriods(_termCollection))
              .Returns(_academicPeriodCollection);

            var results = await _academicPeriodService.GetAcademicPeriods3Async(false);
            Assert.AreEqual(_academicPeriodCollection.Count, results.Count());
        }

        [TestMethod]
        public async Task AcademicPeriodService_GetAcademicPeriodsAsync_Properties()
        {
           
            _termRepositoryMock.Setup(repo => repo.GetAsync())
                   .ReturnsAsync(_termCollection);

            _termRepositoryMock.Setup(repo => repo.GetAcademicPeriods(_termCollection))
                .Returns(_academicPeriodCollection);

            var result =
                (await _academicPeriodService.GetAcademicPeriods2Async(false)).FirstOrDefault(x => x.Code == _termCode);
            Assert.IsNotNull(result.Id);
            Assert.IsNotNull(result.Code);
            Assert.IsNull(result.Description);
            Assert.IsNotNull(result.Title);
            Assert.IsNotNull(result.Category);
        }

        [TestMethod]
        public async Task AcademicPeriodService_GetAcademicPeriods3Async_Properties()
        {

            _termRepositoryMock.Setup(repo => repo.GetAsync())
                   .ReturnsAsync(_termCollection);

            _termRepositoryMock.Setup(repo => repo.GetAcademicPeriods(_termCollection))
                .Returns(_academicPeriodCollection);

            var result =
                (await _academicPeriodService.GetAcademicPeriods3Async(false)).FirstOrDefault(x => x.Code == _termCode);
            Assert.IsNotNull(result.Id);
            Assert.IsNotNull(result.Code);
            Assert.IsNull(result.Description);
            Assert.IsNotNull(result.Title);
            Assert.IsNotNull(result.Category);
        }   

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task AcademicPeriodService_GetAcademicPeriodByIdAsync_Empty()
        {
            var term = _termCollection.FirstOrDefault(x => x.Code == _termCode);

            _termRepositoryMock.Setup(repo => repo.GetAsync(It.IsAny<string>()))
                    .ReturnsAsync(term);

            await _academicPeriodService.GetAcademicPeriodByGuid2Async("");
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task AcademicPeriodService_GetAcademicPeriodById3Async_Empty()
        {
            var term = _termCollection.FirstOrDefault(x => x.Code == _termCode);

            _termRepositoryMock.Setup(repo => repo.GetAsync(It.IsAny<string>()))
                    .ReturnsAsync(term);

            await _academicPeriodService.GetAcademicPeriodByGuid3Async("");
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task AcademicPeriodService_GetAcademicPeriodByGuidAsync_Null()
        {
            await _academicPeriodService.GetAcademicPeriodByGuid2Async(null);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task AcademicPeriodService_GetAcademicPeriodByGuid3Async_Null()
        {
            await _academicPeriodService.GetAcademicPeriodByGuid3Async(null);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task AcademicPeriodService_GetAcademicPeriodByGuidAsync_InvalidId()
        {
            _termRepositoryMock.Setup(repo => repo.GetAsync(It.IsAny<string>()))
                .Throws<InvalidOperationException>();

            await _academicPeriodService.GetAcademicPeriodByGuid2Async("99");
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task AcademicPeriodService_GetAcademicPeriodByGuid3Async_InvalidId()
        {
            _termRepositoryMock.Setup(repo => repo.GetAsync(It.IsAny<string>()))
                .Throws<InvalidOperationException>();

            await _academicPeriodService.GetAcademicPeriodByGuid3Async("99");
        }

        [TestMethod]
        public async Task AcademicPeriodService_GetAcademicPeriodByIdAsync_Tests_Expected()
        {
            var term = _termCollection.FirstOrDefault(x => x.Code == _termCode);
            var acadPeriod = _academicPeriodCollection.FirstOrDefault(x => x.Code == _termCode);

            _termRepositoryMock.Setup(repo => repo.GetAsync())
                   .ReturnsAsync(_termCollection);

            _termRepositoryMock.Setup(repo => repo.GetAsync(It.IsAny<string>()))
                    .ReturnsAsync(term);

            _termRepositoryMock.Setup(repo => repo.GetAcademicPeriods(_termCollection))
                .Returns(_academicPeriodCollection);

           
            var actualResult =
                await _academicPeriodService.GetAcademicPeriodByGuid2Async(acadPeriod.Guid);
            Assert.AreEqual(acadPeriod.Guid, actualResult.Id);
            Assert.AreEqual(acadPeriod.Description, actualResult.Title);
            Assert.AreEqual(acadPeriod.Code, actualResult.Code);
            Assert.AreEqual(acadPeriod.StartDate, actualResult.Start);
            Assert.AreEqual(acadPeriod.EndDate, actualResult.End);
            
        }

        [TestMethod]
        public async Task AcademicPeriodService_GetAcademicPeriodById3Async_Tests_Expected()
        {
            var term = _termCollection.FirstOrDefault(x => x.Code == _termCode);
            var acadPeriod = _academicPeriodCollection.FirstOrDefault(x => x.Code == _termCode);

            _termRepositoryMock.Setup(repo => repo.GetAsync())
                   .ReturnsAsync(_termCollection);

            _termRepositoryMock.Setup(repo => repo.GetAsync(It.IsAny<string>()))
                    .ReturnsAsync(term);

            _termRepositoryMock.Setup(repo => repo.GetAcademicPeriods(_termCollection))
                .Returns(_academicPeriodCollection);


            var actualResult =
                await _academicPeriodService.GetAcademicPeriodByGuid3Async(acadPeriod.Guid);
            Assert.AreEqual(acadPeriod.Guid, actualResult.Id);
            Assert.AreEqual(acadPeriod.Description, actualResult.Title);
            Assert.AreEqual(acadPeriod.Code, actualResult.Code);
            Assert.AreEqual(acadPeriod.StartDate, actualResult.Start);
            Assert.AreEqual(acadPeriod.EndDate, actualResult.End);

        }

        [TestMethod]
        public async Task AcademicPeriodService_GetAcademicPeriodByIdAsync_Properties()
        {
            var term = _termCollection.FirstOrDefault(x => x.Code == _termCode);

            _termRepositoryMock.Setup(repo => repo.GetAsync())
                   .ReturnsAsync(_termCollection);

            _termRepositoryMock.Setup(repo => repo.GetAsync(It.IsAny<string>()))
                    .ReturnsAsync(term);

            _termRepositoryMock.Setup(repo => repo.GetAcademicPeriods(_termCollection))
                .Returns(_academicPeriodCollection);

            var acadPeriodGuid = _academicPeriodCollection.FirstOrDefault(x => x.Code == _termCode).Guid;       

            var result =
                await _academicPeriodService.GetAcademicPeriodByGuid2Async(acadPeriodGuid);
            Assert.IsNotNull(result.Id);
            Assert.IsNotNull(result.Code);
            Assert.IsNull(result.Description);
            Assert.IsNotNull(result.Title);
            Assert.IsNotNull(result.Category);
            Assert.IsNotNull(result.Start);
            Assert.IsNotNull(result.End);
        }

        [TestMethod]
        public async Task AcademicPeriodService_GetAcademicPeriodById3Async_Properties()
        {
            var term = _termCollection.FirstOrDefault(x => x.Code == _termCode);

            _termRepositoryMock.Setup(repo => repo.GetAsync())
                   .ReturnsAsync(_termCollection);

            _termRepositoryMock.Setup(repo => repo.GetAsync(It.IsAny<string>()))
                    .ReturnsAsync(term);

            _termRepositoryMock.Setup(repo => repo.GetAcademicPeriods(_termCollection))
                .Returns(_academicPeriodCollection);

            var acadPeriodGuid = _academicPeriodCollection.FirstOrDefault(x => x.Code == _termCode).Guid;

            var result =
                await _academicPeriodService.GetAcademicPeriodByGuid3Async(acadPeriodGuid);
            Assert.IsNotNull(result.Id);
            Assert.IsNotNull(result.Code);
            Assert.IsNull(result.Description);
            Assert.IsNotNull(result.Title);
            Assert.IsNotNull(result.Category);
            Assert.IsNotNull(result.Start);
            Assert.IsNotNull(result.End);
        }
    }
}