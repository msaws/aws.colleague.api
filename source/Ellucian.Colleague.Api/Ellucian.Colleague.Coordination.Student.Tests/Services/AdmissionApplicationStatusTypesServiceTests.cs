﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Dtos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Base.Repositories;

namespace Ellucian.Colleague.Coordination.Student.Tests.Services
{
    [TestClass]
    public class AdmissionApplicationStatusTypesServiceTests
    {
        private const string admissionApplicationStatusTypesGuid = "7a2bf6b5-cdcd-4c8f-b5d8-3053bf5b3fbc";
        private const string admissionApplicationStatusTypesCode = "Started";
        private ICollection<Ellucian.Colleague.Domain.Student.Entities.AdmissionApplicationStatusType> _applicationStatusesCollection;
        private AdmissionApplicationStatusTypesService _admissionApplicationStatusTypesService;
        private Mock<ILogger> _loggerMock;
        private Mock<IAdapterRegistry> _adapterRegistryMock;
        private Mock<ICurrentUserFactory> _currentUserMock;
        private Mock<IRoleRepository> _roleRepository;
        private Mock<IStudentReferenceDataRepository> _referenceRepositoryMock;
        private IConfigurationRepository baseConfigurationRepository;
        private Mock<IConfigurationRepository> baseConfigurationRepositoryMock;

        [TestInitialize]
        public void Initialize()
        {
            _referenceRepositoryMock = new Mock<IStudentReferenceDataRepository>();
            _loggerMock = new Mock<ILogger>();
            _adapterRegistryMock = new Mock<IAdapterRegistry>();
            _currentUserMock = new Mock<ICurrentUserFactory>();
            _roleRepository = new Mock<IRoleRepository>();
            baseConfigurationRepositoryMock = new Mock<IConfigurationRepository>();
            baseConfigurationRepository = baseConfigurationRepositoryMock.Object;

            _applicationStatusesCollection = new List<Ellucian.Colleague.Domain.Student.Entities.AdmissionApplicationStatusType>()
                {
                    new Ellucian.Colleague.Domain.Student.Entities.AdmissionApplicationStatusType("7a2bf6b5-cdcd-4c8f-b5d8-3053bf5b3fbc", "Started", "Started"){ AdmissionApplicationStatusTypesCategory = AdmissionApplicationStatusTypesCategory.Started },
                    new Ellucian.Colleague.Domain.Student.Entities.AdmissionApplicationStatusType("849e6a7c-6cd4-4f98-8a73-ab0aa3627f0d", "Submitted", "Submitted"){ AdmissionApplicationStatusTypesCategory = AdmissionApplicationStatusTypesCategory.Submitted },
                    new Ellucian.Colleague.Domain.Student.Entities.AdmissionApplicationStatusType("d2253ac7-9931-4560-b42f-1fccd43c952e", "Readyforreview", "Readyforreview"){ AdmissionApplicationStatusTypesCategory = AdmissionApplicationStatusTypesCategory.Readyforreview },
                    new Ellucian.Colleague.Domain.Student.Entities.AdmissionApplicationStatusType("e2253ac7-9931-4560-b42f-1fccd43c952f", "Decisionmade", "Decisionmade"){ AdmissionApplicationStatusTypesCategory = AdmissionApplicationStatusTypesCategory.Decisionmade },
                    new Ellucian.Colleague.Domain.Student.Entities.AdmissionApplicationStatusType("g2253ac7-9931-4560-b42f-1fccd43c952h", "Enrollmentcomplete", "Enrollmentcomplete"){ AdmissionApplicationStatusTypesCategory = AdmissionApplicationStatusTypesCategory.Enrollmentcomplete },
                    new Ellucian.Colleague.Domain.Student.Entities.AdmissionApplicationStatusType("i2253ac7-9931-4560-b42f-1fccd43c952j", "ET", "Ectra Terrestrial"){ AdmissionApplicationStatusTypesCategory = AdmissionApplicationStatusTypesCategory.NotSet }
                };


            _referenceRepositoryMock.Setup(repo => repo.GetAdmissionApplicationStatusTypesAsync(It.IsAny<bool>()))
                .ReturnsAsync(_applicationStatusesCollection);

            _admissionApplicationStatusTypesService = new AdmissionApplicationStatusTypesService(_referenceRepositoryMock.Object, _adapterRegistryMock.Object, _currentUserMock.Object,
               _roleRepository.Object, _loggerMock.Object, baseConfigurationRepository);
        }

        [TestCleanup]
        public void Cleanup()
        {
            _admissionApplicationStatusTypesService = null;
            _applicationStatusesCollection = null;
            _referenceRepositoryMock = null;
            _loggerMock = null;
        }

        [TestMethod]
        public async Task AdmissionApplicationStatusTypesService_GetAdmissionApplicationStatusTypesAsync()
        {
            var results = await _admissionApplicationStatusTypesService.GetAdmissionApplicationStatusTypesAsync(true);
            Assert.IsTrue(results is IEnumerable<Ellucian.Colleague.Dtos.AdmissionApplicationStatusType>);
            Assert.IsNotNull(results);
        }

        [TestMethod]
        public async Task AdmissionApplicationStatusTypesService_GetAdmissionApplicationStatusTypesAsync_Count()
        {
            var results = await _admissionApplicationStatusTypesService.GetAdmissionApplicationStatusTypesAsync(true);
            Assert.AreEqual(6, results.Count());
        }

        [TestMethod]
        public async Task AdmissionApplicationStatusTypesService_GetAdmissionApplicationStatusTypesAsync_Properties()
        {
            var result =
                (await _admissionApplicationStatusTypesService.GetAdmissionApplicationStatusTypesAsync(true)).FirstOrDefault(x => x.Code == admissionApplicationStatusTypesCode);
            Assert.IsNotNull(result.Id);
            Assert.IsNotNull(result.Code);
            Assert.IsNull(result.Description);

        }

        [TestMethod]
        public async Task AdmissionApplicationStatusTypesService_GetAdmissionApplicationStatusTypesAsync_Expected()
        {
            var expectedResults = _applicationStatusesCollection.FirstOrDefault(c => c.Guid == admissionApplicationStatusTypesGuid);
            var actualResult =
                (await _admissionApplicationStatusTypesService.GetAdmissionApplicationStatusTypesAsync(true)).FirstOrDefault(x => x.Id == admissionApplicationStatusTypesGuid);
            Assert.AreEqual(expectedResults.Guid, actualResult.Id);
            Assert.AreEqual(expectedResults.Description, actualResult.Title);
            Assert.AreEqual(expectedResults.Code, actualResult.Code);

        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task AdmissionApplicationStatusTypesService_GetAdmissionApplicationStatusTypesByGuidAsync_Empty()
        {
            await _admissionApplicationStatusTypesService.GetAdmissionApplicationStatusTypesByGuidAsync("");
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task AdmissionApplicationStatusTypesService_GetAdmissionApplicationStatusTypesByGuidAsync_Null()
        {
            await _admissionApplicationStatusTypesService.GetAdmissionApplicationStatusTypesByGuidAsync(null);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task AdmissionApplicationStatusTypesService_GetAdmissionApplicationStatusTypesByGuidAsync_InvalidId()
        {
            _referenceRepositoryMock.Setup(repo => repo.GetAdmissionApplicationStatusTypesAsync(It.IsAny<bool>()))
                .Throws<KeyNotFoundException>();

            await _admissionApplicationStatusTypesService.GetAdmissionApplicationStatusTypesByGuidAsync("99");
        }

        [TestMethod]
        public async Task AdmissionApplicationStatusTypesService_GetAdmissionApplicationStatusTypesByGuidAsync_Expected()
        {
            var expectedResults =
                _applicationStatusesCollection.First(c => c.Guid == admissionApplicationStatusTypesGuid);
            var actualResult =
                await _admissionApplicationStatusTypesService.GetAdmissionApplicationStatusTypesByGuidAsync(admissionApplicationStatusTypesGuid);
            Assert.AreEqual(expectedResults.Guid, actualResult.Id);
            Assert.AreEqual(expectedResults.Description, actualResult.Title);
            Assert.AreEqual(expectedResults.Code, actualResult.Code);

        }

        [TestMethod]
        public async Task AdmissionApplicationStatusTypesService_GetAdmissionApplicationStatusTypesByGuidAsync_Properties()
        {
            var result =
                await _admissionApplicationStatusTypesService.GetAdmissionApplicationStatusTypesByGuidAsync(admissionApplicationStatusTypesGuid);
            Assert.IsNotNull(result.Id);
            Assert.IsNotNull(result.Code);
            Assert.IsNull(result.Description);
            Assert.IsNotNull(result.Title);

        }
    }
}