﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Base.Repositories;
using System;
using Ellucian.Colleague.Domain.Student;

namespace Ellucian.Colleague.Coordination.Student.Tests.Services
{
    [TestClass]
    public class AdmissionApplicationsServiceTests
    {
        // sets up a current user
        public abstract class CurrentUserSetup
        {
            protected Domain.Entities.Role personRole = new Domain.Entities.Role(105, "Faculty");

            public class PersonUserFactory : ICurrentUserFactory
            {
                public ICurrentUser CurrentUser
                {
                    get
                    {
                        return new CurrentUser(new Claims()
                        {
                            ControlId = "123",
                            Name = "George",
                            PersonId = "0000015",
                            SecurityToken = "321",
                            SessionTimeout = 30,
                            UserName = "Faculty",
                            Roles = new List<string>() { "Faculty" },
                            SessionFixationId = "abc123",
                        });
                    }
                }
            }
        }

        [TestClass]
        public class AdmissionApplications : CurrentUserSetup
        {
            Mock<IAdmissionApplicationsRepository> admissionRepositoryMock;
            Mock<IStudentReferenceDataRepository> studentReferenceDataRepositoryMock;
            Mock<IAdapterRegistry> adapterRegistryMock;
            Mock<IReferenceDataRepository> referenceRepositoryMock;
            Mock<ITermRepository> termRepositoryMock;
            Mock<IInstitutionRepository> institutionRepositoryMock;
            Mock<ICurrentUserFactory> userFactoryMock;
            ICurrentUserFactory userFactory;
            Mock<IRoleRepository> roleRepoMock;
            Mock<IConfigurationRepository> configRepoMock;
            //Mock<IPersonBaseRepository> personBaseRepositoryMock;
            private ILogger logger;

            AdmissionApplicationsService admissionApplicationService;

            IEnumerable<Domain.Student.Entities.AdmissionApplication> admissionEntities;
            IEnumerable<Dtos.AdmissionApplication> admissionDtos;
            Dictionary<string, string> personGuids = new Dictionary<string, string>();
            IEnumerable<Domain.Student.Entities.AdmissionApplicationType> admissionTypeEntities;
            IEnumerable<Domain.Student.Entities.Term> termEntities;
            IEnumerable<Domain.Student.Entities.AdmissionApplicationStatusType> admissionStatusTypeEntities;
            IEnumerable<Domain.Student.Entities.ApplicationSource> applicationSourceEntities;
            IEnumerable<Domain.Student.Entities.AdmissionPopulation> admissionPopulationEntities;
            IEnumerable<Domain.Student.Entities.AdmissionResidencyType> admissionResidencyEntities;
            IEnumerable<Domain.Student.Entities.AcademicLevel> academicLevelEntities;
            IEnumerable<Domain.Student.Entities.AcademicProgram> academicProgramEntities;
            IEnumerable<Domain.Student.Entities.WithdrawReason> withdrawReasonEntities;
            IEnumerable<Domain.Base.Entities.Location> locationEntities;
            IEnumerable<Domain.Base.Entities.AcademicDiscipline> academicDisciplineEntities;
            IEnumerable<Domain.Base.Entities.School> schoolEntities;

            Tuple<IEnumerable<Domain.Student.Entities.AdmissionApplication>, int> admissionEntitiesTuple;
            Tuple<IEnumerable<Dtos.AdmissionApplication>, int> admissionDtoTuple;

            int offset = 0;
            int limit = 200;

            private Domain.Entities.Permission permissionViewAnyApplication;


            [TestInitialize]
            public void Initialize()
            {
                admissionRepositoryMock = new Mock<IAdmissionApplicationsRepository>();
                studentReferenceDataRepositoryMock = new Mock<IStudentReferenceDataRepository>();
                termRepositoryMock = new Mock<ITermRepository>();
                referenceRepositoryMock = new Mock<IReferenceDataRepository>();
                institutionRepositoryMock = new Mock<IInstitutionRepository>();
                roleRepoMock = new Mock<IRoleRepository>();
                userFactoryMock = new Mock<ICurrentUserFactory>();
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                logger = new Mock<ILogger>().Object;
                configRepoMock = new Mock<IConfigurationRepository>();

                // Set up current user
                userFactory = userFactoryMock.Object;
                userFactory = new CurrentUserSetup.PersonUserFactory();
                // Mock permissions
                permissionViewAnyApplication = new Ellucian.Colleague.Domain.Entities.Permission(StudentPermissionCodes.ViewApplications);
                personRole.AddPermission(permissionViewAnyApplication);

                BuildData();
                BuildMocks();

                admissionApplicationService = new AdmissionApplicationsService(admissionRepositoryMock.Object, termRepositoryMock.Object, institutionRepositoryMock.Object, studentReferenceDataRepositoryMock.Object, referenceRepositoryMock.Object, configRepoMock.Object, adapterRegistryMock.Object, userFactory, roleRepoMock.Object, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                admissionRepositoryMock = null;
                studentReferenceDataRepositoryMock = null;
                admissionEntities = null;
                admissionDtos = null;
            }

            private void BuildData()
            {
                //Dtos
                admissionDtos = new List<Dtos.AdmissionApplication>()
                {
                    new Dtos.AdmissionApplication()
                    {
                        Applicant = new Dtos.GuidObject2("d190d4b5-03b5-41aa-99b8-b8286717c956"),
                        Id = "bbd216fb-0fc5-4f44-ae45-42d3cdd1e89a",
                        Statuses = new List<Dtos.DtoProperties.AdmissionApplicationsStatus>() {
                            new Dtos.DtoProperties.AdmissionApplicationsStatus() {
                                AdmissionApplicationsStatusDetail = new Dtos.GuidObject2("e0c0c94c-53a7-46b7-96c4-76b12512c323"),
                                AdmissionApplicationsStatusStartOn = new DateTime(2016, 09, 03),
                                AdmissionApplicationsStatusType = Dtos.EnumProperties.AdmissionApplicationsStatusType.DecisionMade
                            }
                        },
                        Owner = new Dtos.GuidObject2("b90812ee-b573-4acb-88b0-6999a050be4f")
                    },
                    new Dtos.AdmissionApplication()
                    {
                        Applicant = new Dtos.GuidObject2("d190d4b5-03b5-41aa-99b8-b8286717c956"),
                        Id = "3f67b180-ce1d-4552-8d81-feb96b9fea5b",
                        Statuses = new List<Dtos.DtoProperties.AdmissionApplicationsStatus>() {
                            new Dtos.DtoProperties.AdmissionApplicationsStatus() {
                                AdmissionApplicationsStatusDetail = new Dtos.GuidObject2("0bbb15f2-bb03-4056-bb9b-57a0ddf057ff"),
                                AdmissionApplicationsStatusStartOn = new DateTime(2016, 09, 01),
                                AdmissionApplicationsStatusType = Dtos.EnumProperties.AdmissionApplicationsStatusType.EnrollmentCompleted
                            }
                        },
                        Owner = new Dtos.GuidObject2("b90812ee-b573-4acb-88b0-6999a050be4f")
                    },
                    new Dtos.AdmissionApplication()
                    {
                        Applicant = new Dtos.GuidObject2("cecdce5a-54a7-45fb-a975-5392a579e5bf"),
                        Id = "bf67e156-8f5d-402b-8101-81b0a2796873",
                        Statuses = new List<Dtos.DtoProperties.AdmissionApplicationsStatus>() {
                            new Dtos.DtoProperties.AdmissionApplicationsStatus() {
                                AdmissionApplicationsStatusDetail = new Dtos.GuidObject2("0bbb15f2-bb03-4056-bb9b-57a0ddf057ff"),
                                AdmissionApplicationsStatusStartOn = new DateTime(2016, 04, 01),
                                AdmissionApplicationsStatusType = Dtos.EnumProperties.AdmissionApplicationsStatusType.ReadyForReview
                            }
                        },
                        Owner = new Dtos.GuidObject2("0ac28907-5a9b-4102-a0d7-5d3d9c585512")
                    },
                    new Dtos.AdmissionApplication()
                    {
                        Applicant = new Dtos.GuidObject2("cecdce5a-54a7-45fb-a975-5392a579e5bf"),
                        Id = "0111d6ef-5a86-465f-ac58-4265a997c136",
                        Statuses = new List<Dtos.DtoProperties.AdmissionApplicationsStatus>() {
                            new Dtos.DtoProperties.AdmissionApplicationsStatus() {
                                AdmissionApplicationsStatusDetail = new Dtos.GuidObject2("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52"),
                                AdmissionApplicationsStatusStartOn = new DateTime(2016, 02, 01),
                                AdmissionApplicationsStatusType = Dtos.EnumProperties.AdmissionApplicationsStatusType.ReadyForReview
                            }
                        },
                        Owner = new Dtos.GuidObject2("bb6c261c-3818-4dc3-b693-eb3e64d70d8b")
                    },
                };

                //Entities
                admissionEntities = new List<Domain.Student.Entities.AdmissionApplication>()
                {
                    new Domain.Student.Entities.AdmissionApplication("bbd216fb-0fc5-4f44-ae45-42d3cdd1e89a", "1")
                    {
                        ApplicantPersonId = "1",
                        AdmissionApplicationStatuses = new List<Domain.Student.Entities.AdmissionApplicationStatus>() {
                            new Domain.Student.Entities.AdmissionApplicationStatus() { ApplicationStatusDate = new DateTime(2016, 02, 01), ApplicationDecisionBy = "1", ApplicationStatus = "CODE1", ApplicationStatusTime = new DateTime(1900, 01, 01, 21, 36, 56, 000) }
                        },
                    },
                    new Domain.Student.Entities.AdmissionApplication("3f67b180-ce1d-4552-8d81-feb96b9fea5b", "1")
                    {
                        ApplicantPersonId = "1",
                        AdmissionApplicationStatuses = new List<Domain.Student.Entities.AdmissionApplicationStatus>() {
                            new Domain.Student.Entities.AdmissionApplicationStatus() { ApplicationStatusDate = new DateTime(2016, 02, 01), ApplicationDecisionBy = "1", ApplicationStatus = "CODE1", ApplicationStatusTime = new DateTime(1900, 01, 01, 21, 36, 56, 000) }
                        },
                    },
                    new Domain.Student.Entities.AdmissionApplication("bf67e156-8f5d-402b-8101-81b0a2796873", "3")
                    {
                        ApplicantPersonId = "2",
                        AdmissionApplicationStatuses = new List<Domain.Student.Entities.AdmissionApplicationStatus>() {
                            new Domain.Student.Entities.AdmissionApplicationStatus() { ApplicationStatusDate = new DateTime(2016, 02, 01), ApplicationDecisionBy = "1", ApplicationStatus = "CODE1", ApplicationStatusTime = new DateTime(1900, 01, 01, 21, 36, 56, 000) }
                        },
                    },
                    new Domain.Student.Entities.AdmissionApplication("0111d6ef-5a86-465f-ac58-4265a997c136", "3")
                    {
                        ApplicantPersonId = "2",
                        AdmissionApplicationStatuses = new List<Domain.Student.Entities.AdmissionApplicationStatus>() {
                            new Domain.Student.Entities.AdmissionApplicationStatus() { ApplicationStatusDate = new DateTime(2016, 02, 01), ApplicationDecisionBy = "1", ApplicationStatus = "CODE1", ApplicationStatusTime = new DateTime(1900, 01, 01, 21, 36, 56, 000) }
                        },
                    },
                };

                //Persons
                personGuids.Add("1", "d190d4b5-03b5-41aa-99b8-b8286717c956");
                personGuids.Add("2", "cecdce5a-54a7-45fb-a975-5392a579e5bf");


                //AdmissionTypes
                admissionTypeEntities = new List<Domain.Student.Entities.AdmissionApplicationType>()
                {
                    new Domain.Student.Entities.AdmissionApplicationType("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                    new Domain.Student.Entities.AdmissionApplicationType("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                    new Domain.Student.Entities.AdmissionApplicationType("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                    new Domain.Student.Entities.AdmissionApplicationType("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                };

                //Terms
                termEntities = new List<Domain.Student.Entities.Term>()
                {
                    new Domain.Student.Entities.Term("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1", new DateTime(2016, 02, 01), new DateTime(2016, 03, 01), 2016, 1, false, false, "WINTER", false),
                    new Domain.Student.Entities.Term("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2", new DateTime(2016, 03, 01), new DateTime(2016, 04, 01), 2016, 2, false, false, "WINTER", false),
                    new Domain.Student.Entities.Term("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3", new DateTime(2016, 04, 01), new DateTime(2016, 05, 01), 2016, 3, false, false, "WINTER", false),
                    new Domain.Student.Entities.Term("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4", new DateTime(2016, 05, 01), new DateTime(2016, 06, 01), 2016, 4, false, false, "WINTER", false)
                };

                //AdmissionApplicationStatusTypes
                admissionStatusTypeEntities = new List<Domain.Student.Entities.AdmissionApplicationStatusType>()
                {
                    new Domain.Student.Entities.AdmissionApplicationStatusType("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                    new Domain.Student.Entities.AdmissionApplicationStatusType("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                    new Domain.Student.Entities.AdmissionApplicationStatusType("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                    new Domain.Student.Entities.AdmissionApplicationStatusType("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                };

                //ApplicationSources
                applicationSourceEntities = new List<Domain.Student.Entities.ApplicationSource>()
                {
                    new Domain.Student.Entities.ApplicationSource("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                    new Domain.Student.Entities.ApplicationSource("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                    new Domain.Student.Entities.ApplicationSource("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                    new Domain.Student.Entities.ApplicationSource("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                };

                //AdmissionPopulations
                admissionPopulationEntities = new List<Domain.Student.Entities.AdmissionPopulation>()
                {
                    new Domain.Student.Entities.AdmissionPopulation("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                    new Domain.Student.Entities.AdmissionPopulation("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                    new Domain.Student.Entities.AdmissionPopulation("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                    new Domain.Student.Entities.AdmissionPopulation("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                };

                //AdmissionResidencyTypes
                admissionResidencyEntities = new List<Domain.Student.Entities.AdmissionResidencyType>()
                {
                    new Domain.Student.Entities.AdmissionResidencyType("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                    new Domain.Student.Entities.AdmissionResidencyType("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                    new Domain.Student.Entities.AdmissionResidencyType("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                    new Domain.Student.Entities.AdmissionResidencyType("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                };

                //AcademicLevels
                academicLevelEntities = new List<Domain.Student.Entities.AcademicLevel>()
                {
                    new Domain.Student.Entities.AcademicLevel("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                    new Domain.Student.Entities.AcademicLevel("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                    new Domain.Student.Entities.AcademicLevel("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                    new Domain.Student.Entities.AcademicLevel("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                };

                //AcademicPrograms
                academicProgramEntities = new List<Domain.Student.Entities.AcademicProgram>()
                {
                    new Domain.Student.Entities.AcademicProgram("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                    new Domain.Student.Entities.AcademicProgram("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                    new Domain.Student.Entities.AcademicProgram("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                    new Domain.Student.Entities.AcademicProgram("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                };

                //WithdrawReasons
                withdrawReasonEntities = new List<Domain.Student.Entities.WithdrawReason>()
                {
                    new Domain.Student.Entities.WithdrawReason("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                    new Domain.Student.Entities.WithdrawReason("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                    new Domain.Student.Entities.WithdrawReason("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                    new Domain.Student.Entities.WithdrawReason("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                };

                //Locations
                locationEntities = new List<Domain.Base.Entities.Location>()
                {
                    new Domain.Base.Entities.Location("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                    new Domain.Base.Entities.Location("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                    new Domain.Base.Entities.Location("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                    new Domain.Base.Entities.Location("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                };

                //AcademicDisciplines
                academicDisciplineEntities = new List<Domain.Base.Entities.AcademicDiscipline>()
                {
                    new Domain.Base.Entities.AcademicDiscipline("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1", Domain.Base.Entities.AcademicDisciplineType.Concentration),
                    new Domain.Base.Entities.AcademicDiscipline("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2", Domain.Base.Entities.AcademicDisciplineType.Major),
                    new Domain.Base.Entities.AcademicDiscipline("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3", Domain.Base.Entities.AcademicDisciplineType.Minor),
                    new Domain.Base.Entities.AcademicDiscipline("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4", Domain.Base.Entities.AcademicDisciplineType.Concentration)
                };

                //Schools
                schoolEntities = new List<Domain.Base.Entities.School>()
                {
                    new Domain.Base.Entities.School("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                    new Domain.Base.Entities.School("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                    new Domain.Base.Entities.School("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                    new Domain.Base.Entities.School("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                };

                //Tuple
                admissionEntitiesTuple = new Tuple<IEnumerable<Domain.Student.Entities.AdmissionApplication>, int>(admissionEntities, admissionEntities.Count());
                admissionDtoTuple = new Tuple<IEnumerable<Dtos.AdmissionApplication>, int>(admissionDtos, admissionDtos.Count());
            }

            private void BuildMocks()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { personRole });

                admissionRepositoryMock.Setup(i => i.GetPersonGuidsAsync(It.IsAny<IEnumerable<string>>())).ReturnsAsync(personGuids);
                studentReferenceDataRepositoryMock.Setup(i => i.GetAdmissionApplicationTypesAsync(It.IsAny<bool>())).ReturnsAsync(admissionTypeEntities);
                termRepositoryMock.Setup(i => i.GetAsync(It.IsAny<bool>())).ReturnsAsync(termEntities);
                studentReferenceDataRepositoryMock.Setup(i => i.GetAdmissionApplicationStatusTypesAsync(It.IsAny<bool>())).ReturnsAsync(admissionStatusTypeEntities);
                studentReferenceDataRepositoryMock.Setup(i => i.GetApplicationSourcesAsync(It.IsAny<bool>())).ReturnsAsync(applicationSourceEntities);
                studentReferenceDataRepositoryMock.Setup(i => i.GetAdmissionPopulationsAsync(It.IsAny<bool>())).ReturnsAsync(admissionPopulationEntities);
                studentReferenceDataRepositoryMock.Setup(i => i.GetAdmissionResidencyTypesAsync(It.IsAny<bool>())).ReturnsAsync(admissionResidencyEntities);
                studentReferenceDataRepositoryMock.Setup(i => i.GetAcademicLevelsAsync(It.IsAny<bool>())).ReturnsAsync(academicLevelEntities);
                studentReferenceDataRepositoryMock.Setup(i => i.GetAcademicProgramsAsync(It.IsAny<bool>())).ReturnsAsync(academicProgramEntities);
                studentReferenceDataRepositoryMock.Setup(i => i.GetWithdrawReasonsAsync(It.IsAny<bool>())).ReturnsAsync(withdrawReasonEntities);
                referenceRepositoryMock.Setup(i => i.GetLocationsAsync(It.IsAny<bool>())).ReturnsAsync(locationEntities);
                referenceRepositoryMock.Setup(i => i.GetAcademicDisciplinesAsync(It.IsAny<bool>())).ReturnsAsync(academicDisciplineEntities);
                referenceRepositoryMock.Setup(i => i.GetSchoolsAsync(It.IsAny<bool>())).ReturnsAsync(schoolEntities);
            }

            [TestMethod]
            public async Task AdmissionApplicationService__GetAdmissionApplicationsAsync()
            {
                admissionRepositoryMock.Setup(i => i.GetAdmissionApplicationsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).ReturnsAsync(admissionEntitiesTuple);
                var admissionApplications = await admissionApplicationService.GetAdmissionApplicationsAsync(offset, limit, false);

                Assert.IsNotNull(admissionApplications);

                foreach (var actual in admissionApplications.Item1)
                {
                    var expected = admissionDtos.FirstOrDefault(i => i.Id.Equals(actual.Id, StringComparison.OrdinalIgnoreCase));
                    Assert.IsNotNull(expected);

                    Assert.AreEqual(expected.Id, actual.Id);
                    Assert.AreEqual(expected.Applicant.Id, actual.Applicant.Id);
                }
            }

            [TestMethod]
            public async Task AdmissionApplicationService__GetAdmissionApplicationByGuidAsync()
            {
                var id = "0111d6ef-5a86-465f-ac58-4265a997c136";
                var addmissionEntity = admissionEntities.FirstOrDefault(i => i.Guid.Equals(id));
                admissionRepositoryMock.Setup(i => i.GetAdmissionApplicationByIdAsync(It.IsAny<string>())).ReturnsAsync(addmissionEntity);

                var actual = await admissionApplicationService.GetAdmissionApplicationsByGuidAsync(id);

                Assert.IsNotNull(actual);

                var expected = admissionDtos.FirstOrDefault(i => i.Id.Equals(actual.Id, StringComparison.OrdinalIgnoreCase));
                Assert.IsNotNull(expected);

                Assert.AreEqual(expected.Id, actual.Id);
                Assert.AreEqual(expected.Applicant.Id, actual.Applicant.Id);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task AdmissionApplications_GetById_Exception()
            {
                admissionRepositoryMock.Setup(i => i.GetAdmissionApplicationByIdAsync(It.IsAny<string>())).ThrowsAsync(new Exception());
                var actual = await admissionApplicationService.GetAdmissionApplicationsByGuidAsync("abc");
            }
        }
    }
}
