﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Coordination.Student.Tests.UserFactories;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;

namespace Ellucian.Colleague.Coordination.Student.Tests.Services
{
    [TestClass]
    public class StudentTaxFormStatementServiceTests
    {
        #region Initialize and Cleanup
        private StudentTaxFormStatementService service;

        [TestInitialize]
        public void Initialize()
        {
            taxFormConfiguration = new TaxFormConfiguration(TaxForms.Form1098);
            BuildService();
        }

        [TestCleanup]
        public void Cleanup()
        {
            taxFormConfiguration = null;
            service = null;
        }
        #endregion

        #region GetAsync
        [TestMethod]
        public async Task GetAsync_NullPersonId()
        {
            var expectedParam = "personid";
            var actualParam = "";
            try
            {
                await service.GetAsync(null, Dtos.Base.TaxForms.Form1098);
            }
            catch (ArgumentNullException anex)
            {
                actualParam = anex.ParamName.ToLower();
            }

            Assert.AreEqual(expectedParam, actualParam);
        }

        [TestMethod]
        public async Task GetAsync_EmptyPersonId()
        {
            var expectedParam = "personid";
            var actualParam = "";
            try
            {
                await service.GetAsync("", Dtos.Base.TaxForms.Form1098);
            }
            catch (ArgumentNullException anex)
            {
                actualParam = anex.ParamName.ToLower();
            }

            Assert.AreEqual(expectedParam, actualParam);
        }

        [TestMethod]
        public async Task GetAsync_InvalidTaxFormId()
        {
            var expectedParam = "taxform";
            var actualParam = "";
            try
            {
                await service.GetAsync("1", Dtos.Base.TaxForms.FormW2);
            }
            catch (ArgumentException aex)
            {
                actualParam = aex.ParamName.ToLower();
            }

            Assert.AreEqual(expectedParam, actualParam);
        }

        [TestMethod]
        public async Task GetAsync_Success()
        {
            var actualTaxFormStatements = await service.GetAsync("1", Dtos.Base.TaxForms.Form1098);

            foreach (var expectedStatement in expectedTaxFormStatements)
            {
                var actualStatements = actualTaxFormStatements.Where(x =>
                    x.Notation == Dtos.Base.TaxFormNotations2.None
                    && x.PdfRecordId == expectedStatement.PdfRecordId
                    && x.PersonId == expectedStatement.PersonId
                    && x.TaxForm == Dtos.Base.TaxForms.Form1098
                    && x.TaxYear == expectedStatement.TaxYear).ToList();
                Assert.AreEqual(1, actualStatements.Count);
            }
        }
        #endregion

        #region Build service method
        /// <summary>
        /// Builds multiple cost center service objects.
        /// </summary>
        private void BuildService()
        {
            var statementRepositoryMock = new Mock<IStudentTaxFormStatementRepository>();
            var configurationRepositoryMock = new Mock<IConfigurationRepository>();
            var adapterRegistryMock = new Mock<IAdapterRegistry>();
            var userFactory = new StudentUserFactory.UserFactory();
            var roleRepositoryMock = new Mock<IRoleRepository>();
            var loggerMock = new Mock<ILogger>();

            // Set up and mock the adapter, and setup the GetAdapter method.
            var costCenterDtoAdapter = new AutoMapperAdapter<Domain.Base.Entities.TaxFormStatement2, Dtos.Base.TaxFormStatement2>(adapterRegistryMock.Object, loggerMock.Object);
            adapterRegistryMock.Setup(x => x.GetAdapter<Domain.Base.Entities.TaxFormStatement2, Dtos.Base.TaxFormStatement2>()).Returns(costCenterDtoAdapter);

            statementRepositoryMock.Setup(x => x.GetAsync(It.IsAny<string>(), It.IsAny<Domain.Base.Entities.TaxForms>())).Returns(() =>
                {
                    return Task.FromResult(getStatements());
                });

            configurationRepositoryMock.Setup(x => x.GetTaxFormAvailabilityConfigurationAsync(It.IsAny<Domain.Base.Entities.TaxForms>())).Returns(() =>
                {
                    return Task.FromResult(getConfiguration());
                });

            service = new StudentTaxFormStatementService(statementRepositoryMock.Object, configurationRepositoryMock.Object,
                adapterRegistryMock.Object, userFactory, roleRepositoryMock.Object, loggerMock.Object);
        }

        private List<Domain.Base.Entities.TaxFormStatement2> expectedTaxFormStatements = new List<Domain.Base.Entities.TaxFormStatement2>()
            {
                new TaxFormStatement2("1", "2016", TaxForms.Form1098, "1234"),
                new TaxFormStatement2("1", "2015", TaxForms.Form1098, "3452")
            };
        private IEnumerable<Domain.Base.Entities.TaxFormStatement2> getStatements()
        {
            return expectedTaxFormStatements;
        }

        private TaxFormConfiguration taxFormConfiguration;
        private TaxFormConfiguration getConfiguration()
        {
            taxFormConfiguration.AddAvailability(new TaxFormAvailability("2016", true));
            taxFormConfiguration.AddAvailability(new TaxFormAvailability("2015", true));
            return taxFormConfiguration;
        }
        #endregion
    }
}