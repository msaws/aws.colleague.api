﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Student.Services;
using Ellucian.Colleague.Domain.Student.Entities.Requirements;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Dtos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using Ellucian.Web.Adapters;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Base.Repositories;

namespace Ellucian.Colleague.Coordination.Student.Tests.Services
{
    [TestClass]
    public class AcademicProgramServiceTests
    {
        [TestClass]
        public class GetAcademicProgram
        {
            private Mock<IAdapterRegistry> _adapterRegistryMock;
            private Mock<IFacilitiesService> _institutionServiceMock;
            private Mock<IStudentReferenceDataRepository> _studentReferenceDataRepositoryMock;
            private Mock<IReferenceDataRepository> _referenceDataRepositoryMock;
            private Mock<IInstitutionRepository> _institutionRepositoryMock;
            private Mock<ILogger> _loggerMock;
            private IAdapterRegistry _adapterRegistry;
            private IFacilitiesService _institutionService;
            private IStudentReferenceDataRepository _studentReferenceDataRepository;
            private IReferenceDataRepository _referenceDataRepository;
            
            private IPersonRepository _personRepository;
            private ILogger _logger;
            private AcademicProgramService _academicProgramService;
            private ICollection<Domain.Student.Entities.AcademicProgram> _academicProgramCollection = new List<Domain.Student.Entities.AcademicProgram>();
            private ICollection<Organization> _organizationCollection = new List<Organization>();
            private ICollection<AcademicCredential> _credentialCollection = new List<AcademicCredential>();
            private ICollection<AcademicDiscipline> _disciplineCollection = new List<AcademicDiscipline>();
            private ICollection<Domain.Student.Entities.AcademicLevel> _academicLevelCollection = new List<Domain.Student.Entities.AcademicLevel>();

            [TestInitialize]
            public void Initialize()
            {
                SetupValidRepositoryData();

                _academicProgramService = new AcademicProgramService(_adapterRegistry, _institutionService, _studentReferenceDataRepository, _referenceDataRepository, _personRepository,  _logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                _academicProgramCollection = null;
                _academicProgramService = null;
            }

            [TestMethod]
            public async Task AcademicProgramService__AcademicPrograms_NonHeDM()
            {
                var results = await _academicProgramService.GetAsync();
                Assert.IsTrue(results is IEnumerable<Dtos.Student.AcademicProgram>);
                Assert.IsNotNull(results);
            }

            [TestMethod]
            public async Task AcademicProgramService_AcademicPrograms_NonHeDM_Count()
            {
                var results = await _academicProgramService.GetAsync();
                Assert.AreEqual(3, results.Count());
            }

            [TestMethod]
            public async Task AcademicProgramService_AcademicPrograms_NonHeDM_Properties()
            {
                var results = await _academicProgramService.GetAsync();
                var academicProgram = results.First(x => x.Code == "GR-UNDEC");
                Assert.IsNotNull(academicProgram.Code);
                Assert.IsNotNull(academicProgram.Description);
                Assert.IsNotNull(academicProgram.MinorCodes);

                academicProgram = results.First(x => x.Code == "BA-EDUC");
                Assert.IsNotNull(academicProgram.Code);
                Assert.IsNotNull(academicProgram.Description);
                Assert.IsNotNull(academicProgram.MajorCodes);

                academicProgram = results.First(x => x.Code == "MA-HIST");
                Assert.IsNotNull(academicProgram.Code);
                Assert.IsNotNull(academicProgram.Description);
                Assert.IsNotNull(academicProgram.SpecializationCodes);
            }

            [TestMethod]
            public async Task AcademicProgramService_AcademicPrograms_NonHeDM_Expected()
            {
                var expectedResults = _academicProgramCollection.First(c => c.Code == "GR-UNDEC");
                var results = await _academicProgramService.GetAsync();
                var academicProgram = results.First(s => s.Code == "GR-UNDEC");
                Assert.AreEqual(expectedResults.Code, academicProgram.Code);
                Assert.AreEqual(expectedResults.Description, academicProgram.Description);
                Assert.AreEqual(expectedResults.MinorCodes.FirstOrDefault(), academicProgram.MinorCodes.FirstOrDefault());

                expectedResults = _academicProgramCollection.First(c => c.Code == "BA-EDUC");
                academicProgram = results.First(s => s.Code == "BA-EDUC");
                Assert.AreEqual(expectedResults.Code, academicProgram.Code);
                Assert.AreEqual(expectedResults.Description, academicProgram.Description);
                Assert.AreEqual(expectedResults.MajorCodes.FirstOrDefault(), academicProgram.MajorCodes.FirstOrDefault());
                
                expectedResults = _academicProgramCollection.First(c => c.Code == "MA-HIST");
                academicProgram = results.First(s => s.Code == "MA-HIST");
                Assert.AreEqual(expectedResults.Code, academicProgram.Code);
                Assert.AreEqual(expectedResults.Description, academicProgram.Description);
                Assert.AreEqual(expectedResults.CertificateCodes.FirstOrDefault(), academicProgram.CertificateCodes.FirstOrDefault());
                Assert.AreEqual(expectedResults.SpecializationCodes.FirstOrDefault(), academicProgram.SpecializationCodes.FirstOrDefault());
            }

            //[TestMethod]
            //public async Task AcademicProgramService__AcademicPrograms()
            //{
            //    var results = await _academicProgramService.GetAcademicProgramsAsync(false);
            //    Assert.IsTrue(results is IEnumerable<AcademicProgram>);
            //    Assert.IsNotNull(results);
            //}

            //[TestMethod]
            //public async Task AcademicProgramService_AcademicPrograms_Count()
            //{
            //    var results = await _academicProgramService.GetAcademicProgramsAsync(false);
            //    Assert.AreEqual(3, results.Count());
            //}

            //[TestMethod]
            //public async Task AcademicProgramService_AcademicPrograms_Properties()
            //{
            //    var results = await _academicProgramService.GetAcademicProgramsAsync(false);
            //    var academicProgram = results.First(x => x.Code == "GR-UNDEC");
            //    Assert.IsNotNull(academicProgram.Id);
            //    Assert.IsNotNull(academicProgram.Code);
            //    Assert.IsNotNull(academicProgram.StartDate);
            //    Assert.IsNotNull(academicProgram.EndDate);
            //    Assert.IsNotNull(academicProgram.Status);
            //}

            //[TestMethod]
            //public async Task AcademicProgramService_AcademicPrograms_Expected()
            //{
            //    var expectedResults = _academicProgramCollection.First(c => c.Code == "MA-HIST");
            //    var results = await _academicProgramService.GetAcademicProgramsAsync(false);
            //    var academicProgram = results.First(s => s.Code == "MA-HIST");
            //    Assert.AreEqual(expectedResults.Guid, academicProgram.Id);
            //    Assert.AreEqual(expectedResults.Code, academicProgram.Code);
            //    Assert.AreEqual(expectedResults.StartDate, academicProgram.StartDate);
            //    Assert.AreEqual(expectedResults.EndDate, academicProgram.EndDate);
            //    Assert.AreEqual(AcademicProgramStatus.active, academicProgram.Status);
            //}

            [TestMethod]
            [ExpectedException(typeof (KeyNotFoundException))]
            public async Task AcademicProgramService_GetAcademicProgramByGuid_Empty()
            {
                await _academicProgramService.GetAcademicProgramByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof (KeyNotFoundException))]
            public async Task AcademicProgramService_GetAcademicProgramByGuid_Null()
            {
                await _academicProgramService.GetAcademicProgramByGuidAsync(null);
            }

            //[TestMethod]
            //public async Task AcademicProgramService_GetAcademicProgramByGuid_Expected()
            //{
            //    var expectedResults =
            //        _academicProgramCollection.First(c => c.Guid == "1df164eb-8178-4321-a9f7-24f12d3991d8");
            //    var academicProgram =
            //        await _academicProgramService.GetAcademicProgramByGuidAsync("1df164eb-8178-4321-a9f7-24f12d3991d8");
            //    Assert.AreEqual(expectedResults.Guid, academicProgram.Id);
            //    Assert.AreEqual(expectedResults.Code, academicProgram.Code);
            //    Assert.AreEqual(expectedResults.StartDate, academicProgram.StartDate);
            //    Assert.AreEqual(expectedResults.EndDate, academicProgram.EndDate);
            //    Assert.AreEqual(AcademicProgramStatus.inactive, academicProgram.Status);
            //}

            //[TestMethod]
            //public async Task AcademicProgramService_GetAcademicProgramByGuid_Properties()
            //{
            //     var academicProgram =
            //        await _academicProgramService.GetAcademicProgramByGuidAsync("1df164eb-8178-4321-a9f7-24f12d3991d8");
            //    Assert.IsNotNull(academicProgram.Id);
            //    Assert.IsNotNull(academicProgram.Code);
            //    Assert.IsNotNull(academicProgram.StartDate);
            //    Assert.IsNotNull(academicProgram.EndDate);
            //    Assert.IsNotNull(academicProgram.Status);
            //}

            #region V10 GET/GET GET ALL

            [TestMethod]
            public async Task AcademicProgramService_GetAcademicProgramByGuid3_Expected()
            {
                var expectedResults =
                    _academicProgramCollection.First(c => c.Guid == "1df164eb-8178-4321-a9f7-24f12d3991d8");
                var academicProgram =
                    await _academicProgramService.GetAcademicProgramByGuid3Async("1df164eb-8178-4321-a9f7-24f12d3991d8");
                Assert.AreEqual(expectedResults.Guid, academicProgram.Id);
                Assert.AreEqual(expectedResults.Code, academicProgram.Code);
                Assert.AreEqual(expectedResults.StartDate, academicProgram.StartDate);
                Assert.AreEqual(expectedResults.EndDate, academicProgram.EndDate);
                Assert.AreEqual(AcademicProgramStatus.inactive, academicProgram.Status);
            }

            [TestMethod]
            public async Task AcademicProgramService_GetAcademicProgramByGuid3_Properties()
            {
                var academicProgram =
                   await _academicProgramService.GetAcademicProgramByGuid3Async("1df164eb-8178-4321-a9f7-24f12d3991d8");
                Assert.IsNotNull(academicProgram.Id);
                Assert.IsNotNull(academicProgram.Code);
                Assert.IsNotNull(academicProgram.StartDate);
                Assert.IsNotNull(academicProgram.EndDate);
                Assert.IsNotNull(academicProgram.Status);
            }

            #endregion






            private void SetupValidRepositoryData()
            {                
                // Mock InstitutionService for Organizations
                _institutionServiceMock = new Mock<IFacilitiesService>();
                _institutionService = _institutionServiceMock.Object;
                _organizationCollection.Add(new Organization()
                    {
                        Abbreviation = "MATH",
                        Guid = "1df164eb-8178-4321-a9f7-24f27f3991d8",
                        Type = OrganizationType.Department
                    });
                _organizationCollection.Add(new Organization()
                    {
                        Abbreviation = "ENGL",
                        Guid = "1df164eb-3367-8374-a9f7-24f27f3991d8",
                        Type = OrganizationType.Department
                    });
                _organizationCollection.Add(new Organization()
                    {
                        Abbreviation = "HIST",
                        Guid = "1df164eb-8178-2298-b8h7-24f27f3991d8",
                        Type = OrganizationType.Department
                    });
                _institutionServiceMock.Setup(svc => svc.GetOrganizationsAsync()).ReturnsAsync(_organizationCollection);
                
                // Mock Reference Repository for Academic Program Entities
                _studentReferenceDataRepositoryMock = new Mock<IStudentReferenceDataRepository>();
                _studentReferenceDataRepository = _studentReferenceDataRepositoryMock.Object;

                _academicProgramCollection.Add(new Domain.Student.Entities.AcademicProgram("9C3B805D-CFE6-483B-86C3-4C20562F8C15", "BA-EDUC", "BA in Education")
                    {
                        StartDate = new DateTime(2011, 12, 31),
                        DeptartmentCodes = new List<string>() { "HIST", "ENGL" },
                        DegreeCode = "BA",
                        AcadLevelCode = "UG",
                        MajorCodes = new List<string>() { "HIST" },
                        AuthorizingInstitute = new List<string>()
                    });
                _academicProgramCollection.Add(new Domain.Student.Entities.AcademicProgram("73244057-D1EC-4094-A0B7-DE602533E3A6", "MA-HIST", "MA History")
                    {
                        StartDate = new DateTime(2012, 12, 31),
                        CertificateCodes = new List<string>() { "TEACH" },
                        AcadLevelCode = "CE",
                        SpecializationCodes = new List<string>() { "CHIL" },
                        AuthorizingInstitute = new List<string>()
                    });
                _academicProgramCollection.Add(new Domain.Student.Entities.AcademicProgram("1df164eb-8178-4321-a9f7-24f12d3991d8", "GR-UNDEC", "Graduate Undecided")
                    {
                        StartDate = new DateTime(2012, 12, 31),
                        EndDate = new DateTime(2016, 12, 31),
                        HonorCode = "ACA",
                        AcadLevelCode = "GR",
                        MinorCodes = new List<string>() { "ENGL" },
                        AuthorizingInstitute = new List<string>()
                    });
                _studentReferenceDataRepositoryMock.Setup(repo => repo.GetAcademicProgramsAsync(false)).ReturnsAsync(_academicProgramCollection);
                _studentReferenceDataRepositoryMock.Setup(repo => repo.GetAcademicProgramsAsync(true)).ReturnsAsync(_academicProgramCollection);
                // Mock Reference Repository for Academic Level Entities
                _academicLevelCollection.Add(new Domain.Student.Entities.AcademicLevel("74588696-D1EC-2267-A0B7-DE602533E3A6", "UG", "Undergraduate"));
                _academicLevelCollection.Add(new Domain.Student.Entities.AcademicLevel("74826546-D1EC-2267-A0B7-DE602533E3A6", "GR", "Graduate"));
                _academicLevelCollection.Add(new Domain.Student.Entities.AcademicLevel("54364536-D1EC-2267-A0B7-DE602533E3A6", "CE", "Continuing Education"));
                _studentReferenceDataRepositoryMock.Setup(repo => repo.GetAcademicLevelsAsync()).ReturnsAsync(_academicLevelCollection);
                
                // Mock Academic Credentials Service
               _credentialCollection.Add(new AcademicCredential()
                    {
                        Abbreviation = "BA",
                        Id = "1df164eb-8178-4321-a9f7-24f27f4456d8",
                        Title = "Bachelor of Arts",
                        AcademicCredentialType = AcademicCredentialType.Degree
                    });
                _credentialCollection.Add(new AcademicCredential()
                    {
                        Abbreviation = "ACA",
                        Id = "2br164eb-8178-4321-a9f7-24f27f4456d8",
                        Title = "General Academic Honors",
                        AcademicCredentialType = AcademicCredentialType.Honorary
                    });
                _credentialCollection.Add(new AcademicCredential()
                    {
                        Abbreviation = "TEACH",
                        Id = "2br164eb-8178-4321-a9f7-24f27f2276d8",
                        Title = "Teaching Certificate",
                        AcademicCredentialType = AcademicCredentialType.Certificate
                    });
               // _academicCredentialServiceMock.Setup(svc => svc.GetAcademicCredentialsAsync(false)).ReturnsAsync(_credentialCollection);

                // Mock Academic Disciplines Service for Majors, Minors and Specializations
                _disciplineCollection.Add(new AcademicDiscipline()
                    {
                        Abbreviation = "HIST",
                        Title = "History",
                        Id = "2br164eb-8178-4321-a9f7-24f27f2276d8",
                        Type = AcademicDisciplineType.Major
                    });
                _disciplineCollection.Add(new AcademicDiscipline()
                    {
                        Abbreviation = "ENGL",
                        Title = "English",
                        Id = "2fd164eb-8178-4333-a9f7-24f27f2276d8",
                        Type = AcademicDisciplineType.Minor
                    });
                _disciplineCollection.Add(new AcademicDiscipline()
                    {
                        Abbreviation = "CHIL",
                        Title = "Child Development",
                        Id = "2fd164eb-8178-4487-a9f7-24f27f2276d8",
                        Type = AcademicDisciplineType.Concentration
                    });
                //_academicDisciplineServiceMock.Setup(svc => svc.GetAcademicDisciplinesAsync(false)).ReturnsAsync(_disciplineCollection);

                
                var testRefDataRepo = new Domain.Base.Tests.TestAcademicDisciplineRepository();
                


                Mock<IReferenceDataRepository> refdataRepo = new Mock<IReferenceDataRepository>();
                refdataRepo.Setup(rd => rd.GetOtherMajorsAsync(false)).ReturnsAsync(testRefDataRepo.GetOtherMajors());
                refdataRepo.Setup(rd => rd.GetOtherMinorsAsync(false)).ReturnsAsync(testRefDataRepo.GetOtherMinors());
                refdataRepo.Setup(rd => rd.GetOtherSpecialsAsync(false)).ReturnsAsync(testRefDataRepo.GetOtherSpecials());

                _referenceDataRepository = refdataRepo.Object; 




                _loggerMock = new Mock<ILogger>();
                _logger = _loggerMock.Object;


                _adapterRegistryMock = new Mock<IAdapterRegistry>();
                _adapterRegistry = _adapterRegistryMock.Object;
                var academicProgramAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.AcademicProgram, Dtos.Student.AcademicProgram>(_adapterRegistry, _logger);
                _adapterRegistryMock.Setup(reg => reg.GetAdapter<Ellucian.Colleague.Domain.Student.Entities.AcademicProgram, Dtos.Student.AcademicProgram>()).Returns(academicProgramAdapter);
            }
        }
    }
}