//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 8/13/2014 4:17:53 PM by user kmf
//
//     Type: ENTITY
//     Entity: IMMEDIATE.PAYMENT.CONTROL
//     Application: ST
//     Environment: dvcoll_wstst01
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;
using Ellucian.Data.Colleague;

namespace Ellucian.Colleague.Data.Finance.DataContracts
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract(Name = "ImmediatePaymentControl")]
	[ColleagueDataContract(GeneratedDateTime = "8/13/2014 4:17:53 PM", User = "kmf")]
	[EntityDataContract(EntityName = "IMMEDIATE.PAYMENT.CONTROL", EntityType = "PERM")]
	public class ImmediatePaymentControl : IColleagueEntity
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		/// <summary>
		/// Record ID
		/// </summary>
		[DataMember]
		public string Recordkey { get; set; }
		
		public void setKey(string key)
		{
			Recordkey = key;
		}	
		
		/// <summary>
		/// CDD Name: IPC.ENABLED
		/// </summary>
		[DataMember(Order = 0, Name = "IPC.ENABLED")]
		public string IpcEnabled { get; set; }
		
		/// <summary>
		/// CDD Name: IPC.TERMS.AND.CONDITIONS.DOC
		/// </summary>
		[DataMember(Order = 1, Name = "IPC.TERMS.AND.CONDITIONS.DOC")]
		public string IpcTermsAndConditionsDoc { get; set; }
		
		/// <summary>
		/// CDD Name: IPC.CANCELLATION.DOC
		/// </summary>
		[DataMember(Order = 2, Name = "IPC.CANCELLATION.DOC")]
		public string IpcCancellationDoc { get; set; }
		
		/// <summary>
		/// CDD Name: IPC.DEFERRAL.DOC
		/// </summary>
		[DataMember(Order = 3, Name = "IPC.DEFERRAL.DOC")]
		public string IpcDeferralDoc { get; set; }
		
		/// <summary>
		/// CDD Name: IPC.REG.ACKNOWLEDGEMENT.DOC
		/// </summary>
		[DataMember(Order = 4, Name = "IPC.REG.ACKNOWLEDGEMENT.DOC")]
		public string IpcRegAcknowledgementDoc { get; set; }
		
	
		// build up all the Associated objects and add them to the properties
		public void buildAssociations()
		{	
			   
		}
	}
	
	// EntityAssociation classes
}