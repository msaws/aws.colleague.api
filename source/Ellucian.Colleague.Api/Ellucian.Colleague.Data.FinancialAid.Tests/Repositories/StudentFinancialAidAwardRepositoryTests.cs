﻿/*Copyright 2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Data.FinancialAid.Tests.Repositories;
using Ellucian.Colleague.Data.FinancialAid.DataContracts;
using Ellucian.Colleague.Data.FinancialAid.Repositories;
using Ellucian.Colleague.Domain.FinancialAid.Entities;
using Ellucian.Colleague.Domain.FinancialAid.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.FinancialAid.Transactions;
using Ellucian.Colleague.Domain.FinancialAid.Exceptions;
using Ellucian.Data.Colleague;
using Ellucian.Web.Cache;
using slf4net;
using Ellucian.Web.Http.Configuration;
using System.Threading;

namespace Ellucian.Colleague.Data.FinancialAid.Tests.Repositories
{
    [TestClass]
    public class StudentFinancialAidAwardRepositoryTests
    {
        #region SETUP
        Mock<IColleagueTransactionFactory> transFactoryMock;
        Mock<ICacheProvider> cacheProviderMock;
        Mock<IColleagueDataReader> dataReaderMock;
        Mock<ILogger> loggerMock;

        string valcodeName;
        ApiSettings apiSettings;

        StudentFinancialAidAwardRepository awardRepo;
        IEnumerable<StudentFinancialAidAward> allFinancialAidAwards;
        IEnumerable<StudentAwardHistoryByPeriod> allStudentAwardHistoryByPeriods;
        IEnumerable<StudentAwardHistoryStatus> allStudentAwardHistoryStatuses;
        Collection<Ellucian.Colleague.Data.FinancialAid.DataContracts.TcAcyr> tcAcyrDataContracts;

        public string awardId;
        public string[] studentFinancialAidAwardIds;
        //public IEnumerable<string> fundYears;
        StudentFinancialAidAwardRepository repositoryUnderTest;
        public TestStudentFinancialAidAwardRepository testDataRepository;

        public async Task<IEnumerable<StudentFinancialAidAward>> getExpectedStudentFinancialAidAwards()
        {
            return await testDataRepository.GetStudentFinancialAidAwardsAsync(false);
        }

        public async Task<Tuple<IEnumerable<StudentFinancialAidAward>, int>> getActualStudentFinancialAidAwards()
        {
            return await awardRepo.GetAsync(0, 10, false, false, new List<string>() { "FUND1", "FUND2" }, new List<string>() { "YEAR1", "YEAR2" } );
            //return await awardRepo.GetAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<IEnumerable<string>>(), It.IsAny<IEnumerable<string>>());
        }

        public async Task<StudentFinancialAidAward> getActualStudentFinancialAidAwardsById(string id)
        {
            return await awardRepo.GetByIdAsync(id);
            //return await awardRepo.GetAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<IEnumerable<string>>(), It.IsAny<IEnumerable<string>>());
        }

        [TestInitialize]
        public void MockInitialize()
        {
            loggerMock = new Mock<ILogger>();
            apiSettings = new ApiSettings("TEST");

            allFinancialAidAwards = new TestStudentFinancialAidAwardRepository().GetStudentFinancialAidAwardsAsync(false).Result;
            allStudentAwardHistoryByPeriods = new TestStudentFinancialAidAwardRepository().GetStudentAwardHistoryByPeriodsAsync().Result;
            allStudentAwardHistoryStatuses = new TestStudentFinancialAidAwardRepository().GetStudentAwardHistoryStatusesAsync().Result;

            studentFinancialAidAwardIds = new string[2];
            studentFinancialAidAwardIds[0] = "5*FUND1";
            studentFinancialAidAwardIds[1] = "6*FUND2";
            //awardId = "0003914";

            tcAcyrDataContracts = new Collection<DataContracts.TcAcyr>() 
                {
                    new DataContracts.TcAcyr()
                    {
                        Recordkey = "1", 
                        RecordGuid = "bb66b971-3ee0-4477-9bb7-539721f93434", 
                        //VisaType = "F1", 
                        //VisaIssuedDate = new DateTime(2016, 01, 01), 
                        //VisaExpDate = new DateTime(2017, 12, 31), 
                        //PersonCountryEntryDate = new DateTime(2016, 02, 01)
                    },
                    new DataContracts.TcAcyr()
                    {
                        Recordkey = "2", 
                        RecordGuid = "5aeebc5c-c973-4f83-be4b-f64c95002124", 
                        //VisaType = "F1", 
                        //VisaIssuedDate = new DateTime(2015, 01, 01), 
                        //VisaExpDate = new DateTime(2018, 12, 31), 
                        //PersonCountryEntryDate = new DateTime(2015, 02, 01)
                    },
                    new DataContracts.TcAcyr()
                    {
                        Recordkey = "3", 
                        RecordGuid = "27178aab-a6e8-4d1e-ae27-eca1f7b33363", 
                        //VisaType = "F1", 
                        //VisaIssuedDate = new DateTime(2014, 01, 01), 
                        //VisaExpDate = new DateTime(2019, 12, 31), 
                        //PersonCountryEntryDate = new DateTime(2014, 02, 01)
                    },
                };

            //fundYears = new List<string>() {"2008"};
            testDataRepository = new TestStudentFinancialAidAwardRepository();

            awardRepo = BuildValidFundRepository();
            //valcodeName = awardRepo.BuildFullCacheKey("AllFinancialAidFunds");
        }

        [TestCleanup]
        public void Cleanup()
        {
            allFinancialAidAwards = null;
            valcodeName = string.Empty;
            apiSettings = null;
        }
        #endregion

        #region MOCK EVENTS
        private StudentFinancialAidAwardRepository BuildValidFundRepository()
        {
            // transaction factory mock
            transFactoryMock = new Mock<IColleagueTransactionFactory>();

            // Cache Provider Mock
            cacheProviderMock = new Mock<ICacheProvider>();

            // Set up data accessor for mocking 
            dataReaderMock = new Mock<IColleagueDataReader>();
            apiSettings = new ApiSettings("TEST");

            // Set up dataAccessorMock as the object for the DataAccessor
            transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(dataReaderMock.Object);

            //var records = new Collection<DataContracts.Awards>();
            //foreach (var item in allFinancialAidAwards)
            //{
            //    DataContracts.Awards record = new DataContracts.Awards();
            //    record.RecordGuid = item.Guid;
            //    record.AwDescription = item.Description;
            //    record.Recordkey = item.Code;
            //    record.AwExplanationText = item.Description2;
            //    record.AwType = item.Source;
            //    record.AwCategory = item.CategoryCode;
            //    record.AwReportingFundingType = item.FundingType;
            //    records.Add(record);
            //}
            //dataReaderMock.Setup(acc => acc.BulkReadRecordAsync<DataContracts.Awards>("AWARDS", "", true)).ReturnsAsync(records);

            cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
             x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
             .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

            //dataReaderMock.Setup(acc => acc.SelectAsync(It.IsAny<RecordKeyLookup[]>())).Returns<RecordKeyLookup[]>(recordKeyLookups =>
            //{
            //    var result = new Dictionary<string, RecordKeyLookupResult>();
            //    foreach (var recordKeyLookup in recordKeyLookups)
            //    {
            //        var record = allFinancialAidAwards.Where(e => e.StudentId == recordKeyLookup.PrimaryKey).FirstOrDefault();
            //        result.Add(string.Join("+", new string[] { "TC.YEAR1", record.StudentId }),
            //            new RecordKeyLookupResult() { Guid = record.Guid });
            //    }
            //    return Task.FromResult(result);
            //});

            dataReaderMock.Setup(acc => acc.SelectAsync(It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(studentFinancialAidAwardIds);
            //(ids =>
            //{
            //    var result = "";
            //    foreach (var id in ids)
            //    {
            //        var record = allFinancialAidAwards.Where(e => e.AwardFundId == id).FirstOrDefault();
            //        result.Add(string.Join("+", new string[] { "TC.YEAR1", record.StudentId }),
            //            new RecordKeyLookupResult() { Guid = record.Guid });
            //    }
            //    return Task.FromResult(result);
            //});

            var tcAcyrRecords = new Collection<DataContracts.TcAcyr>();
            foreach (var item in allFinancialAidAwards)
            {
                DataContracts.TcAcyr record = new DataContracts.TcAcyr();
                record.Recordkey = item.StudentId + "*" + item.AwardFundId;
                record.TcTaTerms = new List<string>() { item.StudentId + "*" + item.AwardFundId };
                record.RecordGuid = allFinancialAidAwards.Where(f => f.StudentId == item.StudentId).FirstOrDefault().Guid;
                //record.FundofcOverAmt = (long) item.MaximumOfferedBudgetAmount;
                //record.FundofcOverPct = null;
                tcAcyrRecords.Add(record);
            }

            var taAcyrRecords = new Collection<DataContracts.TaAcyr>();
            foreach (var item in allStudentAwardHistoryByPeriods)
            {
                DataContracts.TaAcyr record = new DataContracts.TaAcyr();
                record.Recordkey = allFinancialAidAwards.FirstOrDefault().StudentId + "*" + allFinancialAidAwards.FirstOrDefault().AwardFundId + "*" + allFinancialAidAwards.FirstOrDefault().AidYearId;
                record.TaTermAction = item.Status;
                record.TaTermActionDate = item.StatusDate;
                record.TaTermAmount = item.Amount;
                record.TaTermXmitAmt = item.XmitAmount;
                //record.FundofcOverAmt = (long) item.MaximumOfferedBudgetAmount;
                //record.FundofcOverPct = null;
                taAcyrRecords.Add(record);
            }
            //dataReaderMock.Setup(acc => acc.BulkReadRecordAsync<DataContracts.FundOfficeAcyr>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(acyrRecords);

            dataReaderMock.Setup(d => d.BulkReadRecordAsync<TcAcyr>(It.IsAny<string>(), It.IsAny<string[]>(), It.IsAny<bool>()))
                .Returns<string, string[], bool>((x, y, z) =>
                    Task.FromResult(new Collection<TcAcyr>(allFinancialAidAwards.Select(a => 
                        new TcAcyr() 
                        {
                            Recordkey = a.StudentId + "*" + a.AwardFundId,
                            TcTaTerms = new List<string>() { a.StudentId + "*" + a.AwardFundId + "*" + allFinancialAidAwards.FirstOrDefault().AidYearId },
                            RecordGuid = allFinancialAidAwards.Where(f => f.StudentId == a.StudentId).FirstOrDefault().Guid,
                        }).ToList())));

            dataReaderMock.Setup(d => d.BulkReadRecordAsync<TaAcyr>(It.IsAny<string>(), It.IsAny<string[]>(), It.IsAny<bool>()))
                .Returns<string, string[], bool>((x, y, z) =>
                    Task.FromResult(new Collection<TaAcyr>(allStudentAwardHistoryByPeriods.Select(a =>
                        new TaAcyr()
                        {
                            Recordkey = allFinancialAidAwards.FirstOrDefault().StudentId + "*" + allFinancialAidAwards.FirstOrDefault().AwardFundId + "*" + allFinancialAidAwards.FirstOrDefault().AidYearId,
                            TaTermAction = a.Status,
                            TaTermActionDate = a.StatusDate,
                            TaTermAmount = a.Amount,
                            TaTermXmitAmt = a.XmitAmount
                        }).ToList())));

            dataReaderMock.Setup(d => d.BulkReadRecordAsync<FaAwardHistory>(It.IsAny<string[]>(), It.IsAny<bool>()))
                .Returns<string[], bool>((x, y) =>
                    Task.FromResult(new Collection<FaAwardHistory>(allStudentAwardHistoryByPeriods.Select(a =>
                        new FaAwardHistory()
                        {
                            Recordkey = allFinancialAidAwards.FirstOrDefault().StudentId + "*" + allFinancialAidAwards.FirstOrDefault().AwardFundId + "*" + allFinancialAidAwards.FirstOrDefault().AidYearId,
                            FawhCrntTermAction = a.Status,
                            FaAwardHistoryChgdate = a.StatusDate,
                            FawhChgTime = a.StatusDate,
                            FawhCrntTermAmt = a.Amount,
                        }).ToList())));

            dataReaderMock.Setup(acc => acc.SelectAsync(It.IsAny<GuidLookup[]>())).Returns<GuidLookup[]>(gla =>
            {
                var result = new Dictionary<string, GuidLookupResult>();
                foreach (var gl in gla)
                {
                    var rel = tcAcyrDataContracts.FirstOrDefault(x => x.RecordGuid == gl.Guid);
                    result.Add(gl.Guid, rel == null ? null : new GuidLookupResult() { Entity = "TC.ACYR", PrimaryKey = rel.Recordkey });
                }
                return Task.FromResult(result);
            });

            dataReaderMock.Setup(d => d.ReadRecordAsync<TcAcyr>(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>()))
                .Returns<string, string, bool>((x, y, z) => Task.FromResult(tcAcyrRecords.FirstOrDefault()));

            // Construct repository
            awardRepo = new StudentFinancialAidAwardRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);

            return awardRepo;
        }


        #endregion

        #region GetStudentFinancialAidAwardsTests
        [TestClass]
        public class GetStudentFinancialAidAwardsTests : StudentFinancialAidAwardRepositoryTests
        {
            [TestInitialize]
            public void Initialize()
            {
                MockInitialize();
            }

            #region TESTS FOR FUNCTIONALITY
            [TestMethod]
            public async Task ExpectedEqualsActualStudentFinancialAidAwardsTest()
            {
                var expected = await getExpectedStudentFinancialAidAwards();
                var actual = await getActualStudentFinancialAidAwards();
                Assert.AreEqual(expected.Count(), actual.Item2);
            }

            //[TestMethod]
            //public async Task ActualFinancialAidFundsNoFundYearsTest()
            //{
            //    var actual = await getActualFinancialAidFundsFinancialsNoYears();
            //    Assert.AreEqual(0, actual.Count());
            //}

            [TestMethod]
            public async Task ExpectedEqualsActualStudentFinancialAidAwardsByIdTest()
            {
                var expected = await getExpectedStudentFinancialAidAwards();
                var actual = await getActualStudentFinancialAidAwardsById(expected.FirstOrDefault().Guid);
                Assert.AreEqual(expected.FirstOrDefault().Guid, actual.Guid);
            }

            [TestMethod]
            public async Task StudentFinancialAidAwardRepository_GetStudentFinancialAidAwardsAsync_False()
            {
                var results = await getActualStudentFinancialAidAwards();
                Assert.AreEqual(allFinancialAidAwards.Count(), results.Item2);

                foreach (var financialAidAward in allFinancialAidAwards)
                {
                    var result = results.Item1.FirstOrDefault(i => i.Guid == financialAidAward.Guid);

                    //Assert.AreEqual(financialAidFund.Code, result.Code);
                    //Assert.AreEqual(financialAidFund.Description, result.Description);
                    Assert.AreEqual(financialAidAward.Guid, result.Guid);
                }

            }
            #endregion
        }
        #endregion

    }
}