﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Domain.HumanResources.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.HumanResources.Utilities
{
    public class TaxFormPdfUtility
    {
        public static void Populate1095CDependentRow(ref Dictionary<string, string> pdfData, Form1095cCoveredIndividualsPdfData coveredIndividual, int rowNumber)
        {
            string key = "Box" + rowNumber.ToString() + "Name";
            pdfData[key] = coveredIndividual.CoveredIndividualName();

            key = "Box" + rowNumber.ToString() + "SSN";
            pdfData[key] = coveredIndividual.CoveredIndividualSsn;

            key = "Box" + rowNumber.ToString() + "DOB";
            pdfData[key] = coveredIndividual.CoveredIndividualDateOfBirth.HasValue ? coveredIndividual.CoveredIndividualDateOfBirth.Value.ToString("yyyy-MM-dd") : string.Empty;

            key = "Box" + rowNumber.ToString() + "All";
            pdfData[key] = coveredIndividual.Covered12Month ? "X" : "";

            key = "Box" + rowNumber.ToString() + "Jan";
            pdfData[key] = coveredIndividual.CoveredJanuary ? "X" : "";

            key = "Box" + rowNumber.ToString() + "Feb";
            pdfData[key] = coveredIndividual.CoveredFebruary ? "X" : "";

            key = "Box" + rowNumber.ToString() + "Mar";
            pdfData[key] = coveredIndividual.CoveredMarch ? "X" : "";

            key = "Box" + rowNumber.ToString() + "Apr";
            pdfData[key] = coveredIndividual.CoveredApril ? "X" : "";

            key = "Box" + rowNumber.ToString() + "May";
            pdfData[key] = coveredIndividual.CoveredMay ? "X" : "";

            key = "Box" + rowNumber.ToString() + "June";
            pdfData[key] = coveredIndividual.CoveredJune ? "X" : "";

            key = "Box" + rowNumber.ToString() + "July";
            pdfData[key] = coveredIndividual.CoveredJuly ? "X" : "";

            key = "Box" + rowNumber.ToString() + "Aug";
            pdfData[key] = coveredIndividual.CoveredAugust ? "X" : "";

            key = "Box" + rowNumber.ToString() + "Sept";
            pdfData[key] = coveredIndividual.CoveredSeptember ? "X" : "";

            key = "Box" + rowNumber.ToString() + "Oct";
            pdfData[key] = coveredIndividual.CoveredOctober ? "X" : "";

            key = "Box" + rowNumber.ToString() + "Nov";
            pdfData[key] = coveredIndividual.CoveredNovember ? "X" : "";

            key = "Box" + rowNumber.ToString() + "Dec";
            pdfData[key] = coveredIndividual.CoveredDecember ? "X" : "";
        }
    }
}