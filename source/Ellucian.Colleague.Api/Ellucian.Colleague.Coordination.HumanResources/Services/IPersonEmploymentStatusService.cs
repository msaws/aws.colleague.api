﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Dtos.HumanResources;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.HumanResources.Services
{
    public interface IPersonEmploymentStatusService
    {
        Task<IEnumerable<PersonEmploymentStatus>> GetPersonEmploymentStatusesAsync();
    }
}
