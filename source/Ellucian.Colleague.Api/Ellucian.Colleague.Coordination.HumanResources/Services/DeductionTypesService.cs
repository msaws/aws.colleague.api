﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.HumanResources.Services
{
    [RegisterType]
    public class DeductionTypesService : BaseCoordinationService, IDeductionTypesService
    {
        private readonly IHumanResourcesReferenceDataRepository _referenceDataRepository;

        /// <summary>
        /// ..ctor
        /// </summary>
        /// <param name="referenceDataRepository"></param>
        /// <param name="adapterRegistry"></param>
        /// <param name="currentUserFactory"></param>
        /// <param name="roleRepository"></param>
        /// <param name="logger"></param>
        public DeductionTypesService(
            IHumanResourcesReferenceDataRepository referenceDataRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this._referenceDataRepository = referenceDataRepository;
        }

        /// <summary>
        /// Gets all deduction types.
        /// </summary>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Dtos.DeductionType>> GetDeductionTypesAsync(bool bypassCache = false)
        {
            var deductionTypes = new List<Dtos.DeductionType>();

            var deductionTypeEntities = await _referenceDataRepository.GetDeductionTypesAsync(bypassCache);
            if (deductionTypeEntities != null && deductionTypeEntities.Any())
            {
                foreach (var deductionType in deductionTypeEntities)
                {
                    deductionTypes.Add(ConvertDeductionTypeEntityToDto(deductionType));
                }
            }
            return deductionTypes.Any() ? deductionTypes : null;
        }

        /// <summary>
        /// Gets a deduction type by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Dtos.DeductionType> GetDeductionTypeByIdAsync(string id)
        {
            try
            {
                var entities = await _referenceDataRepository.GetDeductionTypesAsync(true);

                var entity = entities.FirstOrDefault(i => i.Guid.Equals(id));
                if (entity == null)
                {
                    throw new KeyNotFoundException("Deduction type not found for GUID " + id);
                }
                return ConvertDeductionTypeEntityToDto(entity);
            }
            catch (KeyNotFoundException ex)
            {
                throw ex;
            }
            catch (Exception)
            {
                throw new Exception("Unknown error getting deduction type.");
            }
        }

        /// <summary>
        /// Converts domain entity into dto.
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private Dtos.DeductionType ConvertDeductionTypeEntityToDto(Domain.HumanResources.Entities.DeductionType source)
        {
            var deductionType = new Dtos.DeductionType();

            deductionType.Id = source.Guid;
            deductionType.Code = source.Code;
            deductionType.Title = source.Description;
            deductionType.Description = null;

            return deductionType;
        }
    }
}
