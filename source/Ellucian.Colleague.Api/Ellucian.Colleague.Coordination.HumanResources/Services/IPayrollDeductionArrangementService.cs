﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Dtos;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;

namespace Ellucian.Colleague.Coordination.HumanResources.Services
{
    public interface IPayrollDeductionArrangementService : IBaseService
    {
        /// <summary>
        /// Accept requests from external systems for new employee deductions in the authoritative HR system.
        /// </summary>
        /// <param name="offset">Page offset for paging.</param>
        /// <param name="limit">Page limit for paging.</param>
        /// <param name="bypassCache">Bypass use of cache and read directly from disk.</param>
        /// <param name="person">Person GUID filter.</param>
        /// <param name="contribution">Contribution ID filter.</param>
        /// <param name="deductionType">Deduction Type filter.</param>
        /// <param name="statusType">Status Type filter.</param>
        /// <returns>IEnumerable of Objects of type <see cref="Dtos.PayrollDeductionArrangement"/></returns>
        Task<Tuple<IEnumerable<Dtos.PayrollDeductionArrangement>, int>> GetPayrollDeductionArrangementsAsync(int offset, int limit, bool bypassCache = false,
            string person = "", string contribution = "", string deductionType = "", string status = "");
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<Dtos.PayrollDeductionArrangement> GetPayrollDeductionArrangementByIdAsync(string id);
        
        /// <summary>
        /// Update a payroll deduction arrangement by guid
        /// </summary>
        /// <param name="id">guid for the payroll deduction arrangement</param>
        /// <returns>PayrollDeductionArrangement DTO Object</returns>
        Task<Dtos.PayrollDeductionArrangement> PutPayrollDeductionArrangementAsync(string id, Dtos.PayrollDeductionArrangement payrollDeductionArrangementDto);

        /// <summary>
        /// Create a new payroll deduction arrangement
        /// </summary>
        /// <param name="id">guid for the address</param>
        /// <returns>Addresses DTO Object</returns>
        Task<Dtos.PayrollDeductionArrangement> PostPayrollDeductionArrangementAsync(Dtos.PayrollDeductionArrangement payrollDeductionArrangementDto);
    }
}
