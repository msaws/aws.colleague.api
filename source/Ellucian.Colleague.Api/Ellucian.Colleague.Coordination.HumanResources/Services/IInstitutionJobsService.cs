﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;

namespace Ellucian.Colleague.Coordination.HumanResources.Services
{
    /// <summary>
    /// Interface for InstitutionJobs Service
    /// </summary>
    public interface IInstitutionJobsService : IBaseService
    {
        Task<Tuple<IEnumerable<Ellucian.Colleague.Dtos.InstitutionJobs>, int>> GetInstitutionJobsAsync(int offset, int limit,
            string person = "", string employer = "", string position = "", string department = "", string startOn = "", 
            string endOn = "", string status = "", string classification = "", string preference = "", bool bypassCache = false);
        Task<Ellucian.Colleague.Dtos.InstitutionJobs> GetInstitutionJobsByGuidAsync(string guid);

        Task<Ellucian.Colleague.Dtos.InstitutionJobs> PutInstitutionJobsAsync(string guid, Ellucian.Colleague.Dtos.InstitutionJobs institutionJobs);

        Task<Ellucian.Colleague.Dtos.InstitutionJobs> PostInstitutionJobsAsync(Ellucian.Colleague.Dtos.InstitutionJobs institutionJobs);

    }
}