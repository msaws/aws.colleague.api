﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.HumanResources;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.HumanResources.Services
{
    /// <summary>
    /// Human Resource Demographics Service
    /// </summary>
    [RegisterType]
    public class HumanResourceDemographicsService : BaseCoordinationService, IHumanResourceDemographicsService
    {
        private readonly ISupervisorsRepository supervisorsRepository;
        private readonly IPersonBaseRepository personBaseRepository;

        /// <summary>
        /// Constructor for Human Resource Demographics Service
        /// </summary>
        /// <param name="personBaseRepository"></param>
        /// <param name="supervisorsRepository"></param>
        /// <param name="currentUserFactory"></param>
        /// <param name="roleRepository"></param>
        /// <param name="logger"></param>
        public HumanResourceDemographicsService(
             IPersonBaseRepository personBaseRepository,
            ISupervisorsRepository supervisorsRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger
            )
            : base(
              adapterRegistry, currentUserFactory, roleRepository, logger
                 )
        {
            this.supervisorsRepository = supervisorsRepository;
            this.personBaseRepository = personBaseRepository;
        }
        /// <summary>
        /// Gets all HumanResourceDemographics available to the user
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<HumanResourceDemographics>> GetHumanResourceDemographicsAsync()
        {
            var userAndSubordinateAndSupervisorIds = new List<string>() { CurrentUser.PersonId };

            var supervisorIds = (await supervisorsRepository.GetSupervisorsBySuperviseeAsync(CurrentUser.PersonId)).ToList();

            if (supervisorIds.Any())
            {
                userAndSubordinateAndSupervisorIds = userAndSubordinateAndSupervisorIds.Concat(supervisorIds).Distinct().ToList();
            }

            if (HasPermission(HumanResourcesPermissionCodes.ViewSuperviseeData))
            {
                var subordinateIds = (await supervisorsRepository.GetSuperviseesBySupervisorAsync(CurrentUser.PersonId)).ToList();

                if (subordinateIds == null)
                {
                    var message = "Unexpected null person id list (no subordinate ids) returned from supervisors repository";
                    logger.Info(message);
                }
                if (subordinateIds.Any())
                {
                    userAndSubordinateAndSupervisorIds = userAndSubordinateAndSupervisorIds.Concat(subordinateIds).Distinct().ToList();
                }
            }

            var personBaseEntities = await personBaseRepository.GetPersonsBaseAsync(userAndSubordinateAndSupervisorIds);

            if (personBaseEntities == null)
            {
                var message = "Null Person Base Entities returned to coordination service";
                logger.Info(message);
            }

            var personBaseEntityToHumanResourceDemographicsDtoAdapter = _adapterRegistry.GetAdapter<Domain.Base.Entities.PersonBase, Dtos.HumanResources.HumanResourceDemographics>();
            return personBaseEntities.Select(pb => personBaseEntityToHumanResourceDemographicsDtoAdapter.MapToType(pb)).ToList();
        }



        /// <summary>
        /// Gets HumanResourceDemographics for a specific person
        /// </summary>
        /// <returns></returns>
        public async Task<HumanResourceDemographics> GetSpecificHumanResourceDemographicsAsync(string id)
        {
            var userAndSubordinateIds = new List<string>() { CurrentUser.PersonId };
            Domain.Base.Entities.PersonBase personBaseEntity = null;

            //make sure the supplied Id is not null or empty
            if (string.IsNullOrWhiteSpace(id))
            {
                var message = "Supplied Id is null or empty";
                logger.Error(message);
                throw new ArgumentNullException(message);
            }
            // check to see if the current user has permission to access PersonBase entities
            if (HasPermission(HumanResourcesPermissionCodes.ViewSuperviseeData))
            {
                var subordinateIds = (await supervisorsRepository.GetSuperviseesBySupervisorAsync(CurrentUser.PersonId)).ToList();

                if (subordinateIds == null)
                {
                    var message = "Unexpected null person id list returned from supervisors repository";
                    logger.Info(message);
                }
                if (subordinateIds.Any())
                {
                    userAndSubordinateIds = userAndSubordinateIds.Concat(subordinateIds).ToList();
                }
            }
            if (userAndSubordinateIds.Contains(id))
            {
                personBaseEntity = await personBaseRepository.GetPersonBaseAsync(id, true);
            }
            else
            {
                var message = "The given Id did not appear in the User's list of accessible Ids";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            var personBaseEntityToHumanResourceDemographicsDtoAdapter = _adapterRegistry.GetAdapter<Domain.Base.Entities.PersonBase, Dtos.HumanResources.HumanResourceDemographics>();
            return personBaseEntityToHumanResourceDemographicsDtoAdapter.MapToType(personBaseEntity);
        }

    }
}
