﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.HumanResources;
using Ellucian.Colleague.Domain.HumanResources.Entities;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Dtos;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Data.Colleague;

namespace Ellucian.Colleague.Coordination.HumanResources.Services
{
    [RegisterType]
    public class EmployeeService : BaseCoordinationService, IEmployeeService
    {
        private readonly IPersonRepository personRepository;
        private readonly IEmployeeRepository employeeRepository;
        private readonly IReferenceDataRepository referenceDataRepository;
        private readonly IHumanResourcesReferenceDataRepository hrReferenceDataRepository;
        private readonly IConfigurationRepository configurationRepository;

        public EmployeeService(
            IPersonRepository personRepository,
            IEmployeeRepository employeeRepository,
            IReferenceDataRepository referenceDataRepository,
            IHumanResourcesReferenceDataRepository hrReferenceDataRepository,
            IConfigurationRepository configurationRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger, configurationRepository:configurationRepository)
        {
            this.personRepository = personRepository;
            this.employeeRepository = employeeRepository;
            this.referenceDataRepository = referenceDataRepository;
            this.hrReferenceDataRepository = hrReferenceDataRepository;
            this.configurationRepository = configurationRepository;
        }
        //get locations 
        private IEnumerable<Domain.Base.Entities.Location> _location = null;
        private async Task<IEnumerable<Domain.Base.Entities.Location>> GetLocationsAsync(bool bypassCache)
        {
            if (_location == null)
            {
                _location = await referenceDataRepository.GetLocationsAsync(bypassCache);
            }
            return _location;
        }
        //get statuses 
        private IEnumerable<PersonStatuses> _statuses = null;
        private async Task<IEnumerable<PersonStatuses>> GetPersonStatusesAsync(bool bypassCache)
        {
            if (_statuses == null)
            {
                _statuses = await hrReferenceDataRepository.GetPersonStatusesAsync(bypassCache);
            }
            return _statuses;
        }
        //get reasons
        private IEnumerable<Ellucian.Colleague.Domain.HumanResources.Entities.EmploymentStatusEndingReason> _reasons = null;
        private async Task<IEnumerable<Ellucian.Colleague.Domain.HumanResources.Entities.EmploymentStatusEndingReason>> GetEmploymentStatusEndingReasonsAsync(bool bypassCache)
        {
            if (_reasons == null)
            {
                _reasons = await hrReferenceDataRepository.GetEmploymentStatusEndingReasonsAsync(bypassCache);
            }
            return _reasons;
        }
        //get rehiretypes
        private IEnumerable<Ellucian.Colleague.Domain.HumanResources.Entities.RehireType> _rehireTypes = null;
        private async Task<IEnumerable<Ellucian.Colleague.Domain.HumanResources.Entities.RehireType>> GetRehireTypesAsync(bool bypassCache)
        {
            if (_rehireTypes == null)
            {
                _rehireTypes = await hrReferenceDataRepository.GetRehireTypesAsync(bypassCache);
            }
            return _rehireTypes;
        }

        /// <summary>
        /// Converts date to unidata Date
        /// </summary>
        /// <param name="date">UTC datetime</param>
        /// <returns>Unidata Date</returns>
        private async Task<string> ConvertDateArgument(string date)
        {
            try
            {
                return await employeeRepository.GetUnidataFormattedDate(date);
            }
            catch (Exception)
            {
                throw new ArgumentException("Invalid Date format in arguments");
            }
        }
        /// <summary>
        /// Get Employee Data based on the permissions of the current user
        /// </summary>
        /// <param name="bypassCache">Flag to bypass cache and read directly from disk.</param>
        /// <param name="offset">Offset for record index on page reads.</param>
        /// <param name="limit">Take number of records on page reads.</param>
        /// <param name="person">Person id filter.</param>
        /// <param name="campus">Primary campus or location filter.</param>
        /// <param name="status">Status ("active", "terminated", or "leave") filter.</param>
        /// <param name="startOn">Start on a specific date filter.</param>
        /// <param name="endOn">End on a specific date filter.</param>
        /// <param name="rehireableStatus">Rehireable status ("eligible" or "ineligible") filter.</param>
        /// <returns>Tuple of employee objects <see cref="Dtos.Employee"/> and count for paging.</returns>
        public async Task<Tuple<IEnumerable<Dtos.Employee>, int>> GetEmployeesAsync(int offset, int limit, bool bypassCache, string person = "",
            string campus = "", string status = "", string startOn = "", string endOn = "", string rehireableStatusEligibility = "" , string rehireableStatusType= "")
        {
            var employeeDtos = new List<Dtos.Employee>();
            int employeeCount = 0;
            var employeePersonIds = new List<string>();

            if (!HasPermission(HumanResourcesPermissionCodes.ViewEmployeeData))
            {
                throw new PermissionsException("User does not have permission to view Employees.");
            }
            else
            {
                var employeeEntities = new Tuple<IEnumerable<Domain.HumanResources.Entities.Employee>, int>(new List<Domain.HumanResources.Entities.Employee>(), 0);
                var newPerson = string.Empty;
                if (!string.IsNullOrEmpty(person))
                {
                    newPerson = await personRepository.GetPersonIdFromGuidAsync(person);
                    if (string.IsNullOrEmpty(newPerson))
                        throw new ArgumentException("Invalid Person '" + person + "' in the arguments");
                }
                var newCampus = string.Empty;
                if (!string.IsNullOrEmpty(campus))
                {
                    newCampus = ConvertGuidToCode(await GetLocationsAsync(bypassCache), campus);
                    if (string.IsNullOrEmpty(newCampus))
                        throw new ArgumentException("Invalid Campus '" + campus + "' in the arguments");
                }               
                if (!string.IsNullOrEmpty(status))
                {
                    if ((!string.Equals(status, Dtos.EnumProperties.EmployeeStatus.Active.ToString(), StringComparison.OrdinalIgnoreCase))
                        && (!string.Equals(status, Dtos.EnumProperties.EmployeeStatus.Terminated.ToString(), StringComparison.OrdinalIgnoreCase))
                        && (!string.Equals(status, Dtos.EnumProperties.EmployeeStatus.Leave.ToString(), StringComparison.OrdinalIgnoreCase)))
                    {
                        throw new ArgumentException("Invalid Status '" + status + "' in the arguments");
                    }                        
                }
                var newStartOn = (startOn == string.Empty ? string.Empty : await ConvertDateArgument(startOn));
                var newEndOn = (endOn == string.Empty ? string.Empty : await ConvertDateArgument(endOn));
                var newRehireStatus = rehireableStatusEligibility == string.Empty ? string.Empty : await ConvertRehireEligibilityToCode(rehireableStatusEligibility,bypassCache);
                var newRehireType = string.Empty;
                if (!string.IsNullOrEmpty(rehireableStatusType))
                {
                    newRehireType = ConvertGuidToCode(await GetRehireTypesAsync(bypassCache), rehireableStatusType);
                    if (string.IsNullOrEmpty(newRehireType))
                        throw new ArgumentException("Invalid Rehirable Status Type '" + rehireableStatusType + "' in the arguments");
                }
                employeeEntities = await employeeRepository.GetEmployeesAsync(offset, limit, newPerson, newCampus, status, newStartOn, newEndOn, newRehireStatus, newRehireType);
                if (employeeEntities != null)
                {
                    employeeCount = employeeEntities.Item2;

                    foreach (var employeeEntity in employeeEntities.Item1)
                    {
                        var employeeDto = await ConvertEmployeeEntityToDto(employeeEntity, bypassCache);
                        if (employeeDto != null)
                        {
                            employeeDtos.Add(employeeDto);
                        }
                    }

                    return new Tuple<IEnumerable<Dtos.Employee>, int>(employeeDtos, employeeCount);
                }
                else
                    return new Tuple<IEnumerable<Dtos.Employee>, int>(new List<Dtos.Employee>(), 0);
            }
        }

        /// <returns>rehireEligibilityCodes</returns>
        private async Task<string> ConvertRehireEligibilityToCode(string category, bool bypassCache = false)
        {
            var rehireTypeCode = string.Empty;
            if (!string.IsNullOrEmpty(category))
            {
                var rehireType = (await GetRehireTypesAsync(bypassCache)).Where(es => es.Category.ToString().Equals(category, StringComparison.OrdinalIgnoreCase));
                if (rehireType.Any())
                {
                    foreach (var stat in rehireType)
                    {
                        rehireTypeCode += "'" + stat.Code + "' ";
                    }
                }
                else
                    throw new ArgumentException("Invalid rehireableStatus.eligibility of '" + category + "' in the arguments.");

            }
            return rehireTypeCode;
        }
        /// <summary>
        /// Get Employee Data based on the permissions of the current user
        /// </summary>
        /// <param name="id">Guid for the employee.</param>
        /// <returns>Employee object <see cref="Dtos.Employee"./></returns>
        public async Task<Dtos.Employee> GetEmployeeByIdAsync(string id)
        {
            Dtos.Employee employeeDto = new Dtos.Employee();
            if (HasPermission(HumanResourcesPermissionCodes.ViewEmployeeData))
            {
                var ldmGuid = await referenceDataRepository.GetGuidLookupResultFromGuidAsync(id);
                 if (ldmGuid == null)
                    throw new KeyNotFoundException(string.Concat("Employees with ID '", id, "' was not found."));
                if (ldmGuid.Entity.ToUpperInvariant() != "EMPLOYES")
                     throw new KeyNotFoundException(string.Concat("Employees with ID '", id, "' was not found."));
                var employeeEntity = await employeeRepository.GetEmployeeByIdAsync(id);
                employeeDto = await ConvertEmployeeEntityToDto(employeeEntity, false);
                return employeeDto;
            }
            else
                throw new PermissionsException("User does not have permission to view Employees.");
            
        }

        private async Task<Dtos.Employee> ConvertEmployeeEntityToDto(Domain.HumanResources.Entities.Employee employeeEntity, bool bypassCache)
        {
            var employeeDto = new Dtos.Employee();
            employeeDto.Id = employeeEntity.Guid;
            employeeDto.Person = new GuidObject2(await personRepository.GetPersonGuidFromIdAsync(employeeEntity.PersonId));

            if (!string.IsNullOrEmpty(employeeEntity.Location))
            {
                var campusId = (await GetLocationsAsync(bypassCache)).FirstOrDefault(loc => loc.Code == employeeEntity.Location).Guid;
                // Campus or Location/Site
                if (!string.IsNullOrEmpty(campusId))
                {
                    employeeDto.Campus = new GuidObject2(campusId);
                }
            }
            // Contract Type
            employeeDto.ContractType = Dtos.EnumProperties.ContractType.PartTime;
            if (!string.IsNullOrEmpty(employeeEntity.StatusCode))
            {
                var category = (await GetPersonStatusesAsync(bypassCache)).FirstOrDefault(h => h.Code == employeeEntity.StatusCode).Category;
                if (category == ContractType.FullTime)
                {
                    employeeDto.ContractType = Dtos.EnumProperties.ContractType.FullTime;
                }
            }
            // Pay Status
            if (employeeEntity.PayStatus != null)
            {
                switch (employeeEntity.PayStatus)
                {
                    case PayStatus.PartialPay:
                        {
                            employeeDto.PayStatus = Dtos.EnumProperties.PayStatus.PartialPay;
                            break;
                        }
                    case PayStatus.WithoutPay:
                        {
                            employeeDto.PayStatus = Dtos.EnumProperties.PayStatus.WithoutPay;
                            break;
                        }
                    case PayStatus.WithPay:
                        {
                            employeeDto.PayStatus = Dtos.EnumProperties.PayStatus.WithPay;
                            break;
                        }
                    default:
                        break;
                }
            }
            // Benefits Status
            if (employeeEntity.BenefitsStatus != null)
            {
                switch (employeeEntity.BenefitsStatus)
                {
                    case BenefitsStatus.WithBenefits:
                        {
                            employeeDto.BenefitsStatus = Dtos.EnumProperties.BenefitsStatus.WithBenefits;
                            break;
                        }
                    case BenefitsStatus.WithoutBenefits:
                        {
                            employeeDto.BenefitsStatus = Dtos.EnumProperties.BenefitsStatus.WithoutBenefits;
                            break;
                        }
                    default:
                        break;
                }
            }
            // Hours Per Period
            if (employeeEntity.PayPeriodHours != null)
            {
                if (employeeEntity.PayPeriodHours.Any())
                {
                    var hoursPerPeriod = new List<Dtos.DtoProperties.HoursPerPeriodDtoProperty>();
                    foreach (var payPeriodHours in employeeEntity.PayPeriodHours)
                    {
                        if (payPeriodHours > 0)
                        {
                            var period = new Dtos.DtoProperties.HoursPerPeriodDtoProperty()
                            {
                                Hours = payPeriodHours,
                                Period = Dtos.EnumProperties.PayPeriods.PayPeriod
                            };
                            hoursPerPeriod.Add(period);
                        }
                    }
                    if (hoursPerPeriod.Any())
                        employeeDto.HoursPerPeriod = hoursPerPeriod;
                }
            }
            // Employee Status
            if (employeeEntity.EmploymentStatus != null)
            {
                switch (employeeEntity.EmploymentStatus)
                {
                    case EmployeeStatus.Active:
                        {
                            employeeDto.Status = Dtos.EnumProperties.EmployeeStatus.Active;
                            break;
                        }
                    case EmployeeStatus.Leave:
                        {
                            employeeDto.Status = Dtos.EnumProperties.EmployeeStatus.Leave;
                            break;
                        }
                    case EmployeeStatus.Terminated:
                        {
                            employeeDto.Status = Dtos.EnumProperties.EmployeeStatus.Terminated;
                            break;
                        }
                    default:
                        break;
                }
            }
            // Start and End Dates
            if (employeeEntity.StartDate.HasValue) employeeDto.StartOn = employeeEntity.StartDate.Value;
            if (employeeEntity.EndDate.HasValue) employeeDto.EndOn = employeeEntity.EndDate.Value;
            // Termination Reason
            if (!string.IsNullOrEmpty(employeeEntity.StatusEndReasonCode) && employeeEntity.EmploymentStatus == EmployeeStatus.Terminated)
            {
                var termReasonId = (await GetEmploymentStatusEndingReasonsAsync(bypassCache)).FirstOrDefault(sr => sr.Code == employeeEntity.StatusEndReasonCode).Guid;
                if (!string.IsNullOrEmpty(termReasonId))
                {
                    employeeDto.TerminationReason = new GuidObject2(termReasonId);
                }
            }
            if (!string.IsNullOrEmpty(employeeEntity.RehireEligibilityCode))
            {
                var rehireReason = (await GetRehireTypesAsync(bypassCache)).FirstOrDefault(rr => rr.Code == employeeEntity.RehireEligibilityCode);
                if (rehireReason != null)
                {
                    var eligibilityCategory = Dtos.EnumProperties.RehireEligibility.Ineligible;
                    if (rehireReason.Category == Colleague.Domain.HumanResources.Entities.RehireTypeCategory.Eligible)
                    {
                        eligibilityCategory = Dtos.EnumProperties.RehireEligibility.Eligible;
                    }
                    var rehireStatus = new Dtos.DtoProperties.RehireableStatusDtoProperty()
                    {
                        Eligibility = eligibilityCategory,
                        Type = new GuidObject2(rehireReason.Guid)
                    };
                    employeeDto.RehireableStatus = rehireStatus;
                }
            }
            return employeeDto;
        }
    }
}
