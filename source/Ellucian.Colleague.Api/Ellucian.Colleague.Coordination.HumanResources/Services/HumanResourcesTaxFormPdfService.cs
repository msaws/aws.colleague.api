﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Coordination.HumanResources.Utilities;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.HumanResources.Entities;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;

namespace Ellucian.Colleague.Coordination.HumanResources.Services
{
	/// <summary>
	/// Service for tax form pdfs.
	/// </summary>
	[RegisterType]
	public class HumanResourcesTaxFormPdfService : BaseCoordinationService, IHumanResourcesTaxFormPdfService
	{
		private IHumanResourcesTaxFormPdfDataRepository taxFormPdfDataRepository;
		private IPdfSharpRepository pdfSharpRepository;

		/// <summary>
		/// Constructor TaxFormPdfService
		/// </summary>
		/// <param name="adapterRegistry">AdapterRegistry</param>
		/// <param name="currentUserFactory">CurrentUserFactory</param>
		/// <param name="roleRepository">RoleRepository</param>
		/// <param name="logger">Logger</param>
		public HumanResourcesTaxFormPdfService(IHumanResourcesTaxFormPdfDataRepository taxFormPdfDataRepository,
			IPdfSharpRepository pdfSharpRepository, IAdapterRegistry adapterRegistry,
			ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger)
			: base(adapterRegistry, currentUserFactory, roleRepository, logger)
		{
			this.taxFormPdfDataRepository = taxFormPdfDataRepository;
			this.pdfSharpRepository = pdfSharpRepository;
		}

		/// <summary>
		/// Returns the pdf data to print a W-2 tax form.
		/// </summary>
		/// <param name="personId">ID of the person assigned to and requesting the W-2.</param>
		/// <param name="recordId">The record ID where the W-2 pdf data is stored</param>
		/// <returns>Byte array containing PDF data for the W-2 tax form</returns>
		public async Task<FormW2PdfData> GetW2TaxFormData(string personId, string recordId)
		{
			if (string.IsNullOrEmpty(personId))
				throw new ArgumentNullException("personId", "Person ID must be specified.");

			if (string.IsNullOrEmpty(recordId))
				throw new ArgumentNullException("recordId", "Record ID must be specified.");

			if (personId != CurrentUser.PersonId)
				throw new ApplicationException("Person ID from request is not the same as the Person ID of the current user.");

			try
			{
				// Call the repository to get all the data to print in the pdf.
				var taxFormPdfData = await this.taxFormPdfDataRepository.GetW2PdfAsync(personId, recordId);
				return taxFormPdfData;
			}
			catch (Exception e)
			{
				// Log the error and throw the exception that was given
				logger.Error(e.Message);
				throw;
			}
		}

		/// <summary>
		/// Populates the W-2 PDF with the supplied data.
		/// </summary>
		/// <param name="pdfData">W-2 PDF data</param>
		/// <returns>Byte array containing PDF data for the W-2 tax form</returns>
		public byte[] PopulateW2Pdf(FormW2PdfData pdfData, string documentPath)
		{
			byte[] reportBytes;
			var memoryStream = new MemoryStream();
			try
			{
				if (pdfData != null && !string.IsNullOrEmpty(documentPath))
				{
					var document = pdfSharpRepository.OpenDocument(documentPath);

					var taxFormData = new Dictionary<string, string>()
					{
						{ "Tax_Year", pdfData.TaxYear },
						{ "Box_c_name", pdfData.EmployerName },
						{ "Box_c_addr_1", pdfData.EmployerAddressLine1 },
						{ "Box_c_addr_2", pdfData.EmployerAddressLine2 },
						{ "Box_c_addr_3", pdfData.EmployerAddressLine3 },
						{ "Box_c_addr_4", pdfData.EmployerAddressLine4 },
						{ "Box_e",        pdfData.EmployeeName() },
						{ "Box_f_addr_1", pdfData.EmployeeAddressLine1 },
						{ "Box_f_addr_2", pdfData.EmployeeAddressLine2 },
						{ "Box_f_addr_3", pdfData.EmployeeAddressLine3 },
						{ "Box_f_addr_4", pdfData.EmployeeAddressLine4 },
						{ "Box_1", pdfData.FederalWages },
						{ "Box_2", pdfData.FederalWithholding },
						{ "Box_3", pdfData.SocialSecurityWages },
						{ "Box_4", pdfData.SocialSecurityWithholding },
						{ "Box_5", pdfData.MedicareWages },
						{ "Box_6", pdfData.MedicareWithholding },
						{ "Box_7", pdfData.SocialSecurityTips },
						{ "Box_8", pdfData.AllocatedTips },
						{ "Box_9", pdfData.AdvancedEic},
						{ "Box_10", pdfData.DependentCare },
						{ "Box_11", pdfData.NonqualifiedTotal },
						{ "Box_12a_Code", pdfData.Box12aCode },
						{ "Box_12a_Amt", pdfData.Box12aAmount },
						{ "Box_12b_Code", pdfData.Box12bCode },
						{ "Box_12b_Amt", pdfData.Box12bAmount },
						{ "Box_12c_Code", pdfData.Box12cCode },
						{ "Box_12c_Amt", pdfData.Box12cAmount },
						{ "Box_12d_Code", pdfData.Box12dCode },
						{ "Box_12d_Amt", pdfData.Box12dAmount },
						{ "Box_13_1", pdfData.Box13CheckBox1 },
						{ "Box_13_2", pdfData.Box13CheckBox2 },
						{ "Box_13_3", pdfData.Box13CheckBox3 },
						{ "Box_a", pdfData.EmployeeSsn },
						{ "Box_b", pdfData.EmployerEin },
						{ "Box_14_line_1", pdfData.Box14Line1 },
						{ "Box_14_line_2", pdfData.Box14Line2 },
						{ "Box_14_line_3", pdfData.Box14Line3 },
						{ "Box_14_line_4", pdfData.Box14Line4 },
						{ "Box_15_1_1", pdfData.Box15Line1Section1 },
						{ "Box_15_1_2", pdfData.Box15Line1Section2 },
						{ "Box_15_2_1", pdfData.Box15Line2Section1 },
						{ "Box_15_2_2", pdfData.Box15Line2Section2 },
						{ "Box_16_1", pdfData.Box16Line1 },
						{ "Box_16_2", pdfData.Box16Line2 },
						{ "Box_17_1", pdfData.Box17Line1 },
						{ "Box_17_2", pdfData.Box17Line2 },
						{ "Box_18_1", pdfData.Box18Line1 },
						{ "Box_18_2", pdfData.Box18Line2 },
						{ "Box_19_1", pdfData.Box19Line1 },
						{ "Box_19_2", pdfData.Box19Line2 },
						{ "Box_20_1", pdfData.Box20Line1 },
						{ "Box_20_2", pdfData.Box20Line2 },
					};

					pdfSharpRepository.PopulatePdfDocument(ref document, taxFormData);
					memoryStream = pdfSharpRepository.FinalizePdfDocument(document);
				}

				reportBytes = memoryStream.ToArray();
				return reportBytes;
			}
			catch (Exception e)
			{
				// Log the error and throw the exception that was given
				logger.Error(e.Message);
				throw;
			}
		}

		/// <summary>
		/// Returns the pdf data to print a 1095-C2 tax form.
		/// </summary>
		/// <param name="personId">ID of the person assigned to and requesting the 1095-C.</param>
		/// <param name="recordId">The record ID where the1095-C pdf data is stored</param>
		/// <returns>Byte array containing PDF data for the 1095-C tax form</returns>
		public async Task<Form1095cPdfData> Get1095cTaxFormData(string personId, string recordId)
		{
			if (string.IsNullOrEmpty(personId))
				throw new ArgumentNullException("personId", "Person ID must be specified.");

			if (string.IsNullOrEmpty(recordId))
				throw new ArgumentNullException("recordId", "Record ID must be specified.");

			if (personId != CurrentUser.PersonId)
				throw new ApplicationException("Person ID from request is not the same as the Person ID of the current user.");

			try
			{
				// Call the repository to get all the data to print in the pdf.
				var taxFormPdfData = await this.taxFormPdfDataRepository.Get1095cPdfAsync(personId, recordId);
				return taxFormPdfData;
			}
			catch (Exception e)
			{
				// Log the error and throw the exception that was given
				logger.Error(e.Message);
				throw;
			}
		}

		/// <summary>
		/// Populates the 1095-C PDF with the supplied data.
		/// </summary>
		/// <param name="pdfData">1095-C PDF data</param>
		/// <returns>Byte array containing PDF data for the 1095-C tax form</returns>
		public byte[] Populate1095cPdf(Form1095cPdfData pdfData, string documentPath)
		{
			byte[] reportBytes;
			var memoryStream = new MemoryStream();

			try
			{
				if (pdfData != null && !string.IsNullOrEmpty(documentPath))
				{
					var document = pdfSharpRepository.OpenDocument(documentPath);

					var empBox6 = string.Empty;
					if (!string.IsNullOrEmpty(pdfData.EmployeePostalCode))
					{
						empBox6 = pdfData.EmployeePostalCode;
					}
					if (!string.IsNullOrEmpty(pdfData.EmployeeZipExtension))
					{
						empBox6 = empBox6 + "-" + pdfData.EmployeeZipExtension;
					}
					if (!string.IsNullOrEmpty(pdfData.EmployeeCountry))
					{
						if (!string.IsNullOrEmpty(empBox6))
						{
							empBox6 = empBox6 + " " + pdfData.EmployeeCountry;
						}
						else
						{
							empBox6 = pdfData.EmployeeCountry;
						}
					}

					var isCorrected = pdfData.IsCorrected ? "X" : "";
					string isVoid = string.Empty;
					if (pdfData.IsVoided)
					{
						isCorrected = string.Empty;
						isVoid = "X";
					}

					var taxFormData = new Dictionary<string, string>()
					{
						{ "Void", isVoid },
						{ "Corrected", isCorrected },
						{ "Year2", pdfData.TaxYear },
						{ "Box1", pdfData.EmployeeName() },
						{ "Box2", pdfData.EmployeeSsn },
						{ "Box3", pdfData.EmployeeAddressLine1 + " " + pdfData.EmployeeAddressLine2 },
						{ "Box4", pdfData.EmployeeCityName },
						{ "Box5", pdfData.EmployeeStateCode },
						{ "Box6", empBox6 },
						{ "Box7", pdfData.EmployerName },
						{ "Box8", pdfData.EmployerEin },
						{ "Box9", pdfData.EmployerAddressLine },
						{ "Box10", pdfData.EmployerContactPhoneNumber },
						{ "Box11", pdfData.EmployerCityName },
						{ "Box12", pdfData.EmployerStateCode },
						{ "Box13", pdfData.EmployerZipCode },
						{ "Box14All", pdfData.OfferOfCoverage12Month },
						{ "Box14Jan", pdfData.OfferOfCoverageJanuary },
						{ "Box14Feb", pdfData.OfferOfCoverageFebruary },
						{ "Box14Mar", pdfData.OfferOfCoverageMarch },
						{ "Box14Apr", pdfData.OfferOfCoverageApril },
						{ "Box14May", pdfData.OfferOfCoverageMay },
						{ "Box14June", pdfData.OfferOfCoverageJune },
						{ "Box14July", pdfData.OfferOfCoverageJuly },
						{ "Box14Aug", pdfData.OfferOfCoverageAugust },
						{ "Box14Sept", pdfData.OfferOfCoverageSeptember },
						{ "Box14Oct", pdfData.OfferOfCoverageOctober },
						{ "Box14Nov", pdfData.OfferOfCoverageNovember },
						{ "Box14Dec", pdfData.OfferOfCoverageDecember },
						{ "Box15All", ConvertDecimalToUsString(pdfData.LowestCostAmount12Month) },
						{ "Box15Jan", ConvertDecimalToUsString(pdfData.LowestCostAmountJanuary) },
						{ "Box15Feb", ConvertDecimalToUsString(pdfData.LowestCostAmountFebruary) },
						{ "Box15Mar", ConvertDecimalToUsString(pdfData.LowestCostAmountMarch) },
						{ "Box15Apr", ConvertDecimalToUsString(pdfData.LowestCostAmountApril) },
						{ "Box15May", ConvertDecimalToUsString(pdfData.LowestCostAmountMay) },
						{ "Box15June", ConvertDecimalToUsString(pdfData.LowestCostAmountJune) },
						{ "Box15July", ConvertDecimalToUsString(pdfData.LowestCostAmountJuly) },
						{ "Box15Aug", ConvertDecimalToUsString(pdfData.LowestCostAmountAugust) },
						{ "Box15Sept", ConvertDecimalToUsString(pdfData.LowestCostAmountSeptember) },
						{ "Box15Oct", ConvertDecimalToUsString(pdfData.LowestCostAmountOctober) },
						{ "Box15Nov", ConvertDecimalToUsString(pdfData.LowestCostAmountNovember) },
						{ "Box15Dec", ConvertDecimalToUsString(pdfData.LowestCostAmountDecember) },
						{ "Box16All", pdfData.SafeHarborCode12Month },
						{ "Box16Jan", pdfData.SafeHarborCodeJanuary },
						{ "Box16Feb", pdfData.SafeHarborCodeFebruary },
						{ "Box16Mar", pdfData.SafeHarborCodeMarch },
						{ "Box16Apr", pdfData.SafeHarborCodeApril },
						{ "Box16May", pdfData.SafeHarborCodeMay },
						{ "Box16June", pdfData.SafeHarborCodeJune },
						{ "Box16July", pdfData.SafeHarborCodeJuly },
						{ "Box16Aug", pdfData.SafeHarborCodeAugust },
						{ "Box16Sept", pdfData.SafeHarborCodeSeptember },
						{ "Box16Oct", pdfData.SafeHarborCodeOctober },
						{ "Box16Nov", pdfData.SafeHarborCodeNovember },
						{ "Box16Dec", pdfData.SafeHarborCodeDecember },
						{ "EmpProvCoverage", pdfData.EmployeeIsSelfInsured ? "X" : "" },
						{ "PlanStartMon", pdfData.PlanStartMonthCode },
						{ "NameEmp", pdfData.EmployeeName() },
						{ "EmpSSN", pdfData.EmployeeSsn }
					};

					int counter = 17;
					foreach (var dependant in pdfData.CoveredIndividuals)
					{
						TaxFormPdfUtility.Populate1095CDependentRow(ref taxFormData, dependant, counter);
						counter += 1;
					}

					if (counter < 34)
					{
						Form1095cCoveredIndividualsPdfData emptyDependant = new Form1095cCoveredIndividualsPdfData()
						{
							Covered12Month = false,
							CoveredApril = false,
							CoveredAugust = false,
							CoveredDecember = false,
							CoveredFebruary = false,
							CoveredIndividualDateOfBirth = null,
							CoveredIndividualFirstName = string.Empty,
							CoveredIndividualLastName = string.Empty,
							CoveredIndividualMiddleName = string.Empty,
							CoveredIndividualSsn = string.Empty,
							CoveredJanuary = false,
							CoveredJuly = false,
							CoveredJune = false,
							CoveredMarch = false,
							CoveredMay = false,
							CoveredNovember = false,
							CoveredOctober = false,
							CoveredSeptember = false,
							IsEmployeeItself = false
						};

						for (int i = counter; i < 35; i++)
						{
							TaxFormPdfUtility.Populate1095CDependentRow(ref taxFormData, emptyDependant, i);
						}
					}

					pdfSharpRepository.PopulatePdfDocument(ref document, taxFormData);
					memoryStream = pdfSharpRepository.FinalizePdfDocument(document);
				}

				reportBytes = memoryStream.ToArray();
				return reportBytes;
			}
			catch (Exception e)
			{
				// Log the error and throw the exception that was given
				logger.Error(e.Message);
				throw;
			}
		}

		private string ConvertDecimalToUsString(decimal? amount)
		{
		   if (amount > 0)
		   {
			   Decimal amt = amount.Value;
			   return amt.ToString("C", CultureInfo.GetCultureInfo("en-US"));
		   }
		   else
			   return "";
		}
	}
}