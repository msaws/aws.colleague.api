﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Dtos.HumanResources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Dtos;

namespace Ellucian.Colleague.Coordination.HumanResources.Services
{
    public interface IEmployeeService : IBaseService
    {
        /// <summary>
        /// Get Employee Data based on the permissions of the current user
        /// </summary>
        /// <param name="bypassCache">Flag to bypass cache and read directly from disk.</param>
        /// <param name="offset">Offset for record index on page reads.</param>
        /// <param name="limit">Take number of records on page reads.</param>
        /// <param name="person">Person id filter.</param>
        /// <param name="campus">Primary campus or location filter.</param>
        /// <param name="status">Status ("active", "terminated", or "leave") filter.</param>
        /// <param name="startOn">Start on a specific date filter.</param>
        /// <param name="endOn">End on a specific date filter.</param>
        /// <param name="rehireableStatusEligibility">Rehireable status ("eligible" or "ineligible") filter.</param>
        /// <param name="rehireableStatusType">Rehireable status types.</param>
        /// <returns>Tuple of employee objects <see cref="Dtos.Employee"/> and count for paging.</returns>
        Task<Tuple<IEnumerable<Dtos.Employee>, int>> GetEmployeesAsync(int offset, int limit, bool bypassCache, string person = "",
            string campus = "", string status = "", string startOn = "", string endOn = "", string rehireableStatusEligibility = "", string rehireableStatusType = "");
        
        /// <summary>
        /// Get Employee Data based on the permissions of the current user
        /// </summary>
        /// <param name="id">Guid for the employee.</param>
        /// <returns>Employee object <see cref="Dtos.Employee"./></returns>
        Task<Employee> GetEmployeeByIdAsync(string id);
    }
}
