﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System.Threading.Tasks;
using Ellucian.Colleague.Domain.HumanResources.Entities;

namespace Ellucian.Colleague.Coordination.HumanResources.Services
{
    /// <summary>
    /// Define the methods signatures for a TaxFormPdfService.
    /// </summary>
    public interface IHumanResourcesTaxFormPdfService
    {
        /// <summary>
        /// Returns the pdf data to print a W-2 tax form.
        /// </summary>
        /// <param name="personId">ID of the person assigned to and requesting the W-2.</param>
        /// <param name="recordId">The record ID where the W-2 pdf data is stored</param>
        /// <returns>TaxFormW2PdfData domain entity</returns>
        Task<FormW2PdfData> GetW2TaxFormData(string personId, string recordId);
        
        /// <summary>
        /// Populates the W-2 PDF with the supplied data.
        /// </summary>
        /// <param name="pdfData">W-2 PDF data</param>
        /// <param name="documentPath">Path to the PDF template</param>
        /// <returns>Byte array containing PDF data for the W-2 tax form</returns>
        byte[] PopulateW2Pdf(FormW2PdfData pdfData, string documentPath);

        /// <summary>
        /// Returns the pdf data to print a 1095-C tax form.
        /// </summary>
        /// <param name="personId">ID of the person assigned to and requesting the 1095-C.</param>
        /// <param name="ids">The list of record IDs where the W-2 pdf data is stored</param>
        /// <returns>Form1095cPdfData domain entity</returns>
        Task<Form1095cPdfData> Get1095cTaxFormData(string personId, string recordId);

        /// <summary>
        /// Populates the 1095-C PDF with the supplied data.
        /// </summary>
        /// <param name="pdfData">1095-C PDF data</param>
        /// <param name="documentPath">Path to the PDF template</param>
        /// <returns>Byte array containing PDF data for the 1095-C tax form</returns>
        byte[] Populate1095cPdf(Form1095cPdfData pdfData, string documentPath);
    }
}
