﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.HumanResources;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.HumanResources.Services
{
    [RegisterType]
    public class PersonPositionService : BaseCoordinationService, IPersonPositionService
    {
        private readonly IPersonPositionRepository personPositionRepository;
        private readonly ISupervisorsRepository supervisorsRepository;
        private readonly IPositionRepository positionRepository;

        public PersonPositionService(
            IPersonPositionRepository personPositionRepository,
            ISupervisorsRepository supervisorsRepository,
            IPositionRepository positionRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.personPositionRepository = personPositionRepository;
            this.supervisorsRepository = supervisorsRepository;
            this.positionRepository = positionRepository;
        }


        /// <summary>
        /// Get the PersonPositions based on the permissions of the current user
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<PersonPosition>> GetPersonPositionsAsync()
        {
            var userAndSubordinateIds = new List<string>() { CurrentUser.PersonId };

            if (HasPermission(HumanResourcesPermissionCodes.ViewSuperviseeData))
            {
                var subordinateIds = (await supervisorsRepository.GetSuperviseesBySupervisorAsync(CurrentUser.PersonId)).ToList();

                if (subordinateIds == null)
                {
                    var message = "Unexpected null person id list returned from supervisors repository";
                    logger.Error(message);
                    throw new ApplicationException(message);
                }
                if (subordinateIds.Any())
                {
                    userAndSubordinateIds = userAndSubordinateIds.Concat(subordinateIds).ToList();
                }
            }       

            var personPositionEntities = await personPositionRepository.GetPersonPositionsAsync(userAndSubordinateIds);
            if (personPositionEntities == null)
            {
                var message = "Unexpected null personPositionEntities returned from repository";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            // check if the employee has a PERPOS level supervisor. if not, obtain a list of supervisor IDs from the supervisor position assigned to the employee's position.
            foreach (var personPositionEntity in personPositionEntities)
            {
                if (string.IsNullOrWhiteSpace(personPositionEntity.SupervisorId))
                {
                    // get the supervisor position ID (if any) from this employee's position
                    var allPositionIds = await positionRepository.GetPositionsAsync();
                    if (!string.IsNullOrWhiteSpace(personPositionEntity.PositionId))
                    {
                        var positionEntity = allPositionIds.FirstOrDefault(p => p.Id == personPositionEntity.PositionId);
                        var supervisorPositionId = positionEntity.SupervisorPositionId;
                        if (!string.IsNullOrWhiteSpace(supervisorPositionId))
                        {
                            // we have a supervisor position defined on the position, so get the supervisors assigned to this position
                            var supervisorIds = await supervisorsRepository.GetSupervisorIdsForPositionsAsync(new List<string> { supervisorPositionId });

                            if (supervisorIds.Any())
                            {
                                // set the supervisor ids in the PERPOS entity
                                personPositionEntity.PositionLevelSupervisorIds = supervisorIds.ToList();
                            }
                        }
                    }
                }
            }

            var personPositionEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.HumanResources.Entities.PersonPosition, PersonPosition>();

            return personPositionEntities.Select(pp => personPositionEntityToDtoAdapter.MapToType(pp));
        }
    }
}
