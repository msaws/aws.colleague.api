﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos;
using Ellucian.Colleague.Dtos.EnumProperties;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.HumanResources;
using Ellucian.Colleague.Dtos.DtoProperties;
using Ellucian.Colleague.Domain.Exceptions;

namespace Ellucian.Colleague.Coordination.HumanResources.Services
{
    [RegisterType]
    public class InstitutionJobsService : BaseCoordinationService, IInstitutionJobsService
    {
       
        private readonly IPositionRepository _positionRepository;
        private readonly IHumanResourcesReferenceDataRepository _hrReferenceDataRepository;
        private readonly IReferenceDataRepository _referenceDataRepository;    
        private readonly IPersonRepository _personRepository;
        private readonly IInstitutionJobsRepository _institutionJobsRepository;
        private readonly IConfigurationRepository _configurationRepository;
        private IEnumerable<Domain.HumanResources.Entities.EmploymentClassification> _employmentClassification;
        private IEnumerable<Domain.HumanResources.Entities.JobChangeReason> _jobChangeReasons;
        private Dictionary<string, string> _employerGuidDictionary;

        public InstitutionJobsService(

            IPositionRepository positionRepository,
            IHumanResourcesReferenceDataRepository hrReferenceDataRepository,
             IReferenceDataRepository referenceDataRepository,
            IPersonRepository personRepository,
            IInstitutionJobsRepository institutionJobsRepository,
            IConfigurationRepository configurationRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger, configurationRepository: configurationRepository)
        {

            this._positionRepository = positionRepository;
            this._hrReferenceDataRepository = hrReferenceDataRepository;
            this._referenceDataRepository = referenceDataRepository;
            this._personRepository = personRepository;
            this._institutionJobsRepository = institutionJobsRepository;
            _configurationRepository = configurationRepository;

            _employerGuidDictionary = new Dictionary<string, string>();
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Gets all institution-jobs
        /// </summary>
        /// <returns>Collection of InstitutionJobs DTO objects</returns>
        public async Task<Tuple<IEnumerable<Dtos.InstitutionJobs>, int>> GetInstitutionJobsAsync(int offset, int limit, 
            string person = "", string employer = "", string position = "", string department = "", string startOn = "", 
            string endOn = "", string status = "", string classification = "", string preference = "", bool bypassCache = false)
       {
            try
            {
                //check permissions
                CheckGetInstitutionJobsPermission();

                string personCode = string.Empty;
                if (!string.IsNullOrEmpty(person))
                {

                    personCode = await _personRepository.GetPersonIdFromGuidAsync(person);
                    if (string.IsNullOrEmpty(personCode))
                        throw new ArgumentException("Invalid Person '" + person + "' in the arguments");
                }

                string employerCode = string.Empty;
                if (!string.IsNullOrEmpty(employer))
                {

                    employerCode = await _personRepository.GetPersonIdFromGuidAsync(employer);                  
                    if (string.IsNullOrEmpty(employerCode))
                        throw new ArgumentException("Invalid Employer '" + employer + "' in the arguments");
                }

                var positionCode = string.Empty;
                if (!string.IsNullOrEmpty(position))
                {
                    positionCode = await _positionRepository.GetPositionIdFromGuidAsync(position);
                    if (string.IsNullOrEmpty(positionCode))
                        throw new ArgumentException("Invalid Position '" + position + "' in the arguments");
                }
            
                var convertedStartOn = (startOn == string.Empty ? string.Empty : await ConvertDateArgument(startOn));
                var convertedEndOn = (endOn == string.Empty ? string.Empty : await ConvertDateArgument(endOn));

                //validate status
                if (!string.IsNullOrEmpty(status))
                {
                    status = status.ToLower();

                    if (status == "leave")
                    {
                        throw new ArgumentException("Status 'leave' not supported in Colleague");
                    } 
                    if (status != "active" && status != "ended")
                    {
                        throw new ArgumentException(string.Format("Invalid value '{0}' for status filter sent in.", status));
                    }
                    
                }

                var classificationCode = string.Empty;
                if (!string.IsNullOrEmpty(classification))
                {
                    var classifications = await this.GetAllEmploymentClassificationAsync(bypassCache);
                    if (classifications != null)
                    {
                        var employmentClassification  = classifications.FirstOrDefault(x => x.Guid == classification);
                        if (employmentClassification != null)
                            classificationCode = employmentClassification.Code;
                    }
                    if (string.IsNullOrEmpty(classificationCode))
                        throw new ArgumentException("Invalid Classification '" + classification + "' in the arguments");
                }

                //validate preference
                if (!string.IsNullOrEmpty(preference))
                {
                    if (preference.ToLower() != "primary")
                    {
                        throw new ArgumentException(string.Format("Invalid value '{0}' for preference filter sent in.", preference));
                    }
                }

                var institutionJobsEntitiesTuple = await _institutionJobsRepository.GetInstitutionJobsAsync(offset, limit,  personCode, 
                    employerCode, positionCode, department, convertedStartOn, convertedEndOn, status, classificationCode, 
                    preference, bypassCache);
                if (institutionJobsEntitiesTuple != null)
                {
                    var institutionJobsEntities = institutionJobsEntitiesTuple.Item1.ToList();
                    var totalCount = institutionJobsEntitiesTuple.Item2;

                    if (institutionJobsEntities.Any())
                    {
                        var institutionPositions = new List<Colleague.Dtos.InstitutionJobs>();

                        foreach (var institutionJobsEntity in institutionJobsEntities)
                        {
                            institutionPositions.Add(await this.ConvertInstitutionJobsEntityToDtoAsync(institutionJobsEntity, bypassCache));
                        }
                        return new Tuple<IEnumerable<Dtos.InstitutionJobs>, int>(institutionPositions, totalCount);
                    }
                    // no results
                    return new Tuple<IEnumerable<Dtos.InstitutionJobs>, int>(new List<Dtos.InstitutionJobs>(), totalCount);
                }
                //no results
                return new Tuple<IEnumerable<Dtos.InstitutionJobs>, int>(new List<Dtos.InstitutionJobs>(), 0);
            }
            catch (Exception e)
            {
                throw new ArgumentException(e.Message);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Get a InstitutionJobs from its GUID
        /// </summary>
        /// <returns>InstitutionJobs DTO object</returns>
        public async Task<Dtos.InstitutionJobs> GetInstitutionJobsByGuidAsync(string guid)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw new ArgumentNullException("guid", "GUID is required to get an Institution Job.");
            }
            CheckGetInstitutionJobsPermission();
            try
            {
                var institutionJobsEntity = (await _institutionJobsRepository.GetInstitutionJobsByGuidAsync(guid));
                if (institutionJobsEntity == null)
                {
                    throw new KeyNotFoundException("Institution Job not found for GUID " + guid);
                }
                return await ConvertInstitutionJobsEntityToDtoAsync(institutionJobsEntity);
            }
            catch (KeyNotFoundException)
            {
                throw new KeyNotFoundException("Institution Job not found for GUID " + guid);
            }     
        }

        /// <remarks>FOR USE WITH ELLUCIAN DATA MODEL</remarks>
        /// <summary>
        /// Put (Update) an InstitutionJobs domain entity
        /// </summary>
        /// <param name="guid">guid</param>
        /// <param name="institutionJobsDto"><see cref="Dtos.InstitutionJobs">InstitutionJobs</see></param>
        /// <returns><see cref="Dtos.InstitutionJobs">InstitutionJobs</see></returns>
        /// <exception><see cref="ArgumentNullException">ArgumentNullException</see></exception>
        public async Task<InstitutionJobs> PutInstitutionJobsAsync(string guid, Dtos.InstitutionJobs institutionJobsDto)
        {
            if (institutionJobsDto == null)
                throw new ArgumentNullException("institutionJobs", "Must provide a institutionJobs for update");
            if (string.IsNullOrEmpty(institutionJobsDto.Id))
                throw new ArgumentNullException("institutionJobs", "Must provide a guid for institutionJobs update");

            // get the person ID associated with the incoming guid
            var institutionJobsId = await _institutionJobsRepository.GetInstitutionJobsIdFromGuidAsync(institutionJobsDto.Id);

            // verify the GUID exists to perform an update.  If not, perform a create instead
            if (!string.IsNullOrEmpty(institutionJobsId))
            {
                try
                {
                    // verify the user has the permission to update a institutionJobs
                    CheckCreateInstitutionJobsPermission();
      
                    // map the DTO to entities
                    var institutionJobsEntity
                        = await ConvertInstitutionJobsDtoToEntityAsync(institutionJobsId, institutionJobsDto, true);

                    // update the entity in the database
                    var updatedInstitutionJobsEntity =
                        await _institutionJobsRepository.UpdateInstitutionJobsAsync(institutionJobsEntity);

                     return await ConvertInstitutionJobsEntityToDtoAsync(updatedInstitutionJobsEntity, true);
                   
                }
                catch (RepositoryException ex)
                {
                    throw ex;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
            // perform a create instead
            return await PostInstitutionJobsAsync(institutionJobsDto);
        }


        public async Task<InstitutionJobs> PostInstitutionJobsAsync(InstitutionJobs institutionJobsDto)
        {
            if (institutionJobsDto == null)
            {
                throw new ArgumentNullException("institutionJobsDto", "Must provide an institutionJobs for create");
            }

            if (string.IsNullOrEmpty(institutionJobsDto.Id))
            {
                throw new ArgumentNullException("institutionJobsDto", "Must provide a guid for institutionJobs create");
            }


            try
            {
                var institutionJobsEntity = await ConvertInstitutionJobsDtoToEntityAsync(institutionJobsDto.Id, institutionJobsDto);
                var newEntity = await _institutionJobsRepository.CreateInstitutionJobsAsync(institutionJobsEntity);
                return await ConvertInstitutionJobsEntityToDtoAsync(newEntity);

            }
            catch (RepositoryException ex)
            {
                throw ex;
            }
            catch (InvalidOperationException ex)
            {
                throw new InvalidOperationException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }


        /// <summary>
        /// Convert an InstitutionJobs Dto to an InstitutionJobs Entity
        /// </summary>
        /// <param name="institutionJobsId">guid</param>
        /// <param name="institutionJobsDto"><see cref="Dtos.InstitutionJobs">InstitutionJobs</see></param>
        /// <returns><see cref="Domain.HumanResources.Entities.InstitutionJobs source">InstitutionJobs</see></returns>
        private async Task<Domain.HumanResources.Entities.InstitutionJobs> ConvertInstitutionJobsDtoToEntityAsync(string institutionJobsId, InstitutionJobs institutionJobs, bool bypassCache = false)
        {
            if (institutionJobs == null || string.IsNullOrEmpty(institutionJobs.Id))
                throw new ArgumentNullException("InstitutionJobs", "Must provide guid for an Institution Jobs");
            if (string.IsNullOrEmpty(institutionJobsId))
                throw new ArgumentNullException("InstitutionJobs", string.Concat("Must provide an id for Institution Job.  Guid: ", institutionJobs.Id));


            Domain.HumanResources.Entities.InstitutionJobs response = null;

            try
            {

                if ((institutionJobs.Person == null) || (string.IsNullOrEmpty(institutionJobs.Person.Id)))
                {
                    throw new ArgumentNullException("Person ID is required for Institution Jobs.  Id:" + institutionJobsId);
                }

                var personId = await _personRepository.GetPersonIdFromGuidAsync(institutionJobs.Person.Id);

                if (string.IsNullOrEmpty(personId))
                {
                    throw new ArgumentNullException("Person not found for Id:" + institutionJobs.Person.Id);
                }

                if ((institutionJobs.Position == null) || (string.IsNullOrEmpty(institutionJobs.Position.Id)))
                {
                    throw new ArgumentNullException("Position ID is required for Institution Jobs. Id:" + institutionJobsId);
                }
                //var positionId = await _positionRepository.GetPositionIdFromGuidAsync(institutionJobs.Position.Id);
                var position = await _positionRepository.GetPositionByGuidAsync(institutionJobs.Position.Id);
                if ((position == null) || (string.IsNullOrEmpty(position.Id)) )
                {
                    throw new ArgumentNullException("Position not found for Id:" + position.Id);
                }
                

                response = new Domain.HumanResources.Entities.InstitutionJobs(institutionJobs.Id, institutionJobsId, personId, position.Id, institutionJobs.StartOn);

                if ((institutionJobs.Employer == null) || (string.IsNullOrEmpty(institutionJobs.Employer.Id)))
                {
                    throw new ArgumentNullException("Employer ID is required for Institution Jobs. Id:" + institutionJobsId);
                }
                var employer = await _personRepository.GetPersonIdFromGuidAsync(institutionJobs.Employer.Id);
                if (string.IsNullOrEmpty(employer))
                {
                    throw new ArgumentNullException("Employer not found for Id:" + institutionJobs.Employer.Id);
                }

                response.Employer = employer;

                if (string.IsNullOrEmpty(institutionJobs.Department))
                {
                    throw new ArgumentNullException("Department is a required field. Position Id:" + position.Id);

                }

                if ((position != null) && (!(string.IsNullOrEmpty(institutionJobs.Department))))
                {
                    if (position.PositionDept != institutionJobs.Department)
                    {
                        throw new ArgumentNullException("Position can not be changed on an existing job. Position Id:" + position.Id);
                    }
                    response.Department = institutionJobs.Department;
                }


                if ((institutionJobs.EndOn != null) && (institutionJobs.EndOn.HasValue))
                {
                    response.EndDate = institutionJobs.EndOn;
                }
                else if (institutionJobs.Status == InstitutionJobsStatus.Ended)
                {
                    response.EndDate = DateTime.Now;
                }
                

                if ((institutionJobs.JobChangeReason != null) && (!string.IsNullOrEmpty(institutionJobs.JobChangeReason.Id)))
                {
                    var jobChangeReasons = await GetAllJobChangeReasonsAsync(bypassCache);
                    if (jobChangeReasons != null)
                    {
                        var jobChangeReason = jobChangeReasons.FirstOrDefault(jcr => jcr.Guid == institutionJobs.JobChangeReason.Id);
                        if (jobChangeReason != null)
                        {
                            response.EndReason = jobChangeReason.Code;
                        }
                    }
                }

                var accountingStrings = new List<string>();
                if (institutionJobs.AccountingStrings != null)
                {
                    foreach (var accountingString in institutionJobs.AccountingStrings)
                    {
                        if (!(string.IsNullOrWhiteSpace(accountingString)))
                        {
                            accountingStrings.Add(accountingString.Replace("-", "_"));
                        }
                    }
                }
                if (accountingStrings.Any())
                {
                    response.AccountingStrings = accountingStrings;
                }

                // Pay Status
                if (institutionJobs.PayStatus != Dtos.EnumProperties.PayStatus.NotSet)
                {
                    switch (institutionJobs.PayStatus)
                    {
                        case Dtos.EnumProperties.PayStatus.PartialPay:
                            {
                                response.PayStatus = Domain.HumanResources.Entities.PayStatus.PartialPay;
                                break;
                            }
                        case Dtos.EnumProperties.PayStatus.WithoutPay:
                            {
                                response.PayStatus = Domain.HumanResources.Entities.PayStatus.WithoutPay;
                                break;
                            }
                        case Dtos.EnumProperties.PayStatus.WithPay:
                            {
                                response.PayStatus = Domain.HumanResources.Entities.PayStatus.WithPay;
                                break;
                            }
                        default:
                            break;
                    }
                }

                if (institutionJobs.BenefitsStatus != Dtos.EnumProperties.BenefitsStatus.NotSet)
                {
                    switch (institutionJobs.BenefitsStatus)
                    {
                        case Dtos.EnumProperties.BenefitsStatus.WithBenefits:
                            {
                                response.BenefitsStatus = Domain.HumanResources.Entities.BenefitsStatus.WithBenefits;
                                break;
                            }
                        case Dtos.EnumProperties.BenefitsStatus.WithoutBenefits:
                            {
                                response.BenefitsStatus = Domain.HumanResources.Entities.BenefitsStatus.WithoutBenefits;
                                break;
                            }
                        default:
                            break;
                    }
                }

                if ((institutionJobs.HoursPerPeriod != null) && (institutionJobs.HoursPerPeriod.Any()))
                {
                    foreach (var hoursPerPeriod in institutionJobs.HoursPerPeriod)
                    {

                        if (hoursPerPeriod.Period == PayPeriods.PayPeriod)
                        {
                            response.CycleWorkTimeUnits = "HRS";
                            response.CycleWorkTimeAmount = hoursPerPeriod.Hours;

                        }
                        else if (hoursPerPeriod.Period == PayPeriods.Year)
                        {
                            response.YearWorkTimeUnits = "HRS";
                            response.YearWorkTimeAmount = hoursPerPeriod.Hours;
                        }
                        else
                        {
                            throw new ArgumentException("HoursPerPeriod.period only supports enumerations for 'year' and/or 'payPeriod'");
                        }
                    }
                }

                if ((institutionJobs.FullTimeEquivalent != null) && (institutionJobs.FullTimeEquivalent.HasValue))
                {
                    response.FullTimeEquivalent = institutionJobs.FullTimeEquivalent;
                }
                if (institutionJobs.Preference == JobPreference.Primary)
                {
                    response.Primary = true;
                }


                if (institutionJobs.Classification != null && !(string.IsNullOrEmpty(institutionJobs.Classification.Id)))
                {
                    var classificaitons = await this.GetAllEmploymentClassificationAsync(bypassCache);
                    if (classificaitons != null)
                    {
                        var classification = classificaitons.FirstOrDefault(c => c.Guid == institutionJobs.Classification.Id);
                        if (classification != null)
                        {
                            response.Classification = classification.Code;
                        }
                        else
                        {
                            throw new ArgumentNullException("Classification not found for Id:" + institutionJobs.Classification.Id);
                        }
                    }
                }



                if ((institutionJobs.Supervisors != null) && (institutionJobs.Supervisors.Any()))
                {
                    foreach (var supervisor in institutionJobs.Supervisors)
                    {
                        var supervisorPositionId = await _personRepository.GetPersonIdFromGuidAsync(supervisor.Supervisor.Id);

                        if (string.IsNullOrEmpty(supervisorPositionId))
                        {
                            throw new ArgumentNullException("Supervisor not found for Id:" + supervisor.Supervisor.Id);
                        }
                        if (supervisor.Type == PositionReportsToType.Primary)            
                        {
                            if (!(string.IsNullOrEmpty(response.SupervisorId)))
                            {
                                throw new ArgumentException("Only one supervisor can be provided.");
                            }
                            response.SupervisorId = supervisorPositionId;
                        }
                        else if (supervisor.Type == PositionReportsToType.Alternative)
                        {
                            if (!(string.IsNullOrEmpty(response.AlternateSupervisorId)))
                            {
                                throw new ArgumentException("Only one alternate supervisor can be provided.");
                            }
                            response.AlternateSupervisorId = supervisorPositionId;
                        }
                        else
                        {
                            throw new ArgumentNullException("Supervisor type not recognized: " + supervisor.Type);
                        }
                    }
                }
           
              if (institutionJobs.Salary != null)
                {
                    var salary = institutionJobs.Salary;

                    if ((salary.SalaryAmount != null) && (salary.SalaryAmount.Rate != null))
                    {

                        if (salary.SalaryAmount.Rate.Value != null && salary.SalaryAmount.Rate.Value.HasValue)
                        {
                            if (salary.SalaryAmount.Rate.Value < 0)
                            {
                                throw new ArgumentException("Negative salary amounts are not permitted");
                            }
                            if (salary.SalaryAmount.Period == SalaryPeriod.Year)
                            {
                                response.IsSalary = true;
                                response.PayRate = UnformatSalary(salary.SalaryAmount.Rate.Value);
                            }
                            else if (salary.SalaryAmount.Period == SalaryPeriod.Hour)
                            {
                                response.PayRate = UnformatSalary(salary.SalaryAmount.Rate.Value);
                            }
                            else if (salary.SalaryAmount.Period == SalaryPeriod.Contract)
                            {
                                throw new ArgumentException("Contract SalaryPeriod is not permitted");

                            }
                            if (salary.SalaryAmount.Rate.Currency == null)
                            {
                                throw new ArgumentException("Salary Amount Rate Currency is invalid or missing.");
                            }
                            string currency;
                            switch (salary.SalaryAmount.Rate.Currency)
                            {
                                case CurrencyIsoCode.USD:
                                    currency = "USD";
                                    break;
                                case CurrencyIsoCode.CAD:
                                    currency = "CAD";
                                    break;
                                default:
                                    currency = salary.SalaryAmount.Rate.Currency.ToString();
                                    break;
                            }
                            response.CurrencyCode = currency;

                        }
                    }

                    response.Grade = salary.Grade;
                    response.Step = salary.Step;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("An error occurred processing InstitutionJob Id: ", institutionJobsId, " ", ex.Message));
            }
           
            return response;
        }

       

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Converts a InstitutionJobs domain entity to its corresponding InstitutionJobs DTO
        /// </summary>
        /// <param name="source">InstitutionJobs domain entity</param>
        /// <param name="bypassCache"></param>
        /// <returns>InstitutionJobs DTO</returns>
        private async Task<Dtos.InstitutionJobs> ConvertInstitutionJobsEntityToDtoAsync(Domain.HumanResources.Entities.InstitutionJobs source, bool bypassCache = false)
        {
            var institutionJobs = new Ellucian.Colleague.Dtos.InstitutionJobs();
            if (source == null)
            {
                throw new ArgumentNullException("An unexpected error occurred extracting Institution Jobs.");
            }

            try
            {

                institutionJobs.Id = source.Guid;

                if (string.IsNullOrEmpty(source.PersonId))
                {
                    throw new ArgumentNullException("Person ID is required for Institution Jobs.  Id:" + source.Id);
                }
                var personGuid = await _personRepository.GetPersonGuidFromIdAsync(source.PersonId);
                if (!(string.IsNullOrEmpty(personGuid)))
                    institutionJobs.Person = new GuidObject2(personGuid);

                var employerGuid = string.Empty;
                if (!_employerGuidDictionary.TryGetValue(source.Employer, out employerGuid))
                {
                    employerGuid = await _personRepository.GetPersonGuidFromIdAsync(source.Employer);
                    if (!(string.IsNullOrEmpty(employerGuid))) 
                        _employerGuidDictionary.Add(source.Employer, employerGuid);
                }
                if (!(string.IsNullOrEmpty(employerGuid)))
                    institutionJobs.Employer = new GuidObject2(employerGuid);


                if (string.IsNullOrEmpty(source.PositionId))
                {
                    throw new ArgumentNullException("Position ID is required for Institution Jobs. Id:" + source.Id);
                }
                var positionGuid = await _positionRepository.GetPositionGuidFromIdAsync(source.PositionId);
                if (!(string.IsNullOrEmpty(positionGuid)))
                    institutionJobs.Position = new GuidObject2(positionGuid);


                institutionJobs.Department = source.Department;

                institutionJobs.StartOn = source.StartDate;
                institutionJobs.EndOn = source.EndDate;


                if (!(string.IsNullOrEmpty(source.EndReason)))
                {
                    var jobChangeReasons = await this.GetAllJobChangeReasonsAsync(bypassCache);
                    if (jobChangeReasons != null)
                    {
                        var jobChangeReason = jobChangeReasons.FirstOrDefault(jcr => jcr.Code == source.EndReason);
                        if (jobChangeReason != null)
                        {
                            institutionJobs.JobChangeReason = new GuidObject2(jobChangeReason.Guid);
                        }
                    }
                }
                var accountingStrings = new List<string>();
                if (source.AccountingStrings != null)
                {
                    foreach (var accountingString in source.AccountingStrings)
                    {
                        if (!(string.IsNullOrWhiteSpace(accountingString)))
                        {
                            accountingStrings.Add(accountingString.Replace("_", "-"));
                        }
                    }
                }
                if (accountingStrings.Any())
                {
                    institutionJobs.AccountingStrings = accountingStrings;
                }

                // If the PERPOS.END.DATE is null or the PERPOS.END.DATE is populated with a date that is on or after the request date.
                if ((!source.EndDate.HasValue) || (source.EndDate >= DateTime.Now))
                {
                    institutionJobs.Status = InstitutionJobsStatus.Active;
                }
                //The PERPOS.END.DATE is not null and is a date prior to the request date.
                else if (source.EndDate < DateTime.Now)
                {
                    institutionJobs.Status = InstitutionJobsStatus.Ended;
                }
                else
                {
                    throw new Exception("Unable to determine institution job status.  Id" + source.Id);
                }

                // Pay Status
                if (source.PayStatus != null)
                {
                    switch (source.PayStatus)
                    {
                        case Domain.HumanResources.Entities.PayStatus.PartialPay:
                        {
                            institutionJobs.PayStatus = Dtos.EnumProperties.PayStatus.PartialPay;
                            break;
                        }
                        case Domain.HumanResources.Entities.PayStatus.WithoutPay:
                        {
                            institutionJobs.PayStatus = Dtos.EnumProperties.PayStatus.WithoutPay;
                            break;
                        }
                        case Domain.HumanResources.Entities.PayStatus.WithPay:
                        {
                            institutionJobs.PayStatus = Dtos.EnumProperties.PayStatus.WithPay;
                            break;
                        }
                        default:
                            break;
                    }
                }

                // Benefits Status
                if (source.BenefitsStatus != null)
                {
                    switch (source.BenefitsStatus)
                    {
                        case Domain.HumanResources.Entities.BenefitsStatus.WithBenefits:
                        {
                            institutionJobs.BenefitsStatus = Dtos.EnumProperties.BenefitsStatus.WithBenefits;
                            break;
                        }
                        case Domain.HumanResources.Entities.BenefitsStatus.WithoutBenefits:
                        {
                            institutionJobs.BenefitsStatus = Dtos.EnumProperties.BenefitsStatus.WithoutBenefits;
                            break;
                        }
                        default:
                            break;
                    }
                }

                var hoursPerPeriodDtoProperties = new List<HoursPerPeriodDtoProperty>();
                if ((source.CycleWorkTimeUnits == "HRS") && (source.CycleWorkTimeAmount.HasValue))
                {
                    hoursPerPeriodDtoProperties.Add(new HoursPerPeriodDtoProperty()
                    {
                        Hours = source.CycleWorkTimeAmount,
                        Period = PayPeriods.PayPeriod
                    });
                }
                if ((source.YearWorkTimeUnits == "HRS") && (source.YearWorkTimeAmount.HasValue))
                {
                    hoursPerPeriodDtoProperties.Add(new HoursPerPeriodDtoProperty()
                    {
                        Hours = source.YearWorkTimeAmount,
                        Period = PayPeriods.Year
                    });
                }
                institutionJobs.HoursPerPeriod = hoursPerPeriodDtoProperties.Any() ? hoursPerPeriodDtoProperties : null;



                if ((source.FullTimeEquivalent != null) && (source.FullTimeEquivalent > 0))
                    institutionJobs.FullTimeEquivalent = source.FullTimeEquivalent;


                var supervisors = new List<SupervisorsDtoProperty>();
                if (!string.IsNullOrEmpty(source.SupervisorId))
                {
                    try
                    {
                        var supervisorPositionGuid = await _personRepository.GetPersonGuidFromIdAsync(source.SupervisorId);
                        if (!string.IsNullOrEmpty(supervisorPositionGuid))
                        {
                            var supervisorsDtoProperty = new SupervisorsDtoProperty()
                            {
                                Supervisor = new GuidObject2(supervisorPositionGuid),
                                Type = PositionReportsToType.Primary
                            };
                            supervisors.Add(supervisorsDtoProperty);
                        }
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        if (logger.IsErrorEnabled)
                        {
                            logger.Error(ex, string.Concat("An error occurred obtaining InstitutionJob  supervisor Id: ", source.Id));
                        }
                    }
                }
                if (!string.IsNullOrEmpty(source.AlternateSupervisorId))
                {
                    try
                    {
                        var altSupervisorPositionGuid = await _personRepository.GetPersonGuidFromIdAsync(source.AlternateSupervisorId);
                        if (!string.IsNullOrEmpty(altSupervisorPositionGuid))
                        {
                            var supervisorsDtoProperty = new SupervisorsDtoProperty()
                            {
                                Supervisor = new GuidObject2(altSupervisorPositionGuid),
                                Type = PositionReportsToType.Alternative
                            };
                            supervisors.Add(supervisorsDtoProperty);
                        }
                    }
                    catch (ArgumentOutOfRangeException ex)
                    {
                        if (logger.IsErrorEnabled)
                        {
                            logger.Error(ex, string.Concat("An error occurred obtaining alt supervisor InstitutionJob Id: ", source.Id) );
                        }
                    }
                }
                if (supervisors.Any())
                    institutionJobs.Supervisors = supervisors;


                if (!(string.IsNullOrWhiteSpace(source.PayRate)))
                {
                    var salary = new SalaryDtoProperty
                    {
                        Grade = source.Grade,
                        Step = source.Step
                    };

                    var hostCountry = source.HostCountry;
                    var currencyIsoCode = CurrencyIsoCode.NotSet;
                    //var currencyIsoCode = ((hostCountry == "CAN") || (hostCountry == "CANADA")) ? CurrencyIsoCode.CAD :
                    //    CurrencyIsoCode.USD;

                    if (!string.IsNullOrEmpty(hostCountry))
                    {                       
                        switch (hostCountry)
                        {
                            case "US":
                            case "USA":
                                currencyIsoCode = CurrencyIsoCode.USD;
                                break;
                            case "CAN":
                            case "CANADA":
                                currencyIsoCode = CurrencyIsoCode.CAD;                              
                                break;
                            default:
                                throw new ArgumentException("Currency is only supported for 'USA' and 'CAD' ");
                        }
                    }
                    else
                    {
                        throw new ArgumentNullException("Host country not found.");
                    }

                    var amount = new Amount2DtoProperty {Currency = currencyIsoCode};
                    var salaryAmount = new SalaryAmountDtoProperty {Rate = amount};

                    if (source.IsSalary)
                    {
                        salaryAmount.Period = SalaryPeriod.Year;
                        amount.Value = FormatSalary(source.PayRate, true);
                    }
                    else
                    {
                        salaryAmount.Period = SalaryPeriod.Hour;
                        amount.Value = FormatSalary(source.PayRate, false);
                    }

                    salary.SalaryAmount = salaryAmount;
                    if (amount.Value != null)
                        institutionJobs.Salary = salary;
                }

                if (!(string.IsNullOrEmpty(source.Classification)))
                {
                    var classificaitons = await this.GetAllEmploymentClassificationAsync(bypassCache);
                    if (classificaitons != null)
                    {
                        var classification = classificaitons.FirstOrDefault(c => c.Code == source.Classification);
                        if (classification != null)
                        {
                            institutionJobs.Classification = new GuidObject2(classification.Guid);
                        }
                    }
                }

                if (source.Primary)
                    institutionJobs.Preference = JobPreference.Primary;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Concat("An error occurred obtaining InstitutionJob Id: ", source.Id, " ", ex.Message));
            }
            return institutionJobs;
        }

        /// <summary>
        /// Helper method to format salary 
        /// </summary>
        /// <param name="amount"></param>
        /// <param name="isSalary"></param>
        /// <returns></returns>
        private decimal? FormatSalary(string amount, bool isSalary)
        {
            if (string.IsNullOrWhiteSpace(amount) || amount == "0") return null;
            try
            {
                var paddedValue = isSalary ? amount.Insert(amount.Length - 2, ".") : amount.Insert(amount.Length - 4, ".");
                return Convert.ToDecimal(paddedValue);
            }
            catch (Exception)
            {
                return null;
            }
        }

        /// <summary>
        /// Helper method to unformat salary 
        /// </summary>
        /// <param name="amount"></param>
        /// <returns></returns>
        private string UnformatSalary(decimal? amount)
        {
            if (!(amount.HasValue)) return null;

            return amount.ToString(); //.Replace(".", "");
        }


        /// <summary>
        /// Helper method to determine if the user has permission to view Institution Jobs.
        /// </summary>
        /// <exception><see cref="PermissionsException">PermissionsException</see></exception>
        private void CheckGetInstitutionJobsPermission()
        {
            var hasPermission = HasPermission(HumanResourcesPermissionCodes.ViewInstitutionJob);

            if (!hasPermission)
            {
                throw new PermissionsException("User " + CurrentUser.UserId + " does not have permission to view Institution Jobs.");
            }
        }

        /// <summary>
        /// Helper method to determine if the user has permission to create/update Institution Jobs.
        /// </summary>
        /// <exception><see cref="PermissionsException">PermissionsException</see></exception>
        private void CheckCreateInstitutionJobsPermission()
        {
            var hasPermission = HasPermission(HumanResourcesPermissionCodes.CreateInstitutionJob);

            if (!hasPermission)
            {
                throw new PermissionsException("User " + CurrentUser.UserId + " does not have permission to create or update Institution Jobs.");
            }
        }

        /// <summary>
        /// Converts date to unidata Date
        /// </summary>
        /// <param name="date">UTC datetime</param>
        /// <returns>Unidata Date</returns>
        private async Task<string> ConvertDateArgument(string date)
        {
            try
            {
                return await _referenceDataRepository.GetUnidataFormattedDate(date);
            }
            catch (Exception)
            {
                throw new ArgumentException("Invalid Date format in arguments");
            }
        }

        /// <summary>
        /// Get all JobChangeReasons Entity Objects
        /// </summary>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        private async Task<IEnumerable<Domain.HumanResources.Entities.JobChangeReason>> GetAllJobChangeReasonsAsync(bool bypassCache)
        {
            if (_jobChangeReasons == null)
            {
                _jobChangeReasons = await _hrReferenceDataRepository.GetJobChangeReasonsAsync(bypassCache);
            }
            return _jobChangeReasons;
        }

        /// <summary>
        /// Get all EmploymentClassification Entity Objects
        /// </summary>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        private async Task<IEnumerable<Domain.HumanResources.Entities.EmploymentClassification>> GetAllEmploymentClassificationAsync(bool bypassCache)
        {
            if (_employmentClassification == null)
            {
                _employmentClassification = await _hrReferenceDataRepository.GetEmploymentClassificationsAsync(bypassCache);
            }
            return _employmentClassification;
        }

       
        

    }
}