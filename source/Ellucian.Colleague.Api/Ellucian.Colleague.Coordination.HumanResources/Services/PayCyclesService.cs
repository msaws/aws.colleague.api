﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.HumanResources.Services
{
    [RegisterType]
    public class PayCycleService : BaseCoordinationService, IPayCycleService
    {
        private readonly IPayCycleRepository payCycleRepository;

        public PayCycleService(
            IPayCycleRepository payCycleRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.payCycleRepository = payCycleRepository;
        }

        /// <summary>
        /// Get all Pay Cycles
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<PayCycle>> GetPayCyclesAsync()
        {
            var payCycleEntities = await payCycleRepository.GetPayCyclesAsync();
            var entityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.HumanResources.Entities.PayCycle, PayCycle>();
            return payCycleEntities.Select(payCycle => entityToDtoAdapter.MapToType(payCycle));
        }
    }
}
