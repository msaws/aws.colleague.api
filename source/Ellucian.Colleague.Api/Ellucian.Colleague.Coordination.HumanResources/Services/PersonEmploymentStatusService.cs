﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.HumanResources;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.HumanResources.Services
{
    [RegisterType]
    public class PersonEmploymentStatusService : BaseCoordinationService, IPersonEmploymentStatusService
    {
        private readonly IPersonEmploymentStatusRepository personEmploymentStatusRepository;
        private readonly ISupervisorsRepository supervisorsRepository;

        public PersonEmploymentStatusService(
            IPersonEmploymentStatusRepository personEmploymentStatusRepository,
            ISupervisorsRepository supervisorsRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.personEmploymentStatusRepository = personEmploymentStatusRepository;
            this.supervisorsRepository = supervisorsRepository;
        }


        /// <summary>
        /// Get the PersonStatuses based on the permissions of the current user
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<PersonEmploymentStatus>> GetPersonEmploymentStatusesAsync()
        {
            var userAndSubordinateIds = new List<string>() { CurrentUser.PersonId };

            if (HasPermission(HumanResourcesPermissionCodes.ViewSuperviseeData))
            {
                var subordinateIds = (await supervisorsRepository.GetSuperviseesBySupervisorAsync(CurrentUser.PersonId)).ToList();

                if (subordinateIds == null)
                {
                    var message = "Unexpected null person id list returned from supervisors repository";
                    logger.Error(message);
                    throw new ApplicationException(message);
                }
                if (subordinateIds.Any())
                {
                    userAndSubordinateIds = userAndSubordinateIds.Concat(subordinateIds).ToList();
                }
            }       

            var personEmploymentStatusEntities = await personEmploymentStatusRepository.GetPersonEmploymentStatusesAsync(userAndSubordinateIds);

            if (personEmploymentStatusEntities == null)
            {
                var message = "Unexpected null personStatusEntities returned from repository";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            var personEmploymentStatusEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.HumanResources.Entities.PersonEmploymentStatus, PersonEmploymentStatus>();

            return personEmploymentStatusEntities.Select(perstat => personEmploymentStatusEntityToDtoAdapter.MapToType(perstat));
        }
    }
}
