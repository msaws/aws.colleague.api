﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Colleague.Coordination.ColleagueFinance.Tests.UserFactories;
using Ellucian.Colleague.Domain.ColleagueFinance.Tests;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Web.Security;
using System.Collections.Generic;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.Exceptions;
using System;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Dtos.EnumProperties;
using System.Collections.ObjectModel;
using Ellucian.Colleague.Dtos;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Tests.Services
{
    /// <summary>
    /// This class tests the AccountsPayableTypeService class.
    /// </summary>
    [TestClass]
    public class AccountsPayableInvoicesServiceTests : GeneralLedgerCurrentUser
    {
        #region Initialize and Cleanup
        private AccountsPayableInvoicesService AccountsPayableInvoicesService;
        private TestVoucherRepository testVoucherRepository;
        private Mock<IAccountsPayableInvoicesRepository> mockAccountsPayableInvoices;
        private Mock<IColleagueFinanceReferenceDataRepository> mockcolleagueFinanceReferenceDataRepository;
        private Mock<IReferenceDataRepository> mockreferenceDataRepository;
        private Mock<IGeneralLedgerConfigurationRepository> mockGeneralLedgerConfigurationRepository;
        private Mock<IPersonRepository> mockPersonRepository;
        private Mock<IAddressRepository> mockaddressRepository;
        private Mock<IVendorsRepository> mockvendorsRepository;
        private Mock<IAccountFundsAvailableRepository> mockAccountFundsAvailable;
        private AccountFundsAvailableUser currentUserFactory = new GeneralLedgerCurrentUser.AccountFundsAvailableUser();
        private Mock<IRoleRepository> roleRepositoryMock;
        protected Ellucian.Colleague.Domain.Entities.Role viewAccountsPayableInvoicesRole = new Ellucian.Colleague.Domain.Entities.Role(1, "VIEW.AP.INVOICES");
        protected Ellucian.Colleague.Domain.Entities.Role updateAccountsPayableInvoicesRole = new Domain.Entities.Role(1, "UPDATE.AP.INVOICES");

        private IConfigurationRepository baseConfigurationRepository;
        private Mock<IConfigurationRepository> baseConfigurationRepositoryMock;
        
        private Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableInvoices AccountsPayableInvoicesEntity;
        private Ellucian.Colleague.Dtos.AccountsPayableInvoices _accountsPayableInvoiceDto;
        private Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableInvoices> accountsPayableInvoicesEntities = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableInvoices>();
        string guid = "4f937f08-f6a0-4a1c-8d55-9f2a6dd6be46";

        string[] voucherIds = { "1" , "2", "3","4","11","13", "14", "15", "16", "17", "18"
                , "19", "20" ,"21", "22", "23", "24", "25", "26", "27", "29"};

        string[] guids = { "4f937f08-f6a0-4a1c-8d55-9f2a6dd6be46", "guid2-f6a0-4a1c-8d55-9f2a6dd6be46", "guid3-f6a0-4a1c-8d55-9f2a6dd6be46"
            , "guid4-f6a0-4a1c-8d55-9f2a6dd6be46"};
        private int versionNumber;

        [TestInitialize]
        public void Initialize()
        {
            mockAccountsPayableInvoices = new Mock<IAccountsPayableInvoicesRepository>();
            baseConfigurationRepositoryMock = new Mock<IConfigurationRepository>();
            baseConfigurationRepository = baseConfigurationRepositoryMock.Object;
            BuildValidVoucherService();
            versionNumber = 2;
            BuildDto();
        }

        

        [TestCleanup]
        public void Cleanup()
        {
            // Reset all of the services and repository variables.
            AccountsPayableInvoicesService = null;
            testVoucherRepository = null;
            mockAccountsPayableInvoices = null;
            mockcolleagueFinanceReferenceDataRepository = null;
            mockreferenceDataRepository = null;
            mockaddressRepository = null;
            mockvendorsRepository = null;
            currentUserFactory = null;
            mockGeneralLedgerConfigurationRepository = null;
            mockPersonRepository = null;
        }
        #endregion

        [TestMethod]
        public async Task AccountsPayableInvoicesServiceTests_GetAccountsPayableInvoicesByGuidAsync()
        {
            string voucherId = "1";
            var voucherDomainEntity = await testVoucherRepository.GetVoucherAsync(voucherId, "00000001", GlAccessLevel.Full_Access, null, versionNumber);
             
            Collection<Ellucian.Colleague.Domain.Base.Entities.CommerceTaxCode> TaxCodInfo = new Collection<Ellucian.Colleague.Domain.Base.Entities.CommerceTaxCode>() { new Ellucian.Colleague.Domain.Base.Entities.CommerceTaxCode("TaxGuid", "ST", "TestGUIDdesc") };
            mockreferenceDataRepository.Setup(repo => repo.GetCommerceTaxCodesAsync(It.IsAny <bool>())).ReturnsAsync(TaxCodInfo);

            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityCode> commodityCodes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityCode>() { new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityCode("CommodityGuid321", "00402", "Test Commodity") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetCommodityCodesAsync(It.IsAny<bool>())).ReturnsAsync(commodityCodes);

            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources> apTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources>() { new Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources("apTypeGuid321", "AP", "Account Payable") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetAccountsPayableSourcesAsync(It.IsAny<bool>())).ReturnsAsync(apTypes);

            Collection<VendorTerm> Terms = new Collection<VendorTerm>() { new VendorTerm("TermsGuid321", "02", "02-15 days") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetVendorTermsAsync(It.IsAny<bool>())).ReturnsAsync(Terms);

            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType> UnitTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType>() {
                new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid321", "rock", "Rocks"),
                new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid123", "thing", "Things") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetCommodityUnitTypesAsync(It.IsAny<bool>())).ReturnsAsync(UnitTypes);

            AccountsPayableInvoicesEntity = ConvertVoucherEntityToAPI(voucherDomainEntity);
            mockAccountsPayableInvoices.Setup(repo => repo.GetAccountsPayableInvoicesByGuidAsync(It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(AccountsPayableInvoicesEntity);
            mockvendorsRepository.Setup(repo => repo.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("VendorIDGuid");
            mockaddressRepository.Setup(repo => repo.GetAddressGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("AddressGuid");
            
            var actual = await AccountsPayableInvoicesService.GetAccountsPayableInvoicesByGuidAsync(guid);
            Assert.IsNotNull(actual);
            Assert.AreEqual(guid, actual.Id);
            Assert.AreEqual(AccountsPayableInvoicesEntity.Comments, actual.InvoiceComment);
            Assert.AreEqual(AccountsPayableInvoicesEntity.VoucherDiscAmt, actual.InvoiceDiscountAmount.Value);
            Assert.AreEqual(AccountsPayableInvoicesInvoiceType.Invoice, actual.InvoiceType);
            Assert.AreEqual(AccountsPayableInvoicesProcessState.Inprogress, actual.ProcessState);
            if (actual.ProcessState != AccountsPayableInvoicesProcessState.NotSet)
            {
                 if (AccountsPayableInvoicesEntity.VoucherPayFlag == "Y")
                {
                    Assert.AreEqual(AccountsPayableInvoicesPaymentStatus.Nohold, actual.PaymentStatus);
                } else
                {
                    Assert.AreEqual(AccountsPayableInvoicesPaymentStatus.Hold, actual.PaymentStatus);
                }
            }
            Assert.AreEqual("RefNo1111", actual.ReferenceNumber);
            Assert.AreEqual("taxguid", actual.Taxes[0].TaxCode.Id);
            Assert.AreEqual(AccountsPayableInvoicesEntity.VoucherTaxes[0].TaxAmount, actual.Taxes[0].VendorAmount.Value);
            Assert.AreEqual(Dtos.EnumProperties.CurrencyIsoCode.USD, actual.Taxes[0].VendorAmount.Currency);
            
            Assert.AreEqual("VendorIDGuid", actual.Vendor.Id);
            Assert.AreEqual("AddressGuid", actual.VendorAddress.Id);
            Assert.AreEqual(AccountsPayableInvoicesEntity.VoucherInvoiceAmt, actual.VendorBilledAmount.Value);
            Assert.AreEqual(Dtos.EnumProperties.CurrencyIsoCode.USD, actual.VendorBilledAmount.Currency);
            Assert.AreEqual(AccountsPayableInvoicesEntity.InvoiceDate, actual.VendorInvoiceDate);
            Assert.AreEqual(AccountsPayableInvoicesEntity.InvoiceNumber, actual.VendorInvoiceNumber);
            Assert.AreEqual(AccountsPayableInvoicesEntity.VoucherVoidGlTranDate, actual.VoidDate);
            Assert.AreEqual("aptypeguid321", actual.Payment.Source.Id);
            Assert.AreEqual(AccountsPayableInvoicesEntity.DueDate, actual.Payment.PaymentDueOn);
            Assert.AreEqual("termsguid321", actual.Payment.PaymentTerms.Id);

            Assert.AreEqual(AccountsPayableInvoicesEntity.LineItems.Count(), actual.LineItems.Count());
            for (int x =0; x > actual.LineItems.Count(); x++)
            {
                var dtoLi = actual.LineItems[x];
                var entityLi = AccountsPayableInvoicesEntity.LineItems[x];

                Assert.AreEqual(entityLi.Description, dtoLi.Description);
                Assert.AreEqual("CommodityGuid321", dtoLi.CommodityCode.Id);
                Assert.AreEqual(entityLi.Quantity, dtoLi.Quantity);
                Assert.AreEqual(null, dtoLi.VendorBilledQuantity);
                if (entityLi.UnitOfIssue == "rocks")
                {
                    Assert.AreEqual("unitGuid321", dtoLi.Description);
                } else
                {
                    Assert.AreEqual("unitGuid123", dtoLi.Description);
                }
                Assert.AreEqual(entityLi.Price, dtoLi.UnitPrice.Value);
                Assert.AreEqual(Dtos.EnumProperties.CurrencyIsoCode.USD, dtoLi.UnitPrice.Currency);
                Assert.AreEqual(null, dtoLi.VendorBilledUnitPrice.Value);
                Assert.AreEqual(null, dtoLi.VendorBilledUnitPrice.Currency);
                Assert.AreEqual(null, dtoLi.AdditionalAmount.Value);
                Assert.AreEqual(null, dtoLi.AdditionalAmount.Currency);
                Assert.AreEqual(entityLi.LineItemTaxes.Count(), dtoLi.Taxes.Count());
                for (int i = 0; i < dtoLi.Taxes.Count(); i++)
                {
                    Assert.AreEqual(entityLi.LineItemTaxes[i].TaxCode, dtoLi.Taxes[i].TaxCode);
                    Assert.AreEqual(entityLi.LineItemTaxes[i].TaxAmount, dtoLi.Taxes[i].VendorAmount.Value);
                    Assert.AreEqual(Dtos.EnumProperties.CurrencyIsoCode.USD, dtoLi.Taxes[i].VendorAmount.Currency);
                }
                
                Assert.AreEqual(entityLi.CashDiscountAmount + entityLi.TradeDiscountAmount, dtoLi.Discount.Amount.Value);
                Assert.AreEqual(Dtos.EnumProperties.CurrencyIsoCode.USD, dtoLi.Discount.Amount.Currency);
                Assert.AreEqual(entityLi.TradeDiscountPercent, dtoLi.Discount.Percent);
                Assert.AreEqual(Dtos.EnumProperties.AccountsPayableInvoicesPaymentStatus.Nohold, dtoLi.PaymentStatus);
                Assert.AreEqual(entityLi.Comments, dtoLi.Comment);

                if (AccountsPayableInvoicesEntity.Status == VoucherStatus.InProgress 
                    || AccountsPayableInvoicesEntity.Status == VoucherStatus.NotApproved
                    || AccountsPayableInvoicesEntity.Status == VoucherStatus.Outstanding)
                {
                    Assert.AreEqual(AccountsPayableInvoicesStatus.Open, dtoLi.Status);
                } else
                {
                    Assert.AreEqual(AccountsPayableInvoicesStatus.Closed, dtoLi.Status);
                }
                Assert.AreEqual(entityLi.GlDistributions.Count(), dtoLi.AccountDetails.Count());
                for (int j = 0; j < entityLi.GlDistributions.Count(); j++)
                {
                    Assert.AreEqual(j, dtoLi.AccountDetails[j].SequenceNumber);
                    Assert.AreEqual(entityLi.GlDistributions[j].GlAccountNumber, dtoLi.AccountDetails[j].AccountingString);
                    Assert.AreEqual(entityLi.GlDistributions[j].Amount, dtoLi.AccountDetails[j].Allocation.Allocated.Amount.Value);
                    Assert.AreEqual(Dtos.EnumProperties.CurrencyIsoCode.USD, dtoLi.AccountDetails[j].Allocation.Allocated.Amount.Currency);
                    Assert.AreEqual(entityLi.GlDistributions[j].Quantity, dtoLi.AccountDetails[j].Allocation.Allocated.Quantity);
                    Assert.AreEqual(entityLi.GlDistributions[j].Percent, dtoLi.AccountDetails[j].Allocation.Allocated.Percentage);
                    Assert.AreEqual(null, dtoLi.AccountDetails[j].Allocation.AdditionalAmount.Value);
                    Assert.AreEqual(null, dtoLi.AccountDetails[j].Allocation.AdditionalAmount.Currency);
                    Assert.AreEqual(null, dtoLi.AccountDetails[j].Allocation.DiscountAmount.Value);
                    Assert.AreEqual(null, dtoLi.AccountDetails[j].Allocation.DiscountAmount.Currency);
                    Assert.AreEqual("apTypeGuid321", dtoLi.AccountDetails[j].Source.Id);
                }
            }
        }

        [TestMethod]
        public async Task AccountsPayableInvoicesServiceTests_GetAccountsPayableInvoicesAsync()
        {
            for(int x = 0; x <4; x++)
            {
                string voucherId = voucherIds[x];
                guid = guids[x];
                var voucherDomainEntity = await testVoucherRepository.GetVoucherAsync(voucherId, "00000001", GlAccessLevel.Full_Access, null, versionNumber);
                AccountsPayableInvoicesEntity = ConvertVoucherEntityToAPI(voucherDomainEntity);

                accountsPayableInvoicesEntities.Add(AccountsPayableInvoicesEntity);

            }

            Tuple<IEnumerable<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableInvoices>, int> GetAPIValues = new Tuple<IEnumerable<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableInvoices>, int>(accountsPayableInvoicesEntities, 4);
            mockAccountsPayableInvoices.Setup(repo => repo.GetAccountsPayableInvoicesAsync(0,100)).ReturnsAsync(GetAPIValues);
            mockvendorsRepository.Setup(repo => repo.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("VendorIDGuid");
            mockaddressRepository.Setup(repo => repo.GetAddressGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("AddressGuid");

            var actuals = await AccountsPayableInvoicesService.GetAccountsPayableInvoicesAsync(0,100);
            Assert.IsNotNull(actuals.Item1);
            Assert.AreEqual(4, actuals.Item2);
            Assert.AreEqual(4, actuals.Item1.Count());

            foreach(var actual in actuals.Item1)
            {
                var expected = accountsPayableInvoicesEntities.FirstOrDefault(x => x.Guid == actual.Id);

                Assert.IsNotNull(expected, actual.Id);
                Assert.AreEqual(expected.Comments, actual.InvoiceComment, actual.Id);
                Assert.AreEqual(expected.VoucherDiscAmt, actual.InvoiceDiscountAmount.Value, actual.Id);
                if (actual.ProcessState != AccountsPayableInvoicesProcessState.NotSet)
                {
                    if (AccountsPayableInvoicesEntity.VoucherPayFlag == "Y")
                    {
                        Assert.AreEqual(AccountsPayableInvoicesPaymentStatus.Nohold, actual.PaymentStatus, actual.Id);
                    }
                    else
                    {
                        Assert.AreEqual(AccountsPayableInvoicesPaymentStatus.Hold, actual.PaymentStatus, actual.Id);
                    }
                }
                Assert.AreEqual("RefNo1111", actual.ReferenceNumber, actual.Id);
                // Assert.AreEqual(AccountsPayableInvoicesEntity.VoucherTaxes, actual.Taxes);
                
                Assert.AreEqual("VendorIDGuid", actual.Vendor.Id,actual.Id);
                Assert.AreEqual("AddressGuid", actual.VendorAddress.Id, actual.Id);
                Assert.AreEqual(expected.VoucherInvoiceAmt, actual.VendorBilledAmount.Value, actual.Id);
                Dtos.EnumProperties.CurrencyIsoCode ThisCurrency = (expected.CurrencyCode == "CAD" ? Dtos.EnumProperties.CurrencyIsoCode.CAD : Dtos.EnumProperties.CurrencyIsoCode.USD);
                Assert.AreEqual(ThisCurrency, actual.VendorBilledAmount.Currency, actual.Id);
                Assert.AreEqual(expected.InvoiceDate, actual.VendorInvoiceDate, actual.Id);
                Assert.AreEqual(expected.InvoiceNumber, actual.VendorInvoiceNumber, actual.Id);
                Assert.AreEqual(expected.VoucherVoidGlTranDate, actual.VoidDate, actual.Id);

                if (expected.LineItems.Count() > 0)
                {
                    Assert.AreEqual(expected.LineItems.Count(), actual.LineItems.Count());
                    for (int x = 0; x > actual.LineItems.Count(); x++)
                    {
                        var dtoLi = actual.LineItems[x];
                        var entityLi = expected.LineItems[x];

                        Assert.AreEqual(entityLi.Description, dtoLi.Description);
                        Assert.AreEqual(entityLi.Quantity, dtoLi.Quantity);
                        Assert.AreEqual(null, dtoLi.VendorBilledQuantity);
                        Assert.AreEqual(entityLi.Price, dtoLi.UnitPrice.Value);
                        Assert.AreEqual(Dtos.EnumProperties.CurrencyCodes.USD, dtoLi.UnitPrice.Currency);
                        Assert.AreEqual(null, dtoLi.VendorBilledUnitPrice.Value);
                        Assert.AreEqual(null, dtoLi.VendorBilledUnitPrice.Currency);
                        Assert.AreEqual(null, dtoLi.AdditionalAmount.Value);
                        Assert.AreEqual(null, dtoLi.AdditionalAmount.Currency);
                        Assert.AreEqual(entityLi.LineItemTaxes.Count(), dtoLi.Taxes.Count());
                        for (int i = 0; i < dtoLi.Taxes.Count(); i++)
                        {
                            Assert.AreEqual(entityLi.LineItemTaxes[i].TaxCode, dtoLi.Taxes[i].TaxCode);
                            Assert.AreEqual(entityLi.LineItemTaxes[i].TaxAmount, dtoLi.Taxes[i].VendorAmount.Value);
                            Assert.AreEqual(Dtos.EnumProperties.CurrencyCodes.USD, dtoLi.Taxes[i].VendorAmount.Currency);
                        }

                        Assert.AreEqual(entityLi.CashDiscountAmount + entityLi.TradeDiscountAmount, dtoLi.Discount.Amount.Value);
                        Assert.AreEqual(Dtos.EnumProperties.CurrencyCodes.USD, dtoLi.Discount.Amount.Currency);
                        Assert.AreEqual(entityLi.TradeDiscountPercent, dtoLi.Discount.Percent);
                        Assert.AreEqual(Dtos.EnumProperties.AccountsPayableInvoicesPaymentStatus.Nohold, dtoLi.PaymentStatus);
                        Assert.AreEqual(entityLi.Comments, dtoLi.Comment);

                        if (AccountsPayableInvoicesEntity.Status == VoucherStatus.InProgress
                            || AccountsPayableInvoicesEntity.Status == VoucherStatus.NotApproved
                            || AccountsPayableInvoicesEntity.Status == VoucherStatus.Outstanding)
                        {
                            Assert.AreEqual(AccountsPayableInvoicesStatus.Open, dtoLi.Status);
                        }
                        else
                        {
                            Assert.AreEqual(AccountsPayableInvoicesStatus.Closed, dtoLi.Status);
                        }
                        Assert.AreEqual(entityLi.GlDistributions.Count(), dtoLi.AccountDetails.Count());
                        for (int j = 0; j < entityLi.GlDistributions.Count(); j++)
                        {
                            Assert.AreEqual(j, dtoLi.AccountDetails[j].SequenceNumber);
                            Assert.AreEqual(entityLi.GlDistributions[j].GlAccountNumber, dtoLi.AccountDetails[j].AccountingString);
                            Assert.AreEqual(entityLi.GlDistributions[j].Amount, dtoLi.AccountDetails[j].Allocation.Allocated.Amount.Value);
                            Assert.AreEqual(Dtos.EnumProperties.CurrencyCodes.USD, dtoLi.AccountDetails[j].Allocation.Allocated.Amount.Currency);
                            Assert.AreEqual(entityLi.GlDistributions[j].Quantity, dtoLi.AccountDetails[j].Allocation.Allocated.Quantity);
                            Assert.AreEqual(entityLi.GlDistributions[j].Percent, dtoLi.AccountDetails[j].Allocation.Allocated.Percentage);
                            Assert.AreEqual(null, dtoLi.AccountDetails[j].Allocation.AdditionalAmount.Value);
                            Assert.AreEqual(null, dtoLi.AccountDetails[j].Allocation.AdditionalAmount.Currency);
                            Assert.AreEqual(null, dtoLi.AccountDetails[j].Allocation.DiscountAmount.Value);
                            Assert.AreEqual(null, dtoLi.AccountDetails[j].Allocation.DiscountAmount.Currency);
                        }
                    }
                }
            }

        }

        [TestMethod]
        public async Task AccountsPayableInvoicesServiceTests_GetAccountsPayableInvoicesAsync_Offset()
        {
            for (int x = 1; x < 3; x++)
            {
                string voucherId = voucherIds[x];
                guid = guids[x];
                var voucherDomainEntity = await testVoucherRepository.GetVoucherAsync(voucherId, "00000001", GlAccessLevel.Full_Access, null, versionNumber);
                AccountsPayableInvoicesEntity = ConvertVoucherEntityToAPI(voucherDomainEntity);

                accountsPayableInvoicesEntities.Add(AccountsPayableInvoicesEntity);

            }

            Tuple<IEnumerable<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableInvoices>, int> GetAPIValues = new Tuple<IEnumerable<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableInvoices>, int>(accountsPayableInvoicesEntities, 2);
            mockAccountsPayableInvoices.Setup(repo => repo.GetAccountsPayableInvoicesAsync(1, 100)).ReturnsAsync(GetAPIValues);
            mockvendorsRepository.Setup(repo => repo.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("VendorIDGuid");
            mockaddressRepository.Setup(repo => repo.GetAddressGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("AddressGuid");

            var actuals = await AccountsPayableInvoicesService.GetAccountsPayableInvoicesAsync(1, 100);
            int i = 1;
            foreach(var actual in actuals.Item1)
            {
                guid = guids[i];
                Assert.AreEqual(guid, actual.Id);
                i++;
            }
        }

        [TestMethod]
        public async Task AccountsPayableInvoicesServiceTests_GetAccountsPayableInvoicesAsync_limit()
        {
            for (int x = 0; x < 2; x++)
            {
                string voucherId = voucherIds[x];
                guid = guids[x];
                var voucherDomainEntity = await testVoucherRepository.GetVoucherAsync(voucherId, "00000001", GlAccessLevel.Full_Access, null, versionNumber);
                AccountsPayableInvoicesEntity = ConvertVoucherEntityToAPI(voucherDomainEntity);

                accountsPayableInvoicesEntities.Add(AccountsPayableInvoicesEntity);

            }

            Tuple<IEnumerable<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableInvoices>, int> GetAPIValues = new Tuple<IEnumerable<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableInvoices>, int>(accountsPayableInvoicesEntities, 2);
            mockAccountsPayableInvoices.Setup(repo => repo.GetAccountsPayableInvoicesAsync(0, 2)).ReturnsAsync(GetAPIValues);
            mockvendorsRepository.Setup(repo => repo.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("VendorIDGuid");
            mockaddressRepository.Setup(repo => repo.GetAddressGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("AddressGuid");

            var actuals = await AccountsPayableInvoicesService.GetAccountsPayableInvoicesAsync(0, 2);

            Assert.AreEqual(2, actuals.Item1.Count());
        }

        [TestMethod]
        public async Task AccountsPayableInvoicesServiceTests_PUT()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });

            string voucherId = "1";
            var voucherDomainEntity = await testVoucherRepository.GetVoucherAsync(voucherId, "00000001", GlAccessLevel.Full_Access, null, versionNumber);

            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockaddressRepository.Setup(i => i.GetAddressFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources> apTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources>() { new Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources("apTypeGuid321", "AP", "Account Payable") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetAccountsPayableSourcesAsync(It.IsAny<bool>())).ReturnsAsync(apTypes);

            Collection<VendorTerm> Terms = new Collection<VendorTerm>() { new VendorTerm("TermsGuid321", "02", "02-15 days") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetVendorTermsAsync(It.IsAny<bool>())).ReturnsAsync(Terms);

            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType> UnitTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType>() {
                new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid321", "rock", "Rocks"),
                new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid123", "thing", "Things") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetCommodityUnitTypesAsync(It.IsAny<bool>())).ReturnsAsync(UnitTypes);
            Collection<Domain.ColleagueFinance.Entities.CommodityCode> commodityCodes = new Collection<Domain.ColleagueFinance.Entities.CommodityCode>() 
            {
                new Domain.ColleagueFinance.Entities.CommodityCode("commodity-guid", "code1", "Desc 1")
            };
            mockcolleagueFinanceReferenceDataRepository.Setup(i => i.GetCommodityCodesAsync(It.IsAny<bool>())).ReturnsAsync(commodityCodes);

            AccountsPayableInvoicesEntity = ConvertVoucherEntityToAPI(voucherDomainEntity);
            mockAccountsPayableInvoices.Setup(repo => repo.UpdateAccountsPayableInvoicesAsync(It.IsAny<Domain.ColleagueFinance.Entities.AccountsPayableInvoices>())).ReturnsAsync(AccountsPayableInvoicesEntity);
            mockvendorsRepository.Setup(repo => repo.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("VendorIDGuid");
            mockaddressRepository.Setup(repo => repo.GetAddressGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("AddressGuid");

            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        public async Task AccountsPayableInvoicesServiceTests_POST()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });

            string voucherId = "1";
            var voucherDomainEntity = await testVoucherRepository.GetVoucherAsync(voucherId, "00000001", GlAccessLevel.Full_Access, null, versionNumber);

            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockaddressRepository.Setup(i => i.GetAddressFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources> apTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources>() { new Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources("apTypeGuid321", "AP", "Account Payable") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetAccountsPayableSourcesAsync(It.IsAny<bool>())).ReturnsAsync(apTypes);

            Collection<VendorTerm> Terms = new Collection<VendorTerm>() { new VendorTerm("TermsGuid321", "02", "02-15 days") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetVendorTermsAsync(It.IsAny<bool>())).ReturnsAsync(Terms);

            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType> UnitTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType>() {
                new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid321", "rock", "Rocks"),
                new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid123", "thing", "Things") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetCommodityUnitTypesAsync(It.IsAny<bool>())).ReturnsAsync(UnitTypes);
            Collection<Domain.ColleagueFinance.Entities.CommodityCode> commodityCodes = new Collection<Domain.ColleagueFinance.Entities.CommodityCode>() 
            {
                new Domain.ColleagueFinance.Entities.CommodityCode("commodity-guid", "code1", "Desc 1")
            };
            mockcolleagueFinanceReferenceDataRepository.Setup(i => i.GetCommodityCodesAsync(It.IsAny<bool>())).ReturnsAsync(commodityCodes);

            AccountsPayableInvoicesEntity = ConvertVoucherEntityToAPI(voucherDomainEntity);
            mockAccountsPayableInvoices.Setup(repo => repo.CreateAccountsPayableInvoicesAsync(It.IsAny<Domain.ColleagueFinance.Entities.AccountsPayableInvoices>())).ReturnsAsync(AccountsPayableInvoicesEntity);
            mockvendorsRepository.Setup(repo => repo.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("VendorIDGuid");
            mockaddressRepository.Setup(repo => repo.GetAddressGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("AddressGuid");

            var result = await AccountsPayableInvoicesService.PostAccountsPayableInvoicesAsync(_accountsPayableInvoiceDto);
            Assert.IsNotNull(result);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_NullDto_ArgumentNullException()
        {
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_Null_DtoId_ArgumentNullException()
        {
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, new Dtos.AccountsPayableInvoices());
        }

        [TestMethod]
        [ExpectedException(typeof(RepositoryException))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_RepositoryException()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });

            string voucherId = "1";
            var voucherDomainEntity = await testVoucherRepository.GetVoucherAsync(voucherId, "00000001", GlAccessLevel.Full_Access, null, versionNumber);

            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ThrowsAsync(new RepositoryException());
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(RepositoryException))]
        public async Task AccountsPayableInvoicesServiceTests_POST_RepositoryException()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });

            string voucherId = "1";
            var voucherDomainEntity = await testVoucherRepository.GetVoucherAsync(voucherId, "00000001", GlAccessLevel.Full_Access, null, versionNumber);

            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ThrowsAsync(new RepositoryException());
            var result = await AccountsPayableInvoicesService.PostAccountsPayableInvoicesAsync(_accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_Exception()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });

            string voucherId = "1";
            var voucherDomainEntity = await testVoucherRepository.GetVoucherAsync(voucherId, "00000001", GlAccessLevel.Full_Access, null, versionNumber);

            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ThrowsAsync(new Exception());
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_POST_Exception()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });

            string voucherId = "1";
            var voucherDomainEntity = await testVoucherRepository.GetVoucherAsync(voucherId, "00000001", GlAccessLevel.Full_Access, null, versionNumber);

            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ThrowsAsync(new Exception());
            var result = await AccountsPayableInvoicesService.PostAccountsPayableInvoicesAsync(_accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_VendorId_Null_Exception()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            _accountsPayableInvoiceDto.Vendor = new GuidObject2("");

            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_Bad_VendorId_ApplicationException()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("");

            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_ProcessState_Approved_ApplicationException()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            //mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("");

            _accountsPayableInvoiceDto.ProcessState = AccountsPayableInvoicesProcessState.Approved;
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_Notapproved_VendorId_ApplicationException()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("");

            _accountsPayableInvoiceDto.ProcessState = AccountsPayableInvoicesProcessState.Notapproved;
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_Outstanding_VendorId_ApplicationException()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("");

            _accountsPayableInvoiceDto.ProcessState = AccountsPayableInvoicesProcessState.Outstanding;
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_Paid_VendorId_ApplicationException()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("");

            _accountsPayableInvoiceDto.ProcessState = AccountsPayableInvoicesProcessState.Paid;
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_Reconciled_VendorId_ApplicationException()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("");

            _accountsPayableInvoiceDto.ProcessState = AccountsPayableInvoicesProcessState.Reconciled;
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_Voided_VendorId_ApplicationException()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("");

            _accountsPayableInvoiceDto.ProcessState = AccountsPayableInvoicesProcessState.Voided;
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_Bad_AddressId_ApplicationException()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockaddressRepository.Setup(i => i.GetAddressFromGuidAsync(It.IsAny<string>())).ReturnsAsync("");

            //_accountsPayableInvoiceDto.ProcessState = AccountsPayableInvoicesProcessState.Approved;
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_PaymentSource_KeyNotFoundException()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockaddressRepository.Setup(i => i.GetAddressFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources> apTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources>() { new Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources("apTypeGuid321", "AP", "Account Payable") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetAccountsPayableSourcesAsync(It.IsAny<bool>())).ReturnsAsync(apTypes);

            _accountsPayableInvoiceDto.Payment.Source.Id = "1234";
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_PaymentTermsId_KeyNotFoundException()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockaddressRepository.Setup(i => i.GetAddressFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources> apTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources>() { new Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources("apTypeGuid321", "AP", "Account Payable") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetAccountsPayableSourcesAsync(It.IsAny<bool>())).ReturnsAsync(apTypes);
            Collection<VendorTerm> Terms = new Collection<VendorTerm>() { new VendorTerm("TermsGuid321", "02", "02-15 days") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetVendorTermsAsync(It.IsAny<bool>())).ReturnsAsync(Terms);

            _accountsPayableInvoiceDto.Payment.PaymentTerms.Id = "1234";
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_CommodityCodes_Exception()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockaddressRepository.Setup(i => i.GetAddressFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources> apTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources>() { new Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources("apTypeGuid321", "AP", "Account Payable") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetAccountsPayableSourcesAsync(It.IsAny<bool>())).ReturnsAsync(apTypes);
            Collection<VendorTerm> Terms = new Collection<VendorTerm>() { new VendorTerm("TermsGuid321", "02", "02-15 days") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetVendorTermsAsync(It.IsAny<bool>())).ReturnsAsync(Terms);
            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType> UnitTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType>() {
                new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid321", "rock", "Rocks"),
                new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid123", "thing", "Things") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetCommodityUnitTypesAsync(It.IsAny<bool>())).ReturnsAsync(UnitTypes);
            mockcolleagueFinanceReferenceDataRepository.Setup(i => i.GetCommodityCodesAsync(It.IsAny<bool>())).ReturnsAsync(null);
            
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_VendorBilledAmount_CAN_CommodityCodes_Exception()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockaddressRepository.Setup(i => i.GetAddressFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources> apTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources>() { new Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources("apTypeGuid321", "AP", "Account Payable") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetAccountsPayableSourcesAsync(It.IsAny<bool>())).ReturnsAsync(apTypes);
            Collection<VendorTerm> Terms = new Collection<VendorTerm>() { new VendorTerm("TermsGuid321", "02", "02-15 days") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetVendorTermsAsync(It.IsAny<bool>())).ReturnsAsync(Terms);
            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType> UnitTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType>() {
                new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid321", "rock", "Rocks"),
                new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid123", "thing", "Things") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetCommodityUnitTypesAsync(It.IsAny<bool>())).ReturnsAsync(UnitTypes);
            mockcolleagueFinanceReferenceDataRepository.Setup(i => i.GetCommodityCodesAsync(It.IsAny<bool>())).ReturnsAsync(null);

            _accountsPayableInvoiceDto.VendorBilledAmount.Currency = Dtos.EnumProperties.CurrencyIsoCode.CAD;
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_VendorBilledAmount_Default_CommodityCodes_Exception()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockaddressRepository.Setup(i => i.GetAddressFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources> apTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources>() { new Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources("apTypeGuid321", "AP", "Account Payable") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetAccountsPayableSourcesAsync(It.IsAny<bool>())).ReturnsAsync(apTypes);
            Collection<VendorTerm> Terms = new Collection<VendorTerm>() { new VendorTerm("TermsGuid321", "02", "02-15 days") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetVendorTermsAsync(It.IsAny<bool>())).ReturnsAsync(Terms);
            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType> UnitTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType>() {
                new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid321", "rock", "Rocks"),
                new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid123", "thing", "Things") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetCommodityUnitTypesAsync(It.IsAny<bool>())).ReturnsAsync(UnitTypes);
            mockcolleagueFinanceReferenceDataRepository.Setup(i => i.GetCommodityCodesAsync(It.IsAny<bool>())).ReturnsAsync(null);

            _accountsPayableInvoiceDto.VendorBilledAmount.Currency = null;
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_CommodityUnitTypes_Exception()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockaddressRepository.Setup(i => i.GetAddressFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources> apTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources>() { new Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources("apTypeGuid321", "AP", "Account Payable") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetAccountsPayableSourcesAsync(It.IsAny<bool>())).ReturnsAsync(apTypes);
            Collection<VendorTerm> Terms = new Collection<VendorTerm>() { new VendorTerm("TermsGuid321", "02", "02-15 days") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetVendorTermsAsync(It.IsAny<bool>())).ReturnsAsync(Terms);
            Collection<Domain.ColleagueFinance.Entities.CommodityCode> commodityCodes = new Collection<Domain.ColleagueFinance.Entities.CommodityCode>() 
            {
                new Domain.ColleagueFinance.Entities.CommodityCode("commodity-guid", "code1", "Desc 1")
            };
            mockcolleagueFinanceReferenceDataRepository.Setup(i => i.GetCommodityCodesAsync(It.IsAny<bool>())).ReturnsAsync(commodityCodes);
            //Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType> UnitTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType>() {
            //    new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid321", "rock", "Rocks"),
            //    new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid123", "thing", "Things") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetCommodityUnitTypesAsync(It.IsAny<bool>())).ReturnsAsync(null);

            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_CommodityCodeId_Exception()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockaddressRepository.Setup(i => i.GetAddressFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources> apTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources>() { new Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources("apTypeGuid321", "AP", "Account Payable") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetAccountsPayableSourcesAsync(It.IsAny<bool>())).ReturnsAsync(apTypes);
            Collection<VendorTerm> Terms = new Collection<VendorTerm>() { new VendorTerm("TermsGuid321", "02", "02-15 days") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetVendorTermsAsync(It.IsAny<bool>())).ReturnsAsync(Terms);
            Collection<Domain.ColleagueFinance.Entities.CommodityCode> commodityCodes = new Collection<Domain.ColleagueFinance.Entities.CommodityCode>() 
            {
                new Domain.ColleagueFinance.Entities.CommodityCode("commodity-guid", "code1", "Desc 1")
            };
            mockcolleagueFinanceReferenceDataRepository.Setup(i => i.GetCommodityCodesAsync(It.IsAny<bool>())).ReturnsAsync(commodityCodes);
            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType> UnitTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType>() {
                new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid321", "rock", "Rocks"),
                new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid123", "thing", "Things") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetCommodityUnitTypesAsync(It.IsAny<bool>())).ReturnsAsync(UnitTypes);

            _accountsPayableInvoiceDto.LineItems.First().CommodityCode.Id = "abcd";
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_UnitofMeasureId_Exception()
        {
            updateAccountsPayableInvoicesRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdateApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updateAccountsPayableInvoicesRole });
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("00000001");
            mockAccountsPayableInvoices.Setup(i => i.GetAccountsPayableInvoicesIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockvendorsRepository.Setup(i => i.GetVendorIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            mockaddressRepository.Setup(i => i.GetAddressFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");
            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources> apTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources>() { new Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources("apTypeGuid321", "AP", "Account Payable") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetAccountsPayableSourcesAsync(It.IsAny<bool>())).ReturnsAsync(apTypes);
            Collection<VendorTerm> Terms = new Collection<VendorTerm>() { new VendorTerm("TermsGuid321", "02", "02-15 days") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetVendorTermsAsync(It.IsAny<bool>())).ReturnsAsync(Terms);
            Collection<Domain.ColleagueFinance.Entities.CommodityCode> commodityCodes = new Collection<Domain.ColleagueFinance.Entities.CommodityCode>() 
            {
                new Domain.ColleagueFinance.Entities.CommodityCode("commodity-guid", "code1", "Desc 1")
            };
            mockcolleagueFinanceReferenceDataRepository.Setup(i => i.GetCommodityCodesAsync(It.IsAny<bool>())).ReturnsAsync(commodityCodes);
            Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType> UnitTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType>() {
                new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid321", "rock", "Rocks"),
                new Ellucian.Colleague.Domain.ColleagueFinance.Entities.CommodityUnitType("unitGuid123", "thing", "Things") };
            mockcolleagueFinanceReferenceDataRepository.Setup(repo => repo.GetCommodityUnitTypesAsync(It.IsAny<bool>())).ReturnsAsync(UnitTypes);

            _accountsPayableInvoiceDto.LineItems.First().UnitofMeasure.Id = "abcd";
            var result = await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(guid, _accountsPayableInvoiceDto);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_DtoNull_ArgumentNullException()
        {
            await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync(null, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task AccountsPayableInvoicesServiceTests_PUT_DtoIdNull_ArgumentNullException()
        {
            await AccountsPayableInvoicesService.PutAccountsPayableInvoicesAsync("", new Dtos.AccountsPayableInvoices() { });
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task AccountsPayableInvoicesServiceTests_POST_DtoNull_ArgumentNullException()
        {
            await AccountsPayableInvoicesService.PostAccountsPayableInvoicesAsync(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task AccountsPayableInvoicesServiceTests_POST_DtoIdNull_ArgumentNullException()
        {
            await AccountsPayableInvoicesService.PostAccountsPayableInvoicesAsync(new Dtos.AccountsPayableInvoices() { });
        }

        [TestMethod]
        [ExpectedException(typeof(PermissionsException))]
        public async Task AccountsPayableInvoicesServiceTests_GetAccountsPayableInvoicesByGuidAsync_NoApPermission()
        {
            string voucherId = "1";
            var voucherDomainEntity = await testVoucherRepository.GetVoucherAsync(voucherId, "00000001", GlAccessLevel.Full_Access, null, versionNumber);

            UserFactoryAll TestcurrentUserFactory = new GeneralLedgerCurrentUser.UserFactoryAll();
            var adapterRegistry = new Mock<IAdapterRegistry>();
            var loggerObject = new Mock<ILogger>().Object;

            AccountsPayableInvoicesService = new AccountsPayableInvoicesService(mockcolleagueFinanceReferenceDataRepository.Object,
                mockreferenceDataRepository.Object, mockAccountsPayableInvoices.Object, mockaddressRepository.Object, mockvendorsRepository.Object,
                mockGeneralLedgerConfigurationRepository.Object,  mockPersonRepository.Object, baseConfigurationRepository,
                adapterRegistry.Object, TestcurrentUserFactory, roleRepositoryMock.Object, mockAccountFundsAvailable.Object, loggerObject);

            AccountsPayableInvoicesEntity = ConvertVoucherEntityToAPI(voucherDomainEntity);
            mockAccountsPayableInvoices.Setup(repo => repo.GetAccountsPayableInvoicesByGuidAsync(It.IsAny<string>(),It.IsAny<bool>())).ReturnsAsync(AccountsPayableInvoicesEntity);
            mockvendorsRepository.Setup(repo => repo.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("VendorIDGuid");
            mockaddressRepository.Setup(repo => repo.GetAddressGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("AddressGuid");

            var actual = await AccountsPayableInvoicesService.GetAccountsPayableInvoicesByGuidAsync(guid);
            
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task AccountsPayableInvoicesServiceTests_GetAccountsPayableInvoicesByGuidAsync_NoKeyException()
        {
            string voucherId = "1";
            var voucherDomainEntity = await testVoucherRepository.GetVoucherAsync(voucherId, "00000001", GlAccessLevel.Full_Access, null, versionNumber);
            
            AccountsPayableInvoicesEntity = ConvertVoucherEntityToAPI(voucherDomainEntity);
            mockAccountsPayableInvoices.Setup(repo => repo.GetAccountsPayableInvoicesByGuidAsync(It.IsAny<string>(), It.IsAny<bool>())).ThrowsAsync(new KeyNotFoundException());

            var actual = await AccountsPayableInvoicesService.GetAccountsPayableInvoicesByGuidAsync(guid);

        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task AccountsPayableInvoicesServiceTests_GetAccountsPayableInvoicesByGuidAsync_NullGuid()
        {
            string voucherId = "1";
            var voucherDomainEntity = await testVoucherRepository.GetVoucherAsync(voucherId, "00000001", GlAccessLevel.Full_Access, null, versionNumber);

            AccountsPayableInvoicesEntity = ConvertVoucherEntityToAPI(voucherDomainEntity);
            mockAccountsPayableInvoices.Setup(repo => repo.GetAccountsPayableInvoicesByGuidAsync(It.IsAny<string>(), It.IsAny<bool>())).ThrowsAsync(new RepositoryException()); ;

            var actual = await AccountsPayableInvoicesService.GetAccountsPayableInvoicesByGuidAsync(null);

        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public async Task AccountsPayableInvoicesServiceTests_GetAccountsPayableInvoicesByGuidAsync_InvalidOperExcept()
        {
            string voucherId = "1";
            var voucherDomainEntity = await testVoucherRepository.GetVoucherAsync(voucherId, "00000001", GlAccessLevel.Full_Access, null, versionNumber);

            AccountsPayableInvoicesEntity = ConvertVoucherEntityToAPI(voucherDomainEntity);
            mockAccountsPayableInvoices.Setup(repo => repo.GetAccountsPayableInvoicesByGuidAsync(It.IsAny<string>(), It.IsAny<bool>())).ThrowsAsync(new InvalidOperationException()); ;

            var actual = await AccountsPayableInvoicesService.GetAccountsPayableInvoicesByGuidAsync(guid);

        }

        [TestMethod]
        [ExpectedException(typeof(RepositoryException))]
        public async Task AccountsPayableInvoicesServiceTests_GetAccountsPayableInvoicesByGuidAsync_RepoException()
        {
            string voucherId = "1";
            var voucherDomainEntity = await testVoucherRepository.GetVoucherAsync(voucherId, "00000001", GlAccessLevel.Full_Access, null, versionNumber);

            AccountsPayableInvoicesEntity = ConvertVoucherEntityToAPI(voucherDomainEntity);
            mockAccountsPayableInvoices.Setup(repo => repo.GetAccountsPayableInvoicesByGuidAsync(It.IsAny<string>(), It.IsAny<bool>())).ThrowsAsync(new RepositoryException()); ;

            var actual = await AccountsPayableInvoicesService.GetAccountsPayableInvoicesByGuidAsync(guid);

        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task AccountsPayableInvoicesServiceTests_GetAccountsPayableInvoicesByGuidAsync_Exception()
        {
            string voucherId = "1";
            var voucherDomainEntity = await testVoucherRepository.GetVoucherAsync(voucherId, "00000001", GlAccessLevel.Full_Access, null, versionNumber);

            AccountsPayableInvoicesEntity = ConvertVoucherEntityToAPI(voucherDomainEntity);
            mockAccountsPayableInvoices.Setup(repo => repo.GetAccountsPayableInvoicesByGuidAsync(It.IsAny<string>(), It.IsAny<bool>())).ThrowsAsync(new Exception()); ;

            var actual = await AccountsPayableInvoicesService.GetAccountsPayableInvoicesByGuidAsync(guid);

        }


        private async void BuildValidVoucherService()
        { 
            var loggerObject = new Mock<ILogger>().Object;

            testVoucherRepository = new TestVoucherRepository();
            mockcolleagueFinanceReferenceDataRepository = new Mock<IColleagueFinanceReferenceDataRepository>();
            mockreferenceDataRepository = new Mock<IReferenceDataRepository>();
            mockAccountsPayableInvoices = new Mock<IAccountsPayableInvoicesRepository>();
            mockvendorsRepository = new Mock<IVendorsRepository>();
            mockaddressRepository = new Mock<IAddressRepository>();
            mockPersonRepository = new Mock<IPersonRepository>();
            mockGeneralLedgerConfigurationRepository = new Mock<IGeneralLedgerConfigurationRepository>();
            roleRepositoryMock = new Mock<IRoleRepository>();
            mockAccountFundsAvailable = new Mock<IAccountFundsAvailableRepository>();

            // Set up and mock the adapter, and setup the GetAdapter method.
            var adapterRegistry = new Mock<IAdapterRegistry>();

            viewAccountsPayableInvoicesRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewApInvoices));
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewAccountsPayableInvoicesRole });

            var glAcctStructure = await new TestGeneralLedgerConfigurationRepository().GetAccountStructureAsync();
            glAcctStructure.CheckAvailableFunds = "Y";
            mockGeneralLedgerConfigurationRepository.Setup(repo => repo.GetAccountStructureAsync()).ReturnsAsync(glAcctStructure);

            var checkFund = new AccountFundsAvailable()
            {
                AccountingStringValue = "test",
                BalanceOn = new DateTime(2017, 01, 15),
                FundsAvailable = Dtos.EnumProperties.FundsAvailable.Available
            };

            //mockAccountFundsAvailable.Setup(x => x.GetAccountFundsAvailableByFilterCriteriaAsync(It.IsAny<string>(), It.IsAny<decimal>(),
            //    It.IsAny<DateTime>(), It.IsAny<string>())).ReturnsAsync(checkFund);

            mockAccountFundsAvailable.Setup(x => x.CheckAvailableFundsAsync(It.IsAny<List<Domain.ColleagueFinance.Entities.FundsAvailable>>(),
                It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(null);

            mockPersonRepository.Setup(repo => repo.GetPersonGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("ebf585ad-cc1f-478f-a7a3-aefae87f873a");
               
            AccountsPayableInvoicesService = new AccountsPayableInvoicesService(mockcolleagueFinanceReferenceDataRepository.Object, 
                mockreferenceDataRepository.Object, mockAccountsPayableInvoices.Object, mockaddressRepository.Object, mockvendorsRepository.Object,
                mockGeneralLedgerConfigurationRepository.Object, mockPersonRepository.Object, baseConfigurationRepository,
                adapterRegistry.Object, currentUserFactory, roleRepositoryMock.Object, mockAccountFundsAvailable.Object, loggerObject);
            
        }
        private void BuildDto()
        {
            _accountsPayableInvoiceDto = new Ellucian.Colleague.Dtos.AccountsPayableInvoices()
            {
                Id = guid,
                Vendor = new GuidObject2("0123VendorGuid"),
                VendorAddress = new GuidObject2("02344AddressGuid"),
                ReferenceNumber = "refNo012",
                VendorInvoiceNumber = "VIN021",
                TransactionDate = new DateTime(2017, 1, 12),
                VendorInvoiceDate = new DateTime(2017, 1, 12),
                VoidDate = new DateTime(2017, 1, 25),
                ProcessState = Dtos.EnumProperties.AccountsPayableInvoicesProcessState.Inprogress,
                VendorBilledAmount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 40m, Currency = Dtos.EnumProperties.CurrencyIsoCode.USD },
                InvoiceDiscountAmount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 5m, Currency = Dtos.EnumProperties.CurrencyIsoCode.USD },
                Taxes = new List<Dtos.DtoProperties.AccountsPayableInvoicesTaxesDtoProperty>()
                {
                    new Dtos.DtoProperties.AccountsPayableInvoicesTaxesDtoProperty()
                    {
                        TaxCode = new GuidObject2("TaxCodeGuid"),
                        VendorAmount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 1m, Currency = Dtos.EnumProperties.CurrencyIsoCode.USD }
                    }
                },
                InvoiceType = Dtos.EnumProperties.AccountsPayableInvoicesInvoiceType.Invoice,
                Payment = new Dtos.DtoProperties.AccountsPayableInvoicesPaymentDtoProperty()
                {
                    Source = new GuidObject2("aptypeguid321"),
                    PaymentDueOn = new DateTime(2017, 1, 17),
                    PaymentTerms = new GuidObject2("termsguid321")
                },
                InvoiceComment = "This is a Comment 321",
                GovernmentReporting = new List<Dtos.DtoProperties.GovernmentReportingDtoProperty>()
                {
                    new Dtos.DtoProperties.GovernmentReportingDtoProperty()
                    {
                        Code = CountryCodeType.USA,
                        TransactionType = Dtos.EnumProperties.AccountsPayableInvoicesTransactionType.NotSet
                    }
                },
                LineItems = new List<Dtos.DtoProperties.AccountsPayableInvoicesLineItemDtoProperty>()
                {
                    new Dtos.DtoProperties.AccountsPayableInvoicesLineItemDtoProperty()
                    {
                         AccountDetails =new List<Dtos.DtoProperties.AccountsPayableInvoicesAccountDetailDtoProperty>()
                         {
                             new Dtos.DtoProperties.AccountsPayableInvoicesAccountDetailDtoProperty()
                             {
                                  AccountingString = "10-10-1000-400",
                                   Allocation = new Dtos.DtoProperties.AccountsPayableInvoicesAllocationDtoProperty()
                                   {
                                        Allocated = new Dtos.DtoProperties.AccountsPayableInvoicesAllocatedDtoProperty()
                                        {
                                             Amount = new Dtos.DtoProperties.Amount2DtoProperty() {
                                                 Value = 10m,
                                                 Currency = Dtos.EnumProperties.CurrencyIsoCode.USD
                                             },
                                              Percentage = 100m,
                                              Quantity = 2m
                                        }
                                   },
                                   BudgetCheck = Dtos.EnumProperties.AccountsPayableInvoicesAccountBudgetCheck.NotRequired,
                                   SequenceNumber = 1,
                                   Source = new GuidObject2( "asbc-321"),
                                   SubmittedBy = new GuidObject2("submit-guid"),
                                   
                             }
                         },
                           Comment = "LineItem comment",
                         Description = "line item Description",
                         CommodityCode = new GuidObject2("commodity-guid"),
                         Quantity = 2m,
                         UnitofMeasure = new GuidObject2("unitguid321"),
                         UnitPrice = new Dtos.DtoProperties.Amount2DtoProperty() {
                            Value = 5m, Currency = Dtos.EnumProperties.CurrencyIsoCode.USD},
                         Taxes = new List<Dtos.DtoProperties.AccountsPayableInvoicesTaxesDtoProperty>()
                         {
                             new Dtos.DtoProperties.AccountsPayableInvoicesTaxesDtoProperty()
                             {
                                  TaxCode = new GuidObject2("taxCode-guid"),
                                  VendorAmount = new Dtos.DtoProperties.Amount2DtoProperty()
                                  {
                                      Value = 1m,
                                      Currency = Dtos.EnumProperties.CurrencyIsoCode.USD
                                  }
                             }
                         },
                         Discount = new Dtos.DtoProperties.AccountsPayableInvoicesDiscountDtoProperty()
                         {
                             Amount = new Dtos.DtoProperties.Amount2DtoProperty()
                             {
                                 Value = 1m,
                                 Currency = Dtos.EnumProperties.CurrencyIsoCode.USD
                             },
                              Percent = 1m
                         }
                        , PaymentStatus = Dtos.EnumProperties.AccountsPayableInvoicesPaymentStatus.Nohold
                        
                    }
                }
            };
        }

        private Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableInvoices ConvertVoucherEntityToAPI(Voucher voucher)
        {
            Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableInvoices NewApi = new Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableInvoices(voucher.Id, voucher.Date, voucher.Status, voucher.VendorName, voucher.InvoiceNumber, voucher.InvoiceDate);
            NewApi.Guid = guid;
            NewApi.Amount = voucher.Amount;

            foreach (var appr in voucher.Approvers)
            {
                Approver approver = new Approver(appr.ApproverId);
                approver.ApprovalDate = appr.ApprovalDate;
                approver.SetApprovalName(appr.ApprovalName);
                NewApi.AddApprover(approver);
            }
            NewApi.ApType = voucher.ApType;
            NewApi.CheckDate = voucher.CheckDate;
            NewApi.CheckNumber = (string.IsNullOrEmpty(voucher.CheckNumber) ? null : voucher.CheckNumber);
            NewApi.Comments = voucher.Comments;
            NewApi.CurrencyCode = voucher.CurrencyCode;
            NewApi.DueDate = voucher.DueDate;
            NewApi.MaintenanceDate = voucher.MaintenanceDate;
            NewApi.VendorAddressId = "00001";
            NewApi.VendorId = voucher.VendorId;
            NewApi.VoucherAddressId = "000002";
            NewApi.VoucherInvoiceAmt = 20m;
            NewApi.VoucherDiscAmt =10m;
            NewApi.VoucherNet = 20m;
            NewApi.VoucherPayFlag = "Y";
            NewApi.VoucherReferenceNo = new List<string>() { "RefNo1111" };
            NewApi.VoucherStatusDate = new DateTime(2017, 1, 11);
            NewApi.VoucherVendorTerms = "testTerm";
            NewApi.VoucherVoidGlTranDate = new DateTime(2018, 1, 11);
            NewApi.VoucherTaxes = new List<LineItemTax>() { new LineItemTax("ST", 3m) };
            NewApi.VoucherVendorTerms = "02";

            foreach(var lineItem in voucher.LineItems)
            {
                AccountsPayableInvoicesLineItem newLi = new AccountsPayableInvoicesLineItem(lineItem.Id,lineItem.Description, lineItem.Quantity, lineItem.Price, lineItem.ExtendedPrice);
                newLi.CommodityCode = "00402";
                newLi.UnitOfIssue = lineItem.UnitOfIssue;
                foreach (var tax in lineItem.LineItemTaxes)
                {
                    newLi.AddTax(tax);
                }
                foreach (var gl in lineItem.GlDistributions)
                {
                    newLi.AddGlDistribution(gl);
                }
                newLi.CashDiscountAmount = 0m;
                newLi.TradeDiscountAmount = 10m;
                newLi.TradeDiscountPercent = 1m;
                newLi.Comments = lineItem.Comments;
                NewApi.AddAccountsPayableInvoicesLineItem(newLi);
            }

            return NewApi;
        }
    }
}