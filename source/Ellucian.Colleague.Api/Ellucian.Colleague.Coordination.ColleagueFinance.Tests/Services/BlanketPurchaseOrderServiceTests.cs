﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Colleague.Coordination.ColleagueFinance.Tests.UserFactories;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Colleague.Domain.ColleagueFinance.Tests;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Tests.Services
{
    /// <summary>
    /// This class tests that the service returns a specified blanket purchase order.
    ///  We use GeneralLedgerCurrentUser to mimic the user logged in.
    /// </summary>
    [TestClass]
    public class BlanketPurchaseOrderServiceTests : GeneralLedgerCurrentUser
    {
        #region Initialize and Cleanup
        private BlanketPurchaseOrderService service = null;
        private BlanketPurchaseOrderService service2 = null;
        private TestBlanketPurchaseOrderRepository testBlanketPurchaseOrderRepository;
        private TestGeneralLedgerConfigurationRepository testGeneralLedgerConfigurationRepository;
        private TestGeneralLedgerUserRepository testGeneralLedgerUserRepository;
        private UserFactorySubset currentUserFactory = new GeneralLedgerCurrentUser.UserFactorySubset();
        private Mock<IBlanketPurchaseOrderRepository> mockBlanketPurchaseOrderRepository;

        [TestInitialize]
        public void Initialize()
        {
            // Set up the mock PO repository
            this.mockBlanketPurchaseOrderRepository = new Mock<IBlanketPurchaseOrderRepository>();

            // Build all service objects to use each of the user factories built above
            BuildValidBlanketPurchaseOrderService();
        }

        [TestCleanup]
        public void Cleanup()
        {
            // Reset all of the services and repository variables.
            service = null;
            service2 = null;
            testBlanketPurchaseOrderRepository = null;
            testGeneralLedgerConfigurationRepository = null;
            testGeneralLedgerUserRepository = null;
            this.mockBlanketPurchaseOrderRepository = null;
        }
        #endregion

        #region Tests for GetBlanketPurchaseOrder
        [TestMethod]
        public async Task GetBlanketPurchaseOrder()
        {
            // Get a specified blanket purchase order
            var blanketPurchaseOrderId = "1";
            var personId = "1";
            var blanketPurchaseOrderDto = await service.GetBlanketPurchaseOrderAsync(blanketPurchaseOrderId);

            // Get the blanket purchase order domain entity from the test repository
            var blanketPurchaseOrderDomainEntity = await testBlanketPurchaseOrderRepository.GetBlanketPurchaseOrderAsync(blanketPurchaseOrderId, personId, GlAccessLevel.Full_Access, null);

            // Confirm that the data in the vouchers DTO matches the domain entity
            Assert.AreEqual(blanketPurchaseOrderDto.Id, blanketPurchaseOrderDomainEntity.Id);
            Assert.AreEqual(blanketPurchaseOrderDto.Number, blanketPurchaseOrderDomainEntity.Number);
            Assert.AreEqual(blanketPurchaseOrderDto.Amount, blanketPurchaseOrderDomainEntity.Amount);
            Assert.AreEqual(blanketPurchaseOrderDto.ApType, blanketPurchaseOrderDomainEntity.ApType);
            Assert.AreEqual(blanketPurchaseOrderDto.Comments, blanketPurchaseOrderDomainEntity.Comments);
            Assert.AreEqual(blanketPurchaseOrderDto.CurrencyCode, blanketPurchaseOrderDomainEntity.CurrencyCode);
            Assert.AreEqual(blanketPurchaseOrderDto.Date, blanketPurchaseOrderDomainEntity.Date);
            Assert.AreEqual(blanketPurchaseOrderDto.ExpirationDate, blanketPurchaseOrderDomainEntity.ExpirationDate);
            Assert.AreEqual(blanketPurchaseOrderDto.InitiatorName, blanketPurchaseOrderDomainEntity.InitiatorName);
            Assert.AreEqual(blanketPurchaseOrderDto.InternalComments, blanketPurchaseOrderDomainEntity.InternalComments);
            Assert.AreEqual(blanketPurchaseOrderDto.MaintenanceDate, blanketPurchaseOrderDomainEntity.MaintenanceDate);
            Assert.AreEqual(blanketPurchaseOrderDto.Status.ToString(), blanketPurchaseOrderDomainEntity.Status.ToString());
            Assert.AreEqual(blanketPurchaseOrderDto.StatusDate, blanketPurchaseOrderDomainEntity.StatusDate);
            Assert.AreEqual(blanketPurchaseOrderDto.VendorId, blanketPurchaseOrderDomainEntity.VendorId);
            Assert.AreEqual(blanketPurchaseOrderDto.VendorName, blanketPurchaseOrderDomainEntity.VendorName);

            // Confirm that the data in the approvers DTOs matches the domain entity
            for (int i = 0; i < blanketPurchaseOrderDto.Approvers.Count(); i++)
            {
                var approverDto = blanketPurchaseOrderDto.Approvers[i];
                var approverDomain = blanketPurchaseOrderDto.Approvers[i];
                Assert.AreEqual(approverDto.ApprovalName, approverDomain.ApprovalName);
                Assert.AreEqual(approverDto.ApprovalDate, approverDomain.ApprovalDate);
            }

            // Confirm that the data in the list of requisition DTOs matches the domain entity
            for (int i = 0; i < blanketPurchaseOrderDto.Requisitions.Count(); i++)
            {
                Assert.AreEqual(blanketPurchaseOrderDto.Requisitions[i], blanketPurchaseOrderDomainEntity.Requisitions[i]);
            }

            // Confirm that the data in the list of voucher DTOs matches the domain entity
            for (int i = 0; i < blanketPurchaseOrderDto.Vouchers.Count(); i++)
            {
                Assert.AreEqual(blanketPurchaseOrderDto.Vouchers[i], blanketPurchaseOrderDomainEntity.Vouchers[i]);
            }

            // Confirm that the data in the GL distribution DTOs matches the domain entity
            for (int i = 0; i < blanketPurchaseOrderDto.GlDistributions.Count(); i++)
            {
                var glDistributionDto = blanketPurchaseOrderDto.GlDistributions[i];
                var glDistributionDomain = blanketPurchaseOrderDomainEntity.GlDistributions[i];
                Assert.AreEqual(glDistributionDto.Description, glDistributionDomain.GlAccountDescription);
                Assert.AreEqual(glDistributionDto.EncumberedAmount, glDistributionDomain.EncumberedAmount);
                Assert.AreEqual(glDistributionDto.ExpensedAmount, glDistributionDomain.ExpensedAmount);
                Assert.AreEqual(glDistributionDto.GlAccount, glDistributionDomain.GlAccountNumber);
                Assert.AreEqual(glDistributionDto.ProjectLineItemCode, glDistributionDomain.ProjectLineItemCode);
                Assert.AreEqual(glDistributionDto.ProjectNumber, glDistributionDomain.ProjectNumber);
            }
        }

        [TestMethod]
        public async Task GetBlanketPurchaseOrder_BpoHasRequisitions()
        {
            var bpoId = "1";
            var personId = "1";
            var bpoDto = await service.GetBlanketPurchaseOrderAsync(bpoId);

            // Get the blanket purchase order domain entiy from the test repository
            var bpoDomainEntity = await testBlanketPurchaseOrderRepository.GetBlanketPurchaseOrderAsync(bpoId, personId, GlAccessLevel.Full_Access, null);

            // Confirm that the data in the list of requisition DTOs matches the domain entity
            for (int i = 0; i < bpoDto.Requisitions.Count(); i++)
            {
                Assert.AreEqual(bpoDto.Requisitions[i], bpoDomainEntity.Requisitions[i]);
            }
        }

        [TestMethod]
        public async Task GetBlanketPurchaseOrder_StatusClosed()
        {
            var bpoId = "5";
            var personId = "1";
            var bpoDto = await service.GetBlanketPurchaseOrderAsync(bpoId);

            // Get the blanket purchase order domain entiy from the test repository
            var bpoDomainEntity = await testBlanketPurchaseOrderRepository.GetBlanketPurchaseOrderAsync(bpoId, personId, GlAccessLevel.Full_Access, null);

            Assert.AreEqual(bpoDomainEntity.Status.ToString(), bpoDto.Status.ToString(), "Status must be the same.");
        }

        [TestMethod]
        public async Task GetBlanketPurchaseOrder_StatusInProgress()
        {
            var bpoId = "3";
            var personId = "1";
            var bpoDto = await service.GetBlanketPurchaseOrderAsync(bpoId);

            // Get the blanket purchase order domain entiy from the test repository
            var bpoDomainEntity = await testBlanketPurchaseOrderRepository.GetBlanketPurchaseOrderAsync(bpoId, personId, GlAccessLevel.Full_Access, null);

            Assert.AreEqual(bpoDomainEntity.Status.ToString(), bpoDto.Status.ToString(), "Status must be the same.");
        }

        [TestMethod]
        public async Task GetBlanketPurchaseOrder_StatusNotApproved()
        {
            var bpoId = "6";
            var personId = "1";
            var bpoDto = await service.GetBlanketPurchaseOrderAsync(bpoId);

            // Get the blanket purchase order domain entiy from the test repository
            var bpoDomainEntity = await testBlanketPurchaseOrderRepository.GetBlanketPurchaseOrderAsync(bpoId, personId, GlAccessLevel.Full_Access, null);

            Assert.AreEqual(bpoDomainEntity.Status.ToString(), bpoDto.Status.ToString(), "Status must be the same.");
        }

        [TestMethod]
        public async Task GetBlanketPurchaseOrder_StatusOutstanding()
        {
            var bpoId = "4";
            var personId = "1";
            var bpoDto = await service.GetBlanketPurchaseOrderAsync(bpoId);

            // Get the blanket purchase order domain entiy from the test repository
            var bpoDomainEntity = await testBlanketPurchaseOrderRepository.GetBlanketPurchaseOrderAsync(bpoId, personId, GlAccessLevel.Full_Access, null);

            Assert.AreEqual(bpoDomainEntity.Status.ToString(), bpoDto.Status.ToString(), "Status must be the same.");
        }

        [TestMethod]
        public async Task GetBlanketPurchaseOrder_StatusVoided()
        {
            var bpoId = "7";
            var personId = "1";
            var bpoDto = await service.GetBlanketPurchaseOrderAsync(bpoId);

            // Get the blanket purchase order domain entiy from the test repository
            var bpoDomainEntity = await testBlanketPurchaseOrderRepository.GetBlanketPurchaseOrderAsync(bpoId, personId, GlAccessLevel.Full_Access, null);

            Assert.AreEqual(bpoDomainEntity.Status.ToString(), bpoDto.Status.ToString(), "Status must be the same.");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task GetBlanketPurchaseOrder_RepositoryReturnsNullObject()
        {
            // Mock the GetBlanketPurchaseOrder repository method to return a null object within the Service method
            BlanketPurchaseOrder nullBlanketPurchaseOrder = null;
            this.mockBlanketPurchaseOrderRepository.Setup<Task<BlanketPurchaseOrder>>(bpoRepo => bpoRepo.GetBlanketPurchaseOrderAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>(), It.IsAny<List<string>>())).Returns(Task.FromResult(nullBlanketPurchaseOrder));
            var blanketPurchaseOrderDto = await service2.GetBlanketPurchaseOrderAsync("1");
        }
        #endregion

        #region Build service method
        /// <summary>
        /// Builds multiple blanket purchase order service objects
        /// </summary>
        /// <returns>Nothing.</returns>
        private void BuildValidBlanketPurchaseOrderService()
        {
            // We need the unit tests to be independent of "real" implementations of these classes,
            // so we use Moq to create mock implementations that are based on the same interfaces
            var roleRepository = new Mock<IRoleRepository>().Object;
            var loggerObject = new Mock<ILogger>().Object;

            testBlanketPurchaseOrderRepository = new TestBlanketPurchaseOrderRepository();
            testGeneralLedgerConfigurationRepository = new TestGeneralLedgerConfigurationRepository();
            testGeneralLedgerUserRepository = new TestGeneralLedgerUserRepository();

            // Set up and mock the adapter, and setup the GetAdapter method.
            var adapterRegistry = new Mock<IAdapterRegistry>();
            var blanketPurchaseOrderDtoAdapter = new AutoMapperAdapter<Domain.ColleagueFinance.Entities.BlanketPurchaseOrder, Dtos.ColleagueFinance.BlanketPurchaseOrder>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Domain.ColleagueFinance.Entities.BlanketPurchaseOrder, Dtos.ColleagueFinance.BlanketPurchaseOrder>()).Returns(blanketPurchaseOrderDtoAdapter);

            // Set up the current user with a subset of projects and set up the service.
            service = new BlanketPurchaseOrderService(testBlanketPurchaseOrderRepository, testGeneralLedgerConfigurationRepository, testGeneralLedgerUserRepository, adapterRegistry.Object, currentUserFactory, roleRepository, loggerObject);
            service2 = new BlanketPurchaseOrderService(this.mockBlanketPurchaseOrderRepository.Object, testGeneralLedgerConfigurationRepository, testGeneralLedgerUserRepository, adapterRegistry.Object, currentUserFactory, roleRepository, loggerObject);
        }
        #endregion
    }
}
