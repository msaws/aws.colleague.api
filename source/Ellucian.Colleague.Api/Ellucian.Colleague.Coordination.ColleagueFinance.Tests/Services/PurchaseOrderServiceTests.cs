﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Colleague.Coordination.ColleagueFinance.Tests.UserFactories;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Colleague.Domain.ColleagueFinance.Tests;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using Ellucian.Colleague.Domain.Base.Repositories;
using System.Collections.ObjectModel;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.ColleagueFinance;
using Ellucian.Colleague.Domain.Exceptions;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Tests.Services
{
    /// <summary>
    /// Test that the service returns a valid purchase order.
    /// We use GeneralLedgerCurrentUser to mimic the user logged in.
    /// </summary>
    [TestClass]
    public class PurchaseOrderServiceTests : GeneralLedgerCurrentUser
    {
        #region Initialize and Cleanup

        private PurchaseOrderService service = null;
        private PurchaseOrderService service2 = null;
        private TestPurchaseOrderRepository testPurchaseOrderRepository;
        private TestGeneralLedgerConfigurationRepository testGeneralLedgerConfigurationRepository;
        private TestGeneralLedgerUserRepository testGeneralLedgerUserRepository;
        private UserFactorySubset currentUserFactory = new GeneralLedgerCurrentUser.UserFactorySubset();
        private Mock<IPurchaseOrderRepository> mockPurchaseOrderRepository;
        private Mock<IPersonRepository> mockPersonRepository;
        private Mock<IAccountFundsAvailableRepository> mockAccountFundAvailableRepo;
        private IConfigurationRepository baseConfigurationRepository;
        private Mock<IConfigurationRepository> baseConfigurationRepositoryMock;

        [TestInitialize]
        public void Initialize()
        {
            // Set up the mock PO repository
            this.mockPurchaseOrderRepository = new Mock<IPurchaseOrderRepository>();

            // build all service objects to use in testing
            BuildValidPurchaseOrderService();
        }

        [TestCleanup]
        public void Cleanup()
        {
            // Reset all of the services and repository variables.
            service = null;
            service2 = null;
            testPurchaseOrderRepository = null;
            testGeneralLedgerConfigurationRepository = null;
            testGeneralLedgerUserRepository = null;
            this.mockPurchaseOrderRepository = null;
        }
        #endregion

        #region Tests for GetPurchaseOrder
        [TestMethod]
        public async Task GetPurchaseOrder()
        {
            var purchaseOrderId = "1";
            var personId = "1";
            var purchaseOrderDto = await service.GetPurchaseOrderAsync(purchaseOrderId);

            // Get the purchase order domain entity from the test repository
            var purchaseOrderDomainEntity = await testPurchaseOrderRepository.GetPurchaseOrderAsync(purchaseOrderId, personId, GlAccessLevel.Full_Access, null);

            // Confirm that the data in the DTO matches the domain entity
            Assert.AreEqual(purchaseOrderDto.Id, purchaseOrderDomainEntity.Id);
            Assert.AreEqual(purchaseOrderDto.Number, purchaseOrderDomainEntity.Number);
            Assert.AreEqual(purchaseOrderDto.Amount, purchaseOrderDomainEntity.Amount);
            Assert.AreEqual(purchaseOrderDto.ApType, purchaseOrderDomainEntity.ApType);
            Assert.AreEqual(purchaseOrderDto.Comments, purchaseOrderDomainEntity.Comments);
            Assert.AreEqual(purchaseOrderDto.CurrencyCode, purchaseOrderDomainEntity.CurrencyCode);
            Assert.AreEqual(purchaseOrderDto.Date, purchaseOrderDomainEntity.Date);
            Assert.AreEqual(purchaseOrderDto.DeliveryDate, purchaseOrderDomainEntity.DeliveryDate);
            Assert.AreEqual(purchaseOrderDto.InitiatorName, purchaseOrderDomainEntity.InitiatorName);
            Assert.AreEqual(purchaseOrderDto.InternalComments, purchaseOrderDomainEntity.InternalComments);
            Assert.AreEqual(purchaseOrderDto.MaintenanceDate, purchaseOrderDomainEntity.MaintenanceDate);
            Assert.AreEqual(purchaseOrderDto.RequestorName, purchaseOrderDomainEntity.RequestorName);
            Assert.AreEqual(purchaseOrderDto.ShipToCodeName, purchaseOrderDomainEntity.ShipToCodeName);
            Assert.AreEqual(purchaseOrderDto.Status.ToString(), purchaseOrderDomainEntity.Status.ToString());
            Assert.AreEqual(purchaseOrderDto.StatusDate, purchaseOrderDomainEntity.StatusDate);
            Assert.AreEqual(purchaseOrderDto.VendorId, purchaseOrderDomainEntity.VendorId);
            Assert.AreEqual(purchaseOrderDto.VendorName, purchaseOrderDomainEntity.VendorName);

            // Confirm that the data in the approvers DTOs matches the domain entity
            for (int i = 0; i < purchaseOrderDto.Approvers.Count(); i++)
            {
                var approverDto = purchaseOrderDto.Approvers[i];
                var approverDomain = purchaseOrderDomainEntity.Approvers[i];
                Assert.AreEqual(approverDto.ApprovalName, approverDomain.ApprovalName);
                Assert.AreEqual(approverDto.ApprovalDate, approverDomain.ApprovalDate);
            }

            // Confirm that the data in the list of requisition DTOs matches the domain entity
            for (int i = 0; i < purchaseOrderDto.Requisitions.Count(); i++)
            {
                Assert.AreEqual(purchaseOrderDto.Requisitions[i], purchaseOrderDomainEntity.Requisitions[i]);
            }

            // Confirm that the data in the list of voucher DTOs matches the domain entity
            for (int i = 0; i < purchaseOrderDto.Vouchers.Count(); i++)
            {
                Assert.AreEqual(purchaseOrderDto.Vouchers[i], purchaseOrderDomainEntity.Vouchers[i]);
            }

            // Confirm that the data in the line item DTOs matches the domain entity
            for (int i = 0; i < purchaseOrderDto.LineItems.Count(); i++)
            {
                var lineItemDto = purchaseOrderDto.LineItems[i];
                var lineItemDomain = purchaseOrderDomainEntity.LineItems[i];
                Assert.AreEqual(lineItemDto.Comments, lineItemDomain.Comments);
                Assert.AreEqual(lineItemDto.Description, lineItemDomain.Description);
                Assert.AreEqual(lineItemDto.ExpectedDeliveryDate, lineItemDomain.ExpectedDeliveryDate);
                Assert.AreEqual(lineItemDto.ExtendedPrice, lineItemDomain.ExtendedPrice);
                Assert.AreEqual(lineItemDto.Price, lineItemDomain.Price);
                Assert.AreEqual(lineItemDto.Quantity, lineItemDomain.Quantity);
                Assert.AreEqual(lineItemDto.TaxForm, lineItemDomain.TaxForm);
                Assert.AreEqual(lineItemDto.TaxFormCode, lineItemDomain.TaxFormCode);
                Assert.AreEqual(lineItemDto.TaxFormLocation, lineItemDomain.TaxFormLocation);
                Assert.AreEqual(lineItemDto.UnitOfIssue, lineItemDomain.UnitOfIssue);
                Assert.AreEqual(lineItemDto.VendorPart, lineItemDomain.VendorPart);

                // Confirm that the data in the line item GL distribution DTOs matches the domain entity
                for (int j = 0; j < lineItemDto.GlDistributions.Count(); j++)
                {
                    var glDistributionDto = lineItemDto.GlDistributions[j];
                    var glDistributionDomain = lineItemDomain.GlDistributions[j];
                    Assert.AreEqual(glDistributionDto.Amount, glDistributionDomain.Amount);
                    Assert.AreEqual(glDistributionDto.GlAccount, glDistributionDomain.GlAccountNumber);
                    Assert.AreEqual(glDistributionDto.ProjectLineItemCode, glDistributionDomain.ProjectLineItemCode);
                    Assert.AreEqual(glDistributionDto.ProjectNumber, glDistributionDomain.ProjectNumber);
                    Assert.AreEqual(glDistributionDto.Quantity, glDistributionDomain.Quantity);
                }

                // Confirm that the data in the line item tax DTOs matches the domain entity
                for (int k = 0; k < lineItemDto.LineItemTaxes.Count(); k++)
                {
                    var lineItemTaxDto = lineItemDto.LineItemTaxes[k];
                    var lineItemTaxDomain = lineItemDomain.LineItemTaxes[k];
                    Assert.AreEqual(lineItemTaxDto.TaxCode, lineItemTaxDomain.TaxCode);
                    Assert.AreEqual(lineItemTaxDto.TaxAmount, lineItemTaxDomain.TaxAmount);
                }
            }
        }

        [TestMethod]
        public async Task GetPurchaseOrder_NoAccessToOneGlAccount()
        {
            var purchaseOrderId = "999";
            var personId = "1";
            var purchaseOrderDto = await service.GetPurchaseOrderAsync(purchaseOrderId);

            // Get the purchase order domain entity from the test repository
            var purchaseOrderDomainEntity = await testPurchaseOrderRepository.GetPurchaseOrderAsync(purchaseOrderId, personId, GlAccessLevel.Full_Access, null);

            // Confirm that the data in the DTO matches the domain entity
            Assert.AreEqual(purchaseOrderDto.Id, purchaseOrderDomainEntity.Id);

            // There should only be one line item.
            Assert.AreEqual(1, purchaseOrderDomainEntity.LineItems.Count);
            Assert.AreEqual(1, purchaseOrderDto.LineItems.Count);

            // Confirm that the DTO line item data matches the data in the domain entity.
            var lineItemDto = purchaseOrderDto.LineItems.First();
            var lineItemEntity = purchaseOrderDomainEntity.LineItems.First();

            Assert.AreEqual(lineItemDto.Comments, lineItemEntity.Comments);
            Assert.AreEqual(lineItemDto.Description, lineItemEntity.Description);
            Assert.AreEqual(lineItemDto.ExpectedDeliveryDate, lineItemEntity.ExpectedDeliveryDate);
            Assert.AreEqual(lineItemDto.ExtendedPrice, lineItemEntity.ExtendedPrice);
            Assert.AreEqual(lineItemDto.Price, lineItemEntity.Price);
            Assert.AreEqual(lineItemDto.Quantity, lineItemEntity.Quantity);
            Assert.AreEqual(lineItemDto.TaxForm, lineItemEntity.TaxForm);
            Assert.AreEqual(lineItemDto.TaxFormCode, lineItemEntity.TaxFormCode);
            Assert.AreEqual(lineItemDto.TaxFormLocation, lineItemEntity.TaxFormLocation);
            Assert.AreEqual(lineItemDto.UnitOfIssue, lineItemEntity.UnitOfIssue);
            Assert.AreEqual(lineItemDto.VendorPart, lineItemEntity.VendorPart);

            var glConfiguration = await testGeneralLedgerConfigurationRepository.GetAccountStructureAsync();

            // Confirm that the data in the line item GL distribution DTOs matches the domain entity
            foreach (var glDistributionEntity in lineItemEntity.GlDistributions)
            {
                var glDistributionDto = lineItemDto.GlDistributions.FirstOrDefault(x => x.GlAccount == glDistributionEntity.GlAccountNumber);

                // Check the values in each of the non-masked GL accounts.
                if (glDistributionDto != null)
                {
                    Assert.AreEqual(glDistributionDto.GlAccount, glDistributionEntity.GlAccountNumber);
                    Assert.AreEqual(glDistributionDto.FormattedGlAccount, glDistributionEntity.GetFormattedMaskedGlAccount(glConfiguration.MajorComponentStartPositions));
                    Assert.AreEqual(glDistributionDto.ProjectNumber, glDistributionEntity.ProjectNumber);
                    Assert.AreEqual(glDistributionDto.ProjectLineItemCode, glDistributionEntity.ProjectLineItemCode);
                    Assert.AreEqual(glDistributionDto.Quantity, glDistributionEntity.Quantity);
                    Assert.AreEqual(glDistributionDto.Amount, glDistributionEntity.Amount);
                }
            }

            // Get all of the masked GL account DTOs and confirm that their values are either 0 or null.
            var maskedGlDistributionEntity = lineItemEntity.GlDistributions.FirstOrDefault(x => x.Masked);
            var maskedGlAccountNumber = maskedGlDistributionEntity.GetFormattedMaskedGlAccount(glConfiguration.MajorComponentStartPositions);
            var maskedGlDistributionDtos = lineItemDto.GlDistributions.Where(x => x.FormattedGlAccount == maskedGlAccountNumber).ToList();

            foreach (var glDistributionDto in maskedGlDistributionDtos)
            {
                Assert.AreEqual(null, glDistributionDto.GlAccount);
                Assert.AreEqual(maskedGlAccountNumber, glDistributionDto.FormattedGlAccount);
                Assert.AreEqual(null, glDistributionDto.ProjectNumber);
                Assert.AreEqual(null, glDistributionDto.ProjectLineItemCode);
                Assert.AreEqual(0m, glDistributionDto.Quantity);
                Assert.AreEqual(0m, glDistributionDto.Amount);
            }
        }

        [TestMethod]
        public async Task GetPurchaseOrder_PoHasRequisitions()
        {
            var purchaseOrderId = "3";
            var personId = "1";
            var purchaseOrderDto = await service.GetPurchaseOrderAsync(purchaseOrderId);

            // Get the purchase order domain entiy from the test repository
            var purchaseOrderDomainEntity = await testPurchaseOrderRepository.GetPurchaseOrderAsync(purchaseOrderId, personId, GlAccessLevel.Full_Access, null);

            // Confirm that the data in the list of requisition DTOs matches the domain entity
            for (int i = 0; i < purchaseOrderDto.Requisitions.Count(); i++)
            {
                Assert.AreEqual(purchaseOrderDto.Requisitions[i], purchaseOrderDomainEntity.Requisitions[i]);
            }
        }

        [TestMethod]
        public async Task GetPurchaseOrder_StatusAccepted()
        {
            var purchaseOrderId = "7";
            var personId = "1";
            var purchaseOrderDto = await service.GetPurchaseOrderAsync(purchaseOrderId);

            // Get the purchase order domain entiy from the test repository
            var purchaseOrderDomainEntity = await testPurchaseOrderRepository.GetPurchaseOrderAsync(purchaseOrderId, personId, GlAccessLevel.Full_Access, null);

            Assert.AreEqual(purchaseOrderDomainEntity.Status.ToString(), purchaseOrderDto.Status.ToString(), "Status must be the same.");
        }

        [TestMethod]
        public async Task GetPurchaseOrder_StatusBackordered()
        {
            var purchaseOrderId = "6";
            var personId = "1";
            var purchaseOrderDto = await service.GetPurchaseOrderAsync(purchaseOrderId);

            // Get the purchase order domain entiy from the test repository
            var purchaseOrderDomainEntity = await testPurchaseOrderRepository.GetPurchaseOrderAsync(purchaseOrderId, personId, GlAccessLevel.Full_Access, null);

            Assert.AreEqual(purchaseOrderDomainEntity.Status.ToString(), purchaseOrderDto.Status.ToString(), "Status must be the same.");
        }

        [TestMethod]
        public async Task GetPurchaseOrder_StatusClosed()
        {
            var purchaseOrderId = "10";
            var personId = "1";
            var purchaseOrderDto = await service.GetPurchaseOrderAsync(purchaseOrderId);

            // Get the purchase order domain entiy from the test repository
            var purchaseOrderDomainEntity = await testPurchaseOrderRepository.GetPurchaseOrderAsync(purchaseOrderId, personId, GlAccessLevel.Full_Access, null);

            Assert.AreEqual(purchaseOrderDomainEntity.Status.ToString(), purchaseOrderDto.Status.ToString(), "Status must be the same.");
        }

        [TestMethod]
        public async Task GetPurchaseOrder_StatusInProgress()
        {
            var purchaseOrderId = "4";
            var personId = "1";
            var purchaseOrderDto = await service.GetPurchaseOrderAsync(purchaseOrderId);

            // Get the purchase order domain entiy from the test repository
            var purchaseOrderDomainEntity = await testPurchaseOrderRepository.GetPurchaseOrderAsync(purchaseOrderId, personId, GlAccessLevel.Full_Access, null);

            Assert.AreEqual(purchaseOrderDomainEntity.Status.ToString(), purchaseOrderDto.Status.ToString(), "Status must be the same.");
        }

        [TestMethod]
        public async Task GetPurchaseOrder_StatusNotApproved()
        {
            var purchaseOrderId = "2";
            var personId = "1";
            var purchaseOrderDto = await service.GetPurchaseOrderAsync(purchaseOrderId);

            // Get the purchase order domain entiy from the test repository
            var purchaseOrderDomainEntity = await testPurchaseOrderRepository.GetPurchaseOrderAsync(purchaseOrderId, personId, GlAccessLevel.Full_Access, null);

            Assert.AreEqual(purchaseOrderDomainEntity.Status.ToString(), purchaseOrderDto.Status.ToString(), "Status must be the same.");
        }

        [TestMethod]
        public async Task GetPurchaseOrder_StatusOutstanding()
        {
            var purchaseOrderId = "3";
            var personId = "1";
            var purchaseOrderDto = await service.GetPurchaseOrderAsync(purchaseOrderId);

            // Get the purchase order domain entiy from the test repository
            var purchaseOrderDomainEntity = await testPurchaseOrderRepository.GetPurchaseOrderAsync(purchaseOrderId, personId, GlAccessLevel.Full_Access, null);

            Assert.AreEqual(purchaseOrderDomainEntity.Status.ToString(), purchaseOrderDto.Status.ToString(), "Status must be the same.");
        }

        [TestMethod]
        public async Task GetPurchaseOrder_StatusPaid()
        {
            var purchaseOrderId = "11";
            var personId = "1";
            var purchaseOrderDto = await service.GetPurchaseOrderAsync(purchaseOrderId);

            // Get the purchase order domain entiy from the test repository
            var purchaseOrderDomainEntity = await testPurchaseOrderRepository.GetPurchaseOrderAsync(purchaseOrderId, personId, GlAccessLevel.Full_Access, null);

            Assert.AreEqual(purchaseOrderDomainEntity.Status.ToString(), purchaseOrderDto.Status.ToString(), "Status must be the same.");
        }

        [TestMethod]
        public async Task GetPurchaseOrder_StatusReconciled()
        {
            var purchaseOrderId = "9";
            var personId = "1";
            var purchaseOrderDto = await service.GetPurchaseOrderAsync(purchaseOrderId);

            // Get the purchase order domain entiy from the test repository
            var purchaseOrderDomainEntity = await testPurchaseOrderRepository.GetPurchaseOrderAsync(purchaseOrderId, personId, GlAccessLevel.Full_Access, null);

            Assert.AreEqual(purchaseOrderDomainEntity.Status.ToString(), purchaseOrderDto.Status.ToString(), "Status must be the same.");
        }

        [TestMethod]
        public async Task GetPurchaseOrder_StatusVoided()
        {
            var purchaseOrderId = "12";
            var personId = "1";
            var purchaseOrderDto = await service.GetPurchaseOrderAsync(purchaseOrderId);

            // Get the purchase order domain entiy from the test repository
            var purchaseOrderDomainEntity = await testPurchaseOrderRepository.GetPurchaseOrderAsync(purchaseOrderId, personId, GlAccessLevel.Full_Access, null);

            Assert.AreEqual(purchaseOrderDomainEntity.Status.ToString(), purchaseOrderDto.Status.ToString(), "Status must be the same.");
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task GetPurchaseOrder_RepositoryReturnsNullObject()
        {
            // Mock the GetRequisition repository method to return a null object within the Service method
            PurchaseOrder nullPurchaseOrder = null;
            this.mockPurchaseOrderRepository.Setup<Task<PurchaseOrder>>(poRepo => poRepo.GetPurchaseOrderAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>(), It.IsAny<List<string>>())).Returns(Task.FromResult(nullPurchaseOrder));
            var purchaseOrderDto = await service2.GetPurchaseOrderAsync("1");
        }
        #endregion

        #region Build service method

        /// <summary>
        /// Builds multiple purchase order service objects.
        /// </summary>
        private void BuildValidPurchaseOrderService()
        {
            // We need the unit tests to be independent of "real" implementations of these classes,
            // so we use Moq to create mock implementations that are based on the same interfaces
            var roleRepository = new Mock<IRoleRepository>().Object;
            var loggerObject = new Mock<ILogger>().Object;

            testPurchaseOrderRepository = new TestPurchaseOrderRepository();
            testGeneralLedgerConfigurationRepository = new TestGeneralLedgerConfigurationRepository();
            testGeneralLedgerUserRepository = new TestGeneralLedgerUserRepository();
            mockAccountFundAvailableRepo = new Mock<IAccountFundsAvailableRepository>();
            mockPersonRepository = new Mock<IPersonRepository>();
            var colleagueFinanceReferenceDataRepository = new TestColleagueFinanceReferenceDataRepository();
            var referenceDataRepo = new Mock<IReferenceDataRepository>();
            var vendorsDataRepo = new Mock<IVendorsRepository>();
            baseConfigurationRepositoryMock = new Mock<IConfigurationRepository>();
            baseConfigurationRepository = baseConfigurationRepositoryMock.Object;

            var buyersDataRepo = new Mock<IBuyerRepository>();

            // Set up and mock the adapter, and setup the GetAdapter method.
            var adapterRegistry = new Mock<IAdapterRegistry>();
            var purchaseOrderDtoAdapter = new AutoMapperAdapter<Domain.ColleagueFinance.Entities.PurchaseOrder, Dtos.ColleagueFinance.PurchaseOrder>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Domain.ColleagueFinance.Entities.PurchaseOrder, Dtos.ColleagueFinance.PurchaseOrder>()).Returns(purchaseOrderDtoAdapter);

            // Set up the service
            service = new PurchaseOrderService(testPurchaseOrderRepository, testGeneralLedgerConfigurationRepository, testGeneralLedgerUserRepository,
                colleagueFinanceReferenceDataRepository, referenceDataRepo.Object, buyersDataRepo.Object, vendorsDataRepo.Object, baseConfigurationRepository, adapterRegistry.Object, currentUserFactory, mockAccountFundAvailableRepo.Object, mockPersonRepository.Object, roleRepository, loggerObject);
            service2 = new PurchaseOrderService(this.mockPurchaseOrderRepository.Object, testGeneralLedgerConfigurationRepository, testGeneralLedgerUserRepository,
                colleagueFinanceReferenceDataRepository, referenceDataRepo.Object, buyersDataRepo.Object, vendorsDataRepo.Object, baseConfigurationRepository, adapterRegistry.Object, currentUserFactory, mockAccountFundAvailableRepo.Object, mockPersonRepository.Object, roleRepository, loggerObject);
        }
        #endregion
    }

    [TestClass]
    public class PurchaseOrderEEDMServiceTests : GeneralLedgerCurrentUser
    {
        #region Initialize and Cleanup

        private PurchaseOrderService service = null;
        private string guid = "b61b3a19-f164-47ad-afbc-dc5947340cdc";
        private TestPurchaseOrderRepository testPurchaseOrderRepository;
        private TestGeneralLedgerConfigurationRepository testGeneralLedgerConfigurationRepository;
        private TestGeneralLedgerUserRepository testGeneralLedgerUserRepository;
        private PurchaseOrderUser currentUserFactory = new GeneralLedgerCurrentUser.PurchaseOrderUser();
        private Mock<IPurchaseOrderRepository> mockPurchaseOrderRepository;

        private Mock<IColleagueFinanceReferenceDataRepository> colleagueFinanceReferenceDataRepository;
        private Mock<IReferenceDataRepository> referenceDataRepo;
        private Mock<IVendorsRepository> vendorsDataRepo;
        private Mock<IBuyerRepository> buyersDataRepo;
        private Mock<IAccountFundsAvailableRepository> mockAccountFundAvailableRepo;
        private Mock<IPersonRepository> mockPersonRepository;
        private Mock<IRoleRepository> roleRepository;
        private IConfigurationRepository baseConfigurationRepository;
        private Mock<IConfigurationRepository> baseConfigurationRepositoryMock;

        protected Domain.Entities.Role viewPurchaseOrderRole = new Domain.Entities.Role(1, "VIEW.PURCHASE.ORDERS");
        protected Domain.Entities.Role updatePurchaseOrderRole = new Domain.Entities.Role(2, "UPDATE.PURCHASE.ORDERS");

        private List<ShipToDestination> shipToDestinationsCollection;
        private List<FreeOnBoardType> freeOnBoardTypesCollection;
        private Collection<VendorTerm> vendorTermsCollection;
        private Collection<CommodityCode> commodityCodeCollection;
        private Collection<CommodityUnitType> commodityUnitTypeCollection;
        private Collection<CommerceTaxCode> commerceTaxCodeCollection;

        [TestInitialize]
        public void Initialize()
        {
            // Set up the mock PO repository
            this.mockPurchaseOrderRepository = new Mock<IPurchaseOrderRepository>();

            // build all service objects to use in testing
            BuildValidPurchaseOrderService();
        }

        [TestCleanup]
        public void Cleanup()
        {
            // Reset all of the services and repository variables.
            service = null;

            testPurchaseOrderRepository = null;
            testGeneralLedgerConfigurationRepository = null;
            testGeneralLedgerUserRepository = null;
            this.mockPurchaseOrderRepository = null;
        }
        #endregion

        [TestMethod]
        [ExpectedException(typeof(PermissionsException))]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrders_NoPermissions()
        {
            mockPurchaseOrderRepository.Setup(repo => repo.GetPurchaseOrdersAsync(It.IsAny<int>(), It.IsAny<int>())).ReturnsAsync(null);
            await service.GetPurchaseOrdersAsync(1, 1);
        }

        [TestMethod]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrder_MinimumData()
        {
            viewPurchaseOrderRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewPurchaseOrders));
            roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewPurchaseOrderRole });

            var purchaseOrder = new PurchaseOrder("1", guid, "P000001", "VendorName", PurchaseOrderStatus.Closed, new DateTime(2017, 01, 01), new DateTime(2017, 01, 01));
            purchaseOrder.VendorId = "0000010";

            var purchaseOrderCollection = new List<PurchaseOrder>() { purchaseOrder };

            Tuple<IEnumerable<PurchaseOrder>, int> purchaseOrderTuple = new Tuple<IEnumerable<PurchaseOrder>, int>(purchaseOrderCollection, 4);
            mockPurchaseOrderRepository.Setup(repo => repo.GetPurchaseOrdersAsync(It.IsAny<int>(), It.IsAny<int>())).ReturnsAsync(purchaseOrderTuple);
            var vendorsGuid = Guid.NewGuid().ToString();
            vendorsDataRepo.Setup(repo => repo.GetVendorGuidFromIdAsync("0000010")).ReturnsAsync(vendorsGuid);


            var actualCollection = await service.GetPurchaseOrdersAsync(0, 1);
            Assert.IsNotNull(actualCollection);
            var actual = actualCollection.Item1.FirstOrDefault(x => x.Id == guid);
            Assert.IsNotNull(actual);
            Assert.AreEqual(guid, actual.Id);
            Assert.AreEqual("P000001", actual.OrderNumber);
            Assert.AreEqual(vendorsGuid, actual.Vendor.ExistingVendor.Id);
            Assert.AreEqual(actual.Status, Dtos.EnumProperties.PurchaseOrdersStatus.Closed);
        }

        [TestMethod]
        [ExpectedException(typeof(PermissionsException))]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrdersByGuid_NoPermissions()
        {
            mockPurchaseOrderRepository.Setup(repo => repo.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ReturnsAsync(null);
            await service.GetPurchaseOrdersByGuidAsync(guid);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrdersByGuid_Null()
        {
            viewPurchaseOrderRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewPurchaseOrders));
            roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewPurchaseOrderRole });
            await service.GetPurchaseOrdersByGuidAsync(null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrdersByGuid_RepoReturnsNull()
        {
            viewPurchaseOrderRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewPurchaseOrders));
            roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewPurchaseOrderRole });

            mockPurchaseOrderRepository.Setup(repo => repo.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ReturnsAsync(null);
            await service.GetPurchaseOrdersByGuidAsync(guid);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrdersByGuid_NoVendor()
        {
            viewPurchaseOrderRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewPurchaseOrders));
            roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewPurchaseOrderRole });

            var purchaseOrder = new PurchaseOrder("1", guid, "P000001", "VendorName", PurchaseOrderStatus.Outstanding, new DateTime(2017, 01, 01), new DateTime(2017, 01, 01));

            mockPurchaseOrderRepository.Setup(repo => repo.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ReturnsAsync(purchaseOrder);

            var actual = await service.GetPurchaseOrdersByGuidAsync(guid);
        }

        [TestMethod]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrdersByGuid_MinimumData()
        {
            viewPurchaseOrderRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewPurchaseOrders));
            roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewPurchaseOrderRole });

            var purchaseOrder = new PurchaseOrder("1", guid, "P000001", "VendorName", PurchaseOrderStatus.Closed, new DateTime(2017, 01, 01), new DateTime(2017, 01, 01));
            purchaseOrder.VendorId = "0000010";

            mockPurchaseOrderRepository.Setup(repo => repo.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ReturnsAsync(purchaseOrder);
            var vendorsGuid = Guid.NewGuid().ToString();
            vendorsDataRepo.Setup(repo => repo.GetVendorGuidFromIdAsync("0000010")).ReturnsAsync(vendorsGuid);

            var actual = await service.GetPurchaseOrdersByGuidAsync(guid);
            Assert.IsNotNull(actual);
            Assert.AreEqual(guid, actual.Id);
            Assert.AreEqual("P000001", actual.OrderNumber);
            Assert.AreEqual(vendorsGuid, actual.Vendor.ExistingVendor.Id);
            Assert.AreEqual(actual.Status, Dtos.EnumProperties.PurchaseOrdersStatus.Closed);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrdersByGuid_MissingBuyer()
        {
            viewPurchaseOrderRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewPurchaseOrders));
            roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewPurchaseOrderRole });

            var purchaseOrder = new PurchaseOrder("1", guid, "P000001", "VendorName", PurchaseOrderStatus.Invoiced, new DateTime(2017, 01, 01), new DateTime(2017, 01, 01));
            purchaseOrder.VendorId = "0000010";
            purchaseOrder.Buyer = "0000011";
            mockPurchaseOrderRepository.Setup(repo => repo.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ReturnsAsync(purchaseOrder);
            var vendorsGuid = Guid.NewGuid().ToString();
            vendorsDataRepo.Setup(repo => repo.GetVendorGuidFromIdAsync("0000010")).ReturnsAsync(vendorsGuid);

            var buyersGuid = Guid.NewGuid().ToString();
            buyersDataRepo.Setup(repo => repo.GetBuyerGuidFromIdAsync("0000011")).ReturnsAsync(null);

            await service.GetPurchaseOrdersByGuidAsync(guid);
        }


        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrdersByGuid_MissingDefaultInitiator()
        {
            viewPurchaseOrderRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewPurchaseOrders));
            roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewPurchaseOrderRole });

            var purchaseOrder = new PurchaseOrder("1", guid, "P000001", "VendorName", PurchaseOrderStatus.NotApproved, new DateTime(2017, 01, 01), new DateTime(2017, 01, 01));
            purchaseOrder.VendorId = "0000010";
            purchaseOrder.DefaultInitiator = "0000012";
            mockPurchaseOrderRepository.Setup(repo => repo.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ReturnsAsync(purchaseOrder);
            var vendorsGuid = Guid.NewGuid().ToString();
            vendorsDataRepo.Setup(repo => repo.GetVendorGuidFromIdAsync("0000010")).ReturnsAsync(vendorsGuid);

            var initiatorGuid = Guid.NewGuid().ToString();
            buyersDataRepo.Setup(repo => repo.GetBuyerGuidFromIdAsync("0000012")).ReturnsAsync(null);
            await service.GetPurchaseOrdersByGuidAsync(guid);

        }


        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrdersByGuid_InvalidShipToCode()
        {
            viewPurchaseOrderRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewPurchaseOrders));
            roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewPurchaseOrderRole });
            var purchaseOrder = new PurchaseOrder("1", guid, "P000001", "VendorName", PurchaseOrderStatus.Backordered, new DateTime(2017, 01, 01), new DateTime(2017, 01, 01));
            purchaseOrder.VendorId = "0000010";
            purchaseOrder.ShipToCode = "INVALID";
            mockPurchaseOrderRepository.Setup(repo => repo.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ReturnsAsync(purchaseOrder);
            var vendorsGuid = Guid.NewGuid().ToString();
            vendorsDataRepo.Setup(repo => repo.GetVendorGuidFromIdAsync("0000010")).ReturnsAsync(vendorsGuid);

            await service.GetPurchaseOrdersByGuidAsync(guid);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrdersByGuid_InvalidFOB()
        {
            viewPurchaseOrderRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewPurchaseOrders));
            roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewPurchaseOrderRole });
            var purchaseOrder = new PurchaseOrder("1", guid, "P000001", "VendorName", PurchaseOrderStatus.Accepted, new DateTime(2017, 01, 01), new DateTime(2017, 01, 01));
            purchaseOrder.VendorId = "0000010";
            purchaseOrder.Fob = "INVALID";
            mockPurchaseOrderRepository.Setup(repo => repo.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ReturnsAsync(purchaseOrder);
            var vendorsGuid = Guid.NewGuid().ToString();
            vendorsDataRepo.Setup(repo => repo.GetVendorGuidFromIdAsync("0000010")).ReturnsAsync(vendorsGuid);

            await service.GetPurchaseOrdersByGuidAsync(guid);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrdersByGuid_InvalidVendorTerm()
        {
            viewPurchaseOrderRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewPurchaseOrders));
            roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewPurchaseOrderRole });

            var purchaseOrder = new PurchaseOrder("1", guid, "P000001", "VendorName", PurchaseOrderStatus.InProgress, new DateTime(2017, 01, 01), new DateTime(2017, 01, 01));
            purchaseOrder.VendorId = "0000010";
            purchaseOrder.VendorTerms = "INVALID";

            mockPurchaseOrderRepository.Setup(repo => repo.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ReturnsAsync(purchaseOrder);
            var vendorsGuid = Guid.NewGuid().ToString();
            vendorsDataRepo.Setup(repo => repo.GetVendorGuidFromIdAsync("0000010")).ReturnsAsync(vendorsGuid);

            await service.GetPurchaseOrdersByGuidAsync(guid);


        }

        [TestMethod]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrdersByGuid()
        {
            viewPurchaseOrderRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewPurchaseOrders));
            roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewPurchaseOrderRole });

            var shipToDestination = shipToDestinationsCollection.FirstOrDefault(sd => sd.Code == "CD");
            var fob = freeOnBoardTypesCollection.FirstOrDefault(f => f.Code == "DS");
            var vendorTerm = vendorTermsCollection.FirstOrDefault(f => f.Code == "30");

            var purchaseOrder = new PurchaseOrder("1", guid, "P000001", "VendorName", PurchaseOrderStatus.Closed, new DateTime(2017, 01, 01), new DateTime(2017, 01, 01));
            purchaseOrder.VendorId = "0000010";
            purchaseOrder.Buyer = "0000011";
            purchaseOrder.DefaultInitiator = "0000012";
            purchaseOrder.InitiatorName = "Renato Carosone";
            purchaseOrder.ShipToCode = shipToDestination.Code;
            purchaseOrder.Fob = fob.Code;
            purchaseOrder.MaintenanceDate = new DateTime(2017, 01, 02);
            purchaseOrder.VendorTerms = vendorTerm.Code;
            purchaseOrder.Comments = "Hello";
            purchaseOrder.InternalComments = "World";
            purchaseOrder.AltShippingAddress = new List<string>() { "123 Main St" };
            purchaseOrder.AltShippingCity = "Fairfax";
            purchaseOrder.AltShippingName = "Shipping Name";
            purchaseOrder.AltShippingPhone = "123-456-7890";
            purchaseOrder.AltShippingPhoneExt = "1";
            purchaseOrder.AltShippingState = "VA";
            purchaseOrder.AltShippingZip = "12345";

            purchaseOrder.MiscAddress = new List<string>() { "123 Main St" };
            purchaseOrder.MiscCity = "Fairfax";
            purchaseOrder.MiscName = new List<string>() { "Name" };
            purchaseOrder.MiscState = "VA";
            purchaseOrder.MiscZip = "12345";

            purchaseOrder.MiscCountry = "US";

            mockPurchaseOrderRepository.Setup(repo => repo.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ReturnsAsync(purchaseOrder);
            var vendorsGuid = Guid.NewGuid().ToString();
            vendorsDataRepo.Setup(repo => repo.GetVendorGuidFromIdAsync("0000010")).ReturnsAsync(vendorsGuid);

            var buyersGuid = Guid.NewGuid().ToString();
            buyersDataRepo.Setup(repo => repo.GetBuyerGuidFromIdAsync("0000011")).ReturnsAsync(buyersGuid);
            var initiatorGuid = Guid.NewGuid().ToString();
            buyersDataRepo.Setup(repo => repo.GetBuyerGuidFromIdAsync("0000012")).ReturnsAsync(initiatorGuid);

            var actual = await service.GetPurchaseOrdersByGuidAsync(guid);
            Assert.IsNotNull(actual);
            Assert.AreEqual(guid, actual.Id);
            Assert.AreEqual("P000001", actual.OrderNumber);
            Assert.IsNotNull(actual.Vendor);
            Assert.AreEqual(vendorsGuid, actual.Vendor.ExistingVendor.Id);
            Assert.IsNotNull(actual.Buyer);
            Assert.AreEqual(buyersGuid, actual.Buyer.Id);
            Assert.IsNotNull(actual.Initiator);
            Assert.AreEqual(initiatorGuid, actual.Initiator.Detail.Id);
            Assert.AreEqual("Renato Carosone", actual.Initiator.Name);
            Assert.IsNotNull(actual.Shipping);
            Assert.AreEqual(shipToDestination.Guid, actual.Shipping.ShipTo.Id);
            Assert.AreEqual(fob.Guid, actual.Shipping.FreeOnBoard.Id);
            Assert.AreEqual(Dtos.EnumProperties.PurchaseOrdersStatus.Closed, actual.Status);
            var printedComment = actual.Comments.FirstOrDefault(c => c.Type == Dtos.EnumProperties.CommentTypes.Printed);
            Assert.AreEqual("Hello", printedComment.Comment);
            var notPrintedComment = actual.Comments.FirstOrDefault(c => c.Type == Dtos.EnumProperties.CommentTypes.NotPrinted);
            Assert.AreEqual("World", notPrintedComment.Comment);

            Assert.AreEqual(vendorTerm.Guid, actual.PaymentTerms.Id);

            Assert.AreEqual(new DateTime(2017, 01, 02), actual.TransactionDate);
        }


        [TestMethod]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrdersByGuid_LineItem_Minimum()
        {
            viewPurchaseOrderRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewPurchaseOrders));
            roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewPurchaseOrderRole });

            var purchaseOrder = new PurchaseOrder("1", guid, "P000001", "VendorName", PurchaseOrderStatus.Closed, new DateTime(2017, 01, 01), new DateTime(2017, 01, 01));
            purchaseOrder.VendorId = "0000010";
            purchaseOrder.AddLineItem(new LineItem("1", "rocks", 2, 50, 0));

            mockPurchaseOrderRepository.Setup(repo => repo.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ReturnsAsync(purchaseOrder);

            var vendorsGuid = Guid.NewGuid().ToString();
            vendorsDataRepo.Setup(repo => repo.GetVendorGuidFromIdAsync("0000010")).ReturnsAsync(vendorsGuid);

            var actual = await service.GetPurchaseOrdersByGuidAsync(guid);
            Assert.IsNotNull(actual);
            Assert.AreEqual(guid, actual.Id);
            Assert.AreEqual("P000001", actual.OrderNumber);
            Assert.AreEqual(vendorsGuid, actual.Vendor.ExistingVendor.Id);
            Assert.AreEqual(actual.Status, Dtos.EnumProperties.PurchaseOrdersStatus.Closed);

            Assert.AreEqual(1, actual.LineItems.Count());
            Assert.AreEqual("1", actual.LineItems[0].LineItemNumber);
            Assert.AreEqual(2, actual.LineItems[0].Quantity);
            Assert.AreEqual(50, actual.LineItems[0].UnitPrice.Value);
            Assert.AreEqual(Dtos.EnumProperties.CurrencyIsoCode.USD, actual.LineItems[0].UnitPrice.Currency);


        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrdersByGuid_LineItem_InvalidCommodityCode()
        {
            viewPurchaseOrderRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewPurchaseOrders));
            roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewPurchaseOrderRole });

            var commodityCode = commodityCodeCollection.FirstOrDefault(cc => cc.Code == "00402");

            var purchaseOrder = new PurchaseOrder("1", guid, "P000001", "VendorName", PurchaseOrderStatus.Closed, new DateTime(2017, 01, 01), new DateTime(2017, 01, 01));
            purchaseOrder.VendorId = "0000010";
            purchaseOrder.AddLineItem(
                new LineItem("1", "rocks", 2, 50, 0)
                {
                    Comments = "Hello World",
                    CommodityCode = "INVALID"
                }
               );

            mockPurchaseOrderRepository.Setup(repo => repo.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ReturnsAsync(purchaseOrder);

            var vendorsGuid = Guid.NewGuid().ToString();
            vendorsDataRepo.Setup(repo => repo.GetVendorGuidFromIdAsync("0000010")).ReturnsAsync(vendorsGuid);

            await service.GetPurchaseOrdersByGuidAsync(guid);
        }

        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrdersByGuid_LineItem_InvalidUnitType()
        {
            viewPurchaseOrderRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewPurchaseOrders));
            roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewPurchaseOrderRole });

            var purchaseOrder = new PurchaseOrder("1", guid, "P000001", "VendorName", PurchaseOrderStatus.Closed, new DateTime(2017, 01, 01), new DateTime(2017, 01, 01));
            purchaseOrder.VendorId = "0000010";
            purchaseOrder.AddLineItem(
                new LineItem("1", "rocks", 2, 50, 0)
                {
                    UnitOfIssue = "INVALID"
                }
                );

            mockPurchaseOrderRepository.Setup(repo => repo.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ReturnsAsync(purchaseOrder);

            var vendorsGuid = Guid.NewGuid().ToString();
            vendorsDataRepo.Setup(repo => repo.GetVendorGuidFromIdAsync("0000010")).ReturnsAsync(vendorsGuid);
            await service.GetPurchaseOrdersByGuidAsync(guid);
        }

        [TestMethod]
        public async Task PurchaseOrderEEDMServiceTests_GetPurchaseOrdersByGuid_LineItem()
        {
            viewPurchaseOrderRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewPurchaseOrders));
            roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewPurchaseOrderRole });

            var commodityCode = commodityCodeCollection.FirstOrDefault(cc => cc.Code == "00402");
            var commodityUnitTypeCode = commodityUnitTypeCollection.FirstOrDefault(cuc => cuc.Code == "rock");
            var commerceTaxCode = commerceTaxCodeCollection.FirstOrDefault(cuc => cuc.Code == "ST");

            var purchaseOrder = new PurchaseOrder("1", guid, "P000001", "VendorName", PurchaseOrderStatus.Closed, new DateTime(2017, 01, 01), new DateTime(2017, 01, 01));
            purchaseOrder.VendorId = "0000010";

            var purchaseOrderLineItem = new LineItem("1", "rocks", 2, 50, 0)
            {
                Comments = "Hello World",
                CommodityCode = commodityCode.Code,
                UnitOfIssue = commodityUnitTypeCode.Code,
                TradeDiscountAmount = 10

            };
            purchaseOrderLineItem.AddGlDistribution(new LineItemGlDistribution("000001", 1, 25));
            purchaseOrderLineItem.AddTax(new LineItemTax(commerceTaxCode.Code, 5));

            purchaseOrder.AddLineItem(purchaseOrderLineItem);

            mockPurchaseOrderRepository.Setup(repo => repo.GetPurchaseOrdersByGuidAsync(It.IsAny<string>())).ReturnsAsync(purchaseOrder);

            var vendorsGuid = Guid.NewGuid().ToString();
            vendorsDataRepo.Setup(repo => repo.GetVendorGuidFromIdAsync("0000010")).ReturnsAsync(vendorsGuid);

            var actual = await service.GetPurchaseOrdersByGuidAsync(guid);
            Assert.IsNotNull(actual);
            Assert.AreEqual(guid, actual.Id);
            Assert.AreEqual("P000001", actual.OrderNumber);
            Assert.AreEqual(vendorsGuid, actual.Vendor.ExistingVendor.Id);
            Assert.AreEqual(actual.Status, Dtos.EnumProperties.PurchaseOrdersStatus.Closed);

            Assert.IsNotNull(actual.LineItems);
            Assert.AreEqual(1, actual.LineItems.Count());
            var lineItem = actual.LineItems[0];

            Assert.AreEqual("1", lineItem.LineItemNumber);
            Assert.AreEqual(2, lineItem.Quantity);
            Assert.AreEqual(50, lineItem.UnitPrice.Value);
            Assert.AreEqual(Dtos.EnumProperties.CurrencyIsoCode.USD, lineItem.UnitPrice.Currency);
            Assert.AreEqual("Hello World", lineItem.Comments[0].Comment);
            Assert.AreEqual(Dtos.EnumProperties.CommentTypes.NotPrinted, lineItem.Comments[0].Type);
            Assert.AreEqual(commodityCode.Guid, lineItem.CommodityCode.Id);
            Assert.AreEqual(commodityUnitTypeCode.Guid, lineItem.UnitOfMeasure.Id);


            var lineItemTax = lineItem.TaxCodes.First();
            Assert.IsNotNull(lineItemTax);
            Assert.AreEqual(commerceTaxCode.Guid, lineItemTax.Id);


        }


        #region Build service method

        /// <summary>
        /// Builds multiple purchase order service objects.
        /// </summary>
        private void BuildValidPurchaseOrderService()
        {
            roleRepository = new Mock<IRoleRepository>();
            var loggerObject = new Mock<ILogger>().Object;
            var testColleagueFinanceReferenceDataRepository = new TestColleagueFinanceReferenceDataRepository();

            testPurchaseOrderRepository = new TestPurchaseOrderRepository();
            testGeneralLedgerConfigurationRepository = new TestGeneralLedgerConfigurationRepository();
            testGeneralLedgerUserRepository = new TestGeneralLedgerUserRepository();

            colleagueFinanceReferenceDataRepository = new Mock<IColleagueFinanceReferenceDataRepository>();  //new TestColleagueFinanceReferenceDataRepository();
            referenceDataRepo = new Mock<IReferenceDataRepository>();
            vendorsDataRepo = new Mock<IVendorsRepository>();
            buyersDataRepo = new Mock<IBuyerRepository>();
            mockAccountFundAvailableRepo = new Mock<IAccountFundsAvailableRepository>();
            mockPersonRepository = new Mock<IPersonRepository>();

            baseConfigurationRepositoryMock = new Mock<IConfigurationRepository>();
            baseConfigurationRepository = baseConfigurationRepositoryMock.Object;

            // Set up and mock the adapter, and setup the GetAdapter method.
            var adapterRegistry = new Mock<IAdapterRegistry>();

            commerceTaxCodeCollection = new Collection<CommerceTaxCode>()
                { new CommerceTaxCode("82bf6b5-cdcd-4c8f-b5d8-3053bf5b3fbc", "ST", "TestGUIDdesc") };
            referenceDataRepo.Setup(repo => repo.GetCommerceTaxCodesAsync(It.IsAny<bool>())).ReturnsAsync(commerceTaxCodeCollection);

            commodityCodeCollection = new Collection<CommodityCode>()
                { new CommodityCode("772bf6b5-cdcd-4c8f-b5d8-3053bf5b3fbc", "00402", "Test Commodity") };
            colleagueFinanceReferenceDataRepository.Setup(repo => repo.GetCommodityCodesAsync(It.IsAny<bool>())).ReturnsAsync(commodityCodeCollection);

            var apTypes = new Collection<Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources>()
            { new Ellucian.Colleague.Domain.ColleagueFinance.Entities.AccountsPayableSources("apTypeGuid321", "AP", "Account Payable") };
            colleagueFinanceReferenceDataRepository.Setup(repo => repo.GetAccountsPayableSourcesAsync(It.IsAny<bool>())).ReturnsAsync(apTypes);

            vendorTermsCollection = new Collection<VendorTerm>()
            {   new VendorTerm("e338c649-db4b-4094-bb05-30ecd56ba82f", "02", "2-10-30"),
                new VendorTerm("d3a915c4-7914-4048-aa17-56d62911264a", "03", "3-10-30"),
                new VendorTerm("88393aeb-8239-4324-8203-707aa1181122", "30", "Net 30 days")
            };
            colleagueFinanceReferenceDataRepository.Setup(repo => repo.GetVendorTermsAsync(It.IsAny<bool>())).ReturnsAsync(vendorTermsCollection);

            commodityUnitTypeCollection = new Collection<CommodityUnitType>() {
                new CommodityUnitType("6a2bf6b5-cdcd-4c8f-b5d8-3053bf5b3fbc", "rock", "Rocks"),
                new CommodityUnitType("449e6a7c-6cd4-4f98-8a73-ab0aa3627f0d", "thing", "Things") };
            colleagueFinanceReferenceDataRepository.Setup(repo => repo.GetCommodityUnitTypesAsync(It.IsAny<bool>())).ReturnsAsync(commodityUnitTypeCollection);



            shipToDestinationsCollection = new List<ShipToDestination>()
                {
                    new ShipToDestination("580b7bbf-db6b-4241-8ce7-f2f94b302dd6", "CD", "Datatel - Central Dist. Office"),
                    new ShipToDestination("20916661-8560-4316-a2f8-72211489a4b9", "DT", "Datatel - Downtown"),
                    new ShipToDestination("7e14d03f-9a39-44db-abea-115b6a1642b0", "EC", "Datatel - Extension Center")
                };
            colleagueFinanceReferenceDataRepository.Setup(repo => repo.GetShipToDestinationsAsync(It.IsAny<bool>()))
                .ReturnsAsync(shipToDestinationsCollection);

            freeOnBoardTypesCollection = new List<FreeOnBoardType>()
                {
                    new FreeOnBoardType("b69f7e05-89de-466a-8a2c-3c6db4b83501", "DS", "Destination"),
                    new FreeOnBoardType("dcf759b4-5f22-472f-9d1d-e0c6ae7fb62e", "T1", "Test1"),
                    new FreeOnBoardType("4e90b207-c9c7-42e1-8d93-20776e43ae65", "T3", "Test2")
                };
            colleagueFinanceReferenceDataRepository.Setup(repo => repo.GetFreeOnBoardTypesAsync(It.IsAny<bool>()))
                .ReturnsAsync(freeOnBoardTypesCollection);

            // Mock the reference repository for states
            var states = new List<State>()
                {
                    new State("VA","Virginia"),
                    new State("MD","Maryland"),
                    new State("NY","New York"),
                    new State("MA","Massachusetts"),
                    new State("DC","District of Columbia"),
                    new State("TN","tennessee")
                };
            referenceDataRepo.Setup(repo => repo.GetStateCodesAsync()).ReturnsAsync(states);
            referenceDataRepo.Setup(repo => repo.GetStateCodesAsync(It.IsAny<bool>())).ReturnsAsync(states);


            // Mock the reference repository for country
            var countries = new List<Domain.Base.Entities.Country>()
                 {
                    new Domain.Base.Entities.Country("US","United States","US"){ IsoAlpha3Code = "USA" },
                    new Domain.Base.Entities.Country("CA","Canada","CA"){ IsoAlpha3Code = "CAN" },
                    new Domain.Base.Entities.Country("MX","Mexico","MX"){ IsoAlpha3Code = "MEX" },
                    new Domain.Base.Entities.Country("FR","France","FR"){ IsoAlpha3Code = "FRA" },
                    new Domain.Base.Entities.Country("BR","Brazil","BR"){ IsoAlpha3Code = "BRA" },
                    new Domain.Base.Entities.Country("AU","Australia","AU"){ IsoAlpha3Code = "AUS" },
                };
            referenceDataRepo.Setup(repo => repo.GetCountryCodesAsync()).ReturnsAsync(countries);
            referenceDataRepo.Setup(repo => repo.GetCountryCodesAsync(It.IsAny<bool>())).ReturnsAsync(countries);

            //viewPurchaseOrderRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.ViewPurchaseOrders));
            //roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { viewPurchaseOrderRole });

            //updatePurchaseOrderRole.AddPermission(new Domain.Entities.Permission(Ellucian.Colleague.Domain.ColleagueFinance.ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
            //roleRepository.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { updatePurchaseOrderRole });


            // Set up the service
            service = new PurchaseOrderService(mockPurchaseOrderRepository.Object, testGeneralLedgerConfigurationRepository, testGeneralLedgerUserRepository,
                colleagueFinanceReferenceDataRepository.Object, referenceDataRepo.Object, buyersDataRepo.Object, vendorsDataRepo.Object, baseConfigurationRepository, adapterRegistry.Object,
                currentUserFactory, mockAccountFundAvailableRepo.Object, mockPersonRepository.Object, roleRepository.Object, loggerObject);

        }
        #endregion
    }

    [TestClass]
    public class PurchaseOrderServiceTests_V10
    {
        [TestClass]
        public class PurchaseOrderServiceTests_POST : GeneralLedgerCurrentUser
        {
            #region DECLARATION

            protected Domain.Entities.Role createPurchaseOrder = new Domain.Entities.Role(1, "UPDATE.PURCHASE.ORDERS");

            private Mock<IPurchaseOrderRepository> purchaseOrderRepositoryMock;
            private Mock<IGeneralLedgerConfigurationRepository> generalLedgerConfigurationRepositoryMock;
            private Mock<IGeneralLedgerUserRepository> generalLedgerUserRepositoryMock;
            private Mock<IColleagueFinanceReferenceDataRepository> colleagueFinanceReferenceDataRepositoryMock;
            private Mock<IBuyerRepository> buyerRepositoryMock;
            private Mock<IReferenceDataRepository> referenceDataRepositoryMock;
            private Mock<IVendorsRepository> vendorsRepositoryMock;
            private Mock<IAccountFundsAvailableRepository> mockAccountFundAvailableRepo;
            private Mock<IPersonRepository> personRepositoryMock;
            private Mock<IRoleRepository> roleRepositoryMock;
            private Mock<ILogger> loggerMock;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private Mock<IConfigurationRepository> configurationRepositoryMock;
            private PurchaseOrderUser currentUserFactory;

            private PurchaseOrderService purchaseOrderService;

            private Dtos.PurchaseOrders purchaseOrder;
            private PurchaseOrder domainPurchaseOrder;
            private List<Dtos.PurchaseOrdersLineItemsDtoProperty> lineItems;
            private List<LineItem> domainLineItems;
            private List<Dtos.DtoProperties.PurchaseOrdersAccountDetailDtoProperty> accountDetails;
            private List<FundsAvailable> accountFundsAvailable;
            private Dtos.DtoProperties.PurchaseOrdersAllocationDtoProperty allocation;
            private Dtos.DtoProperties.PurchaseOrdersAllocatedDtoProperty allocated;
            private Dtos.PurchaseOrdersVendorDtoProperty vendor;
            private Dtos.DtoProperties.PurchaseOrdersInitiatorDtoProperty initiator;
            private Dtos.DtoProperties.PurchaseOrdersShippingDtoProperty shipping;
            private IEnumerable<ShipToDestination> shipToDestination;
            private IEnumerable<FreeOnBoardType> freeOnBoardType;
            private Dtos.OverrideShippingDestinationDtoProperty overrideShippingDestination;
            private Dtos.AddressPlace place;
            private IEnumerable<AccountsPayableSources> paymentSource;
            private IEnumerable<VendorTerm> vendorTerms;
            private IEnumerable<CommodityCode> commodityCodes;
            private IEnumerable<CommodityUnitType> commodityUnitTypes;
            private IEnumerable<CommerceTaxCode> taxCodes;
            private IEnumerable<Country> countries;
            private IEnumerable<State> states;

            private string guid = "1a59eed8-5fe7-4120-b1cf-f23266b9e874";

            #endregion

            #region TEST SETUP

            [TestInitialize]
            public void Initialize()
            {
                purchaseOrderRepositoryMock = new Mock<IPurchaseOrderRepository>();
                generalLedgerConfigurationRepositoryMock = new Mock<IGeneralLedgerConfigurationRepository>();
                generalLedgerUserRepositoryMock = new Mock<IGeneralLedgerUserRepository>();
                colleagueFinanceReferenceDataRepositoryMock = new Mock<IColleagueFinanceReferenceDataRepository>();
                buyerRepositoryMock = new Mock<IBuyerRepository>();
                referenceDataRepositoryMock = new Mock<IReferenceDataRepository>();
                vendorsRepositoryMock = new Mock<IVendorsRepository>();
                mockAccountFundAvailableRepo = new Mock<IAccountFundsAvailableRepository>();
                personRepositoryMock = new Mock<IPersonRepository>();
                roleRepositoryMock = new Mock<IRoleRepository>();
                loggerMock = new Mock<ILogger>();
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                configurationRepositoryMock = new Mock<IConfigurationRepository>();
                currentUserFactory = new GeneralLedgerCurrentUser.PurchaseOrderUser();

                purchaseOrderService = new PurchaseOrderService(purchaseOrderRepositoryMock.Object, generalLedgerConfigurationRepositoryMock.Object, generalLedgerUserRepositoryMock.Object,
                    colleagueFinanceReferenceDataRepositoryMock.Object, referenceDataRepositoryMock.Object, buyerRepositoryMock.Object,
                    vendorsRepositoryMock.Object, configurationRepositoryMock.Object, adapterRegistryMock.Object, currentUserFactory, mockAccountFundAvailableRepo.Object,
                    personRepositoryMock.Object, roleRepositoryMock.Object, loggerMock.Object);

                InitializeTestData();

                InitializeMock();
            }

            [TestCleanup]
            public void Cleanup()
            {
                purchaseOrderRepositoryMock = null;
                generalLedgerConfigurationRepositoryMock = null;
                generalLedgerUserRepositoryMock = null;
                colleagueFinanceReferenceDataRepositoryMock = null;
                buyerRepositoryMock = null;
                referenceDataRepositoryMock = null;
                vendorsRepositoryMock = null;
                mockAccountFundAvailableRepo = null;
                personRepositoryMock = null;
                roleRepositoryMock = null;
                loggerMock = null;
                adapterRegistryMock = null;
                currentUserFactory = null;
                configurationRepositoryMock = null;
            }

            private void InitializeTestData()
            {
                domainLineItems = new List<LineItem>()
                {
                    new LineItem("1", "desc", 10, 100, 110)
                    {
                       Comments = "Comments",
                       CommodityCode = "1",
                       VendorPart = "1",
                       UnitOfIssue = "1",
                       TradeDiscountAmount = 100,
                       StatusDate = DateTime.Today,
                       Status = PurchaseOrderStatus.InProgress
                    },
                    new LineItem("2", "desc", 10, 100, 110)
                    {
                       Comments = "Comments",
                       CommodityCode = "1",
                       VendorPart = "1",
                       UnitOfIssue = "1",
                       TradeDiscountPercentage = 10
                    }
                };

                states = new List<State>() { new State("FL", "Florida", "AUS") { } };

                countries = new List<Country>() { new Country("USA", "Desc", "USA", "USA") { } };

                taxCodes = new List<CommerceTaxCode>() { new CommerceTaxCode("1l49eed8-5fe7-4120-b1cf-f23266b9e874", "1", "Desc") { } };

                commodityUnitTypes = new List<CommodityUnitType>() { new CommodityUnitType("1k49eed8-5fe7-4120-b1cf-f23266b9e874", "1", "Desc") { } };

                commodityCodes = new List<CommodityCode>() { new CommodityCode("1j49eed8-5fe7-4120-b1cf-f23266b9e874", "1", "Desc") { } };

                vendorTerms = new List<VendorTerm>() { new VendorTerm("1i49eed8-5fe7-4120-b1cf-f23266b9e874", "1", "Desc") { } };

                paymentSource = new List<AccountsPayableSources>() { new AccountsPayableSources("1h49eed8-5fe7-4120-b1cf-f23266b9e874", "1", "Desc") { } };

                place = new Dtos.AddressPlace()
                {
                    Country = new Dtos.AddressCountry()
                    {
                        Code = Dtos.EnumProperties.IsoCode.USA,
                        Locality = "L",
                        Region = new Dtos.AddressRegion() { Code = "R-R" },
                        PostalCode = "12345"
                    }
                };

                overrideShippingDestination = new Dtos.OverrideShippingDestinationDtoProperty()
                {
                    Description = "D",
                    AddressLines = new List<string>() { "A1" },
                    Place = place
                };

                freeOnBoardType = new List<FreeOnBoardType>() { new FreeOnBoardType("1g49eed8-5fe7-4120-b1cf-f23266b9e874", "1", "Description") { } };

                shipToDestination = new List<ShipToDestination>() { new ShipToDestination("1f49eed8-5fe7-4120-b1cf-f23266b9e874", "1", "Description") { } };

                shipping = new Dtos.DtoProperties.PurchaseOrdersShippingDtoProperty()
                {
                    ShipTo = new Dtos.GuidObject2("1f49eed8-5fe7-4120-b1cf-f23266b9e874"),
                    FreeOnBoard = new Dtos.GuidObject2("1g49eed8-5fe7-4120-b1cf-f23266b9e874")
                };

                initiator = new Dtos.DtoProperties.PurchaseOrdersInitiatorDtoProperty() { Detail = new Dtos.GuidObject2("1f49eed8-5fe7-4120-b1cf-f23266b9e874") };

                vendor = new Dtos.PurchaseOrdersVendorDtoProperty()
                {
                    ExistingVendor = new Dtos.GuidObject2("1d49eed8-5fe7-4120-b1cf-f23266b9e874"),
                    ManualVendorDetails = new Dtos.ManualVendorDetailsDtoProperty()
                    {
                        Name = "Name",
                        AddressLines = new List<string>() { "A1" },
                        Place = place
                    }
                };

                allocated = new Dtos.DtoProperties.PurchaseOrdersAllocatedDtoProperty()
                {
                    Amount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 100, Currency = Dtos.EnumProperties.CurrencyIsoCode.USD },
                    Quantity = 10,
                    Percentage = 90
                };

                allocation = new Dtos.DtoProperties.PurchaseOrdersAllocationDtoProperty() { Allocated = allocated };

                accountFundsAvailable = new List<FundsAvailable>();

                accountDetails = new List<Dtos.DtoProperties.PurchaseOrdersAccountDetailDtoProperty>()
                {
                    new Dtos.DtoProperties.PurchaseOrdersAccountDetailDtoProperty()
                    {
                        SubmittedBy = new Dtos.GuidObject2("1c49eed8-5fe7-4120-b1cf-f23266b9e874"),
                        AccountingString = "1",
                        Allocation = allocation,
                        StatusDate = DateTime.Today,
                        Status = Dtos.EnumProperties.PurchaseOrdersStatus.InProgress
                    },
                    new Dtos.DtoProperties.PurchaseOrdersAccountDetailDtoProperty()
                    {
                        SubmittedBy = new Dtos.GuidObject2("2c49eed8-5fe7-4120-b1cf-f23266b9e874"),
                        AccountingString = "2",
                        Allocation = allocation
                    }
                };

                lineItems = new List<Dtos.PurchaseOrdersLineItemsDtoProperty>()
                {
                    new Dtos.PurchaseOrdersLineItemsDtoProperty()
                    {
                        AccountDetail = accountDetails,
                        Description = "Desc",
                        Quantity = 100,
                        UnitPrice = new Dtos.DtoProperties.Amount2DtoProperty() { Currency = Dtos.EnumProperties.CurrencyIsoCode.USD, Value = 10 },
                        CommodityCode = new Dtos.GuidObject2("1j49eed8-5fe7-4120-b1cf-f23266b9e874"),
                        UnitOfMeasure = new Dtos.GuidObject2("1k49eed8-5fe7-4120-b1cf-f23266b9e874")

                    },
                    new Dtos.PurchaseOrdersLineItemsDtoProperty()
                    {
                        AccountDetail = accountDetails,
                        Description = "Desc",
                        Comments = new List<Dtos.PurchaseOrdersCommentsDtoProperty>()
                        {
                            new Dtos.PurchaseOrdersCommentsDtoProperty() { Comment = "comment", Type = Dtos.EnumProperties.CommentTypes.Printed}
                        },
                        TradeDiscount = new Dtos.TradeDiscountDtoProperty()
                        {
                            Amount = new Dtos.DtoProperties.Amount2DtoProperty() { Currency = Dtos.EnumProperties.CurrencyIsoCode.USD, Value = 100},
                            Percent = 10
                        },
                        PartNumber = "1",
                        DesiredDate = DateTime.Today.AddDays(2),
                        TaxCodes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("1l49eed8-5fe7-4120-b1cf-f23266b9e874") }

                    },

                };

                purchaseOrder = new Dtos.PurchaseOrders()
                {
                    Id = "1a49eed8-5fe7-4120-b1cf-f23266b9e874",
                    SubmittedBy = new Dtos.GuidObject2("1b49eed8-5fe7-4120-b1cf-f23266b9e874"),
                    LineItems = lineItems,
                    TransactionDate = DateTime.Today,
                    Vendor = vendor,
                    Status = Dtos.EnumProperties.PurchaseOrdersStatus.InProgress,
                    OrderNumber = "1",
                    OrderedOn = DateTime.Today,
                    Type = Dtos.EnumProperties.PurchaseOrdersTypes.Travel,
                    DeliveredBy = DateTime.Today.AddDays(2),
                    Buyer = new Dtos.GuidObject2("1e49eed8-5fe7-4120-b1cf-f23266b9e874"),
                    Initiator = initiator,
                    Shipping = shipping,
                    OverrideShippingDestination = overrideShippingDestination,
                    ReferenceNumbers = new List<string>() { "1" },
                    PaymentSource = new Dtos.GuidObject2("1h49eed8-5fe7-4120-b1cf-f23266b9e874"),
                    PaymentTerms = new Dtos.GuidObject2("1i49eed8-5fe7-4120-b1cf-f23266b9e874"),
                    Comments = new List<Dtos.PurchaseOrdersCommentsDtoProperty>()
                    {
                        new Dtos.PurchaseOrdersCommentsDtoProperty() {Type = Dtos.EnumProperties.CommentTypes.NotPrinted, Comment = "1"},
                        new Dtos.PurchaseOrdersCommentsDtoProperty() {Type = Dtos.EnumProperties.CommentTypes.Printed, Comment = "2"}
                    }
                };

                domainPurchaseOrder = new PurchaseOrder("1", "number", "VendorName", PurchaseOrderStatus.InProgress, DateTime.Today, DateTime.Today)
                {
                    CurrencyCode = "USD",
                    Type = "Travel",
                    SubmittedBy = "1",
                    MaintenanceDate = DateTime.Today,
                    DeliveryDate = DateTime.Today,
                    VoidGlTranDate = DateTime.Today,
                    ReferenceNo = new List<string>() { "1" },
                    Buyer = "1",
                    InitiatorName = "Name",
                    DefaultInitiator = "1",
                    ShipToCode = "1",
                    Fob = "1",
                    AltShippingName = "A",
                    AltShippingAddress = new List<string>() { "A" },
                    MiscCountry = "USA",
                    AltShippingCity = "C",
                    AltShippingState = "FL",
                    AltShippingZip = "Z",
                    AltShippingCountry = "USA",
                    VendorId = "1",
                    MiscName = new List<string>() { "Name" },
                    MiscAddress = new List<string>() { "Line1" },
                    VendorTerms = "1",
                    ApType = "1",
                    Comments = "comments",
                    InternalComments = "Internalcomments",
                };

                domainLineItems.ForEach(d =>
                {
                    domainPurchaseOrder.AddLineItem(d);
                    d.AddTax(new LineItemTax("1", 100) { LineGlNumber = "1" });
                    d.AddGlDistribution(new LineItemGlDistribution("1", 10, 100, 10));
                });

                domainPurchaseOrder.AddLineItem(domainLineItems.FirstOrDefault());
            }

            private void InitializeMock()
            {
                mockAccountFundAvailableRepo.Setup(a => a.CheckAvailableFundsAsync(It.IsAny<List<FundsAvailable>>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                                               .ReturnsAsync(accountFundsAvailable);

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync(null);

                personRepositoryMock.Setup(p => p.GetPersonIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetShipToDestinationsAsync(true)).ReturnsAsync(shipToDestination);

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetFreeOnBoardTypesAsync(true)).ReturnsAsync(freeOnBoardType);

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetAccountsPayableSourcesAsync(true)).ReturnsAsync(paymentSource);

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetVendorTermsAsync(true)).ReturnsAsync(vendorTerms);

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetCommodityCodesAsync(true)).ReturnsAsync(commodityCodes);

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetCommodityUnitTypesAsync(true)).ReturnsAsync(commodityUnitTypes);

                referenceDataRepositoryMock.Setup(r => r.GetCountryCodesAsync(true)).ReturnsAsync(countries);

                referenceDataRepositoryMock.Setup(r => r.GetStateCodesAsync(true)).ReturnsAsync(states);
            }

            #endregion

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_ArgumentNullException_PurchaseOrders_Null()
            {
                await purchaseOrderService.PostPurchaseOrdersAsync(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_ArgumentNullException_PurchaseOrders_Id_NUll()
            {
                await purchaseOrderService.PostPurchaseOrdersAsync(new Dtos.PurchaseOrders() { });
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_PermissionsException()
            {
                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            //[TestMethod]
            //[ExpectedException(typeof(Exception))]
            //public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_ArgumentException_Fund_NotAvailable()
            //{
            //    createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
            //    roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

            //    accountFundsAvailable.FundsAvailable = Dtos.EnumProperties.FundsAvailable.NotAvailable;

            //    await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            //}

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_Exception_AccountingString_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.LineItems.FirstOrDefault().AccountDetail.LastOrDefault().AccountingString = "1";

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_ArgumentNullException_ExistingVendor_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.Vendor.ExistingVendor.Id = null;

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_ApplicationException_Invalid_PurchaseOrder_Status()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.Status = Dtos.EnumProperties.PurchaseOrdersStatus.Closed;

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_Exception_When_Person_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                personRepositoryMock.Setup(p => p.GetPersonIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync(null);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_When_Buyer_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                personRepositoryMock.SetupSequence(p => p.GetPersonIdFromGuidAsync(It.IsAny<string>()))
                                    .Returns(Task.FromResult("1"))
                                    .Returns(Task.FromResult(""));

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_When_PurchaseOrder_Initiator_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                personRepositoryMock.SetupSequence(p => p.GetPersonIdFromGuidAsync(It.IsAny<string>()))
                                    .Returns(Task.FromResult("1"))
                                    .Returns(Task.FromResult("1"))
                                    .Returns(Task.FromResult(""));

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_Exception_When_Shipping_Destination_Null()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetShipToDestinationsAsync(true)).ReturnsAsync(null);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_KeyNotFoundException_When_Shipping_Destination_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.Shipping.ShipTo.Id = "1";

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_Exception_When_Shipping_FreeOnboard_Null()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetFreeOnBoardTypesAsync(true)).ReturnsAsync(null);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_KeyNotFoundException_When_Shipping_FreeOnboard_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.Shipping.FreeOnBoard.Id = "1";

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_ArgumentException_Invalid_Vendor()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                var numberOfCalls = 0;
                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>()))
                                           .ReturnsAsync(null)
                                           .Callback(() => { numberOfCalls++; if (numberOfCalls == 2) throw new ArgumentException(); });

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_ArgumentNullException_When_VendorId_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_KeyNotFoundException_When_PaymentSource_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });
                mockAccountFundAvailableRepo.Setup(a => a.CheckAvailableFundsAsync(It.IsAny<List<FundsAvailable>>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                                               .ReturnsAsync(accountFundsAvailable);

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                purchaseOrder.PaymentSource.Id = "1";

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_KeyNotFoundException_When_VendorTerms_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                purchaseOrder.PaymentTerms.Id = "1";

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_Exception_When_CommodityCodes_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetCommodityCodesAsync(true)).ReturnsAsync(null);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_Exception_When_CommodityUnitTypes_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetCommodityUnitTypesAsync(true)).ReturnsAsync(null);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_Exception_When_CoomodityCode_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                purchaseOrder.LineItems.FirstOrDefault().CommodityCode.Id = "1";

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_DtoToEntity_Exception_When_CoomodityUnit_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                purchaseOrder.LineItems.FirstOrDefault().UnitOfMeasure.Id = "1";

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(RepositoryException))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_RepositoryException()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ThrowsAsync(new RepositoryException());

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_EntityToDto_ArgumentNullException()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(null);

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_EntityToDto_Exception_SubmittedBy_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(null);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_EntityToDto_Exception_Buyer_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                domainPurchaseOrder.CurrencyCode = null;
                domainPurchaseOrder.Type = "Procurement";

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.SetupSequence(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>()))
                                   .Returns(Task.FromResult(guid)).Returns(Task.FromResult(""));


                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_DefaultInitiator_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                domainPurchaseOrder.CurrencyCode = null;
                domainPurchaseOrder.HostCountry = "CAN";
                domainPurchaseOrder.Type = "Eprocurement";

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.SetupSequence(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>()))
                                   .Returns(Task.FromResult(guid)).Returns(Task.FromResult(guid)).Returns(Task.FromResult(""));

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_ShipToDestination_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                domainPurchaseOrder.CurrencyCode = "USD1";
                domainPurchaseOrder.HostCountry = "CAN";
                domainPurchaseOrder.Type = "";
                domainPurchaseOrder.ShipToCode = "2";

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_FreeOnBoardType_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                domainPurchaseOrder.CurrencyCode = "USD1";
                domainPurchaseOrder.Fob = "2";

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_Country_Null_And_MiscCountry_NotNull()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                domainPurchaseOrder.MiscCountry = "AUS";

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_Country_Null_When_State_NotNull()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                domainPurchaseOrder.MiscCountry = null;
                domainPurchaseOrder.AltShippingCountry = "AUS";

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_EntityToDto_ArgumentException_State_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                domainPurchaseOrder.AltShippingState = "AL";

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_EntityToDto_Exception_Vendor_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                vendorsRepositoryMock.Setup(r => r.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(null);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_VendorTerm_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                domainPurchaseOrder.VendorTerms = "2";

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                vendorsRepositoryMock.Setup(r => r.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_ApType_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                domainPurchaseOrder.ApType = "2";

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                vendorsRepositoryMock.Setup(r => r.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_CommodityCode_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                domainLineItems.FirstOrDefault().CommodityCode = "2";

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                domainPurchaseOrder.AddLineItem(domainLineItems.FirstOrDefault());

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                vendorsRepositoryMock.Setup(r => r.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_UnitOfIssue_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                domainLineItems.FirstOrDefault().UnitOfIssue = "2";

                domainPurchaseOrder.AddLineItem(domainLineItems.FirstOrDefault());

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                vendorsRepositoryMock.Setup(r => r.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_EntityToDto_CommerceTaxCodes_Null()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                domainPurchaseOrder.AddLineItem(domainLineItems.FirstOrDefault());

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                vendorsRepositoryMock.Setup(r => r.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(null);

                await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);
            }

            [TestMethod]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                vendorsRepositoryMock.Setup(r => r.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                var result = await purchaseOrderService.PostPurchaseOrdersAsync(purchaseOrder);

                Assert.IsNotNull(result);
            }
        }

        [TestClass]
        public class PurchaseOrderServiceTests_PUT : GeneralLedgerCurrentUser
        {
            #region DECLARATION

            protected Domain.Entities.Role createPurchaseOrder = new Domain.Entities.Role(1, "UPDATE.PURCHASE.ORDERS");

            private Mock<IPurchaseOrderRepository> purchaseOrderRepositoryMock;
            private Mock<IGeneralLedgerConfigurationRepository> generalLedgerConfigurationRepositoryMock;
            private Mock<IGeneralLedgerUserRepository> generalLedgerUserRepositoryMock;
            private Mock<IColleagueFinanceReferenceDataRepository> colleagueFinanceReferenceDataRepositoryMock;
            private Mock<IBuyerRepository> buyerRepositoryMock;
            private Mock<IReferenceDataRepository> referenceDataRepositoryMock;
            private Mock<IVendorsRepository> vendorsRepositoryMock;
            private Mock<IAccountFundsAvailableRepository> accountFundAvailableRepoMock;
            private Mock<IPersonRepository> personRepositoryMock;
            private Mock<IRoleRepository> roleRepositoryMock;
            private Mock<ILogger> loggerMock;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private Mock<IConfigurationRepository> configurationRepositoryMock;
            private PurchaseOrderUser currentUserFactory;

            private PurchaseOrderService purchaseOrderService;

            private Dtos.PurchaseOrders purchaseOrder;
            private PurchaseOrder domainPurchaseOrder;
            private List<Dtos.PurchaseOrdersLineItemsDtoProperty> lineItems;
            private List<LineItem> domainLineItems;
            private List<Dtos.DtoProperties.PurchaseOrdersAccountDetailDtoProperty> accountDetails;
            private List<FundsAvailable> accountFundsAvailable;
            private Dtos.DtoProperties.PurchaseOrdersAllocationDtoProperty allocation;
            private Dtos.DtoProperties.PurchaseOrdersAllocatedDtoProperty allocated;
            private Dtos.PurchaseOrdersVendorDtoProperty vendor;
            private Dtos.DtoProperties.PurchaseOrdersInitiatorDtoProperty initiator;
            private Dtos.DtoProperties.PurchaseOrdersShippingDtoProperty shipping;
            private IEnumerable<ShipToDestination> shipToDestination;
            private IEnumerable<FreeOnBoardType> freeOnBoardType;
            private Dtos.OverrideShippingDestinationDtoProperty overrideShippingDestination;
            private Dtos.AddressPlace place;
            private IEnumerable<AccountsPayableSources> paymentSource;
            private IEnumerable<VendorTerm> vendorTerms;
            private IEnumerable<CommodityCode> commodityCodes;
            private IEnumerable<CommodityUnitType> commodityUnitTypes;
            private IEnumerable<CommerceTaxCode> taxCodes;
            private IEnumerable<Country> countries;
            private IEnumerable<State> states;

            private string guid = "1a59eed8-5fe7-4120-b1cf-f23266b9e874";

            #endregion

            #region TEST SETUP

            [TestInitialize]
            public void Initialize()
            {
                purchaseOrderRepositoryMock = new Mock<IPurchaseOrderRepository>();
                generalLedgerConfigurationRepositoryMock = new Mock<IGeneralLedgerConfigurationRepository>();
                generalLedgerUserRepositoryMock = new Mock<IGeneralLedgerUserRepository>();
                colleagueFinanceReferenceDataRepositoryMock = new Mock<IColleagueFinanceReferenceDataRepository>();
                buyerRepositoryMock = new Mock<IBuyerRepository>();
                referenceDataRepositoryMock = new Mock<IReferenceDataRepository>();
                vendorsRepositoryMock = new Mock<IVendorsRepository>();
                accountFundAvailableRepoMock = new Mock<IAccountFundsAvailableRepository>();
                personRepositoryMock = new Mock<IPersonRepository>();
                roleRepositoryMock = new Mock<IRoleRepository>();
                loggerMock = new Mock<ILogger>();
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                configurationRepositoryMock = new Mock<IConfigurationRepository>();
                currentUserFactory = new GeneralLedgerCurrentUser.PurchaseOrderUser();

                purchaseOrderService = new PurchaseOrderService(purchaseOrderRepositoryMock.Object, generalLedgerConfigurationRepositoryMock.Object, generalLedgerUserRepositoryMock.Object,
                    colleagueFinanceReferenceDataRepositoryMock.Object, referenceDataRepositoryMock.Object, buyerRepositoryMock.Object,
                    vendorsRepositoryMock.Object, configurationRepositoryMock.Object, adapterRegistryMock.Object, currentUserFactory, accountFundAvailableRepoMock.Object,
                    personRepositoryMock.Object, roleRepositoryMock.Object, loggerMock.Object);

                InitializeTestData();

                InitializeMock();
            }

            [TestCleanup]
            public void Cleanup()
            {
                purchaseOrderRepositoryMock = null;
                generalLedgerConfigurationRepositoryMock = null;
                generalLedgerUserRepositoryMock = null;
                colleagueFinanceReferenceDataRepositoryMock = null;
                buyerRepositoryMock = null;
                referenceDataRepositoryMock = null;
                vendorsRepositoryMock = null;
                accountFundAvailableRepoMock = null;
                personRepositoryMock = null;
                roleRepositoryMock = null;
                loggerMock = null;
                adapterRegistryMock = null;
                currentUserFactory = null;
                configurationRepositoryMock = null;
            }

            private void InitializeTestData()
            {
                domainLineItems = new List<LineItem>()
                {
                    new LineItem("1", "desc", 10, 100, 110)
                    {
                       Comments = "Comments",
                       CommodityCode = "1",
                       VendorPart = "1",
                       UnitOfIssue = "1",
                       TradeDiscountAmount = 100,
                       StatusDate = DateTime.Today,
                       Status = PurchaseOrderStatus.InProgress
                    },
                    new LineItem("2", "desc", 10, 100, 110)
                    {
                       Comments = "Comments",
                       CommodityCode = "1",
                       VendorPart = "1",
                       UnitOfIssue = "1",
                       TradeDiscountPercentage = 10
                    }
                };

                states = new List<State>() { new State("FL", "Florida", "AUS") { } };

                countries = new List<Country>() { new Country("USA", "Desc", "USA", "USA") { } };

                taxCodes = new List<CommerceTaxCode>() { new CommerceTaxCode("1l49eed8-5fe7-4120-b1cf-f23266b9e874", "1", "Desc") { } };

                commodityUnitTypes = new List<CommodityUnitType>() { new CommodityUnitType("1k49eed8-5fe7-4120-b1cf-f23266b9e874", "1", "Desc") { } };

                commodityCodes = new List<CommodityCode>() { new CommodityCode("1j49eed8-5fe7-4120-b1cf-f23266b9e874", "1", "Desc") { } };

                vendorTerms = new List<VendorTerm>() { new VendorTerm("1i49eed8-5fe7-4120-b1cf-f23266b9e874", "1", "Desc") { } };

                paymentSource = new List<AccountsPayableSources>() { new AccountsPayableSources("1h49eed8-5fe7-4120-b1cf-f23266b9e874", "1", "Desc") { } };

                place = new Dtos.AddressPlace()
                {
                    Country = new Dtos.AddressCountry()
                    {
                        Code = Dtos.EnumProperties.IsoCode.USA,
                        Locality = "L",
                        Region = new Dtos.AddressRegion() { Code = "R-R" },
                        PostalCode = "12345"
                    }
                };

                overrideShippingDestination = new Dtos.OverrideShippingDestinationDtoProperty()
                {
                    Description = "D",
                    AddressLines = new List<string>() { "A1" },
                    Place = place
                };

                freeOnBoardType = new List<FreeOnBoardType>() { new FreeOnBoardType("1g49eed8-5fe7-4120-b1cf-f23266b9e874", "1", "Description") { } };

                shipToDestination = new List<ShipToDestination>() { new ShipToDestination("1f49eed8-5fe7-4120-b1cf-f23266b9e874", "1", "Description") { } };

                shipping = new Dtos.DtoProperties.PurchaseOrdersShippingDtoProperty()
                {
                    ShipTo = new Dtos.GuidObject2("1f49eed8-5fe7-4120-b1cf-f23266b9e874"),
                    FreeOnBoard = new Dtos.GuidObject2("1g49eed8-5fe7-4120-b1cf-f23266b9e874")
                };

                initiator = new Dtos.DtoProperties.PurchaseOrdersInitiatorDtoProperty() { Detail = new Dtos.GuidObject2("1f49eed8-5fe7-4120-b1cf-f23266b9e874") };

                vendor = new Dtos.PurchaseOrdersVendorDtoProperty()
                {
                    ExistingVendor = new Dtos.GuidObject2("1d49eed8-5fe7-4120-b1cf-f23266b9e874"),
                    ManualVendorDetails = new Dtos.ManualVendorDetailsDtoProperty()
                    {
                        Name = "Name",
                        AddressLines = new List<string>() { "A1" },
                        Place = place
                    }
                };

                allocated = new Dtos.DtoProperties.PurchaseOrdersAllocatedDtoProperty()
                {
                    Amount = new Dtos.DtoProperties.Amount2DtoProperty() { Value = 100, Currency = Dtos.EnumProperties.CurrencyIsoCode.USD },
                    Quantity = 10,
                    Percentage = 90
                };

                allocation = new Dtos.DtoProperties.PurchaseOrdersAllocationDtoProperty() { Allocated = allocated };

                accountFundsAvailable = new List<FundsAvailable>();

                accountDetails = new List<Dtos.DtoProperties.PurchaseOrdersAccountDetailDtoProperty>()
                {
                    new Dtos.DtoProperties.PurchaseOrdersAccountDetailDtoProperty()
                    {
                        SubmittedBy = new Dtos.GuidObject2("1c49eed8-5fe7-4120-b1cf-f23266b9e874"),
                        AccountingString = "1",
                        Allocation = allocation,
                        StatusDate = DateTime.Today,
                        Status = Dtos.EnumProperties.PurchaseOrdersStatus.InProgress
                    },
                    new Dtos.DtoProperties.PurchaseOrdersAccountDetailDtoProperty()
                    {
                        SubmittedBy = new Dtos.GuidObject2("2c49eed8-5fe7-4120-b1cf-f23266b9e874"),
                        AccountingString = "2",
                        Allocation = allocation
                    }
                };

                lineItems = new List<Dtos.PurchaseOrdersLineItemsDtoProperty>()
                {
                    new Dtos.PurchaseOrdersLineItemsDtoProperty()
                    {
                        AccountDetail = accountDetails,
                        Description = "Desc",
                        Quantity = 100,
                        UnitPrice = new Dtos.DtoProperties.Amount2DtoProperty() { Currency = Dtos.EnumProperties.CurrencyIsoCode.USD, Value = 10 },
                        CommodityCode = new Dtos.GuidObject2("1j49eed8-5fe7-4120-b1cf-f23266b9e874"),
                        UnitOfMeasure = new Dtos.GuidObject2("1k49eed8-5fe7-4120-b1cf-f23266b9e874")

                    },
                    new Dtos.PurchaseOrdersLineItemsDtoProperty()
                    {
                        AccountDetail = accountDetails,
                        Description = "Desc",
                        Comments = new List<Dtos.PurchaseOrdersCommentsDtoProperty>()
                        {
                            new Dtos.PurchaseOrdersCommentsDtoProperty() { Comment = "comment", Type = Dtos.EnumProperties.CommentTypes.Printed}
                        },
                        TradeDiscount = new Dtos.TradeDiscountDtoProperty()
                        {
                            Amount = new Dtos.DtoProperties.Amount2DtoProperty() { Currency = Dtos.EnumProperties.CurrencyIsoCode.USD, Value = 100},
                            Percent = 10
                        },
                        PartNumber = "1",
                        DesiredDate = DateTime.Today.AddDays(2),
                        TaxCodes = new List<Dtos.GuidObject2>() { new Dtos.GuidObject2("1l49eed8-5fe7-4120-b1cf-f23266b9e874") }

                    },

                };

                purchaseOrder = new Dtos.PurchaseOrders()
                {
                    Id = "1a49eed8-5fe7-4120-b1cf-f23266b9e874",
                    SubmittedBy = new Dtos.GuidObject2("1b49eed8-5fe7-4120-b1cf-f23266b9e874"),
                    LineItems = lineItems,
                    TransactionDate = DateTime.Today,
                    Vendor = vendor,
                    Status = Dtos.EnumProperties.PurchaseOrdersStatus.InProgress,
                    OrderNumber = "1",
                    OrderedOn = DateTime.Today,
                    Type = Dtos.EnumProperties.PurchaseOrdersTypes.Travel,
                    DeliveredBy = DateTime.Today.AddDays(2),
                    Buyer = new Dtos.GuidObject2("1e49eed8-5fe7-4120-b1cf-f23266b9e874"),
                    Initiator = initiator,
                    Shipping = shipping,
                    OverrideShippingDestination = overrideShippingDestination,
                    ReferenceNumbers = new List<string>() { "1" },
                    PaymentSource = new Dtos.GuidObject2("1h49eed8-5fe7-4120-b1cf-f23266b9e874"),
                    PaymentTerms = new Dtos.GuidObject2("1i49eed8-5fe7-4120-b1cf-f23266b9e874"),
                    Comments = new List<Dtos.PurchaseOrdersCommentsDtoProperty>()
                    {
                        new Dtos.PurchaseOrdersCommentsDtoProperty() {Type = Dtos.EnumProperties.CommentTypes.NotPrinted, Comment = "1"},
                        new Dtos.PurchaseOrdersCommentsDtoProperty() {Type = Dtos.EnumProperties.CommentTypes.Printed, Comment = "2"}
                    }
                };

                domainPurchaseOrder = new PurchaseOrder("1", "number", "VendorName", PurchaseOrderStatus.InProgress, DateTime.Today, DateTime.Today)
                {
                    CurrencyCode = "USD",
                    Type = "Travel",
                    SubmittedBy = "1",
                    MaintenanceDate = DateTime.Today,
                    DeliveryDate = DateTime.Today,
                    VoidGlTranDate = DateTime.Today,
                    ReferenceNo = new List<string>() { "1" },
                    Buyer = "1",
                    InitiatorName = "Name",
                    DefaultInitiator = "1",
                    ShipToCode = "1",
                    Fob = "1",
                    AltShippingName = "A",
                    AltShippingAddress = new List<string>() { "A" },
                    MiscCountry = "USA",
                    AltShippingCity = "C",
                    AltShippingState = "FL",
                    AltShippingZip = "Z",
                    AltShippingCountry = "USA",
                    VendorId = "1",
                    MiscName = new List<string>() { "Name" },
                    MiscAddress = new List<string>() { "Line1" },
                    VendorTerms = "1",
                    ApType = "1",
                    Comments = "comments",
                    InternalComments = "Internalcomments",
                };

                domainLineItems.ForEach(d =>
                {
                    domainPurchaseOrder.AddLineItem(d);
                    d.AddTax(new LineItemTax("1", 100) { LineGlNumber = "1" });
                    d.AddGlDistribution(new LineItemGlDistribution("1", 10, 100, 10));
                });

                domainPurchaseOrder.AddLineItem(domainLineItems.FirstOrDefault());
            }

            private void InitializeMock()
            {
                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                accountFundAvailableRepoMock.Setup(a => a.CheckAvailableFundsAsync(It.IsAny<List<FundsAvailable>>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>()))
                                               .ReturnsAsync(accountFundsAvailable);

                personRepositoryMock.Setup(p => p.GetPersonIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetShipToDestinationsAsync(true)).ReturnsAsync(shipToDestination);

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetFreeOnBoardTypesAsync(true)).ReturnsAsync(freeOnBoardType);

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetAccountsPayableSourcesAsync(true)).ReturnsAsync(paymentSource);

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetVendorTermsAsync(true)).ReturnsAsync(vendorTerms);

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetCommodityCodesAsync(true)).ReturnsAsync(commodityCodes);

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetCommodityUnitTypesAsync(true)).ReturnsAsync(commodityUnitTypes);

                referenceDataRepositoryMock.Setup(r => r.GetCountryCodesAsync(true)).ReturnsAsync(countries);

                referenceDataRepositoryMock.Setup(r => r.GetStateCodesAsync(true)).ReturnsAsync(states);
            }

            #endregion

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_ArgumentNullException_PurchaseOrder_Null()
            {
                await purchaseOrderService.PutPurchaseOrdersAsync(It.IsAny<string>(), null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_ArgumentNullException_PurchaseOrders_Id_Null()
            {
                await purchaseOrderService.PutPurchaseOrdersAsync(It.IsAny<string>(), new Dtos.PurchaseOrders() { });
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task PurchaseOrdersService_PostPurchaseOrdersAsync_PermissionsException()
            {
                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            //[TestMethod]
            //[ExpectedException(typeof(ArgumentException))]
            //public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_ArgumentException_Fund_NotAvailable()
            //{
            //    createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
            //    roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

            //    accountFundsAvailable.FundsAvailable = Dtos.EnumProperties.FundsAvailable.NotAvailable;

            //    await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            //}

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_Exception_AccountingString_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.LineItems.FirstOrDefault().AccountDetail.LastOrDefault().AccountingString = "1";

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_ArgumentNullException_ExistingVendor_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.Vendor.ExistingVendor.Id = null;

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_ApplicationException_Invalid_PurchaseOrder_Status()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.Status = Dtos.EnumProperties.PurchaseOrdersStatus.Closed;

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_Exception_When_Person_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                personRepositoryMock.Setup(p => p.GetPersonIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync(null);

                purchaseOrder.Status = Dtos.EnumProperties.PurchaseOrdersStatus.Notapproved;
                purchaseOrder.Type = Dtos.EnumProperties.PurchaseOrdersTypes.Procurement;

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            //[TestMethod]
            //[ExpectedException(typeof(Exception))]
            //public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_When_Buyer_NullOrEmpty()
            //{
            //    createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
            //    roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

            //    personRepositoryMock.SetupSequence(p => p.GetPersonIdFromGuidAsync(It.IsAny<string>()))
            //                        .Returns(Task.FromResult("1"))
            //                        .Returns(Task.FromResult(""));

            //    purchaseOrder.Status = Dtos.EnumProperties.PurchaseOrdersStatus.Outstanding;
            //    purchaseOrder.Type = Dtos.EnumProperties.PurchaseOrdersTypes.Eprocurement;

            //    await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            //}

            //[TestMethod]
            //[ExpectedException(typeof(Exception))]
            //public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_When_PurchaseOrder_Initiator_NullOrEmpty()
            //{
            //    createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
            //    roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

            //    personRepositoryMock.SetupSequence(p => p.GetPersonIdFromGuidAsync(It.IsAny<string>()))
            //                        .Returns(Task.FromResult("1"))
            //                        .Returns(Task.FromResult("1"))
            //                        .Returns(Task.FromResult(""));

            //    purchaseOrder.Status = Dtos.EnumProperties.PurchaseOrdersStatus.Paid;

            //    await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            //}

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_Exception_When_Shipping_Destination_Null()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetShipToDestinationsAsync(true)).ReturnsAsync(null);

                purchaseOrder.Status = Dtos.EnumProperties.PurchaseOrdersStatus.Reconciled;

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_KeyNotFoundException_When_Shipping_Destination_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.Shipping.ShipTo.Id = "1";
                purchaseOrder.Status = Dtos.EnumProperties.PurchaseOrdersStatus.Voided;

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_Exception_When_Shipping_FreeOnboard_Null()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetFreeOnBoardTypesAsync(true)).ReturnsAsync(null);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_KeyNotFoundException_When_Shipping_FreeOnboard_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.Shipping.FreeOnBoard.Id = "1";

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_ArgumentException_Invalid_Vendor()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                var numberOfCalls = 0;
                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>()))
                                           .Returns(Task.FromResult("1"))
                                           .Callback(() => { numberOfCalls++; if (numberOfCalls == 3) throw new ArgumentException(); });

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_ArgumentNullException_When_VendorId_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.SetupSequence(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>()))
                                           .Returns(Task.FromResult("1"))
                                           .Returns(Task.FromResult("1"))
                                           .Returns(Task.FromResult(""));

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_KeyNotFoundException_When_PaymentSource_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                //accountFundAvailableRepoMock.Setup(a => a.GetAccountFundsAvailableByFilterCriteriaAsync(It.IsAny<string>(), It.IsAny<decimal>(), It.IsAny<DateTime?>(), It.IsAny<string>()))
                //                               .ReturnsAsync(accountFundsAvailable);

                purchaseOrder.PaymentSource.Id = "1";

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_KeyNotFoundException_When_VendorTerms_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.PaymentTerms.Id = "1";

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_Exception_When_CommodityCodes_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetCommodityCodesAsync(true)).ReturnsAsync(null);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_Exception_When_CommodityUnitTypes_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                colleagueFinanceReferenceDataRepositoryMock.Setup(r => r.GetCommodityUnitTypesAsync(true)).ReturnsAsync(null);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_Exception_When_CoomodityCode_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.LineItems.FirstOrDefault().CommodityCode.Id = "1";

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_DtoToEntity_Exception_When_CoomodityUnit_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });
                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);
                purchaseOrder.LineItems.FirstOrDefault().UnitOfMeasure.Id = "1";

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(RepositoryException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_RepositoryException()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Status = Dtos.EnumProperties.PurchaseOrdersStatus.Accepted;

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ThrowsAsync(new RepositoryException());

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_EntityToDto_ArgumentNullException()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Status = Dtos.EnumProperties.PurchaseOrdersStatus.Backordered;
                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);
                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(null);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_EntityToDto_Exception_SubmittedBy_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Status = Dtos.EnumProperties.PurchaseOrdersStatus.Closed;

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(null);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_EntityToDto_Exception_Buyer_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Status = Dtos.EnumProperties.PurchaseOrdersStatus.Invoiced;

                domainPurchaseOrder.CurrencyCode = null;
                domainPurchaseOrder.Type = "Procurement";

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                buyerRepositoryMock.SetupSequence(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>()))
                                   .Returns(Task.FromResult(guid)).Returns(Task.FromResult(""));

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_DefaultInitiator_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Status = Dtos.EnumProperties.PurchaseOrdersStatus.Notapproved;

                domainPurchaseOrder.CurrencyCode = null;
                domainPurchaseOrder.HostCountry = "CAN";
                domainPurchaseOrder.Type = "Eprocurement";
                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);
                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.SetupSequence(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>()))
                                   .Returns(Task.FromResult(guid)).Returns(Task.FromResult(guid)).Returns(Task.FromResult(""));

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_ShipToDestination_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Status = Dtos.EnumProperties.PurchaseOrdersStatus.Outstanding;

                domainPurchaseOrder.CurrencyCode = "USD1";
                domainPurchaseOrder.HostCountry = "CAN";
                domainPurchaseOrder.Type = "";
                domainPurchaseOrder.ShipToCode = "2";

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);
                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_FreeOnBoardType_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Status = Dtos.EnumProperties.PurchaseOrdersStatus.Paid;

                domainPurchaseOrder.CurrencyCode = "USD1";
                domainPurchaseOrder.Fob = "2";

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_Country_Null_And_MiscCountry_NotNull()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Status = Dtos.EnumProperties.PurchaseOrdersStatus.Reconciled;
                domainPurchaseOrder.MiscCountry = "AUS";
                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_Country_Null_When_State_NotNull()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Status = Dtos.EnumProperties.PurchaseOrdersStatus.Voided;
                domainPurchaseOrder.MiscCountry = null;
                domainPurchaseOrder.AltShippingCountry = "AUS";
                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);
                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_EntityToDto_Exception_When_Country_IsoAlpha3Code_NotMatch()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrder.LineItems.FirstOrDefault().AccountDetail.FirstOrDefault().Status = Dtos.EnumProperties.PurchaseOrdersStatus.Voided;
                countries.FirstOrDefault().IsoAlpha3Code = "USA1";
                domainPurchaseOrder.MiscCountry = "USA";

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_EntityToDto_ArgumentException_State_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                domainPurchaseOrder.AltShippingState = "AL";

                countries.FirstOrDefault().IsoAlpha3Code = "CAN";
                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);
                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_EntityToDto_Exception_Vendor_NullOrEmpty()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                countries.FirstOrDefault().IsoAlpha3Code = "AUS";

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);
                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);
                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                vendorsRepositoryMock.Setup(r => r.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(null);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_VendorTerm_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                countries.FirstOrDefault().IsoAlpha3Code = "BRA";

                domainPurchaseOrder.VendorTerms = "2";

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                vendorsRepositoryMock.Setup(r => r.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_ApType_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                countries.FirstOrDefault().IsoAlpha3Code = "MEX";

                domainPurchaseOrder.ApType = "2";

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                vendorsRepositoryMock.Setup(r => r.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_CommodityCode_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                countries.FirstOrDefault().IsoAlpha3Code = "NLD";

                domainLineItems.FirstOrDefault().CommodityCode = "2";

                domainPurchaseOrder.AddLineItem(domainLineItems.FirstOrDefault());
                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);
                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                vendorsRepositoryMock.Setup(r => r.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_EntityToDto_KeyNotFoundException_UnitOfIssue_NotFound()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                countries.FirstOrDefault().IsoAlpha3Code = "GBR";

                domainLineItems.FirstOrDefault().UnitOfIssue = "2";

                domainPurchaseOrder.AddLineItem(domainLineItems.FirstOrDefault());
                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);
                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                vendorsRepositoryMock.Setup(r => r.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync_EntityToDto_CommerceTaxCodes_Null()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                countries.FirstOrDefault().IsoAlpha3Code = "BFA";

                domainPurchaseOrder.AddLineItem(domainLineItems.FirstOrDefault());

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                vendorsRepositoryMock.Setup(r => r.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(null);

                await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);
            }

            [TestMethod]
            public async Task PurchaseOrdersService_PutPurchaseOrdersAsync()
            {
                createPurchaseOrder.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(ColleagueFinancePermissionCodes.UpdatePurchaseOrders));
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { createPurchaseOrder });

                purchaseOrderRepositoryMock.Setup(r => r.GetPurchaseOrdersIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("1");

                purchaseOrderRepositoryMock.Setup(r => r.UpdatePurchaseOrdersAsync(It.IsAny<PurchaseOrder>())).ReturnsAsync(domainPurchaseOrder);

                buyerRepositoryMock.Setup(r => r.GetBuyerGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                vendorsRepositoryMock.Setup(r => r.GetVendorGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync(guid);

                referenceDataRepositoryMock.Setup(r => r.GetCommerceTaxCodesAsync(true)).ReturnsAsync(taxCodes);

                var result = await purchaseOrderService.PutPurchaseOrdersAsync(purchaseOrder.Id, purchaseOrder);

                Assert.IsNotNull(result);
            }
        }
    }
}