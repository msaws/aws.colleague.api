﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Colleague.Coordination.ColleagueFinance.Tests.UserFactories;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Colleague.Domain.ColleagueFinance.Tests;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos;
using Ellucian.Colleague.Dtos.DtoProperties;
using Ellucian.Colleague.Dtos.EnumProperties;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using CreditOrDebit = Ellucian.Colleague.Domain.ColleagueFinance.Entities.CreditOrDebit;
using CurrencyCodes = Ellucian.Colleague.Domain.ColleagueFinance.Entities.CurrencyCodes;
using GeneralLedgerTransaction = Ellucian.Colleague.Domain.ColleagueFinance.Entities.GeneralLedgerTransaction;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Tests.Services
{
   
    [TestClass]
    public class GeneralLedgerTransactionServiceTests : GeneralLedgerCurrentUser
    {
        #region Initialize and Cleanup
        private GeneralLedgerTransactionService _generalLedgerTransactionService = null;
      
        private TestGeneralLedgerUserRepository _testGeneralLedgerUserRepository;
        private TestGeneralLedgerConfigurationRepository _testGlConfigurationRepository;
        private GeneralLedgerAccountStructure _testGlAccountStructure;
        private GeneralLedgerClassConfiguration _testGlClassConfiguration;
        private Mock<IGeneralLedgerTransactionRepository>  _generalLedgerTransactionRepository;
        private Mock<IPersonRepository> _personRepositoryMock;

        // Define user factories
        private readonly UserFactoryAll _glUserFactoryAll = new GeneralLedgerCurrentUser.UserFactoryAll();
      
        [TestInitialize]
        public void Initialize()
        {
            BuildValidGeneralLedgerTransactionService();

        }

        [TestCleanup]
        public void Cleanup()
        {
            _generalLedgerTransactionService = null;
            _testGeneralLedgerUserRepository = null;
            _testGlConfigurationRepository = null;
            _testGlAccountStructure = null;
            _testGlClassConfiguration = null;
            _personRepositoryMock = null;
           
        }
        #endregion

        #region GetGeneralLedgerTransactionsAsync test methods

        [TestMethod]
        public async Task GetGeneralLedgerTransactions_GetAsync()
        {

            const string accountingString = "01-02-03-04-05550-66077*A1";
            const string referenceNumber = "GL122312321";
            const string projectId = "A1";
            const string id = "0001234";

            var generalLedgerTransaction = GetTestGeneralLedgerTransactions().FirstOrDefault(x => x.Id == id);

            // Get the necessary configuration settings and build the GL user object.
            _testGlAccountStructure = await _testGlConfigurationRepository.GetAccountStructureAsync();
            _testGlClassConfiguration = await _testGlConfigurationRepository.GetClassConfigurationAsync();
            var generalLedgerUser = await _testGeneralLedgerUserRepository.GetGeneralLedgerUserAsync(_glUserFactoryAll.CurrentUser.PersonId, _testGlAccountStructure.FullAccessRole, _testGlClassConfiguration.ClassificationName, _testGlClassConfiguration.ExpenseClassValues);

            var generalLedgerConfigurationRepository = new Mock<IGeneralLedgerConfigurationRepository>();
            generalLedgerConfigurationRepository.Setup(x => x.GetAccountStructureAsync()).ReturnsAsync(_testGlAccountStructure);

            generalLedgerConfigurationRepository.Setup(x => x.GetClassConfigurationAsync())
               .ReturnsAsync(_testGlClassConfiguration);

            var generalLedgerUserRepository = new Mock<IGeneralLedgerUserRepository>();
            generalLedgerUserRepository.Setup(x => x.GetGeneralLedgerUserAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<string>>()))
               .ReturnsAsync(generalLedgerUser);



            var glTransactions = new List<GeneralLedgerTransaction>();
            var glTransaction = new GeneralLedgerTransaction()
            {
                Id = generalLedgerTransaction.Id,
                ProcessMode = generalLedgerTransaction.ProcessMode.ToString()
            };

            var transaction = generalLedgerTransaction.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);

            var genLedgrTransaction = new GenLedgrTransaction("DN", transaction.LedgerDate)
            {
                ReferenceNumber = transaction.ReferenceNumber,
                TransactionTypeReferenceDate = transaction.TransactionTypeReferenceDate,
                TransactionNumber = transaction.TransactionNumber

            };

            var transactionDetail = transaction.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);

            var genLedgrTransactionDetail = new GenLedgrTransactionDetail(transactionDetail.AccountingString, projectId, transactionDetail.Description,
                CreditOrDebit.Credit, new AmountAndCurrency(25, CurrencyCodes.USD));

            glTransactions.Add(glTransaction);

            genLedgrTransaction.TransactionDetailLines = new List<GenLedgrTransactionDetail>() { genLedgrTransactionDetail };

            glTransaction.GeneralLedgerTransactions = new List<GenLedgrTransaction>() { genLedgrTransaction };

            _generalLedgerTransactionRepository.Setup(x => x.GetByIdAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>())).ReturnsAsync(glTransaction);
            _generalLedgerTransactionRepository.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>(), _testGlAccountStructure)).ReturnsAsync(glTransaction);
            _generalLedgerTransactionRepository.Setup(x => x.GetAsync(It.IsAny<string>(), It.IsAny<GlAccessLevel>())).ReturnsAsync(glTransactions);

             var generalLedgerTransactionDtos =
                await _generalLedgerTransactionService.GetAsync();

            Assert.IsNotNull(generalLedgerTransactionDtos);

            var generalLedgerTransactionDto = generalLedgerTransactionDtos.FirstOrDefault();

            Assert.AreEqual(Dtos.EnumProperties.ProcessMode.Update, generalLedgerTransactionDto.ProcessMode);

            var actual =
                generalLedgerTransactionDto.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);
            var expected =
                generalLedgerTransaction.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);

            Assert.AreEqual(expected.ReferenceNumber, actual.ReferenceNumber);
            Assert.AreEqual(expected.LedgerDate.Value.Date, actual.LedgerDate.Value.Date);
            Assert.AreEqual(Dtos.EnumProperties.GeneralLedgerTransactionType.Donation, actual.Type);

            Assert.IsNotNull(actual.TransactionDetailLines);
            var actualTransactionDetailLine =
                actual.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);
            var expectedTransactionDetailLine =
               expected.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);
            Assert.AreEqual(expectedTransactionDetailLine.Description, actualTransactionDetailLine.Description);
            Assert.AreEqual(expectedTransactionDetailLine.Amount.Currency, actualTransactionDetailLine.Amount.Currency);
            Assert.AreEqual(expectedTransactionDetailLine.Amount.Value, actualTransactionDetailLine.Amount.Value);
            Assert.AreEqual(expectedTransactionDetailLine.Type, actualTransactionDetailLine.Type);
            Assert.AreEqual(expectedTransactionDetailLine.AccountingString, actualTransactionDetailLine.AccountingString);

        }

        [TestMethod]
        public async Task GetGeneralLedgerTransactions_Get2Async()
        {

            const string accountingString = "01-02-03-04-05550-66077*A1";
            const string referenceNumber = "GL122312321";
            const string projectId = "A1";
            const string id = "0001234";

            var generalLedgerTransaction = GetTestGeneralLedgerTransactions2().FirstOrDefault(x => x.Id == id);

            // Get the necessary configuration settings and build the GL user object.
            _testGlAccountStructure = await _testGlConfigurationRepository.GetAccountStructureAsync();
            _testGlClassConfiguration = await _testGlConfigurationRepository.GetClassConfigurationAsync();
            var generalLedgerUser = await _testGeneralLedgerUserRepository.GetGeneralLedgerUserAsync(_glUserFactoryAll.CurrentUser.PersonId, _testGlAccountStructure.FullAccessRole, _testGlClassConfiguration.ClassificationName, _testGlClassConfiguration.ExpenseClassValues);

            var generalLedgerConfigurationRepository = new Mock<IGeneralLedgerConfigurationRepository>();
            generalLedgerConfigurationRepository.Setup(x => x.GetAccountStructureAsync()).ReturnsAsync(_testGlAccountStructure);

            generalLedgerConfigurationRepository.Setup(x => x.GetClassConfigurationAsync())
               .ReturnsAsync(_testGlClassConfiguration);

            var generalLedgerUserRepository = new Mock<IGeneralLedgerUserRepository>();
            generalLedgerUserRepository.Setup(x => x.GetGeneralLedgerUserAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<string>>()))
               .ReturnsAsync(generalLedgerUser);


            _personRepositoryMock.Setup(pr => pr.GetPersonGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("123456");
            _personRepositoryMock.Setup(pr => pr.GetPersonIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("123456");

            var glTransactions = new List<GeneralLedgerTransaction>();
            var glTransaction = new GeneralLedgerTransaction()
            {
                Id = generalLedgerTransaction.Id,
                ProcessMode = generalLedgerTransaction.ProcessMode.ToString(),
                SubmittedBy = generalLedgerTransaction.SubmittedBy.ToString()
            };

            var transaction = generalLedgerTransaction.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);

            var genLedgrTransaction = new GenLedgrTransaction("DN", transaction.LedgerDate)
            {
                ReferenceNumber = transaction.ReferenceNumber,
                TransactionTypeReferenceDate = transaction.TransactionTypeReferenceDate,
                TransactionNumber = transaction.TransactionNumber

            };

            var transactionDetail = transaction.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);

            var genLedgrTransactionDetail = new GenLedgrTransactionDetail(transactionDetail.AccountingString, projectId, transactionDetail.Description,
                CreditOrDebit.Credit, new AmountAndCurrency(25, CurrencyCodes.USD));
            genLedgrTransactionDetail.SubmittedBy = "123456";

            glTransactions.Add(glTransaction);

            genLedgrTransaction.TransactionDetailLines = new List<GenLedgrTransactionDetail>() { genLedgrTransactionDetail };

            glTransaction.GeneralLedgerTransactions = new List<GenLedgrTransaction>() { genLedgrTransaction };

            _generalLedgerTransactionRepository.Setup(x => x.GetByIdAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>())).ReturnsAsync(glTransaction);
            _generalLedgerTransactionRepository.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>(), _testGlAccountStructure)).ReturnsAsync(glTransaction);
            _generalLedgerTransactionRepository.Setup(x => x.GetAsync(It.IsAny<string>(), It.IsAny<GlAccessLevel>())).ReturnsAsync(glTransactions);

            var generalLedgerTransactionDtos =
               await _generalLedgerTransactionService.Get2Async();

            Assert.IsNotNull(generalLedgerTransactionDtos);

            var generalLedgerTransactionDto = generalLedgerTransactionDtos.FirstOrDefault();

            Assert.AreEqual(Dtos.EnumProperties.ProcessMode.Update, generalLedgerTransactionDto.ProcessMode);
            Assert.AreEqual(generalLedgerTransaction.SubmittedBy.Id, generalLedgerTransactionDto.SubmittedBy.Id);

            var actual =
                generalLedgerTransactionDto.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);
            var expected =
                generalLedgerTransaction.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);

            Assert.AreEqual(expected.ReferenceNumber, actual.ReferenceNumber);
            Assert.AreEqual(expected.LedgerDate.Value.Date, actual.LedgerDate.Value.Date);
            Assert.AreEqual(Dtos.EnumProperties.GeneralLedgerTransactionType.Donation, actual.Type);

            Assert.IsNotNull(actual.TransactionDetailLines);
            var actualTransactionDetailLine =
                actual.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);
            var expectedTransactionDetailLine =
               expected.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);
            Assert.AreEqual(expectedTransactionDetailLine.Description, actualTransactionDetailLine.Description);
            Assert.AreEqual(expectedTransactionDetailLine.Amount.Currency, actualTransactionDetailLine.Amount.Currency);
            Assert.AreEqual(expectedTransactionDetailLine.Amount.Value, actualTransactionDetailLine.Amount.Value);
            Assert.AreEqual(expectedTransactionDetailLine.Type, actualTransactionDetailLine.Type);
            Assert.AreEqual(expectedTransactionDetailLine.SubmittedBy.Id, actualTransactionDetailLine.SubmittedBy.Id);
            Assert.AreEqual(expectedTransactionDetailLine.AccountingString, actualTransactionDetailLine.AccountingString);

        }
        
      
        [TestMethod]
        public async Task GetGeneralLedgerTransactions_GetByIdAsync()
        {

            const string accountNumber = "01-02-03-04-05550-66077";
            const string referenceNumber = "GL122312321";
            const string projectId = "A1";

            // Get the necessary configuration settings and build the GL user object.
            _testGlAccountStructure = await _testGlConfigurationRepository.GetAccountStructureAsync();
            _testGlClassConfiguration = await _testGlConfigurationRepository.GetClassConfigurationAsync();
           var generalLedgerUser = await _testGeneralLedgerUserRepository.GetGeneralLedgerUserAsync(_glUserFactoryAll.CurrentUser.PersonId, _testGlAccountStructure.FullAccessRole, _testGlClassConfiguration.ClassificationName, _testGlClassConfiguration.ExpenseClassValues);

           //var glConfiguration = await generalLedgerConfigurationRepository.GetAccountStructureAsync();

           var generalLedgerConfigurationRepository = new Mock<IGeneralLedgerConfigurationRepository>();
           generalLedgerConfigurationRepository.Setup(x => x.GetAccountStructureAsync()).ReturnsAsync(_testGlAccountStructure);

          // var glClassConfiguration = await generalLedgerConfigurationRepository.GetClassConfigurationAsync();
            generalLedgerConfigurationRepository.Setup(x => x.GetClassConfigurationAsync())
                .ReturnsAsync(_testGlClassConfiguration);

            var generalLedgerUserRepository = new Mock<IGeneralLedgerUserRepository>();
            //var generalLedgerUser = await generalLedgerUserRepository.GetGeneralLedgerUserAsync(CurrentUser.PersonId, glConfiguration.FullAccessRole, glClassConfiguration.ClassificationName, glClassConfiguration.ExpenseClassValues);
            generalLedgerUserRepository.Setup(x => x.GetGeneralLedgerUserAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<string>>()))
                .ReturnsAsync(generalLedgerUser);

            var glTransaction = new GeneralLedgerTransaction()
            {
                Id = "001234",
                ProcessMode = "Update"
            };
            var genLedgrTransaction = new GenLedgrTransaction("DN", DateTimeOffset.Now)
            {
                ReferenceNumber = referenceNumber, TransactionTypeReferenceDate = DateTimeOffset.Now, TransactionNumber = "1"
                
            };

            var genLedgrTransactionDetail = new GenLedgrTransactionDetail(accountNumber, projectId, "DESC",
                CreditOrDebit.Credit, new AmountAndCurrency(25, CurrencyCodes.USD));

            genLedgrTransaction.TransactionDetailLines = new List<GenLedgrTransactionDetail>() { genLedgrTransactionDetail };

            glTransaction.GeneralLedgerTransactions = new List<GenLedgrTransaction>() { genLedgrTransaction };
            

             //var generalLedgerTransactionDomainEntity = await generalLedgerTransactionRepository.GetByIdAsync(id, CurrentUser.PersonId, generalLedgerUser.GlAccessLevel);

            //_generalLedgerTransactionRepository.Setup(x => x.GetByIdAsync("001234", generalLedgerUser.Id, GlAccessLevel.Full_Access)).ReturnsAsync(glTransaction);
            _generalLedgerTransactionRepository.Setup(x => x.GetByIdAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>())).ReturnsAsync(glTransaction);

           
            var generalLedgerTransactionDto = await _generalLedgerTransactionService.GetByIdAsync("001234");

            Assert.IsNotNull(generalLedgerTransactionDto);
            Assert.AreEqual(Dtos.EnumProperties.ProcessMode.Update, generalLedgerTransactionDto.ProcessMode);

            var generalLedgerTransaction = generalLedgerTransactionDto.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);

            Assert.AreEqual(referenceNumber, generalLedgerTransaction.ReferenceNumber);
            Assert.AreEqual(DateTime.Now.Date, generalLedgerTransaction.LedgerDate.Value.Date);
            Assert.AreEqual(Dtos.EnumProperties.GeneralLedgerTransactionType.Donation, generalLedgerTransaction.Type);

            Assert.IsNotNull(generalLedgerTransaction.TransactionDetailLines);
            var transactionDetailLine =
                generalLedgerTransaction.TransactionDetailLines.FirstOrDefault(x => x.Description == "DESC");

            Assert.AreEqual("DESC", transactionDetailLine.Description);
            Assert.AreEqual(Dtos.EnumProperties.CurrencyCodes.USD, transactionDetailLine.Amount.Currency);
            Assert.AreEqual(25, transactionDetailLine.Amount.Value);
            Assert.AreEqual(Dtos.EnumProperties.CreditOrDebit.Credit, transactionDetailLine.Type);
            Assert.AreEqual( string.Concat(accountNumber,"*",projectId), transactionDetailLine.AccountingString);

        }

        [TestMethod]
        public async Task GetGeneralLedgerTransactions_GetById2Async()
        {

            const string accountNumber = "01-02-03-04-05550-66077";
            const string referenceNumber = "GL122312321";
            const string projectId = "A1";

            // Get the necessary configuration settings and build the GL user object.
            _testGlAccountStructure = await _testGlConfigurationRepository.GetAccountStructureAsync();
            _testGlClassConfiguration = await _testGlConfigurationRepository.GetClassConfigurationAsync();
            var generalLedgerUser = await _testGeneralLedgerUserRepository.GetGeneralLedgerUserAsync(_glUserFactoryAll.CurrentUser.PersonId, _testGlAccountStructure.FullAccessRole, _testGlClassConfiguration.ClassificationName, _testGlClassConfiguration.ExpenseClassValues);

            //var glConfiguration = await generalLedgerConfigurationRepository.GetAccountStructureAsync();

            var generalLedgerConfigurationRepository = new Mock<IGeneralLedgerConfigurationRepository>();
            generalLedgerConfigurationRepository.Setup(x => x.GetAccountStructureAsync()).ReturnsAsync(_testGlAccountStructure);

            // var glClassConfiguration = await generalLedgerConfigurationRepository.GetClassConfigurationAsync();
            generalLedgerConfigurationRepository.Setup(x => x.GetClassConfigurationAsync())
                .ReturnsAsync(_testGlClassConfiguration);

            var generalLedgerUserRepository = new Mock<IGeneralLedgerUserRepository>();
            //var generalLedgerUser = await generalLedgerUserRepository.GetGeneralLedgerUserAsync(CurrentUser.PersonId, glConfiguration.FullAccessRole, glClassConfiguration.ClassificationName, glClassConfiguration.ExpenseClassValues);
            generalLedgerUserRepository.Setup(x => x.GetGeneralLedgerUserAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<string>>()))
                .ReturnsAsync(generalLedgerUser);

            _personRepositoryMock.Setup(pr => pr.GetPersonGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("123456");
            _personRepositoryMock.Setup(pr => pr.GetPersonIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("123456");

            var glTransaction = new GeneralLedgerTransaction()
            {
                Id = "001234",
                ProcessMode = "Update",
                SubmittedBy = "123456"
            };
            var genLedgrTransaction = new GenLedgrTransaction("DN", DateTimeOffset.Now)
            {
                ReferenceNumber = referenceNumber,
                TransactionTypeReferenceDate = DateTimeOffset.Now,
                TransactionNumber = "1"

            };

            var genLedgrTransactionDetail = new GenLedgrTransactionDetail(accountNumber, projectId, "DESC",
                CreditOrDebit.Credit, new AmountAndCurrency(25, CurrencyCodes.USD));
            genLedgrTransactionDetail.SubmittedBy = "123456";

            genLedgrTransaction.TransactionDetailLines = new List<GenLedgrTransactionDetail>() { genLedgrTransactionDetail };

            glTransaction.GeneralLedgerTransactions = new List<GenLedgrTransaction>() { genLedgrTransaction };


            //var generalLedgerTransactionDomainEntity = await generalLedgerTransactionRepository.GetByIdAsync(id, CurrentUser.PersonId, generalLedgerUser.GlAccessLevel);

            //_generalLedgerTransactionRepository.Setup(x => x.GetByIdAsync("001234", generalLedgerUser.Id, GlAccessLevel.Full_Access)).ReturnsAsync(glTransaction);
            _generalLedgerTransactionRepository.Setup(x => x.GetByIdAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>())).ReturnsAsync(glTransaction);


            var generalLedgerTransactionDto = await _generalLedgerTransactionService.GetById2Async("001234");

            Assert.IsNotNull(generalLedgerTransactionDto);
            Assert.AreEqual(Dtos.EnumProperties.ProcessMode.Update, generalLedgerTransactionDto.ProcessMode);
            Assert.AreEqual("123456", generalLedgerTransactionDto.SubmittedBy.Id);

            var generalLedgerTransaction = generalLedgerTransactionDto.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);

            Assert.AreEqual(referenceNumber, generalLedgerTransaction.ReferenceNumber);
            Assert.AreEqual(DateTime.Now.Date, generalLedgerTransaction.LedgerDate.Value.Date);
            Assert.AreEqual(Dtos.EnumProperties.GeneralLedgerTransactionType.Donation, generalLedgerTransaction.Type);

            Assert.IsNotNull(generalLedgerTransaction.TransactionDetailLines);
            var transactionDetailLine =
                generalLedgerTransaction.TransactionDetailLines.FirstOrDefault(x => x.Description == "DESC");

            Assert.AreEqual("DESC", transactionDetailLine.Description);
            Assert.AreEqual(Dtos.EnumProperties.CurrencyCodes.USD, transactionDetailLine.Amount.Currency);
            Assert.AreEqual(25, transactionDetailLine.Amount.Value);
            Assert.AreEqual("123456", transactionDetailLine.SubmittedBy.Id);
            Assert.AreEqual(Dtos.EnumProperties.CreditOrDebit.Credit, transactionDetailLine.Type);
            Assert.AreEqual(string.Concat(accountNumber, "*", projectId), transactionDetailLine.AccountingString);

        }
     
        #endregion

        #region CreateGeneralLedgerTransactionsAsync test methods
      
        [TestMethod]
        public async Task CreateGeneralLedgerTransactions_CreateAsync()
        {

            const string accountingString = "01-02-03-04-05550-66077*A1";
            const string referenceNumber = "GL122312321";
            const string projectId = "A1";
            const string id = "0001234";

            var generalLedgerTransaction = GetTestGeneralLedgerTransactions().FirstOrDefault(x => x.Id == id);

            // Get the necessary configuration settings and build the GL user object.
            _testGlAccountStructure = await _testGlConfigurationRepository.GetAccountStructureAsync();
            _testGlClassConfiguration = await _testGlConfigurationRepository.GetClassConfigurationAsync();
            var generalLedgerUser = await _testGeneralLedgerUserRepository.GetGeneralLedgerUserAsync(_glUserFactoryAll.CurrentUser.PersonId, _testGlAccountStructure.FullAccessRole, _testGlClassConfiguration.ClassificationName, _testGlClassConfiguration.ExpenseClassValues);

            var generalLedgerConfigurationRepository = new Mock<IGeneralLedgerConfigurationRepository>();
            generalLedgerConfigurationRepository.Setup(x => x.GetAccountStructureAsync()).ReturnsAsync(_testGlAccountStructure);

             generalLedgerConfigurationRepository.Setup(x => x.GetClassConfigurationAsync())
                .ReturnsAsync(_testGlClassConfiguration);

            var generalLedgerUserRepository = new Mock<IGeneralLedgerUserRepository>();
             generalLedgerUserRepository.Setup(x => x.GetGeneralLedgerUserAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<string>>()))
                .ReturnsAsync(generalLedgerUser);

            _personRepositoryMock.Setup(pr => pr.IsCorpAsync(It.IsAny<string>())).ReturnsAsync(false);
            _personRepositoryMock.Setup(x => x.GetPersonIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("0002839");

            var glTransaction = new GeneralLedgerTransaction()
            {
                Id = generalLedgerTransaction.Id,
                ProcessMode = generalLedgerTransaction.ProcessMode.ToString()
            };

            var transaction = generalLedgerTransaction.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);

            var genLedgrTransaction = new GenLedgrTransaction("DN", transaction.LedgerDate)
            {
                ReferenceNumber = transaction.ReferenceNumber,
                TransactionTypeReferenceDate = transaction.TransactionTypeReferenceDate,
                TransactionNumber = transaction.TransactionNumber

            };

            var transactionDetail = transaction.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);

            var genLedgrTransactionDetail = new GenLedgrTransactionDetail(transactionDetail.AccountingString, projectId, transactionDetail.Description,
                CreditOrDebit.Credit, new AmountAndCurrency(25, CurrencyCodes.USD));
            var genLedgrTransactionDetail2 = new GenLedgrTransactionDetail(transactionDetail.AccountingString, projectId, transactionDetail.Description,
                CreditOrDebit.Debit, new AmountAndCurrency(25, CurrencyCodes.USD));

            genLedgrTransaction.TransactionDetailLines = new List<GenLedgrTransactionDetail>() { genLedgrTransactionDetail, genLedgrTransactionDetail2 };

            glTransaction.GeneralLedgerTransactions = new List<GenLedgrTransaction>() { genLedgrTransaction };
            
            _generalLedgerTransactionRepository.Setup(x => x.GetByIdAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>())).ReturnsAsync(glTransaction);
            _generalLedgerTransactionRepository.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>(), _testGlAccountStructure)).ReturnsAsync(glTransaction);


            var generalLedgerTransactionDto =
                await _generalLedgerTransactionService.CreateAsync(generalLedgerTransaction);

            Assert.IsNotNull(generalLedgerTransactionDto);
            Assert.AreEqual(Dtos.EnumProperties.ProcessMode.Update, generalLedgerTransactionDto.ProcessMode);

            var actual =
                generalLedgerTransactionDto.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);
            var expected = 
                generalLedgerTransaction.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);

            Assert.AreEqual(expected.ReferenceNumber, actual.ReferenceNumber);
            Assert.AreEqual(expected.LedgerDate.Value.Date, actual.LedgerDate.Value.Date);
            Assert.AreEqual(Dtos.EnumProperties.GeneralLedgerTransactionType.Donation, actual.Type);

            Assert.IsNotNull(actual.TransactionDetailLines);
            var actualTransactionDetailLine =
                actual.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);
            var expectedTransactionDetailLine =
               expected.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);
            Assert.AreEqual(expectedTransactionDetailLine.Description, actualTransactionDetailLine.Description);
            Assert.AreEqual(expectedTransactionDetailLine.Amount.Currency, actualTransactionDetailLine.Amount.Currency);
            Assert.AreEqual(expectedTransactionDetailLine.Amount.Value, actualTransactionDetailLine.Amount.Value);
            Assert.AreEqual(expectedTransactionDetailLine.Type, actualTransactionDetailLine.Type);
            Assert.AreEqual(expectedTransactionDetailLine.AccountingString, actualTransactionDetailLine.AccountingString);

        }

        [TestMethod]
        public async Task CreateGeneralLedgerTransactions_Create2Async()
        {

            const string accountingString = "01-02-03-04-05550-66077*A1";
            const string referenceNumber = "GL122312321";
            const string projectId = "A1";
            const string id = "0001234";

            var generalLedgerTransaction = GetTestGeneralLedgerTransactions2().FirstOrDefault(x => x.Id == id);

            // Get the necessary configuration settings and build the GL user object.
            _testGlAccountStructure = await _testGlConfigurationRepository.GetAccountStructureAsync();
            _testGlClassConfiguration = await _testGlConfigurationRepository.GetClassConfigurationAsync();
            var generalLedgerUser = await _testGeneralLedgerUserRepository.GetGeneralLedgerUserAsync(_glUserFactoryAll.CurrentUser.PersonId, _testGlAccountStructure.FullAccessRole, _testGlClassConfiguration.ClassificationName, _testGlClassConfiguration.ExpenseClassValues);

            var generalLedgerConfigurationRepository = new Mock<IGeneralLedgerConfigurationRepository>();
            generalLedgerConfigurationRepository.Setup(x => x.GetAccountStructureAsync()).ReturnsAsync(_testGlAccountStructure);

            generalLedgerConfigurationRepository.Setup(x => x.GetClassConfigurationAsync())
               .ReturnsAsync(_testGlClassConfiguration);

            var generalLedgerUserRepository = new Mock<IGeneralLedgerUserRepository>();
            generalLedgerUserRepository.Setup(x => x.GetGeneralLedgerUserAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<string>>()))
               .ReturnsAsync(generalLedgerUser);

            _personRepositoryMock.Setup(pr => pr.IsCorpAsync(It.IsAny<string>())).ReturnsAsync(false);
            _personRepositoryMock.Setup(pr => pr.GetPersonGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("123456");
            _personRepositoryMock.Setup(pr => pr.GetPersonIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("123456");

            var glTransaction = new GeneralLedgerTransaction()
            {
                Id = generalLedgerTransaction.Id,
                ProcessMode = generalLedgerTransaction.ProcessMode.ToString(),
                SubmittedBy = generalLedgerTransaction.SubmittedBy.ToString()

            };

            var transaction = generalLedgerTransaction.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);

            var genLedgrTransaction = new GenLedgrTransaction("DN", transaction.LedgerDate)
            {
                ReferenceNumber = transaction.ReferenceNumber,
                TransactionTypeReferenceDate = transaction.TransactionTypeReferenceDate,
                TransactionNumber = transaction.TransactionNumber

            };

            var transactionDetail = transaction.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);

            var genLedgrTransactionDetail = new GenLedgrTransactionDetail(transactionDetail.AccountingString, projectId, transactionDetail.Description,
                CreditOrDebit.Credit, new AmountAndCurrency(25, CurrencyCodes.USD));
            genLedgrTransactionDetail.SubmittedBy = "123456";
            var genLedgrTransactionDetail2 = new GenLedgrTransactionDetail(transactionDetail.AccountingString, projectId, transactionDetail.Description,
                CreditOrDebit.Debit, new AmountAndCurrency(25, CurrencyCodes.USD));
            genLedgrTransactionDetail2.SubmittedBy = "123456";
            genLedgrTransaction.TransactionDetailLines = new List<GenLedgrTransactionDetail>() { genLedgrTransactionDetail, genLedgrTransactionDetail2 };

            glTransaction.GeneralLedgerTransactions = new List<GenLedgrTransaction>() { genLedgrTransaction };

            _generalLedgerTransactionRepository.Setup(x => x.GetByIdAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>())).ReturnsAsync(glTransaction);
            _generalLedgerTransactionRepository.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>(), _testGlAccountStructure)).ReturnsAsync(glTransaction);


            var generalLedgerTransactionDto =
                await _generalLedgerTransactionService.Create2Async(generalLedgerTransaction);

            Assert.IsNotNull(generalLedgerTransactionDto);
            Assert.AreEqual(Dtos.EnumProperties.ProcessMode.Update, generalLedgerTransactionDto.ProcessMode);
            Assert.AreEqual(generalLedgerTransaction.SubmittedBy.Id, generalLedgerTransactionDto.SubmittedBy.Id);

            var actual =
                generalLedgerTransactionDto.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);
            var expected =
                generalLedgerTransaction.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);

            Assert.AreEqual(expected.ReferenceNumber, actual.ReferenceNumber);
            Assert.AreEqual(expected.LedgerDate.Value.Date, actual.LedgerDate.Value.Date);
            Assert.AreEqual(Dtos.EnumProperties.GeneralLedgerTransactionType.Donation, actual.Type);

            Assert.IsNotNull(actual.TransactionDetailLines);
            var actualTransactionDetailLine =
                actual.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);
            var expectedTransactionDetailLine =
               expected.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);
            Assert.AreEqual(expectedTransactionDetailLine.Description, actualTransactionDetailLine.Description);
            Assert.AreEqual(expectedTransactionDetailLine.Amount.Currency, actualTransactionDetailLine.Amount.Currency);
            Assert.AreEqual(expectedTransactionDetailLine.Amount.Value, actualTransactionDetailLine.Amount.Value);
            Assert.AreEqual(expectedTransactionDetailLine.Type, actualTransactionDetailLine.Type);
            Assert.AreEqual(expectedTransactionDetailLine.SubmittedBy.Id, actualTransactionDetailLine.SubmittedBy.Id);
            Assert.AreEqual(expectedTransactionDetailLine.AccountingString, actualTransactionDetailLine.AccountingString);

        }

        #endregion

        #region UpdateGeneralLedgerTransactionsAsync test methods

        [TestMethod]
        public async Task UpdateGeneralLedgerTransactions_UpdateAsync()
        {

            const string accountingString = "01-02-03-04-05550-66077*A1";
            const string referenceNumber = "GL122312321";
            const string projectId = "A1";
            const string id = "0001234";

            var generalLedgerTransaction = GetTestGeneralLedgerTransactions().FirstOrDefault(x => x.Id == id);

            // Get the necessary configuration settings and build the GL user object.
            _testGlAccountStructure = await _testGlConfigurationRepository.GetAccountStructureAsync();
            _testGlClassConfiguration = await _testGlConfigurationRepository.GetClassConfigurationAsync();
            var generalLedgerUser = await _testGeneralLedgerUserRepository.GetGeneralLedgerUserAsync(_glUserFactoryAll.CurrentUser.PersonId, _testGlAccountStructure.FullAccessRole, _testGlClassConfiguration.ClassificationName, _testGlClassConfiguration.ExpenseClassValues);

            var generalLedgerConfigurationRepository = new Mock<IGeneralLedgerConfigurationRepository>();
            generalLedgerConfigurationRepository.Setup(x => x.GetAccountStructureAsync()).ReturnsAsync(_testGlAccountStructure);

            generalLedgerConfigurationRepository.Setup(x => x.GetClassConfigurationAsync())
               .ReturnsAsync(_testGlClassConfiguration);

            var generalLedgerUserRepository = new Mock<IGeneralLedgerUserRepository>();
            generalLedgerUserRepository.Setup(x => x.GetGeneralLedgerUserAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<string>>()))
               .ReturnsAsync(generalLedgerUser);

            _personRepositoryMock.Setup(pr => pr.IsCorpAsync(It.IsAny<string>())).ReturnsAsync(false);
            _personRepositoryMock.Setup(x => x.GetPersonIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("0002839");

            var glTransaction = new GeneralLedgerTransaction()
            {
                Id = generalLedgerTransaction.Id,
                ProcessMode = generalLedgerTransaction.ProcessMode.ToString()
            };

            var transaction = generalLedgerTransaction.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);

            var genLedgrTransaction = new GenLedgrTransaction("DN", transaction.LedgerDate)
            {
                ReferenceNumber = transaction.ReferenceNumber,
                TransactionTypeReferenceDate = transaction.TransactionTypeReferenceDate,
                TransactionNumber = transaction.TransactionNumber

            };

            var transactionDetail = transaction.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);

            var genLedgrTransactionDetail = new GenLedgrTransactionDetail(transactionDetail.AccountingString, projectId, transactionDetail.Description,
                CreditOrDebit.Credit, new AmountAndCurrency(25, CurrencyCodes.USD));
            var genLedgrTransactionDetail2 = new GenLedgrTransactionDetail(transactionDetail.AccountingString, projectId, transactionDetail.Description,
                CreditOrDebit.Debit, new AmountAndCurrency(25, CurrencyCodes.USD));

            genLedgrTransaction.TransactionDetailLines = new List<GenLedgrTransactionDetail>() { genLedgrTransactionDetail, genLedgrTransactionDetail2 };

            glTransaction.GeneralLedgerTransactions = new List<GenLedgrTransaction>() { genLedgrTransaction };

            _generalLedgerTransactionRepository.Setup(x => x.GetByIdAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>())).ReturnsAsync(glTransaction);
            _generalLedgerTransactionRepository.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>(), _testGlAccountStructure)).ReturnsAsync(glTransaction);
            _generalLedgerTransactionRepository.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>(), _testGlAccountStructure)).ReturnsAsync(glTransaction);


            var generalLedgerTransactionDto =
                await _generalLedgerTransactionService.UpdateAsync(generalLedgerTransaction.Id, generalLedgerTransaction);

            Assert.IsNotNull(generalLedgerTransactionDto);
            Assert.AreEqual(Dtos.EnumProperties.ProcessMode.Update, generalLedgerTransactionDto.ProcessMode);

            var actual =
                generalLedgerTransactionDto.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);
            var expected =
                generalLedgerTransaction.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);

            Assert.AreEqual(expected.ReferenceNumber, actual.ReferenceNumber);
            Assert.AreEqual(expected.LedgerDate.Value.Date, actual.LedgerDate.Value.Date);
            Assert.AreEqual(Dtos.EnumProperties.GeneralLedgerTransactionType.Donation, actual.Type);

            Assert.IsNotNull(actual.TransactionDetailLines);
            var actualTransactionDetailLine =
                actual.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);
            var expectedTransactionDetailLine =
               expected.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);
            Assert.AreEqual(expectedTransactionDetailLine.Description, actualTransactionDetailLine.Description);
            Assert.AreEqual(expectedTransactionDetailLine.Amount.Currency, actualTransactionDetailLine.Amount.Currency);
            Assert.AreEqual(expectedTransactionDetailLine.Amount.Value, actualTransactionDetailLine.Amount.Value);
            Assert.AreEqual(expectedTransactionDetailLine.Type, actualTransactionDetailLine.Type);
            Assert.AreEqual(expectedTransactionDetailLine.AccountingString, actualTransactionDetailLine.AccountingString);

        }

        [TestMethod]
        public async Task UpdateGeneralLedgerTransactions_Update2Async()
        {

            const string accountingString = "01-02-03-04-05550-66077*A1";
            const string referenceNumber = "GL122312321";
            const string projectId = "A1";
            const string id = "0001234";

            var generalLedgerTransaction = GetTestGeneralLedgerTransactions2().FirstOrDefault(x => x.Id == id);

            // Get the necessary configuration settings and build the GL user object.
            _testGlAccountStructure = await _testGlConfigurationRepository.GetAccountStructureAsync();
            _testGlClassConfiguration = await _testGlConfigurationRepository.GetClassConfigurationAsync();
            var generalLedgerUser = await _testGeneralLedgerUserRepository.GetGeneralLedgerUserAsync(_glUserFactoryAll.CurrentUser.PersonId, _testGlAccountStructure.FullAccessRole, _testGlClassConfiguration.ClassificationName, _testGlClassConfiguration.ExpenseClassValues);

            var generalLedgerConfigurationRepository = new Mock<IGeneralLedgerConfigurationRepository>();
            generalLedgerConfigurationRepository.Setup(x => x.GetAccountStructureAsync()).ReturnsAsync(_testGlAccountStructure);

            generalLedgerConfigurationRepository.Setup(x => x.GetClassConfigurationAsync())
               .ReturnsAsync(_testGlClassConfiguration);

            var generalLedgerUserRepository = new Mock<IGeneralLedgerUserRepository>();
            generalLedgerUserRepository.Setup(x => x.GetGeneralLedgerUserAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<string>>()))
               .ReturnsAsync(generalLedgerUser);

            _personRepositoryMock.Setup(pr => pr.IsCorpAsync(It.IsAny<string>())).ReturnsAsync(false);
            _personRepositoryMock.Setup(pr => pr.GetPersonGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("123456");
            _personRepositoryMock.Setup(pr => pr.GetPersonIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync("123456");
            var glTransaction = new GeneralLedgerTransaction()
            {
                Id = generalLedgerTransaction.Id,
                ProcessMode = generalLedgerTransaction.ProcessMode.ToString(),
                SubmittedBy = generalLedgerTransaction.SubmittedBy.ToString()
            };

            var transaction = generalLedgerTransaction.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);

            var genLedgrTransaction = new GenLedgrTransaction("DN", transaction.LedgerDate)
            {
                ReferenceNumber = transaction.ReferenceNumber,
                TransactionTypeReferenceDate = transaction.TransactionTypeReferenceDate,
                TransactionNumber = transaction.TransactionNumber

            };

            var transactionDetail = transaction.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);

            var genLedgrTransactionDetail = new GenLedgrTransactionDetail(transactionDetail.AccountingString, projectId, transactionDetail.Description,
                CreditOrDebit.Credit, new AmountAndCurrency(25, CurrencyCodes.USD));
            genLedgrTransactionDetail.SubmittedBy = "123456";
            var genLedgrTransactionDetail2 = new GenLedgrTransactionDetail(transactionDetail.AccountingString, projectId, transactionDetail.Description,
                CreditOrDebit.Debit, new AmountAndCurrency(25, CurrencyCodes.USD));
            genLedgrTransactionDetail2.SubmittedBy = "123456";
            genLedgrTransaction.TransactionDetailLines = new List<GenLedgrTransactionDetail>() { genLedgrTransactionDetail, genLedgrTransactionDetail2 };

            glTransaction.GeneralLedgerTransactions = new List<GenLedgrTransaction>() { genLedgrTransaction };

            _generalLedgerTransactionRepository.Setup(x => x.GetByIdAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>())).ReturnsAsync(glTransaction);
            _generalLedgerTransactionRepository.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>(), _testGlAccountStructure)).ReturnsAsync(glTransaction);
            _generalLedgerTransactionRepository.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>(), _testGlAccountStructure)).ReturnsAsync(glTransaction);


            var generalLedgerTransactionDto =
                await _generalLedgerTransactionService.Update2Async(generalLedgerTransaction.Id, generalLedgerTransaction);

            Assert.IsNotNull(generalLedgerTransactionDto);
            Assert.AreEqual(Dtos.EnumProperties.ProcessMode.Update, generalLedgerTransactionDto.ProcessMode);
            Assert.AreEqual(generalLedgerTransaction.SubmittedBy.Id, generalLedgerTransactionDto.SubmittedBy.Id);

            var actual =
                generalLedgerTransactionDto.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);
            var expected =
                generalLedgerTransaction.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);

            Assert.AreEqual(expected.ReferenceNumber, actual.ReferenceNumber);
            Assert.AreEqual(expected.LedgerDate.Value.Date, actual.LedgerDate.Value.Date);
            Assert.AreEqual(Dtos.EnumProperties.GeneralLedgerTransactionType.Donation, actual.Type);

            Assert.IsNotNull(actual.TransactionDetailLines);
            var actualTransactionDetailLine =
                actual.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);
            var expectedTransactionDetailLine =
               expected.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);
            Assert.AreEqual(expectedTransactionDetailLine.Description, actualTransactionDetailLine.Description);
            Assert.AreEqual(expectedTransactionDetailLine.Amount.Currency, actualTransactionDetailLine.Amount.Currency);
            Assert.AreEqual(expectedTransactionDetailLine.Amount.Value, actualTransactionDetailLine.Amount.Value);
            Assert.AreEqual(expectedTransactionDetailLine.Type, actualTransactionDetailLine.Type);
            Assert.AreEqual(expectedTransactionDetailLine.SubmittedBy.Id, actualTransactionDetailLine.SubmittedBy.Id);
            Assert.AreEqual(expectedTransactionDetailLine.AccountingString, actualTransactionDetailLine.AccountingString);

        }


        #endregion

        #region DeleteGeneralLedgerTransactionsAsync test methods

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task DeleteGeneralLedgerTransactions_DeleteAsync_EmptyId()
        {

            const string accountingString = "01-02-03-04-05550-66077*A1";
            const string referenceNumber = "GL122312321";
            const string projectId = "A1";
            const string id = "0001234";

            var generalLedgerTransaction = GetTestGeneralLedgerTransactions().FirstOrDefault(x => x.Id == id);

            // Get the necessary configuration settings and build the GL user object.
            _testGlAccountStructure = await _testGlConfigurationRepository.GetAccountStructureAsync();
            _testGlClassConfiguration = await _testGlConfigurationRepository.GetClassConfigurationAsync();
            var generalLedgerUser = await _testGeneralLedgerUserRepository.GetGeneralLedgerUserAsync(_glUserFactoryAll.CurrentUser.PersonId, _testGlAccountStructure.FullAccessRole, _testGlClassConfiguration.ClassificationName, _testGlClassConfiguration.ExpenseClassValues);

            var generalLedgerConfigurationRepository = new Mock<IGeneralLedgerConfigurationRepository>();
            generalLedgerConfigurationRepository.Setup(x => x.GetAccountStructureAsync()).ReturnsAsync(_testGlAccountStructure);

            generalLedgerConfigurationRepository.Setup(x => x.GetClassConfigurationAsync())
               .ReturnsAsync(_testGlClassConfiguration);

            var generalLedgerUserRepository = new Mock<IGeneralLedgerUserRepository>();
            generalLedgerUserRepository.Setup(x => x.GetGeneralLedgerUserAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<List<string>>()))
               .ReturnsAsync(generalLedgerUser);

            var glTransaction = new GeneralLedgerTransaction()
            {
                Id = generalLedgerTransaction.Id,
                ProcessMode = generalLedgerTransaction.ProcessMode.ToString()
            };

            var transaction = generalLedgerTransaction.Transactions.FirstOrDefault(x => x.ReferenceNumber == referenceNumber);

            var genLedgrTransaction = new GenLedgrTransaction("DN", transaction.LedgerDate)
            {
                ReferenceNumber = transaction.ReferenceNumber,
                TransactionTypeReferenceDate = transaction.TransactionTypeReferenceDate,
                TransactionNumber = transaction.TransactionNumber

            };

            var transactionDetail = transaction.TransactionDetailLines.FirstOrDefault(x => x.AccountingString == accountingString);

            var genLedgrTransactionDetail = new GenLedgrTransactionDetail(transactionDetail.AccountingString, projectId, transactionDetail.Description,
                CreditOrDebit.Credit, new AmountAndCurrency(25, CurrencyCodes.USD));

            genLedgrTransaction.TransactionDetailLines = new List<GenLedgrTransactionDetail>() { genLedgrTransactionDetail };

            glTransaction.GeneralLedgerTransactions = new List<GenLedgrTransaction>() { genLedgrTransaction };

            _generalLedgerTransactionRepository.Setup(x => x.GetByIdAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>())).ReturnsAsync(glTransaction);
            _generalLedgerTransactionRepository.Setup(x => x.CreateAsync(It.IsAny<GeneralLedgerTransaction>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>(), _testGlAccountStructure)).ReturnsAsync(glTransaction);
            _generalLedgerTransactionRepository.Setup(x => x.UpdateAsync(It.IsAny<string>(), It.IsAny<GeneralLedgerTransaction>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>(),_testGlAccountStructure)).ReturnsAsync(glTransaction);
            _generalLedgerTransactionRepository.Setup(x => x.DeleteAsync(It.IsAny<string>())).ReturnsAsync(glTransaction);

            
            await _generalLedgerTransactionService.DeleteAsync("");

        }


        #endregion

        #region Build service method
        /// <summary>
        /// Builds multiple cost center service objects.
        /// </summary>
        private void BuildValidGeneralLedgerTransactionService()
        {
            #region Initialize mock objects
         
            var roleRepository = new Mock<IRoleRepository>().Object;
            var loggerObject = new Mock<ILogger>().Object;

            _generalLedgerTransactionRepository = new Mock<IGeneralLedgerTransactionRepository>();

            var generalLedgerTransactionRepository = _generalLedgerTransactionRepository.Object; 

            //   var currentUserFactory = new Mock<ICurrentUserFactory>().Object;
            
            //testGeneralLedgerTransactionRepository = new TestGeneralLedgerTransactionRepository();
            _testGeneralLedgerUserRepository = new TestGeneralLedgerUserRepository();
            _testGlConfigurationRepository = new TestGeneralLedgerConfigurationRepository();

            // Set up and mock the adapter, and setup the GetAdapter method.
            var adapterRegistry = new Mock<IAdapterRegistry>();

            _personRepositoryMock = new Mock<IPersonRepository>();
            
            #endregion

            #region Set up the service

            // Set up the current user with all cost centers and set up the service.
            _generalLedgerTransactionService = new GeneralLedgerTransactionService
            (generalLedgerTransactionRepository, _personRepositoryMock.Object, _testGlConfigurationRepository, _testGeneralLedgerUserRepository, adapterRegistry.Object, _glUserFactoryAll, roleRepository, loggerObject);
           
            #endregion
        }
        #endregion

        #region Create Data

        private List<Dtos.GeneralLedgerTransaction> GetTestGeneralLedgerTransactions()
        {
            var generalLedgerTransactions = new List<Dtos.GeneralLedgerTransaction>();

            #region record 1
            var generalLedgerDetailDtoProperty1Record1 = new GeneralLedgerDetailDtoProperty()
            {
                AccountingString = "01-02-03-04-05550-66077*A1",
                Description = "description",
                SequenceNumber = 1,
                Amount = new AmountDtoProperty() { Currency = Dtos.EnumProperties.CurrencyCodes.USD, Value = 25 },
                Type = Dtos.EnumProperties.CreditOrDebit.Credit
            };

            var generalLedgerDetailDtoProperty2Record1 = new GeneralLedgerDetailDtoProperty()
            {
                AccountingString = "01-02-03-04-05550-66077*A1",
                Description = "description",
                SequenceNumber = 1,
                Amount = new AmountDtoProperty() { Currency = Dtos.EnumProperties.CurrencyCodes.USD, Value = 25 },
                Type = Dtos.EnumProperties.CreditOrDebit.Debit
            };

            var gltDtoProperty = new GeneralLedgerTransactionDtoProperty()
            {
                LedgerDate = DateTimeOffset.Now.DateTime,
                ReferenceNumber = "GL122312321",
                Reference = new Dtos.DtoProperties.GeneralLedgerReferenceDtoProperty()
                {
                    //Organization = new GuidObject2("B17F7796-53D1-403C-A883-934D4DE04F1D"),
                    Person = new GuidObject2("C17F7796-53D1-403C-A883-934D4DE04F1D"),
                },
                TransactionNumber = "1",
                TransactionTypeReferenceDate = DateTimeOffset.Now,
                Type = GeneralLedgerTransactionType.Donation,
                TransactionDetailLines = new List<GeneralLedgerDetailDtoProperty>() { generalLedgerDetailDtoProperty1Record1, generalLedgerDetailDtoProperty2Record1 }
            };

            var generalLedgerTransaction1 = new Dtos.GeneralLedgerTransaction
            {
                Id = "0001234",
                ProcessMode = ProcessMode.Update,
                Transactions = new List<GeneralLedgerTransactionDtoProperty>() { gltDtoProperty }
            };

            generalLedgerTransactions.Add(generalLedgerTransaction1);
            #endregion 
             
            #region record 2
            var transactions = new List<GeneralLedgerTransactionDtoProperty>(); 
            
            var generalLedgerDetailDtoProperty1 = new GeneralLedgerDetailDtoProperty()
            {
                AccountingString = "01-02-03-04-05550-66078*A1",
                Description = "US",
                SequenceNumber = 1,
                Amount = new AmountDtoProperty() { Currency = Dtos.EnumProperties.CurrencyCodes.USD, Value = 25 },
                Type = Dtos.EnumProperties.CreditOrDebit.Credit
            };

            var generalLedgerDetailDtoProperty2 = new GeneralLedgerDetailDtoProperty()
            {
                AccountingString = "01-02-03-04-05550-66078*A1",
                Description = "US",
                SequenceNumber = 1,
                Amount = new AmountDtoProperty() { Currency = Dtos.EnumProperties.CurrencyCodes.USD, Value = 25 },
                Type = Dtos.EnumProperties.CreditOrDebit.Debit
            };        

            var gltDtoProperty1 = new GeneralLedgerTransactionDtoProperty()
            {
                LedgerDate = DateTimeOffset.Now.DateTime,
                ReferenceNumber = "GL45323220",
                Reference = new GeneralLedgerReferenceDtoProperty()
                {
                   // Organization = new GuidObject2("B17F7796-53D1-403C-A883-934D4DE04F1D"),
                    Person = new GuidObject2("C17F7796-53D1-403C-A883-934D4DE04F1D")
                },
                TransactionNumber = "2",
                TransactionTypeReferenceDate = DateTimeOffset.Now,
                Type = GeneralLedgerTransactionType.Donation,
                TransactionDetailLines = new List<GeneralLedgerDetailDtoProperty>() { generalLedgerDetailDtoProperty1, generalLedgerDetailDtoProperty2 }
            };           
            transactions.Add(gltDtoProperty1);

            var generalLedgerDetailDtoProperty3 = new GeneralLedgerDetailDtoProperty()
            {
                AccountingString = "01-02-03-04-05550-66079*A1",
                Description = "Canadian",
                SequenceNumber = 2,
                Amount = new AmountDtoProperty() { Currency = Dtos.EnumProperties.CurrencyCodes.CAD, Value = 100 },
                Type = Dtos.EnumProperties.CreditOrDebit.Credit
            };

            var generalLedgerDetailDtoProperty4 = new GeneralLedgerDetailDtoProperty()
            {
                AccountingString = "01-02-03-04-05550-66079*A1",
                Description = "Canadian",
                SequenceNumber = 2,
                Amount = new AmountDtoProperty() { Currency = Dtos.EnumProperties.CurrencyCodes.CAD, Value = 100 },
                Type = Dtos.EnumProperties.CreditOrDebit.Debit
            };

            var gltDtoProperty2 = new GeneralLedgerTransactionDtoProperty()
            {
                LedgerDate = DateTimeOffset.Now.DateTime,
                ReferenceNumber = "GL45323221",
                Reference = new GeneralLedgerReferenceDtoProperty()
                {
                    //Organization = new GuidObject2("B17F7796-53D1-403C-A883-934D4DE04F1D"),
                    Person = new GuidObject2("C17F7796-53D1-403C-A883-934D4DE04F1D")
                },
                TransactionNumber = "2",
                TransactionTypeReferenceDate = DateTimeOffset.Now,
                Type = GeneralLedgerTransactionType.Pledge,
                TransactionDetailLines = new List<GeneralLedgerDetailDtoProperty>() { generalLedgerDetailDtoProperty3, generalLedgerDetailDtoProperty4 }
            };
            transactions.Add(gltDtoProperty2);

            var generalLedgerTransaction2 = new Dtos.GeneralLedgerTransaction
            {
                Id = "0002345",
                ProcessMode = ProcessMode.Update,
                Transactions = transactions
            };

            generalLedgerTransactions.Add(generalLedgerTransaction2 );
           #endregion

            return generalLedgerTransactions;
        }

        private List<Dtos.GeneralLedgerTransaction2> GetTestGeneralLedgerTransactions2()
        {
            var generalLedgerTransactions = new List<Dtos.GeneralLedgerTransaction2>();

            #region record 1
            var generalLedgerDetailDtoProperty1Record1 = new GeneralLedgerDetailDtoProperty2()
            {
                AccountingString = "01-02-03-04-05550-66077*A1",
                Description = "description",
                SequenceNumber = 1,
                SubmittedBy = new GuidObject2 () { Id = "123456"},
                Amount = new AmountDtoProperty() { Currency = Dtos.EnumProperties.CurrencyCodes.USD, Value = 25 },
                Type = Dtos.EnumProperties.CreditOrDebit.Credit
            };

            var generalLedgerDetailDtoProperty2Record1 = new GeneralLedgerDetailDtoProperty2()
            {
                AccountingString = "01-02-03-04-05550-66077*A1",
                Description = "description",
                SequenceNumber = 1,
                SubmittedBy = new GuidObject2() { Id = "123456" },
                Amount = new AmountDtoProperty() { Currency = Dtos.EnumProperties.CurrencyCodes.USD, Value = 25 },
                Type = Dtos.EnumProperties.CreditOrDebit.Debit
            };

            var gltDtoProperty = new GeneralLedgerTransactionDtoProperty2()
            {
                LedgerDate = DateTimeOffset.Now.DateTime,
                ReferenceNumber = "GL122312321",
                Reference = new GeneralLedgerReferenceDtoProperty()
                {
                    //Organization = new GuidObject2("B17F7796-53D1-403C-A883-934D4DE04F1D"),
                    Person = new GuidObject2("C17F7796-53D1-403C-A883-934D4DE04F1D")
                },
                TransactionNumber = "1",
                TransactionTypeReferenceDate = DateTimeOffset.Now,
                Type = GeneralLedgerTransactionType.Donation,
                TransactionDetailLines = new List<GeneralLedgerDetailDtoProperty2>() { generalLedgerDetailDtoProperty1Record1, generalLedgerDetailDtoProperty2Record1 }
            };

            var generalLedgerTransaction1 = new Dtos.GeneralLedgerTransaction2
            {
                Id = "0001234",
                ProcessMode = ProcessMode.Update,
                SubmittedBy = new GuidObject2() { Id = "123456" },
                Transactions = new List<GeneralLedgerTransactionDtoProperty2>() { gltDtoProperty }
            };

            generalLedgerTransactions.Add(generalLedgerTransaction1);
            #endregion

            #region record 2
            var transactions = new List<GeneralLedgerTransactionDtoProperty2>();

            var generalLedgerDetailDtoProperty1 = new GeneralLedgerDetailDtoProperty2()
            {
                AccountingString = "01-02-03-04-05550-66078*A1",
                Description = "US",
                SequenceNumber = 1,
                SubmittedBy = new GuidObject2() { Id = "123456" },
                Amount = new AmountDtoProperty() { Currency = Dtos.EnumProperties.CurrencyCodes.USD, Value = 25 },
                Type = Dtos.EnumProperties.CreditOrDebit.Credit
            };

            var generalLedgerDetailDtoProperty2 = new GeneralLedgerDetailDtoProperty2()
            {
                AccountingString = "01-02-03-04-05550-66078*A1",
                Description = "US",
                SequenceNumber = 1,
                SubmittedBy = new GuidObject2() { Id = "123456" },
                Amount = new AmountDtoProperty() { Currency = Dtos.EnumProperties.CurrencyCodes.USD, Value = 25 },
                Type = Dtos.EnumProperties.CreditOrDebit.Debit
            };

            var gltDtoProperty1 = new GeneralLedgerTransactionDtoProperty2()
            {
                LedgerDate = DateTimeOffset.Now.DateTime,
                ReferenceNumber = "GL45323220",
                Reference = new GeneralLedgerReferenceDtoProperty()
                {
                    Organization = new GuidObject2("B17F7796-53D1-403C-A883-934D4DE04F1D"),
                    //Person = new GuidObject2("C17F7796-53D1-403C-A883-934D4DE04F1D")
                },
                TransactionNumber = "2",
                TransactionTypeReferenceDate = DateTimeOffset.Now,
                Type = GeneralLedgerTransactionType.Donation,
                TransactionDetailLines = new List<GeneralLedgerDetailDtoProperty2>() { generalLedgerDetailDtoProperty1, generalLedgerDetailDtoProperty2 }
            };
            transactions.Add(gltDtoProperty1);

            var generalLedgerDetailDtoProperty3 = new GeneralLedgerDetailDtoProperty2()
            {
                AccountingString = "01-02-03-04-05550-66079*A1",
                Description = "Canadian",
                SequenceNumber = 2,
                SubmittedBy = new GuidObject2() { Id = "123456" },
                Amount = new AmountDtoProperty() { Currency = Dtos.EnumProperties.CurrencyCodes.CAD, Value = 100 },
                Type = Dtos.EnumProperties.CreditOrDebit.Credit
            };

            var generalLedgerDetailDtoProperty4 = new GeneralLedgerDetailDtoProperty2()
            {
                AccountingString = "01-02-03-04-05550-66079*A1",
                Description = "Canadian",
                SequenceNumber = 2,
                SubmittedBy = new GuidObject2() { Id = "123456" },
                Amount = new AmountDtoProperty() { Currency = Dtos.EnumProperties.CurrencyCodes.CAD, Value = 100 },
                Type = Dtos.EnumProperties.CreditOrDebit.Debit
            };

            var gltDtoProperty2 = new GeneralLedgerTransactionDtoProperty2()
            {
                LedgerDate = DateTimeOffset.Now.DateTime,
                ReferenceNumber = "GL45323221",
                Reference = new GeneralLedgerReferenceDtoProperty()
                {
                    Organization = new GuidObject2("B17F7796-53D1-403C-A883-934D4DE04F1D"),
                    //Person = new GuidObject2("C17F7796-53D1-403C-A883-934D4DE04F1D")
                },
                TransactionNumber = "2",
                TransactionTypeReferenceDate = DateTimeOffset.Now,
                Type = GeneralLedgerTransactionType.Pledge,
                TransactionDetailLines = new List<GeneralLedgerDetailDtoProperty2>() { generalLedgerDetailDtoProperty3, generalLedgerDetailDtoProperty4 }
            };
            transactions.Add(gltDtoProperty2);

            var generalLedgerTransaction2 = new Dtos.GeneralLedgerTransaction2
            {
                Id = "0002345",
                ProcessMode = ProcessMode.Update,
                SubmittedBy = new GuidObject2() { Id = "123456" },
                Transactions = transactions
            };

            generalLedgerTransactions.Add(generalLedgerTransaction2);
            #endregion

            return generalLedgerTransactions;
        }
           
        #endregion
    }
}