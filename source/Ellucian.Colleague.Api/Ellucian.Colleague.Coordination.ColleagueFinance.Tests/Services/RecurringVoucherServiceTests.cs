﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Colleague.Coordination.ColleagueFinance.Tests.UserFactories;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Colleague.Domain.ColleagueFinance.Tests;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Tests.Services
{
    /// <summary>
    /// This class tests that the service returns a specified recurring voucher.
    /// </summary>
    [TestClass]
    public class RecurringVoucherServiceTests : GeneralLedgerCurrentUser
    {
        #region Initialize and Cleanup
        private RecurringVoucherService service = null;
        private RecurringVoucherService service2 = null;
        private TestRecurringVoucherRepository testRecurringVoucherRepository;
        private UserFactorySubset currentUserFactory = new GeneralLedgerCurrentUser.UserFactorySubset();
        private Mock<IRecurringVoucherRepository> mockRecurringVoucherRepository;

        [TestInitialize]
        public void Initialize()
        {
            // Initialize the mock repository
            this.mockRecurringVoucherRepository = new Mock<IRecurringVoucherRepository>();

            // Build all service objects to use each of the user factories built above
            BuildValidRecurringVoucherService();
        }

        [TestCleanup]
        public void Cleanup()
        {
            // Reset all of the services and repository variables.
            service = null;
            service2 = null;
            testRecurringVoucherRepository = null;
            this.mockRecurringVoucherRepository = null;
        }
        #endregion

        #region Tests for GetRecurringVoucher

        [TestMethod]
        public async Task GetRecurringVoucher_AllData()
        {
            // Get a specified voucher
            var recurringVoucherId = "RV0001000";
            var recurringVoucherDto = await service.GetRecurringVoucherAsync(recurringVoucherId);

            // Build the projects accounting user object and get the list of project domain entities from the test repository.
            var recurringVoucherDomainEntity = await testRecurringVoucherRepository.GetRecurringVoucherAsync(recurringVoucherId);

            // Confirm that the data in the vouchers DTO matches the domain entity
            Assert.AreEqual(recurringVoucherDto.RecurringVoucherId, recurringVoucherDomainEntity.Id);
            Assert.AreEqual(recurringVoucherDto.Amount, recurringVoucherDomainEntity.Amount);
            Assert.AreEqual(recurringVoucherDto.ApType, recurringVoucherDomainEntity.ApType);
            Assert.AreEqual(recurringVoucherDto.Comments, recurringVoucherDomainEntity.Comments);
            Assert.AreEqual(recurringVoucherDto.Date, recurringVoucherDomainEntity.Date);
            Assert.AreEqual(recurringVoucherDto.InvoiceDate, recurringVoucherDomainEntity.InvoiceDate);
            Assert.AreEqual(recurringVoucherDto.InvoiceNumber, recurringVoucherDomainEntity.InvoiceNumber);
            Assert.AreEqual(recurringVoucherDto.MaintenanceDate, recurringVoucherDomainEntity.MaintenanceDate);
            Assert.AreEqual(recurringVoucherDto.Status.ToString(), recurringVoucherDomainEntity.Status.ToString());
            Assert.AreEqual(recurringVoucherDto.VendorId, recurringVoucherDomainEntity.VendorId);
            Assert.AreEqual(recurringVoucherDto.VendorName, recurringVoucherDomainEntity.VendorName);
            Assert.AreEqual(recurringVoucherDto.TotalScheduleAmountInLocalCurrency, recurringVoucherDomainEntity.TotalScheduleAmountInLocalCurrency, "Local total amounts should match.");
            Assert.AreEqual(recurringVoucherDto.TotalScheduleTaxAmountInLocalCurrency, recurringVoucherDomainEntity.TotalScheduleTaxAmountInLocalCurrency, "Local total amounts should match.");
            Assert.AreEqual(recurringVoucherDto.CurrencyCode, recurringVoucherDomainEntity.CurrencyCode, "Currency code should match.");

            // Confirm that the data in the approvers DTOs matches the domain entity
            for (int i = 0; i < recurringVoucherDto.Approvers.Count(); i++)
            {
                var approverDto = recurringVoucherDto.Approvers[i];
                var approverDomain = recurringVoucherDomainEntity.Approvers[i];
                Assert.AreEqual(approverDto.ApprovalName, approverDomain.ApprovalName);
                Assert.AreEqual(approverDto.ApprovalDate, approverDomain.ApprovalDate);
            }
        }

        [TestMethod]
        public async Task GetRecurringVoucher_StatusNotApproved()
        {
            var recurringVoucherId = "RV0001000";

            // Build the projects accounting user object and get the list of project domain entities from the test repository.
            var recurringVoucherDomainEntity = testRecurringVoucherRepository.CreateRecurringVoucherDomainEntity(RecurringVoucherStatus.NotApproved);

            // Get a specified recurring voucher

            var recurringVoucherDto = await service.GetRecurringVoucherAsync(recurringVoucherId);

            // Confirm that the data in the vouchers DTO matches the domain entity
            Assert.AreEqual(recurringVoucherDto.Status.ToString(), recurringVoucherDomainEntity.Status.ToString());
        }

        [TestMethod]
        public async Task GetRecurringVoucher_StatusVoided()
        {
            var recurringVoucherId = "RV0001000";

            // Build the projects accounting user object and get the list of project domain entities from the test repository.
            var recurringVoucherDomainEntity = testRecurringVoucherRepository.CreateRecurringVoucherDomainEntity(RecurringVoucherStatus.Voided);

            // Get a specified recurring voucher

            var recurringVoucherDto = await service.GetRecurringVoucherAsync(recurringVoucherId);

            // Confirm that the data in the vouchers DTO matches the domain entity
            Assert.AreEqual(recurringVoucherDto.Status.ToString(), recurringVoucherDomainEntity.Status.ToString());
        }

        [TestMethod]
        public async Task GetRecurringVoucher_StatusCancelled()
        {
            var recurringVoucherId = "RV0001000";

            // Build the projects accounting user object and get the list of project domain entities from the test repository.
            var recurringVoucherDomainEntity = testRecurringVoucherRepository.CreateRecurringVoucherDomainEntity(RecurringVoucherStatus.Cancelled);

            // Get a specified recurring voucher

            var recurringVoucherDto = await service.GetRecurringVoucherAsync(recurringVoucherId);

            // Confirm that the data in the vouchers DTO matches the domain entity
            Assert.AreEqual(recurringVoucherDto.Status.ToString(), recurringVoucherDomainEntity.Status.ToString());
        }

        [TestMethod]
        public async Task GetRecurringVoucher_StatusClosed()
        {
            var recurringVoucherId = "RV0001000";

            // Build the projects accounting user object and get the list of project domain entities from the test repository.
            var recurringVoucherDomainEntity = testRecurringVoucherRepository.CreateRecurringVoucherDomainEntity(RecurringVoucherStatus.Closed);

            // Get a specified recurring voucher

            var recurringVoucherDto = await service.GetRecurringVoucherAsync(recurringVoucherId);

            // Confirm that the data in the vouchers DTO matches the domain entity
            Assert.AreEqual(recurringVoucherDto.Status.ToString(), recurringVoucherDomainEntity.Status.ToString());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task GetRecurringVoucher_RepositoryReturnsNullObject()
        {
            // Mock the GetRecurringVoucher repository method to return a null object within the Service method
            RecurringVoucher nullRecurringVoucher = null;
            this.mockRecurringVoucherRepository.Setup<Task<RecurringVoucher>>(recurringVoucherRepo => recurringVoucherRepo.GetRecurringVoucherAsync(It.IsAny<string>())).Returns(Task.FromResult(nullRecurringVoucher));
            var recurringVoucherDto = await service2.GetRecurringVoucherAsync("RV0000001");
        }
        #endregion

        #region Build service method
        /// <summary>
        /// Builds multiple recurring voucher service objects.
        /// </summary>
        /// <returns>Nothing.</returns>
        private void BuildValidRecurringVoucherService()
        {
            // We need the unit tests to be independent of "real" implementations of these classes,
            // so we use Moq to create mock implementations that are based on the same interfaces
            var roleRepository = new Mock<IRoleRepository>().Object;
            var loggerObject = new Mock<ILogger>().Object;

            testRecurringVoucherRepository = new TestRecurringVoucherRepository();

            // Set up and mock the adapter, and setup the GetAdapter method.
            var adapterRegistry = new Mock<IAdapterRegistry>();
            var recurringVoucherDtoAdapter = new AutoMapperAdapter<Domain.ColleagueFinance.Entities.RecurringVoucher, Dtos.ColleagueFinance.RecurringVoucher>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Domain.ColleagueFinance.Entities.RecurringVoucher, Dtos.ColleagueFinance.RecurringVoucher>()).Returns(recurringVoucherDtoAdapter);

            // Set up the current user with a subset of projects and set up the service.
            service = new RecurringVoucherService(testRecurringVoucherRepository, adapterRegistry.Object, currentUserFactory, roleRepository, loggerObject);
            service2 = new RecurringVoucherService(this.mockRecurringVoucherRepository.Object, adapterRegistry.Object, currentUserFactory, roleRepository, loggerObject);
        }
        #endregion
    }
}
