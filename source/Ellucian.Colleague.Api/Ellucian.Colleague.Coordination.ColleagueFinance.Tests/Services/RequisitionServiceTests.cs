﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.ColleagueFinance.Services;
using Ellucian.Colleague.Coordination.ColleagueFinance.Tests.UserFactories;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Colleague.Domain.ColleagueFinance.Tests;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;

namespace Ellucian.Colleague.Coordination.ColleagueFinance.Tests.Services
{
    /// <summary>
    /// Test that the service returns a valid requisition.
    /// We use GeneralLedgerCurrentUser to mimic the user logged in.
    /// </summary>
    [TestClass]
    public class RequisitionServiceTests : GeneralLedgerCurrentUser
    {
        #region Initialize and Cleanup

        private RequisitionService service = null;
        private RequisitionService service2 = null;
        private TestRequisitionRepository testRequisitionRepository;
        private TestGeneralLedgerConfigurationRepository testGeneralLedgerConfigurationRepository;
        private TestGeneralLedgerUserRepository testGeneralLedgerUserRepository;
        private UserFactorySubset currentUserFactory = new GeneralLedgerCurrentUser.UserFactorySubset();
        private Mock<IRequisitionRepository> mockRequisitionRepository = null;

        [TestInitialize]
        public void Initialize()
        {
            // Set up mock requisition repository
            this.mockRequisitionRepository = new Mock<IRequisitionRepository>();

            // build all service objects to use in testing
            BuildValidRequisitionService();
        }

        [TestCleanup]
        public void Cleanup()
        {
            // Reset all of the services and repository variables
            service = null;
            service2 = null;
            testRequisitionRepository = null;
            testGeneralLedgerConfigurationRepository = null;
            testGeneralLedgerUserRepository = null;
            this.mockRequisitionRepository = null;
        }
        #endregion

        #region Tests for GetRequisition
        [TestMethod]
        public async Task GetRequisition()
        {
            var requisitionId = "9";
            var personId = "1";
            var requisitionDto = await service.GetRequisitionAsync(requisitionId);

            // Get the requisition domain entity from the test repository
            var requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);

            // Confirm that the data in the DTO matches the domain entity
            Assert.AreEqual(requisitionDto.Id, requisitionDomainEntity.Id);
            Assert.AreEqual(requisitionDto.Number, requisitionDomainEntity.Number);
            Assert.AreEqual(requisitionDto.Amount, requisitionDomainEntity.Amount);
            Assert.AreEqual(requisitionDto.ApType, requisitionDomainEntity.ApType);
            Assert.AreEqual(requisitionDto.BlanketPurchaseOrder, requisitionDomainEntity.BlanketPurchaseOrder);
            Assert.AreEqual(requisitionDto.Comments, requisitionDomainEntity.Comments);
            Assert.AreEqual(requisitionDto.CurrencyCode, requisitionDomainEntity.CurrencyCode);
            Assert.AreEqual(requisitionDto.Date, requisitionDomainEntity.Date);
            Assert.AreEqual(requisitionDto.DesiredDate, requisitionDomainEntity.DesiredDate);
            Assert.AreEqual(requisitionDto.InitiatorName, requisitionDomainEntity.InitiatorName);
            Assert.AreEqual(requisitionDto.InternalComments, requisitionDomainEntity.InternalComments);
            Assert.AreEqual(requisitionDto.MaintenanceDate, requisitionDomainEntity.MaintenanceDate);
            Assert.AreEqual(requisitionDto.RequestorName, requisitionDomainEntity.RequestorName);
            Assert.AreEqual(requisitionDto.ShipToCode, requisitionDomainEntity.ShipToCode);
            Assert.AreEqual(requisitionDto.Status.ToString(), requisitionDomainEntity.Status.ToString());
            Assert.AreEqual(requisitionDto.StatusDate, requisitionDomainEntity.StatusDate);
            Assert.AreEqual(requisitionDto.VendorId, requisitionDomainEntity.VendorId);
            Assert.AreEqual(requisitionDto.VendorName, requisitionDomainEntity.VendorName);

            // Confirm that the data in the approvers DTOs matches the domain entity
            for (int i = 0; i < requisitionDto.Approvers.Count(); i++)
            {
                var approverDto = requisitionDto.Approvers[i];
                var approverDomain = requisitionDomainEntity.Approvers[i];
                Assert.AreEqual(approverDto.ApprovalName, approverDomain.ApprovalName);
                Assert.AreEqual(approverDto.ApprovalDate, approverDomain.ApprovalDate);
            }

            // Confirm that the data in the list of purchase order DTOs matches the domain entity
            for (int i = 0; i < requisitionDto.PurchaseOrders.Count(); i++)
            {
                Assert.AreEqual(requisitionDto.PurchaseOrders[i], requisitionDomainEntity.PurchaseOrders[i]);
            }

            // Confirm that the data in the line item DTOs matches the domain entity
            for (int i = 0; i < requisitionDto.LineItems.Count(); i++)
            {
                var lineItemDto = requisitionDto.LineItems[i];
                var lineItemDomain = requisitionDomainEntity.LineItems[i];
                Assert.AreEqual(lineItemDto.Comments, lineItemDomain.Comments);
                Assert.AreEqual(lineItemDto.Description, lineItemDomain.Description);
                Assert.AreEqual(lineItemDto.DesiredDate, lineItemDomain.DesiredDate);
                Assert.AreEqual(lineItemDto.ExtendedPrice, lineItemDomain.ExtendedPrice);
                Assert.AreEqual(lineItemDto.Price, lineItemDomain.Price);
                Assert.AreEqual(lineItemDto.Quantity, lineItemDomain.Quantity);
                Assert.AreEqual(lineItemDto.TaxForm, lineItemDomain.TaxForm);
                Assert.AreEqual(lineItemDto.TaxFormCode, lineItemDomain.TaxFormCode);
                Assert.AreEqual(lineItemDto.TaxFormLocation, lineItemDomain.TaxFormLocation);
                Assert.AreEqual(lineItemDto.UnitOfIssue, lineItemDomain.UnitOfIssue);
                Assert.AreEqual(lineItemDto.VendorPart, lineItemDomain.VendorPart);

                // Confirm that the data in the line item GL distribution DTOs matches the domain entity
                for (int j = 0; j < lineItemDto.GlDistributions.Count(); j++)
                {
                    var glDistributionDto = lineItemDto.GlDistributions[j];
                    var glDistributionDomain = lineItemDomain.GlDistributions[j];
                    Assert.AreEqual(glDistributionDto.Amount, glDistributionDomain.Amount);
                    Assert.AreEqual(glDistributionDto.GlAccount, glDistributionDomain.GlAccountNumber);
                    Assert.AreEqual(glDistributionDto.ProjectLineItemCode, glDistributionDomain.ProjectLineItemCode);
                    Assert.AreEqual(glDistributionDto.ProjectNumber, glDistributionDomain.ProjectNumber);
                    Assert.AreEqual(glDistributionDto.Quantity, glDistributionDomain.Quantity);
                }

                // Confirm that the data in the line item tax DTOs matches the domain entity
                for (int k = 0; k < lineItemDto.LineItemTaxes.Count(); k++)
                {
                    var lineItemTaxDto = lineItemDto.LineItemTaxes[k];
                    var lineItemTaxDomain = lineItemDomain.LineItemTaxes[k];
                    Assert.AreEqual(lineItemTaxDto.TaxCode, lineItemTaxDomain.TaxCode);
                    Assert.AreEqual(lineItemTaxDto.TaxAmount, lineItemTaxDomain.TaxAmount);
                }
            }
        }

        [TestMethod]
        public async Task GetRequisition_OneGlAccountIsMasked()
        {
            var requisitionId = "999";
            var personId = "1";
            var requisitionDto = await service.GetRequisitionAsync(requisitionId);

            // Get the purchase order domain entity from the test repository
            var requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);

            // Confirm that the data in the DTO matches the domain entity
            Assert.AreEqual(requisitionDto.Id, requisitionDomainEntity.Id);

            // There should only be one line item.
            Assert.AreEqual(1, requisitionDomainEntity.LineItems.Count);
            Assert.AreEqual(1, requisitionDto.LineItems.Count);

            // Confirm that the DTO line item data matches the data in the domain entity.
            var lineItemDto = requisitionDto.LineItems.First();
            var lineItemEntity = requisitionDomainEntity.LineItems.First();

            Assert.AreEqual(lineItemDto.Comments, lineItemEntity.Comments);
            Assert.AreEqual(lineItemDto.Description, lineItemEntity.Description);
            Assert.AreEqual(lineItemDto.ExpectedDeliveryDate, lineItemEntity.ExpectedDeliveryDate);
            Assert.AreEqual(lineItemDto.ExtendedPrice, lineItemEntity.ExtendedPrice);
            Assert.AreEqual(lineItemDto.Price, lineItemEntity.Price);
            Assert.AreEqual(lineItemDto.Quantity, lineItemEntity.Quantity);
            Assert.AreEqual(lineItemDto.TaxForm, lineItemEntity.TaxForm);
            Assert.AreEqual(lineItemDto.TaxFormCode, lineItemEntity.TaxFormCode);
            Assert.AreEqual(lineItemDto.TaxFormLocation, lineItemEntity.TaxFormLocation);
            Assert.AreEqual(lineItemDto.UnitOfIssue, lineItemEntity.UnitOfIssue);
            Assert.AreEqual(lineItemDto.VendorPart, lineItemEntity.VendorPart);

            var glConfiguration = await testGeneralLedgerConfigurationRepository.GetAccountStructureAsync();

            // Confirm that the data in the line item GL distribution DTOs matches the domain entity
            foreach (var glDistributionEntity in lineItemEntity.GlDistributions)
            {
                var glDistributionDto = lineItemDto.GlDistributions.FirstOrDefault(x => x.GlAccount == glDistributionEntity.GlAccountNumber);

                // Check the values in each of the non-masked GL accounts.
                if (glDistributionDto != null)
                {
                    Assert.AreEqual(glDistributionDto.GlAccount, glDistributionEntity.GlAccountNumber);
                    Assert.AreEqual(glDistributionDto.FormattedGlAccount, glDistributionEntity.GetFormattedMaskedGlAccount(glConfiguration.MajorComponentStartPositions));
                    Assert.AreEqual(glDistributionDto.ProjectNumber, glDistributionEntity.ProjectNumber);
                    Assert.AreEqual(glDistributionDto.ProjectLineItemCode, glDistributionEntity.ProjectLineItemCode);
                    Assert.AreEqual(glDistributionDto.Quantity, glDistributionEntity.Quantity);
                    Assert.AreEqual(glDistributionDto.Amount, glDistributionEntity.Amount);
                }
            }

            // Get all of the masked GL account DTOs and confirm that their values are either 0 or null.
            var maskedGlDistributionEntity = lineItemEntity.GlDistributions.FirstOrDefault(x => x.Masked);
            var maskedGlAccountNumber = maskedGlDistributionEntity.GetFormattedMaskedGlAccount(glConfiguration.MajorComponentStartPositions);
            var maskedGlDistributionDtos = lineItemDto.GlDistributions.Where(x => x.FormattedGlAccount == maskedGlAccountNumber).ToList();

            foreach (var glDistributionDto in maskedGlDistributionDtos)
            {
                Assert.AreEqual(null, glDistributionDto.GlAccount);
                Assert.AreEqual(maskedGlAccountNumber, glDistributionDto.FormattedGlAccount);
                Assert.AreEqual(null, glDistributionDto.ProjectNumber);
                Assert.AreEqual(null, glDistributionDto.ProjectLineItemCode);
                Assert.AreEqual(0m, glDistributionDto.Quantity);
                Assert.AreEqual(0m, glDistributionDto.Amount);
            }
        }

        [TestMethod]
        public async Task GetRequisition_InProgressStatus()
        {
            var requisitionId = "4";
            var personId = "1";
            var requisitionDto = await service.GetRequisitionAsync(requisitionId);

            // Get the requisition domain entity from the test repository
            var requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);

            // Confirm that the data in the DTO matches the domain entity
            Assert.AreEqual(requisitionDto.Status.ToString(), requisitionDomainEntity.Status.ToString());
        }

        [TestMethod]
        public async Task GetRequisition_NotApprovedStatus()
        {
            var requisitionId = "2";
            var personId = "1";
            var requisitionDto = await service.GetRequisitionAsync(requisitionId);

            // Get the requisition domain entity from the test repository
            var requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);

            // Confirm that the data in the DTO matches the domain entity
            Assert.AreEqual(requisitionDto.Status.ToString(), requisitionDomainEntity.Status.ToString());
        }

        [TestMethod]
        public async Task GetRequisition_PoCreatedStatus()
        {
            var requisitionId = "6";
            var personId = "1";
            var requisitionDto = await service.GetRequisitionAsync(requisitionId);

            // Get the requisition domain entity from the test repository
            var requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);

            // Confirm that the data in the DTO matches the domain entity
            Assert.AreEqual(requisitionDto.Status.ToString(), requisitionDomainEntity.Status.ToString());
        }

        [TestMethod]
        public async Task GetRequisition_HasMultiplePurchaseOrders()
        {
            var requisitionId = "3";
            var personId = "1";
            var requisitionDto = await service.GetRequisitionAsync(requisitionId);

            // Get the requisition domain entity from the test repository
            var requisitionDomainEntity = await testRequisitionRepository.GetRequisitionAsync(requisitionId, personId, GlAccessLevel.Full_Access, null);

            // Confirm that the data in the list of purchase order DTOs matches the domain entity
            foreach (var purchaseOrder in requisitionDomainEntity.PurchaseOrders)
            {
                Assert.IsTrue(requisitionDto.PurchaseOrders.Any(poId => poId == purchaseOrder), "The PO IDs in the domain entity must be the same as the IDs in the DTO.");
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task GetRequisition_RepositoryReturnsNullObject()
        {
            // Mock the GetRequisition repository method to return a null object within the Service method
            Requisition nullRequisition = null;
            this.mockRequisitionRepository.Setup<Task<Requisition>>(reqRepo => reqRepo.GetRequisitionAsync(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<GlAccessLevel>(), It.IsAny<List<string>>())).Returns(Task.FromResult(nullRequisition));
            var requisitionDto = await service2.GetRequisitionAsync("1");
        }
        #endregion

        #region Build service method

        /// <summary>
        /// Builds multiple requisition service objects.
        /// </summary>
        private void BuildValidRequisitionService()
        {
            // We need the unit tests to be independent of "real" implementations of these classes,
            // so we use Moq to create mock implementations that are based on the same interfaces
            var roleRepository = new Mock<IRoleRepository>().Object;
            var loggerObject = new Mock<ILogger>().Object;

            testRequisitionRepository = new TestRequisitionRepository();
            testGeneralLedgerConfigurationRepository = new TestGeneralLedgerConfigurationRepository();
            testGeneralLedgerUserRepository = new TestGeneralLedgerUserRepository();

            // Set up and mock the adapter, and setup the GetAdapter method.
            var adapterRegistry = new Mock<IAdapterRegistry>();
            var requisitionDtoAdapter = new AutoMapperAdapter<Domain.ColleagueFinance.Entities.Requisition, Dtos.ColleagueFinance.Requisition>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Domain.ColleagueFinance.Entities.Requisition, Dtos.ColleagueFinance.Requisition>()).Returns(requisitionDtoAdapter);

            // Set up the service objects
            service = new RequisitionService(testRequisitionRepository, testGeneralLedgerConfigurationRepository, testGeneralLedgerUserRepository, adapterRegistry.Object, currentUserFactory, roleRepository, loggerObject);
            service2 = new RequisitionService(this.mockRequisitionRepository.Object, testGeneralLedgerConfigurationRepository, testGeneralLedgerUserRepository, adapterRegistry.Object, currentUserFactory, roleRepository, loggerObject);
        }
        #endregion
    }
}
