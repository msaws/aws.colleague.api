﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.ResidenceLife
{

    /// <summary>
    /// A meal plan assignment.
    /// </summary>
    public class MealPlanAssignment
    {
        /// <summary>
        /// The unique identifier of the meal plan assignment in the target system. 
        /// This must be blank or not supplied when creating a new meal plan assignment.
        /// This or an External Id must be specified when updating an existing meal plan assignment.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// One or more unique External Ids for the meal plan assignment.
        /// </summary>
        public List<ResidenceLifeExternalId> ExternalIds { get; set; }

        /// <summary>
        /// The identifier in the target system of the person assigned to the meal plan.
        /// Required.
        /// </summary>
        public string PersonId { get; set; }

        /// <summary>
        /// The identifier in the target system of the meal plan to which the person is assigned.
        /// Required.
        /// </summary>
        public string MealPlanId { get; set; }

        /// <summary>
        /// Number of Rate Periods of the meal plan assignment. The particular rate period, Term, Weekly, etc., is specified
        /// in the Meal Plan in the target system specified by the MealPlanId property.
        /// Required.
        /// </summary>
        public int NumberOfRatePeriods { get; set; }

        /// <summary>
        /// The start date of the meal plan assignment.
        /// Required.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// The end date of the meal plan assignment.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// The current status of the meal plan assignment. Must be a valid meal plan assignment
        /// status code in the target system.
        /// Required.
        /// </summary>
        public string CurrentStatusCode { get; set; }

        /// <summary>
        /// The effective date of the current status of the meal plan assignment.
        /// Required.
        /// </summary>
        public DateTime CurrentStatusDate { get; set; }

        /// <summary>
        /// The term id of the meal plan assignment. Must be a valid term id in the target system.
        /// </summary>
        public string TermId { get; set; }

        /// <summary>
        /// Number of Rate Periods Used of the meal plan assignment. If billing is performed by the target system, this
        /// optional value is only applied when the Current Status Code represents an early termination status. 
        /// </summary>
        public int? UsedRatePeriods { get; set; }

        /// <summary>
        /// Percentage of the meal plan used. If billing is performed by the target system, this
        /// optional value is only applied when the Current Status Code represents an early termination status. 
        /// </summary>
        public int? UsedPercent { get; set; }

        /// <summary>
        /// The card number associated with the meal plan assignment
        /// </summary>
        public string CardNumber { get; set; }

        /// <summary>
        /// An override rate that replaces the rate specified with the Meal Plan in the target system. The override rate is per units used.
        /// </summary>
        public decimal? OverrideRate { get; set; }

        /// <summary>
        /// A reason code for an override rate.  Must be a valid override reason code in the target system.
        /// </summary>
        public string OverrideRateReason { get; set; }

        /// <summary>
        /// An override AR code for the meal plan assignment charge. Only used if billing is performed by the target system, in
        /// which case it replaces the value that would otherwise be assigned by the target system.
        /// Must be a valid AR code in the target system.
        /// </summary>
        public string OverrideChargeCode { get; set; }

        /// <summary>
        /// An override AR type code for the meal plan assignment charge. Only used if billing is performed by the target system, in
        /// which case it replaces the value that would otherwise be assigned by the target system.
        /// Must be a valid AR type code in the target system.
        /// </summary>
        public string OverrideReceivableType { get; set; }

        /// <summary>
        /// An override refund formula code for the meal plan assignment. Only used if billing is performed by the target system, in
        /// which case it replaces the value that would otherwise be assigned by the target system.
        /// Must be a valid refund formula code in the target system.
        /// </summary>
        public string OverrideRefundFormula { get; set; }

        /// <summary>
        /// Comments about the meal plan assignment.
        /// </summary>
        public string Comments { get; set; }

    }
}
