﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.ResidenceLife
{
    /// <summary>
    /// A housing assignment.
    /// </summary>
    public class HousingAssignment
    {

        /// <summary>
        /// The unique identifier of the housing assignment in the target system. 
        /// This must be blank or not supplied when creating a new housing assignment.
        /// This or an External Id must be specified when updating an existing housing assignment.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// One or more unique External Ids for the housing assignment.
        /// </summary>
        public List<ResidenceLifeExternalId> ExternalIds { get; set; }

        /// <summary>
        /// The identifier in the target system of the person assigned to the housing.
        /// Required.
        /// </summary>
        public string PersonId { get; set; }

        /// <summary>
        /// The start date of the housing assignment.
        /// Required.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// The end date of the housing assignment.
        /// Required.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// The current status of the housing assignment. Must be a valid housing assignment
        /// status code in the target system.
        /// Required.
        /// </summary>
        public string CurrentStatus { get; set; }

        /// <summary>
        /// The effective date of the current status of the housing assignment.
        /// Required.
        /// </summary>
        public DateTime CurrentStatusDate { get; set; }

        /// <summary>
        /// The identifier in the target system of the building of the housing assignment.
        /// </summary>
        public string Building { get; set; }

        /// <summary>
        /// The identifier in the target system of the room of the housing assignment.
        /// If the identifier of the room in the target system is a combination of the building
        /// and room identifiers, include only the room part of the identifier.
        /// </summary>
        public string Room { get; set; }

        /// <summary>
        /// The term id of the housing assignment. Must be a valid term id in the target system.
        /// </summary>
        public string TermId { get; set; }

        /// <summary>
        /// The room rate table of the housing assignment. Must be a valid room rate table code in
        /// the target system.
        /// </summary>
        public string RoomRateTable { get; set; }

        /// <summary>
        /// The rate period of the room rate table. 
        /// </summary>
        public RoomRatePeriods? RoomRatePeriod { get; set; }

        /// <summary>
        /// An override rate that replaces the rate derived from the room rate table. The override rate is per
        /// rate period. For instance if the rate period is weekly, the override rate will be
        /// multiplied times the number of weeks. 
        /// A room rate table must be specified if an override rate is specified.
        /// </summary>
        public decimal? OverrideRate { get; set; }

        /// <summary>
        /// A reason code for an override rate. Must be a valid override reason code in the target system.
        /// </summary>
        public string OverrideRateReason { get; set; }

        /// <summary>
        /// An override AR code for the room assignment rate. Only used if billing is performed by the target system, in
        /// which case it replaces the value that would otherwise be assigned by the target system.
        /// Must be a valid AR code in the target system.
        /// </summary>
        public string OverrideChargeCode { get; set; }

        /// <summary>
        /// An override AR type code for the room assignment rate. Only used if billing is performed by the target system, in
        /// which case it replaces the value that would otherwise be assigned by the target system.
        /// Must be a valid AR type in the target system.
        /// </summary>
        public string OverrideReceivableType { get; set; }

        /// <summary>
        /// An override refund formula code for the room assignment. Only used if billing is performed by the target system, in
        /// which case it replaces the value that would otherwise be assigned by the target system.
        /// Must be a valid refund formula code in the target system.
        /// </summary>
        public string OverrideRefundFormula { get; set; }

        /// <summary>
        /// A list of additional charges against the housing assignment.
        /// </summary>
        public List<HousingAdditionalChargeOrCredit> AdditionalChargesOrCredits { get; set; }

        /// <summary>
        /// A unique contract number associated with the housing assignment.
        /// </summary>
        public string Contract { get; set; }

        /// <summary>
        /// Comments about the housing assignment.
        /// </summary>
        public string Comments { get; set; }

        /// <summary>
        /// A code that identifies the type of staff member the room resident is, if applicable. Must be a valid resident
        /// staff indicator code in the target system.
        /// </summary>
        public string ResidentStaffIndicator { get; set; }

    }
}
