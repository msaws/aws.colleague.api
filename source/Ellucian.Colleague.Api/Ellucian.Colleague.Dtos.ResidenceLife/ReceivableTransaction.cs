﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.ResidenceLife
{
    /// <summary>
    /// A generic receivables transaction
    /// </summary>
    public abstract class ReceivableTransaction
    {
        /// <summary>
        /// The identifier of the transaction in the external system
        /// </summary>
        public string ExternalIdentifier { get; set; }

        /// <summary>
        /// ID of the person for whom the transaction was made
        /// </summary>
        public string PersonId { get; set; }

        /// <summary>
        /// ID of the term for which the transaction was made
        /// </summary>
        public string TermId { get; set; }

        /// <summary>
        /// Date the transaction was made
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// ID of the location the transaction was made
        /// </summary>
        public string Location { get; set; }
    }
}
