﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.ResidenceLife
{
    /// <summary>
    /// A charge against a receivable account
    /// </summary>
    public class ReceivableCharge
    {
        /// <summary>
        /// Charge code
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Charge description
        /// </summary>
        public List<string> Description { get; set; }

        /// <summary>
        /// Base amount of charge (pre-tax amount)
        /// </summary>
        public decimal BaseAmount { get; set; }
    }
}
