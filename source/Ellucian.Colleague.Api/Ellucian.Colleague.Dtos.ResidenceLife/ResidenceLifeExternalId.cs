﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.ResidenceLife
{
    /// <summary>
    /// An ID/Source pair that comprise a unique external ID for a residence life entity
    /// </summary>
    public class ResidenceLifeExternalId
    {

        /// <summary>
        /// The unique identifier of the entity in the external system.
        /// </summary>
        public string ExternalId { get; set; }

        /// <summary>
        /// The source of ExternalId. Must be a valid source code in the target system.
        /// </summary>
        public string ExternalIdSource { get; set; }

    }
}
