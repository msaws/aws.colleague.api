﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.ResidenceLife
{
    /// <summary>
    /// A receivable invoice
    /// </summary>
    public class ReceivableInvoice : ReceivableTransaction
    {
        /// <summary>
        /// Invoice due date
        /// </summary>
        public DateTime DueDate { get; set; }

        /// <summary>
        /// Invoice description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// List of charges on this invoice
        /// </summary>
        public List<ReceivableCharge> Charges { get; set; }
    }
}
