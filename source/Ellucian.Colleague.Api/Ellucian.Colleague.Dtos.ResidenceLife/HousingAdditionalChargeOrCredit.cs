﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.ResidenceLife
{
    /// <summary>
    /// An additional charge or credit against a housing assignment, in addition to the charge based 
    /// on the room rate.
    /// </summary>
    public class HousingAdditionalChargeOrCredit
    {
        /// <summary>
        /// The charge code of the additional charge or credit. Must be a valid AR code in the target system.
        /// Required.
        /// </summary>
        public string ArCode { get; set; }

        /// <summary>
        /// A description of the additional charge or credit. 
        /// Required.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The amount of the charge or credit. A positive number is a charge. A negative number is a credit.
        /// Required.
        /// </summary>
        public decimal Amount { get; set; }
    }
}
