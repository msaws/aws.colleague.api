﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace Ellucian.Colleague.Dtos.ResidenceLife
{
    /// <summary>
    /// Room rate periods for a housing assignment.
    /// </summary>
    [JsonConverter(typeof(StringEnumConverter))]
    public enum RoomRatePeriods
    {
        /// <summary>
        /// Rate applied per day
        /// </summary>
        Daily, 

        /// <summary>
        /// Rate applied per week
        /// </summary>
        Weekly,

        /// <summary>
        /// Rate applied per month
        /// </summary>
        Monthly,

        /// <summary>
        /// Rate applied per year
        /// </summary>
        Yearly,

        /// <summary>
        /// Rate applied per term
        /// </summary>
        Term
    }
}
