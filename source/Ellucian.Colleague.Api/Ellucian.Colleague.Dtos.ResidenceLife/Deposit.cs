﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;

namespace Ellucian.Colleague.Dtos.ResidenceLife
{
    /// <summary>
    /// A deposit
    /// </summary>
    public class Deposit
    {
        /// <summary>
        /// Payer of the deposit
        /// </summary>
        public string PayerId { get; set; }

        /// <summary>
        /// Name of the payer
        /// </summary>
        public string PayerName { get; set; }

        /// <summary>
        /// ID of the deposit recipient
        /// </summary>
        public string PersonId { get; set; }

        /// <summary>
        /// Date on which the deposit payment was made
        /// </summary>
        public DateTime Date { get; set; }

        /// <summary>
        /// Payment method for the deposit
        /// </summary>
        public string PaymentMethod { get; set; }

        /// <summary>
        /// Deposit amount
        /// </summary>
        public decimal Amount { get; set; }

        /// <summary>
        /// Type of deposit
        /// </summary>
        public string DepositType { get; set; }

        /// <summary>
        /// Term of the deposit
        /// </summary>
        public string TermId { get; set; }

        /// <summary>
        /// External identifier
        /// </summary>
        public string ExternalIdentifier { get; set; }
    }
}
