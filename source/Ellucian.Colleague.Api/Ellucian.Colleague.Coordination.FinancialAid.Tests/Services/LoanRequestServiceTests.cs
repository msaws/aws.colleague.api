﻿/*Copyright 2014 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Coordination.FinancialAid.Adapters;
using Ellucian.Colleague.Coordination.FinancialAid.Services;
using Ellucian.Colleague.Domain.Entities;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Colleague.Domain.FinancialAid.Services;
using Ellucian.Colleague.Domain.FinancialAid.Tests;
using Ellucian.Colleague.Domain.Student;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Colleague.Domain.Base.Repositories;

namespace Ellucian.Colleague.Coordination.FinancialAid.Tests.Services
{
    [TestClass]
    public class LoanRequestServiceTests
    {
        [TestClass]
        public class GetLoanRequestTests : FinancialAidServiceTestsSetup
        {
            private string inputId;
            private string inputStudentId;

            private TestLoanRequestRepository testLoanRequestRepository;
            private TestFinancialAidOfficeRepository testFinancialAidOfficeRepository;
            private TestStudentAwardYearRepository testStudentAwardYearRepository;

            private Domain.FinancialAid.Entities.LoanRequest inputLoanRequestEntity;

            private Dtos.FinancialAid.LoanRequest expectedLoanRequest;
            private Dtos.FinancialAid.LoanRequest actualLoanRequest;

            private LoanRequestService loanRequestService;

            private Mock<ILoanRequestRepository> loanRequestRepositoryMock;
            private Mock<IFinancialAidOfficeRepository> financialAidOfficeRepositoryMock;
            private Mock<IStudentAwardYearRepository> studentAwardYearRepositoryMock;
            private Mock<IStudentRepository> studentRepositoryMock;
            private Mock<IApplicantRepository> applicantRepositoryMock;
            private IConfigurationRepository baseConfigurationRepository;
            private Mock<IConfigurationRepository> baseConfigurationRepositoryMock;

            [TestInitialize]
            public void Initialize()
            {
                BaseInitialize();

                testLoanRequestRepository = new TestLoanRequestRepository();
                testFinancialAidOfficeRepository = new TestFinancialAidOfficeRepository();
                testStudentAwardYearRepository = new TestStudentAwardYearRepository();

                inputId = testLoanRequestRepository.NewLoanRequestList.First().id;
                inputStudentId = testLoanRequestRepository.NewLoanRequestList.First().studentId;

                inputLoanRequestEntity = testLoanRequestRepository.GetLoanRequest(inputId);

                loanRequestRepositoryMock = new Mock<ILoanRequestRepository>();
                loanRequestRepositoryMock.Setup(r => r.GetLoanRequest(inputId)).Returns(inputLoanRequestEntity);

                financialAidOfficeRepositoryMock = new Mock<IFinancialAidOfficeRepository>();
                studentAwardYearRepositoryMock = new Mock<IStudentAwardYearRepository>();
                studentRepositoryMock = new Mock<IStudentRepository>();
                applicantRepositoryMock = new Mock<IApplicantRepository>();
                baseConfigurationRepositoryMock = new Mock<IConfigurationRepository>();
                baseConfigurationRepository = baseConfigurationRepositoryMock.Object;

                var loanRequestDtoAdapter = new LoanRequestEntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);
                expectedLoanRequest = loanRequestDtoAdapter.MapToType(inputLoanRequestEntity);

                adapterRegistryMock.Setup(a => a.GetAdapter<Domain.FinancialAid.Entities.LoanRequest, Dtos.FinancialAid.LoanRequest>()).Returns(loanRequestDtoAdapter);

                loanRequestService = new LoanRequestService(
                    adapterRegistryMock.Object,
                    loanRequestRepositoryMock.Object,
                    financialAidOfficeRepositoryMock.Object,
                    studentAwardYearRepositoryMock.Object,
                    studentRepositoryMock.Object,
                    applicantRepositoryMock.Object,
                    baseConfigurationRepository,
                    currentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object);

            }

            [TestMethod]
            public void ExpectedEqualsActionTest()
            {
                actualLoanRequest = loanRequestService.GetLoanRequest(inputId);
                Assert.AreEqual(expectedLoanRequest.Id, actualLoanRequest.Id);
                Assert.AreEqual(expectedLoanRequest.StudentId, actualLoanRequest.StudentId);
                Assert.AreEqual(expectedLoanRequest.AwardYear, actualLoanRequest.AwardYear);
                Assert.AreEqual(expectedLoanRequest.RequestDate, actualLoanRequest.RequestDate);
                Assert.AreEqual(expectedLoanRequest.TotalRequestAmount, actualLoanRequest.TotalRequestAmount);
                Assert.AreEqual(expectedLoanRequest.LoanRequestPeriods.Count, actualLoanRequest.LoanRequestPeriods.Count);
                Assert.AreEqual(expectedLoanRequest.Status, actualLoanRequest.Status);
                Assert.AreEqual(expectedLoanRequest.StatusDate, actualLoanRequest.StatusDate);
                Assert.AreEqual(expectedLoanRequest.AssignedToId, actualLoanRequest.AssignedToId);
                Assert.AreEqual(expectedLoanRequest.StudentComments, actualLoanRequest.StudentComments);

                for (var i = 0; i < expectedLoanRequest.LoanRequestPeriods.Count; i++)
                {
                    var expectedLoanRequestPeriod = expectedLoanRequest.LoanRequestPeriods[i];
                    Assert.IsTrue(actualLoanRequest.LoanRequestPeriods.Select(lrp => lrp.Code).Contains(expectedLoanRequestPeriod.Code));
                    Assert.AreEqual(expectedLoanRequestPeriod.LoanAmount, actualLoanRequest.LoanRequestPeriods.First(lrp => lrp.Code == expectedLoanRequestPeriod.Code).LoanAmount);
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullInputIdThrowsExceptionTest()
            {
                loanRequestService.GetLoanRequest(null);
            }

            [TestMethod]
            public void NullObjectFromRepoThrowsExceptionTest()
            {
                var exceptionCaught = false;
                try
                {
                    inputLoanRequestEntity = null;
                    loanRequestRepositoryMock.Setup(l => l.GetLoanRequest(inputId)).Returns(inputLoanRequestEntity);
                    loanRequestService.GetLoanRequest(inputId);
                }
                catch (KeyNotFoundException)
                {
                    exceptionCaught = true;
                }
                Assert.IsTrue(exceptionCaught);

                loggerMock.Verify(l => l.Error(string.Format("Unable to find requested LoanRequest resource {0}", inputId)));

            }

            [TestMethod]
            public void CounselorHasPermissionTest()
            {
                currentUserFactory = new CurrentUserSetup.CounselorUserFactory();
                counselorRole.AddPermission(new Permission(StudentPermissionCodes.ViewFinancialAidInformation));
                roleRepositoryMock.Setup(r => r.Roles).Returns(new List<Role>() { counselorRole });

                loanRequestService = new LoanRequestService(
                    adapterRegistryMock.Object,
                    loanRequestRepositoryMock.Object,
                    financialAidOfficeRepositoryMock.Object,
                    studentAwardYearRepositoryMock.Object,
                    studentRepositoryMock.Object,
                    applicantRepositoryMock.Object,
                    baseConfigurationRepository,
                    currentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object);

                actualLoanRequest = loanRequestService.GetLoanRequest(inputId);
            }

            [TestMethod]
            public void PermissionExceptionLogsErrorTest()
            {
                var currentUserId = inputLoanRequestEntity.StudentId;
                inputLoanRequestEntity = new Domain.FinancialAid.Entities.LoanRequest(
                    inputLoanRequestEntity.Id,
                    "foobar",
                    inputLoanRequestEntity.AwardYear,
                    inputLoanRequestEntity.RequestDate,
                    inputLoanRequestEntity.TotalRequestAmount,
                    inputLoanRequestEntity.AssignedToId,
                    inputLoanRequestEntity.Status,
                    inputLoanRequestEntity.StatusDate,
                    inputLoanRequestEntity.ModifierId);
                inputId = inputLoanRequestEntity.Id;

                loanRequestRepositoryMock.Setup(r => r.GetLoanRequest(inputId)).Returns(inputLoanRequestEntity);

                var exceptionCaught = false;
                var message = string.Format("{0} does not have permission to get LoanRequest resource {1}", currentUserId, inputId);
                try
                {
                    loanRequestService.GetLoanRequest(inputId);
                }
                catch (PermissionsException)
                {
                    exceptionCaught = true;
                }

                Assert.IsTrue(exceptionCaught);

                loggerMock.Verify(l => l.Error(message));
            }
        }

        [TestClass]
        public class CreateLoanRequestTests : FinancialAidServiceTestsSetup
        {
            //Used for Fakes when possible
            //protected static IDisposable Context { get; set; }

            private Dtos.FinancialAid.LoanRequest inputLoanRequestDto;
            private string inputStudentId;
            private Domain.Student.Entities.Student inputStudentEntity;
            private Domain.Student.Entities.Applicant inputApplicantEntity;

            private TestLoanRequestRepository testLoanRequestRepository;
            private TestFinancialAidOfficeRepository testFinancialAidOfficeRepository;
            private TestStudentAwardYearRepository testStudentAwardYearRepository;

            private Domain.FinancialAid.Entities.LoanRequest newLoanRequestEntity;
            private LoanRequestDtoToEntityAdapter loanRequestEntityAdapter;

            private Dtos.FinancialAid.LoanRequest expectedLoanRequest;
            private Dtos.FinancialAid.LoanRequest actualLoanRequest;

            private LoanRequestService loanRequestService;

            private Mock<ILoanRequestRepository> loanRequestRepositoryMock;
            private Mock<IFinancialAidOfficeRepository> financialAidOfficeRepositoryMock;
            private Mock<IStudentAwardYearRepository> studentAwardYearRepositoryMock;
            private Mock<IStudentRepository> studentRepositoryMock;
            private Mock<IApplicantRepository> applicantRepositoryMock;

            [TestInitialize]
            public void Initialize()
            {
                BaseInitialize();

                //Used for Fakes when possible
                //Context = ShimsContext.Create();

                testLoanRequestRepository = new TestLoanRequestRepository();
                testFinancialAidOfficeRepository = new TestFinancialAidOfficeRepository();
                testStudentAwardYearRepository = new TestStudentAwardYearRepository();

                testStudentAwardYearRepository.FaStudentData.PendingLoanRequestIds = new List<string>();

                inputStudentId = testLoanRequestRepository.NewLoanRequestList.First().studentId;

                inputLoanRequestDto = new Dtos.FinancialAid.LoanRequest()
                {
                    Id = "-2",
                    StudentId = inputStudentId,
                    AwardYear = "2014",
                    TotalRequestAmount = 5555,
                    LoanRequestPeriods = new List<LoanRequestPeriod>()
                    {
                        new LoanRequestPeriod(){
                            Code = "13/FA",
                            LoanAmount = 2500
                        },

                        new LoanRequestPeriod(){
                            Code = "14/SP",
                            LoanAmount = 3055
                        }                    
                    },
                    StudentComments = "These are my comments"
                };

                inputStudentEntity = new Domain.Student.Entities.Student(inputStudentId, "foobar", null, new List<string>(), new List<string>())
                {
                    FinancialAidCounselorId = "1111111"
                };
                inputApplicantEntity = null;



                expectedLoanRequest = new Dtos.FinancialAid.LoanRequest()
                {
                    Id = "-1",
                    StudentId = inputLoanRequestDto.StudentId,
                    AwardYear = inputLoanRequestDto.AwardYear,
                    TotalRequestAmount = inputLoanRequestDto.TotalRequestAmount,
                    LoanRequestPeriods = inputLoanRequestDto.LoanRequestPeriods,
                    StudentComments = inputLoanRequestDto.StudentComments,
                    RequestDate = DateTime.Today,
                    AssignedToId = inputStudentEntity.FinancialAidCounselorId,
                    Status = Dtos.FinancialAid.LoanRequestStatus.Pending,
                    StatusDate = DateTime.Today
                };

                loanRequestRepositoryMock = new Mock<ILoanRequestRepository>();
                loanRequestRepositoryMock.Setup(r => r.CreateLoanRequest(It.IsAny<Domain.FinancialAid.Entities.LoanRequest>(), It.IsAny<Domain.FinancialAid.Entities.StudentAwardYear>()))
                    .Returns(
                    (Domain.FinancialAid.Entities.LoanRequest lr, Domain.FinancialAid.Entities.StudentAwardYear y) =>
                    {
                        newLoanRequestEntity = new Domain.FinancialAid.Entities.LoanRequest(lr.Id, inputStudentId, lr.AwardYear, lr.RequestDate, lr.TotalRequestAmount, lr.AssignedToId, lr.Status, lr.StatusDate, lr.ModifierId)
                        {
                            StudentComments = lr.StudentComments,
                            ModifierComments = lr.ModifierComments
                        };
                        foreach (var period in lr.LoanRequestPeriods)
                        {
                            newLoanRequestEntity.AddLoanPeriod(period.Code, period.LoanAmount);
                        }
                        return newLoanRequestEntity;
                    });

                financialAidOfficeRepositoryMock = new Mock<IFinancialAidOfficeRepository>();
                financialAidOfficeRepositoryMock.Setup(r => r.GetFinancialAidOffices()).Returns(testFinancialAidOfficeRepository.GetFinancialAidOffices());

                studentAwardYearRepositoryMock = new Mock<IStudentAwardYearRepository>();
                studentAwardYearRepositoryMock.Setup(r => r.GetStudentAwardYears(inputStudentId, It.IsAny<CurrentOfficeService>()))
                    .Returns(testStudentAwardYearRepository.GetStudentAwardYears(inputStudentId,
                    new CurrentOfficeService(testFinancialAidOfficeRepository.GetFinancialAidOffices())));

                studentRepositoryMock = new Mock<IStudentRepository>();
                studentRepositoryMock.Setup(r => r.Get(inputStudentId)).Returns(inputStudentEntity);

                applicantRepositoryMock = new Mock<IApplicantRepository>();
                applicantRepositoryMock.Setup(r => r.GetApplicant(inputStudentId)).Returns(inputApplicantEntity);

                loanRequestEntityAdapter = new LoanRequestDtoToEntityAdapter(adapterRegistryMock.Object, loggerMock.Object);
                adapterRegistryMock.Setup(a => a.GetAdapter<Dtos.FinancialAid.LoanRequest, Domain.FinancialAid.Entities.LoanRequest>()).Returns(loanRequestEntityAdapter);

                var loanRequestDtoAdapter = new LoanRequestEntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);

                adapterRegistryMock.Setup(a => a.GetAdapter<Domain.FinancialAid.Entities.LoanRequest, Dtos.FinancialAid.LoanRequest>()).Returns(loanRequestDtoAdapter);

                loanRequestService = new LoanRequestService(
                    adapterRegistryMock.Object,
                    loanRequestRepositoryMock.Object,
                    financialAidOfficeRepositoryMock.Object,
                    studentAwardYearRepositoryMock.Object,
                    studentRepositoryMock.Object,
                    applicantRepositoryMock.Object,
                    baseConfigurationRepository,
                    currentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object);

            }

            [TestCleanup]
            public void Cleanup()
            {
                // Context.Dispose();
                // Context = null;
            }

            [TestMethod]
            public void ExpectedEqualsActualTest()
            {
                actualLoanRequest = loanRequestService.CreateLoanRequest(inputLoanRequestDto);
                Assert.AreEqual(expectedLoanRequest.Id, actualLoanRequest.Id);
                Assert.AreEqual(expectedLoanRequest.StudentId, actualLoanRequest.StudentId);
                Assert.AreEqual(expectedLoanRequest.AwardYear, actualLoanRequest.AwardYear);
                Assert.AreEqual(expectedLoanRequest.RequestDate, actualLoanRequest.RequestDate);
                Assert.AreEqual(expectedLoanRequest.TotalRequestAmount, actualLoanRequest.TotalRequestAmount);
                Assert.AreEqual(expectedLoanRequest.LoanRequestPeriods.Count, actualLoanRequest.LoanRequestPeriods.Count);
                Assert.AreEqual(expectedLoanRequest.Status, actualLoanRequest.Status);
                Assert.AreEqual(expectedLoanRequest.StatusDate, actualLoanRequest.StatusDate);
                Assert.AreEqual(expectedLoanRequest.AssignedToId, actualLoanRequest.AssignedToId);
                Assert.AreEqual(expectedLoanRequest.StudentComments, actualLoanRequest.StudentComments);

                for (var i = 0; i < expectedLoanRequest.LoanRequestPeriods.Count; i++)
                {
                    var expectedLoanRequestPeriod = expectedLoanRequest.LoanRequestPeriods[i];
                    Assert.IsTrue(actualLoanRequest.LoanRequestPeriods.Select(lrp => lrp.Code).Contains(expectedLoanRequestPeriod.Code));
                    Assert.AreEqual(expectedLoanRequestPeriod.LoanAmount, actualLoanRequest.LoanRequestPeriods.First(lrp => lrp.Code == expectedLoanRequestPeriod.Code).LoanAmount);
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullInputArgumentThrowsExceptionTest()
            {
                loanRequestService.CreateLoanRequest(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void StudentIdIsRequiredTest()
            {
                inputLoanRequestDto.StudentId = "";
                loanRequestService.CreateLoanRequest(inputLoanRequestDto);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void AwardYearIsRequiredTest()
            {
                inputLoanRequestDto.AwardYear = "";
                loanRequestService.CreateLoanRequest(inputLoanRequestDto);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void TotalRequestAmountIsRequiredTest()
            {
                inputLoanRequestDto.TotalRequestAmount = 0;
                loanRequestService.CreateLoanRequest(inputLoanRequestDto);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void LoanRequestPeriodListIsRequiredTest()
            {
                inputLoanRequestDto.LoanRequestPeriods = null;
                loanRequestService.CreateLoanRequest(inputLoanRequestDto);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void LoanRequestPeriodListIsRequiredNotBeEmptyTest()
            {
                inputLoanRequestDto.LoanRequestPeriods = new List<LoanRequestPeriod>();
                loanRequestService.CreateLoanRequest(inputLoanRequestDto);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public void CounselorCannotCreateLoanRequestTest()
            {
                currentUserFactory = new CurrentUserSetup.CounselorUserFactory();
                counselorRole.AddPermission(new Permission(StudentPermissionCodes.ViewFinancialAidInformation));
                roleRepositoryMock.Setup(r => r.Roles).Returns(new List<Role>() { counselorRole });

                loanRequestService = new LoanRequestService(
                    adapterRegistryMock.Object,
                    loanRequestRepositoryMock.Object,
                    financialAidOfficeRepositoryMock.Object,
                    studentAwardYearRepositoryMock.Object,
                    studentRepositoryMock.Object,
                    applicantRepositoryMock.Object,
                    baseConfigurationRepository,
                    currentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object);

                loanRequestService.CreateLoanRequest(inputLoanRequestDto);
            }

            [TestMethod]
            public void CatchPermissionsExceptionLogErrorTest()
            {
                var currentUserId = inputLoanRequestDto.StudentId;
                inputLoanRequestDto.StudentId = "foobar";

                var exceptionCaught = false;
                var message = string.Format("{0} does not have permission to create LoanRequest resource for {1}", currentUserId, inputLoanRequestDto.StudentId);
                try
                {
                    loanRequestService.CreateLoanRequest(inputLoanRequestDto);
                }
                catch (PermissionsException)
                {
                    exceptionCaught = true;
                }

                Assert.IsTrue(exceptionCaught);

                loggerMock.Verify(l => l.Error(message));
            }

            [TestMethod]
            public void StudentIsStudent_AssignToCounselorTest()
            {
                actualLoanRequest = loanRequestService.CreateLoanRequest(inputLoanRequestDto);
                var expectedAssignedTo = inputStudentEntity.FinancialAidCounselorId;
                Assert.AreEqual(expectedAssignedTo, actualLoanRequest.AssignedToId);
            }

            [TestMethod]
            public void StudentRepositoryReturnsNull_StudentIsApplicantTest()
            {
                var expectedAssignedToId = "2222222";
                inputApplicantEntity = new Domain.Student.Entities.Applicant(inputStudentId, "foobar")
                {
                    FinancialAidCounselorId = expectedAssignedToId
                };
                applicantRepositoryMock.Setup(r => r.GetApplicant(inputStudentId)).Returns(inputApplicantEntity);

                inputStudentEntity = null;
                studentRepositoryMock.Setup(r => r.Get(inputStudentId)).Returns(inputStudentEntity);

                actualLoanRequest = loanRequestService.CreateLoanRequest(inputLoanRequestDto);
                Assert.AreEqual(expectedAssignedToId, actualLoanRequest.AssignedToId);
            }

            [TestMethod]
            public void StudentRepositoryThrowsException_StudentIsApplicantTest()
            {
                var expectedAssignedToId = "2222222";
                inputApplicantEntity = new Domain.Student.Entities.Applicant(inputStudentId, "foobar")
                {
                    FinancialAidCounselorId = expectedAssignedToId
                };

                studentRepositoryMock.Setup(r => r.Get(inputStudentId)).Throws(new Exception("e"));
                applicantRepositoryMock.Setup(r => r.GetApplicant(inputStudentId)).Returns(inputApplicantEntity);

                actualLoanRequest = loanRequestService.CreateLoanRequest(inputLoanRequestDto);
                Assert.AreEqual(expectedAssignedToId, actualLoanRequest.AssignedToId);
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidOperationException))]
            public void StudentAndApplicantRepositoriesReturnNullTest()
            {
                inputStudentEntity = null;
                inputApplicantEntity = null;
                studentRepositoryMock.Setup(r => r.Get(inputStudentId)).Returns(inputStudentEntity);
                applicantRepositoryMock.Setup(r => r.GetApplicant(inputStudentId)).Returns(inputApplicantEntity);

                loanRequestService.CreateLoanRequest(inputLoanRequestDto);
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidOperationException))]
            public void StudentAndApplicantRepositoriesThrowExceptionTest()
            {
                inputStudentEntity = null;
                inputApplicantEntity = null;
                studentRepositoryMock.Setup(r => r.Get(inputStudentId)).Throws(new Exception("e"));
                applicantRepositoryMock.Setup(r => r.GetApplicant(inputStudentId)).Throws(new Exception("e"));

                loanRequestService.CreateLoanRequest(inputLoanRequestDto);
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidOperationException))]
            public void ApplicantRepositoryThrowsExceptionTest()
            {

                inputStudentEntity = null;
                studentRepositoryMock.Setup(r => r.Get(inputStudentId)).Returns(inputStudentEntity);
                applicantRepositoryMock.Setup(r => r.GetApplicant(inputStudentId)).Throws(new Exception("e"));

                loanRequestService.CreateLoanRequest(inputLoanRequestDto);
            }

            [TestMethod]
            public void LoanRequestRepositoryReturnsNull_ThrowExceptionLogsErrorTest()
            {
                newLoanRequestEntity = null;
                loanRequestRepositoryMock.Setup(r => r.CreateLoanRequest(It.IsAny<Domain.FinancialAid.Entities.LoanRequest>(), It.IsAny<Domain.FinancialAid.Entities.StudentAwardYear>()))
                    .Returns(newLoanRequestEntity);

                var exceptionCaught = false;
                try
                {
                    loanRequestService.CreateLoanRequest(inputLoanRequestDto);
                }
                catch (Exception)
                {
                    exceptionCaught = true;
                }

                Assert.IsTrue(exceptionCaught);

                loggerMock.Verify(l => l.Error(string.Format("Unknown resource id for new LoanRequest resource for student id {0} and awardYear {1}", inputLoanRequestDto.StudentId, inputLoanRequestDto.AwardYear)));
            }
        }
    }
}
