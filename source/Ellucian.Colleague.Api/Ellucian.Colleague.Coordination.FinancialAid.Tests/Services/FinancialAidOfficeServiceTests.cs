﻿/*Copyright 2014-2017 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Coordination.FinancialAid.Adapters;
using Ellucian.Colleague.Coordination.FinancialAid.Services;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Colleague.Domain.FinancialAid.Tests;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;
using slf4net;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Base.Repositories;

namespace Ellucian.Colleague.Coordination.FinancialAid.Tests.Services
{
    [TestClass]
    public class FinancialAidOfficeServiceTests
    {
        [TestClass]
        public class GetFinancialAidOfficeTests : FinancialAidServiceTestsSetup
        {
            private string studentId;

            private TestFinancialAidOfficeRepository testFinancialAidOfficeRepository;

            private IEnumerable<Domain.FinancialAid.Entities.FinancialAidOffice> inputOfficeEntities;
            private AutoMapperAdapter<Domain.FinancialAid.Entities.FinancialAidOffice, Dtos.FinancialAid.FinancialAidOffice> officeDtoAdapter;

            private List<Dtos.FinancialAid.FinancialAidOffice> expectedOffices;
            private IEnumerable<Ellucian.Colleague.Dtos.FinancialAid.FinancialAidOffice> actualOffices;

            private Mock<IFinancialAidOfficeRepository> officeRepositoryMock;

            private FinancialAidOfficeService financialAidOfficeService;

            [TestInitialize]
            public void Initialize()
            {
                BaseInitialize();

                currentUserFactory = new CurrentUserSetup.StudentUserFactory();
                studentId = currentUserFactory.CurrentUser.PersonId;

                testFinancialAidOfficeRepository = new TestFinancialAidOfficeRepository();
                inputOfficeEntities = testFinancialAidOfficeRepository.GetFinancialAidOffices();

                officeRepositoryMock = new Mock<IFinancialAidOfficeRepository>();
                officeRepositoryMock.Setup(r => r.GetFinancialAidOffices()).Returns(testFinancialAidOfficeRepository.GetFinancialAidOffices());

                officeDtoAdapter = new FinancialAidOfficeEntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);
                expectedOffices = new List<Dtos.FinancialAid.FinancialAidOffice>();
                foreach (var inputOffice in inputOfficeEntities)
                {
                    expectedOffices.Add(officeDtoAdapter.MapToType(inputOffice));
                }

                adapterRegistryMock.Setup(a => a.GetAdapter<Domain.FinancialAid.Entities.FinancialAidOffice, Dtos.FinancialAid.FinancialAidOffice>()
                    ).Returns(officeDtoAdapter);

                financialAidOfficeService = new FinancialAidOfficeService(
                    adapterRegistryMock.Object,
                    officeRepositoryMock.Object,
                    baseConfigurationRepository,
                    currentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object
                    );

                actualOffices = financialAidOfficeService.GetFinancialAidOffices();
            }

            [TestMethod]
            public void ObjectsHaveValueTest()
            {
                Assert.IsNotNull(expectedOffices);
                Assert.IsNotNull(actualOffices);
            }

            [TestMethod]
            public void NumOfficesAreEqualTest()
            {
                Assert.IsTrue(expectedOffices.Count() > 0);
                Assert.IsTrue(actualOffices.Count() > 0);
                Assert.AreEqual(expectedOffices.Count(), actualOffices.Count());
            }

            [TestMethod]
            public void ObjectsAreEqualTest()
            {
                foreach (var expectedOffice in expectedOffices)
                {
                    var actualOffice = actualOffices.FirstOrDefault(o => o.Id == expectedOffice.Id);
                    Assert.IsNotNull(actualOffice);

                    Assert.AreEqual(expectedOffice.Name, actualOffice.Name);
                    Assert.AreEqual(expectedOffice.PhoneNumber, actualOffice.PhoneNumber);
                    Assert.AreEqual(expectedOffice.DirectorName, actualOffice.DirectorName);
                    Assert.AreEqual(expectedOffice.EmailAddress, actualOffice.EmailAddress);
                    Assert.AreEqual(expectedOffice.AddressLabel.Count(), actualOffice.AddressLabel.Count());
                    for (int i = 0; i < expectedOffice.AddressLabel.Count(); i++)
                    {
                        Assert.AreEqual(expectedOffice.AddressLabel[i], actualOffice.AddressLabel[i]);
                    }
                }
            }

            [TestMethod]
            public void EmptyOfficeListTest()
            {
                officeRepositoryMock.Setup(o => o.GetFinancialAidOffices()).Returns(new List<Domain.FinancialAid.Entities.FinancialAidOffice>());

                actualOffices = financialAidOfficeService.GetFinancialAidOffices();
                Assert.IsNotNull(actualOffices);
                Assert.IsTrue(actualOffices.Count() == 0);
            }

            [TestMethod]
            public void NullOffices_ThrowsException_LogsMessageTest()
            {
                inputOfficeEntities = null;
                officeRepositoryMock.Setup(o => o.GetFinancialAidOffices()).Returns(inputOfficeEntities);

                var exceptionCaught = false;
                try
                {
                    actualOffices = financialAidOfficeService.GetFinancialAidOffices();
                }
                catch (KeyNotFoundException)
                {
                    exceptionCaught = true;
                }

                Assert.IsTrue(exceptionCaught);

                loggerMock.Verify(l => l.Error("Null FinancialAidOffice object returned by repository"));


            }

            [TestCleanup]
            public void Cleanup()
            {
                BaseCleanup();

                studentId = null;
                testFinancialAidOfficeRepository = null;
                inputOfficeEntities = null;
                officeDtoAdapter = null;
                expectedOffices = null;
                actualOffices = null;
                officeRepositoryMock = null;
                financialAidOfficeService = null;
            }
        }

        [TestClass]
        public class GetFinancialAidOfficeAsyncTests : FinancialAidServiceTestsSetup
        {
            private string studentId;

            private TestFinancialAidOfficeRepository testFinancialAidOfficeRepository;

            private IEnumerable<Domain.FinancialAid.Entities.FinancialAidOffice> inputOfficeEntities;
            private AutoMapperAdapter<Domain.FinancialAid.Entities.FinancialAidOffice, Ellucian.Colleague.Dtos.FinancialAid.FinancialAidOffice> officeDtoAdapter;

            private List<Dtos.FinancialAid.FinancialAidOffice> expectedOffices;
            private IEnumerable<Ellucian.Colleague.Dtos.FinancialAid.FinancialAidOffice> actualOffices;

            private Mock<IFinancialAidOfficeRepository> officeRepositoryMock;

            private FinancialAidOfficeService financialAidOfficeService;

            [TestInitialize]
            public async void Initialize()
            {
                BaseInitialize();

                currentUserFactory = new CurrentUserSetup.StudentUserFactory();
                studentId = currentUserFactory.CurrentUser.PersonId;

                testFinancialAidOfficeRepository = new TestFinancialAidOfficeRepository();
                inputOfficeEntities = await testFinancialAidOfficeRepository.GetFinancialAidOfficesAsync();

                officeRepositoryMock = new Mock<IFinancialAidOfficeRepository>();
                officeRepositoryMock.Setup(r => r.GetFinancialAidOfficesAsync()).Returns(testFinancialAidOfficeRepository.GetFinancialAidOfficesAsync());

                officeDtoAdapter = new FinancialAidOfficeEntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);
                expectedOffices = new List<Dtos.FinancialAid.FinancialAidOffice>();
                foreach (var inputOffice in inputOfficeEntities)
                {
                    expectedOffices.Add(officeDtoAdapter.MapToType(inputOffice));
                }

                adapterRegistryMock.Setup(a => a.GetAdapter<Domain.FinancialAid.Entities.FinancialAidOffice, Dtos.FinancialAid.FinancialAidOffice>()
                    ).Returns(officeDtoAdapter);

                financialAidOfficeService = new FinancialAidOfficeService(
                    adapterRegistryMock.Object,
                    officeRepositoryMock.Object,
                    baseConfigurationRepository,
                    currentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object
                    );

                actualOffices = await financialAidOfficeService.GetFinancialAidOfficesAsync();
            }

            [TestMethod]
            public void ObjectsHaveValueTest()
            {
                Assert.IsNotNull(expectedOffices);
                Assert.IsNotNull(actualOffices);
            }

            [TestMethod]
            public void NumOfficesAreEqualTest()
            {
                Assert.IsTrue(expectedOffices.Count() > 0);
                Assert.IsTrue(actualOffices.Count() > 0);
                Assert.AreEqual(expectedOffices.Count(), actualOffices.Count());
            }

            [TestMethod]
            public void ObjectsAreEqualTest()
            {
                foreach (var expectedOffice in expectedOffices)
                {
                    var actualOffice = actualOffices.FirstOrDefault(o => o.Id == expectedOffice.Id);
                    Assert.IsNotNull(actualOffice);

                    Assert.AreEqual(expectedOffice.Name, actualOffice.Name);
                    Assert.AreEqual(expectedOffice.PhoneNumber, actualOffice.PhoneNumber);
                    Assert.AreEqual(expectedOffice.DirectorName, actualOffice.DirectorName);
                    Assert.AreEqual(expectedOffice.EmailAddress, actualOffice.EmailAddress);
                    Assert.AreEqual(expectedOffice.AddressLabel.Count(), actualOffice.AddressLabel.Count());
                    for (int i = 0; i < expectedOffice.AddressLabel.Count(); i++)
                    {
                        Assert.AreEqual(expectedOffice.AddressLabel[i], actualOffice.AddressLabel[i]);
                    }
                }
            }

            [TestMethod]
            public async Task EmptyOfficeListTest()
            {
                officeRepositoryMock.Setup(o => o.GetFinancialAidOfficesAsync()).ReturnsAsync(new List<Domain.FinancialAid.Entities.FinancialAidOffice>());

                actualOffices = await financialAidOfficeService.GetFinancialAidOfficesAsync();
                Assert.IsNotNull(actualOffices);
                Assert.IsTrue(actualOffices.Count() == 0);
            }

            [TestMethod]
            public async Task NullOffices_ThrowsException_LogsMessageTest()
            {
                inputOfficeEntities = null;
                officeRepositoryMock.Setup(o => o.GetFinancialAidOfficesAsync()).ReturnsAsync(inputOfficeEntities);

                var exceptionCaught = false;
                try
                {
                    actualOffices = await financialAidOfficeService.GetFinancialAidOfficesAsync();
                }
                catch (KeyNotFoundException)
                {
                    exceptionCaught = true;
                }

                Assert.IsTrue(exceptionCaught);

                loggerMock.Verify(l => l.Error("Null FinancialAidOffice object returned by repository"));


            }

            [TestCleanup]
            public void Cleanup()
            {
                BaseCleanup();

                studentId = null;
                testFinancialAidOfficeRepository = null;
                inputOfficeEntities = null;
                officeDtoAdapter = null;
                expectedOffices = null;
                actualOffices = null;
                officeRepositoryMock = null;
                financialAidOfficeService = null;
            }
        }

        [TestClass]
        public class GetFinancialAidOffice2Tests : FinancialAidServiceTestsSetup
        {
            private string studentId;

            private TestFinancialAidOfficeRepository testFinancialAidOfficeRepository;

            private IEnumerable<Domain.FinancialAid.Entities.FinancialAidOffice> inputOfficeEntities;
            private AutoMapperAdapter<Domain.FinancialAid.Entities.FinancialAidOffice, Dtos.FinancialAid.FinancialAidOffice2> officeDtoAdapter;

            private List<Dtos.FinancialAid.FinancialAidOffice2> expectedOffices;
            private IEnumerable<Ellucian.Colleague.Dtos.FinancialAid.FinancialAidOffice2> actualOffices;

            private Mock<IFinancialAidOfficeRepository> officeRepositoryMock;

            private FinancialAidOfficeService financialAidOfficeService;

            [TestInitialize]
            public void Initialize()
            {
                BaseInitialize();

                currentUserFactory = new CurrentUserSetup.StudentUserFactory();
                studentId = currentUserFactory.CurrentUser.PersonId;

                testFinancialAidOfficeRepository = new TestFinancialAidOfficeRepository();
                inputOfficeEntities = testFinancialAidOfficeRepository.GetFinancialAidOffices();

                officeRepositoryMock = new Mock<IFinancialAidOfficeRepository>();
                officeRepositoryMock.Setup(r => r.GetFinancialAidOffices()).Returns(testFinancialAidOfficeRepository.GetFinancialAidOffices());

                officeDtoAdapter = new FinancialAidOffice2EntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);
                expectedOffices = new List<Dtos.FinancialAid.FinancialAidOffice2>();
                foreach (var inputOffice in inputOfficeEntities)
                {
                    expectedOffices.Add(officeDtoAdapter.MapToType(inputOffice));
                }

                adapterRegistryMock.Setup(a => a.GetAdapter<Domain.FinancialAid.Entities.FinancialAidOffice, Dtos.FinancialAid.FinancialAidOffice2>()
                    ).Returns(officeDtoAdapter);

                financialAidOfficeService = new FinancialAidOfficeService(
                    adapterRegistryMock.Object,
                    officeRepositoryMock.Object,
                    baseConfigurationRepository,
                    currentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object
                    );

                actualOffices = financialAidOfficeService.GetFinancialAidOffices2();
            }

            [TestMethod]
            public void ObjectsHaveValueTest2()
            {
                Assert.IsNotNull(expectedOffices);
                Assert.IsNotNull(actualOffices);
            }

            [TestMethod]
            public void NumOfficesAreEqualTest2()
            {
                Assert.IsTrue(expectedOffices.Count() > 0);
                Assert.IsTrue(actualOffices.Count() > 0);
                Assert.AreEqual(expectedOffices.Count(), actualOffices.Count());
            }

            [TestMethod]
            public void ObjectsAreEqualTest2()
            {
                foreach (var expectedOffice in expectedOffices)
                {
                    var actualOffice = actualOffices.FirstOrDefault(o => o.Id == expectedOffice.Id);
                    Assert.IsNotNull(actualOffice);

                    Assert.AreEqual(expectedOffice.Name, actualOffice.Name);
                    Assert.AreEqual(expectedOffice.PhoneNumber, actualOffice.PhoneNumber);
                    Assert.AreEqual(expectedOffice.DirectorName, actualOffice.DirectorName);
                    Assert.AreEqual(expectedOffice.EmailAddress, actualOffice.EmailAddress);
                    Assert.AreEqual(expectedOffice.AddressLabel.Count(), actualOffice.AddressLabel.Count());
                    for (int i = 0; i < expectedOffice.AddressLabel.Count(); i++)
                    {
                        Assert.AreEqual(expectedOffice.AddressLabel[i], actualOffice.AddressLabel[i]);
                    }
                }
            }

            [TestMethod]
            public void EmptyOfficeListTest2()
            {
                officeRepositoryMock.Setup(o => o.GetFinancialAidOffices()).Returns(new List<Domain.FinancialAid.Entities.FinancialAidOffice>());

                actualOffices = financialAidOfficeService.GetFinancialAidOffices2();
                Assert.IsNotNull(actualOffices);
                Assert.IsTrue(actualOffices.Count() == 0);
            }

            [TestMethod]
            public void NullOffices_ThrowsException_LogsMessageTest2()
            {
                inputOfficeEntities = null;
                officeRepositoryMock.Setup(o => o.GetFinancialAidOffices()).Returns(inputOfficeEntities);

                var exceptionCaught = false;
                try
                {
                    actualOffices = financialAidOfficeService.GetFinancialAidOffices2();
                }
                catch (KeyNotFoundException)
                {
                    exceptionCaught = true;
                }

                Assert.IsTrue(exceptionCaught);

                loggerMock.Verify(l => l.Error("Null FinancialAidOffice object returned by repository"));


            }

            [TestCleanup]
            public void Cleanup()
            {
                BaseCleanup();

                studentId = null;
                testFinancialAidOfficeRepository = null;
                inputOfficeEntities = null;
                officeDtoAdapter = null;
                expectedOffices = null;
                actualOffices = null;
                officeRepositoryMock = null;
                financialAidOfficeService = null;
            }
        }

        [TestClass]
        public class GetFinancialAidOffice2AsyncTests : FinancialAidServiceTestsSetup
        {
            private string studentId;

            private TestFinancialAidOfficeRepository testFinancialAidOfficeRepository;

            private IEnumerable<Domain.FinancialAid.Entities.FinancialAidOffice> inputOfficeEntities;
            private AutoMapperAdapter<Domain.FinancialAid.Entities.FinancialAidOffice, Dtos.FinancialAid.FinancialAidOffice2> officeDtoAdapter;

            private List<Dtos.FinancialAid.FinancialAidOffice2> expectedOffices;
            private IEnumerable<Ellucian.Colleague.Dtos.FinancialAid.FinancialAidOffice2> actualOffices;

            private Mock<IFinancialAidOfficeRepository> officeRepositoryMock;

            private FinancialAidOfficeService financialAidOfficeService;

            [TestInitialize]
            public async void Initialize()
            {
                BaseInitialize();

                currentUserFactory = new CurrentUserSetup.StudentUserFactory();
                studentId = currentUserFactory.CurrentUser.PersonId;

                testFinancialAidOfficeRepository = new TestFinancialAidOfficeRepository();
                inputOfficeEntities = await testFinancialAidOfficeRepository.GetFinancialAidOfficesAsync();

                officeRepositoryMock = new Mock<IFinancialAidOfficeRepository>();
                officeRepositoryMock.Setup(r => r.GetFinancialAidOfficesAsync()).Returns(testFinancialAidOfficeRepository.GetFinancialAidOfficesAsync());

                officeDtoAdapter = new FinancialAidOffice2EntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);
                expectedOffices = new List<Dtos.FinancialAid.FinancialAidOffice2>();
                foreach (var inputOffice in inputOfficeEntities)
                {
                    expectedOffices.Add(officeDtoAdapter.MapToType(inputOffice));
                }

                adapterRegistryMock.Setup(a => a.GetAdapter<Domain.FinancialAid.Entities.FinancialAidOffice, Dtos.FinancialAid.FinancialAidOffice2>()
                    ).Returns(officeDtoAdapter);

                financialAidOfficeService = new FinancialAidOfficeService(
                    adapterRegistryMock.Object,
                    officeRepositoryMock.Object,
                    baseConfigurationRepository,
                    currentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object
                    );

                actualOffices = await financialAidOfficeService.GetFinancialAidOffices2Async();
            }

            [TestMethod]
            public void ObjectsHaveValueTest2()
            {
                Assert.IsNotNull(expectedOffices);
                Assert.IsNotNull(actualOffices);
            }

            [TestMethod]
            public void NumOfficesAreEqualTest2()
            {
                Assert.IsTrue(expectedOffices.Count() > 0);
                Assert.IsTrue(actualOffices.Count() > 0);
                Assert.AreEqual(expectedOffices.Count(), actualOffices.Count());
            }

            [TestMethod]
            public void ObjectsAreEqualTest2()
            {
                foreach (var expectedOffice in expectedOffices)
                {
                    var actualOffice = actualOffices.FirstOrDefault(o => o.Id == expectedOffice.Id);
                    Assert.IsNotNull(actualOffice);

                    Assert.AreEqual(expectedOffice.Name, actualOffice.Name);
                    Assert.AreEqual(expectedOffice.PhoneNumber, actualOffice.PhoneNumber);
                    Assert.AreEqual(expectedOffice.DirectorName, actualOffice.DirectorName);
                    Assert.AreEqual(expectedOffice.EmailAddress, actualOffice.EmailAddress);
                    Assert.AreEqual(expectedOffice.AddressLabel.Count(), actualOffice.AddressLabel.Count());
                    for (int i = 0; i < expectedOffice.AddressLabel.Count(); i++)
                    {
                        Assert.AreEqual(expectedOffice.AddressLabel[i], actualOffice.AddressLabel[i]);
                    }
                }
            }

            [TestMethod]
            public async Task EmptyOfficeListTest2()
            {
                officeRepositoryMock.Setup(o => o.GetFinancialAidOfficesAsync()).ReturnsAsync(new List<Domain.FinancialAid.Entities.FinancialAidOffice>());

                actualOffices = await financialAidOfficeService.GetFinancialAidOffices2Async();
                Assert.IsNotNull(actualOffices);
                Assert.IsTrue(actualOffices.Count() == 0);
            }

            [TestMethod]
            public async Task NullOffices_ThrowsException_LogsMessageTest2()
            {
                inputOfficeEntities = null;
                officeRepositoryMock.Setup(o => o.GetFinancialAidOfficesAsync()).ReturnsAsync(inputOfficeEntities);

                var exceptionCaught = false;
                try
                {
                    actualOffices = await financialAidOfficeService.GetFinancialAidOffices2Async();
                }
                catch (KeyNotFoundException)
                {
                    exceptionCaught = true;
                }

                Assert.IsTrue(exceptionCaught);

                loggerMock.Verify(l => l.Error("Null FinancialAidOffice object returned by repository"));


            }

            [TestCleanup]
            public void Cleanup()
            {
                BaseCleanup();

                studentId = null;
                testFinancialAidOfficeRepository = null;
                inputOfficeEntities = null;
                officeDtoAdapter = null;
                expectedOffices = null;
                actualOffices = null;
                officeRepositoryMock = null;
                financialAidOfficeService = null;
            }
        }
    
        [TestClass]
        public class GetFinancialAidOffice3AsyncTests : FinancialAidServiceTestsSetup
        {
            private string studentId;

            private TestFinancialAidOfficeRepository testFinancialAidOfficeRepository;

            private IEnumerable<Domain.FinancialAid.Entities.FinancialAidOffice> inputOfficeEntities;
            private AutoMapperAdapter<Domain.FinancialAid.Entities.FinancialAidOffice, Dtos.FinancialAid.FinancialAidOffice3> officeDtoAdapter;

            private List<Dtos.FinancialAid.FinancialAidOffice3> expectedOffices;
            private IEnumerable<Dtos.FinancialAid.FinancialAidOffice3> actualOffices;

            private Mock<IFinancialAidOfficeRepository> officeRepositoryMock;

            private FinancialAidOfficeService financialAidOfficeService;

            [TestInitialize]
            public async void Initialize()
            {
                BaseInitialize();

                currentUserFactory = new CurrentUserSetup.StudentUserFactory();
                studentId = currentUserFactory.CurrentUser.PersonId;

                testFinancialAidOfficeRepository = new TestFinancialAidOfficeRepository();
                inputOfficeEntities = await testFinancialAidOfficeRepository.GetFinancialAidOfficesAsync();

                officeRepositoryMock = new Mock<IFinancialAidOfficeRepository>();
                officeRepositoryMock.Setup(r => r.GetFinancialAidOfficesAsync()).Returns(testFinancialAidOfficeRepository.GetFinancialAidOfficesAsync());

                officeDtoAdapter = new FinancialAidOffice3EntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);
                expectedOffices = new List<Dtos.FinancialAid.FinancialAidOffice3>();
                foreach (var inputOffice in inputOfficeEntities)
                {
                    expectedOffices.Add(officeDtoAdapter.MapToType(inputOffice));
                }

                adapterRegistryMock.Setup(a => a.GetAdapter<Domain.FinancialAid.Entities.FinancialAidOffice, Dtos.FinancialAid.FinancialAidOffice3>()
                    ).Returns(officeDtoAdapter);

                financialAidOfficeService = new FinancialAidOfficeService(
                    adapterRegistryMock.Object,
                    officeRepositoryMock.Object,
                    baseConfigurationRepository,
                    currentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object
                    );

                actualOffices = await financialAidOfficeService.GetFinancialAidOffices3Async();
            }

            [TestMethod]
            public void ObjectsHaveValueTest2()
            {
                Assert.IsNotNull(expectedOffices);
                Assert.IsNotNull(actualOffices);
            }

            [TestMethod]
            public void NumOfficesAreEqualTest2()
            {
                Assert.IsTrue(expectedOffices.Count() > 0);
                Assert.IsTrue(actualOffices.Count() > 0);
                Assert.AreEqual(expectedOffices.Count(), actualOffices.Count());
            }

            [TestMethod]
            public void ObjectsAreEqualTest2()
            {
                foreach (var expectedOffice in expectedOffices)
                {
                    var actualOffice = actualOffices.FirstOrDefault(o => o.Id == expectedOffice.Id);
                    Assert.IsNotNull(actualOffice);

                    Assert.AreEqual(expectedOffice.Name, actualOffice.Name);
                    Assert.AreEqual(expectedOffice.PhoneNumber, actualOffice.PhoneNumber);
                    Assert.AreEqual(expectedOffice.DirectorName, actualOffice.DirectorName);
                    Assert.AreEqual(expectedOffice.EmailAddress, actualOffice.EmailAddress);
                    Assert.AreEqual(expectedOffice.AddressLabel.Count(), actualOffice.AddressLabel.Count());
                    for (int i = 0; i < expectedOffice.AddressLabel.Count(); i++)
                    {
                        Assert.AreEqual(expectedOffice.AddressLabel[i], actualOffice.AddressLabel[i]);
                    }
                }
            }

            [TestMethod]
            public async Task EmptyOfficeListTest2()
            {
                officeRepositoryMock.Setup(o => o.GetFinancialAidOfficesAsync()).ReturnsAsync(new List<Domain.FinancialAid.Entities.FinancialAidOffice>());

                actualOffices = await financialAidOfficeService.GetFinancialAidOffices3Async();
                Assert.IsNotNull(actualOffices);
                Assert.IsTrue(actualOffices.Count() == 0);
            }

            [TestMethod]
            public async Task NullOffices_ThrowsException_LogsMessageTest2()
            {
                inputOfficeEntities = null;
                officeRepositoryMock.Setup(o => o.GetFinancialAidOfficesAsync()).ReturnsAsync(inputOfficeEntities);

                var exceptionCaught = false;
                try
                {
                    actualOffices = await financialAidOfficeService.GetFinancialAidOffices3Async();
                }
                catch (KeyNotFoundException)
                {
                    exceptionCaught = true;
                }

                Assert.IsTrue(exceptionCaught);

                loggerMock.Verify(l => l.Error("Null FinancialAidOffice object returned by repository"));


            }

            [TestCleanup]
            public void Cleanup()
            {
                BaseCleanup();

                studentId = null;
                testFinancialAidOfficeRepository = null;
                inputOfficeEntities = null;
                officeDtoAdapter = null;
                expectedOffices = null;
                actualOffices = null;
                officeRepositoryMock = null;
                financialAidOfficeService = null;
            }
        }
    
        [TestClass]
        public class GetFinancialAidOfficesEedm
        {
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private Mock<IFinancialAidOfficeRepository> referenceRepositoryMock;
            private IFinancialAidOfficeRepository referenceRepository;
            private IAdapterRegistry adapterRegistry;
            private ILogger logger;
            private Mock<ICurrentUserFactory> userFactoryMock;
            private ICurrentUserFactory userFactory;
            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private FinancialAidOfficeService financialAidOfficeService;
            private ICollection<Domain.FinancialAid.Entities.FinancialAidOfficeItem> financialAidOfficeCollection = new List<Domain.FinancialAid.Entities.FinancialAidOfficeItem>();
            private IConfigurationRepository baseConfigurationRepository;
            private Mock<IConfigurationRepository> baseConfigurationRepositoryMock;

            [TestInitialize]
            public void Initialize()
            {
                referenceRepositoryMock = new Mock<IFinancialAidOfficeRepository>();
                referenceRepository = referenceRepositoryMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                userFactoryMock = new Mock<ICurrentUserFactory>();
                userFactory = userFactoryMock.Object;
                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                logger = new Mock<ILogger>().Object;
                baseConfigurationRepositoryMock = new Mock<IConfigurationRepository>();
                baseConfigurationRepository = baseConfigurationRepositoryMock.Object;


                financialAidOfficeCollection.Add(new Domain.FinancialAid.Entities.FinancialAidOfficeItem("9C3B805D-CFE6-483B-86C3-4C20562F8C15", "2001", "CODE1", "NAME1"));
                financialAidOfficeCollection.Add(new Domain.FinancialAid.Entities.FinancialAidOfficeItem("73244057-D1EC-4094-A0B7-DE602533E3A6", "2002", "CODE2", "NAME2"));
                financialAidOfficeCollection.Add(new Domain.FinancialAid.Entities.FinancialAidOfficeItem("1df164eb-8178-4321-a9f7-24f12d3991d8", "2003", "CODE3", "NAME3"));
                referenceRepositoryMock.Setup(repo => repo.GetFinancialAidOfficesAsync(true)).ReturnsAsync(financialAidOfficeCollection);
                referenceRepositoryMock.Setup(repo => repo.GetFinancialAidOfficesAsync(false)).ReturnsAsync(financialAidOfficeCollection);

                financialAidOfficeService = new FinancialAidOfficeService(adapterRegistry, referenceRepository, baseConfigurationRepository, userFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                financialAidOfficeCollection = null;
                referenceRepository = null;
                financialAidOfficeService = null;
            }

            [TestMethod]
            public async Task FinancialAidOfficeService__FinancialAidOffices()
            {
                var results = await financialAidOfficeService.GetFinancialAidOfficesAsync(false);
                Assert.IsTrue(results is IEnumerable<Dtos.FinancialAidOffice>);
                Assert.IsNotNull(results);
            }

            [TestMethod]
            public async Task FinancialAidOfficeService_FinancialAidOffices_Count()
            {
                var results = await financialAidOfficeService.GetFinancialAidOfficesAsync(false);
                Assert.AreEqual(3, results.Count());
            }

            [TestMethod]
            public async Task FinancialAidOfficeService_FinancialAidOffices_Properties()
            {
                var results = await financialAidOfficeService.GetFinancialAidOfficesAsync(false);
                var financialAidOffice = results.Where(x => x.Code == "2001").FirstOrDefault();
                Assert.IsNotNull(financialAidOffice.Id);
                Assert.IsNotNull(financialAidOffice.Code);
            }

            [TestMethod]
            public async Task FinancialAidOfficeService_FinancialAidOffices_Expected()
            {
                var expectedResults = financialAidOfficeCollection.Where(c => c.Code == "2002").FirstOrDefault();
                var results = await financialAidOfficeService.GetFinancialAidOfficesAsync(false);
                var financialAidOffice = results.Where(s => s.Code == "2002").FirstOrDefault();
                Assert.AreEqual(expectedResults.Guid, financialAidOffice.Id);
                Assert.AreEqual(expectedResults.Code, financialAidOffice.Code);
            }


            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task FinancialAidOfficeService_GetFinancialAidOfficedByGuid_Empty()
            {
                await financialAidOfficeService.GetFinancialAidOfficeByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task FinancialAidOfficeService_GetFinancialAidOfficeByGuid_Null()
            {
                await financialAidOfficeService.GetFinancialAidOfficeByGuidAsync(null);
            }

            [TestMethod]
            public async Task FinancialAidOfficeService_GetFinancialAidOfficeByGuid_Expected()
            {
                var expectedResults = financialAidOfficeCollection.Where(c => c.Guid == "1df164eb-8178-4321-a9f7-24f12d3991d8").FirstOrDefault();
                var financialAidOffice = await financialAidOfficeService.GetFinancialAidOfficeByGuidAsync("1df164eb-8178-4321-a9f7-24f12d3991d8");
                Assert.AreEqual(expectedResults.Guid, financialAidOffice.Id);
                Assert.AreEqual(expectedResults.Code, financialAidOffice.Code);
            }

            [TestMethod]
            public async Task FinancialAidOfficeService_GetFinancialAidOfficeByGuid_Properties()
            {
                var expectedResults = financialAidOfficeCollection.Where(c => c.Guid == "1df164eb-8178-4321-a9f7-24f12d3991d8").FirstOrDefault();
                var financialAidOffice = await financialAidOfficeService.GetFinancialAidOfficeByGuidAsync("1df164eb-8178-4321-a9f7-24f12d3991d8");
                Assert.IsNotNull(financialAidOffice.Id);
                Assert.IsNotNull(financialAidOffice.Code);
            }
        }
    }
}
