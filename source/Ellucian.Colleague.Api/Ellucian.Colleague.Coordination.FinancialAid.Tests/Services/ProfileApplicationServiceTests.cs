﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Coordination.FinancialAid.Services;
using Ellucian.Colleague.Domain.Entities;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Colleague.Domain.FinancialAid.Services;
using Ellucian.Colleague.Domain.FinancialAid.Tests;
using Ellucian.Colleague.Domain.Student;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.TestUtil;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Ellucian.Colleague.Coordination.FinancialAid.Tests.Services
{
    [TestClass]
    public class ProfileApplicationServiceTests : FinancialAidServiceTestsSetup
    {
        public Mock<IProfileApplicationRepository> profileApplicationRepositoryMock;
        public Mock<IFinancialAidOfficeRepository> financialAidOfficeRepositoryMock;
        public Mock<IStudentAwardYearRepository> studentAwardYearRepositoryMock;

        public TestFinancialAidOfficeRepository expectedFinancialAidOfficeRepository;
        public TestStudentAwardYearRepository expectedStudentAwardYearRepository;
        public TestProfileApplicationRepository expectedProfileApplicationRepository;

        public ITypeAdapter<Colleague.Domain.FinancialAid.Entities.ProfileApplication, ProfileApplication> profileApplicationDtoAdapter;

        public FunctionEqualityComparer<ProfileApplication> profileApplicationDtoComparer;

        public void ProfileApplicationServiceTestsInitialize()
        {
            BaseInitialize();

            profileApplicationRepositoryMock = new Mock<IProfileApplicationRepository>();
            financialAidOfficeRepositoryMock = new Mock<IFinancialAidOfficeRepository>();
            studentAwardYearRepositoryMock = new Mock<IStudentAwardYearRepository>();

            expectedFinancialAidOfficeRepository = new TestFinancialAidOfficeRepository();
            expectedStudentAwardYearRepository = new TestStudentAwardYearRepository();
            expectedProfileApplicationRepository = new TestProfileApplicationRepository();

            profileApplicationDtoAdapter = new AutoMapperAdapter<Domain.FinancialAid.Entities.ProfileApplication, ProfileApplication>(adapterRegistryMock.Object, loggerMock.Object);

            profileApplicationDtoComparer = new FunctionEqualityComparer<ProfileApplication>(
                (p1, p2) => p1.Id == p2.Id,
                (p) => p.Id.GetHashCode());
        }

        [TestClass]
        public class GetProfileApplicationsTests : ProfileApplicationServiceTests
        {
            public string studentId;

            //Domain entities can be modified for tests by changing the record representations in the test repositories
            public IEnumerable<Domain.FinancialAid.Entities.FinancialAidOffice> financialAidOfficeEntities
            {
                get { return expectedFinancialAidOfficeRepository.GetFinancialAidOffices(); }
            }
            public IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear> studentAwardYearEntities
            {
                get { return expectedStudentAwardYearRepository.GetStudentAwardYears(studentId, new CurrentOfficeService(financialAidOfficeEntities)); }
            }
            public IEnumerable<Domain.FinancialAid.Entities.ProfileApplication> profileApplicationEntities
            {
                get { return expectedProfileApplicationRepository.GetProfileApplications(studentId, studentAwardYearEntities); }
            }

            //Dtos
            public List<ProfileApplication> expectedProfileApplications
            {
                get { return profileApplicationEntities.Select(profile => profileApplicationDtoAdapter.MapToType(profile)).ToList(); }
            }
            public List<ProfileApplication> actualProfileApplications
            {
                get { return profileApplicationService.GetProfileApplications(studentId).ToList(); }
            }

            public ProfileApplicationService profileApplicationService;

            [TestInitialize]
            public void Initialize()
            {
                ProfileApplicationServiceTestsInitialize();

                studentId = currentUserFactory.CurrentUser.PersonId;

                financialAidOfficeRepositoryMock.Setup(r => r.GetFinancialAidOffices())
                    .Returns(() => expectedFinancialAidOfficeRepository.GetFinancialAidOffices());

                studentAwardYearRepositoryMock.Setup(r => r.GetStudentAwardYears(It.IsAny<string>(), It.IsAny<CurrentOfficeService>()))
                    .Returns<string, CurrentOfficeService>((id, currentOfficeService) => expectedStudentAwardYearRepository.GetStudentAwardYears(id, currentOfficeService));

                profileApplicationRepositoryMock.Setup(r => r.GetProfileApplications(It.IsAny<string>(), It.IsAny<IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>>()))
                    .Returns<string, IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>>(
                        (id, studentAwardYears) => expectedProfileApplicationRepository.GetProfileApplications(id, studentAwardYears));

                adapterRegistryMock.Setup(r => r.GetAdapter<Domain.FinancialAid.Entities.ProfileApplication, ProfileApplication>())
                    .Returns(profileApplicationDtoAdapter);

                profileApplicationService = new ProfileApplicationService(adapterRegistryMock.Object, profileApplicationRepositoryMock.Object, financialAidOfficeRepositoryMock.Object, studentAwardYearRepositoryMock.Object, baseConfigurationRepository, currentUserFactory, roleRepositoryMock.Object, loggerMock.Object);
            }

            [TestMethod]
            public void ExpectedEqualsActualTest()
            {
                CollectionAssert.AreEqual(expectedProfileApplications, actualProfileApplications, profileApplicationDtoComparer);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void StudentIdRequiredTest()
            {
                profileApplicationService.GetProfileApplications(null);
            }

            [TestMethod]
            public void CounselorCanAccessStudentDataTest()
            {
                currentUserFactory = new CurrentUserSetup.CounselorUserFactory();
                counselorRole.AddPermission(new Permission(StudentPermissionCodes.ViewFinancialAidInformation));
                roleRepositoryMock.Setup(r => r.Roles).Returns(new List<Role>() { counselorRole });

                CollectionAssert.AreEqual(expectedProfileApplications, actualProfileApplications, profileApplicationDtoComparer);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public void UserNotRequestingSelfAndUserNotCounselorTest()
            {
                Assert.IsFalse(currentUserFactory.CurrentUser.IsInRole("FINANCIAL AID COUNSELOR"));

                try
                {
                    profileApplicationService.GetProfileApplications("foobar");
                }
                catch (Exception e)
                {
                    loggerMock.Verify(l => l.Error(string.Format("{0} does not have permission to access profile applications for {1}", currentUserFactory.CurrentUser.PersonId, "foobar")));
                    throw;
                }
            }

            [TestMethod]
            public void StudentAwardYearsIsNull_ReturnEmptyListTest()
            {
                studentAwardYearRepositoryMock.Setup(r => r.GetStudentAwardYears(It.IsAny<string>(), It.IsAny<CurrentOfficeService>()))
                    .Returns<string, CurrentOfficeService>((id, currentOfficeService) => null);

                Assert.AreEqual(0, actualProfileApplications.Count());
                loggerMock.Verify(l => l.Info(string.Format("Student {0} has no award years", studentId)));
            }

            [TestMethod]
            public void StudentAwardYearsIsEmpty_ReturnEmptyListTest()
            {
                studentAwardYearRepositoryMock.Setup(r => r.GetStudentAwardYears(It.IsAny<string>(), It.IsAny<CurrentOfficeService>()))
                    .Returns<string, CurrentOfficeService>((id, currentOfficeService) => new List<Domain.FinancialAid.Entities.StudentAwardYear>());

                Assert.AreEqual(0, actualProfileApplications.Count());
                loggerMock.Verify(l => l.Info(string.Format("Student {0} has no award years", studentId)));
            }

            [TestMethod]
            public void ProfileApplicationEntitiesIsNull_ReturnEmptyListTest()
            {
                profileApplicationRepositoryMock.Setup(r => r.GetProfileApplications(It.IsAny<string>(), It.IsAny<IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>>()))
                    .Returns<string, IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>>(
                        (id, studentAwardYears) => null);

                Assert.AreEqual(0, actualProfileApplications.Count());
                loggerMock.Verify(l => l.Info(string.Format("Student {0} has no profile applications", studentId)));
            }

            [TestMethod]
            public void ProfileApplicationEntitiesIsEmpty_ReturnEmptyListTest()
            {
                profileApplicationRepositoryMock.Setup(r => r.GetProfileApplications(It.IsAny<string>(), It.IsAny<IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>>()))
                    .Returns<string, IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>>(
                        (id, studentAwardYears) => new List<Domain.FinancialAid.Entities.ProfileApplication>());

                Assert.AreEqual(0, actualProfileApplications.Count());
                loggerMock.Verify(l => l.Info(string.Format("Student {0} has no profile applications", studentId)));
            }
        }
    }
}
