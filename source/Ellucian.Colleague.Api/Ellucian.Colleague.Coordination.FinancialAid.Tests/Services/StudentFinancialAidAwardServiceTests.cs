﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Coordination.FinancialAid.Services;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Base.Repositories;
using System;
using Ellucian.Colleague.Domain.FinancialAid;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Colleague.Domain.Student.Repositories;

namespace Ellucian.Colleague.Coordination.FinancialAid.Tests.Services
{
    [TestClass]
    public class StudentFinancialAidAwardServiceTests
    {
        public abstract class CurrentUserSetup
        {
            protected Domain.Entities.Role personRole = new Domain.Entities.Role(105, "Faculty");

            public class PersonUserFactory : ICurrentUserFactory
            {
                public ICurrentUser CurrentUser
                {
                    get
                    {
                        return new CurrentUser(new Claims()
                        {
                            ControlId = "123",
                            Name = "George",
                            PersonId = "0000015",
                            SecurityToken = "321",
                            SessionTimeout = 30,
                            UserName = "Faculty",
                            Roles = new List<string>() { "Faculty" },
                            SessionFixationId = "abc123",
                        });
                    }
                }
            }
        }

        [TestClass]
        public class StudentFinancialAidAwards : CurrentUserSetup
        {   
            Mock<IStudentFinancialAidAwardRepository> awardRepositoryMock;
            Mock<IFinancialAidFundRepository> fundRepositoryMock;
            Mock<IFinancialAidOfficeRepository> officeRepositoryMock;
            Mock<IAdapterRegistry> adapterRegistryMock;
            Mock<IFinancialAidReferenceDataRepository> referenceRepositoryMock;
            Mock<ITermRepository> termRepositoryMock;
            Mock<IPersonRepository> personRepositoryMock;
            Mock<IStudentLoanLimitationRepository> loanlimitRepositoryMock;
            Mock<IStudentAwardYearRepository> awardYearRepositoryMock;
            Mock<IAwardPackageChangeRequestRepository> changeRequestRepositoryMock;
            Mock<ICommunicationRepository> communicationRepositoryMock;
            Mock<ICurrentUserFactory> userFactoryMock;
            ICurrentUserFactory userFactory;
            Mock<IRoleRepository> roleRepoMock;
            private ILogger logger;

            StudentFinancialAidAwardService studentFinancialAidAwardService;

            IEnumerable<Domain.FinancialAid.Entities.StudentFinancialAidAward> aidAwardEntities;
            IEnumerable<Dtos.StudentFinancialAidAward> aidAwardDtos;

            //Dictionary<string, string> personGuids = new Dictionary<string, string>();
            IEnumerable<Domain.FinancialAid.Entities.FinancialAidFundCategory> financialCategoryEntities;
            IEnumerable<Domain.Student.Entities.Term> termEntities;
            IEnumerable<Domain.FinancialAid.Entities.FinancialAidFund> aidFundEntities;
            IEnumerable<Domain.FinancialAid.Entities.FinancialAidYear> aidYearEntities;
            IEnumerable<Domain.FinancialAid.Entities.FinancialAidAwardPeriod> awardPeriodEntities;
            IEnumerable<string> notAwardCategories;
            IEnumerable<Domain.FinancialAid.Entities.AwardStatus> awardStatusEntities;
            //IEnumerable<Domain.Student.Entities.AcademicLevel> academicLevelEntities;
            //IEnumerable<Domain.Student.Entities.AcademicProgram> academicProgramEntities;
            //IEnumerable<Domain.Student.Entities.WithdrawReason> withdrawReasonEntities;
            //IEnumerable<Domain.Base.Entities.Location> locationEntities;
            //IEnumerable<Domain.Base.Entities.AcademicDiscipline> academicDisciplineEntities;
            //IEnumerable<Domain.Base.Entities.School> schoolEntities;

            Tuple<IEnumerable<Domain.FinancialAid.Entities.StudentFinancialAidAward>, int> aidAwardEntitiesTuple;
            Tuple<IEnumerable<Dtos.StudentFinancialAidAward>, int> aidAwardDtoTuple;

            private IConfigurationRepository baseConfigurationRepository;
            private Mock<IConfigurationRepository> baseConfigurationRepositoryMock;

            int offset = 0;
            int limit = 200;

            private Domain.Entities.Permission permissionViewAnyApplication;

            protected Domain.Entities.Role personRole = new Domain.Entities.Role(105, "Faculty");


            [TestInitialize]
            public void Initialize()
            {
                awardRepositoryMock = new Mock<IStudentFinancialAidAwardRepository>();
                fundRepositoryMock = new Mock<IFinancialAidFundRepository>();
                officeRepositoryMock = new Mock<IFinancialAidOfficeRepository>();
                termRepositoryMock = new Mock<ITermRepository>();
                referenceRepositoryMock = new Mock<IFinancialAidReferenceDataRepository>();
                loanlimitRepositoryMock = new Mock<IStudentLoanLimitationRepository>();
                awardYearRepositoryMock = new Mock<IStudentAwardYearRepository>();
                changeRequestRepositoryMock = new Mock<IAwardPackageChangeRequestRepository>();
                communicationRepositoryMock = new Mock<ICommunicationRepository>();                
                personRepositoryMock = new Mock<IPersonRepository>();
                roleRepoMock = new Mock<IRoleRepository>();
                userFactoryMock = new Mock<ICurrentUserFactory>();
                adapterRegistryMock = new Mock<IAdapterRegistry>();

                baseConfigurationRepositoryMock = new Mock<IConfigurationRepository>();
                baseConfigurationRepository = baseConfigurationRepositoryMock.Object;
                //personBaseRepositoryMock = new Mock<IPersonBaseRepository>();
                logger = new Mock<ILogger>().Object;

                // Set up current user
                userFactory = userFactoryMock.Object;
                userFactory = new CurrentUserSetup.PersonUserFactory();
                // Mock permissions
                permissionViewAnyApplication = new Ellucian.Colleague.Domain.Entities.Permission(FinancialAidPermissionCodes.ViewStudentFinancialAidAwards);
                personRole.AddPermission(permissionViewAnyApplication);

                BuildData();
                BuildMocks();

                studentFinancialAidAwardService = new StudentFinancialAidAwardService(adapterRegistryMock.Object,
                    awardRepositoryMock.Object, fundRepositoryMock.Object, personRepositoryMock.Object,
                    loanlimitRepositoryMock.Object, referenceRepositoryMock.Object, officeRepositoryMock.Object,
                    awardYearRepositoryMock.Object, changeRequestRepositoryMock.Object,
                    communicationRepositoryMock.Object, termRepositoryMock.Object, baseConfigurationRepository, userFactory, roleRepoMock.Object,
                    logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                awardRepositoryMock = null;
                fundRepositoryMock = null;
                //personBaseRepositoryMock = null;
                aidAwardEntities = null;
                aidAwardDtos = null;
                //campusInvRolesEntities = null;
            }

            private void BuildData()
            {
                //Dtos
                aidAwardDtos = new List<Dtos.StudentFinancialAidAward>() 
                {
                    new Dtos.StudentFinancialAidAward()
                    {
                        Student = new Dtos.GuidObject2("d190d4b5-03b5-41aa-99b8-b8286717c956"), 
                        Id = "bbd216fb-0fc5-4f44-ae45-42d3cdd1e89a", 
                        AidYear = new Dtos.GuidObject2("e0c0c94c-53a7-46b7-96c4-76b12512c323"),    
                        AwardFund = new Dtos.GuidObject2("b90812ee-b573-4acb-88b0-6999a050be4f")
                    },
                    new Dtos.StudentFinancialAidAward()
                    {
                        Student = new Dtos.GuidObject2("d190d4b5-03b5-41aa-99b8-b8286717c956"), 
                        Id = "3f67b180-ce1d-4552-8d81-feb96b9fea5b", 
                        AidYear = new Dtos.GuidObject2("0bbb15f2-bb03-4056-bb9b-57a0ddf057ff"),
                        AwardFund = new Dtos.GuidObject2("b90812ee-b573-4acb-88b0-6999a050be4f")
                    },
                    new Dtos.StudentFinancialAidAward()
                    {
                        Student = new Dtos.GuidObject2("cecdce5a-54a7-45fb-a975-5392a579e5bf"), 
                        Id = "bf67e156-8f5d-402b-8101-81b0a2796873", 
                        AidYear = new Dtos.GuidObject2("0bbb15f2-bb03-4056-bb9b-57a0ddf057ff"),
                        AwardFund = new Dtos.GuidObject2("0ac28907-5a9b-4102-a0d7-5d3d9c585512")
                    },
                    new Dtos.StudentFinancialAidAward()
                    {
                        Student = new Dtos.GuidObject2("cecdce5a-54a7-45fb-a975-5392a579e5bf"), 
                        Id = "0111d6ef-5a86-465f-ac58-4265a997c136", 
                        AidYear = new Dtos.GuidObject2("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52"),
                        AwardFund = new Dtos.GuidObject2("bb6c261c-3818-4dc3-b693-eb3e64d70d8b")
                    },
                };

                //Entities
                aidAwardEntities = new List<Domain.FinancialAid.Entities.StudentFinancialAidAward>() 
                {
                    new Domain.FinancialAid.Entities.StudentFinancialAidAward("bbd216fb-0fc5-4f44-ae45-42d3cdd1e89a", "CODE1", "CODE1", "CODE1")
                    {
                        AwardHistory = new List<Domain.FinancialAid.Entities.StudentAwardHistoryByPeriod>()
                        {
                            new Domain.FinancialAid.Entities.StudentAwardHistoryByPeriod() 
                            {
                                AwardPeriod = "CODE1",
                                Status = "A",
                                StatusDate = new DateTime(2016, 02, 01),
                                Amount = (decimal) 100000000
                            },
                            new Domain.FinancialAid.Entities.StudentAwardHistoryByPeriod() 
                            {
                                AwardPeriod = "CODE1",
                                Status = "A",
                                StatusDate = new DateTime(2015, 02, 02),
                                Amount = (decimal) 100000
                            }
                        }
                    },
                    new Domain.FinancialAid.Entities.StudentFinancialAidAward("3f67b180-ce1d-4552-8d81-feb96b9fea5b", "CODE2", "CODE2", "CODE2")
                    {
                    },
                    new Domain.FinancialAid.Entities.StudentFinancialAidAward("bf67e156-8f5d-402b-8101-81b0a2796873", "CODE3", "CODE3", "CODE3")
                    {
                    },
                    new Domain.FinancialAid.Entities.StudentFinancialAidAward("0111d6ef-5a86-465f-ac58-4265a997c136", "CODE4", "CODE4", "CODE4")
                    { 
                        AwardHistory = new List<Domain.FinancialAid.Entities.StudentAwardHistoryByPeriod>()
                        {
                            new Domain.FinancialAid.Entities.StudentAwardHistoryByPeriod() 
                            {
                                AwardPeriod = "CODE4",
                                Status = "A",
                                StatusDate = new DateTime(2016, 02, 02),
                                Amount = (decimal) 100000,
                                StatusChanges = new List<Domain.FinancialAid.Entities.StudentAwardHistoryStatus>() 
                                {
                                    new Domain.FinancialAid.Entities.StudentAwardHistoryStatus()
                                    {
                                        Status = "R",
                                        StatusDate = new DateTime(2016, 03, 02),
                                        Amount = (decimal) 3029023
                                    }
                                }
                            },
                            new Domain.FinancialAid.Entities.StudentAwardHistoryByPeriod() 
                            {
                                AwardPeriod = "CODE4",
                                Status = "A",
                                StatusDate = new DateTime(2015, 02, 02),
                                Amount = (decimal) 100000,
                                StatusChanges = new List<Domain.FinancialAid.Entities.StudentAwardHistoryStatus>() 
                                {
                                    new Domain.FinancialAid.Entities.StudentAwardHistoryStatus()
                                    {
                                        Status = "R",
                                        StatusDate = new DateTime(2015, 03, 02),
                                        Amount = (decimal) 3029023
                                    }
                                }
                            }
                        }
                    },
                };

                ////Persons
                //personGuids.Add("1", "d190d4b5-03b5-41aa-99b8-b8286717c956");
                //personGuids.Add("2", "cecdce5a-54a7-45fb-a975-5392a579e5bf");


                //FinancialFundCategories
                financialCategoryEntities = new List<Domain.FinancialAid.Entities.FinancialAidFundCategory>() 
                {
                    new Domain.FinancialAid.Entities.FinancialAidFundCategory("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                    new Domain.FinancialAid.Entities.FinancialAidFundCategory("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                    new Domain.FinancialAid.Entities.FinancialAidFundCategory("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                    new Domain.FinancialAid.Entities.FinancialAidFundCategory("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                };

                //Terms
                termEntities = new List<Domain.Student.Entities.Term>() 
                {
                    new Domain.Student.Entities.Term("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1", new DateTime(2016, 02, 01), new DateTime(2016, 03, 01), 2016, 1, false, false, "WINTER", false),
                    new Domain.Student.Entities.Term("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2", new DateTime(2016, 03, 01), new DateTime(2016, 04, 01), 2016, 2, false, false, "WINTER", false),
                    new Domain.Student.Entities.Term("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3", new DateTime(2016, 04, 01), new DateTime(2016, 05, 01), 2016, 3, false, false, "WINTER", false),
                    new Domain.Student.Entities.Term("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4", new DateTime(2016, 05, 01), new DateTime(2016, 06, 01), 2016, 4, false, false, "WINTER", false)
                };

                //FinancialAidFunds
                aidFundEntities = new List<Domain.FinancialAid.Entities.FinancialAidFund>() 
                {
                    new Domain.FinancialAid.Entities.FinancialAidFund("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                    new Domain.FinancialAid.Entities.FinancialAidFund("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE2", "DESC2"),
                    new Domain.FinancialAid.Entities.FinancialAidFund("0ac28907-5a9b-4102-a0d7-5d3d9c585512", "CODE3", "DESC3"),
                    new Domain.FinancialAid.Entities.FinancialAidFund("bb6c261c-3818-4dc3-b693-eb3e64d70d8b", "CODE4", "DESC4")
                };

                //FinancialAidYears
                aidYearEntities = new List<Domain.FinancialAid.Entities.FinancialAidYear>() 
                {
                    new Domain.FinancialAid.Entities.FinancialAidYear("e0c0c94c-53a7-46b7-96c4-76b12512c323", "CODE1", "DESC1", "STATUS1"),
                    new Domain.FinancialAid.Entities.FinancialAidYear("0bbb15f2-bb03-4056-bb9b-57a0ddf057ff", "CODE2", "DESC2", "STATUS2"),
                    new Domain.FinancialAid.Entities.FinancialAidYear("0bbb15f2-bb03-4056-bb9b-57a0ddf057ff", "CODE3", "DESC3", "STATUS3"),
                    new Domain.FinancialAid.Entities.FinancialAidYear("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE4", "DESC4", "STATUS4")
                };

                //FinancialAidAwardPeriods
                awardPeriodEntities = new List<Domain.FinancialAid.Entities.FinancialAidAwardPeriod>() 
                {
                    new Domain.FinancialAid.Entities.FinancialAidAwardPeriod("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1", "STATUS1")
                    {
                        AwardTerms = new List<string>() 
                        {
                            "CODE1"
                        }
                    },
                    new Domain.FinancialAid.Entities.FinancialAidAwardPeriod("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2", "STATUS2"),
                    new Domain.FinancialAid.Entities.FinancialAidAwardPeriod("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3", "STATUS3"),
                    new Domain.FinancialAid.Entities.FinancialAidAwardPeriod("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4", "STATUS4")
                };

                //NotAwardedCategories
                notAwardCategories = new List<string>()
                {
                    "P", "E"
                };
                //notAwardCategories.ToList().Add("P");
                //notAwardCategories.ToList().Add("E");

                //AwardStatuses
                awardStatusEntities = new List<Domain.FinancialAid.Entities.AwardStatus>() 
                {
                    new Domain.FinancialAid.Entities.AwardStatus("A", "DESC1", Domain.FinancialAid.Entities.AwardStatusCategory.Accepted),
                    new Domain.FinancialAid.Entities.AwardStatus("E", "DESC2", Domain.FinancialAid.Entities.AwardStatusCategory.Estimated),
                    new Domain.FinancialAid.Entities.AwardStatus("A", "DESC3", Domain.FinancialAid.Entities.AwardStatusCategory.Accepted),
                    new Domain.FinancialAid.Entities.AwardStatus("E", "DESC4", Domain.FinancialAid.Entities.AwardStatusCategory.Estimated)
                };

                ////AdmissionResidencyTypes
                //admissionResidencyEntities = new List<Domain.Student.Entities.AdmissionResidencyType>() 
                //{
                //    new Domain.Student.Entities.AdmissionResidencyType("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                //    new Domain.Student.Entities.AdmissionResidencyType("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                //    new Domain.Student.Entities.AdmissionResidencyType("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                //    new Domain.Student.Entities.AdmissionResidencyType("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                //};

                ////AcademicLevels
                //academicLevelEntities = new List<Domain.Student.Entities.AcademicLevel>() 
                //{
                //    new Domain.Student.Entities.AcademicLevel("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                //    new Domain.Student.Entities.AcademicLevel("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                //    new Domain.Student.Entities.AcademicLevel("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                //    new Domain.Student.Entities.AcademicLevel("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                //};

                ////AcademicPrograms
                //academicProgramEntities = new List<Domain.Student.Entities.AcademicProgram>() 
                //{
                //    new Domain.Student.Entities.AcademicProgram("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                //    new Domain.Student.Entities.AcademicProgram("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                //    new Domain.Student.Entities.AcademicProgram("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                //    new Domain.Student.Entities.AcademicProgram("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                //};

                ////WithdrawReasons
                //withdrawReasonEntities = new List<Domain.Student.Entities.WithdrawReason>() 
                //{
                //    new Domain.Student.Entities.WithdrawReason("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                //    new Domain.Student.Entities.WithdrawReason("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                //    new Domain.Student.Entities.WithdrawReason("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                //    new Domain.Student.Entities.WithdrawReason("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                //};

                ////Locations
                //locationEntities = new List<Domain.Base.Entities.Location>() 
                //{
                //    new Domain.Base.Entities.Location("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                //    new Domain.Base.Entities.Location("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                //    new Domain.Base.Entities.Location("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                //    new Domain.Base.Entities.Location("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                //};

                ////AcademicDisciplines
                //academicDisciplineEntities = new List<Domain.Base.Entities.AcademicDiscipline>() 
                //{
                //    new Domain.Base.Entities.AcademicDiscipline("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1", Domain.Base.Entities.AcademicDisciplineType.Concentration),
                //    new Domain.Base.Entities.AcademicDiscipline("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2", Domain.Base.Entities.AcademicDisciplineType.Major),
                //    new Domain.Base.Entities.AcademicDiscipline("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3", Domain.Base.Entities.AcademicDisciplineType.Minor),
                //    new Domain.Base.Entities.AcademicDiscipline("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4", Domain.Base.Entities.AcademicDisciplineType.Concentration)
                //};

                ////Schools
                //schoolEntities = new List<Domain.Base.Entities.School>() 
                //{
                //    new Domain.Base.Entities.School("b90812ee-b573-4acb-88b0-6999a050be4f", "CODE1", "DESC1"),
                //    new Domain.Base.Entities.School("f9871d1d-a7c0-4239-b4e3-6ee6b5bc9d52", "CODE2", "DESC2"),
                //    new Domain.Base.Entities.School("abe5524b-6704-4f09-b858-763ee2ab5fe4", "CODE3", "DESC3"),
                //    new Domain.Base.Entities.School("2158ad73-3416-467b-99d5-1b7b92599389", "CODE4", "DESC4")
                //};

                //Tuple
                aidAwardEntitiesTuple = new Tuple<IEnumerable<Domain.FinancialAid.Entities.StudentFinancialAidAward>, int>(aidAwardEntities, aidAwardEntities.Count());
                aidAwardDtoTuple = new Tuple<IEnumerable<Dtos.StudentFinancialAidAward>, int>(aidAwardDtos, aidAwardDtos.Count());
            }

            private void BuildMocks()
            {
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { personRole });

                awardRepositoryMock.Setup(i => i.GetByIdAsync(It.IsAny<string>())).ReturnsAsync(aidAwardEntities.FirstOrDefault());
                referenceRepositoryMock.Setup(i => i.GetFinancialAidFundCategoriesAsync(It.IsAny<bool>())).ReturnsAsync(financialCategoryEntities);
                fundRepositoryMock.Setup(i => i.GetFinancialAidFundsAsync(It.IsAny<bool>())).ReturnsAsync(aidFundEntities);
                referenceRepositoryMock.Setup(i => i.GetFinancialAidYearsAsync(It.IsAny<bool>())).ReturnsAsync(aidYearEntities);
                awardRepositoryMock.Setup(i => i.GetAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<bool>(), It.IsAny<IEnumerable<string>>(), It.IsAny<IEnumerable<string>>())).ReturnsAsync(aidAwardEntitiesTuple);
                personRepositoryMock.Setup(i => i.GetPersonGuidFromIdAsync("CODE1")).ReturnsAsync("d190d4b5-03b5-41aa-99b8-b8286717c956");
                personRepositoryMock.Setup(i => i.GetPersonGuidFromIdAsync("CODE2")).ReturnsAsync("d190d4b5-03b5-41aa-99b8-b8286717c956");
                personRepositoryMock.Setup(i => i.GetPersonGuidFromIdAsync("CODE3")).ReturnsAsync("cecdce5a-54a7-45fb-a975-5392a579e5bf");
                personRepositoryMock.Setup(i => i.GetPersonGuidFromIdAsync("CODE4")).ReturnsAsync("cecdce5a-54a7-45fb-a975-5392a579e5bf");
                referenceRepositoryMock.Setup(i => i.GetFinancialAidAwardPeriodsAsync(It.IsAny<bool>())).ReturnsAsync(awardPeriodEntities);
                referenceRepositoryMock.Setup(i => i.GetHostCountryAsync()).ReturnsAsync("USA");
                awardRepositoryMock.Setup(i => i.GetNotAwardedCategoriesAsync()).ReturnsAsync(notAwardCategories);
                termRepositoryMock.Setup(i => i.GetAsync(It.IsAny<bool>())).ReturnsAsync(termEntities);
                termRepositoryMock.Setup(i => i.GetAsync()).ReturnsAsync(termEntities);
                referenceRepositoryMock.Setup(i => i.AwardStatuses).Returns(awardStatusEntities);

                //fundRepositoryMock.Setup(i => i.GetApplicationSourcesAsync(It.IsAny<bool>())).ReturnsAsync(applicationSourceEntities);
                //fundRepositoryMock.Setup(i => i.GetAdmissionPopulationsAsync(It.IsAny<bool>())).ReturnsAsync(admissionPopulationEntities);
                //fundRepositoryMock.Setup(i => i.GetAdmissionResidencyTypesAsync(It.IsAny<bool>())).ReturnsAsync(admissionResidencyEntities);
                //fundRepositoryMock.Setup(i => i.GetAcademicLevelsAsync(It.IsAny<bool>())).ReturnsAsync(academicLevelEntities);
                //fundRepositoryMock.Setup(i => i.GetAcademicProgramsAsync(It.IsAny<bool>())).ReturnsAsync(academicProgramEntities);
                //fundRepositoryMock.Setup(i => i.GetWithdrawReasonsAsync(It.IsAny<bool>())).ReturnsAsync(withdrawReasonEntities);
                //referenceRepositoryMock.Setup(i => i.GetAcademicDisciplinesAsync(It.IsAny<bool>())).ReturnsAsync(academicDisciplineEntities);
                //referenceRepositoryMock.Setup(i => i.GetSchoolsAsync(It.IsAny<bool>())).ReturnsAsync(schoolEntities);
            }

            [TestMethod]
            public async Task StudentFinancialAidAwardService__GetStudentFinancialAidAwardsAsync()
            {
                //awardRepositoryMock.Setup(i => i.GetAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>())).ReturnsAsync(aidAwardEntitiesTuple);
                var studentFinancialAidAwards = await studentFinancialAidAwardService.GetAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<bool>());

                Assert.IsNotNull(studentFinancialAidAwards);

                foreach (var actual in studentFinancialAidAwards.Item1)
                {
                    var expected = aidAwardDtos.FirstOrDefault(i => i.Id.Equals(actual.Id, StringComparison.OrdinalIgnoreCase));
                    Assert.IsNotNull(expected);

                    Assert.AreEqual(expected.Id, actual.Id);
                    Assert.AreEqual(expected.AidYear.Id, actual.AidYear.Id);
                    Assert.AreEqual(expected.Student.Id, actual.Student.Id);
                    Assert.AreEqual(expected.AwardFund.Id, actual.AwardFund.Id);
                    //Assert.AreEqual(expected.Statuses.FirstOrDefault().AdmissionApplicationsStatusDetail.Id, actual.Statuses.FirstOrDefault().AdmissionApplicationsStatusDetail.Id);
                    //if (actual.InvolvementRole == null)
                    //{
                    //    Assert.IsNull(actual.InvolvementRole);
                    //    Assert.IsNull(expected.InvolvementRole);
                    //}
                    //else
                    //{
                    //    Assert.AreEqual(expected.InvolvementRole.Id, actual.InvolvementRole.Id);
                    //}

                    //Assert.AreEqual(expected.InvolvementStartOn, actual.InvolvementStartOn);
                    //Assert.AreEqual(expected.PersonId.Id, actual.PersonId.Id);
                }
            }

            [TestMethod]
            public async Task StudentFinancialAidAwardService__GetStudentFinancialAidAwardByGuidAsync()
            {
                var id = "0111d6ef-5a86-465f-ac58-4265a997c136";
                var aidAwardEntity = aidAwardEntities.FirstOrDefault(i => i.Guid.Equals(id));
                awardRepositoryMock.Setup(i => i.GetByIdAsync(It.IsAny<string>())).ReturnsAsync(aidAwardEntity);

                var actual = await studentFinancialAidAwardService.GetByIdAsync(id, false);

                Assert.IsNotNull(actual);

                var expected = aidAwardDtos.FirstOrDefault(i => i.Id.Equals(actual.Id, StringComparison.OrdinalIgnoreCase));
                Assert.IsNotNull(expected);

                Assert.AreEqual(expected.Id, actual.Id);
                Assert.AreEqual(expected.AidYear.Id, actual.AidYear.Id);
                Assert.AreEqual(expected.Student.Id, actual.Student.Id);
                Assert.AreEqual(expected.AwardFund.Id, actual.AwardFund.Id);
            }


        }
    }
}
