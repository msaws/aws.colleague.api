﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Coordination.FinancialAid.Services;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Base.Repositories;

namespace Ellucian.Colleague.Coordination.FinancialAid.Tests.Services
{
    [TestClass]
    public class FinancialAidFundServiceTests
    {
        [TestClass]
        public class GetFinancialAidFunds
        {
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private Mock<IFinancialAidReferenceDataRepository> referenceRepositoryMock;
            private IFinancialAidReferenceDataRepository referenceRepository;
            private Mock<IFinancialAidFundRepository> fundRepositoryMock;
            private IFinancialAidFundRepository fundRepository;
            private Mock<IFinancialAidOfficeRepository> officeRepositoryMock;
            private IFinancialAidOfficeRepository officeRepository;
            private IAdapterRegistry adapterRegistry;
            private ILogger logger;
            private Mock<ICurrentUserFactory> userFactoryMock;
            private ICurrentUserFactory userFactory;
            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private FinancialAidFundsService financialAidFundService;
            private ICollection<Domain.FinancialAid.Entities.FinancialAidFund> financialAidFundCollection = new List<Domain.FinancialAid.Entities.FinancialAidFund>();
            private ICollection<Domain.FinancialAid.Entities.FinancialAidYear> financialAidYearCollection = new List<Domain.FinancialAid.Entities.FinancialAidYear>();
            private ICollection<Domain.FinancialAid.Entities.FinancialAidOfficeItem> financialAidOfficeCollection = new List<Domain.FinancialAid.Entities.FinancialAidOfficeItem>();
            private ICollection<Domain.FinancialAid.Entities.FinancialAidFundCategory> financialAidCategoryCollection = new List<Domain.FinancialAid.Entities.FinancialAidFundCategory>();
            private ICollection<Domain.FinancialAid.Entities.FinancialAidFundClassification> financialAidClassificationCollection = new List<Domain.FinancialAid.Entities.FinancialAidFundClassification>();
            private ICollection<Domain.FinancialAid.Entities.FinancialAidFundsFinancialProperty> financialAidFundFinancialCollection = new List<Domain.FinancialAid.Entities.FinancialAidFundsFinancialProperty>();
            private IConfigurationRepository baseConfigurationRepository;
            private Mock<IConfigurationRepository> baseConfigurationRepositoryMock;

            [TestInitialize]
            public void Initialize()
            {
                referenceRepositoryMock = new Mock<IFinancialAidReferenceDataRepository>();
                referenceRepository = referenceRepositoryMock.Object;
                fundRepositoryMock = new Mock<IFinancialAidFundRepository>();
                fundRepository = fundRepositoryMock.Object;
                officeRepositoryMock = new Mock<IFinancialAidOfficeRepository>();
                officeRepository = officeRepositoryMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                userFactoryMock = new Mock<ICurrentUserFactory>();
                userFactory = userFactoryMock.Object;
                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                logger = new Mock<ILogger>().Object;
                baseConfigurationRepositoryMock = new Mock<IConfigurationRepository>();
                baseConfigurationRepository = baseConfigurationRepositoryMock.Object;


                financialAidFundCollection.Add(new Domain.FinancialAid.Entities.FinancialAidFund("9C3B805D-CFE6-483B-86C3-4C20562F8C15", "CODE1", "2001") { CategoryCode = "CODE1", FundingType = "CODE2" });
                financialAidFundCollection.Add(new Domain.FinancialAid.Entities.FinancialAidFund("73244057-D1EC-4094-A0B7-DE602533E3A6", "CODE2", "2002") { CategoryCode = "CODE2", FundingType = "CODE3" });
                financialAidFundCollection.Add(new Domain.FinancialAid.Entities.FinancialAidFund("1df164eb-8178-4321-a9f7-24f12d3991d8", "CODE3", "2003") { CategoryCode = "CODE3", FundingType = "CODE1" });
                financialAidYearCollection.Add(new Domain.FinancialAid.Entities.FinancialAidYear("9C3B805D-CFE6-483B-86C3-4C20562F8C15", "2001", "DESC1", "STATUS1") { });
                financialAidYearCollection.Add(new Domain.FinancialAid.Entities.FinancialAidYear("73244057-D1EC-4094-A0B7-DE602533E3A6", "2002", "DESC2", "STATUS2") { });
                financialAidYearCollection.Add(new Domain.FinancialAid.Entities.FinancialAidYear("1df164eb-8178-4321-a9f7-24f12d3991d8", "2003", "DESC3", "STATUS3") { });
                financialAidOfficeCollection.Add(new Domain.FinancialAid.Entities.FinancialAidOfficeItem("9C3B805D-CFE6-483B-86C3-4C20562F8C15", "OFFICE1", "DESC1", "NAME1") { });
                financialAidOfficeCollection.Add(new Domain.FinancialAid.Entities.FinancialAidOfficeItem("73244057-D1EC-4094-A0B7-DE602533E3A6", "OFFICE2", "DESC2", "NAME2") { });
                financialAidOfficeCollection.Add(new Domain.FinancialAid.Entities.FinancialAidOfficeItem("1df164eb-8178-4321-a9f7-24f12d3991d8", "OFFICE3", "DESC3", "NAME3") { });
                financialAidFundFinancialCollection.Add(new Domain.FinancialAid.Entities.FinancialAidFundsFinancialProperty("2001", "OFFICE1", (decimal) 10000.00, "CODE1", 60) { });
                financialAidFundFinancialCollection.Add(new Domain.FinancialAid.Entities.FinancialAidFundsFinancialProperty("2002", "OFFICE2", (decimal) 20000.00, "CODE2", 60) { });
                financialAidFundFinancialCollection.Add(new Domain.FinancialAid.Entities.FinancialAidFundsFinancialProperty("2003", "OFFICE3", (decimal) 30000.00, "CODE3", 60) { });
                financialAidCategoryCollection.Add(new Domain.FinancialAid.Entities.FinancialAidFundCategory("9C3B805D-CFE6-483B-86C3-4C20562F8C15", "CODE1", "2001", Domain.FinancialAid.Entities.AwardCategoryType.Scholarship, Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.academicCompetitivenessGrant, true) { });
                financialAidCategoryCollection.Add(new Domain.FinancialAid.Entities.FinancialAidFundCategory("73244057-D1EC-4094-A0B7-DE602533E3A6", "CODE2", "2002", Domain.FinancialAid.Entities.AwardCategoryType.Loan, Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.federalSubsidizedLoan, false) { });
                financialAidCategoryCollection.Add(new Domain.FinancialAid.Entities.FinancialAidFundCategory("1df164eb-8178-4321-a9f7-24f12d3991d8", "CODE3", "2003", Domain.FinancialAid.Entities.AwardCategoryType.Work, Domain.FinancialAid.Entities.FinancialAidFundAidCategoryType.graduatePlusLoan, true) { });
                financialAidClassificationCollection.Add(new Domain.FinancialAid.Entities.FinancialAidFundClassification("9C3B805D-CFE6-483B-86C3-4C20562F8C15", "CODE1", "2001") { });
                financialAidClassificationCollection.Add(new Domain.FinancialAid.Entities.FinancialAidFundClassification("73244057-D1EC-4094-A0B7-DE602533E3A6", "CODE2", "2002") { });
                financialAidClassificationCollection.Add(new Domain.FinancialAid.Entities.FinancialAidFundClassification("1df164eb-8178-4321-a9f7-24f12d3991d8", "CODE3", "2003") { });
                fundRepositoryMock.Setup(repo => repo.GetFinancialAidFundsAsync(true)).ReturnsAsync(financialAidFundCollection);
                fundRepositoryMock.Setup(repo => repo.GetFinancialAidFundsAsync(false)).ReturnsAsync(financialAidFundCollection);
                referenceRepositoryMock.Setup(repo => repo.GetFinancialAidYearsAsync(It.IsAny<bool>())).ReturnsAsync(financialAidYearCollection);
                officeRepositoryMock.Setup(repo => repo.GetFinancialAidOfficesAsync(It.IsAny<bool>())).ReturnsAsync(financialAidOfficeCollection);
                referenceRepositoryMock.Setup(repo => repo.GetHostCountryAsync()).ReturnsAsync("USA");
                fundRepositoryMock.Setup(repo => repo.GetFinancialAidFundFinancialsAsync(It.IsAny<string>(), It.IsAny<IEnumerable<string>>(), It.IsAny<string>())).ReturnsAsync(financialAidFundFinancialCollection);
                referenceRepositoryMock.Setup(repo => repo.GetFinancialAidFundCategoriesAsync(It.IsAny<bool>())).ReturnsAsync(financialAidCategoryCollection);
                referenceRepositoryMock.Setup(repo => repo.GetFinancialAidFundClassificationsAsync(It.IsAny<bool>())).ReturnsAsync(financialAidClassificationCollection);

                financialAidFundService = new FinancialAidFundsService(referenceRepository, fundRepository, officeRepository, baseConfigurationRepository, adapterRegistry, userFactory, roleRepo, logger);
            }

            [TestCleanup]
            public void Cleanup()
            {
                financialAidFundCollection = null;
                referenceRepository = null;
                officeRepository = null;
                fundRepository = null;
                financialAidFundService = null;
            }

            [TestMethod]
            public async Task FinancialAidFundService__FinancialAidFunds()
            {
                var results = await financialAidFundService.GetFinancialAidFundsAsync();
                Assert.IsTrue(results is IEnumerable<Dtos.FinancialAidFunds>);
                Assert.IsNotNull(results);
            }

            [TestMethod]
            public async Task FinancialAidFundService_FinancialAidFunds_Count()
            {
                var results = await financialAidFundService.GetFinancialAidFundsAsync();
                Assert.AreEqual(3, results.Count());
            }

            [TestMethod]
            public async Task FinancialAidFundService_FinancialAidFunds_Properties()
            {
                var results = await financialAidFundService.GetFinancialAidFundsAsync();
                var financialAidFund = results.Where(x => x.Code == "CODE1").FirstOrDefault();
                Assert.IsNotNull(financialAidFund.Id);
                Assert.IsNotNull(financialAidFund.Code);
            }

            [TestMethod]
            public async Task FinancialAidFundService_FinancialAidFunds_Expected()
            {
                var expectedResults = financialAidFundCollection.Where(c => c.Code == "CODE2").FirstOrDefault();
                var results = await financialAidFundService.GetFinancialAidFundsAsync();
                var financialAidFund = results.Where(s => s.Code == "CODE2").FirstOrDefault();
                Assert.AreEqual(expectedResults.Guid, financialAidFund.Id);
                Assert.AreEqual(expectedResults.Code, financialAidFund.Code);
            }


            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task FinancialAidFundService_GetFinancialAidFundByGuid_Empty()
            {
                await financialAidFundService.GetFinancialAidFundsByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task FinancialAidFundService_GetFinancialAidFundByGuid_Null()
            {
                await financialAidFundService.GetFinancialAidFundsByGuidAsync(null);
            }

            [TestMethod]
            public async Task FinancialAidFundService_GetFinancialAidFundByGuid_Expected()
            {
                var expectedResults = financialAidFundCollection.Where(c => c.Guid == "1df164eb-8178-4321-a9f7-24f12d3991d8").FirstOrDefault();
                var financialAidFund = await financialAidFundService.GetFinancialAidFundsByGuidAsync("1df164eb-8178-4321-a9f7-24f12d3991d8");
                Assert.AreEqual(expectedResults.Guid, financialAidFund.Id);
                Assert.AreEqual(expectedResults.Code, financialAidFund.Code);
            }

            [TestMethod]
            public async Task FinancialAidFundService_GetFinancialAidFundByGuid_Properties()
            {
                var expectedResults = financialAidFundCollection.Where(c => c.Guid == "1df164eb-8178-4321-a9f7-24f12d3991d8").FirstOrDefault();
                var financialAidFund = await financialAidFundService.GetFinancialAidFundsByGuidAsync("1df164eb-8178-4321-a9f7-24f12d3991d8");
                Assert.IsNotNull(financialAidFund.Id);
                Assert.IsNotNull(financialAidFund.Code);
            }
        }
    }
}
