﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Coordination.FinancialAid.Services;
using Ellucian.Colleague.Domain.Entities;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Colleague.Domain.FinancialAid.Services;
using Ellucian.Colleague.Domain.FinancialAid.Tests;
using Ellucian.Colleague.Domain.Student;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Domain.Student.Tests;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.TestUtil;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Colleague.Domain.Base.Repositories;

namespace Ellucian.Colleague.Coordination.FinancialAid.Tests.Services
{
    [TestClass]
    public class FafsaServiceTests : FinancialAidServiceTestsSetup
    {
        public Mock<IFafsaRepository> fafsaRepositoryMock;
        public Mock<ITermRepository> termRepositoryMock;
        public Mock<IFinancialAidOfficeRepository> financialAidOfficeRepositoryMock;
        public Mock<IStudentAwardYearRepository> studentAwardYearRepositoryMock;
        private IConfigurationRepository baseConfigurationRepository;
        private Mock<IConfigurationRepository> baseConfigurationRepositoryMock;


        public TestFinancialAidOfficeRepository expectedFinancialAidOfficeRepository;
        public TestStudentAwardYearRepository expectedStudentAwardYearRepository;
        public TestTermRepository expectedTermRepository;
        public TestFafsaRepository expectedFafsaRepository;

        public ITypeAdapter<Colleague.Domain.FinancialAid.Entities.Fafsa, Fafsa> fafsaDtoAdapter;

        public FunctionEqualityComparer<Fafsa> fafsaDtoComparer;

        public void FafsaServiceTestsInitialize()
        {
            BaseInitialize();

            fafsaRepositoryMock = new Mock<IFafsaRepository>();
            termRepositoryMock = new Mock<ITermRepository>();
            financialAidOfficeRepositoryMock = new Mock<IFinancialAidOfficeRepository>();
            studentAwardYearRepositoryMock = new Mock<IStudentAwardYearRepository>();
            baseConfigurationRepositoryMock = new Mock<IConfigurationRepository>();
            baseConfigurationRepository = baseConfigurationRepositoryMock.Object;

            expectedFinancialAidOfficeRepository = new TestFinancialAidOfficeRepository();
            expectedStudentAwardYearRepository = new TestStudentAwardYearRepository();
            expectedTermRepository = new TestTermRepository();
            expectedFafsaRepository = new TestFafsaRepository();

            fafsaDtoComparer = new FunctionEqualityComparer<Fafsa>(
                (f1, f2) => (f1.Id == f2.Id && f1.StudentId == f2.StudentId && f1.AwardYear == f2.AwardYear),
                (f) => (f.Id.GetHashCode()));

            fafsaDtoAdapter = new AutoMapperAdapter<Colleague.Domain.FinancialAid.Entities.Fafsa, Fafsa>(adapterRegistryMock.Object, loggerMock.Object);
        }

        [TestClass]
        public class GetStudentFafsasTests : FafsaServiceTests
        {
            public string studentId;

            //Domain entities can be modified for tests by changing the record representations in the test repositories
            public IEnumerable<Colleague.Domain.FinancialAid.Entities.FinancialAidOffice> financialAidOfficeEntities
            {
                get
                {
                    return expectedFinancialAidOfficeRepository.GetFinancialAidOffices();
                }
            }
            public IEnumerable<Colleague.Domain.FinancialAid.Entities.StudentAwardYear> studentAwardYearEntities
            {
                get
                {
                    return expectedStudentAwardYearRepository.GetStudentAwardYears(studentId, new CurrentOfficeService(financialAidOfficeEntities));
                }
            }
            public IEnumerable<Colleague.Domain.FinancialAid.Entities.Fafsa> fafsaEntities
            {
                get
                {
                    return expectedFafsaRepository.GetFafsas(new List<string>() { studentId }, studentAwardYearEntities.Select(y => y.Code));
                }
            }

            //Dtos
            public IEnumerable<Fafsa> expectedFafsas
            {
                get
                {
                    return fafsaEntities.Select(fafsa => fafsaDtoAdapter.MapToType(fafsa));
                }
            }
            public IEnumerable<Fafsa> actualFafsas
            {
                get
                {
                    return fafsaService.GetStudentFafsas(studentId);
                }
            }

            public FafsaService fafsaService;

            [TestInitialize]
            public void Initialize()
            {
                FafsaServiceTestsInitialize();

                studentId = currentUserFactory.CurrentUser.PersonId;

                financialAidOfficeRepositoryMock.Setup(r => r.GetFinancialAidOffices())
                    .Returns(() => expectedFinancialAidOfficeRepository.GetFinancialAidOffices());

                studentAwardYearRepositoryMock.Setup(r => r.GetStudentAwardYears(It.IsAny<string>(), It.IsAny<CurrentOfficeService>()))
                    .Returns<string, CurrentOfficeService>((id, currentOfficeService) => expectedStudentAwardYearRepository.GetStudentAwardYears(id, currentOfficeService));

                fafsaRepositoryMock.Setup(r => r.GetFafsas(It.IsAny<IEnumerable<string>>(), It.IsAny<IEnumerable<string>>()))
                    .Returns<IEnumerable<string>, IEnumerable<string>>(
                        (ids, awardYearCodes) => expectedFafsaRepository.GetFafsas(ids, awardYearCodes));

                adapterRegistryMock.Setup(r => r.GetAdapter<Colleague.Domain.FinancialAid.Entities.Fafsa, Fafsa>())
                    .Returns(fafsaDtoAdapter);

                fafsaService = new FafsaService(adapterRegistryMock.Object, fafsaRepositoryMock.Object, termRepositoryMock.Object, financialAidOfficeRepositoryMock.Object, studentAwardYearRepositoryMock.Object, baseConfigurationRepository, currentUserFactory, roleRepositoryMock.Object, loggerMock.Object);
            }

            [TestMethod]
            public void ExpectedEqualsActualTest()
            {
                Assert.IsTrue(expectedFafsas.Count() > 0);
                Assert.AreEqual(expectedFafsas.Count(), actualFafsas.Count());
                CollectionAssert.AreEqual(expectedFafsas.ToList(), actualFafsas.ToList(), fafsaDtoComparer);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void StudentIdRequiredTest()
            {
                fafsaService.GetStudentFafsas(null);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public void UserNotRequestingSelfAndUserNotCounselorTest()
            {
                Assert.IsFalse(currentUserFactory.CurrentUser.IsInRole("FINANCIAL AID COUNSELOR"));

                try
                {
                    fafsaService.GetStudentFafsas("foobar");
                }
                catch (Exception e)
                {
                    loggerMock.Verify(l => l.Error(string.Format("{0} does not have permission to access fafsa information for {1}", currentUserFactory.CurrentUser.PersonId, "foobar")));
                    throw;
                }
            }

            [TestMethod]
            public void UserIsCounselorTest()
            {
                currentUserFactory = new CurrentUserSetup.CounselorUserFactory();
                counselorRole.AddPermission(new Permission(StudentPermissionCodes.ViewFinancialAidInformation));
                roleRepositoryMock.Setup(r => r.Roles).Returns(new List<Role>() { counselorRole });

                CollectionAssert.AreEqual(expectedFafsas.ToList(), actualFafsas.ToList(), fafsaDtoComparer);
            }

            [TestMethod]
            public void StudentAwardYearsIsNull_ReturnEmptyListTest()
            {
                studentAwardYearRepositoryMock.Setup(r => r.GetStudentAwardYears(It.IsAny<string>(), It.IsAny<CurrentOfficeService>()))
                    .Returns<string, CurrentOfficeService>((id, currentOfficeService) => null);

                Assert.AreEqual(0, actualFafsas.Count());
                loggerMock.Verify(l => l.Info(string.Format("Student {0} has no award years", studentId)));
            }

            [TestMethod]
            public void StudentAwardYearsIsEmpty_ReturnEmptyListTest()
            {
                studentAwardYearRepositoryMock.Setup(r => r.GetStudentAwardYears(It.IsAny<string>(), It.IsAny<CurrentOfficeService>()))
                    .Returns<string, CurrentOfficeService>((id, currentOfficeService) => new List<Domain.FinancialAid.Entities.StudentAwardYear>());

                Assert.AreEqual(0, actualFafsas.Count());
                loggerMock.Verify(l => l.Info(string.Format("Student {0} has no award years", studentId)));
            }

            [TestMethod]
            public void FafsaEntitiesIsNull_ReturnEmptyListTest()
            {
                fafsaRepositoryMock.Setup(r => r.GetFafsas(It.IsAny<IEnumerable<string>>(), It.IsAny<IEnumerable<string>>()))
                    .Returns<IEnumerable<string>, IEnumerable<string>>(
                        (ids, awardYearCodes) => null);

                Assert.AreEqual(0, actualFafsas.Count());
                loggerMock.Verify(l => l.Info(string.Format("Student {0} has no fafsas", studentId)));
            }

            [TestMethod]
            public void FafsaEntitiesIsEmpty_ReturnEmptyListTest()
            {
                fafsaRepositoryMock.Setup(r => r.GetFafsas(It.IsAny<IEnumerable<string>>(), It.IsAny<IEnumerable<string>>()))
                    .Returns<IEnumerable<string>, IEnumerable<string>>(
                        (ids, awardYearCodes) => new List<Domain.FinancialAid.Entities.Fafsa>());

                Assert.AreEqual(0, actualFafsas.Count());
                loggerMock.Verify(l => l.Info(string.Format("Student {0} has no fafsas", studentId)));
            }


        }
    }
}
