//Copyright 2017 Ellucian Company L.P. and its affiliates.


using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.FinancialAid.Services;
using Ellucian.Colleague.Domain.FinancialAid.Entities;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Colleague.Dtos;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.FinancialAid.Tests;
using Ellucian.Colleague.Domain.Entities;
using Ellucian.Colleague.Domain.FinancialAid;

namespace Ellucian.Colleague.Coordination.FinancialAid.Tests.Services
{    
    [TestClass]
    public class FinancialAidApplicationOutcomeerviceTests : FinancialAidServiceTestsSetup
    {
        private const string financialAidApplicationOutcomeGuid = "7a2bf6b5-cdcd-4c8f-b5d8-3053bf5b3fbc";
        private const string financialAidApplicationOutcomeStudent = "0003914";
        private IEnumerable<Fafsa> _financialAidApplicationOutcomeCollection;
        private FinancialAidApplicationOutcomeService _financialAidApplicationOutcomeService;
        private Mock<IFinancialAidApplicationOutcomeRepository> _financialAidApplicationOutcomeRepositoryMock;
        private Mock<IPersonRepository> _personRepositoryMock;
        private Mock<IFinancialAidReferenceDataRepository> _referenceRepositoryMock;
        public TestFafsaRepository expectedFafsaRepository;
        private IConfigurationRepository baseConfigurationRepository;
        private Mock<IConfigurationRepository> baseConfigurationRepositoryMock;

        [TestInitialize]
        public async void Initialize()
        {
            BaseInitialize();
            baseConfigurationRepositoryMock = new Mock<IConfigurationRepository>();
            baseConfigurationRepository = baseConfigurationRepositoryMock.Object;

            currentUserFactory = new CurrentUserSetup.CounselorUserFactory();
            counselorRole.AddPermission(new Permission(FinancialAidPermissionCodes.ViewFinancialAidApplicationOutcomes));
            roleRepositoryMock.Setup(r => r.Roles).Returns(new List<Domain.Entities.Role>() { counselorRole });

            _financialAidApplicationOutcomeRepositoryMock = new Mock<IFinancialAidApplicationOutcomeRepository>();
            _personRepositoryMock = new Mock<IPersonRepository>();
            _referenceRepositoryMock = new Mock<IFinancialAidReferenceDataRepository>();
            loggerMock = new Mock<ILogger>();
            expectedFafsaRepository = new TestFafsaRepository();

            _financialAidApplicationOutcomeCollection = expectedFafsaRepository.GetFafsas(new List<string>() { "0003914" }, new List<string>() { "2013" });

            _financialAidApplicationOutcomeRepositoryMock.Setup(repo => repo.GetAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<bool>(), It.IsAny<List<string>>()))
                .ReturnsAsync(new Tuple<IEnumerable<Fafsa>, int>(_financialAidApplicationOutcomeCollection, 3));

            _financialAidApplicationOutcomeService = new FinancialAidApplicationOutcomeService(_financialAidApplicationOutcomeRepositoryMock.Object,
                _personRepositoryMock.Object, _referenceRepositoryMock.Object, baseConfigurationRepository,
                adapterRegistryMock.Object, currentUserFactory, roleRepositoryMock.Object, loggerMock.Object);
        }

        [TestCleanup]
        public void Cleanup()
        {
            BaseCleanup();

            _financialAidApplicationOutcomeService = null;
            _financialAidApplicationOutcomeCollection = null;
            _personRepositoryMock = null;
            _referenceRepositoryMock = null;
            adapterRegistryMock = null;
            currentUserFactory = null;
            roleRepositoryMock = null;
            loggerMock = null;
        }

        [TestMethod]
        public async Task FinancialAidApplicationOutcomeService_GetFinancialAidApplicationOutcomeAsync()
        {
            var resultsTuple = await _financialAidApplicationOutcomeService.GetAsync(0, 3, true);
            var results = resultsTuple.Item1;
            Assert.IsTrue(results is IEnumerable<FinancialAidApplicationOutcome>);
            Assert.IsNotNull(results);
        }

        [TestMethod]
        public async Task FinancialAidApplicationOutcomeService_GetFinancialAidApplicationOutcomeAsync_Count()
        {
            var resultsTuple = await _financialAidApplicationOutcomeService.GetAsync(0, 3, true);
            var resultCount = resultsTuple.Item2;
            Assert.AreEqual(resultCount, 3);
        }

        [TestMethod]
        public async Task FinancialAidApplicationOutcomeService_GetFinancialAidApplicationOutcomeAsync_Properties()
        {
            var result =
                (await _financialAidApplicationOutcomeService.GetAsync(0, 3, true)).Item1.FirstOrDefault(x => x.Id == financialAidApplicationOutcomeGuid);
            Assert.IsNotNull(result.Id);
           
        }

        [TestMethod]
        public async Task FinancialAidApplicationOutcomeService_GetFinancialAidApplicationOutcomeAsync_Expected()
        {
            var expectedResults = _financialAidApplicationOutcomeCollection.FirstOrDefault(c => c.Guid == financialAidApplicationOutcomeGuid);
            var actualResult =
                (await _financialAidApplicationOutcomeService.GetAsync(0, 3, true)).Item1.FirstOrDefault(x => x.Id == financialAidApplicationOutcomeGuid);
            Assert.AreEqual(expectedResults.Guid, actualResult.Id);
            
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentNullException))]
        public async Task FinancialAidApplicationOutcomeService_GetFinancialAidApplicationOutcomeByGuidAsync_Empty()
        {
            await _financialAidApplicationOutcomeService.GetByIdAsync("");
        }

        [TestMethod]
        [ExpectedException(typeof (ArgumentNullException))]
        public async Task FinancialAidApplicationOutcomeService_GetFinancialAidApplicationOutcomeByGuidAsync_Null()
        {
            await _financialAidApplicationOutcomeService.GetByIdAsync(null);
        }

        [TestMethod]
        [ExpectedException(typeof (KeyNotFoundException))]
        public async Task FinancialAidApplicationOutcomeService_GetFinancialAidApplicationOutcomeByGuidAsync_InvalidId()
        {
            _financialAidApplicationOutcomeRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<string>()))
                .Throws<KeyNotFoundException>();

            await _financialAidApplicationOutcomeService.GetByIdAsync("99");
        }

        [TestMethod]
        public async Task FinancialAidApplicationOutcomeService_GetFinancialAidApplicationOutcomeByGuidAsync_Expected()
        {
            var expectedResults =
                _financialAidApplicationOutcomeCollection.First(c => c.Guid == financialAidApplicationOutcomeGuid);
            _financialAidApplicationOutcomeRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<string>()))
                .ReturnsAsync(expectedResults);
            var actualResult =
                await _financialAidApplicationOutcomeService.GetByIdAsync(financialAidApplicationOutcomeGuid);
            Assert.AreEqual(expectedResults.Guid, actualResult.Id);
            
        }

        [TestMethod]
        public async Task FinancialAidApplicationOutcomeService_GetFinancialAidApplicationOutcomeByGuidAsync_Properties()
        {
            var expectedResults =
                _financialAidApplicationOutcomeCollection.First(c => c.Guid == financialAidApplicationOutcomeGuid);
            _financialAidApplicationOutcomeRepositoryMock.Setup(repo => repo.GetByIdAsync(It.IsAny<string>()))
                .ReturnsAsync(expectedResults);
            var result =
                await _financialAidApplicationOutcomeService.GetByIdAsync(financialAidApplicationOutcomeGuid);
            Assert.IsNotNull(result.Id);
            
        }
    }
}