﻿/*Copyright 2015-2017 Ellucian Company L.P. and its affiliates.*/

using Ellucian.Colleague.Coordination.FinancialAid.Adapters;
using Ellucian.Colleague.Coordination.FinancialAid.Services;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Entities;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Colleague.Domain.FinancialAid.Services;
using Ellucian.Colleague.Domain.FinancialAid.Tests;
using Ellucian.Colleague.Domain.Student;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.TestUtil;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.FinancialAid.Tests.Services
{
    [TestClass]
    public class ShoppingSheetServiceTests : FinancialAidServiceTestsSetup
    {

        public Mock<IFinancialAidOfficeRepository> financialAidOfficeRepositoryMock;
        public Mock<IStudentAwardYearRepository> studentAwardYearRepositoryMock;
        public Mock<IFinancialAidReferenceDataRepository> financialAidReferenceDataRepositoryMock;
        public Mock<IStudentAwardRepository> studentAwardRepositoryMock;
        public Mock<IStudentBudgetComponentRepository> studentBudgetComponentRepositoryMock;
        public Mock<IFafsaRepository> fafsaRepositoryMock;
        public Mock<IProfileApplicationRepository> profileApplicationRepositoryMock;
        public Mock<IRuleTableRepository> ruleTableRepositoryMock;
        public Mock<IRuleRepository> ruleRepositoryMock;

        public TestFinancialAidOfficeRepository financialAidOfficeRepository;
        public TestStudentAwardYearRepository studentAwardYearRepository;
        public TestFinancialAidReferenceDataRepository financialAidReferenceDataRepository;
        public TestStudentAwardRepository studentAwardRepository;
        public TestStudentBudgetComponentRepository studentBudgetComponentRepository;
        public TestFafsaRepository fafsaRepository;
        public TestProfileApplicationRepository profileApplicationRepository;
      

        public ITypeAdapter<Domain.FinancialAid.Entities.ShoppingSheet, ShoppingSheet> shoppingSheetEntityToDtoAdapter;

        public FunctionEqualityComparer<ShoppingSheet> shoppingSheetDtoComparer;

        public void ShoppingSheetServiceTestsInitialize()
        {
            BaseInitialize();

            financialAidOfficeRepositoryMock = new Mock<IFinancialAidOfficeRepository>();
            studentAwardYearRepositoryMock = new Mock<IStudentAwardYearRepository>();
            financialAidReferenceDataRepositoryMock = new Mock<IFinancialAidReferenceDataRepository>();
            studentAwardRepositoryMock = new Mock<IStudentAwardRepository>();
            studentBudgetComponentRepositoryMock = new Mock<IStudentBudgetComponentRepository>();
            fafsaRepositoryMock = new Mock<IFafsaRepository>();
            profileApplicationRepositoryMock = new Mock<IProfileApplicationRepository>();
            ruleTableRepositoryMock = new Mock<IRuleTableRepository>();
            ruleRepositoryMock = new Mock<IRuleRepository>();


            financialAidOfficeRepository = new TestFinancialAidOfficeRepository();
            studentAwardYearRepository = new TestStudentAwardYearRepository();
            financialAidReferenceDataRepository = new TestFinancialAidReferenceDataRepository();
            studentAwardRepository = new TestStudentAwardRepository();
            studentBudgetComponentRepository = new TestStudentBudgetComponentRepository();
            fafsaRepository = new TestFafsaRepository();
            profileApplicationRepository = new TestProfileApplicationRepository();

            shoppingSheetEntityToDtoAdapter = new ShoppingSheetEntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);

            shoppingSheetDtoComparer = new FunctionEqualityComparer<ShoppingSheet>(
                (s1, s2) => s1.AwardYear == s2.AwardYear && s1.StudentId == s2.StudentId,
                (ss) => ss.AwardYear.GetHashCode() ^ ss.StudentId.GetHashCode());
        }

        [TestClass]
        public class GetShoppingSheetsTests : ShoppingSheetServiceTests
        {
            public string studentId;

            ////Domain entities can be modified for tests by changing the record representations in the test repositories
            public IEnumerable<Domain.FinancialAid.Entities.FinancialAidOffice> financialAidOfficeEntities
            { get { return financialAidOfficeRepository.GetFinancialAidOffices(); } }

            public IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear> studentAwardYearEntities
            { get { return studentAwardYearRepository.GetStudentAwardYears(studentId, new CurrentOfficeService(financialAidOfficeEntities)); } }

            public IEnumerable<Domain.FinancialAid.Entities.BudgetComponent> budgetComponentEntities
            { get { return financialAidReferenceDataRepository.BudgetComponents; } }

            public IEnumerable<Domain.FinancialAid.Entities.StudentAward> studentAwardEntities
            { get { return studentAwardRepository.GetAllStudentAwards(studentId, studentAwardYearEntities, financialAidReferenceDataRepository.Awards, financialAidReferenceDataRepository.AwardStatuses); } }

            public IEnumerable<Domain.FinancialAid.Entities.StudentBudgetComponent> studentBudgetComponentEntities
            { get { return studentBudgetComponentRepository.GetStudentBudgetComponents(studentId, studentAwardYearEntities); } }

            public IEnumerable<Domain.FinancialAid.Entities.Fafsa> fafsaEntities
            { get { return fafsaRepository.GetFafsas(new List<string>() { studentId }, studentAwardYearEntities.Select(y => y.Code)); } }

            public IEnumerable<Domain.FinancialAid.Entities.ProfileApplication> profileApplicationEntities
            { get { return profileApplicationRepository.GetProfileApplications(studentId, studentAwardYearEntities); } }

            public IEnumerable<Domain.FinancialAid.Entities.FinancialAidApplication2> financialAidApplicationEntities
            {
                get
                {
                    var financialAidApplications = new List<Domain.FinancialAid.Entities.FinancialAidApplication2>();
                    financialAidApplications.AddRange(fafsaEntities);
                    financialAidApplications.AddRange(profileApplicationEntities);
                    return financialAidApplications;
                }
            }

            public IEnumerable<Domain.FinancialAid.Entities.ShoppingSheet> shoppingSheetEntities
            {
                get
                {
                    return studentAwardYearEntities.Select(studentAwardYear =>
                        {
                            try { return ShoppingSheetDomainService.BuildShoppingSheet(studentAwardYear, studentAwardEntities, budgetComponentEntities, studentBudgetComponentEntities, financialAidApplicationEntities, null); }
                            catch (Exception) { return null; }
                        }).Where(s => s != null);
                }
            }

            //Dtos
            public List<ShoppingSheet> expectedShoppingSheets
            {
                get { return shoppingSheetEntities.Select(shoppingSheet => shoppingSheetEntityToDtoAdapter.MapToType(shoppingSheet)).ToList(); }
            }

            public async Task<List<ShoppingSheet>> actualShoppingSheetsAsync()
            {
                return (await shoppingSheetService.GetShoppingSheetsAsync(studentId)).ToList(); 
            }

            public ShoppingSheetService shoppingSheetService;

            [TestInitialize]
            public void Initialize()
            {
                ShoppingSheetServiceTestsInitialize();

                studentId = currentUserFactory.CurrentUser.PersonId;

                financialAidOfficeRepositoryMock.Setup(r => r.GetFinancialAidOffices())
                    .Returns(() => financialAidOfficeRepository.GetFinancialAidOffices());

                studentAwardYearRepositoryMock.Setup(r => r.GetStudentAwardYears(It.IsAny<string>(), It.IsAny<CurrentOfficeService>()))
                    .Returns<string, CurrentOfficeService>((id, currentOfficeService) => studentAwardYearRepository.GetStudentAwardYears(id, currentOfficeService));

                financialAidReferenceDataRepositoryMock.Setup(r => r.Awards)
                    .Returns(() => financialAidReferenceDataRepository.Awards);

                financialAidReferenceDataRepositoryMock.Setup(r => r.AwardStatuses)
                    .Returns(() => financialAidReferenceDataRepository.AwardStatuses);

                financialAidReferenceDataRepositoryMock.Setup(r => r.BudgetComponents)
                    .Returns(() => financialAidReferenceDataRepository.BudgetComponents);

                studentAwardRepositoryMock.Setup(r => r.GetAllStudentAwards(It.IsAny<string>(), It.IsAny<IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>>(), It.IsAny<IEnumerable<Domain.FinancialAid.Entities.Award>>(), It.IsAny<IEnumerable<Domain.FinancialAid.Entities.AwardStatus>>()))
                    .Returns<string, IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>, IEnumerable<Domain.FinancialAid.Entities.Award>, IEnumerable<Domain.FinancialAid.Entities.AwardStatus>>
                        ((id, studentAwardYears, awards, awardStatuses) => studentAwardRepository.GetAllStudentAwards(id, studentAwardYears, awards, awardStatuses));

                studentBudgetComponentRepositoryMock.Setup(r => r.GetStudentBudgetComponents(It.IsAny<string>(), It.IsAny<IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>>()))
                    .Returns<string, IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>>((id, studentAwardYears) => studentBudgetComponentRepository.GetStudentBudgetComponents(id, studentAwardYears));

                fafsaRepositoryMock.Setup(r => r.GetFafsas(It.IsAny<IEnumerable<string>>(), It.IsAny<IEnumerable<string>>()))
                    .Returns<IEnumerable<string>, IEnumerable<string>>((ids, awardYears) => fafsaRepository.GetFafsas(ids, awardYears));

                profileApplicationRepositoryMock.Setup(r => r.GetProfileApplications(It.IsAny<string>(), It.IsAny<IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>>()))
                    .Returns<string, IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>>
                    ((id, studentAwardYears) => profileApplicationRepository.GetProfileApplications(id, studentAwardYears));

                adapterRegistryMock.Setup(r => r.GetAdapter<Domain.FinancialAid.Entities.ShoppingSheet, ShoppingSheet>())
                    .Returns(shoppingSheetEntityToDtoAdapter);

                shoppingSheetService = new ShoppingSheetService(
                    adapterRegistryMock.Object,
                    financialAidOfficeRepositoryMock.Object,
                    studentAwardYearRepositoryMock.Object,
                    financialAidReferenceDataRepositoryMock.Object,
                    studentAwardRepositoryMock.Object,
                    studentBudgetComponentRepositoryMock.Object,
                    fafsaRepositoryMock.Object,
                    profileApplicationRepositoryMock.Object,
                    ruleTableRepositoryMock.Object,
                    ruleRepositoryMock.Object,
                    baseConfigurationRepository,
                    currentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object);
            }

            [TestMethod]
            public async Task ExpectedEqualsActual()
            {
                CollectionAssert.AreEqual(expectedShoppingSheets, await actualShoppingSheetsAsync(), shoppingSheetDtoComparer);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task StudentIdIsRequiredTest()
            {
                await shoppingSheetService.GetShoppingSheetsAsync(null);
            }

            [TestMethod]
            public async Task CounselorCanAccessDataTest()
            {
                currentUserFactory = new CurrentUserSetup.CounselorUserFactory();
                counselorRole.AddPermission(new Permission(StudentPermissionCodes.ViewFinancialAidInformation));
                roleRepositoryMock.Setup(r => r.Roles).Returns(new List<Role>() { counselorRole });

                CollectionAssert.AreEqual(expectedShoppingSheets, await actualShoppingSheetsAsync(), shoppingSheetDtoComparer);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task UserNotRequestingSelfAndUserNotCounselorTest()
            {
                Assert.IsFalse(currentUserFactory.CurrentUser.IsInRole("FINANCIAL AID COUNSELOR"));

                try
                {
                    await shoppingSheetService.GetShoppingSheetsAsync("foobar");
                }
                catch (Exception)
                {
                    loggerMock.Verify(l => l.Error(string.Format("{0} does not have permission to access shopping sheet resources for {1}", currentUserFactory.CurrentUser.PersonId, "foobar")));
                    throw;
                }
            }

            [TestMethod]
            public async Task NullStudentAwardYearsReturnsEmptyListTest()
            {
                studentAwardYearRepositoryMock.Setup(r => r.GetStudentAwardYears(It.IsAny<string>(), It.IsAny<CurrentOfficeService>()))
                    .Returns<string, CurrentOfficeService>((id, currentOfficeService) => null);

                Assert.AreEqual(0, (await actualShoppingSheetsAsync()).Count());
                loggerMock.Verify(l => l.Info(string.Format("Student {0} has no award years for which to get shopping sheets", studentId)));
            }

            [TestMethod]
            public async Task EmptyStudentAwardYearsReturnsEmptyListTest()
            {
                studentAwardYearRepositoryMock.Setup(r => r.GetStudentAwardYears(It.IsAny<string>(), It.IsAny<CurrentOfficeService>()))
                    .Returns<string, CurrentOfficeService>((id, currentOfficeService) => new List<Domain.FinancialAid.Entities.StudentAwardYear>());

                Assert.AreEqual(0, (await actualShoppingSheetsAsync()).Count());
                loggerMock.Verify(l => l.Info(string.Format("Student {0} has no award years for which to get shopping sheets", studentId)));
            }

            /// <summary>
            /// ShoppingSheetDomainService will throw an exception if BudgetComponents are null, but StudentBudgetComponents
            /// are not null. That's tested in ShoppingSheetDomainServiceTests, so just testing that it has no effect
            /// on the ShoppingSheetService method.
            /// </summary>
            [TestMethod]
            public async Task NullBudgetAndStudentBudgetComponentsHasNoEffectTest()
            {
                financialAidReferenceDataRepository.BudgetComponentData = new List<TestFinancialAidReferenceDataRepository.BudgetComponentRecord>();
                studentBudgetComponentRepository.csStudentRecords = new List<TestStudentBudgetComponentRepository.CsStudentRecord>();

                Assert.AreEqual(expectedShoppingSheets.Count(), (await actualShoppingSheetsAsync()).Count());
            }

            [TestMethod]
            public async Task NullStudentAwardsHasNoEffectTest()
            {
                studentAwardRepository.awardPeriodData = new List<TestStudentAwardRepository.TestAwardPeriodRecord>();

                Assert.AreEqual(expectedShoppingSheets.Count(), (await actualShoppingSheetsAsync()).Count());
            }

            [TestMethod]
            public async Task NullFafsasAndProfilesDoNotGetSentToShoppingSheetDomainServiceTest()
            {
                fafsaRepositoryMock.Setup(r => r.GetFafsas(It.IsAny<IEnumerable<string>>(), It.IsAny<IEnumerable<string>>()))
                    .Returns<IEnumerable<string>, IEnumerable<string>>((ids, awardYears) => null);

                profileApplicationRepositoryMock.Setup(r => r.GetProfileApplications(It.IsAny<string>(), It.IsAny<IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>>()))
                    .Returns<string, IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>>
                    ((id, studentAwardYears) => null);

                Assert.IsTrue((await actualShoppingSheetsAsync()).All(s => !s.FamilyContribution.HasValue));
            }

            [TestMethod]
            public async Task CatchExceptionThrownFromBuildingShoppingSheet_LogMessageTest()
            {
                studentBudgetComponentRepositoryMock.Setup(r => r.GetStudentBudgetComponents(It.IsAny<string>(), It.IsAny<IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>>()))
                    .Returns<string, IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>>((id, studentAwardYears) => null);
                studentAwardRepositoryMock.Setup(r => r.GetAllStudentAwards(It.IsAny<string>(), It.IsAny<IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>>(), It.IsAny<IEnumerable<Domain.FinancialAid.Entities.Award>>(), It.IsAny<IEnumerable<Domain.FinancialAid.Entities.AwardStatus>>()))
                    .Returns<string, IEnumerable<Domain.FinancialAid.Entities.StudentAwardYear>, IEnumerable<Domain.FinancialAid.Entities.Award>, IEnumerable<Domain.FinancialAid.Entities.AwardStatus>>
                        ((id, studentAwardYears, awards, awardStatuses) => null);

                Assert.AreEqual(0, (await actualShoppingSheetsAsync()).Count());

                loggerMock.Verify(l => l.Info(It.IsAny<Exception>(), "Unable to create shopping sheet for studentId {0} and awardYear {1}", It.IsAny<object[]>()));
            }

        }
    }
}
