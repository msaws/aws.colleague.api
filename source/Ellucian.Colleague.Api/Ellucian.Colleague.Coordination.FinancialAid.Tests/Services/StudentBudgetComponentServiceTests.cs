﻿/*Copyright 2015 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Coordination.FinancialAid.Services;
using Ellucian.Colleague.Domain.Entities;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Colleague.Domain.FinancialAid.Services;
using Ellucian.Colleague.Domain.FinancialAid.Tests;
using Ellucian.Colleague.Domain.Student;
using Ellucian.Colleague.Dtos.FinancialAid;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Web.Http.TestUtil;
using Ellucian.Colleague.Domain.Base.Repositories;

namespace Ellucian.Colleague.Coordination.FinancialAid.Tests.Services
{
    [TestClass]
    public class StudentBudgetComponentServiceTests : FinancialAidServiceTestsSetup
    {
        public string studentId;

        public TestFinancialAidOfficeRepository testFinancialAidOfficeRepository;
        public Mock<IFinancialAidOfficeRepository> financialAidOfficeRepositoryMock;

        public TestStudentAwardYearRepository testStudentAwardYearRepository;
        public Mock<IStudentAwardYearRepository> studentAwardYearRepositoryMock;

        public TestStudentBudgetComponentRepository testStudentBudgetComponentRepository;
        public Mock<IStudentBudgetComponentRepository> studentBudgetComponentRepositoryMock;

        private IConfigurationRepository baseConfigurationRepository;
        private Mock<IConfigurationRepository> baseConfigurationRepositoryMock;

        public ITypeAdapter<Colleague.Domain.FinancialAid.Entities.StudentBudgetComponent, StudentBudgetComponent> studentBudgetComponentDtoAdapter;

        public FunctionEqualityComparer<StudentBudgetComponent> studentBudgetComponentDtoComparer;

        public void StudentBudgetComponentsTestsInitialize()
        {
            BaseInitialize();

            studentId = currentUserFactory.CurrentUser.PersonId;

            studentBudgetComponentDtoComparer = new FunctionEqualityComparer<StudentBudgetComponent>(
                (sbc1, sbc2) => (sbc1.AwardYear == sbc2.AwardYear && sbc1.StudentId == sbc2.StudentId && sbc1.BudgetComponentCode == sbc2.BudgetComponentCode),
                (sbc) => (sbc.AwardYear.GetHashCode() ^ sbc.StudentId.GetHashCode() ^ sbc.BudgetComponentCode.GetHashCode()));

            testFinancialAidOfficeRepository = new TestFinancialAidOfficeRepository();
            testStudentAwardYearRepository = new TestStudentAwardYearRepository();
            testStudentBudgetComponentRepository = new TestStudentBudgetComponentRepository();
            studentBudgetComponentDtoAdapter = new AutoMapperAdapter<Colleague.Domain.FinancialAid.Entities.StudentBudgetComponent, StudentBudgetComponent>(adapterRegistryMock.Object, loggerMock.Object);

            financialAidOfficeRepositoryMock = new Mock<IFinancialAidOfficeRepository>();
            studentAwardYearRepositoryMock = new Mock<IStudentAwardYearRepository>();
            studentBudgetComponentRepositoryMock = new Mock<IStudentBudgetComponentRepository>();

            baseConfigurationRepositoryMock = new Mock<IConfigurationRepository>();
            baseConfigurationRepository = baseConfigurationRepositoryMock.Object;
        }

        [TestClass]
        public class GetStudentBudgetComponentsTests : StudentBudgetComponentServiceTests
        {

            public IEnumerable<StudentBudgetComponent> expectedBudgets
            {
                get
                {
                    return testStudentBudgetComponentRepository
                        .GetStudentBudgetComponents(
                            studentId,
                            testStudentAwardYearRepository.GetStudentAwardYears(
                                studentId,
                                new CurrentOfficeService(testFinancialAidOfficeRepository.GetFinancialAidOffices())))
                        .Select(budgetEntity => studentBudgetComponentDtoAdapter.MapToType(budgetEntity));
                }
            }

            //Initialize this
            public StudentBudgetComponentService actualService;

            public IEnumerable<StudentBudgetComponent> actualBudgets
            {
                get
                {
                    return actualService.GetStudentBudgetComponents(studentId);
                }
            }



            [TestInitialize]
            public void Initialize()
            {
                StudentBudgetComponentsTestsInitialize();

                financialAidOfficeRepositoryMock.Setup(r => r.GetFinancialAidOffices())
                    .Returns(() => testFinancialAidOfficeRepository.GetFinancialAidOffices());

                studentAwardYearRepositoryMock.Setup(r => r.GetStudentAwardYears(It.IsAny<string>(), It.IsAny<CurrentOfficeService>()))
                    .Returns<string, CurrentOfficeService>((id, officeService) => testStudentAwardYearRepository.GetStudentAwardYears(id, officeService));

                studentBudgetComponentRepositoryMock.Setup(r => r.GetStudentBudgetComponents(It.IsAny<string>(), It.IsAny<IEnumerable<Colleague.Domain.FinancialAid.Entities.StudentAwardYear>>()))
                    .Returns<string, IEnumerable<Colleague.Domain.FinancialAid.Entities.StudentAwardYear>>((id, studentAwardYears) =>
                        testStudentBudgetComponentRepository.GetStudentBudgetComponents(id, studentAwardYears));

                adapterRegistryMock.Setup(r => r.GetAdapter<Colleague.Domain.FinancialAid.Entities.StudentBudgetComponent, StudentBudgetComponent>())
                    .Returns(() => studentBudgetComponentDtoAdapter);

                actualService = new StudentBudgetComponentService(
                    adapterRegistryMock.Object,
                    studentBudgetComponentRepositoryMock.Object,
                    studentAwardYearRepositoryMock.Object,
                    financialAidOfficeRepositoryMock.Object,
                    baseConfigurationRepository,
                    currentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object);
            }

            [TestMethod]
            public void ExpectedEqualsActualTest()
            {
                Assert.IsNotNull(actualBudgets);
                Assert.IsTrue(actualBudgets.Count() > 0);
                CollectionAssert.AreEqual(expectedBudgets.ToList(), actualBudgets.ToList(), studentBudgetComponentDtoComparer);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void StudentIdRequiredTest()
            {
                actualService.GetStudentBudgetComponents(null);
            }

            //The studentId is initialized to the current user id
            [TestMethod]
            public void StudentIdIsCurrentUserTest()
            {
                Assert.IsTrue(actualBudgets.All(budget => budget.StudentId == studentId));
            }

            [TestMethod]
            public void CurrentUserIsCounselorTest()
            {
                currentUserFactory = new CurrentUserSetup.CounselorUserFactory();
                counselorRole.AddPermission(new Permission(StudentPermissionCodes.ViewFinancialAidInformation));
                roleRepositoryMock.Setup(r => r.Roles).Returns(new List<Role>() { counselorRole });

                CollectionAssert.AreEqual(expectedBudgets.ToList(), actualBudgets.ToList(), studentBudgetComponentDtoComparer);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public void UserNotRequestingSelfAndNotCounselorTest()
            {
                Assert.IsFalse(currentUserFactory.CurrentUser.IsInRole("FINANCIAL AID COUNSELOR"));

                try
                {
                    actualService.GetStudentBudgetComponents("foobar");
                }
                catch (Exception)
                {
                    loggerMock.Verify(l => l.Error(string.Format("User {0} does not have permission to get StudentBudgetComponents for student {1}", currentUserFactory.CurrentUser.PersonId, "foobar")));
                    throw;
                }
            }

            [TestMethod]
            public void NullStudentAwardYears_ReturnEmptyListTest()
            {
                studentAwardYearRepositoryMock.Setup(r => r.GetStudentAwardYears(It.IsAny<string>(), It.IsAny<CurrentOfficeService>()))
                    .Returns<string, CurrentOfficeService>((id, officeService) => null);

                Assert.AreEqual(0, actualBudgets.Count());
                loggerMock.Verify(l => l.Info(string.Format("Student {0} has no award years for which to get budget components", studentId)));
            }

            [TestMethod]
            public void NullBudgetComponents_ReturnEmptyListTest()
            {
                studentBudgetComponentRepositoryMock.Setup(r => r.GetStudentBudgetComponents(It.IsAny<string>(), It.IsAny<IEnumerable<Colleague.Domain.FinancialAid.Entities.StudentAwardYear>>()))
                    .Returns<string, IEnumerable<Colleague.Domain.FinancialAid.Entities.StudentAwardYear>>((id, studentAwardYears) => null);

                Assert.AreEqual(0, actualBudgets.Count());
                loggerMock.Verify(l => l.Info(string.Format("Student {0} has no budget components for any award years", studentId)));
            }

        }
    }
}
