﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.ResidenceLife;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;
using Moq;
using Ellucian.Web.Adapters;
using slf4net;
using Ellucian.Colleague.Coordination.ResidenceLife.Adapters;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Tests.Adapters
{
    // Test the mapping of a Meal Plan Assignment entity to dto
    [TestClass]
    public class MealPlanAssignmentEntityAdapterTests
    {
        private string id;
        private List<Domain.ResidenceLife.Entities.ResidenceLifeExternalId> externalIds = new List<Domain.ResidenceLife.Entities.ResidenceLifeExternalId>();
        private string personId;
        private MealPlan mealPlan;
        private int numberOfRatePeriods;
        private DateTime startDate;
        private DateTime? endDate;
        private MealPlanAssignmentStatus currentStatus;
        private DateTime currentStatusDate;
        private string term;
        private int? usedRatePeriods;
        private int? usedPercent;
        private string cardNumber;
        private decimal? overrideRate;
        private string overrideRateReason;
        private string overrideChargeCode;
        private string overrideReceivableType;
        private string overrideRefundFormula;
        private string comments;

        private Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignmentEntity;

        [TestInitialize]
        public void Initialize()
        {
            // Populate the entity
            id = "id12344";
            externalIds = new List<Domain.ResidenceLife.Entities.ResidenceLifeExternalId>() { new Domain.ResidenceLife.Entities.ResidenceLifeExternalId("EXTL1", "HD"), 
                          new Domain.ResidenceLife.Entities.ResidenceLifeExternalId("EXTL2", "RMS") };
            personId = "personid222";
            mealPlan = new MealPlan(new DateTime(2010, 1, 4), "a meal plan");
            mealPlan.Id = "anId";
            numberOfRatePeriods = 2;
            startDate = new DateTime(2014, 1, 1);
            endDate = new DateTime(2014, 5, 31);
            currentStatus = new MealPlanAssignmentStatus("A", "Active", MealPlanAssignmentStatusType.None);
            currentStatusDate = new DateTime(2014, 8, 10);
            term = "aterm";
            usedRatePeriods = 2;
            usedPercent = 30;
            cardNumber = "cardnum333";
            overrideRate = 45.55M;
            overrideRateReason = "OvrReason";
            overrideChargeCode = "CHG1";
            overrideReceivableType = "RCV";
            overrideRefundFormula = "Formula";
            comments = "Comments and more comments.";

            mealPlanAssignmentEntity = new Domain.ResidenceLife.Entities.MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            mealPlanAssignmentEntity.Id = id;
            mealPlanAssignmentEntity.CardNumber = cardNumber;
            mealPlanAssignmentEntity.SetOverrideRate(overrideRate, overrideRateReason);
            mealPlanAssignmentEntity.OverrideChargeCode = overrideChargeCode;
            mealPlanAssignmentEntity.OverrideReceivableType = overrideReceivableType;
            mealPlanAssignmentEntity.OverrideRefundFormula = overrideRefundFormula;
            mealPlanAssignmentEntity.Comments = comments;
            foreach (Domain.ResidenceLife.Entities.ResidenceLifeExternalId eid in externalIds)
            {
                mealPlanAssignmentEntity.AddExternalId(eid.ExternalId, eid.ExternalIdSource);
            }
        }

        [TestMethod]
        public void TestAllAdapterMappings()
        {
            // Instantiate the adapter using a mock logger and adapter registry
            var loggerMock = new Mock<ILogger>();
            var adapterRegistryMock = new Mock<IAdapterRegistry>();
            var mealPlanAssignmentDtoAdapter = new MealPlanAssignmentEntityAdapter(adapterRegistryMock.Object, loggerMock.Object);

            // Convert housing assignment domain entity to DTO
            Colleague.Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentDto =
                mealPlanAssignmentDtoAdapter.MapToType(mealPlanAssignmentEntity);

            Assert.AreEqual(mealPlanAssignmentEntity.Id, mealPlanAssignmentDto.Id);
            Assert.AreEqual(mealPlanAssignmentEntity.PersonId, mealPlanAssignmentDto.PersonId);
            Assert.AreEqual(mealPlanAssignmentEntity.MealPlanId, mealPlanAssignmentDto.MealPlanId);
            Assert.AreEqual(mealPlanAssignmentEntity.NumberOfRatePeriods, mealPlanAssignmentDto.NumberOfRatePeriods);
            Assert.AreEqual(mealPlanAssignmentEntity.StartDate, mealPlanAssignmentDto.StartDate);
            Assert.AreEqual(mealPlanAssignmentEntity.EndDate, mealPlanAssignmentDto.EndDate);
            Assert.AreEqual(mealPlanAssignmentEntity.CurrentStatusCode, mealPlanAssignmentDto.CurrentStatusCode);
            Assert.AreEqual(mealPlanAssignmentEntity.CurrentStatusDate, mealPlanAssignmentDto.CurrentStatusDate);
            Assert.AreEqual(mealPlanAssignmentEntity.TermId, mealPlanAssignmentDto.TermId);
            Assert.AreEqual(mealPlanAssignmentEntity.UsedRatePeriods, mealPlanAssignmentDto.UsedRatePeriods);
            Assert.AreEqual(mealPlanAssignmentEntity.UsedPercent, mealPlanAssignmentDto.UsedPercent);
            Assert.AreEqual(mealPlanAssignmentEntity.CardNumber, mealPlanAssignmentDto.CardNumber);
            Assert.AreEqual(mealPlanAssignmentEntity.OverrideRate, mealPlanAssignmentDto.OverrideRate);
            Assert.AreEqual(mealPlanAssignmentEntity.OverrideRateReason, mealPlanAssignmentDto.OverrideRateReason);
            Assert.AreEqual(mealPlanAssignmentEntity.OverrideChargeCode, mealPlanAssignmentDto.OverrideChargeCode);
            Assert.AreEqual(mealPlanAssignmentEntity.OverrideReceivableType, mealPlanAssignmentDto.OverrideReceivableType);
            Assert.AreEqual(mealPlanAssignmentEntity.OverrideRefundFormula, mealPlanAssignmentDto.OverrideRefundFormula);
            Assert.AreEqual(mealPlanAssignmentEntity.Comments, mealPlanAssignmentDto.Comments);
            Assert.AreEqual(mealPlanAssignmentEntity.ExternalIds.Count, mealPlanAssignmentDto.ExternalIds.Count);
            for (int i = 0; i < mealPlanAssignmentEntity.ExternalIds.Count; i++)
            {
                Assert.AreEqual(mealPlanAssignmentEntity.ExternalIds[i].ExternalId, mealPlanAssignmentDto.ExternalIds[i].ExternalId);
                Assert.AreEqual(mealPlanAssignmentEntity.ExternalIds[i].ExternalIdSource, mealPlanAssignmentDto.ExternalIds[i].ExternalIdSource);
            }
        }

    }
}
