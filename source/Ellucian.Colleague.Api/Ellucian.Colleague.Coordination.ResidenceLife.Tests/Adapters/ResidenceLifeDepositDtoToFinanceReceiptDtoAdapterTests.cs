﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Coordination.ResidenceLife.Adapters;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Tests.Adapters
{
    [TestClass]
    public class ResidenceLifeDepositDtoToFinanceReceiptDtoAdapterTests
    {
        private string
            _payerId = "PAYER1",
            _payerName = "PAYER Name",
            _personId = "HOLDER1",
            _payMethod = "PAYMETHOD",
            _externalIdentifier = "EXTERNAL1",
            _depositType = "DTYPE",
            _term = "TERM"
            ;
        private decimal
            _amount = 10000;
        private DateTime
            _date = DateTime.Now.Date;

        private Dtos.ResidenceLife.Deposit _rlDeposit;

        [TestInitialize]
        public void Initialize()
        {
            _rlDeposit = new Dtos.ResidenceLife.Deposit()
            {
                PayerId = _payerId,
                PayerName = _payerName,
                PersonId = _personId,
                PaymentMethod = _payMethod,
                DepositType = _depositType,
                TermId = _term,
                ExternalIdentifier = _externalIdentifier,
                Amount = _amount,
                Date = _date,
            };
        }

        [TestMethod]
        public void ResidenceLifeDepositDtoToFinanceReceiptDtoAdapter_ValidInput(){
            // Instantiate the adapter using a mock logger and adapter registry
            var loggerMock = new Mock<ILogger>();
            var adapterRegistryMock = new Mock<IAdapterRegistry>();
            var adapter = new ResidenceLifeDepositDtoToFinanceReceiptDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);
            var receipt = adapter.MapToType(_rlDeposit);

            Assert.AreEqual(_payerId, receipt.PayerId);
            Assert.AreEqual(_payerName, receipt.PayerName);
            Assert.AreEqual(_externalIdentifier, receipt.ExternalIdentifier);
            Assert.AreEqual(_date, receipt.Date);

            Assert.IsNotNull(receipt.NonCashPayments);
            Assert.AreEqual(1, receipt.NonCashPayments.Count);
            Assert.AreEqual(_payMethod, receipt.NonCashPayments.First().PaymentMethodCode);
            Assert.AreEqual(_amount, receipt.NonCashPayments.First().Amount);
        }
    }
}
