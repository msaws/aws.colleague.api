﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Dtos.ResidenceLife;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;
using Moq;
using Ellucian.Web.Adapters;
using slf4net;
using Ellucian.Colleague.Coordination.ResidenceLife.Adapters;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Tests.Adapters
{
    // Test the mapping of a Housing Assignment DTO to a Housing Assignment domain entity
    [TestClass]
    public class HousingAssignmentDtoAdapterTests
    {
        string id = "2";
        string building = "EAR";
        string comments = "A really long comment.";
        string contract = "12345";
        DateTime endDate = new DateTime(2014, 12, 31);
        string[] externalId = new string[]  {"EXT1", "EXT2"};
        string[] externalIdSource = { "RMS", "HD" };
        string overrideChargeCode = "AB";
        string overrideReceivableType = "AT";
        decimal? overrideRate = 23.44M;
        string overrideRefundFormula = "REFF";
        string personId = "3493939";
        string overrideRateReason = "REA1";
        Ellucian.Colleague.Dtos.ResidenceLife.RoomRatePeriods roomRatePeriod = Ellucian.Colleague.Dtos.ResidenceLife.RoomRatePeriods.Yearly;
        string residentStaffIndic = "S";
        string room = "101";
        string roomRateTable = "EARA1";
        DateTime startDate = new DateTime(2014, 7, 1);
        string currentStatus =  "A";
        DateTime currentStatusDate =  new DateTime(2014, 6, 1);
        string term = "2014/FA";

        string[] addnlArCode = {"AA1", "AA2"};
        string[] addnlDesc = {"ARDESC1", "ARDESC2"};
        decimal[] addnlAmt = {11M, -22M};

        Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDto;

        [TestInitialize]
        public void Initialize()
        {
            // Populate the housing assignment dto to map
            housingAssignmentDto = new Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment();
            housingAssignmentDto.Building = building;
            housingAssignmentDto.Comments = comments;
            housingAssignmentDto.Contract = contract;
            housingAssignmentDto.CurrentStatus = currentStatus;
            housingAssignmentDto.CurrentStatusDate = currentStatusDate;
            housingAssignmentDto.EndDate = endDate;
            Ellucian.Colleague.Dtos.ResidenceLife.ResidenceLifeExternalId eid = new Ellucian.Colleague.Dtos.ResidenceLife.ResidenceLifeExternalId();
            eid.ExternalId = externalId[0];
            eid.ExternalIdSource = externalIdSource[0];
            housingAssignmentDto.ExternalIds = new System.Collections.Generic.List<Dtos.ResidenceLife.ResidenceLifeExternalId>();
            housingAssignmentDto.ExternalIds.Add(eid);
            eid = new Ellucian.Colleague.Dtos.ResidenceLife.ResidenceLifeExternalId();
            eid.ExternalId = externalId[1];
            eid.ExternalIdSource = externalIdSource[1];
            housingAssignmentDto.ExternalIds.Add(eid);
            housingAssignmentDto.Id = id;
            housingAssignmentDto.OverrideChargeCode = overrideChargeCode;
            housingAssignmentDto.OverrideRate = overrideRate;
            housingAssignmentDto.OverrideRateReason = overrideRateReason;
            housingAssignmentDto.OverrideReceivableType = overrideReceivableType;
            housingAssignmentDto.OverrideRefundFormula = overrideRefundFormula;
            housingAssignmentDto.PersonId = personId;
            housingAssignmentDto.ResidentStaffIndicator = residentStaffIndic;
            housingAssignmentDto.Room = room;
            housingAssignmentDto.RoomRatePeriod = roomRatePeriod;
            housingAssignmentDto.RoomRateTable = roomRateTable;
            housingAssignmentDto.StartDate = startDate;
            housingAssignmentDto.TermId = term;

            housingAssignmentDto.AdditionalChargesOrCredits = new System.Collections.Generic.List<Dtos.ResidenceLife.HousingAdditionalChargeOrCredit>();
            Ellucian.Colleague.Dtos.ResidenceLife.HousingAdditionalChargeOrCredit acc = new Ellucian.Colleague.Dtos.ResidenceLife.HousingAdditionalChargeOrCredit();
            acc.Amount = addnlAmt[0];
            acc.ArCode = addnlArCode[0];
            acc.Description = addnlDesc[0];
            housingAssignmentDto.AdditionalChargesOrCredits.Add(acc);

            acc = new Ellucian.Colleague.Dtos.ResidenceLife.HousingAdditionalChargeOrCredit();
            acc.Amount = addnlAmt[1];
            acc.ArCode = addnlArCode[1];
            acc.Description = addnlDesc[1];
            housingAssignmentDto.AdditionalChargesOrCredits.Add(acc);

        }

        [TestMethod]
        public void TestAllAdapterMappings()
        {
            // Instantiate the adapter using a mock logger and adapter registry
            var loggerMock = new Mock<ILogger>();
            var adapterRegistryMock = new Mock<IAdapterRegistry>();
            var housingAssignmentDtoAdapter = new HousingAssignmentDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);

            // Convert housing assignment DTO to domain entity
            Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment housingAssignmentEntity =
                housingAssignmentDtoAdapter.MapToType(housingAssignmentDto);

            Assert.AreEqual(housingAssignmentDto.Building, housingAssignmentEntity.Building);
            Assert.AreEqual(housingAssignmentDto.Comments, housingAssignmentEntity.Comments);
            Assert.AreEqual(housingAssignmentDto.Contract, housingAssignmentEntity.Contract);
            Assert.AreEqual(housingAssignmentDto.CurrentStatus, housingAssignmentEntity.CurrentStatus);
            Assert.AreEqual(housingAssignmentDto.CurrentStatusDate, housingAssignmentEntity.CurrentStatusDate);
            Assert.AreEqual(housingAssignmentDto.EndDate, housingAssignmentEntity.EndDate);

            Assert.AreEqual(housingAssignmentDto.ExternalIds.Count, housingAssignmentEntity.ExternalIds.Count);
            Assert.AreEqual(housingAssignmentDto.ExternalIds[0].ExternalId, housingAssignmentEntity.ExternalIds[0].ExternalId);
            Assert.AreEqual(housingAssignmentDto.ExternalIds[0].ExternalIdSource, housingAssignmentEntity.ExternalIds[0].ExternalIdSource);
            Assert.AreEqual(housingAssignmentDto.ExternalIds[1].ExternalId, housingAssignmentEntity.ExternalIds[1].ExternalId);
            Assert.AreEqual(housingAssignmentDto.ExternalIds[1].ExternalIdSource, housingAssignmentEntity.ExternalIds[1].ExternalIdSource);

            Assert.AreEqual(housingAssignmentDto.Id, housingAssignmentEntity.Id);
            Assert.AreEqual(housingAssignmentDto.OverrideChargeCode, housingAssignmentEntity.OverrideChargeCode);
            Assert.AreEqual(housingAssignmentDto.OverrideRate, housingAssignmentEntity.OverrideRate);
            Assert.AreEqual(housingAssignmentDto.OverrideRateReason, housingAssignmentEntity.OverrideRateReason);
            Assert.AreEqual(housingAssignmentDto.OverrideReceivableType, housingAssignmentEntity.OverrideReceivableType);
            Assert.AreEqual(housingAssignmentDto.OverrideRefundFormula, housingAssignmentEntity.OverrideRefundFormula);
            Assert.AreEqual(housingAssignmentDto.PersonId, housingAssignmentEntity.PersonId);
            Assert.AreEqual(housingAssignmentDto.ResidentStaffIndicator, housingAssignmentEntity.ResidentStaffIndicator);
            Assert.AreEqual(housingAssignmentDto.Room, housingAssignmentEntity.Room);
            Assert.AreEqual(housingAssignmentDto.RoomRatePeriod.ToString(), housingAssignmentEntity.RoomRatePeriod.ToString());
            Assert.AreEqual(housingAssignmentDto.RoomRateTable, housingAssignmentEntity.RoomRateTable);
            Assert.AreEqual(housingAssignmentDto.StartDate, housingAssignmentEntity.StartDate);
            Assert.AreEqual(housingAssignmentDto.TermId, housingAssignmentEntity.TermId);

            Assert.AreEqual(housingAssignmentDto.AdditionalChargesOrCredits.Count, housingAssignmentEntity.AdditionalChargesOrCredits.Count);
            Assert.AreEqual(housingAssignmentDto.AdditionalChargesOrCredits[0].Amount, housingAssignmentEntity.AdditionalChargesOrCredits[0].Amount);
            Assert.AreEqual(housingAssignmentDto.AdditionalChargesOrCredits[0].ArCode, housingAssignmentEntity.AdditionalChargesOrCredits[0].ArCode);
            Assert.AreEqual(housingAssignmentDto.AdditionalChargesOrCredits[0].Description, housingAssignmentEntity.AdditionalChargesOrCredits[0].Description);
            Assert.AreEqual(housingAssignmentDto.AdditionalChargesOrCredits[1].Amount, housingAssignmentEntity.AdditionalChargesOrCredits[1].Amount);
            Assert.AreEqual(housingAssignmentDto.AdditionalChargesOrCredits[1].ArCode, housingAssignmentEntity.AdditionalChargesOrCredits[1].ArCode);
            Assert.AreEqual(housingAssignmentDto.AdditionalChargesOrCredits[1].Description, housingAssignmentEntity.AdditionalChargesOrCredits[1].Description);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestDefaultStartDate()
        {
            // Instantiate the adapter using a mock logger and adapter registry
            var loggerMock = new Mock<ILogger>();
            var adapterRegistryMock = new Mock<IAdapterRegistry>();
            var housingAssignmentDtoAdapter = new HousingAssignmentDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);

            // Convert housing assignment DTO to domain entity
            DateTime dtNotSet = new DateTime();
            housingAssignmentDto.StartDate = dtNotSet;
            Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment housingAssignmentEntity =
                housingAssignmentDtoAdapter.MapToType(housingAssignmentDto);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestDefaultEndDate()
        {
            // Instantiate the adapter using a mock logger and adapter registry
            var loggerMock = new Mock<ILogger>();
            var adapterRegistryMock = new Mock<IAdapterRegistry>();
            var housingAssignmentDtoAdapter = new HousingAssignmentDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);

            // Convert housing assignment DTO to domain entity
            DateTime dtNotSet = new DateTime();
            housingAssignmentDto.EndDate = dtNotSet;
            Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment housingAssignmentEntity =
                housingAssignmentDtoAdapter.MapToType(housingAssignmentDto);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void TestDefaultCurrentStatusDate()
        {
            // Instantiate the adapter using a mock logger and adapter registry
            var loggerMock = new Mock<ILogger>();
            var adapterRegistryMock = new Mock<IAdapterRegistry>();
            var housingAssignmentDtoAdapter = new HousingAssignmentDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);

            // Convert housing assignment DTO to domain entity
            DateTime dtNotSet = new DateTime();
            housingAssignmentDto.CurrentStatusDate = dtNotSet;
            Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment housingAssignmentEntity =
                housingAssignmentDtoAdapter.MapToType(housingAssignmentDto);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestDefaultAddnlAmt()
        {
            // Instantiate the adapter using a mock logger and adapter registry
            var loggerMock = new Mock<ILogger>();
            var adapterRegistryMock = new Mock<IAdapterRegistry>();
            var housingAssignmentDtoAdapter = new HousingAssignmentDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);

            // Convert housing assignment DTO to domain entity
            Decimal decNotSet = new Decimal();
            housingAssignmentDto.AdditionalChargesOrCredits[1].Amount = decNotSet;
            Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment housingAssignmentEntity =
                housingAssignmentDtoAdapter.MapToType(housingAssignmentDto);
        }


    }
}
