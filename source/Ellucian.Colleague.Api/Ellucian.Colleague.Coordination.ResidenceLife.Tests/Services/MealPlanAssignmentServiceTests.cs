﻿using Ellucian.Colleague.Coordination.ResidenceLife.Adapters;
using Ellucian.Colleague.Coordination.ResidenceLife.Services;
using Ellucian.Colleague.Domain.Entities;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.ResidenceLife;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;
using Ellucian.Colleague.Domain.ResidenceLife.Repositories;
using Ellucian.Colleague.Dtos.ResidenceLife;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Tests.Services
{
    [TestClass]
    public class MealPlanAssignmentServiceTests
    {
        Mock<IMealPlanRepository> mealPlanRepoMock;
        IMealPlanRepository mealPlanRepo;
        ILogger logger;
        ICurrentUserFactory currentUserFactory;
        Mock<IRoleRepository> roleRepoMock;
        IRoleRepository roleRepo;
        Mock<IAdapterRegistry> adapterRegistryMock;
        IAdapterRegistry adapterRegistry;
        MealPlanAssignmentService mealPlanAssignmentService;
        Ellucian.Colleague.Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignment;
        Role resLifeApiRole;

        // Keys used between setup and tests
        string mealPlanId = "mealplanid";
        string personId = "personid";
        string mealPlanAssignmentId = "mealplanassignid";
        string externalId = "externalId";

        [TestInitialize]
        public void TestInitialize()
        {
            // Set up current user
            currentUserFactory = new MealPlanAssignmentServiceTestUserFactory();

            // Mock the meal plan repository
            mealPlanRepoMock = new Mock<IMealPlanRepository>();
            mealPlanRepo = mealPlanRepoMock.Object;

            // A MealPlan entity, used by multiple mocks below
            Domain.ResidenceLife.Entities.MealPlan mealPlan = new MealPlan(new DateTime(2014, 1, 1), "desc");
            mealPlan.Id = mealPlanId;

            // A MealPlanAssignment entity, used by multiple mocks below
            mealPlanAssignment = new Domain.ResidenceLife.Entities.MealPlanAssignment(personId,
                mealPlan, 5, new DateTime(2014, 1, 1), new DateTime(2014, 12, 31),
                new Domain.ResidenceLife.Entities.MealPlanAssignmentStatus("A", "active", MealPlanAssignmentStatusType.None), new DateTime(2014, 5, 1), "2014/SP",
                null, null);
            mealPlanAssignment.Id = mealPlanAssignmentId;

            // Mock the Get of a MealPlan, which is executed in the service while building a MealPlanAssignment entity
            mealPlanRepoMock.Setup(repo => repo.Get(mealPlanId)).Returns(mealPlan);

            // Mock the return of the list of MealPlanAssignmentStatuses, which is obtained in the service while building a MealPlanAssignment entity
            List<MealPlanAssignmentStatus> mealPlanAssignmentStatuses = new List<MealPlanAssignmentStatus>() { 
                new MealPlanAssignmentStatus("A", "active", MealPlanAssignmentStatusType.None),
                new MealPlanAssignmentStatus("C", "canceled", MealPlanAssignmentStatusType.Canceled) };
            mealPlanRepoMock.Setup(repo => repo.MealPlanAssignmentStatuses).Returns(mealPlanAssignmentStatuses);

            // Mock the GetMealPlanAssignment repo method to return our simple meal plan assignment entity for our ID
            mealPlanRepoMock.Setup(repo => repo.GetMealPlanAssignment(mealPlanAssignmentId)).Returns(mealPlanAssignment);

            // Mock the AddMealPlanAssignment repo method to return our simple housing assignment entity for our person
            mealPlanRepoMock.Setup(repo =>
                repo.AddMealPlanAssignment(It.Is<Domain.ResidenceLife.Entities.MealPlanAssignment>(ent => ent.PersonId == personId))).Returns(mealPlanAssignment);

            // Mock the UpdateMealPlanAssignment repo method to return our simple housing assignment entity for our ID or External ID
            mealPlanRepoMock.Setup(repo =>
                repo.UpdateMealPlanAssignment(It.Is<Domain.ResidenceLife.Entities.MealPlanAssignment>(ent =>
                    (ent.Id == mealPlanId) || (ent.ExternalIds != null && ent.ExternalIds.Count > 0 && ent.ExternalIds[0].ExternalId == externalId)))).Returns(mealPlanAssignment);

            // Mock the adapter registry
            var emptyAdapterRegistryMock = new Mock<IAdapterRegistry>();
            adapterRegistryMock = new Mock<IAdapterRegistry>();
            adapterRegistry = adapterRegistryMock.Object;

            // Return the meal plan assignment entity to dto adapter
            var mealPlanAssignmentEntityAdapter = new AutoMapperAdapter<Domain.ResidenceLife.Entities.MealPlanAssignment,
                Dtos.ResidenceLife.MealPlanAssignment>(emptyAdapterRegistryMock.Object, logger);
            adapterRegistryMock.Setup(x => x.GetAdapter<Domain.ResidenceLife.Entities.MealPlanAssignment,
                Dtos.ResidenceLife.MealPlanAssignment>()).Returns(mealPlanAssignmentEntityAdapter);

            // Mock the role repository to return a role with both permissions needed for all housing assignment services.
            resLifeApiRole = new Role(1, "RES.LIFE.API");
            resLifeApiRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(
                ResidenceLifePermissionCodes.ViewMealPlanAssignments));
            resLifeApiRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(
                ResidenceLifePermissionCodes.CreateUpdateMealPlanAssignments));
            roleRepoMock = new Mock<IRoleRepository>();
            roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Role>() { resLifeApiRole });
            roleRepo = roleRepoMock.Object;

            logger = new Mock<ILogger>().Object;

            mealPlanAssignmentService = new MealPlanAssignmentService(adapterRegistry, currentUserFactory, roleRepo, logger, mealPlanRepo);
        }

        [TestMethod]
        public void TestSuccessfulGet()
        {
            // Get with our meal plan assignment id
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentDto =
               mealPlanAssignmentService.GetMealPlanAssignment(mealPlanAssignmentId);

            // Look for the person ID in the basic MealPlanAssignment returned by the mock repo
            Assert.AreEqual(personId, mealPlanAssignmentDto.PersonId);
        }

        [TestMethod]
        [ExpectedException(typeof(PermissionsException))]
        public void TestGetWithoutPermissions()
        {
            // Instantiate an entire new service, passing in different role repository that returns
            // different permissions.
            Role myResLifeApiRole;
            myResLifeApiRole = new Role(1, "RES.LIFE.API");
            myResLifeApiRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(
                ResidenceLifePermissionCodes.CreateUpdateMealPlanAssignments));
            Mock<IRoleRepository> myRoleRepoMock;
            myRoleRepoMock = new Mock<IRoleRepository>();
            myRoleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { myResLifeApiRole });
            IRoleRepository myRoleRepo;
            myRoleRepo = myRoleRepoMock.Object;
            MealPlanAssignmentService myMealPlanAssignmentService = new MealPlanAssignmentService(
                adapterRegistry, currentUserFactory, myRoleRepo, logger, mealPlanRepo);


            // Invoke GetMealPlanAssignment, which will fail its permissions check
            Dtos.ResidenceLife.MealPlanAssignment myMealPlanAssignmentDto =
                myMealPlanAssignmentService.GetMealPlanAssignment("doesn't matter");
        }

        [TestMethod]
        public void TestSuccessfulCreate()
        {
            // Populate a MealPlanAssignment dto with just enough data that a MealPlanAssignment domain entity can be created
            // from it. 
            // Set the MealPlanID and CurrentStatusCode to values that have been mocked in the repository. They will be fetched when
            // the entity is constructed.
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentDtoInput = new Dtos.ResidenceLife.MealPlanAssignment();
            mealPlanAssignmentDtoInput.PersonId = personId;
            mealPlanAssignmentDtoInput.MealPlanId = mealPlanId;
            mealPlanAssignmentDtoInput.NumberOfRatePeriods = 1;
            mealPlanAssignmentDtoInput.StartDate = new DateTime(2014, 1, 1);
            mealPlanAssignmentDtoInput.EndDate = new DateTime(2014, 5, 1);
            mealPlanAssignmentDtoInput.CurrentStatusCode = "A";
            mealPlanAssignmentDtoInput.CurrentStatusDate = new DateTime(2014, 5, 1);
            mealPlanAssignmentDtoInput.TermId = "2014/FA";

            // The CreateMealPlanAssignment service will call a mocked AddMealPlanAssignment repository method based on the person ID
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentDtoReturn;
            mealPlanAssignmentDtoReturn = mealPlanAssignmentService.CreateMealPlanAssignment(mealPlanAssignmentDtoInput);

            // Verify the ID of the returned MealPlanAssignment
            Assert.AreEqual(mealPlanAssignmentId, mealPlanAssignmentDtoReturn.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(PermissionsException))]
        public void TestCreateWithoutPermissions()
        {
            // Instantiate an entire new service, passing in different role repository that returns
            // different permissions.
            Role myResLifeApiRole;
            myResLifeApiRole = new Role(1, "RES.LIFE.API");
            myResLifeApiRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(
                ResidenceLifePermissionCodes.ViewMealPlanAssignments));
            Mock<IRoleRepository> myRoleRepoMock;
            myRoleRepoMock = new Mock<IRoleRepository>();
            myRoleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { myResLifeApiRole });
            IRoleRepository myRoleRepo;
            myRoleRepo = myRoleRepoMock.Object;
            MealPlanAssignmentService myMealPlanAssignmentService = new MealPlanAssignmentService(
                adapterRegistry, currentUserFactory, myRoleRepo, logger, mealPlanRepo);


            // Invoke CreateMealPlanAssignment, which will fail its permissions check
            Dtos.ResidenceLife.MealPlanAssignment myMealPlanAssignmentDto =
                myMealPlanAssignmentService.CreateMealPlanAssignment(new Dtos.ResidenceLife.MealPlanAssignment());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestCreateWithId()
        {
            // Populate a MealPlanAssignment dto with just enough data that a MealPlanAssignment domain entity can be created
            // from it. 
            // Set the MealPlanID and CurrentStatusCode to values that have been mocked in the repository. They will be fetched when
            // the entity is constructed.
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentDtoInput = new Dtos.ResidenceLife.MealPlanAssignment();
            mealPlanAssignmentDtoInput.PersonId = personId;
            mealPlanAssignmentDtoInput.MealPlanId = mealPlanId;
            mealPlanAssignmentDtoInput.NumberOfRatePeriods = 1;
            mealPlanAssignmentDtoInput.StartDate = new DateTime(2014, 1, 1);
            mealPlanAssignmentDtoInput.EndDate = new DateTime(2014, 5, 1);
            mealPlanAssignmentDtoInput.CurrentStatusCode = "A";
            mealPlanAssignmentDtoInput.CurrentStatusDate = new DateTime(2014, 5, 1);
            mealPlanAssignmentDtoInput.TermId = "2014/FA";

            // Assign an ID
            mealPlanAssignmentDtoInput.Id = "222";

            // Call the Create service
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentDtoReturn;
            mealPlanAssignmentDtoReturn = mealPlanAssignmentService.CreateMealPlanAssignment(mealPlanAssignmentDtoInput);
        }

        [TestMethod]
        public void TestSuccessfulUpdate()
        {
            // Populate a MealPlanAssignment dto with just enough data that a MealPlanAssignment domain entity can be created
            // from it. 
            // Set the MealPlanID and CurrentStatusCode to values that have been mocked in the repository. They will be fetched when
            // the entity is constructed.
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentDtoInput = new Dtos.ResidenceLife.MealPlanAssignment();
            mealPlanAssignmentDtoInput.PersonId = personId;
            mealPlanAssignmentDtoInput.MealPlanId = mealPlanId;
            mealPlanAssignmentDtoInput.NumberOfRatePeriods = 1;
            mealPlanAssignmentDtoInput.StartDate = new DateTime(2014, 1, 1);
            mealPlanAssignmentDtoInput.EndDate = new DateTime(2014, 5, 1);
            mealPlanAssignmentDtoInput.CurrentStatusCode = "A";
            mealPlanAssignmentDtoInput.CurrentStatusDate = new DateTime(2014, 5, 1);
            mealPlanAssignmentDtoInput.TermId = "2014/FA";

            // The UpdateMealPlanAssignment service will call a mocked UpdateMealPlanAssignment repository method based on the external ID
            mealPlanAssignmentDtoInput.ExternalIds = new List<Dtos.ResidenceLife.ResidenceLifeExternalId>() { new Dtos.ResidenceLife.ResidenceLifeExternalId { ExternalId = externalId, ExternalIdSource = "HD" } };
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentDtoReturn;
            mealPlanAssignmentDtoReturn = mealPlanAssignmentService.UpdateMealPlanAssignment(mealPlanAssignmentDtoInput);

            // Verify the ID of the returned MealPlanAssignment
            Assert.AreEqual(personId, mealPlanAssignmentDtoReturn.PersonId);
        }

        [TestMethod]
        [ExpectedException(typeof(PermissionsException))]
        public void TestUpdateWithoutPermissions()
        {
            // Instantiate an entire new service, passing in different role repository that returns
            // different permissions.
            Role myResLifeApiRole;
            myResLifeApiRole = new Role(1, "RES.LIFE.API");
            myResLifeApiRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(
                ResidenceLifePermissionCodes.ViewMealPlanAssignments));
            Mock<IRoleRepository> myRoleRepoMock;
            myRoleRepoMock = new Mock<IRoleRepository>();
            myRoleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { myResLifeApiRole });
            IRoleRepository myRoleRepo;
            myRoleRepo = myRoleRepoMock.Object;
            MealPlanAssignmentService myMealPlanAssignmentService = new MealPlanAssignmentService(
                adapterRegistry, currentUserFactory, myRoleRepo, logger, mealPlanRepo);


            // Invoke UpdateMealPlanAssignment, which will fail its permissions check
            Dtos.ResidenceLife.MealPlanAssignment myMealPlanAssignmentDto =
                myMealPlanAssignmentService.UpdateMealPlanAssignment(new Dtos.ResidenceLife.MealPlanAssignment());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestUpdateWithoutId()
        {
            // Populate a MealPlanAssignment dto with just enough data that a MealPlanAssignment domain entity can be created
            // from it. 
            // Set the MealPlanID and CurrentStatusCode to values that have been mocked in the repository. They will be fetched when
            // the entity is constructed.
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentDtoInput = new Dtos.ResidenceLife.MealPlanAssignment();
            mealPlanAssignmentDtoInput.PersonId = personId;
            mealPlanAssignmentDtoInput.MealPlanId = mealPlanId;
            mealPlanAssignmentDtoInput.NumberOfRatePeriods = 1;
            mealPlanAssignmentDtoInput.StartDate = new DateTime(2014, 1, 1);
            mealPlanAssignmentDtoInput.EndDate = new DateTime(2014, 5, 1);
            mealPlanAssignmentDtoInput.CurrentStatusCode = "A";
            mealPlanAssignmentDtoInput.CurrentStatusDate = new DateTime(2014, 5, 1);
            mealPlanAssignmentDtoInput.TermId = "2014/FA";

            // Instantiate the list of external IDs, but with a count of zero.
            mealPlanAssignmentDtoInput.ExternalIds = new List<Dtos.ResidenceLife.ResidenceLifeExternalId>();

            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentDtoReturn;
            mealPlanAssignmentDtoReturn = mealPlanAssignmentService.UpdateMealPlanAssignment(mealPlanAssignmentDtoInput);
        }

        [TestMethod]
        public void TestCreateMealPlanAssignmentEntityFromDto()
        {
            // Fully populate a DTO, then verify that all attributes were copied to the domain entity correctly.
            Dtos.ResidenceLife.MealPlanAssignment mealPlanAssignmentDtoInput = new Dtos.ResidenceLife.MealPlanAssignment();
            mealPlanAssignmentDtoInput.PersonId = personId;
            // CreateMealPlanAssignmentEntityFromDto will read the MealPlan entity from a mock repository
            mealPlanAssignmentDtoInput.MealPlanId = mealPlanId;
            mealPlanAssignmentDtoInput.NumberOfRatePeriods = 10;
            mealPlanAssignmentDtoInput.StartDate = new DateTime(2014, 1, 1);
            mealPlanAssignmentDtoInput.EndDate = new DateTime(2014, 5, 1);
            // CreateMealPlanAssignmentEntityFromDto will read the MealPlanAssignmentStatus from a mock repository
            mealPlanAssignmentDtoInput.CurrentStatusCode = "A";
            mealPlanAssignmentDtoInput.CurrentStatusDate = new DateTime(2014, 5, 1);
            mealPlanAssignmentDtoInput.TermId = "2014/FA";
            mealPlanAssignmentDtoInput.CardNumber = "cardno";
            mealPlanAssignmentDtoInput.Comments = "these are comments";
            mealPlanAssignmentDtoInput.ExternalIds = new List<Dtos.ResidenceLife.ResidenceLifeExternalId>() 
                { new Dtos.ResidenceLife.ResidenceLifeExternalId { ExternalId = externalId, ExternalIdSource = "HD" },
                  new Dtos.ResidenceLife.ResidenceLifeExternalId { ExternalId = "340404", ExternalIdSource = "RMS" }};
            mealPlanAssignmentDtoInput.Id = "id";
            mealPlanAssignmentDtoInput.OverrideChargeCode = "OAR";
            mealPlanAssignmentDtoInput.OverrideRate = 34.34M;
            mealPlanAssignmentDtoInput.OverrideRateReason = "REA";
            mealPlanAssignmentDtoInput.OverrideReceivableType = "RECV";
            mealPlanAssignmentDtoInput.OverrideRefundFormula = "REFFORM";
            mealPlanAssignmentDtoInput.UsedPercent = 34;
            mealPlanAssignmentDtoInput.UsedRatePeriods = 9;

            // Invoke CreateMealPlanAssignmentEntityFromDto
            PrivateObject privateMealPlanService = new PrivateObject(mealPlanAssignmentService);
            Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignment =
                (Domain.ResidenceLife.Entities.MealPlanAssignment)
                privateMealPlanService.Invoke("CreateMealPlanAssignmentEntityFromDto", mealPlanAssignmentDtoInput);

            Assert.AreEqual(mealPlanAssignmentDtoInput.PersonId, mealPlanAssignment.PersonId);
            Assert.AreEqual(mealPlanAssignmentDtoInput.MealPlanId, mealPlanAssignment.MealPlanId);
            Assert.AreEqual(mealPlanAssignmentDtoInput.NumberOfRatePeriods, mealPlanAssignment.NumberOfRatePeriods);
            Assert.AreEqual(mealPlanAssignmentDtoInput.StartDate, mealPlanAssignment.StartDate);
            Assert.AreEqual(mealPlanAssignmentDtoInput.EndDate, mealPlanAssignment.EndDate);
            Assert.AreEqual(mealPlanAssignmentDtoInput.CurrentStatusCode, mealPlanAssignment.CurrentStatusCode);
            Assert.AreEqual(mealPlanAssignmentDtoInput.CurrentStatusDate, mealPlanAssignment.CurrentStatusDate);
            Assert.AreEqual(mealPlanAssignmentDtoInput.TermId, mealPlanAssignment.TermId);
            Assert.AreEqual(mealPlanAssignmentDtoInput.CardNumber, mealPlanAssignment.CardNumber);
            Assert.AreEqual(mealPlanAssignmentDtoInput.Comments, mealPlanAssignment.Comments);
            Assert.AreEqual(mealPlanAssignmentDtoInput.ExternalIds.Count, mealPlanAssignment.ExternalIds.Count);
            for (int i = 0; i < mealPlanAssignmentDtoInput.ExternalIds.Count; i++)
            {
                Assert.AreEqual(mealPlanAssignmentDtoInput.ExternalIds[i].ExternalId, mealPlanAssignment.ExternalIds[i].ExternalId);
                Assert.AreEqual(mealPlanAssignmentDtoInput.ExternalIds[i].ExternalIdSource, mealPlanAssignment.ExternalIds[i].ExternalIdSource);
            }
            Assert.AreEqual(mealPlanAssignmentDtoInput.Id,mealPlanAssignment.Id);
            Assert.AreEqual(mealPlanAssignmentDtoInput.OverrideChargeCode, mealPlanAssignment.OverrideChargeCode);
            Assert.AreEqual(mealPlanAssignmentDtoInput.OverrideRate, mealPlanAssignment.OverrideRate);
            Assert.AreEqual(mealPlanAssignmentDtoInput.OverrideRateReason, mealPlanAssignment.OverrideRateReason);
            Assert.AreEqual(mealPlanAssignmentDtoInput.OverrideReceivableType, mealPlanAssignment.OverrideReceivableType);
            Assert.AreEqual(mealPlanAssignmentDtoInput.OverrideRefundFormula, mealPlanAssignment.OverrideRefundFormula);
            Assert.AreEqual(mealPlanAssignmentDtoInput.UsedPercent, mealPlanAssignment.UsedPercent);
            Assert.AreEqual(mealPlanAssignmentDtoInput.UsedRatePeriods, mealPlanAssignment.UsedRatePeriods);
        }

    }

    // Setup a fake ICurrentUserFactory to pass into the service constructor.
    public class MealPlanAssignmentServiceTestUserFactory : ICurrentUserFactory
    {
        public ICurrentUser CurrentUser
        {
            get
            {
                return new CurrentUser(new Claims()
                {
                    ControlId = "123",
                    Name = "George",
                    PersonId = "0000011",
                    SecurityToken = "321",
                    SessionTimeout = 30,
                    UserName = "ResLifeApi",
                    Roles = new List<string>() {"RES.LIFE.API"},
                    SessionFixationId = "abc123"
                });
            }
        }
    }
}
