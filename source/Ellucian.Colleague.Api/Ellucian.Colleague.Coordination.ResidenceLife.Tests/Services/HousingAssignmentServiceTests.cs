﻿using Ellucian.Colleague.Coordination.ResidenceLife.Adapters;
using Ellucian.Colleague.Coordination.ResidenceLife.Services;
using Ellucian.Colleague.Domain.Entities;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.ResidenceLife;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;
using Ellucian.Colleague.Domain.ResidenceLife.Repositories;
using Ellucian.Colleague.Dtos.ResidenceLife;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;


namespace Ellucian.Colleague.Coordination.ResidenceLife.Tests.Services
{
    [TestClass]
    public class HousingAssignmentServiceTests
    {

        Mock<IHousingAssignmentRepository> housingAssignmentRepoMock;
        IHousingAssignmentRepository housingAssignmentRepo;
        ILogger logger;
        ICurrentUserFactory currentUserFactory;
        Mock<IRoleRepository> roleRepoMock;
        IRoleRepository roleRepo;
        Mock<IAdapterRegistry> adapterRegistryMock;
        IAdapterRegistry adapterRegistry;
        HousingAssignmentService housingAssignmentService;
        Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment housingAssignment;
        Role resLifeApiRole;

        [TestInitialize]
        public void TestInitialize()
        {
            // Set up current user
            currentUserFactory = new HousingAssignmentServiceTestUserFactory();

            // Mock the housing assignment repository
            housingAssignmentRepoMock = new Mock<IHousingAssignmentRepository>();
            housingAssignmentRepo = housingAssignmentRepoMock.Object;
            // Mock the Get method to return our simple housing assignment entity for id "1"
            housingAssignment = new Domain.ResidenceLife.Entities.HousingAssignment("123456", new DateTime(2014, 1, 1), new DateTime(2014, 5, 31), "A", new DateTime(2015, 1, 2));
            housingAssignmentRepoMock.Setup(repo => repo.Get("1")).Returns(housingAssignment);
            // Mock the Add method to return our simple housing assignment entity for person "1"
            housingAssignmentRepoMock.Setup(repo => 
                repo.Add(It.Is<Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment>(ent => ent.PersonId == "1"))).Returns(housingAssignment);
            // Mock the Update method to return our simple housing assignment entity for id "1", or ExternalId "50"
            housingAssignmentRepoMock.Setup(repo =>
                repo.Update(It.Is<Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment>(ent => 
                    (ent.Id == "1") || (ent.ExternalIds != null && ent.ExternalIds.Count > 0 && ent.ExternalIds[0].ExternalId == "50")))).Returns(housingAssignment);

            // Mock the adapter registry
            adapterRegistryMock = new Mock<IAdapterRegistry>();
            adapterRegistry = adapterRegistryMock.Object;
            // Return the housing assignment dto to entity adapter
            var emptyAdapterRegistryMock = new Mock<IAdapterRegistry>();
            var housingAssignmentDtoAdapter = new HousingAssignmentDtoAdapter(emptyAdapterRegistryMock.Object, logger);
            adapterRegistryMock.Setup(x =>
                x.GetAdapter<Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment,
                            Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment>()).Returns(housingAssignmentDtoAdapter);
            // Return the housing assignment entity to dto adapter
            var housingAssignmentEntityAdapter = new AutoMapperAdapter<Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment,
                Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment>(emptyAdapterRegistryMock.Object, logger);
            adapterRegistryMock.Setup(x => x.GetAdapter<Ellucian.Colleague.Domain.ResidenceLife.Entities.HousingAssignment,
                Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment>()).Returns(housingAssignmentEntityAdapter);

            // Mock the role repository to return a role with both permissions needed for all housing assignment services.
            resLifeApiRole = new Role(1, "RES.LIFE.API");
            resLifeApiRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(
                ResidenceLifePermissionCodes.ViewHousingAssignments));
            resLifeApiRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(
                ResidenceLifePermissionCodes.CreateUpdateHousingAssignments));
            roleRepoMock = new Mock<IRoleRepository>();     
            roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Role>() {resLifeApiRole});
            roleRepo = roleRepoMock.Object;

            logger = new Mock<ILogger>().Object;

            housingAssignmentService = new HousingAssignmentService(adapterRegistry, currentUserFactory, roleRepo, logger, housingAssignmentRepo);
        }        

        [TestMethod]
        public void TestSuccessfulGet()
        {
           Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDto;
           housingAssignmentDto = housingAssignmentService.GetHousingAssignment("1");
           Assert.AreEqual("123456", housingAssignmentDto.PersonId);
        }

        [TestMethod]
        [ExpectedException(typeof(PermissionsException))]
        public void TestGetWithoutPermissions()
        {
            // Instantiate an entire new service, passing in different role repository that returns
            // different permissions.
            Role myResLifeApiRole;
            myResLifeApiRole = new Role(1, "RES.LIFE.API");
            myResLifeApiRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(
                ResidenceLifePermissionCodes.CreateUpdateHousingAssignments));
            Mock<IRoleRepository> myRoleRepoMock;
            myRoleRepoMock = new Mock<IRoleRepository>();
            myRoleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { myResLifeApiRole });
            IRoleRepository myRoleRepo;
            myRoleRepo = myRoleRepoMock.Object;
            HousingAssignmentService myHousingAssignmentService = new HousingAssignmentService(
                adapterRegistry, currentUserFactory, myRoleRepo, logger, housingAssignmentRepo);

            Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDto;
            housingAssignmentDto = myHousingAssignmentService.GetHousingAssignment("1");
        }


        [TestMethod]
        public void TestSuccessfulCreate()
        {
            Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDtoInput = new Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment();
            housingAssignmentDtoInput.PersonId = "1";
            housingAssignmentDtoInput.StartDate = new DateTime(2014, 1, 1);
            housingAssignmentDtoInput.EndDate = new DateTime(2014, 5, 1);
            housingAssignmentDtoInput.CurrentStatus = "A";
            housingAssignmentDtoInput.CurrentStatusDate = new DateTime(2014, 5, 1);

            Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDtoReturn;
            housingAssignmentDtoReturn = housingAssignmentService.CreateHousingAssignment(housingAssignmentDtoInput);
            Assert.AreEqual("123456", housingAssignmentDtoReturn.PersonId);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestCreateWithId()
        {
            Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDtoInput = new Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment();
            housingAssignmentDtoInput.PersonId = "1";
            housingAssignmentDtoInput.Id = "23";

            Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDtoReturn;
            housingAssignmentDtoReturn = housingAssignmentService.CreateHousingAssignment(housingAssignmentDtoInput);
        }

        [TestMethod]
        [ExpectedException(typeof(PermissionsException))]
        public void TestCreateWithoutPermissions()
        {
            // Instantiate an entire new service, passing in different role repository that returns
            // different permissions.
            Role myResLifeApiRole;
            myResLifeApiRole = new Role(1, "RES.LIFE.API");
            myResLifeApiRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(
                ResidenceLifePermissionCodes.ViewHousingAssignments));
            Mock<IRoleRepository> myRoleRepoMock;
            myRoleRepoMock = new Mock<IRoleRepository>();
            myRoleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { myResLifeApiRole });
            IRoleRepository myRoleRepo;
            myRoleRepo = myRoleRepoMock.Object;
            HousingAssignmentService myHousingAssignmentService = new HousingAssignmentService(
                adapterRegistry, currentUserFactory, myRoleRepo, logger, housingAssignmentRepo);

            Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDtoInput = new Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment();
            housingAssignmentDtoInput.PersonId = "1";

            Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDtoReturn;
            housingAssignmentDtoReturn = myHousingAssignmentService.CreateHousingAssignment(housingAssignmentDtoInput);
        }


        [TestMethod]
        public void TestSuccessfulUpdate()
        {
            Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDtoInput = new Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment();
            housingAssignmentDtoInput.Id = "1";
            housingAssignmentDtoInput.PersonId = "23";
            housingAssignmentDtoInput.StartDate = new DateTime(2014, 1, 1);
            housingAssignmentDtoInput.EndDate = new DateTime(2014, 5, 1);
            housingAssignmentDtoInput.CurrentStatus = "A";
            housingAssignmentDtoInput.CurrentStatusDate = new DateTime(2014, 5, 1);


            Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDtoReturn;
            housingAssignmentDtoReturn = housingAssignmentService.UpdateHousingAssignment(housingAssignmentDtoInput);
            Assert.AreEqual("123456", housingAssignmentDtoReturn.PersonId);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void TestUpdateWithNoIds()
        {
            Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDtoInput = new Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment();

            Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDtoReturn;
            housingAssignmentDtoReturn = housingAssignmentService.UpdateHousingAssignment(housingAssignmentDtoInput);
        }

        [TestMethod]
        public void TestUpdateWithExternalIdOnly()
        {
            Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDtoInput = new Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment();
            housingAssignmentDtoInput.PersonId = "23";
            housingAssignmentDtoInput.StartDate = new DateTime(2014, 1, 1);
            housingAssignmentDtoInput.EndDate = new DateTime(2014, 5, 1);
            housingAssignmentDtoInput.CurrentStatus = "A";
            housingAssignmentDtoInput.CurrentStatusDate = new DateTime(2014, 5, 1);

            Ellucian.Colleague.Dtos.ResidenceLife.ResidenceLifeExternalId extlId = new Dtos.ResidenceLife.ResidenceLifeExternalId();
            extlId.ExternalId = "50";
            extlId.ExternalIdSource = "HD";
            housingAssignmentDtoInput.ExternalIds = new List<Ellucian.Colleague.Dtos.ResidenceLife.ResidenceLifeExternalId>();
            housingAssignmentDtoInput.ExternalIds.Add(extlId);

            Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDtoReturn;
            housingAssignmentDtoReturn = housingAssignmentService.UpdateHousingAssignment(housingAssignmentDtoInput);
            Assert.AreEqual("123456", housingAssignmentDtoReturn.PersonId);
        }

        [TestMethod]
        [ExpectedException(typeof(PermissionsException))]
        public void TestUpdateWithoutPermissions()
        {
            // Instantiate an entire new service, passing in different role repository that returns
            // different permissions.
            Role myResLifeApiRole;
            myResLifeApiRole = new Role(1, "RES.LIFE.API");
            myResLifeApiRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(
                ResidenceLifePermissionCodes.ViewHousingAssignments));
            Mock<IRoleRepository> myRoleRepoMock;
            myRoleRepoMock = new Mock<IRoleRepository>();
            myRoleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { myResLifeApiRole });
            IRoleRepository myRoleRepo;
            myRoleRepo = myRoleRepoMock.Object;
            HousingAssignmentService myHousingAssignmentService = new HousingAssignmentService(
                adapterRegistry, currentUserFactory, myRoleRepo, logger, housingAssignmentRepo);

            Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDtoInput = new Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment();
            housingAssignmentDtoInput.Id = "23";

            Ellucian.Colleague.Dtos.ResidenceLife.HousingAssignment housingAssignmentDtoReturn;
            housingAssignmentDtoReturn = myHousingAssignmentService.UpdateHousingAssignment(housingAssignmentDtoInput);
        }

    }

    // Setup a fake ICurrentUserFactory to pass into the service constructor.
    public class HousingAssignmentServiceTestUserFactory : ICurrentUserFactory
    {
        public ICurrentUser CurrentUser
        {
            get
            {
                return new CurrentUser(new Claims()
                {
                    ControlId = "123",
                    Name = "George",
                    PersonId = "0000011",
                    SecurityToken = "321",
                    SessionTimeout = 30,
                    UserName = "ResLifeApi",
                    Roles = new List<string>() {"RES.LIFE.API"},
                    SessionFixationId = "abc123"
                });
            }
        }
    }

}
