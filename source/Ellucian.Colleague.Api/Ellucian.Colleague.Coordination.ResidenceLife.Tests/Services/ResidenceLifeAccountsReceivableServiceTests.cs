﻿using Ellucian.Colleague.Coordination.ResidenceLife.Adapters;
using Ellucian.Colleague.Coordination.ResidenceLife.Services;
using Ellucian.Colleague.Coordination.Finance;
using Ellucian.Colleague.Domain.Entities;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.ResidenceLife;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;
using Ellucian.Colleague.Domain.ResidenceLife.Repositories;
using Ellucian.Colleague.Dtos.ResidenceLife;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;

namespace Ellucian.Colleague.Coordination.ResidenceLife.Tests.Services
{
    [TestClass]
    public class ResidenceLifeAccountsReceivableServiceTests
    {
        Mock<IAccountsReceivableService> accountsReceivableServiceMock;
        IAccountsReceivableService accountsReceivableService;
        Mock<IResidenceLifeConfigurationRepository> residenceLifeConfigurationRepositoryMock;
        IResidenceLifeConfigurationRepository residenceLifeConfigurationRepository;
        ILogger logger;
        ICurrentUserFactory currentUserFactory;
        Mock<IRoleRepository> roleRepoMock;
        IRoleRepository roleRepo;
        Mock<IAdapterRegistry> adapterRegistryMock;
        IAdapterRegistry adapterRegistry;
        Mock<IReceiptService> _receiptServiceMock;
        ResidenceLifeAccountsReceivableService residenceLifeAccountsReceivableService;
        Role resLifeApiRole;

        // Keys and values used between setup and tests

        // ExternalId of successful CreateReceivableInvoice call
        string successExternalId = "1";

        // ResidenceLifeConfiguration values
        string invoiceReceivableTypeCode = "IRT";
        string invoiceTypeCode = "ITC";
        string externalSystemId = "ESI";

        [TestInitialize]
        public void TestInitialize()
        {
            // Set up current user
            currentUserFactory = new ResidenceLifeAccountsReceivableServiceTestUserFactory();

            // Initialize the ResidenceLifeConfigurationRepository mock
            residenceLifeConfigurationRepositoryMock = new Mock<IResidenceLifeConfigurationRepository>();
            residenceLifeConfigurationRepository = residenceLifeConfigurationRepositoryMock.Object;

            // Initialize the AccountsReceivableService mock
            accountsReceivableServiceMock = new Mock<IAccountsReceivableService>();
            accountsReceivableService = accountsReceivableServiceMock.Object;

            // Initialize the adapter registry mock
            var emptyAdapterRegistryMock = new Mock<IAdapterRegistry>();
            adapterRegistryMock = new Mock<IAdapterRegistry>();
            adapterRegistry = adapterRegistryMock.Object;

            // Mock the adapter registry request for res-life dto to finance dto adapter
            adapterRegistryMock.Setup(x => x.GetAdapter<Dtos.ResidenceLife.ReceivableInvoice,
                    Dtos.Finance.ReceivableInvoice>()).Returns(new ResidenceLifeReceivableInvoiceDtoToDtoAdapter(emptyAdapterRegistryMock.Object, logger));

            // Mock the ResidenceLifeConfigurationRepository GetResidenceLifeConfiguration method
            Domain.ResidenceLife.Entities.ResidenceLifeConfiguration residenceLifeConfiguration =
                new ResidenceLifeConfiguration(invoiceReceivableTypeCode, invoiceTypeCode, externalSystemId, "", "");
            residenceLifeConfigurationRepositoryMock.Setup(x => x.GetResidenceLifeConfiguration()).Returns(residenceLifeConfiguration);

            // Mock a successful AccountsReceivableService.PostReceivableInvoice method
            // Return success only if the values from the ResidenceLifeConfiguration are in the DTO which is passed in, testing that
            // configuration values are populated into the dto correctly.
            // Return the dto that was passed in (the real implementation will add an ID and other properties, but that is not needed
            // for this test.)
            accountsReceivableServiceMock.Setup(x => x.PostReceivableInvoice(
                It.Is<Dtos.Finance.ReceivableInvoice>(ri => (ri.ExternalIdentifier == successExternalId && ri.ReceivableType == invoiceReceivableTypeCode
                                                      && ri.InvoiceType == invoiceTypeCode && ri.ExternalSystem == externalSystemId))))
                .Returns((Dtos.Finance.ReceivableInvoice ri) => ri);

            // Mock a call to the AccountsReceivableService.PostReceivableInvoice method with the success ExternalID, but
            // with incorrect ResidenceLifeConfiguration values. This will report if the configuration values are not handled
            // properly by the CreateReceivableInvoice service.
            accountsReceivableServiceMock.Setup(x => x.PostReceivableInvoice(
                It.Is<Dtos.Finance.ReceivableInvoice>(ri => (ri.ExternalIdentifier == successExternalId && (ri.ReceivableType != invoiceReceivableTypeCode
                                                      || ri.InvoiceType != invoiceTypeCode || ri.ExternalSystem != externalSystemId)))))
                .Throws(new Exception("ResidenceLifeConfiguration values were not properly passed to the Finance.PostReceivableInvoice method."));

            // Initialize the ILogger mock            
            logger = new Mock<ILogger>().Object;

            // Mock the role repository to return a role with the permissions needed to access all ResidenceLifeAccountsReceivable services
            resLifeApiRole = new Role(1, "RES.LIFE.API");
            resLifeApiRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(
                ResidenceLifePermissionCodes.ViewMealPlanAssignments));
            resLifeApiRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(
                ResidenceLifePermissionCodes.CreateRlInvoices));
            roleRepoMock = new Mock<IRoleRepository>();
            roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Role>() { resLifeApiRole });
            roleRepo = roleRepoMock.Object;

            // Create the ReceiptService mock
            _receiptServiceMock = new Mock<IReceiptService>();

            // Create the actual service we will test
            residenceLifeAccountsReceivableService = new ResidenceLifeAccountsReceivableService(adapterRegistry, currentUserFactory, roleRepo, logger,
                accountsReceivableService, residenceLifeConfigurationRepository, _receiptServiceMock.Object);                                                    
        }

        [TestMethod]
        public void TestSuccessfulCreateReceivableInvoice()
        {
            // Setup an input ReceivableInvoice dto
            DateTime dueDate = DateTime.Today;
            string description = "A description";
            List<Dtos.ResidenceLife.ReceivableCharge> receivableCharges = new List<Dtos.ResidenceLife.ReceivableCharge> () {
                    new Dtos.ResidenceLife.ReceivableCharge {BaseAmount = 23.33M, Code = "CD1", Description = new List<string> {"LN1", "LN2"}},
                    new Dtos.ResidenceLife.ReceivableCharge {BaseAmount = 22M, Code = "CD2", Description = new List<string> {"A", "B"}}};
            string termId = "2014/FA";
            DateTime date = DateTime.Today.AddDays(-2);
            string location = "LOC";

            Dtos.ResidenceLife.ReceivableInvoice receivableInvoiceDtoIn = new Dtos.ResidenceLife.ReceivableInvoice {
                DueDate = dueDate,
                Description = description,
                Charges = receivableCharges,
                ExternalIdentifier = successExternalId,
                TermId = termId,
                Date = date,
                Location = location};

            // Invoke CreateReceivableInvoice
            Dtos.Finance.ReceivableInvoice receivableInvoiceDtoOut = residenceLifeAccountsReceivableService.CreateReceivableInvoice(receivableInvoiceDtoIn);

            // If successful, the returned attributes will match those passed in. This tests the adapter mapping of the res life dto
            // to the finance dto in CreateReceivableInvoice.
            // (That the mocked ResidenceLifeConfiguration attributes were properly read is implicitly checked in the mock call to
            //  PostReceivableInvoice which will throw an exception if the configuration values are not correctly passed to it.)
            Assert.AreEqual(dueDate, receivableInvoiceDtoOut.DueDate);
            Assert.AreEqual(description, receivableInvoiceDtoOut.Description);
            Assert.AreEqual(receivableCharges.Count, receivableInvoiceDtoOut.Charges.Count);
            for (int i = 0; i < receivableCharges.Count; i++)
            {
                Assert.AreEqual(receivableCharges[i].BaseAmount, receivableInvoiceDtoOut.Charges[i].BaseAmount);
                Assert.AreEqual(receivableCharges[i].Code, receivableInvoiceDtoOut.Charges[i].Code);
                CollectionAssert.AreEqual(receivableCharges[i].Description, receivableInvoiceDtoOut.Charges[i].Description);
            }
            Assert.AreEqual(successExternalId, receivableInvoiceDtoOut.ExternalIdentifier);
            Assert.AreEqual(termId, receivableInvoiceDtoOut.TermId);
            Assert.AreEqual(date, receivableInvoiceDtoOut.Date);
            Assert.AreEqual(location, receivableInvoiceDtoOut.Location);
        }

        //[TestMethod]
        //[ExpectedException(typeof(PermissionsException))]
        //public void TestGetWithoutPermissions()
        //{
        //    // Instantiate an entire new service, passing in different role repository that returns
        //    // different permissions.
        //    Role myResLifeApiRole;
        //    myResLifeApiRole = new Role(1, "RES.LIFE.API");
        //    myResLifeApiRole.AddPermission(new Ellucian.Colleague.Domain.Entities.Permission(
        //        ResidenceLifePermissionCodes.CreateUpdateMealPlanAssignments));
        //    Mock<IRoleRepository> myRoleRepoMock;
        //    myRoleRepoMock = new Mock<IRoleRepository>();
        //    myRoleRepoMock.Setup(rpm => rpm.GetRolesAsync()).ReturnsAsync(new List<Role>() { myResLifeApiRole });
        //    IRoleRepository myRoleRepo;
        //    myRoleRepo = myRoleRepoMock.Object;
        //    MealPlanAssignmentService myMealPlanAssignmentService = new MealPlanAssignmentService(
        //        adapterRegistry, currentUserFactory, myRoleRepo, logger, mealPlanRepo);


        //    // Invoke GetMealPlanAssignment, which will fail its permissions check
        //    Dtos.ResidenceLife.MealPlanAssignment myMealPlanAssignmentDto =
        //        myMealPlanAssignmentService.GetMealPlanAssignment("doesn't matter");
        //}

    }

    // Setup a fake ICurrentUserFactory to pass into the service constructor.
    public class ResidenceLifeAccountsReceivableServiceTestUserFactory : ICurrentUserFactory
    {
        public ICurrentUser CurrentUser
        {
            get
            {
                return new CurrentUser(new Claims()
                {
                    ControlId = "123",
                    Name = "George",
                    PersonId = "0000011",
                    SecurityToken = "321",
                    SessionTimeout = 30,
                    UserName = "ResLifeApi",
                    Roles = new List<string>() {"RES.LIFE.API"},
                    SessionFixationId = "abc123"
                });
            }
        }
    }
}
