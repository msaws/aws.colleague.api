﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.ProjectsAccounting.DataContracts;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Repositories;
using Ellucian.Data.Colleague.DataContracts;

namespace Ellucian.Colleague.Data.ProjectsAccounting.Tests
{
    public class TestProjectsAccountingReferenceDataRepository : IProjectsAccountingReferenceDataRepository
    {
        #region Item Codes
        public Collection<ProjectsItemCodes> ItemCodesDataContracts = new Collection<ProjectsItemCodes>()
        {
            new ProjectsItemCodes() { Recordkey = "FB", PrjicDesc = "Fringe Benefits" },
            new ProjectsItemCodes() { Recordkey = "PC", PrjicDesc = "Personnel Costs" }
        };

        public async Task<IEnumerable<ProjectItemCode>> GetLineItemCodesAsync()
        {
            var testProjectItemCodes = new List<ProjectItemCode>();
            foreach (var itemCode in this.ItemCodesDataContracts)
            {
                testProjectItemCodes.Add(new ProjectItemCode(itemCode.Recordkey, itemCode.PrjicDesc));
            }

            return await Task.Run(() => testProjectItemCodes);
        }
        #endregion

        #region Project Types
        public ApplValcodes ProjectTypesValcode = new ApplValcodes()
        {
            ValInternalCode = new List<string>() { "R", "H", "C" },
            ValExternalRepresentation = new List<string>() { "Research", "Housing", "Construction" }
        };

        public async Task<IEnumerable<ProjectType>> GetProjectTypesAsync()
        {
            this.ProjectTypesValcode.buildAssociations();
            var TestProjectTypes = new List<ProjectType>();
            foreach (var projectType in this.ProjectTypesValcode.ValsEntityAssociation)
            {
                TestProjectTypes.Add(new ProjectType(projectType.ValInternalCodeAssocMember, projectType.ValExternalRepresentationAssocMember));
            }

            return await Task.Run(() => TestProjectTypes);
        }
        #endregion
    }
}
