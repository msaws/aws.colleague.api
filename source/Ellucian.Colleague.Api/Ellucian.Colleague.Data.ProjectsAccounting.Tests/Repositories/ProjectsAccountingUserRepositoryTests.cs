﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Data.ColleagueFinance.DataContracts;
using Ellucian.Colleague.Data.ProjectsAccounting.Repositories;
using Ellucian.Colleague.Data.ProjectsAccounting.Transactions;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Tests;
using Ellucian.Data.Colleague;
using Ellucian.Web.Cache;
using Ellucian.Web.Http.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;

namespace Ellucian.Colleague.Data.ProjectsAccounting.Tests.Repositories
{
    /// <summary>
    /// This class tests the methods in the ProjectsAccountingUser repository. 
    /// </summary>
    [TestClass]
    public class ProjectsAccountingUserRepositoryTests
    {
        #region Initialize and Cleanup
        private ProjectsAccountingUserRepository repository = null;
        private Mock<IColleagueDataReader> DataReader = null;
        private Mock<IColleagueTransactionInvoker> transactionInvoker = null;
        private TestProjectsAccountingUserRepository testProjectsAccountingUserRepository = null;
        private ProjectsAccountingUser projectsAccountingUserDomainEntity;
        private Mock<ICacheProvider> cacheProviderMock = new Mock<ICacheProvider>();
        private ApiSettings apiSettings;

        private GetProjectsForPaUserResponse paUserResponse;

        #region PaUserDataContract
        public class PaUserDataContract
        {
            public Person personDataContract { get; set; }
            public Staff staffDataContract { get; set; }
            public Glusers glUsersDataContract { get; set; }
            public Glroles glRolesDataContract { get; set; }

            public PaUserDataContract()
            {
                personDataContract = null;
                staffDataContract = null;
                glUsersDataContract = null;
                glRolesDataContract = null;
            }
        }

        private List<PaUserDataContract> paUserDataContracts;
        #endregion

        private Collection<Glroles> glRolesResponse;

        [TestInitialize]
        public void Initialize()
        {
            // Set up a mock data reader. All Colleague repositories have a local instance of an IColleagueDataReader.
            // We don't want the unit tests to rely on a real Colleague data reader 
            // (which would require a real Colleague environment).
            // Instead, we create a mock data reader which we can control locally.
            DataReader = new Mock<IColleagueDataReader>();

            // Set up a mock transaction invoker for the colleague transaction that gets
            // the GL accounts descriptions for the GL accounts in a project line item.
            transactionInvoker = new Mock<IColleagueTransactionInvoker>();
            apiSettings = new ApiSettings("TEST");
            repository = BuildValidProjectsAccountingUserRepository();
            testProjectsAccountingUserRepository = new TestProjectsAccountingUserRepository();

            // Initialize variables to be used by mock
            paUserDataContracts = new List<PaUserDataContract>();

        }

        [TestCleanup]
        public void Cleanup()
        {
            repository = null;
            DataReader = null;
            transactionInvoker = null;
            testProjectsAccountingUserRepository = null;

            paUserDataContracts = null;
            paUserResponse = null;
            glRolesResponse = null;
        }
        #endregion

        #region Tests for GetProjectsAccountingUser
        [TestMethod]
        public async Task GetProjectsAccountingUser_NoAccessSpecified()
        {
            var personId = "0000007";
            this.projectsAccountingUserDomainEntity = await testProjectsAccountingUserRepository.GetProjectsAccountingUserAsync(personId);

            InitializeCtxResponsesAndDataContracts();
            InitializeMockMethods();

            // Run the repository GetProjectsAccountingUser method and check the result;
            var projectsAccountingUser = await repository.GetProjectsAccountingUserAsync(personId);

            #region Execute the tests
            // Make sure the ProjectsAccountingUser information is correct
            Assert.AreEqual(projectsAccountingUser.Projects.Count(), projectsAccountingUserDomainEntity.Projects.Count());
            for (int i= 0; i<projectsAccountingUser.Projects.Count(); i++)
            {
                var repositoryProjectId = projectsAccountingUser.Projects[i];
                var testRepositoryProjectId = this.projectsAccountingUserDomainEntity.Projects[i];
                Assert.AreEqual(repositoryProjectId,testRepositoryProjectId);
            }

            Assert.AreEqual(GlAccessLevel.No_Access, projectsAccountingUser.GlAccessLevel);

            #endregion
        }

        [TestMethod]
        public async Task GetProjectsAccountingUser_AccessSpecified()
        {
            var personId = "0000006";
            this.projectsAccountingUserDomainEntity = await testProjectsAccountingUserRepository.GetProjectsAccountingUserAsync(personId);

            InitializeCtxResponsesAndDataContracts();
            InitializeMockMethods();

            // Run the repository GetProjectsAccountingUser method and check the result;
            var projectsAccountingUser = await repository.GetProjectsAccountingUserAsync(personId);

            #region Execute the tests
            // Make sure the ProjectsAccountingUser information is correct
            Assert.AreEqual(projectsAccountingUser.Projects.Count(), projectsAccountingUserDomainEntity.Projects.Count());
            for (int i = 0; i < projectsAccountingUser.Projects.Count(); i++)
            {
                var repositoryProjectId = projectsAccountingUser.Projects[i];
                var testRepositoryProjectId = this.projectsAccountingUserDomainEntity.Projects[i];
                Assert.AreEqual(repositoryProjectId, testRepositoryProjectId);
            }

            Assert.AreEqual(GlAccessLevel.No_Access, projectsAccountingUser.GlAccessLevel);

            #endregion
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task GetProjectsAccountingUser_NullId()
        {
            var project = await repository.GetProjectsAccountingUserAsync(null);
        }
        #endregion

        #region Private methods
        /// <summary>
        /// This private method builds a valid projects accounting user repository, 
        /// with the appropriate mocks, from which we can test.
        /// </summary>
        /// <returns>Returns a ProjectsAccountingUserRepository object.</returns>
        private ProjectsAccountingUserRepository BuildValidProjectsAccountingUserRepository()
        {
            // A ProjectRepository requires three objects for its constructor:
            //    1. an implementation of a cache provider
            //    2. an implementation of a Colleague transaction factory
            //    3. an implementation of a logger
            // We need the unit tests to be independent of "real" implementations of these classes, so we use
            // Moq to create mock implementations that are based on the same interfaces
            var cacheProviderObject = cacheProviderMock.Object;
            var transactionFactory = new Mock<IColleagueTransactionFactory>();
            var transactionFactoryObject = transactionFactory.Object;
            var loggerObject = new Mock<ILogger>().Object;

            // The transaction factory has a method to get its data reader
            // Make sure that method returns our mock data reader
            transactionFactory.Setup(transFac => transFac.GetDataReader()).Returns(DataReader.Object);
            transactionFactory.Setup(transFac => transFac.GetTransactionInvoker()).Returns(transactionInvoker.Object);

            // Using the three mock objects, we can now construct a repository.
            var projectAccountingUserRepository = new ProjectsAccountingUserRepository(cacheProviderObject, transactionFactoryObject, loggerObject, apiSettings);


            cacheProviderMock.Setup<Task<Tuple<object, SemaphoreSlim>>>(x =>
                   x.GetAndLockSemaphoreAsync(It.IsAny<string>(), null))
                   .ReturnsAsync(new Tuple<object, SemaphoreSlim>(null, new SemaphoreSlim(1, 1)));

            return projectAccountingUserRepository;
        }

        /// <summary>
        /// Take the list of project domain entities and convert all of the contained objects into data contracts
        /// </summary>
        private void InitializeCtxResponsesAndDataContracts()
        {

            this.paUserResponse = new GetProjectsForPaUserResponse()
            {
                UserProjects = this.projectsAccountingUserDomainEntity.Projects.ToList()
            };
            Glroles glRolesDataContract = null;
            Person personDataContract = null;
            Staff staffDataContract = null;
            Glusers glUsersDataContract = null;
            PaUserDataContract paUserDataContract = null;

            #region PaUser full GL access
            glRolesDataContract = new Glroles()
            {
                Recordkey = "ALL-ACCESS",
                GlrRoleUse = "B"
            };
            personDataContract = new Person()
             {
                 Recordkey = "0000005",
                 LastName = "Thorne"
             };
                       
            staffDataContract = new Staff()
             {
                 Recordkey = "0000005",
                 StaffLoginId = "GTT"
             };
                        
            glUsersDataContract = new Glusers()
             {
                 Recordkey = "GTT",
                 GlusStartDate = new DateTime(2015, 01, 01),
                 GlusRoleIds = new List<string>(){"ALL-ACCESS"},
                 GlusRoleStartDates = new List<DateTime?>(){new DateTime (2015, 01, 01)}
             };
            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion

            #region PaUser possible GL access
            glRolesDataContract = new Glroles()
            {
                Recordkey = "ALL-ACCESS",
                GlrRoleUse = "B"
            };
            
            personDataContract = new Person()
            {
                Recordkey = "0000006",
                LastName = "Kleehammer"
            };

            staffDataContract = new Staff()
            {
                Recordkey = "0000006",
                StaffLoginId = "AJK"
            };

            glUsersDataContract = new Glusers()
            {
                Recordkey = "AJK",
                GlusStartDate = new DateTime(2015, 01, 01),
                GlusRoleIds = new List<string>() { "ANDY" },
                GlusRoleStartDates = new List<DateTime?>() { new DateTime(2015, 01, 01) }

            };
            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion

            #region PaUser no GL active roles
            glRolesDataContract = new Glroles()
            {
                Recordkey = "ALL-ACCESS",
                GlrRoleUse = "B"
            };

            personDataContract = new Person()
            {
                Recordkey = "0000007",
                LastName = "Longerbeam"
            };

            staffDataContract = new Staff()
            {
                Recordkey = "0000007",
                StaffLoginId = "TGL"
            };

            glUsersDataContract = new Glusers()
            {
                Recordkey = "TGL",
                GlusStartDate = new DateTime(2014, 01, 01),
                GlusRoleIds = new List<string>() { "ALL-ACCESS" },
                GlusRoleStartDates = new List<DateTime?>() { new DateTime(2014, 01, 01) },
                GlusRoleEndDates = new List<DateTime?>() { new DateTime(2014, 07, 31)}

            };
            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion

            #region PaUser with and expired GL access record
            glRolesDataContract = new Glroles()
            {
                Recordkey = "ALL-ACCESS",
                GlrRoleUse = "B"
            }; 
            
            personDataContract = new Person()
            {
                Recordkey = "0000008",
                LastName = "Longerbeam"
            };

            staffDataContract = new Staff()
            {
                Recordkey = "0000008",
                StaffLoginId = "TL2"
            };

            glUsersDataContract = new Glusers()
            {
                Recordkey = "TL2",
                GlusStartDate = new DateTime(2014, 01, 01),
                GlusEndDate = new DateTime(2014, 07, 31),
                GlusRoleIds = new List<string>() { "ALL-ACCESS" },
                GlusRoleStartDates = new List<DateTime?>() { new DateTime(2014, 01, 01) },
                GlusRoleEndDates = new List<DateTime?>() { new DateTime(2014, 07, 31) }

            };
            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion

            #region PaUser with no GL access record
            glRolesDataContract = new Glroles()
            {
                Recordkey = "ALL-ACCESS",
                GlrRoleUse = "B"
            }; 
            
            personDataContract = new Person()
            {
                Recordkey = "0000009",
                LastName = "Longerbeam"
            };

            staffDataContract = new Staff()
            {
                Recordkey = "0000009",
                StaffLoginId = "TL3"
            };

            glUsersDataContract = new Glusers()
                {

                };

            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion

            #region PaUser with no staff login ID
            glRolesDataContract = new Glroles()
            {
                Recordkey = "ALL-ACCESS",
                GlrRoleUse = "B"
            }; 
            
            personDataContract = new Person()
            {
                Recordkey = "0000010",
                LastName = "Longerbeam"
            };

            staffDataContract = new Staff()
            {
                Recordkey = "0000010",
                StaffLoginId = ""
            };

            glUsersDataContract = new Glusers()
            {

            };

            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion

            #region PaUser with no staff record
            glRolesDataContract = new Glroles()
            {
                Recordkey = "ALL-ACCESS",
                GlrRoleUse = "B"
            }; 
            
            personDataContract = new Person()
            {
                Recordkey = "0000011",
                LastName = "Longerbeam"
            };

            staffDataContract = new Staff()
            {

            };

            glUsersDataContract = new Glusers()
            {

            };

            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion

            #region PaUser full GL access and other roles
            glRolesDataContract = new Glroles()
            {
                Recordkey = "ALL-ACCESS",
                GlrRoleUse = "B"
            }; 
            
            personDataContract = new Person()
            {
                Recordkey = "0000012",
                LastName = "Thorne"
            };

            staffDataContract = new Staff()
            {
                Recordkey = "0000012",
                StaffLoginId = "GT5"
            };

            glUsersDataContract = new Glusers()
            {
                Recordkey = "GT5",
                GlusStartDate = new DateTime(2015, 01, 01),
                GlusRoleIds = new List<string>() { "ALL-ACCESS", "GTT4" },
                GlusRoleStartDates = new List<DateTime?>() { new DateTime(2015, 01, 01), new DateTime(2015, 01, 01) }
            };
            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion

            #region PaUser full GL access and other roles, missing full access role
            glRolesDataContract = new Glroles()
            {
                Recordkey = null,
                GlrRoleUse = null
            };

            personDataContract = new Person()
            {
                Recordkey = "0000013",
                LastName = "Thorne"
            };

            staffDataContract = new Staff()
            {
                Recordkey = "0000013",
                StaffLoginId = "GT2"
            };

            glUsersDataContract = new Glusers()
            {
                Recordkey = "GT2",
                GlusStartDate = new DateTime(2015, 01, 01),
                GlusRoleIds = new List<string>() { "ALL-ACCESS", "GTT4" },
                GlusRoleStartDates = new List<DateTime?>() { new DateTime(2015, 01, 01), new DateTime(2015, 01, 01) }
            };
            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion

            #region PaUser full GL access and other roles, full access role for Colleague only
            glRolesDataContract = new Glroles()
            {
                Recordkey = "FULL-ACCESS",
                GlrRoleUse = "C"
            };

            personDataContract = new Person()
            {
                Recordkey = "0000014",
                LastName = "Thorne"
            };

            staffDataContract = new Staff()
            {
                Recordkey = "0000014",
                StaffLoginId = "GT2"
            };

            glUsersDataContract = new Glusers()
            {
                Recordkey = "GT2",
                GlusStartDate = new DateTime(2015, 01, 01),
                GlusRoleIds = new List<string>() { "FULL-ACCESS", "GTT4" },
                GlusRoleStartDates = new List<DateTime?>() { new DateTime(2015, 01, 01), new DateTime(2015, 01, 01) }
            };
            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion

            #region PaUser full GL access and other roles, null full access role
            glRolesDataContract = new Glroles()
            {
                Recordkey = null,
                GlrRoleUse = null
            };

            personDataContract = new Person()
            {
                Recordkey = "0000015",
                LastName = "Thorne"
            };

            staffDataContract = new Staff()
            {
                Recordkey = "0000015",
                StaffLoginId = "GT6"
            };

            glUsersDataContract = new Glusers()
            {
                Recordkey = "GT6",
                GlusStartDate = new DateTime(2015, 01, 01),
                GlusRoleIds = new List<string>() { "ALL=GL", "GTT4" },
                GlusRoleStartDates = new List<DateTime?>() { new DateTime(2015, 01, 01), new DateTime(2015, 01, 01) }
            };
            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion

            #region PaUser has full GL access which is null and no other active roles
            glRolesDataContract = new Glroles()
            {
                Recordkey = null,
                GlrRoleUse = null
            };

            personDataContract = new Person()
            {
                Recordkey = "0000016",
                LastName = "Thorne"
            };

            staffDataContract = new Staff()
            {
                Recordkey = "0000016",
                StaffLoginId = "GT7"
            };

            glUsersDataContract = new Glusers()
            {
                Recordkey = "GT7",
                GlusStartDate = new DateTime(2015, 01, 01),
                GlusRoleIds = new List<string>() { "ALL=GL", "GTT3" },
                GlusRoleStartDates = new List<DateTime?>() { new DateTime(2015, 01, 01), new DateTime(2015, 01, 01) }
            };
            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion

            #region PaUser has 3 roles with 2nd role missing start date
            glRolesDataContract = new Glroles()
            {
                Recordkey = "ALL-ACCESS",
                GlrRoleUse = "B"
            };

            personDataContract = new Person()
            {
                Recordkey = "0000017",
                LastName = "Thorne"
            };

            staffDataContract = new Staff()
            {
                Recordkey = "0000017",
                StaffLoginId = "GT8"
            };

            glUsersDataContract = new Glusers()
            {
                Recordkey = "GT8",
                GlusStartDate = new DateTime(2015, 01, 01),
                GlusRoleIds = new List<string>() { "GTT4", "ANDY", "GTT3" },
                GlusRoleStartDates = new List<DateTime?>() { new DateTime(2015, 01, 01), null, new DateTime(2015, 01, 01) },
                GlusRoleEndDates = new List<DateTime?>() { new DateTime(2120, 04, 01), new DateTime(2120, 04, 01), new DateTime(2120, 04, 01) }
            };
            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion

            #region PaUser has 3 roles with 2nd role missing end date
            glRolesDataContract = new Glroles()
            {
                Recordkey = "ALL-ACCESS",
                GlrRoleUse = "B"
            };

            personDataContract = new Person()
            {
                Recordkey = "0000018",
                LastName = "Thorne"
            };

            staffDataContract = new Staff()
            {
                Recordkey = "0000018",
                StaffLoginId = "GT9"
            };

            glUsersDataContract = new Glusers()
            {
                Recordkey = "GT9",
                GlusStartDate = new DateTime(2015, 01, 01),
                GlusRoleIds = new List<string>() { "GTT4", "ANDY", "GTT3" },
                GlusRoleStartDates = new List<DateTime?>() { new DateTime(2015, 01, 01), new DateTime(2015, 01, 01), new DateTime(2015, 01, 01) },
                GlusRoleEndDates = new List<DateTime?>() { new DateTime(2015, 04, 01), null, new DateTime(2015, 04, 01) }
            };
            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion

            #region PaUser has 3 roles with 3rd role missing end date
            glRolesDataContract = new Glroles()
            {
                Recordkey = "ALL-ACCESS",
                GlrRoleUse = "B"
            };

            personDataContract = new Person()
            {
                Recordkey = "0000019",
                LastName = "Thorne"
            };

            staffDataContract = new Staff()
            {
                Recordkey = "0000019",
                StaffLoginId = "GT10"
            };

            glUsersDataContract = new Glusers()
            {
                Recordkey = "GT10",
                GlusStartDate = new DateTime(2015, 01, 01),
                GlusRoleIds = new List<string>() { "GTT4", "ANDY", "GTT3" },
                GlusRoleStartDates = new List<DateTime?>() { new DateTime(2015, 01, 01), new DateTime(2015, 01, 01), new DateTime(2015, 01, 01) },
                GlusRoleEndDates = new List<DateTime?>() { new DateTime(2120, 04, 01), new DateTime(2120, 04, 01), null }
            };
            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion

            #region PaUser has 3 roles with 3rd role missing start date
            glRolesDataContract = new Glroles()
            {
                Recordkey = "ALL-ACCESS",
                GlrRoleUse = "B"
            };

            personDataContract = new Person()
            {
                Recordkey = "0000020",
                LastName = "Thorne"
            };

            staffDataContract = new Staff()
            {
                Recordkey = "0000020",
                StaffLoginId = "GT11"
            };

            glUsersDataContract = new Glusers()
            {
                Recordkey = "GT11",
                GlusStartDate = new DateTime(2015, 01, 01),
                GlusRoleIds = new List<string>() { "GTT4", "ANDY", "GTT3" },
                GlusRoleStartDates = new List<DateTime?>() { new DateTime(2015, 01, 01), new DateTime(2015, 01, 01), null },
                GlusRoleEndDates = new List<DateTime?>() { new DateTime(2120, 04, 01), new DateTime(2120, 04, 01), new DateTime(2120, 04, 01) }
            };
            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion

            #region PaUser has 1 roles with role missing start date
            glRolesDataContract = new Glroles()
            {
                Recordkey = "ALL-ACCESS",
                GlrRoleUse = "B"
            };

            personDataContract = new Person()
            {
                Recordkey = "0000021",
                LastName = "Thorne"
            };

            staffDataContract = new Staff()
            {
                Recordkey = "0000021",
                StaffLoginId = "GT12"
            };

            glUsersDataContract = new Glusers()
            {
                Recordkey = "GT12",
                GlusStartDate = new DateTime(2015, 01, 01),
                GlusRoleIds = new List<string>() { "GTT4" },
                GlusRoleStartDates = new List<DateTime?>() { null },
                GlusRoleEndDates = new List<DateTime?>() { new DateTime(2120, 04, 01) }
            };
            paUserDataContract = new PaUserDataContract();
            paUserDataContract.glRolesDataContract = glRolesDataContract;
            paUserDataContract.personDataContract = personDataContract;
            paUserDataContract.staffDataContract = staffDataContract;
            paUserDataContract.glUsersDataContract = glUsersDataContract;
            this.paUserDataContracts.Add(paUserDataContract);
            #endregion
        }

        /// <summary>
        /// Set up all mock methods to return pre-defined data
        /// </summary>
        private void InitializeMockMethods()
        {
            // Mock ReadRecord to return a pre-defined paUserDataContract
            foreach (var paUser in this.paUserDataContracts)
            {
                // Mock DataReadRecord to return a Person data contract
                DataReader.Setup(acc => acc.ReadRecordAsync<Person>("PERSON", paUser.personDataContract.Recordkey, true)).Returns(Task.FromResult(paUser.personDataContract));
                DataReader.Setup(acc => acc.ReadRecordAsync<Person>("PERSON", paUser.personDataContract.Recordkey, true)).ReturnsAsync(paUser.personDataContract);

                // Mock DataReadRecord to return a Staff data contract
                DataReader.Setup(acc => acc.ReadRecordAsync<Staff>("STAFF", paUser.staffDataContract.Recordkey, true)).Returns(Task.FromResult(paUser.staffDataContract));

                // Mock DataReadRecord to return a Glusers data contract
                DataReader.Setup(acc => acc.ReadRecordAsync<Glusers>("GLUSERS", paUser.glUsersDataContract.Recordkey, true)).Returns(Task.FromResult(paUser.glUsersDataContract));

                // Mock DataReadRecord to return a Glroless data contract
                DataReader.Setup(acc => acc.ReadRecordAsync<Glroles>("GLROLES", paUser.glRolesDataContract.Recordkey, true)).Returns(Task.FromResult(paUser.glRolesDataContract));

                // Mock DataReadRecord to return name address hierarcy for the person's preferred name.
                DataReader.Setup<Task<Ellucian.Colleague.Data.Base.DataContracts.NameAddrHierarchy>>(a =>
                    a.ReadRecordAsync<Ellucian.Colleague.Data.Base.DataContracts.NameAddrHierarchy>("NAME.ADDR.HIERARCHY", "PREFERRED", true))
                    .ReturnsAsync(new Ellucian.Colleague.Data.Base.DataContracts.NameAddrHierarchy()
                    {
                        Recordkey = "PREFERRED",
                        NahNameHierarchy = new List<string>() { "PF" }
                    });
            }

            // Mock bulk read GLROLES bulk read
            glRolesResponse = new Collection<Glroles>()
                {
                    new Glroles()
                    {
                        // "ALL-ACCESS"
                        Recordkey = "ALL-ACCESS", GlrRoleUse = "B"
                    },
                    new Glroles
                    {
                        // "GTT3"
                        Recordkey = "GTT3", GlrRoleUse = "C"
                    },
                    new Glroles()
                    {
                        // "ANDY"
                        Recordkey = "ANDY", GlrRoleUse = "W"
                    },
                    new Glroles()
                    {
                        // "FULL-ACCESS"
                        Recordkey = "FULL-ACCESS", GlrRoleUse = "C"                    
                    },
                    new Glroles()
                    {
                        // "GTT4"
                        Recordkey = "GTT4", GlrRoleUse = "W"
                    }
                };
            DataReader.Setup<Task<Collection<Glroles>>>(acc => acc.BulkReadRecordAsync<Glroles>("GLROLES", It.IsAny<string[]>(), true)).Returns(Task.FromResult(glRolesResponse));

            var glRolesList = new string[] { "ALL-ACCESS", "ANDY", "GTT4" };
            DataReader.Setup<Task<string[]>>(acc => acc.SelectAsync("GLROLES", It.IsAny<string[]>(), It.IsAny<string>())).Returns(Task.FromResult(glRolesList));

            // Mock Execute within the transaction invoker to return a GetGlAccountDescriptionResponse object
            transactionInvoker.Setup(tio => tio.Execute<GetProjectsForPaUserRequest, GetProjectsForPaUserResponse>(It.IsAny<GetProjectsForPaUserRequest>())).Returns(this.paUserResponse);
        }
        #endregion
    }
}
