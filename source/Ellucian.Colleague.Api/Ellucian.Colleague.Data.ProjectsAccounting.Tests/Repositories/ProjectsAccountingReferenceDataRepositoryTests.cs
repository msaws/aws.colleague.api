﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.ProjectsAccounting.DataContracts;
using Ellucian.Colleague.Data.ProjectsAccounting.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using slf4net;
using Ellucian.Data.Colleague.DataContracts;

namespace Ellucian.Colleague.Data.ProjectsAccounting.Tests.Repositories
{
    [TestClass]
    public class ProjectsAccountingReferenceDataRepositoryTests : BaseRepositorySetup
    {
        #region Initialize and Cleanup
        //private Mock<BaseColleagueRepository> repositoryMock = null;
        private ProjectsAccountingReferenceDataRepository actualRepository;
        private TestProjectsAccountingReferenceDataRepository expectedRepository;

        [TestInitialize]
        public void Initialize()
        {
            this.MockInitialize();

            // Initialize the projects accounting reference data repository
            expectedRepository = new TestProjectsAccountingReferenceDataRepository();

            this.actualRepository = BuildRepository();

            // Mock BulkReadRecord to return pre-defined Projects Item Codes data contracts
            dataReaderMock.Setup(acc => acc.BulkReadRecordAsync<ProjectsItemCodes>(It.IsAny<string>(), "", true)).Returns(() =>
            {
                return Task.FromResult(this.expectedRepository.ItemCodesDataContracts);
            });

            // Mock ReadRecord to return pre-defined Project Type Valcode information
            dataReaderMock.Setup(dr => dr.ReadRecordAsync<ApplValcodes>(It.IsAny<string>(), It.IsAny<string>(), true)).Returns(() =>
            {
                return Task.FromResult(this.expectedRepository.ProjectTypesValcode);
            });
        }

        [TestCleanup]
        public void Cleanup()
        {
            expectedRepository = null;
        }
        #endregion

        #region Tests
        [TestMethod]
        public async Task GetLineItemCodes()
        {
            var expectedCodes = await this.expectedRepository.GetLineItemCodesAsync();
            var actualCodes = await this.actualRepository.GetLineItemCodesAsync();

            foreach (var expectedCode in expectedCodes)
            {
                Assert.IsTrue(actualCodes.Any(actualCode =>
                    actualCode.Code == expectedCode.Code
                    && actualCode.Description == expectedCode.Description));
            }
        }

        [TestMethod]
        public async Task GetProjectTypes()
        {
            var expectedTypes = await this.expectedRepository.GetProjectTypesAsync();
            var actualTypes = await this.actualRepository.GetProjectTypesAsync();

            foreach (var expectedType in expectedTypes)
            {
                Assert.IsTrue(actualTypes.Any(actualType =>
                    actualType.Code == expectedType.Code
                    && actualType.Description == expectedType.Description));
            }
        }
        #endregion

        #region Private methods
        private ProjectsAccountingReferenceDataRepository BuildRepository()
        {
            return new ProjectsAccountingReferenceDataRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);
        }
        #endregion
    }
}
