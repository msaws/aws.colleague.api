﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Ellucian.Colleague.Data.ColleagueFinance.DataContracts;
using Ellucian.Colleague.Data.ColleagueFinance.Transactions;
using Ellucian.Colleague.Data.ProjectsAccounting.DataContracts;
using Ellucian.Colleague.Data.ProjectsAccounting.Repositories;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;
using Ellucian.Colleague.Domain.ProjectsAccounting.Tests;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.DataContracts;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Ellucian.Colleague.Data.ProjectsAccounting.Tests.Repositories
{
    /// <summary>
    /// This class tests two conditions. 
    /// The first test is of the GetProjects method in the project repository class,
    /// and that it actually gets all of the project domain entities assign to a user. 
    /// The second test is for testing a condition where one of the project domain entities is corrupt.
    /// </summary>
    [TestClass]
    public class ProjectRepositoryTests : BaseRepositorySetup
    {
        #region Initialize and Cleanup
        private ProjectRepository repository = null;
        private ApplValcodes GlSourceCodes = null;
        private Collection<PaGla> paGlaTransactions = null;
        private TestProjectRepository testProjectRepository = null;
        private List<Project> projectDomainEntities;

        // Private variables to hold pre-defined mock data
        private Collection<Projects> projectDataContracts;
        private string[] projectsLineItemIds;
        private Collection<ProjectsLineItems> projectLineItemDataContracts;
        private Collection<Ellucian.Colleague.Data.ProjectsAccounting.DataContracts.ProjectsCf> projectsCfDataContracts;
        private GetGlAccountDescriptionResponse glAccountDescriptionResponse;
        private GetPurchasingDocumentIdsResponse purchasingDocumentIdsResponse;

        [TestInitialize]
        public void Initialize()
        {
            this.MockInitialize();

            repository = new ProjectRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);
            BuildGlSourceCodes();
            paGlaTransactions = new Collection<PaGla>();
            testProjectRepository = new TestProjectRepository();
            projectDomainEntities = new List<Project>();

            InitializeMockMethods();
            InitializeMockData();
        }

        [TestCleanup]
        public void Cleanup()
        {
            repository = null;
            GlSourceCodes = null;
            paGlaTransactions = null;
            testProjectRepository = null;
            projectDomainEntities = null;
            projectDataContracts = null;
            projectLineItemDataContracts = null;
            projectsCfDataContracts = null;
            glAccountDescriptionResponse = null;
            this.purchasingDocumentIdsResponse = null;
        }
        #endregion

        #region Project summary
        [TestMethod]
        public async Task GetProjectsAsync_SummaryOnly_HappyPath()
        {
            // Call the repository method to get the specified project
            var actualProjects = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);

            foreach (var projectContract in projectDataContracts)
            {
                var projectEntity = actualProjects.FirstOrDefault(x => x.ProjectId == projectContract.Recordkey);
                Assert.AreEqual(ProjectStatus.Active, projectEntity.Status);
                Assert.AreEqual(projectContract.PrjRefNo, projectEntity.Number);
                Assert.AreEqual(projectContract.PrjTitle, projectEntity.Title);
                Assert.AreEqual(projectContract.PrjType, projectEntity.ProjectType);

                // Make sure the project line item information is correct.
                var lineItemDataContracts = projectLineItemDataContracts.Where(x => x.PrjlnProjectsCf == projectEntity.ProjectId).ToList();
                Assert.AreEqual(lineItemDataContracts.Count(), projectEntity.LineItems.Count);

                // All of the line items on the entity should be expense or revenue.
                Assert.AreEqual(projectEntity.LineItems.Count, projectEntity.LineItems
                    .Where(x => x.GlClass == GlClass.Expense || x.GlClass == GlClass.Revenue).Count());

                // The budget period data for the line items should match what's in the entity.
                foreach (var lineItemContract in lineItemDataContracts)
                {
                    var lineItemEntity = projectEntity.LineItems.FirstOrDefault(x => x.Id == lineItemContract.Recordkey);

                    // The total number of budget periods on the line item contract should be the same as the line item entity.
                    Assert.AreEqual(lineItemContract.PrjlnBudgetEntityAssociation.Count, lineItemEntity.BudgetPeriodAmounts.Count);

                    foreach (var budgetPeriodAssoc in lineItemContract.PrjlnBudgetEntityAssociation)
                    {
                        var budgetPeriodEntity = lineItemEntity.BudgetPeriodAmounts.FirstOrDefault(x => x.SequenceNumber == budgetPeriodAssoc.PrjlnPeriodSeqNosAssocMember);
                        Assert.AreEqual(budgetPeriodAssoc.PrjlnBudgetAmtsAssocMember, budgetPeriodEntity.BudgetAmount);
                        Assert.AreEqual(budgetPeriodAssoc.PrjlnRequisitionMemosAssocMember, budgetPeriodEntity.RequisitionAmount);
                        Assert.AreEqual(budgetPeriodAssoc.PrjlnEncumbranceMemosAssocMember, budgetPeriodEntity.MemoEncumbranceAmount);
                        Assert.AreEqual(budgetPeriodAssoc.PrjlnEncumbrancePostedAssocMember, budgetPeriodEntity.PostedEncumbranceAmount);
                        Assert.AreEqual(budgetPeriodAssoc.PrjlnActualMemosAssocMember, budgetPeriodEntity.MemoActualsAmount);
                        Assert.AreEqual(budgetPeriodAssoc.PrjlnActualPostedAssocMember, budgetPeriodEntity.PostedActualsAmount);
                    }
                }
            }
        }

        [TestMethod]
        public async Task GetProjectsAsync_SummaryOnly_BudgetPeriodAmountsAreNull()
        {
            foreach (var lineItemContract in projectLineItemDataContracts)
            {
                foreach (var budgetPeriodAssoc in lineItemContract.PrjlnBudgetEntityAssociation)
                {
                    budgetPeriodAssoc.PrjlnBudgetAmtsAssocMember = null;
                    budgetPeriodAssoc.PrjlnRequisitionMemosAssocMember = null;
                    budgetPeriodAssoc.PrjlnEncumbranceMemosAssocMember = null;
                    budgetPeriodAssoc.PrjlnEncumbrancePostedAssocMember = null;
                    budgetPeriodAssoc.PrjlnActualMemosAssocMember = null;
                    budgetPeriodAssoc.PrjlnActualPostedAssocMember = null;
                }
            }

            // Call the repository method to get the specified project
            var actualProjects = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);

            foreach (var dataContract in projectDataContracts)
            {
                var projectEntity = actualProjects.FirstOrDefault(x => x.ProjectId == dataContract.Recordkey);

                // Make sure the project line item information is correct.
                var lineItemDataContracts = projectLineItemDataContracts.Where(x => x.PrjlnProjectsCf == projectEntity.ProjectId);
                Assert.AreEqual(lineItemDataContracts.Count(), projectEntity.LineItems.Count);

                // All of the line items on the entity should be expense or revenue.
                Assert.AreEqual(projectEntity.LineItems.Count, projectEntity.LineItems.Where(x => x.GlClass == GlClass.Expense || x.GlClass == GlClass.Revenue).Count());

                // The budget period data for the line items should match what's in the entity.
                foreach (var lineItemContract in lineItemDataContracts)
                {
                    // The total number of budget periods on the line item contract should be the same as the line item entity.
                    var lineItemEntity = projectEntity.LineItems.FirstOrDefault(x => x.Id == lineItemContract.Recordkey);
                    Assert.AreEqual(lineItemContract.PrjlnBudgetEntityAssociation.Count, lineItemEntity.BudgetPeriodAmounts.Count);

                    foreach (var budgetPeriodAssoc in lineItemContract.PrjlnBudgetEntityAssociation)
                    {
                        var budgetPeriodEntity = lineItemEntity.BudgetPeriodAmounts.FirstOrDefault(x => x.SequenceNumber == budgetPeriodAssoc.PrjlnPeriodSeqNosAssocMember);
                        Assert.AreEqual(0, budgetPeriodEntity.BudgetAmount);
                        Assert.AreEqual(0, budgetPeriodEntity.RequisitionAmount);
                        Assert.AreEqual(0, budgetPeriodEntity.MemoEncumbranceAmount);
                        Assert.AreEqual(0, budgetPeriodEntity.PostedEncumbranceAmount);
                        Assert.AreEqual(0, budgetPeriodEntity.MemoActualsAmount);
                        Assert.AreEqual(0, budgetPeriodEntity.PostedActualsAmount);
                    }
                }
            }
        }
        #endregion

        #region Project details - budget periods
        [TestMethod]
        public async Task GetProjectAsync_HappyPath_ProjectBudgetPeriods()
        {
            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            // The project budget periods should match the data from PROJECTS.CF.
            var projectsCfDataContract = this.projectsCfDataContracts.FirstOrDefault(x => x.Recordkey == projectEntity.ProjectId);
            Assert.AreEqual(projectsCfDataContract.PrjcfPeriodsEntityAssociation.Count, projectEntity.BudgetPeriods.Count);

            foreach (var budgetPeriodEntity in projectEntity.BudgetPeriods)
            {
                var budgetPeriodContract = projectsCfDataContract.PrjcfPeriodsEntityAssociation.FirstOrDefault(x =>
                    x.PrjcfPeriodSeqNoAssocMember == budgetPeriodEntity.SequenceNumber);
                Assert.AreEqual(budgetPeriodContract.PrjcfPeriodStartDatesAssocMember, budgetPeriodEntity.StartDate);
                Assert.AreEqual(budgetPeriodContract.PrjcfPeriodEndDatesAssocMember, budgetPeriodEntity.EndDate);
            }
        }
        #endregion

        #region Project details - GL account descriptions
        [TestMethod]
        public async Task GetProjectAsync_HappyPath_GlAccountDescriptions()
        {
            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            foreach (var lineItemEntity in projectEntity.LineItems)
            {
                // Each line item should have the same number of GL accounts at the line item data contract.
                var lineItemContract = this.projectLineItemDataContracts.FirstOrDefault(x => x.Recordkey == lineItemEntity.Id);
                Assert.AreEqual(lineItemContract.PrjlnGlAccts.Count, lineItemEntity.GlAccounts.Count);

                foreach (var glAccountEntity in lineItemEntity.GlAccounts)
                {
                    var glIndex = this.glAccountDescriptionResponse.GlAccountIds.IndexOf(glAccountEntity.GlAccountNumber);
                    Assert.AreEqual(this.glAccountDescriptionResponse.GlDescriptions[glIndex], glAccountEntity.GlAccountDescription);
                }
            }
        }
        #endregion

        #region Project details - GL transactions
        [TestMethod]
        public async Task GetProjectAsync_HappyPath_GlTransactions_NoSequenceNumber()
        {
            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            var projectTransactionEntities = projectEntity.LineItems.SelectMany(y => y.GlAccounts.SelectMany(z => z.ProjectTransactions)).ToList();
            foreach (var transactionEntity in projectTransactionEntities)
            {
                var transactionContract = this.paGlaTransactions.FirstOrDefault(x => x.Recordkey == transactionEntity.Id);
                Assert.AreEqual(transactionContract.PaGlaDebit - transactionContract.PaGlaCredit, transactionEntity.Amount);
                Assert.AreEqual(transactionContract.PaGlaDescription, transactionEntity.Description);
                Assert.AreEqual(transactionContract.PaGlaGlNo, transactionEntity.GlAccount);
                Assert.AreEqual(transactionContract.PaGlaPrjItemId, transactionEntity.ProjectLineItemId);
                Assert.AreEqual(transactionContract.PaGlaRefNo, transactionEntity.ReferenceNumber);
                Assert.AreEqual(transactionContract.PaGlaSource, transactionEntity.Source);
                Assert.AreEqual(transactionContract.PaGlaTrDate, transactionEntity.TransactionDate);
            }
        }

        [TestMethod]
        public async Task GetProjectAsync_HappyPath_GlTransactions_WithSequenceNumber()
        {
            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, "1");

            var projectTransactionEntities = projectEntity.LineItems.SelectMany(y => y.GlAccounts.SelectMany(z => z.ProjectTransactions)).ToList();
            foreach (var transactionEntity in projectTransactionEntities)
            {
                var transactionContract = this.paGlaTransactions.FirstOrDefault(x => x.Recordkey == transactionEntity.Id);
                Assert.AreEqual(transactionContract.PaGlaDebit - transactionContract.PaGlaCredit, transactionEntity.Amount);
                Assert.AreEqual(transactionContract.PaGlaDescription, transactionEntity.Description);
                Assert.AreEqual(transactionContract.PaGlaGlNo, transactionEntity.GlAccount);
                Assert.AreEqual(transactionContract.PaGlaPrjItemId, transactionEntity.ProjectLineItemId);
                Assert.AreEqual(transactionContract.PaGlaRefNo, transactionEntity.ReferenceNumber);
                Assert.AreEqual(transactionContract.PaGlaSource, transactionEntity.Source);
                Assert.AreEqual(transactionContract.PaGlaTrDate, transactionEntity.TransactionDate);
            }
        }

        [TestMethod]
        public async Task GetProjectAsync_HappyPath_GlTransactions_WithZeroDollarTransactionAmounts()
        {
            foreach (var transactionContract in this.paGlaTransactions)
            {
                transactionContract.PaGlaDebit = 0m;
                transactionContract.PaGlaCredit = 0m;
            }
            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            // There should be zero encumbrance transactions.
            var encumbranceTransactionEntities = projectEntity.LineItems.SelectMany(y => y.GlAccounts.SelectMany(z => z.ProjectTransactions))
                .Where(x => x.GlTransactionType == GlTransactionType.Encumbrance || x.GlTransactionType == GlTransactionType.Requisition).ToList();
            Assert.AreEqual(0, encumbranceTransactionEntities.Count);

            // All of the actual transactions should have amounts of $0.
            var actualTransactionEntities = projectEntity.LineItems.SelectMany(y => y.GlAccounts.SelectMany(z => z.ProjectTransactions))
                .Where(x => x.GlTransactionType == GlTransactionType.Actual).ToList();
            foreach (var actualTransactionEntity in actualTransactionEntities)
            {
                Assert.AreEqual(0, actualTransactionEntity.Amount);
            }
        }

        [TestMethod]
        public async Task GetProjectAsync_HappyPath_GlTransactions_WithNullDollarTransactionAmounts()
        {
            foreach (var transactionContract in this.paGlaTransactions)
            {
                transactionContract.PaGlaDebit = null;
                transactionContract.PaGlaCredit = null;
            }
            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            // There should be zero encumbrance transactions.
            var encumbranceTransactionEntities = projectEntity.LineItems.SelectMany(y => y.GlAccounts.SelectMany(z => z.ProjectTransactions))
                .Where(x => x.GlTransactionType == GlTransactionType.Encumbrance || x.GlTransactionType == GlTransactionType.Requisition).ToList();
            Assert.AreEqual(0, encumbranceTransactionEntities.Count);

            // All of the actual transactions should have amounts of $0.
            var actualTransactionEntities = projectEntity.LineItems.SelectMany(y => y.GlAccounts.SelectMany(z => z.ProjectTransactions))
                .Where(x => x.GlTransactionType == GlTransactionType.Actual).ToList();
            foreach (var actualTransactionEntity in actualTransactionEntities)
            {
                Assert.AreEqual(0, actualTransactionEntity.Amount);
            }
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_GlTransactionsHaveNullDebitAmounts()
        {
            foreach (var transactionContract in this.paGlaTransactions)
            {
                transactionContract.PaGlaDebit = null;
            }

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            var projectTransactionEntities = projectEntity.LineItems.SelectMany(y => y.GlAccounts.SelectMany(z => z.ProjectTransactions)).ToList();
            foreach (var transactionEntity in projectTransactionEntities)
            {
                var transactionContract = this.paGlaTransactions.FirstOrDefault(x => x.Recordkey == transactionEntity.Id);
                Assert.AreEqual(transactionContract.PaGlaCredit * -1, transactionEntity.Amount);
            }
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_GlTransactionsHaveNullCreditAmounts()
        {
            foreach (var transactionContract in this.paGlaTransactions)
            {
                transactionContract.PaGlaCredit = null;
            }

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            var lineItemEntities = projectEntity.LineItems;
            foreach (var lineItemEntity in lineItemEntities)
            {
                foreach (var glAccountEntity in lineItemEntity.GlAccounts)
                {
                    var transactionContractsForGlAccount = this.paGlaTransactions.Where(x => x.PaGlaGlNo == glAccountEntity.GlAccountNumber)
                        .Where(x => x.PaGlaPrjItemId == lineItemEntity.Id);
                    var encumbranceTransactionContracts = transactionContractsForGlAccount
                        .Where(x => x.PaGlaSource == "EP" || x.PaGlaSource == "ER").ToList();
                    var actualTransactionContracts = transactionContractsForGlAccount.Where(x => x.PaGlaSource == "PJ").ToList();

                    // The encumbrance transactions should sum to one amount and match the encumbrance amount in the domain.
                    Assert.AreEqual(encumbranceTransactionContracts.Sum(x => x.PaGlaDebit ?? 0), glAccountEntity.Encumbrances);

                    // The actual amount of each contract should match each transaction in the domain.
                    Assert.AreEqual(actualTransactionContracts.Sum(x => x.PaGlaDebit ?? 0), glAccountEntity.Actuals);
                }
            }
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_GlTransactionsHaveNullDates()
        {
            foreach (var transactionContract in this.paGlaTransactions)
            {
                transactionContract.PaGlaTrDate = null;
            }

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            var projectTransactionEntities = projectEntity.LineItems.SelectMany(y => y.GlAccounts.SelectMany(z => z.ProjectTransactions)).ToList();
            Assert.AreEqual(0, projectTransactionEntities.Count);
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_GlTransactionsAreNotActualsEncumbrancesOrRequisitions()
        {
            foreach (var transactionContract in this.paGlaTransactions)
            {
                transactionContract.PaGlaSource = "AB";
            }

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            var projectTransactionEntities = projectEntity.LineItems.SelectMany(y => y.GlAccounts.SelectMany(z => z.ProjectTransactions)).ToList();
            Assert.AreEqual(0, projectTransactionEntities.Count);
        }
        #endregion

        #region Project details - Document IDs
        [TestMethod]
        public async Task GetProjectAsync_Detail_TransactionDocumentIdsAssignedCorrectly()
        {
            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            // Make sure each of the transactions gets assigned the appropriate document ID
            foreach (var lineItem in projectEntity.LineItems)
            {
                foreach (var glAccount in lineItem.GlAccounts)
                {
                    foreach (var transaction in glAccount.ProjectTransactions)
                    {
                        // Check requisition transactions
                        if (transaction.GlTransactionType == GlTransactionType.Requisition)
                        {
                            for (int i = 0; i < this.purchasingDocumentIdsResponse.ReqNumbers.Count(); i++)
                            {
                                if (this.purchasingDocumentIdsResponse.ReqNumbers[i] == transaction.ReferenceNumber)
                                {
                                    Assert.AreEqual(this.purchasingDocumentIdsResponse.ReqIds[i], transaction.DocumentId, "The requisition ID from the CTX should be saved in the transaction.");
                                }
                            }
                        }

                        // Check encumbrance transactions
                        else
                        {
                            if (transaction.ReferenceNumber[0].ToString().ToUpper() == "P")
                            {
                                for (int i = 0; i < this.purchasingDocumentIdsResponse.PoNumbers.Count(); i++)
                                {
                                    if (this.purchasingDocumentIdsResponse.PoNumbers[i] == transaction.ReferenceNumber)
                                    {
                                        Assert.AreEqual(this.purchasingDocumentIdsResponse.PoIds[i], transaction.DocumentId, "The PO ID from the CTX should be saved in the transaction.");
                                    }
                                }
                            }
                            else
                            {
                                if (transaction.ReferenceNumber[0].ToString().ToUpper() == "B")
                                {
                                    for (int i = 0; i < this.purchasingDocumentIdsResponse.BpoNumbers.Count(); i++)
                                    {
                                        if (this.purchasingDocumentIdsResponse.BpoNumbers[i] == transaction.ReferenceNumber)
                                        {
                                            Assert.AreEqual(this.purchasingDocumentIdsResponse.BpoIds[i], transaction.DocumentId, "The BPO ID from the CTX should be saved in the transaction.");
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_NullDocumentIdsResponse()
        {
            this.purchasingDocumentIdsResponse = null;

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            // All of the encumbrance transactions should have null/empty document IDs.
            var encumbranceTransactionEntities = projectEntity.LineItems.SelectMany(x => x.GlAccounts.SelectMany(y => y.ProjectTransactions))
                .Where(x => !string.IsNullOrEmpty(x.DocumentId)).ToList();
            Assert.AreEqual(0, encumbranceTransactionEntities.Count);
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_NullReqNumbers()
        {
            this.purchasingDocumentIdsResponse.ReqNumbers = null;

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            // All of the encumbrance transactions should have null/empty document IDs.
            var requisitionTransactionEntities = projectEntity.LineItems.SelectMany(x => x.GlAccounts.SelectMany(y => y.ProjectTransactions))
                .Where(x => x.GlTransactionType == GlTransactionType.Requisition)
                .Where(x => !string.IsNullOrEmpty(x.DocumentId)).ToList();
            Assert.AreEqual(0, requisitionTransactionEntities.Count);
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_EmtpyReqNumbers()
        {
            this.purchasingDocumentIdsResponse.ReqNumbers = new List<string>();

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            // All of the encumbrance transactions should have null/empty document IDs.
            var requisitionTransactionEntities = projectEntity.LineItems.SelectMany(x => x.GlAccounts.SelectMany(y => y.ProjectTransactions))
                .Where(x => x.GlTransactionType == GlTransactionType.Requisition)
                .Where(x => !string.IsNullOrEmpty(x.DocumentId)).ToList();
            Assert.AreEqual(0, requisitionTransactionEntities.Count);
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_NullPoNumbers()
        {
            this.purchasingDocumentIdsResponse.PoNumbers = null;

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            // All of the encumbrance transactions should have null/empty document IDs.
            var poTransactionEntities = projectEntity.LineItems.SelectMany(x => x.GlAccounts.SelectMany(y => y.ProjectTransactions))
                .Where(x => x.GlTransactionType == GlTransactionType.Encumbrance)
                .Where(x => x.ReferenceNumber[0].ToString().ToUpper() == "P" && !string.IsNullOrEmpty(x.DocumentId)).ToList();

            Assert.AreEqual(0, poTransactionEntities.Count);
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_EmptyPoNumbers()
        {
            this.purchasingDocumentIdsResponse.PoNumbers = new List<string>();

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            // All of the encumbrance transactions should have null/empty document IDs.
            var poTransactionEntities = projectEntity.LineItems.SelectMany(x => x.GlAccounts.SelectMany(y => y.ProjectTransactions))
                .Where(x => x.GlTransactionType == GlTransactionType.Encumbrance)
                .Where(x => x.ReferenceNumber[0].ToString().ToUpper() == "P" && !string.IsNullOrEmpty(x.DocumentId)).ToList();

            Assert.AreEqual(0, poTransactionEntities.Count);
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_NullBpoNumbers()
        {
            this.purchasingDocumentIdsResponse.BpoNumbers = null;

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            // All of the encumbrance transactions should have null/empty document IDs.
            var bpoTransactionEntities = projectEntity.LineItems.SelectMany(x => x.GlAccounts.SelectMany(y => y.ProjectTransactions))
                .Where(x => x.GlTransactionType == GlTransactionType.Encumbrance)
                .Where(x => x.ReferenceNumber[0].ToString().ToUpper() == "B" && !string.IsNullOrEmpty(x.DocumentId)).ToList();

            Assert.AreEqual(0, bpoTransactionEntities.Count);
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_EmptyBpoNumbers()
        {
            this.purchasingDocumentIdsResponse.BpoNumbers = new List<string>();

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            // All of the encumbrance transactions should have null/empty document IDs.
            var bpoTransactionEntities = projectEntity.LineItems.SelectMany(x => x.GlAccounts.SelectMany(y => y.ProjectTransactions))
                .Where(x => x.GlTransactionType == GlTransactionType.Encumbrance)
                .Where(x => x.ReferenceNumber[0].ToString().ToUpper() == "B" && !string.IsNullOrEmpty(x.DocumentId)).ToList();

            Assert.AreEqual(0, bpoTransactionEntities.Count);
        }
        #endregion

        #region Project status
        [TestMethod]
        public async Task GetProjectsAsync_SummaryOnly_NullStatus()
        {
            foreach (var projectDataContract in projectDataContracts)
            {
                projectDataContract.PrjCurrentStatus = null;
            }

            // Call the repository method to get the specified project
            var actualProjects = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);

            Assert.AreEqual(0, actualProjects.Count());
        }

        [TestMethod]
        public async Task GetProjectsAsync_SummaryOnly_InactiveStatus()
        {
            foreach (var projectDataContract in projectDataContracts)
            {
                projectDataContract.PrjCurrentStatus = "I";
            }

            // Call the repository method to get the specified project
            var actualProjects = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);

            foreach (var dataContract in projectDataContracts)
            {
                var projectEntity = actualProjects.FirstOrDefault(x => x.ProjectId == dataContract.Recordkey);
                Assert.AreEqual(ProjectStatus.Inactive, projectEntity.Status);
            }
        }

        [TestMethod]
        public async Task GetProjectsAsync_SummaryOnly_ClosedStatus()
        {
            foreach (var projectDataContract in projectDataContracts)
            {
                projectDataContract.PrjCurrentStatus = "X";
            }

            // Call the repository method to get the specified project
            var actualProjects = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);

            foreach (var dataContract in projectDataContracts)
            {
                var projectEntity = actualProjects.FirstOrDefault(x => x.ProjectId == dataContract.Recordkey);
                Assert.AreEqual(ProjectStatus.Closed, projectEntity.Status);
            }
        }

        [TestMethod]
        public async Task GetProjectsAsync_SummaryOnly_InvalidStatus()
        {
            foreach (var projectDataContract in projectDataContracts)
            {
                projectDataContract.PrjCurrentStatus = "Z";
            }

            // Call the repository method to get the specified project
            var actualProjects = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);

            // There should be zero projects because they all have invalid statuses.
            Assert.AreEqual(0, actualProjects.Count());
        }
        #endregion

        #region GL Class types
        [TestMethod]
        public async Task GetProjectsAsync_SummaryOnly_AssetLineItems()
        {
            foreach (var lineItemContract in projectLineItemDataContracts)
            {
                lineItemContract.PrjlnGlClassType = "A";
            }

            // Call the repository method to get the specified project
            var actualProjects = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);

            // The line items should be the same since the repository now includes Asset type line items.
            foreach (var projectEntity in actualProjects)
            {
                var lineItemsForProject = this.projectLineItemDataContracts.Where(x => x.PrjlnProjectsCf == projectEntity.ProjectId).ToList();
                Assert.AreEqual(lineItemsForProject.Count, projectEntity.LineItems.Count);
            }
        }

        [TestMethod]
        public async Task GetProjectsAsync_SummaryOnly_RevenueLineItems()
        {
            foreach (var lineItemContract in projectLineItemDataContracts)
            {
                lineItemContract.PrjlnGlClassType = "R";
            }

            // Call the repository method to get the specified project
            var actualProjects = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);

            // The line items should be the same since the repository now includes Revenue type line items.
            foreach (var projectEntity in actualProjects)
            {
                var lineItemsForProject = this.projectLineItemDataContracts.Where(x => x.PrjlnProjectsCf == projectEntity.ProjectId).ToList();
                Assert.AreEqual(lineItemsForProject.Count, projectEntity.LineItems.Count);
            }
        }

        [TestMethod]
        public async Task GetProjectsAsync_SummaryOnly_FundBalanceLineItems()
        {
            foreach (var lineItemContract in projectLineItemDataContracts)
            {
                lineItemContract.PrjlnGlClassType = "F";
            }

            // Call the repository method to get the specified project
            var actualProjects = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);

            // The line items should be the same since the repository now includes Fund Balance type line items.
            foreach (var projectEntity in actualProjects)
            {
                var lineItemsForProject = this.projectLineItemDataContracts.Where(x => x.PrjlnProjectsCf == projectEntity.ProjectId).ToList();
                Assert.AreEqual(lineItemsForProject.Count, projectEntity.LineItems.Count);
            }
        }

        [TestMethod]
        public async Task GetProjectsAsync_SummaryOnly_LiabilityLineItems()
        {
            foreach (var lineItemContract in projectLineItemDataContracts)
            {
                lineItemContract.PrjlnGlClassType = "L";
            }

            // Call the repository method to get the specified project
            var actualProjects = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);

            // The line items should be the same since the repository now includes Fund Balance type line items.
            foreach (var projectEntity in actualProjects)
            {
                var lineItemsForProject = this.projectLineItemDataContracts.Where(x => x.PrjlnProjectsCf == projectEntity.ProjectId).ToList();
                Assert.AreEqual(lineItemsForProject.Count, projectEntity.LineItems.Count);

            }
        }

        [TestMethod]
        public async Task GetProjectsAsync_SummaryOnly_InvalidGlClass()
        {
            foreach (var lineItemContract in projectLineItemDataContracts)
            {
                lineItemContract.PrjlnGlClassType = "Z";
            }

            // Call the repository method to get the specified project
            var actualProjects = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);

            // There should be zero line items because they each have an invalid GL Class.
            foreach (var projectEntity in actualProjects)
            {
                Assert.AreEqual(0, projectEntity.LineItems.Count);
            }
        }
        #endregion

        #region Error checking on method arguments
        [TestMethod]
        public async Task GetProjectsAsync_NullListOfProjectIds()
        {
            var actualProjects = await repository.GetProjectsAsync(null, true, null);
            Assert.AreEqual(0, actualProjects.Count());
        }

        [TestMethod]
        public async Task GetProjectsAsync_EmptyListOfProjectIds()
        {
            var actualProjects = await repository.GetProjectsAsync(new List<string>(), true, null);
            Assert.AreEqual(0, actualProjects.Count());
        }
        #endregion

        #region Generic error checking
        [TestMethod]
        public async Task GetProjectsAsync_SummaryOnly_NullListReturnedFromDataReader()
        {
            this.projectDataContracts = null;

            // Call the repository method to get the specified project
            var projectEntities = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);

            Assert.AreEqual(0, projectEntities.Count());
        }

        [TestMethod]
        public async Task GetProjectsAsync_SummaryOnly_NullProjectReturnedFromDataReader()
        {
            this.projectDataContracts.Add(null);

            // Call the repository method to get the specified project
            var projectEntities = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);

            Assert.AreEqual(this.projectDataContracts.Count - 1, projectEntities.Count());
        }

        [TestMethod]
        public async Task GetProjectsAsync_NullStatus()
        {
            projectDataContracts[0].PrjCurrentStatus = null;

            var actualProjects = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);

            Assert.AreEqual(projectDataContracts.Count - 1, actualProjects.Count());
        }

        [TestMethod]
        public async Task GetProjectsAsync_EmptyStatus()
        {
            projectDataContracts[0].PrjCurrentStatus = "";
            var actualProjects = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);
            Assert.AreEqual(projectDataContracts.Count - 1, actualProjects.Count());
        }

        [TestMethod]
        public async Task GetProjectsAsync_BulkReadReturnsNullLineItems()
        {
            this.projectLineItemDataContracts = null;
            var projectEntities = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);

            foreach (var projectEntity in projectEntities)
            {
                Assert.AreEqual(0, projectEntity.LineItems.Count);
            }
        }

        [TestMethod]
        public async Task GetProjectAsync_OneLineItemContractIsNull()
        {
            this.projectLineItemDataContracts[0] = null;
            var projectEntities = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, true, null);

            foreach (var projectEntity in projectEntities)
            {
                Assert.AreEqual(this.projectLineItemDataContracts.Where(x => x != null && x.PrjlnProjectsCf == projectEntity.ProjectId).Count(), projectEntity.LineItems.Count);
            }
        }

        [TestMethod]
        public async Task GetProjectAsync_NullProjectId()
        {
            string expectedParam = "projectId";
            string actualParam = "";
            try
            {
                await repository.GetProjectAsync(null, true, null);
            }
            catch (ArgumentNullException anex)
            {
                actualParam = anex.ParamName;
            }
            Assert.AreEqual(expectedParam, actualParam);
        }

        [TestMethod]
        public async Task GetProjectAsync_NullGlSourceCodes()
        {
            this.GlSourceCodes = null;
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            var glAccountEntities = projectEntity.LineItems.SelectMany(x => x.GlAccounts).ToList();
            foreach (var glAccountEntity in glAccountEntities)
            {
                Assert.AreEqual(0, glAccountEntity.ProjectTransactions.Count());
            }
        }

        [TestMethod]
        public async Task GetProjectsAsync_ProjectsCfContractIsNull()
        {
            projectsCfDataContracts = null;

            // Call the repository method to get the specified project
            var projectEntities = await repository.GetProjectsAsync(new List<string>() { "8", "9" }, false, null);

            foreach (var projectEntity in projectEntities)
            {
                Assert.AreEqual(0, projectEntity.BudgetPeriods.Count);
            }
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_BudgetPeriodAssociationIsNull()
        {
            foreach (var projectsCfDataContract in projectsCfDataContracts)
            {
                projectsCfDataContract.PrjcfPeriodsEntityAssociation = null;
            }

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8" , false, null);

            Assert.AreEqual(0, projectEntity.BudgetPeriods.Count);
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_BudgetPeriodHasNullStartDate()
        {
            foreach (var projectsCfDataContract in projectsCfDataContracts)
            {
                projectsCfDataContract.PrjcfPeriodsEntityAssociation[0].PrjcfPeriodStartDatesAssocMember = null;
            }

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);
            Assert.IsNull(projectEntity);
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_BudgetPeriodHasNullEndDate()
        {
            foreach (var projectsCfDataContract in projectsCfDataContracts)
            {
                projectsCfDataContract.PrjcfPeriodsEntityAssociation[0].PrjcfPeriodStartDatesAssocMember = null;
            }

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);
            Assert.IsNull(projectEntity);
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_HappyPath_GlAccountDescriptionResponseIsNull()
        {
            this.glAccountDescriptionResponse = null;

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            var glAccountCount = projectEntity.LineItems.SelectMany(y => y.GlAccounts).Count();
            Assert.AreEqual(0, glAccountCount);
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_PaGlaTransactionsHaveNullReferenceNumber()
        {
            foreach (var transaction in this.paGlaTransactions)
            {
                transaction.PaGlaRefNo = null;
            }

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            var glAccounts = projectEntity.LineItems.SelectMany(y => y.GlAccounts).ToList();

            foreach (var glAccount in glAccounts)
            {
                Assert.AreEqual(0, glAccount.ProjectTransactions.Count);
            }
        }

        [TestMethod]
        public async Task GetProjectAsync_Detail_PaGlaTransactionsHaveEmptyReferenceNumber()
        {
            foreach (var transaction in this.paGlaTransactions)
            {
                transaction.PaGlaRefNo = "";
            }

            // Call the repository method to get the specified project
            var projectEntity = await repository.GetProjectAsync("8", false, null);

            var glAccounts = projectEntity.LineItems.SelectMany(y => y.GlAccounts).ToList();

            foreach (var glAccount in glAccounts)
            {
                Assert.AreEqual(0, glAccount.ProjectTransactions.Count);
            }
        }
        #endregion

        #region Private methods
        private void InitializeMockMethods()
        {
            projectDataContracts = new Collection<Projects>();
            projectsLineItemIds = new string[] { };
            projectLineItemDataContracts = new Collection<ProjectsLineItems>();
            projectsCfDataContracts = new Collection<Ellucian.Colleague.Data.ProjectsAccounting.DataContracts.ProjectsCf>();

            // Mock BulkReadRecord to return a list of Projects data contracts
            dataReaderMock.Setup(acc => acc.BulkReadRecordAsync<Projects>(It.IsAny<string[]>(), true)).Returns<string[], bool>(
                (projectIds, replaceTextVMs) =>
                {
                    var projectsToReturn = new Collection<Projects>();

                    if (this.projectDataContracts == null)
                    {
                        return Task.FromResult(this.projectDataContracts);
                    }

                    var projectsToConvert = this.projectDataContracts.Where(x => x != null && projectIds.Contains(x.Recordkey)).ToList();
                    foreach (var project in projectsToConvert)
                    {
                        projectsToReturn.Add(project);
                    }

                    return Task.FromResult(projectsToReturn);
                });

            // Mock up the Select method to return an empty string array when selecting IDs for PROJECTS.LINE.ITEMS. The array can be empty because the bulk read call
            // that utilizes the array is mocked up to return exactly what we want, so effectively it doesn't matter what data is returned here.
            dataReaderMock.Setup(acc => acc.SelectAsync("PROJECTS.LINE.ITEMS", It.IsAny<string>(), It.IsAny<string[]>(), "?", true, It.IsAny<int>())).Returns(() =>
                {
                    return Task.FromResult(this.projectsLineItemIds);
                });

            // Mock BulkReadRecord to return a list of ProjectsLineItems data contracts
            dataReaderMock.Setup(acc => acc.BulkReadRecordAsync<ProjectsLineItems>(It.IsAny<string[]>(), true)).Returns(() =>
                {
                    return Task.FromResult(this.projectLineItemDataContracts);
                });

            // Mock ReadRecord to return a list of ProjectsCf data contracts
            dataReaderMock.Setup(acc => acc.BulkReadRecordAsync<Ellucian.Colleague.Data.ProjectsAccounting.DataContracts.ProjectsCf>(It.IsAny<string[]>(), true)).Returns(() =>
                {
                    return Task.FromResult(this.projectsCfDataContracts);
                });

            // Mock Execute within the transaction invoker to return a GetGlAccountDescriptionResponse object
            transManagerMock.Setup(tio => tio.ExecuteAsync<GetGlAccountDescriptionRequest, GetGlAccountDescriptionResponse>(It.IsAny<GetGlAccountDescriptionRequest>())).Returns(() =>
                {
                    return Task.FromResult(this.glAccountDescriptionResponse);
                });

            // Mock up the Select method to return an empty string array when selecting IDs for PA.GLA. The array can be empty because the bulk read call
            // that utilizes the array is mocked up to return exactly what we want, so effectively it doesn't matter what data is returned here.
            dataReaderMock.Setup(acc => acc.SelectAsync("PA.GLA", It.IsAny<string>(), It.IsAny<string[]>(), "?", true, It.IsAny<int>())).Returns(() =>
                {
                    return Task.FromResult(new String[0]);
                });

            // Mock the BulkReadRecord moethod to return a list of PaGla transactions
            dataReaderMock.Setup(acc => acc.BulkReadRecordAsync<PaGla>(It.IsAny<string[]>(), true)).Returns(() =>
                {
                    return Task.FromResult(paGlaTransactions);
                });

            // Mock Execute within the transaction invoker to return a GetPurchasingDocumentIdsResponse object
            transManagerMock.Setup(tio => tio.ExecuteAsync<GetPurchasingDocumentIdsRequest, GetPurchasingDocumentIdsResponse>(It.IsAny<GetPurchasingDocumentIdsRequest>())).Returns(() =>
                {
                    return Task.FromResult(this.purchasingDocumentIdsResponse);
                });

            // Mock the ReadRecord method to return an ApplValcodes data contract.
            dataReaderMock.Setup<Task<ApplValcodes>>(acc => acc.ReadRecordAsync<ApplValcodes>("CF.VALCODES", It.IsAny<string>(), true)).Returns(() =>
                {
                    return Task.FromResult(GlSourceCodes);
                });
        }

        private void InitializeMockData()
        {
            #region Project #1
            projectDataContracts.Add(new Projects()
            {
                Recordkey = "8",
                PrjRefNo = "GTT-007",
                PrjTitle = "Gary's Bond Project",
                PrjCurrentStatus = "A",
                PrjType = "R"
            });

            ProjectsLineItems tempLineItem1 = new ProjectsLineItems()
            {
                Recordkey = "20",
                PrjlnGlClassType = "E",
                PrjlnProjectsCf = "8",
                PrjlnProjectItemCode = "C",
                PrjlnPeriodSeqNos = new List<string>() { "1", "2", "3" },
                PrjlnBudgetAmts = new List<decimal?>() { 1000000, 1000000, 1000000 },
                PrjlnActualMemos = new List<decimal?>() { 100, 100, 100 },
                PrjlnActualPosted = new List<decimal?>() { 200, 200, 200 },
                PrjlnEncumbranceMemos = new List<decimal?>() { 300, 300, 300 },
                PrjlnEncumbrancePosted = new List<decimal?>() { 400, 400, 400 },
                PrjlnRequisitionMemos = new List<decimal?>() { 500, 500, 500 },
                PrjlnGlAccts = new List<string>() { "10_00_01_02_20601_51001", "10_00_01_02_20601_51002", "10_00_01_02_20601_51003" }
            };
            tempLineItem1.buildAssociations();
            projectLineItemDataContracts.Add(tempLineItem1);

            ProjectsLineItems tempLineItem2 = new ProjectsLineItems()
            {
                Recordkey = "21",
                PrjlnGlClassType = "R",
                PrjlnProjectsCf = "8",
                PrjlnProjectItemCode = "C",
                PrjlnPeriodSeqNos = new List<string>() { "1", "2", "3" },
                PrjlnBudgetAmts = new List<decimal?>() { 1000000, 1000000, 1000000 },
                PrjlnActualMemos = new List<decimal?>() { 100, 100, 100 },
                PrjlnActualPosted = new List<decimal?>() { 200, 200, 200 },
                PrjlnEncumbranceMemos = new List<decimal?>() { 300, 300, 300 },
                PrjlnEncumbrancePosted = new List<decimal?>() { 400, 400, 400 },
                PrjlnRequisitionMemos = new List<decimal?>() { 500, 500, 500 },
                PrjlnGlAccts = new List<string>() { "10_00_01_02_20601_41001", "10_00_01_02_20601_41002", "10_00_01_02_20601_41003" }
            };
            tempLineItem2.buildAssociations();
            projectLineItemDataContracts.Add(tempLineItem2);

            var projectsCfContract = new Ellucian.Colleague.Data.ProjectsAccounting.DataContracts.ProjectsCf()
            {
                Recordkey = "8",
                PrjcfPeriodSeqNo = new List<string>() { "1", "2", "3" },
                PrjcfPeriodStartDates = new List<DateTime?>() { new DateTime(2013, 1, 1), new DateTime(2014, 1, 1), new DateTime(2014, 1, 1) },
                PrjcfPeriodEndDates = new List<DateTime?>() { new DateTime(2013, 12, 31), new DateTime(2014, 12, 31), new DateTime(2014, 12, 31) }
            };
            projectsCfContract.buildAssociations();
            projectsCfDataContracts.Add(projectsCfContract);
            #endregion

            #region Project #2
            projectDataContracts.Add(new Projects()
            {
                Recordkey = "9",
                PrjRefNo = "GTT-008",
                PrjTitle = "Gary's Bond Plus+1 Project",
                PrjCurrentStatus = "A",
                PrjType = "R"
            });
            tempLineItem1 = new ProjectsLineItems()
            {
                Recordkey = "25",
                PrjlnGlClassType = "E",
                PrjlnProjectsCf = "9",
                PrjlnProjectItemCode = "C",
                PrjlnPeriodSeqNos = new List<string>() { "1", "2", "3" },
                PrjlnBudgetAmts = new List<decimal?>() { 1000000, 1000000, 1000000 },
                PrjlnActualMemos = new List<decimal?>() { 100, 100, 100 },
                PrjlnActualPosted = new List<decimal?>() { 200, 200, 200 },
                PrjlnEncumbranceMemos = new List<decimal?>() { 300, 300, 300 },
                PrjlnEncumbrancePosted = new List<decimal?>() { 400, 400, 400 },
                PrjlnRequisitionMemos = new List<decimal?>() { 500, 500, 500 },
                PrjlnGlAccts = new List<string>() { "10_00_01_02_20601_51001", "10_00_01_02_20601_51002", "10_00_01_02_20601_51003" }
            };
            tempLineItem1.buildAssociations();
            projectLineItemDataContracts.Add(tempLineItem1);

            tempLineItem2 = new ProjectsLineItems()
            {
                Recordkey = "26",
                PrjlnGlClassType = "R",
                PrjlnProjectsCf = "9",
                PrjlnProjectItemCode = "C",
                PrjlnPeriodSeqNos = new List<string>() { "1", "2", "3" },
                PrjlnBudgetAmts = new List<decimal?>() { 1000000, 1000000, 1000000 },
                PrjlnActualMemos = new List<decimal?>() { 100, 100, 100 },
                PrjlnActualPosted = new List<decimal?>() { 200, 200, 200 },
                PrjlnEncumbranceMemos = new List<decimal?>() { 300, 300, 300 },
                PrjlnEncumbrancePosted = new List<decimal?>() { 400, 400, 400 },
                PrjlnRequisitionMemos = new List<decimal?>() { 500, 500, 500 },
                PrjlnGlAccts = new List<string>() { "10_00_01_02_20601_41001", "10_00_01_02_20601_41002", "10_00_01_02_20601_41003" }
            };
            tempLineItem2.buildAssociations();
            projectLineItemDataContracts.Add(tempLineItem2);

            projectsCfContract = new Ellucian.Colleague.Data.ProjectsAccounting.DataContracts.ProjectsCf()
            {
                Recordkey = "9",
                PrjcfPeriodSeqNo = new List<string>() { "1", "2", "3" },
                PrjcfPeriodStartDates = new List<DateTime?>() { new DateTime(2013, 1, 1), new DateTime(2014, 1, 1), new DateTime(2014, 1, 1) },
                PrjcfPeriodEndDates = new List<DateTime?>() { new DateTime(2013, 12, 31), new DateTime(2014, 12, 31), new DateTime(2014, 12, 31) }
            };
            projectsCfContract.buildAssociations();
            projectsCfDataContracts.Add(projectsCfContract);
            #endregion

            var allGlNumbers = new Dictionary<string, string>()
            {
                { "10_00_01_02_20601_51001", "Operating Fund : Main Campus" },
                { "10_00_01_02_20601_51002", "Operating Fund : North Campus" },
                { "10_00_01_02_20601_51003", "Operating Fund : South Campus" },
                { "10_00_01_02_20601_51004", "Operating Fund : East Campus" },
                { "10_00_01_02_20601_51005", "Operating Fund : West Campus" },
                { "10_00_01_02_20601_41001", "Tuition Full Time : General" },
                { "10_00_01_02_20601_41002", "Tuition Part Time : General" },
                { "10_00_01_02_20601_41003", "Tuition Overload : General" },
                { "10_00_01_02_20601_41004", "Tuition Forfieture : General" },
                { "10_00_01_02_20601_41005", "Tuition Continuing Education : General" },

            };

            this.glAccountDescriptionResponse = new GetGlAccountDescriptionResponse()
            {
                GlAccountIds = allGlNumbers.Keys.ToList(),
                GlDescriptions = allGlNumbers.Values.ToList()
            };

            int x = 1;
            int docId = 1;
            List<string> requisitionReferenceNumbers = new List<string>(),
                purchaseOrderReferenceNumbers = new List<string>(),
                blanketPurchaseOrderReferenceNumbers = new List<string>(),
                requisitionIds = new List<string>(),
                poIds = new List<string>(),
                bpoIds = new List<string>();
            foreach (var projectContract in this.projectDataContracts)
            {
                foreach (var lineItemContract in this.projectLineItemDataContracts)
                {
                    foreach (var glAccount in allGlNumbers)
                    {
                        // Add a requisition transaction for this GL account
                        var source = "ER";
                        var refNo = "R00000" + x.ToString();
                        requisitionReferenceNumbers.Add(refNo);
                        requisitionIds.Add(docId.ToString());
                        AddPaGlaTransaction(x.ToString(), source, glAccount.Key, glAccount.Value, refNo, projectContract.Recordkey, lineItemContract.Recordkey);
                        x++;
                        docId++;

                        // Add a PO transaction for this GL account
                        source = "EP";
                        refNo = "P00000" + x.ToString();
                        purchaseOrderReferenceNumbers.Add(refNo);
                        poIds.Add(docId.ToString());
                        AddPaGlaTransaction(x.ToString(), source, glAccount.Key, glAccount.Value, refNo, projectContract.Recordkey, lineItemContract.Recordkey);
                        x++;
                        docId++;

                        // Add a BPO transaction for this GL account
                        source = "EP";
                        refNo = "B00000" + x.ToString();
                        blanketPurchaseOrderReferenceNumbers.Add(refNo);
                        bpoIds.Add(docId.ToString());
                        AddPaGlaTransaction(x.ToString(), source, glAccount.Key, glAccount.Value, refNo, projectContract.Recordkey, lineItemContract.Recordkey);
                        x++;
                        docId++;

                        // Add a voucher transaction for this GL account
                        source = "PJ";
                        refNo = "V00000" + x.ToString();
                        AddPaGlaTransaction(x.ToString(), source, glAccount.Key, glAccount.Value, refNo, projectContract.Recordkey, lineItemContract.Recordkey);
                        x++;
                    }
                }
            }

            this.purchasingDocumentIdsResponse = new GetPurchasingDocumentIdsResponse()
            {
                ReqNumbers = requisitionReferenceNumbers,
                PoNumbers = purchaseOrderReferenceNumbers,
                BpoNumbers = blanketPurchaseOrderReferenceNumbers,
                ReqIds = requisitionIds,
                PoIds = poIds,
                BpoIds = bpoIds
            };
        }

        private void AddPaGlaTransaction(string recordKey, string source, string glNumber, string glDescription, string referenceNumber, string projectId, string projectLineItemId)
        {
            this.paGlaTransactions.Add(new PaGla()
            {
                Recordkey = recordKey,
                PaGlaSource = source,
                PaGlaDebit = 40000m,
                PaGlaCredit = 40m,
                PaGlaGlNo = glNumber,
                PaGlaDescription = glDescription,
                PaGlaTrDate = new DateTime(2016, 01, 01),
                PaGlaRefNo = referenceNumber,
                PaGlaPrjItemId = projectLineItemId,
                PaGlaProjectId = projectId
            });
        }

        private void BuildGlSourceCodes()
        {
            // Mock up GL.SOURCE.CODES
            GlSourceCodes = new ApplValcodes();
            GlSourceCodes.ValInternalCode = new List<string>()
            {
                "AA",
                "AE",
                "EP",
                "ER",
                "PJ",
                "JE",
                "AB"
            };
            GlSourceCodes.ValActionCode2 = new List<string>()
            {
                "1",
                "3",
                "3",
                "4",
                "1",
                "1",
                "2"
            };
            GlSourceCodes.buildAssociations();
        }
        #endregion
    }
}