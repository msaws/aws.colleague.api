﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Dtos.HumanResources
{
    /// <summary>
    /// Defines an earnings type for wages or leave associated with an employment position
    /// </summary>
    public class EarningsType
    {
        /// <summary>
        /// The database Id
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The earnings type description
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// The category specifies whether earnings are regular, overtime, leave, college work study or miscellaneous
        /// </summary>
        public EarningsCategory Category { get; set; }

        /// <summary>
        /// Is this earnings type active or not?
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// The method specifies whether the earnings are for leave accrued, leave taken, or time not paid
        /// </summary>
        public EarningsMethod Method { get; set; }
    }
}
