﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Dtos.HumanResources
{
    /// <summary>
    /// Information about an Employment Proficiency Level
    /// </summary>
    public class PersonAchievement
    {
        /// <summary>
        /// Unique system Id for this Person Achievement
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Description of the Person Achievement
        /// </summary>
        public string Description { get; set; }

    }
}
