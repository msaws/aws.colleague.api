//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 12/14/2016 11:53:48 AM by user dvcoll-srm
//
//     Type: ENTITY
//     Entity: PERBEN
//     Application: HR
//     Environment: dvcoll
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;
using Ellucian.Data.Colleague;

namespace Ellucian.Colleague.Data.HumanResources.DataContracts
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract(Name = "Perben")]
	[ColleagueDataContract(GeneratedDateTime = "12/14/2016 11:53:48 AM", User = "dvcoll-srm")]
	[EntityDataContract(EntityName = "PERBEN", EntityType = "PHYS")]
	public class Perben : IColleagueGuidEntity
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		/// <summary>
		/// Record Key
		/// </summary>
		[DataMember]
		public string Recordkey { get; set; }
		
		public void setKey(string key)
		{
			Recordkey = key;
		}
	
		/// <summary>
		/// Record GUID
		/// </summary>
		[DataMember(Name = "RecordGuid")]
		public string RecordGuid { get; set; }

		/// <summary>
		/// Record Model Name
		/// </summary>
		[DataMember(Name = "RecordModelName")]
		public string RecordModelName { get; set; }	
		
		/// <summary>
		/// CDD Name: PERBEN.HRP.ID
		/// </summary>
		[DataMember(Order = 0, Name = "PERBEN.HRP.ID")]
		public string PerbenHrpId { get; set; }
		
		/// <summary>
		/// CDD Name: PERBEN.BD.ID
		/// </summary>
		[DataMember(Order = 1, Name = "PERBEN.BD.ID")]
		public string PerbenBdId { get; set; }
		
		/// <summary>
		/// CDD Name: PERBEN.ENROLL.DATE
		/// </summary>
		[DataMember(Order = 4, Name = "PERBEN.ENROLL.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public DateTime? PerbenEnrollDate { get; set; }
		
		/// <summary>
		/// CDD Name: PERBEN.CANCEL.DATE
		/// </summary>
		[DataMember(Order = 8, Name = "PERBEN.CANCEL.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public DateTime? PerbenCancelDate { get; set; }
		
		/// <summary>
		/// CDD Name: ALL.BENEFIT.COSTS
		/// </summary>
		[DataMember(Order = 14, Name = "ALL.BENEFIT.COSTS")]
		public List<string> AllBenefitCosts { get; set; }
		
		/// <summary>
		/// CDD Name: PERBEN.CHANGE.REASONS
		/// </summary>
		[DataMember(Order = 43, Name = "PERBEN.CHANGE.REASONS")]
		public List<string> PerbenChangeReasons { get; set; }
		
		/// <summary>
		/// CDD Name: PERBEN.CHANGE.REASONS.DATES
		/// </summary>
		[DataMember(Order = 44, Name = "PERBEN.CHANGE.REASONS.DATES")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public List<DateTime?> PerbenChangeReasonsDates { get; set; }
		
		/// <summary>
		/// CDD Name: PERBEN.INTG.CONTRIBUTION
		/// </summary>
		[DataMember(Order = 70, Name = "PERBEN.INTG.CONTRIBUTION")]
		public string PerbenIntgContribution { get; set; }
		
		/// <summary>
		/// CDD Name: PERBEN.INTG.COMMITMENT.TYPE
		/// </summary>
		[DataMember(Order = 71, Name = "PERBEN.INTG.COMMITMENT.TYPE")]
		public string PerbenIntgCommitmentType { get; set; }
		
		/// <summary>
		/// CDD Name: PERBEN.INTG.INTERVAL
		/// </summary>
		[DataMember(Order = 73, Name = "PERBEN.INTG.INTERVAL")]
		public int? PerbenIntgInterval { get; set; }
		
		/// <summary>
		/// CDD Name: PERBEN.INTG.MON.PAY.PERIODS
		/// </summary>
		[DataMember(Order = 74, Name = "PERBEN.INTG.MON.PAY.PERIODS")]
		public List<int?> PerbenIntgMonPayPeriods { get; set; }
		
		/// <summary>
		/// Entity assocation member
		/// </summary>
		[DataMember]
		public List<PerbenChgreas> ChgreasEntityAssociation { get; set; }
		
	
		// build up all the Associated objects and add them to the properties
		public void buildAssociations()
		{	
			// EntityAssociation Name: CHGREAS
			
			ChgreasEntityAssociation = new List< PerbenChgreas >();
			if( PerbenChangeReasons != null)
			{
				int numChgreas = PerbenChangeReasons.Count;
				for (int i = 0; i < numChgreas; i++)
				{
					string value0 = "";
					value0 = PerbenChangeReasons[i];

					DateTime? value1 = null;
					if (PerbenChangeReasonsDates != null && i < PerbenChangeReasonsDates.Count)
					{
						value1 = PerbenChangeReasonsDates[i];
					}

					ChgreasEntityAssociation.Add(new PerbenChgreas( value0, value1));
				}
			}
			   
		}
	}
	
	// EntityAssociation classes
	
	[Serializable]
	public class PerbenChgreas
	{
		public string PerbenChangeReasonsAssocMember;	
		public DateTime? PerbenChangeReasonsDatesAssocMember;	
		public PerbenChgreas() {}
		public PerbenChgreas(
			string inPerbenChangeReasons,
			DateTime? inPerbenChangeReasonsDates)
		{
			PerbenChangeReasonsAssocMember = inPerbenChangeReasons;
			PerbenChangeReasonsDatesAssocMember = inPerbenChangeReasonsDates;
		}
	}
}