//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 9/16/2016 2:19:11 PM by user bromney
//
//     Type: ENTITY
//     Entity: PAYCYCLE
//     Application: HR
//     Environment: dvcoll_wstst01
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;
using Ellucian.Data.Colleague;

namespace Ellucian.Colleague.Data.HumanResources.DataContracts
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract(Name = "Paycycle")]
	[ColleagueDataContract(GeneratedDateTime = "9/16/2016 2:19:11 PM", User = "bromney")]
	[EntityDataContract(EntityName = "PAYCYCLE", EntityType = "PHYS")]
	public class Paycycle : IColleagueEntity
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		/// <summary>
		/// Record Key
		/// </summary>
		[DataMember]
		public string Recordkey { get; set; }
		
		public void setKey(string key)
		{
			Recordkey = key;
		}
		
		/// <summary>
		/// CDD Name: PCY.DESC
		/// </summary>
		[DataMember(Order = 0, Name = "PCY.DESC")]
		public string PcyDesc { get; set; }
		
		/// <summary>
		/// CDD Name: PCY.FREQUENCY
		/// </summary>
		[DataMember(Order = 1, Name = "PCY.FREQUENCY")]
		public string PcyFrequency { get; set; }
		
		/// <summary>
		/// CDD Name: PCY.START.DATE
		/// </summary>
		[DataMember(Order = 3, Name = "PCY.START.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public List<DateTime?> PcyStartDate { get; set; }
		
		/// <summary>
		/// CDD Name: PCY.END.DATE
		/// </summary>
		[DataMember(Order = 4, Name = "PCY.END.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public List<DateTime?> PcyEndDate { get; set; }
		
		/// <summary>
		/// CDD Name: PCY.PAYCHECK.DATE
		/// </summary>
		[DataMember(Order = 5, Name = "PCY.PAYCHECK.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public List<DateTime?> PcyPaycheckDate { get; set; }
		
		/// <summary>
		/// CDD Name: PCY.PAYCLASSES
		/// </summary>
		[DataMember(Order = 8, Name = "PCY.PAYCLASSES")]
		public List<string> PcyPayclasses { get; set; }
		
		/// <summary>
		/// CDD Name: PCY.BENDED.PERIODS
		/// </summary>
		[DataMember(Order = 15, Name = "PCY.BENDED.PERIODS")]
		public List<string> PcyBendedPeriods { get; set; }
		
		/// <summary>
		/// CDD Name: PCY.EXCLUDE.EARN.TYPES
		/// </summary>
		[DataMember(Order = 16, Name = "PCY.EXCLUDE.EARN.TYPES")]
		public List<string> PcyExcludeEarnTypes { get; set; }
		
		/// <summary>
		/// CDD Name: PCY.TAKE.BENEFITS
		/// </summary>
		[DataMember(Order = 17, Name = "PCY.TAKE.BENEFITS")]
		public List<string> PcyTakeBenefits { get; set; }
		
		/// <summary>
		/// CDD Name: PCY.PERIOD.STATUS
		/// </summary>
		[DataMember(Order = 18, Name = "PCY.PERIOD.STATUS")]
		public List<string> PcyPeriodStatus { get; set; }
		
		/// <summary>
		/// CDD Name: PCY.WORK.WEEK.START.DY
		/// </summary>
		[DataMember(Order = 21, Name = "PCY.WORK.WEEK.START.DY")]
		public string PcyWorkWeekStartDy { get; set; }
		
		/// <summary>
		/// CDD Name: PCY.WORK.WEEK.START.TM
		/// </summary>
		[DataMember(Order = 22, Name = "PCY.WORK.WEEK.START.TM")]
		[DisplayFormat(DataFormatString = "{0:T}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public DateTime? PcyWorkWeekStartTm { get; set; }
		
		/// <summary>
		/// Entity assocation member
		/// </summary>
		[DataMember]
		public List<PaycyclePayperiods> PayperiodsEntityAssociation { get; set; }
		
	
		// build up all the Associated objects and add them to the properties
		public void buildAssociations()
		{	
			// EntityAssociation Name: PAYPERIODS
			
			PayperiodsEntityAssociation = new List< PaycyclePayperiods >();
			if( PcyStartDate != null)
			{
				int numPayperiods = PcyStartDate.Count;
				for (int i = 0; i < numPayperiods; i++)
				{
					DateTime? value0 = null;
					value0 = PcyStartDate[i];

					DateTime? value1 = null;
					if (PcyEndDate != null && i < PcyEndDate.Count)
					{
						value1 = PcyEndDate[i];
					}

					DateTime? value2 = null;
					if (PcyPaycheckDate != null && i < PcyPaycheckDate.Count)
					{
						value2 = PcyPaycheckDate[i];
					}

					string value3 = "";
					if (PcyBendedPeriods != null && i < PcyBendedPeriods.Count)
					{
						value3 = PcyBendedPeriods[i];
					}

					string value4 = "";
					if (PcyExcludeEarnTypes != null && i < PcyExcludeEarnTypes.Count)
					{
						value4 = PcyExcludeEarnTypes[i];
					}

					string value5 = "";
					if (PcyTakeBenefits != null && i < PcyTakeBenefits.Count)
					{
						value5 = PcyTakeBenefits[i];
					}

					string value6 = "";
					if (PcyPeriodStatus != null && i < PcyPeriodStatus.Count)
					{
						value6 = PcyPeriodStatus[i];
					}

					PayperiodsEntityAssociation.Add(new PaycyclePayperiods( value0, value1, value2, value3, value4, value5, value6));
				}
			}
			   
		}
	}
	
	// EntityAssociation classes
	
	[Serializable]
	public class PaycyclePayperiods
	{
		public DateTime? PcyStartDateAssocMember;	
		public DateTime? PcyEndDateAssocMember;	
		public DateTime? PcyPaycheckDateAssocMember;	
		public string PcyBendedPeriodsAssocMember;	
		public string PcyExcludeEarnTypesAssocMember;	
		public string PcyTakeBenefitsAssocMember;	
		public string PcyPeriodStatusAssocMember;	
		public PaycyclePayperiods() {}
		public PaycyclePayperiods(
			DateTime? inPcyStartDate,
			DateTime? inPcyEndDate,
			DateTime? inPcyPaycheckDate,
			string inPcyBendedPeriods,
			string inPcyExcludeEarnTypes,
			string inPcyTakeBenefits,
			string inPcyPeriodStatus)
		{
			PcyStartDateAssocMember = inPcyStartDate;
			PcyEndDateAssocMember = inPcyEndDate;
			PcyPaycheckDateAssocMember = inPcyPaycheckDate;
			PcyBendedPeriodsAssocMember = inPcyBendedPeriods;
			PcyExcludeEarnTypesAssocMember = inPcyExcludeEarnTypes;
			PcyTakeBenefitsAssocMember = inPcyTakeBenefits;
			PcyPeriodStatusAssocMember = inPcyPeriodStatus;
		}
	}
}