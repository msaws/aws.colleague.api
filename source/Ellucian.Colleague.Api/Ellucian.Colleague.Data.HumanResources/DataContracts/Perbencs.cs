//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by the DSL/T4 Generator - Version 1.1
//     Last generated on 12/13/2016 1:25:09 PM by user dvcoll-srm
//
//     Type: ENTITY
//     Entity: PERBENCS
//     Application: HR
//     Environment: dvcoll
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.CodeDom.Compiler;
using Ellucian.Dmi.Runtime;
using Ellucian.Data.Colleague;

namespace Ellucian.Colleague.Data.HumanResources.DataContracts
{
	[GeneratedCodeAttribute("Colleague Data Contract Generator", "1.1")]
	[DataContract(Name = "Perbencs")]
	[ColleagueDataContract(GeneratedDateTime = "12/13/2016 1:25:09 PM", User = "dvcoll-srm")]
	[EntityDataContract(EntityName = "PERBENCS", EntityType = "PHYS")]
	public class Perbencs : IColleagueEntity
	{
		/// <summary>
		/// Version
		/// </summary>
		[DataMember]
		public int _AppServerVersion { get; set; }

		/// <summary>
		/// Record Key
		/// </summary>
		[DataMember]
		public string Recordkey { get; set; }
		
		public void setKey(string key)
		{
			Recordkey = key;
		}
		
		/// <summary>
		/// CDD Name: PBC.HRP.ID
		/// </summary>
		[DataMember(Order = 0, Name = "PBC.HRP.ID")]
		public string PbcHrpId { get; set; }
		
		/// <summary>
		/// CDD Name: PBC.BD.ID
		/// </summary>
		[DataMember(Order = 1, Name = "PBC.BD.ID")]
		public string PbcBdId { get; set; }
		
		/// <summary>
		/// CDD Name: PBC.START.DATE
		/// </summary>
		[DataMember(Order = 16, Name = "PBC.START.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public DateTime? PbcStartDate { get; set; }
		
		/// <summary>
		/// CDD Name: PBC.END.DATE
		/// </summary>
		[DataMember(Order = 17, Name = "PBC.END.DATE")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public DateTime? PbcEndDate { get; set; }
		
		/// <summary>
		/// CDD Name: PBC.EMPLYE.PAY.COST
		/// </summary>
		[DataMember(Order = 18, Name = "PBC.EMPLYE.PAY.COST")]
		[DisplayFormat(DataFormatString = "{0:N2}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public Decimal? PbcEmplyePayCost { get; set; }
		
		/// <summary>
		/// CDD Name: PBC.EMPLYE.LIMIT.AMT
		/// </summary>
		[DataMember(Order = 26, Name = "PBC.EMPLYE.LIMIT.AMT")]
		[DisplayFormat(DataFormatString = "{0:N2}")]
		[ColleagueDataMember(UseEnvisionInternalFormat = true)]
		public Decimal? PbcEmplyeLimitAmt { get; set; }
		
	
		// build up all the Associated objects and add them to the properties
		public void buildAssociations()
		{	
			   
		}
	}
	
	// EntityAssociation classes
}