﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Data.HumanResources.DataContracts;
using Ellucian.Colleague.Domain.HumanResources.Entities;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Web.Http.Configuration;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.HumanResources.Repositories
{
    /// <summary>
    /// Repository for PersonPositionWage endpoints
    /// </summary>
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class PersonPositionWageRepository : BaseColleagueRepository, IPersonPositionWageRepository
    {
        private readonly int bulkReadSize; 

        public PersonPositionWageRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings settings)
            : base(cacheProvider, transactionFactory, logger)
        {
            bulkReadSize = settings != null && settings.BulkReadSize > 0 ? settings.BulkReadSize : 5000;        
        }


        /// <summary>
        /// Get PersonPositionWages for the given personIds
        /// </summary>
        /// <param name="personIds">a list of personids for whom to get PersonPositionWages</param>
        /// <returns></returns>
        public async Task<IEnumerable<PersonPositionWage>> GetPersonPositionWagesAsync(IEnumerable<string> personIds)
        {
            if (personIds == null)
            {
                throw new ArgumentNullException("personIds");
            }
            if (!personIds.Any())
            {
                throw new ArgumentException("personIds is required to get PersonPositionWages");
            }

            //select all the PERPOSWG ids with the HRP.ID equal to the input personids
            var criteria = "WITH PPWG.HRP.ID EQ ?";
            var perposwgKeys = await DataReader.SelectAsync("PERPOSWG", criteria, personIds.Select(id => string.Format("\"{0}\"", id)).ToArray());
            if (perposwgKeys == null)
            {
                var message = "Unexpected null returned from PERPOSWG SelectAsync";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            if (!perposwgKeys.Any())
            {
                logger.Info("No PERPOSWG keys exist for the given person Ids: " + string.Join(",", personIds));
            }

            //bulkread the records in chunks for all the keys
            var perposwgRecords = new List<Perposwg>();
            for (int i = 0; i < perposwgKeys.Count(); i += bulkReadSize)
            {
                var subList = perposwgKeys.Skip(i).Take(bulkReadSize);
                var records = await DataReader.BulkReadRecordAsync<Perposwg>(subList.ToArray());
                if (records == null)
                {
                    logger.Error("Unexpected null from bulk read of Perposwg records");
                }
                else
                {
                    perposwgRecords.AddRange(records);
                }
            }

            //build the entities
            var personPositionWageEntities = new List<PersonPositionWage>();
            foreach (var perposwgRecord in perposwgRecords)
            {
                if (perposwgRecord != null)
                {
                    try
                    {
                        personPositionWageEntities.Add(BuildPersonPositionWage(perposwgRecord));
                    }
                    catch (Exception e)
                    {
                        LogDataError("Perposwg", perposwgRecord.Recordkey, perposwgRecord, e, e.Message);
                    }
                }
            }

            return personPositionWageEntities;
        }

        /// <summary>
        /// Helper to build a PersonPositionWage object based on a PERPOSWG record
        /// </summary>
        /// <param name="perposwgRecord"></param>
        /// <returns></returns>
        private PersonPositionWage BuildPersonPositionWage(Perposwg perposwgRecord)
        {
            if (perposwgRecord == null)
            {
                throw new ArgumentNullException("perposwgRecord");
            }

            if (!perposwgRecord.PpwgStartDate.HasValue)
            {
                throw new ArgumentException("Perposwg Start Date must have value", "perposwgRecord.PpwgStartDate");
            }

            var fundingSources = new List<PositionFundingSource>();
            foreach (var payItemRecord in perposwgRecord.PpwitemsEntityAssociation)
            {
                var fundingSource = new PositionFundingSource(
                    payItemRecord.PpwgPiFndsrcIdAssocMember,
                    perposwgRecord.PpwitemsEntityAssociation.IndexOf(payItemRecord))
                    {
                        ProjectId = payItemRecord.PpwgProjectsIdsAssocMember
                    };

                fundingSources.Add(fundingSource);
            }

            var personPositionWage = new PersonPositionWage(perposwgRecord.Recordkey,
                perposwgRecord.PpwgHrpId,
                perposwgRecord.PpwgPositionId,
                perposwgRecord.PpwgPerposId,
                perposwgRecord.PpwgPospayId,
                perposwgRecord.PpwgPayclassId,
                perposwgRecord.PpwgPaycycleId,
                perposwgRecord.PpwgBaseEt,
                perposwgRecord.PpwgStartDate.Value)
                {
                    EndDate = perposwgRecord.PpwgEndDate,
                    IsPaySuspended = !string.IsNullOrEmpty(perposwgRecord.PpwgSuspendPayFlag) && perposwgRecord.PpwgSuspendPayFlag.Equals("Y", StringComparison.InvariantCultureIgnoreCase),                    
                    FundingSources = fundingSources
                };

            return personPositionWage;
        }
    }
}
