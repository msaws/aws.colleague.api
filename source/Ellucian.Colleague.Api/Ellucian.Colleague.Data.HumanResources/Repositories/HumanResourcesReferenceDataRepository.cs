﻿using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Data.HumanResources.DataContracts;
//Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Domain.HumanResources.Entities;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.HumanResources.Repositories
{
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class HumanResourcesReferenceDataRepository : BaseColleagueRepository, IHumanResourcesReferenceDataRepository
    {
        /// <summary>
        /// ..ctor
        /// </summary>
        /// <param name="cacheProvider"></param>
        /// <param name="transactionFactory"></param>
        /// <param name="logger"></param>
        public HumanResourcesReferenceDataRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            // Using level 1 cache time out value for data that rarely changes.
            CacheTimeout = Level1CacheTimeoutValue;
        }

        /// <summary>
        /// Get all Deduction type objects, built from database data
        /// </summary>
        /// <returns>A list of Deduction type objects</returns>
        public async Task<IEnumerable<DeductionType>> GetDeductionTypesAsync(bool ignoreCache = false)
        {
            if (ignoreCache)
            {
                return await BuildAllDeductionTypes();
            }
            else 
            {
                return await GetOrAddToCacheAsync<IEnumerable<DeductionType>>("AllDeductionTypes", async () => await this.BuildAllDeductionTypes(), Level1CacheTimeoutValue);
            }
        }

        private async Task<IEnumerable<DeductionType>> BuildAllDeductionTypes()
        {
            var deductionTypeEntities = new List<DeductionType>();
            var deductionTypeIds = await DataReader.SelectAsync("BENDED", "WITH BD.PAYERS = 'E''S'");

            var deductionTypeRecords = await DataReader.BulkReadRecordAsync<DataContracts.Bended>(deductionTypeIds);


            foreach (var deductionTypeRecord in deductionTypeRecords)
            {

                deductionTypeEntities.Add(new DeductionType(deductionTypeRecord.RecordGuid, deductionTypeRecord.Recordkey, deductionTypeRecord.BdDesc));

            }


            return deductionTypeEntities;
        }

        /// <summary>
        /// Get a collection of job change reasons
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of job change reasons</returns>
        public async Task<IEnumerable<JobChangeReason>> GetJobChangeReasonsAsync(bool ignoreCache)
        {
            return await GetGuidValcodeAsync<JobChangeReason>("HR", "POSITION.ENDING.REASONS",
                (e, g) => new JobChangeReason(g, e.ValInternalCodeAssocMember, e.ValExternalRepresentationAssocMember),
                bypassCache: ignoreCache);
        }

        /// <summary>
        /// Get a collection of rehire types
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of rehire types</returns>
        public async Task<IEnumerable<RehireType>> GetRehireTypesAsync(bool ignoreCache)
        {
            return await GetGuidValcodeAsync<RehireType>("HR", "REHIRE.ELIGIBILITY.CODES",
                (e, g) => new RehireType(g, e.ValInternalCodeAssocMember, e.ValExternalRepresentationAssocMember,
                    e.ValActionCode3AssocMember), bypassCache: ignoreCache);
        }

        /// <summary>
        /// Get a collection of employee classifications
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of employee classifications</returns>
        public async Task<IEnumerable<EmploymentClassification>> GetEmploymentClassificationsAsync(bool ignoreCache)
        {
            return await GetGuidValcodeAsync<EmploymentClassification>("HR", "CLASSIFICATIONS",
                (e, g) => new EmploymentClassification(g, e.ValInternalCodeAssocMember, e.ValExternalRepresentationAssocMember,
                    EmploymentClassificationType.Position), bypassCache: ignoreCache);
        }

        /// <summary>
        /// Get a collection of employment proficiencies
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of instructional methods</returns>
        public async Task<IEnumerable<EmploymentProficiency>> GetEmploymentProficienciesAsync(bool ignoreCache)
        {
            return await GetGuidCodeItemAsync<Jobskills, EmploymentProficiency>("AllEmploymentProficiencies", "JOBSKILLS",
                (ep, g) => new EmploymentProficiency(g, ep.Recordkey, ep.JskDesc) { Certification = ep.JskLicenseCert, Comment = ep.JskComment, Authority = ep.JskAuthority }, bypassCache: ignoreCache);
        }

        /// <summary>
        /// Returns employment status ending reasons.
        /// </summary>
        /// <param name="ignoreCache"></param>
        /// <returns></returns>
        public async Task<IEnumerable<EmploymentStatusEndingReason>> GetEmploymentStatusEndingReasonsAsync(bool ignoreCache)
        {
            return await GetGuidValcodeAsync<EmploymentStatusEndingReason>("HR", "STATUS.ENDING.REASONS",
               (e, g) => new EmploymentStatusEndingReason(g, e.ValInternalCodeAssocMember, e.ValExternalRepresentationAssocMember),
               bypassCache: ignoreCache);
        }

        ///// <summary>
        ///// Get a collection of institution job supervisors
        ///// </summary>
        ///// <param name="ignoreCache">Bypass cache flag</param>
        ///// <returns>Collection of institution job supervisors</returns>
        //public async Task<IEnumerable<InstitutionJobSupervisor>> GetInstitutionJobSupervisorsAsync(bool ignoreCache)
        //{
        //    var coreDefaultData = GetDefaults();

        //    return await GetGuidCodeItemAsync<Perpos, InstitutionJobSupervisor>("AllInstitutionJobSupervisors", "PERPOS",
        //        (ijs, g) => new InstitutionJobSupervisor(g, ijs.Recordkey, ijs.RecordModelName, ijs.PerposHrpId, ijs.PerposPositionId) { SupervisorId = ijs.PerposSupervisorHrpId, AlternateSupervisorId = ijs.PerposAltSupervisorId, Employer = coreDefaultData.DefaultHostCorpId }, bypassCache: ignoreCache);
        //}

        /// <summary>
        /// Returns payroll deduction arrangement change reasons.
        /// </summary>
        /// <param name="ignoreCache"></param>
        /// <returns></returns>
        public async Task<IEnumerable<PayrollDeductionArrangementChangeReason>> GetPayrollDeductionArrangementChangeReasonsAsync(bool ignoreCache)
        {
            return await GetGuidValcodeAsync<PayrollDeductionArrangementChangeReason>("HR", "BENDED.CHANGE.REASONS",
               (e, g) => new PayrollDeductionArrangementChangeReason(g, e.ValInternalCodeAssocMember, e.ValExternalRepresentationAssocMember),
               bypassCache: ignoreCache);
        }

        /// <summary>
        /// Returns HR Person statuses used in PERSTAT.STATUS.
        /// </summary>
        /// <param name="ignoreCache"></param>
        /// <returns></returns>
        public async Task<IEnumerable<PersonStatuses>> GetPersonStatusesAsync(bool ignoreCache)
        {
            return await GetGuidValcodeAsync<PersonStatuses>("HR", "HR.STATUSES",
               (e, g) => new PersonStatuses(g, e.ValInternalCodeAssocMember, e.ValExternalRepresentationAssocMember, e.ValActionCode3AssocMember),
               bypassCache: ignoreCache);
        }

        /// <summary>
        /// Get the Defaults from CORE to compare default institution Id
        /// </summary>
        /// <returns>Core Defaults</returns>
        private Base.DataContracts.Defaults GetDefaults()
        {
            return GetOrAddToCache<Data.Base.DataContracts.Defaults>("CoreDefaults",
                () =>
                {
                    var coreDefaults = DataReader.ReadRecord<Data.Base.DataContracts.Defaults>("CORE.PARMS", "DEFAULTS");
                    if (coreDefaults == null)
                    {
                        logger.Info("Unable to access DEFAULTS from CORE.PARMS table.");
                        coreDefaults = new Defaults();
                    }
                    return coreDefaults;
                }, Level1CacheTimeoutValue);
        }
    }
}
