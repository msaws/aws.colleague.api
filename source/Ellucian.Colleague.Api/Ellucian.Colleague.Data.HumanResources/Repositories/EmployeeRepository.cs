﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Data.HumanResources.DataContracts;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.HumanResources.Entities;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Web.Http.Configuration;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos;

namespace Ellucian.Colleague.Data.HumanResources.Repositories
{
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class EmployeeRepository : BaseColleagueRepository, IEmployeeRepository
    {
        private readonly int bulkReadSize;

        public EmployeeRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings settings)
            : base(cacheProvider, transactionFactory, logger)
        {
            bulkReadSize = settings != null && settings.BulkReadSize > 0 ? settings.BulkReadSize : 5000;
        }

        /// <summary>
        /// Get Employees objects for all employees bypassing cache and reading directly from the database.
        /// </summary>
        /// <param name="offset">Offset for record index on page reads.</param>
        /// <param name="limit">Take number of records on page reads.</param>
        /// <param name="person">Person id filter.</param>
        /// <param name="campus">Primary campus or location filter.</param>
        /// <param name="status">Status ("active", "terminated", or "leave") filter.</param>
        /// <param name="startOn">Start on a specific date filter.</param>
        /// <param name="endOn">End on a specific date filter.</param>
        /// <param name="rehireableStatusEligibility">Rehireable status ("eligible" or "ineligible") filter.</param>
        /// <returns>Tuple of Employee Entity objects <see cref="Employee"/> and a count for paging.</returns>
        public async Task<Tuple<IEnumerable<Ellucian.Colleague.Domain.HumanResources.Entities.Employee>, int>> GetEmployeesAsync(int offset, int limit, string person = "",
            string campus = "", string status = "", string startOn = "", string endOn = "", string rehireableStatusEligibility = "", string rehireableStatusType = "")
        {
            try
            {
                // Read Colleague-specified default values for:
                // - list of benefits to exclude for consideration of employee benefit status
                // - list of HR.STATUSES indication a leave status.
                var ldmDefaults = DataReader.ReadRecord<LdmDefaults>("CORE.PARMS", "LDM.DEFAULTS");

                var criteria = string.Empty;
                var empCriteria = "";
                var hrperCriteria = "";
                if (!string.IsNullOrEmpty(person))
                {
                    empCriteria = string.Concat(empCriteria, "WITH EMPLOYES.ID EQ '", person, "'");
                }
                if (!string.IsNullOrEmpty(campus))
                {
                    hrperCriteria = string.Concat(hrperCriteria, "WITH HRP.PRI.CAMPUS.LOCATION EQ '", campus, "'");
                }
                if (!string.IsNullOrEmpty(status))
                {
                    var today = await GetUnidataFormatDateAsync(DateTime.Now);
                    if (string.Equals(status, Dtos.EnumProperties.EmployeeStatus.Terminated.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        if (string.IsNullOrEmpty(hrperCriteria))
                            hrperCriteria = string.Concat(hrperCriteria, "WITH HRP.EFFECT.TERM.DATE NE '' AND HRP.EFFECT.TERM.DATE LT '", today, "'");
                        else
                            hrperCriteria = string.Concat(hrperCriteria, " AND WITH HRP.EFFECT.TERM.DATE NE '' AND HRP.EFFECT.TERM.DATE LT '", today, "'");
                    }

                    if (string.Equals(status, Dtos.EnumProperties.EmployeeStatus.Leave.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        string leaveCodes = "";
                        if (ldmDefaults != null)
                        {
                            var leaveCodeIds = ldmDefaults.LdmdLeaveStatusCodes;

                            foreach (var leaveCodeId in leaveCodeIds)
                            {
                                leaveCodes = string.Concat(leaveCodes, "'", leaveCodeId, "'");
                            }
                        }
                        if (leaveCodes != "")
                        {
                            if (string.IsNullOrEmpty(hrperCriteria))
                                hrperCriteria = string.Concat(hrperCriteria, "WITH HRP.CURRENT.STATUS EQ ", leaveCodes);
                            else
                                hrperCriteria = string.Concat(hrperCriteria, " AND WITH HRP.CURRENT.STATUS EQ ", leaveCodes);
                        }
                    }

                    if (string.Equals(status, Dtos.EnumProperties.EmployeeStatus.Active.ToString(), StringComparison.OrdinalIgnoreCase))
                    {
                        if (string.IsNullOrEmpty(hrperCriteria))
                            hrperCriteria = string.Concat(hrperCriteria, "WITH (HRP.EFFECT.TERM.DATE EQ '' OR HRP.EFFECT.TERM.DATE GE '", today, "')");
                        else
                            hrperCriteria = string.Concat(hrperCriteria, " AND WITH (HRP.EFFECT.TERM.DATE EQ '' OR HRP.EFFECT.TERM.DATE GE '", today, "')");

                        if (ldmDefaults != null)
                        {
                            var leaveCodeIds = ldmDefaults.LdmdLeaveStatusCodes;

                            foreach (var leaveCodeId in leaveCodeIds)
                            {
                                hrperCriteria = string.Concat(hrperCriteria, "AND WITH HRP.CURRENT.STATUS NE '", leaveCodeId, "'");
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(startOn))
                {
                    if (string.IsNullOrEmpty(hrperCriteria))
                        hrperCriteria = string.Concat(hrperCriteria, "WITH HRP.EFFECT.EMPLOY.DATE EQ '", startOn, "'");
                    else
                        hrperCriteria = string.Concat(hrperCriteria, " AND WITH HRP.EFFECT.EMPLOY.DATE EQ '", startOn, "'");
                }
                if (!string.IsNullOrEmpty(endOn))
                {
                    if (string.IsNullOrEmpty(hrperCriteria))
                        hrperCriteria = string.Concat(hrperCriteria, "WITH HRP.EFFECT.TERM.DATE EQ '", endOn, "'");
                    else
                        hrperCriteria = string.Concat(hrperCriteria, " AND WITH HRP.EFFECT.TERM.DATE EQ '", endOn, "'");
                }
                if (!string.IsNullOrEmpty(rehireableStatusEligibility))
                {
                    if (string.IsNullOrEmpty(hrperCriteria))
                        hrperCriteria = string.Concat(hrperCriteria, "WITH HRP.REHIRE.ELIGIBILITY EQ ", rehireableStatusEligibility);
                    else
                        hrperCriteria = string.Concat(hrperCriteria, " AND WITH HRP.REHIRE.ELIGIBILITY EQ ", rehireableStatusEligibility);
                }
                if (!string.IsNullOrEmpty(rehireableStatusType))
                {
                    if (string.IsNullOrEmpty(hrperCriteria))
                        hrperCriteria = string.Concat(hrperCriteria, "WITH HRP.REHIRE.ELIGIBILITY EQ '", rehireableStatusType, "'");
                    else
                        hrperCriteria = string.Concat(hrperCriteria, " AND WITH HRP.REHIRE.ELIGIBILITY EQ '", rehireableStatusType, "'");
                }

                var employeeKeys = await DataReader.SelectAsync("EMPLOYES", empCriteria);
                if (!string.IsNullOrEmpty(hrperCriteria))
                {
                    var hrperKeys = await DataReader.SelectAsync("HRPER", hrperCriteria);
                    if (hrperKeys.Any())
                    {
                        employeeKeys = employeeKeys.Intersect(hrperKeys).ToArray();
                    }
                    else
                    {
                        employeeKeys = null;
                    }
                }
                //if (employeeKeys == null)
                //{
                //    var message = "Unexpected null returned from EMPLOYES SelectAsyc";
                //    logger.Error(message);
                //    throw new RepositoryException(message);
                //}

                //if (!employeeKeys.Any())
                //{
                //    logger.Info("No EMPLOYES records selected from the database");
                //}

                var employeeRecords = new List<Employes>();
                var totalCount = 0;
                var perposRecords = new List<Perpos>();
                var perposwgRecords = new List<Perposwg>();
                var perstatRecords = new List<Perstat>();
                var perbenRecords = new List<Perben>(); var hrperRecords = new List<Hrper>();

                if (employeeKeys != null)
                {
                    totalCount = employeeKeys.Count();

                    Array.Sort(employeeKeys);

                    var employeeSubList = employeeKeys.Skip(offset).Take(limit).ToArray();

                    //bulkread the records for all the keys
                    var bulkRecords = await DataReader.BulkReadRecordAsync<Employes>(employeeSubList);
                    if (bulkRecords == null)
                    {
                        logger.Error("Unexpected null from bulk read of Employes records");
                    }
                    else
                    {
                        employeeRecords.AddRange(bulkRecords);
                    }

                    var personIds = employeeRecords.Select(e => e.Recordkey);
                    // select all the PERPOS ids with the HRP.ID equal to the input person id.
                    criteria = "WITH PERPOS.HRP.ID EQ ?";
                    var perposKeys = await DataReader.SelectAsync("PERPOS", criteria, personIds.Select(id => string.Format("\"{0}\"", id)).ToArray());

                    //bulkread the records for all the keys
                    for (int i = 0; i < perposKeys.Count(); i += bulkReadSize)
                    {
                        var subList = perposKeys.Skip(i).Take(bulkReadSize);
                        var records = await DataReader.BulkReadRecordAsync<Perpos>(subList.ToArray());
                        if (records != null)
                        {
                            perposRecords.AddRange(records);
                        }
                    }

                    // select all the PERPOSWG ids with the HRP.ID equal to the input person id.
                    criteria = "WITH PPWG.HRP.ID EQ ?";
                    var perposwgKeys = await DataReader.SelectAsync("PERPOSWG", criteria, personIds.Select(id => string.Format("\"{0}\"", id)).ToArray());

                    //bulkread the records for all the keys
                    for (int i = 0; i < perposwgKeys.Count(); i += bulkReadSize)
                    {
                        var subList = perposwgKeys.Skip(i).Take(bulkReadSize);
                        var records = await DataReader.BulkReadRecordAsync<Perposwg>(subList.ToArray());
                        if (records != null)
                        {
                            perposwgRecords.AddRange(records);
                        }
                    }

                    // select all the PERSTAT ids with the HRP.ID equal to the input person id.
                    criteria = "WITH PERSTAT.HRP.ID EQ ?";
                    var perstatKeys = await DataReader.SelectAsync("PERSTAT", criteria, personIds.Select(id => string.Format("\"{0}\"", id)).ToArray());

                    for (int i = 0; i < perstatKeys.Count(); i += bulkReadSize)
                    {
                        var subList = perstatKeys.Skip(i).Take(bulkReadSize);
                        var records = await DataReader.BulkReadRecordAsync<Perstat>(subList.ToArray());
                        if (records != null)
                        {
                            perstatRecords.AddRange(records);
                        }
                    }

                    // select all the PERBEN ids with the HRP.ID equal to the input person id.
                    criteria = "WITH PERBEN.HRP.ID EQ ?";
                    var perbenKeys = await DataReader.SelectAsync("PERBEN", criteria, personIds.Select(id => string.Format("\"{0}\"", id)).ToArray());

                    for (int i = 0; i < perbenKeys.Count(); i += bulkReadSize)
                    {
                        var subList = perbenKeys.Skip(i).Take(bulkReadSize);
                        var records = await DataReader.BulkReadRecordAsync<Perben>(subList.ToArray());
                        if (records != null)
                        {
                            perbenRecords.AddRange(records);
                        }
                    }

                    // bulkread all HRPER records from Colleauge
                    for (int i = 0; i < personIds.Count(); i += bulkReadSize)
                    {
                        var subList = personIds.Skip(i).Take(bulkReadSize);
                        var records = await DataReader.BulkReadRecordAsync<Hrper>(subList.ToArray());
                        if (records != null)
                        {
                            hrperRecords.AddRange(records);
                        }
                    }
                }

                //build the Employee objects
                var employeeEntities = new List<Ellucian.Colleague.Domain.HumanResources.Entities.Employee>();
                if (employeeRecords.Any())
                {
                    foreach (var employesRecord in employeeRecords)
                    {
                        if (employesRecord != null)
                        {
                            try
                            {
                                var personPositionRecords = perposRecords.Where(pp => pp.PerposHrpId == employesRecord.Recordkey);
                                var personPositionWages = perposwgRecords.Where(ppw => ppw.PpwgHrpId == employesRecord.Recordkey);
                                var personStatusRecords = perstatRecords.Where(ps => ps.PerstatHrpId == employesRecord.Recordkey);
                                var personBenefitsRecords = perbenRecords.Where(pb => pb.PerbenHrpId == employesRecord.Recordkey);
                                var hrPersonRecord = hrperRecords.FirstOrDefault(hr => hr.Recordkey == employesRecord.Recordkey);
                                employeeEntities.Add(
                                    BuildEmployee(employesRecord, hrPersonRecord, personPositionRecords,
                                    personPositionWages, personStatusRecords, personBenefitsRecords, ldmDefaults));
                            }
                            catch (Exception e)
                            {
                                LogDataError("Employees", employesRecord.Recordkey, employesRecord, e, e.Message);
                            }
                        }
                    }
                }

                return new Tuple<IEnumerable<Ellucian.Colleague.Domain.HumanResources.Entities.Employee>, int>(employeeEntities, totalCount);
            }
            catch (RepositoryException e)
            {
                throw e;
            }
        }

        /// <summary>
        /// Return a Unidata Formatted Date string from an input argument of string type
        /// </summary>
        /// <param name="date">String representing a Date</param>
        /// <returns>Unidata formatted Date string for use in Colleague Selection.</returns>
        public async Task<string> GetUnidataFormattedDate(string date)
        {
            var internationalParameters = await GetInternationalParametersAsync();
            var newDate = DateTime.Parse(date).Date;
            return UniDataFormatter.UnidataFormatDate(newDate, internationalParameters.HostShortDateFormat, internationalParameters.HostDateDelimiter);
        }

        /// <summary>
        /// Get Employees objects for all employees.
        /// </summary>   
        /// <param name="id">guid of the employees record.</param>
        /// <returns>Employee Entity <see cref="Employee"./></returns>
        public async Task<Ellucian.Colleague.Domain.HumanResources.Entities.Employee> GetEmployeeByIdAsync(string id)
        {
            var guidLookUp = new GuidLookup(id);
            var employesRecord = await DataReader.ReadRecordAsync<Employes>(guidLookUp);
            if (employesRecord == null)
            {
                var exception = new RepositoryException();
                exception.AddError(new Domain.Entities.RepositoryError("employee.id", string.Format("The employee record for guid '{0}' is not valid.", id)));
                throw exception;
            }

            var personId = employesRecord.Recordkey;
            // select all the PERPOS ids with the HRP.ID equal to the input person id.
            var criteria = string.Format("WITH PERPOS.HRP.ID EQ '{0}'", personId);
            var perposKeys = await DataReader.SelectAsync("PERPOS", criteria);

            //bulkread the records for all the keys
            var perposRecords = new List<Perpos>();
            for (int i = 0; i < perposKeys.Count(); i += bulkReadSize)
            {
                var subList = perposKeys.Skip(i).Take(bulkReadSize);
                var records = await DataReader.BulkReadRecordAsync<Perpos>(subList.ToArray());
                if (records != null)
                {
                    perposRecords.AddRange(records);
                }
            }

            // select all the PERPOSWG ids with the HRP.ID equal to the input person id.
            criteria = string.Format("WITH PPWG.HRP.ID EQ '{0}'", personId);
            var perposwgKeys = await DataReader.SelectAsync("PERPOSWG", criteria);

            //bulkread the records for all the keys
            var perposwgRecords = new List<Perposwg>();
            for (int i = 0; i < perposwgKeys.Count(); i += bulkReadSize)
            {
                var subList = perposwgKeys.Skip(i).Take(bulkReadSize);
                var records = await DataReader.BulkReadRecordAsync<Perposwg>(subList.ToArray());
                if (records != null)
                {
                    perposwgRecords.AddRange(records);
                }
            }

            // select all the PERSTAT ids with the HRP.ID equal to the input person id.
            criteria = string.Format("WITH PERSTAT.HRP.ID EQ '{0}'", personId);
            var perstatKeys = await DataReader.SelectAsync("PERSTAT", criteria);

            var perstatRecords = new List<Perstat>();
            for (int i = 0; i < perstatKeys.Count(); i += bulkReadSize)
            {
                var subList = perstatKeys.Skip(i).Take(bulkReadSize);
                var records = await DataReader.BulkReadRecordAsync<Perstat>(subList.ToArray());
                if (records != null)
                {
                    perstatRecords.AddRange(records);
                }
            }

            // select all the PERBEN ids with the HRP.ID equal to the input person id.
            criteria = string.Format("WITH PERBEN.HRP.ID EQ '{0}'", personId);
            var perbenKeys = await DataReader.SelectAsync("PERBEN", criteria);

            var perbenRecords = new List<Perben>();
            for (int i = 0; i < perbenKeys.Count(); i += bulkReadSize)
            {
                var subList = perbenKeys.Skip(i).Take(bulkReadSize);
                var records = await DataReader.BulkReadRecordAsync<Perben>(subList.ToArray());
                if (records != null)
                {
                    perbenRecords.AddRange(records);
                }
            }

            // read the HRPER record from Colleauge
            var hrPersonRecord = await DataReader.ReadRecordAsync<Hrper>(personId);

            // Get list of benefits to exclude for consideration of employee benefit status
            var ldmDefaults = DataReader.ReadRecord<LdmDefaults>("CORE.PARMS", "LDM.DEFAULTS");

            //build the Employee objects

            return BuildEmployee(employesRecord, hrPersonRecord, perposRecords,
                    perposwgRecords, perstatRecords, perbenRecords, ldmDefaults);
        }

        /// <summary>
        /// Helper to build PersonPosition objects
        /// </summary>
        /// <param name="employRecord">the Perpos db record</param>
        /// <returns></returns>
        private Ellucian.Colleague.Domain.HumanResources.Entities.Employee BuildEmployee(Employes employRecord,
            Hrper hrPersonRecord,
            IEnumerable<Perpos> personPositionRecords,
            IEnumerable<Perposwg> personPositionWages,
            IEnumerable<Perstat> personStatusRecords,
            IEnumerable<Perben> personBenefitsRecords,
            LdmDefaults ldmDefaults
          )
        {
            Ellucian.Colleague.Domain.HumanResources.Entities.Employee employeeEntity = null;
            var guid = employRecord.RecordGuid;
            var personId = employRecord.Recordkey;
            if (!string.IsNullOrEmpty(guid) && !string.IsNullOrEmpty(personId))
            {
                // Build the Employees Entity from the gathered data.
                employeeEntity = new Domain.HumanResources.Entities.Employee(guid, personId);

                // HRPER values for employee
                employeeEntity.Location = hrPersonRecord.HrpPriCampusLocation;
                employeeEntity.StartDate = hrPersonRecord.HrpEffectEmployDate;
                employeeEntity.EndDate = hrPersonRecord.HrpEffectTermDate;
                employeeEntity.RehireEligibilityCode = hrPersonRecord.HrpRehireEligibility;
                employeeEntity.EmploymentStatus = EmployeeStatus.Active;
                if (hrPersonRecord.HrpEffectTermDate != null && hrPersonRecord.HrpEffectTermDate <= DateTime.Today)
                {
                    employeeEntity.EmploymentStatus = EmployeeStatus.Terminated;
                }

                List<string> leaveCodes = new List<string>();
                if (ldmDefaults != null)
                {
                    leaveCodes = ldmDefaults.LdmdLeaveStatusCodes;
                }

                // PERSTAT records
                if (personStatusRecords.Any())
                {
                    foreach (var perstat in personStatusRecords)
                    {
                        if (perstat.PerstatEndDate == null || perstat.PerstatEndDate > DateTime.Today)
                        {
                            if (perstat.PerstatStartDate <= DateTime.Today)
                            {
                                employeeEntity.StatusCode = perstat.PerstatStatus;
                                if (employeeEntity.EmploymentStatus != EmployeeStatus.Terminated)
                                {
                                    if (leaveCodes.Contains(employeeEntity.StatusCode))
                                    {
                                        employeeEntity.EmploymentStatus = EmployeeStatus.Leave;
                                        break;
                                    }
                                }
                            }
                        }
                        if (hrPersonRecord.HrpEffectTermDate != null)
                        {
                            employeeEntity.StatusEndReasonCode = perstat.PerstatEndReason;
                        }
                    }
                }

                // PERPOSWG records
                employeeEntity.PayStatus = PayStatus.WithoutPay;
                var payPeriodHours = new List<decimal?>();
                if (personPositionWages.Any())
                {
                    foreach (var perwage in personPositionWages)
                    {
                        if (perwage.PpwgEndDate == null || perwage.PpwgEndDate > DateTime.Today)
                        {
                            employeeEntity.PayStatus = PayStatus.WithPay;
                            payPeriodHours.Add(perwage.PpwgCycleWorkTimeAmt);
                        }
                    }
                }
                employeeEntity.PayPeriodHours = payPeriodHours;
                // PERBEN records
                employeeEntity.BenefitsStatus = BenefitsStatus.WithoutBenefits;
                if (personBenefitsRecords.Any())
                {
                    List<string> excludeBenefits = new List<string>();
                    if (ldmDefaults != null)
                    {
                        excludeBenefits = ldmDefaults.LdmdExcludeBenefits;
                    }
                    foreach (var perben in personBenefitsRecords)
                    {
                        var benefit = perben.PerbenBdId;
                        // Make sure this benefit is not on the list for exclusion before using it to determine
                        // employee benefit status
                        if (!excludeBenefits.Contains(benefit))
                        {
                            if (perben.PerbenCancelDate == null || perben.PerbenCancelDate > DateTime.Today)
                            {
                                employeeEntity.BenefitsStatus = BenefitsStatus.WithBenefits;
                                break;
                            }
                        }
                    }
                }
            }
            return employeeEntity;
        }
    }
}
