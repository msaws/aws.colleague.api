﻿namespace Ellucian.Colleague.Dtos.Base
{
    /// <summary>
    /// Information about ethnicities
    /// </summary>
    public class Ethnicity
    {
        /// <summary>
        /// Unique system Id for this ethnicity
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// Description of this ethnicity
        /// </summary>
        public string Description { get; set; }
    }
}