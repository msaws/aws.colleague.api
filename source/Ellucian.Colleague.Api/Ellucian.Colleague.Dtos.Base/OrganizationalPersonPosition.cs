﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;

namespace Ellucian.Colleague.Dtos.Base
{
    /// <summary>
    /// There may be any number of instances of a specific position in an organization since several different
    /// persons may be assigned to the same position title. This entity represents one such instance
    /// of a position, including its relationships to other organizational person positions. This entity includes a 
    /// person ID when currently assigned to a person.
    /// </summary>
    public class OrganizationalPersonPosition
    {
        /// <summary>
        /// The unique Id of the organizational person position, which is an assigned position in an organization.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The Id of the position
        /// </summary>
        public string PositionId { get; set; }

        /// <summary>
        /// The Title for the position
        /// </summary>
        public string PositionTitle { get; set; }

        /// <summary>
        /// Id of the person assigned to the organizational person position
        /// </summary>
        public string PersonId { get; set; }

        /// <summary>
        /// The name of the person
        /// </summary>
        public string PersonName { get; set; }

        /// <summary>
        /// List of slots organizationally above this slot
        /// </summary>
        public IEnumerable<OrganizationalRelationship> Relationships { get; set; }
    }
}
