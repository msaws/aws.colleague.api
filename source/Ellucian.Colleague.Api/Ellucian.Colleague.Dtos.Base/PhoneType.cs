﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
namespace Ellucian.Colleague.Dtos.Base
{
    /// <summary>
    /// Phone type
    /// </summary>
    public class PhoneType
    {
        /// <summary>
        /// Phone type code
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// User-readable phone type description
        /// </summary>
        public string Description { get; set; }
    }
}
