﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

namespace Ellucian.Colleague.Dtos.Base
{
    /// <summary>
    /// List of supported tax forms.
    /// </summary>
    public enum TaxForms
    {
        /// <summary>
        /// This represents form W-2.
        /// </summary>
        FormW2,

        /// <summary>
        /// This represents form 1095-C.
        /// </summary>
        Form1095C,

        /// <summary>
        /// This represents form 1098.
        /// </summary>
        Form1098
    }
}
