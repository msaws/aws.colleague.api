﻿//Copyright 2014-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Domain.FinancialAid.Entities;
using Ellucian.Colleague.Domain.FinancialAid.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Colleague.Domain.FinancialAid.Tests.Services
{
    [TestClass]
    public class CurrentOfficeServiceTests
    {
        [TestClass]
        public class CurrentOfficeConstructorTests
        {
            private IEnumerable<FinancialAidOffice> inputOffices;
            private TestFinancialAidOfficeRepository officeRepository;

            [TestInitialize]
            public void Initialize()
            {
                officeRepository = new TestFinancialAidOfficeRepository();
                inputOffices = officeRepository.GetFinancialAidOffices();
            }

            [TestMethod]
            public void OfficeInputListIsValidTest()
            {
                new CurrentOfficeService(inputOffices);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void NullOfficesThrowsExceptionTest()
            {
                new CurrentOfficeService(null);
            }

            [TestMethod]
            public void EmptyOfficeInputListIsValidTest()
            {
                new CurrentOfficeService(new List<FinancialAidOffice>());
            }
        }

        [TestClass]
        public class CurrentOfficeByLocationTests
        {
            private IEnumerable<FinancialAidOffice> inputOffices;
            private TestFinancialAidOfficeRepository officeRepository;

            private CurrentOfficeService currentOfficeService;

            [TestInitialize]
            public void Initialize()
            {
                officeRepository = new TestFinancialAidOfficeRepository();
                inputOffices = officeRepository.GetFinancialAidOffices();

                currentOfficeService = new CurrentOfficeService(inputOffices);
            }

            [TestMethod]
            public void NullInputLocationIdReturnsDefaultOfficeTest()
            {
                var expectedDefaultOffice = inputOffices.First(o => o.IsDefault);
                var actualDefaultOffice = currentOfficeService.GetCurrentOfficeByLocationId(null);

                Assert.AreEqual(expectedDefaultOffice, actualDefaultOffice);
            }

            [TestMethod]
            public void ValidLocationIdReturnsOfficeByLocationTest()
            {
                var expectedOffice = inputOffices.First(o => o.LocationIds != null && o.LocationIds.Count() > 0);
                var actualOffice = currentOfficeService.GetCurrentOfficeByLocationId(expectedOffice.LocationIds.First());

                Assert.AreEqual(expectedOffice, actualOffice);
                CollectionAssert.AreEqual(expectedOffice.LocationIds, actualOffice.LocationIds);
            }

            [TestMethod]
            public void InvalidLocationIdReturnsDefaultOfficeTest()
            {
                var expectedOffice = inputOffices.First(o => o.IsDefault);
                var actualOffice = currentOfficeService.GetCurrentOfficeByLocationId("foobar");

                Assert.AreEqual(expectedOffice, actualOffice);
            }

            [TestMethod]
            public void InvalidLocationIdAndNoDefaultOfficeReturnsNullTest()
            {
                inputOffices.ToList().ForEach(o => o.IsDefault = false);
                FinancialAidOffice expectedOffice = inputOffices.FirstOrDefault(o => o.IsDefault);
                currentOfficeService = new CurrentOfficeService(inputOffices);
                var actualOffice = currentOfficeService.GetCurrentOfficeByLocationId("foobar");

                Assert.IsNull(expectedOffice);
                Assert.IsNull(actualOffice);
            }
        }

        [TestClass]
        public class GetDefaultOfficeTests
        {
            private IEnumerable<FinancialAidOffice> inputOffices;
            private TestFinancialAidOfficeRepository officeRepository;

            private CurrentOfficeService currentOfficeService;

            [TestInitialize]
            public void Initialize()
            {
                officeRepository = new TestFinancialAidOfficeRepository();
                inputOffices = officeRepository.GetFinancialAidOffices();

                currentOfficeService = new CurrentOfficeService(inputOffices);
            }

            [TestMethod]
            public void ReturnDefaultOfficeTest()
            {
                var expectedOffice = inputOffices.First(o => o.IsDefault);
                var actualOffice = currentOfficeService.GetDefaultOffice();

                Assert.AreEqual(expectedOffice, actualOffice);
            }

            [TestMethod]
            public void NoDefaultOfficeReturnsNullTest()
            {
                inputOffices.ToList().ForEach(o => o.IsDefault = false);
                currentOfficeService = new CurrentOfficeService(inputOffices);

                var expectedOffice = inputOffices.FirstOrDefault(o => o.IsDefault);
                var actualOffice = currentOfficeService.GetDefaultOffice();

                Assert.IsNull(expectedOffice);
                Assert.IsNull(actualOffice);
            }
        }

        [TestClass]
        public class GetCurrentOfficeByIdTests
        {
            private IEnumerable<FinancialAidOffice> inputOffices;
            private TestFinancialAidOfficeRepository officeRepository;

            private CurrentOfficeService currentOfficeService;

            [TestInitialize]
            public async void Initialize()
            {
                officeRepository = new TestFinancialAidOfficeRepository();
                inputOffices = await officeRepository.GetFinancialAidOfficesAsync();

                currentOfficeService = new CurrentOfficeService(inputOffices);
            }

            [TestMethod]
            public void GetCurrentOfficeById_ReturnsExpectedOfficeTest()
            {
                var expectedOffice = inputOffices.First();
                var actualOffice = currentOfficeService.GetCurrentOfficeByOfficeId(expectedOffice.Id);
                Assert.AreEqual(expectedOffice.Id, actualOffice.Id);
                Assert.AreEqual(expectedOffice.Name, actualOffice.Name);
                Assert.AreEqual(expectedOffice.OpeId, actualOffice.OpeId);
                Assert.AreEqual(expectedOffice.TitleIVCode, actualOffice.TitleIVCode);
            }

            [TestMethod]
            public void GetCurrentOfficeById_ReturnsDefaultIfIdNullTest()
            {
                var expectedOffice = inputOffices.FirstOrDefault(o => o.IsDefault);
                var actualOffice = currentOfficeService.GetCurrentOfficeByOfficeId(null);
                Assert.IsTrue(actualOffice.IsDefault);
                Assert.AreEqual(expectedOffice.Id, actualOffice.Id);
                Assert.AreEqual(expectedOffice.Name, actualOffice.Name);
                Assert.AreEqual(expectedOffice.OpeId, actualOffice.OpeId);
                Assert.AreEqual(expectedOffice.TitleIVCode, actualOffice.TitleIVCode);
            }

            [TestMethod]
            public void GetCurrentOfficeById_ReturnsDefaultIfNoMatchingOfficeTest()
            {
                var expectedOffice = inputOffices.FirstOrDefault(o => o.IsDefault);
                var actualOffice = currentOfficeService.GetCurrentOfficeByOfficeId("foo");
                Assert.IsTrue(actualOffice.IsDefault);
                Assert.AreEqual(expectedOffice.Id, actualOffice.Id);
                Assert.AreEqual(expectedOffice.Name, actualOffice.Name);
                Assert.AreEqual(expectedOffice.OpeId, actualOffice.OpeId);
                Assert.AreEqual(expectedOffice.TitleIVCode, actualOffice.TitleIVCode);
            }
        }
    }
}
