﻿/*Copyright 2014-2017 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Ellucian.Colleague.Domain.FinancialAid.Entities;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Dmi.Runtime;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.FinancialAid.Tests
{
    public class TestFinancialAidFundRepository : IFinancialAidFundRepository
    {
        public Task<IEnumerable<FinancialAidFundsFinancialProperty>> GetFinancialAidFundFinancialsAsync(string awardId, IEnumerable<string> fundYears, string hostCountry)
        {
            return Task.FromResult<IEnumerable<FinancialAid.Entities.FinancialAidFundsFinancialProperty>>(new List<FinancialAid.Entities.FinancialAidFundsFinancialProperty>()
                {
                    new FinancialAid.Entities.FinancialAidFundsFinancialProperty("2008", "OFFICE1", (decimal) 10000, "AWARD1"),
                    new FinancialAid.Entities.FinancialAidFundsFinancialProperty("2009", "OFFICE2", (decimal) 20000, "AWARD2"),
                    new FinancialAid.Entities.FinancialAidFundsFinancialProperty("2010", "OFFICE3", (decimal) 30000, "AWARD3"),
                });
        }


        public Task<IEnumerable<FinancialAidFund>> GetFinancialAidFundsAsync(bool ignoreCache)
        {
            return Task.FromResult<IEnumerable<FinancialAid.Entities.FinancialAidFund>>(new List<FinancialAid.Entities.FinancialAidFund>()
                {
                    new FinancialAid.Entities.FinancialAidFund("bb66b971-3ee0-4477-9bb7-539721f93434", "CODE1", "DESC1"),
                    new FinancialAid.Entities.FinancialAidFund("5aeebc5c-c973-4f83-be4b-f64c95002124", "CODE2", "DESC2"),
                    new FinancialAid.Entities.FinancialAidFund("27178aab-a6e8-4d1e-ae27-eca1f7b33363", "CODE3", "DESC3")
                });
        }

    }

}
