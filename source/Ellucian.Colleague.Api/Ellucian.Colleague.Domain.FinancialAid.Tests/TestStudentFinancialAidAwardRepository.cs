﻿/*Copyright 2017 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Ellucian.Colleague.Domain.FinancialAid.Entities;
using Ellucian.Colleague.Domain.FinancialAid.Repositories;
using Ellucian.Dmi.Runtime;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.FinancialAid.Tests
{
    public class TestStudentFinancialAidAwardRepository
    {
        public Task<IEnumerable<StudentAwardHistoryStatus>> GetStudentAwardHistoryStatusesAsync()
        {
            return Task.FromResult<IEnumerable<FinancialAid.Entities.StudentAwardHistoryStatus>>(new List<FinancialAid.Entities.StudentAwardHistoryStatus>()
                {
                    new FinancialAid.Entities.StudentAwardHistoryStatus()
                    {
                        Status = "A",
                        StatusDate = new DateTime(2016, 02, 01),
                        Amount = (decimal) 2000000,
                        XmitAmount = (decimal) 1000
                    },
                    new FinancialAid.Entities.StudentAwardHistoryStatus()
                    {
                        Status = "A",
                        StatusDate = new DateTime(2016, 02, 01),
                        Amount = (decimal) 1000000,
                        XmitAmount = (decimal) 1000
                    },
                    new FinancialAid.Entities.StudentAwardHistoryStatus()
                    {
                        Status = "A",
                        StatusDate = new DateTime(2016, 02, 01),
                        Amount = (decimal) 3000000,
                        XmitAmount = (decimal) 1000
                    },
                });
        }

        public Task<IEnumerable<StudentAwardHistoryByPeriod>> GetStudentAwardHistoryByPeriodsAsync()
        {
            return Task.FromResult<IEnumerable<FinancialAid.Entities.StudentAwardHistoryByPeriod>>(new List<FinancialAid.Entities.StudentAwardHistoryByPeriod>()
                {
                    new FinancialAid.Entities.StudentAwardHistoryByPeriod()
                    {
                        AwardPeriod = "CODE1",
                        Status = "A",
                        StatusDate = new DateTime(2016, 02, 01),
                        Amount = (decimal) 2000000,
                        XmitAmount = (decimal) 1000
                    },
                    new FinancialAid.Entities.StudentAwardHistoryByPeriod()
                    {
                        AwardPeriod = "CODE2",
                        Status = "A",
                        StatusDate = new DateTime(2016, 02, 01),
                        Amount = (decimal) 1000000,
                        XmitAmount = (decimal) 1000
                    },
                    new FinancialAid.Entities.StudentAwardHistoryByPeriod()
                    {
                        AwardPeriod = "CODE3",
                        Status = "A",
                        StatusDate = new DateTime(2016, 02, 01),
                        Amount = (decimal) 3000000,
                        XmitAmount = (decimal) 1000
                    },
                });
        }


        public Task<IEnumerable<StudentFinancialAidAward>> GetStudentFinancialAidAwardsAsync(bool ignoreCache)
        {
            return Task.FromResult<IEnumerable<FinancialAid.Entities.StudentFinancialAidAward>>(new List<FinancialAid.Entities.StudentFinancialAidAward>()
                {
                    new FinancialAid.Entities.StudentFinancialAidAward("bb66b971-3ee0-4477-9bb7-539721f93434", "STUDENT1", "FUND1", "YEAR1"),
                    new FinancialAid.Entities.StudentFinancialAidAward("5aeebc5c-c973-4f83-be4b-f64c95002124", "STUDENT2", "FUND2", "YEAR2"),
                    new FinancialAid.Entities.StudentFinancialAidAward("27178aab-a6e8-4d1e-ae27-eca1f7b33363", "STUDENT3", "FUND3", "YEAR3"),
                    new FinancialAid.Entities.StudentFinancialAidAward("71278bac-8e6a-e1d4-72ea-cef1a337b633", "STUDENT4", "FUND4", "YEAR4")
                });
        }

    }

}
