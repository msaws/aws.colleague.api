﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;

namespace Ellucian.Colleague.Domain.Student.Tests
{
    public class TestStudentTaxFormPdfDataRepository
    {
        public List<Form1098PdfData> Form1098PdfDataObjects;

        public TestStudentTaxFormPdfDataRepository()
        {
            this.Form1098PdfDataObjects = new List<Form1098PdfData>()
            {
                new Form1098PdfData("2016", "1"),
                new Form1098PdfData("2015", "2"),
                new Form1098PdfData("2014", "3"),
                new Form1098PdfData("2013", "4"),
                new Form1098PdfData("2012", "5"),
                new Form1098PdfData("2011", "6"),
                new Form1098PdfData("2010", "7"),
            };
        }

        public Form1098PdfData Get1098TPdfAsync(string personId, string recordId)
        {
            return this.Form1098PdfDataObjects.Where(x => x.TaxYear == recordId).FirstOrDefault();
        }
    }
}
