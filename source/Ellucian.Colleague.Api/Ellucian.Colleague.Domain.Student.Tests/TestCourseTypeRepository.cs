﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Student.Entities;


namespace Ellucian.Colleague.Domain.Student.Tests
{
    public class TestCourseTypeRepository
    {
                private string[,] courseTypes = {
                                            //CODE   DESCRIPTION SPECIAL_PROCESSING_2
                                            {"HONOR", "Honors",               "Y"}, 
                                            {"STND",  "Standard",              ""},
                                            {"COOP",  "Coop Work Experience", "N"},
                                            {"REMED", "Remedial",             ""}, 
                                            {"VOC",   "Vocational",           ""},
                                            {"WR",    "Writing-Intensive",    "O"}
                                      };


        public IEnumerable<CourseType> Get()
        {
            var crsTypes = new List<CourseType>();

            // There are 2 fields for each courseType in the array
            var items = courseTypes.Length / 3;

            for (int x = 0; x < items; x++)
            {
                crsTypes.Add(new CourseType(courseTypes[x, 0], courseTypes[x, 1], courseTypes[x, 2] != "N" ));
            }
            return crsTypes;
        }
    }
}