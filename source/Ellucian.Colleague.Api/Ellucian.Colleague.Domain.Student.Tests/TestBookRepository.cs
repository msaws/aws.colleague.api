﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;
using System.Threading.Tasks;



namespace Ellucian.Colleague.Domain.Student.Tests
{
    public class TestBookRepository : IBookRepository 
    {
        private string[,] books = {
 
                                            //ID         TITLE														 
                                            {"40917",  "400.00", "Finger Printing for the Ages"},
                                            {"1000",   "45.44",  "Admissions"},
                                            {"1001",   "",       "Attaboy"},
                                            {"40677",  "44.26", "The History of Chad"},
                                            {"111",   "150.00", "Animal Science"},
                                            {"AOJU",  "33.00",  "Administration of Justice"},
                                            };

        public async Task<IEnumerable<Book>> GetAsync()
        {
            var bookEntities = new List<Book>();

            // There are 2 fields for each book in the array (only 2 used at the moment)
            var items = books.Length / 3;

            for (int x = 0; x < items; x++)
            {
                var book = new Book(books[x, 0]);
                if (!string.IsNullOrEmpty(books[x, 1]))
                {
                    book.Price = decimal.Parse(books[x, 1]); 
                }
                bookEntities.Add(book);
            }
            return await Task.FromResult(bookEntities);
        }

        public async Task<Book> GetAsync(string id)
        {
            Book book = (await GetAsync()).Where(b => b.Id == id).FirstOrDefault();
            return book;
        }
    }
}
