﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Domain.Student.Entities;

namespace Ellucian.Colleague.Domain.Student.Tests.Entities
{
    [TestClass]
    public class TopicCodeTests
    {
        [TestClass]
        public class TopicCodeConstructor
        {
            private string code;
            private string desc;
            private TopicCode topicCode;

            [TestInitialize]
            public void Initialize()
            {
                code = "TC";
                desc = "Topic Code";
                topicCode = new TopicCode(code, desc);
            }

            [TestMethod]
            public void TopicCodeCode()
            {
                Assert.AreEqual(code, topicCode.Code);
            }

            [TestMethod]
            public void TopicCodeDescription()
            {
                Assert.AreEqual(desc, topicCode.Description);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void TopicCodeCodeNullException()
            {
                new TopicCode(null, desc);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void TopicCodeDescNullException()
            {
                new TopicCode(code, null);
            }

        }

        [TestClass]
        public class TopicCodeEquals
        {
            private string code;
            private string desc;
            private TopicCode topicCode1;
            private TopicCode topicCode2;
            private TopicCode topicCode3;

            [TestInitialize]
            public void Initialize()
            {
                code = "TC";
                desc = "Topic Code";
                topicCode1 = new TopicCode(code, desc);
                topicCode2 = new TopicCode(code, "Topic Code2");
                topicCode3 = new TopicCode("TC2", desc);
            }

            [TestMethod]
            public void TopicCodeSameCodesEqual()
            {
                Assert.IsTrue(topicCode1.Equals(topicCode2));
            }

            [TestMethod]
            public void TopicCodeDifferentCodeNotEqual()
            {
                Assert.IsFalse(topicCode1.Equals(topicCode3));
            }
        }

        [TestClass]
        public class TopicCodeGetHashCode
        {
            private string code;
            private string desc;
            private TopicCode topicCode1;
            private TopicCode topicCode2;
            private TopicCode topicCode3;

            [TestInitialize]
            public void Initialize()
            {
                code = "TC";
                desc = "Topic Code";
                topicCode1 = new TopicCode(code, desc);
                topicCode2 = new TopicCode(code, "Topic Code2");
                topicCode3 = new TopicCode("TC2", desc);
            }

            [TestMethod]
            public void TopicCodeSameCodeHashEqual()
            {
                Assert.AreEqual(topicCode1.GetHashCode(), topicCode2.GetHashCode());
            }

            [TestMethod]
            public void TopicCodeDifferentCodeHashNotEqual()
            {
                Assert.AreNotEqual(topicCode1.GetHashCode(), topicCode3.GetHashCode());
            }
        }
    }
}