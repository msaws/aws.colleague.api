﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Web.Http.TestUtil;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Entities.Requirements;
using Ellucian.Colleague.Domain.Student.Entities.Requirements.Modifications;

namespace Ellucian.Colleague.Domain.Student.Tests.Entities.Requirements.Modifications
{
    [TestClass]
    public class BlockReplacementTests
    {
        private ProgramRequirements pr;
        private List<Requirement> requirements;
        private List<Requirement> additionalrequirements;
        private TestProgramRequirementsRepository tprr;
        private TestRequirementRepository trr;
        private TestStudentProgramRepository tspr;

        public Requirement req1;



        [TestInitialize]
        public async void Initialize()
        {
            tprr = new TestProgramRequirementsRepository();
            trr = new TestRequirementRepository();
            tspr = new TestStudentProgramRepository();

            pr = tprr.Get("MATHPROG", "2033");
            requirements =(await  trr.GetAsync(new List<string>() { })).ToList();  // caveat: this test repo's data doesn't match the others'
            req1 = requirements.First();
            additionalrequirements = new List<Requirement>();
        }

        [TestMethod]
        public void Constructor()
        {
            BlockReplacement br = new BlockReplacement("999", req1, "");
            Assert.AreEqual("999", br.blockId);
            Assert.AreEqual(req1.Code, br.NewRequirement.Code);

        }
        [TestMethod]
        public void Constructor_allowsNullBlock()
        {
            BlockReplacement br = new BlockReplacement("999", null, "");
            Assert.AreEqual("999", br.blockId);
            Assert.IsNull(br.NewRequirement);
        }

        // Modify() works at subrequirement level with null repl block (QAD 49874)
        [TestMethod]
        public void Modify_ProgramRequirements_SetValue()
        {
            BlockReplacement br = new BlockReplacement("18000", null, "");
            br.Modify(pr, additionalrequirements);
        }

        // Modify() 
        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void Modify_ProgramRequirements_SetValue_ThrowsIfBlockNotFound()
        {
            BlockReplacement br = new BlockReplacement("999", null, "");
            br.Modify(pr, additionalrequirements);
        }

    }
}
