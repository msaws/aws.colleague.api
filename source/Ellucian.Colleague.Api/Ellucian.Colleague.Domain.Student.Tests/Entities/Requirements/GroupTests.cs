﻿using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Entities.Requirements;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Ellucian.Colleague.Domain.Student.Tests.Entities.Requirements
{
    [TestClass]
    public class GroupTests
    {
        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        /// <summary>
        /// GroupTests
        /// These tests use GroupTestsData.csv. This test data file is best modified by opening as a spreadsheet. 
        /// Steps to modify the csv file:
        ///     	* In api, right click csv file in visual studio and "check out for edit"
        ///     	* right click and "open with" and choose excel. 
        ///     	* hint: click upper left corner of spreadsheet, then double-click any column border to expand all columns. 
        ///     	* When done making changes, save as .csv, ignore dire warnings about formatting loss. When closing excel, continue save and say Yes when asked (questions will be repeated)
        ///         (check in as normal when all work complete)
        /// The test data consists of the following data:
        ///     TestNumber: Sequence number simply enables quick determination in debugger which test your are on
        ///     Phrase: The (approximate) group degree audit language to describe the test scenario--for reference only
        ///     Taken: Academic credits to be used as input to eval, using course name. The TestAcademicCredit repo provides logic
        ///         in the method that will take course names and select academic credits from the static test repository. (Use asterisk
        ///         delimiter between subject and number). Note that each course named here represents an academic credit, so this tool cannot
        ///         be used to test scenarios that specifically involve planned courses or duplicate takes of the same course.
        ///     Result: The expected group evaluation result, must be a value from GroupExplanation enum (found at the bottom of GroupResult.cs)
        ///     TestFile: The ID of the group to use for the test, set up as a case option in the BuildGroup method in the TestProgramRequirementsRepository.
        ///         This is the spec-version of the DA language, should reflect the Phrase information and whatever other specifications are needed to complete
        ///         the group entity to accurately reflect the scenario.
        ///     ResultDetails: Each course name (representing AcadResult) and the associated Result enum value expected for each item.
        /// </summary>
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "GroupTestsData.csv", "GroupTestsData#csv", DataAccessMethod.Sequential), DeploymentItem("Ellucian.Colleague.Domain.Student.Tests\\Entities\\Requirements\\TestData\\GroupTestsData.csv"), TestMethod]
        public async Task GroupTest()
        {

            int testNumber = System.Convert.ToInt32(TestContext.DataRow["TestNumber"]);
            // if you need to test a specific test, set up an if statement + breakpoint combo to stop when testNumber == <desiredTestNumber>
            string phrase = System.Convert.ToString(TestContext.DataRow["Phrase"]);
            string strTaken = System.Convert.ToString(TestContext.DataRow["Taken"]);
            string strPlanned = System.Convert.ToString(TestContext.DataRow["Planned"]);
            string result = System.Convert.ToString(TestContext.DataRow["Result"]);//.ToUpper();
            string testFile = System.Convert.ToString(TestContext.DataRow["TestFile"]); // Case sensitive
            string resultDetails = System.Convert.ToString(TestContext.DataRow["ResultDetails"]);//.ToUpper();

            if (string.IsNullOrEmpty(testFile))
            {
                // no data yet
                return;
            }

            Console.WriteLine("TestNumber: " + testNumber);
            Console.WriteLine("TestFile: " + testFile);
            Console.WriteLine("Phrase: " + phrase);
            Console.WriteLine("Taken: " + strTaken);
            Console.WriteLine("Expected Group Result: " + result);


            ////for breakpoint on particular test
            //if (testNumber == 253)
            //{
            //    Console.WriteLine("Breaking on test " + testNumber);
            //}


            Group group;
            // Student student = TestUtil.FromJson<Student>(TestUtil.GetAssemblyName(), "Entities.Requirements.TestData.0000896.js");

            var allCredits = new TestAcademicCreditRepository().GetAsync().Result;
            var equatedCourses = new TestCourseRepository().GetAsync().Result;
            var allPlannedCourses = GetPlannedCourses(allCredits, equatedCourses);

            // This replaces the JSON group data from the TestXX.js files   
            // This must now be mocked up properly even though we are just testing the group.

            List<string> reqnames = new List<string>();
            List<string> subnames = new List<string>();
            List<string> grpnames = new List<string>() { testFile };
            Dictionary<string, List<string>> Subreq = new Dictionary<string, List<string>>();
            Dictionary<string, List<string>> groups = new Dictionary<string, List<string>>();

            // This is confusing but it makes sense in its own way
            reqnames.Add("GroupTestRequirement");           // list of requirement names to handle
            subnames.Add("GroupTestSubrequirement");        // list of Subrequirement names
            Subreq.Add(reqnames[0], subnames);              // dictionary as pointer from requirement name to list of Subrequirement names under it
            grpnames.Add(testFile);                         // list of group names
            groups.Add(subnames[0], grpnames);              // dictionary as pointer from Subrequirement name to list of group names under it

            ProgramRequirements pr = await new TestProgramRequirementsRepository().BuildTestProgramRequirementsAsync(testFile, reqnames, Subreq, groups);


            group = pr.Requirements.First().SubRequirements.First().Groups.First();

            List<AcademicCredit> taken = GetCredits(strTaken, allCredits.ToList());
            List<PlannedCredit> plannedCourses = GetPlannedCredits(strPlanned, allPlannedCourses.ToList());

            //  ****** this is where the magic happens
            // First add the credits taken from the specified test
            List<AcadResult> results = new List<AcadResult>();
            foreach (var cred in taken)
            {
                results.Add(new CreditResult(cred));
            }
            // Next add any planned courses from the specified test
            List<AcadResult> plannedResults = new List<AcadResult>();
            foreach (var pc in plannedCourses)
            {
                results.Add(new CourseResult(pc));
            }

            var allRules = group.GetRules();

            var requests = new List<RuleRequest<AcademicCredit>>();
            foreach (var rule in allRules)
            {
                foreach (var credit in taken)
                {
                    requests.Add(new RuleRequest<AcademicCredit>(rule.CreditRule, credit));
                }
            }
            var ruleResults = new TestRuleRepository2().Execute(requests);
            foreach (var rule in allRules)
            {
                var matchingResult = ruleResults.FirstOrDefault(rr => rr.RuleId == rule.CreditRule.Id);
                if (matchingResult != null)
                {
                    rule.SetAnswer(matchingResult.Passed, matchingResult.Context);
                }
            }

            // **********
            
            GroupResult gr = group.Evaluate(results, new List<Override> {new Override("10003",new List<string>(){"73"},null) }, equatedCourses);

            // **********

            Assert.IsNotNull(gr);


            List<Tuple<AcademicCredit, PlannedCredit, Result>> expectedResults = ParseResultDetails(resultDetails, taken, plannedCourses);
            if (testNumber != 183)
            {
                Assert.AreEqual(taken.Count + plannedCourses.Count, expectedResults.Count, "Sanity check the ResultDetails column, should have all credits or planned courses listed");
            }
            foreach (var expectedResultTest in expectedResults)
            {
                Result expectedResultForTest = expectedResultTest.Item3;
                if (expectedResultTest.Item1 != null)
                {
                    var credit = expectedResultTest.Item1;
                    var creditResults = gr.Results.OfType<CreditResult>().ToList();
                    AcadResult ar = creditResults.FirstOrDefault(crr => ((CreditResult)crr).Credit.Equals(credit));
                    Assert.IsNotNull(ar);
                    try
                    {
                        Assert.AreEqual(expectedResultForTest, ar.Result); //, "Result for " + cr.Credit.Course + " should match");
                    }
                    catch (AssertFailedException)
                    {
                        Visit(gr);
                        throw new AssertFailedException("Expected academic credit result " + expectedResultForTest.ToString() + " does not match actual result " + ar.Result.ToString() +
                                                        " for course " + ar.GetCourse().ToString() + ".");
                    }
                }
                else
                {
                    // Since there is no academic credit this must be a planned course.
                    var pc = expectedResultTest.Item2;
                    var courseResults = gr.Results.OfType<CourseResult>().ToList();
                    AcadResult ar = courseResults.FirstOrDefault(prr => ((CourseResult)prr).PlannedCourse.Equals(pc));
                    try
                    {
                        Assert.AreEqual(expectedResultForTest, ar.Result); //, "Result for " + plannedCourse + " should match");
                    }
                    catch (AssertFailedException)
                    {
                        Visit(gr);
                        throw new AssertFailedException("Expected planned credit result " + expectedResultForTest.ToString() + " does not match actual result " + ar.Result.ToString() +
                                                        " for course " + ar.GetCourse().ToString() + ".");
                    }
                }
            }
            

            string actualresults = "";
            foreach (GroupExplanation ex in gr.Explanations)
            {
                if (!(actualresults == ""))
                {
                    actualresults += ",";
                }
                actualresults += ex.ToString();
            }


            Console.WriteLine("Actual Group Results " + actualresults);

            // Assert all expected group results are present
            string[] expectedGroupResults = result.Split(',');
            foreach (string expectedGroupResult in expectedGroupResults)
            {
                GroupExplanation e = (GroupExplanation)Enum.Parse(typeof(GroupExplanation), expectedGroupResult);
                try
                {
                    Assert.IsTrue(gr.Explanations.Contains(e));
                }
                catch (AssertFailedException)
                {
                    Visit(gr);
                    throw new AssertFailedException("Expected result " + expectedGroupResult + " not in actual results " + actualresults + ".");
                }

                // free(e);    haha just kidding
            }

            // Assert all group results present are expected
            foreach (GroupExplanation ee in gr.Explanations)
            {
                //Assert.IsTrue(expectedGroupResults.Contains(ee.ToString()));
                try
                {
                    Assert.IsTrue(expectedGroupResults.Contains(ee.ToString()));
                }
                catch (AssertFailedException)
                {
                    Visit(gr);
                    throw new AssertFailedException("Actual result " + ee.ToString() + " not in expected results " + result + ".");
                }
            }

            // Visit results for details
            Console.WriteLine("Individial Academic Goal Contributor (AGC) results:");
            Visit(gr);

        }

        private static List<Tuple<AcademicCredit, PlannedCredit, Result>> ParseResultDetails(string resultDetails, ICollection<AcademicCredit> credits, ICollection<PlannedCredit> plannedCourses)
        {
            List<Tuple<AcademicCredit, PlannedCredit, Result>> results = new List<Tuple<AcademicCredit, PlannedCredit, Result>>();
            if (!string.IsNullOrEmpty(resultDetails))
            {
                string[] assignments = resultDetails.Split(',');
                foreach (var assignment in assignments)
                {
                    var expectedResult = assignment;
                    bool isPlanned = false;

                    if (expectedResult.IndexOf("P:") >= 0)
                    {
                        isPlanned = true;
                        expectedResult = expectedResult.Substring(2);
                    }
                    string courseId = expectedResult.Split('=')[0];
                    string result = expectedResult.Split('=')[1];
                    PlannedCredit pc = null;
                    AcademicCredit credit = null;
                    if (isPlanned)
                    {
                        pc = GetPlannedCredit(courseId, plannedCourses);
                        Assert.IsNotNull(pc);
                    }
                    else
                    {
                        credit = GetCredit(courseId, credits);
                        Assert.IsNotNull(credit);
                    }
                    Result r = (Result)Enum.Parse(typeof(Result), result);
                    Assert.IsNotNull(r);
                    results.Add(Tuple.Create<AcademicCredit, PlannedCredit, Result>(credit, pc, r));
                }
            }
            return results;
        }

        private static List<string> GetCourseIds(string strCourseIds)
        {
            List<string> ids = new List<string>();
            if (!string.IsNullOrEmpty(strCourseIds))
            {
                ids.AddRange(strCourseIds.Split(','));
            }
            return ids;
        }

        private List<AcademicCredit> GetCredits(string strTaken, ICollection<AcademicCredit> credits)
        {
            List<AcademicCredit> taken = new List<AcademicCredit>();
            foreach (var courseId in GetCourseIds(strTaken))
            {
                taken.Add(GetCredit(courseId, credits));
            }
            return taken;
        }

        private List<PlannedCredit> GetPlannedCredits(string strTaken, ICollection<PlannedCredit> plannedCredits)
        {
            List<PlannedCredit> planned = new List<PlannedCredit>();
            foreach (var courseId in GetCourseIds(strTaken))
            {
                planned.Add(GetPlannedCredit(courseId, plannedCredits));
            }
            return planned;
        }

        private static AcademicCredit GetCredit(string courseId, ICollection<AcademicCredit> credits)
        {
            // Handle MATH-101 and MATH 101 as well as MATH*101

            courseId.Replace("-", "*");
            courseId.Replace(" ", "*");

            string subj = courseId.Trim().Split('*')[0];
            string num = courseId.Trim().Split('*')[1];

            return credits.First(ac => ac.Course.SubjectCode == subj && ac.Course.Number == num);
        }

        private static PlannedCredit GetPlannedCredit(string courseId, ICollection<PlannedCredit> plannedCredits)
        {
            // Handle MATH-101 and MATH 101 as well as MATH*101

            courseId.Replace("-", "*");
            courseId.Replace(" ", "*");

            string subj = courseId.Trim().Split('*')[0];
            string num = courseId.Trim().Split('*')[1];
            var plannedCourse = plannedCredits.First(pc => pc.Course.SubjectCode == subj && pc.Course.Number == num);
            return plannedCourse;
        }

        private static Group GetFirstGroup(ProgramRequirements pr)
        {
            return pr.Requirements.First(r => r.Id == "BSV.REQ1").SubRequirements.First(sr => sr.Id == "938").Groups.First();
        }

        // RTM FIX
        //public void Visit(ProgramEvaluation programResult)
        //{
        //    throw new NotImplementedException();
        //}

        public void Visit(RequirementResult requirementResult)
        {
            throw new NotImplementedException();
        }

        public void Visit(SubrequirementResult SubrequirementResult)
        {
            throw new NotImplementedException();
        }

        public void Visit(GroupResult gr)
        {

            foreach (var cr in gr.Results)
            {
                string type = cr.GetType() == typeof(CourseResult) ? "Planned Course: " : "Academic Credit: ";
                Console.WriteLine(type + cr.GetCourse().ToString() + "  " + cr.Result.ToString());
            }
        }
        // Convert the academic credits into viable planned courses.
        private List<PlannedCredit> GetPlannedCourses(IEnumerable<AcademicCredit> allCredits, IEnumerable<Course> allCourses)
        {
            var plannedCourses = new List<PlannedCredit>();
            foreach (var credit in allCredits)
            {
                if (credit.Course != null)
                {
                    string termCode = "Future";
                    var course = allCourses.Where(c => c.Id == credit.Course.Id).FirstOrDefault();
                    if (course != null)
                    {
                        var plannedCourse = new PlannedCredit(course, termCode);
                        plannedCourse.Credits = credit.Credit;
                        plannedCourses.Add(plannedCourse);
                    }

                }

            }

            return plannedCourses;
        }
    }

    [TestClass]
    public class GetRules
    {
        private Requirement requirement;
        private Subrequirement subrequirement;
        private Group group;

        [TestInitialize]
        public void Initialize()
        {
            requirement = new Requirement("1", "R1", "Req1", "UG", null);
            subrequirement = new Subrequirement("2", "subreq2");
            subrequirement.Requirement = requirement;
            group = new Group("1", "group1", subrequirement);
        }

        [TestMethod]
        public void GetsAllRules()
        {
            group.AcademicCreditRules = new List<RequirementRule>() { new RequirementRule(new Rule<AcademicCredit>("rule1")), new RequirementRule(new Rule<AcademicCredit>("rule2")) };
            group.MaxCoursesRule = new RequirementRule(new Rule<AcademicCredit>("rule3"));
            group.MaxCreditsRule = new RequirementRule(new Rule<AcademicCredit>("rule4"));
            var rules = group.GetRules();
            Assert.AreEqual(4, rules.Count());
            Assert.IsTrue(rules.Contains(group.AcademicCreditRules.ElementAt(0)));
            Assert.IsTrue(rules.Contains(group.AcademicCreditRules.ElementAt(1)));
            Assert.IsTrue(rules.Contains(group.MaxCoursesRule));
            Assert.IsTrue(rules.Contains(group.MaxCreditsRule));
        }

        [TestMethod]
        public void ReturnsEmptyListIfNoRules()
        {
            var rules = group.GetRules();
            Assert.AreEqual(0, rules.Count());
        }

        [TestMethod]
        public void ReturnsEmptyListExclusions()
        {
            Assert.AreEqual(0, group.Exclusions.Count());
        }
    }

    [TestClass]
    public class Group_GroupTypeEvalSequence
    {
        private Requirement requirement;
        private Subrequirement subreq;

        [TestInitialize]
        public void Initialize()
        {
            requirement = new Requirement("1", "R1", "Req1", "UG", null);
            subreq = new Subrequirement("11", "SUBREQ1");
            subreq.Requirement = requirement;
        }

        [TestMethod]
        public void SortsTakeAllFirst()
        {
            var g = new Group("1", "GROUP1", subreq);
            g.GroupType = GroupType.TakeAll;
            Assert.AreEqual(1, g.GroupTypeEvalSequence);
        }

        [TestMethod]
        public void SortsTakeSelectedSecond()
        {
            var g = new Group("1", "GROUP1", subreq);
            g.GroupType = GroupType.TakeSelected;
            Assert.AreEqual(2, g.GroupTypeEvalSequence);
        }

        [TestMethod]
        public void SortsTakeCoursesThird()
        {
            var g = new Group("1", "GROUP1", subreq);
            g.GroupType = GroupType.TakeCourses;
            Assert.AreEqual(3, g.GroupTypeEvalSequence);
        }

        [TestMethod]
        public void SortsTakeCreditsThird()
        {
            var g = new Group("1", "GROUP1", subreq);
            g.GroupType = GroupType.TakeCredits;
            Assert.AreEqual(3, g.GroupTypeEvalSequence);
        }

    }

    [TestClass]
    public class HasAcademicCreditBasedRules
    {
        private Subrequirement subreq;
        private Requirement requirement;

        [TestInitialize]
        public void Initialize()
        {
            subreq = new Subrequirement("11", "SUBREQ1");
            requirement = new Requirement("1", "R1", "Req1", "UG", null);
            subreq.Requirement = requirement;
        }

        [TestMethod]
        public void HasAcademicCreditBasedRules_TrueIfCombination()
        {
            var group = new Group("1", "group1", subreq);
            group.AcademicCreditRules = new List<RequirementRule>() { new RequirementRule(new Rule<Course>("rule1")), new RequirementRule(new Rule<AcademicCredit>("rule2")) };
            group.MaxCoursesRule = new RequirementRule(new Rule<AcademicCredit>("rule3"));
            group.MaxCreditsRule = new RequirementRule(new Rule<AcademicCredit>("rule4"));
            var rules = group.GetRules();
            Assert.AreEqual(4, rules.Count());
            Assert.IsTrue(group.HasAcademicCreditBasedRules);
        }

        [TestMethod]
        public void HasAcademicCreditBasedRules_FalseIfOnlyCourseRules()
        {
            var group = new Group("1", "group1", subreq);
            group.AcademicCreditRules = new List<RequirementRule>() { new RequirementRule(new Rule<Course>("rule1")), new RequirementRule(new Rule<Course>("rule2")) };
            group.MaxCoursesRule = new RequirementRule(new Rule<AcademicCredit>("rule3"));
            group.MaxCreditsRule = new RequirementRule(new Rule<AcademicCredit>("rule4"));
            var rules = group.GetRules();
            Assert.AreEqual(4, rules.Count());
            Assert.IsFalse(group.HasAcademicCreditBasedRules);
        }

        [TestMethod]
        public void HasAcademicCreditBasedRules_FalseIfNoRules()
        {
            var group = new Group("1", "group1", subreq);
            group.MaxCoursesRule = new RequirementRule(new Rule<AcademicCredit>("rule3"));
            group.MaxCreditsRule = new RequirementRule(new Rule<AcademicCredit>("rule4"));
            var rules = group.GetRules();
            Assert.AreEqual(2, rules.Count());
            Assert.IsFalse(group.HasAcademicCreditBasedRules);
        }

        [TestMethod]
        public void HasAcademicCreditBasedRules_FalseIfNullRules()
        {
            var group = new Group("1", "group1", subreq);
            group.AcademicCreditRules = null;
            group.MaxCoursesRule = new RequirementRule(new Rule<AcademicCredit>("rule3"));
            group.MaxCreditsRule = new RequirementRule(new Rule<AcademicCredit>("rule4"));
            var rules = group.GetRules();
            Assert.AreEqual(2, rules.Count());
            Assert.IsFalse(group.HasAcademicCreditBasedRules);
        }
    }


}

