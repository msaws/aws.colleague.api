﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Domain.Student.Entities;

namespace Ellucian.Colleague.Domain.Student.Tests.Entities
{
    [TestClass]
    public class BookTests
    {
        Book book;
        
        [TestInitialize]
        public void Initialize()
        {
            book = new Book("111") { Price = 30.00m };
        }

        [TestMethod]
        public void Id()
        {
            Assert.AreEqual("111", book.Id);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void IdNullException()
        {
            Book newBook = new Book(null);
        }

        [TestMethod]
        public void Price()
        {
            Assert.AreEqual(30.00m, book.Price);
        }

        [TestMethod]
        public void NullPrice()
        {
            Book anotherBook = new Book("222");
            Assert.IsNull(anotherBook.Price);
        }
    }
}
