﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Domain.Student.Entities;

namespace Ellucian.Colleague.Domain.Student.Tests.Entities
{
    [TestClass]
    public class CourseStatusItemTests
    {
        [TestClass]
        public class CourseStatusItem_Constructor
        {
            private string guid;
            private string code;
            private string desc;
            private CourseStatusItem csItem;

            [TestInitialize]
            public void Initialize()
            {
                guid = GetGuid();
                code = "A";
                desc = "Active";
                csItem = new CourseStatusItem(guid, code, desc);
            }

            [TestMethod]
            public void CourseStatusItem_Guid()
            {
                Assert.AreEqual(guid, csItem.Guid);
            }

            [TestMethod]
            public void CourseStatusItem_Code()
            {
                Assert.AreEqual(code, csItem.Code);
            }

            [TestMethod]
            public void CourseStatusItem_Description()
            {
                Assert.AreEqual(desc, csItem.Description);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CourseStatusItem_GuidNullException()
            {
                new CourseStatusItem(null, code, desc);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CourseStatusItem_GuidEmptyException()
            {
                new CourseStatusItem(string.Empty, code, desc);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CourseStatusItem_CodeNullException()
            {
                new CourseStatusItem(guid, null, desc);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CourseStatusItemCodeEmptyException()
            {
                new CourseStatusItem(guid, string.Empty, desc);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CourseStatusItemDescEmptyException()
            {
                new CourseStatusItem(guid, code, string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CourseStatusItem_DescNullException()
            {
                new CourseStatusItem(guid, code, null);
            }

        }

        private static string GetGuid()
        {
            return Guid.NewGuid().ToString();
        }
    }
}