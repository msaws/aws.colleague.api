﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Domain.Student.Entities;

namespace Ellucian.Colleague.Domain.Student.Tests.Entities
{
    [TestClass]
    public class GradeTests
    {
        [TestClass]
        public class GradeConstructor
        {
            private string guid;
            private string id;
            private string letterGrade;
            private string credit;
            private string desc;
            private string schema;
            private Grade grade;

            [TestInitialize]
            public void Initialize()
            {
                guid = Guid.NewGuid().ToString();
                id = "1";
                letterGrade = "A";
                credit = "4";
                desc = "Undergraduate";
                schema = "UG";
                grade = new Grade(guid, id, letterGrade, credit, desc, schema);
            }

            [TestMethod]
            public void GradeGuid()
            {
                Assert.AreEqual(guid, grade.Guid);
            }

            [TestMethod]
            public void GradeId()
            {
                Assert.AreEqual(id, grade.Id);
            }

            [TestMethod]
            public void GradeLetterGrade()
            {
                Assert.AreEqual(letterGrade, grade.LetterGrade);
            }

            [TestMethod]
            public void GradeCredit()
            {
                Assert.AreEqual(credit, grade.Credit);
            }

            [TestMethod]
            public void GradeCode()
            {
                Assert.AreEqual(schema, grade.GradeSchemeCode);
            }

            [TestMethod]
            public void GradeDescription()
            {
                Assert.AreEqual(desc, grade.Description);
            }

            [TestMethod]
            public void GradeExcludeFromFacultyGrading_DefaultValue()
            {
                Assert.IsFalse(grade.ExcludeFromFacultyGrading);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void GradeGuidNullException()
            {
                new Grade(null, id, letterGrade, credit, desc, schema);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void GradeIdNullException()
            {
                new Grade(guid, null, letterGrade, credit, desc, schema);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void GradeCodeNullException()
            {
                new Grade(guid, id, letterGrade, credit, desc, null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void GradeDescNullException()
            {
                new Grade(guid, id, letterGrade, credit, null, schema);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void GradeLetterGradeNullException()
            {
                new Grade(guid, id, null, credit, desc, schema);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void GradeGuidEmptyException()
            {
                new Grade(string.Empty, id, letterGrade, credit, desc, schema);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void GradeIdEmptyException()
            {
                new Grade(guid, string.Empty, letterGrade, credit, desc, schema);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void GradeDescEmptyException()
            {
                new Grade(guid, id, letterGrade, credit, string.Empty, schema);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void GradeLetterGradeEmptyException()
            {
                new Grade(guid, id, string.Empty, credit, desc, schema);
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidOperationException))]
            public void GradeId2()
            {
                Grade grade = new Grade("2", letterGrade, desc, schema);
                grade.Id = "1";
            }

            [TestMethod]
            public void GradeIdNull2Exception()
            {
                Grade grade = new Grade(null, letterGrade, desc, schema);
                grade.Id = "1";
            }
        }
    }
}