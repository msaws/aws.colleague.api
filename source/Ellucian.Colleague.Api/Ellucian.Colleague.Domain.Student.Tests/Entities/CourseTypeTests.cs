﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Student.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Colleague.Domain.Student.Tests.Entities
{
    [TestClass]
    public class CourseTypeTests
    {
        [TestClass]
        public class CourseTypeConstructor
        {
            private string code;
            private string desc;
            private CourseType courseType;

            [TestInitialize]
            public void Initialize()
            {
                code = "STND";
                desc = "Standard";
                courseType = new CourseType(code, desc);
            }

            [TestMethod]
            public void CourseTypeCode()
            {
                Assert.AreEqual(code, courseType.Code);
            }

            [TestMethod]
            public void CourseTypeDescription()
            {
                Assert.AreEqual(desc, courseType.Description);
            }

            [TestMethod]
            public void CourseTypeShowInCatalog_TrueByDefault()
            {
                Assert.IsTrue(courseType.ShowInCourseSearch);
            }

            [TestMethod]
            public void CourseTypeShowInCatalog_SetToFalse()
            {
                var testCourseType = new CourseType(code, desc, false);
                Assert.IsFalse(testCourseType.ShowInCourseSearch);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CourseTypeCodeNullException()
            {
                new CourseType(null, desc);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CourseTypeDescNullException()
            {
                new CourseType(code, null);
            }

        }

        [TestClass]
        public class CourseTypeEquals
        {
            private string code;
            private string desc;
            private CourseType courseType1;
            private CourseType courseType2;
            private CourseType courseType3;

            [TestInitialize]
            public void Initialize()
            {
                code = "STND";
                desc = "Standard";
                courseType1 = new CourseType(code, desc);
                courseType2 = new CourseType(code, "Coop Work Experience");
                courseType3 = new CourseType("COOP", desc);
            }

            [TestMethod]
            public void CourseTypeSameCodesEqual()
            {
                Assert.IsTrue(courseType1.Equals(courseType2));
            }

            [TestMethod]
            public void CourseTypeDifferentCodeNotEqual()
            {
                Assert.IsFalse(courseType1.Equals(courseType3));
            }
        }

        [TestClass]
        public class CourseTypeGetHashCode
        {
            private string code;
            private string desc;
            private CourseType courseType1;
            private CourseType courseType2;
            private CourseType courseType3;

            [TestInitialize]
            public void Initialize()
            {
                code = "STND";
                desc = "Standard";
                courseType1 = new CourseType(code, desc);
                courseType2 = new CourseType(code, "Coop Work Experience");
                courseType3 = new CourseType("COOP", desc);
            }

            [TestMethod]
            public void CourseTypeSameCodeHashEqual()
            {
                Assert.AreEqual(courseType1.GetHashCode(), courseType2.GetHashCode());
            }

            [TestMethod]
            public void CourseTypeDifferentCodeHashNotEqual()
            {
                Assert.AreNotEqual(courseType1.GetHashCode(), courseType3.GetHashCode());
            }
        }
    }
}
