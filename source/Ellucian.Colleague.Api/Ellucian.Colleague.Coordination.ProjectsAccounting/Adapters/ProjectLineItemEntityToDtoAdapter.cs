﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Adapters;
using slf4net;

namespace Ellucian.Colleague.Coordination.ProjectsAccounting.Adapters
{
    /// <summary>
    /// Adapter for mapping the line item domain entity to a line item DTO.
    /// </summary>
    public class ProjectLineItemEntityToDtoAdapter : AutoMapperAdapter<Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectLineItem, Ellucian.Colleague.Dtos.ProjectsAccounting.ProjectLineItem>
    {
        public ProjectLineItemEntityToDtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
        }

        public ProjectLineItem MapToType(Domain.ProjectsAccounting.Entities.ProjectLineItem Source, string sequenceNumber)
        {
            var projectLineItemDto = new ProjectLineItem();
            projectLineItemDto.ItemCode = Source.ItemCode;

            // Translate the domain GlClass into the DTO GlClass
            switch (Source.GlClass)
            {
                case Ellucian.Colleague.Domain.ColleagueFinance.Entities.GlClass.Asset:
                    projectLineItemDto.GlClass = Dtos.ColleagueFinance.GlClass.Asset;
                    break;
                case Ellucian.Colleague.Domain.ColleagueFinance.Entities.GlClass.Expense:
                    projectLineItemDto.GlClass = Dtos.ColleagueFinance.GlClass.Expense;
                    break;
                case Ellucian.Colleague.Domain.ColleagueFinance.Entities.GlClass.FundBalance:
                    projectLineItemDto.GlClass = Dtos.ColleagueFinance.GlClass.FundBalance;
                    break;
                case Ellucian.Colleague.Domain.ColleagueFinance.Entities.GlClass.Liability:
                    projectLineItemDto.GlClass = Dtos.ColleagueFinance.GlClass.Liability;
                    break;
                case Ellucian.Colleague.Domain.ColleagueFinance.Entities.GlClass.Revenue:
                    projectLineItemDto.GlClass = Dtos.ColleagueFinance.GlClass.Revenue;
                    break;
            }

            projectLineItemDto.TotalActuals = Source.TotalActuals(sequenceNumber);
            projectLineItemDto.TotalBudget = Source.TotalBudget(sequenceNumber);
            projectLineItemDto.TotalEncumbrances = Source.TotalEncumbrances(sequenceNumber);
            projectLineItemDto.TotalMemoActuals = Source.TotalMemoActuals(sequenceNumber);
            projectLineItemDto.TotalMemoEncumbrances = Source.TotalMemoEncumbrances(sequenceNumber);
            projectLineItemDto.GlAccounts = new List<ProjectLineItemGlAccount>();

            return projectLineItemDto;
        }
    }
}

