﻿// Copyright 2014 - 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Adapters;
using slf4net;

namespace Ellucian.Colleague.Coordination.ProjectsAccounting.Adapters
{
    /// <summary>
    /// Adapter for mapping from the budget period domain entity to a DTO.
    /// </summary>
    public class ProjectBudgetPeriodEntityToDtoAdapter : AutoMapperAdapter<Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectBudgetPeriod, Ellucian.Colleague.Dtos.ProjectsAccounting.ProjectBudgetPeriod>
    {
        public ProjectBudgetPeriodEntityToDtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
        }

        public override Dtos.ProjectsAccounting.ProjectBudgetPeriod MapToType(Domain.ProjectsAccounting.Entities.ProjectBudgetPeriod Source)
        {
            var budgetPeriodDto = new ProjectBudgetPeriod();
            budgetPeriodDto.SequenceNumber = Source.SequenceNumber;
            budgetPeriodDto.StartDate = Source.StartDate;
            budgetPeriodDto.EndDate = Source.EndDate;

            return budgetPeriodDto;
        }
    }
}
