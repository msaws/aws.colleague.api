﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Web.Adapters;
using slf4net;

namespace Ellucian.Colleague.Coordination.ProjectsAccounting.Adapters
{
    /// <summary>
    /// Adapter for mapping from the Project entity to DTO.
    /// </summary>
    public class ProjectEntityToDtoAdapter : AutoMapperAdapter<Ellucian.Colleague.Domain.ProjectsAccounting.Entities.Project, Ellucian.Colleague.Dtos.ProjectsAccounting.Project>
    {
        public ProjectEntityToDtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
        }

        /// <summary>
        /// Convert a project domain entity and all of its descendent objects into DTOs.
        /// </summary>
        /// <param name="Source">Source project domain entity to be converted.</param>
        /// <param name="summaryOnly">Boolean flag used to determine whether or not detailed information is for the project.</param>
        /// <param name="sequenceNumber">Project budget period sequence number used to calculate project totals.</param>
        /// <param name="glMajorComponentStartPositions">List of GL major component start positions to properly format GL numbers.</param>
        /// <returns>Project DTO.</returns>
        public Dtos.ProjectsAccounting.Project MapToType(Domain.ProjectsAccounting.Entities.Project Source, bool summaryOnly, string sequenceNumber, IEnumerable<string> glMajorComponentStartPositions)
        {
            // Copy the project-level properties.
            var projectDto = new Dtos.ProjectsAccounting.Project();
            projectDto.Id = Source.ProjectId;
            projectDto.Number = Source.Number;
            projectDto.Title = Source.Title;

            // Translate the domain status into the DTO status
            switch (Source.Status)
            {
                case Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectStatus.Active:
                    projectDto.Status = Dtos.ProjectsAccounting.ProjectStatus.Active;
                    break;
                case Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectStatus.Inactive:
                    projectDto.Status = Dtos.ProjectsAccounting.ProjectStatus.Inactive;
                    break;
                case Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectStatus.Closed:
                    projectDto.Status = Dtos.ProjectsAccounting.ProjectStatus.Closed;
                    break;
            }

            projectDto.TotalBudgetRevenue = Source.TotalBudgetRevenue(sequenceNumber);
            projectDto.TotalActualsRevenue = Source.TotalActualsRevenue(sequenceNumber);
            projectDto.TotalEncumbrancesRevenue = Source.TotalEncumbranceRevenue(sequenceNumber);
            projectDto.TotalActualsAssets = Source.TotalActualsAsset(sequenceNumber);
            projectDto.TotalEncumbrancesAssets = Source.TotalEncumbranceAsset(sequenceNumber);
            projectDto.TotalActualsLiabilities = Source.TotalActualsLiability(sequenceNumber);
            projectDto.TotalEncumbrancesLiabilities = Source.TotalEncumbranceLiability(sequenceNumber);
            projectDto.TotalActualsFundBalance = Source.TotalActualsFundBalance(sequenceNumber);
            projectDto.TotalEncumbrancesFundBalance = Source.TotalEncumbranceFundBalance(sequenceNumber);
            projectDto.TotalBudget = Source.TotalBudgetExpenses(sequenceNumber);
            projectDto.TotalActuals = Source.TotalActualsExpenses(sequenceNumber);
            projectDto.TotalEncumbrances = Source.TotalEncumbranceExpenses(sequenceNumber);
            projectDto.TotalExpenses = Source.TotalExpenses(sequenceNumber);
            projectDto.BudgetPeriods = new List<Dtos.ProjectsAccounting.ProjectBudgetPeriod>();
            projectDto.LineItems = new List<Dtos.ProjectsAccounting.ProjectLineItem>();

            // Initialize all necessary adapters to convert the descendent elements within the project.
            var budgetPeriodDtoAdapter = new ProjectBudgetPeriodEntityToDtoAdapter(adapterRegistry, logger);
            var projectLineItemDtoAdapter = new ProjectLineItemEntityToDtoAdapter(adapterRegistry, logger);
            var projectLineItemGlAccountDtoAdapter = new ProjectLineItemGlAccountEntityToDtoAdapter(adapterRegistry, logger);
            var glTransactionAdapter = new AutoMapperAdapter<Domain.ColleagueFinance.Entities.GlTransaction, Dtos.ColleagueFinance.GlTransaction>(adapterRegistry, logger);

            // Convert the budget period domain entities into DTOs.
            foreach (var budgetPeriod in Source.BudgetPeriods)
            {
                // Add the budget periods DTOs to the project DTO.
                projectDto.BudgetPeriods.Add(budgetPeriodDtoAdapter.MapToType(budgetPeriod));
            }

            // Convert the project line item domain entities into DTOS.
            foreach (var lineItem in Source.LineItems)
            {
                // First convert the properties to DTOs.
                var projectLineItemDto = projectLineItemDtoAdapter.MapToType(lineItem, sequenceNumber);

                // Now convert each GL account domain entity into a DTO.
                foreach (var glAccount in lineItem.GlAccounts)
                {
                    var glAccountDto = projectLineItemGlAccountDtoAdapter.MapToType(glAccount, glMajorComponentStartPositions);

                    foreach (var transaction in glAccount.ProjectTransactions)
                    {
                        var transactionDto = glTransactionAdapter.MapToType(transaction);
                        if (transaction.GlTransactionType == GlTransactionType.Actual)
                        {
                            // Add actuals transactions to the actuals list.
                            glAccountDto.ActualsGlTransactions.Add(transactionDto);

                        }
                        else
                        // Encumbrances and requisitions go together in the list.
                        // There are no budget transactions.
                        {
                            glAccountDto.EncumbranceGlTransactions.Add(transactionDto);
                        }
                    }

                    // Add the GL account DTOs to the line item DTO.
                    projectLineItemDto.GlAccounts.Add(glAccountDto);
                }

                // Add the project line item DTO to the project DTO.
                projectDto.LineItems.Add(projectLineItemDto);
            }

            return projectDto;
        }
    }
}
