﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using Ellucian.Colleague.Dtos.ColleagueFinance;
using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Web.Adapters;
using slf4net;

namespace Ellucian.Colleague.Coordination.ProjectsAccounting.Adapters
{
    /// <summary>
    /// Adapter for mapping from the ProjectLineItemGlAccount entity to DTO.
    /// </summary>
    public class ProjectLineItemGlAccountEntityToDtoAdapter : AutoMapperAdapter<Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectLineItemGlAccount, Ellucian.Colleague.Dtos.ProjectsAccounting.ProjectLineItemGlAccount>
    {
        public ProjectLineItemGlAccountEntityToDtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
        }

        public Dtos.ProjectsAccounting.ProjectLineItemGlAccount MapToType(Domain.ProjectsAccounting.Entities.ProjectLineItemGlAccount Source, IEnumerable<string> majorComponentStartPosition)
        {
            var glAccountDto = new ProjectLineItemGlAccount();
            glAccountDto.GlAccount = Source.GlAccountNumber;
            glAccountDto.FormattedGlAccount = Source.GetFormattedGlAccount(majorComponentStartPosition);
            glAccountDto.Description = Source.GlAccountDescription;
            glAccountDto.Actuals = Source.Actuals;
            glAccountDto.Encumbrances = Source.Encumbrances;
            glAccountDto.ActualsGlTransactions = new List<GlTransaction>();
            glAccountDto.EncumbranceGlTransactions = new List<GlTransaction>();

            return glAccountDto;
        }
    }
}
