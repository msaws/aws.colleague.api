﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.ProjectsAccounting.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;

namespace Ellucian.Colleague.Coordination.ProjectsAccounting.Services
{
    /// <summary>
    /// This class implements the IProjectItemCodeService interface
    /// </summary>
    [RegisterType]
    public class ProjectItemCodeService : BaseCoordinationService, IProjectItemCodeService
    {
        private IProjectsAccountingReferenceDataRepository projectsAccountingReferenceRepository;

        // This constructor initializes the private attributes.
        public ProjectItemCodeService(IProjectsAccountingReferenceDataRepository projectsAccountingReferenceRepository, IAdapterRegistry adapterRegistry, ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.projectsAccountingReferenceRepository = projectsAccountingReferenceRepository;
        }

        /// <summary>
        /// Returns a list of project item codes.
        /// </summary>
        /// <returns>A list of project item codes.</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.ProjectsAccounting.ProjectItemCode>> GetProjectItemCodesAsync()
        {
            // Get the domain entities
            var projectItemCodesDomainEntities = await projectsAccountingReferenceRepository.GetLineItemCodesAsync();

            // Create the adapter to convert ProjectType domain entities to DTOs.
            var projectItemCodeDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectItemCode, Ellucian.Colleague.Dtos.ProjectsAccounting.ProjectItemCode>();
            var projectItemCodeDtos = new List<Ellucian.Colleague.Dtos.ProjectsAccounting.ProjectItemCode>();

            foreach (var itemCode in projectItemCodesDomainEntities)
            {
                projectItemCodeDtos.Add(projectItemCodeDtoAdapter.MapToType(itemCode));
            }

            return projectItemCodeDtos;
        }
    }
}
