﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.ProjectsAccounting;

namespace Ellucian.Colleague.Coordination.ProjectsAccounting.Services
{
    /// <summary>
    /// Interface for Project Type
    /// </summary>
    public interface IProjectTypeService
    {
        /// <summary>
        /// Returns a list of project types
        /// </summary>
        Task<IEnumerable<ProjectType>> GetProjectTypesAsync();
    }
}
