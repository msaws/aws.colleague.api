﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Coordination.ProjectsAccounting.Adapters;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;
using Ellucian.Colleague.Domain.ProjectsAccounting.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;

namespace Ellucian.Colleague.Coordination.ProjectsAccounting.Services
{
    /// <summary>
    /// This class implements the IProjectService interface.
    /// </summary>
    [RegisterType]
    public class ProjectService : BaseCoordinationService, IProjectService
    {
        private IProjectRepository projectRepository;
        private IProjectsAccountingUserRepository projectsAccountingUserRepository;
        private IGeneralLedgerConfigurationRepository generalLedgerConfigurationRepository;

        // This constructor initializes the private attributes.
        public ProjectService(IProjectRepository projectRepository,
            IProjectsAccountingUserRepository projectsAccountingUserRepository,
            IGeneralLedgerConfigurationRepository generalLedgerConfigurationRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.projectRepository = projectRepository;
            this.projectsAccountingUserRepository = projectsAccountingUserRepository;
            this.generalLedgerConfigurationRepository = generalLedgerConfigurationRepository;
        }

        /// <summary>
        /// Returns the project DTOs that are associated with the user logged in to self service.
        /// </summary>
        /// <param name="summaryOnly">Specify whether or not detailed information will be returned with the project.</param>
        /// <param name="filter">Filter criteria used to reduce the number of projects returned.</param>
        /// <returns>List of project DTOs.</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.ProjectsAccounting.Project>> GetProjectsAsync(bool summaryOnly, IEnumerable<UrlFilter> filter)
        {
            // Get the ID for the person who is logged in, and use the ID to get his list of projects.
            var projectsAccountingUser = await projectsAccountingUserRepository.GetProjectsAccountingUserAsync(CurrentUser.PersonId);

            // Use this method to get the project information from the project repository 
            // for the projects assigned to the user.
            var projectDomainEntities = await projectRepository.GetProjectsAsync(projectsAccountingUser.Projects, summaryOnly, null);

            // Apply filter criteria to domain entities
            projectDomainEntities = ApplyFilters(projectDomainEntities, filter);

            // Get the GL Configuration so we know how to format the GL numbers
            var glConfiguration = await generalLedgerConfigurationRepository.GetAccountStructureAsync();

            // Create the adapter to convert project domain entities to DTOs.
            var projectDtoAdapter = new ProjectEntityToDtoAdapter(_adapterRegistry, logger);
            var projectDtos = new List<Ellucian.Colleague.Dtos.ProjectsAccounting.Project>();

            // Convert the domain entities into DTOs
            string sequenceNumber = null;
            foreach (var project in projectDomainEntities)
            {
                var projectDto = projectDtoAdapter.MapToType(project, summaryOnly, sequenceNumber, glConfiguration.MajorComponentStartPositions);
                projectDtos.Add(projectDto);
            }
            return projectDtos;
        }

        /// <summary>
        /// Returns the project selected by the user.
        /// </summary>
        /// <param name="id">ID of the project requested.</param>
        /// <param name="summaryOnly">Specify whether or not detailed information will be returned with the project.</param>
        /// <param name="sequenceNumber">Specify a sequence number to return project totals for a single budget period.</param>
        /// <returns>Project DTO.</returns>
        public async Task<Ellucian.Colleague.Dtos.ProjectsAccounting.Project> GetProjectAsync(string id, bool summaryOnly, string sequenceNumber)
        {
            // Get the list of projects for the person who is logged in.
            var projectsAccountingUser = await projectsAccountingUserRepository.GetProjectsAccountingUserAsync(CurrentUser.PersonId);

            // Throw an exception if the user does not have access to the 
            if (!projectsAccountingUser.Projects.Contains(id))
            {
                throw new PermissionsException("You do not have access to this project.");
            }

            // Get the project domain entity from the repository.
            var projectDomainEntity = await projectRepository.GetProjectAsync(id, summaryOnly, sequenceNumber);

            // Get the GL Configuration so we know how to format the GL numbers
            var glConfiguration = await generalLedgerConfigurationRepository.GetAccountStructureAsync();

            if (projectDomainEntity == null)
            {
                throw new ArgumentNullException("projectDomainEntity", "projectDomainEntity cannot be null.");
            }

            // Convert the project and all its child objects into DTOs.
            var projectDtoAdapter = new ProjectEntityToDtoAdapter(_adapterRegistry, logger);
            var projectDto = projectDtoAdapter.MapToType(projectDomainEntity, summaryOnly, sequenceNumber, glConfiguration.MajorComponentStartPositions);

            return projectDto;
        }

        /// <summary>
        /// Filter the list of projects such that only projects whose properties match the filter criteria remain.
        /// </summary>
        /// <param name="projectDomainEntities">Seed list of project domain entities.</param>
        /// <param name="filter">List of filter criteria objects</param>
        /// <returns>List of filtered project domain entities.</returns>
        private IEnumerable<Domain.ProjectsAccounting.Entities.Project> ApplyFilters(IEnumerable<Domain.ProjectsAccounting.Entities.Project> projectDomainEntities, IEnumerable<UrlFilter> filter)
        {
            // Start with a copy of the initial set of projects
            var filteredProjects = new List<Domain.ProjectsAccounting.Entities.Project>();
            bool typeSelected = false;
            bool statusSelected = false;
            bool matchesStatus = false;
            bool matchesType = false;
            if (filter != null && filter.Count() > 0)
            {
                // Evaluate all of the projects
                foreach (var project in projectDomainEntities)
                {
                    matchesStatus = false;
                    matchesType = false;

                    // Apply each filter to the project
                    foreach (var filterCriteria in filter)
                    {
                        // Only continue if all three parts of the filter object are not null. Otherwise, include the record.
                        if (!string.IsNullOrEmpty(filterCriteria.Field) && !string.IsNullOrEmpty(filterCriteria.Operator) && !string.IsNullOrEmpty(filterCriteria.Value))
                        {
                            // Filter by status; if the project status matches any of the statuses then include it
                            if (filterCriteria.Field.ToLower() == "status")
                            {
                                statusSelected = true;
                                if (filterCriteria.Operator.ToLower() == "eq")
                                {
                                    if (filterCriteria.Value == project.Status.ToString())
                                    {
                                        matchesStatus = true;
                                    }
                                }
                            }

                            // Filter by type; if the project type matches any of the types then include it
                            if (filterCriteria.Field.ToLower() == "projecttype")
                            {
                                typeSelected = true;
                                if (filterCriteria.Operator.ToLower() == "eq")
                                {
                                    if (filterCriteria.Value == project.ProjectType)
                                    {
                                        matchesType = true;
                                    }
                                }
                            }
                        }
                        else
                        {
                            matchesStatus = true;
                            matchesType = true;
                        }
                    }

                    // If no status options have been selected, the project satisfies the status criteria.
                    if (statusSelected == false)
                    {
                        matchesStatus = true;
                    }

                    // If no type options have been selected, the project satisfies the type criteria.
                    if (typeSelected == false)
                    {
                        matchesType = true;
                    }

                    // Only keep the status if both sets of criteria are satisfied
                    if (matchesStatus && matchesType)
                    {
                        filteredProjects.Add(project);
                    }
                }
            }
            else
            {
                filteredProjects = projectDomainEntities.ToList();
            }

            return filteredProjects;
        }
    }
}
