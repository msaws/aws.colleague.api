﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.ProjectsAccounting.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;

namespace Ellucian.Colleague.Coordination.ProjectsAccounting.Services
{
    /// <summary>
    /// This class implements the IProjectTypeService interface
    /// </summary>
    [RegisterType]
    public class ProjectTypeService : BaseCoordinationService, IProjectTypeService
    {
        private IProjectsAccountingReferenceDataRepository projectsAccountingReferenceRepository;

        // This constructor initializes the private attributes.
        public ProjectTypeService(IProjectsAccountingReferenceDataRepository projectsAccountingReferenceRepository, IAdapterRegistry adapterRegistry, ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.projectsAccountingReferenceRepository = projectsAccountingReferenceRepository;
        }

        /// <summary>
        /// Get all project type codes
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.ProjectsAccounting.ProjectType>> GetProjectTypesAsync()
        {
            var projectTypeDomainEntities = await projectsAccountingReferenceRepository.GetProjectTypesAsync();

            // Create the adapter to convert ProjectType domain entities to DTOs.
            var projectTypeDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.ProjectsAccounting.Entities.ProjectType, Ellucian.Colleague.Dtos.ProjectsAccounting.ProjectType>();
            var projectTypeDtos = new List<Ellucian.Colleague.Dtos.ProjectsAccounting.ProjectType>();

            // Convert domain entities into DTOs
            foreach (var projectType in projectTypeDomainEntities)
            {
                projectTypeDtos.Add(projectTypeDtoAdapter.MapToType(projectType));
            }

            return projectTypeDtos;
        }
    }
}