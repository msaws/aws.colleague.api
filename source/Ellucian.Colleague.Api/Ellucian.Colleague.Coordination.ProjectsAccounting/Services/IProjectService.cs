﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.ProjectsAccounting;

namespace Ellucian.Colleague.Coordination.ProjectsAccounting.Services
{
    /// <summary>
    /// This is the definition of the methods that must be implemented
    /// for a project service.
    /// </summary>
    public interface IProjectService
    {
        /// <summary>
        /// Returns the project DTOs that are associated with the user logged in to self service.
        /// </summary>
        /// <param name="summaryOnly">Specify whether or not detailed information will be returned with the project.</param>
        /// <param name="filter">Filter criteria used to reduce the number of projects returned.</param>
        /// <returns>List of project DTOs.</returns>
        Task<IEnumerable<Project>> GetProjectsAsync(bool summaryOnly, IEnumerable<UrlFilter> filter);

        /// <summary>
        /// Returns the project selected by the user.
        /// </summary>
        /// <param name="id">ID of the project requested.</param>
        /// <param name="summaryOnly">Specify whether or not detailed information will be returned with the project.</param>
        /// <param name="sequenceNumber">Specify a sequence number to return project totals for a single budget period.</param>
        /// <returns>Project DTO.</returns>
        Task<Project> GetProjectAsync(string projectId, bool summaryOnly, string sequenceNumber);
    }
}
