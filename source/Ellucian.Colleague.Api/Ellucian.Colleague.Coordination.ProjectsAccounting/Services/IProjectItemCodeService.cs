﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.ProjectsAccounting;

namespace Ellucian.Colleague.Coordination.ProjectsAccounting.Services
{
    /// <summary>
    /// Interface for Project Item Code
    /// </summary>
    public interface IProjectItemCodeService
    {
        /// <summary>
        /// Returns a list of project item codes.
        /// </summary>
        /// <returns>A list of project item codes.</returns>
        Task<IEnumerable<ProjectItemCode>> GetProjectItemCodesAsync();
    }
}
