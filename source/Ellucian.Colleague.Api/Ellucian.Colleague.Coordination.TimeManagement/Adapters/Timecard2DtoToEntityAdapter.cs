﻿/*Copyright 2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Web.Adapters;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Adapters
{
    public class Timecard2DtoToEntityAdapter : BaseAdapter<Dtos.TimeManagement.Timecard2, Domain.TimeManagement.Entities.Timecard2>
    {

        public Timecard2DtoToEntityAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {

        }

        /// <summary>
        /// Map a Timecard2 DTO to a Timecard Entity
        /// </summary>
        /// <param name="Source"></param>
        /// <returns></returns>
        public override Domain.TimeManagement.Entities.Timecard2 MapToType(Dtos.TimeManagement.Timecard2 Source)
        {
            if (Source == null)
            {
                throw new ArgumentNullException("Source");
            }

            var timecard2Entity = new Domain.TimeManagement.Entities.Timecard2(Source.EmployeeId, Source.PayCycleId,
                Source.PositionId, Source.PeriodStartDate, Source.PeriodEndDate, Source.StartDate, Source.EndDate, Source.Id);

            if (Source.TimeEntries != null)
            {
                foreach (var sourceTimeEntry in Source.TimeEntries)
                {
                    Domain.TimeManagement.Entities.TimeEntry2 timeEntry2 = null;
                    if (string.IsNullOrWhiteSpace(sourceTimeEntry.Id))
                    {
                        timeEntry2 = new Domain.TimeManagement.Entities.TimeEntry2(sourceTimeEntry.TimecardId,
                            sourceTimeEntry.EarningsTypeId, sourceTimeEntry.WorkedTime, sourceTimeEntry.WorkedDate.Date);
                    }
                    else
                    {
                        timeEntry2 = new Domain.TimeManagement.Entities.TimeEntry2(sourceTimeEntry.TimecardId, sourceTimeEntry.EarningsTypeId,
                            sourceTimeEntry.WorkedTime, sourceTimeEntry.WorkedDate.Date, sourceTimeEntry.Id);
                    }

                    timeEntry2.InDateTime = sourceTimeEntry.InDateTime;
                    timeEntry2.OutDateTime = sourceTimeEntry.OutDateTime;
                    timeEntry2.ProjectId = sourceTimeEntry.ProjectId;


                    timecard2Entity.TimeEntries.Add(timeEntry2);
                }
            }
            return timecard2Entity;
        }
    }
}
