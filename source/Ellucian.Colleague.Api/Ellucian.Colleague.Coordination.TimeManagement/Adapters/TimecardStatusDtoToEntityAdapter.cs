﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Web.Adapters;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Adapters
{
    /// <summary>
    /// Custom Adapter for TimecardStatus DTO to Entity
    /// </summary>
    public class TimecardStatusDtoToEntityAdapter : 
        AutoMapperAdapter<Dtos.TimeManagement.TimecardStatus, Domain.TimeManagement.Entities.TimecardStatus>
    {
        public TimecardStatusDtoToEntityAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
            AddMappingDependency<Dtos.TimeManagement.StatusAction, Domain.TimeManagement.Entities.StatusAction>();
            AddMappingDependency<Dtos.Base.Timestamp, Domain.Base.Entities.Timestamp>();
        }

        /// <summary>
        /// Map a TimecardStatus DTO to a TimecardStatus Entity
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public override Domain.TimeManagement.Entities.TimecardStatus MapToType(Dtos.TimeManagement.TimecardStatus source)
        {
            if (source == null)
            {
                throw new ArgumentNullException("source", "Source timecard status");
            }
            
            var actionAdapter = adapterRegistry.GetAdapter<Dtos.TimeManagement.StatusAction,Domain.TimeManagement.Entities.StatusAction>();
            var timestampDtoToEntityAdapter = adapterRegistry.GetAdapter<Dtos.Base.Timestamp, Domain.Base.Entities.Timestamp>();

            var action = actionAdapter.MapToType(source.ActionType);


            if (string.IsNullOrWhiteSpace(source.Id))
            {
                //this is a "new" status
                return new Domain.TimeManagement.Entities.TimecardStatus(source.TimecardId, source.ActionerId, action);

            }
            else
            {
                //this is an existing status
                return new Domain.TimeManagement.Entities.TimecardStatus(source.TimecardId, source.ActionerId, action, source.Id)
            {
                    Timestamp = timestampDtoToEntityAdapter.MapToType(source.Timestamp)
            };
            }
        }
    }
}
