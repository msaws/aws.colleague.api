﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Web.Adapters;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Ellucian.Colleague.Coordination.TimeManagement.Adapters
{
    public class TimecardHistory2EntityToDtoAdapter : AutoMapperAdapter<Domain.TimeManagement.Entities.TimecardHistory2, Dtos.TimeManagement.TimecardHistory2>
    {
        /// <summary>
        /// Constructor for mapping Timecard history domain objects to DTO objects
        /// </summary>
        /// <param name="adapterRegistry"></param>
        /// <param name="logger"></param>
        public TimecardHistory2EntityToDtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
            AddMappingDependency<Domain.TimeManagement.Entities.TimeEntryHistory2, Dtos.TimeManagement.TimeEntryHistory2>();
            AddMappingDependency<Domain.Base.Entities.Timestamp, Dtos.Base.Timestamp>();
        }
    }
}
