﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Web.Adapters;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Ellucian.Colleague.Coordination.TimeManagement.Adapters
{
    public class TimecardStatusEntityToDtoAdapter : AutoMapperAdapter<Domain.TimeManagement.Entities.TimecardStatus, Dtos.TimeManagement.TimecardStatus>
    {
        /// <summary>
        /// Constructor for mapping TimecardStatus domain objects to DTO objects
        /// </summary>
        /// <param name="adapterRegistry"></param>
        /// <param name="logger"></param>
        public TimecardStatusEntityToDtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
            AddMappingDependency<Domain.TimeManagement.Entities.StatusAction, Dtos.TimeManagement.StatusAction>();
            AddMappingDependency<Domain.Base.Entities.Timestamp, Dtos.Base.Timestamp>();
        }
    }
}
