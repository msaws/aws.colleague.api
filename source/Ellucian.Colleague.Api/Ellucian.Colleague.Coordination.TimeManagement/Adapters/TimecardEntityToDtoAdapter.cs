﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Web.Adapters;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Ellucian.Colleague.Coordination.TimeManagement.Adapters
{
    public class TimecardEntityToDtoAdapter : AutoMapperAdapter<Domain.TimeManagement.Entities.Timecard, Dtos.TimeManagement.Timecard>
    {
        /// <summary>
        /// Constructor for mapping TimeCard domain objects to DTO objects
        /// </summary>
        /// <param name="adapterRegistry"></param>
        /// <param name="logger"></param>
        public TimecardEntityToDtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
            AddMappingDependency<Domain.TimeManagement.Entities.TimeEntry, Dtos.TimeManagement.TimeEntry>();
            AddMappingDependency<Domain.Base.Entities.Timestamp, Dtos.Base.Timestamp>();
        }
    }
}
