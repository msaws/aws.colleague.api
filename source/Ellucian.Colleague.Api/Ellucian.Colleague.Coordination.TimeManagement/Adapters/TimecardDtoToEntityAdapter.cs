﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Web.Adapters;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Adapters
{
    /// <summary>
    /// Custom Adapter for Timecard DTO to Entity
    /// </summary>
    public class TimecardDtoToEntityAdapter : BaseAdapter<Dtos.TimeManagement.Timecard, Domain.TimeManagement.Entities.Timecard>
    {
        public TimecardDtoToEntityAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {

        }

        /// <summary>
        /// Map a Timecard DTO to a Timecard Entity
        /// </summary>
        /// <param name="Source"></param>
        /// <returns></returns>
        public override Domain.TimeManagement.Entities.Timecard MapToType(Dtos.TimeManagement.Timecard Source)
        {
            if (Source == null)
            {
                throw new ArgumentNullException("Source");
            }

            var timecardEntity = new Domain.TimeManagement.Entities.Timecard(Source.EmployeeId, Source.PayCycleId,
                Source.PositionId, Source.PeriodStartDate, Source.PeriodEndDate, Source.StartDate, Source.EndDate, Source.Id);

            if (Source.TimeEntries != null)
            {
                foreach (var sourceTimeEntry in Source.TimeEntries)
                {
                    Domain.TimeManagement.Entities.TimeEntry timeEntry = null;
                    if (string.IsNullOrWhiteSpace(sourceTimeEntry.Id))
                    {
                        timeEntry = new Domain.TimeManagement.Entities.TimeEntry(sourceTimeEntry.TimecardId,
                            sourceTimeEntry.EarningsTypeId, sourceTimeEntry.WorkedTime, sourceTimeEntry.WorkedDate.Date);
                    }
                    else
                    {
                        timeEntry = new Domain.TimeManagement.Entities.TimeEntry(sourceTimeEntry.TimecardId, sourceTimeEntry.EarningsTypeId,
                            sourceTimeEntry.WorkedTime, sourceTimeEntry.WorkedDate.Date, sourceTimeEntry.Id);
                    }

                    timeEntry.InDateTime = sourceTimeEntry.InDateTime;
                    timeEntry.OutDateTime = sourceTimeEntry.OutDateTime;
                    timeEntry.ProjectId = sourceTimeEntry.ProjectId;


                    timecardEntity.TimeEntries.Add(timeEntry);
                }
            }
            return timecardEntity;
        }
    }
}
