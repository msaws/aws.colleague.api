﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Web.Adapters;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Ellucian.Colleague.Coordination.TimeManagement.Adapters
{
    public class OvertimeCalculationDefinitionEntityToDtoAdapter : AutoMapperAdapter<Domain.TimeManagement.Entities.OvertimeCalculationDefinition, Dtos.TimeManagement.OvertimeCalculationDefinition>
    {
        /// <summary>
        /// Constructor for mapping OvertimeCalculationDefinition domain objects to DTO objects
        /// </summary>
        /// <param name="adapterRegistry"></param>
        /// <param name="logger"></param>
        public OvertimeCalculationDefinitionEntityToDtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
            AddMappingDependency<Domain.TimeManagement.Entities.OvertimeCalculationDefinition, Dtos.TimeManagement.OvertimeCalculationDefinition>();
            AddMappingDependency<Domain.TimeManagement.Entities.HoursThreshold, Dtos.TimeManagement.HoursThreshold>();
            AddMappingDependency<Domain.TimeManagement.Entities.IncludedEarningsType, Dtos.TimeManagement.IncludedEarningsType>();
            AddMappingDependency<Domain.TimeManagement.Entities.EarningsFactor, Dtos.TimeManagement.EarningsFactor>();
        }
    }
}
