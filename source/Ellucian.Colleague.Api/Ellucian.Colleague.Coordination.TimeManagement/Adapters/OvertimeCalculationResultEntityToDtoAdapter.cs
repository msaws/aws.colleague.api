﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Web.Adapters;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Adapters
{
    public class OvertimeCalculationResultEntityToDtoAdapter : AutoMapperAdapter<Domain.TimeManagement.Entities.OvertimeCalculationResult, Dtos.TimeManagement.OvertimeCalculationResult>
    {
        public OvertimeCalculationResultEntityToDtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
            AddMappingDependency<Domain.TimeManagement.Entities.OvertimeThresholdResult, Dtos.TimeManagement.OvertimeThresholdResult>();
            AddMappingDependency<Domain.TimeManagement.Entities.DailyOvertimeAllocation, Dtos.TimeManagement.DailyOvertimeAllocation>();
        }
    }
}
