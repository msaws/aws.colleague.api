﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Adapters
{
    [RegisterType]
    public class TimeEntryCommentsDtoToEntityAdapter : BaseAdapter<Dtos.TimeManagement.TimeEntryComments, Domain.TimeManagement.Entities.TimeEntryComment>
    {        
        public TimeEntryCommentsDtoToEntityAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
            
        }

        public override Domain.TimeManagement.Entities.TimeEntryComment MapToType(Dtos.TimeManagement.TimeEntryComments Source)
        {
            if (Source == null)
            {
                throw new ArgumentNullException("Source");
            }

            var commentEntity = new Domain.TimeManagement.Entities.TimeEntryComment(
                Source.TimecardId,
                Source.EmployeeId,
                Source.PositionId,
                Source.PayCycleId,
                Source.PayPeriodEndDate,
                Source.Comments);

            return commentEntity;
        }
    }
}
