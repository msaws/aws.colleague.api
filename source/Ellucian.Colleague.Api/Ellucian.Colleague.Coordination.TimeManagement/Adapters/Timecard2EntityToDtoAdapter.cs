﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Web.Adapters;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace Ellucian.Colleague.Coordination.TimeManagement.Adapters
{
    public class Timecard2EntityToDtoAdapter : AutoMapperAdapter<Domain.TimeManagement.Entities.Timecard2, Dtos.TimeManagement.Timecard2>
    {
        /// <summary>
        /// Constructor for mapping TimeCard domain objects to DTO objects
        /// </summary>
        /// <param name="adapterRegistry"></param>
        /// <param name="logger"></param>
        public Timecard2EntityToDtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
            AddMappingDependency<Domain.TimeManagement.Entities.TimeEntry2, Dtos.TimeManagement.TimeEntry2>();
            AddMappingDependency<Domain.Base.Entities.Timestamp, Dtos.Base.Timestamp>();
        }
    }
}
