﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.TimeManagement;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Services
{
    [RegisterType]
    public class OvertimeCalculationService : BaseCoordinationService, IOvertimeCalculationService
    {
        private readonly IOvertimeCalculationRepository overtimeCalculationRepository;

        public OvertimeCalculationService(IOvertimeCalculationRepository overtimeCalculationRepository,
            IAdapterRegistry adapterRegistry, 
            ICurrentUserFactory currentUserFactory, 
            IRoleRepository roleRepository, 
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.overtimeCalculationRepository = overtimeCalculationRepository;
        }


        /// <summary>
        /// Calculate overtime based on the given criteria.
        /// </summary>
        /// <param name="criteria"></param>
        /// <returns></returns>
        public async Task<OvertimeCalculationResult> CalculateOvertime(OvertimeQueryCriteria criteria)
        {
            if (criteria == null)
            {
                throw new ArgumentNullException("criteria");
            }

            if (string.IsNullOrWhiteSpace(criteria.PersonId))
            {
                throw new ArgumentException("PersonId attribute of criteria is required", "criteria");
            }

            if (string.IsNullOrWhiteSpace(criteria.PayCycleId))
            {
                throw new ArgumentException("PayCycleId attribute of criteria is required", "criteria");
            }

            if (criteria.StartDate > criteria.EndDate)
            {
                throw new ArgumentException("Attribute Start Date must be before Attribute End Date of criteria object", "criteria");
            }

            if (!HasPermission(TimeManagementPermissionCodes.ApproveRejectEmployeeTimecard) && !CurrentUser.IsPerson(criteria.PersonId))
            {
                throw new PermissionsException("Current user can only calculate overtime for their self or their supervisees");
            }

            var calculationResultEntity = await overtimeCalculationRepository.CalculateOvertime(criteria.PersonId, criteria.StartDate, criteria.EndDate, criteria.PayCycleId);
            if (calculationResultEntity == null)
            {
                var message = "Unexpected result returned by repository";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            var calculationResultEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.OvertimeCalculationResult, OvertimeCalculationResult>();
            return calculationResultEntityToDtoAdapter.MapToType(calculationResultEntity);
        }
    }
}
