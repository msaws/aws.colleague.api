﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.TimeManagement;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.TimeManagement.Services;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
namespace Ellucian.Colleague.Coordination.TimeManagement.Services
{
    /// <summary>
    /// Timecard Histories service
    /// </summary>
    [RegisterType]
    public class TimecardHistoriesService : BaseCoordinationService, ITimecardHistoriesService
    {
        private readonly ITimecardHistoriesRepository timecardHistoriesRepository;
        private readonly ISupervisorsRepository supervisorsRepository;

        /// <summary>
        /// Constructor for timecard histories service
        /// </summary>
        /// <param name="timeCardHistoriesRepository"></param>
        /// /// <param name="supervisorsRepository"></param>
        /// <param name="adapterRegistry"></param>
        /// <param name="currentUserFactory"></param>
        /// <param name="roleRepository"></param>
        /// <param name="logger"></param>
        public TimecardHistoriesService(
            ITimecardHistoriesRepository timecardHistoriesRepository,
            ISupervisorsRepository supervisorsRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger
            )
            : base(
              adapterRegistry, currentUserFactory, roleRepository, logger
                )
        {
            this.timecardHistoriesRepository = timecardHistoriesRepository;
            this.supervisorsRepository = supervisorsRepository;
        }

        /// <summary>
        /// Gets all timecard histories for current user
        /// </summary>
        /// <param name=></param>
        /// <returns>List of timecard tasks</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        public async Task<IEnumerable<Dtos.TimeManagement.TimecardHistory>> GetTimecardHistoriesAsync(DateTime startDate, DateTime endDate)
        {
            var userAndSubordinateIds = new List<string>() { CurrentUser.PersonId };

            if (HasPermission(TimeManagementPermissionCodes.ApproveRejectEmployeeTimecard))
            {
                var subordinateIds = await supervisorsRepository.GetSuperviseesBySupervisorAsync(CurrentUser.PersonId);

                if (subordinateIds == null)
                {
                    var message = "Unexpected null person id list returned from supervisors repository";
                    logger.Error(message);
                }
                else if (subordinateIds.Any())
                {
                    userAndSubordinateIds = userAndSubordinateIds.Concat(subordinateIds).ToList();
                }
            }

            var timecardHistoriesEntities = await timecardHistoriesRepository.GetTimecardHistoriesAsync(startDate, endDate, userAndSubordinateIds);

            if (timecardHistoriesEntities == null)
            {
                var message = "Null timecard histories returned to coordination service";
                logger.Error(message);
                return new List<Dtos.TimeManagement.TimecardHistory>();
            }

            var timecardHistoriesEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.TimecardHistory, Dtos.TimeManagement.TimecardHistory>();
            return timecardHistoriesEntities.Select(tc => timecardHistoriesEntityToDtoAdapter.MapToType(tc)).ToList();
        }

        /// <summary>
        /// Gets all timecard histories for current user
        /// </summary>
        /// <param name=></param>
        /// <returns>List of timecard tasks</returns>
        public async Task<IEnumerable<Dtos.TimeManagement.TimecardHistory2>> GetTimecardHistories2Async(DateTime startDate, DateTime endDate)
        {
            var userAndSubordinateIds = new List<string>() { CurrentUser.PersonId };

            if (HasPermission(TimeManagementPermissionCodes.ApproveRejectEmployeeTimecard))
            {
                var subordinateIds = await supervisorsRepository.GetSuperviseesBySupervisorAsync(CurrentUser.PersonId);

                if (subordinateIds == null)
                {
                    var message = "Unexpected null person id list returned from supervisors repository";
                    logger.Error(message);
                }
                else if (subordinateIds.Any())
                {
                    userAndSubordinateIds = userAndSubordinateIds.Concat(subordinateIds).ToList();
                }
            }

            var timecardHistory2Entities = await timecardHistoriesRepository.GetTimecardHistories2Async(startDate, endDate, userAndSubordinateIds);

            if (timecardHistory2Entities == null)
            {
                var message = "Null timecard histories returned to coordination service";
                logger.Error(message);
                return new List<Dtos.TimeManagement.TimecardHistory2>();
            }

            var timecardHistories2EntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.TimecardHistory2, Dtos.TimeManagement.TimecardHistory2>();
            return timecardHistory2Entities.Select(tc => timecardHistories2EntityToDtoAdapter.MapToType(tc)).ToList();
        }
    }
}
