﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.TimeManagement;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.TimeManagement.Services;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.HumanResources.Repositories;

namespace Ellucian.Colleague.Coordination.TimeManagement.Services
{
    /// <summary>
    /// Time cards service
    /// </summary>
    [RegisterType]
    public class TimecardsService : BaseCoordinationService, ITimecardsService
    {
        private readonly ITimecardsRepository timecardsRepository;
        private readonly ITimecardDomainService timecardDomainService;
        private readonly ISupervisorsRepository supervisorsRepository;

        /// <summary>
        /// Constructor for time cards service
        /// </summary>
        /// <param name="timecardsRepository"></param>
        /// <param name="adapterRegistry"></param>
        /// <param name="currentUserFactory"></param>
        /// <param name="roleRepository"></param>
        /// <param name="logger"></param>
        public TimecardsService(
            ITimecardsRepository timecardsRepository,
            ITimecardDomainService timecardDomainService,
            ISupervisorsRepository supervisorsRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger
            )
            : base(
              adapterRegistry, currentUserFactory, roleRepository, logger
                )
        {
            this.timecardsRepository = timecardsRepository;
            this.timecardDomainService = timecardDomainService;
            this.supervisorsRepository = supervisorsRepository;
        }

        /// <summary>
        /// Gets all time cards for current user
        /// </summary>
        /// <param name=></param>
        /// <returns>List of timecard tasks</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        public async Task<IEnumerable<Dtos.TimeManagement.Timecard>> GetTimecardsAsync()
        {
            var userAndSubordinateIds = new List<string>() { CurrentUser.PersonId };

            if (HasPermission(TimeManagementPermissionCodes.ApproveRejectEmployeeTimecard))
            {
                var subordinateIds = await supervisorsRepository.GetSuperviseesBySupervisorAsync(CurrentUser.PersonId);

                if (subordinateIds == null)
                {
                    var message = "Unexpected null person id list returned from supervisors repository";
                    logger.Error(message);
                    throw new ApplicationException(message);
                }
                if (subordinateIds.Any())
                {
                    userAndSubordinateIds = userAndSubordinateIds.Concat(subordinateIds).ToList();
                }
            }

            var timecardsEntities = await timecardsRepository.GetTimecardsAsync(userAndSubordinateIds);

            if (timecardsEntities == null)
            {
                var message = "Null timecards returned to coordination service";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            var timecardEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.Timecard, Dtos.TimeManagement.Timecard>();
            return timecardsEntities.Select(tc => timecardEntityToDtoAdapter.MapToType(tc)).ToList();
        }

        /// <summary>
        /// Gets a single time card
        /// </summary>
        /// <param name="timecardId"></param>
        /// <returns>A timecard task</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        public async Task<Dtos.TimeManagement.Timecard> GetTimecardAsync(string timecardId)
        {
            if (string.IsNullOrWhiteSpace(timecardId))
            {
                var message = "timecard id is a required argument to get a timecard";
                logger.Error(message);
                throw new ArgumentNullException("timecardId", message);
            }

            var timecardEntity = await timecardsRepository.GetTimecardAsync(timecardId);

            if (timecardEntity == null)
            {
                var message = string.Format("No timecard with id {0} found. Null timecard returned to coordination service", timecardId);
                logger.Error(message);
                throw new KeyNotFoundException(message);
            }

            //if the timecardEntity's employeeId doesn't match the current user's personId, then check to see if the current user is a supervisor
            //for the employeeId. If not, throw a permissions exception

            if (!CurrentUser.IsPerson(timecardEntity.EmployeeId)) 
            {
                if (HasPermission(TimeManagementPermissionCodes.ApproveRejectEmployeeTimecard))
                {
                    var supervisedEmployeeIds = await supervisorsRepository.GetSuperviseesBySupervisorAsync(CurrentUser.PersonId);
                    if (supervisedEmployeeIds == null || !supervisedEmployeeIds.Contains(timecardEntity.EmployeeId))
                    {
                        var message = string.Format("Supervisor Id {0} does not have permission to access timecard id {1} which is owned by Employee Id {2}", CurrentUser.PersonId, timecardId, timecardEntity.EmployeeId);
                        throw new PermissionsException(message);
                    }
                }
                else
                {
                    var message = string.Format("Employee Id {0} does not have permission to access timecard id {1} which is owned by Employee Id {2}", CurrentUser.PersonId, timecardId, timecardEntity.EmployeeId);
                    throw new PermissionsException(message);
                }
            }

            var timecardEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.Timecard, Dtos.TimeManagement.Timecard>();
            return timecardEntityToDtoAdapter.MapToType(timecardEntity);
        }

        /// <summary>
        /// Updates a single time card and associated time entries
        /// </summary>
        /// <param name="timecard"></param>
        /// <returns>A timecard task</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        public async Task<Dtos.TimeManagement.Timecard> UpdateTimecardAsync(Dtos.TimeManagement.Timecard timecard)
        {
            // exception handling...
            if (timecard == null)
            {
                throw new ArgumentNullException("timeCard");
            }

            if (!CurrentUser.IsPerson(timecard.EmployeeId))
            {
                var message = string.Format("Current user {0} does not have permissions to update timecard for employee {1}.", CurrentUser.PersonId, timecard.EmployeeId);
                logger.Error(message);
                throw new PermissionsException(message);
            }

            // get the adapter to convert the dto to domain entity...
            var timeCardDtoToEntityAdapter = _adapterRegistry.GetAdapter<Dtos.TimeManagement.Timecard, Domain.TimeManagement.Entities.Timecard>();
            var timeCardEntity = timeCardDtoToEntityAdapter.MapToType(timecard);

            // get the timecard currently in the db...
            var existingTimecard = await timecardsRepository.GetTimecardAsync(timeCardEntity.Id);

            if (existingTimecard == null)
            {
                var message = string.Format("Unable to update timecard {0}. Timecard does not exist in db.", timecard.Id);
                logger.Error(message);
                throw new KeyNotFoundException(message);
            }

            // create the timecard helper to determine what time entries to update/delete/create...
            var timecardHelper = timecardDomainService.CreateTimecardHelper(timeCardEntity, existingTimecard);

            // update the timecard in the db with the data from the helper...
            var updatedTimecard = await timecardsRepository.UpdateTimecardAsync(timecardHelper);

            var timeCardEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.Timecard, Dtos.TimeManagement.Timecard>();
            return timeCardEntityToDtoAdapter.MapToType(updatedTimecard);
        }

        /// <summary>
        /// Creates a single timecard and associated time entries
        /// </summary>
        /// <param name="timecard"></param>
        /// <returns>A timecard task</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        public async Task<Dtos.TimeManagement.Timecard> CreateTimecardAsync(Dtos.TimeManagement.Timecard timecard)
        {
            if (timecard == null)
            {
                throw new ArgumentNullException("timecard");
            }

            if (!CurrentUser.IsPerson(timecard.EmployeeId))
            {
                var message = string.Format("Current user {0} does not have permissions to create timecard for employee {1}.", CurrentUser.PersonId, timecard.EmployeeId);
                logger.Error(message);
                throw new PermissionsException(message);
            }

            // get adapter to convert the dto to domain entity...
            var timeCardDtoToEntityAdapter = _adapterRegistry.GetAdapter<Dtos.TimeManagement.Timecard, Domain.TimeManagement.Entities.Timecard>();
            var timeCardEntity = timeCardDtoToEntityAdapter.MapToType(timecard);

            // check that a timecard with this same information does not already exist...
            var employeeId = CurrentUser.PersonId;
            var employeeIds = new List<string>() { employeeId };
            var currentTimecardsEntities = await timecardsRepository.GetTimecardsAsync(employeeIds);

            foreach (var currentTimecard in currentTimecardsEntities)
            {
                if (currentTimecard.CompareTimecard(timeCardEntity))
                {
                    var message = string.Format("Current user {0} already has a timecard for this payperiod {1}.", CurrentUser.PersonId, currentTimecard.Id);
                    logger.Error(message);
                    throw new ExistingResourceException(message, currentTimecard.Id);
                }
            }

            // create new timecard...
            var newTimecard = await timecardsRepository.CreateTimecardAsync(timeCardEntity);

            // get adapter, convert the domain entity to dto, and return the new timecard...
            var timeCardEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.Timecard, Dtos.TimeManagement.Timecard>();
            return timeCardEntityToDtoAdapter.MapToType(newTimecard);
        }

        /// <summary>
        /// Gets all time cards for current user
        /// </summary>
        /// <param name=></param>
        /// <returns>List of timecard tasks</returns>
        public async Task<IEnumerable<Dtos.TimeManagement.Timecard2>> GetTimecards2Async()
        {
            var userAndSubordinateIds = new List<string>() { CurrentUser.PersonId };

            if (HasPermission(TimeManagementPermissionCodes.ApproveRejectEmployeeTimecard))
            {
                var subordinateIds = await supervisorsRepository.GetSuperviseesBySupervisorAsync(CurrentUser.PersonId);

                if (subordinateIds == null)
                {
                    var message = "Unexpected null person id list returned from supervisors repository";
                    logger.Error(message);
                    throw new ApplicationException(message);
                }
                if (subordinateIds.Any())
                {
                    userAndSubordinateIds = userAndSubordinateIds.Concat(subordinateIds).ToList();
                }
            }

            var timecard2Entities = await timecardsRepository.GetTimecards2Async(userAndSubordinateIds);

            if (timecard2Entities == null)
            {
                var message = "Null timecards returned to coordination service";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            var timecardEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.Timecard2, Dtos.TimeManagement.Timecard2>();
            return timecard2Entities.Select(tc => timecardEntityToDtoAdapter.MapToType(tc)).ToList();
        }

        /// <summary>
        /// Gets a single time card
        /// </summary>
        /// <param name="timecardId"></param>
        /// <returns>A timecard task</returns>
        public async Task<Dtos.TimeManagement.Timecard2> GetTimecard2Async(string timecardId)
        {
            if (string.IsNullOrWhiteSpace(timecardId))
            {
                var message = "timecard id is a required argument to get a timecard";
                logger.Error(message);
                throw new ArgumentNullException("timecardId", message);
            }

            var timecard2Entity = await timecardsRepository.GetTimecard2Async(timecardId);

            if (timecard2Entity == null)
            {
                var message = string.Format("No timecard with id {0} found. Null timecard returned to coordination service", timecardId);
                logger.Error(message);
                throw new KeyNotFoundException(message);
            }

            //if the timecardEntity's employeeId doesn't match the current user's personId, then check to see if the current user is a supervisor
            //for the employeeId. If not, throw a permissions exception

            if (!CurrentUser.IsPerson(timecard2Entity.EmployeeId))
            {
                if (HasPermission(TimeManagementPermissionCodes.ApproveRejectEmployeeTimecard))
                {
                    var supervisedEmployeeIds = await supervisorsRepository.GetSuperviseesBySupervisorAsync(CurrentUser.PersonId);
                    if (supervisedEmployeeIds == null || !supervisedEmployeeIds.Contains(timecard2Entity.EmployeeId))
                    {
                        var message = string.Format("Supervisor Id {0} does not have permission to access timecard id {1} which is owned by Employee Id {2}", CurrentUser.PersonId, timecardId, timecard2Entity.EmployeeId);
                        throw new PermissionsException(message);
                    }
                }
                else
                {
                    var message = string.Format("Employee Id {0} does not have permission to access timecard id {1} which is owned by Employee Id {2}", CurrentUser.PersonId, timecardId, timecard2Entity.EmployeeId);
                    throw new PermissionsException(message);
                }
            }

            var timecardEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.Timecard2, Dtos.TimeManagement.Timecard2>();
            return timecardEntityToDtoAdapter.MapToType(timecard2Entity);
        }

        /// <summary>
        /// Updates a single time card and associated time entries
        /// </summary>
        /// <param name="timecard"></param>
        /// <returns>A timecard task</returns>
        public async Task<Dtos.TimeManagement.Timecard2> UpdateTimecard2Async(Dtos.TimeManagement.Timecard2 timecard)
        {
            // exception handling...
            if (timecard == null)
            {
                throw new ArgumentNullException("timeCard");
            }

            if (!CurrentUser.IsPerson(timecard.EmployeeId))
            {
                var message = string.Format("Current user {0} does not have permissions to update timecard for employee {1}.", CurrentUser.PersonId, timecard.EmployeeId);
                logger.Error(message);
                throw new PermissionsException(message);
            }

            // get the adapter to convert the dto to domain entity...
            var timeCardDtoToEntityAdapter = _adapterRegistry.GetAdapter<Dtos.TimeManagement.Timecard2, Domain.TimeManagement.Entities.Timecard2>();
            var timeCardEntity = timeCardDtoToEntityAdapter.MapToType(timecard);

            // get the timecard currently in the db...
            var existingTimecard = await timecardsRepository.GetTimecard2Async(timeCardEntity.Id);

            if (existingTimecard == null)
            {
                var message = string.Format("Unable to update timecard {0}. Timecard does not exist in db.", timecard.Id);
                logger.Error(message);
                throw new KeyNotFoundException(message);
            }

            // create the timecard helper to determine what time entries to update/delete/create...
            var timecardHelper = timecardDomainService.CreateTimecard2Helper(timeCardEntity, existingTimecard);

            // update the timecard in the db with the data from the helper...
            var updatedTimecard = await timecardsRepository.UpdateTimecard2Async(timecardHelper);

            var timeCardEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.Timecard2, Dtos.TimeManagement.Timecard2>();
            return timeCardEntityToDtoAdapter.MapToType(updatedTimecard);
        }

        /// <summary>
        /// Creates a single timecard and associated time entries
        /// </summary>
        /// <param name="timecard"></param>
        /// <returns>A timecard task</returns>
        public async Task<Dtos.TimeManagement.Timecard2> CreateTimecard2Async(Dtos.TimeManagement.Timecard2 timecard)
        {
            if (timecard == null)
            {
                throw new ArgumentNullException("timecard");
            }

            if (!CurrentUser.IsPerson(timecard.EmployeeId))
            {
                var message = string.Format("Current user {0} does not have permissions to create timecard for employee {1}.", CurrentUser.PersonId, timecard.EmployeeId);
                logger.Error(message);
                throw new PermissionsException(message);
            }

            // get adapter to convert the dto to domain entity...
            var timeCardDtoToEntityAdapter = _adapterRegistry.GetAdapter<Dtos.TimeManagement.Timecard2, Domain.TimeManagement.Entities.Timecard2>();
            var timeCardEntity = timeCardDtoToEntityAdapter.MapToType(timecard);

            // check that a timecard with this same information does not already exist...
            var employeeId = CurrentUser.PersonId;
            var employeeIds = new List<string>() { employeeId };
            var currentTimecardsEntities = await timecardsRepository.GetTimecards2Async(employeeIds);

            foreach (var currentTimecard in currentTimecardsEntities)
            {
                if (currentTimecard.CompareTimecard(timeCardEntity))
                {
                    var message = string.Format("Current user {0} already has a timecard for this payperiod {1}.", CurrentUser.PersonId, currentTimecard.Id);
                    logger.Error(message);
                    throw new ExistingResourceException(message, currentTimecard.Id);
                }
            }

            // create new timecard...
            var newTimecard = await timecardsRepository.CreateTimecard2Async(timeCardEntity);

            // get adapter, convert the domain entity to dto, and return the new timecard...
            var timeCardEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.Timecard2, Dtos.TimeManagement.Timecard2>();
            return timeCardEntityToDtoAdapter.MapToType(newTimecard);
        }
    }
}
