﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Dtos.TimeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Services
{
    public interface IOvertimeCalculationService
    {
        Task<OvertimeCalculationResult> CalculateOvertime(OvertimeQueryCriteria criteria);
    }
}
