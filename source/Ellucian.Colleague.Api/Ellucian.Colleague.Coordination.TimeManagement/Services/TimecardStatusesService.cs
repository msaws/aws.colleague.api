﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.TimeManagement;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Services
{
    [RegisterType]
    public class TimecardStatusesService : BaseCoordinationService, ITimecardStatusesService
    {
        private readonly ITimecardStatusesRepository statusesRepository;
        private readonly ITimecardsRepository timecardsRepository;
        private readonly ITimecardHistoriesRepository historiesRepository;
        private readonly ITimeEntryCommentsRepository timeEntryCommentsRepository;
        private readonly ISupervisorsRepository supervisorsRepository;


        /// <summary>
        /// Constructor for time cards service
        /// </summary>
        /// <param name="timeCardsRepository"></param>
        /// <param name="adapterRegistry"></param>
        /// <param name="currentUserFactory"></param>
        /// <param name="roleRepository"></param>
        /// <param name="logger"></param>
        public TimecardStatusesService(
            ITimecardStatusesRepository statusesRepository,
            ITimecardsRepository timecardsRepository,
            ITimecardHistoriesRepository historiesRepository,
            ITimeEntryCommentsRepository timeEntryCommentsRepository,
            ISupervisorsRepository supervisorsRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.statusesRepository = statusesRepository;
            this.timecardsRepository = timecardsRepository;
            this.historiesRepository = historiesRepository;
            this.timeEntryCommentsRepository = timeEntryCommentsRepository;
            this.supervisorsRepository = supervisorsRepository;            
        }

        /// <summary>
        /// Creates timecard status records
        /// </summary>
        /// <param name="statuses"></param>
        /// <returns></returns>
        public async Task<IEnumerable<TimecardStatus>> CreateTimecardStatusesAsync(List<TimecardStatus> statuses)
        {
            try
            {
                if (statuses == null || statuses.Count() == 0)
                {
                    throw new ArgumentNullException("statuses");
                }
                var timecardStatusEntitiesToCreate = new List<Domain.TimeManagement.Entities.TimecardStatus>();
                var timecards = new List<Domain.TimeManagement.Entities.Timecard2>();
                var timecardStatusDtos = new List<Dtos.TimeManagement.TimecardStatus>();

                var statusDtoToEntityAdapter = _adapterRegistry.GetAdapter<TimecardStatus, Domain.TimeManagement.Entities.TimecardStatus>();

                foreach (var status in statuses)
                {
                    // argument checks
                    if (status == null)
                    {
                        throw new ArgumentNullException("status");
                    }

                    if (string.IsNullOrWhiteSpace(status.TimecardId))
                    {
                        throw new ArgumentException("TimecardId attribute of status required", "status");
                    }
                    // get the timecard
                    var timecard = await timecardsRepository.GetTimecard2Async(status.TimecardId);
                    if (!(await CurrentPersonHasTimecardStatusPermissionAsync(timecard.EmployeeId)))
                    {
                        throw new PermissionsException(string.Format("User {0} does not have permission to create statuses for {1}", CurrentUser.PersonId, timecard.EmployeeId));
                    }

                    //create the status
                    timecardStatusEntitiesToCreate.Add(statusDtoToEntityAdapter.MapToType(status));
                    timecards.Add(timecard);
                }

                var newTimecardStatusEntities = await statusesRepository.CreateTimecardStatusesAsync(timecardStatusEntitiesToCreate);
                var statusEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.TimecardStatus, TimecardStatus>();

                // Get timecard comments
                var employeeIds = timecards.Select(tc => tc.EmployeeId).Distinct().ToList();
                var timeEntryComments = await timeEntryCommentsRepository.GetCommentsAsync(employeeIds);
                logger.Info(string.Format("time entry comments for employee id {0} are null from repository", employeeIds.ToString()));

                foreach (Domain.TimeManagement.Entities.TimecardStatus newTimecardStatus in newTimecardStatusEntities)
                {
                    var timecard = timecards.Where(t => t.Id == newTimecardStatus.TimecardId).FirstOrDefault();

                    //select the time entry comments for this timecard
                    var timecardComments = timeEntryComments.Where(c => c.IsLinkedToTimecard(timecard));

                    // create the history
                    await historiesRepository.CreateTimecardHistory2Async(timecard, newTimecardStatus, timecardComments);

                    //map to DTO                
                    timecardStatusDtos.Add(statusEntityToDtoAdapter.MapToType(newTimecardStatus));
                }
                return timecardStatusDtos;

            }
            catch (Exception e)
            {
                var message = e.Message;
                logger.Error(message);
                throw;
            }
        }

        /// <summary>
        /// Helper method to determine whether user has permission to access timecards for the given personId
        /// </summary>
        /// <param name="timecardStatusPersonId"></param>
        /// <returns></returns>
        private async Task<bool> CurrentPersonHasTimecardStatusPermissionAsync(string timecardStatusPersonId)
        {
            if (CurrentUser.IsPerson(timecardStatusPersonId))
            {
                return true;
            }
            else if (!HasPermission(TimeManagementPermissionCodes.ApproveRejectEmployeeTimecard))
            {
                return false;
            }
            else
            {
                var supervieeIds = await supervisorsRepository.GetSuperviseesBySupervisorAsync(CurrentUser.PersonId);
                if (supervieeIds.Contains(timecardStatusPersonId))
                {
                    return true;
                }
                return false;
            }            
        }

        /// <summary>
        /// Gets all timecard statues for a timecard
        /// </summary>
        /// <param name="timecardId"></param>
        /// <returns></returns>
        public async Task<IEnumerable<TimecardStatus>> GetTimecardStatusesByTimecardIdAsync(string timecardId)
        {
            if (string.IsNullOrEmpty(timecardId))
            {
                var message = "timecard id must be provided to get timecard statuses";
                logger.Error(message);
                throw new ArgumentNullException(message);
            }


            //verify a timecard exists for the given id
            var timecard = await timecardsRepository.GetTimecard2Async(timecardId);
            if (timecard == null)
            {
                var message = string.Format("No timecard exists for timecardId {0}", timecardId);
                throw new KeyNotFoundException(message);
            }

            //verify permissions
            if (!(await CurrentPersonHasTimecardStatusPermissionAsync(timecard.EmployeeId)))
            {
                var message = string.Format("User {0} does not have permission to get timecard statuses for {1}", CurrentUser.PersonId, timecard.EmployeeId);
                logger.Error(message);
                throw new PermissionsException(message);
            }

            // get the timecard statuses...
            var domainStatuses = await statusesRepository.GetTimecardStatusesByTimecardIdAsync(timecardId);
            if (domainStatuses == null)
            {
                var message = "unexpected null returned from repository";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            // get adapter for converting the domain entity to the dto...
            var statusEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.TimecardStatus, TimecardStatus>();

            // return the converted timecard statues...
            return domainStatuses.Select(tcs => statusEntityToDtoAdapter.MapToType(tcs));
        }

        /// <summary>
        /// Method gets the most recent timecard status for all the timecards the current user has permission
        /// to access. 
        /// You can also set the getAll parameter to true to get all timecard statuses, not just the most recent.
        /// </summary>
        /// <param name="getAll"></param>
        /// <returns></returns>
        public async Task<IEnumerable<TimecardStatus>> GetTimecardStatusesAsync(bool getAll = false)
        {
            var personIds = new List<string>() { CurrentUser.PersonId };

            if (HasPermission(TimeManagementPermissionCodes.ApproveRejectEmployeeTimecard))
            {
                var subordinateIds = await supervisorsRepository.GetSuperviseesBySupervisorAsync(CurrentUser.PersonId);
                if (subordinateIds == null)
                {
                    logger.Info("supervisorsRepository returned null for superivsor " + CurrentUser.PersonId);
                } 
                else
                {
                    personIds.AddRange(subordinateIds);
                }
            }

            var timecardStatusEntities = await statusesRepository.GetTimecardStatusesByPersonIdsAsync(personIds);
            if (timecardStatusEntities == null) 
            {
                logger.Error("Null timecard status entities returned by repository");
                return new List<TimecardStatus>();
            }

            //select latest
            var filteredEntities = timecardStatusEntities;
            if (!getAll)
            {
                filteredEntities = timecardStatusEntities.GroupBy(ts => ts.TimecardId)
                   .Select(group => group.OrderByDescending(ts => ts.Timestamp.AddDateTime).FirstOrDefault())
                   .Where(ts => ts != null);
            }

            var statusEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.TimecardStatus, TimecardStatus>();

            return filteredEntities.Select(ts => statusEntityToDtoAdapter.MapToType(ts));





        }

        
    }
}
