﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Dtos.TimeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Services
{
    public interface ITimecardsService
    {
        /// <summary>
        /// Gets a list of timecard dtos async
        /// </summary>
        /// <returns>list of timecards</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        Task<IEnumerable<Timecard>> GetTimecardsAsync();

        /// <summary>
        /// Gets a single timecard dto
        /// </summary>
        /// <returns>timecard task</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        Task<Timecard> GetTimecardAsync(string timecardId);

        /// <summary>
        /// Creates a single timecard
        /// </summary>
        /// <returns>the udpated timecard</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        Task<Timecard> CreateTimecardAsync(Timecard timecard);

        /// <summary>
        /// Updates a timecard
        /// </summary>
        /// <returns>the udpated timecard</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        Task<Timecard> UpdateTimecardAsync(Timecard timecard);
        /// <summary>
        /// Gets a list of timecard dtos async
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Timecard2>> GetTimecards2Async();

        /// <summary>
        /// Gets a single timecard dto
        /// </summary>
        /// <returns>timecard task</returns>
        Task<Timecard2> GetTimecard2Async(string timecardId);

        /// <summary>
        /// Creates a single timecard
        /// </summary>
        /// <returns>the created timecard2</returns>
        Task<Timecard2> CreateTimecard2Async(Timecard2 timecard2);

        /// <summary>
        /// Updates a timecard
        /// </summary>
        /// <returns>the updated timecard2</returns>
        Task<Timecard2> UpdateTimecard2Async(Timecard2 timecard2);
    }
}
