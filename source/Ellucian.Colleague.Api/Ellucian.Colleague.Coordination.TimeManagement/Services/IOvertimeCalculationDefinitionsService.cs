﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Services
{
     public interface IOvertimeCalculationDefinitionsService
     {
          /// <summary>
          /// Gets a list of OvertimeCalculationDefinition dtos async
          /// </summary>
          /// <returns></returns>
          Task<IEnumerable<Dtos.TimeManagement.OvertimeCalculationDefinition>> GetOvertimeCalculationDefinitionsAsync();
     }
}
