﻿using Ellucian.Colleague.Dtos.TimeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Services
{
    public interface ITimeEntryCommentsService
    {
        Task<IEnumerable<TimeEntryComments>> GetCommentsAsync();

        Task<TimeEntryComments> CreateCommentsAsync(TimeEntryComments comments);
    }
}
