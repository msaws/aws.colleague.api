﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.TimeManagement;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.TimeManagement.Services;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.Base.Repositories;
namespace Ellucian.Colleague.Coordination.TimeManagement.Services
{
    [RegisterType]
    public class TimeEntryCommentsService : BaseCoordinationService, ITimeEntryCommentsService
    {
        private readonly ITimeEntryCommentsRepository commentsRepository;
        private readonly ISupervisorsRepository supervisorsRepository;
        private readonly IPersonBaseRepository personBaseRepository;

        /// <summary>
        /// Comments service constructor
        /// </summary>
        /// <param name="commentsRepository"></param>
        /// <param name="adapterRegistry"></param>
        /// <param name="currentUserFactory"></param>
        /// <param name="roleRepository"></param>
        /// <param name="logger"></param>
        public TimeEntryCommentsService(
            ITimeEntryCommentsRepository commentsRepository,
            ISupervisorsRepository supervisorsRepository,
              IPersonBaseRepository personBaseRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger
              )
            : base(
             adapterRegistry, currentUserFactory, roleRepository, logger
         )
        {
            this.commentsRepository = commentsRepository;
            this.supervisorsRepository = supervisorsRepository;
            this.personBaseRepository = personBaseRepository;
        }

        /// <summary>
        /// Gets all comments a user has permission to get
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Dtos.TimeManagement.TimeEntryComments>> GetCommentsAsync()
        {
            var personIds = new List<string>() { CurrentUser.PersonId };
            IEnumerable<PersonBase> personBaseEntities;

            if (HasPermission(TimeManagementPermissionCodes.ApproveRejectEmployeeTimecard))
            {
                var superviseeIds = (await supervisorsRepository.GetSuperviseesBySupervisorAsync(CurrentUser.PersonId));

                if (superviseeIds == null)
                {
                    var message = "Unexpected null person id list returned from supervisors repository";
                    logger.Error(message);
                }
                if (superviseeIds != null && superviseeIds.Any())
                {
                    personIds.AddRange(superviseeIds);
                }
            }
            var comments = await commentsRepository.GetCommentsAsync(personIds);

            if (comments == null)
            {
                var message = "Null comments returned to coordination service";
                logger.Error(message);
                return new List<Dtos.TimeManagement.TimeEntryComments>();
            }
            var commentsEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.TimeEntryComment, Dtos.TimeManagement.TimeEntryComments>();
            var commentDtos = comments.Select(tc => commentsEntityToDtoAdapter.MapToType(tc)).ToList();

            // get the names of the authors of each comment
            var personAndOpersIds = commentDtos.Select(x => x.CommentsTimestamp.AddOperator).Distinct().ToList();
            var authorIds = new List<string>();
            var opersIdDictionary = new Dictionary<string, string>();
            // resolve all opersIds to numerical Ids
            foreach (var opersId in personAndOpersIds)
            {
                var personId = await personBaseRepository.GetPersonIdFromOpersAsync(opersId);
                opersIdDictionary.Add(opersId, personId);
                authorIds.Add(personId);
            }

            // only call GetPersonsBaseAsync if authorIds contains elements
            if (authorIds.Any())
            {
                personBaseEntities = await personBaseRepository.GetPersonsBaseAsync(authorIds);

                foreach (var dto in commentDtos)
                {
                    if (personBaseEntities != null)
                    {
                        // try to select the correct PersonBaseEntity for the current Dto
                        string personId;
                        opersIdDictionary.TryGetValue(dto.CommentsTimestamp.AddOperator, out personId);
                        var personBaseEntity = personBaseEntities.FirstOrDefault(x => x.Id == personId);
                        if (personBaseEntity != null)
                        {
                            // if the entity has a FirstName, use FirstName and LastName, else just set dto.CommentAuthorName to LastName
                            if (!string.IsNullOrWhiteSpace(personBaseEntity.FirstName))
                                dto.CommentAuthorName = personBaseEntity.FirstName + " " + personBaseEntity.LastName;
                            else
                                dto.CommentAuthorName = personBaseEntity.LastName;
                        }
                        //if a matching PersonBaseEntity was not found for the supplied AddOperator
                        else
                        {
                            dto.CommentAuthorName = dto.CommentsTimestamp.AddOperator;
                        }
                    }
                    // if there were no PersonBaseEntities returned
                    else
                    {
                        dto.CommentAuthorName = dto.CommentsTimestamp.AddOperator;
                    }
                }
            }
            return commentDtos;
        }

        /// <summary>
        /// Creates a comment record
        /// </summary>
        /// <param name="comments"></param>
        /// <returns></returns>
        public async Task<Dtos.TimeManagement.TimeEntryComments> CreateCommentsAsync(Dtos.TimeManagement.TimeEntryComments comments)
        {
            if (comments == null)
            {
                throw new ArgumentNullException("comments");
            }


            if (CurrentUser.IsPerson(comments.EmployeeId))
            {
                return await CreateCommentsForPersonAsync(comments);
            }
            else if (HasPermission(TimeManagementPermissionCodes.ApproveRejectEmployeeTimecard))
            {
                var subordinateIds = (await supervisorsRepository.GetSuperviseesBySupervisorAsync(CurrentUser.PersonId)).ToList();

                if (subordinateIds == null || !subordinateIds.Contains(comments.EmployeeId))
                {
                    var message = string.Format("Supervisor {0} doesn't supervise {1}", CurrentUser.PersonId, comments.EmployeeId);
                    logger.Error(message);
                    throw new PermissionsException(message);

                }
                else
                {
                    return await CreateCommentsForPersonAsync(comments);
                }
            }
            else
            {
                var message = string.Format("Current user {0} does not have permissions to create comments for employee {1}.", CurrentUser.PersonId, comments.EmployeeId);
                logger.Error(message);
                throw new PermissionsException(message);
            }
        }

        private async Task<Dtos.TimeManagement.TimeEntryComments> CreateCommentsForPersonAsync(Dtos.TimeManagement.TimeEntryComments comments)
        {
            var commentsDtoToEntityAdapter = _adapterRegistry.GetAdapter<Dtos.TimeManagement.TimeEntryComments, Domain.TimeManagement.Entities.TimeEntryComment>();
            var commentsEntity = commentsDtoToEntityAdapter.MapToType(comments);

            var newComments = await commentsRepository.CreateCommentsAsync(commentsEntity);
            if (newComments == null)
            {
                var message = "Unexpected null returned by comments repository while trying to create comments";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            var commentsEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.TimeEntryComment, Dtos.TimeManagement.TimeEntryComments>();
            var returnCommentDto = commentsEntityToDtoAdapter.MapToType(newComments);

            var authorId = await personBaseRepository.GetPersonIdFromOpersAsync(returnCommentDto.CommentsTimestamp.AddOperator);
            var personBaseEntity = await personBaseRepository.GetPersonBaseAsync(authorId);
            if (personBaseEntity != null)
            {
                // if the entity has a FirstName, use FirstName and LastName, else just set dto.CommentAuthorName to LastName
                if (!string.IsNullOrWhiteSpace(personBaseEntity.FirstName))
                    returnCommentDto.CommentAuthorName = personBaseEntity.FirstName + " " + personBaseEntity.LastName;
                else
                    returnCommentDto.CommentAuthorName = personBaseEntity.LastName;
            }
            //if a matching PersonBaseEntity was not found for the supplied AddOperator
            else
            {
                returnCommentDto.CommentAuthorName = returnCommentDto.CommentsTimestamp.AddOperator;
            }
            return returnCommentDto;
        }
    }
}
