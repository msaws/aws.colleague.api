﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Data.TimeManagement.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Services
{
     [RegisterType]
     public class OvertimeCalculationDefinitionsService: BaseCoordinationService, IOvertimeCalculationDefinitionsService
     {
          private readonly IOvertimeCalculationDefinitionsRepository overtimeCalculationDefinitionRepository;

        /// <summary>
        /// Constructor for OvertimeCalculationDefinition service
        /// </summary>
        /// <param name="overtimeCalculationDefinitionRepository"></param>
        /// <param name="adapterRegistry"></param>
        /// <param name="currentUserFactory"></param>
        /// <param name="roleRepository"></param>
        /// <param name="logger"></param>
        public OvertimeCalculationDefinitionsService(
            IOvertimeCalculationDefinitionsRepository overtimeCalculationDefinitionRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger
            ) : base (
                adapterRegistry, currentUserFactory, roleRepository, logger
            )
        {
            this.overtimeCalculationDefinitionRepository = overtimeCalculationDefinitionRepository;
        }
        /// <summary>
        /// Gets all OvertimeCalculationDefinition Dtos
        /// </summary>
        /// <param name=></param>
        /// <returns>List of OvertimeCalculationDefinition tasks</returns>
        public async Task<IEnumerable<Dtos.TimeManagement.OvertimeCalculationDefinition>> GetOvertimeCalculationDefinitionsAsync()
        {

             var overtimeCalculationDefinitionEntities = await overtimeCalculationDefinitionRepository.GetOvertimeCalculationDefinitionsAsync();

             if (overtimeCalculationDefinitionEntities == null)
             {
                  var message = "Null OvertimeCalculationDefinitions returned to coordination service";
                  logger.Error(message);
                  throw new ApplicationException(message);
             }

             var overtimeCalculationDefinitionEntityToDtoAdapter = _adapterRegistry.GetAdapter<Domain.TimeManagement.Entities.OvertimeCalculationDefinition, Dtos.TimeManagement.OvertimeCalculationDefinition>();
             return overtimeCalculationDefinitionEntities.Select(otcd => overtimeCalculationDefinitionEntityToDtoAdapter.MapToType(otcd)).ToList();
        }
     }
}
