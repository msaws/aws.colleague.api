﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Dtos.TimeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Services
{
    public interface ITimecardHistoriesService
    {
        /// <summary>
        /// Gets a list of timecard history DTOs asynchronously
        /// </summary>
        /// <returns>List of timecard histories</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        Task<IEnumerable<TimecardHistory>> GetTimecardHistoriesAsync(DateTime startDate, DateTime endDate);

        /// <summary>
        /// Gets a list of timecard history DTOs asynchronously
        /// </summary>
        /// <returns>List of timecard history 2s</returns>
        Task<IEnumerable<TimecardHistory2>> GetTimecardHistories2Async(DateTime startDate, DateTime endDate);
    }
}
