﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Dtos.TimeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Services
{
    public interface ITimecardStatusesService
    {
        /// <summary>
        /// Creates timecard statuses
        /// </summary>
        /// <param name="statuses"></param>
        /// <returns>List of created TimecardStatus</returns>
        Task<IEnumerable<TimecardStatus>> CreateTimecardStatusesAsync(List<TimecardStatus> statuses);

        /// <summary>
        /// Gets all statuses for a given timecard
        /// </summary>
        /// <param name="timecardId"></param>
        /// <returns></returns>
        Task<IEnumerable<TimecardStatus>> GetTimecardStatusesByTimecardIdAsync(string timecardId);

        /// <summary>
        /// Gets all timecard statuses
        /// </summary>
        /// <param name="getAll"></param>
        /// <returns></returns>
        Task<IEnumerable<TimecardStatus>> GetTimecardStatusesAsync(bool getAll = false);
    }
}
