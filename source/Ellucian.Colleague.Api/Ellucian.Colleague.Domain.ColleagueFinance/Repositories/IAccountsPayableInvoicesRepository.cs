﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;

namespace Ellucian.Colleague.Domain.ColleagueFinance.Repositories
{
    public interface IAccountsPayableInvoicesRepository
    {
        Task<Tuple<IEnumerable<AccountsPayableInvoices>, int>> GetAccountsPayableInvoicesAsync(int offset, int limit);
        Task<AccountsPayableInvoices> GetAccountsPayableInvoicesByGuidAsync(string guid, bool allowVoid);
        Task<AccountsPayableInvoices> UpdateAccountsPayableInvoicesAsync(AccountsPayableInvoices accountsPayableInvoicesEntity);
        Task<AccountsPayableInvoices> CreateAccountsPayableInvoicesAsync(AccountsPayableInvoices accountsPayableInvoicesEntity);
        Task<string> GetAccountsPayableInvoicesIdFromGuidAsync(string guid);
    }
}