﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;

namespace Ellucian.Colleague.Domain.ColleagueFinance.Repositories
{
    /// <summary>
    /// Definition of methods implemented for a requisition repository
    /// </summary>
    public interface IRequisitionRepository
    {
        /// <summary>
        /// Get a single requisition
        /// </summary>
        /// <param name="requisitionId">The requisition ID to retrieve</param>
        /// <param name="personId">The user ID</param>
        /// <param name="glAccessLevel">The user GL account security level</param>
        /// <param name="expenseAccounts">Set of GL Accounts to which the user has access.</param>
        /// <returns>A requisition domain entity</returns>
        Task<Requisition> GetRequisitionAsync(string requisitionId, string personId, GlAccessLevel glAccessLevel, IEnumerable<string> expenseAccounts);
    }
}
