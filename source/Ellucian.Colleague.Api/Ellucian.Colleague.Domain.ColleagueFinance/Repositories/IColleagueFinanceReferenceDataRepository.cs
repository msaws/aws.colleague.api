﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using System;

namespace Ellucian.Colleague.Domain.ColleagueFinance.Repositories
{
    /// <summary>
    /// Provides read-only access to basic data required for Colleague Finance Self Service
    /// </summary>
    public interface IColleagueFinanceReferenceDataRepository
    {
        /// <summary>
        /// Get a collection of AccountComponents
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of AccountComponents</returns>
        Task<IEnumerable<AccountComponents>> GetAccountComponentsAsync(bool ignoreCache);

        /// <summary>
        /// Get a collection of GL source codes
        /// </summary>
        /// <param name="ignoreCache"></param>
        /// <returns></returns>
        Task<IEnumerable<GlSourceCodes>> GetGlSourceCodesValcodeAsync(bool ignoreCache);

        /// <summary>
        /// Get a collectoin of accounting string component values
        /// </summary>
        Task<Tuple<IEnumerable<AccountingStringComponentValues>,int>> GetAccountingStringComponentValuesAsync(int Offset, int Limit, string component, string transactionStatus, string typeAccount, string typeFund, bool bypassCache);

        /// <summary>
        /// Get a accounting string component value from a Guid
        /// </summary>
        /// <param name="Guid"></param>
        /// <returns></returns>
        Task<AccountingStringComponentValues> GetAccountingStringComponentValueByGuid(string Guid);

        /// <summary>
        /// Get a collection of AccountFormats
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of AccountFormats</returns>
        Task<IEnumerable<AccountingFormat>> GetAccountFormatsAsync(bool ignoreCache);

        /// <summary>
        /// Returns domain entities for AccountsPayableSources.
        /// </summary>
        Task<IEnumerable<AccountsPayableSources>> GetAccountsPayableSourcesAsync(bool ignoreCache);

        /// <summary>
        /// Returns domain entities for Accounts Payable Tax codes.
        /// </summary>
        Task<IEnumerable<AccountsPayableTax>> GetAccountsPayableTaxCodesAsync();

        /// <summary>
        /// Returns domain entities for accounts payable type codes.
        /// </summary>
        Task<IEnumerable<AccountsPayableType>> GetAccountsPayableTypeCodesAsync();

        /// <summary>
        /// Returns domain entities for commodity codes.
        /// </summary>
        Task<IEnumerable<CommodityCode>> GetCommodityCodesAsync(bool ignoreCache);

        /// <summary>
        /// Returns domain entities for commodity unit types.
        /// </summary>
        Task<IEnumerable<CommodityUnitType>> GetCommodityUnitTypesAsync(bool ignoreCache);

        /// <summary>
        /// Returns domain entities for CurrencyConversion
        /// </summary>
        Task<IEnumerable<CurrencyConversion>> GetCurrencyConversionAsync();

        /// <summary>
        /// Returns domain entities for free on board types.
        /// </summary>
        Task<IEnumerable<FreeOnBoardType>> GetFreeOnBoardTypesAsync(bool ignoreCache);

        /// <summary>
        /// Returns domain entities for shipping methods.
        /// </summary>
        Task<IEnumerable<ShippingMethod>> GetShippingMethodsAsync(bool ignoreCache);

        /// <summary>
        /// Get a collection of ShipToDestinations
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of ShipToDestinations</returns>
        Task<IEnumerable<ShipToDestination>> GetShipToDestinationsAsync(bool ignoreCache);

        /// <summary>
        /// Returns domain entities for VendorTerms.
        /// </summary>
        Task<IEnumerable<VendorTerm>> GetVendorTermsAsync(bool ignoreCache);

        /// <summary>
        /// Returns domain entities for commodity unit types.
        /// </summary>
        Task<IEnumerable<VendorType>> GetVendorTypesAsync(bool ignoreCache);

        // <summary>
        /// Returns domain entities for VendorHoldReasons.
        /// </summary>
        Task<IEnumerable<VendorHoldReasons>> GetVendorHoldReasonsAsync(bool ignoreCache);
    }
}