﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System.Threading.Tasks;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;

namespace Ellucian.Colleague.Domain.ColleagueFinance.Repositories
{
    /// <summary>
    /// This is the definition of the methods that must be implemented
    /// for a recurring voucher repository.
    /// </summary>
    public interface IRecurringVoucherRepository
    {
        /// <summary>
        /// Get a specific recurring voucher
        /// </summary>
        /// <param name="recurringVoucherId">the requested recurring voucher ID</param>
        /// <returns>Recurring voucher domain entity</returns>
        Task<RecurringVoucher> GetRecurringVoucherAsync(string recurringVoucherId);
    }
}
