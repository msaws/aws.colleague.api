﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;

namespace Ellucian.Colleague.Domain.ColleagueFinance.Repositories
{
    public interface IVendorsRepository
    {
        Task<Tuple<IEnumerable<Vendors>, int>> GetVendorsAsync(int offset, int limit, string vendorDetail = "", string classifications = "",
            string status = "");
        Task<Vendors> GetVendorsByGuidAsync(string guid);

        Task<Vendors> UpdateVendorsAsync(Vendors vendorsEntity);
        Task<Vendors> CreateVendorsAsync(Vendors vendorsEntity);
       
        Task<string> GetVendorGuidFromIdAsync(string id);
        Task<string> GetVendorIdFromGuidAsync(string id);


    }
}