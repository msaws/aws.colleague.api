﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;

namespace Ellucian.Colleague.Domain.ColleagueFinance
{
    [Serializable]
    public static class ColleagueFinancePermissionCodes
    {
        // Update any Account Payable Invoice
        public const string UpdateApInvoices = "UPDATE.AP.INVOICES";

        // Update any Vendor
        public const string UpdateVendors = "UPDATE.VENDORS";
              
        //View any Account Funds Available information
        public const string ViewAccountFundsAvailable = "VIEW.ACCOUNT.FUNDS.AVAILABLE";

        // View any Account Payable Invoice
        public const string ViewApInvoices = "VIEW.AP.INVOICES";

        // View any vendor information
        public const string ViewVendors = "VIEW.VENDORS";

        // Update any Purchase Order
        public const string UpdatePurchaseOrders = "UPDATE.PURCHASE.ORDERS";

        // View any PurchaseOrder
        public const string ViewPurchaseOrders = "VIEW.PURCHASE.ORDERS";

    }
}