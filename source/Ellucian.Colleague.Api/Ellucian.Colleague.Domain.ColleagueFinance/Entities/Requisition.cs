﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.ColleagueFinance.Entities
{
    /// <summary>
    /// This is a Requisition
    /// </summary>
    [Serializable]
    public class Requisition : AccountsPayablePurchasingDocument
    {
        /// <summary>
        /// Private number for public getter
        /// </summary>
        private readonly string number;

        /// <summary>
        /// The requisition number
        /// </summary>
        public string Number { get { return number; } }

        /// <summary>
        /// The requisition desired date
        /// </summary>
        public DateTime? DesiredDate { get; set; }

        /// <summary>
        /// Private current status for public getter
        /// </summary>
        private readonly RequisitionStatus status;

        /// <summary>
        /// The requisition current status
        /// </summary>
        public RequisitionStatus Status { get { return status; } }

        /// <summary>
        /// Private current status date for public getter
        /// </summary>
        private readonly DateTime statusDate;

        /// <summary>
        /// The requisition current status date
        /// </summary>
        public DateTime StatusDate { get { return statusDate; } }

        /// <summary>
        /// The requisition initiator name
        /// </summary>
        public string InitiatorName { get; set; }

        /// <summary>
        /// The requisition requestor name
        /// </summary>
        public string RequestorName { get; set; }

        /// <summary>
        /// The requisition ship to code
        /// </summary>
        public string ShipToCode { get; set; }

        /// <summary>
        /// The blanket purchase order associated with the requisition
        /// </summary>
        public string BlanketPurchaseOrder { get;  set; }

        /// <summary>
        /// The requisition internal comments
        /// </summary>
        public string InternalComments { get; set; }

        /// <summary>
        /// The private list of purchase orders associated with the requisition
        /// </summary>
        private readonly List<string> purchaseOrders = new List<string>();

        /// <summary>
        /// The public getter for the private list of purchase orders associated with the requisition
        /// </summary>
        public ReadOnlyCollection<string> PurchaseOrders { get; private set; }

        /// <summary>
        /// This constructor initializes the requisition domain entity
        /// </summary>
        /// <param name="id">Requisition ID</param>
        /// <param name="number">Requisition number</param>
        /// <param name="vendorName">Requisition vendor name</param>
        /// <param name="status">Requisition status</param>
        /// <param name="statusDate">Requisition status date</param>
        /// <param name="date">Requisition date</param>
        /// <exception cref="ArgumentNullException">Thrown if any applicable parameters are null</exception>
        public Requisition(string id, string number, string vendorName, RequisitionStatus status, DateTime statusDate, DateTime date)
            : base(id, vendorName, date)
        {
            if (string.IsNullOrEmpty(number))
            {
                throw new ArgumentNullException("number", "Number is a required field.");
            }

            this.number = number;
            this.status = status;
            this.statusDate = statusDate;
            PurchaseOrders = this.purchaseOrders.AsReadOnly();
        }

        /// <summary>
        /// This method adds a purchase order to the list of purchase orders for the requisition
        /// </summary>
        /// <param name="purchaseOrder">The associated purchase order</param>
        public void AddPurchaseOrder(string purchaseOrder)
        {
            if (string.IsNullOrEmpty(purchaseOrder))
            {
                throw new ArgumentNullException("purchaseOrder", "Purchase Order cannot be null");
            }
            if (purchaseOrders != null && !this.purchaseOrders.Contains(purchaseOrder))
            {
                this.purchaseOrders.Add(purchaseOrder);
            }
        }
    }
}
