﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using Ellucian.Colleague.Domain.Base.Exceptions;

namespace Ellucian.Colleague.Domain.ColleagueFinance.Entities
{
    /// <summary>
    /// Contains information regarding the fiscal year of the institution.
    /// </summary>
    [Serializable]
    public class GeneralLedgerFiscalYearConfiguration
    {
        /// <summary>
        /// Private variable for the fiscal year start month
        /// </summary>
        private readonly int startMonth;

        /// <summary>
        /// Public getter for the private fiscal year start month.
        /// </summary>
        public int StartMonth { get { return startMonth; } }

        /// <summary>
        /// Private variable for the current fiscal year.
        /// </summary>
        private readonly string currentFiscalYear;

        /// <summary>
        /// Public getter for the private current fiscal year.
        /// </summary>
        public string CurrentFiscalYear { get { return currentFiscalYear; } }

        /// <summary>
        /// Calculate the fiscal year based on the current date.
        /// </summary>
        public int FiscalYearForToday
        {
            get
            {
                // Initialize the current year using the currentDate variable, but increase it to the next year
                // if the current month falls AFTER the start month.
                int currentYear = DateTime.Now.Year;
                if (DateTime.Now.Month >= this.startMonth)
                    currentYear += 1;

                return currentYear;
            }
        }

        /// <summary>
        /// Initializes the GL fiscal year information.
        /// </summary>
        /// <param name="startMonth">Month on which a fiscal year starts.</param>
        /// <param name="currentYear">Current fiscal year for the institution.</param>
        public GeneralLedgerFiscalYearConfiguration(int startMonth, string currentYear)
        {
            if (string.IsNullOrEmpty(currentYear))
                throw new ArgumentNullException("currentYear", "currentYear must have a value.");

            if (startMonth < 1 || startMonth > 12)
                throw new ConfigurationException("Fiscal year start month must be in between 1 and 12.");

            this.startMonth = startMonth;
            this.currentFiscalYear = currentYear;
        }
    }
}