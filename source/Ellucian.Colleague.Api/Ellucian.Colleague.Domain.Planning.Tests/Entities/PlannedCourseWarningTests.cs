﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Domain.Planning.Entities;
using Ellucian.Colleague.Domain.Student.Entities;

namespace Ellucian.Colleague.Domain.Planning.Tests.Entities
{
    class PlannedCourseWarningTests
    {
        [TestClass]
        public class PlannedCourseWarningConstructor
        {
            [TestMethod]
            public void ReturnsValidPlannedCourseWarning()
            {
                Requisite req = new Requisite("PREREQ1", true, RequisiteCompletionOrder.Previous, false);
                PlannedCourseWarning warning = new PlannedCourseWarning(PlannedCourseWarningType.UnmetRequisite);
                warning.Requisite = req;
                Assert.AreEqual(PlannedCourseWarningType.UnmetRequisite, warning.Type);
                var warningReq = warning.Requisite;
                Assert.IsTrue(warningReq.IsRequired);
                Assert.AreEqual("PREREQ1", warningReq.RequirementCode);
                Assert.AreEqual(RequisiteCompletionOrder.Previous, warningReq.CompletionOrder);
            }

            [TestMethod]
            public void SetsWarningType()
            {
                PlannedCourseWarning warning = new PlannedCourseWarning(PlannedCourseWarningType.TimeConflict);
                warning.SectionId = "111";
                Assert.AreEqual(PlannedCourseWarningType.TimeConflict, warning.Type);
                Assert.AreEqual("111", warning.SectionId);
            }

            [TestMethod]
            public void ReturnsValidCoreqWarning()
            {
                Requisite req = new Requisite("1232", false);
                PlannedCourseWarning warning = new PlannedCourseWarning(PlannedCourseWarningType.UnmetRequisite);
                warning.Requisite = req;
                Assert.AreEqual(PlannedCourseWarningType.UnmetRequisite, warning.Type);
                var warningReq = warning.Requisite;
                Assert.IsFalse(warningReq.IsRequired);
                Assert.AreEqual("1232", warningReq.CorequisiteCourseId);
                Assert.AreEqual(RequisiteCompletionOrder.PreviousOrConcurrent, warningReq.CompletionOrder);
            }
        }
    }
}
