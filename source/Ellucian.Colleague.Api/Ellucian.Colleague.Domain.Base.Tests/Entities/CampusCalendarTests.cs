﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Domain.Base.Entities;

namespace Ellucian.Colleague.Domain.Base.Tests.Entities
{
    [TestClass]
    public class CampusCalendarTests
    {
        private string id;
        private string description;
        private DateTime defaultStartOfDay;
        private DateTime defaultEndOfDay;
        private CampusCalendar campusCalendar;

        [TestInitialize]
        public void Initialize()
        {
            id = "MAIN";
            description = "Main Calendar for PID2";
            defaultStartOfDay = new DateTime(1967, 12, 31, 0, 0, 0);
            defaultEndOfDay = new DateTime(1967, 12, 31, 11, 59, 59);
              
            campusCalendar = new CampusCalendar(id, description, defaultStartOfDay, defaultEndOfDay);
        }

        [TestClass]
        public class CampusCalendarConstructorTests : CampusCalendarTests
        {
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CampusCalendarConstructorNullId()
            {
                campusCalendar = new CampusCalendar(null, description, defaultStartOfDay, defaultEndOfDay);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CampusCalendarConstructorEmptyId()
            {
                campusCalendar = new CampusCalendar(string.Empty, description, defaultStartOfDay, defaultEndOfDay);
            }

            [TestMethod]
            public void CampusCalendarConstructorValidId()
            {
                campusCalendar = new CampusCalendar(id, description, defaultStartOfDay, defaultEndOfDay);
                Assert.AreEqual(id, campusCalendar.Id);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CampusCalendarConstructorNullDescription()
            {
                campusCalendar = new CampusCalendar(id, null, defaultStartOfDay, defaultEndOfDay);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CampusCalendarConstructorEmptyDescription()
            {
                campusCalendar = new CampusCalendar(id, string.Empty, defaultStartOfDay, defaultEndOfDay);
            }

            [TestMethod]
            public void CampusCalendarConstructorValidDescription()
            {
                campusCalendar = new CampusCalendar(id, description, defaultStartOfDay, defaultEndOfDay);
                Assert.AreEqual(description, campusCalendar.Description);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void CampusCalendarConstructorInvalidEndOfDay()
            {
                campusCalendar = new CampusCalendar(id, description, defaultEndOfDay, defaultStartOfDay);
            }

            [TestMethod]
            public void CampusCalendarConstructorValidStartOfDay()
            {
                campusCalendar = new CampusCalendar(id, description, defaultStartOfDay, defaultEndOfDay);
                Assert.AreEqual(defaultStartOfDay, campusCalendar.DefaultStartOfDay);
            }

            [TestMethod]
            public void CampusCalendarConstructorValidEndOfDay()
            {
                campusCalendar = new CampusCalendar(id, description, defaultStartOfDay, defaultEndOfDay);
                Assert.AreEqual(defaultEndOfDay, campusCalendar.DefaultEndOfDay);
            }
        }

        [TestClass]
        public class CampusCalendarAddSpecialDayTests : CampusCalendarTests
        {
            [TestMethod]
            public void CampusCalendarAddSpecialDay()
            {
                var specialDay = DateTime.Today.AddDays(5);
                campusCalendar.AddSpecialDay(specialDay);
                Assert.AreEqual(specialDay, campusCalendar.SpecialDays[0]);
            }

            [TestMethod]
            public void CampusCalendarAddDuplicateSpecialDay()
            {
                var specialDay = DateTime.Today.AddDays(5);
                campusCalendar.AddSpecialDay(specialDay);
                campusCalendar.AddSpecialDay(specialDay);
                Assert.AreEqual(1, campusCalendar.SpecialDays.Count);
                Assert.AreEqual(specialDay, campusCalendar.SpecialDays[0]);
            }
        }
    }
}
