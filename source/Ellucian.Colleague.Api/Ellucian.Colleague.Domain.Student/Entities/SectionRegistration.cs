﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.Student.Entities
{
    [Serializable]
    public class SectionRegistration
    {
        public string Guid { get; set; }
        public string SectionId { get; set; }
        public RegistrationAction Action { get; set; }
        public decimal? Credits { get; set; }
        public decimal? Ceus { get; set; }
        public DateTime? RegistrationDate { get; set; }
        public string AcademicLevelCode { get; set; }
    }

    [Serializable]
    public enum RegistrationAction
    {
        Add,
        PassFail,
        Audit,
        Drop,
        Waitlist,
        RemoveFromWaitlist
    }
}
