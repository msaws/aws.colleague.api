﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Domain.Entities;
using System;

namespace Ellucian.Colleague.Domain.Student.Entities
{
    [Serializable]
    public class CourseStatusItem : GuidCodeItem
    {
        /// <summary>
        /// The course status
        /// </summary>
        public CourseStatus Status { get; set; }

        public CourseStatusItem(string guid, string code, string description)
            : base(guid, code, description)
        {
        }
    }
}