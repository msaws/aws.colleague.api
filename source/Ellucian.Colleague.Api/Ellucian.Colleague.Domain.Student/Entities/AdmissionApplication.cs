﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.Student.Entities
{
    [Serializable]
    public class AdmissionApplication
    {
        public AdmissionApplication(string guid, string applicantKey)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw new ArgumentNullException("Admission application guid is required.");
            }
            if (string.IsNullOrEmpty(applicantKey))
            {
                throw new ArgumentNullException("Admission application record key is required.");
            }

            Guid = guid;
            ApplicantRecordKey = applicantKey;
        }

        public string ApplicantPersonId { get; set; }

        public string Guid { get; private set; }

        public string ApplicantRecordKey { get; private set; }

        public string ApplicationOwnerId { get; set; }

        public string ApplicationNo { get; set; }

        public string ApplicationStartTerm { get; set; }

        public List<AdmissionApplicationStatus> AdmissionApplicationStatuses { get; set; }

        public string ApplicationSource { get; set; }

        public string ApplicationAdmissionsRep { get; set; }

        public string ApplicationAdmitStatus { get; set; }

        public List<string> ApplicationLocations { get; set; }

        public string ApplicationResidencyStatus { get; set; }

        public string ApplicationStudentLoadIntent { get; set; }

        public string ApplicationAcadProgram { get; set; }

        public List<string> ApplicationStprAcadPrograms { get; set; }

        public string ApplicationWithdrawReason { get; set; }

        public string ApplicationAttendedInstead { get; set; }

        public string ApplicationComments { get; set; }

        public string ApplicationSchool { get; set; } 
    }
}
