﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.Student.Entities
{
    [Serializable]
    public class Book
    {
        private readonly string _Id;
        public string Id { get { return _Id; } }

        public decimal? Price { get; set; }

        public Book(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id", "Id must be provided");
            }
            _Id = id;
        }
    }
}
