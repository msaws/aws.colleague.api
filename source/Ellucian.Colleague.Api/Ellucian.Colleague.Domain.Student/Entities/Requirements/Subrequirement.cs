﻿// Copyright 2013 - 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;

namespace Ellucian.Colleague.Domain.Student.Entities.Requirements
{
    [Serializable]
    public class Subrequirement : RequirementBlock
    {
        public Requirement Requirement { get; set; }
        public List<Group> Groups { get; set; }
        public int? MinGroups { get; set; }

        public Subrequirement(string id, string code)
            : base(id, code)
        {
            Groups = new List<Group>();
        }

        public SubrequirementResult Evaluate(List<GroupResult> groupresults)
        {
            SubrequirementResult srr = new SubrequirementResult(this);

            srr.GroupResults.AddRange(groupresults);
            int satisfiedgroups = srr.GetSatisfied().Count();
            int plannedSatisfiedGroups = srr.GetPlannedSatisfied().Count();
            int mingroups = (MinGroups.HasValue && MinGroups.Value != 0) ? MinGroups.Value : srr.SubRequirement.Groups.Count();
            
            if (MinGpa.HasValue)
            {
                if (srr.Gpa < MinGpa.Value)
                {
                    srr.Explanations.Add(SubrequirementExplanation.MinGpa);
                }
            }

            if (MinInstitutionalCredits.HasValue)
            {
                if (srr.GetAppliedInstitutionalCredits() < MinInstitutionalCredits.Value)
                {
                    srr.Explanations.Add(SubrequirementExplanation.MinInstitutionalCredits);
                }
            }
            
            if (satisfiedgroups < mingroups)
            {
                srr.Explanations.Add(SubrequirementExplanation.MinGroups);
            }

            // If all groups are not complete or the min institutional credits is not met, determine if we can call it "fully planned"
            if (srr.Explanations.Contains(SubrequirementExplanation.MinGroups) || srr.Explanations.Contains(SubrequirementExplanation.MinInstitutionalCredits))
            {
                // Consider planned satisfied if the minimum number of groups has been planned AND the applied institutional credits + the planned credits exceeds or equals the minimum institutional credits.
                if (satisfiedgroups + plannedSatisfiedGroups >= mingroups && (!MinInstitutionalCredits.HasValue || (srr.GetPlannedAppliedCredits() + srr.GetAppliedInstitutionalCredits()) >= MinInstitutionalCredits.Value))
                {
                    srr.Explanations.Add(SubrequirementExplanation.PlannedSatisfied);
                }
            }

            if (srr.Explanations.Count() == 0)
            {
                srr.Explanations.Add(SubrequirementExplanation.Satisfied);
            }

            return srr;
        }      
    }
}
