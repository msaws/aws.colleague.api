﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.Student.Entities.Requirements.Modifications
{
    [Serializable]
    public class FromCoursesAddition : RequirementModification
    {
        public readonly List<string> AdditionalFromCourses;

        public FromCoursesAddition(string blockid, List<string> additionalfromcourses, string message)
            : base(blockid, message)
        {
            if (blockid == null)
            {
                throw new NotSupportedException("From courses addition must apply to a block.");
            }
            AdditionalFromCourses = additionalfromcourses;
        }

        /// <summary>
        /// Adds additional courses to a Group's "FromCourses" list
        /// </summary>
        /// <param name="programRequirements">The ProgramRequirements object to be modified</param>
        public override void Modify(ProgramRequirements programRequirements, List<Requirement> additionalRequirements)
        {
            foreach (var req in programRequirements.Requirements.Union(additionalRequirements))
            {
                foreach (var sub in req.SubRequirements)
                {
                    foreach (var grp in sub.Groups)
                    {
                        if (grp.Id == blockId)
                        {
                            foreach (var crse in AdditionalFromCourses)
                            {
                                grp.IsModified = true;
                                grp.ModificationMessages.Add(this.modificationMessage);
                                grp.FromCoursesException.Add(crse);
                                grp.IsWaived = false;  // in case this was set to true by a waiver 
                            }
                            return;
                        }
                    }
                }
            }
            // did not find a matching block
            throw new KeyNotFoundException("requirements");
        }
    }
}
