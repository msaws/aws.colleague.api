﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.Student.Entities.Requirements
{
    [Serializable]
    public enum AwardType
    {
        Ccd, Major, Minor, Specialization
    }
}
