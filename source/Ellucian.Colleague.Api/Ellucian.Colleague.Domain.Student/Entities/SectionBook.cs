﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.Student.Entities
{
    [Serializable]
    public class SectionBook
    {
        /// <summary>
        /// Id of the book
        /// </summary>
        private string _BookId;
        public string BookId { get { return _BookId; } }

        /// <summary>
        /// Indicates if the book is required for the section. Anything else is considered optional.
        /// </summary>
        private bool _IsRequired;
        public bool IsRequired { get { return _IsRequired; } }

        /// <summary>
        /// Section Book constructor
        /// </summary>
        /// <param name="bookId">bookId (required)</param>
        public SectionBook(string bookId, bool isRequired)
        {
            if (string.IsNullOrEmpty(bookId))
            {
                throw new ArgumentNullException("bookId", "Book Id must be provided");
            }
            _BookId = bookId;
            _IsRequired = isRequired;
        }
    }
}
