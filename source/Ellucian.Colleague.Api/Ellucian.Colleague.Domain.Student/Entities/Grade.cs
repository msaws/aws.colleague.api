﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
using System;

namespace Ellucian.Colleague.Domain.Student.Entities
{
    [Serializable]
    public class Grade
    {
        private string _id;
        public string Id
        {
            get { return _id; }
            set
            {
                if (_id == null)
                {
                    _id = value;
                }
                else
                {
                    throw new InvalidOperationException("Id cannot be changed");
                }
            }
        }

        /// <summary>
        /// Guid
        /// </summary>        
        public string Guid { get { return _guid; } }
        private readonly string _guid;

        /// <summary>
        /// Credit 
        /// </summary>
        public string Credit { get { return _credit; } }
        private readonly string _credit;

        /// <summary>
        /// The externally appearing letter grade (Required)
        /// </summary>
        public string LetterGrade { get { return _letterGrade; } }
        private readonly string _letterGrade;

        /// <summary>
        /// The grade scheme to which this grade belongs (Required)
        /// </summary>
        public string GradeSchemeCode { get { return _gradeSchemeCode; } }
        private readonly string _gradeSchemeCode;

        /// <summary>
        /// The description for this grade (Required)
        /// </summary>
        public string Description { get { return _description; } }
        private readonly string _description;

        /// <summary>
        /// The numeric value associated with this grade
        /// </summary>
        public decimal? GradeValue { get; set; }
        
        /// <summary>
        /// Indicates if this grade may be assigned when a student Withdraws from a course
        /// </summary>
        public bool IsWithdraw { get; set; }

        /// <summary>
        /// Grade Priority is Repeat Value in Colleague
        /// </summary>
        public decimal? GradePriority { get; set; }

        /// <summary>
        /// ID of a grade in another grade scheme that is equivalent to this grade.
        /// (Used for the purpose of program evaluation of transfer credits)
        /// </summary>
        public string ComparisonGrade { get; set; }

        /// <summary>
        /// The grade to which an incomplete will revert if not updated by the expiration date.
        /// </summary>
        public string IncompleteGrade { get; set; }

        /// <summary>
        /// Indicates if this should be excluded from the list of grades presented to faculty during grading.
        /// </summary>
        public bool ExcludeFromFacultyGrading { get; set; }

        /// <summary>
        /// Indicates if last date of attendance is required if the grade is final grade
        /// </summary>
        public bool RequireLastAttendanceDate { get; set; }

        /// <summary>
        /// Grade object constructor
        /// </summary>
        /// <param name="id">Grade ID</param>
        /// <param name="letterGrade">Letter Grade</param>
        /// <param name="description">Description for this grade</param>
        /// <param name="scheme"></param>
        public Grade(string id, string letterGrade, string description, string scheme)
            : this(letterGrade, description, scheme)
        {
            _id = id;
            ExcludeFromFacultyGrading = false;
        }

        /// <summary>
        /// Grade object constructor
        /// </summary>
        /// <param name="guid">Grade guid</param>
        /// <param name="id">Grade ID</param>
        /// <param name="letterGrade">Letter Grade</param>
        /// <param name="description">Description for this grade</param>
        /// <param name="scheme"></param>
        public Grade(string guid, string id, string letterGrade, string credit, string description, string scheme)
            : this(letterGrade, description, scheme)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw new ArgumentNullException("guid");
            }
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }
            _guid = guid;
            _credit = credit;
            _id = id;
            ExcludeFromFacultyGrading = false;
        }

        public Grade(string letterGrade, string description, string scheme)
        {
            if (string.IsNullOrEmpty(letterGrade))
            {
                throw new ArgumentNullException("letterGrade");
            }
            if (string.IsNullOrEmpty(description))
            {
                throw new ArgumentNullException("description");
            }
            if (scheme == null)
            {
                throw new ArgumentNullException("scheme");
            }
            _letterGrade = letterGrade;
            _description = description;
            _gradeSchemeCode = scheme;
            IsWithdraw = false;
            ExcludeFromFacultyGrading = false;
            RequireLastAttendanceDate = false;
        }
    }
}