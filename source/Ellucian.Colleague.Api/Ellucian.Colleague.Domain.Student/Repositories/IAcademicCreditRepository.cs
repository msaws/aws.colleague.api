﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Domain.Student.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.Student.Repositories
{
    public interface IAcademicCreditRepository
    {
        Task<IEnumerable<AcademicCredit>> GetAsync(ICollection<string> academicCreditIds, bool bestFit = false, bool filter = true);
        Task<Dictionary<string, List<AcademicCredit>>> GetAcademicCreditByStudentIdsAsync(IEnumerable<string> studentIds, bool bestFit = false, bool filter = true);
        Task<Dictionary<string, List<PilotAcademicCredit>>> GetPilotAcademicCreditsByStudentIdsAsync(IEnumerable<string> studentIds, AcademicCreditDataSubset subset, bool bestFit = false, bool filter = true, string term = null);
        Task<bool> GetPilotCensusBooleanAsync();
        Task<CreditStatus> ConvertCreditStatusAsync(string statusCode);
        Task<IEnumerable<AcademicCredit>> GetAcademicCreditsBySectionIdsAsync(IEnumerable<string> sectionIds);

        /// <summary>
        /// Sorts a list of academic credits according to one or more sort specifications and returns a dictionary of
        /// the sorted academic credits, keyed by their sort specification ID
        /// </summary>
        /// <param name="acadCredits">Collection of <see cref="AcademicCredit"/> objects.</param>
        /// <param name="sortSpecIds">Collection of sort specification IDs</param>
        /// <returns>Dictionary of sorted academic credits, keyed by their sort specification ID</returns>
        Task<Dictionary<string, List<AcademicCredit>>> GetSortedAcademicCreditsBySortSpecificationIdAsync(IEnumerable<AcademicCredit> acadCredits, IEnumerable<string> sortSpecIds);
    }
}
