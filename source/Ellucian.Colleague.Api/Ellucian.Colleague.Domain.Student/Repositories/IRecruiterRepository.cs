﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Base;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.Student.Repositories
{
    public interface IRecruiterRepository
    {
        Task UpdateApplicationAsync(Application application);
        Task ImportApplicationAsync(Application application);
        Task ImportTestScoresAsync(TestScore testScore);
        Task ImportTranscriptCoursesAsync(TranscriptCourse transcriptCourse);
        Task ImportCommunicationHistoryAsync(CommunicationHistory communicationHistory);
        Task RequestCommunicationHistoryAsync(CommunicationHistory communicationHistory);
        Task<ConnectionStatus> PostConnectionStatusAsync(ConnectionStatus connectionStatus);
    }
}
