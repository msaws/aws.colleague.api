﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Domain.Student.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.Student.Repositories
{
    public interface ICampusOrganizationRepository
    {
        Task<IEnumerable<CampusOrganization>> GetCampusOrganizationsAsync(bool bypassCache);
        Task<Tuple<IEnumerable<CampusInvolvement>, int>> GetCampusInvolvementsAsync(int offset, int limit);
        Task<CampusInvolvement> GetGetCampusInvolvementByIdAsync(string id);
    }
}
