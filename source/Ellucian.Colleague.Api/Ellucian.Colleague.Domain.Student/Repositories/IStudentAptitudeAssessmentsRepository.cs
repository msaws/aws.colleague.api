﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Student.Entities;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.Student.Repositories
{
    public interface IStudentAptitudeAssessmentsRepository
    {
        Task<StudentTestScores> GetStudentAptitudeAssessmentsByGuidAsync(string id);
        Task<Tuple<IEnumerable<StudentTestScores>, int>> GetStudentAptitudeAssessmentsAsync(int offset, int limit, bool bypassCache = false);
    }
}
