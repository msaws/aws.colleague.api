﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System.Threading.Tasks;
using Ellucian.Colleague.Domain.Student.Entities;

namespace Ellucian.Colleague.Domain.Student.Repositories
{
    public interface IStudentTaxFormPdfDataRepository
    {
        /// <summary>
        /// Get the 1098-T data for a PDF.
        /// </summary>
        /// <param name="personId">ID of the person assigned to and requesting the 1098-T.</param>
        /// <param name="recordId">ID of the record containing the pdf data for a 1098-T tax form</param>
        /// <returns>1098-T data for a pdf</returns>
        Task<Form1098PdfData> Get1098TPdfAsync(string personId, string recordId);
    }
}
