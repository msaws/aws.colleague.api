﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Base;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.Student.Repositories
{
    public interface ICourseRepository
    {
        Task<IEnumerable<Course>> GetAsync();
        Task<IEnumerable<Course>> GetAsync(ICollection<string> courseIds);
        Task<Course> GetAsync(string courseId);
        Task<IEnumerable<Course>> GetCoursesByIdAsync(IEnumerable<string> courseIds);
        Task<Course> GetCourseByGuidAsync(string guid);
        Task<Course> CreateCourseAsync(Course course, string source = null, string version = null);
        Task<Course> UpdateCourseAsync(Course course, string source = null, string version = null);
        Task<string> GetCourseGuidFromIdAsync(string primaryKey);
        Task<IEnumerable<Course>> GetNonCacheAsync();
        Task<IEnumerable<Course>> GetNonCacheAsync(string subject, string number, string academicLevel, string owningInstitutionUnit, string title, string instructionalMethods, string startOn, string endOn);
        Task<IEnumerable<Course>> GetAsync(string newSubject, string number, string newAcademicLevel, string newOwningInstitutionUnit, string title, string newInstructionalMethods, string newStartOn, string newEndOn);
        Task<Tuple<IEnumerable<Course>, int>> GetPagedCoursesAsync(int offset, int limit, string subject, string number, string academicLevel, string owningInstitutionUnit, string title, string instructionalMethods, string startOn, string endOn);
    }
}
