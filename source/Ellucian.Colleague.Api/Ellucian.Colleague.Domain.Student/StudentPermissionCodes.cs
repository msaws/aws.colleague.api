﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.

using System;

namespace Ellucian.Colleague.Domain.Student
{
    [Serializable]
    public static class StudentPermissionCodes
    {
        //Access to view applications
        public const string ViewApplications = "VIEW.APPLICATIONS";

        //Access to view instructor
        public const string ViewInstructors = "VIEW.INSTRUCTORS";

        // Access to view student academic period profile
        public const string ViewStudentAcademicPeriodProfile = "VIEW.STUDENT.ACADEMIC.PERIOD.PROFILE";

        // Access to view student information
        public const string ViewStudentInformation = "VIEW.STUDENT.INFORMATION";

        // Permission to view financial aid information
        public const string ViewFinancialAidInformation = "VIEW.FINANCIAL.AID.INFORMATION";

        // Permission to create and update courses
        public const string CreateAndUpdateCourse = "CREATE.UPDATE.COURSE";

        // Permission to create and update sections
        public const string CreateAndUpdateSection = "CREATE.UPDATE.SECTION";

        // Permission to create/update room bookings
        public const string CreateAndUpdateRoomBooking = "UPDATE.ROOM.BOOKING";

        // Permission to create/update faculty bookings
        public const string CreateAndUpdateFacultyBooking = "UPDATE.FACULTY.BOOKING";

        // Permissiong to create a prerequiste waiver
        public const string CreatePrerequisiteWaiver = "CREATE.PREREQUISITE.WAIVER";

        // Permission to create a new student petition
        public const string CreateStudentPetition = "CREATE.STUDENT.PETITION";

        //Permission to create a new faculty consent
        public const string CreateFacultyConsent = "CREATE.FACULTY.CONSENT";

        //Permission to create/update student academic program
        public const string CreateStudentAcademicProgramConsent = "CREATE.UPDATE.STUDENT.ACADEMIC.PROGRAM";

        //Permission to create/update an academic program enrollment
        public const string ViewStudentAcademicProgramConsent = "VIEW.STUDENT.ACADEMIC.PROGRAM";

        //Permission to Delete an academic program enrollment
        public const string DeleteStudentAcademicProgramConsent = "DELETE.STUDENT.ACADEMIC.PROGRAM";

        // Permission to update student grades by faculty
        public const string UpdateGrades = "UPDATE.GRADES";

        // Permission to view student charges
        public const string ViewStudentCharges = "VIEW.STUDENT.CHARGES";

        // Permission to view student payments
        public const string ViewStudentPayments = "VIEW.STUDENT.PAYMENTS";

        // Permission to create student charges
        public const string CreateStudentCharges = "CREATE.STUDENT.CHARGES";

        // Permission to create student payments
        public const string CreateStudentPayments = "CREATE.STUDENT.PAYMENTS";

         // Permission to view student academic standings
        public const string ViewStudentAcadStandings = "VIEW.STUDENT.ACAD.STANDINGS";

        // Permission to view student aptitude Assessments
        public const string ViewStudentAptitudeAssessmentsConsent = "VIEW.STUDENT.TEST.SCORES";

        // Permission to view student advisor relationships
        public const string ViewStudentAdivsorRelationships = "VIEW.STU.ADV.RELATIONSHIPS";

        // Permission to view any student-meal-plans information
        public const string ViewMealPlanAssignment = "VIEW.MEAL.PLAN.ASSIGNMENT";

        // Permission to update any student-meal-plans information
        public const string CreateMealPlanAssignment = "CREATE.MEAL.PLAN.ASSIGNMENT";

        // Permission to view any student-meal-plans information
        public const string ViewMealPlanRequest = "VIEW.MEAL.PLAN.REQUEST";

        // Permission to update any student-meal-plans information
        public const string CreateMealPlanRequest = "CREATE.MEAL.PLAN.REQUEST";

        //Permission to view housing requests
        public const string ViewHousingRequest = "VIEW.HOUSING.REQS";

        // Permission to create/update housing requests
        public const string CreateHousingRequest = "UPDATE.HOUSING.REQS";

        // Permission to update any student-registration-eligibilities information
        public const string ViewStuRegistrationEligibility = "VIEW.STU.REGISTRATION.ELIGIBILITY";

        //Permission to view any housing-assignments information.
        public const string ViewHousingAssignment = "VIEW.ROOM.ASSIGNMENT";

        //Permission to create/update any housing-assignments information.
        public const string CreateUpdateHousingAssignment = "UPDATE.ROOM.ASSIGNMENT";

        //Permission to view any section-instructors information.
        public const string ViewSectionInstructors = "VIEW.SECTION.INSTRUCTORS";

        //Permission to create/update any section-instructors information.
        public const string CreateSectionInstructors = "UPDATE.SECTION.INSTRUCTORS";

        //Permission to delete any section-instructors information.
        public const string DeleteSectionInstructors = "DELETE.SECTION.INSTRUCTORS";

        //Permission to view any student-section-waitlists information
        public const string ViewStudentSectionWaitlist = "VIEW.STUDENT.SECTION.WAITLIST";
    }
}
