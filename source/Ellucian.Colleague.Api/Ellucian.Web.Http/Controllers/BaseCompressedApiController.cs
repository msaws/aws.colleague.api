﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Web.Http.Exceptions;
using Ellucian.Web.Http.Filters;
using Ellucian.Web.Mvc.Filter;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Ellucian.Web.Http.Controllers
{
    [SessionState(SessionStateBehavior.Disabled)]
    [BrowserCompressionFilter]
    [CustomCredentialsResponseFilter]
    public abstract class BaseCompressedApiController : ApiController
    {
        private const string RestrictedHeaderName = "X-Content-Restricted";

        public BaseCompressedApiController()
        {
            var controllerType = this.GetType();
            System.ComponentModel.LicenseManager.Validate(controllerType);
        }

        /// <summary>
        /// Creates an HttpResponseException with an optional message.
        /// </summary>
        /// <param name="message">Optional message to be returned with the exception e.g. update failed.</param>
        /// <param name="statusCode">HttpStatusCode to be returned. Default is 400 (bad request)</param>
        /// <returns>HttpResponseException</returns>
        protected HttpResponseException CreateHttpResponseException(string message = null, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            if (!string.IsNullOrEmpty(message))
            {
                message = message.Replace(Environment.NewLine, " ").Replace("\n", " ");
            }
            return CreateHttpResponseException(new WebApiException() { Message = message ?? string.Empty }, statusCode);
        }

        /// <summary>
        /// Creates an HttpResponseException using the supplied WebApiException.
        /// </summary>
        /// <param name="apiException">WebApiException object detailing what went wrong.</param>
        /// <param name="statusCode">HttpStatusCode to be returned. Default is 400 (bad request)</param>
        /// <returns>HttpResponseException</returns>
        protected HttpResponseException CreateHttpResponseException(WebApiException apiException, HttpStatusCode statusCode = HttpStatusCode.BadRequest)
        {
            var httpResponseMessage = new HttpResponseMessage(statusCode);

            if (apiException == null)
            {
                apiException = new WebApiException() { Message = "..." };
            }

            if (!string.IsNullOrEmpty(apiException.Message))
            {
                apiException.Message = apiException.Message.Replace(Environment.NewLine, " ").Replace("\n", " ");
            }

            var serialized = JsonConvert.SerializeObject(apiException);
            httpResponseMessage.Content = new StringContent(serialized);

            return new HttpResponseException(httpResponseMessage);
        }

        /// <summary>
        /// Creates a 404 error with a message.
        /// </summary>
        /// <param name="type">A word describing the object in question, e.g. "course", "book", etc.</param>
        /// <param name="id">The id of the object that was not found.</param>
        /// <returns>HttpResponseException</returns>
        protected HttpResponseException CreateNotFoundException(string type, string id)
        {
            return CreateHttpResponseException(string.Format("The {0} with the ID {1} was not found", type, id), HttpStatusCode.NotFound);
        }

        /// <summary>
        /// Creates an HttpResponseException using the supplied IntegrationApiException
        /// </summary>
        /// <param name="apiException">IntegrationApiException object detailing what went wrong.</param>
        /// <param name="statusCode">HttpStatusCode to be returned. Default is 400 (bad request)</param>
        /// <returns>HttpResponseException</returns>
        protected HttpResponseException CreateHttpResponseException(IntegrationApiException apiException, HttpStatusCode? statusCode = null)
        {
            if (!statusCode.HasValue)
            {
                statusCode = apiException.HttpStatusCode;
            }
            if (HttpStatusCode.IsDefined(typeof(HttpStatusCode), statusCode))
            {
                statusCode = (HttpStatusCode)HttpStatusCode.ToObject(typeof(HttpStatusCode), statusCode);
            }
            else
            {
                statusCode = HttpStatusCode.BadRequest;
            }
            var httpResponseMessage = new HttpResponseMessage(statusCode.Value);

            var serialized = JsonConvert.SerializeObject(apiException);
            httpResponseMessage.Content = new StringContent(serialized);

            return new HttpResponseException(httpResponseMessage);
        }

       
        /// <summary>
        /// Sets the Location header on the HTTP response.
        /// </summary>
        /// <param name="routeName">Route name as defined in the route table.</param>
        /// <param name="routeValues">Route values needed by the route template.</param>
        protected void SetResourceLocationHeader(string routeName, object routeValues = null)
        {
            string location = Url.Link(routeName, routeValues);
            HttpContext.Current.Response.AppendHeader(HttpResponseHeader.Location.ToString(), location);
        }

        /// <summary>
        /// Sets the Content Restricted header on the HTTP response
        /// </summary>
        /// <param name="restriction">The restriction value</param>
        protected void SetContentRestrictedHeader(string restriction)
        {
            HttpContext.Current.Response.AppendHeader(RestrictedHeaderName, restriction);
        }

        /// <summary>
        /// Gets value from a JObject in the criteria
        /// </summary>
        /// <param name="keyToSearch"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string GetValueFromJsonObject(string keyToSearch, JObject obj)
        {
            string outValue = string.Empty;
            var token = obj.GetValue(keyToSearch, StringComparison.OrdinalIgnoreCase);
            if (token != null)
            {
                outValue = token.ToString();
            }

            return outValue;
        }

        /// <summary>
        /// Gets the resource name from the route
        /// </summary>
        /// <returns>resource name from the route</returns>
        public virtual string GetRouteResourceName()
        {
            var actionRequestContext = ActionContext.Request;

            if (actionRequestContext == null)
            {
                return string.Empty;
            }

            var routeData = actionRequestContext.GetRouteData();

            if (routeData == null)
            {
                return string.Empty;
            }

            string routeTemplate = string.Empty;
            if (routeData.Route != null)
            {
                routeTemplate = routeData.Route.RouteTemplate;
            }
                

            string[] routeStrings = routeTemplate.Split(new char[]{'/'}, StringSplitOptions.RemoveEmptyEntries );
            if (routeStrings.Any())
            {
                var count = routeStrings.Count();
                if (count == 1)
                {
                    return routeStrings[0];
                }

                if (routeStrings[0].Equals("qapi", StringComparison.OrdinalIgnoreCase))
                {
                    return routeStrings[1];
                }
                else
                {
                    return routeStrings[0];
                }
            }

            throw new Exception("Unable to get route resource name");
        }

        /// <summary>
        /// Adds the data privacy settings list to the actioncontext request properties
        /// </summary>
        /// <param name="dataPrivacySettingsList"></param>
        public void AddDataPrivacyContextProperty(List<string> dataPrivacySettingsList)
        {
            ActionContext.Request.Properties.Add("DataPrivacy", dataPrivacySettingsList);
        }
    }
}
