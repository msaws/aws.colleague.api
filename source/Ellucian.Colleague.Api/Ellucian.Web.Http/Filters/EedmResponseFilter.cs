﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using Ellucian.Web.Http.Routes;
using Microsoft.Practices.EnterpriseLibrary.Common.Utility;
using Newtonsoft.Json.Linq;
using System.Net.Http.Formatting;
using System.Net.Http;
using System.Threading;
using System.Web.Http.Controllers;
using Ellucian.Web.Http.Utilities;
using slf4net;

namespace Ellucian.Web.Http.Filters
{
    /// <summary>
    /// Action Filter to perform Eedm processing Data Privacy and Extensibility
    /// </summary>
    [AttributeUsage(AttributeTargets.Method, AllowMultiple = false)]
    public class EedmResponseFilter : ActionFilterAttribute
    {

        public override Task OnActionExecutedAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            var logger = actionExecutedContext.Request.GetDependencyScope().GetService(typeof(ILogger)) as ILogger;

            //Check if Response, Content are NOT NULL and that the content is ObjectContent
            if (actionExecutedContext.Response != null && actionExecutedContext.Response.Content != null && actionExecutedContext.Response.Content is ObjectContent)
            {
                //only process successful responses
                if (actionExecutedContext.Response.IsSuccessStatusCode)
                {
                    //get objectContent and find out if it is an collection
                    var objectContent = actionExecutedContext.Response.Content as ObjectContent;
                    var isEnumerable = objectContent.ObjectType.Name == "IEnumerable`1" || objectContent.ObjectType.GetInterface("IEnumerable`1") != null;

                    //read json data out
                    var responseMessage = actionExecutedContext.Response.Content.ReadAsStringAsync().Result;

                    object dataPrivacy;
                    actionExecutedContext.Request.Properties.TryGetValue("DataPrivacy", out dataPrivacy);

                    var dataPrivacyList = (IList<string>) dataPrivacy;
                    //if there is nothing to process for DataPrivacy, just return to base.
                    if (dataPrivacyList == null || !dataPrivacyList.Any())
                    {
                        return base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
                    }

                    JContainer jsonToModify;
                    if (isEnumerable)
                    {
                        jsonToModify = JArray.Parse(responseMessage);
                    }
                    else
                    {
                        jsonToModify = JObject.Parse(responseMessage);
                    }

                    var returnedJson = DataPrivacy.ApplyDataPrivacy(jsonToModify, dataPrivacyList, logger);
                    if (returnedJson != null)
                    {
                        actionExecutedContext.Response.Content = new ObjectContent(returnedJson.GetType(), returnedJson, new JsonMediaTypeFormatter(), @"application/json");
                        actionExecutedContext.Response.Headers.Add("X-Content-Restricted", "partial");
                    }
                }
            }
            return base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
        }
    }
}
