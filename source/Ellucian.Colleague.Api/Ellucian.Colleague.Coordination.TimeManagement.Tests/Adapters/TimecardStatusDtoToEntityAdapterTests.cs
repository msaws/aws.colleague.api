﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Coordination.TimeManagement.Adapters;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Tests.Adapters
{
    [TestClass]
    public class TimecardStatusDtoToEntityAdapterTests
    {
        public Dtos.TimeManagement.TimecardStatus inputSource;
        public Domain.TimeManagement.Entities.TimecardStatus actualStatus
        {
            get
            {
                return adapterUnderTest.MapToType(inputSource);
            }
        }

        public TimecardStatusDtoToEntityAdapter adapterUnderTest;

        

        public Mock<IAdapterRegistry> adapterRegistryMock;
        public Mock<ILogger> loggerMock;

        [TestInitialize]
        public void Initialize()
        {
            inputSource = new Dtos.TimeManagement.TimecardStatus()
            {
                ActionerId = "0003914",
                ActionType = Dtos.TimeManagement.StatusAction.Approved,
                HistoryId = "5",
                TimecardId = "6"
            };

            adapterRegistryMock = new Mock<IAdapterRegistry>();
            loggerMock = new Mock<ILogger>();

            adapterRegistryMock.Setup(a => a.GetAdapter<Dtos.TimeManagement.StatusAction, Domain.TimeManagement.Entities.StatusAction>())
                .Returns(() => new AutoMapperAdapter<Dtos.TimeManagement.StatusAction, Domain.TimeManagement.Entities.StatusAction>(adapterRegistryMock.Object, loggerMock.Object));

            adapterRegistryMock.Setup(a => a.GetAdapter<Dtos.Base.Timestamp, Domain.Base.Entities.Timestamp>())
                .Returns(() => new AutoMapperAdapter<Dtos.Base.Timestamp, Domain.Base.Entities.Timestamp>(adapterRegistryMock.Object, loggerMock.Object));

            adapterUnderTest = new TimecardStatusDtoToEntityAdapter(adapterRegistryMock.Object, loggerMock.Object);
        }

        [TestMethod]
        public void TimecardIdTest()
        {
            Assert.AreEqual(inputSource.TimecardId, actualStatus.TimecardId);
        }

        [TestMethod]
        public void ActionerIdTest()
        {
            Assert.AreEqual(inputSource.ActionerId, actualStatus.ActionerId);
        }

        [TestMethod]
        public void ActionTypeTest()
        {
            Assert.AreEqual(inputSource.ActionType.ToString(), actualStatus.ActionType.ToString());
        }

        [TestMethod]
        public void NewStatusTest()
        {
            Assert.IsTrue(string.IsNullOrEmpty(actualStatus.Id));
            Assert.IsNull(actualStatus.Timestamp);
        }

        [TestMethod]
        public void ExistingStatusTest()
        {
            inputSource.Id = "foobar";
            inputSource.Timestamp = new Dtos.Base.Timestamp()
            {
                AddDateTime = DateTime.Today,
                AddOperator = "MCD",
                ChangeDateTime = DateTime.Today,
                ChangeOperator = "MCD"
            };

            Assert.AreEqual(inputSource.Id, actualStatus.Id);
            Assert.IsNotNull(actualStatus.Timestamp);
        }

        [TestMethod]
        public void ExistingStatusWithMissingTimestampTest()
        {
            inputSource.Id = "Foobar";
            Assert.IsNull(actualStatus.Timestamp);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void DomainExceptionsAreUncaughtTest()
        {
            inputSource.TimecardId = null;
            var invalid = actualStatus;
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void InputSourceIsRequiredTest()
        {
            inputSource = null;
            var invalid = actualStatus;
        }
    }
}
