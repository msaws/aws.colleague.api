﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Web.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Tests.Services
{
    public abstract class CurrentUserSetup
    {
        public class PersonUserFactory : ICurrentUserFactory
        {
            public ICurrentUser CurrentUser
            {
                get
                {
                    return new CurrentUser(new Claims()
                    {
                        ControlId = "001",
                        Name = "Jean",
                        PersonId = "0003914",
                        SecurityToken = "999",
                        SessionTimeout = 20,
                        UserName = "Employee",
                        Roles = new List<string>() {  },
                        SessionFixationId = "qwerty44",
                    });
                }
            }
        }
        public class EmployeeUserFactory : ICurrentUserFactory
        {
            public ICurrentUser CurrentUser
            {
                get
                {
                    return new CurrentUser(new Claims()
                    {
                        ControlId = "001",
                        Name = "Ted Cruz",
                        PersonId = "0012882",
                        SecurityToken = "999",
                        SessionTimeout = 20,
                        UserName = "tedcruz",
                        Roles = new List<string>() { "Employee" },
                        SessionFixationId = "qwerty44",
                    });
                }
            }
        }
        public class SupervisorUserFactory : ICurrentUserFactory
        {
            public ICurrentUser CurrentUser
            {
                get
                 { 
                     return new CurrentUser(new Claims()
                         {
                             ControlId = "001",
                             Name = "Ted Cruz",
                             PersonId = "0012882",
                             SecurityToken = "999",
                             SessionTimeout = 20,
                             UserName = "tedcruz",
                             Roles = new List<string>() { "Supervisor" },
                             SessionFixationId = "qwerty44",
                         });
                 }
            }
        }

        public class TimecardSupervisorUserFactory : ICurrentUserFactory
        {
            public ICurrentUser CurrentUser
            {
                get
                {
                    return new CurrentUser(new Claims()
                        {
                            ControlId = "001",
                            Name = "Ted Cruz",
                            PersonId = "0012882",
                            SecurityToken = "999",
                            SessionTimeout = 20,
                            UserName = "tedcruz",
                            Roles = new List<string>() { "TIME MANAGEMENT SUPERVISOR" },
                            SessionFixationId = "qwerty44",
                        });
                }
            }
        }
    }
}
