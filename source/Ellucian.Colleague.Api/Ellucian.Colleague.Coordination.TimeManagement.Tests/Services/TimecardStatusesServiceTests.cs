﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.TestUtil;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Colleague.Domain.TimeManagement.Tests;
using Ellucian.Colleague.Coordination.TimeManagement.Services;
using Ellucian.Colleague.Domain.Repositories;
using slf4net;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Colleague.Coordination.TimeManagement.Adapters;
using Ellucian.Colleague.Coordination.Base.Adapters;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.TimeManagement.Services;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Domain.Entities;
using Ellucian.Colleague.Domain.TimeManagement;

namespace Ellucian.Colleague.Coordination.TimeManagement.Tests.Services
{
    [TestClass]
    public class TimecardStatusesServiceTests : CoordinationServiceTestsSetup
    {
        public Mock<ITimecardStatusesRepository> statusesRepositoryMock;
        public Mock<ITimecardsRepository> timecardsRepositoryMock;
        public Mock<ITimecardHistoriesRepository> historiesRepositoryMock;
        public Mock<ITimeEntryCommentsRepository> timeEntryCommentsRepositoryMock;
        public Mock<ISupervisorsRepository> supervisorRepositoryMock;
        public TestTimecardStatusesRepository fakeStatusesRepository;
        public TestTimecardsRepository fakeTimecardsRepository;
        public TestTimeEntryCommentsRepository fakeTimeEntryCommentsRepository;
        public TestTimecardHistoriesRepository fakeHistoriesRepository;
        public ITypeAdapter<Domain.TimeManagement.Entities.TimecardStatus, Dtos.TimeManagement.TimecardStatus> statusEntityToDtoAdapter;
        public ITypeAdapter<Dtos.TimeManagement.TimecardStatus, Domain.TimeManagement.Entities.TimecardStatus> statusDtoToEntityAdapter;
        public ITypeAdapter<Domain.TimeManagement.Entities.StatusAction, Dtos.TimeManagement.StatusAction> actionEntityToDtoAdapter;
        public ITypeAdapter<Dtos.TimeManagement.StatusAction, Domain.TimeManagement.Entities.StatusAction> actionDtoToEntityAdapter;


        public FunctionEqualityComparer<TimecardStatus> statusDtoComparer;

        public TimecardStatusesService actualStatusesService
        {
            get
            {
                return new TimecardStatusesService(
                    statusesRepositoryMock.Object,
                    timecardsRepositoryMock.Object,
                    historiesRepositoryMock.Object,
                    timeEntryCommentsRepositoryMock.Object,
                    supervisorRepositoryMock.Object,
                    adapterRegistryMock.Object,
                    currentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object);
            }
        }


        public void StatusesServiceTestsInitialize()
        {
            MockInitialize();

            // adapters
            statusEntityToDtoAdapter = new TimecardStatusEntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);
            statusDtoToEntityAdapter = new TimecardStatusDtoToEntityAdapter(adapterRegistryMock.Object, loggerMock.Object);

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Domain.TimeManagement.Entities.TimecardStatus, Dtos.TimeManagement.TimecardStatus>())
                .Returns(() => statusEntityToDtoAdapter);

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Dtos.TimeManagement.TimecardStatus, Domain.TimeManagement.Entities.TimecardStatus>())
                .Returns(() => statusDtoToEntityAdapter);

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Dtos.TimeManagement.StatusAction, Domain.TimeManagement.Entities.StatusAction>())
                .Returns(() => new AutoMapperAdapter<Dtos.TimeManagement.StatusAction, Domain.TimeManagement.Entities.StatusAction>(adapterRegistryMock.Object, loggerMock.Object));

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Dtos.Base.Timestamp, Domain.Base.Entities.Timestamp>())
                .Returns(() => new TimestampDtoToEntityAdapter(adapterRegistryMock.Object, loggerMock.Object));

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Domain.Base.Entities.Timestamp, Dtos.Base.Timestamp>())
                .Returns(() => new AutoMapperAdapter<Domain.Base.Entities.Timestamp, Dtos.Base.Timestamp>(adapterRegistryMock.Object, loggerMock.Object));

            // repositories
            fakeStatusesRepository = new TestTimecardStatusesRepository();
            fakeTimecardsRepository = new TestTimecardsRepository();
            fakeHistoriesRepository = new TestTimecardHistoriesRepository();
            fakeTimeEntryCommentsRepository = new TestTimeEntryCommentsRepository();

            statusesRepositoryMock = new Mock<ITimecardStatusesRepository>();
            timecardsRepositoryMock = new Mock<ITimecardsRepository>();
            timeEntryCommentsRepositoryMock = new Mock<ITimeEntryCommentsRepository>();
            historiesRepositoryMock = new Mock<ITimecardHistoriesRepository>();
            supervisorRepositoryMock = new Mock<ISupervisorsRepository>();
            statusesRepositoryMock.Setup(st => st.CreateTimecardStatusesAsync(It.IsAny<List<Domain.TimeManagement.Entities.TimecardStatus>>()))
                .Returns<List<Domain.TimeManagement.Entities.TimecardStatus>>((s) =>
                    fakeStatusesRepository.CreateTimecardStatusesAsync(s));

            statusesRepositoryMock.Setup(st => st.GetTimecardStatusesByPersonIdsAsync(It.IsAny<IEnumerable<string>>()))
                .Returns<IEnumerable<string>>(inputIds => fakeStatusesRepository.GetTimecardStatusesByPersonIdsAsync(inputIds));

            timecardsRepositoryMock.Setup(tc => tc.GetTimecard2Async(It.IsAny<string>()))
                .Callback<string>((id) => 
                    fakeTimecardsRepository.AddTimecardRecordHelper(id, currentUserFactory.CurrentUser.PersonId))
                .Returns<string>((id) => fakeTimecardsRepository.GetTimecard2Async(id));
                    
            historiesRepositoryMock.Setup(h => h.CreateTimecardHistory2Async(It.IsAny<Domain.TimeManagement.Entities.Timecard2>(), It.IsAny<Domain.TimeManagement.Entities.TimecardStatus>(), It.IsAny<IEnumerable<Domain.TimeManagement.Entities.TimeEntryComment>>()))
                .Returns<Domain.TimeManagement.Entities.Timecard2, Domain.TimeManagement.Entities.TimecardStatus, IEnumerable<Domain.TimeManagement.Entities.TimeEntryComment>>((timecard, status, comments) =>
                    fakeHistoriesRepository.CreateTimecardHistory2Async(timecard, status, comments));

            timeEntryCommentsRepositoryMock.Setup(c => c.GetCommentsAsync(It.IsAny<IEnumerable<string>>()))
                .Returns<IEnumerable<string>>((ids) => fakeTimeEntryCommentsRepository.GetCommentsAsync(ids));

            supervisorRepositoryMock.Setup(s => s.GetSuperviseesBySupervisorAsync(It.IsAny<string>()))
                .Returns<string>(s => Task.FromResult(new List<string>().AsEnumerable()));

            // comparers
            statusDtoComparer = this.TimecardStatusComparer();
        }

        [TestClass]
        public class CreateTimecardStatusTests : TimecardStatusesServiceTests
        {
            TimecardStatus newStatus;
            [TestInitialize]
            public void Initialize()
            {
                StatusesServiceTestsInitialize();
                newStatus = new TimecardStatus()
                {                    
                    ActionType = StatusAction.Submitted,
                    ActionerId = "24601",
                    Id = string.Empty,
                    TimecardId = "001",
                };
            }
            [TestMethod]
            public async Task NewStatusIsReturned()
            {
                var returnedStatus = await actualStatusesService.CreateTimecardStatusesAsync(new List<TimecardStatus>() { newStatus });
                Assert.IsTrue(statusDtoComparer.Equals(newStatus, returnedStatus.ElementAt(0)));
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NullStatusArgumentError()
            {
                    await actualStatusesService.CreateTimecardStatusesAsync(new List<TimecardStatus>());

                }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task NullStatusTimecardIdError()
            {
 
                    newStatus.TimecardId = string.Empty;
                    await actualStatusesService.CreateTimecardStatusesAsync(new List<TimecardStatus>() { newStatus });

                }

            [TestMethod]
            public async Task TimecardAndHistoryRepositoryIsCalled()
            {
                await actualStatusesService.CreateTimecardStatusesAsync(new List<TimecardStatus>() { newStatus });

                timecardsRepositoryMock.Verify(r => r.GetTimecard2Async(It.Is<string>(s => s.Equals(newStatus.TimecardId))), Times.Once);
                historiesRepositoryMock.Verify(r => r.CreateTimecardHistory2Async(
                    It.IsAny<Domain.TimeManagement.Entities.Timecard2>(),
                    It.IsAny<Domain.TimeManagement.Entities.TimecardStatus>(),
                    It.IsAny<IEnumerable<Domain.TimeManagement.Entities.TimeEntryComment>>()), Times.Once);
            }

            [TestMethod]
            public async Task TimeEntryCommentsRepositoryIsCalled()
            {
                await actualStatusesService.CreateTimecardStatusesAsync(new List<TimecardStatus>() { newStatus });

                timeEntryCommentsRepositoryMock.Verify(r => r.GetCommentsAsync(It.Is<IEnumerable<string>>(l => l.First() == currentUserFactory.CurrentUser.PersonId)), Times.Once);
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task ErrorCreatingHistoryRecordTest()
            {
                historiesRepositoryMock.Setup(r => r.CreateTimecardHistory2Async(
                    It.IsAny<Domain.TimeManagement.Entities.Timecard2>(),
                    It.IsAny<Domain.TimeManagement.Entities.TimecardStatus>(),
                    It.IsAny<IEnumerable<Domain.TimeManagement.Entities.TimeEntryComment>>()))
                    .Throws(new ApplicationException());

                await actualStatusesService.CreateTimecardStatusesAsync(new List<TimecardStatus>() { newStatus });
            }


        }

        [TestClass]
        public class GetTimecardStatusesByTimecardIdTests : TimecardStatusesServiceTests
        {
            public string inputTimecardId;

            [TestInitialize]
            public void Initialize()
            {
                StatusesServiceTestsInitialize();
                inputTimecardId = fakeStatusesRepository.timecardStatusRecords.First().timecardId;
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NullStatusIdArgumentError()
            {
                try
                {
                    await actualStatusesService.GetTimecardStatusesByTimecardIdAsync("");
                }
                catch (ArgumentNullException ane)
                {
                    loggerMock.Verify(v => v.Error(It.IsAny<string>()));
                    throw ane;
                }
            }

            [TestMethod]
            public async Task CorrectStatusesAreReturnedTest()
            {
                var expectedStatusRec = await fakeStatusesRepository.GetTimecardStatusesByTimecardIdAsync(inputTimecardId);
                var expected = expectedStatusRec.Select(tcs => statusEntityToDtoAdapter.MapToType(tcs)).ToList();
                var actual = await actualStatusesService.GetTimecardStatusesByTimecardIdAsync(inputTimecardId);
            }
        }

        [TestClass]
        public class GetLatestTimecardStatuseTests : TimecardStatusesServiceTests
        {
            
            [TestInitialize]
            public void Initialize()
            {
                StatusesServiceTestsInitialize();
                
            }

            //[TestMethod]
            //public async Task AllStatusesReturnedTest()
            //{
            //    var expected = (await fakeStatusesRepository.GetTimecardStatusesByPersonIdsAsync(new string[1] { "0003914" }))
            //        .Select(domain => statusEntityToDtoAdapter.MapToType(domain));

            //    var actual = await actualStatusesService.GetTimecardStatusesAsync();

            //    CollectionAssert.AreEqual(expected.ToArray(), actual.ToArray(), statusDtoComparer);
            //}
        
            //[TestMethod]
            //public async Task SupervisorHasPermissionTest()
            //{
            //    var expected = (await fakeStatusesRepository.GetTimecardStatusesByPersonIdsAsync(new string[1] { "0003914" }))
            //        .Select(domain => statusEntityToDtoAdapter.MapToType(domain));

            //    currentUserFactory = new CurrentUserSetup.SupervisorUserFactory();

            //    var role = new Role(5, "Supervisor");
            //    role.AddPermission(new Permission(TimeManagementPermissionCodes.ApproveRejectEmployeeTimecard));
            //    roleRepositoryMock.Setup(r => r.Roles).Returns(() => new List<Role>() { role });

            //    var actual = await actualStatusesService.GetTimecardStatusesAsync();

            //    CollectionAssert.AreEqual(expected.ToArray(), actual.ToArray(), statusDtoComparer);

            //}

            [TestMethod]
            public async Task RepositoryReturnsNullTest()
            {
                statusesRepositoryMock.Setup(st => st.GetTimecardStatusesByPersonIdsAsync(It.IsAny<IEnumerable<string>>()))
                    .Returns<IEnumerable<string>>(inputIds => Task.FromResult<IEnumerable<Domain.TimeManagement.Entities.TimecardStatus>>(null));

                var actual = await actualStatusesService.GetTimecardStatusesAsync();

                Assert.IsFalse(actual.Any());
            }
        }


        private FunctionEqualityComparer<TimecardStatus> TimecardStatusComparer()
        {
            return new FunctionEqualityComparer<TimecardStatus>(
                (s1, s2) =>
                    s1.ActionerId == s2.ActionerId &&
                    s1.ActionType == s2.ActionType &&
                    s1.TimecardId == s2.TimecardId,                    
                    (s) => s.Id.GetHashCode()
                );
        }
    }
}
    

    
