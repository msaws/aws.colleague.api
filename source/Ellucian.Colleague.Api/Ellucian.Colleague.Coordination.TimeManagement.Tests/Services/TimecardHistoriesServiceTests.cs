﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.TestUtil;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Colleague.Domain.TimeManagement.Tests;
using Ellucian.Colleague.Coordination.TimeManagement.Services;
using Ellucian.Colleague.Domain.Repositories;
using slf4net;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Colleague.Coordination.TimeManagement.Adapters;
using Ellucian.Colleague.Coordination.Base.Adapters;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.TimeManagement.Services;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.TimeManagement;

namespace Ellucian.Colleague.Coordination.TimeManagement.Tests.Services
{
    [TestClass]
    public class TimecardHistoriesServiceTests : CoordinationServiceTestsSetup
    {
        #region SETUP
        public DateTime startDate;
        public DateTime endDate;
        public List<string> employeeIds;
        public Mock<ITimecardHistoriesRepository> TimecardHistoriesRepositoryMock;
        public Mock<ISupervisorsRepository> supervisorsRepositoryMock;
        public TestTimecardHistoriesRepository testRepository;

        protected Domain.Entities.Role timeApprovalRole;
        private Domain.Entities.Permission approvalPermission;

        // entity -> dto
        public ITypeAdapter<Domain.TimeManagement.Entities.TimecardHistory, Dtos.TimeManagement.TimecardHistory> timecardHistoryEntityToDtoAdapter;
        public ITypeAdapter<Domain.TimeManagement.Entities.TimecardHistory2, Dtos.TimeManagement.TimecardHistory2> timecardHistory2EntityToDtoAdapter;

        // dto -> entity
        public ITypeAdapter<Dtos.TimeManagement.TimecardHistory, Domain.TimeManagement.Entities.TimecardHistory> timecardHistoryDtoToEntityAdapter;
        public ITypeAdapter<Dtos.TimeManagement.TimecardHistory2, Domain.TimeManagement.Entities.TimecardHistory2> timecardHistory2DtoToEntityAdapter;

        public TimecardHistoriesService actualTimecardHistoriesService
        {
            get
            {
                return new TimecardHistoriesService(
                    TimecardHistoriesRepositoryMock.Object,
                    supervisorsRepositoryMock.Object,
                    adapterRegistryMock.Object,
                    currentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object);
            }
        }

        public FunctionEqualityComparer<TimecardHistory> timecardHistoryDtoComparer;
        public FunctionEqualityComparer<TimecardHistory2> timecardHistory2DtoComparer;

        public void TimecardHistorieserviceTestsInitialize()
        {

            MockInitialize();
            testRepository = new TestTimecardHistoriesRepository();

            // adapters
            timecardHistoryEntityToDtoAdapter = new TimecardHistoryEntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);
            timecardHistory2EntityToDtoAdapter = new TimecardHistory2EntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);

            // mocks
            TimecardHistoriesRepositoryMock = new Mock<ITimecardHistoriesRepository>();
            supervisorsRepositoryMock = new Mock<ISupervisorsRepository>();
            // v1
            TimecardHistoriesRepositoryMock.Setup(tc => tc.GetTimecardHistoriesAsync(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<List<string>>()))
                .Returns<DateTime,DateTime,List<string>>((start, end, ids) =>
                    testRepository.GetTimecardHistoriesAsync(start, end, ids));

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Domain.TimeManagement.Entities.TimecardHistory, Dtos.TimeManagement.TimecardHistory>())
                .Returns(() =>
                    new TimecardHistoryEntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object));

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Domain.TimeManagement.Entities.TimeEntryHistory, Dtos.TimeManagement.TimeEntryHistory>())
                .Returns(() => new AutoMapperAdapter<Domain.TimeManagement.Entities.TimeEntryHistory, Dtos.TimeManagement.TimeEntryHistory>(adapterRegistryMock.Object, loggerMock.Object));

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Domain.Base.Entities.Timestamp, Dtos.Base.Timestamp>())
                .Returns(() => new AutoMapperAdapter<Domain.Base.Entities.Timestamp, Dtos.Base.Timestamp>(adapterRegistryMock.Object, loggerMock.Object));
            // v2
            TimecardHistoriesRepositoryMock.Setup(tc => tc.GetTimecardHistories2Async(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<List<string>>()))
                .Returns<DateTime, DateTime, List<string>>((start, end, ids) =>
                    testRepository.GetTimecardHistories2Async(start, end, ids));

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Domain.TimeManagement.Entities.TimecardHistory2, Dtos.TimeManagement.TimecardHistory2>())
                .Returns(() =>
                    new TimecardHistory2EntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object));

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Domain.TimeManagement.Entities.TimeEntryHistory2, Dtos.TimeManagement.TimeEntryHistory2>())
                .Returns(() => new AutoMapperAdapter<Domain.TimeManagement.Entities.TimeEntryHistory2, Dtos.TimeManagement.TimeEntryHistory2>(adapterRegistryMock.Object, loggerMock.Object));

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Domain.Base.Entities.Timestamp, Dtos.Base.Timestamp>())
                .Returns(() => new AutoMapperAdapter<Domain.Base.Entities.Timestamp, Dtos.Base.Timestamp>(adapterRegistryMock.Object, loggerMock.Object));

            // permissions mock
            timeApprovalRole = new Domain.Entities.Role(76, "TIME MANAGEMENT SUPERVISOR");
            approvalPermission = new Ellucian.Colleague.Domain.Entities.Permission(TimeManagementPermissionCodes.ApproveRejectEmployeeTimecard);
            timeApprovalRole.AddPermission(approvalPermission);
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { timeApprovalRole });

            timecardHistoryDtoComparer = this.TimecardHistoryComparer();
            timecardHistory2DtoComparer = this.TimecardHistory2Comparer();
        }

        #endregion

        #region TESTS v1
        [TestClass]
        public class GetTimecardHistoriesTests : TimecardHistoriesServiceTests
        {
            [TestInitialize]
            public void Initialize()
            {
                TimecardHistorieserviceTestsInitialize();
            }

            [TestMethod]
            public async Task RepositoryCalledWithCurrentUserTest()
            {
                await actualTimecardHistoriesService.GetTimecardHistoriesAsync(startDate, endDate);
                TimecardHistoriesRepositoryMock.Verify(rm => rm.GetTimecardHistoriesAsync(It.IsAny<DateTime>(), It.IsAny<DateTime>(),It.Is<List<string>>(ids =>
                    ids.Count() == 1 && ids.ElementAt(0) == currentUserFactory.CurrentUser.PersonId)));
            }

            [TestMethod]
            public async Task ExpectedEqualsActualTest()
            {
                var expected = (await testRepository.GetTimecardHistoriesAsync(startDate, endDate, new List<string>() { currentUserFactory.CurrentUser.PersonId }))
                    .Select(tc => timecardHistoryEntityToDtoAdapter.MapToType(tc));

                var actual = await actualTimecardHistoriesService.GetTimecardHistoriesAsync(startDate, endDate);

                CollectionAssert.AreEqual(expected.ToArray(), actual.ToArray(), timecardHistoryDtoComparer);

            }

            //[TestMethod]
            //public async Task NullSubordinatesMessageAndErrorTest()
            //{
            //    supervisorsRepositoryMock.Setup(req => req.GetSuperviseesBySupervisorAsync(It.IsAny<string>())).ReturnsAsync(null);
            //    await actualTimecardHistoriesService.GetTimecardHistoriesAsync(startDate, endDate);
            //    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
            //}

            [TestMethod]
            public async Task RepositoryReturnsNullLoggedTest()
            {
                TimecardHistoriesRepositoryMock.Setup(r => r.GetTimecardHistoriesAsync(It.IsAny<DateTime>(), It.IsAny<DateTime>(),It.IsAny<List<string>>()))
                    .Returns<DateTime,DateTime,List<string>>((start,end,ids) => Task.FromResult<IEnumerable<Domain.TimeManagement.Entities.TimecardHistory>>(null));

                await actualTimecardHistoriesService.GetTimecardHistoriesAsync(startDate, endDate);
                loggerMock.Verify(l => l.Error(It.IsAny<string>()));                
            }

        }

        #endregion

        #region TESTS v2
        [TestClass]
        public class GetTimecardHistories2Tests : TimecardHistoriesServiceTests
        {
            [TestInitialize]
            public void Initialize()
            {
                TimecardHistorieserviceTestsInitialize();
            }

            [TestMethod]
            public async Task RepositoryCalledWithCurrentUserTest()
            {
                await actualTimecardHistoriesService.GetTimecardHistories2Async(startDate, endDate);
                TimecardHistoriesRepositoryMock.Verify(rm => rm.GetTimecardHistories2Async(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.Is<List<string>>(ids =>
                    ids.Count() == 1 && ids.ElementAt(0) == currentUserFactory.CurrentUser.PersonId)));
            }

            [TestMethod]
            public async Task ExpectedEqualsActualTest()
            {
                var expected = (await testRepository.GetTimecardHistories2Async(startDate, endDate, new List<string>() { currentUserFactory.CurrentUser.PersonId }))
                    .Select(tc => timecardHistory2EntityToDtoAdapter.MapToType(tc));

                var actual = await actualTimecardHistoriesService.GetTimecardHistories2Async(startDate, endDate);

                CollectionAssert.AreEqual(expected.ToArray(), actual.ToArray(), timecardHistory2DtoComparer);

            }

            //[TestMethod]
            //public async Task NullSubordinatesMessageAndErrorTest()
            //{
            //    supervisorsRepositoryMock.Setup(req => req.GetSuperviseesBySupervisorAsync(It.IsAny<string>())).ReturnsAsync(null);
            //    await actualTimecardHistoriesService.GetTimecardHistoriesAsync(startDate, endDate);
            //    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
            //}

            [TestMethod]
            public async Task RepositoryReturnsNullLoggedTest()
            {
                TimecardHistoriesRepositoryMock.Setup(r => r.GetTimecardHistories2Async(It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<List<string>>()))
                    .Returns<DateTime, DateTime, List<string>>((start, end, ids) => Task.FromResult<IEnumerable<Domain.TimeManagement.Entities.TimecardHistory2>>(null));

                await actualTimecardHistoriesService.GetTimecardHistories2Async(startDate, endDate);
                loggerMock.Verify(l => l.Error(It.IsAny<string>()));
            }

        }

        #endregion

        #region HELPERS
        // v1
        private FunctionEqualityComparer<TimecardHistory> TimecardHistoryComparer()
        {
            return new FunctionEqualityComparer<TimecardHistory>(
                (tc1, tc2) =>
                    tc1.Id == tc2.Id &&
                    tc1.EmployeeId == tc2.EmployeeId &&
                    tc1.PayCycleId == tc2.PayCycleId &&
                    tc1.PositionId == tc2.PositionId &&
                    tc1.PeriodEndDate == tc2.PeriodEndDate &&
                    tc1.PeriodStartDate == tc2.PeriodStartDate &&
                    tc1.Timestamp.AddDateTime == tc2.Timestamp.AddDateTime &&
                    tc1.Timestamp.ChangeDateTime == tc2.Timestamp.ChangeDateTime &&
                    tc1.Timestamp.AddOperator == tc2.Timestamp.AddOperator &&
                    tc1.Timestamp.ChangeOperator == tc2.Timestamp.ChangeOperator &&
                    this.TimeEntryHistoriesEqual(tc1, tc2),
                    (tc) => tc.Id.GetHashCode()
                );
        }

        private FunctionEqualityComparer<TimeEntryHistory> TimeEntryHistoriesComparer()
        {
            return new FunctionEqualityComparer<TimeEntryHistory>((a, b) => a.Id == b.Id, (x) => x.GetHashCode());
        }

        private bool TimeEntryHistoriesEqual(TimecardHistory a, TimecardHistory b)
        {
            CollectionAssert.AreEqual(a.TimeEntryHistories, b.TimeEntryHistories, TimeEntryHistoriesComparer());
            return true;
        }
        // v2
        private FunctionEqualityComparer<TimecardHistory2> TimecardHistory2Comparer()
        {
            return new FunctionEqualityComparer<TimecardHistory2>(
                (tc1, tc2) =>
                    tc1.Id == tc2.Id &&
                    tc1.EmployeeId == tc2.EmployeeId &&
                    tc1.PayCycleId == tc2.PayCycleId &&
                    tc1.PositionId == tc2.PositionId &&
                    tc1.PeriodEndDate == tc2.PeriodEndDate &&
                    tc1.PeriodStartDate == tc2.PeriodStartDate &&
                    tc1.Timestamp.AddDateTime == tc2.Timestamp.AddDateTime &&
                    tc1.Timestamp.ChangeDateTime == tc2.Timestamp.ChangeDateTime &&
                    tc1.Timestamp.AddOperator == tc2.Timestamp.AddOperator &&
                    tc1.Timestamp.ChangeOperator == tc2.Timestamp.ChangeOperator &&
                    this.TimeEntryHistoriesEqual(tc1, tc2),
                    (tc) => tc.Id.GetHashCode()
                );
        }

        private FunctionEqualityComparer<TimeEntryHistory2> TimeEntryHistories2Comparer()
        {
            return new FunctionEqualityComparer<TimeEntryHistory2>((a, b) => a.Id == b.Id, (x) => x.GetHashCode());
        }

        private bool TimeEntryHistoriesEqual(TimecardHistory2 a, TimecardHistory2 b)
        {
            CollectionAssert.AreEqual(a.TimeEntryHistories, b.TimeEntryHistories, TimeEntryHistories2Comparer());
            return true;
        }

        #endregion
    }



}
