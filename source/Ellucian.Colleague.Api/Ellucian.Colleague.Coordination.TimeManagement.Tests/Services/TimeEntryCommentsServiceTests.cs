﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.TestUtil;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Colleague.Domain.TimeManagement.Tests;
using Ellucian.Colleague.Coordination.TimeManagement.Services;
using Ellucian.Colleague.Domain.Repositories;
using slf4net;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Colleague.Coordination.TimeManagement.Adapters;
using Ellucian.Colleague.Coordination.Base.Adapters;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.TimeManagement.Services;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Domain.Entities;
using Ellucian.Colleague.Domain.TimeManagement;
using Ellucian.Colleague.Domain.Base.Repositories;

namespace Ellucian.Colleague.Coordination.TimeManagement.Tests.Services
{
    [TestClass]
    public class TimeEntryCommentsServiceTests : CoordinationServiceTestsSetup
    {
        public Mock<ITimeEntryCommentsRepository> commentsRepositoryMock;
        public Mock<ISupervisorsRepository> supervisorRepositoryMock;
        public Mock<IPersonBaseRepository> personBaseRepositoryMock;
        public TestTimeEntryCommentsRepository fakeCommentsRepository;
        public ITypeAdapter<Domain.TimeManagement.Entities.TimeEntryComment, Dtos.TimeManagement.TimeEntryComments> commentsEntityToDtoAdapter;
        public ITypeAdapter<Dtos.TimeManagement.TimeEntryComments, Domain.TimeManagement.Entities.TimeEntryComment> commentsDtoToEntityAdapter;

        public TimeEntryCommentsService commentsService;

        public FunctionEqualityComparer<TimeEntryComments> commentsDtoComparer;

        private IEnumerable<Role> MakinSomeRoles()
        {
            var role = new Role(1, "Supervisor");
            role.AddPermission(new Permission("APPROVE.REJECT.TIME.ENTRY"));
            return new List<Role>() { role };
        }

        public void CommentsServiceTestsInitialize()
        {
            MockInitialize();

            fakeCommentsRepository = new TestTimeEntryCommentsRepository();
            commentsRepositoryMock = new Mock<ITimeEntryCommentsRepository>();
            supervisorRepositoryMock = new Mock<ISupervisorsRepository>();
            personBaseRepositoryMock = new Mock<IPersonBaseRepository>();
            commentsEntityToDtoAdapter = adapterRegistryMock.Object.GetAdapter<Domain.TimeManagement.Entities.TimeEntryComment, Dtos.TimeManagement.TimeEntryComments>();
            commentsDtoToEntityAdapter = adapterRegistryMock.Object.GetAdapter<Dtos.TimeManagement.TimeEntryComments, Domain.TimeManagement.Entities.TimeEntryComment>();

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Domain.TimeManagement.Entities.TimeEntryComment, Dtos.TimeManagement.TimeEntryComments>())
                 .Returns(() => commentsEntityToDtoAdapter);

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Dtos.TimeManagement.TimeEntryComments, Domain.TimeManagement.Entities.TimeEntryComment>())
                .Returns(() => commentsDtoToEntityAdapter);

            currentUserFactory = new CurrentUserSetup.EmployeeUserFactory();

            commentsService = new TimeEntryCommentsService(
                commentsRepositoryMock.Object,
                supervisorRepositoryMock.Object,
                personBaseRepositoryMock.Object,
                adapterRegistryMock.Object,
                currentUserFactory,
                roleRepositoryMock.Object,
                loggerMock.Object
                );

            roleRepositoryMock.Setup(r => r.Roles).Returns(() => MakinSomeRoles());

            commentsRepositoryMock.Setup(cr => cr.GetCommentsAsync(It.IsAny<List<string>>()))
                .Returns<List<string>>(inputIds => fakeCommentsRepository.GetCommentsAsync(inputIds));

            supervisorRepositoryMock.Setup(s => s.GetSuperviseesBySupervisorAsync(It.IsAny<string>()))
                .Returns<string>(s => Task.FromResult(new List<string>().AsEnumerable()));

            commentsDtoComparer = TimeEntryCommentsComparer();

        }


        [TestClass]
        public class CreateCommentsTests : TimeEntryCommentsServiceTests
        {
            TimeEntryComments commentsToCreate = new TimeEntryComments()
            {
                Comments = "Purified by reverse osmosis and/or distillation to ensure quality",
                EmployeeId = "24601",
                Id = "",
                PayCycleId = "A01",
                PayPeriodEndDate = DateTime.UtcNow,
                TimecardId = "999",
                TimecardStatusId = ""
            };

            [TestInitialize]
            public void Initialize()
            {
                base.CommentsServiceTestsInitialize();
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NullCommentsArgumentTest()
            {
                commentsToCreate = null;
                await commentsService.CreateCommentsAsync(commentsToCreate);
            }


        }

        [TestClass]
        public class GetCommentsTests : TimeEntryCommentsServiceTests
        {
            [TestInitialize]
            public void Initialize()
            {
                base.CommentsServiceTestsInitialize();
            }

            [TestMethod]
            public async Task UserHasApproveRejectPermissionButNullSubordinateIdsLogTest()
            {
                commentsService = new TimeEntryCommentsService(
                    commentsRepositoryMock.Object,
                    supervisorRepositoryMock.Object,
                    personBaseRepositoryMock.Object,
                    adapterRegistryMock.Object,
                    new CurrentUserSetup.SupervisorUserFactory(),
                    roleRepositoryMock.Object,
                    loggerMock.Object);

                supervisorRepositoryMock.Setup(s => s.GetSuperviseesBySupervisorAsync(It.IsAny<string>()))
                    .ReturnsAsync(null);



                await commentsService.GetCommentsAsync();

                loggerMock.Verify(e => e.Error(It.IsAny<string>()));

            }

            [TestMethod]
            public async Task NullCommentsReturnedSituationLoggedAndEmptyListReturnedTest()
            {
                commentsRepositoryMock.Setup(cr => cr.GetCommentsAsync(It.IsAny<List<string>>()))
                     .ReturnsAsync(null);

                var newCommentList = await commentsService.GetCommentsAsync();

                loggerMock.Verify(e => e.Error(It.IsAny<string>()));

                Assert.AreEqual(typeof(List<Ellucian.Colleague.Dtos.TimeManagement.TimeEntryComments>), newCommentList.GetType());
                Assert.IsTrue(!newCommentList.Any());
            }
        }


        private FunctionEqualityComparer<TimeEntryComments> TimeEntryCommentsComparer()
        {
            return new FunctionEqualityComparer<TimeEntryComments>(
                (s1, s2) =>
                    s1.EmployeeId == s2.EmployeeId &&
                    s1.PayCycleId == s2.PayCycleId &&
                    s1.PayPeriodEndDate == s2.PayPeriodEndDate &&
                    s1.TimecardId == s2.TimecardId &&
                    s1.TimecardStatusId == s2.TimecardStatusId &&
                    s1.Comments == s2.Comments &&
                    s1.CommentsTimestamp.AddDateTime == s1.CommentsTimestamp.AddDateTime &&
                    s1.CommentsTimestamp.ChangeDateTime == s1.CommentsTimestamp.ChangeDateTime &&
                    s1.CommentsTimestamp.AddOperator == s1.CommentsTimestamp.AddOperator &&
                    s1.CommentsTimestamp.ChangeOperator == s1.CommentsTimestamp.ChangeOperator,
                    (s) => s.Id.GetHashCode()
                );
        }
    }
}
