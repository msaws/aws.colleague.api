﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Moq;
using Ellucian.Colleague.Domain.TimeManagement.Tests;
using Ellucian.Colleague.Coordination.TimeManagement.Services;
using Ellucian.Web.Adapters;
using Ellucian.Colleague.Domain.Repositories;
using slf4net;
using Ellucian.Web.Security;
using Ellucian.Colleague.Coordination.TimeManagement.Adapters;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using System.Threading.Tasks;
using System.Linq;
using Ellucian.Web.Http.TestUtil;

namespace Ellucian.Colleague.Coordination.TimeManagement.Tests.Services
{

     [TestClass]
     public class OvertimeCalculationDefinitionsServiceTests : OvertimeCalculationDefinitionsServiceTestsSetup
     {
          #region SETUP
          public string employeeId;
          public List<string> employeeIds;
          public Mock<IOvertimeCalculationDefinitionsRepository> overtimeCalculationDefinitionsRepositoryMock;
          public TestOvertimeCalculationDefinitionsRepository testRepository;
          public Mock<IAdapterRegistry> adapterRegistryMock;
          public Mock<IRoleRepository> roleRepositoryMock;
          public Mock<ILogger> loggerMock;
          public ICurrentUserFactory employeeCurrentUserFactory;


          // entity -> dto
          public ITypeAdapter<Domain.TimeManagement.Entities.OvertimeCalculationDefinition, Dtos.TimeManagement.OvertimeCalculationDefinition> overtimeCalculationDefinitionEntityToDtoAdapter;

          public OvertimeCalculationDefinitionsService actualOvertimeCalculationDefinitionsService
          {
               get
               {
                    return new OvertimeCalculationDefinitionsService(
                        overtimeCalculationDefinitionsRepositoryMock.Object,
                        adapterRegistryMock.Object,
                        employeeCurrentUserFactory,
                        roleRepositoryMock.Object,
                        loggerMock.Object);
               }
          }

          public FunctionEqualityComparer<Dtos.TimeManagement.OvertimeCalculationDefinition> overtimeCalculationDefinitionDtoComparer;

          public void OvertimeCalculationDefinitionTestsInitialize()
          {

               adapterRegistryMock = new Mock<IAdapterRegistry>();
               roleRepositoryMock = new Mock<IRoleRepository>();
               loggerMock = new Mock<ILogger>();
               employeeCurrentUserFactory = new CurrentUserSetup.EmployeeUserFactory();

               testRepository = new TestOvertimeCalculationDefinitionsRepository();

               // adapters
               overtimeCalculationDefinitionEntityToDtoAdapter = new OvertimeCalculationDefinitionEntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);

               // mocks
               overtimeCalculationDefinitionsRepositoryMock = new Mock<IOvertimeCalculationDefinitionsRepository>();

               overtimeCalculationDefinitionsRepositoryMock.Setup(ot => ot.GetOvertimeCalculationDefinitionsAsync())
                   .Returns(testRepository.GetOvertimeCalculationDefinitionsAsync());

               adapterRegistryMock.Setup(ar => ar.GetAdapter<Domain.TimeManagement.Entities.OvertimeCalculationDefinition, Dtos.TimeManagement.OvertimeCalculationDefinition>())
                   .Returns(() => overtimeCalculationDefinitionEntityToDtoAdapter);


               overtimeCalculationDefinitionDtoComparer = this.OvertimeCalculationDefinitionComparer();
          }
          #endregion

          #region TESTS
          [TestClass]
          public class GetOvertimeCalculationsServiceTests : OvertimeCalculationDefinitionsServiceTests
          {
               [TestInitialize]
               public void Initialize()
               {
                    OvertimeCalculationDefinitionTestsInitialize();
               }

               [TestMethod]
               public async Task RepositoryReturnsOvertimeCalculationDefinitionEntities()
               {
                    IEnumerable<Dtos.TimeManagement.OvertimeCalculationDefinition> dtos = null;
                    dtos = await actualOvertimeCalculationDefinitionsService.GetOvertimeCalculationDefinitionsAsync();
                    Assert.IsNotNull(dtos);

               }

               [TestMethod]
               public async Task ExpectedEqualsActualTest()
               {
                    var expected = (await testRepository.GetOvertimeCalculationDefinitionsAsync())
                        .Select(tc => overtimeCalculationDefinitionEntityToDtoAdapter.MapToType(tc));

                    var actual = await actualOvertimeCalculationDefinitionsService.GetOvertimeCalculationDefinitionsAsync();

                    CollectionAssert.AreEqual(expected.ToArray(), actual.ToArray(), overtimeCalculationDefinitionDtoComparer);

               }

               [TestMethod]
               [ExpectedException(typeof(ApplicationException))]
               public async Task RepositoryReturnsNullTest()
               {
                    overtimeCalculationDefinitionsRepositoryMock.Setup(r => r.GetOvertimeCalculationDefinitionsAsync())
                        .Returns(Task.FromResult<List<Domain.TimeManagement.Entities.OvertimeCalculationDefinition>>(null));

                    try
                    {
                         await actualOvertimeCalculationDefinitionsService.GetOvertimeCalculationDefinitionsAsync();
                    }
                    catch (Exception)
                    {
                         loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                         throw;
                    }
               }

          }
          #endregion
          private FunctionEqualityComparer<Dtos.TimeManagement.OvertimeCalculationDefinition> OvertimeCalculationDefinitionComparer()
          {
               return new FunctionEqualityComparer<Dtos.TimeManagement.OvertimeCalculationDefinition>(
                   (otcd1, otcd2) =>
                       otcd1.Id == otcd2.Id &&
                       otcd1.Description == otcd2.Description &&
                       otcd1.OvertimePayCalculationMethod == otcd2.OvertimePayCalculationMethod &&
                       this.overtimeCalculationDefinitionEqual(otcd1, otcd2),
                       (otcd) => otcd.Id.GetHashCode()
                   );
          }

          private FunctionEqualityComparer<Dtos.TimeManagement.HoursThreshold> HoursThresholdComparer()
          {
               return new FunctionEqualityComparer<Dtos.TimeManagement.HoursThreshold>(
                    (ht1, ht2) => 
                         ht1.Threshold == ht2.Threshold &&
                         ht1.DefaultEarningsType.EarningsTypeId == ht2.DefaultEarningsType.EarningsTypeId &&
                         ht1.DefaultEarningsType.Factor == ht2.DefaultEarningsType.Factor &&
                         ht1.AlternateEarningsType.EarningsTypeId == ht2.AlternateEarningsType.EarningsTypeId &&
                         ht1.AlternateEarningsType.Factor == ht2.AlternateEarningsType.Factor,
                         (ht) => ht.ToString().GetHashCode()
                         
               );
          }

        private bool overtimeCalculationDefinitionEqual(Dtos.TimeManagement.OvertimeCalculationDefinition a, Dtos.TimeManagement.OvertimeCalculationDefinition b)
        {

            CollectionAssert.AreEqual(a.WeeklyHoursThreshold, b.WeeklyHoursThreshold, HoursThresholdComparer());            
            CollectionAssert.AreEqual(a.DailyHoursThreshold, b.DailyHoursThreshold, HoursThresholdComparer());            
            return true;
        }

     }
     #region BASE

     public abstract class OvertimeCalculationDefinitionsServiceTestsSetup : CurrentUserSetup
     {
          public Mock<IAdapterRegistry> adapterRegistryMock;
          public Mock<IRoleRepository> roleRepositoryMock;
          public Mock<ILogger> loggerMock;
          public ICurrentUserFactory employeeCurrentUserFactory;

          public void MockInitialize()
          {
               adapterRegistryMock = new Mock<IAdapterRegistry>();
               roleRepositoryMock = new Mock<IRoleRepository>();
               loggerMock = new Mock<ILogger>();
               employeeCurrentUserFactory = new CurrentUserSetup.EmployeeUserFactory();
          }
     }
     #endregion
}
