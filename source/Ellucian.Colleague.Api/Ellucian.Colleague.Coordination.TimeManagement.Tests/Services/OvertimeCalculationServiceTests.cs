﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Coordination.TimeManagement.Adapters;
using Ellucian.Colleague.Coordination.TimeManagement.Services;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Colleague.Domain.TimeManagement.Tests;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.TimeManagement.Tests.Services
{
    [TestClass]
    public class OvertimeCalculationServiceTests : CoordinationServiceTestsSetup
    {
        public Mock<IOvertimeCalculationRepository> overtimeCalculationRepositoryMock;

        public TestOvertimeCalculationRepository overtimeCalculationTestData;

        public OvertimeCalculationService serviceUnderTest;

        public void OvertimeCalculationServiceTestsInitialize()
        {
            MockInitialize();
            overtimeCalculationRepositoryMock = new Mock<IOvertimeCalculationRepository>();
            overtimeCalculationTestData = new TestOvertimeCalculationRepository();

            adapterRegistryMock.Setup(r => r.GetAdapter<Domain.TimeManagement.Entities.OvertimeCalculationResult, OvertimeCalculationResult>())
                .Returns(new OvertimeCalculationResultEntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object));

            serviceUnderTest = new OvertimeCalculationService(overtimeCalculationRepositoryMock.Object, adapterRegistryMock.Object, currentUserFactory, roleRepositoryMock.Object, loggerMock.Object);
        }

        [TestClass]
        public class CalculateOvertimeTests : OvertimeCalculationServiceTests
        {
            public OvertimeQueryCriteria inputCriteria;

            [TestInitialize]
            public void Initialize()
            {
                OvertimeCalculationServiceTestsInitialize();

                inputCriteria = new OvertimeQueryCriteria()
                {
                    PersonId = currentUserFactory.CurrentUser.PersonId,
                    PayCycleId = "BW",
                    StartDate = new DateTime(2016, 6, 4),
                    EndDate = new DateTime(2016, 6, 10)
                };

                overtimeCalculationRepositoryMock.Setup(r =>
                    r.CalculateOvertime(
                        It.Is<string>(s => s == inputCriteria.PersonId),
                        It.Is<DateTime>(d => d == inputCriteria.StartDate),
                        It.Is<DateTime>(d => d == inputCriteria.EndDate),
                        It.IsAny<string>()
                    )).Returns<string, DateTime, DateTime, string>((pid, start, end, payCycleId) =>
                        overtimeCalculationTestData.CalculateOvertime(pid, start, end, payCycleId));

            }

            [TestMethod]
            public async Task ResultIsBasedOnCriteriaTest()
            {
                var result = await serviceUnderTest.CalculateOvertime(inputCriteria);
                Assert.AreEqual(inputCriteria.PersonId, result.PersonId);
                Assert.AreEqual(inputCriteria.StartDate, result.StartDate);
                Assert.AreEqual(inputCriteria.EndDate, result.EndDate);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task InputCriteriaRequiredTest()
            {
                await serviceUnderTest.CalculateOvertime(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task PersonIdRequiredTest()
            {
                inputCriteria.PersonId = null;
                await serviceUnderTest.CalculateOvertime(inputCriteria);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task PayCycleIdRequiredTest()
            {
                inputCriteria.PayCycleId = null;
                await serviceUnderTest.CalculateOvertime(inputCriteria);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task StartDateMustBeBeforeEndDateTest()
            {
                inputCriteria.StartDate = inputCriteria.EndDate.AddDays(1);
                await serviceUnderTest.CalculateOvertime(inputCriteria);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task CurrentUserCanOnlyGetDataForSelfTest()
            {
                inputCriteria.PersonId = "foo";
                await serviceUnderTest.CalculateOvertime(inputCriteria);
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task UnexpectedResultFromRepositoryThrowsExceptionTest()
            {
                overtimeCalculationRepositoryMock.Setup(r => r.CalculateOvertime(It.IsAny<string>(), It.IsAny<DateTime>(), It.IsAny<DateTime>(), It.IsAny<string>()))
                    .Returns(() => Task.FromResult<Domain.TimeManagement.Entities.OvertimeCalculationResult>(null));

                await serviceUnderTest.CalculateOvertime(inputCriteria);
            }
        }
    }
}
