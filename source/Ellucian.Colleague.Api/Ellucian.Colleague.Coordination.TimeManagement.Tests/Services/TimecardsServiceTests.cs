﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.TestUtil;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Colleague.Domain.TimeManagement.Tests;
using Ellucian.Colleague.Coordination.TimeManagement.Services;
using Ellucian.Colleague.Domain.Repositories;
using slf4net;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Colleague.Coordination.TimeManagement.Adapters;
using Ellucian.Colleague.Coordination.Base.Adapters;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.TimeManagement.Services;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.TimeManagement;

namespace Ellucian.Colleague.Coordination.TimeManagement.Tests.Services
{
    [TestClass]
    public class TimecardsServiceTests : CoordinationServiceTestsSetup
    {
        #region SETUP
        public string employeeId;
        public List<string> employeeIds;
        public Mock<ITimecardsRepository> timecardsRepositoryMock;
        public Mock<ITimecardDomainService> domainServiceMock;
        public Mock<ISupervisorsRepository> supervisorsRepositoryMock;
        public TimecardDomainService actualDomainService;
        public TestTimecardsRepository testRepository;
        public Domain.TimeManagement.Entities.TimecardHelper timecardHelper;
        public Domain.TimeManagement.Entities.Timecard2Helper timecard2Helper;

        protected Domain.Entities.Role timeApprovalRole;
        private Domain.Entities.Permission timeEntryApprovalPermission;

        // entity -> dto
        public ITypeAdapter<Domain.TimeManagement.Entities.Timecard, Dtos.TimeManagement.Timecard> timecardEntityToDtoAdapter;
        public ITypeAdapter<Domain.TimeManagement.Entities.Timecard2, Dtos.TimeManagement.Timecard2> timecard2EntityToDtoAdapter;

        // dto -> entity
        public ITypeAdapter<Dtos.TimeManagement.Timecard, Domain.TimeManagement.Entities.Timecard> timecardDtoToEntityAdapter;
        public ITypeAdapter<Dtos.TimeManagement.Timecard2, Domain.TimeManagement.Entities.Timecard2> timecard2DtoToEntityAdapter;

        public TimecardsService actualTimecardsService
        {
            get
            {
                return new TimecardsService(
                    timecardsRepositoryMock.Object,
                    domainServiceMock.Object,
                    supervisorsRepositoryMock.Object,
                    adapterRegistryMock.Object,
                    currentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object);
            }
        }

        public FunctionEqualityComparer<Timecard> timecardDtoComparer;
        public FunctionEqualityComparer<Timecard2> timecard2DtoComparer;

        public void TimecardServiceTestsInitialize()
        {

            MockInitialize();
            testRepository = new TestTimecardsRepository();
            
            // adapters
            timecardEntityToDtoAdapter = new TimecardEntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);
            timecardDtoToEntityAdapter = new TimecardDtoToEntityAdapter(adapterRegistryMock.Object, loggerMock.Object);
            timecard2EntityToDtoAdapter = new Timecard2EntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);
            timecard2DtoToEntityAdapter = new Timecard2DtoToEntityAdapter(adapterRegistryMock.Object, loggerMock.Object);

            // mocks
            domainServiceMock = new Mock<ITimecardDomainService>();
            timecardsRepositoryMock = new Mock<ITimecardsRepository>();
            supervisorsRepositoryMock = new Mock<ISupervisorsRepository>();
            actualDomainService = new TimecardDomainService(loggerMock.Object);
            // v1
            timecardsRepositoryMock.Setup(tc => tc.GetTimecardsAsync(It.IsAny<List<string>>()))
                .Returns<List<string>>((ids) =>
                    testRepository.GetTimecardsAsync(ids));

            timecardsRepositoryMock.Setup(tc => tc.GetTimecardAsync(It.IsAny<string>()))
                .Returns<string>((id) =>
                    testRepository.GetTimecardAsync(id));

            domainServiceMock.Setup(ds => ds.CreateTimecardHelper(It.IsAny<Domain.TimeManagement.Entities.Timecard>(), It.IsAny<Domain.TimeManagement.Entities.Timecard>()))
                .Returns<Domain.TimeManagement.Entities.Timecard, Domain.TimeManagement.Entities.Timecard>((tc1, tc2) => 
                    actualDomainService.CreateTimecardHelper(tc1, tc2));

            timecardsRepositoryMock.Setup(tc => tc.UpdateTimecardAsync(It.IsAny<Domain.TimeManagement.Entities.TimecardHelper>()))
                .Returns<Domain.TimeManagement.Entities.TimecardHelper>((helper) =>
                    testRepository.GetTimecardAsync(helper.Timecard.Id));
            // v2
            timecardsRepositoryMock.Setup(tc => tc.GetTimecards2Async(It.IsAny<List<string>>()))
                .Returns<List<string>>((ids) =>
                testRepository.GetTimecards2Async(ids));

            timecardsRepositoryMock.Setup(tc => tc.GetTimecard2Async(It.IsAny<string>()))
                .Returns<string>((id) =>
                    testRepository.GetTimecard2Async(id));

            domainServiceMock.Setup(ds => ds.CreateTimecard2Helper(It.IsAny<Domain.TimeManagement.Entities.Timecard2>(), It.IsAny<Domain.TimeManagement.Entities.Timecard2>()))
                .Returns<Domain.TimeManagement.Entities.Timecard2, Domain.TimeManagement.Entities.Timecard2>((tc1, tc2) =>
                    actualDomainService.CreateTimecard2Helper(tc1, tc2));

            timecardsRepositoryMock.Setup(tc => tc.UpdateTimecard2Async(It.IsAny<Domain.TimeManagement.Entities.Timecard2Helper>()))
                .Returns<Domain.TimeManagement.Entities.Timecard2Helper>((helper) =>
                    testRepository.GetTimecard2Async(helper.Timecard.Id));

            var newTimecardId = 0;
            var newTimeEntryId = 0;
            timecardsRepositoryMock.Setup(tc => tc.CreateTimecardAsync(It.IsAny<Domain.TimeManagement.Entities.Timecard>()))
                .Returns<Domain.TimeManagement.Entities.Timecard>((tcd) =>
                    Task.FromResult(new Domain.TimeManagement.Entities.Timecard(
                        tcd.EmployeeId,
                        tcd.PayCycleId,
                        tcd.PositionId,
                        tcd.PeriodStartDate,
                        tcd.PeriodEndDate,
                        tcd.StartDate,
                        tcd.EndDate,
                        (++newTimecardId).ToString()
                    )
                    {
                        Timestamp = tcd.Timestamp,
                        TimeEntries = tcd.TimeEntries.Select(te => 
                            new Domain.TimeManagement.Entities.TimeEntry(
                                te.TimecardId,
                                te.EarningsTypeId,
                                te.InDateTime.Value.DateTime,
                                te.OutDateTime.Value.DateTime,
                                te.WorkedDate,
                                (++newTimeEntryId).ToString()                                
                            ) 
                            { 
                                PersonLeaveId = te.PersonLeaveId,
                                ProjectId = te.ProjectId,
                                Timestamp = te.Timestamp,
                            }).ToList()  
                    }));
        
            var newTimecard2Id = 0;
            var newTimeEntry2Id = 0;

            timecardsRepositoryMock.Setup(tc => tc.CreateTimecard2Async(It.IsAny<Domain.TimeManagement.Entities.Timecard2>()))
                .Returns<Domain.TimeManagement.Entities.Timecard2>((tcd) =>
                    Task.FromResult(new Domain.TimeManagement.Entities.Timecard2(
                        tcd.EmployeeId,
                        tcd.PayCycleId,
                        tcd.PositionId,
                        tcd.PeriodStartDate,
                        tcd.PeriodEndDate,
                        tcd.StartDate,
                        tcd.EndDate,
                        (++newTimecard2Id).ToString()
                    )
                    {
                        Timestamp = tcd.Timestamp,
                        TimeEntries = tcd.TimeEntries.Select(te => 
                            new Domain.TimeManagement.Entities.TimeEntry2(
                                te.TimecardId,
                                te.EarningsTypeId,
                                te.InDateTime.Value,
                                te.OutDateTime.Value,
                                te.WorkedDate,
                                (++newTimeEntry2Id).ToString()                                
                            ) 
                            { 
                                PersonLeaveId = te.PersonLeaveId,
                                ProjectId = te.ProjectId,
                                Timestamp = te.Timestamp,
                            }).ToList()  
                    }
                )
            );
            // v1
            adapterRegistryMock.Setup(ar => ar.GetAdapter<Domain.TimeManagement.Entities.Timecard, Dtos.TimeManagement.Timecard>())
                .Returns(() => 
                    new TimecardEntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object));

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Dtos.TimeManagement.Timecard, Domain.TimeManagement.Entities.Timecard>())
                .Returns(() => timecardDtoToEntityAdapter);

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Domain.TimeManagement.Entities.TimeEntry, Dtos.TimeManagement.TimeEntry>())
                .Returns(() => new AutoMapperAdapter<Domain.TimeManagement.Entities.TimeEntry, Dtos.TimeManagement.TimeEntry>(adapterRegistryMock.Object, loggerMock.Object));
            // v2
            adapterRegistryMock.Setup(ar => ar.GetAdapter<Domain.TimeManagement.Entities.Timecard2, Dtos.TimeManagement.Timecard2>())
                .Returns(() =>
                    new Timecard2EntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object));

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Dtos.TimeManagement.Timecard2, Domain.TimeManagement.Entities.Timecard2>())
                .Returns(() => timecard2DtoToEntityAdapter);

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Domain.TimeManagement.Entities.TimeEntry2, Dtos.TimeManagement.TimeEntry2>())
                .Returns(() => new AutoMapperAdapter<Domain.TimeManagement.Entities.TimeEntry2, Dtos.TimeManagement.TimeEntry2>(adapterRegistryMock.Object, loggerMock.Object));

            adapterRegistryMock.Setup(ar => ar.GetAdapter<Domain.Base.Entities.Timestamp, Dtos.Base.Timestamp>())
                .Returns(() => new AutoMapperAdapter<Domain.Base.Entities.Timestamp, Dtos.Base.Timestamp>(adapterRegistryMock.Object, loggerMock.Object));

            // permissions mock
            timeApprovalRole = new Domain.Entities.Role(76, "TIME MANAGEMENT SUPERVISOR");
            timeEntryApprovalPermission = new Ellucian.Colleague.Domain.Entities.Permission(TimeManagementPermissionCodes.ApproveRejectEmployeeTimecard);
            timeApprovalRole.AddPermission(timeEntryApprovalPermission);
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { timeApprovalRole });

            timecardDtoComparer = this.TimecardComparer();
            timecard2DtoComparer = this.Timecard2Comparer();
        }

        #endregion

        #region TESTS VERSION 1
        [TestClass]
        public class GetTimecardsTests : TimecardsServiceTests
        {
            [TestInitialize]
            public void Initialize()
            {
                TimecardServiceTestsInitialize();
            }

            [TestMethod]
            public async Task RepositoryCalledWithCurrentUserTest()
            {
                await actualTimecardsService.GetTimecardsAsync();
                timecardsRepositoryMock.Verify(rm => rm.GetTimecardsAsync(It.Is<List<string>>(ids =>
                    ids.Count() == 1 && ids.ElementAt(0) == currentUserFactory.CurrentUser.PersonId)));
            }

            [TestMethod]
            public async Task ExpectedEqualsActualTest()
            {
                var expected = (await testRepository.GetTimecardsAsync(new List<string>() { currentUserFactory.CurrentUser.PersonId }))
                    .Select(tc => timecardEntityToDtoAdapter.MapToType(tc));

                var actual = await actualTimecardsService.GetTimecardsAsync();

                CollectionAssert.AreEqual(expected.ToArray(), actual.ToArray(), timecardDtoComparer);

            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NullSubordinatesMessageAndErrorTest()
            {
                currentUserFactory = new CurrentUserSetup.TimecardSupervisorUserFactory();
                supervisorsRepositoryMock.Setup(req => req.GetSuperviseesBySupervisorAsync(It.IsAny<string>())).ReturnsAsync(null);
                try
                {
                    await actualTimecardsService.GetTimecardsAsync();
                }
                catch(ApplicationException)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            public async Task CanGetTimecardsForSubordinatesTest()
            {
                currentUserFactory = new CurrentUserSetup.TimecardSupervisorUserFactory();
                testRepository.timecardRecords.ForEach(t => t.employeeId = "foobar");
                supervisorsRepositoryMock.Setup(r => r.GetSuperviseesBySupervisorAsync(It.IsAny<string>())).ReturnsAsync(new string[] { "foobar" });
                var expected = await testRepository.GetTimecardsAsync(new List<string>() {currentUserFactory.CurrentUser.PersonId, "foobar"});
                var actual = await actualTimecardsService.GetTimecardsAsync();

                Assert.IsTrue(actual.All(t => t.EmployeeId == "foobar"));
                Assert.AreEqual(expected.Count(), actual.Count());
            }

            

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task RepositoryReturnsNullTest()
            {
                timecardsRepositoryMock.Setup(r => r.GetTimecardsAsync(It.IsAny<List<string>>()))
                    .Returns<List<string>>((ids) => Task.FromResult<IEnumerable<Domain.TimeManagement.Entities.Timecard>>(null));

                try
                {
                    await actualTimecardsService.GetTimecardsAsync();
                }
                catch (Exception)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw;
                }
            }

        }

        [TestClass]
        public class GetTimecardTests : TimecardsServiceTests
        {
            public string inputTimecardId;

            [TestInitialize]
            public void Initialize()
            {
                TimecardServiceTestsInitialize();
                inputTimecardId = testRepository.timecardRecords[0].id;
            }

            [TestMethod]
            public async Task CorrectDtoReturnedTest()
            {
                var expected = timecardEntityToDtoAdapter.MapToType(await testRepository.GetTimecardAsync(inputTimecardId));
                var actual = await actualTimecardsService.GetTimecardAsync(inputTimecardId);
                Assert.IsTrue(timecardDtoComparer.Equals(expected, actual));
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NullTimecardIdLoggedAndErrorTest()
            {
                await actualTimecardsService.GetTimecardAsync(string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task NullReposinitoryResponseLoggedAndErrorTest()
            {
                timecardsRepositoryMock.Setup(tc => tc.GetTimecardAsync(It.IsAny<string>()))
                    .ReturnsAsync(null);

                await actualTimecardsService.GetTimecardAsync(inputTimecardId);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task TimecardDoesNotBelongToCurrentUserTest()
            {
                testRepository.timecardRecords.ForEach(t => t.employeeId = "foobar");
                await actualTimecardsService.GetTimecardAsync(inputTimecardId);
            }

            [TestMethod]
            public async Task SupervisorHasPermissionToGetEmployeeTimecardTest()
            {
                testRepository.timecardRecords.ForEach(t => t.employeeId = "foobar");
                currentUserFactory = new CurrentUserSetup.TimecardSupervisorUserFactory();
                supervisorsRepositoryMock.Setup(r => r.GetSuperviseesBySupervisorAsync(It.IsAny<string>())).ReturnsAsync(new string[] { "foobar" });

                var actual = await actualTimecardsService.GetTimecardAsync(inputTimecardId);
                Assert.AreEqual("foobar", actual.EmployeeId);               
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SupervisorHasPermissionButDoesNotSuperviseEmployeeTest()
            {
                testRepository.timecardRecords.ForEach(t => t.employeeId = "foobar");

                currentUserFactory = new CurrentUserSetup.TimecardSupervisorUserFactory();
                supervisorsRepositoryMock.Setup(r => r.GetSuperviseesBySupervisorAsync(It.IsAny<string>())).ReturnsAsync(new string[] { "raboof" });
                await actualTimecardsService.GetTimecardAsync(inputTimecardId);               
            }
        }

        [TestClass]
        public class CreateTimecardTests : TimecardsServiceTests
        {
            public Dtos.TimeManagement.Timecard newTimecard;

            [TestInitialize]
            public void Initialize()
            {
                TimecardServiceTestsInitialize();
                newTimecard = new Timecard()
                {

                    EmployeeId = "0003914",
                    Id = "",
                    PayCycleId = "52",
                    PeriodEndDate = new DateTime(2014, 1, 1),
                    PeriodStartDate = new DateTime(2013, 1, 1),
                    PositionId = "@#",
                    StartDate = new DateTime(2013, 1, 1),
                    EndDate = new DateTime(2013, 1, 7),                    
                    TimeEntries = new List<Dtos.TimeManagement.TimeEntry>() 
                    { 
                        new Dtos.TimeManagement.TimeEntry()
                        {
                            Id = "",                            
                            EarningsTypeId="1",
                            WorkedTime = new TimeSpan(07,04,00),
                            WorkedDate = new DateTime(2016, 4, 1),
                            InDateTime= new DateTimeOffset(2016, 4, 1, 7, 0,0,new TimeSpan(1,0,0)),
                            OutDateTime = new DateTimeOffset(2016, 4, 1, 15, 0,0, new TimeSpan(1,0,0)),
                            PersonLeaveId ="l",
                            ProjectId = "1l",
                            TimecardId = string.Empty,
                            Timestamp = new Dtos.Base.Timestamp()
                        } 
                    
                    },
                    Timestamp = new Dtos.Base.Timestamp(),
                };
            }

            [TestMethod]
            public async Task NewTimecardIsReturnedTest()
            {
                var expexted = newTimecard;
                var result = await actualTimecardsService.CreateTimecardAsync(newTimecard);
               // Assert.IsTrue(timecardDtoComparer.Equals(expexted, result)); we don't necessarily want to use the DTO comparer because the created timecard will have an idea whereas the original will not
               foreach(var expectedProp in expexted.GetType().GetProperties())
               {
                   foreach(var resultProp in result.GetType().GetProperties())
                   {
                       if (expectedProp.Name != "TimeEntries" // ignore the time entries for now
                           && expectedProp.Name != "Id" // can't compare the id
                           && expectedProp.Name != "Timestamp" // and ignore the timestamp
                           && expectedProp == resultProp)
                       {
                           var name = expectedProp.Name;
                           Assert.AreEqual(expectedProp.GetValue(expexted),resultProp.GetValue(result));
                           break;
                       }
                   }
               }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task TimecardArgumentRequiredTest()
            {
                await actualTimecardsService.CreateTimecardAsync(null);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task UserCannotUpdateOthersTimecardTest()
            {
                newTimecard.EmployeeId = "Javer";
                await actualTimecardsService.CreateTimecardAsync(newTimecard);
            }

            [TestMethod]
            [ExpectedException(typeof(ExistingResourceException))]
            public async Task ExistingTimecardCannotBeRecreatedTest()
            {
                var tcToUpdate = timecardEntityToDtoAdapter.MapToType(await testRepository.GetTimecardAsync(testRepository.timecardRecords[0].id));
                await actualTimecardsService.CreateTimecardAsync(tcToUpdate);
            }

        }

        [TestClass]
        public class UpdateTimecardTests : TimecardsServiceTests
        {
            [TestInitialize]
            public void Initialize()
            {
                TimecardServiceTestsInitialize();
            }

            [TestMethod]
            public async Task CorrectTimecardReturned()
            {
                var expected = timecardEntityToDtoAdapter.MapToType(await testRepository.GetTimecardAsync(testRepository.timecardRecords[0].id));
                var actual = await actualTimecardsService.UpdateTimecardAsync(expected);
                Assert.IsTrue(timecardDtoComparer.Equals(expected, actual));
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task TimecardArgumentRequiredTest()
            {
                await actualTimecardsService.UpdateTimecardAsync(null);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task UserCannotUpdateOthersTimecardTest()
            {
                var tcToUpdate = timecardEntityToDtoAdapter.MapToType(await testRepository.GetTimecardAsync(testRepository.timecardRecords[0].id));
                tcToUpdate.EmployeeId = "Javer";
                await actualTimecardsService.UpdateTimecardAsync(tcToUpdate);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task NoExistingTimecardThrowsErrorTest()
            {
                timecardsRepositoryMock.Setup(m => m.GetTimecardAsync(It.IsAny<string>())).ReturnsAsync(null);

                var tcToUpdate = timecardEntityToDtoAdapter.MapToType(await testRepository.GetTimecardAsync(testRepository.timecardRecords[0].id));
                await actualTimecardsService.UpdateTimecardAsync(tcToUpdate);
            }
        }
        #endregion

        #region TESTS VERSION 2
        [TestClass]
        public class GetTimecards2Tests : TimecardsServiceTests
        {
            [TestInitialize]
            public void Initialize()
            {
                TimecardServiceTestsInitialize();
            }

            [TestMethod]
            public async Task RepositoryCalledWithCurrentUserTest()
            {
                await actualTimecardsService.GetTimecards2Async();
                timecardsRepositoryMock.Verify(rm => rm.GetTimecards2Async(It.Is<List<string>>(ids =>
                    ids.Count() == 1 && ids.ElementAt(0) == currentUserFactory.CurrentUser.PersonId)));
            }

            [TestMethod]
            public async Task ExpectedEqualsActualTest()
            {
                var expected = (await testRepository.GetTimecards2Async(new List<string>() { currentUserFactory.CurrentUser.PersonId }))
                    .Select(tc => timecard2EntityToDtoAdapter.MapToType(tc));

                var actual = await actualTimecardsService.GetTimecards2Async();

                CollectionAssert.AreEqual(expected.ToArray(), actual.ToArray(), timecard2DtoComparer);

            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task NullSubordinatesMessageAndErrorTest()
            {
                currentUserFactory = new CurrentUserSetup.TimecardSupervisorUserFactory();
                supervisorsRepositoryMock.Setup(req => req.GetSuperviseesBySupervisorAsync(It.IsAny<string>())).ReturnsAsync(null);
                try
                {
                    await actualTimecardsService.GetTimecards2Async();
                }
                catch (ApplicationException)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            public async Task CanGetTimecardsForSubordinatesTest()
            {
                currentUserFactory = new CurrentUserSetup.TimecardSupervisorUserFactory();
                testRepository.timecardRecords.ForEach(t => t.employeeId = "foobar");
                supervisorsRepositoryMock.Setup(r => r.GetSuperviseesBySupervisorAsync(It.IsAny<string>())).ReturnsAsync(new string[] { "foobar" });
                var expected = await testRepository.GetTimecardsAsync(new List<string>() { currentUserFactory.CurrentUser.PersonId, "foobar" });
                var actual = await actualTimecardsService.GetTimecards2Async();

                Assert.IsTrue(actual.All(t => t.EmployeeId == "foobar"));
                Assert.AreEqual(expected.Count(), actual.Count());
            }



            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task RepositoryReturnsNullTest()
            {
                timecardsRepositoryMock.Setup(r => r.GetTimecards2Async(It.IsAny<List<string>>()))
                    .Returns<List<string>>((ids) => Task.FromResult<IEnumerable<Domain.TimeManagement.Entities.Timecard2>>(null));

                try
                {
                    await actualTimecardsService.GetTimecards2Async();
                }
                catch (Exception)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw;
                }
            }

        }

        [TestClass]
        public class GetTimecard2Tests : TimecardsServiceTests
        {
            public string inputTimecardId;

            [TestInitialize]
            public void Initialize()
            {
                TimecardServiceTestsInitialize();
                inputTimecardId = testRepository.timecardRecords[0].id;
            }

            [TestMethod]
            public async Task CorrectDtoReturnedTest()
            {
                var expected = timecard2EntityToDtoAdapter.MapToType(await testRepository.GetTimecard2Async(inputTimecardId));
                var actual = await actualTimecardsService.GetTimecard2Async(inputTimecardId);
                Assert.IsTrue(timecard2DtoComparer.Equals(expected, actual));
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task NullTimecardIdLoggedAndErrorTest()
            {
                await actualTimecardsService.GetTimecard2Async(string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task NullReposinitoryResponseLoggedAndErrorTest()
            {
                timecardsRepositoryMock.Setup(tc => tc.GetTimecard2Async(It.IsAny<string>()))
                    .ReturnsAsync(null);

                await actualTimecardsService.GetTimecard2Async(inputTimecardId);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task TimecardDoesNotBelongToCurrentUserTest()
            {
                testRepository.timecardRecords.ForEach(t => t.employeeId = "foobar");
                await actualTimecardsService.GetTimecard2Async(inputTimecardId);
            }

            [TestMethod]
            public async Task SupervisorHasPermissionToGetEmployeeTimecardTest()
            {
                testRepository.timecardRecords.ForEach(t => t.employeeId = "foobar");
                currentUserFactory = new CurrentUserSetup.TimecardSupervisorUserFactory();
                supervisorsRepositoryMock.Setup(r => r.GetSuperviseesBySupervisorAsync(It.IsAny<string>())).ReturnsAsync(new string[] { "foobar" });

                var actual = await actualTimecardsService.GetTimecard2Async(inputTimecardId);
                Assert.AreEqual("foobar", actual.EmployeeId);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task SupervisorHasPermissionButDoesNotSuperviseEmployeeTest()
            {
                testRepository.timecardRecords.ForEach(t => t.employeeId = "foobar");

                currentUserFactory = new CurrentUserSetup.TimecardSupervisorUserFactory();
                supervisorsRepositoryMock.Setup(r => r.GetSuperviseesBySupervisorAsync(It.IsAny<string>())).ReturnsAsync(new string[] { "raboof" });
                await actualTimecardsService.GetTimecard2Async(inputTimecardId);
            }
        }

        [TestClass]
        public class CreateTimecard2Tests : TimecardsServiceTests
        {
            public Dtos.TimeManagement.Timecard2 newTimecard;

            [TestInitialize]
            public void Initialize()
            {
                TimecardServiceTestsInitialize();
                newTimecard = new Timecard2()
                {

                    EmployeeId = "0003914",
                    Id = "",
                    PayCycleId = "52",
                    PeriodEndDate = new DateTime(2014, 1, 1),
                    PeriodStartDate = new DateTime(2013, 1, 1),
                    PositionId = "@#",
                    StartDate = new DateTime(2013, 1, 1),
                    EndDate = new DateTime(2013, 1, 7),
                    TimeEntries = new List<Dtos.TimeManagement.TimeEntry2>() 
                    { 
                        new Dtos.TimeManagement.TimeEntry2()
                        {
                            Id = "",                            
                            EarningsTypeId="1",
                            WorkedTime = new TimeSpan(07,04,00),
                            WorkedDate = new DateTime(2016, 4, 1),
                            InDateTime= new DateTime(2016, 4, 1, 7, 0,0),
                            OutDateTime = new DateTime(2016, 4, 1, 15, 0,0),
                            PersonLeaveId ="l",
                            ProjectId = "1l",
                            TimecardId = string.Empty,
                            Timestamp = new Dtos.Base.Timestamp()
                        } 
                    
                    },
                    Timestamp = new Dtos.Base.Timestamp(),
                };
            }

            [TestMethod]
            public async Task NewTimecardIsReturnedTest()
            {
                var expexted = newTimecard;
                var result = await actualTimecardsService.CreateTimecard2Async(newTimecard);
                // Assert.IsTrue(timecardDtoComparer.Equals(expexted, result)); we don't necessarily want to use the DTO comparer because the created timecard will have an idea whereas the original will not
                foreach (var expectedProp in expexted.GetType().GetProperties())
                {
                    foreach (var resultProp in result.GetType().GetProperties())
                    {
                        if (expectedProp.Name != "TimeEntries" // ignore the time entries for now
                            && expectedProp.Name != "Id" // can't compare the id
                            && expectedProp.Name != "Timestamp" // and ignore the timestamp
                            && expectedProp == resultProp)
                        {
                            var name = expectedProp.Name;
                            Assert.AreEqual(expectedProp.GetValue(expexted), resultProp.GetValue(result));
                            break;
                        }
                    }
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task TimecardArgumentRequiredTest()
            {
                await actualTimecardsService.CreateTimecard2Async(null);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task UserCannotUpdateOthersTimecardTest()
            {
                newTimecard.EmployeeId = "Javer";
                await actualTimecardsService.CreateTimecard2Async(newTimecard);
            }

            [TestMethod]
            [ExpectedException(typeof(ExistingResourceException))]
            public async Task ExistingTimecardCannotBeRecreatedTest()
            {
                var tcToUpdate = timecard2EntityToDtoAdapter.MapToType(await testRepository.GetTimecard2Async(testRepository.timecardRecords[0].id));
                await actualTimecardsService.CreateTimecard2Async(tcToUpdate);
            }

        }

        [TestClass]
        public class UpdateTimecard2Tests : TimecardsServiceTests
        {
            [TestInitialize]
            public void Initialize()
            {
                TimecardServiceTestsInitialize();
            }

            [TestMethod]
            public async Task CorrectTimecardReturned()
            {
                var expected = timecard2EntityToDtoAdapter.MapToType(await testRepository.GetTimecard2Async(testRepository.timecardRecords[0].id));
                var actual = await actualTimecardsService.UpdateTimecard2Async(expected);
                Assert.IsTrue(timecard2DtoComparer.Equals(expected, actual));
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task TimecardArgumentRequiredTest()
            {
                await actualTimecardsService.UpdateTimecard2Async(null);
            }

            [TestMethod]
            [ExpectedException(typeof(PermissionsException))]
            public async Task UserCannotUpdateOthersTimecardTest()
            {
                var tcToUpdate = timecard2EntityToDtoAdapter.MapToType(await testRepository.GetTimecard2Async(testRepository.timecardRecords[0].id));
                tcToUpdate.EmployeeId = "Javer";
                await actualTimecardsService.UpdateTimecard2Async(tcToUpdate);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task NoExistingTimecardThrowsErrorTest()
            {
                timecardsRepositoryMock.Setup(m => m.GetTimecard2Async(It.IsAny<string>())).ReturnsAsync(null);

                var tcToUpdate = timecard2EntityToDtoAdapter.MapToType(await testRepository.GetTimecard2Async(testRepository.timecardRecords[0].id));
                await actualTimecardsService.UpdateTimecard2Async(tcToUpdate);
            }
        }
        #endregion

        #region HELPERS

        private FunctionEqualityComparer<Timecard> TimecardComparer()
        {
            return new FunctionEqualityComparer<Timecard>(
                (tc1, tc2) =>
                    tc1.Id == tc2.Id &&
                    tc1.EmployeeId == tc2.EmployeeId &&
                    tc1.PayCycleId == tc2.PayCycleId &&
                    tc1.PositionId == tc2.PositionId &&
                    tc1.PeriodEndDate == tc2.PeriodEndDate &&
                    tc1.PeriodStartDate == tc2.PeriodStartDate &&
                    tc1.Timestamp.AddDateTime == tc2.Timestamp.AddDateTime &&
                    tc1.Timestamp.ChangeDateTime == tc2.Timestamp.ChangeDateTime &&
                    tc1.Timestamp.AddOperator == tc2.Timestamp.AddOperator &&
                    tc1.Timestamp.ChangeOperator == tc2.Timestamp.ChangeOperator &&
                    this.timeEntryEqual(tc1, tc2),
                    (tc) => tc.Id.GetHashCode()
                );
        }

        private FunctionEqualityComparer<Dtos.TimeManagement.TimeEntry> TimeEntryComparer()
        {
            return new FunctionEqualityComparer<Dtos.TimeManagement.TimeEntry>(
                (te1, te2) =>
                    te1.Id == te2.Id &&
                    te1.EarningsTypeId == te2.EarningsTypeId &&
                    te1.InDateTime == te2.InDateTime &&
                    te1.PersonLeaveId == te2.PersonLeaveId &&
                    te1.OutDateTime == te2.OutDateTime &&
                    te1.ProjectId == te2.ProjectId &&
                    te1.TimecardId == te2.TimecardId &&
                    te1.Timestamp.AddDateTime == te2.Timestamp.AddDateTime &&
                    te1.Timestamp.ChangeDateTime == te2.Timestamp.ChangeDateTime &&
                    te1.Timestamp.AddOperator == te2.Timestamp.AddOperator &&
                    te1.Timestamp.ChangeOperator == te2.Timestamp.ChangeOperator &&
                    te1.WorkedTime == te2.WorkedTime &&
                    te1.WorkedDate == te2.WorkedDate,
                    (te) => te.Id.GetHashCode()//(te) => te.Id.GetHashCode()
                );
        }
        private bool timeEntryEqual(Timecard a, Timecard b)
        {
            CollectionAssert.AreEqual(a.TimeEntries, b.TimeEntries, TimeEntryComparer());
            return true;
        }

        private FunctionEqualityComparer<Timecard2> Timecard2Comparer()
        {
            return new FunctionEqualityComparer<Timecard2>(
                (tc1, tc2) =>
                    tc1.Id == tc2.Id &&
                    tc1.EmployeeId == tc2.EmployeeId &&
                    tc1.PayCycleId == tc2.PayCycleId &&
                    tc1.PositionId == tc2.PositionId &&
                    tc1.PeriodEndDate == tc2.PeriodEndDate &&
                    tc1.PeriodStartDate == tc2.PeriodStartDate &&
                    tc1.Timestamp.AddDateTime == tc2.Timestamp.AddDateTime &&
                    tc1.Timestamp.ChangeDateTime == tc2.Timestamp.ChangeDateTime &&
                    tc1.Timestamp.AddOperator == tc2.Timestamp.AddOperator &&
                    tc1.Timestamp.ChangeOperator == tc2.Timestamp.ChangeOperator &&
                    this.timeEntry2Equal(tc1, tc2),
                    (tc) => tc.Id.GetHashCode()
                );
        }

        private FunctionEqualityComparer<Dtos.TimeManagement.TimeEntry2> TimeEntry2Comparer()
        {
            return new FunctionEqualityComparer<Dtos.TimeManagement.TimeEntry2>(
                (te1, te2) =>
                    te1.Id == te2.Id &&
                    te1.EarningsTypeId == te2.EarningsTypeId &&
                    te1.InDateTime == te2.InDateTime &&
                    te1.PersonLeaveId == te2.PersonLeaveId &&
                    te1.OutDateTime == te2.OutDateTime &&
                    te1.ProjectId == te2.ProjectId &&
                    te1.TimecardId == te2.TimecardId &&
                    te1.Timestamp.AddDateTime == te2.Timestamp.AddDateTime &&
                    te1.Timestamp.ChangeDateTime == te2.Timestamp.ChangeDateTime &&
                    te1.Timestamp.AddOperator == te2.Timestamp.AddOperator &&
                    te1.Timestamp.ChangeOperator == te2.Timestamp.ChangeOperator &&
                    te1.WorkedTime == te2.WorkedTime &&
                    te1.WorkedDate == te2.WorkedDate,
                    (te) => te.Id.GetHashCode()//(te) => te.Id.GetHashCode()
                );
        }
        private bool timeEntry2Equal(Timecard2 a, Timecard2 b)
        {
            CollectionAssert.AreEqual(a.TimeEntries, b.TimeEntries, TimeEntry2Comparer());
            return true;
        }


        public class TimecardUserFactory : ICurrentUserFactory
        {
            public ICurrentUser CurrentUser
            {
                get
                {
                    return new CurrentUser(new Claims()
                    {
                        ControlId = "123",
                        Name = "Johnny",
                        PersonId = "0000895",
                        SecurityToken = "321",
                        SessionTimeout = 30,
                        UserName = "Student",
                        Roles = new List<string>() { },
                        SessionFixationId = "abc123",
                    });
                }
            }
        }

        #endregion
    }
}
