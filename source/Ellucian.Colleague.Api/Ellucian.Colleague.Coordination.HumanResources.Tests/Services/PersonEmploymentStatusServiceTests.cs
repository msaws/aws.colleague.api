﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Coordination.HumanResources.Services;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Domain.HumanResources.Tests;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.TestUtil;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.HumanResources.Tests.Services
{
    [TestClass]
    public class PersonEmploymentStatusServiceTests : HumanResourcesServiceTestsSetup
    {
        public Mock<IPersonEmploymentStatusRepository> personEmploymentStatusRepositoryMock;

        public Mock<ISupervisorsRepository> supervisorsRepositoryMock;

        public TestPersonEmploymentStatusRepository testPersonEmploymentStatusRepository;

        public ITypeAdapter<Domain.HumanResources.Entities.PersonEmploymentStatus, Dtos.HumanResources.PersonEmploymentStatus> personEmploymentStatusEntityToDtoAdapter;

        protected Domain.Entities.Role timeApprovalRole;
        private Domain.Entities.Permission timeEntryApprovalPermission;

        public PersonEmploymentStatusService actualService
        {
            get
            {
                return new PersonEmploymentStatusService(
                    personEmploymentStatusRepositoryMock.Object,
                    supervisorsRepositoryMock.Object,
                    adapterRegistryMock.Object,
                    employeeCurrentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object);
            }
        }

        public FunctionEqualityComparer<PersonEmploymentStatus> personEmploymentStatusDtoComparer;

        public void PersonEmploymentStatusServiceTestsInitialize()
        {
            MockInitialize();

            supervisorsRepositoryMock = new Mock<ISupervisorsRepository>();
            personEmploymentStatusRepositoryMock = new Mock<IPersonEmploymentStatusRepository>();
            testPersonEmploymentStatusRepository = new TestPersonEmploymentStatusRepository();

            personEmploymentStatusEntityToDtoAdapter = new AutoMapperAdapter<Domain.HumanResources.Entities.PersonEmploymentStatus, PersonEmploymentStatus>(adapterRegistryMock.Object, loggerMock.Object);

            personEmploymentStatusRepositoryMock.Setup(r => r.GetPersonEmploymentStatusesAsync(It.IsAny<IEnumerable<string>>()))
                .Returns<IEnumerable<string>>((personIds) => testPersonEmploymentStatusRepository.GetPersonEmploymentStatusesAsync(personIds));

            adapterRegistryMock.Setup(r => r.GetAdapter<Domain.HumanResources.Entities.PersonEmploymentStatus, PersonEmploymentStatus>())
                .Returns(personEmploymentStatusEntityToDtoAdapter);

            // permissions mock
            timeApprovalRole = new Domain.Entities.Role(76, "TIME MANAGEMENT SUPERVISOR");
            timeEntryApprovalPermission = new Ellucian.Colleague.Domain.Entities.Permission("APPROVE.REJECT.TIME.ENTRY");
            timeApprovalRole.AddPermission(timeEntryApprovalPermission);
            roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { timeApprovalRole });

            personEmploymentStatusDtoComparer = new FunctionEqualityComparer<PersonEmploymentStatus>(
                (p1, p2) => 
                    p1.Id == p2.Id && 
                    p1.PersonId == p2.PersonId && 
                    p1.PersonPositionId == p2.PersonPositionId && 
                    p1.PrimaryPositionId == p2.PrimaryPositionId && 
                    p1.StartDate == p2.StartDate &&
                    p1.EndDate == p2.EndDate,
                (p) => p.Id.GetHashCode());
        }

        [TestClass]
        public class GetPersonEmploymentStatussTests : PersonEmploymentStatusServiceTests
        {
            [TestInitialize]
            public void Initialize()
            {
                PersonEmploymentStatusServiceTestsInitialize();
            }

            [TestMethod]
            public async Task RepositoryCalledWithCurrentUserIdTest()
            {
                await actualService.GetPersonEmploymentStatusesAsync();
                personEmploymentStatusRepositoryMock.Verify(r =>
                    r.GetPersonEmploymentStatusesAsync(It.Is<IEnumerable<string>>(list =>
                        list.Count() == 1 && list.ElementAt(0) == employeeCurrentUserFactory.CurrentUser.PersonId)));
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task RepositoryReturnsNullTest()
            {
                personEmploymentStatusRepositoryMock.Setup(r => r.GetPersonEmploymentStatusesAsync(It.IsAny<IEnumerable<string>>()))
                    .Returns<IEnumerable<string>>((ids) => Task.FromResult<IEnumerable<Domain.HumanResources.Entities.PersonEmploymentStatus>>(null));

                try
                {
                    await actualService.GetPersonEmploymentStatusesAsync();
                }
                catch (Exception)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            public async Task ExpectedEqualsActualTest()
            {
                var expected = (await testPersonEmploymentStatusRepository.GetPersonEmploymentStatusesAsync(new List<string>() { employeeCurrentUserFactory.CurrentUser.PersonId }))
                    .Select(ppEntity => personEmploymentStatusEntityToDtoAdapter.MapToType(ppEntity));

                var actual = await actualService.GetPersonEmploymentStatusesAsync();

                CollectionAssert.AreEqual(expected.ToArray(), actual.ToArray(), personEmploymentStatusDtoComparer);
            }
        }
    }
}
