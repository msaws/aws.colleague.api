﻿//Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Coordination.HumanResources.Services;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.HumanResources;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.HumanResources.Tests.Services
{
    [TestClass]
    public class InstitutionJobServiceTests
    {
        [TestClass]
        public class InstitutionJobServiceTests_GET: CurrentUserSetup
        {
            Mock<IPositionRepository> positionRepositoryMock;
            Mock<IInstitutionJobsRepository> institutionJobRepositoryMock;
            Mock<IHumanResourcesReferenceDataRepository> hrReferenceDataRepositoryMock;
            Mock<IPersonRepository> personRepositoryMock;
            Mock<IEmployeeRepository> employeeRepositoryMock;
            Mock<IAdapterRegistry> adapterRegistryMock;
            Mock<IReferenceDataRepository> referenceDataRepositoryMock;
            ICurrentUserFactory currentUserFactory;
            Mock<IRoleRepository> roleRepositoryMock;
            Mock<ILogger> loggerMock;

            InstitutionJobsService institutionJobService;
            IEnumerable<Domain.HumanResources.Entities.InstitutionJobs> institutionJobEntities;
            Tuple<IEnumerable<Domain.HumanResources.Entities.InstitutionJobs>, int> institutionJobEntityTuple;

            //IEnumerable<Domain.Base.Entities.Person> personEntities;
            private IConfigurationRepository baseConfigurationRepository;
            private Mock<IConfigurationRepository> baseConfigurationRepositoryMock;

            IEnumerable<Domain.HumanResources.Entities.EmploymentClassification> employmentClassificationEntities;
            IEnumerable<Domain.HumanResources.Entities.JobChangeReason> jobChangeReasonEntities;
            //IEnumerable<Domain.ColleagueFinance.Entities.AccountsPayableSources> acctPaySourceEntities;
            //IEnumerable<Domain.ColleagueFinance.Entities.CurrencyConversion> currencyConversionEntities;
            //IEnumerable<Domain.Base.Entities.Institution> institutionsEntities;
            //IEnumerable<Domain.HumanResources.Entities.PositionPay> positionPayEntities;

            private Domain.Entities.Permission permissionViewAnyPerson;

            int offset = 0;
            int limit = 4;

            [TestInitialize]
            public void Initialize() 
            {
                positionRepositoryMock = new Mock<IPositionRepository>();
                institutionJobRepositoryMock = new Mock<IInstitutionJobsRepository>();
                hrReferenceDataRepositoryMock = new Mock<IHumanResourcesReferenceDataRepository>();
                personRepositoryMock = new Mock<IPersonRepository>();
                referenceDataRepositoryMock = new Mock<IReferenceDataRepository>();
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                roleRepositoryMock = new Mock<IRoleRepository>();
                loggerMock = new Mock<ILogger>();

                baseConfigurationRepositoryMock = new Mock<IConfigurationRepository>();
                baseConfigurationRepository = baseConfigurationRepositoryMock.Object;

                BuildData();
                // Set up current user
                currentUserFactory = new CurrentUserSetup.PersonUserFactory();

                // Mock permissions
                permissionViewAnyPerson = new Ellucian.Colleague.Domain.Entities.Permission(HumanResourcesPermissionCodes.ViewInstitutionJob);
                personRole.AddPermission(permissionViewAnyPerson);
                roleRepositoryMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { personRole });

                institutionJobService = new InstitutionJobsService(positionRepositoryMock.Object, hrReferenceDataRepositoryMock.Object, referenceDataRepositoryMock.Object, personRepositoryMock.Object, institutionJobRepositoryMock.Object,
                                               baseConfigurationRepository, adapterRegistryMock.Object, currentUserFactory, roleRepositoryMock.Object, loggerMock.Object);
            }

            [TestCleanup]
            public void Cleanup() 
            {
                institutionJobEntityTuple = null;
                institutionJobEntities = null;
                employmentClassificationEntities = null;
                jobChangeReasonEntities = null;
                institutionJobRepositoryMock = null;
                hrReferenceDataRepositoryMock = null;
                adapterRegistryMock = null;
                currentUserFactory = null;
                roleRepositoryMock = null;
                loggerMock = null;
                referenceDataRepositoryMock = null;
            }

            [TestMethod]
            public async Task InstitutionJobs_GETAllAsync()
            {
                var actualsTuple =
                    await
                        institutionJobService.GetInstitutionJobsAsync(offset, limit, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>());

                Assert.IsNotNull(actualsTuple);

                int count = actualsTuple.Item1.Count();

                for (int i = 0; i < count; i++)
                {
                    var expected = institutionJobEntities.ToList()[i];
                    var actual = actualsTuple.Item1.ToList()[i];

                    Assert.IsNotNull(actual);

                    Assert.AreEqual(expected.Guid, actual.Id);
                }
            }

            [TestMethod]
            public async Task InstitutionJobs_GETAllFilterAsync()
            {
                string personId = "0000011";
                personRepositoryMock.Setup(i => i.GetPersonIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync(personId);
                string positionId = "x";
                positionRepositoryMock.Setup(i => i.GetPositionIdFromGuidAsync(It.IsAny<string>())).ReturnsAsync(positionId);

                var actualsTuple =
                    await
                        institutionJobService.GetInstitutionJobsAsync(offset, limit, "cd385d31-75ed-4d93-9a1b-4776a951396d",
                        It.IsAny<string>(), "fadbb5f0-e39d-4b1e-82c9-77617ee2164c", "Math", "2000-01-01 00:00:00.000",
                        "2020-12-31 00:00:00.000", "active", It.IsAny<string>(), "primary", It.IsAny<bool>());

                Assert.IsNotNull(actualsTuple);

                int count = actualsTuple.Item1.Count();

                for (int i = 0; i < count; i++)
                {
                    var expected = institutionJobEntities.ToList()[i];
                    var actual = actualsTuple.Item1.ToList()[i];

                    Assert.IsNotNull(actual);

                    Assert.AreEqual(expected.Guid, actual.Id);
                }
            }

            [TestMethod]
            public async Task InstitutionJobs_GETAllAsync_EmptyTuple()
            {
                institutionJobEntities = new List<Domain.HumanResources.Entities.InstitutionJobs>()
                {

                };
                institutionJobEntityTuple = new Tuple<IEnumerable<Domain.HumanResources.Entities.InstitutionJobs>, int>(institutionJobEntities, 0);
                institutionJobRepositoryMock.Setup(i => i.GetInstitutionJobsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(institutionJobEntityTuple);
                var actualsTuple = await institutionJobService.GetInstitutionJobsAsync(offset, limit, It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>());

                Assert.AreEqual(0, actualsTuple.Item1.Count());
            }

            [TestMethod]
            public async Task InstitutionJobs_GET_ById()
            {
                var id = "ce4d68f6-257d-4052-92c8-17eed0f088fa";
                var expected = institutionJobEntities.ToList()[0];
                institutionJobRepositoryMock.Setup(i => i.GetInstitutionJobsByGuidAsync(id)).ReturnsAsync(expected);
                var actual = await institutionJobService.GetInstitutionJobsByGuidAsync(id);

                Assert.IsNotNull(actual);

                Assert.AreEqual(expected.Guid, actual.Id);
            }       

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task InstitutionJobs_GET_ById_NullId_ArgumentNullException()
            {
                var actual = await institutionJobService.GetInstitutionJobsByGuidAsync(string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task InstitutionJobs_GET_ById_ReturnsNullEntity_KeyNotFoundException()
            {
                var id = "ce4d68f6-257d-4052-92c8-17eed0f088fa";
                institutionJobRepositoryMock.Setup(i => i.GetInstitutionJobsByGuidAsync(id)).Throws<KeyNotFoundException>();
                var actual = await institutionJobService.GetInstitutionJobsByGuidAsync(id);
            }

            [TestMethod]
            [ExpectedException(typeof(InvalidOperationException))]
            public async Task InstitutionJobs_GET_ById_ReturnsNullEntity_InvalidOperationException()
            {
                var id = "ce4d68f6-257d-4052-92c8-17eed0f088fa";
                institutionJobRepositoryMock.Setup(i => i.GetInstitutionJobsByGuidAsync(id)).Throws<InvalidOperationException>();
                var actual = await institutionJobService.GetInstitutionJobsByGuidAsync(id);
            }

            [TestMethod]
            [ExpectedException(typeof(RepositoryException))]
            public async Task InstitutionJobs_GET_ById_ReturnsNullEntity_RepositoryException()
            {
                var id = "ce4d68f6-257d-4052-92c8-17eed0f088fa";
                institutionJobRepositoryMock.Setup(i => i.GetInstitutionJobsByGuidAsync(id)).Throws<RepositoryException>();
                var actual = await institutionJobService.GetInstitutionJobsByGuidAsync(id);
            }

            [TestMethod]
            [ExpectedException(typeof(Exception))]
            public async Task InstitutionJobs_GET_ById_ReturnsNullEntity_Exception()
            {
                var id = "ce4d68f6-257d-4052-92c8-17eed0f088fa";
                institutionJobRepositoryMock.Setup(i => i.GetInstitutionJobsByGuidAsync(id)).Throws<Exception>();
                var actual = await institutionJobService.GetInstitutionJobsByGuidAsync(id);
            }

            private void BuildData()
            {
                jobChangeReasonEntities = new List<Domain.HumanResources.Entities.JobChangeReason>() 
                {
                    new Domain.HumanResources.Entities.JobChangeReason("d4ff9cf9-3300-4dca-b52e-59c905021893", "Admissions", "Admissions"),
                    new Domain.HumanResources.Entities.JobChangeReason("161b17b2-5b8b-482b-8ff3-2454323aa8e6", "Agriculture Business", "Agriculture Business"),
                    new Domain.HumanResources.Entities.JobChangeReason("5f8aeedd-8102-4d8f-8dbc-ecd32c374e87", "Agriculture Mechanics", "Agriculture Mechanics"),
                    new Domain.HumanResources.Entities.JobChangeReason("ba66205d-79a8-4244-95f9-d2770a129a97", "Animal Science", "Animal Science"),
                    new Domain.HumanResources.Entities.JobChangeReason("ccce9689-aab1-47ab-ae76-fa128fe8b97e", "Anthropology", "Anthropology"),
                };
                hrReferenceDataRepositoryMock.Setup(i => i.GetJobChangeReasonsAsync(It.IsAny<bool>())).ReturnsAsync(jobChangeReasonEntities);                

                employmentClassificationEntities = new List<Domain.HumanResources.Entities.EmploymentClassification>() 
                {
                    new Domain.HumanResources.Entities.EmploymentClassification("c1b91008-ba77-4b5b-8b77-84f5a7ae1632", "ADJ", "Adjunct Faculty", Domain.HumanResources.Entities.EmploymentClassificationType.Employee),
                    new Domain.HumanResources.Entities.EmploymentClassification("874dee09-8662-47e6-af0d-504c257493a3", "SUP", "Support", Domain.HumanResources.Entities.EmploymentClassificationType.Employee),
                    new Domain.HumanResources.Entities.EmploymentClassification("29391a8c-75e7-41e8-a5ff-5d7f7598b87c", "AS", "Anuj Test", Domain.HumanResources.Entities.EmploymentClassificationType.Position),
                    new Domain.HumanResources.Entities.EmploymentClassification("5b05410c-c94c-464a-98ee-684198bde60b", "ITS", "IT Support", Domain.HumanResources.Entities.EmploymentClassificationType.Position),
                };
                hrReferenceDataRepositoryMock.Setup(i => i.GetEmploymentClassificationsAsync(It.IsAny<bool>())).ReturnsAsync(employmentClassificationEntities);

              
                institutionJobEntities = new List<Domain.HumanResources.Entities.InstitutionJobs>() 
                {
                    new Domain.HumanResources.Entities.InstitutionJobs("ce4d68f6-257d-4052-92c8-17eed0f088fa", "e9e6837f-2c51-431b-9069-4ac4c0da3041", "9ae3a175-1dfd-4937-b97b-3c9ad596e023", "bfea651b-8e27-4fcd-abe3-04573443c04c", DateTime.Now)
                    { 
                        Employer = "ID",
                        Department = "5b05410c-c94c-464a-98ee-684198bde60b",
                        EndDate = DateTime.Now,
                        EndReason = "Admissions",
                        AccountingStrings = new List<string>() 
                        {
                            "accounting_string",
                        },
                        PayStatus = Domain.HumanResources.Entities.PayStatus.WithPay,
                        BenefitsStatus = Domain.HumanResources.Entities.BenefitsStatus.WithBenefits,
                        CycleWorkTimeUnits = "HRS",
                        CycleWorkTimeAmount = new decimal(40.0),
                        FullTimeEquivalent = new decimal(40.0),
                        YearWorkTimeUnits = "HRS",
                        YearWorkTimeAmount = new decimal(1600.0),
                        SupervisorId = "supId",
                        AlternateSupervisorId = "altSupId",
                        PayRate = "40000",
                        Grade = "grade",
                        Step = "step",
                        Classification = "ADJ",
                        Primary = true,
                        HostCountry = "USA"

                    },
                    new Domain.HumanResources.Entities.InstitutionJobs("5bc2d86c-6a0c-46b1-824d-485ccb27dc67", "9ae3a175-1dfd-4937-b97b-3c9ad596e023", "e9e6837f-2c51-431b-9069-4ac4c0da3041", "g5u4827d-1a54-232b-9239-5ac4f6dt3257", DateTime.Now)
                    {
                        Employer = "ID",
                        Department = "5b05410c-c94c-464a-98ee-684198bde60b"
                    },
                    new Domain.HumanResources.Entities.InstitutionJobs("7ea5142f-12f1-4ac9-b9f3-73e4205dfc11", "e9e6837f-2c51-431b-9069-4ac4c0da3041", "g5u4827d-1a54-232b-9239-5ac4f6dt3257", "bfea651b-8e27-4fcd-abe3-04573443c04c", DateTime.Now)
                    {
                        Employer = "ID",
                        Department = "5b05410c-c94c-464a-98ee-684198bde60b"   
                    },
                    new Domain.HumanResources.Entities.InstitutionJobs("db8f690b-071f-4d98-8da8-d4312511a4c1", "bfea651b-8e27-4fcd-abe3-04573443c04c", "g5u4827d-1a54-232b-9239-5ac4f6dt3257", "9ae3a175-1dfd-4937-b97b-3c9ad596e023", DateTime.Now)
                    {
                        Employer = "ID",
                        Department = "5b05410c-c94c-464a-98ee-684198bde60b"
                    }
                };
                institutionJobEntityTuple = new Tuple<IEnumerable<Domain.HumanResources.Entities.InstitutionJobs>, int>(institutionJobEntities, institutionJobEntities.Count());
                institutionJobRepositoryMock.Setup(i => i.GetInstitutionJobsAsync(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>(), It.IsAny<bool>())).ReturnsAsync(institutionJobEntityTuple);
                institutionJobRepositoryMock.Setup(i => i.GetInstitutionJobsByGuidAsync(It.IsAny<string>())).ReturnsAsync(institutionJobEntities.ToList()[0]);
                personRepositoryMock.Setup(i => i.GetPersonGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("db8f690b-071f-4d98-8da8-d4312511a4c2");
                positionRepositoryMock.Setup(i => i.GetPositionGuidFromIdAsync(It.IsAny<string>())).ReturnsAsync("db8f690b-071f-4d98-8da8-d4312511a4c2");

               
            }
        }
    }
}
