﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Coordination.HumanResources.Services;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Domain.HumanResources.Tests;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.TestUtil;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.HumanResources.Tests.Services
{
    [TestClass]
    public class PayCycleServiceTests : HumanResourcesServiceTestsSetup
    {
        public Mock<IPayCycleRepository> payCycleRepositoryMock;

        public TestPayCycleRepository testPayCycleRepository;

        public ITypeAdapter<Domain.HumanResources.Entities.PayCycle, Dtos.HumanResources.PayCycle> payCycleEntityToDtoAdapter;

        public PayCycleService actualService
        {
            get
            {
                return new PayCycleService(
                    payCycleRepositoryMock.Object,
                    adapterRegistryMock.Object,
                    employeeCurrentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object);
            }
        }

        public FunctionEqualityComparer<PayCycle> payCycleDtoComparer;

        public void PersonEmploymentStatusServiceTestsInitialize()
        {
            MockInitialize();

            payCycleRepositoryMock = new Mock<IPayCycleRepository>();
            testPayCycleRepository = new TestPayCycleRepository();

            payCycleEntityToDtoAdapter = new AutoMapperAdapter<Domain.HumanResources.Entities.PayCycle, PayCycle>(adapterRegistryMock.Object, loggerMock.Object);

            payCycleRepositoryMock.Setup(r => r.GetPayCyclesAsync())
                .Returns<IEnumerable<string>>((p) => testPayCycleRepository.GetPayCyclesAsync());

            adapterRegistryMock.Setup(r => r.GetAdapter<Domain.HumanResources.Entities.PayCycle, PayCycle>())
                .Returns(payCycleEntityToDtoAdapter);

            payCycleDtoComparer = new FunctionEqualityComparer<PayCycle>(
                (p1, p2) =>
                    p1.Id == p2.Id &&
                    p1.AnnualPayFrequency == p2.AnnualPayFrequency &&
                    p1.Description == p2.Description &&
                    p1.PayClassIds == p2.PayClassIds &&
                    p1.PayPeriods == p2.PayPeriods,
                (p) => p.Id.GetHashCode());
        }

        [TestClass]
        public class GetPersonEmploymentStatussTests : PersonEmploymentStatusServiceTests
        {
            [TestInitialize]
            public void Initialize()
            {
                PersonEmploymentStatusServiceTestsInitialize();
            }

            [TestMethod]
            public async Task RepositoryCalledWithCurrentUserIdTest()
            {
                await actualService.GetPersonEmploymentStatusesAsync();
                personEmploymentStatusRepositoryMock.Verify(r =>
                    r.GetPersonEmploymentStatusesAsync(It.Is<IEnumerable<string>>(list =>
                        list.Count() == 1 && list.ElementAt(0) == employeeCurrentUserFactory.CurrentUser.PersonId)));
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task RepositoryReturnsNullTest()
            {
                personEmploymentStatusRepositoryMock.Setup(r => r.GetPersonEmploymentStatusesAsync(It.IsAny<IEnumerable<string>>()))
                    .Returns<IEnumerable<string>>((ids) => Task.FromResult<IEnumerable<Domain.HumanResources.Entities.PersonEmploymentStatus>>(null));

                try
                {
                    await actualService.GetPersonEmploymentStatusesAsync();
                }
                catch (Exception)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            public async Task ExpectedEqualsActualTest()
            {
                var expected = (await testPersonEmploymentStatusRepository.GetPersonEmploymentStatusesAsync(new List<string>() { employeeCurrentUserFactory.CurrentUser.PersonId }))
                    .Select(ppEntity => personEmploymentStatusEntityToDtoAdapter.MapToType(ppEntity));

                var actual = await actualService.GetPersonEmploymentStatusesAsync();

                CollectionAssert.AreEqual(expected.ToArray(), actual.ToArray(), personEmploymentStatusDtoComparer);
            }
        }
    }
}
