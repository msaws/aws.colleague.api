﻿/* Copyright 2016 Ellucian Company L.P. and its affiliates. */
using Ellucian.Colleague.Coordination.HumanResources.Adapters;
using Ellucian.Colleague.Coordination.HumanResources.Services;
using Ellucian.Colleague.Domain.HumanResources.Repositories;
using Ellucian.Colleague.Domain.HumanResources.Tests;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Adapters;
using Ellucian.Web.Http.TestUtil;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.HumanResources.Tests.Services
{
    [TestClass]
    public class PersonPositionWageServiceTests : HumanResourcesServiceTestsSetup
    {
        public Mock<IPersonPositionWageRepository> personPositionWageRepositoryMock;
        public Mock<ISupervisorsRepository> supervisorsRepositoryMock;
        public TestPersonPositionWageRepository testPersonPositionWageRepository;

        public PersonPositionWageEntityToDtoAdapter personPositionWageEntityToDtoAdapter;

        public PersonPositionWageService actualService
        {
            get
            {
                return new PersonPositionWageService(
                    personPositionWageRepositoryMock.Object,
                    supervisorsRepositoryMock.Object,
                    adapterRegistryMock.Object,
                    employeeCurrentUserFactory,
                    roleRepositoryMock.Object,
                    loggerMock.Object);
            }
        }

        public FunctionEqualityComparer<PersonPositionWage> personPositionWageDtoComparer;

        public void PersonPositionWageServiceTestsInitialize()
        {
            MockInitialize();

            supervisorsRepositoryMock = new Mock<ISupervisorsRepository>();
            personPositionWageRepositoryMock = new Mock<IPersonPositionWageRepository>();
            testPersonPositionWageRepository = new TestPersonPositionWageRepository();

            personPositionWageEntityToDtoAdapter = new PersonPositionWageEntityToDtoAdapter(adapterRegistryMock.Object, loggerMock.Object);

            personPositionWageRepositoryMock.Setup(r => r.GetPersonPositionWagesAsync(It.IsAny<IEnumerable<string>>()))
                .Returns<IEnumerable<string>>((ids) => testPersonPositionWageRepository.GetPersonPositionWagesAsync(ids));

            adapterRegistryMock.Setup(r => r.GetAdapter<Domain.HumanResources.Entities.PersonPositionWage, Dtos.HumanResources.PersonPositionWage>())
                .Returns(() => (ITypeAdapter<Domain.HumanResources.Entities.PersonPositionWage, Dtos.HumanResources.PersonPositionWage>)personPositionWageEntityToDtoAdapter);

            personPositionWageDtoComparer = new FunctionEqualityComparer<PersonPositionWage>(
                (p1, p2) => p1.Id == p2.Id && p1.PersonId == p2.PersonId && p1.PositionId == p2.PositionId && p1.StartDate == p2.StartDate,
                (p) => p.Id.GetHashCode());
        }

        [TestClass]
        public class GetPersonPositionWagesTests : PersonPositionWageServiceTests
        {
            [TestInitialize]
            public void Initialize()
            {
                PersonPositionWageServiceTestsInitialize();
            }

            [TestMethod]
            public async Task RepositoryCalledWithCurrentUserIdTest()
            {
                await actualService.GetPersonPositionWagesAsync();
                personPositionWageRepositoryMock.Verify(r =>
                    r.GetPersonPositionWagesAsync(It.Is<IEnumerable<string>>(list => list.Count() == 1 && list.ElementAt(0) == employeeCurrentUserFactory.CurrentUser.PersonId)));
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public async Task RepositoryReturnsNullTest()
            {
                personPositionWageRepositoryMock.Setup(r => r.GetPersonPositionWagesAsync(It.IsAny<IEnumerable<string>>()))
                    .Returns<IEnumerable<string>>((ids) => Task.FromResult<IEnumerable<Domain.HumanResources.Entities.PersonPositionWage>>(null));

                try
                {
                    await actualService.GetPersonPositionWagesAsync();
                }
                catch(Exception)
                {
                    loggerMock.Verify(l => l.Error(It.IsAny<string>()));
                    throw;
                }
            }

            [TestMethod]
            public async Task ExpectedEqualsActualTest()
            {
                var expected = (await testPersonPositionWageRepository.GetPersonPositionWagesAsync(new List<string>() { employeeCurrentUserFactory.CurrentUser.PersonId }))
                    .Select(ppEntity => personPositionWageEntityToDtoAdapter.MapToType(ppEntity));

                var actual = await actualService.GetPersonPositionWagesAsync();

                CollectionAssert.AreEqual(expected.ToArray(), actual.ToArray(), personPositionWageDtoComparer);
                    
            }
        }
    }
}
