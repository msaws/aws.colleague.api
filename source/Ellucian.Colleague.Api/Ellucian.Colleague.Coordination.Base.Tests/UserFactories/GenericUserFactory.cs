﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Web.Security;
using System.Collections.Generic;

namespace Ellucian.Colleague.Coordination.Base.Tests.UserFactories
{
    /// <summary>
    /// Define a user factory to simulate the user
    /// </summary>
    public abstract class GenericUserFactory
    {
        public class UserFactory : ICurrentUserFactory
        {
            public ICurrentUser CurrentUser
            {
                get
                {
                    return new CurrentUser(new Claims()
                    {
                        PersonId = "0000001",
                        ControlId = "123",
                        Name = "Johnny",
                        SecurityToken = "321",
                        SessionTimeout = 30,
                        UserName = "Student",
                        Roles = new List<string>() { "Student" },
                        SessionFixationId = "abc123"
                    });
                }
            }
        }
    }
}
