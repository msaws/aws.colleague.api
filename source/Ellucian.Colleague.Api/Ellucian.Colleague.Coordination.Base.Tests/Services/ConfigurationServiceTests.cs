﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Coordination.Base.Tests.UserFactories;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Base.Tests;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;


namespace Ellucian.Colleague.Coordination.Base.Tests.Services
{
    /// <summary>
    /// This class tests that the service returns the various configurations.
    /// </summary>
    [TestClass]
    public class ConfigurationServiceTests : GenericUserFactory
    {
        #region Initialize and Cleanup
        private ConfigurationService configurationService = null;
        private ConfigurationService configurationServiceToReturnNull = null;
        private TestConfigurationRepository testConfigurationRepository = null;
        private ICurrentUserFactory currentUserFactory;

        // Mock/fake objects to construct ConfigurationService
        private Mock<IConfigurationRepository> configRepoMock;
        private IConfigurationRepository configRepo;
        private Mock<IAdapterRegistry> adapterRegistryMock;
        private IAdapterRegistry adapterRegistry;
        private ILogger logger;
        private Mock<IRoleRepository> roleRepoMock;
        private IRoleRepository roleRepo;
        private ICurrentUserFactory currentUserFactoryFake;

        [TestInitialize]
        public void Initialize()
        {
            // Instantiate mock and fake objects used to construct the service
            configRepoMock = new Mock<IConfigurationRepository>();
            configRepo = configRepoMock.Object;

            adapterRegistryMock = new Mock<IAdapterRegistry>();
            adapterRegistry = adapterRegistryMock.Object;
            logger = new Mock<ILogger>().Object;
            roleRepoMock = new Mock<IRoleRepository>();
            roleRepo = roleRepoMock.Object;
            currentUserFactoryFake = new Person001UserFactory();

            // Mock the adapter registry to use the automappers between the EmergencyInformation domain entity and dto. 
            var emptyAdapterRegistryMock = new Mock<IAdapterRegistry>(); // An empty mock adapter registry to instantiate AutoMapperAdapter

            // Instantiate the service
            configurationService = new ConfigurationService(configRepoMock.Object, adapterRegistry, currentUserFactoryFake, roleRepo, logger);
            BuildConfigurationService();
        }

        [TestCleanup]
        public void Cleanup()
        {
            // Reset the services and repository variables.
            configRepo = null;
            adapterRegistry = null;
            logger = null;
            roleRepo = null;
            currentUserFactoryFake = null;
            configurationService = null;
            testConfigurationRepository = null;
            currentUserFactory = null;
        }
        #endregion

        #region GetTaxFormConsentConfigurationAsync tests
        [TestMethod]
        public async Task GetTaxFormConsentConfigurationAsync_W2_Success()
        {
            var configurationDto = await this.configurationService.GetTaxFormConsentConfigurationAsync(Dtos.Base.TaxForms.FormW2);
            var configurationDomainEntity = await testConfigurationRepository.GetTaxFormConsentConfigurationAsync(Domain.Base.Entities.TaxForms.FormW2);

            Assert.AreEqual(Dtos.Base.TaxForms.FormW2, configurationDto.TaxFormId);
            Assert.AreEqual(configurationDomainEntity.ConsentParagraphs.ConsentText, configurationDto.ConsentText);
            Assert.AreEqual(configurationDomainEntity.ConsentParagraphs.ConsentWithheldText, configurationDto.ConsentWithheldText);
        }

        [TestMethod]
        public async Task GetTaxFormConsentConfigurationAsync_1095_Success()
        {
            var configurationDto = await this.configurationService.GetTaxFormConsentConfigurationAsync(Dtos.Base.TaxForms.Form1095C);
            var configurationDomainEntity = await testConfigurationRepository.GetTaxFormConsentConfigurationAsync(Domain.Base.Entities.TaxForms.Form1095C);

            Assert.AreEqual(Dtos.Base.TaxForms.Form1095C, configurationDto.TaxFormId);
            Assert.AreEqual(configurationDomainEntity.ConsentParagraphs.ConsentText, configurationDto.ConsentText);
            Assert.AreEqual(configurationDomainEntity.ConsentParagraphs.ConsentWithheldText, configurationDto.ConsentWithheldText);
        }

        [TestMethod]
        public async Task GetTaxFormConsentConfigurationAsync_1098_Success()
        {
            var configurationDto = await this.configurationService.GetTaxFormConsentConfigurationAsync(Dtos.Base.TaxForms.Form1098);
            var configurationDomainEntity = await testConfigurationRepository.GetTaxFormConsentConfigurationAsync(Domain.Base.Entities.TaxForms.Form1098);

            Assert.AreEqual(Dtos.Base.TaxForms.Form1098, configurationDto.TaxFormId);
            Assert.AreEqual(configurationDomainEntity.ConsentParagraphs.ConsentText, configurationDto.ConsentText);
            Assert.AreEqual(configurationDomainEntity.ConsentParagraphs.ConsentWithheldText, configurationDto.ConsentWithheldText);
        }

        [TestMethod]
        public async Task GetTaxFormConsentConfigurationAsync_1098_NullConfigurationReturned()
        {
            var expectedParam = "configuration";
            var actualParam = "";
            try
            {
                await this.configurationServiceToReturnNull.GetTaxFormConsentConfigurationAsync(Dtos.Base.TaxForms.Form1095C);
            }
            catch (ArgumentNullException anex)
            {
                actualParam = anex.ParamName;
            }

            Assert.AreEqual(expectedParam, actualParam);
        }
        #endregion

        [TestMethod]
        public async Task GetUserProfileConfigurationAsync_Success()
        {
            var upcDto = await this.configurationService.GetUserProfileConfigurationAsync();
            var upcEntity = await testConfigurationRepository.GetUserProfileConfigurationAsync();

            Assert.AreEqual(upcEntity.AddressesAreUpdatable, upcDto.AddressesAreUpdatable);
            Assert.AreEqual(upcEntity.AllAddressTypesAreViewable, upcDto.AllAddressTypesAreViewable);
            Assert.AreEqual(upcEntity.AllEmailTypesAreUpdatable, upcDto.AllEmailTypesAreUpdatable);
            Assert.AreEqual(upcEntity.AllEmailTypesAreViewable, upcDto.AllEmailTypesAreViewable);
            Assert.AreEqual(upcEntity.AllPhoneTypesAreViewable, upcDto.AllPhoneTypesAreViewable);
            Assert.AreEqual(upcEntity.CanUpdateAddressWithoutPermission, upcDto.CanUpdateAddressWithoutPermission);
            Assert.AreEqual(upcEntity.CanUpdateEmailWithoutPermission, upcDto.CanUpdateEmailWithoutPermission);
            Assert.AreEqual(upcEntity.CanUpdatePhoneWithoutPermission, upcDto.CanUpdatePhoneWithoutPermission);
            Assert.AreEqual(upcEntity.Text, upcDto.Text);
            CollectionAssert.AreEqual(upcEntity.UpdatableAddressTypes, upcDto.UpdatableAddressTypes);
            CollectionAssert.AreEqual(upcEntity.UpdatableEmailTypes, upcDto.UpdatableEmailTypes);
            CollectionAssert.AreEqual(upcEntity.UpdatablePhoneTypes, upcDto.UpdatablePhoneTypes);
            CollectionAssert.AreEqual(upcEntity.ViewableAddressTypes, upcDto.ViewableAddressTypes);
            CollectionAssert.AreEqual(upcEntity.ViewableEmailTypes, upcDto.ViewableEmailTypes);
            CollectionAssert.AreEqual(upcEntity.ViewablePhoneTypes, upcDto.ViewablePhoneTypes);
        }

        [TestMethod]
        public async Task GetEmergencyInformationConfigurationAsync()
        {
            var eicDto = await this.configurationService.GetEmergencyInformationConfigurationAsync();
            var eicEntity = await testConfigurationRepository.GetEmergencyInformationConfigurationAsync();

            Assert.AreEqual(eicEntity.AllowOptOut, eicDto.AllowOptOut);
            Assert.AreEqual(eicEntity.HideHealthConditions, eicDto.HideHealthConditions);
            Assert.AreEqual(eicEntity.RequireContact, eicDto.RequireContactToConfirm);
        }

        [TestMethod]
        public async Task GetEmergencyInformationConfiguration2Async()
        {
            var eicDto = await this.configurationService.GetEmergencyInformationConfiguration2Async();
            var eicEntity = await testConfigurationRepository.GetEmergencyInformationConfigurationAsync();

            Assert.AreEqual(eicEntity.AllowOptOut, eicDto.AllowOptOut);
            Assert.AreEqual(eicEntity.HideHealthConditions, eicDto.HideHealthConditions);
            Assert.AreEqual(eicEntity.HideOtherInformation, eicDto.HideOtherInformation);
            Assert.AreEqual(eicEntity.RequireContact, eicDto.RequireContact);
        }

        [TestMethod]
        public async Task GetRestrictionConfigurationAsync()
        {
            var rcDto = await this.configurationService.GetRestrictionConfigurationAsync();
            var rcEntity = await testConfigurationRepository.GetRestrictionConfigurationAsync();

            Assert.AreEqual(rcEntity.Mapping.Count, rcDto.Mapping.Count);
            Assert.AreEqual(rcEntity.Mapping[0].SeverityStart, rcDto.Mapping[0].SeverityStart);
            Assert.AreEqual(rcEntity.Mapping[0].SeverityEnd, rcDto.Mapping[0].SeverityEnd);
            Assert.AreEqual(rcEntity.Mapping[0].Style.ToString(), rcDto.Mapping[0].Style.ToString());
        }

        [TestMethod]
        public async Task ConfigurationService_GetPrivacyConfigurationAsync()
        {
            var pcDto = await this.configurationService.GetPrivacyConfigurationAsync();
            var pcEntity = await testConfigurationRepository.GetPrivacyConfigurationAsync();

            Assert.AreEqual(pcEntity.RecordDenialMessage, pcDto.RecordDenialMessage);
        }

        [TestMethod]
        public async Task ConfigurationService_GetOrganizationalRelationshipConfigurationAsync_Success()
        {
            var expectedConfigCount = 1;
            var expectedConfigCode = "MGR";
            var configuration = await configurationService.GetOrganizationalRelationshipConfigurationAsync();
            Assert.AreEqual(expectedConfigCount, configuration.RelationshipTypeCodeMapping.Keys.Count);
            Assert.AreEqual(expectedConfigCode, configuration.RelationshipTypeCodeMapping[Dtos.Base.OrganizationalRelationshipType.Manager][0]);

        }

        /// <summary>
        /// Fake an ICurrentUserFactory implementation to construct ConfigurationService
        /// </summary>
        public class Person001UserFactory : ICurrentUserFactory
        {
            public ICurrentUser CurrentUser
            {
                get
                {
                    return new CurrentUser(new Claims()
                    {
                        // Only the PersonId is part of the test, whether it matches the ID of the person whose 
                        // emergency information is requested. The remaining fields are arbitrary.
                        ControlId = "123",
                        Name = "Fred",
                        PersonId = "0001234",   /* From the test data of the test class */
                        SecurityToken = "321",
                        SessionTimeout = 30,
                        UserName = "Student",
                        Roles = new List<string>() { "Student" },
                        SessionFixationId = "abc123"
                    });
                }
            }
        }

        /// <summary>
        /// Builds a configuration service object.
        /// </summary>
        private void BuildConfigurationService()
        {
            testConfigurationRepository = new TestConfigurationRepository();

            // Set up current user
            currentUserFactory = new GenericUserFactory.UserFactory();

            var roleRepository = new Mock<IRoleRepository>().Object;
            var loggerObject = new Mock<ILogger>().Object;
            var adapterRegistry = new Mock<IAdapterRegistry>();

            var taxFormConfigurationDtoAdapter = new AutoMapperAdapter<Domain.Base.Entities.TaxFormConfiguration, Dtos.Base.TaxFormConfiguration>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Domain.Base.Entities.TaxFormConfiguration, Dtos.Base.TaxFormConfiguration>()).Returns(taxFormConfigurationDtoAdapter);

            var userProfileConfigurationAdapter = new AutoMapperAdapter<Domain.Base.Entities.UserProfileConfiguration, Dtos.Base.UserProfileConfiguration>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Domain.Base.Entities.UserProfileConfiguration, Dtos.Base.UserProfileConfiguration>()).Returns(userProfileConfigurationAdapter);

            var emergencyInformationConfigurationAdapter = new AutoMapperAdapter<Domain.Base.Entities.EmergencyInformationConfiguration, Dtos.Base.EmergencyInformationConfiguration>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Domain.Base.Entities.EmergencyInformationConfiguration, Dtos.Base.EmergencyInformationConfiguration>()).Returns(emergencyInformationConfigurationAdapter);

            var emergencyInformationConfiguration2Adapter = new AutoMapperAdapter<Domain.Base.Entities.EmergencyInformationConfiguration, Dtos.Base.EmergencyInformationConfiguration2>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Domain.Base.Entities.EmergencyInformationConfiguration, Dtos.Base.EmergencyInformationConfiguration2>()).Returns(emergencyInformationConfiguration2Adapter);

            var restrictionConfigurationAdapter = new AutoMapperAdapter<Domain.Base.Entities.RestrictionConfiguration, Dtos.Base.RestrictionConfiguration>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Domain.Base.Entities.RestrictionConfiguration, Dtos.Base.RestrictionConfiguration>()).Returns(restrictionConfigurationAdapter);

            var severityStyleMappingAdapter = new AutoMapperAdapter<Domain.Base.Entities.SeverityStyleMapping, Dtos.Base.SeverityStyleMapping>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Domain.Base.Entities.SeverityStyleMapping, Dtos.Base.SeverityStyleMapping>()).Returns(severityStyleMappingAdapter);

            var privacyConfigurationMappingAdapter = new AutoMapperAdapter<Domain.Base.Entities.PrivacyConfiguration, Dtos.Base.PrivacyConfiguration>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Domain.Base.Entities.PrivacyConfiguration, Dtos.Base.PrivacyConfiguration>()).Returns(privacyConfigurationMappingAdapter);

            var organizationalRelationshipConfigurationMappingAdapter = new AutoMapperAdapter<Domain.Base.Entities.OrganizationalRelationshipConfiguration, Dtos.Base.OrganizationalRelationshipConfiguration>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Domain.Base.Entities.OrganizationalRelationshipConfiguration, Dtos.Base.OrganizationalRelationshipConfiguration>()).Returns(organizationalRelationshipConfigurationMappingAdapter);

            configurationService = new ConfigurationService(testConfigurationRepository, adapterRegistry.Object, currentUserFactory, roleRepository, loggerObject);

            // Set up the mock statement to make the configuration repository method return null.
            Mock<IConfigurationRepository> testConfigurationRepositoryMock = new Mock<IConfigurationRepository>();
            Domain.Base.Entities.TaxFormConfiguration configuration = new Domain.Base.Entities.TaxFormConfiguration(Domain.Base.Entities.TaxForms.Form1095C);
            configuration = null;
            testConfigurationRepositoryMock.Setup<Task<Domain.Base.Entities.TaxFormConfiguration>>(x => x.GetTaxFormConsentConfigurationAsync(It.IsAny<Domain.Base.Entities.TaxForms>())).Returns(() =>
            {
                return Task.FromResult(configuration);
            });
            testConfigurationRepositoryMock.Setup<Task<Domain.Base.Entities.PrivacyConfiguration>>(x => x.GetPrivacyConfigurationAsync()).Returns(
                async () => await testConfigurationRepository.GetPrivacyConfigurationAsync());

            configurationServiceToReturnNull = new ConfigurationService(testConfigurationRepositoryMock.Object, adapterRegistry.Object, currentUserFactory, roleRepository, loggerObject);
        }
    }
}
