﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Base.Tests;
using Ellucian.Web.Adapters;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Base;
using Ellucian.Colleague.Dtos;
using Ellucian.Colleague.Domain.Base.Entities;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Base.Tests.Services
{
    [TestClass]
    public class GeographicAreaServiceTests 
    {
        // sets up a current user
        public abstract class CurrentUserSetup
        {
            protected Domain.Entities.Role personRole = new Domain.Entities.Role(105, "Faculty");

           public class PersonUserFactory : ICurrentUserFactory
            {
                public ICurrentUser CurrentUser
                {
                    get
                    {
                        return new CurrentUser(new Claims()
                        {
                            ControlId = "123",
                            Name = "George",
                            PersonId = "0000015",
                            SecurityToken = "321",
                            SessionTimeout = 30,
                            UserName = "Faculty",
                            Roles = new List<string>() { "Faculty" },
                            SessionFixationId = "abc123",
                        });
                    }
                }
            }         
        }

        [TestClass]
        public class GeographicAreaService_Get : CurrentUserSetup
        {
            private Mock<IPersonRepository> personRepoMock;
            private IPersonRepository personRepo;
            private Mock<IReferenceDataRepository> refRepoMock;
            private IReferenceDataRepository refRepo;
            private Mock<IAdapterRegistry> adapterRegistryMock;
            private IAdapterRegistry adapterRegistry;
            private ILogger logger;
            private Mock<IRoleRepository> roleRepoMock;
            private IRoleRepository roleRepo;
            private ICurrentUserFactory currentUserFactory;
            private IEnumerable<Domain.Base.Entities.Chapter> allChapters;
            private IEnumerable<Domain.Base.Entities.County> allCounties;
            private IEnumerable<Domain.Base.Entities.ZipcodeXlat> allZipCodeXlats;
            private IEnumerable<Domain.Base.Entities.GeographicAreaType> allGeographicAreaTypes;
            private GeographicAreaService geographicAreaService;
            private string chapterGuid = "9ae3a175-1dfd-4937-b97b-3c9ad596e023";
            private string countyGuid = "dd0c42ca-c61d-4ca6-8d21-96ab5be35623";
            private string zipCodeXlatGuid = "72b7737b-27db-4a06-944b-97d00c29b3db";
            private Domain.Entities.Permission permissionViewAnyPerson;
            private string invalidGuid;

            [TestInitialize]
            public void Initialize() {
                invalidGuid = "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";
                
                personRepoMock = new Mock<IPersonRepository>();
                personRepo = personRepoMock.Object;
                refRepoMock = new Mock<IReferenceDataRepository>();
                refRepo = refRepoMock.Object;
                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;
                roleRepoMock = new Mock<IRoleRepository>();
                roleRepo = roleRepoMock.Object;
                logger = new Mock<ILogger>().Object;

                allChapters = new TestGeographicAreaRepository().GetChapters();
                allCounties = new TestGeographicAreaRepository().GetCounties();
                allZipCodeXlats = new TestGeographicAreaRepository().GetZipCodeXlats();
                allGeographicAreaTypes = new TestGeographicAreaRepository().Get();

                adapterRegistryMock = new Mock<IAdapterRegistry>();
                adapterRegistry = adapterRegistryMock.Object;

                // Set up current user
                currentUserFactory = new CurrentUserSetup.PersonUserFactory();

                // Mock permissions
                permissionViewAnyPerson = new Ellucian.Colleague.Domain.Entities.Permission(BasePermissionCodes.ViewAnyPerson);
                personRole.AddPermission(permissionViewAnyPerson);
                roleRepoMock.Setup(rpm => rpm.Roles).Returns(new List<Domain.Entities.Role>() { personRole });

                geographicAreaService = new GeographicAreaService(adapterRegistry, refRepo, personRepo, currentUserFactory, roleRepo, logger);
                refRepoMock.Setup(repo => repo.GetGeographicAreaTypesAsync(It.IsAny<bool>())).ReturnsAsync(allGeographicAreaTypes);
            }

            [TestCleanup]
            public void Cleanup() {
                refRepo = null;
                personRepo = null;
                allChapters = null;
                allCounties = null;
                allZipCodeXlats = null;
                adapterRegistry = null;
                roleRepo = null;
                logger = null;
                geographicAreaService = null;
            }

            [TestMethod]
            public async Task GetGeographicAreaByGuid_ValidChapterGuid() {
                Ellucian.Colleague.Domain.Base.Entities.Chapter thisChapter = allChapters.Where(m => m.Guid == chapterGuid).FirstOrDefault();
                refRepoMock.Setup(repo => repo.GetChaptersAsync(true)).ReturnsAsync(allChapters.Where(m => m.Guid == chapterGuid));
                refRepoMock.Setup(repo => repo.GetCountiesAsync(It.IsAny<bool>())).ReturnsAsync(new List<Domain.Base.Entities.County>());
                refRepoMock.Setup(repo => repo.GetZipCodeXlatAsync(It.IsAny<bool>())).ReturnsAsync(new List<Domain.Base.Entities.ZipcodeXlat>());
                refRepoMock.Setup(repo => repo.GetRecordInfoFromGuidGeographicAreaAsync(It.IsAny<string>())).ReturnsAsync(Domain.Base.Entities.GeographicAreaTypeCategory.Fundraising);
                GeographicArea geographicArea = await geographicAreaService.GetGeographicAreaByGuidAsync(chapterGuid);
                Assert.AreEqual(thisChapter.Guid, geographicArea.Id);
                Assert.AreEqual(thisChapter.Code, geographicArea.Code);
                Assert.AreEqual(thisChapter.Description, geographicArea.Title);
                
            }

            
            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task GetGeographicAreaByGuid_InvalidChapterGuid()
            {
                refRepoMock.Setup(repo => repo.GetRecordInfoFromGuidGeographicAreaAsync(It.IsAny<string>())).Throws<KeyNotFoundException>();
                await geographicAreaService.GetGeographicAreaByGuidAsync("");
            }

            [TestMethod]
            [ExpectedException(typeof(KeyNotFoundException))]
            public async Task GetGeographicAreaByGuid_InvalidChapter()
            {
                refRepoMock.Setup(repo => repo.GetRecordInfoFromGuidGeographicAreaAsync(It.IsAny<string>())).Throws<KeyNotFoundException>();
                await geographicAreaService.GetGeographicAreaByGuidAsync(It.IsAny<string>());
            }


            [TestMethod]
            public async Task GetGeographicAreaByGuid_ValidCountyGuid()
            {
                County thisCounty = allCounties.Where(m => m.Guid == countyGuid).FirstOrDefault();
                refRepoMock.Setup(repo => repo.GetCountiesAsync(true)).ReturnsAsync(allCounties.Where(m => m.Guid == countyGuid));
                refRepoMock.Setup(repo => repo.GetChaptersAsync(It.IsAny<bool>())).ReturnsAsync(new List<Domain.Base.Entities.Chapter>());
                refRepoMock.Setup(repo => repo.GetZipCodeXlatAsync(It.IsAny<bool>())).ReturnsAsync(new List<Domain.Base.Entities.ZipcodeXlat>());
                refRepoMock.Setup(repo => repo.GetRecordInfoFromGuidGeographicAreaAsync(It.IsAny<string>())).ReturnsAsync(Domain.Base.Entities.GeographicAreaTypeCategory.Governmental);
                GeographicArea geographicArea = await geographicAreaService.GetGeographicAreaByGuidAsync(countyGuid);
                Assert.AreEqual(thisCounty.Guid, geographicArea.Id);
                Assert.AreEqual(thisCounty.Code, geographicArea.Code);
                Assert.AreEqual(thisCounty.Description, geographicArea.Title);
              
            }

            [TestMethod]
            public async Task GetGeographicAreaByGuid_ValidZipCodeXlatGuid()
            {
                ZipcodeXlat thisZipCodeXlat = allZipCodeXlats.Where(m => m.Guid == zipCodeXlatGuid).FirstOrDefault();
                refRepoMock.Setup(repo => repo.GetZipCodeXlatAsync(true)).ReturnsAsync(allZipCodeXlats.Where(m => m.Guid == zipCodeXlatGuid));
                refRepoMock.Setup(repo => repo.GetChaptersAsync(It.IsAny<bool>())).ReturnsAsync(new List<Domain.Base.Entities.Chapter>());
                refRepoMock.Setup(repo => repo.GetCountiesAsync(It.IsAny<bool>())).ReturnsAsync(new List<Domain.Base.Entities.County>());
                refRepoMock.Setup(repo => repo.GetRecordInfoFromGuidGeographicAreaAsync(It.IsAny<string>())).ReturnsAsync(Domain.Base.Entities.GeographicAreaTypeCategory.Postal);
                GeographicArea geographicArea = await geographicAreaService.GetGeographicAreaByGuidAsync(zipCodeXlatGuid);
                Assert.AreEqual(thisZipCodeXlat.Guid, geographicArea.Id);
                Assert.AreEqual(thisZipCodeXlat.Code, geographicArea.Code);
                Assert.AreEqual(thisZipCodeXlat.Description, geographicArea.Title);
               
            }

            [TestMethod]
            public async Task GetGeographicAreas_CountGeographicAreas()
            {
                refRepoMock.Setup(repo => repo.GetChaptersAsync(false)).ReturnsAsync(allChapters);
                refRepoMock.Setup(repo => repo.GetCountiesAsync(false)).ReturnsAsync(allCounties);
                refRepoMock.Setup(repo => repo.GetZipCodeXlatAsync(false)).ReturnsAsync(allZipCodeXlats);
                IEnumerable<GeographicArea> geographicArea = await geographicAreaService.GetGeographicAreasAsync();
                Assert.AreEqual(6, geographicArea.Count());
            }

            [TestMethod]
            public async Task GetGeographicAreas_CompareGeographicAreasChapters()
            {
                refRepoMock.Setup(repo => repo.GetChaptersAsync(false)).ReturnsAsync(allChapters);
                refRepoMock.Setup(repo => repo.GetCountiesAsync(false)).ReturnsAsync(allCounties);
                refRepoMock.Setup(repo => repo.GetZipCodeXlatAsync(false)).ReturnsAsync(allZipCodeXlats);
                IEnumerable<GeographicArea> geographicArea = await geographicAreaService.GetGeographicAreasAsync();
                Assert.AreEqual(allChapters.ElementAt(0).Guid, geographicArea.ElementAt(0).Id);
                Assert.AreEqual(allChapters.ElementAt(0).Code, geographicArea.ElementAt(0).Code);
                Assert.AreEqual(allChapters.ElementAt(0).Description, geographicArea.ElementAt(0).Title);
               
            }

            [TestMethod]
            public async Task GetGeographicAreas_CompareGeographicAreasCounties()
            {
                refRepoMock.Setup(repo => repo.GetChaptersAsync(false)).ReturnsAsync(allChapters);
                refRepoMock.Setup(repo => repo.GetCountiesAsync(false)).ReturnsAsync(allCounties);
                refRepoMock.Setup(repo => repo.GetZipCodeXlatAsync(false)).ReturnsAsync(allZipCodeXlats);
                IEnumerable<GeographicArea> geographicArea = await geographicAreaService.GetGeographicAreasAsync();
                Assert.AreEqual(allCounties.ElementAt(0).Guid, geographicArea.ElementAt(2).Id);
                Assert.AreEqual(allCounties.ElementAt(0).Code, geographicArea.ElementAt(2).Code);
                Assert.AreEqual(allCounties.ElementAt(0).Description, geographicArea.ElementAt(2).Title);
              
            }

            [TestMethod]
            public async Task GetGeographicAreas_CompareGeographicAreasZipCodeXlats()
            {
                refRepoMock.Setup(repo => repo.GetChaptersAsync(false)).ReturnsAsync(allChapters);
                refRepoMock.Setup(repo => repo.GetCountiesAsync(false)).ReturnsAsync(allCounties);
                refRepoMock.Setup(repo => repo.GetZipCodeXlatAsync(false)).ReturnsAsync(allZipCodeXlats);
                IEnumerable<GeographicArea> geographicArea = await geographicAreaService.GetGeographicAreasAsync();
                Assert.AreEqual(allZipCodeXlats.ElementAt(0).Guid, geographicArea.ElementAt(4).Id);
                Assert.AreEqual(allZipCodeXlats.ElementAt(0).Code, geographicArea.ElementAt(4).Code);
                Assert.AreEqual(allZipCodeXlats.ElementAt(0).Description, geographicArea.ElementAt(4).Title);
            }

            //[TestMethod]
            //[ExpectedException(typeof(KeyNotFoundException))]
            //public async Task GetGeographicAreas_GATNotPopulated_Chapters()
            //{
            //    List<Domain.Base.Entities.GeographicAreaType> gat = new List<Domain.Base.Entities.GeographicAreaType>();
            //    gat.Add(new Domain.Base.Entities.GeographicAreaType(Guid.NewGuid().ToString(), "GOV", "Description", Domain.Base.Entities.GeographicAreaTypeCategory.Governmental)); 
            //    refRepoMock.Setup(repo => repo.GetChaptersAsync(false)).ReturnsAsync(allChapters);
            //    refRepoMock.Setup(repo => repo.GetCountiesAsync(false)).ReturnsAsync(allCounties);
            //    refRepoMock.Setup(repo => repo.GetZipCodeXlatAsync(false)).ReturnsAsync(allZipCodeXlats);
            //    refRepoMock.Setup(repo => repo.GetGeographicAreaTypesAsync(false)).ReturnsAsync(gat);
            //    refRepoMock.Setup(repo => repo.GetRecordInfoFromGuidGeographicAreaAsync(It.IsAny<string>())).ReturnsAsync(Domain.Base.Entities.GeographicAreaTypeCategory.Fundraising);
            //    IEnumerable<GeographicArea> geographicArea = await geographicAreaService.GetGeographicAreasAsync();
            //}

            //[TestMethod]
            //[ExpectedException(typeof(KeyNotFoundException))]
            //public async Task GetGeographicAreas_GATNotPopulated_Counties()
            //{
            //    List<Domain.Base.Entities.GeographicAreaType> gat = new List<Domain.Base.Entities.GeographicAreaType>();
            //    gat.Add(new Domain.Base.Entities.GeographicAreaType(Guid.NewGuid().ToString(), "FUND", "Description", Domain.Base.Entities.GeographicAreaTypeCategory.Fundraising));
            //    refRepoMock.Setup(repo => repo.GetChaptersAsync(false)).ReturnsAsync(allChapters);
            //    refRepoMock.Setup(repo => repo.GetCountiesAsync(false)).ReturnsAsync(allCounties);
            //    refRepoMock.Setup(repo => repo.GetZipCodeXlatAsync(false)).ReturnsAsync(allZipCodeXlats);
            //    refRepoMock.Setup(repo => repo.GetGeographicAreaTypesAsync(false)).ReturnsAsync(gat);
            //    refRepoMock.Setup(repo => repo.GetRecordInfoFromGuidGeographicAreaAsync(It.IsAny<string>())).ReturnsAsync(Domain.Base.Entities.GeographicAreaTypeCategory.Governmental);
            //    IEnumerable<GeographicArea> geographicArea = await geographicAreaService.GetGeographicAreasAsync();
            //}

            //[TestMethod]
            //[ExpectedException(typeof(KeyNotFoundException))]
            //public async Task GetGeographicAreas_GATNotPopulated_ZipCodeXlats()
            //{
            //    List<Domain.Base.Entities.GeographicAreaType> gat = new List<Domain.Base.Entities.GeographicAreaType>();
            //    gat.Add(new Domain.Base.Entities.GeographicAreaType(Guid.NewGuid().ToString(), "FUND", "Description", Domain.Base.Entities.GeographicAreaTypeCategory.Fundraising));
            //    gat.Add(new Domain.Base.Entities.GeographicAreaType(Guid.NewGuid().ToString(), "GOV", "Description", Domain.Base.Entities.GeographicAreaTypeCategory.Governmental)); 
            //    refRepoMock.Setup(repo => repo.GetChaptersAsync(false)).ReturnsAsync(allChapters);
            //    refRepoMock.Setup(repo => repo.GetCountiesAsync(false)).ReturnsAsync(allCounties);
            //    refRepoMock.Setup(repo => repo.GetZipCodeXlatAsync(false)).ReturnsAsync(allZipCodeXlats);
            //    refRepoMock.Setup(repo => repo.GetGeographicAreaTypesAsync(false)).ReturnsAsync(gat);
            //    refRepoMock.Setup(repo => repo.GetRecordInfoFromGuidGeographicAreaAsync(It.IsAny<string>())).ReturnsAsync(Domain.Base.Entities.GeographicAreaTypeCategory.Postal);
            //    IEnumerable<GeographicArea> geographicArea = await geographicAreaService.GetGeographicAreasAsync();
            //}
        }
    }
}
