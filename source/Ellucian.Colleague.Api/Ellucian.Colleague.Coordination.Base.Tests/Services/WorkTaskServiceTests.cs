﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Coordination.Base.Tests.UserFactories;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Base.Tests.Services
{
    [TestClass]
    public class WorkTaskServiceTestsBase : GenericUserFactory
    {
        protected WorkTaskService service = null;
        protected Mock<IWorkTaskRepository> mockWorkTaskRepository;
        protected Mock<IRoleRepository> mockRoleRepository;
        protected ICurrentUserFactory currentUserFactory;
        protected List<Domain.Base.Entities.WorkTask> workTaskEntities;
        protected List<WorkTask> workTaskDtos;
        protected IEnumerable<Domain.Entities.Role> repoRoles;
        protected AutoMapperAdapter<Domain.Base.Entities.WorkTask, Dtos.Base.WorkTask> entityToDtoAdapter;
        protected AutoMapperAdapter<Dtos.Base.WorkTask, Domain.Base.Entities.WorkTask> dtoToEntityAdapter;
        protected string personId;
        protected string roleId;
        
        public void InitializeBase()
        {
            personId = "0000001";
            roleId = "1";

            // Build all service objects to use each of the user factories built above
            BuildWorkTaskService();

            workTaskEntities = new List<Domain.Base.Entities.WorkTask>() {
                new Domain.Base.Entities.WorkTask("1","Category 1","First Work Task", "SSHRTA"),
                new Domain.Base.Entities.WorkTask("2","Category 2","Second Work Task", "SSHRLVA"),
                new Domain.Base.Entities.WorkTask("3","Category 1","Third Work Task", "SSHRTA")
            };

            workTaskDtos = new List<WorkTask>();
            foreach (var item in workTaskEntities)
            {
                workTaskDtos.Add(entityToDtoAdapter.MapToType(item));
            };
            
            repoRoles = new List<Domain.Entities.Role>() {
                new Domain.Entities.Role(2, "Parent"),
                new Domain.Entities.Role(1, "Student")
            };
        }

        public void CleanupBase()
        {
            service = null;
            mockWorkTaskRepository = null;
            mockRoleRepository = null;
            workTaskEntities = null;
            workTaskDtos = null;
        }

        private void BuildWorkTaskService()
        {
            // We need the unit tests to be independent of "real" implementations of these classes,
            // so we use Moq to create mock implementations that are based on the same interfaces
            // Initialize the mock repositories
            mockWorkTaskRepository = new Mock<IWorkTaskRepository>();
            mockRoleRepository = new Mock<IRoleRepository>(); 
            var loggerObject = new Mock<ILogger>().Object;

            // Set up current user
            currentUserFactory = new GenericUserFactory.UserFactory();

            // Set up and mock the adapter, and setup the GetAdapter method.
            var adapterRegistry = new Mock<IAdapterRegistry>();

            // Set up the entity to DTO adapter
            entityToDtoAdapter = new AutoMapperAdapter<Domain.Base.Entities.WorkTask, Dtos.Base.WorkTask>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Domain.Base.Entities.WorkTask, Dtos.Base.WorkTask>()).Returns(entityToDtoAdapter);

            // Set up the DTO to entity adapter
            dtoToEntityAdapter = new AutoMapperAdapter<Dtos.Base.WorkTask, Domain.Base.Entities.WorkTask>(adapterRegistry.Object, loggerObject);
            adapterRegistry.Setup(x => x.GetAdapter<Dtos.Base.WorkTask, Domain.Base.Entities.WorkTask>()).Returns(dtoToEntityAdapter);

            // Set up the current user with a subset of projects and set up the service.
            service = new WorkTaskService(adapterRegistry.Object, mockWorkTaskRepository.Object, currentUserFactory, mockRoleRepository.Object, loggerObject);
        }
    }

    [TestClass]
    public class WorkTaskServiceTests : WorkTaskServiceTestsBase
    {
        [TestInitialize]
        public void Initialize()
        {
            InitializeBase();
        }

        [TestCleanup]
        public void Cleanup()
        {
            CleanupBase();
        }

        [TestMethod]
        public async Task GetAsync_Success()
        {
            // Set up role repository response
            mockRoleRepository.Setup(repo => repo.GetRolesAsync()).ReturnsAsync(repoRoles);
            // Set up WorkTask repository response--which, by expecting values in the roles field, also verifies that role ids were built based on the role response + the current user roles.
            mockWorkTaskRepository.Setup<Task<List<Domain.Base.Entities.WorkTask>>>(repo => repo.GetAsync(personId, It.Is<List<string>>(y => y.Contains(roleId))))
                .ReturnsAsync(workTaskEntities);

            var actualResult = await this.service.GetAsync("0000001");
            Assert.AreEqual(3, actualResult.Count());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public async Task GetAsync_NoPersonId_ThrowsException()
        {
            var actualResult = await this.service.GetAsync(null);
        }

        [TestMethod]
        [ExpectedException(typeof(PermissionsException))]
        public async Task GetAsync_PersonIdDoesNotMatchCurrentUser_ThrowsException()
        {
            var actualResult = await this.service.GetAsync("0000002");
        }
    }
}
