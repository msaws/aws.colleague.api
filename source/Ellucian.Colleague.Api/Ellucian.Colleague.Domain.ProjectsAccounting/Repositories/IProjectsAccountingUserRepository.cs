﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System.Threading.Tasks;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Repositories
{
    /// <summary>
    /// This is the definition of the methods that must be implemented
    /// for a projects accounting user repository.
    /// </summary>
    public interface IProjectsAccountingUserRepository
    {
        /// <summary>
        /// This is the method definition for getting a single projects accounting user object.
        /// </summary>
        /// <param name="id">This is the user id.</param>
        /// <returns>Returns a projects accounting user object that has a Person Id and a list of assigned project ids.</returns>
        Task<ProjectsAccountingUser> GetProjectsAccountingUserAsync(string id);

    }
}
