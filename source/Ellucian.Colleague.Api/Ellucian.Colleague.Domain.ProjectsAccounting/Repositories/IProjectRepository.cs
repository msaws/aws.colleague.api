﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Repositories
{
    /// <summary>
    /// This is the definition of the methods that must be implemented
    /// for a project repository.
    /// </summary>
    public interface IProjectRepository
    {
        /// <summary>
        /// Get a list of projects based on the list of IDs supplied.
        /// </summary>
        /// <param name="projectIds">Specify a list of project IDs to determine which projects will be selected.</param>
        /// <param name="summaryOnly">Specify whether or not detailed information will be returned with the project.</param>
        /// <param name="sequenceNumber">Specify a sequence number to return project totals for a single budget period.</param>
        /// <returns>List of project domain entities.</returns>
        Task<IEnumerable<Project>> GetProjectsAsync(IEnumerable<string> projectIds, bool summaryOnly, string sequenceNumber);

        /// <summary>
        /// Get a single project.
        /// </summary>
        /// <param name="projectId">Specify a project ID to determine which project to select.</param>
        /// <param name="summaryOnly">Specify whether or not detailed information will be returned with the project.</param>
        /// <param name="sequenceNumber">Specify a sequence number to return project totals for a single budget period.</param>
        /// <returns>Project domain entity.</returns>
        Task<Project> GetProjectAsync(string projectId, bool summaryOnly, string sequenceNumber);
    }
}
