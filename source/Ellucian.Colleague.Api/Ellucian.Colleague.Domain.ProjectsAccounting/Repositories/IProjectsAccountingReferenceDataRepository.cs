﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.ProjectsAccounting.Entities;


namespace Ellucian.Colleague.Domain.ProjectsAccounting.Repositories
{
    /// <summary>
    /// Provides read-only access to basic data required for Projects Accounting Self Service
    /// </summary>
    public interface IProjectsAccountingReferenceDataRepository
    {
        /// <summary>
        /// Returns domain entities for project types
        /// </summary>
        Task<IEnumerable<ProjectItemCode>> GetLineItemCodesAsync();

        /// <summary>
        /// Returns domain entities for project types
        /// </summary>
        Task<IEnumerable<ProjectType>> GetProjectTypesAsync();
    }
}
