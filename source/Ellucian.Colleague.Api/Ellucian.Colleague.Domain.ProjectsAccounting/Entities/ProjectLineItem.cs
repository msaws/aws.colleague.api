﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Entities
{
    /// <summary>
    /// This is a project line item.
    /// </summary>
    [Serializable]
    public class ProjectLineItem
    {
        /// <summary>
        /// System-generated id that is read-only.
        /// </summary>
        private readonly string id;

        /// <summary>
        /// Public getter for the private id.
        /// </summary>
        public string Id { get { return id; } }

        /// <summary>
        /// Private glClass member
        /// </summary>
        private readonly GlClass glClass;

        /// <summary>
        /// Public getter for private glClass
        /// </summary>
        public GlClass GlClass { get { return glClass; } }

        /// <summary>
        /// Private Line Item Code
        /// </summary>
        private readonly string itemCode;

        /// <summary>
        /// Public getter for the line item code
        /// </summary>
        public string ItemCode { get { return itemCode; } }

        /// <summary>
        /// This is the private list of GL accounts associated with the line item.
        /// </summary>
        private readonly List<ProjectLineItemGlAccount> glAccounts = new List<ProjectLineItemGlAccount>();

        /// <summary>
        /// This is the public getter for the private list of GL accounts.
        /// </summary>
        public ReadOnlyCollection<ProjectLineItemGlAccount> GlAccounts { get; private set; }

        /// <summary>
        /// This is the private list of project line item budget period amounts associated with the line item.
        /// </summary>
        private readonly List<BudgetPeriodAmount> budgetPeriodAmounts = new List<BudgetPeriodAmount>();

        /// <summary>
        /// This is the public getter for the private list of budget period amounts.
        /// </summary>
        public ReadOnlyCollection<BudgetPeriodAmount> BudgetPeriodAmounts { get; private set; }

        /// <summary>
        /// Get the total budget amount for a specified budget period or all budget periods.
        /// </summary>
        public decimal TotalBudget(string budgetPeriodSequenceNumber)
        {
            decimal totalBudget = 0;

            foreach (var budgetPeriodAmount in budgetPeriodAmounts)
            {
                if (budgetPeriodAmount.SequenceNumber == budgetPeriodSequenceNumber || string.IsNullOrEmpty(budgetPeriodSequenceNumber))
                {
                    totalBudget += budgetPeriodAmount.BudgetAmount;
                }
            }

            return totalBudget;
        }

        /// <summary>
        /// Get the total actuals amount for a specified budget period or all budget periods.
        /// </summary>
        public decimal TotalActuals(string budgetPeriodSequenceNumber)
        {
            decimal totalActuals = 0;

            foreach (var budgetPeriodAmount in budgetPeriodAmounts)
            {
                if (budgetPeriodAmount.SequenceNumber == budgetPeriodSequenceNumber || string.IsNullOrEmpty(budgetPeriodSequenceNumber))
                {
                    totalActuals += budgetPeriodAmount.MemoActualsAmount;
                    totalActuals += budgetPeriodAmount.PostedActualsAmount;
                }
            }

            return totalActuals;
        }

        /// <summary>
        /// Get the total memo actuals amount for a specified budget period or all budget periods.
        /// </summary>
        public decimal TotalMemoActuals(string budgetPeriodSequenceNumber)
        {
            decimal totalMemoActuals = 0;

            foreach (var budgetPeriodAmount in budgetPeriodAmounts)
            {
                if (budgetPeriodAmount.SequenceNumber == budgetPeriodSequenceNumber || string.IsNullOrEmpty(budgetPeriodSequenceNumber))
                {
                    totalMemoActuals += budgetPeriodAmount.MemoActualsAmount;
                }
            }

            return totalMemoActuals;
        }

        /// <summary>
        /// Get the total encumbrances amount for a specified budget period or all budget periods.
        /// </summary>
        public decimal TotalEncumbrances(string budgetPeriodSequenceNumber)
        {
            decimal totalEncumbrances = 0;

            foreach (var budgetPeriodAmount in budgetPeriodAmounts)
            {
                if (budgetPeriodAmount.SequenceNumber == budgetPeriodSequenceNumber || string.IsNullOrEmpty(budgetPeriodSequenceNumber))
                {
                    totalEncumbrances += budgetPeriodAmount.RequisitionAmount;
                    totalEncumbrances += budgetPeriodAmount.MemoEncumbranceAmount;
                    totalEncumbrances += budgetPeriodAmount.PostedEncumbranceAmount;
                }
            }

            return totalEncumbrances;
        }

        /// <summary>
        /// Get the total memo encumbrances amount for a specified budget period or all budget periods.
        /// </summary>
        public decimal TotalMemoEncumbrances(string budgetPeriodSequenceNumber)
        {
            decimal totalMemoEncumbrances = 0;

            foreach (var budgetPeriodAmount in budgetPeriodAmounts)
            {

                if (budgetPeriodAmount.SequenceNumber == budgetPeriodSequenceNumber || string.IsNullOrEmpty(budgetPeriodSequenceNumber))
                {
                    totalMemoEncumbrances += budgetPeriodAmount.MemoEncumbranceAmount;
                }
            }

            return totalMemoEncumbrances;
        }

        /// <summary>
        /// Initialize a project line item object.
        /// </summary>
        /// <param name="id">ID of project line item</param>
        /// <param name="glClass">The General Ledger type: Expense, Revenue, Asset, etc.</param>
        /// <param name="itemCode">The item code for this line item.</param>
        public ProjectLineItem(string id, GlClass glClass, string itemCode)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id", "Id is a required field.");
            }

            if (string.IsNullOrEmpty(itemCode))
            {
                throw new ArgumentNullException("itemCode", "ItemCode is a required field.");
            }

            this.id = id;
            this.glClass = glClass;
            this.itemCode = itemCode;
            GlAccounts = glAccounts.AsReadOnly();
            BudgetPeriodAmounts = budgetPeriodAmounts.AsReadOnly();
        }

        /// <summary>
        /// This method adds a project line item GL account to the list of
        /// GL accounts that belong to the project line item.
        /// </summary>
        /// <param name="projectLineItemGlAccount">Project line item GL account.</param>
        public void AddGlAccount(ProjectLineItemGlAccount projectLineItemGlAccount)
        {
            if (projectLineItemGlAccount == null)
            {
                throw new ArgumentNullException("projectLineItemGlAccount", "GL account cannot be null");
            }

            bool isInList = false;
            foreach (var glAccount in glAccounts)
            {
                if (glAccount.GlAccountNumber == projectLineItemGlAccount.GlAccountNumber)
                {
                    isInList = true;
                }
            }

            if (!isInList)
            {
                glAccounts.Add(projectLineItemGlAccount);
            }
        }

        /// <summary>
        /// Add a budget period amount object to the line item.
        /// </summary>
        /// <param name="budgetPeriodAmount">Budget period amount object.</param>
        public void AddBudgetPeriodAmount(BudgetPeriodAmount budgetPeriodAmount)
        {
            if (budgetPeriodAmount == null)
            {
                throw new ArgumentNullException("budgetPeriodAmount", "Budget period amount cannot be null");
            }

            budgetPeriodAmounts.Add(budgetPeriodAmount);
        }
    }
}