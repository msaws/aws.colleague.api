﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Entities;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Entities
{
    /// <summary>
    /// Class for project type
    /// </summary>
    [Serializable]
    public class ProjectType : CodeItem
    {
        /// <summary>
        /// Project type constructor
        /// </summary>
        /// <param name="code">code</param>
        /// <param name="description">description</param>
        public ProjectType(string code, string description)
            : base(code, description)
        {
            // no additional work to do
        }
    }
}
