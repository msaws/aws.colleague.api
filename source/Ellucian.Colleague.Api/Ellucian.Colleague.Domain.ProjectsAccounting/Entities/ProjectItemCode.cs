﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Entities;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Entities
{
    /// <summary>
    /// Class for the project line items codes
    /// </summary>
    [Serializable]
    public class ProjectItemCode : CodeItem
    {
        /// <summary>
        /// Line Item Code constructor
        /// </summary>
        /// <param name="code">code</param>
        /// <param name="description">description</param>
        public ProjectItemCode(string code, string description)
            : base(code, description)
        {
            // no additional work to do
        }
    }
}
