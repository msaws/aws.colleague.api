﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.ColleagueFinance.Entities
{
    /// <summary>
    /// This is a project line item budget period amount.
    /// </summary>
    [Serializable]
    public class BudgetPeriodAmount
    {
        /// <summary>
        /// Private variable for the SequenceNumber property.
        /// </summary>
        private readonly string sequenceNumber;

        /// <summary>
        /// Public getter for the private sequence number.
        /// </summary>
        public string SequenceNumber { get { return sequenceNumber; } }

        /// <summary>
        /// Private variable for the BudgetAmount property.
        /// </summary>
        private readonly decimal budgetAmount;

        /// <summary>
        /// Public getter for the private budget amount.
        /// </summary>
        public decimal BudgetAmount { get { return budgetAmount; } }

        /// <summary>
        /// Private variable for the MemoAmount property.
        /// </summary>
        private readonly decimal requisitionAmount;

        /// <summary>
        /// Public getter for the private memo amount.
        /// </summary>
        public decimal RequisitionAmount { get { return requisitionAmount; } }

        /// <summary>
        /// Private variable for the MemoEncumbranceAmount property.
        /// </summary>
        private readonly decimal memoEncumbranceAmount;

        /// <summary>
        /// Public getter for the private memo encumbrance amount.
        /// </summary>
        public decimal MemoEncumbranceAmount { get { return memoEncumbranceAmount; } }

        /// <summary>
        /// Private variable for the PostedEncumbranceAmount property.
        /// </summary>
        private readonly decimal postedEncumbranceAmount;

        /// <summary>
        /// Public getter for the private posted encumbrance amount.
        /// </summary>
        public decimal PostedEncumbranceAmount { get { return postedEncumbranceAmount; } }

        /// <summary>
        /// Private variable for the MemoActualsAmount property.
        /// </summary>
        private readonly decimal memoActualsAmount;

        /// <summary>
        /// Public getter for the private memo actuals amount.
        /// </summary>
        public decimal MemoActualsAmount { get { return memoActualsAmount; } }

        /// <summary>
        /// Private variable for the PostedActualsAmount property.
        /// </summary>
        private readonly decimal postedActualsAmount;

        /// <summary>
        /// Public getter for the private posted actuals amount.
        /// </summary>
        public decimal PostedActualsAmount { get { return postedActualsAmount; } }
        
        /// <summary>
        /// This constructor initializes a Project Line Item Budget Period Amount domain entity.
        /// </summary>
        /// <param name="sequenceNumber">Project line item budget period sequence number.</param>
        /// <param name="budgetAmount">Project line item budget period budget amount.</param>
        /// <param name="requisitionAmount">Project line item budget period requisition amount.</param>
        /// <param name="memoEncumbranceAmount">Project line item budget period memo encumbrance amount.</param>
        /// <param name="postedEncumbranceAmount">Project line item budget period posted encumbrance amount.</param>
        /// <param name="memoActualsAmount">Project line item budget period memo actuals amount.</param>
        /// <param name="postedActualsAmount">Project line item budget period posted actuals amount.</param>
        public BudgetPeriodAmount(string sequenceNumber, decimal budgetAmount, decimal requisitionAmount, decimal memoEncumbranceAmount, decimal postedEncumbranceAmount, decimal memoActualsAmount, decimal postedActualsAmount)
        {
            if (string.IsNullOrEmpty(sequenceNumber))
            {
                throw new ArgumentNullException("sequenceNumber", "SequenceNumber is a required field.");
            }

            this.sequenceNumber = sequenceNumber;
            this.budgetAmount = budgetAmount;
            this.requisitionAmount = requisitionAmount;
            this.memoEncumbranceAmount = memoEncumbranceAmount;
            this.postedEncumbranceAmount = postedEncumbranceAmount;
            this.memoActualsAmount = memoActualsAmount;
            this.postedActualsAmount = postedActualsAmount;
        }
    }
}
