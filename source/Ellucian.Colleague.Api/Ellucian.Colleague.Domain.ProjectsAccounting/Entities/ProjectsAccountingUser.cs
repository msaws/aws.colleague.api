﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Entities
{
    /// <summary>
    /// This class extends the Person class and builds a ProjectsAccountingUser object.
    /// </summary>
    [Serializable]
    public class ProjectsAccountingUser : Person
    {
        /// <summary>
        /// Private list of projects for which the user has access.
        /// </summary>
        private readonly List<string> projects = new List<string>();

        /// <summary>
        /// Public getter for the list of projects for which the user has access.
        /// </summary>
        public ReadOnlyCollection<string> Projects { get; private set; }

        /// <summary>
        /// Private variable to indicate whether the user has access to all general ledger numbers.
        /// </summary>
        private GlAccessLevel glAccessLevel;
        
        /// <summary>
        /// Public getter for the private full general ledger access variable.
        /// </summary>
        public GlAccessLevel GlAccessLevel { get { return glAccessLevel; } }

        /// <summary>
        /// This constructor initializes a Projects Accounting User domain entity.
        /// </summary>
        /// <param name="id">This is the user ID.</param>
        /// <param name="lastName">This is the user last name.</param>
        public ProjectsAccountingUser(string id, string lastName)
            : base(id, lastName)
        {
            Projects = projects.AsReadOnly();
            glAccessLevel = GlAccessLevel.No_Access;
        }

        /// <summary>
        /// This method adds a project to the list of projects for which the user
        /// has access.
        /// </summary>
        /// <param name="projectIds">This is the project ID.</param>
        public void AddProjects(IEnumerable<string> projectIds)
        {
            if (projectIds != null)
            {
                foreach (var projectId in projectIds)
                {
                    // If you end up keeping this block then update the unit tests to expect an exception.
                    if (string.IsNullOrEmpty(projectId))
                    {
                        throw new ArgumentNullException("projectId", "Project ID cannot be null or an empty string.");
                    }

                    if (!projects.Contains(projectId))
                    {
                        projects.Add(projectId);
                    }
                }
            }
        }

        /// <summary>
        /// This method removes all projects from the list of projects for which the
        /// user has access.
        /// </summary>
        public void RemoveAll()
        {
            projects.RemoveAll(x => true);
        }

        /// <summary>
        /// This method sets the private general ledger access level for the user.
        /// </summary>
        /// <param name="glAccess"></param>
        public void SetGlAccessLevel(GlAccessLevel glAccess)
        {
            glAccessLevel = glAccess;
        }
    }
}
