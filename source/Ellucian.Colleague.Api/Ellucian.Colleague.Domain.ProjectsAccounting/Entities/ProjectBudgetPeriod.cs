﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Entities
{
    /// <summary>
    /// This is a project budget period.
    /// </summary>
    [Serializable]
    public class ProjectBudgetPeriod
    {
        /// <summary>
        /// Private variable for the SequenceNumber property.
        /// </summary>
        private readonly string sequenceNumber;

        /// <summary>
        /// Public getter for the budget period sequence number.
        /// </summary>
        public string SequenceNumber { get { return sequenceNumber; } }

        /// <summary>
        /// Private variable for the budget period start date.
        /// </summary>
        private readonly DateTime startDate;

        /// <summary>
        /// This is the public getter for the private budget period start date.
        /// </summary>
        public DateTime StartDate { get { return startDate; } }

        /// <summary>
        /// Private variable for the budget period end date.
        /// </summary>
        private readonly DateTime endDate;

        /// <summary>
        /// This is the public getter for the private budget period end date.
        /// </summary>
        public DateTime EndDate { get { return endDate; } }

        /// <summary>
        /// This constructor initializes a Project Budget Period domain entity.
        /// </summary>
        /// <param name="sequenceNumber">This is the project budget period sequence number.</param>
        /// <param name="startDate">This is the project budget period start date.</param>
        /// <param name="endDate">This is the project budget period end date.</param>
        /// <exception cref="ApplicationException">Thrown if the budget period end date is before the budget period start date.</exception>
        public ProjectBudgetPeriod(string sequenceNumber, DateTime startDate, DateTime endDate)
        {
            if (string.IsNullOrEmpty(sequenceNumber))
            {
                throw new ArgumentNullException("sequenceNumber", "SequenceNumber is a required field.");
            }

            // Throw an exception when the start date is not before the end date
            int result = DateTime.Compare(startDate, endDate);
            if (result >= 0)
            {
                throw new ApplicationException("The Budget Period Start Date must be earlier than the End Date.");
            }

            this.sequenceNumber = sequenceNumber;
            this.startDate = startDate;
            this.endDate = endDate;
        }
    }
}