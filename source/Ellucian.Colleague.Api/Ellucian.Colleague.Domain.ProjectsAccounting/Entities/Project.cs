﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Entities
{
    /// <summary>
    /// This is a Projects Accounting Project.
    /// </summary>
    [Serializable]
    public class Project
    {
        /// <summary>
        /// System-generated id that is read-only.
        /// </summary>
        private readonly string projectId;

        /// <summary>
        /// This is the public getter for the private id.
        /// </summary>
        public string ProjectId { get { return projectId; } }

        /// <summary>
        /// Private variable for the project number.
        /// </summary>
        private readonly string number;

        /// <summary>
        /// This is the public getter for the private project number.
        /// </summary>
        public string Number { get { return number; } }

        /// <summary>
        /// Private variable for the project title.
        /// </summary>
        private readonly string title;

        /// <summary>
        /// This is the public getter for the private project title.
        /// </summary>
        public string Title { get { return title; } }

        /// <summary>
        /// Private variable for the project status.
        /// </summary>
        private readonly ProjectStatus status;

        /// <summary>
        /// This is the public getter for the private project status.
        /// </summary>
        public ProjectStatus Status { get { return status; } }

        /// <summary>
        /// Private variable for the project type.
        /// </summary>
        private readonly string type;

        /// <summary>
        /// Public getter for the private project type.
        /// </summary>
        public string ProjectType { get { return type; } }

        /// <summary>
        /// This is the private list of project line items associated with the project.
        /// </summary>
        private readonly List<ProjectLineItem> lineItems = new List<ProjectLineItem>();

        /// <summary>
        /// This is the public getter for the private list of project line items associated with the project.
        /// </summary>
        public ReadOnlyCollection<ProjectLineItem> LineItems { get; private set; }

        /// <summary>
        /// This is the private list of project budget periods associated with the project.
        /// </summary>
        private readonly List<ProjectBudgetPeriod> budgetPeriods = new List<ProjectBudgetPeriod>();

        /// <summary>
        /// This is the public getter for the private list of project budget periods associated with the project.
        /// </summary>
        public ReadOnlyCollection<ProjectBudgetPeriod> BudgetPeriods { get; private set; }

        #region Revenue

        /// <summary>
        /// Returns the total budget amount for all of the revenue-type line items associated with this project
        /// for the budget period specified or for all budget periods.
        /// </summary>
        public decimal TotalBudgetRevenue(string budgetPeriodSequenceNumber)
        {
            decimal totalBudgetRevenue = 0;
            foreach (var lineItem in lineItems)
            {
                if (lineItem.GlClass == GlClass.Revenue)
                {
                    totalBudgetRevenue += lineItem.TotalBudget(budgetPeriodSequenceNumber);
                }
            }

            return totalBudgetRevenue;
        }

        /// <summary>
        /// Returns the total actual amount for all of the revenue-type line items associated with this project
        /// for the budget period specified or for all budget periods.
        /// </summary>
        public decimal TotalActualsRevenue(string budgetPeriodSequenceNumber)
        {
            decimal totalActualsRevenue = 0;
            foreach (var lineItem in lineItems)
            {
                if (lineItem.GlClass == GlClass.Revenue)
                {
                    totalActualsRevenue += lineItem.TotalActuals(budgetPeriodSequenceNumber);
                }
            }
            return totalActualsRevenue;
        }

        /// <summary>
        /// Returns the total encumbrance amount for all of the revenue-type line items associated with this project
        /// for the budget period specified or for all budget periods.
        /// </summary>
        public decimal TotalEncumbranceRevenue(string budgetPeriodSequenceNumber)
        {
            decimal totalEncumbrancesRevenue = 0;
            foreach (var lineItem in lineItems)
            {
                if (lineItem.GlClass == GlClass.Revenue)
                {
                    totalEncumbrancesRevenue += lineItem.TotalEncumbrances(budgetPeriodSequenceNumber);
                }
            }
            return totalEncumbrancesRevenue;
        }

        #endregion

        #region Expense

        /// <summary>
        /// Returns the total budget amount for all of the expense-type line items associated with this project
        /// for the budget period specified or for all budget periods.
        /// </summary>
        public decimal TotalBudgetExpenses(string budgetPeriodSequenceNumber)
        {
            decimal totalBudgetExpenses = 0;
            foreach (var lineItem in lineItems)
            {
                if (lineItem.GlClass == GlClass.Expense)
                {
                    totalBudgetExpenses += lineItem.TotalBudget(budgetPeriodSequenceNumber);
                }
            }

            return totalBudgetExpenses;
        }

        /// <summary>
        /// Returns the total expense amount for all of the expense-type line items associated with this project
        /// for the budget period specified or for all budget periods.
        /// </summary>
        public decimal TotalExpenses(string budgetPeriodSequenceNumber)
        {
            decimal totalExpenses = 0;

            totalExpenses += TotalActualsExpenses(budgetPeriodSequenceNumber);
            totalExpenses += TotalEncumbranceExpenses(budgetPeriodSequenceNumber);

            return totalExpenses;
        }

        /// <summary>
        /// Returns the total actual amount for all of the expense-type line items associated with this project
        /// for the budget period specified or for all budget periods.
        /// </summary>
        public decimal TotalActualsExpenses(string budgetPeriodSequenceNumber)
        {
            decimal totalActualsExpenses = 0;
            foreach (var lineItem in lineItems)
            {
                if (lineItem.GlClass == GlClass.Expense)
                {
                    totalActualsExpenses += lineItem.TotalActuals(budgetPeriodSequenceNumber);
                }
            }
            return totalActualsExpenses;
        }

        /// <summary>
        /// Returns the total encumbrance amount for all of the expense-type line items associated with this project.
        /// </summary>
        public decimal TotalEncumbranceExpenses(string budgetPeriodSequenceNumber)
        {
            decimal totalEncumbrancesExpenses = 0;
            foreach (var lineItem in lineItems)
            {
                if (lineItem.GlClass == GlClass.Expense)
                {
                    totalEncumbrancesExpenses += lineItem.TotalEncumbrances(budgetPeriodSequenceNumber);
                }
            }
            return totalEncumbrancesExpenses;
        }

        #endregion

        #region Asset

        /// <summary>
        /// Returns the total actual amount for all of the asset-type line items associated with this project
        /// for the budget period specified or for all budget periods.
        /// </summary>
        public decimal TotalActualsAsset(string budgetPeriodSequenceNumber)
        {
            decimal totalActualsAsset = 0;
            foreach (var lineItem in lineItems)
            {
                if (lineItem.GlClass == GlClass.Asset)
                {
                    totalActualsAsset += lineItem.TotalActuals(budgetPeriodSequenceNumber);
                }
            }
            return totalActualsAsset;
        }

        /// <summary>
        /// Returns the total encumbrance amount for all of the asset-type line items associated with this project
        /// for the budget period specified or for all budget periods.
        /// </summary>
        public decimal TotalEncumbranceAsset(string budgetPeriodSequenceNumber)
        {
            decimal totalEncumbrancesAsset = 0;
            foreach (var lineItem in lineItems)
            {
                if (lineItem.GlClass == GlClass.Asset)
                {
                    totalEncumbrancesAsset += lineItem.TotalEncumbrances(budgetPeriodSequenceNumber);
                }
            }
            return totalEncumbrancesAsset;
        }

        #endregion

        #region Liability

        /// <summary>
        /// Returns the total actual amount for all of the liability-type line items associated with this project
        /// for the budget period specified or for all budget periods.
        /// </summary>
        public decimal TotalActualsLiability(string budgetPeriodSequenceNumber)
        {
            decimal totalActualsLiability = 0;
            foreach (var lineItem in lineItems)
            {
                if (lineItem.GlClass == GlClass.Liability)
                {
                    totalActualsLiability += lineItem.TotalActuals(budgetPeriodSequenceNumber);
                }
            }
            return totalActualsLiability;
        }

        /// <summary>
        /// Returns the total encumbrance amount for all of the liability-type line items associated with this project
        /// for the budget period specified or for all budget periods.
        /// </summary>
        public decimal TotalEncumbranceLiability(string budgetPeriodSequenceNumber)
        {
            decimal totalEncumbrancesLiability = 0;
            foreach (var lineItem in lineItems)
            {
                if (lineItem.GlClass == GlClass.Liability)
                {
                    totalEncumbrancesLiability += lineItem.TotalEncumbrances(budgetPeriodSequenceNumber);
                }
            }
            return totalEncumbrancesLiability;
        }

        #endregion

        #region Fund Balance

        /// <summary>
        /// Returns the total actual amount for all of the fund balance-type line items associated with this project
        /// for the budget period specified or for all budget periods.
        /// </summary>
        public decimal TotalActualsFundBalance(string budgetPeriodSequenceNumber)
        {
            decimal totalActualsFundBalance = 0;
            foreach (var lineItem in lineItems)
            {
                if (lineItem.GlClass == GlClass.FundBalance)
                {
                    totalActualsFundBalance += lineItem.TotalActuals(budgetPeriodSequenceNumber);
                }
            }
            return totalActualsFundBalance;
        }

        /// <summary>
        /// Returns the total encumbrance amount for all of the fund balance-type line items associated with this project
        /// for the budget period specified or for all budget periods.
        /// </summary>
        public decimal TotalEncumbranceFundBalance(string budgetPeriodSequenceNumber)
        {
            decimal totalEncumbrancesFundBalance = 0;
            foreach (var lineItem in lineItems)
            {
                if (lineItem.GlClass == GlClass.FundBalance)
                {
                    totalEncumbrancesFundBalance += lineItem.TotalEncumbrances(budgetPeriodSequenceNumber);
                }
            }
            return totalEncumbrancesFundBalance;
        }

        #endregion

        /// <summary>
        /// This constructor initializes a Project domain entity.
        /// </summary>
        /// <param name="id">This is the project id.</param>
        /// <param name="number">This is the project number.</param>
        /// <param name="title">This is the title.</param>
        /// <param name="status">This is the status.</param>
        /// <param name="type">Type of the project.</param>
        /// <exception cref="ArgumentNullException">Thrown if any of the applicable parameters are null.</exception>
        public Project(string id, string number, string title, ProjectStatus status, string type)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id", "ID is a required field.");
            }

            if (string.IsNullOrEmpty(number))
            {
                throw new ArgumentNullException("number", "Number is a required field.");
            }

            if (string.IsNullOrEmpty(title))
            {
                throw new ArgumentNullException("title", "Title is a required field.");
            }

            if (string.IsNullOrEmpty(type))
            {
                throw new ArgumentNullException("type", "Type is a required field.");
            }

            this.projectId = id;
            this.number = number;
            this.title = title;
            this.status = status;
            this.type = type;
            LineItems = lineItems.AsReadOnly();
            BudgetPeriods = budgetPeriods.AsReadOnly();
        }

        /// <summary>
        /// This method adds a project line item to the list of line items that belong to the project.
        /// </summary>
        /// <param name="projectLineItem">Project line item object.</param>
        public void AddLineItem(ProjectLineItem projectLineItem)
        {
            if (projectLineItem == null)
            {
                throw new ArgumentNullException("projectLineItem", "Line item cannot be null");
            }

            bool isInList = false;
            foreach (var lineItem in lineItems)
            {
                if (lineItem.Id == projectLineItem.Id)
                {
                    isInList = true;
                }
            }

            if (!isInList)
            {
                lineItems.Add(projectLineItem);
            }
        }

        /// <summary>
        /// This method adds a project budget period to the list of budget periods that belong to the project.
        /// </summary>
        /// <param name="projectBudgetPeriod">Project budget period.</param>
        public void AddBudgetPeriod(ProjectBudgetPeriod projectBudgetPeriod)
        {
            if (projectBudgetPeriod == null)
            {
                throw new ArgumentNullException("projectBudgetPeriod", "Budget Period cannot be null");
            }

            bool isInList = false;
            foreach (var budgetPeriod in budgetPeriods)
            {
                if (budgetPeriod.SequenceNumber == projectBudgetPeriod.SequenceNumber)
                {
                    isInList = true;
                }
            }

            if (!isInList)
            {
                budgetPeriods.Add(projectBudgetPeriod);
            }
        }
    }
}
