﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Entities
{
    /// <summary>
    /// This is a projects accounting transaction.
    /// </summary>
    [Serializable]
    public class ProjectTransaction : GlTransaction
    {
        /// <summary>
        /// Private project line item id for the transaction.
        /// </summary>
        private readonly string projectLineItemId;

        /// <summary>
        /// ID of the project line item associated to this transaction.
        /// </summary>
        public string ProjectLineItemId { get { return projectLineItemId; } }

        public ProjectTransaction(string id, GlTransactionType type, string source, string glAccount, decimal amount, string referenceNumber, DateTime transactionDate, string description, string projectLineItemId)
            : base(id, type, source, glAccount, amount, referenceNumber, transactionDate, description)
        {
            if (string.IsNullOrEmpty(projectLineItemId))
            {
                throw new ArgumentNullException("projectLineItemId", "Project line item ID is a required field.");
            }

            this.projectLineItemId = projectLineItemId;
        }
    }
}
