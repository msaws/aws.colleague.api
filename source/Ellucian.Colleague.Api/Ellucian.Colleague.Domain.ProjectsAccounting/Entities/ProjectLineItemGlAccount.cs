﻿// Copyright 2014-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;

namespace Ellucian.Colleague.Domain.ProjectsAccounting.Entities
{
    /// <summary>
    /// This is a project line item general ledger account.
    /// </summary>
    [Serializable]
    public class ProjectLineItemGlAccount : GlAccount
    {
        /// <summary>
        /// This is the private list of transactions for the general ledger account.
        /// </summary>
        private readonly List<ProjectTransaction> projectTransactions = new List<ProjectTransaction>();

        /// <summary>
        /// Returns the list of transactions associated to this line item.
        /// </summary>
        public ReadOnlyCollection<ProjectTransaction> ProjectTransactions { get; private set; }

        /// <summary>
        /// Calculate the actuals amount using the general ledger transactions.
        /// </summary>
        public decimal Actuals
        {
            get
            {
                return projectTransactions.Where(x => x.GlTransactionType == GlTransactionType.Actual).Sum(x => x.Amount);
            }
        }

        /// <summary>
        /// Calculate the encumbrance amount using the general ledger transactions.
        /// </summary>
        public decimal Encumbrances
        {
            get
            {
                return projectTransactions.Where(x =>
                    x.GlTransactionType == GlTransactionType.Encumbrance
                    || x.GlTransactionType == GlTransactionType.Requisition).Sum(x => x.Amount);
            }
        }

        /// <summary>
        /// This constructor initializes a Project Line Item general ledger account domain entity.
        /// </summary>
        /// <param name="glAccount">This is the general ledger account.</param>
        public ProjectLineItemGlAccount(string glAccount)
            : base(glAccount)
        {
            this.GlAccountDescription = string.Empty;
            ProjectTransactions = projectTransactions.AsReadOnly();
        }

        /// <summary>
        /// Add a project transaction to the list of transactions for this general ledger account.
        /// The transactions are sorted by descendant date. 
        /// Actuals transactions are not changed.
        /// Encumbrance and requisitions are summarized for each reference number.
        /// If the sum of the amounts is $0.00, they are excluded.
        /// </summary>
        /// <param name="newTransaction">A project transaction</param>
        /// <exception cref="ArgumentNullException">Thrown if the supplied transaction is null.</exception>
        public void AddProjectTransaction(ProjectTransaction newTransaction)
        {
            if (newTransaction == null)
            {
                throw new ArgumentNullException("newTransaction", "Project transaction cannot be null.");
            }

            // First, differentiate between Actuals and Encumbrances.
            // Encumbrance and Requisition transactions are added up into the transaction
            // with the most recent date and later removed if the amount adds up to $0.00.
            if (newTransaction.GlTransactionType == GlTransactionType.Encumbrance || newTransaction.GlTransactionType == GlTransactionType.Requisition)
            {
                var transactionFound = false;
                foreach (var transaction in projectTransactions)
                {
                    if (!(string.IsNullOrEmpty(newTransaction.ReferenceNumber)))
                    {
                        if (newTransaction.ReferenceNumber == transaction.ReferenceNumber)
                        {
                            transaction.Amount += newTransaction.Amount;

                            if (DateTime.Compare(transaction.TransactionDate.Date, newTransaction.TransactionDate.Date) < 0)
                            // The existing transaction's date is earlier than the new one.
                            // Use the latest date for the transaction.
                            {
                                transaction.TransactionDate = newTransaction.TransactionDate;
                            }
                            transactionFound = true;
                            break;
                        }
                    }
                }

                // This is a new encumbrance or requisition transaction.
                if (transactionFound == false)
                {
                    projectTransactions.Add(newTransaction);
                }
            }
            else
            {
                // Check for duplicate records - mainly to safeguard againt programmer error
                // before adding the transaction to the list.
                // Two transactions can be exactly the same except for the record ID.
                bool duplicateId = false;
                foreach (var projectTransaction in projectTransactions)
                {
                    if (projectTransaction.Id == newTransaction.Id)
                    {
                        duplicateId = true;
                    }
                }
                if (!duplicateId)
                {
                    projectTransactions.Add(newTransaction);
                }
            }
        }

        /// <summary>
        /// Remove those encumbrance or requisition transactions whose amount is $0.00
        /// </summary>
        /// <param name="transactions"></param>
        public void RemoveZeroEncumbranceTransactions()
        {
            projectTransactions.RemoveAll(x => (x.GlTransactionType == GlTransactionType.Encumbrance || x.GlTransactionType == GlTransactionType.Requisition) &&
                (x.Amount == 0));
        }
    }
}