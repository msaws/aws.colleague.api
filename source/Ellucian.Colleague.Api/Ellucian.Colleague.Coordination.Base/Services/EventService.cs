﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DDay.iCal;
using DDay.iCal.Serialization;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;

namespace Ellucian.Colleague.Coordination.Base.Services
{
    /// <summary>
    /// Representation of an event service
    /// </summary>
    [RegisterType]
    public class EventService : IEventService 
    {
        private readonly IEventRepository _calsRepository;
        private readonly Ellucian.Web.Adapters.IAdapterRegistry _adapterRegistry;

        /// <summary>
        /// Initializes a new instance of the <see cref="EventService"/> class.
        /// </summary>
        /// <param name="adapterRegistry">The adapter registry.</param>
        /// <param name="calsRepository">The event repository.</param>
        public EventService(IAdapterRegistry adapterRegistry, IEventRepository calsRepository) 
        {
            _adapterRegistry = adapterRegistry;
            _calsRepository = calsRepository;
        }

        /// <summary>
        /// Gets the section events.
        /// </summary>
        /// <param name="sectionIds">The section ids.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        public Ellucian.Colleague.Dtos.Base.EventsICal GetSectionEvents(IEnumerable<string> sectionIds, DateTime? startDate, DateTime? endDate)
        {
            var events = _calsRepository.Get("CS", sectionIds, startDate, endDate);
            Ellucian.Colleague.Domain.Base.Entities.EventsICal eventsICalEntity = EventListToEventsICal(events);
            Ellucian.Colleague.Dtos.Base.EventsICal eventsICalDto = new Dtos.Base.EventsICal(eventsICalEntity.iCal);
            return eventsICalDto;
        }

        /// <summary>
        /// Gets the faculty events.
        /// </summary>
        /// <param name="facultyIds">The faculty ids.</param>
        /// <param name="startDate">The start date.</param>
        /// <param name="endDate">The end date.</param>
        /// <returns></returns>
        public Ellucian.Colleague.Dtos.Base.EventsICal GetFacultyEvents(IEnumerable<string> facultyIds, DateTime? startDate, DateTime? endDate)
        {
            var events = _calsRepository.Get("FI", facultyIds, startDate, endDate);
            Ellucian.Colleague.Domain.Base.Entities.EventsICal eventsICalEntity = EventListToEventsICal(events);
            Ellucian.Colleague.Dtos.Base.EventsICal eventsICalDto = new Dtos.Base.EventsICal(eventsICalEntity.iCal);
            return eventsICalDto;
        }

        private Ellucian.Colleague.Domain.Base.Entities.EventsICal EventListToEventsICal(IEnumerable<Ellucian.Colleague.Domain.Base.Entities.Event> events) {
            var iCal = new iCalendar();
            foreach (var anEvent in events) {
                DDay.iCal.Event evt = iCal.Create<DDay.iCal.Event>();
                evt.Name = "VEVENT";
                evt.Summary = anEvent.Description;
                evt.Categories = new List<string>() { anEvent.Type };
                evt.Location = anEvent.Location;
                evt.DTStart = new iCalDateTime(anEvent.StartTime.UtcDateTime);
                evt.DTStart.IsUniversalTime = true;
                evt.DTEnd = new iCalDateTime(anEvent.EndTime.UtcDateTime);
                evt.DTEnd.IsUniversalTime = true;
            }
            string result = null;
            var ctx = new SerializationContext();
            var factory = new DDay.iCal.Serialization.iCalendar.SerializerFactory();
            var serializer = factory.Build(iCal.GetType(), ctx) as IStringSerializer;
            if (serializer != null) {
                result = serializer.SerializeToString(iCal);
            }
            return new Ellucian.Colleague.Domain.Base.Entities.EventsICal(result);
        }

    }
}
