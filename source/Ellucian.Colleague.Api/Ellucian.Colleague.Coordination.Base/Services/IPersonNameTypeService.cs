﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Base.Services
{
    public interface IPersonNameTypeService
    {
        Task<IEnumerable<Ellucian.Colleague.Dtos.PersonNameTypeItem>> GetPersonNameTypesAsync(bool bypassCache);
        Task<Ellucian.Colleague.Dtos.PersonNameTypeItem> GetPersonNameTypeByIdAsync(string id);
    }
}