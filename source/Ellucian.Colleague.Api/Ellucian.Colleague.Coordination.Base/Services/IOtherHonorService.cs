﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Base.Services
{
    public interface IOtherHonorService
    {
        Task<IEnumerable<Ellucian.Colleague.Dtos.OtherHonor>> GetOtherHonorsAsync(bool bypassCache);
        Task<Ellucian.Colleague.Dtos.OtherHonor> GetOtherHonorByGuidAsync(string guid);
    }
}
