﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Base.Services
{
    public interface IExternalEducationService : IBaseService
    {
        Task<Tuple<IEnumerable<Dtos.ExternalEducation>, int>> GetExternalEducationsAsync(int offset, int limit, bool bypassCache = false, string personGuid = "");
        Task<Dtos.ExternalEducation> GetExternalEducationByGuidAsync(string guid);
    }
}