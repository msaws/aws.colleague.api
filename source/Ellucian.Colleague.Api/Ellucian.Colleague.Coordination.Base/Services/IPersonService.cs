﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Base.Services
{
    /// <summary>
    /// Interface for person services
    /// </summary>
    public interface IPersonService : IBaseService
    {
        /// <summary>
        /// Get a person by guid.
        /// </summary>
        /// <param name="guid">Guid of the person in Colleague.</param>
        /// <returns>The <see cref="Person">person</see></returns>
        Task<Person> GetPersonByGuidNonCachedAsync(string guid);

        /// <summary>
        /// Get a HEDM V6 person by guid.
        /// </summary>
        /// <param name="guid">Guid of the person in Colleague.</param>
        /// <returns>The <see cref="Person2">person</see></returns>
        Task<Person2> GetPerson2ByGuidNonCachedAsync(string guid);


        /// <summary>
        /// Get a HEDM V6 person by guid.
        /// </summary>
        /// <param name="guid">Guid of the person in Colleague.</param>
        /// <param name="bypassCache"></param>
        /// <returns>The <see cref="Person2">person</see></returns>
        Task<Person2> GetPerson2ByGuidAsync(string guid, bool bypassCache);


        /// <summary>
        /// Get a person credentials by guid.
        /// </summary>
        /// <param name="guid">Guid of the person in Colleague.</param>
        /// <returns>The <see cref="Dtos.PersonCredential">person credential</see></returns>
        Task<Dtos.PersonCredential> GetPersonCredentialByGuidAsync(string guid);

        /// <summary>
        /// Get a list of persons by guid without using a cache.
        /// </summary>
        /// <param name="guids">List of person guids</param>
        /// <returns>List of <see cref="Person">persons</see></returns>
        Task<IEnumerable<Dtos.Person>> GetPersonByGuidNonCachedAsync(IEnumerable<string> guids);

        /// <summary>
        /// Get a list of persons by guid without using a cache.
        /// </summary>
        /// <param name="guids">List of person guids</param>
        /// <returns>List of <see cref="Person2">persons</see></returns>
        Task<IEnumerable<Dtos.Person2>> GetPerson2ByGuidNonCachedAsync(IEnumerable<string> guids);

        /// <summary>
        /// Get a list of persons by guid without using a cache.
        /// </summary>
        /// <param name="guids">List of person guids</param>
        /// <returns>List of <see cref="Person3">persons</see></returns>
        Task<IEnumerable<Dtos.Person3>> GetPerson3ByGuidNonCachedAsync(IEnumerable<string> guids);

        /// <summary>
        /// Create a person.
        /// </summary>
        /// <param name="person">The <see cref="Person">person</see> entity to create in the database.</param>
        /// <returns>The newly created <see cref="Person">person</see></returns>
        Task<Person> CreatePersonAsync(Person person);

        /// <summary>
        /// Create a person.
        /// </summary>
        /// <param name="person">The <see cref="Person2">person</see> entity to create in the database.</param>
        /// <returns>The newly created <see cref="Person2">person</see></returns>
        Task<Person2> CreatePerson2Async(Person2 person);

        /// <summary>
        /// Create a person.
        /// </summary>
        /// <param name="person">The <see cref="Person3">person</see> entity to create in the database.</param>
        /// <returns>The newly created <see cref="Person3">person</see></returns>
        Task<Person3> CreatePerson3Async(Person3 person);

        /// <summary>
        /// Update a person.
        /// </summary>
        /// <param name="person">The <see cref="Person">person</see> entity to update in the database.</param>
        /// <returns>The newly updated <see cref="Person">person</see></returns>
        Task<Person> UpdatePersonAsync(Person person);

        /// <summary>
        /// Update a person.
        /// </summary>
        /// <param name="person">The <see cref="Person2">person</see> entity to update in the database.</param>
        /// <returns>The newly updated <see cref="Person2">person</see></returns>
        Task<Person2> UpdatePerson2Async(Person2 person);

        /// <summary>
        /// Update a person.
        /// </summary>
        /// <param name="person">The <see cref="Person3">person</see> entity to update in the database.</param>
        /// <returns>The newly updated <see cref="Person3">person</see></returns>
        Task<Person3> UpdatePerson3Async(Person3 person);

        /// <summary>
        /// Query person by person DTO.
        /// </summary>
        /// <param name="person">The <see cref="Person">person</see> entity to query by.</param>
        /// <returns>List of matching <see cref="Person">persons</see></returns>
        Task<IEnumerable<Person>> QueryPersonByPostAsync(Person person);

        /// <summary>
        /// Query person by person DTO.
        /// </summary>
        /// <param name="person">The <see cref="Person2">person</see> entity to query by.</param>
        /// <returns>List of matching <see cref="Person2">persons</see></returns>
        Task<IEnumerable<Person2>> QueryPerson2ByPostAsync(Person2 person);

        /// <summary>
        /// Query person by person DTO.
        /// </summary>
        /// <param name="person">The <see cref="Person3">person</see> entity to query by.</param>
        /// <returns>List of matching <see cref="Person3">persons</see></returns>
        Task<IEnumerable<Person3>> QueryPerson3ByPostAsync(Person3 person);

        /// <summary>
        /// Query person by criteria and return the results of the matching algorithm
        /// </summary>
        /// <param name="person">The <see cref="Dtos.Base.PersonMatchCriteria">criteria</see> to query by.</param>
        /// <returns>List of matching <see cref="Dtos.Base.PersonMatchResult">results</see></returns>
        Task<IEnumerable<Dtos.Base.PersonMatchResult>> QueryPersonMatchResultsByPostAsync(Dtos.Base.PersonMatchCriteria criteria);

        /// <summary>
        /// Get person data associated with a particular role.
        /// </summary>
        /// <param name="role">Name of the role to get persons by</param>
        /// <returns>List of <see cref="Dtos.Person">persons</see></returns>
        Task<Tuple<IEnumerable<Dtos.Person>,int>> GetPersonsByRoleNonCachedAsync(int offset, int limit, string role);

        /// <summary>
        /// Get HEDM V6 person data associated with a particular role.
        /// </summary>
        /// <param name="role">Name of the role to get persons by</param>
        /// <returns>List of <see cref="Dtos.Person">persons</see></returns>
        Task<Tuple<IEnumerable<Dtos.Person2>, int>> GetPersons2ByRoleNonCachedAsync(int offset, int limit, string role);

        /// <summary>
        /// Get HEDM V6 person data associated to the specified filters
        /// </summary>
        /// <param name="Offset">Paging offset</param>
        /// <param name="Limit">Paging limit</param>
        /// <param name="bypassCache">Flag to bypass Cache</param>
        /// <param name="title">Specific title</param>
        /// <param name="firstName">Specific first name</param>
        /// <param name="middleName">Specific middle name</param>
        /// <param name="lastNamePrefix">Last name beings with</param>
        /// <param name="lastName">Specific last name</param>
        /// <param name="pedigree">Specific suffix</param>
        /// <param name="preferredName">Specific preferred name</param>
        /// <param name="role">Specific role of a person</param>
        /// <param name="credentialType">Credential type of either colleagueId or ssn</param>
        /// <param name="credentialValue">Specific value of the credential to be evaluated</param>
        /// <param name="personFilter">Person Saved List selection or list name from person-filters</param>
        /// <returns>List of Person2 objects for filtered persons only.</returns>
        Task<Tuple<IEnumerable<Dtos.Person2>, int>> GetPerson2NonCachedAsync(int Offset, int Limit, bool bypassCache,
            string title, string firstName, string middleName, string lastNamePrefix, string lastName, string pedigree,
            string preferredName, string role, string credentialType, string credentialValue, string personFilter);
        
        /// <summary>
        /// Get a person's profile data.
        /// </summary>
        /// <param name="personId">Id of the person</param>
        /// <returns><see cref="Profile"/>Profile data for a person</returns>
        Task<Dtos.Base.Profile> GetProfileAsync(string personId, bool useCache = true);

        /// <summary>
        /// Updates a person's profile data.
        /// </summary>
        /// <param name="profileDto">Profile dto to update</param>
        /// <returns>The updated Profile dto</returns>
        [Obsolete("Obsolete as of API 1.16. Use version 2 of this method instead.")]
        Task<Dtos.Base.Profile> UpdateProfileAsync(Dtos.Base.Profile profileDto);

        /// <summary>
        /// Updates a person's profile data.
        /// </summary>
        /// <param name="profileDto">Profile dto to update</param>
        /// <returns>The updated Profile dto</returns>
        Task<Dtos.Base.Profile> UpdateProfile2Async(Dtos.Base.Profile profileDto);

        /// <summary>
        /// Create a Organization.
        /// </summary>
        /// <param name="person">The <see cref="Organization2">organization</see> entity to create in the database.</param>
        /// <returns>The newly created <see cref="Organization2">organization</see></returns>
        Task<Organization2> CreateOrganizationAsync(Organization2 organization);

        /// <summary>
        /// Update a Organization.
        /// </summary>
        /// <param name="person">The <see cref="Organization2">organization</see> entity to update in the database.</param>
        /// <returns>The newly updated <see cref="Organization2">organization</see></returns>
        Task<Organization2> UpdateOrganizationAsync(Organization2 organization);

        /// <summary>
        /// Get an organization from its GUID
        /// </summary>
        /// <returns>Organization2 DTO object</returns>
        Task<Organization2> GetOrganization2Async(string guid);

        /// <summary>
        /// Get HEDM V6 Organization data associated to the specified filters
        /// </summary>
        /// <param name="Offset">Paging offset</param>
        /// <param name="Limit">Paging limit</param>
        /// <param name="role">Specific role of a organization</param>
        /// <param name="credentialType">Credential type of colleagueId</param>
        /// <param name="credentialValue">Specific value of the credential to be evaluated</param>
        /// <returns>List of Organization2 objects for filtered organizations only.</returns>
        Task<Tuple<IEnumerable<Dtos.Organization2>, int>> GetOrganizations2Async(int Offset, int Limit, string role, string credentialType, string credentialValue);

        /// <summary>
        /// Gets all persons credentials
        /// </summary>
        /// <param name="Offset">Paging offset</param>
        /// <param name="Limit">Paging limit</param>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        Task<Tuple<IEnumerable<Dtos.PersonCredential>, int>> GetAllPersonCredentialsAsync(int offset, int limit, bool bypassCache);

        //V8 Changes
        /// <summary>
        /// Gets all persons credentials
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        Task<Tuple<IEnumerable<Dtos.PersonCredential2>, int>> GetAllPersonCredentials2Async(int offset, int limit, bool bypassCache);

        /// <summary>
        /// Get a person credentials by guid.
        /// </summary>
        /// <param name="guid">Guid of the person in Colleague.</param>
        /// <returns>The <see cref="Dtos.PersonCredential">person credential</see></returns>
        Task<Dtos.PersonCredential2> GetPersonCredential2ByGuidAsync(string guid);

        /// <summary>
        /// Get a HEDM V8 person by guid.
        /// </summary>
        /// <param name="guid">Guid of the person in Colleague.</param>
        /// <param name="bypassCache"></param>
        /// <returns>The <see cref="Person3">person</see></returns>
        Task<Person3> GetPerson3ByGuidAsync(string guid, bool bypassCache);

        /// <summary>
        /// Get HEDM V8 person data associated with a particular role.
        /// </summary>
        /// <param name="role">Name of the role to get persons by</param>
        /// <returns>List of <see cref="Dtos.Person3">persons</see></returns>
        Task<Tuple<IEnumerable<Dtos.Person3>, int>> GetPersons3ByRoleNonCachedAsync(int offset, int limit, string role);

        /// <summary>
        /// Get HEDM V8 person data associated to the specified filters
        /// </summary>
        /// <param name="Offset">Paging offset</param>
        /// <param name="Limit">Paging limit</param>
        /// <param name="bypassCache">Flag to bypass Cache</param>
        /// <param name="title">Specific title</param>
        /// <param name="firstName">Specific first name</param>
        /// <param name="middleName">Specific middle name</param>
        /// <param name="lastNamePrefix">Last name beings with</param>
        /// <param name="lastName">Specific last name</param>
        /// <param name="pedigree">Specific suffix</param>
        /// <param name="preferredName">Specific preferred name</param>
        /// <param name="role">Specific role of a person</param>
        /// <param name="credentialType">Credential type of either colleagueId or ssn</param>
        /// <param name="credentialValue">Specific value of the credential to be evaluated</param>
        /// <param name="personFilter">Person Saved List selection or list name from person-filters</param>
        /// <returns>List of Person2 objects for filtered persons only.</returns>
        Task<Tuple<IEnumerable<Dtos.Person3>, int>> GetPerson3NonCachedAsync(int Offset, int Limit, bool bypassCache,
            string title, string firstName, string middleName, string lastNamePrefix, string lastName, string pedigree,
            string preferredName, string role, string credentialType, string credentialValue, string personFilter);

        /// <summary>
        /// Retrieves the matching Persons for the ids provided or searches keyword
        /// for the matching Persons if a first and last name are provided.  
        /// In the latter case, a middle name is optional.
        /// Matching is done by partial name; i.e., 'Bro' will match 'Brown' or 'Brodie'. 
        /// Capitalization is ignored.
        /// </summary>
        /// <remarks>the following keyword input is legal
        /// <list type="bullet">
        /// <item>a Colleague id.  Short ids will be zero-padded.</item>
        /// <item>First Last</item>
        /// <item>First Middle Last</item>
        /// <item>Last, First</item>
        /// <item>Last, First Middle</item>
        /// </list>
        /// </remarks>
        /// <param name="criteria">Keyword can either be a Person ID or a first and last name.  A middle name is optional.</param>
        /// <returns>An enumeration of <see cref="Dtos.Base.Person">Person</see> with populated ID and first, middle and last names</returns>
        Task<IEnumerable<Dtos.Base.Person>> QueryPersonNamesByPostAsync(Dtos.Base.PersonNameQueryCriteria criteria);
    }
}