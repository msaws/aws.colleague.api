﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using Ellucian.Colleague.Domain.Base;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Dependency;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Services;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Dtos;
using Ellucian.Colleague.Dtos.DtoProperties;
using Ellucian.Colleague.Dtos.EnumProperties;
using CitizenshipStatusType = Ellucian.Colleague.Domain.Base.Entities.CitizenshipStatusType;
using CredentialType = Ellucian.Colleague.Dtos.EnumProperties.CredentialType;
using EthnicityType = Ellucian.Colleague.Domain.Base.Entities.EthnicityType;
using IdentityDocumentTypeCategory = Ellucian.Colleague.Domain.Base.Entities.IdentityDocumentTypeCategory;
using PersonName = Ellucian.Colleague.Domain.Base.Entities.PersonName;
using PersonRoleType = Ellucian.Colleague.Domain.Base.Entities.PersonRoleType;
using PersonVisa = Ellucian.Colleague.Domain.Base.Entities.PersonVisa;
using PrivacyStatusType = Ellucian.Colleague.Domain.Base.Entities.PrivacyStatusType;
using RaceType = Ellucian.Colleague.Domain.Base.Entities.RaceType;
using VisaTypeCategory = Ellucian.Colleague.Domain.Base.Entities.VisaTypeCategory;
using System.Text;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Dmi.Runtime;

namespace Ellucian.Colleague.Coordination.Base.Services
{
    /// <summary>
    /// Services for person objects.
    /// </summary>
    [RegisterType]
    public class PersonService : BaseCoordinationService, IPersonService
    {
        private readonly IPersonRepository _personRepository;
        private readonly IPersonBaseRepository _personBaseRepository;
        private readonly IReferenceDataRepository _referenceDataRepository;
        private readonly IProfileRepository _profileRepository;
        private readonly IConfigurationRepository _configurationRepository;
        private readonly IRelationshipRepository _relationshipRepository;
        private readonly IProxyRepository _proxyRepository;
        public static char _SM = Convert.ToChar(DynamicArray.SM);

        /// <summary>
        /// Initializes a new instance of the <see cref="PersonService"/> class.
        /// </summary>
        /// <param name="adapterRegistry">The adapter registry.</param>
        /// <param name="personRepository">The person repository.</param>
        /// <param name="referenceDataRepository">The reference data repository.</param>
        /// <param name="relationshipRepository">The relationship repository.</param>
        /// <param name="profileRepository">The profile repository.</param>
        /// <param name="configurationRepository">The configuration repository</param>
        /// <param name="proxyRepository">The proxy repository</param>
        /// <param name="currentUserFactory">The current user factory.</param>
        /// <param name="roleRepository">The role repository.</param>
        /// <param name="logger">The logger.</param>
        public PersonService(IAdapterRegistry adapterRegistry,
            IPersonRepository personRepository,
            IPersonBaseRepository personBaseRepository,
            IReferenceDataRepository referenceDataRepository,
            IProfileRepository profileRepository,
            IConfigurationRepository configurationRepository,
            IRelationshipRepository relationshipRepository,
            IProxyRepository proxyRepository,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger, configurationRepository: configurationRepository)
        {
            _personRepository = personRepository;
            _personBaseRepository = personBaseRepository;
            _referenceDataRepository = referenceDataRepository;
            _profileRepository = profileRepository;
            _relationshipRepository = relationshipRepository;
            _configurationRepository = configurationRepository;
            _proxyRepository = proxyRepository;
        }

        #region Get Person Methods

        /// <summary>
        /// Get a person by guid without using a cache.
        /// </summary>
        /// <param name="guid">Guid of the person in Colleague.</param>
        /// <returns>The <see cref="Domain.Base.Entities.Person">person</see></returns>
        public async Task<Dtos.Person> GetPersonByGuidNonCachedAsync(string guid)
        {
            if (string.IsNullOrEmpty(guid))
                throw new ArgumentNullException("guid", "Must provide a guid to get a person");

            // get the person ID associated with the incoming guid
            var personId = await _personRepository.GetPersonIdFromGuidAsync(guid);
            if (string.IsNullOrEmpty(personId))
                throw new KeyNotFoundException("Person ID associated to guid '" + guid + "' not found in repository");

            // verify the user has the permission to view themselves or any person
            CheckUserPersonViewPermissions(personId);

            var personEntity = await _personRepository.GetPersonByGuidNonCachedAsync(guid);

            if (personEntity == null)
                throw new KeyNotFoundException("Person '" + guid + "' not found in repository");

            return await ConvertPersonEntityToDtoAsync(personEntity, false);
        }

        /// <summary>
        /// Get a list of persons by guid without using a cache.
        /// </summary>
        /// <param name="guids">List of person guids</param>
        /// <returns>List of <see cref="Domain.Base.Entities.Person">persons</see></returns>
        public async Task<IEnumerable<Dtos.Person>> GetPersonByGuidNonCachedAsync(IEnumerable<string> guids)
        {
            if (guids == null || guids.Count() == 0)
                throw new ArgumentNullException("guids", "Must provide guids to get persons");

            // verify the user has the permission to view any person
            CheckUserPersonViewPermissions();

            var personEntities = await _personRepository.GetPersonByGuidNonCachedAsync(guids);

            if (personEntities == null || personEntities.Count() == 0)
                throw new KeyNotFoundException("Person guids not found in repository");

            var personDtos = new List<Dtos.Person>();
            foreach (var personEntity in personEntities)
            {
                personDtos.Add(await ConvertPersonEntityToDtoAsync(personEntity, false));
            }

            return personDtos;
        }

        /// <summary>
        /// Gets paged persons credentials
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        public async Task<Tuple<IEnumerable<Dtos.PersonCredential>, int>> GetAllPersonCredentialsAsync(int offset, int limit, bool bypassCache)
        {
            //verify the user has the permission to view any person
            CheckUserPersonViewPermissions();

            Tuple<IEnumerable<string>, int> personIds = await _personRepository.GetPersonGuidsAsync(offset, limit, bypassCache);
            if (personIds == null)
            {
                return new Tuple<IEnumerable<Dtos.PersonCredential>, int>(null, 0);
            }

            var personEntities = await _personRepository.GetPersonIntegrationByGuidNonCachedAsync(personIds.Item1);

            List<Dtos.PersonCredential> personCredentials = new List<PersonCredential>();

            foreach (var personEntity in personEntities)
            {
                var personCredentialDtos = new Dtos.PersonCredential();
                personCredentialDtos.Id = personEntity.Guid;
                personCredentialDtos.Credentials = await GetPersonCredentials(personEntity);
                personCredentials.Add(personCredentialDtos);
            }
            return personCredentials.Any() ? new Tuple<IEnumerable<Dtos.PersonCredential>, int>(personCredentials, personIds.Item2) : null;
        }

        /// <summary>
        /// Get a person credentials by guid without using a cache.
        /// </summary>
        /// <param name="guids">A global identifier of a person.</param>
        /// <returns>The <see cref="Dtos.PersonCredential">person credentials</see></returns>
        public async Task<Dtos.PersonCredential> GetPersonCredentialByGuidAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException("id", "Must provide id to get person credential");

            // verify the user has the permission to view any person
            CheckUserPersonViewPermissions();

            var personEntity = await _personRepository.GetPersonByGuidNonCachedAsync(id);

            if (personEntity == null)
                throw new KeyNotFoundException("Person id not found in repository");

            var personDtos = new Dtos.PersonCredential();
            personDtos.Id = id;
            personDtos.Credentials = await GetPersonCredentials(personEntity);

            return personDtos;
        }

        /// <summary>
        /// Gets paged persons credentials
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        public async Task<Tuple<IEnumerable<Dtos.PersonCredential2>, int>> GetAllPersonCredentials2Async(int offset, int limit, bool bypassCache)
        {
            try
            {
                //verify the user has the permission to view any person
                CheckUserPersonViewPermissions();

                Tuple<IEnumerable<string>, int> personIds = await _personRepository.GetPersonGuidsAsync(offset, limit, bypassCache);
                if (personIds == null)
                {
                    return new Tuple<IEnumerable<Dtos.PersonCredential2>, int>(new List<PersonCredential2>(), 0);
                }

                await this.GetPersonPins(personIds.Item1.ToArray());

                var personEntities = await _personRepository.GetPersonIntegrationByGuidNonCachedAsync(personIds.Item1);

                List<Dtos.PersonCredential2> personCredentials = new List<PersonCredential2>();

                foreach (var personEntity in personEntities)
                {
                    var personCredentialDtos = new Dtos.PersonCredential2();
                    personCredentialDtos.Id = personEntity.Guid;
                    personCredentialDtos.Credentials = await GetPersonCredentials2(personEntity);
                    personCredentials.Add(personCredentialDtos);
                }
                return personCredentials.Any() ? new Tuple<IEnumerable<Dtos.PersonCredential2>, int>(personCredentials, personIds.Item2) :
                                                 new Tuple<IEnumerable<Dtos.PersonCredential2>, int>(new List<PersonCredential2>(), 0);
            }
            catch (RepositoryException rex)
            {
                logger.Error(rex.Message);
                throw rex;
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                throw ex;
            }
        }

        /// <summary>
        /// Get a person credentials by guid without using a cache.
        /// </summary>
        /// <param name="guids">A global identifier of a person.</param>
        /// <returns>The <see cref="Dtos.PersonCredential">person credentials</see></returns>
        public async Task<Dtos.PersonCredential2> GetPersonCredential2ByGuidAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
                throw new ArgumentNullException("id", "Must provide id to get person credential");

            // verify the user has the permission to view any person
            CheckUserPersonViewPermissions();

            var personEntity = await _personRepository.GetPersonByGuidNonCachedAsync(id);

            if (personEntity == null)
                throw new KeyNotFoundException("Person id not found in repository");

            await this.GetPersonPins(new string[] { personEntity.Guid });

            var personDtos = new Dtos.PersonCredential2();
            personDtos.Id = id;
            personDtos.Credentials = await GetPersonCredentials2(personEntity);

            return personDtos;
        }

        /// <summary>
        /// Gets person credentials
        /// </summary>
        /// <param name="person"></param>
        /// <returns></returns>
        private async Task<IEnumerable<Dtos.DtoProperties.CredentialDtoProperty2>> GetPersonCredentials2(
            Domain.Base.Entities.Person person)
        {
            // Colleague Person ID
            var credentials = new List<Dtos.DtoProperties.CredentialDtoProperty2>()
            {
                new Dtos.DtoProperties.CredentialDtoProperty2()
                {
                    Type = Dtos.EnumProperties.CredentialType2.ColleaguePersonId,
                    Value = person.Id
                }
            };
            // Elevate ID
            if (person.PersonAltIds != null && person.PersonAltIds.Count() > 0)
            {
                //Produce an error if there are more than one elevate id's, it means bad data
                if (person.PersonAltIds.Count(altId => altId.Type.Equals("ELEV", StringComparison.OrdinalIgnoreCase)) > 1)
                {
                    throw new InvalidOperationException(string.Format("Person ID '{0}': You cannot have more than one elevate id.", person.Id));
                }
                var elevPersonAltId =
                    person.PersonAltIds.FirstOrDefault(
                        a => a.Type == Domain.Base.Entities.PersonAlt.ElevatePersonAltType);
                if (elevPersonAltId != null && !string.IsNullOrEmpty(elevPersonAltId.Id))
                {
                    credentials.Add(new Dtos.DtoProperties.CredentialDtoProperty2()
                    {
                        Type = Dtos.EnumProperties.CredentialType2.ElevateID,
                        Value = elevPersonAltId.Id
                    });
                }
            }
            // SSN
            if (!string.IsNullOrEmpty(person.GovernmentId))
            {
                var type = Dtos.EnumProperties.CredentialType2.Sin;
                var countryCode = await _personRepository.GetHostCountryAsync();
                if (countryCode.Equals("USA", StringComparison.OrdinalIgnoreCase))
                {
                    type = Dtos.EnumProperties.CredentialType2.Ssn;
                }
                credentials.Add(new Dtos.DtoProperties.CredentialDtoProperty2()
                {
                    Type = type,
                    Value = person.GovernmentId
                });
            }
            //PERSON.PIN
            if (_personPins != null)
            {
                var personPinEntity = _personPins.FirstOrDefault(i => i.PersonId.Equals(person.Id, StringComparison.OrdinalIgnoreCase));
                if (personPinEntity != null)
                {
                    credentials.Add(new Dtos.DtoProperties.CredentialDtoProperty2()
                    {
                        Type = Dtos.EnumProperties.CredentialType2.ColleagueUserName,
                        Value = personPinEntity.PersonPinUserId
                    });
                }
            }           
            return credentials;
        }

        /// <summary>
        /// Get all person pins
        /// </summary>
        public IEnumerable<PersonPin> _personPins { get; set; }
        private async Task<IEnumerable<PersonPin>> GetPersonPins(string[] guids)
        {
            if (_personPins == null)
            {
                _personPins = await _personRepository.GetPersonPinsAsync(guids);
            }
            return _personPins;
        }

        /// <summary>
        /// Get a list of persons by guid without using a cache.
        /// </summary>
        /// <param name="guids">List of person guids</param>
        /// <returns>List of <see cref="Domain.Base.Entities.Person">persons</see></returns>
        public async Task<IEnumerable<Dtos.Person>> GetPersonMatchesByGuidNonCachedAsync(IEnumerable<string> guids)
        {
            if (guids == null || guids.Count() == 0)
                throw new ArgumentNullException("guids", "Must provide guids to get persons");

            var personEntities = await _personRepository.GetPersonByGuidNonCachedAsync(guids);

            if (personEntities == null || personEntities.Count() == 0)
                throw new KeyNotFoundException("Person guids not found in repository");

            var personDtos = new List<Dtos.Person>();
            foreach (var personEntity in personEntities)
            {
                personDtos.Add(await ConvertPersonEntityToDtoAsync(personEntity, false));
            }

            return personDtos;
        }

        /// <summary>
        /// Get person data associated with a particular role.
        /// </summary>
        /// <returns>List of <see cref="Dtos.Person">persons</see></returns>
        public async Task<Tuple<IEnumerable<Dtos.Person>, int>> GetPersonsByRoleNonCachedAsync(int offset, int limit,
            string role)
        {
            if (string.IsNullOrEmpty(role) || (role != Dtos.RoleType.Faculty.ToString()))
                throw new ArgumentException("Unsupported person role");

            // verify the user has the permission to view any person
            CheckUserPersonViewPermissions();

            var personDtos = new List<Dtos.Person>();
            var totalcount = new int();
            if (role == Dtos.RoleType.Faculty.ToString())
            {
                // get all faculty guids
                var facultyTuple = await _personRepository.GetFacultyPersonGuidsAsync(offset, limit);

                if (facultyTuple.Item1 != null && facultyTuple.Item1.Any())
                {

                    totalcount = facultyTuple.Item2;
                    var personEntities = await _personRepository.GetPersonByGuidNonCachedAsync(facultyTuple.Item1);
                    foreach (var personEntity in personEntities)
                    {
                        // get all faculty person object with no addresses and phones
                        personDtos.Add(await ConvertPersonEntityToDtoAsync(personEntity, true));
                    }
                }
            }

            return new Tuple<IEnumerable<Dtos.Person>, int>(personDtos, totalcount);

        }

        /// <summary>
        /// Get profile information for a person
        /// </summary>
        /// <param name="id">Person Id</param>
        /// <returns><see cref="Profile">Profile</see> DTO object</returns>
        public async Task<Dtos.Base.Profile> GetProfileAsync(string personId, bool useCache = true)
        {
            if (string.IsNullOrEmpty(personId))
            {
                throw new ArgumentNullException("personId", "A person's id is required to retrieve profile information");
            }

            // For now, profile information may only be viewed by that person. 
            if (!(await UserCanViewProfileForPerson(personId)))
            {
                string message = CurrentUser + " cannot view profile for person " + personId;
                logger.Info(message);
                throw new PermissionsException();
            }

            Domain.Base.Entities.Profile profileEntity = await _profileRepository.GetProfileAsync(personId, useCache);
            if (profileEntity == null)
            {
                throw new Exception("Profile information could not be retrieved for person " + personId);
            }

            var profileDtoAdapter = _adapterRegistry.GetAdapter<Domain.Base.Entities.Profile, Dtos.Base.Profile>();
            Dtos.Base.Profile profileDto = profileDtoAdapter.MapToType(profileEntity);

            return profileDto;
        }

        #endregion

        #region Get HEDM V6 Person Methods

        /// <summary>
        /// Get a person by guid without using a cache.
        /// </summary>
        /// <param name="guid">Guid of the person in Colleague.</param>
        /// <returns>The <see cref="Dtos.Person2">person</see></returns>
        public async Task<Dtos.Person2> GetPerson2ByGuidNonCachedAsync(string guid)
        {
            if (string.IsNullOrEmpty(guid))
                throw new ArgumentNullException("guid", "Must provide a guid to get a person");

            // get the person ID associated with the incoming guid
            var personId = await _personRepository.GetPersonIdFromGuidNoCofilesAsync(guid);
            if (string.IsNullOrEmpty(personId))
                throw new KeyNotFoundException("Person ID associated to guid '" + guid + "' not found in repository");

            // verify the user has the permission to view themselves or any person
            CheckUserPersonViewPermissions(personId);

            var personEntity = await _personRepository.GetPersonIntegrationByGuidNonCachedAsync(guid);

            if (personEntity == null)
                throw new KeyNotFoundException("Person '" + guid + "' not found in repository");
            if ((!string.IsNullOrEmpty(personEntity.PersonCorpIndicator)) &&
                    (personEntity.PersonCorpIndicator.Equals("Y", StringComparison.InvariantCultureIgnoreCase)))
                throw new KeyNotFoundException(string.Format("'{0}' belongs to organization or educational institution. Can not be retrieved.", guid));

            return await ConvertPerson2EntityToDtoAsync(personEntity, false);
        }

        /// <summary>
        /// Get a list of persons by guid without using a cache.
        /// </summary>
        /// <param name="guids">List of person guids</param>
        /// <returns>List of <see cref="Dtos.Person2">persons</see></returns>
        public async Task<IEnumerable<Dtos.Person2>> GetPerson2ByGuidNonCachedAsync(IEnumerable<string> guids)
        {
            if (guids == null || guids.Count() == 0)
                throw new ArgumentNullException("guids", "Must provide guids to get persons");

            // verify the user has the permission to view any person
            CheckUserPersonViewPermissions();

            var personEntities = await _personRepository.GetPersonIntegrationByGuidNonCachedAsync(guids);

            if (personEntities == null || personEntities.Count() == 0)
                throw new KeyNotFoundException("Person guids not found in repository");

            var personDtos = new List<Dtos.Person2>();
            foreach (var personEntity in personEntities)
            {
                personDtos.Add(await ConvertPerson2EntityToDtoAsync(personEntity, false));
            }

            return personDtos;
        }

        /// <summary>
        /// Get a list of persons by guid without using a cache.
        /// </summary>
        /// <param name="guids">List of person guids</param>
        /// <returns>List of <see cref="Dtos.Person3">persons</see></returns>
        public async Task<IEnumerable<Dtos.Person3>> GetPerson3ByGuidNonCachedAsync(IEnumerable<string> guids)
        {
            if (guids == null || guids.Count() == 0)
                throw new ArgumentNullException("guids", "Must provide guids to get persons");

            // verify the user has the permission to view any person
            CheckUserPersonViewPermissions();

            var personEntities = await _personRepository.GetPersonIntegrationByGuidNonCachedAsync(guids);

            if (personEntities == null || personEntities.Count() == 0)
                throw new KeyNotFoundException("Person guids not found in repository");

            var personDtos = new List<Dtos.Person3>();
            foreach (var personEntity in personEntities)
            {
                personDtos.Add(await ConvertPerson3EntityToDtoAsync(personEntity, false));
            }

            return personDtos;
        }

        /// <summary>
        /// Get a person by guid using a cache.
        /// </summary>
        /// <param name="guid">Guid of the person in Colleague.</param>
        /// <param name="bypassCache"></param>
        /// <returns>The <see cref="Dtos.Person2">person</see></returns>
        public async Task<Dtos.Person2> GetPerson2ByGuidAsync(string guid, bool bypassCache)
        {
            if (string.IsNullOrEmpty(guid))
                throw new ArgumentNullException("guid", "Must provide a guid to get a person");

            // get the person ID associated with the incoming guid
            var personId = await _personRepository.GetPersonIdFromGuidNoCofilesAsync(guid);
            if (string.IsNullOrEmpty(personId))
                throw new KeyNotFoundException("Person ID associated to guid '" + guid + "' not found in repository");

            // verify the user has the permission to view themselves or any person
            CheckUserPersonViewPermissions(personId);

            var personEntity = await _personRepository.GetPersonIntegrationByGuidAsync(guid, bypassCache);

            if (personEntity == null)
                throw new KeyNotFoundException("Person '" + guid + "' not found in repository");

            if ((!string.IsNullOrEmpty(personEntity.PersonCorpIndicator)) &&
                    (personEntity.PersonCorpIndicator.Equals("Y", StringComparison.InvariantCultureIgnoreCase)))
                throw new KeyNotFoundException(string.Format("'{0}' belongs to organization or educational institution. Can not be retrieved.", guid));

            return await ConvertPerson2EntityToDtoAsync(personEntity, false, bypassCache);
        }


        /// <summary>
        /// Get person data associated with a particular role.
        /// </summary>
        /// <returns>List of <see cref="Dtos.Person">persons</see></returns>
        public async Task<Tuple<IEnumerable<Dtos.Person2>, int>> GetPersons2ByRoleNonCachedAsync(int offset, int limit,
            string role)
        {
            if (string.IsNullOrEmpty(role) ||
                (role != Dtos.EnumProperties.PersonRoleType.Instructor.ToString().ToLower()))
            {
                return await GetPerson2NonCachedAsync(offset, limit, true, "", "", "", "", "", "", "", role, "", "", "");
                //throw new ArgumentException("Unsupported person role");
            }

            // verify the user has the permission to view any person
            CheckUserPersonViewPermissions();

            var personDtos = new List<Dtos.Person2>();
            var totalcount = new int();
            if (role.ToLower() == Dtos.EnumProperties.PersonRoleType.Instructor.ToString().ToLower())
            {
                // get all faculty guids
                var facultyTuple = await _personRepository.GetFacultyPersonGuidsAsync(offset, limit);

                if (facultyTuple.Item1 != null && facultyTuple.Item1.Any())
                {

                    totalcount = facultyTuple.Item2;
                    var personEntities =
                        await _personRepository.GetPersonIntegrationByGuidNonCachedAsync(facultyTuple.Item1);
                    foreach (var personEntity in personEntities)
                    {
                        // get all faculty person object with no addresses and phones
                        personDtos.Add(await ConvertPerson2EntityToDtoAsync(personEntity, true));
                    }
                }
            }

            return new Tuple<IEnumerable<Dtos.Person2>, int>(personDtos, totalcount);
        }

        /// <summary>
        /// Get person data associated with a particular role.
        /// </summary>
        /// <param name="Offset">Paging offset</param>
        /// <param name="Limit">Paging limit</param>
        /// <param name="bypassCache">Flag to bypass cache</param>
        /// <param name="title">Specific title</param>
        /// <param name="firstName">Specific first name</param>
        /// <param name="middleName">Specific middle name</param>
        /// <param name="lastNamePrefix">Last name beings with</param>
        /// <param name="lastName">Specific last name</param>
        /// <param name="pedigree">Specific suffix</param>
        /// <param name="preferredName">Specific preferred name</param>
        /// <param name="role">Specific role of a person</param>
        /// <param name="credentialType">Credential type of either colleagueId or ssn</param>
        /// <param name="credentialValue">Specific value of the credential to be evaluated</param>
        /// <param name="personFilter">Person Saved List selection or list name from person-filters</param>
        /// <returns>List of <see cref="Dtos.Person">persons</see></returns>
        public async Task<Tuple<IEnumerable<Dtos.Person2>, int>> GetPerson2NonCachedAsync(int offset, int limit, bool bypassCache,
            string title, string firstName, string middleName, string lastNamePrefix, string lastName, string pedigree,
            string preferredName, string role, string credentialType, string credentialValue, string personFilter)
        {
            // verify the user has the permission to view any person
            CheckUserPersonViewPermissions();

            var personDtos = new List<Dtos.Person2>();
            var totalcount = new int();

            // get all faculty guids
            var filteredTuple = await _personRepository.GetFilteredPersonGuidsAsync(offset, limit, bypassCache,
                title, firstName, middleName, lastNamePrefix, lastName, pedigree,
                preferredName, role, credentialType, credentialValue, personFilter);

            if (filteredTuple.Item1 != null && filteredTuple.Item1.Any())
            {

                totalcount = filteredTuple.Item2;
                var personEntities = await _personRepository.GetPersonIntegrationByGuidNonCachedAsync(filteredTuple.Item1);
                foreach (var personEntity in personEntities)
                {
                    // get all faculty person object with no addresses and phones
                    personDtos.Add(await ConvertPerson2EntityToDtoAsync(personEntity, false, bypassCache));
                }
            }

            return new Tuple<IEnumerable<Dtos.Person2>, int>(personDtos, totalcount);
        }

        /// <summary>
        /// Get a person by guid using a cache.
        /// </summary>
        /// <param name="guid">Guid of the person in Colleague.</param>
        /// <param name="bypassCache"></param>
        /// <returns>The <see cref="Dtos.Person3">person</see></returns>
        public async Task<Dtos.Person3> GetPerson3ByGuidAsync(string guid, bool bypassCache)
        {
            if (string.IsNullOrEmpty(guid))
                throw new ArgumentNullException("guid", "Must provide a guid to get a person");

            // get the person ID associated with the incoming guid
            var personId = await _personRepository.GetPersonIdFromGuidNoCofilesAsync(guid);
            
            if (string.IsNullOrEmpty(personId))
                throw new KeyNotFoundException("Person ID associated to guid '" + guid + "' not found in repository");

            // verify the user has the permission to view themselves or any person
            CheckUserPersonViewPermissions(personId);

            var personEntity = await _personRepository.GetPersonIntegrationByGuidAsync(guid, bypassCache);

            if (personEntity == null)
                throw new KeyNotFoundException("Person '" + guid + "' not found in repository");

            if ((!string.IsNullOrEmpty(personEntity.PersonCorpIndicator)) &&
                    (personEntity.PersonCorpIndicator.Equals("Y", StringComparison.InvariantCultureIgnoreCase)))
                throw new KeyNotFoundException(string.Format("'{0}' belongs to organization or educational institution. Can not be retrieved.", guid));


            return await ConvertPerson3EntityToDtoAsync(personEntity, false, bypassCache);
            
        }


        /// <summary>
        /// Get person data associated with a particular role.
        /// </summary>
        /// <returns>List of <see cref="Dtos.Person3">persons</see></returns>
        public virtual async Task<Tuple<IEnumerable<Dtos.Person3>, int>> GetPersons3ByRoleNonCachedAsync(int offset, int limit,
            string role)
        {
            if (string.IsNullOrEmpty(role) ||
                (role != Dtos.EnumProperties.PersonRoleType.Instructor.ToString().ToLower()))
            {
                return await GetPerson3NonCachedAsync(offset, limit, true, "", "", "", "", "", "", "", role, "", "", "");
                //throw new ArgumentException("Unsupported person role");
            }

            // verify the user has the permission to view any person
            CheckUserPersonViewPermissions();

            var personDtos = new List<Dtos.Person3>();
            var totalcount = new int();
            if (role.ToLower() == Dtos.EnumProperties.PersonRoleType.Instructor.ToString().ToLower())
            {
                // get all faculty guids
                var facultyTuple = await _personRepository.GetFacultyPersonGuidsAsync(offset, limit);

                if (facultyTuple.Item1 != null && facultyTuple.Item1.Any())
                {

                    totalcount = facultyTuple.Item2;
                    var personEntities =
                        await _personRepository.GetPersonIntegrationByGuidNonCachedAsync(facultyTuple.Item1);
                    foreach (var personEntity in personEntities)
                    {
                        // get all faculty person object with no addresses and phones
                        personDtos.Add(await ConvertPerson3EntityToDtoAsync(personEntity, true));
                    }
                }
            }

            return new Tuple<IEnumerable<Dtos.Person3>, int>(personDtos, totalcount);
        }

        /// <summary>
        /// Get person data associated with a particular role.
        /// </summary>
        /// <param name="Offset">Paging offset</param>
        /// <param name="Limit">Paging limit</param>
        /// <param name="bypassCache">Flag to bypass cache</param>
        /// <param name="title">Specific title</param>
        /// <param name="firstName">Specific first name</param>
        /// <param name="middleName">Specific middle name</param>
        /// <param name="lastNamePrefix">Last name beings with</param>
        /// <param name="lastName">Specific last name</param>
        /// <param name="pedigree">Specific suffix</param>
        /// <param name="preferredName">Specific preferred name</param>
        /// <param name="role">Specific role of a person</param>
        /// <param name="credentialType">Credential type of either colleagueId or ssn</param>
        /// <param name="credentialValue">Specific value of the credential to be evaluated</param>
        /// <param name="personFilter">Person Saved List selection or list name from person-filters</param>
        /// <returns>List of <see cref="Dtos.Person3">persons</see></returns>
        public async Task<Tuple<IEnumerable<Dtos.Person3>, int>> GetPerson3NonCachedAsync(int offset, int limit, bool bypassCache,
            string title, string firstName, string middleName, string lastNamePrefix, string lastName, string pedigree,
            string preferredName, string role, string credentialType, string credentialValue, string personFilter)
        {
            // verify the user has the permission to view any person
            CheckUserPersonViewPermissions();

            var personDtos = new List<Dtos.Person3>();
            var totalcount = new int();

            // get all faculty guids
            var filteredTuple = await _personRepository.GetFilteredPersonGuidsAsync(offset, limit, bypassCache,
                title, firstName, middleName, lastNamePrefix, lastName, pedigree,
                preferredName, role, credentialType, credentialValue, personFilter);

            if (filteredTuple.Item1 != null && filteredTuple.Item1.Any())
            {

                totalcount = filteredTuple.Item2;
                var personEntities = await _personRepository.GetPersonIntegrationByGuidNonCachedAsync(filteredTuple.Item1);
                foreach (var personEntity in personEntities)
                {
                    // get all faculty person object with no addresses and phones
                    personDtos.Add(await ConvertPerson3EntityToDtoAsync(personEntity, false, bypassCache));
                }
            }

            return new Tuple<IEnumerable<Dtos.Person3>, int>(personDtos, totalcount);
        }

        #endregion

        #region Create Person Methods

        /// <summary>
        /// Create a person.
        /// </summary>
        /// <param name="person">The <see cref="Domain.Base.Entities.Person">person</see> entity to create in the database.</param>
        /// <returns>The newly created <see cref="Domain.Base.Entities.Person">person</see></returns>
        public async Task<Dtos.Person> CreatePersonAsync(Dtos.Person personDto)
        {
            if (personDto == null)
                throw new ArgumentNullException("personDto", "Must provide a person object for creation");
            if (string.IsNullOrEmpty(personDto.Guid))
                throw new ArgumentNullException("personDto", "Must provide a guid for person creation");

            // verify the user has the permission to create a person
            CheckUserPersonCreatePermissions();

            // map the person DTO to entities
            var personEntity = await ConvertPersonDtoToEntityAsync(null, personDto);
            var addressEntities = await ConvertPersonDtoToAddressEntitiesAsync(personDto);
            var phoneEntities = ConvertPersonDtoToPhoneEntities(personDto);

            // create the person entity in the database
            var createdPersonEntity = await _personRepository.CreateAsync(personEntity, addressEntities, phoneEntities);

            // return the newly created person
            return await ConvertPersonEntityToDtoAsync(createdPersonEntity, false);
        }

        /// <summary>
        /// Create a person.
        /// </summary>
        /// <param name="personDto">The <see cref="Dtos.Person2">person</see> entity to create in the database.</param>
        /// <returns>The newly created <see cref="Dtos.Person2">person</see></returns>
        public async Task<Dtos.Person2> CreatePerson2Async(Dtos.Person2 personDto)
        {
            if (personDto == null)
                throw new ArgumentNullException("personDto", "Must provide a person object for creation");
            if (string.IsNullOrEmpty(personDto.Id))
                throw new ArgumentNullException("personDto", "Must provide a guid for person creation");

            PersonIntegration createdPersonEntity = null;
            // verify the user has the permission to create a person
            CheckUserPersonCreatePermissions();

            try
            {
                // map the person DTO to entities
                var personEntity = await ConvertPerson2DtoToEntityAsync(null, personDto);
                var addressEntities = await ConvertAddressDtoToAddressEntitiesAsync(personDto.Addresses);
                var phoneEntities = await ConvertPhoneDtoToPhoneEntities(personDto.Phones);

                // create the person entity in the database
                createdPersonEntity = await _personRepository.Create2Async(personEntity, addressEntities, phoneEntities);
            }
            catch (RepositoryException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }

            // return the newly created person
            return await ConvertPerson2EntityToDtoAsync(createdPersonEntity, false);
        }

        /// <summary>
        /// Create a person.
        /// </summary>
        /// <param name="personDto">The <see cref="Dtos.Person3">person</see> entity to create in the database.</param>
        /// <returns>The newly created <see cref="Dtos.Person3">person</see></returns>
        public async Task<Dtos.Person3> CreatePerson3Async(Dtos.Person3 personDto)
        {
            if (personDto == null)
                throw new ArgumentNullException("personDto", "Must provide a person object for creation");
            if (string.IsNullOrEmpty(personDto.Id))
                throw new ArgumentNullException("personDto", "Must provide a guid for person creation");

            PersonIntegration createdPersonEntity = null;
            // verify the user has the permission to create a person
            CheckUserPersonCreatePermissions();

            try
            {
                // map the person DTO to entities
                var personEntity = await ConvertPerson3DtoToEntityAsync(null, personDto);
                var addressEntities = await ConvertAddressDtoToAddressEntitiesAsync(personDto.Addresses);
                var phoneEntities = await ConvertPhoneDtoToPhoneEntities(personDto.Phones);

                // create the person entity in the database
                createdPersonEntity = await _personRepository.Create2Async(personEntity, addressEntities, phoneEntities);
            }
            catch (RepositoryException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }

            // return the newly created person
            return await ConvertPerson3EntityToDtoAsync(createdPersonEntity, false);
        }

        #endregion

        #region Update Person Methods

        /// <summary>
        /// Update a person.
        /// </summary>
        /// <param name="person">The <see cref="Domain.Base.Entities.Person">person</see> entity to update in the database.</param>
        /// <returns>The newly updated <see cref="Domain.Base.Entities.Person">person</see></returns>
        public async Task<Dtos.Person> UpdatePersonAsync(Dtos.Person personDto)
        {
            if (personDto == null)
                throw new ArgumentNullException("personDto", "Must provide a person for update");
            if (string.IsNullOrEmpty(personDto.Guid))
                throw new ArgumentNullException("personDto", "Must provide a guid for person update");

            // get the person ID associated with the incoming guid
            var personId = await _personRepository.GetPersonIdFromGuidAsync(personDto.Guid);

            // verify the GUID exists to perform an update.  If not, perform a create instead
            if (!string.IsNullOrEmpty(personId))
            {
                // verify the user has the permission to update a person
                CheckUserPersonUpdatePermissions(personId);

                // map the person DTO to entities
                var personEntity = await ConvertPersonDtoToEntityAsync(personId, personDto);
                var addressEntites = await ConvertPersonDtoToAddressEntitiesAsync(personDto);
                var phoneEntities = ConvertPersonDtoToPhoneEntities(personDto);

                // update the person in the database
                var updatedPersonEntity =
                    await _personRepository.UpdateAsync(personEntity, addressEntites, phoneEntities);

                // return the newly updated person
                return await ConvertPersonEntityToDtoAsync(updatedPersonEntity, false);
            }
            else
            {
                // perform a create instead
                return await CreatePersonAsync(personDto);
            }
        }

        /// <summary>
        /// Update a person.
        /// </summary>
        /// <param name="personDto">The <see cref="Dtos.Person2">person</see> entity to update in the database.</param>
        /// <returns>The newly updated <see cref="Dtos.Person2">person</see></returns>
        public async Task<Dtos.Person2> UpdatePerson2Async(Dtos.Person2 personDto)
        {
            if (personDto == null)
                throw new ArgumentNullException("personDto", "Must provide a person for update");
            if (string.IsNullOrEmpty(personDto.Id))
                throw new ArgumentNullException("personDto", "Must provide a guid for person update");

            // get the person ID associated with the incoming guid
            var personId = await _personRepository.GetPersonIdFromGuidAsync(personDto.Id);

            // verify the GUID exists to perform an update.  If not, perform a create instead
            if (!string.IsNullOrEmpty(personId))
            {
                try
                {
                    // verify the user has the permission to update a person
                    CheckUserPersonUpdatePermissions(personId);

                    // map the person DTO to entities
                    var personEntity = await ConvertPerson2DtoToEntityAsync(personId, personDto);
                    var addressEntites = await ConvertAddressDtoToAddressEntitiesAsync(personDto.Addresses);
                    var phoneEntities = await ConvertPhoneDtoToPhoneEntities(personDto.Phones);

                    // update the person in the database
                    var updatedPersonEntity =
                        await _personRepository.Update2Async(personEntity, addressEntites, phoneEntities);

                    // return the newly updated person
                    return await ConvertPerson2EntityToDtoAsync(updatedPersonEntity, false);

                }
                catch (RepositoryException ex)
                {
                    throw ex;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
            else
            {
                // perform a create instead
                return await CreatePerson2Async(personDto);
            }
        }


        /// <summary>
        /// Update a person.
        /// </summary>
        /// <param name="personDto">The <see cref="Dtos.Person3">person</see> entity to update in the database.</param>
        /// <returns>The newly updated <see cref="Dtos.Person3">person</see></returns>
        public async Task<Dtos.Person3> UpdatePerson3Async(Dtos.Person3 personDto)
        {
            if (personDto == null)
                throw new ArgumentNullException("personDto", "Must provide a person for update");
            if (string.IsNullOrEmpty(personDto.Id))
                throw new ArgumentNullException("personDto", "Must provide a guid for person update");

            // get the person ID associated with the incoming guid
            var personId = await _personRepository.GetPersonIdFromGuidAsync(personDto.Id);

            // verify the GUID exists to perform an update.  If not, perform a create instead
            if (!string.IsNullOrEmpty(personId))
            {
                try
                {
                    // verify the user has the permission to update a person
                    CheckUserPersonUpdatePermissions(personId);

                    // map the person DTO to entities
                    var personEntity = await ConvertPerson3DtoToEntityAsync(personId, personDto);
                    var addressEntites = await ConvertAddressDtoToAddressEntitiesAsync(personDto.Addresses);
                    var phoneEntities = await ConvertPhoneDtoToPhoneEntities(personDto.Phones);

                    // update the person in the database
                    var updatedPersonEntity =
                        await _personRepository.Update2Async(personEntity, addressEntites, phoneEntities);

                    // return the newly updated person
                    return await ConvertPerson3EntityToDtoAsync(updatedPersonEntity, false);

                }
                catch (RepositoryException ex)
                {
                    throw ex;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex.InnerException);
                }
            }
            else
            {
                // perform a create instead
                return await CreatePerson3Async(personDto);
            }
        }

        /// <summary>
        /// Updates a person's profile data.
        /// </summary>
        /// <param name="profileDto">Profile dto to update</param>
        /// <returns>The updated Profile dto</returns>
        [Obsolete("Obsolete as of API 1.16. Use version 2 of this method instead.")]
        public async Task<Dtos.Base.Profile> UpdateProfileAsync(Dtos.Base.Profile profileDto)
        {
            if (profileDto == null)
                throw new ArgumentNullException("profileDto", "Must provide a profile for update");

            if (string.IsNullOrEmpty(profileDto.Id))
                throw new ArgumentNullException("profileDto", "Must provide an ID for profile update");

            var profileDtoToEntityAdapter =
                _adapterRegistry.GetAdapter<Dtos.Base.Profile, Domain.Base.Entities.Profile>();
            Domain.Base.Entities.Profile profileEntity = null;
            try
            {
                profileEntity = profileDtoToEntityAdapter.MapToType(profileDto);
            }
            catch (Exception ex)
            {
                // The adapter will throw an exception if anything about the data is not acceptable to domain rules
                logger.Error("Error occurred converting profile dto to entity: " + ex.Message);
                throw;
            }

            var repoProfile = await _profileRepository.GetProfileAsync(profileEntity.Id, false);
            var configuration = await _configurationRepository.GetUserProfileConfigurationAsync();
            var userPermissions = GetUserPermissionCodes();
            ProfileProcessor.InitializeLogger(logger);
            bool isProfileChanged = false;
            Domain.Base.Entities.Profile verifiedProfile = ProfileProcessor.VerifyProfileUpdate(profileEntity,
                repoProfile, configuration, CurrentUser, userPermissions, out isProfileChanged);

            var profileEntityToDtoAdapter =
                _adapterRegistry.GetAdapter<Domain.Base.Entities.Profile, Dtos.Base.Profile>();
            Domain.Base.Entities.Profile returnedProfileEntity = null;

            // If changes detected, update the profile and return the updated item. If no changes detected, return the profile retrieved from the repository
            if (isProfileChanged)
            {
                try
                {
                    returnedProfileEntity = await _profileRepository.UpdateProfileAsync(verifiedProfile);
                }
                catch (Exception ex)
                {
                    logger.Error("Error occurred during profile update: " + ex.Message);
                    throw;
                }
            }
            else
            {
                returnedProfileEntity = repoProfile;
            }

            try
            {
                return profileEntityToDtoAdapter.MapToType(returnedProfileEntity);
            }
            catch (Exception ex)
            {
                logger.Error("Error occurred converting profile entity to dto: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Updates a person's profile data.
        /// </summary>
        /// <param name="profileDto">Profile dto to update</param>
        /// <returns>The updated Profile dto</returns>
        public async Task<Dtos.Base.Profile> UpdateProfile2Async(Dtos.Base.Profile profileDto)
        {
            if (profileDto == null)
                throw new ArgumentNullException("profileDto", "Must provide a profile for update");

            if (string.IsNullOrEmpty(profileDto.Id))
                throw new ArgumentNullException("profileDto", "Must provide an ID for profile update");

            var profileDtoToEntityAdapter =
                _adapterRegistry.GetAdapter<Dtos.Base.Profile, Domain.Base.Entities.Profile>();
            Domain.Base.Entities.Profile profileEntity = null;
            try
            {
                profileEntity = profileDtoToEntityAdapter.MapToType(profileDto);
            }
            catch (Exception ex)
            {
                // The adapter will throw an exception if anything about the data is not acceptable to domain rules
                logger.Error("Error occurred converting profile dto to entity: " + ex.Message);
                throw;
            }

            var repoProfile = await _profileRepository.GetProfileAsync(profileEntity.Id, false);
            var configuration = await _configurationRepository.GetUserProfileConfiguration2Async();
            var userPermissions = GetUserPermissionCodes();
            ProfileProcessor.InitializeLogger(logger);
            bool isProfileChanged = false;
            Domain.Base.Entities.Profile verifiedProfile = ProfileProcessor.VerifyProfileUpdate2(profileEntity,
                repoProfile, configuration, CurrentUser, userPermissions, out isProfileChanged);

            var profileEntityToDtoAdapter =
                _adapterRegistry.GetAdapter<Domain.Base.Entities.Profile, Dtos.Base.Profile>();
            Domain.Base.Entities.Profile returnedProfileEntity = null;

            // If changes detected, update the profile and return the updated item. If no changes detected, return the profile retrieved from the repository
            if (isProfileChanged)
            {
                try
                {
                    returnedProfileEntity = await _profileRepository.UpdateProfileAsync(verifiedProfile);
                }
                catch (Exception ex)
                {
                    logger.Error("Error occurred during profile update: " + ex.Message);
                    throw;
                }
            }
            else
            {
                returnedProfileEntity = repoProfile;
            }

            try
            {
                return profileEntityToDtoAdapter.MapToType(returnedProfileEntity);
            }
            catch (Exception ex)
            {
                logger.Error("Error occurred converting profile entity to dto: " + ex.Message);
                throw;
            }
        }

        #endregion

        #region Organization Methods
        //organizations are really persons so these methods are here to reuse code and service logic already in place

        /// <summary>
        /// Get person data associated with a particular role.
        /// </summary>
        /// <param name="Offset">Paging offset</param>
        /// <param name="Limit">Paging limit</param>
        /// <param name="role">Specific role of a person</param>
        /// <param name="credentialType">Credential type of either colleagueId or ssn</param>
        /// <param name="credentialValue">Specific value of the credential to be evaluated</param>
        /// <returns>List of <see cref="Dtos.Person">persons</see></returns>
        public async Task<Tuple<IEnumerable<Dtos.Organization2>, int>> GetOrganizations2Async(int offset, int limit,
            string role, string credentialType, string credentialValue)
        {
            // verify the user has the permission to view any person
            CheckUserOrganizationViewPermissions();

            var orgDtos = new List<Dtos.Organization2>();
            var totalcount = new int();

            // get all faculty guids
            var filteredTuple = await _personRepository.GetFilteredOrganizationGuidsAsync(offset, limit, 
                role, credentialType, credentialValue);

            if (filteredTuple.Item1 != null && filteredTuple.Item1.Any())
            {
                totalcount = filteredTuple.Item2;
                var personEntities = await _personRepository.GetPersonIntegrationByGuidNonCachedAsync(filteredTuple.Item1);
                foreach (var personEntity in personEntities)
                {
                    orgDtos.Add(await ConvertPersonIntegrationEntityToOrganizationDtoAsync(personEntity));
                }
            }

            return new Tuple<IEnumerable<Dtos.Organization2>, int>(orgDtos, totalcount);
        }

        /// <remarks>FOR USE WITH ELLUCIAN Data Model</remarks>
        /// <summary>
        /// Get an organization from its GUID
        /// </summary>
        /// <returns>Organization2 DTO object</returns>
        public async Task<Organization2> GetOrganization2Async(string guid)
        {
            // verify the user has the permission to view any person
            CheckUserOrganizationViewPermissions();

            try
            {
                var personOrg = await _personRepository.GetPersonByGuidNonCachedAsync(guid);

                //make sure the person record retreived is a organization, not just a person
                if (!string.Equals(personOrg.PersonCorpIndicator, "Y", StringComparison.OrdinalIgnoreCase))
                {
                    throw new KeyNotFoundException(string.Concat("Organization not found for id: ", guid));
                }
                
                var personIntgData = await _personRepository.GetPersonIntegrationByGuidNonCachedAsync(guid);

                return await ConvertPersonIntegrationEntityToOrganizationDtoAsync(personIntgData);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                throw new KeyNotFoundException(string.Concat("Organization not found for id: ", guid));
            }
        }

        /// <summary>
        /// Create a Organization.
        /// </summary>
        /// <param name="organizationDto">The <see cref="Organization2">organization</see> entity to create in the database.</param>
        /// <returns>The newly created <see cref="Organization2">organization</see></returns>
        public async Task<Organization2> CreateOrganizationAsync(Organization2 organizationDto)
        {
            if (organizationDto == null)
                throw new ArgumentNullException("organizationDto", "Must provide a organization for create");
            if (string.IsNullOrEmpty(organizationDto.Id))
                throw new ArgumentNullException("organizationDto", "Must provide a guid for organization create, provide nil guid when id is not known");

            try
            {
                // verify the user has the permission to create a organization
                CheckUserOrganizationCreatePermissions();

                // map the organization DTO to entities
                var personEntity = await ConvertOrganization2DtoToPersonEntityAsync(null, organizationDto);
                var addressEntites = await ConvertAddressDtoToAddressEntitiesAsync(organizationDto.Addresses);
                var phoneEntities = await ConvertPhoneDtoToPhoneEntities(organizationDto.Phones);

                // update the organization in the database
                var createdOrganizationEntity = await _personRepository.CreateOrganizationAsync(personEntity, addressEntites, phoneEntities);

                // return the newly updated organization
                return await ConvertPersonIntegrationEntityToOrganizationDtoAsync(createdOrganizationEntity);
            }
            catch (RepositoryException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        /// <summary>
        /// Update a Organization.
        /// </summary>
        /// <param name="organizationDto">The <see cref="Organization2">organization</see> entity to update in the database.</param>
        /// <returns>The newly updated <see cref="Organization2">organization</see></returns>
        public async Task<Organization2> UpdateOrganizationAsync(Organization2 organizationDto)
        {
            if (organizationDto == null)
                throw new ArgumentNullException("organizationDto", "Must provide a organization for update");
            if (string.IsNullOrEmpty(organizationDto.Id))
                throw new ArgumentNullException("organizationDto", "Must provide a guid for organization update");
            try
            {

                var guidInfo = await _referenceDataRepository.GetGuidLookupResultFromGuidAsync(organizationDto.Id);

                if (guidInfo != null && !string.Equals(guidInfo.Entity, "PERSON", StringComparison.OrdinalIgnoreCase))
                {
                    throw new ArgumentException(string.Concat("The id ", organizationDto.Id.ToString(),
                        " already exists and it does not belong to an Organization. If you are trying to create with a specific id use a different id."));
                }

                // get the person ID associated with the incoming guid
                var personOrgId = await _personRepository.GetPersonIdFromGuidAsync(organizationDto.Id);

                // verify the GUID exists to perform an update.  If not, perform a create instead
                if (!string.IsNullOrEmpty(personOrgId))
                {

                    // verify the user has the permission to update a organization
                    CheckUserOrganizationUpdatePermissions();

                    // map the organization DTO to entities
                    var personEntity = await ConvertOrganization2DtoToPersonEntityAsync(personOrgId, organizationDto);
                    var addressEntites = await ConvertAddressDtoToAddressEntitiesAsync(organizationDto.Addresses);
                    var phoneEntities = await ConvertPhoneDtoToPhoneEntities(organizationDto.Phones);

                    // update the organization in the database
                    var updatedOrganizationEntity =
                        await _personRepository.UpdateOrganizationAsync(personEntity, addressEntites, phoneEntities);

                    // return the newly updated organization
                    return await ConvertPersonIntegrationEntityToOrganizationDtoAsync(updatedOrganizationEntity);

                }
                else
                {
                    // perform a create instead
                    return await CreateOrganizationAsync(organizationDto);
                }

            }
            catch (RepositoryException ex)
            {
                throw ex;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex.InnerException);
            }
        }

        #endregion

        #region Query Person

        /// <summary>
        /// Query person by person DTO.
        /// </summary>
        /// <param name="person">The <see cref="Person">person</see> entity to query by.</param>
        /// <returns>List of matching <see cref="Person">persons</see></returns>
        public async Task<IEnumerable<Dtos.Person>> QueryPersonByPostAsync(Dtos.Person personDtoIn)
        {
            if (personDtoIn == null)
                throw new ArgumentNullException("personDtoIn", "Person required to query");

            var personPrimaryName =
                personDtoIn.PersonNames.Where(p => p.NameType == Dtos.PersonNameType.Primary).FirstOrDefault();
            if (personPrimaryName == null || string.IsNullOrEmpty(personPrimaryName.FirstName) ||
                string.IsNullOrEmpty(personPrimaryName.LastName))
                throw new ArgumentNullException("personDtoIn", "Person first and last name required to query");

            var personBirthName =
                personDtoIn.PersonNames.Where(p => p.NameType == Dtos.PersonNameType.Birth).FirstOrDefault();

            // verify the user has the permission to view any person
            CheckUserPersonViewPermissions();

            IEnumerable<Dtos.Person> personDtos = new List<Dtos.Person>();

            // create a person entity of the allowable match criteria
            Domain.Base.Entities.Person personEntity = null;
            string personId = MapColleagueId(personDtoIn.Credentials);
            personEntity = new Domain.Base.Entities.Person(personId, personPrimaryName.LastName);
            personEntity.FirstName = personPrimaryName.FirstName;
            personEntity.MiddleName = personPrimaryName.MiddleName;
            personEntity.BirthDate = personDtoIn.BirthDate;
            personEntity.BirthNameFirst = personBirthName == null ? string.Empty : personBirthName.FirstName;
            personEntity.BirthNameMiddle = personBirthName == null ? string.Empty : personBirthName.MiddleName;
            personEntity.BirthNameLast = personBirthName == null ? string.Empty : personBirthName.LastName;
            personEntity.Gender = MapGenderType(personDtoIn.GenderType);
            personEntity.GovernmentId = MapSsn(personDtoIn.Credentials);
            if (personDtoIn.EmailAddresses != null && personDtoIn.EmailAddresses.Count() > 0)
            {
                var emailAddressEntity = MapEmailAddresses(personDtoIn.EmailAddresses).FirstOrDefault();
                if (emailAddressEntity != null)
                {
                    personEntity.EmailAddresses.Add(emailAddressEntity);
                }
            }

            // get the person matches
            var personGuids = await _personRepository.GetMatchingPersonsAsync(personEntity);

            // get the DTOs for the person matches 
            if (personGuids != null && personGuids.Count() > 0)
            {
                personDtos = await GetPersonByGuidNonCachedAsync(personGuids);
            }

            return personDtos;
        }

        /// <summary>
        /// Query person by person DTO.
        /// </summary>
        /// <param name="person">The <see cref="Person2">person</see> entity to query by.</param>
        /// <returns>List of matching <see cref="Person2">persons</see></returns>
        public async Task<IEnumerable<Dtos.Person2>> QueryPerson2ByPostAsync(Dtos.Person2 personDtoIn)
        {
            if (personDtoIn == null)
                throw new ArgumentNullException("personDtoIn", "Person required to query");
            if (personDtoIn.PersonNames == null)
                throw new ArgumentNullException("personDtoIn", "PersonNames must be defined in query");

            var personPrimaryName =
                personDtoIn.PersonNames.Where(p => p.NameType.Category == Dtos.PersonNameType2.Legal).FirstOrDefault();
            if (personPrimaryName == null || string.IsNullOrEmpty(personPrimaryName.FirstName) ||
                string.IsNullOrEmpty(personPrimaryName.LastName))
                throw new ArgumentNullException("personDtoIn", "Person first and last name required to query");

            var personBirthName =
                personDtoIn.PersonNames.Where(p => p.NameType.Category == Dtos.PersonNameType2.Birth).FirstOrDefault();

            // verify the user has the permission to view any person
            CheckUserPersonViewPermissions();

            IEnumerable<Dtos.Person2> personDtos = new List<Dtos.Person2>();

            // create a person entity of the allowable match criteria
            Domain.Base.Entities.Person personEntity = null;
            string personId = MapColleagueId2(personDtoIn.Credentials);
            personEntity = new Domain.Base.Entities.Person(personId, personPrimaryName.LastName);
            personEntity.FirstName = personPrimaryName.FirstName;
            personEntity.MiddleName = personPrimaryName.MiddleName;
            personEntity.BirthNameFirst = personBirthName == null ? string.Empty : personBirthName.FirstName;
            personEntity.BirthNameMiddle = personBirthName == null ? string.Empty : personBirthName.MiddleName;
            personEntity.BirthNameLast = personBirthName == null ? string.Empty : personBirthName.LastName;
            personEntity.BirthDate = personDtoIn.BirthDate;
            personEntity.Gender = MapGenderType2(personDtoIn.GenderType);
            personEntity.GovernmentId = MapSsn2(personDtoIn.Credentials);
            if (personDtoIn.EmailAddresses != null && personDtoIn.EmailAddresses.Count() > 0)
            {
                var emailAddressEntity = (await MapEmailAddresses2(personDtoIn.EmailAddresses)).FirstOrDefault();
                if (emailAddressEntity != null)
                {
                    personEntity.EmailAddresses.Add(emailAddressEntity);
                }
            }

            // get the person matches
            var personGuids = await _personRepository.GetMatchingPersonsAsync(personEntity);

            // get the DTOs for the person matches 
            if (personGuids != null && personGuids.Count() > 0)
            {
                personDtos = await GetPerson2ByGuidNonCachedAsync(personGuids);
            }

            return personDtos;
        }

        /// <summary>
        /// Query person by person DTO.
        /// </summary>
        /// <param name="person">The <see cref="Person3">person</see> entity to query by.</param>
        /// <returns>List of matching <see cref="Person3">persons</see></returns>
        public async Task<IEnumerable<Dtos.Person3>> QueryPerson3ByPostAsync(Dtos.Person3 personDtoIn)
        {
            if (personDtoIn == null)
                throw new ArgumentNullException("personDtoIn", "Person required to query");
            if (personDtoIn.PersonNames == null)
                throw new ArgumentNullException("personDtoIn", "PersonNames must be defined in query");

            var personPrimaryName =
                personDtoIn.PersonNames.Where(p => p.NameType.Category == Dtos.PersonNameType2.Legal).FirstOrDefault();
            if (personPrimaryName == null || string.IsNullOrEmpty(personPrimaryName.FirstName) ||
                string.IsNullOrEmpty(personPrimaryName.LastName))
                throw new ArgumentNullException("personDtoIn", "Person first and last name required to query");

            var personBirthName =
                personDtoIn.PersonNames.Where(p => p.NameType.Category == Dtos.PersonNameType2.Birth).FirstOrDefault();

            // verify the user has the permission to view any person
            CheckUserPersonViewPermissions();

            IEnumerable<Dtos.Person3> personDtos = new List<Dtos.Person3>();

            // create a person entity of the allowable match criteria
            Domain.Base.Entities.Person personEntity = null;
            string personId = MapColleagueId3(personDtoIn.Credentials);
            personEntity = new Domain.Base.Entities.Person(personId, personPrimaryName.LastName);
            personEntity.FirstName = personPrimaryName.FirstName;
            personEntity.MiddleName = personPrimaryName.MiddleName;
            personEntity.BirthNameFirst = personBirthName == null ? string.Empty : personBirthName.FirstName;
            personEntity.BirthNameMiddle = personBirthName == null ? string.Empty : personBirthName.MiddleName;
            personEntity.BirthNameLast = personBirthName == null ? string.Empty : personBirthName.LastName;
            personEntity.BirthDate = personDtoIn.BirthDate;
            personEntity.Gender = MapGenderType2(personDtoIn.GenderType);
            personEntity.GovernmentId = MapSsn3(personDtoIn.Credentials);
            if (personDtoIn.EmailAddresses != null && personDtoIn.EmailAddresses.Count() > 0)
            {
                var emailAddressEntity = (await MapEmailAddresses2(personDtoIn.EmailAddresses)).FirstOrDefault();
                if (emailAddressEntity != null)
                {
                    personEntity.EmailAddresses.Add(emailAddressEntity);
                }
            }

            // get the person matches
            var personGuids = await _personRepository.GetMatchingPersonsAsync(personEntity);

            // get the DTOs for the person matches 
            if (personGuids != null && personGuids.Count() > 0)
            {
                personDtos = await GetPerson3ByGuidNonCachedAsync(personGuids);
            }

            return personDtos;
        }

        /// <summary>
        /// Query person by criteria and return the results of the matching algorithm
        /// </summary>
        /// <param name="person">The <see cref="Dtos.Base.PersonMatchCriteria">criteria</see> to query by.</param>
        /// <returns>List of matching <see cref="Dtos.Base.PersonMatchResult">results</see></returns>
        public async Task<IEnumerable<Dtos.Base.PersonMatchResult>> QueryPersonMatchResultsByPostAsync(
            Dtos.Base.PersonMatchCriteria criteria)
        {
            if (criteria == null)
                throw new ArgumentNullException("criteria", "Criteria required to query");

            // Remove any invalid names - for example, if the Former Name fields were not supplied/filled in, 
            // or if a first name was sent without a corresponding last name.
            if (criteria.MatchNames != null)
            {
                criteria.MatchNames =
                    criteria.MatchNames.Where(
                        n => !string.IsNullOrEmpty(n.FamilyName) && !string.IsNullOrEmpty(n.GivenName));
            }

            var adapter =
                _adapterRegistry.GetAdapter<Dtos.Base.PersonMatchCriteria, Domain.Base.Entities.PersonMatchCriteria>();
            var entCriteria = adapter.MapToType(criteria);
            var entResults = await _personRepository.GetMatchingPersonResultsAsync(entCriteria);

            var results = new List<Dtos.Base.PersonMatchResult>();
            if (entResults != null && entResults.Any())
            {
                var dtoAdapter =
                    _adapterRegistry.GetAdapter<Domain.Base.Entities.PersonMatchResult, Dtos.Base.PersonMatchResult>();
                results = entResults.Select(x => dtoAdapter.MapToType(x)).ToList();
            }

            return results;
        }

        /// <summary>
        /// Retrieves the matching Persons for the ids provided or searches keyword
        /// for the matching Persons if a first and last name are provided.  
        /// In the latter case, a middle name is optional.
        /// Matching is done by partial name; i.e., 'Bro' will match 'Brown' or 'Brodie'. 
        /// Capitalization is ignored.
        /// </summary>
        /// <remarks>the following keyword input is legal
        /// <list type="bullet">
        /// <item>a Colleague id.  Short ids will be zero-padded.</item>
        /// <item>First Last</item>
        /// <item>First Middle Last</item>
        /// <item>Last, First</item>
        /// <item>Last, First Middle</item>
        /// </list>
        /// </remarks>
        /// <param name="criteria">Keyword can either be a Person ID or a first and last name.  A middle name is optional.</param>
        /// <returns>An enumeration of <see cref="Dtos.Base.Person">Person</see> with populated ID and first, middle and last names</returns>
        /// <exception cref="ArgumentNullException">Criteria must be provided</exception>
        /// <exception cref="PermissionsException">Person must have permissions to search for persons</exception>
        public async Task<IEnumerable<Dtos.Base.Person>> QueryPersonNamesByPostAsync(PersonNameQueryCriteria criteria)
        {
            if (criteria == null)
            {
                throw new ArgumentNullException("criteria", "criteria must be provided to search for persons.");
            }

            CheckUserNameSearchPermissions();

            var personBases = await _personBaseRepository.SearchByIdsOrNamesAsync(criteria.Ids, criteria.QueryKeyword);
            var results = new List<Dtos.Base.Person>();

            foreach (var personBase in personBases)
            {
                var personDto = new Dtos.Base.Person()
                {
                    Id = personBase.Id,
                    FirstName = personBase.FirstName,
                    MiddleName = personBase.MiddleName,
                    LastName = personBase.LastName,
                    BirthNameFirst = personBase.BirthNameFirst,
                    BirthNameMiddle = personBase.BirthNameMiddle,
                    BirthNameLast = personBase.BirthNameLast,
                    PreferredName = personBase.PreferredName,
                    PrivacyStatusCode = personBase.PrivacyStatusCode
                };
                results.Add(personDto);
            }
            return results;
        }

        #endregion

        #region Permissions

        /// <summary>
        /// Verifies if the user has the correct permission to view the person.
        /// </summary>
        private void CheckUserPersonViewPermissions(string personId)
        {
            // access is ok if the current user is the person being viewed
            if (!CurrentUser.IsPerson(personId))
            {
                // not the current user, must have view any person permission
                CheckUserPersonViewPermissions();
            }
        }

        /// <summary>
        /// Verifies if the user has the correct permission to view any person.
        /// </summary>
        private void CheckUserPersonViewPermissions()
        {
            // access is ok if the current user has the view any person permission
            if (!HasPermission(BasePermissionCodes.ViewAnyPerson))
            {
                logger.Error("User '" + CurrentUser.UserId + "' is not authorized to view any person.");
                throw new PermissionsException("User is not authorized to view any person.");
            }
        }

        /// <summary>
        /// Verifies if the user has the correct permissions to create a organization.
        /// </summary>
        private void CheckUserOrganizationCreatePermissions()
        {
            // access is ok if the current user has the create organization permission
            if (!HasPermission(BasePermissionCodes.CreateOrganization))
            {
                logger.Error("User '" + CurrentUser.UserId + "' is not authorized to create an organization.");
                throw new PermissionsException("User is not authorized to create an organization.");
            }
        }

        /// <summary>
        /// Verifies if the user has the correct permissions to update a organization.
        /// </summary>
        private void CheckUserOrganizationUpdatePermissions()
        {
            // access is ok if the current user has the update organization permission
            if (!HasPermission(BasePermissionCodes.UpdateOrganization))
            {
                logger.Error("User '" + CurrentUser.UserId + "' is not authorized to update a organization.");
                throw new PermissionsException("User is not authorized to update a organization.");
            }
        }

        /// <summary>
        /// Verifies if the user has the correct permissions to view a organization.
        /// </summary>
        private void CheckUserOrganizationViewPermissions()
        {
            // access is ok if the current user has the view organization permission
            if (!HasPermission(BasePermissionCodes.ViewOrganization))
            {
                logger.Error("User '" + CurrentUser.UserId + "' is not authorized to view a organization.");
                throw new PermissionsException("User is not authorized to view a organization.");
            }
        }

        /// <summary>
        /// Verifies if the user has the correct permissions to create a person.
        /// </summary>
        private void CheckUserPersonCreatePermissions()
        {
            // access is ok if the current user has the create person permission
            if (!HasPermission(BasePermissionCodes.CreatePerson))
            {
                logger.Error("User '" + CurrentUser.UserId + "' is not authorized to create a person.");
                throw new PermissionsException("User is not authorized to create a person.");
            }
        }

        /// <summary>
        /// Verifies if the user has the correct permissions to update a person.
        /// </summary>
        private void CheckUserPersonUpdatePermissions(string personId)
        {
            // access is ok if the current user is the person being updated
            if (!CurrentUser.IsPerson(personId))
            {
                // access is ok if the current user has the update person permission
                if (!HasPermission(BasePermissionCodes.UpdatePerson))
                {
                    logger.Error("User '" + CurrentUser.UserId + "' is not authorized to update a person.");
                    throw new PermissionsException("User is not authorized to update a person.");
                }
            }
        }

        /// <summary>
        /// Verifies if the user is permitted to view another person's profile
        /// A person can only view a profile for:
        /// 1. Himself/herself
        /// 2. Someone with whom they have an ERP-recognized relationship
        /// 3. Someone who currently has permission to proxy for the user
        /// </summary>
        private async Task<bool> UserCanViewProfileForPerson(string personId)
        {
            if (!CurrentUser.IsPerson(personId))
            {
                var relationshipIds = await _relationshipRepository.GetRelatedPersonIdsAsync(CurrentUser.PersonId);
                if (relationshipIds.Contains(personId))
                {
                    return true;
                }
                var proxyUsers = await _proxyRepository.GetUserProxyPermissionsAsync(CurrentUser.PersonId);
                if (proxyUsers.Select(pu => pu.Id).Contains(personId))
                {
                    return true;
                }
                return false;
            }
            return true;
        }

        /// <summary>
        /// Verifies if a user has the correct permissions to search for person names.
        /// </summary>
        private void CheckUserNameSearchPermissions()
        {
            if (!HasPermission(BasePermissionCodes.ViewAnyPerson) &&
                !HasPermission(BasePermissionCodes.ViewPersonEmergencyContacts) &&
                !HasPermission(BasePermissionCodes.ViewPersonHealthConditions) &&
                !HasPermission(BasePermissionCodes.ViewPersonOtherEmergencyInformation))
            {
                logger.Error("User '" + CurrentUser.UserId + "' is not authorized to search for a person name.");
                throw new PermissionsException("User is not authorized to search for a person name.");
            }
        }

        #endregion

        #region Convert Person Entity to Dto Methods

        private async Task<Dtos.Person> ConvertPersonEntityToDtoAsync(Domain.Base.Entities.Person personEntity,
            bool getLimitedData)
        {
            // create the person DTO
            var personDto = new Dtos.Person();
            personDto.Guid = personEntity.Guid;
            personDto.BirthDate = personEntity.BirthDate;
            personDto.DeceasedDate = personEntity.DeceasedDate;
            personDto.GenderType = GetGenderType(personEntity.Gender);
            personDto.Credentials = GetCredentials(personEntity);
            personDto.MaritalStatus = await GetMaritalStatusGuidAsync(personEntity.MaritalStatusCode);
            personDto.Races = await GetRaceGuidsAsync(personEntity.RaceCodes);
            personDto.Ethnicity = await GetEthnicityGuidAsync(personEntity.EthnicCodes);
            personDto.Roles = await GetRolesAsync(personEntity.Id);
            // names
            var personNames = new List<Dtos.PersonName>();
            personNames.Add(GetPersonPrimaryName(personEntity));
            if (!string.IsNullOrEmpty(personEntity.BirthNameFirst) ||
                !string.IsNullOrEmpty(personEntity.BirthNameMiddle) || !string.IsNullOrEmpty(personEntity.BirthNameLast))
            {
                personNames.Add(GetPersonBirthName(personEntity));
            }
            personDto.PersonNames = personNames;
            var emailEntities = new List<Domain.Base.Entities.EmailAddress>();
            if (getLimitedData)
            {
                // return only the preferred email address and not addresses or phones for performance
                if (personEntity.EmailAddresses.Count() > 0)
                {
                    var preferredEmailAddress =
                        personEntity.EmailAddresses.Where(e => e.IsPreferred == true).FirstOrDefault();
                    if (preferredEmailAddress != null)
                    {
                        emailEntities.Add(new Domain.Base.Entities.EmailAddress(preferredEmailAddress.Value,
                            Dtos.EmailAddressType.Institution.ToString()));
                    }
                }
                personDto.EmailAddresses = GetEmailAddresses(emailEntities);
                personDto.Addresses = await GetAddressesAsync(null);
                personDto.Phones = GetPhones(null);
            }
            else
            {
                // get addresses, email addresses and phones
                List<Domain.Base.Entities.Phone> phoneEntities = null;
                List<Domain.Base.Entities.Address> addressEntities = null;
                var tupleResult = await _personRepository.GetPersonIntegrationDataAsync(personEntity.Id);
                emailEntities = tupleResult.Item1;
                phoneEntities = tupleResult.Item2;
                addressEntities = tupleResult.Item3;
                personDto.EmailAddresses = GetEmailAddresses(emailEntities);
                personDto.Addresses = await GetAddressesAsync(addressEntities);
                personDto.Phones = GetPhones(phoneEntities);
            }

            return personDto;
        }

        private Dtos.PersonName GetPersonPrimaryName(Domain.Base.Entities.Person person)
        {
            return new Dtos.PersonName()
            {
                NameType = Dtos.PersonNameType.Primary,
                Title = ConvertPrefixToCode(person.Prefix),
                // Colleague does not require a first name but this model does.  Must send a blank string if empty.
                FirstName = string.IsNullOrEmpty(person.FirstName) ? "" : person.FirstName,
                MiddleName = string.IsNullOrEmpty(person.MiddleName) ? null : person.MiddleName,
                LastNamePrefix = null, // this does not exist in Colleague
                LastName = person.LastName,
                Pedigree = ConvertSuffixToCode(person.Suffix),
                PreferredName = string.IsNullOrEmpty(person.Nickname) ? null : person.Nickname
            };
        }

        private Dtos.PersonName GetPersonBirthName(Domain.Base.Entities.Person person)
        {
            return new Dtos.PersonName()
            {
                NameType = Dtos.PersonNameType.Birth,
                Title = null,
                FirstName = string.IsNullOrEmpty(person.BirthNameFirst) ? null : person.BirthNameFirst,
                MiddleName = string.IsNullOrEmpty(person.BirthNameMiddle) ? null : person.BirthNameMiddle,
                LastNamePrefix = null, // this does not exist in Colleague
                LastName = person.BirthNameLast,
                Pedigree = null,
                PreferredName = null
            };
        }

        private string ConvertPrefixToCode(string prefix)
        {
            string prefixCode = null;
            if (!string.IsNullOrEmpty(prefix))
            {
                var prefixEntity = _referenceDataRepository.Prefixes.FirstOrDefault(p => p.Abbreviation == prefix);
                if (prefixEntity != null && !string.IsNullOrEmpty(prefixEntity.Code))
                {
                    prefixCode = prefixEntity.Code;
                }
            }
            return prefixCode;
        }

        private string ConvertSuffixToCode(string suffix)
        {
            string suffixCode = null;
            if (!string.IsNullOrEmpty(suffix))
            {
                var suffixEntity = _referenceDataRepository.Suffixes.FirstOrDefault(s => s.Abbreviation == suffix);
                if (suffixEntity != null && !string.IsNullOrEmpty(suffixEntity.Code))
                {
                    suffixCode = suffixEntity.Code;
                }
            }
            return suffixCode;
        }

        private Dtos.GenderType? GetGenderType(string gender)
        {
            switch (gender)
            {
                case "M":
                    {
                        return Dtos.GenderType.Male;
                    }
                case "F":
                    {
                        return Dtos.GenderType.Female;
                    }
                default:
                    {
                        return Dtos.GenderType.Unknown;
                    }
            }
        }

        private async Task<Dtos.GuidObject> GetMaritalStatusGuidAsync(string maritalStatusCode)
        {
            // get the marital status guid
            Dtos.GuidObject maritalStatusGuid = null;
            if (!string.IsNullOrEmpty(maritalStatusCode))
            {
                var maritalStatusEntity =
                    (await _referenceDataRepository.MaritalStatusesAsync()).FirstOrDefault(
                        m => m.Code == maritalStatusCode);
                if (maritalStatusEntity != null && !string.IsNullOrEmpty(maritalStatusEntity.Guid))
                {
                    maritalStatusGuid = new Dtos.GuidObject(maritalStatusEntity.Guid);
                }
            }
            return maritalStatusGuid;
        }

        private async Task<IEnumerable<Dtos.GuidObject>> GetRaceGuidsAsync(IEnumerable<string> raceCodes)
        {
            // get the race guids
            var raceGuids = new List<Dtos.GuidObject>();
            if (raceCodes != null && raceCodes.Count() > 0)
            {
                foreach (var raceCode in raceCodes)
                {
                    var raceEntity =
                        (await _referenceDataRepository.RacesAsync()).FirstOrDefault(r => r.Code == raceCode);
                    if (raceEntity != null && !string.IsNullOrEmpty(raceEntity.Guid))
                    {
                        raceGuids.Add(new Dtos.GuidObject(raceEntity.Guid));
                    }
                }
            }
            return raceGuids;
        }

        private async Task<Dtos.GuidObject> GetEthnicityGuidAsync(IEnumerable<string> ethnicityCodes)
        {
            // get the ethnicity guid
            Dtos.GuidObject ethnicityGuid = null;
            if (ethnicityCodes != null && ethnicityCodes.Count() > 0)
            {
                var ethnicityEntity =
                    (await _referenceDataRepository.EthnicitiesAsync()).FirstOrDefault(
                        e => e.Code == ethnicityCodes.First());
                if (ethnicityEntity != null && !string.IsNullOrEmpty(ethnicityEntity.Guid))
                {
                    ethnicityGuid = new Dtos.GuidObject(ethnicityEntity.Guid);
                }
            }
            return ethnicityGuid;
        }

        private IEnumerable<Dtos.Phone> GetPhones(IEnumerable<Domain.Base.Entities.Phone> phoneEntities)
        {
            var phoneDtos = new List<Dtos.Phone>();
            if (phoneEntities != null && phoneEntities.Count() > 0)
            {
                foreach (var phoneEntity in phoneEntities)
                {
                    phoneDtos.Add(new Dtos.Phone()
                    {
                        Number = phoneEntity.Number,
                        Extension = string.IsNullOrEmpty(phoneEntity.Extension) ? null : phoneEntity.Extension,
                        PhoneType = (Dtos.PhoneType)Enum.Parse(typeof(Dtos.PhoneType), phoneEntity.TypeCode)
                    });
                }
            }
            return phoneDtos;
        }

        private IEnumerable<Dtos.Credential> GetCredentials(Domain.Base.Entities.Person person)
        {
            // Colleague Person ID
            var credentials = new List<Dtos.Credential>()
            {
                new Dtos.Credential()
                {
                    CredentialType = Dtos.CredentialType.ColleaguePersonId,
                    Id = person.Id
                }
            };
            // Elevate ID
            if (person.PersonAltIds != null && person.PersonAltIds.Count() > 0)
            {
                var elevPersonAltId =
                    person.PersonAltIds.FirstOrDefault(
                        a => a.Type == Domain.Base.Entities.PersonAlt.ElevatePersonAltType);
                if (elevPersonAltId != null && !string.IsNullOrEmpty(elevPersonAltId.Id))
                {
                    credentials.Add(new Dtos.Credential()
                    {
                        CredentialType = Dtos.CredentialType.ElevatePersonId,
                        Id = elevPersonAltId.Id
                    });
                }
            }
            // SSN
            if (!string.IsNullOrEmpty(person.GovernmentId))
            {
                credentials.Add(new Dtos.Credential()
                {
                    CredentialType = Dtos.CredentialType.SocialSecurityNumber,
                    Id = person.GovernmentId
                });
            }
            return credentials;
        }

        private async Task<IEnumerable<Dtos.DtoProperties.CredentialDtoProperty>> GetPersonCredentials(
            Domain.Base.Entities.Person person)
        {
            // Colleague Person ID
            var credentials = new List<Dtos.DtoProperties.CredentialDtoProperty>()
            {
                new Dtos.DtoProperties.CredentialDtoProperty()
                {
                    Type = Dtos.EnumProperties.CredentialType.ColleaguePersonId,
                    Value = person.Id
                }
            };
            // Elevate ID
            if (person.PersonAltIds != null && person.PersonAltIds.Count() > 0)
            {
                //Produce an error if there are more than one elevate id's, it means bad data
                if (person.PersonAltIds.Count(altId => altId.Type.Equals("ELEV", StringComparison.OrdinalIgnoreCase)) > 1)
                {
                    throw new InvalidOperationException(string.Format("Person ID '{0}': You cannot have more than one elevate id.", person.Id));
                }
                var elevPersonAltId =
                    person.PersonAltIds.FirstOrDefault(
                        a => a.Type == Domain.Base.Entities.PersonAlt.ElevatePersonAltType);
                if (elevPersonAltId != null && !string.IsNullOrEmpty(elevPersonAltId.Id))
                {
                    credentials.Add(new Dtos.DtoProperties.CredentialDtoProperty()
                    {
                        Type = Dtos.EnumProperties.CredentialType.ElevateID,
                        Value = elevPersonAltId.Id
                    });
                }
            }
            // SSN
            if (!string.IsNullOrEmpty(person.GovernmentId))
            {
                var type = Dtos.EnumProperties.CredentialType.Sin;
                var countryCode = await _personRepository.GetHostCountryAsync();
                if (countryCode.Equals("USA", StringComparison.OrdinalIgnoreCase))
                {
                    type = Dtos.EnumProperties.CredentialType.Ssn;
                }
                credentials.Add(new Dtos.DtoProperties.CredentialDtoProperty()
                {
                    Type = type,
                    Value = person.GovernmentId
                });
            }
            return credentials;
        }

        private async Task<IEnumerable<Dtos.Role>> GetRolesAsync(string personId)
        {
            var roles = new List<Dtos.Role>();
            if ((await _personRepository.IsFacultyAsync(personId)))
                roles.Add(new Dtos.Role() { RoleType = Dtos.RoleType.Faculty });
            if ((await _personRepository.IsStudentAsync(personId)))
                roles.Add(new Dtos.Role() { RoleType = Dtos.RoleType.Student });
            return roles;
        }

        private async Task<IEnumerable<Dtos.Address>> GetAddressesAsync(
            IEnumerable<Domain.Base.Entities.Address> addressEntities)
        {
            var addressDtos = new List<Dtos.Address>();
            if (addressEntities != null && addressEntities.Count() > 0)
            {
                foreach (var addressEntity in addressEntities)
                {
                    if (addressEntity != null)
                    {
                        var addressDto = new Dtos.Address();
                        addressDto.AddressType =
                            (Dtos.AddressTypeDtoProperty)
                                Enum.Parse(typeof(Dtos.AddressTypeDtoProperty), addressEntity.Type);
                        addressDto.City = addressEntity.City;
                        addressDto.PostalCode = addressEntity.PostalCode;
                        addressDto.Region = addressEntity.State;
                        // send the county description instead of the code
                        if (!string.IsNullOrEmpty(addressEntity.County))
                        {
                            var county =
                                _referenceDataRepository.Counties.FirstOrDefault(c => c.Code == addressEntity.County);
                            if (county != null)
                            {
                                addressDto.County = county.Description;
                            }
                        }
                        // send the ISO country code and description
                        if (!string.IsNullOrEmpty(addressEntity.Country))
                        {
                            var countries = await _referenceDataRepository.GetCountryCodesAsync();
                            var country = countries.FirstOrDefault(c => c.IsoCode == addressEntity.Country);
                            if (country != null)
                            {
                                addressDto.Country = new Dtos.Country();
                                addressDto.Country.Code = country.IsoCode;
                                addressDto.Country.Value = country.Description;
                            }
                        }
                        if (addressEntity.AddressLines != null && addressEntity.AddressLines.Count() > 0)
                        {
                            addressDto.StreetAddress1 = addressEntity.AddressLines[0];
                            if (addressEntity.AddressLines.Count() > 1)
                            {
                                addressDto.StreetAddress2 = addressEntity.AddressLines[1];
                            }
                            if (addressEntity.AddressLines.Count() > 2)
                            {
                                addressDto.StreetAddress3 = addressEntity.AddressLines[2];
                            }
                        }
                        addressDtos.Add(addressDto);
                    }
                }
            }
            return addressDtos;
        }

        private IEnumerable<Dtos.EmailAddress> GetEmailAddresses(
            IEnumerable<Domain.Base.Entities.EmailAddress> emailAddressEntities)
        {
            var emailAddressDtos = new List<Dtos.EmailAddress>();
            if (emailAddressEntities != null && emailAddressEntities.Count() > 0)
            {
                foreach (var emailAddressEntity in emailAddressEntities)
                {
                    var addressDto = new Dtos.EmailAddress()
                    {
                        EmailAddressType =
                            (Dtos.EmailAddressType)
                                Enum.Parse(typeof(Dtos.EmailAddressType), emailAddressEntity.TypeCode),
                        Address = emailAddressEntity.Value
                    };
                    emailAddressDtos.Add(addressDto);
                }
            }
            return emailAddressDtos;
        }

        #endregion

        #region Convert Person HEDM V6 Entity to Dto Methods

        private async Task<Dtos.Person2> ConvertPerson2EntityToDtoAsync(
            Domain.Base.Entities.PersonIntegration personEntity, bool getLimitedData, bool bypassCache = false)
        {
            // create the person DTO
            var personDto = new Dtos.Person2();

            personDto.Id = personEntity.Guid;
            personDto.PrivacyStatus = await GetPersonPrivacyAsync(personEntity.PrivacyStatus, personEntity.PrivacyStatusCode, bypassCache);
            personDto.PersonNames = await GetPersonNamesAsync(personEntity, bypassCache);
            personDto.BirthDate = personEntity.BirthDate;
            personDto.DeceasedDate = personEntity.DeceasedDate;
            if (!string.IsNullOrEmpty(personEntity.Gender)) personDto.GenderType = GetGenderType2(personEntity.Gender);
            personDto.Religion = (!string.IsNullOrEmpty(personEntity.Religion)
                ? await GetPersonReligionGuidAsync(personEntity.Religion)
                : null);
            personDto.Ethnicity = (personEntity.EthnicCodes != null && personEntity.EthnicCodes.Any()
                ? await GetPersonEthnicityGuidAsync(personEntity.EthnicCodes)
                : null);
            personDto.Races = (personEntity.RaceCodes != null && personEntity.RaceCodes.Any()
                ? await GetPersonRaceGuidsAsync(personEntity.RaceCodes)
                : null);
            personDto.Languages = (personEntity.Languages != null && personEntity.Languages.Any() ? GetPersonLanguages(personEntity.Languages) : null);
            if (!string.IsNullOrEmpty(personEntity.MaritalStatusCode))
                personDto.MaritalStatus = await GetPersonMaritalStatusGuidAsync(personEntity.MaritalStatusCode);
            if (personEntity.AlienStatus != null)
                personDto.CitizenshipStatus = await GetPersonCitizenshipAsync(personEntity.AlienStatus, bypassCache);
            if (!string.IsNullOrEmpty(personEntity.BirthCountry))
                personDto.CountryOfBirth = await GetPersonCountryAsync(personEntity.BirthCountry);
            if (!string.IsNullOrEmpty(personEntity.Citizenship))
                personDto.CitizenshipCountry = await GetPersonCountryAsync(personEntity.Citizenship);
            personDto.Roles = await GetPersonRolesAsync(personEntity.Id, personEntity.Roles);
            if (personEntity.Passport != null || personEntity.DriverLicense != null || (personEntity.IdentityDocuments != null && personEntity.IdentityDocuments.Any()))
            {
                personDto.IdentityDocuments = await GetPersonIdentityDocumentsAsync(personEntity, bypassCache);
            }

            // Until we have more granular security, HEDM models will not populat SSN or SIN

            var creds = await GetPersonCredentials(personEntity);
            var credDto = new List<CredentialDtoProperty>();
            foreach (var cred in creds)
            {
                if (cred.Type != CredentialType.Sin && cred.Type != CredentialType.Ssn)
                {
                    credDto.Add(cred);
                }

            }
            personDto.Credentials = credDto.AsEnumerable();
            
            if ((personEntity.Interests != null) && (personEntity.Interests.Any()))
                personDto.Interests = await GetPersonInterestsAsync(personEntity.Interests, bypassCache);

            // get addresses, email addresses and phones
            List<Domain.Base.Entities.EmailAddress> emailEntities = personEntity.EmailAddresses;
            List<Domain.Base.Entities.Phone> phoneEntities = personEntity.Phones;
            List<Domain.Base.Entities.Address> addressEntities = personEntity.Addresses;
            List<Domain.Base.Entities.SocialMedia> socialMediaEntities = personEntity.SocialMedia;

            var emailAddresses = await GetEmailAddresses2(emailEntities, bypassCache);
            if ((emailAddresses != null) && (emailAddresses.Any())) personDto.EmailAddresses = emailAddresses;
            var addresses = await GetAddresses2Async(addressEntities, bypassCache);
            if ((addresses != null) && (addresses.Any())) personDto.Addresses = addresses;
            var phoneNumbers = await GetPhones2Async(phoneEntities, bypassCache);
            if ((phoneNumbers != null) && (phoneNumbers.Any())) personDto.Phones = phoneNumbers;
            if ((socialMediaEntities != null) && (socialMediaEntities.Any()))
                personDto.SocialMedia = await GetPersonSocialMediaAsync(socialMediaEntities, bypassCache);

            return personDto;
        }

        private async Task<Dtos.Person3> ConvertPerson3EntityToDtoAsync(
            Domain.Base.Entities.PersonIntegration personEntity, bool getLimitedData, bool bypassCache = false)
        {
            // create the person DTO
            var personDto = new Dtos.Person3();

            personDto.Id = personEntity.Guid;
            personDto.PrivacyStatus = await GetPersonPrivacyAsync(personEntity.PrivacyStatus, personEntity.PrivacyStatusCode, bypassCache);
            personDto.PersonNames = await GetPersonNamesAsync(personEntity, bypassCache);
            personDto.BirthDate = personEntity.BirthDate;
            personDto.DeceasedDate = personEntity.DeceasedDate;
            if (!string.IsNullOrEmpty(personEntity.Gender)) personDto.GenderType = GetGenderType2(personEntity.Gender);
            personDto.Religion = (!string.IsNullOrEmpty(personEntity.Religion)
                ? await GetPersonReligionGuidAsync(personEntity.Religion)
                : null);
            personDto.Ethnicity = (personEntity.EthnicCodes != null && personEntity.EthnicCodes.Any()
                ? await GetPersonEthnicityGuidAsync(personEntity.EthnicCodes)
                : null);
            personDto.Races = (personEntity.RaceCodes != null && personEntity.RaceCodes.Any()
                ? await GetPersonRaceGuidsAsync(personEntity.RaceCodes)
                : null);
            personDto.Languages = (personEntity.Languages != null && personEntity.Languages.Any() ? GetPersonLanguages(personEntity.Languages) : null);
            if (!string.IsNullOrEmpty(personEntity.MaritalStatusCode))
                personDto.MaritalStatus = await GetPersonMaritalStatusGuidAsync(personEntity.MaritalStatusCode);
            if (personEntity.AlienStatus != null)
                personDto.CitizenshipStatus = await GetPersonCitizenshipAsync(personEntity.AlienStatus, bypassCache);
            if (!string.IsNullOrEmpty(personEntity.BirthCountry))
                personDto.CountryOfBirth = await GetPersonCountryAsync(personEntity.BirthCountry);
            if (!string.IsNullOrEmpty(personEntity.Citizenship))
                personDto.CitizenshipCountry = await GetPersonCountryAsync(personEntity.Citizenship);
            personDto.Roles = await GetPersonRolesAsync(personEntity.Id, personEntity.Roles);
            if (personEntity.Passport != null || personEntity.DriverLicense != null || (personEntity.IdentityDocuments != null && personEntity.IdentityDocuments.Any()))
            {
                personDto.IdentityDocuments = await GetPersonIdentityDocumentsAsync(personEntity, bypassCache);
            }
            string[] personGuids = { personEntity.Guid };
            await this.GetPersonPins(personGuids);

            // Until we have more granular security, HEDM models will not populate SSN or SIN

            var creds = await GetPersonCredentials2(personEntity);
            var credDto = new List<CredentialDtoProperty2>();
            foreach (var cred in creds)
            {
                if (cred.Type != CredentialType2.Sin && cred.Type != CredentialType2.Ssn)
                {
                    credDto.Add(cred);
                }

            }
            personDto.Credentials = credDto.AsEnumerable();
            if ((personEntity.Interests != null) && (personEntity.Interests.Any()))
                personDto.Interests = await GetPersonInterestsAsync(personEntity.Interests, bypassCache);

            // get addresses, email addresses and phones
            List<Domain.Base.Entities.EmailAddress> emailEntities = personEntity.EmailAddresses;
            List<Domain.Base.Entities.Phone> phoneEntities = personEntity.Phones;
            List<Domain.Base.Entities.Address> addressEntities = personEntity.Addresses;
            List<Domain.Base.Entities.SocialMedia> socialMediaEntities = personEntity.SocialMedia;

            var emailAddresses = await GetEmailAddresses2(emailEntities, bypassCache);
            if ((emailAddresses != null) && (emailAddresses.Any())) personDto.EmailAddresses = emailAddresses;
            var addresses = await GetAddresses2Async(addressEntities, bypassCache);
            if ((addresses != null) && (addresses.Any())) personDto.Addresses = addresses;


            var phoneNumbers = await GetPhones2Async(phoneEntities, bypassCache);
            if ((phoneNumbers != null) && (phoneNumbers.Any())) personDto.Phones = phoneNumbers;
            if ((socialMediaEntities != null) && (socialMediaEntities.Any()))
                personDto.SocialMedia = await GetPersonSocialMediaAsync(socialMediaEntities, bypassCache);

            return personDto;
        }

        private async Task<IEnumerable<Dtos.DtoProperties.PersonNameDtoProperty>> GetPersonNamesAsync(
            Domain.Base.Entities.PersonIntegration person, bool bypassCache = false)
        {
            List<Dtos.DtoProperties.PersonNameDtoProperty> personNames =
                new List<Dtos.DtoProperties.PersonNameDtoProperty>();

            var preferredNameType = person.PreferredNameType;

            // Legal Name for a person
            var personNameTypeItem = (await _referenceDataRepository.GetPersonNameTypesAsync(bypassCache)).FirstOrDefault(
                pn => pn.Code == "LEGAL");
            if (personNameTypeItem != null)
            {
                var personName = new Dtos.DtoProperties.PersonNameDtoProperty()
                {
                    NameType = new Dtos.DtoProperties.PersonNameTypeDtoProperty()
                    {
                        Category = Dtos.PersonNameType2.Legal,
                        Detail =
                            new Dtos.GuidObject2(
                                    personNameTypeItem.Guid)
                    },
                    FullName =
                        !string.IsNullOrEmpty(preferredNameType) &&
                            preferredNameType.Equals("LEGAL", StringComparison.CurrentCultureIgnoreCase) &&
                            !string.IsNullOrEmpty(person.PreferredName) ?
                            person.PreferredName :
                        BuildFullName("FM", person.Prefix, person.FirstName, person.MiddleName,
                            person.LastName, person.Suffix),
                    Preference = (string.IsNullOrEmpty(preferredNameType) || preferredNameType.Equals("LEGAL", StringComparison.CurrentCultureIgnoreCase)) ?
                            Dtos.EnumProperties.PersonNamePreference.Preferred : (PersonNamePreference?)null,
                    Title = string.IsNullOrEmpty(person.Prefix) ? null : person.Prefix,
                    FirstName = string.IsNullOrEmpty(person.FirstName) ? null : person.FirstName,
                    MiddleName = string.IsNullOrEmpty(person.MiddleName) ? null : person.MiddleName,
                    LastName = string.IsNullOrEmpty(person.LastName) ? null : person.LastName,
                    LastNamePrefix =
                        person.LastName.Contains(" ")
                            ? person.LastName.Split(" ".ToCharArray())[0].ToString()
                            : null,
                    Pedigree = string.IsNullOrEmpty(person.Suffix) ? null : person.Suffix,
                    ProfessionalAbbreviation =
                        person.ProfessionalAbbreviations.Any() ? person.ProfessionalAbbreviations : null
                };
                personNames.Add(personName);
            }

            // Birth Name
            if (!string.IsNullOrEmpty(person.BirthNameLast) || !string.IsNullOrEmpty(person.BirthNameFirst) ||
                !string.IsNullOrEmpty(person.BirthNameMiddle))
            {
                var birthNameTypeItem = (await _referenceDataRepository.GetPersonNameTypesAsync(bypassCache))
                    .FirstOrDefault(pn => pn.Code == "BIRTH");
                if (birthNameTypeItem != null)
                {
                    var birthName = new Dtos.DtoProperties.PersonNameDtoProperty()
                    {
                        NameType = new Dtos.DtoProperties.PersonNameTypeDtoProperty()
                        {
                            Category = Dtos.PersonNameType2.Birth,
                            Detail =
                                new Dtos.GuidObject2(
                                        birthNameTypeItem.Guid)
                        },
                        FullName =
                            !string.IsNullOrEmpty(preferredNameType) &&
                            preferredNameType.Equals("BIRTH", StringComparison.CurrentCultureIgnoreCase) &&
                            !string.IsNullOrEmpty(person.PreferredName) ?
                            person.PreferredName :
                            BuildFullName("FM", "", person.BirthNameFirst, person.BirthNameMiddle, person.BirthNameLast, ""),
                        FirstName = string.IsNullOrEmpty(person.BirthNameFirst) ? null : person.BirthNameFirst,
                        MiddleName = string.IsNullOrEmpty(person.BirthNameMiddle) ? null : person.BirthNameMiddle,
                        LastName = string.IsNullOrEmpty(person.BirthNameLast) ? null : person.BirthNameLast,
                        LastNamePrefix =
                            person.BirthNameLast.Contains(" ")
                                ? person.BirthNameLast.Split(" ".ToCharArray())[0].ToString()
                                    : null,
                        Preference = !string.IsNullOrEmpty(preferredNameType) && preferredNameType.Equals("BIRTH", StringComparison.CurrentCultureIgnoreCase) ?
                            Dtos.EnumProperties.PersonNamePreference.Preferred : (PersonNamePreference?)null,
                    };
                    personNames.Add(birthName);
                }
            }

            // Chosen Name
            if (!string.IsNullOrEmpty(person.ChosenLastName) || !string.IsNullOrEmpty(person.ChosenFirstName) ||
                !string.IsNullOrEmpty(person.ChosenMiddleName))
            {
                var chosenNameTypeItem = (await _referenceDataRepository.GetPersonNameTypesAsync(bypassCache))
                    .FirstOrDefault(pn => pn.Code == "CHOSEN");
                if (chosenNameTypeItem != null)
                {
                    var chosenName = new Dtos.DtoProperties.PersonNameDtoProperty()
                    {
                        NameType = new Dtos.DtoProperties.PersonNameTypeDtoProperty()
                        {
                            Category = Dtos.PersonNameType2.Personal,
                            Detail =
                                new Dtos.GuidObject2(
                                        chosenNameTypeItem.Guid)
                        },
                        FullName =
                            !string.IsNullOrEmpty(preferredNameType) &&
                            preferredNameType.Equals("CHOSEN", StringComparison.CurrentCultureIgnoreCase) &&
                            !string.IsNullOrEmpty(person.PreferredName) ?
                            person.PreferredName :
                            BuildFullName("FM", "", person.ChosenFirstName, person.ChosenMiddleName, person.ChosenLastName, ""),
                        FirstName = string.IsNullOrEmpty(person.ChosenFirstName) ? null : person.ChosenFirstName,
                        MiddleName = string.IsNullOrEmpty(person.ChosenMiddleName) ? null : person.ChosenMiddleName,
                        LastName = string.IsNullOrEmpty(person.ChosenLastName) ? null : person.ChosenLastName,
                        LastNamePrefix =
                            person.ChosenLastName.Contains(" ")
                                ? person.ChosenLastName.Split(" ".ToCharArray())[0].ToString()
                                    : null,
                        Preference = !string.IsNullOrEmpty(preferredNameType) && preferredNameType.Equals("CHOSEN", StringComparison.CurrentCultureIgnoreCase) ?
                            Dtos.EnumProperties.PersonNamePreference.Preferred : (PersonNamePreference?)null,
                    };
                    personNames.Add(chosenName);
                }
            }

            // Nickname
            if (!string.IsNullOrEmpty(person.Nickname))
            {
                var nickNameTypeItem = (await _referenceDataRepository.GetPersonNameTypesAsync(false))
                    .FirstOrDefault(pn => pn.Code == "NICKNAME");
                if (nickNameTypeItem != null)
                {
                    var nickName = new Dtos.DtoProperties.PersonNameDtoProperty()
                    {
                        NameType = new Dtos.DtoProperties.PersonNameTypeDtoProperty()
                        {
                            Category = Dtos.PersonNameType2.Personal,
                            Detail =
                                new Dtos.GuidObject2(
                                        nickNameTypeItem.Guid)
                        },
                        FullName = person.Nickname,
                        Preference = !string.IsNullOrEmpty(preferredNameType) && preferredNameType.Equals("NICKNAME", StringComparison.CurrentCultureIgnoreCase) ?
                           Dtos.EnumProperties.PersonNamePreference.Preferred : (PersonNamePreference?)null,
                    };

                    personNames.Add(nickName);
                }
            }

            // Name History
            if ((person.FormerNames != null) && (person.FormerNames.Any()))
            {
                var historyNameTypeItem = (await _referenceDataRepository.GetPersonNameTypesAsync(false))
                    .FirstOrDefault(pn => pn.Code == "HISTORY");
                if (historyNameTypeItem != null)
                {
                    foreach (var name in person.FormerNames)
                    {
                        var formerName = new Dtos.DtoProperties.PersonNameDtoProperty()
                        {
                            NameType = new Dtos.DtoProperties.PersonNameTypeDtoProperty()
                            {
                                Category = Dtos.PersonNameType2.Personal,
                                Detail =
                                    new Dtos.GuidObject2(
                                            historyNameTypeItem.Guid)
                            },
                            FullName = BuildFullName("FM", "", name.GivenName, name.MiddleName, name.FamilyName, ""),
                            FirstName = string.IsNullOrEmpty(name.GivenName) ? null : name.GivenName,
                            MiddleName = string.IsNullOrEmpty(name.MiddleName) ? null : name.MiddleName,
                            LastName = string.IsNullOrEmpty(name.FamilyName) ? null : name.FamilyName,
                            LastNamePrefix =
                                name.FamilyName.Contains(" ")
                                    ? name.FamilyName.Split(" ".ToCharArray())[0].ToString()
                                    : null
                        };
                        personNames.Add(formerName);
                    }
                }
            }
            return personNames;
        }

        private string BuildFullName(string preferredName, string prefix, string first, string middle, string last,
            string suffix)
        {
            string fullName = "";
            if (string.IsNullOrEmpty(preferredName)) preferredName = "FM";
            if ((first != null) && (first.Length == 1)) first = string.Concat(first, ".");
            if ((middle != null) && (middle.Length == 1)) middle = string.Concat(middle, ".");
            var firstInitial = !string.IsNullOrEmpty(first) ? string.Concat(first.Remove(1), ". ") : string.Empty;
            var middleInitial = !string.IsNullOrEmpty(middle) ? string.Concat(middle.Remove(1), ". ") : string.Empty;
            if (!string.IsNullOrEmpty(suffix)) suffix = string.Concat(", ", suffix);
            first = !string.IsNullOrEmpty(first) ? string.Concat(first, " ") : string.Empty;
            middle = !string.IsNullOrEmpty(middle) ? string.Concat(middle, " ") : string.Empty;
            prefix = !string.IsNullOrEmpty(prefix) ? string.Concat(prefix, " ") : string.Empty;

            switch (preferredName.ToUpper())
            {
                case ("IM"):
                    fullName = string.Concat(prefix, firstInitial, middle, last, suffix);
                    break;
                case ("II"):
                    fullName = string.Concat(prefix, firstInitial, middleInitial, last, suffix);
                    break;
                case ("FM"):
                    fullName = string.Concat(prefix, first, middle, last, suffix);
                    break;
                case ("FI"):
                    fullName = string.Concat(prefix, first, middleInitial, last, suffix);
                    break;
                default:
                    fullName = preferredName;
                    break;
            }
            return fullName.Trim();
        }

        private Dtos.EnumProperties.GenderType2? GetGenderType2(string gender)
        {
            switch (gender)
            {
                case "M":
                    {
                        return Dtos.EnumProperties.GenderType2.Male;
                    }
                case "F":
                    {
                        return Dtos.EnumProperties.GenderType2.Female;
                    }
                default:
                    {
                        return Dtos.EnumProperties.GenderType2.Unknown;
                    }
            }
        }

        private async Task<Dtos.DtoProperties.PersonMaritalStatusDtoProperty> GetPersonMaritalStatusGuidAsync(
            string maritalStatusCode)
        {
            Dtos.DtoProperties.PersonMaritalStatusDtoProperty maritalStatusDto =
                new Dtos.DtoProperties.PersonMaritalStatusDtoProperty();
            // get the marital status guid and category
            if (!string.IsNullOrEmpty(maritalStatusCode))
            {
                var maritalStatusEntity =
                    (await _referenceDataRepository.MaritalStatusesAsync()).FirstOrDefault(
                        m => m.Code == maritalStatusCode);
                if (maritalStatusEntity != null && !string.IsNullOrEmpty(maritalStatusEntity.Guid))
                {
                    maritalStatusDto.Detail = new Dtos.GuidObject2(maritalStatusEntity.Guid);
                    switch (maritalStatusEntity.Type)
                    {
                        case (Ellucian.Colleague.Domain.Base.Entities.MaritalStatusType.Single):
                            maritalStatusDto.MaritalCategory = Dtos.EnumProperties.PersonMaritalStatusCategory.Single;
                            break;
                        case (Ellucian.Colleague.Domain.Base.Entities.MaritalStatusType.Married):
                            maritalStatusDto.MaritalCategory = Dtos.EnumProperties.PersonMaritalStatusCategory.Married;
                            break;
                        case (Ellucian.Colleague.Domain.Base.Entities.MaritalStatusType.Divorced):
                            maritalStatusDto.MaritalCategory = Dtos.EnumProperties.PersonMaritalStatusCategory.Divorced;
                            break;
                        case (Ellucian.Colleague.Domain.Base.Entities.MaritalStatusType.Separated):
                            maritalStatusDto.MaritalCategory = Dtos.EnumProperties.PersonMaritalStatusCategory.Separated;
                            break;
                        case (Ellucian.Colleague.Domain.Base.Entities.MaritalStatusType.Widowed):
                            maritalStatusDto.MaritalCategory = Dtos.EnumProperties.PersonMaritalStatusCategory.Widowed;
                            break;
                        default:
                            maritalStatusDto.MaritalCategory = Dtos.EnumProperties.PersonMaritalStatusCategory.Single;
                            break;
                    }
                }
            }
            return maritalStatusDto;
        }

        private async Task<IEnumerable<Dtos.DtoProperties.PersonRaceDtoProperty>> GetPersonRaceGuidsAsync(
            IEnumerable<string> raceCodes)
        {
            List<Dtos.DtoProperties.PersonRaceDtoProperty> raceDtos =
                new List<Dtos.DtoProperties.PersonRaceDtoProperty>();

            // get the race guids and category
            if (raceCodes != null && raceCodes.Count() > 0)
            {
                foreach (var raceCode in raceCodes)
                {
                    Dtos.DtoProperties.PersonRaceDtoProperty raceDto = new Dtos.DtoProperties.PersonRaceDtoProperty();

                    var raceEntity =
                        (await _referenceDataRepository.RacesAsync()).FirstOrDefault(r => r.Code == raceCode);
                    if (raceEntity != null && !string.IsNullOrEmpty(raceEntity.Guid))
                    {
                        raceDto.Race = new Dtos.GuidObject2(raceEntity.Guid);
                        var raceReportingItem = new Dtos.DtoProperties.PersonRaceReporting();
                        raceReportingItem.Country = new Dtos.DtoProperties.PersonRaceReportingCountry()
                        {
                            Code = Dtos.CountryCodeType.USA,
                            RacialCategory = null
                        };

                        switch (raceEntity.Type)
                        {
                            case (RaceType.AmericanIndian):
                                raceReportingItem.Country.RacialCategory =
                                    Dtos.EnumProperties.PersonRaceCategory.AmericanIndianOrAlaskaNative;
                                break;
                            case (RaceType.Asian):
                                raceReportingItem.Country.RacialCategory = Dtos.EnumProperties.PersonRaceCategory.Asian;
                                break;
                            case (RaceType.Black):
                                raceReportingItem.Country.RacialCategory =
                                    Dtos.EnumProperties.PersonRaceCategory.BlackOrAfricanAmerican;
                                break;
                            case (RaceType.PacificIslander):
                                raceReportingItem.Country.RacialCategory =
                                    Dtos.EnumProperties.PersonRaceCategory.HawaiianOrPacificIslander;
                                break;
                            case (RaceType.White):
                                raceReportingItem.Country.RacialCategory = Dtos.EnumProperties.PersonRaceCategory.White;
                                break;
                        }
                        if (raceEntity.Type != null)
                        {
                            var raceReporting = new List<Dtos.DtoProperties.PersonRaceReporting>();
                            raceReporting.Add(raceReportingItem);
                            raceDto.Reporting = raceReporting;
                        }

                        raceDtos.Add(raceDto);
                    }
                }
            }
            return raceDtos;
        }

        private async Task<Dtos.GuidObject2> GetPersonReligionGuidAsync(string religionCode)
        {
            Dtos.GuidObject2 religionDto = new Dtos.GuidObject2();
            // get the ethnicity guid and category
            if (!string.IsNullOrEmpty(religionCode))
            {
                try
                {
                    var religionEntity =
                        (await _referenceDataRepository.DenominationsAsync()).FirstOrDefault(r => r.Code == religionCode);
                    if (religionEntity != null && !string.IsNullOrEmpty(religionEntity.Guid))
                    {
                        religionDto = new Dtos.GuidObject2(religionEntity.Guid);
                    }
                }
                catch
                {
                    // Do nothing if not found in denominations table
                }
            }
            return religionDto;
        }

        private async Task<Dtos.DtoProperties.PersonEthnicityDtoProperty> GetPersonEthnicityGuidAsync(
            IEnumerable<string> ethnicityCodes)
        {
            Dtos.DtoProperties.PersonEthnicityDtoProperty ethnicityDto =
                new Dtos.DtoProperties.PersonEthnicityDtoProperty();
            // get the ethnicity guid and category
            if (ethnicityCodes != null && ethnicityCodes.Count() > 0)
            {
                var ethnicityEntity =
                    (await _referenceDataRepository.EthnicitiesAsync()).FirstOrDefault(
                        e => e.Code == ethnicityCodes.First());
                if (ethnicityEntity != null && !string.IsNullOrEmpty(ethnicityEntity.Guid))
                {
                    ethnicityDto.EthnicGroup = new Dtos.GuidObject2(ethnicityEntity.Guid);
                    var ethnicityReportingItem = new Dtos.DtoProperties.PersonEthnicityReporting();
                    ethnicityReportingItem.Country = new Dtos.DtoProperties.PersonEthnicityReportingCountry()
                    {
                        Code = Dtos.CountryCodeType.USA,
                        EthnicCategory = null
                    };

                    switch (ethnicityEntity.Type)
                    {
                        case EthnicityType.Hispanic:
                            ethnicityReportingItem.Country.EthnicCategory =
                                Dtos.EnumProperties.PersonEthnicityCategory.Hispanic;
                            break;
                        default:
                            ethnicityReportingItem.Country.EthnicCategory =
                                Dtos.EnumProperties.PersonEthnicityCategory.NonHispanic;
                            break;
                    }
                    var ethnicityReporting = new List<Dtos.DtoProperties.PersonEthnicityReporting>();
                    ethnicityReporting.Add(ethnicityReportingItem);
                    ethnicityDto.Reporting = ethnicityReporting;
                }
            }
            return ethnicityDto;
        }

        private async Task<IEnumerable<Dtos.DtoProperties.PersonRoleDtoProperty>> GetPersonRolesAsync(string personId,
            IEnumerable<Ellucian.Colleague.Domain.Base.Entities.PersonRole> personRoles)
        {
            var roles = new List<Dtos.DtoProperties.PersonRoleDtoProperty>();

            if (personRoles != null)
            {
                foreach (var personRole in personRoles)
                {
                    var role = new Dtos.DtoProperties.PersonRoleDtoProperty()
                    {
                        RoleType =
                            (Dtos.EnumProperties.PersonRoleType)Enum.Parse(typeof(Dtos.EnumProperties.PersonRoleType),
                                personRole.RoleType.ToString()),
                        StartOn = personRole.StartDate,
                        EndOn = personRole.EndDate
                    };
                    roles.Add(role);
                }
            }

            if (!roles.Any(r => r.RoleType == Dtos.EnumProperties.PersonRoleType.Instructor) && (await _personRepository.IsFacultyAsync(personId)))
            {
                roles.Add(new Dtos.DtoProperties.PersonRoleDtoProperty()
                {
                    RoleType = Dtos.EnumProperties.PersonRoleType.Instructor
                });
            }

            if (!roles.Any(r => r.RoleType == Dtos.EnumProperties.PersonRoleType.Advisor) && (await _personRepository.IsAdvisorAsync(personId)))
            {
                roles.Add(new Dtos.DtoProperties.PersonRoleDtoProperty()
                {
                    RoleType = Dtos.EnumProperties.PersonRoleType.Advisor
                });
            }

            if (!roles.Any(r => r.RoleType == Dtos.EnumProperties.PersonRoleType.Student) && (await _personRepository.IsStudentAsync(personId)))
            {
                roles.Add(new Dtos.DtoProperties.PersonRoleDtoProperty()
                {
                    RoleType = Dtos.EnumProperties.PersonRoleType.Student
                });
            }

            return roles.Any() ? roles : null;
        }

        private async Task<IEnumerable<Dtos.DtoProperties.PersonAddressDtoProperty>> GetAddresses2Async(
            IEnumerable<Domain.Base.Entities.Address> addressEntities, bool bypassCache = false)
        {
            var addressDtos = new List<Dtos.DtoProperties.PersonAddressDtoProperty>();
            if (addressEntities != null && addressEntities.Count() > 0)
            {
                foreach (var addressEntity in addressEntities)
                {
                    if (addressEntity != null && addressEntity.TypeCode != null && !string.IsNullOrEmpty(addressEntity.TypeCode))
                    {
                        // Repeate the address when we have multiple types.
                        // Multiple types are separated by sub-value marks.
                        var addressTypes = await _referenceDataRepository.GetAddressTypes2Async(bypassCache);
                        string[] addrTypes = addressEntity.TypeCode.Split(_SM);
                        for (int i = 0; i < addrTypes.Length; i++)
                        {
                            var addrType = addrTypes[i];
                            var addressDto = new Dtos.DtoProperties.PersonAddressDtoProperty();
                            addressDto.address = new Dtos.PersonAddress() { Id = addressEntity.Guid };
                            var type = addressTypes.FirstOrDefault(at => at.Code == addrType);
                            if (type != null)
                            {
                                addressDto.Type = new Dtos.DtoProperties.PersonAddressTypeDtoProperty();
                                addressDto.Type.AddressType =
                                    (Dtos.EnumProperties.AddressType)
                                        Enum.Parse(typeof(Dtos.EnumProperties.AddressType),
                                            type.AddressTypeCategory.ToString());
                                addressDto.Type.Detail = new Dtos.GuidObject2(type.Guid);
                            }
                            else
                            {
                                var addressId = await _personRepository.GetAddressIdFromGuidAsync(addressEntity.Guid);
                                var errorMessage = String.Format("Address record ID '{0}' contains an invalid address type '{1}'", addressId, addrType);
                                var exception = new RepositoryException(errorMessage);
                                exception.AddError(new Domain.Entities.RepositoryError("invalid.AddressType", errorMessage));
                                logger.Error(errorMessage);
                                throw exception;
  
                            }
                            if (addressEntity.IsPreferredResidence && i == 0)
                                addressDto.Preference = Dtos.EnumProperties.PersonPreference.Primary;
                            addressDto.AddressEffectiveStart = addressEntity.EffectiveStartDate;
                            addressDto.AddressEffectiveEnd = addressEntity.EffectiveEndDate;

                            if (addressEntity.SeasonalDates != null)
                            {
                                var seasonalOccupancies = new List<Dtos.DtoProperties.PersonAddressRecurrenceDtoProperty>();
                                int year = DateTime.Today.Year;
                                foreach (var assocEntity in addressEntity.SeasonalDates)
                                {
                                    try
                                    {
                                        int endYear = year;
                                        int startMonth = int.Parse(assocEntity.StartOn.Split("/".ToCharArray())[0]);
                                        int startDay = int.Parse(assocEntity.StartOn.Split("/".ToCharArray())[1]);
                                        int endMonth = int.Parse(assocEntity.EndOn.Split("/".ToCharArray())[0]);
                                        int endDay = int.Parse(assocEntity.EndOn.Split("/".ToCharArray())[1]);
                                        if (endMonth < startMonth) endYear = endYear + 1;

                                        var recurrence = new Dtos.Recurrence3()
                                        {
                                            TimePeriod = new Dtos.RepeatTimePeriod2()
                                            {
                                                StartOn = new DateTime(year, startMonth, startDay),
                                                EndOn = new DateTime(endYear, endMonth, endDay)
                                            }
                                        };
                                        recurrence.RepeatRule = new Dtos.RepeatRuleDaily()
                                        {
                                            Type = Dtos.FrequencyType2.Daily,
                                            Interval = 1,
                                            Ends = new Dtos.RepeatRuleEnds() { Date = new DateTime(year, endMonth, endDay) }
                                        };
                                        seasonalOccupancies.Add(
                                            new Dtos.DtoProperties.PersonAddressRecurrenceDtoProperty()
                                            {
                                                Recurrence = recurrence
                                            });
                                    }
                                    catch
                                    {
                                        // Invalid seasonal start or end dates, just ignore and don't include
                                    }
                                }
                                if (seasonalOccupancies.Any())
                                {
                                    addressDto.SeasonalOccupancies = seasonalOccupancies;
                                }
                            }

                            addressDtos.Add(addressDto);
                        }
                    }
                }
            }
            return addressDtos;
        }

        private async Task<IEnumerable<Dtos.DtoProperties.PersonPhoneDtoProperty>> GetPhones2Async(
            IEnumerable<Domain.Base.Entities.Phone> phoneEntities, bool bypassCache = false)
        {
            var phoneDtos = new List<Dtos.DtoProperties.PersonPhoneDtoProperty>();
            if (phoneEntities != null && phoneEntities.Count() > 0)
            {
                foreach (var phoneEntity in phoneEntities)
                {
                    string guid = "";
                    string category = "Other";
                    try
                    {
                        var phoneTypeEntity =
                            (await _referenceDataRepository.GetPhoneTypesAsync(bypassCache)).FirstOrDefault(
                                pt => pt.Code == phoneEntity.TypeCode);
                        if (phoneTypeEntity != null)
                        {
                            guid = phoneTypeEntity.Guid;
                            category = phoneTypeEntity.PhoneTypeCategory.ToString();
                        }

                        var phoneDto = new Dtos.DtoProperties.PersonPhoneDtoProperty()
                        {
                            Number = phoneEntity.Number,
                            Extension = string.IsNullOrEmpty(phoneEntity.Extension) ? null : phoneEntity.Extension,
                            Type = new Dtos.DtoProperties.PersonPhoneTypeDtoProperty()
                            {
                                PhoneType =
                                    (Dtos.EnumProperties.PersonPhoneTypeCategory)
                                        Enum.Parse(typeof(Dtos.EnumProperties.PersonPhoneTypeCategory), category),
                                Detail = string.IsNullOrEmpty(guid) ? null : new Dtos.GuidObject2(guid)
                            }
                        };
                        if (!string.IsNullOrEmpty(phoneEntity.CountryCallingCode)) phoneDto.CountryCallingCode = phoneEntity.CountryCallingCode;
                        if (phoneEntity.IsPreferred) phoneDto.Preference = Dtos.EnumProperties.PersonPreference.Primary;

                        phoneDtos.Add(phoneDto);
                    }
                    catch
                    {
                        // do not fail if we can't find a guid from the code table or category
                        // Just exclude the phone number from the output.
                    }
                }
            }
            return phoneDtos;
        }

        private async Task<IEnumerable<Dtos.DtoProperties.PersonEmailDtoProperty>> GetEmailAddresses2(
            IEnumerable<Domain.Base.Entities.EmailAddress> emailAddressEntities, bool bypassCache = false)
        {
            var emailAddressDtos = new List<Dtos.DtoProperties.PersonEmailDtoProperty>();
            if (emailAddressEntities != null && emailAddressEntities.Count() > 0)
            {
                foreach (var emailAddressEntity in emailAddressEntities)
                {
                    string guid = "";
                    string category = "Other";
                    try
                    {
                        var codeItem =
                            (await _referenceDataRepository.GetEmailTypesAsync(bypassCache)).FirstOrDefault(
                                pt => pt.Code == emailAddressEntity.TypeCode);
                        if (codeItem != null)
                        {
                            guid = codeItem.Guid;
                            category = codeItem.EmailTypeCategory.ToString();
                        }

                        var addressDto = new Dtos.DtoProperties.PersonEmailDtoProperty()
                        {
                            Type = new Dtos.DtoProperties.PersonEmailTypeDtoProperty()
                            {
                                EmailType = (Dtos.EmailTypeList)Enum.Parse(typeof(Dtos.EmailTypeList), category),
                                Detail = string.IsNullOrEmpty(guid) ? null : new Dtos.GuidObject2(guid)
                            },
                            Address = emailAddressEntity.Value
                        };
                        if (emailAddressEntity.IsPreferred)
                            addressDto.Preference = Dtos.EnumProperties.PersonEmailPreference.Primary;

                        emailAddressDtos.Add(addressDto);
                    }
                    catch
                    {
                        // do not fail if we can't find a guid from the code table or translate the cateory
                        // Just exclude this email address.
                    }
                }
            }
            return emailAddressDtos;
        }

        private async Task<string> GetPersonCountryAsync(string countryCode)
        {
            string iso3Code = "";
            if (!string.IsNullOrEmpty(countryCode))
            {
                try
                {
                    iso3Code =
                        (await _referenceDataRepository.GetCountryCodesAsync()).FirstOrDefault(
                            cc => cc.Code == countryCode).Iso3Code;
                }
                catch
                {
                    iso3Code = (countryCode.Length > 3) ? countryCode.Substring(0, 3) : countryCode;
                }
            }
            return iso3Code;
        }

        private async Task<Dtos.DtoProperties.PersonPrivacyDtoProperty> GetPersonPrivacyAsync(
            PrivacyStatusType privacyStatus, string privacyStatusCode, bool bypassCache)
        {
            Dtos.DtoProperties.PersonPrivacyDtoProperty personPrivacy =
                new Dtos.DtoProperties.PersonPrivacyDtoProperty();

            try
            {
                if (string.IsNullOrEmpty(privacyStatusCode))
                {
                    var statusEntity =
                        (await _referenceDataRepository.GetPrivacyStatusesAsync(bypassCache)).FirstOrDefault(
                            ps => ps.PrivacyStatusType == privacyStatus);
                    personPrivacy.PrivacyCategory = (privacyStatus == PrivacyStatusType.restricted)
                        ? Dtos.PrivacyStatusType.Restricted
                        : Dtos.PrivacyStatusType.Unrestricted;
                    personPrivacy.Detail = new Dtos.GuidObject2(statusEntity.Guid);
                }
                else
                {
                    var statusEntity =
                        (await _referenceDataRepository.GetPrivacyStatusesAsync(bypassCache)).FirstOrDefault(
                            ps => ps.Code == privacyStatusCode);
                    personPrivacy.PrivacyCategory = (statusEntity.PrivacyStatusType == PrivacyStatusType.restricted)
                        ? Dtos.PrivacyStatusType.Restricted
                        : Dtos.PrivacyStatusType.Unrestricted;
                    personPrivacy.Detail = new Dtos.GuidObject2(statusEntity.Guid);
                }
            }
            catch
            {
                // Do nothing if the code doesn't exist.
            }

            return personPrivacy;
        }

        private IEnumerable<Dtos.DtoProperties.PersonLanguageDtoProperty> GetPersonLanguages(
            IEnumerable<Ellucian.Colleague.Domain.Base.Entities.PersonLanguage> languages)
        {
            var personLanguages = new List<Dtos.DtoProperties.PersonLanguageDtoProperty>();
            foreach (var language in languages)
            {
                if (language != null && !string.IsNullOrEmpty(language.Code))
                {
                    personLanguages.Add(new Dtos.DtoProperties.PersonLanguageDtoProperty()
                    {
                        Code = language.Code,
                        Preference =
                            (language.IsPrimary
                                ? Dtos.EnumProperties.PersonLanguagePreference.Primary
                                : Dtos.EnumProperties.PersonLanguagePreference.Secondary)
                    });
                }
            }
            return personLanguages;
        }

        private async Task<Dtos.DtoProperties.PersonCitizenshipDtoProperty> GetPersonCitizenshipAsync(
            string alienStatusCode, bool bypassCache = false)
        {
            var citizenShipStatus = new Dtos.DtoProperties.PersonCitizenshipDtoProperty();

            if (!string.IsNullOrEmpty(alienStatusCode))
            {
                try
                {
                    var citizenshipStatusEntity =
                        (await _referenceDataRepository.GetCitizenshipStatusesAsync(bypassCache)).FirstOrDefault(
                            ct => ct.Code == alienStatusCode);
                    if (citizenshipStatusEntity != null)
                        citizenShipStatus = new Dtos.DtoProperties.PersonCitizenshipDtoProperty()
                        {
                            Category =
                                (Dtos.CitizenshipStatusType)
                                    Enum.Parse(typeof(Dtos.CitizenshipStatusType),
                                        citizenshipStatusEntity.CitizenshipStatusType.ToString()),
                            Detail = new Dtos.GuidObject2(citizenshipStatusEntity.Guid)
                        };
                }
                catch
                {
                    // do not fail if we can't find a guid and category from the code table
                    // just return an empty status object
                }
            }
            else
            {
                return null;
            }

            return citizenShipStatus;
        }

        private async Task<Dtos.DtoProperties.PersonVisaDtoProperty> GetPersonVisaStatusAsync(PersonVisa personVisaEntity, bool bypassCache = false)
        {
            Dtos.DtoProperties.PersonVisaDtoProperty personVisa = new Dtos.DtoProperties.PersonVisaDtoProperty();

            if (personVisaEntity != null)
            {
                try
                {
                    var visaEntity =
                        (await _referenceDataRepository.GetVisaTypesAsync(bypassCache)).FirstOrDefault(
                            vt => vt.Code == personVisaEntity.Type);
                    personVisa = new Dtos.DtoProperties.PersonVisaDtoProperty()
                    {
                        Category =
                            (Dtos.VisaTypeCategory)
                                Enum.Parse(typeof(Dtos.VisaTypeCategory), visaEntity.VisaTypeCategory.ToString()),
                        Detail = new Dtos.GuidObject2(visaEntity.Guid),
                        Status =
                            (personVisaEntity.ExpireDate != null &&
                             personVisaEntity.ExpireDate.GetValueOrDefault().Date <= DateTime.Today.Date)
                                ? Dtos.EnumProperties.VisaStatus.Expired
                                : Dtos.EnumProperties.VisaStatus.Current,
                        StartOn = personVisaEntity.IssueDate,
                        EndOn = personVisaEntity.ExpireDate
                    };
                }
                catch
                {
                    // do not fail if we can't find a guid and category from the code table
                    // just return an empty status object
                }
            }

            return personVisa;
        }

        private async Task<IEnumerable<Dtos.DtoProperties.PersonIdentityDocument>> GetPersonIdentityDocumentsAsync(PersonIntegration personEntity, bool bypassCache = false)
        {
            var personIdentityDocuments = new List<Dtos.DtoProperties.PersonIdentityDocument>();

            // Passport
            var passportEntity = personEntity.Passport;
            if (passportEntity != null)
            {
                if (!string.IsNullOrEmpty(passportEntity.PassportNumber))
                {
                    string guid = "";
                    try
                    {
                        var identityDocumentType = (await _referenceDataRepository.GetIdentityDocumentTypesAsync(bypassCache)).FirstOrDefault(
                                pt =>
                                    pt.IdentityDocumentTypeCategory ==
                                Domain.Base.Entities.IdentityDocumentTypeCategory.Passport);
                        if (identityDocumentType != null)
                            guid = identityDocumentType.Guid;
                    }
                    catch
                    {
                        // do not fail if we can't find a guid from the code table
                    }

                    if (string.IsNullOrEmpty(passportEntity.IssuingCountry.Trim()))
                    {
                        throw new ArgumentException(string.Concat("Person ID '", personEntity.Id, "': Passport number: ", passportEntity.PassportNumber, " does not have a valid Issuing Country set."));
                    }

                    var identityDocument = new Dtos.DtoProperties.PersonIdentityDocument()
                    {

                        DocumentId = passportEntity.PassportNumber,
                        ExpiresOn = passportEntity.ExpireDate,
                        Type = new Dtos.DtoProperties.PersonIdentityDocumentType()
                        {
                            Category = Dtos.EnumProperties.PersonIdentityDocumentCategory.Passport,
                            Detail = (string.IsNullOrEmpty(guid)) ? null : new Dtos.GuidObject2(guid)
                        }
                    };
                    try
                    {
                        if (!string.IsNullOrEmpty(passportEntity.IssuingCountry.Trim()))
                        {
                            identityDocument.Country = new PersonIdentityDocumentCountryDtoProperty()
                            {
                                Code = (Dtos.EnumProperties.IsoCode)System.Enum.Parse(typeof(Dtos.EnumProperties.IsoCode), (await GetPersonCountryAsync(passportEntity.IssuingCountry)))
                            };
                        }
                        personIdentityDocuments.Add(identityDocument);
                    }
                    catch
                    {
                        // Do not include identity document
                    }
                }
            }

            // Drivers License
            var driverEntity = personEntity.DriverLicense;
            if (driverEntity != null)
            {
                if (!string.IsNullOrEmpty(driverEntity.LicenseNumber))
                {
                    string guid = string.Empty;
                    try
                    {
                        var identityDocumentType = (await _referenceDataRepository.GetIdentityDocumentTypesAsync(bypassCache)).FirstOrDefault(
                                pt =>
                                    pt.IdentityDocumentTypeCategory ==
                                Domain.Base.Entities.IdentityDocumentTypeCategory.PhotoId);
                        if (identityDocumentType != null)
                            guid = identityDocumentType.Guid;
                    }
                    catch
                    {
                        // do not fail if we can't find a guid from the code table
                    }

                    var stateInfo = (await _referenceDataRepository.GetStateCodesAsync()).FirstOrDefault(s => s.Code == driverEntity.IssuingState);

                    if (stateInfo != null)
                    {
                        //default country to USA if it is not set
                        var countryCode = await _personRepository.GetHostCountryAsync();
                        string countryCodeIso3 = string.Empty;
                        switch (countryCode)
                        {
                            case "USA":
                                countryCode = "US";
                                countryCodeIso3 = "USA";
                                break;
                            case "CANADA":
                                countryCode = "CA";
                                countryCodeIso3 = "CAN";
                                break;
                            default:
                                countryCode = "US";
                                countryCodeIso3 = "USA";
                                break;
                        }
                        if (!string.IsNullOrEmpty(stateInfo.CountryCode))
                        {
                            var country = stateInfo.CountryCode;
                            var countryEntity = (await _referenceDataRepository.GetCountryCodesAsync()).FirstOrDefault(x => x.Code == country);
                            if (countryEntity != null && !string.IsNullOrEmpty(countryEntity.IsoAlpha3Code))
                            {
                                countryCode = countryEntity.IsoCode;
                                countryCodeIso3 = countryEntity.IsoAlpha3Code;
                            }
                        }
                        string regionCode = string.Empty;
                        if (!string.IsNullOrEmpty(driverEntity.IssuingState))
                        {
                            // Validate the region code against the places table
                            regionCode = string.Concat(countryCode, "-", driverEntity.IssuingState);
                            var place = (await _personRepository.GetPlacesAsync()).FirstOrDefault(x => x.PlacesRegion == regionCode && x.PlacesCountry == countryCodeIso3);
                            if (place == null)
                            {
                                regionCode = string.Empty;
                            }
                        }

                        var identityDocument = new Dtos.DtoProperties.PersonIdentityDocument()
                        {
                            DocumentId = driverEntity.LicenseNumber,
                            ExpiresOn = driverEntity.ExpireDate,
                            Type = new Dtos.DtoProperties.PersonIdentityDocumentType()
                            {
                                Category = Dtos.EnumProperties.PersonIdentityDocumentCategory.PhotoId,
                                Detail = (string.IsNullOrEmpty(guid)) ? null : new Dtos.GuidObject2(guid)
                            }
                        };

                        //Colleague only supports drivers licenses for US and CA
                        if (!string.IsNullOrEmpty(regionCode))
                        {
                            identityDocument.Country = new PersonIdentityDocumentCountryDtoProperty()
                            {
                                Code = (IsoCode)Enum.Parse(typeof(IsoCode), countryCodeIso3),
                                Region = new AddressRegion()
                                {
                                    Code = regionCode
                                }
                            };
                        }

                        personIdentityDocuments.Add(identityDocument);
                    }
                    else
                    {
                        var identityDocument = new Dtos.DtoProperties.PersonIdentityDocument()
                        {
                            DocumentId = driverEntity.LicenseNumber,
                            ExpiresOn = driverEntity.ExpireDate,
                            Type = new Dtos.DtoProperties.PersonIdentityDocumentType()
                            {
                                Category = Dtos.EnumProperties.PersonIdentityDocumentCategory.PhotoId,
                                Detail = (string.IsNullOrEmpty(guid)) ? null : new Dtos.GuidObject2(guid)
                            }
                        };

                        personIdentityDocuments.Add(identityDocument);
                    }
                }
            }
            
            // Other Identity Documents
            if (personEntity.IdentityDocuments != null)
            {
                foreach (var document in personEntity.IdentityDocuments)
                {
                    if (!string.IsNullOrEmpty(document.Number))
                    {
                        string guid = "";
                        try
                        {
                            var identityDocumentType = (await _referenceDataRepository.GetIdentityDocumentTypesAsync(bypassCache)).FirstOrDefault(
                                    pt =>
                                        pt.IdentityDocumentTypeCategory ==
                                    Domain.Base.Entities.IdentityDocumentTypeCategory.Other);
                            if (identityDocumentType != null)
                                guid = identityDocumentType.Guid;
                        }
                        catch
                        {
                            // do not fail if we can't find a guid from the code table
                        }

                        var identityDocument = new Dtos.DtoProperties.PersonIdentityDocument()
                        {

                            DocumentId = document.Number,
                            ExpiresOn = document.ExpireDate,
                            Type = new Dtos.DtoProperties.PersonIdentityDocumentType()
                            {
                                Category = Dtos.EnumProperties.PersonIdentityDocumentCategory.Other,
                                Detail = (string.IsNullOrEmpty(guid)) ? null : new Dtos.GuidObject2(guid)
                            }
                        };
                        try
                        {
                            if (!string.IsNullOrEmpty(document.Country.Trim()))
                            {
                                identityDocument.Country = new PersonIdentityDocumentCountryDtoProperty()
                                {
                                    Code = (Dtos.EnumProperties.IsoCode)System.Enum.Parse(typeof(Dtos.EnumProperties.IsoCode), document.Country.Trim())
                                };
                                if (!string.IsNullOrEmpty(document.Region))
                                {
                                    identityDocument.Country.Region = new AddressRegion() { Code = document.Region };
                                }
                            }
                            personIdentityDocuments.Add(identityDocument);
                        }
                        catch
                        {
                            // Do not include identity document
                        }
                    }
                }
            }

            return personIdentityDocuments.Any() ? personIdentityDocuments : null;
        }

        private async Task<IEnumerable<Dtos.GuidObject2>> GetPersonInterestsAsync(List<string> interestCodes, bool bypassCache = false)
        {
            List<Dtos.GuidObject2> interestGuids = new List<Dtos.GuidObject2>();

            foreach (var interest in interestCodes)
            {
                try
                {
                    var firstOrDefault = (await _referenceDataRepository.GetInterestsAsync(bypassCache)).FirstOrDefault(
                        ic => ic.Code == interest);
                    if (firstOrDefault != null)
                    {
                        var guid = firstOrDefault.Guid;
                        interestGuids.Add(new Dtos.GuidObject2(guid));
                    }
                }
                catch
                {
                    // do nothing if guid does not exist
                }
            }

            return interestGuids;
        }

        private async Task<IEnumerable<Dtos.DtoProperties.PersonSocialMediaDtoProperty>> GetPersonSocialMediaAsync(
            List<Ellucian.Colleague.Domain.Base.Entities.SocialMedia> mediaTypes, bool bypassCache = false)
        {
            List<Dtos.DtoProperties.PersonSocialMediaDtoProperty> socialMediaEntries =
                new List<Dtos.DtoProperties.PersonSocialMediaDtoProperty>();

            foreach (var mediaType in mediaTypes)
            {
                try
                {
                    var socialMedia = new Dtos.DtoProperties.PersonSocialMediaDtoProperty();
                    if (mediaType.TypeCode.ToLowerInvariant() == "website")
                    {
                        string guid = "";
                        var socialMediaEntity =
                            (await _referenceDataRepository.GetSocialMediaTypesAsync(bypassCache)).FirstOrDefault(
                                ic => ic.Type.ToString() == mediaType.TypeCode);
                        if (socialMediaEntity != null)
                        {
                            guid = socialMediaEntity.Guid;
                            socialMedia = new Dtos.DtoProperties.PersonSocialMediaDtoProperty()
                            {
                                Type = new Dtos.DtoProperties.PersonSocialMediaType()
                                {
                                    Category =
                                        (Dtos.SocialMediaTypeCategory)
                                            Enum.Parse(typeof(Dtos.SocialMediaTypeCategory),
                                                mediaType.TypeCode.ToString()),
                                    Detail = new Dtos.GuidObject2(guid)
                                },
                                Address = mediaType.Handle
                            };
                        }
                        else
                        {
                            socialMedia = new Dtos.DtoProperties.PersonSocialMediaDtoProperty()
                            {
                                Type = new Dtos.DtoProperties.PersonSocialMediaType()
                                {
                                    Category =
                                        (Dtos.SocialMediaTypeCategory)
                                            Enum.Parse(typeof(Dtos.SocialMediaTypeCategory),
                                                mediaType.TypeCode.ToString())
                                },
                                Address = mediaType.Handle
                            };
                        }
                    }
                    else
                    {
                        var socialMediaEntity =
                            (await _referenceDataRepository.GetSocialMediaTypesAsync(bypassCache)).FirstOrDefault(
                                ic => ic.Code == mediaType.TypeCode);
                        socialMedia = new Dtos.DtoProperties.PersonSocialMediaDtoProperty()
                        {
                            Type = new Dtos.DtoProperties.PersonSocialMediaType()
                            {
                                Category =
                                    (Dtos.SocialMediaTypeCategory)
                                        Enum.Parse(typeof(Dtos.SocialMediaTypeCategory),
                                            socialMediaEntity.Type.ToString()),
                                Detail = new Dtos.GuidObject2(socialMediaEntity.Guid)
                            },
                            Address = mediaType.Handle
                        };
                    }
                    if (mediaType.IsPreferred) socialMedia.Preference = Dtos.EnumProperties.PersonPreference.Primary;

                    socialMediaEntries.Add(socialMedia);
                }
                catch
                {
                    // Do not include code since we couldn't find a category
                }
            }

            return socialMediaEntries;
        }

        #endregion

        #region Convert Person Dto to Entity Methods

        private async Task<Domain.Base.Entities.Person> ConvertPersonDtoToEntityAsync(string personId, Dtos.Person personDto)
        {
            if (personDto == null || string.IsNullOrEmpty(personDto.Guid))
                throw new ArgumentNullException("personDto", "Must provide guid for person");

            if (personDto.PersonNames == null)
                throw new ArgumentNullException("personDto", "Must provide person name");

            // person primary name
            var primaryName =
                personDto.PersonNames.Where(pn => pn.NameType == Dtos.PersonNameType.Primary).FirstOrDefault();
            if (primaryName == null || string.IsNullOrEmpty(primaryName.FirstName) ||
                string.IsNullOrEmpty(primaryName.LastName))
                throw new ArgumentNullException("personDto", "Must provide person primary first and last name");
            // person birth name
            var birthName = personDto.PersonNames.Where(pn => pn.NameType == Dtos.PersonNameType.Birth).FirstOrDefault();

            var personEntity = new Domain.Base.Entities.Person(personId, primaryName.LastName);
            personEntity.Guid = personDto.Guid;
            personEntity.Prefix = MapPrefixCode(primaryName.Title);
            personEntity.FirstName = primaryName.FirstName;
            personEntity.MiddleName = primaryName.MiddleName;
            // ignoring last name prefix since Colleague does not support it
            personEntity.Suffix = MapSuffixCode(primaryName.Pedigree);
            personEntity.Nickname = primaryName.PreferredName;
            if (birthName != null)
            {
                personEntity.BirthNameLast = birthName.LastName;
                personEntity.BirthNameFirst = birthName.FirstName;
                personEntity.BirthNameMiddle = birthName.MiddleName;
            }
            personEntity.BirthDate = personDto.BirthDate;
            personEntity.DeceasedDate = personDto.DeceasedDate;
            personEntity.Gender = MapGenderType(personDto.GenderType);
            personEntity.MaritalStatusCode = await MapMaritalStatusAsync(personDto.MaritalStatus);
            personEntity.EthnicCodes = await MapEthnicityAsync(personDto.Ethnicity);
            personEntity.RaceCodes = await MapRacesAsync(personDto.Races);
            personEntity.GovernmentId = MapSsn(personDto.Credentials);
            // person alt Ids
            var personAltIdEntities = MapPersonAltIds(personDto.Credentials);
            if (personAltIdEntities != null && personAltIdEntities.Count() > 0)
            {
                foreach (var personAltIdEntity in personAltIdEntities)
                {
                    personEntity.PersonAltIds.Add(personAltIdEntity);
                }
            }
            // email addresses
            var emailAddressEntities = MapEmailAddresses(personDto.EmailAddresses);
            if (emailAddressEntities != null && emailAddressEntities.Count() > 0)
            {
                foreach (var emailAddressEntity in emailAddressEntities)
                {
                    personEntity.EmailAddresses.Add(emailAddressEntity);
                }
            }

            return personEntity;
        }


        private async Task<Domain.Base.Entities.PersonIntegration> ConvertPerson2DtoToEntityAsync(string personId,
            Dtos.Person2 personDto)
        {
            if (personDto == null || string.IsNullOrEmpty(personDto.Id))
                throw new ArgumentNullException("personDto", "Must provide guid for person");

            PersonIntegration personEntity = null;
            personEntity = await ConvertPersonNames(personId, personDto.Id, personDto.PersonNames);
            personEntity.BirthDate = personDto.BirthDate;
            personEntity.DeceasedDate = personDto.DeceasedDate;

            // email addresses
            var emailAddressEntities = await MapEmailAddresses2(personDto.EmailAddresses);
            if (emailAddressEntities != null && emailAddressEntities.Count() > 0)
            {
                foreach (var emailAddressEntity in emailAddressEntities)
                {
                    personEntity.EmailAddresses.Add(emailAddressEntity);
                }
            }

            //Make sure birth date is before today's date
            if (personDto.BirthDate != null && personDto.BirthDate > DateTime.Today)
            {
                throw new InvalidOperationException("Date of birth cannot be after the current date.");
            }

            //Make sure birth date is before deceased date
            if (personDto.BirthDate != null && personDto.DeceasedDate != null && personDto.BirthDate > personDto.DeceasedDate)
            {
                throw new InvalidOperationException("Date of birth cannot be after deceased date.");
            }

            //privacy status
            if (personDto.PrivacyStatus != null)
            {
                var privacyStatusEntity = await ConvertPerson2PrivacyStatusAsync(personDto.PrivacyStatus);
                if (privacyStatusEntity == null)
                {
                    personEntity.PrivacyStatus = PrivacyStatusType.unrestricted;
                }
                else
                {
                    personEntity.PrivacyStatus = privacyStatusEntity.PrivacyStatusType;
                    personEntity.PrivacyStatusCode = privacyStatusEntity.Code;
                }
            }

            //Gender
            if (personDto.GenderType != null)
            {
                personEntity.Gender = ConvertGenderType2String(personDto.GenderType);
            }

            //religion
            if (personDto.Religion != null)
            {
                personEntity.Religion = await ConvertPerson2DtoReligionCodeToEntityAsync(personDto.Religion.Id);
            }

            //ethnicity code
            if (personDto.Ethnicity != null)
            {
                personEntity.EthnicCodes = await ConvertPerson2DtoEthnicityCodesEntityAsync(personDto.Ethnicity);
            }

            //races
            if (personDto.Races != null && personDto.Races.Any())
            {
                personEntity.RaceCodes = await ConvertPerson2DtoRaceCodesToEntityAsync(personDto.Races);
            }

            //language
            if (personDto.Languages != null && personDto.Languages.Any())
            {
                personEntity.Languages.AddRange(ConvertPerson2DtoLanguagesToEntity(personId, personDto.Id,
                    personDto.Languages));
            }

            //marital status
            if (personDto.MaritalStatus != null)
            {
                personEntity.MaritalStatus = await ConvertPerson2MaritalStatusDtoToEntityAsync(personDto.MaritalStatus);
            }

            //citizenshipStatus
            if (personDto.CitizenshipStatus != null)
            {
                personEntity.AlienStatus =
                    await ConvertPerson2CitizenshipStatusDtoToEntityAsync(personDto.CitizenshipStatus);
            }

            //countryOfBirth
            if (!string.IsNullOrEmpty(personDto.CountryOfBirth))
            {
                personEntity.BirthCountry = await ConvertPerson2CountryCodeDtoToEntityAsync(personDto.CountryOfBirth);
            }

            //countryOfCitizenship
            if (!string.IsNullOrEmpty(personDto.CitizenshipCountry))
            {
                personEntity.Citizenship = await ConvertPerson2CountryCodeDtoToEntityAsync(personDto.CitizenshipCountry);
            }

            //roles
            if (personDto.Roles != null && personDto.Roles.Any())
            {
                personEntity.Roles.AddRange(ConvertPerson2DtoRolesToEntity(personDto.Roles));
            }

            //identityDocuments
            if (personDto.IdentityDocuments != null && personDto.IdentityDocuments.Any())
            {
                personEntity.Passport =
                    await
                        ConvertPerson2DtoPassportDocumentToEntityAsync(personId, personDto.Id,
                            personDto.IdentityDocuments);
                personEntity.DriverLicense =
                    await
                        ConvertPerson2DtoDriversLicenseToEntityDocumentAsync(personId, personDto.Id,
                            personDto.IdentityDocuments);
                personEntity.IdentityDocuments =
                    await
                        ConvertPerson2DtoIdentityDocumentsToEntityAsync(personId, personDto.Id,
                            personDto.IdentityDocuments);
            }

            // Social Media
            if (personDto.SocialMedia != null && personDto.SocialMedia.Any())
            {
                personEntity.SocialMedia.AddRange(await ConvertPerson2DtoSocialMediaToEntity(personDto.SocialMedia));
            }

            // credentials
            if (personDto.Credentials != null && personDto.Credentials.Any())
            {
                ConvertPerson2DtoCredsToEntity(personId, personDto.Credentials, personEntity);
            }

            // interests
            if (personDto.Interests != null && personDto.Interests.Any())
            {
                personEntity.Interests = await ConvertPerson2DtoInterestsToEntityAsync(personDto.Interests);
            }

            return personEntity;
        }

        private async Task<Domain.Base.Entities.PersonIntegration> ConvertPerson3DtoToEntityAsync(string personId,
            Dtos.Person3 personDto)
        {
            if (personDto == null || string.IsNullOrEmpty(personDto.Id))
                throw new ArgumentNullException("personDto", "Must provide guid for person");

            PersonIntegration personEntity = null;
            personEntity = await ConvertPersonNames(personId, personDto.Id, personDto.PersonNames);
            personEntity.BirthDate = personDto.BirthDate;
            personEntity.DeceasedDate = personDto.DeceasedDate;

            // email addresses
            var emailAddressEntities = await MapEmailAddresses2(personDto.EmailAddresses);
            if (emailAddressEntities != null && emailAddressEntities.Count() > 0)
            {
                foreach (var emailAddressEntity in emailAddressEntities)
                {
                    personEntity.EmailAddresses.Add(emailAddressEntity);
                }
            }

            //Make sure birth date is before today's date
            if (personDto.BirthDate != null && personDto.BirthDate > DateTime.Today)
            {
                throw new InvalidOperationException("Date of birth cannot be after the current date.");
            }

            //Make sure birth date is before deceased date
            if (personDto.BirthDate != null && personDto.DeceasedDate != null && personDto.BirthDate > personDto.DeceasedDate)
            {
                throw new InvalidOperationException("Date of birth cannot be after deceased date.");
            }

            //privacy status
            if (personDto.PrivacyStatus != null)
            {
                var privacyStatusEntity = await ConvertPerson2PrivacyStatusAsync(personDto.PrivacyStatus);
                if (privacyStatusEntity == null)
                {
                    personEntity.PrivacyStatus = PrivacyStatusType.unrestricted;
                }
                else
                {
                    personEntity.PrivacyStatus = privacyStatusEntity.PrivacyStatusType;
                    personEntity.PrivacyStatusCode = privacyStatusEntity.Code;
                }
            }

            //Gender
            if (personDto.GenderType != null)
            {
                personEntity.Gender = ConvertGenderType2String(personDto.GenderType);
            }

            //religion
            if (personDto.Religion != null)
            {
                personEntity.Religion = await ConvertPerson2DtoReligionCodeToEntityAsync(personDto.Religion.Id);
            }

            //ethnicity code
            if (personDto.Ethnicity != null)
            {
                personEntity.EthnicCodes = await ConvertPerson2DtoEthnicityCodesEntityAsync(personDto.Ethnicity);
            }

            //races
            if (personDto.Races != null && personDto.Races.Any())
            {
                personEntity.RaceCodes = await ConvertPerson2DtoRaceCodesToEntityAsync(personDto.Races);
            }

            //language
            if (personDto.Languages != null && personDto.Languages.Any())
            {
                personEntity.Languages.AddRange(ConvertPerson2DtoLanguagesToEntity(personId, personDto.Id,
                    personDto.Languages));
            }

            //marital status
            if (personDto.MaritalStatus != null)
            {
                personEntity.MaritalStatus = await ConvertPerson2MaritalStatusDtoToEntityAsync(personDto.MaritalStatus);
            }

            //citizenshipStatus
            if (personDto.CitizenshipStatus != null)
            {
                personEntity.AlienStatus =
                    await ConvertPerson2CitizenshipStatusDtoToEntityAsync(personDto.CitizenshipStatus);
            }

            //countryOfBirth
            if (!string.IsNullOrEmpty(personDto.CountryOfBirth))
            {
                personEntity.BirthCountry = await ConvertPerson2CountryCodeDtoToEntityAsync(personDto.CountryOfBirth);
            }

            //countryOfCitizenship
            if (!string.IsNullOrEmpty(personDto.CitizenshipCountry))
            {
                personEntity.Citizenship = await ConvertPerson2CountryCodeDtoToEntityAsync(personDto.CitizenshipCountry);
            }

            //roles
            if (personDto.Roles != null && personDto.Roles.Any())
            {
                personEntity.Roles.AddRange(ConvertPerson2DtoRolesToEntity(personDto.Roles));
            }

            //identityDocuments
            if (personDto.IdentityDocuments != null && personDto.IdentityDocuments.Any())
            {
                personEntity.Passport =
                    await
                        ConvertPerson2DtoPassportDocumentToEntityAsync(personId, personDto.Id,
                            personDto.IdentityDocuments);
                personEntity.DriverLicense =
                    await
                        ConvertPerson2DtoDriversLicenseToEntityDocumentAsync(personId, personDto.Id,
                            personDto.IdentityDocuments);
                personEntity.IdentityDocuments =
                    await
                        ConvertPerson2DtoIdentityDocumentsToEntityAsync(personId, personDto.Id,
                            personDto.IdentityDocuments);
            }

            // Social Media
            if (personDto.SocialMedia != null && personDto.SocialMedia.Any())
            {
                personEntity.SocialMedia.AddRange(await ConvertPerson2DtoSocialMediaToEntity(personDto.SocialMedia));
            }

            // credentials
            if (personDto.Credentials != null && personDto.Credentials.Any())
            {
                if (personDto.Credentials.Count(c => c.Type == Dtos.EnumProperties.CredentialType2.ColleagueUserName) > 0 && !string.IsNullOrEmpty(personId))
                {
                    // check to see if user added a Colleague Username; They cannot maintain a username through API's but 
                    // If they entered the same value that already stored in colleague we won't issue an error.
                    // Otherwise issue an error indicating that colleague usernames are to be maintained in colleague.
                    CredentialDtoProperty2 username = personDto.Credentials.FirstOrDefault(i => i.Type == Dtos.EnumProperties.CredentialType2.ColleagueUserName);

                    string[] personGuids = { personEntity.Guid };
                    await this.GetPersonPins(personGuids);
                    var personPin = _personPins.FirstOrDefault();

                    if (personPin == null || personPin.PersonPinUserId != username.Value)
                    {
                        throw new InvalidOperationException("You cannot add/edit Colleague usernames. You must maintain them in Colleague.");
                    }
                    if (personDto.Credentials.Count(c => c.Type == Dtos.EnumProperties.CredentialType2.ColleagueUserName) > 1)
                    {
                        throw new InvalidOperationException("You cannot include more than one Colleague username.");
                    }
                } else if (personDto.Credentials.Count(c => c.Type == Dtos.EnumProperties.CredentialType2.ColleagueUserName) > 0 && string.IsNullOrEmpty(personId)){
                    throw new InvalidOperationException("You cannot add/edit Colleague usernames. You must maintain them in Colleague.");
                }
                ConvertPerson3DtoCredsToEntity(personId, personDto.Credentials, personEntity);
            }

            // interests
            if (personDto.Interests != null && personDto.Interests.Any())
            {
                personEntity.Interests = await ConvertPerson2DtoInterestsToEntityAsync(personDto.Interests);
            }

            return personEntity;
        }

        private async Task<PersonIntegration> ConvertPersonNames(string personId, string guid, IEnumerable<Dtos.DtoProperties.PersonNameDtoProperty> PersonNames)
        {
            PersonIntegration personEntity = null;

            if (PersonNames == null)
                throw new ArgumentNullException("personDto", "Must provide person name");

            var nameCategories =
              PersonNames.Where(pn => pn.NameType == null || pn.NameType.Category == null).ToList();

            if (nameCategories.Any())
            {
                throw new ArgumentNullException("personDto", "Name type category is required.");
            }

            var preferredNames =
                PersonNames.Where(pn => pn.Preference == Dtos.EnumProperties.PersonNamePreference.Preferred)
                    .ToList();

            if (preferredNames.Any() && preferredNames.Count > 1)
            {
                throw new ArgumentNullException("personDto", "Only one name type can be identified as preferred.");
            }
            var nameTypes = (await _referenceDataRepository.GetPersonNameTypesAsync(false)).ToList();

            // person legal name
            var primaryNames =
                PersonNames.Where(pn => pn.NameType.Category == PersonNameType2.Legal).ToList();
            if (!primaryNames.Any())
            {
                throw new ArgumentNullException("personDto", "Must provide person primary name.");
            }
            if (primaryNames.Count() > 1)
            {
                throw new ArgumentException("personDto",
                    "Colleague does not support more than one legal name for a person.");
            }
            var primaryName = primaryNames.FirstOrDefault();

            if (string.IsNullOrWhiteSpace(primaryName.LastName))
            {
                throw new ArgumentNullException("personDto", "Last name is required for a legal name.");
            }
            if (string.IsNullOrEmpty(primaryName.FullName))
            {
                throw new ArgumentNullException("personDto", "Full name is required for a legal name.");
            }
            if ((primaryName.NameType.Detail != null) && (primaryName.NameType.Detail.Id == null))
            {
                throw new ArgumentException("personDto", "If providing Legal NameType Detail, then Detail.id is required.");
            }
            if ((primaryName.NameType.Detail != null) && (primaryName.NameType.Detail.Id != null))
            {
                var legalNameType = nameTypes.FirstOrDefault(x => x.Code == "LEGAL");
                if (legalNameType == null)
                {
                    throw new RepositoryException("Unable to retrieve PersonNameType used to detemine legal name.");
                }
                if (!primaryName.NameType.Detail.Id.Equals(legalNameType.Guid, StringComparison.InvariantCulture))
                {
                    throw new ArgumentException("personDto", "Detail.id provided for category legal not valid.");
                }
            }

            personEntity = new PersonIntegration(personId, primaryName.LastName)
            {
                Guid = guid,
                Prefix = await ValidatePrefixAsync(primaryName.Title),
                FirstName = primaryName.FirstName,
                MiddleName = primaryName.MiddleName,
                Suffix = await ValdiateSuffixAsync(primaryName.Pedigree),
            };
            if (primaryName.Preference == PersonNamePreference.Preferred)
            {
                personEntity.PreferredName = primaryName.FullName;
                personEntity.PreferredNameType = "LEGAL";
            }


            // person birth name
            var birthNames =
                PersonNames.Where(pn => pn.NameType.Category == PersonNameType2.Birth).ToList();

            if (birthNames.Any())
            {
                if (birthNames.Count() > 1)
                {
                    throw new ArgumentNullException("personDto",
                        "Colleague does not support more than one birth name for a person.");
                }
                var birthName = birthNames.FirstOrDefault();

                if (birthName != null)
                {
                    if ((string.IsNullOrEmpty(birthName.FirstName)) &&
                        (string.IsNullOrEmpty(birthName.LastName)) &&
                        (string.IsNullOrEmpty(birthName.MiddleName)))
                    {
                        throw new ArgumentNullException("personDto",
                            "Either the firstName, middleName, or lastName is needed for a birth name.");
                    }
                    if (string.IsNullOrEmpty(birthName.FullName))
                    {
                        throw new ArgumentNullException("personDto", "Full Name is needed for a birth name.");
                    }
                    if ((birthName.NameType.Detail != null) && (birthName.NameType.Detail.Id == null))
                    {
                        throw new ArgumentException("personDto", "If providing Birth NameType Detail, then Detail.id is required.");
                    }
                    if ((birthName.NameType.Detail != null) && (birthName.NameType.Detail.Id != null))
                    {
                        var birthNameType = nameTypes.FirstOrDefault(x => x.Code == "BIRTH");
                        if (birthNameType == null)
                        {
                            throw new RepositoryException(
                                "Unable to retrieve PersonNameType used to detemine birth name.");
                        }
                        if (!birthName.NameType.Detail.Id.Equals(birthNameType.Guid, StringComparison.InvariantCulture))
                        {
                            throw new ArgumentException("personDto", "Detail.id provided for category birth not valid.");
                        }
                    }

                    personEntity.BirthNameLast = birthName.LastName;
                    personEntity.BirthNameFirst = birthName.FirstName;
                    personEntity.BirthNameMiddle = birthName.MiddleName;

                    if (birthName.Preference == PersonNamePreference.Preferred)
                    {
                        personEntity.PreferredName = birthName.FullName;
                        personEntity.PreferredNameType = "BIRTH";
                    }
                }
            }

            var personalNames =
                PersonNames.Where(pn => pn.NameType.Category == PersonNameType2.Personal).ToList();

            if (personalNames.Any())
            {
                // Person Nick Name
                // Nickname are a sub-group of the personal name type.  Determined by matching detail.id      
                var nickNameType = nameTypes.FirstOrDefault(x => x.Code == "NICKNAME");
                if (nickNameType == null)
                {
                    throw new RepositoryException("Unable to retrieve PersonNameType used to detemine nickname.");
                }

                var nickNames =
                        personalNames.Where(pn => pn.NameType.Detail.Id == nickNameType.Guid).ToList();
                if (nickNames.Any())
                {
                    if (nickNames.Count() > 1)
                    {
                        throw new ArgumentNullException("personDto",
                            "Colleague does not support more than one nickname for a person.");
                    }
                    var nickName = nickNames.FirstOrDefault();
                    if (nickName != null)
                    {
                        if (string.IsNullOrEmpty(nickName.FullName))
                        {
                            throw new ArgumentNullException("personDto", "Full Name is required for a nickname.");
                        }

                        // On PUT and POST, if NickName is identified as Preferred, than the First Name property of NickName will be stored into 
                        // Colleague’s Nickname field unless the First Name property of NickName is blank.  If that is blank, the fullName
                        // property of NickName will be stored in Colleague’s NickName 
                        if (nickName.Preference == PersonNamePreference.Preferred)
                        {
                            personEntity.PreferredName = nickName.FullName;
                            personEntity.Nickname = string.IsNullOrEmpty(nickName.FirstName)
                                ? nickName.FullName
                                : nickName.FirstName;
                            personEntity.PreferredNameType = "NICKNAME";
                        }
                        else
                        {
                            personEntity.Nickname = nickName.FullName;
                        }
                    }
                }

                // person chosen name
                // Chosen names are a sub-group of the personal name type.  Determined by matching detail.id      
                var chosenNameType = nameTypes.FirstOrDefault(x => x.Code == "CHOSEN");
                if (chosenNameType == null)
                {
                    throw new RepositoryException("Unable to retrieve PersonNameType used to detemine chosen name.");
                }

                var chosenNames =
                        personalNames.Where(pn => pn.NameType.Detail.Id == chosenNameType.Guid).ToList();

                if (chosenNames.Any())
                {
                    if (chosenNames.Count() > 1)
                    {
                        throw new ArgumentNullException("personDto",
                            "Colleague does not support more than one chosen name for a person.");
                    }
                    var chosenName = chosenNames.FirstOrDefault();

                    if (chosenName != null)
                    {
                        if ((string.IsNullOrEmpty(chosenName.FirstName)) &&
                            (string.IsNullOrEmpty(chosenName.LastName)) &&
                            (string.IsNullOrEmpty(chosenName.MiddleName)))
                        {
                            throw new ArgumentNullException("personDto",
                                "Either the firstName, middleName, or lastName is needed for a chosen name.");
                        }
                        if (string.IsNullOrEmpty(chosenName.FullName))
                        {
                            throw new ArgumentNullException("personDto", "Full Name is needed for a chosen name.");
                        }
                        if ((chosenName.NameType.Detail != null) && (chosenName.NameType.Detail.Id == null))
                        {
                            throw new ArgumentException("personDto", "If providing Chosen NameType Detail, then Detail.id is required.");
                        }
                        if ((chosenName.NameType.Detail != null) && (chosenName.NameType.Detail.Id != null))
                        {
                            if (chosenNameType == null)
                            {
                                throw new RepositoryException(
                                    "Unable to retrieve PersonNameType used to detemine chosen name.");
                            }
                            if (!chosenName.NameType.Detail.Id.Equals(chosenNameType.Guid, StringComparison.InvariantCulture))
                            {
                                throw new ArgumentException("personDto", "Detail.id provided for category personal not valid.");
                            }
                        }

                        personEntity.ChosenLastName = chosenName.LastName;
                        personEntity.ChosenFirstName = chosenName.FirstName;
                        personEntity.ChosenMiddleName = chosenName.MiddleName;

                        if (chosenName.Preference == PersonNamePreference.Preferred)
                        {
                            personEntity.PreferredName = chosenName.FullName;
                            personEntity.PreferredNameType = "CHOSEN";
                        }
                    }
                }

                // all other remaining personal names are recorded as historical names.
                var historyNameType = nameTypes.FirstOrDefault(x => x.Code == "HISTORY");
                if (historyNameType == null)
                {
                    throw new RepositoryException("Unable to retrieve PersonNameType used to detemine historyname.");
                }

                // From the subset of personal names, retrieve history/former names
                // For a PUT or POST where the names.type.category is "personal" with no corresponding detail guid, 
                // we will save the incoming name to the NAME.HISTORY.FIRST.NAME, NAME.HISTORY.LAST.NAME, and 
                // NAME.HISTORY.MIDDLE.NAME. 

                var historyNames =
                        personalNames.Where(
                            pn =>
                                (pn.NameType.Detail == null || pn.NameType.Detail.Id == null) ||
                                pn.NameType.Detail.Id == historyNameType.Guid).ToList();

                var formerNames = new List<PersonName>();
                foreach (var historyName in historyNames)
                {
                    if ((historyName.NameType.Detail != null) && (historyName.NameType.Detail.Id == null))
                    {
                        throw new ArgumentException("personDto", "If providing History NameType Detail, then Detail.id is required.");
                    }
                    if (string.IsNullOrEmpty(historyName.LastName))
                    {
                        throw new ArgumentNullException("personDto",
                                                "Last Name is required for a former name.");
                    }
                    if (historyName.Preference == Dtos.EnumProperties.PersonNamePreference.Preferred)
                    {
                        throw new ArgumentException("personDto",
                            "Can not assign Preferred to a name type of Former Name.");
                    }
                    if (string.IsNullOrEmpty(historyName.FullName))
                    {
                        throw new ArgumentNullException("personDto", "Full Name is required for a former name.");
                    }

                    var formerName = new PersonName(historyName.FirstName, historyName.MiddleName,
                        historyName.LastName);
                    formerNames.Add(formerName);

                }
                personEntity.FormerNames = formerNames;
            }
            return personEntity;
        }

        /// <summary>
        /// Converts privacy status
        /// </summary>
        /// <param name="personPrivacyDtoProperty"></param>
        /// <returns>PrivacyStatusType</returns>
        private async Task<Domain.Base.Entities.PrivacyStatus> ConvertPerson2PrivacyStatusAsync(Dtos.DtoProperties.PersonPrivacyDtoProperty personPrivacyDtoProperty)
        {
            if (personPrivacyDtoProperty.Detail != null && string.IsNullOrEmpty(personPrivacyDtoProperty.Detail.Id))
            {
                throw new ArgumentNullException("privacyStatus.detail", "Must provide an id for privacyStatus detail.");
            }

            try
            {
                var isDefined = Dtos.PrivacyStatusType.IsDefined(typeof(Dtos.PrivacyStatusType), personPrivacyDtoProperty.PrivacyCategory);
            }
            catch
            {
                throw new ArgumentNullException("privacyStatus.privacyCategory", "Must provide privacyCategory for privacyStatus.");
            }

            Domain.Base.Entities.PrivacyStatus privacyStatusEntity = null;
            if (personPrivacyDtoProperty.Detail != null && !string.IsNullOrEmpty(personPrivacyDtoProperty.Detail.Id))
            {
                var privacyStatusEntities = await _referenceDataRepository.GetPrivacyStatusesAsync(false);

                privacyStatusEntity = privacyStatusEntities.FirstOrDefault(st => st.Guid.Equals(personPrivacyDtoProperty.Detail.Id, StringComparison.OrdinalIgnoreCase));
                if (privacyStatusEntity == null)
                {
                    throw new KeyNotFoundException("Privacy status associated to guid '" + personPrivacyDtoProperty.Detail.Id + "' not found in repository.");
                }
                Domain.Base.Entities.PrivacyStatusType privacyStatus;
                try
                {
                    privacyStatus = (Domain.Base.Entities.PrivacyStatusType)Enum.Parse(
                        typeof(Domain.Base.Entities.PrivacyStatusType), personPrivacyDtoProperty.PrivacyCategory.ToString().ToLower());
                }
                catch (Exception)
                {
                    throw new InvalidOperationException("Error occured parsing privacy status type: " + personPrivacyDtoProperty.PrivacyCategory.ToString());
                }
                if (!privacyStatusEntity.PrivacyStatusType.Equals(privacyStatus))
                {
                    throw new InvalidOperationException(string.Concat("Provided privacy status type ", personPrivacyDtoProperty.PrivacyCategory.ToString(),
                        " does not match the privacy status type by id ", personPrivacyDtoProperty.Detail.Id));
                }
            }
            else if (personPrivacyDtoProperty.Detail == null)
            {
                var privacyStatusEntities = await _referenceDataRepository.GetPrivacyStatusesAsync(false);

                try
                {
                    if (personPrivacyDtoProperty.PrivacyCategory != Dtos.PrivacyStatusType.Unrestricted)
                    {
                        Domain.Base.Entities.PrivacyStatusType privacyStatus = (Domain.Base.Entities.PrivacyStatusType)Enum.Parse(typeof(Domain.Base.Entities.PrivacyStatusType), personPrivacyDtoProperty.PrivacyCategory.ToString().ToLower());

                        privacyStatusEntity = privacyStatusEntities.FirstOrDefault(st => st.PrivacyStatusType.Equals(privacyStatus));
                        if (privacyStatusEntity == null)
                        {
                            throw new KeyNotFoundException("Privacy status associated with enum '" + privacyStatus.ToString() + "' not found in repository.");
                        }
                    }
                }
                catch (ArgumentException)
                {
                    throw new ArgumentException("Privacy category not found with category type: " + personPrivacyDtoProperty.PrivacyCategory.ToString());
                }
            }
            return privacyStatusEntity;
        }

        /// <summary>
        /// Convert Race code
        /// </summary>
        /// <param name="racesDtoProperties"></param>
        /// <returns>List<string></returns>
        private async Task<List<string>> ConvertPerson2DtoRaceCodesToEntityAsync(IEnumerable<Dtos.DtoProperties.PersonRaceDtoProperty> racesDtoProperties)
        {
            List<string> raceCodes = null;
            var raceEntities = await _referenceDataRepository.RacesAsync();

            foreach (var raceDtoProperty in racesDtoProperties)
            {
                Domain.Base.Entities.Race raceEntity = null;

                //Validate race id is provided
                if (raceDtoProperty.Race != null && string.IsNullOrEmpty(raceDtoProperty.Race.Id))
                {
                    throw new ArgumentNullException("races.race.id", "Must provide an id for race detail.");
                }

                //Validate that reporting race country racial category, per discussion with Kelly only one should be in payload
                if (raceDtoProperty.Reporting != null && raceDtoProperty.Reporting.Count() > 1)
                {
                    throw new InvalidOperationException("More than one entry in the reporting array for a single race is not supported.");
                }

                //Validate if reporting country is not null then racial category is required
                if (raceDtoProperty.Reporting != null && raceDtoProperty.Reporting.Any())
                {
                    var reporting = raceDtoProperty.Reporting.FirstOrDefault();
                    // Only support racial category reporting country of USA
                    if (reporting.Country != null && reporting.Country.Code != CountryCodeType.USA)
                    {
                        throw new ArgumentOutOfRangeException("Only USA is supported for race reporting in Colleague.");
                    }
                    if (reporting.Country != null && reporting.Country.Code == Dtos.CountryCodeType.USA && reporting.Country.RacialCategory == null)
                    {
                        throw new ArgumentNullException("If the country.code is 'USA' then the races.reporting.country.racialCategory property is required.");
                    }
                }

                //Validate that entity RacialCategoryType by id is same as RacialCategory
                if (raceDtoProperty.Race != null &&
                    !string.IsNullOrEmpty(raceDtoProperty.Race.Id) &&
                    raceDtoProperty.Reporting != null &&
                    raceDtoProperty.Reporting.Any())
                {
                    var reporting = raceDtoProperty.Reporting.FirstOrDefault();
                    if (reporting.Country != null && reporting.Country.RacialCategory != null)
                    {
                        raceEntity = raceEntities.FirstOrDefault(r => r.Guid.Equals(raceDtoProperty.Race.Id, StringComparison.OrdinalIgnoreCase));
                        if (raceEntity == null)
                        {
                            throw new KeyNotFoundException("Race ID associated with guid '" + raceDtoProperty.Race.Id + "' was not found.");
                        }

                        try
                        {
                            RaceType? entityRaceType = ConvertDtoRaceCategoryToEntityType(reporting.Country.RacialCategory);
                            if (!raceEntity.Type.Equals(entityRaceType))
                            {
                                throw new InvalidOperationException(string.Concat("Provided racial category of ", raceEntity.Type.ToString(),
                                    " does not match the race specified by id ", raceDtoProperty.Race.Id));
                            }
                        }
                        catch (ArgumentException)
                        {
                            throw new ArgumentException("Racial category of " + reporting.Country.RacialCategory.Value.ToString() + " is not valid.");
                        }
                    }
                }


                if (raceDtoProperty.Race != null && !string.IsNullOrEmpty(raceDtoProperty.Race.Id))
                {
                    raceEntity = raceEntities.FirstOrDefault(r => r.Guid.Equals(raceDtoProperty.Race.Id, StringComparison.OrdinalIgnoreCase));
                    if (raceEntity == null)
                    {
                        throw new KeyNotFoundException("Race ID associated with guid '" + raceDtoProperty.Race.Id + "' was not found.");
                    }

                    if (raceCodes == null) raceCodes = new List<string>();
                    raceCodes.Add(raceEntity.Code);
                }
                else if (raceDtoProperty.Race == null && raceDtoProperty.Reporting != null && raceDtoProperty.Reporting.Any())
                {
                    /*
                      For a POST or PUT, if no races.race.id is specified in the payload, then use the enumeration here to find the corresponding code from the PERSON.RACES valcode table 
                      (retrieve the first one w/ the 1st special processing code corresponding to the HEDM enumeration) and store the resulting code or codes to PER.RACES.  
                      Be careful to maintain the same order of races as what was submitted in the payload. 
                    */
                    var reporting = raceDtoProperty.Reporting.FirstOrDefault();
                    if (reporting != null && reporting.Country != null && reporting.Country.RacialCategory != null)
                    {
                        if (raceCodes == null) raceCodes = new List<string>();

                        if (reporting.Country.RacialCategory != null)
                        {
                            try
                            {
                                Domain.Base.Entities.RaceType? entityRaceType = ConvertDtoRaceCategoryToEntityType(reporting.Country.RacialCategory);
                                if (entityRaceType == null)
                                {
                                    throw new ArgumentException("Racial category of " + reporting.Country.RacialCategory.Value.ToString() + " is not valid.");
                                }
                                raceEntity = raceEntities.FirstOrDefault(r => r.Type.Equals(entityRaceType));
                                raceCodes.Add(raceEntity.Code);
                            }
                            catch (ArgumentException)
                            {
                                throw new ArgumentException("Could not find race type with reporting.country.racialCategory : " + reporting.Country.RacialCategory.Value.ToString());
                            }
                        }
                    }
                }
            }
            return raceCodes;
        }

        private RaceType? ConvertDtoRaceCategoryToEntityType(Dtos.EnumProperties.PersonRaceCategory? personRaceCategory)
        {
            switch (personRaceCategory)
            {
                case Dtos.EnumProperties.PersonRaceCategory.AmericanIndianOrAlaskaNative:
                    return RaceType.AmericanIndian;
                case Dtos.EnumProperties.PersonRaceCategory.Asian:
                    return RaceType.Asian;
                case Dtos.EnumProperties.PersonRaceCategory.BlackOrAfricanAmerican:
                    return RaceType.Black;
                case Dtos.EnumProperties.PersonRaceCategory.HawaiianOrPacificIslander:
                    return RaceType.PacificIslander;
                case Dtos.EnumProperties.PersonRaceCategory.White:
                    return RaceType.White;
                default:
                    return null;
            }
        }

        /// <summary>
        /// Converts language
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="personGuid"></param>
        /// <param name="languagesDtoProperty"></param>
        /// <returns>List<PersonLanguage></returns>
        private List<PersonLanguage> ConvertPerson2DtoLanguagesToEntity(string personId, string personGuid, IEnumerable<Dtos.DtoProperties.PersonLanguageDtoProperty> languagesDtoProperty)
        {
            List<PersonLanguage> languagesEntities = new List<PersonLanguage>();

            if (languagesDtoProperty.Count(i => i.Preference == Dtos.EnumProperties.PersonLanguagePreference.Primary) > 1)
            {
                throw new InvalidOperationException("You can only submit one primary language.");
            }

            foreach (var languageDtoProperty in languagesDtoProperty)
            {
                if (string.IsNullOrEmpty(languageDtoProperty.Code))
                {
                    throw new ArgumentNullException("languageDtoProperty.code", "Must provide code for language.");
                }

                PersonLanguage personLanguageEntity = new PersonLanguage(string.IsNullOrEmpty(personId) ? "$NEW.PERSON" : personId, languageDtoProperty.Code);
                personLanguageEntity.PersonGuid = personGuid;
                if (languageDtoProperty.Preference != null)
                {
                    personLanguageEntity.Preference = (LanguagePreference)Enum.Parse(typeof(LanguagePreference), languageDtoProperty.Preference.ToString());
                }
                languagesEntities.Add(personLanguageEntity);
            }
            return languagesEntities;
        }

        /// <summary>
        /// Converts Marital Status
        /// </summary>
        /// <param name="personMaritalStatusDtoProperty"></param>
        /// <returns>Domain.Base.Entities.MaritalState?</returns>
        private async Task<Domain.Base.Entities.MaritalState?> ConvertPerson2MaritalStatusDtoToEntityAsync(Dtos.DtoProperties.PersonMaritalStatusDtoProperty personMaritalStatusDtoProperty)
        {
            try
            {
                var isDefined = Enum.IsDefined(typeof(Dtos.EnumProperties.PersonMaritalStatusCategory), personMaritalStatusDtoProperty.MaritalCategory);
            }
            catch
            {
                throw new ArgumentNullException("maritalStatus.maritalCategory", "Must provide a valid category for marital status.");
            }

            if (personMaritalStatusDtoProperty.Detail != null && string.IsNullOrEmpty(personMaritalStatusDtoProperty.Detail.Id))
            {
                throw new ArgumentNullException("maritalStatus.detail.id", "Must provide an id for marital status detail.");
            }

            Domain.Base.Entities.MaritalState? maritalState = null;
            var maritalStatusEntities = await _referenceDataRepository.MaritalStatusesAsync();

            if (personMaritalStatusDtoProperty.Detail != null && !string.IsNullOrEmpty(personMaritalStatusDtoProperty.Detail.Id))
            {
                var maritalStatusEntityById = maritalStatusEntities.FirstOrDefault(
                            m => m.Guid == personMaritalStatusDtoProperty.Detail.Id);
                if (maritalStatusEntityById == null)
                {
                    throw new KeyNotFoundException("Could not find marital status with id: " + personMaritalStatusDtoProperty.Detail.Id);
                }

                try
                {
                    Domain.Base.Entities.MaritalStatusType maritalStatusCategory = (Domain.Base.Entities.MaritalStatusType)Enum.Parse(typeof(Domain.Base.Entities.MaritalStatusType),
                                                                                personMaritalStatusDtoProperty.MaritalCategory.ToString());

                    if (!maritalStatusEntityById.Type.Equals(maritalStatusCategory))
                    {
                        throw new InvalidOperationException("maritalStatus.maritalCategory does not match the entity marital category by id: " + personMaritalStatusDtoProperty.Detail.Id);
                    }

                    maritalState = (Domain.Base.Entities.MaritalState?)maritalStatusEntityById.Type;
                    return maritalState;
                }
                catch (ArgumentException)
                {
                    throw new ArgumentException("Could not find marital status type with marital category: " + personMaritalStatusDtoProperty.MaritalCategory.ToString());
                }
            }
            else if (personMaritalStatusDtoProperty.Detail == null)
            {
                try
                {
                    Domain.Base.Entities.MaritalStatusType maritalStatusCategory = (Domain.Base.Entities.MaritalStatusType)Enum.Parse(typeof(Domain.Base.Entities.MaritalStatusType),
                                                                personMaritalStatusDtoProperty.MaritalCategory.ToString());

                    var maritalStatusEntity = maritalStatusEntities.FirstOrDefault(m => m.Type.Equals(maritalStatusCategory));
                    if (maritalStatusEntity == null)
                    {
                        throw new KeyNotFoundException("Could not find marital status with marital category: " + personMaritalStatusDtoProperty.MaritalCategory.ToString());
                    }
                    maritalState = (Domain.Base.Entities.MaritalState)Enum.Parse(typeof(Domain.Base.Entities.MaritalState), maritalStatusEntity.Type.ToString());
                }
                catch (ArgumentException)
                {
                    throw new ArgumentException("Could not find marital status type with marital category: " + personMaritalStatusDtoProperty.MaritalCategory.ToString());
                }
            }
            return maritalState;
        }

        /// <summary>
        /// Converts citizenship status
        /// </summary>
        /// <param name="personCitizenshipDtoProperty"></param>
        /// <returns>string</returns>
        private async Task<string> ConvertPerson2CitizenshipStatusDtoToEntityAsync(Dtos.DtoProperties.PersonCitizenshipDtoProperty personCitizenshipDtoProperty)
        {
            string citizenshipStatus = string.Empty;
            try
            {
                var isDefined = Enum.IsDefined(typeof(Dtos.CitizenshipStatusType), personCitizenshipDtoProperty.Category);
            }
            catch
            {
                throw new ArgumentNullException("citizenshipStatus.category", "Must provide a valid category for citizenshipStatus");
            }

            //check the required id field
            if (personCitizenshipDtoProperty != null && personCitizenshipDtoProperty.Detail != null && string.IsNullOrEmpty(personCitizenshipDtoProperty.Detail.Id))
            {
                throw new ArgumentNullException("citizenshipStatus.detail.id", "Citizenship detail id is required.");
            }

            var citizenshipStatusesEntities = await _referenceDataRepository.GetCitizenshipStatusesAsync(false);
            Domain.Base.Entities.CitizenshipStatus statusEntityByid = null;

            //validate that provided category matched the one in the entitybyid
            if (personCitizenshipDtoProperty != null &&
                personCitizenshipDtoProperty.Detail != null &&
                !string.IsNullOrEmpty(personCitizenshipDtoProperty.Detail.Id))
            {
                try
                {
                    var statusEntityByCategory = (CitizenshipStatusType)Enum.Parse(typeof(CitizenshipStatusType), personCitizenshipDtoProperty.Category.ToString());
                    statusEntityByid = citizenshipStatusesEntities.FirstOrDefault(i => i.Guid.Equals(personCitizenshipDtoProperty.Detail.Id, StringComparison.OrdinalIgnoreCase));

                    if (statusEntityByid == null)
                    {
                        throw new KeyNotFoundException("Citizenship status ID associated with guid '" + personCitizenshipDtoProperty.Detail.Id + "' not found in repository.");
                    }

                    if (statusEntityByid != null & !statusEntityByCategory.Equals(statusEntityByid.CitizenshipStatusType))
                    {
                        throw new InvalidOperationException("Citizenship category does not match with the category type by id.");
                    }
                }
                catch (ArgumentException)
                {
                    throw new ArgumentException("Citizenship status type entity not found with category: " + personCitizenshipDtoProperty.Category.ToString());
                }
            }

            if (personCitizenshipDtoProperty != null && personCitizenshipDtoProperty.Detail != null && !string.IsNullOrEmpty(personCitizenshipDtoProperty.Detail.Id))
            {
                if (statusEntityByid == null)
                {
                    statusEntityByid = citizenshipStatusesEntities
                        .FirstOrDefault(i => i.Guid.Equals(personCitizenshipDtoProperty.Detail.Id, StringComparison.OrdinalIgnoreCase));
                }

                if (statusEntityByid == null)
                {
                    throw new KeyNotFoundException("Citizenship status ID associated with guid '" + personCitizenshipDtoProperty.Detail.Id + "' not found in repository.");
                }

                if (statusEntityByid != null)
                {
                    citizenshipStatus = statusEntityByid.Code;
                }
            }
            else if (personCitizenshipDtoProperty != null)
            {
                try
                {
                    var statusCategory = (CitizenshipStatusType)Enum.Parse(typeof(CitizenshipStatusType), personCitizenshipDtoProperty.Category.ToString());
                    var statusEntityByCategory = citizenshipStatusesEntities
                        .FirstOrDefault(i => i.CitizenshipStatusType.Equals(statusCategory));
                    if (statusEntityByCategory != null)
                    {
                        citizenshipStatus = statusEntityByCategory.Code;
                    }
                }
                catch (ArgumentException)
                {
                    throw new ArgumentException("Citizenship status type entity not found with category: " + personCitizenshipDtoProperty.Category.ToString());
                }
            }

            return citizenshipStatus;
        }

        /// <summary>
        /// Converts visa
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="personVisaDtoProperty"></param>
        /// <returns>PersonVisa</returns>
        private async Task<PersonVisa> ConvertPerson2VisaToEntityAsync(string personId, Dtos.DtoProperties.PersonVisaDtoProperty personVisaDtoProperty)
        {
            PersonVisa personVisa = null;
            try
            {
                var isDefined = Enum.IsDefined(typeof(Dtos.VisaTypeCategory), personVisaDtoProperty.Category);
            }
            catch
            {
                throw new InvalidOperationException("Visa type category is required.");
            }

            if (personVisaDtoProperty.StartOn != null && personVisaDtoProperty.EndOn != null & personVisaDtoProperty.StartOn > personVisaDtoProperty.EndOn)
            {
                throw new InvalidOperationException("Start on date cannot be after the end on date.");
            }

            if (personVisaDtoProperty.Detail != null && string.IsNullOrEmpty(personVisaDtoProperty.Detail.Id))
            {
                throw new ArgumentNullException("visaStatus.detail.id", "Must provide id for visa status detail.");
            }
            if (personVisaDtoProperty.Detail != null && !string.IsNullOrEmpty(personVisaDtoProperty.Detail.Id))
            {
                var personVisaTypes = await _referenceDataRepository.GetVisaTypesAsync(false);
                var personVisaType = personVisaTypes.FirstOrDefault(vt => vt.Guid.Equals(personVisaDtoProperty.Detail.Id, StringComparison.OrdinalIgnoreCase));

                if (personVisaType == null)
                {
                    throw new KeyNotFoundException("Person visa type ID associated with guid '" + personVisaDtoProperty.Detail.Id + "' not found in repository.");
                }

                try
                {
                    VisaTypeCategory visaTypeCategory =
                        (VisaTypeCategory)
                            Enum.Parse(typeof(VisaTypeCategory), personVisaDtoProperty.Category.ToString());

                    if (!personVisaType.VisaTypeCategory.Equals(visaTypeCategory))
                    {
                        throw new InvalidOperationException(
                            "Visa type category does not match with the category type by id.");
                    }

                    personVisa = new PersonVisa(string.IsNullOrEmpty(personId) ? "$NEW.PERSON" : personId,
                        personVisaDtoProperty.Category.ToString());
                    personVisa.ExpireDate = (personVisaDtoProperty.EndOn == null)
                        ? default(DateTime?)
                        : personVisaDtoProperty.EndOn.Value.DateTime;
                    personVisa.Guid = personVisaDtoProperty.Detail.Id;
                    personVisa.IssueDate = (personVisaDtoProperty.StartOn == null)
                        ? default(DateTime?)
                        : personVisaDtoProperty.StartOn.Value.DateTime;
                }
                catch (ArgumentException)
                {
                    throw new ArgumentException("Person visa type entity not found with category: " +
                                                personVisaDtoProperty.Category.ToString());
                }
            }
            else
            {
                try
                {
                    VisaTypeCategory visaTypeCategory = (VisaTypeCategory)Enum.Parse(typeof(VisaTypeCategory), personVisaDtoProperty.Category.ToString());

                    var personVisaTypes = await _referenceDataRepository.GetVisaTypesAsync(false);
                    var personVisaType = personVisaTypes.FirstOrDefault(vt => vt.VisaTypeCategory.Equals(visaTypeCategory));

                    if (personVisaType == null)
                    {
                        throw new KeyNotFoundException("Person visa type entity associated with category '" + personVisaDtoProperty.Category.ToString() + "' not found in repository.");
                    }

                    personVisa = new PersonVisa(personId, personVisaDtoProperty.Category.ToString());
                    personVisa.ExpireDate = (personVisaDtoProperty.EndOn == null) ? default(DateTime?) : personVisaDtoProperty.EndOn.Value.DateTime;
                    personVisa.Guid = personVisaType.Guid;
                    personVisa.IssueDate = (personVisaDtoProperty.StartOn == null) ? default(DateTime?) : personVisaDtoProperty.StartOn.Value.DateTime;
                }
                catch (ArgumentException)
                {
                    throw new ArgumentException("Person visa type entity not found with category: " + personVisaDtoProperty.Category.ToString());
                }
            }
            return personVisa;
        }

        /// <summary>
        /// Converts roles
        /// </summary>
        /// <param name="personDtoRoles"></param>
        /// <returns>List<PersonRole></returns>
        private List<PersonRole> ConvertPerson2DtoRolesToEntity(IEnumerable<Dtos.DtoProperties.PersonRoleDtoProperty> personDtoRoles)
        {
            List<PersonRole> personRolesEntities = new List<PersonRole>();

            if (personDtoRoles != null && personDtoRoles.Any())
            {
                foreach (var roleDto in personDtoRoles)
                {
                    try
                    {
                        if (roleDto.StartOn != null && roleDto.EndOn != null && roleDto.StartOn > roleDto.EndOn)
                        {
                            throw new InvalidOperationException("Start on date cannot be after the end on date.");
                        }

                        PersonRoleType roleType = (PersonRoleType)Enum.Parse(typeof(PersonRoleType), roleDto.RoleType.ToString());
                        PersonRole role =
                            new PersonRole(
                                            roleType,
                                            (roleDto.StartOn == null) ? default(DateTime?) : roleDto.StartOn.Value.DateTime,
                                            (roleDto.EndOn == null) ? default(DateTime?) : roleDto.EndOn.Value.DateTime
                                          );
                        personRolesEntities.Add(role);
                    }
                    catch (ArgumentException)
                    {
                        throw new ArgumentException("Role type entity not found with role type: " + roleDto.RoleType.ToString());
                    }
                }
            }

            return personRolesEntities;
        }

        /// <summary>
        /// Converts roles
        /// </summary>
        /// <param name="organizationDtoRoles"></param>
        /// <returns>List<PersonRole></returns>
        private List<PersonRole> ConvertOrganization2DtoRolesToEntity(IEnumerable<Dtos.DtoProperties.OrganizationRoleDtoProperty> organizationDtoRoles)
        {
            List<PersonRole> personRolesEntities = new List<PersonRole>();

            if (organizationDtoRoles != null && organizationDtoRoles.Any())
            {
                foreach (var roleDto in organizationDtoRoles)
                {
                    try
                    {
                        if (roleDto.StartOn != null && roleDto.EndOn != null && roleDto.StartOn > roleDto.EndOn)
                        {
                            throw new InvalidOperationException("Start on date cannot be after the end on date.");
                        }

                        PersonRoleType roleType = (PersonRoleType)Enum.Parse(typeof(PersonRoleType), roleDto.Type.ToString());
                        PersonRole role =
                            new PersonRole(
                                            roleType,
                                            (roleDto.StartOn == null) ? default(DateTime?) : roleDto.StartOn.Value.DateTime,
                                            (roleDto.EndOn == null) ? default(DateTime?) : roleDto.EndOn.Value.DateTime
                                          );
                        personRolesEntities.Add(role);
                    }
                    catch (ArgumentException)
                    {
                        throw new ArgumentException("Role type entity not found with role type: " + roleDto.Type.ToString());
                    }
                }
            }

            return personRolesEntities;
        }        

        /// <summary>
        /// Converts interests
        /// </summary>
        /// <param name="interests"></param>
        /// <returns>List<string></returns>
        private async Task<List<string>> ConvertPerson2DtoInterestsToEntityAsync(IEnumerable<Dtos.GuidObject2> interests)
        {
            List<string> interestList = null;
            if (interests.Any())
            {
                var interestEntities = await _referenceDataRepository.GetInterestsAsync(false);
                foreach (var interest in interests)
                {
                    if (string.IsNullOrEmpty(interest.Id))
                    {
                        throw new ArgumentNullException("interest.id", "Must provide an id for interest.");
                    }

                    var interestEntity = interestEntities.FirstOrDefault(i => i.Guid == interest.Id);
                    if (interestEntity == null)
                    {
                        throw new KeyNotFoundException("Interest entity not found with id: " + interest.Id);
                    }
                    if (interestList == null) interestList = new List<string>();

                    if (!interestList.Contains(interestEntity.Code))
                        interestList.Add(interestEntity.Code);
                }
            }
            return interestList;
        }

        /// <summary>
        /// Converts social media data
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="personDto"></param>
        /// <param name="personEntity"></param>
        private async Task<List<Domain.Base.Entities.SocialMedia>> ConvertPerson2DtoSocialMediaToEntity(IEnumerable<PersonSocialMediaDtoProperty> socialMediaDtoList )
        {
            var socialMediaEntities = new List<Domain.Base.Entities.SocialMedia>();
            if (socialMediaDtoList != null)
            {
                var socialMediaTypeEntities = await _referenceDataRepository.GetSocialMediaTypesAsync(false);
                foreach (var socialMediaDto in socialMediaDtoList)
                {
                    if (socialMediaDto.Type == null)
                        throw new ArgumentNullException("personDto", "Social media type is required to create a new social media");
                    if (string.IsNullOrEmpty(socialMediaDto.Address))
                        throw new ArgumentNullException("personDto", "Social media handle is required to create a new social media");

                    if (socialMediaDto.Type.Category != Dtos.SocialMediaTypeCategory.website)
                    {
                        string socialMediaType = "";
                        try
                        {
                            if (socialMediaDto.Type.Detail != null && !string.IsNullOrEmpty(socialMediaDto.Type.Detail.Id))
                            {
                                socialMediaType = socialMediaTypeEntities.FirstOrDefault(et => et.Guid == socialMediaDto.Type.Detail.Id).Code;
                            }
                            else
                            {
                                socialMediaType = socialMediaTypeEntities.FirstOrDefault(et => string.Equals(et.Type.ToString(), socialMediaDto.Type.Category.ToString(), StringComparison.OrdinalIgnoreCase)).Code;
                            }

                            bool socialMediaPreference = false;
                            if (socialMediaDto.Preference == Dtos.EnumProperties.PersonPreference.Primary) socialMediaPreference = true;
                            var socialMediaEntity = new Domain.Base.Entities.SocialMedia(socialMediaType, socialMediaDto.Address, socialMediaPreference);

                            socialMediaEntities.Add(socialMediaEntity);
                        }
                        catch
                        {
                            throw new ArgumentOutOfRangeException(string.Format("Could not find the social media type for handle '{0}'. ", socialMediaDto.Address), "socialMediaDto.Type");
                        }
                    }
                    else
                    {
                        var socialMediaEntity = new Domain.Base.Entities.SocialMedia("website", socialMediaDto.Address, false);
                        socialMediaEntities.Add(socialMediaEntity);
                    }
                }
            }
            return socialMediaEntities;          
        }

        /// <summary>
        /// Converts credentials
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="credentials"></param>
        /// <param name="personEntity"></param>
        private static void ConvertPerson2DtoCredsToEntity(string personId, IEnumerable<CredentialDtoProperty> credentials , PersonIntegration personEntity)
        {
            //Validate to make sure not more than one of each type is passed in the payload
            if (credentials != null && credentials.Count(c => c.Type == Dtos.EnumProperties.CredentialType.ColleaguePersonId) > 1)
            {
                throw new InvalidOperationException("You cannot include more than one Colleague id.");
            }

            if (credentials != null && credentials.Count(c => c.Type == Dtos.EnumProperties.CredentialType.Ssn) > 1)
            {
                throw new InvalidOperationException("You cannot include more than one SSN.");
            }

            if (credentials != null && credentials.Count(c => c.Type == Dtos.EnumProperties.CredentialType.Sin) > 1)
            {
                throw new InvalidOperationException("You cannot include more than one SIN.");
            }

            if (credentials != null && credentials.Count(c => c.Type == Dtos.EnumProperties.CredentialType.ElevateID) > 1)
            {
                throw new InvalidOperationException("You cannot include more than one elevate id.");
            }

            //Validate that only Ssn or Sin is included and not both
            Dtos.EnumProperties.CredentialType[] creds = new[] { Dtos.EnumProperties.CredentialType.Sin, Dtos.EnumProperties.CredentialType.Ssn };
            var ssnSin = credentials.Where(c => creds.Contains(c.Type));
            if (ssnSin != null && ssnSin.Count() > 1)
            {
                throw new InvalidOperationException("You cannot include SSN and SIN at the same time.");
            }

            //Validate the colleagueId type against personId
            var colleagueIdCred = credentials.FirstOrDefault(c => c.Type == Dtos.EnumProperties.CredentialType.ColleaguePersonId);
            if (colleagueIdCred != null)
            {
                if (!string.IsNullOrEmpty(personId) && !personId.Equals(colleagueIdCred.Value, StringComparison.OrdinalIgnoreCase))
                {
                    throw new InvalidOperationException("Provided Colleague credentials id does not match Colleague person id.");
                }
                else
                {
                    if (string.IsNullOrEmpty(personId) && !string.IsNullOrEmpty(colleagueIdCred.Value))
                    {
                        throw new InvalidOperationException("Assignment of Colleague credentials id is not allowed on a new person.");
                    }
                }
            }

            //Cannot include any other type of credentials such as bannerId, bannerSourceId, bannerUserName, bannerUdcId
            if (credentials != null && credentials.Any(crd => crd.Type.Equals(Dtos.EnumProperties.CredentialType.BannerId) ||
                                                                                 crd.Type.Equals(Dtos.EnumProperties.CredentialType.BannerSourcedId) ||
                                                                                 crd.Type.Equals(Dtos.EnumProperties.CredentialType.BannerUdcId) ||
                                                                                 crd.Type.Equals(Dtos.EnumProperties.CredentialType.BannerUserName)))
            {
                throw new InvalidOperationException("Credential type bannerId, bannerSourceId, bannerUserName, bannerUdcId are not supported in colleague.");
            }

            if (credentials != null)
            {
                //SSN
                var ssn = credentials.FirstOrDefault(c => c.Type == Dtos.EnumProperties.CredentialType.Ssn);
                if (ssn != null)
                {
                    if (string.IsNullOrEmpty(ssn.Value))
                    {
                        throw new ArgumentNullException("credentials.value", "Must provide a value for SSN if the credentials property is included.");
                    }
                    PersonAlt personAlt = new PersonAlt(ssn.Value, "SSN");
                    personEntity.PersonAltIds.Add(personAlt);
                }

                //Sin
                var sin = credentials.FirstOrDefault(c => c.Type == Dtos.EnumProperties.CredentialType.Sin);
                if (sin != null)
                {
                    if (string.IsNullOrEmpty(sin.Value))
                    {
                        throw new ArgumentNullException("credentials.value", "Must provide a value for SIN if the credentials property is included.");
                    }
                    PersonAlt personAlt = new PersonAlt(sin.Value, "SIN");
                    personEntity.PersonAltIds.Add(personAlt);
                }

                //Elevate Id
                var elevateId = credentials.FirstOrDefault(c => c.Type == Dtos.EnumProperties.CredentialType.ElevateID);
                if (elevateId != null)
                {
                    if (string.IsNullOrEmpty(elevateId.Value))
                    {
                        throw new ArgumentNullException("credentials.value", "Must provide a value for elevate id if the credentials property is included.");
                    }
                    PersonAlt personAlt = new PersonAlt(elevateId.Value, "ELEV");
                    personEntity.PersonAltIds.Add(personAlt);
                }
            }
        }

        /// <summary>
        /// Converts credentials
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="credentials"></param>
        /// <param name="personEntity"></param>
        private void ConvertPerson3DtoCredsToEntity(string personId, IEnumerable<CredentialDtoProperty2> credentials, PersonIntegration personEntity)
        {
            //Validate to make sure not more than one of each type is passed in the payload
            if (credentials != null && credentials.Count(c => c.Type == Dtos.EnumProperties.CredentialType2.ColleaguePersonId) > 1)
            {
                throw new InvalidOperationException("You cannot include more than one Colleague id.");
            }

            if (credentials != null && credentials.Count(c => c.Type == Dtos.EnumProperties.CredentialType2.Ssn) > 1)
            {
                throw new InvalidOperationException("You cannot include more than one SSN.");
            }

            if (credentials != null && credentials.Count(c => c.Type == Dtos.EnumProperties.CredentialType2.Sin) > 1)
            {
                throw new InvalidOperationException("You cannot include more than one SIN.");
            }

            if (credentials != null && credentials.Count(c => c.Type == Dtos.EnumProperties.CredentialType2.ElevateID) > 1)
            {
                throw new InvalidOperationException("You cannot include more than one elevate id.");
            }

            //Validate that only Ssn or Sin is included and not both
            Dtos.EnumProperties.CredentialType2[] creds = new[] { Dtos.EnumProperties.CredentialType2.Sin, Dtos.EnumProperties.CredentialType2.Ssn };
            var ssnSin = credentials.Where(c => creds.Contains(c.Type));
            if (ssnSin != null && ssnSin.Count() > 1)
            {
                throw new InvalidOperationException("You cannot include SSN and SIN at the same time.");
            }

            //Validate the colleagueId type against personId
            var colleagueIdCred = credentials.FirstOrDefault(c => c.Type == Dtos.EnumProperties.CredentialType2.ColleaguePersonId);
            if (colleagueIdCred != null)
            {
                if (!string.IsNullOrEmpty(personId) && !personId.Equals(colleagueIdCred.Value, StringComparison.OrdinalIgnoreCase))
                {
                    throw new InvalidOperationException("Provided Colleague credentials id does not match Colleague person id.");
                }
                else
                {
                    if (string.IsNullOrEmpty(personId) && !string.IsNullOrEmpty(colleagueIdCred.Value))
                    {
                        throw new InvalidOperationException("Assignment of Colleague credentials id is not allowed on a new person.");
                    }
                }
            }

            //Cannot include any other type of credentials such as bannerId, bannerSourceId, bannerUserName, bannerUdcId
            if (credentials != null && credentials.Any(crd => crd.Type.Equals(Dtos.EnumProperties.CredentialType2.BannerId) ||
                                                                                 crd.Type.Equals(Dtos.EnumProperties.CredentialType2.BannerSourcedId) ||
                                                                                 crd.Type.Equals(Dtos.EnumProperties.CredentialType2.BannerUdcId) ||
                                                                                 crd.Type.Equals(Dtos.EnumProperties.CredentialType2.BannerUserName)))
            {
                throw new InvalidOperationException("Credential type bannerId, bannerSourceId, bannerUserName, bannerUdcId are not supported in colleague.");
            }

            if (credentials != null)
            {
                //SSN
                var ssn = credentials.FirstOrDefault(c => c.Type == Dtos.EnumProperties.CredentialType2.Ssn);
                if (ssn != null)
                {
                    if (string.IsNullOrEmpty(ssn.Value))
                    {
                        throw new ArgumentNullException("credentials.value", "Must provide a value for SSN if the credentials property is included.");
                    }
                    PersonAlt personAlt = new PersonAlt(ssn.Value, "SSN");
                    personEntity.PersonAltIds.Add(personAlt);
                }

                //Sin
                var sin = credentials.FirstOrDefault(c => c.Type == Dtos.EnumProperties.CredentialType2.Sin);
                if (sin != null)
                {
                    if (string.IsNullOrEmpty(sin.Value))
                    {
                        throw new ArgumentNullException("credentials.value", "Must provide a value for SIN if the credentials property is included.");
                    }
                    PersonAlt personAlt = new PersonAlt(sin.Value, "SIN");
                    personEntity.PersonAltIds.Add(personAlt);
                }

                //Elevate Id
                var elevateId = credentials.FirstOrDefault(c => c.Type == Dtos.EnumProperties.CredentialType2.ElevateID);
                if (elevateId != null)
                {
                    if (string.IsNullOrEmpty(elevateId.Value))
                    {
                        throw new ArgumentNullException("credentials.value", "Must provide a value for elevate id if the credentials property is included.");
                    }
                    PersonAlt personAlt = new PersonAlt(elevateId.Value, "ELEV");
                    personEntity.PersonAltIds.Add(personAlt);
                }
            }
        }

        /// <summary>
        /// Converts gender type
        /// </summary>
        /// <param name="genderType2"></param>
        /// <returns></returns>
        private string ConvertGenderType2String(Dtos.EnumProperties.GenderType2? genderType2)
        {
            switch (genderType2)
            {
                case Dtos.EnumProperties.GenderType2.Male:
                    {
                        return "M";
                    }
                case Dtos.EnumProperties.GenderType2.Female:
                    {
                        return "F";
                    }
                default:
                    {
                        return string.Empty;
                    }
            }
        }

        /// <summary>
        /// Converts religion code
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        private async Task<string> ConvertPerson2DtoReligionCodeToEntityAsync(string guid)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw new ArgumentNullException("personDto.religion.id", "Must provide an id for religion.");
            }

            string religionCode = string.Empty;

            if (!string.IsNullOrEmpty(guid))
            {
                var religionEntity = (await _referenceDataRepository.DenominationsAsync()).FirstOrDefault(r => r.Guid == guid);
                if (religionEntity == null)
                {
                    throw new KeyNotFoundException("Religion ID associated to guid '" + guid + "' not found in repository");
                }

                if (religionEntity != null && !string.IsNullOrEmpty(religionEntity.Guid))
                {
                    religionCode = religionEntity.Code;
                }
            }
            return religionCode;
        }

        /// <summary>
        /// Converts ethnicity
        /// </summary>
        /// <param name="personEthnicityDtoProperty"></param>
        /// <returns></returns>
        private async Task<List<string>> ConvertPerson2DtoEthnicityCodesEntityAsync(Dtos.DtoProperties.PersonEthnicityDtoProperty personEthnicityDtoProperty)
        {
            List<string> ethnicityCodes = null;

            //Validate id is required
            if (personEthnicityDtoProperty.EthnicGroup != null && string.IsNullOrEmpty(personEthnicityDtoProperty.EthnicGroup.Id))
            {
                throw new ArgumentNullException("PersonEthnicityDtoProperty.ethnicGroup.id", "Must provide an id for ethnicity group");
            }

            //Validate reporting contains only one item - per discussions with Kelly
            if (personEthnicityDtoProperty.Reporting != null && personEthnicityDtoProperty.Reporting.Count() > 1)
            {
                throw new InvalidOperationException("More than one ethnicity reporting category is not supported.");
            }

            //Validate required items in reporting
            if (personEthnicityDtoProperty.Reporting != null)
            {
                var reportingItem = personEthnicityDtoProperty.Reporting.FirstOrDefault();
                // Only support racial category reporting country of USA
                if (reportingItem.Country != null && reportingItem.Country.Code != CountryCodeType.USA)
                {
                    throw new ArgumentOutOfRangeException("Only USA is supported for ethnicity reporting in Colleague.");
                }
                if (reportingItem != null && reportingItem.Country != null && reportingItem.Country.EthnicCategory == null)
                {
                    throw new ArgumentNullException("reporting.country.ethnicCategory", "Reporting country ethnic category is required.");
                }
            }

            var ethnicityEntities = await _referenceDataRepository.EthnicitiesAsync();

            if (personEthnicityDtoProperty.EthnicGroup != null &&
                !string.IsNullOrEmpty(personEthnicityDtoProperty.EthnicGroup.Id) &&
                personEthnicityDtoProperty.Reporting != null
                && personEthnicityDtoProperty.Reporting.Any())
            {
                var reporting = personEthnicityDtoProperty.Reporting.FirstOrDefault();

                if (reporting.Country != null && reporting.Country.EthnicCategory != null)
                {
                    try
                    {
                        var ethnicCategory = (EthnicityType)Enum.Parse(typeof(Ellucian.Colleague.Domain.Base.Entities.EthnicityType), reporting.Country.EthnicCategory.Value.ToString());
                        var ethnicityEntity = ethnicityEntities
                            .FirstOrDefault(e => e.Guid.Equals(personEthnicityDtoProperty.EthnicGroup.Id, StringComparison.OrdinalIgnoreCase));

                        if (ethnicityEntity == null)
                        {
                            throw new KeyNotFoundException("Ethnicity ID associated with guid '" + personEthnicityDtoProperty.EthnicGroup.Id + "' not found in repository.");
                        }

                        if (reporting.Country != null && reporting.Country.EthnicCategory != null && reporting.Country.EthnicCategory.HasValue)
                        {
                            if (ethnicityEntity != null && !ethnicityEntity.Type.Equals(ethnicCategory))
                            {
                                throw new InvalidOperationException(string.Concat("Provided ethnic category '", reporting.Country.EthnicCategory.Value.ToString(),
                                    "' does not match the ethnic category for id ", personEthnicityDtoProperty.EthnicGroup.Id));
                            }
                        }
                    }
                    catch (ArgumentException)
                    {
                        throw new ArgumentException("Could not find enitity for ethnicity.reporting.country.ethnicCategory :" + reporting.Country.EthnicCategory.Value.ToString());
                    }
                }
            }

            //Conversion
            if (personEthnicityDtoProperty.EthnicGroup != null && !string.IsNullOrEmpty(personEthnicityDtoProperty.EthnicGroup.Id))
            {
                var ethnicityEntity = ethnicityEntities.FirstOrDefault(e => e.Guid.Equals(personEthnicityDtoProperty.EthnicGroup.Id, StringComparison.OrdinalIgnoreCase));
                if (ethnicityEntity == null)
                {
                    throw new KeyNotFoundException("Could not find enitity for ethnicity.ethnicGroup.id:" + personEthnicityDtoProperty.EthnicGroup.Id);
                }
                string ethnicityCode = ethnicityEntity.Code;

                if (ethnicityCodes == null) ethnicityCodes = new List<string>();

                ethnicityCodes.Add(ethnicityCode);
            }
            else if (personEthnicityDtoProperty.EthnicGroup == null && personEthnicityDtoProperty.Reporting != null && personEthnicityDtoProperty.Reporting.Any())
            {
                string ethnicCategory = string.Empty;
                try
                {
                    var reportingItem = personEthnicityDtoProperty.Reporting.FirstOrDefault();

                    if (reportingItem.Country != null && reportingItem.Country.EthnicCategory != null)
                    {
                        ethnicCategory = reportingItem.Country.EthnicCategory.Value.ToString();
                        var ethnicityEnityType = (EthnicityType)Enum.Parse(typeof(Ellucian.Colleague.Domain.Base.Entities.EthnicityType), ethnicCategory);
                        var ethnicityEntity = ethnicityEntities.FirstOrDefault(e => e.Type.Equals(ethnicityEnityType));
                        if (ethnicityCodes == null) ethnicityCodes = new List<string>();

                        if (ethnicityEntity != null) ethnicityCodes.Add(ethnicityEntity.Code);
                    }
                }
                catch (ArgumentException)
                {
                    throw new ArgumentException("Could not find enitity for ethnicity.reporting.country.ethnicCategory :" + ethnicCategory);
                }
            }
            return ethnicityCodes;
        }

        /// <summary>
        /// Converts drivers license
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="personGuid"></param>
        /// <param name="personIdentityDocuments"></param>
        /// <returns></returns>
        private async Task<PersonDriverLicense> ConvertPerson2DtoDriversLicenseToEntityDocumentAsync(string personId, string personGuid, IEnumerable<Dtos.DtoProperties.PersonIdentityDocument> personIdentityDocuments)
        {
            PersonDriverLicense personDriverLicense = null;

            if (personIdentityDocuments != null && personIdentityDocuments.Any())
            {
                if (personIdentityDocuments.Where(doc => doc.Type.Category == Dtos.EnumProperties.PersonIdentityDocumentCategory.PhotoId).Count() > 1)
                {
                    throw new InvalidOperationException("Providing more than one drivers license is not supported.");
                }

                var driversLic = personIdentityDocuments.FirstOrDefault(d => d.Type.Category == Dtos.EnumProperties.PersonIdentityDocumentCategory.PhotoId);

                if (driversLic != null)
                {
                    if (driversLic.Type.Detail != null && string.IsNullOrEmpty(driversLic.Type.Detail.Id))
                    {
                        throw new ArgumentNullException("identityDocuments.type.detail.id", "An identity document detail id is required.");
                    }
                    if (driversLic.Country != null && driversLic.Country.Code != IsoCode.USA && driversLic.Country.Code != IsoCode.CAN)
                    {
                        throw new ArgumentOutOfRangeException("identityDocuments.country.code", "Only 'USA' and 'CAN' country codes are supported for a drivers license");
                    }

                    if (driversLic.Type.Detail != null && !string.IsNullOrEmpty(driversLic.Type.Detail.Id))
                    {
                        var identityDocTypes = await _referenceDataRepository.GetIdentityDocumentTypesAsync(false);

                        var driverLicenseDocType = identityDocTypes.FirstOrDefault(doc => doc.Guid.Equals(driversLic.Type.Detail.Id, StringComparison.OrdinalIgnoreCase));
                        if (driverLicenseDocType == null)
                        {
                            throw new KeyNotFoundException("Could not find enitity for identity document type with detail id: " + driversLic.Type.Detail.Id);
                        }

                        if (driverLicenseDocType.IdentityDocumentTypeCategory != IdentityDocumentTypeCategory.PhotoId)
                        {
                            throw new InvalidOperationException("Document entity type by Id '" + driversLic.Type.Detail.Id + "' does not match the document category of photoId");
                        }

                        personDriverLicense = new PersonDriverLicense(string.IsNullOrEmpty(personId) ? "$NEW.PERSON" : personId, driversLic.DocumentId, driversLic.ExpiresOn)
                        {
                            PersonGuid = personGuid,
                            DocType = driverLicenseDocType.Code
                        };

                        if (driversLic.Country != null && driversLic.Country.Region != null && !string.IsNullOrEmpty(driversLic.Country.Region.Code))
                        {
                            var issuingState = driversLic.Country.Region.Code.Split("-".ToCharArray()).ElementAt(1);
                            personDriverLicense.IssuingState = issuingState;
                        }
                    }
                    else if (Enum.IsDefined(typeof(Dtos.EnumProperties.PersonIdentityDocumentCategory), driversLic.Type.Category))
                    {
                        try
                        {
                            var identityDocTypes = await _referenceDataRepository.GetIdentityDocumentTypesAsync(false);

                            IdentityDocumentTypeCategory docTypeCategory = (IdentityDocumentTypeCategory)Enum.Parse(typeof(IdentityDocumentTypeCategory), driversLic.Type.Category.ToString());
                            var driverLicenseDocType = identityDocTypes.FirstOrDefault(doc => doc.IdentityDocumentTypeCategory == docTypeCategory && doc.Code.Equals("LICENSE", StringComparison.OrdinalIgnoreCase));
                            if (driverLicenseDocType == null)
                            {
                                throw new KeyNotFoundException("Could not find enitity for identity document type with category: " + driversLic.Type.Category.ToString());
                            }

                            personDriverLicense = new PersonDriverLicense(string.IsNullOrEmpty(personId) ? "$NEW.PERSON" : personId, driversLic.DocumentId, driversLic.ExpiresOn)
                            {
                                PersonGuid = personGuid,
                                DocType = driverLicenseDocType.Code
                            };

                            if (driversLic.Country != null && driversLic.Country.Region != null && !string.IsNullOrEmpty(driversLic.Country.Region.Code))
                            {
                                var issuingState = driversLic.Country.Region.Code.Split("-".ToCharArray()).ElementAt(1);
                                personDriverLicense.IssuingState = issuingState;
                            }
                        }
                        catch (ArgumentException)
                        {
                            throw new ArgumentException("Drivers license type entity not found with type: " + driversLic.Type.Category.ToString());
                        }
                    }
                }
            }
            return personDriverLicense;
        }

        /// <summary>
        /// Converts other identity documents to person Entity
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="personGuid"></param>
        /// <param name="personIdentityDocuments"></param>
        /// <returns></returns>
        private async Task<List<Domain.Base.Entities.PersonIdentityDocuments>> ConvertPerson2DtoIdentityDocumentsToEntityAsync(string personId, string personGuid, IEnumerable<Dtos.DtoProperties.PersonIdentityDocument> personIdentityDocuments)
        {
            List<Domain.Base.Entities.PersonIdentityDocuments> personDocuments = new List<PersonIdentityDocuments>();

            if (personIdentityDocuments != null && personIdentityDocuments.Any())
            {
                foreach (var document in personIdentityDocuments)
                {
                    Domain.Base.Entities.PersonIdentityDocuments personDocument = null;
                    if (document != null && document.Type != null && document.Type.Category == PersonIdentityDocumentCategory.Other)
                    {
                        string countryCode = "";
                        if (document.Country != null && document.Country.Code != null)
                        {
                            countryCode = document.Country.Code.ToString();
                            var place = (await _personRepository.GetPlacesAsync()).FirstOrDefault(x => x.PlacesCountry == countryCode);
                            if (place == null)
                            {
                                throw new ArgumentException(string.Format("Country code '{0}' cannot be found on the places table. ", countryCode), "identityDocuments.country.code");
                            }
                        }
                        if (document.Country != null && document.Country.Region != null && !string.IsNullOrEmpty(document.Country.Region.Code))
                        {
                            if (document.Country.Region != null && !string.IsNullOrEmpty(document.Country.Region.Code))
                            {
                                var regionCode = document.Country.Region.Code;
                                var place = (await _personRepository.GetPlacesAsync()).FirstOrDefault(x => x.PlacesCountry == countryCode && x.PlacesRegion == regionCode);
                                if (place == null)
                                {
                                    throw new ArgumentException(string.Format("Region code '{0}' with country '{1}' cannot be found on the places table. ", regionCode, countryCode), "identityDocuments.country.region.code");
                                }
                            }
                        }
                        if (document.Type.Detail != null && !string.IsNullOrEmpty(document.Type.Detail.Id))
                        {
                            var identityDocTypes = await _referenceDataRepository.GetIdentityDocumentTypesAsync(false);

                            var documentDocType = identityDocTypes.FirstOrDefault(doc => doc.Guid.Equals(document.Type.Detail.Id, StringComparison.OrdinalIgnoreCase));
                            if (documentDocType == null)
                            {
                                throw new KeyNotFoundException("Could not find enitity for identity document type with detail id: " + document.Type.Detail.Id);
                            }

                            if (documentDocType.IdentityDocumentTypeCategory != IdentityDocumentTypeCategory.Other)
                            {
                                throw new InvalidOperationException("Document entity type by Id '" + document.Type.Detail.Id + "' does not match the document category of 'other'");
                            }

                            personDocument = new Domain.Base.Entities.PersonIdentityDocuments(string.IsNullOrEmpty(personId) ? "$NEW.PERSON" : personId, document.DocumentId, document.ExpiresOn)
                            {
                                PersonGuid = personGuid,
                                DocType = documentDocType.Code,
                                Country = (document.Country != null) ? document.Country.Code.ToString() : null,
                                Region = (document.Country != null && document.Country.Region != null) ? document.Country.Region.Code : null
                            };
                        }
                        else if (Enum.IsDefined(typeof(Dtos.EnumProperties.PersonIdentityDocumentCategory), document.Type.Category))
                        {
                            try
                            {
                                var identityDocTypes = await _referenceDataRepository.GetIdentityDocumentTypesAsync(false);

                                IdentityDocumentTypeCategory docTypeCategory = (IdentityDocumentTypeCategory)Enum.Parse(typeof(IdentityDocumentTypeCategory), document.Type.Category.ToString());
                                var identityDocType = identityDocTypes.FirstOrDefault(doc => doc.IdentityDocumentTypeCategory == docTypeCategory && doc.Code.Equals("OTHER", StringComparison.OrdinalIgnoreCase));
                                if (identityDocType == null)
                                {
                                    throw new KeyNotFoundException("Could not find enitity for identity document type with category: " + document.Type.Category.ToString());
                                }

                                personDocument = new Domain.Base.Entities.PersonIdentityDocuments(string.IsNullOrEmpty(personId) ? "$NEW.PERSON" : personId, document.DocumentId, document.ExpiresOn)
                                {
                                    PersonGuid = personGuid,
                                    DocType = identityDocType.Code,
                                    Country = (document.Country != null) ? document.Country.Code.ToString() : null,
                                    Region = (document.Country != null && document.Country.Region != null) ? document.Country.Region.Code : null
                                };
                            }
                            catch (ArgumentException)
                            {
                                throw new ArgumentException("Identity Document type entity not found with type: " + document.Type.Category.ToString());
                            }
                        }
                        if (personDocument != null)
                        {
                            personDocuments.Add(personDocument);
                        }
                    }
                }
            }
            return personDocuments.Any() ? personDocuments : null;
        }

        /// <summary>
        /// Converts passport
        /// </summary>
        /// <param name="personId"></param>
        /// <param name="personGuid"></param>
        /// <param name="personIdentityDocuments"></param>
        /// <returns>PersonPassport</returns>
        private async Task<PersonPassport> ConvertPerson2DtoPassportDocumentToEntityAsync(string personId, string personGuid, IEnumerable<Dtos.DtoProperties.PersonIdentityDocument> personIdentityDocuments)
        {
            PersonPassport personPassport = null;
            if (personIdentityDocuments != null && personIdentityDocuments.Any())
            {
                if (personIdentityDocuments.Where(doc => doc.Type.Category == Dtos.EnumProperties.PersonIdentityDocumentCategory.Passport).Count() > 1)
                {
                    throw new InvalidOperationException("Providing information on more than one passport is not supported.");
                }

                var passportDoc = personIdentityDocuments.FirstOrDefault(d => d.Type.Category == Dtos.EnumProperties.PersonIdentityDocumentCategory.Passport);
                if (passportDoc != null)
                {
                    if (passportDoc.Type.Detail != null && string.IsNullOrEmpty(passportDoc.Type.Detail.Id))
                    {
                        throw new ArgumentNullException("identityDocuments.type.detail.id", "An identity document detail id is required.");
                    }
                    if (passportDoc.Country != null && passportDoc.Country.Region != null)
                    {
                        throw new ArgumentOutOfRangeException("identityDocuments.country.region", "Region cannot be specified for a passport");
                    }

                    if (passportDoc.Type.Detail != null && !string.IsNullOrEmpty(passportDoc.Type.Detail.Id))
                    {
                        var identityDocTypes = await _referenceDataRepository.GetIdentityDocumentTypesAsync(false);

                        var passportDocType = identityDocTypes.FirstOrDefault(doc => doc.Guid.Equals(passportDoc.Type.Detail.Id, StringComparison.OrdinalIgnoreCase));
                        if (passportDocType == null)
                        {
                            throw new KeyNotFoundException("Could not find enitity for identity document type with detail id: " + passportDoc.Type.Detail.Id);
                        }

                        personPassport = new PersonPassport(string.IsNullOrEmpty(personId) ? "$NEW.PERSON" : personId, passportDoc.DocumentId)
                        {
                            ExpireDate = passportDoc.ExpiresOn,
                            IssuingCountry = passportDoc.Country != null && passportDoc.Country.Code != null ?
                                                await ConvertPerson2CountryCodeDtoToEntityAsync(passportDoc.Country.Code.ToString()) : null,
                            PersonGuid = personGuid,
                            DocType = passportDocType.Code
                        };
                    }
                    else if (Enum.IsDefined(typeof(Dtos.EnumProperties.PersonIdentityDocumentCategory), passportDoc.Type.Category))
                    {
                        try
                        {
                            var identityDocTypes = await _referenceDataRepository.GetIdentityDocumentTypesAsync(false);

                            IdentityDocumentTypeCategory docTypeCategory = (IdentityDocumentTypeCategory)Enum.Parse(typeof(IdentityDocumentTypeCategory), passportDoc.Type.Category.ToString());
                            var passportDocType = identityDocTypes.FirstOrDefault(doc => doc.IdentityDocumentTypeCategory == docTypeCategory && doc.Code.Equals("PASSPORT", StringComparison.OrdinalIgnoreCase));
                            if (passportDocType == null)
                            {
                                throw new KeyNotFoundException("Could not find enitity for identity document type with category: " + passportDoc.Type.Category.ToString());
                            }

                            personPassport = new PersonPassport(string.IsNullOrEmpty(personId) ? "$NEW.PERSON" : personId, passportDoc.DocumentId)
                            {
                                ExpireDate = passportDoc.ExpiresOn,
                                IssuingCountry = passportDoc.Country != null && passportDoc.Country.Code != null ?
                                                await ConvertPerson2CountryCodeDtoToEntityAsync(passportDoc.Country.Code.ToString()) : null,
                                PersonGuid = personGuid,
                                DocType = passportDocType.Code
                            };
                        }
                        catch (ArgumentException)
                        {
                            throw new ArgumentException("Could not find enitity for identity document type with category: " + passportDoc.Type.Category.ToString());
                        }
                    }
                }
            }
            return personPassport;
        }

        /// <summary>
        /// Converts country code
        /// </summary>
        /// <param name="countryIso3Code"></param>
        /// <returns>string</returns>
        private async Task<string> ConvertPerson2CountryCodeDtoToEntityAsync(string countryIso3Code)
        {
            string code = "";
            if (!string.IsNullOrEmpty(countryIso3Code))
            {
                var entities = await _referenceDataRepository.GetCountryCodesAsync();
                var entity = entities.FirstOrDefault(c => c.Iso3Code.Equals(countryIso3Code, StringComparison.OrdinalIgnoreCase));
                if (entity == null)
                {
                    throw new KeyNotFoundException("Country not found with Iso3 code: " + countryIso3Code);
                }
                code = entity.Code;
            }
            return code;
        }

        private string MapPrefixCode(string prefixCode)
        {
            string prefix = null;
            if (!string.IsNullOrEmpty(prefixCode))
            {
                var prefixEntity = _referenceDataRepository.Prefixes.FirstOrDefault(p => p.Code == prefixCode);
                if (prefixEntity != null && !string.IsNullOrEmpty(prefixEntity.Abbreviation))
                {
                    prefix = prefixEntity.Abbreviation;
                }
                else
                {
                    logger.Info("The '" + prefixCode +
                                "' prefix code is not defined.  Ignoring during person create/update.");
                }
            }
            return prefix;
        }

        private async Task<string> ValidatePrefixAsync(string prefixCode)
        {

            if (string.IsNullOrEmpty(prefixCode))
            {
                return null;
            }
            var prefixEntity =
                (await _referenceDataRepository.GetPrefixesAsync()).FirstOrDefault(p => p.Abbreviation == prefixCode);
            if (prefixEntity != null)
            {
                return prefixEntity.Abbreviation;
            }
            throw new ArgumentException(string.Format("The '{0}' prefix code is not defined.", prefixCode));
        }

        private string MapSuffixCode(string suffixCode)
        {
            string suffix = null;
            if (!string.IsNullOrEmpty(suffixCode))
            {
                var suffixEntity = _referenceDataRepository.Suffixes.FirstOrDefault(s => s.Code == suffixCode);
                if (suffixEntity != null && !string.IsNullOrEmpty(suffixEntity.Abbreviation))
                {
                    suffix = suffixEntity.Abbreviation;
                }
                else
                {
                    logger.Info("The '" + suffixCode +
                                "' suffix code is not defined.  Ignoring during person create/update.");
                }
            }
            return suffix;
        }

        private async Task<string> ValdiateSuffixAsync(string suffixCode)
        {
            if (string.IsNullOrEmpty(suffixCode))
            {
                return null;
            }
            var suffixEntity =
                (await _referenceDataRepository.GetSuffixesAsync()).FirstOrDefault(s => s.Abbreviation == suffixCode);
            if (suffixEntity != null && !string.IsNullOrEmpty(suffixEntity.Abbreviation))
            {
                return suffixEntity.Abbreviation;
            }
            throw new ArgumentException(string.Format("The '{0}' suffix code is not defined.", suffixCode));
        }

        private string MapSsn(IEnumerable<Dtos.Credential> credentials)
        {
            string ssn = null;
            if (credentials != null)
            {
                var ssnCredential =
                    credentials.Where(c => c.CredentialType == Dtos.CredentialType.SocialSecurityNumber)
                        .FirstOrDefault();
                if (ssnCredential != null && !string.IsNullOrEmpty(ssnCredential.Id))
                {
                    ssn = ssnCredential.Id;
                }
            }
            return ssn;
        }

        private string MapSsn2(IEnumerable<Dtos.DtoProperties.CredentialDtoProperty> credentials)
        {
            string ssn = null;
            if (credentials != null)
            {
                var ssnCredential =
                    credentials.Where(c => c.Type == Dtos.EnumProperties.CredentialType.Ssn || c.Type == Dtos.EnumProperties.CredentialType.Sin)
                        .FirstOrDefault();
                if (ssnCredential != null && !string.IsNullOrEmpty(ssnCredential.Value))
                {
                    ssn = ssnCredential.Value;
                }
            }
            return ssn;
        }

        private string MapSsn3(IEnumerable<Dtos.DtoProperties.CredentialDtoProperty2> credentials)
        {
            string ssn = null;
            if (credentials != null)
            {
                var ssnCredential =
                    credentials.Where(c => c.Type == Dtos.EnumProperties.CredentialType2.Ssn || c.Type == Dtos.EnumProperties.CredentialType2.Sin)
                        .FirstOrDefault();
                if (ssnCredential != null && !string.IsNullOrEmpty(ssnCredential.Value))
                {
                    ssn = ssnCredential.Value;
                }
            }
            return ssn;
        }

        private string MapColleagueId(IEnumerable<Dtos.Credential> credentials)
        {
            string personId = null;
            if (credentials != null)
            {
                var personIdCredential =
                    credentials.Where(c => c.CredentialType == Dtos.CredentialType.ColleaguePersonId).FirstOrDefault();
                if (personIdCredential != null && !string.IsNullOrEmpty(personIdCredential.Id))
                {
                    personId = personIdCredential.Id;
                }
            }
            return personId;
        }

        private string MapColleagueId2(IEnumerable<Dtos.DtoProperties.CredentialDtoProperty> credentials)
        {
            string personId = null;
            if (credentials != null)
            {
                var personIdCredential =
                    credentials.Where(c => c.Type == Dtos.EnumProperties.CredentialType.ColleaguePersonId).FirstOrDefault();
                if (personIdCredential != null && !string.IsNullOrEmpty(personIdCredential.Value))
                {
                    personId = personIdCredential.Value;
                }
            }
            return personId;
        }

        private string MapColleagueId3(IEnumerable<Dtos.DtoProperties.CredentialDtoProperty2> credentials)
        {
            string personId = null;
            if (credentials != null)
            {
                var personIdCredential =
                    credentials.Where(c => c.Type == Dtos.EnumProperties.CredentialType2.ColleaguePersonId).FirstOrDefault();
                if (personIdCredential != null && !string.IsNullOrEmpty(personIdCredential.Value))
                {
                    personId = personIdCredential.Value;
                }
            }
            return personId;
        }

        private IEnumerable<Domain.Base.Entities.PersonAlt> MapPersonAltIds(IEnumerable<Dtos.Credential> credentials)
        {
            var personAltIds = new List<Domain.Base.Entities.PersonAlt>();
            if (credentials != null)
            {
                // map the person's Elevate ID
                var elevatePersonIdCredential =
                    credentials.Where(c => c.CredentialType == Dtos.CredentialType.ElevatePersonId).FirstOrDefault();
                if (elevatePersonIdCredential != null && !string.IsNullOrEmpty(elevatePersonIdCredential.Id))
                {
                    personAltIds.Add(new Domain.Base.Entities.PersonAlt(elevatePersonIdCredential.Id,
                        Domain.Base.Entities.PersonAlt.ElevatePersonAltType));
                }
            }
            return personAltIds;
        }

        private string MapGenderType(Dtos.GenderType? genderType)
        {
            switch (genderType)
            {
                case Dtos.GenderType.Male:
                    {
                        return "M";
                    }
                case Dtos.GenderType.Female:
                    {
                        return "F";
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        private string MapGenderType2(Dtos.EnumProperties.GenderType2? genderType)
        {
            switch (genderType)
            {
                case Dtos.EnumProperties.GenderType2.Male:
                    {
                        return "M";
                    }
                case Dtos.EnumProperties.GenderType2.Female:
                    {
                        return "F";
                    }
                default:
                    {
                        return null;
                    }
            }
        }

        private async Task<string> MapMaritalStatusAsync(Dtos.GuidObject maritalStatusGuid)
        {
            string maritalStatus = null;
            if (maritalStatusGuid != null && !string.IsNullOrEmpty(maritalStatusGuid.Guid))
            {
                var maritalStatusEntity
                    =
                    (await _referenceDataRepository.MaritalStatusesAsync()).FirstOrDefault(
                        m => m.Guid == maritalStatusGuid.Guid);
                if (maritalStatusEntity == null || string.IsNullOrEmpty(maritalStatusEntity.Code))
                {
                    string error = "The '" + maritalStatusGuid.Guid + "' marital status guid is not defined.";
                    logger.Error(error);
                    throw new ArgumentException(error);
                }
                maritalStatus = maritalStatusEntity.Code;
            }
            return maritalStatus;
        }

        private async Task<List<string>> MapEthnicityAsync(Dtos.GuidObject ethnicityGuid)
        {
            List<string> ethnicityCodes = null;
            if (ethnicityGuid != null && !string.IsNullOrEmpty(ethnicityGuid.Guid))
            {
                var ethnicityEntity
                    =
                    (await _referenceDataRepository.EthnicitiesAsync()).FirstOrDefault(e => e.Guid == ethnicityGuid.Guid);
                if (ethnicityEntity == null || string.IsNullOrEmpty(ethnicityEntity.Code))
                {
                    string error = "The '" + ethnicityGuid.Guid + "' ethnicity guid is not defined.";
                    logger.Error(error);
                    throw new ArgumentException(error);
                }
                ethnicityCodes = new List<string>() { ethnicityEntity.Code };
            }
            return ethnicityCodes;
        }

        private async Task<List<string>> MapRacesAsync(IEnumerable<Dtos.GuidObject> raceGuids)
        {
            List<string> raceCodes = null;
            if (raceGuids != null && raceGuids.Count() > 0)
            {
                raceCodes = new List<string>();
                foreach (var raceGuid in raceGuids)
                {
                    var raceEntity =
                        (await _referenceDataRepository.RacesAsync()).FirstOrDefault(r => r.Guid == raceGuid.Guid);
                    if (raceEntity == null || string.IsNullOrEmpty(raceEntity.Code))
                    {
                        string error = "The '" + raceGuid.Guid + "' race guid is not defined.";
                        logger.Error(error);
                        throw new ArgumentException(error);
                    }
                    raceCodes.Add(raceEntity.Code);
                }
            }
            return raceCodes;
        }

        private IEnumerable<Domain.Base.Entities.EmailAddress> MapEmailAddresses(
            IEnumerable<Dtos.EmailAddress> emailAddressDtos)
        {
            List<Domain.Base.Entities.EmailAddress> emailAddressEntities = null;
            if (emailAddressDtos != null && emailAddressDtos.Count() > 0)
            {
                emailAddressEntities = new List<Domain.Base.Entities.EmailAddress>();

                foreach (var emailAddressDto in emailAddressDtos)
                {
                    emailAddressEntities.Add(new Domain.Base.Entities.EmailAddress(emailAddressDto.Address,
                        emailAddressDto.EmailAddressType.ToString()));
                }
            }
            return emailAddressEntities;
        }

        private async Task<IEnumerable<Domain.Base.Entities.EmailAddress>> MapEmailAddresses2(
            IEnumerable<Dtos.DtoProperties.PersonEmailDtoProperty> emailAddressDtos)
        {
            List<Domain.Base.Entities.EmailAddress> emailAddressEntities = null;
            if (emailAddressDtos != null && emailAddressDtos.Any())
            {
                emailAddressEntities = new List<Domain.Base.Entities.EmailAddress>();
                var emailTypeEntities = await _referenceDataRepository.GetEmailTypesAsync(false);
                // Disallow duplicate email types in the DTO
                List<string> duplicateTypes = new List<string>();

                foreach (var emailAddressDto in emailAddressDtos)
                {
                    if (emailAddressDto.Type == null || emailAddressDto.Type.EmailType == null)
                        throw new ArgumentNullException("emailAddressDto.Type", "Email type is required to create a new email");
                    if (string.IsNullOrEmpty(emailAddressDto.Address))
                        throw new ArgumentNullException("emailAddressDto.Address", "Email address is required to create a new email");

                    string emailType = "";

                    if (emailAddressDto.Type.Detail != null && !string.IsNullOrEmpty(emailAddressDto.Type.Detail.Id))
                    {
                        try
                        {
                            emailType = emailTypeEntities.FirstOrDefault(et => et.Guid == emailAddressDto.Type.Detail.Id).Code;
                        }
                        catch
                        {
                            throw new ArgumentOutOfRangeException("emailAddressDto.Type", string.Format("Could not find the email type for email address '{0}' using guid '{1}. ", emailAddressDto.Address, emailAddressDto.Type.Detail.Id));
                        }
                    }
                    else
                    {
                        try
                        {
                            emailType = emailTypeEntities.FirstOrDefault(et => string.Equals(et.EmailTypeCategory.ToString(), emailAddressDto.Type.EmailType.ToString(), StringComparison.OrdinalIgnoreCase)).Code;
                        }
                        catch
                        {
                            throw new ArgumentOutOfRangeException("emailAddressDto.Type", string.Format("Could not find the email type for email address '{0}' using type '{1}'. ", emailAddressDto.Address, emailAddressDto.Type.EmailType.ToString()));
                        }
                    }
                    if (string.IsNullOrEmpty(emailType))
                    {
                        throw new ArgumentException(string.Format("Cannot find email type for '{0}'. ", emailAddressDto.Type.EmailType.ToString()), "emailAddressDto.Type.EmailType");
                    }
                    if (duplicateTypes.Contains(emailType))
                    {
                        throw new ArgumentException(string.Format("An email type cannot be duplicated for '{0}', type '{1}'", emailAddressDto.Address, emailAddressDto.Type.EmailType.ToString()));
                    }
                    duplicateTypes.Add(emailType);
                    var emailEntity = new Domain.Base.Entities.EmailAddress(emailAddressDto.Address, emailType);

                    if (emailAddressDto.Preference == Dtos.EnumProperties.PersonEmailPreference.Primary)
                    {
                        emailEntity.IsPreferred = true;
                    }
                    emailAddressEntities.Add(emailEntity);
                }
            }
            return emailAddressEntities;
        }

        #endregion

        #region Convert Person Dto to Address Entities Methods

        private async Task<IEnumerable<Domain.Base.Entities.Address>> ConvertPersonDtoToAddressEntitiesAsync(
            Dtos.Person personDto)
        {
            List<Domain.Base.Entities.Address> addressEntities = null;
            if (personDto.Addresses != null)
            {
                addressEntities = new List<Domain.Base.Entities.Address>();
                foreach (var addressDto in personDto.Addresses)
                {
                    if (addressDto.AddressType == null)
                        throw new ArgumentNullException("personDto", "Address type is required");
                    if (string.IsNullOrEmpty(addressDto.StreetAddress1))
                        throw new ArgumentNullException("personDto", "Street address line 1 is required");
                    if (string.IsNullOrEmpty(addressDto.City))
                        throw new ArgumentNullException("personDto", "City is required");

                    var address = new Domain.Base.Entities.Address();
                    address.City = addressDto.City;
                    address.Type = addressDto.AddressType.ToString();
                    address.PostalCode = addressDto.PostalCode;
                    var country = await MapCountryToEntityAsync(addressDto.Country);
                    if (country != null) address.Country = country.Code;
                    var county = MapCountyToEntity(addressDto.County);
                    if (county != null) address.County = county.Code;
                    var state = Task.Run(async () =>
                    {
                        return await MapStateToEntityAsync(addressDto.Region);
                    }).GetAwaiter().GetResult();
                    if (state != null) address.State = state.Code;
                    var addressLines = new List<string>();
                    addressLines.Add(addressDto.StreetAddress1);
                    if (!string.IsNullOrEmpty(addressDto.StreetAddress2)) addressLines.Add(addressDto.StreetAddress2);
                    if (!string.IsNullOrEmpty(addressDto.StreetAddress3)) addressLines.Add(addressDto.StreetAddress3);
                    address.AddressLines = addressLines;
                    addressEntities.Add(address);
                }
            }
            return addressEntities;
        }

        private async Task<IEnumerable<Domain.Base.Entities.Address>> ConvertAddressDtoToAddressEntitiesAsync(IEnumerable<PersonAddressDtoProperty> addressList )
        {
            List<Domain.Base.Entities.Address> addressEntities = null;
            if (addressList != null && addressList.Any())
            {
                addressEntities = new List<Domain.Base.Entities.Address>();
                foreach (var addressDto in addressList)
                {
                    if (addressDto.address == null)
                        throw new ArgumentNullException("personDto", "Address property is required");

                    if (addressDto.Type == null || addressDto.Type.AddressType == null)
                        throw new ArgumentNullException("personDto", "Address type is required");

                    var addressDtoAddress = addressDto.address;
                    var addressEntity = new Domain.Base.Entities.Address();
                    if (string.IsNullOrEmpty(addressDtoAddress.Id))
                    {
                        if (addressDtoAddress.Place == null)
                            throw new ArgumentNullException("personDto", "Address place is required");
                        if (!addressDtoAddress.AddressLines.Any())
                            throw new ArgumentNullException("personDto", "Street address lines is required");
                        if (addressDtoAddress.Place.Country == null)
                            throw new ArgumentNullException("personDto", "Address place country is required");
                        //if (string.IsNullOrEmpty(addressDtoAddress.Place.Country.Locality))
                        //    throw new ArgumentNullException("personDto", "City is required");
                        var addressCountry = new Dtos.AddressCountry();

                        addressEntity.Guid = addressDtoAddress.Id;
                        addressEntity.AddressLines = addressDtoAddress.AddressLines;
                        addressEntity.Latitude = addressDtoAddress.Latitude;
                        addressEntity.Longitude = addressDtoAddress.Longitude;
                        addressEntity.AddressChapter = new List<string>();

                        if ((addressDtoAddress.GeographicAreas != null) && (addressDtoAddress.GeographicAreas.Any()))
                        {
                            var chapterEntities = await _referenceDataRepository.GetChaptersAsync(true);

                            foreach (var area in addressDtoAddress.GeographicAreas)
                            {
                                var geographicAreaEntity = await _referenceDataRepository.GetRecordInfoFromGuidGeographicAreaAsync(area.Id);
                                if (geographicAreaEntity == Domain.Base.Entities.GeographicAreaTypeCategory.Fundraising)
                                {
                                    var chapter = chapterEntities.FirstOrDefault(x => x.Guid == area.Id);
                                    if (chapter != null)
                                        addressEntity.AddressChapter.Add(chapter.Code);
                                }
                            }
                        }
                        if (addressDtoAddress.Place != null)
                        {
                            if (addressDtoAddress.Place != null && addressDtoAddress.Place.Country != null && !string.IsNullOrEmpty(addressDtoAddress.Place.Country.Code.ToString()))
                            {
                                addressCountry = addressDtoAddress.Place.Country;
                            }
                            else
                            {
                                throw new ArgumentNullException("addressDto.place.country.code", "A country code is required for an address with a place defined.");
                            }
                            var country = (await _referenceDataRepository.GetCountryCodesAsync()).FirstOrDefault(x => x.IsoAlpha3Code == addressCountry.Code.ToString());
                            if (country == null)
                            {
                                throw new KeyNotFoundException("Unable to locate country code. ");
                            }

                            switch (addressCountry.Code)
                            {
                                case Dtos.EnumProperties.IsoCode.USA:
                                    addressEntity.CountryCode = country.IsoAlpha3Code;
                                    addressEntity.CorrectionDigit = string.IsNullOrEmpty(addressCountry.CorrectionDigit) ? null : addressCountry.CorrectionDigit;
                                    addressEntity.CarrierRoute = string.IsNullOrEmpty(addressCountry.CarrierRoute) ? null : addressCountry.CarrierRoute;
                                    addressEntity.DeliveryPoint = string.IsNullOrEmpty(addressCountry.DeliveryPoint) ? null : addressCountry.DeliveryPoint;
                                    break;
                                default:
                                    addressEntity.CountryCode = country.IsoAlpha3Code;
                                    if (!string.IsNullOrEmpty(addressCountry.CorrectionDigit) || !string.IsNullOrEmpty(addressCountry.CarrierRoute) || !string.IsNullOrEmpty(addressCountry.DeliveryPoint))
                                    {
                                        throw new ArgumentOutOfRangeException("addressDto.place.country", "correctionDigit, carrierRoute and deliveryPoint can only be specified when code is 'USA'.");
                                    }
                                    break;
                            }

                            if (addressCountry.Region != null && !string.IsNullOrEmpty(addressCountry.Region.Code))
                            {
                                if (addressCountry.Code == IsoCode.USA || addressCountry.Code == IsoCode.CAN)
                                {
                                    string state = "";
                                    if (addressCountry.Region.Code.Contains("-"))
                                        state = addressCountry.Region.Code.Substring(3);
                                    var states =
                                        (await _referenceDataRepository.GetStateCodesAsync()).FirstOrDefault(
                                            x => x.Code == state);

                                    if (states != null)
                                    {
                                        addressEntity.State = states.Code;
                                    }
                                    else
                                    {
                                        addressEntity.IntlRegion = addressCountry.Region == null
                                            ? null
                                            : addressCountry.Region.Code;
                                    }
                                }
                                else
                                {
                                    addressEntity.IntlRegion = addressCountry.Region == null
                                        ? null
                                        : addressCountry.Region.Code;
                                }
                            }

                            if (addressCountry.SubRegion != null && !string.IsNullOrEmpty(addressCountry.SubRegion.Code))
                            {
                                var county = (await _referenceDataRepository.GetCountiesAsync(false)).FirstOrDefault(c => c.Code == addressCountry.SubRegion.Code);
                                if (county != null)
                                    addressEntity.County = county.Code;
                                else
                                    addressEntity.IntlSubRegion = addressCountry.SubRegion == null ? null : addressCountry.SubRegion.Code;
                            }

                            addressEntity.City = string.IsNullOrEmpty(addressCountry.Locality) ? null : addressCountry.Locality;
                            addressEntity.PostalCode = string.IsNullOrEmpty(addressCountry.PostalCode) ? null : addressCountry.PostalCode;
                            addressEntity.IntlLocality = addressCountry.Locality;
                            addressEntity.IntlPostalCode = addressCountry.PostalCode;
                            addressEntity.IntlRegion = addressCountry.Region == null ? null : addressCountry.Region.Code;
                            addressEntity.IntlSubRegion = addressCountry.SubRegion == null ? null : addressCountry.SubRegion.Code;
                        }
                    }
                    else
                    {
                        addressEntity.Guid = !string.IsNullOrEmpty(addressDtoAddress.Id) ? addressDtoAddress.Id : null;
                    }
                    var addressTypes = await _referenceDataRepository.GetAddressTypes2Async(false);
                    if (addressDto.Type.Detail != null && !string.IsNullOrEmpty(addressDto.Type.Detail.Id))
                    {
                        try
                        {
                            addressEntity.TypeCode = addressTypes.FirstOrDefault(at => at.Guid == addressDto.Type.Detail.Id).Code;
                        }
                        catch
                        {
                            throw new ArgumentOutOfRangeException("address.Type.Detail.Id", string.Format("Invalid Address Type guid '{0}' ", addressDto.Type.Detail.Id));
                        }
                    }
                    else
                    {
                        try
                        {
                            addressEntity.TypeCode = addressTypes.FirstOrDefault(at => at.AddressTypeCategory.ToString().Equals(addressDto.Type.AddressType.ToString(), StringComparison.OrdinalIgnoreCase)).Code;
                        }
                        catch
                        {
                            throw new ArgumentOutOfRangeException("address.Type.Addresstype", string.Format("Invalid Address Type '{0}' ", addressDto.Type.AddressType.ToString()));
                        }
                    }
                    // Update Effective dates and seasonal dates
                    var effectiveStartDate = addressDto.AddressEffectiveStart;
                    var effectiveEndDate = addressDto.AddressEffectiveEnd;
                    if (effectiveStartDate > effectiveEndDate && effectiveEndDate.HasValue)
                    {
                        throw new ArgumentOutOfRangeException("address.startOn", "Address effective end date must be after effective start date.");
                    }
                    if (addressDto.AddressEffectiveStart.HasValue)
                    {
                        addressEntity.EffectiveStartDate = addressDto.AddressEffectiveStart;
                    }
                    if (addressDto.AddressEffectiveEnd.HasValue)
                    {
                        addressEntity.EffectiveEndDate = addressDto.AddressEffectiveEnd;
                    }
                    if (addressDto.SeasonalOccupancies != null)
                    {
                        addressEntity.SeasonalDates = new List<AddressSeasonalDates>();
                        foreach (var seasonal in addressDto.SeasonalOccupancies)
                        {
                            string startOn = "";
                            string endOn = "";
                            if (seasonal.Recurrence != null)
                            {
                                if (seasonal.Recurrence.TimePeriod != null)
                                {
                                    var startDate = seasonal.Recurrence.TimePeriod.StartOn;
                                    var endDate = seasonal.Recurrence.TimePeriod.EndOn;
                                    if (startDate > endDate)
                                    {
                                        throw new ArgumentOutOfRangeException(
                                            "address.seasonalOccupancies.recurrance.timePeriod.startOn",
                                            "Seasonal end date must be after seasonal start date.");
                                    }
                                    startOn = string.Concat(startDate.Month, "/", startDate.Day);
                                    endOn = string.Concat(endDate.Month, "/", endDate.Day);
                                }
                            }
                            if (!string.IsNullOrEmpty(startOn) && !string.IsNullOrEmpty(endOn))
                            {
                                var seasonalDates = new AddressSeasonalDates(startOn, endOn);
                                addressEntity.SeasonalDates.Add(seasonalDates);
                            }
                        }
                    }
                    addressEntity.IsPreferredResidence = addressDto.Preference == PersonPreference.Primary ? true : false;

                    // If we already have the address ID in the collection than consolidate
                    // the data for address type and make sure we don't have conflicting data
                    var existingAddress = addressEntities.FirstOrDefault(adr => adr.Guid == addressEntity.Guid &&
                        adr.AddressLines.SequenceEqual(addressEntity.AddressLines) &&
                        adr.IntlLocality == addressEntity.IntlLocality && adr.IntlPostalCode == adr.IntlPostalCode &&
                        adr.IntlRegion == addressEntity.IntlRegion && adr.IntlSubRegion == addressEntity.IntlSubRegion &&
                        adr.PostalCode == addressEntity.PostalCode && adr.State == addressEntity.State &&
                        adr.CountryCode == addressEntity.CountryCode);
                    if (existingAddress != null)
                    {
                        // Remove the original address entity
                        var index = addressEntities.FindIndex(adr => adr.Guid == addressEntity.Guid &&
                            adr.AddressLines.SequenceEqual(addressEntity.AddressLines) &&
                            adr.IntlLocality == addressEntity.IntlLocality && adr.IntlPostalCode == adr.IntlPostalCode &&
                            adr.IntlRegion == addressEntity.IntlRegion && adr.IntlSubRegion == addressEntity.IntlSubRegion &&
                            adr.PostalCode == addressEntity.PostalCode && adr.State == addressEntity.State &&
                            adr.CountryCode == addressEntity.CountryCode);
                        addressEntities.RemoveAt(index);
                        // Validate properties that must match
                        if (addressEntity.EffectiveStartDate.HasValue && existingAddress.EffectiveStartDate.HasValue)
                        {
                            if (addressEntity.EffectiveStartDate.Value != existingAddress.EffectiveStartDate.Value)
                            {
                                throw new ArgumentOutOfRangeException(
                                    "persons.addresses.address.startOn",
                                    "Existing Address of the same Id must have the same start date across all instances");
                            }
                        }
                        if (addressEntity.TypeCode == existingAddress.TypeCode)
                        {
                            throw new ArgumentOutOfRangeException(

                                "persons.addresses.address.type.addressType",
                                "An address can not be defined more than once with the same address type");
                        }
                        // Check effective end date.  If passed than do not add the address type to the new entity.
                        if (addressEntity.EffectiveEndDate <= DateTime.Today)
                        {
                            addressEntity.TypeCode = existingAddress.TypeCode;
                            addressEntity.EffectiveEndDate = existingAddress.EffectiveEndDate;
                        }
                        if (existingAddress.EffectiveEndDate <= DateTime.Today)
                        {
                            existingAddress.TypeCode = addressEntity.TypeCode;
                        }
                        // Update the address type to include types from both address entities
                        var addrTypes = existingAddress.TypeCode;
                        var addrType = addressEntity.TypeCode;
                        string[] newAddressTypes = addrTypes.Split(_SM);
                        if (!newAddressTypes.Contains(addrType))
                        {
                            if (addressEntity.IsPreferredResidence || addressEntity.IsPreferredAddress)
                            {
                                // Put preferred address type first
                                addrTypes = string.Concat(addrType, _SM, addrTypes);
                            }
                            else
                            {
                                addrTypes = string.Concat(addrTypes, _SM, addrType);
                            }
                            addressEntity.TypeCode = addrTypes;
                        }
                        // Update the Seasonal Dates
                        if (existingAddress.SeasonalDates != null)
                        {
                            if (existingAddress.SeasonalDates.Any())
                            {
                                foreach (var seasonalDates in existingAddress.SeasonalDates)
                                {
                                    // This could be subvalued so need to split on subvalue mark ASCII 252.
                                    var startDate = seasonalDates.StartOn;
                                    var endDate = seasonalDates.EndOn;
                                    try
                                    {
                                        AddressSeasonalDates newSeasonalDates = new AddressSeasonalDates(startDate, endDate);
                                        if (!addressEntity.SeasonalDates.Contains(newSeasonalDates))
                                        {
                                            addressEntity.SeasonalDates.Add(seasonalDates);
                                        }
                                    }
                                    catch (Exception ex)
                                    {
                                        throw new ArgumentOutOfRangeException("persons.addresses.address.seasonalOccupancies", ex);
                                    }
                                }
                            }
                        }
                        // Address Preferences
                        if (existingAddress.IsPreferredAddress) addressEntity.IsPreferredAddress = true;
                        if (existingAddress.IsPreferredResidence) addressEntity.IsPreferredResidence = true;
                        // Phone numbers
                        foreach (var phone in existingAddress.PhoneNumbers)
                        {
                            if (!addressEntity.PhoneNumbers.Contains(phone))
                            {
                                addressEntity.AddPhone(phone);
                            }
                        }
                    }
                    addressEntities.Add(addressEntity);
                }
            }
            return addressEntities;
        }

        private async Task<Domain.Base.Entities.Country> MapCountryToEntityAsync(Dtos.Country countryDto)
        {
            // validate the ISO country code and map it to the Colleague code
            Domain.Base.Entities.Country countryEntity = null;
            if (countryDto != null)
            {
                if (string.IsNullOrEmpty(countryDto.Code))
                    throw new ArgumentNullException("countryDto",
                        "Country ISO code is required when country is specified");

                var countryEntities = await _referenceDataRepository.GetCountryCodesAsync();
                countryEntity = countryEntities.FirstOrDefault(c => c.IsoCode == countryDto.Code);
                if (countryEntity == null)
                    throw new ArgumentNullException("countryDto", "Country ISO code is invalid");
            }
            return countryEntity;
        }

        private Domain.Base.Entities.County MapCountyToEntity(string countyDto)
        {
            // map the incoming county description to the Colleague description
            Domain.Base.Entities.County countyEntity = null;
            if (!string.IsNullOrEmpty(countyDto))
            {
                countyEntity = _referenceDataRepository.Counties.Where(c =>
                    c.Description.Equals(countyDto, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
                if (countyEntity == null)
                {
                    logger.Info("The '" + countyDto + "' county is not defined.  Ignoring during person create/update.");
                }
            }
            return countyEntity;
        }

        private async Task<Domain.Base.Entities.State> MapStateToEntityAsync(string stateDto)
        {
            // map the incoming state to Colleague
            Domain.Base.Entities.State stateEntity = null;
            if (!string.IsNullOrEmpty(stateDto))
            {
                if (stateDto.Length == 2)
                {
                    // check by code
                    var stateEntities = await _referenceDataRepository.GetStateCodesAsync();
                    stateEntity =
                        stateEntities.Where(s => s.Code.Equals(stateDto, StringComparison.OrdinalIgnoreCase))
                            .FirstOrDefault();
                }
                else
                {
                    // check by description
                    var stateEntityTask = await _referenceDataRepository.GetStateCodesAsync();
                    stateEntity =
                        stateEntityTask.Where(s => s.Description.Equals(stateDto, StringComparison.OrdinalIgnoreCase))
                            .FirstOrDefault();
                }
                if (stateEntity == null)
                {
                    logger.Info("The '" + stateDto + "' state is not defined.  Ignoring during person create/update.");
                }
            }
            return stateEntity;
        }

        #endregion

        #region Convert Person Dto to Phone Entities Methods

        private IEnumerable<Domain.Base.Entities.Phone> ConvertPersonDtoToPhoneEntities(Dtos.Person personDto)
        {
            var phoneEntities = new List<Domain.Base.Entities.Phone>();
            if (personDto.Phones != null)
            {
                foreach (var phoneDto in personDto.Phones)
                {
                    if (phoneDto.PhoneType == null)
                        throw new ArgumentNullException("personDto", "Phone type is required to create a new phone");
                    if (string.IsNullOrEmpty(phoneDto.Number))
                        throw new ArgumentNullException("personDto", "Phone number is required to create a new phone");

                    phoneEntities.Add(new Domain.Base.Entities.Phone(phoneDto.Number, phoneDto.PhoneType.ToString(),
                        phoneDto.Extension));
                }
            }
            return phoneEntities;
        }

        private async Task<IEnumerable<Domain.Base.Entities.Phone>> ConvertPhoneDtoToPhoneEntities(IEnumerable<PersonPhoneDtoProperty> phoneList)
        {
            var phoneEntities = new List<Domain.Base.Entities.Phone>();
            if (phoneList != null && phoneList.Any())
            {
                var phoneTypeEntities = await _referenceDataRepository.GetPhoneTypesAsync(false);
                foreach (var phoneDto in phoneList)
                {
                    if (string.IsNullOrEmpty(phoneDto.Number))
                        throw new ArgumentNullException("personDto.Phone.Number", "Phone number is required to create a new phone");
                    if (phoneDto.Type == null || phoneDto.Type.PhoneType == null)
                        throw new ArgumentNullException("personDto.Phone.Type", string.Format("A valid Phone type is required for phone number '{0}' ", phoneDto.Number));
                    if (phoneDto.Type.Detail != null && string.IsNullOrEmpty(phoneDto.Type.Detail.Id))
                        throw new ArgumentNullException("personDto.Phone.Type.Detail.Id", "The Detail Id is required when Detail has been defined.");

                    string phoneType = "";
                    if (phoneDto.Type.Detail != null && !string.IsNullOrEmpty(phoneDto.Type.Detail.Id))
                    {
                        try
                        {
                            phoneType = phoneTypeEntities.FirstOrDefault(et => et.Guid == phoneDto.Type.Detail.Id).Code;
                        }
                        catch
                        {
                            throw new ArgumentOutOfRangeException("phoneDto.Type.Detail.Id", string.Format("Could not find the phone type detail id '{0}' for phone number '{1}'. ", phoneDto.Type.Detail.Id, phoneDto.Number));
                        }
                    }
                    else
                    {
                        var phoneTypeEntity = phoneTypeEntities.FirstOrDefault(et => string.Equals(et.PhoneTypeCategory.ToString(), phoneDto.Type.PhoneType.ToString(), StringComparison.OrdinalIgnoreCase));
                        if (phoneTypeEntity != null)
                        {
                            phoneType = phoneTypeEntity.Code;
                        }
                    }

                    var phoneEntity = new Domain.Base.Entities.Phone(phoneDto.Number, phoneType, phoneDto.Extension);
                    if (phoneDto.Preference == Dtos.EnumProperties.PersonPreference.Primary) phoneEntity.IsPreferred = true;
                    phoneEntity.CountryCallingCode = phoneDto.CountryCallingCode;

                    phoneEntities.Add(phoneEntity);
                }
            }
            return phoneEntities;
        }

        #endregion

        #region Convert Organiztion Dto to Entity Methods

        private async Task<Domain.Base.Entities.PersonIntegration> ConvertOrganization2DtoToPersonEntityAsync(string personOrgId, Dtos.Organization2 organizationDto)
        {

            PersonIntegration personEntity = null;

            if (organizationDto == null || string.IsNullOrEmpty(organizationDto.Id))
                throw new ArgumentNullException("organizationDto", "Must provide guid for organization");

            if (organizationDto.Title == null)
                throw new ArgumentNullException("organizationDto", "Must provide organization title");
            
            personEntity = new PersonIntegration(personOrgId, organizationDto.Title)
            {
                Guid = organizationDto.Id.Equals(Guid.Empty.ToString(), StringComparison.OrdinalIgnoreCase) ? null : organizationDto.Id
            };

            personEntity.PreferredName = organizationDto.Title;
            
            // email addresses
            var emailAddressEntities = await MapEmailAddresses2(organizationDto.EmailAddresses);
            if (emailAddressEntities != null && emailAddressEntities.Count() > 0)
            {
                foreach (var emailAddressEntity in emailAddressEntities)
                {
                    personEntity.EmailAddresses.Add(emailAddressEntity);
                }
            }
            
            //roles
            if (organizationDto.Roles != null && organizationDto.Roles.Any())
            {
                personEntity.Roles.AddRange(ConvertOrganization2DtoRolesToEntity(organizationDto.Roles));
            }

            // Social Media
            if (organizationDto.SocialMedia != null && organizationDto.SocialMedia.Any())
            {
                personEntity.SocialMedia.AddRange(await ConvertPerson2DtoSocialMediaToEntity(organizationDto.SocialMedia));
            }

            // credentials
            if (organizationDto.Credentials != null && organizationDto.Credentials.Any())
            {
                //check to see if any unallowed credential types are listed
                if (organizationDto.Credentials.Any(
                        c => c.Type == CredentialType.Ssn || c.Type == CredentialType.Sin || 
                            c.Type == CredentialType.BannerId || c.Type == CredentialType.BannerSourcedId || 
                            c.Type == CredentialType.BannerUdcId || c.Type == CredentialType.BannerUserName))
                {
                   throw new ArgumentException("Unsupported credentials used. ssn, sin, bannerId, bannerSourceId, bannerUserName and bannerUdcId are not supported for use on Organizations"); 
                }

                //get the credentials that are of colleaguepersonid present, if any
                var colleaguePersonIdCreds = organizationDto.Credentials.Where(c => c.Type == CredentialType.ColleaguePersonId).ToList();

                if (colleaguePersonIdCreds.Any())
                {
                    //more than one colleaguepersonid is not allowed
                    if (colleaguePersonIdCreds.Count > 1)
                    {
                        throw new ArgumentException("Only one ColleaguePersonId credential is allowed.");
                    }
                    else if (colleaguePersonIdCreds.Count == 1 && !string.IsNullOrEmpty(personOrgId) && 
                        !string.Equals(personOrgId, colleaguePersonIdCreds[0].Value, StringComparison.OrdinalIgnoreCase))
                    {
                        //can only get here if there is a colleagueid present and the organization already exists so it has a colleague person id
                        //won't get here if the organization doesn't exists as the id will be passed in as null
                        throw new ArgumentException("ColleaguePersonId used does not match the existing id, this is not allowed to be changed.");
                    }
                }

                //get the credentials that are of colleaguepersonid present, if any
                var elevateIdCreds = organizationDto.Credentials.Where(c => c.Type == CredentialType.ElevateID).ToList();

                //more than one elevateid is not allowed
                if (elevateIdCreds.Count > 1)
                {
                    throw new ArgumentException("Only one ElevateId credential is allowed.");
                }

                ConvertPerson2DtoCredsToEntity(personOrgId, organizationDto.Credentials, personEntity);
            }

            return personEntity;
        }

        #endregion

        #region Convert Person Entity to Organization DTO

        /// <summary>
        /// Convert PersonIntegration return to Organization DTO
        /// </summary>
        /// <param name="personEntity"></param>
        /// <param name="bypassCache"></param>
        /// <returns>Organization2 DTO for Data Model</returns>
        private async Task<Dtos.Organization2> ConvertPersonIntegrationEntityToOrganizationDtoAsync(
            Domain.Base.Entities.PersonIntegration personEntity, bool bypassCache = false)
        {
            // create the person DTO
            var organizationDto = new Dtos.Organization2();

            organizationDto.Id = personEntity.Guid;
            organizationDto.Title = personEntity.PreferredName;

            var orgRoles = new List<Dtos.DtoProperties.OrganizationRoleDtoProperty>();

            if (personEntity.Roles != null && personEntity.Roles.Any())
            {
                foreach (var personRole in personEntity.Roles)
                {
                    //only add org roles that match to correct type
                    var orgRole = new Dtos.DtoProperties.OrganizationRoleDtoProperty();
                    switch (personRole.RoleType)
                    {
                        case PersonRoleType.Vendor:
                            orgRole.Type = OrganizationRoleType.Vendor;
                            orgRole.StartOn = personRole.StartDate;
                            orgRole.EndOn = personRole.EndDate;
                            orgRoles.Add(orgRole);
                            break;
                        case PersonRoleType.Partner:
                            orgRole.Type = OrganizationRoleType.Partner;
                            orgRole.StartOn = personRole.StartDate;
                            orgRole.EndOn = personRole.EndDate;
                            orgRoles.Add(orgRole);
                            break;
                        case PersonRoleType.Affiliate:
                            orgRole.Type = OrganizationRoleType.Affiliate;
                            orgRole.StartOn = personRole.StartDate;
                            orgRole.EndOn = personRole.EndDate;
                            orgRoles.Add(orgRole);
                            break;
                        case PersonRoleType.Constituent:
                            orgRole.Type = OrganizationRoleType.Constituent;
                            orgRole.StartOn = personRole.StartDate;
                            orgRole.EndOn = personRole.EndDate;
                            orgRoles.Add(orgRole);
                            break;
                    }
                }
            }

            if (orgRoles.Any())
            {
                organizationDto.Roles = orgRoles;
            }
            
            organizationDto.Credentials = await GetPersonCredentials(personEntity);

            List<Domain.Base.Entities.EmailAddress> emailEntities = personEntity.EmailAddresses;
            List<Domain.Base.Entities.Phone> phoneEntities = personEntity.Phones;
            List<Domain.Base.Entities.Address> addressEntities = personEntity.Addresses;
            List<Domain.Base.Entities.SocialMedia> socialMediaEntities = personEntity.SocialMedia;

            var emailAddresses = await GetEmailAddresses2(emailEntities, bypassCache);
            if ((emailAddresses != null) && (emailAddresses.Any())) organizationDto.EmailAddresses = emailAddresses;
            if (addressEntities != null)
            {
                var addresses = await GetAddresses2Async(addressEntities.Where(a => a.AddressLines != null && a.AddressLines.Any()).ToList(), bypassCache);
                if ((addresses != null) && (addresses.Any())) organizationDto.Addresses = addresses;
            }
            var phoneNumbers = await GetPhones2Async(phoneEntities, bypassCache);
            if ((phoneNumbers != null) && (phoneNumbers.Any())) organizationDto.Phones = phoneNumbers;
            if ((socialMediaEntities != null) && (socialMediaEntities.Any()))
                organizationDto.SocialMedia = await GetPersonSocialMediaAsync(socialMediaEntities, bypassCache);
            

            return organizationDto;
        }

        #endregion

        #region Shared Conversion Methods

        #endregion
    }
}