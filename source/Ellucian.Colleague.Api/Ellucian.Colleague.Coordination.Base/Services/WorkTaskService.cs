﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Base.Services
{
    /// <summary>
    /// Based on current user, processes work tasks for current user.
    /// </summary>
    [RegisterType]
    public class WorkTaskService : BaseCoordinationService, IWorkTaskService
    {
        private IWorkTaskRepository _workTaskRepository;
        private IRoleRepository _roleRepository;

        /// <summary>
        /// Constructor of the Work Task Service
        /// </summary>
        /// <param name="adapterRegistry"></param>
        /// <param name="workTaskRepository"></param>
        /// <param name="currentUserFactory"></param>
        /// <param name="roleRepository"></param>
        /// <param name="logger"></param>
        public WorkTaskService(IAdapterRegistry adapterRegistry, IWorkTaskRepository workTaskRepository, 
           ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            _workTaskRepository = workTaskRepository;
            _roleRepository = roleRepository;
        }

        /// <summary>
        /// Returns list of work tasks assigned to the current user and the current user's roles. 
        /// </summary>
        /// <param name="personId"></param>
        /// <returns>List of WorkTask items</returns>
        public async Task<List<Ellucian.Colleague.Dtos.Base.WorkTask>> GetAsync(string personId)
        {
            var workTaskDtos = new List<Ellucian.Colleague.Dtos.Base.WorkTask>();
            if (string.IsNullOrEmpty(personId))
            {
                throw new ArgumentNullException(personId);
            }
            if (!CurrentUser.IsPerson(personId))
            {
                throw new PermissionsException("Current user does not match passed person Id");
            }

            // Get the Role IDs for the user's roles
            var roleIds = new List<string>(); 
            var allRoles = await _roleRepository.GetRolesAsync();
            if (allRoles != null && allRoles.Count() > 0 && CurrentUser.Roles.Count() > 0)
            {
                roleIds = (from roleTitle in CurrentUser.Roles
                               join roleEntity in allRoles
                               on roleTitle equals roleEntity.Title into joinRoles
                               from role in joinRoles
                               select role.Id.ToString()).ToList();
            }

            // Get the workTasks pertinent to the specified users and roles
            var workTasks = await _workTaskRepository.GetAsync(personId, roleIds);
            if (workTasks != null)
            {
                var workTaskAdapter = _adapterRegistry.GetAdapter<Domain.Base.Entities.WorkTask, Dtos.Base.WorkTask>();
                foreach (var item in workTasks)
                {
                    workTaskDtos.Add(workTaskAdapter.MapToType(item));
                }
            }
            return workTaskDtos;
        }
    }
}
