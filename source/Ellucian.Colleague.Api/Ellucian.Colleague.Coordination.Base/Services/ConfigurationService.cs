﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Adapters;
using Ellucian.Colleague.Domain.Base;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;

namespace Ellucian.Colleague.Coordination.Base.Services
{
    [RegisterType]
    public class ConfigurationService : BaseCoordinationService, IConfigurationService
    {
        private readonly IConfigurationRepository _configurationRepository;
        private readonly ILogger repoLogger;

        /// <summary>
        /// Initializes a new instance of the <see cref="FacilitiesService"/> class.
        /// </summary>
        /// <param name="configurationRepository">The configuration repository.</param>
        /// <param name="adapterRegistry">The adapter registry.</param>
        /// <param name="currentUserFactory">The current user factory.</param>
        /// <param name="roleRepository">The role repository.</param>
        /// <param name="logger">The logger.</param>
        public ConfigurationService(IConfigurationRepository configurationRepository, IAdapterRegistry adapterRegistry, ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            _configurationRepository = configurationRepository;
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }
            this.repoLogger = logger;
        }

        /// <summary>
        /// Gets an integration configuration
        /// </summary>
        /// <param name="id">Integration Configuration ID</param>
        /// <returns>An integration configuration</returns>
        public Dtos.Base.IntegrationConfiguration GetIntegrationConfiguration(string id)
        {
            CheckIntegrationConfigPermission();
            var configEntity = _configurationRepository.GetIntegrationConfiguration(id);
            var integrationConfigurationAdapter = new IntegrationConfigurationEntityAdapter(_adapterRegistry, logger);
            return integrationConfigurationAdapter.MapToType(configEntity);
        }

        /// <summary>
        /// Gets the tax form configuration.
        /// </summary>
        /// <param name="taxFormId">The tax form (W-2, 1095-C, 1098-T, etc.)</param>
        /// <returns>Tax form configuration DTO.</returns>
        public async Task<Dtos.Base.TaxFormConfiguration> GetTaxFormConsentConfigurationAsync(Dtos.Base.TaxForms taxFormId)
        {
            Domain.Base.Entities.TaxForms taxFormDomain = new Domain.Base.Entities.TaxForms();

            // Convert the tax form dto into the domain dto.
            switch (taxFormId)
            {
                case Dtos.Base.TaxForms.FormW2:
                    taxFormDomain = Domain.Base.Entities.TaxForms.FormW2;
                    break;
                case Dtos.Base.TaxForms.Form1095C:
                    taxFormDomain = Domain.Base.Entities.TaxForms.Form1095C;
                    break;
                case Dtos.Base.TaxForms.Form1098:
                    taxFormDomain = Domain.Base.Entities.TaxForms.Form1098;
                    break;
            }

            // Retrieve the configuration parameters for this tax form
            var configuration = await this._configurationRepository.GetTaxFormConsentConfigurationAsync(taxFormDomain);

            if (configuration == null)
                throw new ArgumentNullException("configuration", "configuration cannot be null.");

            var taxFormConfigurationDtoAdapter = new TaxFormConfigurationEntityToDtoAdapter(_adapterRegistry, logger);
            var configurationDto = taxFormConfigurationDtoAdapter.MapToType(configuration);

            return configurationDto;
        }

        /// <summary>
        /// Helper method to determine if the user has permission to view integration configuration information.
        /// </summary>
        /// <exception><see cref="PermissionsException">PermissionsException</see></exception>
        protected void CheckIntegrationConfigPermission()
        {
            bool hasConfigPermission = HasPermission(BasePermissionCodes.ViewIntegrationConfig);

            // User is not allowed to view integration configuration information without the appropriate permissions
            if (!hasConfigPermission)
            {
                var message = "User " + CurrentUser.UserId + " does not have permission to view integration configuration information.";
                logger.Info(message);
                throw new PermissionsException(message);
            }
        }

        /// <summary>
        /// Calls configuration repository and returns the User Profile Configuration data transfer object.
        /// </summary>
        /// <returns><see cref="Dtos.Base.UserProfileConfiguration">User Profile Configuration</see> data transfer object.</returns>
        [Obsolete("Obsolete as of API 1.16. Use version 2 instead.")]
        public async Task<Dtos.Base.UserProfileConfiguration> GetUserProfileConfigurationAsync()
        {
            var configuration = await _configurationRepository.GetUserProfileConfigurationAsync();
            var adapter = _adapterRegistry.GetAdapter<Domain.Base.Entities.UserProfileConfiguration, Dtos.Base.UserProfileConfiguration>();
            var configurationDto = adapter.MapToType(configuration);
            return configurationDto;
        }

        /// <summary>
        /// Calls configuration repository and returns the User Profile Configuration data transfer object.
        /// </summary>
        /// <returns><see cref="Dtos.Base.UserProfileConfiguration2">User Profile Configuration</see> data transfer object.</returns>
        public async Task<Dtos.Base.UserProfileConfiguration2> GetUserProfileConfiguration2Async()
        {
            var configuration = await _configurationRepository.GetUserProfileConfiguration2Async();
            var adapter = _adapterRegistry.GetAdapter<Domain.Base.Entities.UserProfileConfiguration2, Dtos.Base.UserProfileConfiguration2>();
            var configurationDto = adapter.MapToType(configuration);
            return configurationDto;
        }

        /// <summary>
        /// Calls configuration repository and returns the Emergency Information Configuration data transfer object.
        /// </summary>
        /// <returns><see cref="Dtos.Base.EmergencyInformationConfiguration">Emergency Information Configuration</see> data transfer object.</returns>
        [Obsolete("Obsolete as of API 1.16. Use GetEmergencyInformationConfiguration2Async instead.")]
        public async Task<Dtos.Base.EmergencyInformationConfiguration> GetEmergencyInformationConfigurationAsync()
        {
            var configuration = await _configurationRepository.GetEmergencyInformationConfigurationAsync();
            var adapter = _adapterRegistry.GetAdapter<Domain.Base.Entities.EmergencyInformationConfiguration, Dtos.Base.EmergencyInformationConfiguration>();
            var configurationDto = adapter.MapToType(configuration);
            return configurationDto;
        }

        /// <summary>
        /// Calls configuration repository and returns the Emergency Information Configuration data transfer object.
        /// </summary>
        /// <returns><see cref="EmergencyInformationConfiguration2">Emergency Information Configuration</see> data transfer object.</returns>
        public async Task<Dtos.Base.EmergencyInformationConfiguration2> GetEmergencyInformationConfiguration2Async()
        {
            var configuration = await _configurationRepository.GetEmergencyInformationConfigurationAsync();
            var adapter = _adapterRegistry.GetAdapter<Domain.Base.Entities.EmergencyInformationConfiguration, Dtos.Base.EmergencyInformationConfiguration2>();
            var configurationDto = adapter.MapToType(configuration);
            return configurationDto;
        }

        /// <summary>
        /// Calls configuration repository and returns the Restriction Configuration data transfer object
        /// </summary>
        /// <returns><see cref="RestrictionConfiguration">Restriction Configuration</see></returns>
        public async Task<Dtos.Base.RestrictionConfiguration> GetRestrictionConfigurationAsync()
        {
            var configuration = await _configurationRepository.GetRestrictionConfigurationAsync();
            var adapter = _adapterRegistry.GetAdapter<Domain.Base.Entities.RestrictionConfiguration, Dtos.Base.RestrictionConfiguration>();
            var configurationDto = adapter.MapToType(configuration);
            return configurationDto;
        }

        /// <summary>
        /// Returns the privacy configuration
        /// </summary>
        /// <returns>Privacy Configuration DTO</returns>
        public async Task<Dtos.Base.PrivacyConfiguration> GetPrivacyConfigurationAsync()
        {
            var configuration = await _configurationRepository.GetPrivacyConfigurationAsync();
            var adapter = _adapterRegistry.GetAdapter<Domain.Base.Entities.PrivacyConfiguration, Dtos.Base.PrivacyConfiguration>();
            var configurationDto = adapter.MapToType(configuration);
            return configurationDto;
        }

        /// <summary>
        /// Retrieves the organizational relationship configuration
        /// </summary>
        /// <returns>Organizational relationship configuration DTO</returns>
        public async Task<Dtos.Base.OrganizationalRelationshipConfiguration> GetOrganizationalRelationshipConfigurationAsync()
        {
            var configuration = await _configurationRepository.GetOrganizationalRelationshipConfigurationAsync();
            var adapter = _adapterRegistry.GetAdapter<Domain.Base.Entities.OrganizationalRelationshipConfiguration, Dtos.Base.OrganizationalRelationshipConfiguration>();
            var configurationDto = adapter.MapToType(configuration);
            return configurationDto;
        }
    }
}