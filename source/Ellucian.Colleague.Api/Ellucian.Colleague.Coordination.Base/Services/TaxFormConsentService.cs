﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;

namespace Ellucian.Colleague.Coordination.Base.Services
{
    /// <summary>
    /// Service for tax form consents.
    /// </summary>
    [RegisterType]
    public class TaxFormConsentService : BaseCoordinationService, ITaxFormConsentService
    {
        private ITaxFormConsentRepository taxFormConsentRepository;

        /// <summary>
        /// Constructor TaxFormConsentService
        /// </summary>
        /// <param name="taxFormConsentRepository">TaxFormConsentRepository</param>
        /// <param name="adapterRegistry">AdapterRegistry</param>
        /// <param name="currentUserFactory">CurrentUserFactory</param>
        /// <param name="roleRepository">RoleRepository</param>
        /// <param name="logger">Logger</param>
        public TaxFormConsentService(ITaxFormConsentRepository taxFormConsentRepository, IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.taxFormConsentRepository = taxFormConsentRepository;
        }

        /// <summary>
        /// Get a set of TaxFormConsent DTOs for the specified person.
        /// </summary>
        /// <param name="personId">Person ID</param>
        /// <returns>Set of TaxFormConsent DTOs.</returns>
        public async Task<IEnumerable<Dtos.Base.TaxFormConsent>> GetAsync(string personId, Dtos.Base.TaxForms taxForm)
        {
            if (string.IsNullOrEmpty(personId))
                throw new ArgumentNullException("personId", "Person ID must be specified.");

            TaxForms taxFormDomain = new TaxForms();

            switch (taxForm)
            {
                case Dtos.Base.TaxForms.FormW2:
                    taxFormDomain = TaxForms.FormW2;
                    break;
                case Dtos.Base.TaxForms.Form1095C:
                    taxFormDomain = TaxForms.Form1095C;
                    break;
                case Dtos.Base.TaxForms.Form1098:
                    taxFormDomain = TaxForms.Form1098;
                    break;
            }

            var taxFormConsents = await this.taxFormConsentRepository.GetAsync(personId, taxFormDomain);
            var adapter = _adapterRegistry.GetAdapter<Domain.Base.Entities.TaxFormConsent, Dtos.Base.TaxFormConsent>();
            var taxFormConsentsDtos = new List<Dtos.Base.TaxFormConsent>();
            foreach (var item in taxFormConsents)
            {
                var dto = adapter.MapToType(item);
                taxFormConsentsDtos.Add(dto);
            }

            return taxFormConsentsDtos;
        }

        /// <summary>
        /// Create a new TaxFormConsent record.
        /// </summary>
        /// <param name="newTaxFormConsentDto">TaxFormConsent DTO</param>
        /// <returns>TaxFormConsent DTO</returns>
        public async Task<Dtos.Base.TaxFormConsent> PostAsync(Dtos.Base.TaxFormConsent newTaxFormConsentDto)
        {
            if (newTaxFormConsentDto == null)
                throw new ArgumentNullException("newTaxFormConsentDto", "newTaxFormConsentDto cannot be null.");

            var adapter = _adapterRegistry.GetAdapter<Dtos.Base.TaxFormConsent, Domain.Base.Entities.TaxFormConsent>();
            var newTaxFormConsentEntity = adapter.MapToType(newTaxFormConsentDto);
            var consentEntity = await taxFormConsentRepository.PostAsync(newTaxFormConsentEntity);

            return newTaxFormConsentDto;
        }
    }
}
