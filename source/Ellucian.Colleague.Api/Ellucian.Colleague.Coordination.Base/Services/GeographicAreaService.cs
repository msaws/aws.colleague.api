﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Coordination.Base.Adapters;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Data.Colleague.Repositories;

namespace Ellucian.Colleague.Coordination.Base.Services
{
    [RegisterType]
    public class GeographicAreaService : BaseCoordinationService, IGeographicAreaService
    {
        private readonly IReferenceDataRepository _referenceDataRepository;
        private readonly IPersonRepository _personRepository;
        private const string _dataOrigin = "Colleague";

        public GeographicAreaService(IAdapterRegistry adapterRegistry, IReferenceDataRepository referenceDataRepository,
                                         IPersonRepository personRepository,
                                         ICurrentUserFactory currentUserFactory,
                                         IRoleRepository roleRepository,
                                         ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            _referenceDataRepository = referenceDataRepository;
            _personRepository = personRepository;
        }

        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Gets all geographic areas
        /// </summary>
        /// <returns>Collection of GeographicArea DTO objects</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.GeographicArea>> GetGeographicAreasAsync(bool bypassCache = false)
        {
            var geographicAreaCollection = new List<Ellucian.Colleague.Dtos.GeographicArea>();

            var allGeographicAreaTypes = await _referenceDataRepository.GetGeographicAreaTypesAsync(false);
            var typeGuidFund = ConvertCodeToGuid(allGeographicAreaTypes, "FUND");
            var typeGuidPost = ConvertCodeToGuid(allGeographicAreaTypes, "POST");
            var typeGuidGov = ConvertCodeToGuid(allGeographicAreaTypes, "GOV");
            if (string.IsNullOrEmpty(typeGuidFund) && string.IsNullOrEmpty(typeGuidPost) && string.IsNullOrEmpty(typeGuidGov))
            {
                throw new KeyNotFoundException("Geographic Area Type is not populated.");
            }

            var chaptersEntities = await _referenceDataRepository.GetChaptersAsync(bypassCache);
            if (chaptersEntities != null && chaptersEntities.Count() > 0)
            {
                foreach (var chapter in chaptersEntities)
                {
                    geographicAreaCollection.Add(await ConvertChapterEntityToGeographicAreaDto(chapter, typeGuidFund));
                }
            }
            var countiesEntities = await _referenceDataRepository.GetCountiesAsync(bypassCache);
            if (countiesEntities != null && countiesEntities.Count() > 0)
            {
                foreach (var county in countiesEntities)
                {
                    geographicAreaCollection.Add(await ConvertCountyEntityToGeographicAreaDto(county, typeGuidGov));
                }
            }
            var zipcodeXlatsEntities = await _referenceDataRepository.GetZipCodeXlatAsync(bypassCache);
            if (zipcodeXlatsEntities != null && zipcodeXlatsEntities.Count() > 0)
            {
                foreach (var zipcodeXlat in zipcodeXlatsEntities)
                {
                    geographicAreaCollection.Add(await ConvertZipcodeXlatEntityToGeographicAreaDto(zipcodeXlat, typeGuidPost));
                }
            }
            return geographicAreaCollection;
        }

        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Get an geographic area from its GUID
        /// </summary>
        /// <returns>GeographicArea DTO object</returns>
        public async Task<Ellucian.Colleague.Dtos.GeographicArea> GetGeographicAreaByGuidAsync(string guid)
        {
            try
            {
                var geographicAreaCollection = new List<Ellucian.Colleague.Dtos.GeographicArea>();
                var allGeographicAreaTypes = await _referenceDataRepository.GetGeographicAreaTypesAsync(false);

                var entityType = await _referenceDataRepository.GetRecordInfoFromGuidGeographicAreaAsync(guid);
                string typeGuid;

                switch (entityType)
                {
                    case GeographicAreaTypeCategory.Fundraising:
                        typeGuid = ConvertCodeToGuid(allGeographicAreaTypes, "FUND");
                        var chaptersEntities = await _referenceDataRepository.GetChaptersAsync(true);
                        if (chaptersEntities != null && chaptersEntities.Count() > 0)
                        {
                            foreach (var chapter in chaptersEntities)
                            {
                                geographicAreaCollection.Add(await ConvertChapterEntityToGeographicAreaDto(chapter, typeGuid));
                            }
                        }
                        break;
                    case GeographicAreaTypeCategory.Governmental:
                        typeGuid = ConvertCodeToGuid(allGeographicAreaTypes, "GOV");
                        var countiesEntities = await _referenceDataRepository.GetCountiesAsync(true);
                        if (countiesEntities != null && countiesEntities.Count() > 0)
                        {
                            foreach (var county in countiesEntities)
                            {
                                geographicAreaCollection.Add(await ConvertCountyEntityToGeographicAreaDto(county, typeGuid));
                            }
                        }
                        break;
                    case GeographicAreaTypeCategory.Postal:
                        typeGuid = ConvertCodeToGuid(allGeographicAreaTypes, "POST");
                        var zipcodeXlatsEntities = await _referenceDataRepository.GetZipCodeXlatAsync(true);
                        if (zipcodeXlatsEntities != null && zipcodeXlatsEntities.Count() > 0)
                        {
                            foreach (var zipcodeXlat in zipcodeXlatsEntities)
                            {
                                geographicAreaCollection.Add(await ConvertZipcodeXlatEntityToGeographicAreaDto(zipcodeXlat, typeGuid));
                            }
                        }
                        break;
                }
                return geographicAreaCollection.Where(om => om.Id == guid).FirstOrDefault();
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException("Geographic area not found for GUID " + guid, ex);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Converts an Chapter domain entity to its corresponding GeographicArea DTO
        /// </summary>
        /// <param name="source">Chapter domain entity</param>
        /// <returns>GeographicArea DTO</returns>
        private async Task<Dtos.GeographicArea> ConvertChapterEntityToGeographicAreaDto(Chapter source, string typeGuid)
        {
            var geographicArea = new Dtos.GeographicArea();
            geographicArea.Id = source.Guid;
            geographicArea.Code = source.Code;
            geographicArea.Title = source.Description;
            geographicArea.Description = null;
            geographicArea.Type = new Dtos.GeographicAreaTypeProperty() 
            { 
                category = Dtos.GeographicAreaTypeCategory.Fundraising, 
                detail = new Dtos.GuidObject2() { Id = typeGuid} 
            };

            return geographicArea;
        }

        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Converts an ZipcodeXlat domain entity to its corresponding GeographicArea DTO
        /// </summary>
        /// <param name="source">ZipcodeXlat domain entity</param>
        /// <returns>GeographicArea DTO</returns>
        private async Task<Dtos.GeographicArea> ConvertZipcodeXlatEntityToGeographicAreaDto(ZipcodeXlat source, string typeGuid)
        {
            var geographicArea = new Dtos.GeographicArea();
            geographicArea.Id = source.Guid;
            geographicArea.Code = source.Code;
            geographicArea.Title = source.Description;
            geographicArea.Description = null;

            geographicArea.Type = new Dtos.GeographicAreaTypeProperty()
            {
                category = Dtos.GeographicAreaTypeCategory.Postal,
                detail = new Dtos.GuidObject2() { Id = typeGuid }
            };

            return geographicArea;
        }

        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Converts an County domain entity to its corresponding GeographicArea DTO
        /// </summary>
        /// <param name="source">County domain entity</param>
        /// <returns>GeographicArea DTO</returns>
        private async Task<Dtos.GeographicArea> ConvertCountyEntityToGeographicAreaDto(County source, string typeGuid)
        {
            var geographicArea = new Dtos.GeographicArea();
            geographicArea.Id = source.Guid;
            geographicArea.Code = source.Code;
            geographicArea.Title = source.Description;
            geographicArea.Description = null;

            geographicArea.Type = new Dtos.GeographicAreaTypeProperty()
            {
                category = Dtos.GeographicAreaTypeCategory.Governmental,
                detail = new Dtos.GuidObject2() { Id = typeGuid }
            };

            return geographicArea;
        }
    }
}
