﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Base.Services
{
    public interface IFacilitiesService
    {
        /// <summary>
        /// Gets all organizations
        /// </summary>
        /// <returns>Collection of Organization DTO objects</returns>
        [Obsolete("Obsolete as of HeDM Version 6, use GetOrganizations2Async instead.")]
        Task<IEnumerable<Ellucian.Colleague.Dtos.Organization>> GetOrganizationsAsync();

        /// <summary>
        /// Get an organization from its GUID
        /// </summary>
        /// <returns>Organization DTO object</returns>
        [Obsolete("Obsolete as of HeDM Version 6, use GetOrganization2Async instead.")]
        Task<Ellucian.Colleague.Dtos.Organization> GetOrganizationAsync(string guid);

        ///// <summary>
        ///// Gets all organizations
        ///// </summary>
        ///// <returns>Collection of Organization2 DTO objects</returns>
        //Task<IEnumerable<Ellucian.Colleague.Dtos.Organization2>> GetOrganizations2Async();

        ///// <summary>
        ///// Get an organization from its GUID
        ///// </summary>
        ///// <returns>Organization2 DTO object</returns>
        //Task<Ellucian.Colleague.Dtos.Organization2> GetOrganization2Async(string guid);

        /// <summary>
        /// Gets all sites
        /// </summary>
        /// <returns>Collection of Site DTO objects</returns>
        [Obsolete("Obsolete as of HeDM Version 4, use GetSites2Async instead.")]
        Task<IEnumerable<Ellucian.Colleague.Dtos.Site>> GetSitesAsync(bool bypassCache);

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Get a site from its GUID
        /// </summary>
        /// <returns>Site DTO object</returns>
        [Obsolete("Obsolete as of HeDM Version 4, use GetSite2Async instead.")]
        Task<Ellucian.Colleague.Dtos.Site> GetSiteAsync(string guid);

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Gets all sites
        /// </summary>
        /// <returns>Collection of Site DTO objects</returns>
       Task<IEnumerable<Ellucian.Colleague.Dtos.Site2>> GetSites2Async(bool bypassCache);

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Get a site from its ID
        /// </summary>
        /// <returns>Site DTO object</returns>
        Task<Ellucian.Colleague.Dtos.Site2> GetSite2Async(string id);

        /// <summary>
        /// Gets all buildings
        /// </summary>
        /// <returns>Collection of Building DTO objects</returns>
        Task<IEnumerable<Ellucian.Colleague.Dtos.Building>> GetBuildingsAsync();

        /// <summary>
        /// Get a building from its GUID
        /// </summary>
        /// <returns>Building DTO object</returns>
        Task<Ellucian.Colleague.Dtos.Building> GetBuildingAsync(string guid);

        /// <summary>
        /// Gets all buildings
        /// </summary>
        /// <returns>Collection of Building DTO objects</returns>
        Task<IEnumerable<Ellucian.Colleague.Dtos.Building2>> GetBuildings2Async(bool bypassCache);

        /// <summary>
        /// Get a building from its GUID
        /// </summary>
        /// <returns>Building DTO object</returns>
        Task<Ellucian.Colleague.Dtos.Building2> GetBuilding2Async(string guid);

        /// <summary>
        /// Gets all building wings
        /// </summary>
        /// <returns>Collection of Building Wing DTO objects</returns>
        Task<IEnumerable<Ellucian.Colleague.Dtos.BuildingWing>> GetBuildingWingsAsync(bool bypassCache);

        /// <summary>
        /// Get a building wing from its GUID
        /// </summary>
        /// <returns>Building DTO object</returns>
        Task<Ellucian.Colleague.Dtos.BuildingWing> GetBuildingWingsByGuidAsync(string guid);

        /// <summary>
        /// Gets all rooms
        /// </summary>
        /// <returns>Collection of Room DTO objects</returns>
        [Obsolete("Obsolete as of HeDM Version 4, use GetRooms2Async instead.")]
        Task<IEnumerable<Ellucian.Colleague.Dtos.Room>> GetRoomsAsync(bool bypassCache);

        /// <summary>
        /// Gets all rooms
        /// </summary>
        /// <returns>Collection of Room DTO objects</returns>
        [Obsolete("Obsolete as of HeDM Version 6, use GetRooms3Async instead.")]
        Task<IEnumerable<Ellucian.Colleague.Dtos.Room2>> GetRooms2Async(bool bypassCache);

        /// <summary>
        /// Gets all rooms
        /// </summary>
        /// <returns>Collection of Room DTO objects</returns>
        Task<Tuple<IEnumerable<Ellucian.Colleague.Dtos.Room3>, int>> GetRooms3Async(int offset, int limit, bool bypassCache = false);

        /// <summary>
        /// Get a room from its GUID
        /// </summary>
        /// <returns>Room DTO object</returns>
        [Obsolete("Obsolete as of HeDM Version 4, use GetRoomById2Async instead.")]
        Task<Ellucian.Colleague.Dtos.Room> GetRoomByGuidAsync(string guid);

        /// <summary>
        /// Get a room from its GUID
        /// </summary>
        /// <returns>Room DTO object</returns>
         [Obsolete("Obsolete as of HeDM Version 6, use GetRoomById3Async instead.")]
        Task<Ellucian.Colleague.Dtos.Room2> GetRoomById2Async(string guid);

        /// <summary>
        /// Get a room from its GUID
        /// </summary>
        /// <returns>Room DTO object</returns>
        Task<Ellucian.Colleague.Dtos.Room3> GetRoomById3Async(string guid);


        /// <summary>
        /// Check for room availability for a given date range, start and end time, and frequency
        /// </summary>
        /// <param name="request">Room availability request</param>
        /// <returns>Collection of Room DTO objects</returns>
        [Obsolete("Obsolete as of HeDM Version 4, use CheckRoomAvailability2Async instead.")]
        Task<IEnumerable<Ellucian.Colleague.Dtos.Room>> CheckRoomAvailabilityAsync(Dtos.RoomsAvailabilityRequest request);

        /// <summary>
        /// Check for room availability for a given date range, start and end time, and frequency
        /// </summary>
        /// <param name="request">Room availability request</param>
        /// <returns>Collection of Room DTO objects</returns>
        Task<IEnumerable<Ellucian.Colleague.Dtos.Room2>> CheckRoomAvailability2Async(Dtos.RoomsAvailabilityRequest2 request);

        /// <summary>
        /// Check for room availability for a given date range, start and end time, and frequency
        /// </summary>
        /// <param name="request">Room availability request</param>
        /// <returns>Collection of Room DTO objects</returns>
        Task<IEnumerable<Ellucian.Colleague.Dtos.Room2>> CheckRoomAvailability3Async(Dtos.RoomsAvailabilityRequest2 request);

        /// <summary>
        /// Check for room availability for a given date range, start and end time, and frequency, and return minimal room object 
        /// </summary>
        /// <returns>RoomsMinimumResponse DTO object</returns>
        Task<IEnumerable<Ellucian.Colleague.Dtos.RoomsMinimumResponse>> GetRoomsMinimumAsync(Dtos.RoomsAvailabilityRequest2 request);  
    }
}