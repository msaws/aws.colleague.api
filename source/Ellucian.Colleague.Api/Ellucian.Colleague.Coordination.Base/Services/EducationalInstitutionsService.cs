﻿// Copyright 2016 - 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Web.Dependency;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Adapters;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Dmi.Runtime;
using Ellucian.Colleague.Dtos.DtoProperties;
using Ellucian.Colleague.Dtos;
using Ellucian.Colleague.Dtos.EnumProperties;
using Ellucian.Data.Colleague;

namespace Ellucian.Colleague.Coordination.Base.Services
{
    [RegisterType]
    public class EducationalInstitutionsService : BaseCoordinationService, IEducationalInstitutionsService
    {
        private readonly IReferenceDataRepository _referenceDataRepository;
        private readonly IInstitutionRepository _institutionRepository;
        private readonly IPersonRepository _personRepository;
        private readonly IConfigurationRepository _configurationRepository;
        private readonly IAdapterRegistry _iAdapterRegistry;
        private readonly ILogger _logger;
        public static char _SM = Convert.ToChar(DynamicArray.SM);

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="referenceDataRepository"></param>
        /// <param name="personRepository"></param>
        /// <param name="institutionRepository"></param>
        /// <param name="configurationRepository"></param>
        /// <param name="iAdapterRegistry"></param>
        /// <param name="logger"></param>
        public EducationalInstitutionsService(IReferenceDataRepository referenceDataRepository, IPersonRepository personRepository,
            IInstitutionRepository institutionRepository, IConfigurationRepository configurationRepository, IAdapterRegistry iAdapterRegistry,
            ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger)
            : base(iAdapterRegistry, currentUserFactory, roleRepository, logger, configurationRepository: configurationRepository)
        {
            _referenceDataRepository = referenceDataRepository;
            _institutionRepository = institutionRepository;
            _personRepository = personRepository;
            _configurationRepository = configurationRepository;
            _iAdapterRegistry = iAdapterRegistry;
            _logger = logger;
        }

        /// <summary>
        /// get page of educational institutions
        /// </summary>
        /// <param name="offset">item number to start at</param>
        /// <param name="limit">number of items to return on page</param>
        /// <param name="ignoreCache"></param>
        /// <returns></returns>
        public async Task<Tuple<IEnumerable<EducationalInstitution>, int>> GetEducationalInstitutionsAsync(int offset, int limit, bool ignoreCache = false)
        {
            var institutions = await _institutionRepository.GetAllInstitutionsAsync(ignoreCache);
            var totalCount = institutions.Count();

            if (totalCount > 0)
            {
                var subInstitutionList = institutions.Skip(offset).Take(limit).ToList();

                var convertedInstitutionList = await ConvertInstitutionEntityListToEducationInsitutionDtoList(subInstitutionList);

                return new Tuple<IEnumerable<EducationalInstitution>, int>(convertedInstitutionList, totalCount);
            }
            else
            {
                return new Tuple<IEnumerable<EducationalInstitution>, int>(new List<EducationalInstitution>(), totalCount);
            }
        }

        /// <summary>
        /// get page of educational institutions by type
        /// </summary>
        /// <param name="offset">item number to start at</param>
        /// <param name="limit">number of items to return on page</param>
        /// <param name="type"></param>
        /// <param name="ignoreCache"></param>
        /// <returns></returns>
        public async Task<Tuple<IEnumerable<EducationalInstitution>, int>> GetEducationalInstitutionsByTypeAsync(int offset, int limit, Dtos.EnumProperties.EducationalInstitutionType type, bool ignoreCache = false)
        {
            InstType typeFilter = InstType.Unknown;

            switch (type)
            {
                case EducationalInstitutionType.PostSecondarySchool:
                    typeFilter = InstType.College;
                    break;
                case EducationalInstitutionType.SecondarySchool:
                    typeFilter = InstType.HighSchool;
                    break;
            }

            var institutions = await _institutionRepository.GetAllInstitutionsAsync(ignoreCache);
            var filteredInstitutions = institutions.Where(i => i.InstitutionType == typeFilter).OrderBy(i => i.Id).ToList();
            var totalCount = filteredInstitutions.Count();

            if (totalCount > 0)
            {
                var subInstitutionList = filteredInstitutions.Skip(offset).Take(limit).ToList();

                var convertedInstitutionList = await ConvertInstitutionEntityListToEducationInsitutionDtoList(subInstitutionList);

                return new Tuple<IEnumerable<EducationalInstitution>, int>(convertedInstitutionList, totalCount);
            }
            else
            {
                return new Tuple<IEnumerable<EducationalInstitution>, int>(new List<EducationalInstitution>(), totalCount);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<EducationalInstitution> GetEducationalInstitutionByGuidAsync(string id)
        {
            var guidInfo = await _referenceDataRepository.GetGuidLookupResultFromGuidAsync(id);

            if (guidInfo != null && !string.Equals(guidInfo.Entity, "PERSON", StringComparison.OrdinalIgnoreCase))
            {
                throw new KeyNotFoundException(string.Concat("The educational-institutions with the ID  ", id, " was not found"));
            }

            var institutions = await _institutionRepository.GetAllInstitutionsAsync(true);

            var institution = institutions.Where(i => i.Id == guidInfo.PrimaryKey).FirstOrDefault();

            if (institution == null)
            {
                throw new KeyNotFoundException(string.Concat("The educational-institutions with the ID  ", id, " was not found")); 
            }
            
            var returnedList = await ConvertInstitutionEntityListToEducationInsitutionDtoList(new List<Institution>(){ institution });

            if (returnedList.Any())
            {
                return returnedList.First();
            }
            else
            {
                throw new Exception(string.Concat("Data conversion for educational-institutions with the ID  ", id, " failed"));
            }
        }

        /// <summary>
        /// Convert ienumerable of institution entities to ienumerable of educational-institutions dtos 
        /// </summary>
        /// <param name="institutions">ienumerable of institution entities</param>
        /// <returns>ienumerable of educational-institutions dtos</returns>
        private async Task<IEnumerable<EducationalInstitution>> ConvertInstitutionEntityListToEducationInsitutionDtoList(IEnumerable<Institution> institutions)
        {
            var returnEducationInsitutionList = new List<EducationalInstitution>();
            bool errorsThrown = false;
            var errorStringBuilder = new StringBuilder();
            //email types for use in conversion adapter
            var emailTypes = await _referenceDataRepository.GetEmailTypesAsync(false);
            var emailAdapter = new PersonIntgEmailAddressEntityToDtoAdapter(_iAdapterRegistry, _logger);

            var socialMediaTypesList = await _referenceDataRepository.GetSocialMediaTypesAsync(false);
            var socialMediaAdapter = _iAdapterRegistry.GetAdapter<Tuple<IEnumerable<Domain.Base.Entities.SocialMedia>, IEnumerable<Domain.Base.Entities.SocialMediaType>>,
                        IEnumerable<Dtos.DtoProperties.PersonSocialMediaDtoProperty>>();

            foreach (var institution in institutions)
            {
                var returnEducationInsitution = new EducationalInstitution();

                try
                {
                    returnEducationInsitution.Id = await _personRepository.GetPersonGuidFromIdAsync(institution.Id);
                    returnEducationInsitution.Title = institution.Name;
                    switch (institution.InstitutionType)
                    {
                        case InstType.College:
                            returnEducationInsitution.Type = EducationalInstitutionType.PostSecondarySchool;
                            break;
                        case InstType.HighSchool:
                            returnEducationInsitution.Type = EducationalInstitutionType.SecondarySchool;
                            break;
                        default:
                            throw new Exception(string.Concat("The educational-institution with the ID ", returnEducationInsitution.Id, " does not have a valid type."));
                    }

                    var homeInstitutionsList = await _referenceDataRepository.GetHomeInstitutionIdList();
                    if (homeInstitutionsList.Contains(institution.Id))
                    {
                        returnEducationInsitution.HomeInstitution = HomeInstitutionType.Home;
                    }
                    else
                    {
                        returnEducationInsitution.HomeInstitution = HomeInstitutionType.External;
                    }

                    if (!string.IsNullOrEmpty(institution.Ceeb))
                    {
                        returnEducationInsitution.StandardizedCodes = new List<StandardizedCodesDtoProperty>()
                        {
                            new StandardizedCodesDtoProperty()
                            {
                                Country = new StandardizedCeebCodesDtoProperty()
                                {
                                    Ceeb = institution.Ceeb
                                }
                            }
                        };
                    }

                    var tuplePerson = await _personRepository.GetPersonIntegrationData2Async(institution.Id);
                    var emailEntities = tuplePerson.Item1;
                    var phoneEntities = tuplePerson.Item2;
                    var addressEntities = tuplePerson.Item3;
                    var socialMediaEntities = tuplePerson.Item4;

                    var emailDtoReturn = emailAdapter.MapToType(emailEntities, emailTypes);
                    if (emailDtoReturn != null && emailDtoReturn.Any())
                    {
                        returnEducationInsitution.EmailAddresses = emailDtoReturn;
                    }

                    var addressDtoReturn = await GetAddressesAsync(addressEntities);
                    if (addressDtoReturn != null && addressDtoReturn.Any())
                    {
                        returnEducationInsitution.Addresses = addressDtoReturn;
                    }

                    var phoneDtoReturn = await GetPhonesAsync(phoneEntities);
                    if (phoneDtoReturn != null && phoneDtoReturn.Any())
                    {
                        returnEducationInsitution.Phones = phoneDtoReturn;
                    }

                    //var socialMediaDtoReturn = await GetPersonSocialMediaAsync(socialMediaEntities);

                    var socialTuple = new Tuple <IEnumerable<Domain.Base.Entities.SocialMedia>, IEnumerable<Domain.Base.Entities.SocialMediaType>>
                        (socialMediaEntities, socialMediaTypesList);

                    var socialMediaDtoReturn = socialMediaAdapter.MapToType(socialTuple);
                    if (socialMediaDtoReturn != null && socialMediaDtoReturn.Any())
                    {
                        returnEducationInsitution.SocialMedia = socialMediaDtoReturn;
                    }

                    returnEducationInsitutionList.Add(returnEducationInsitution);
                }
                catch (Exception exception)
                {
                    errorStringBuilder.Append(exception.Message);
                    errorStringBuilder.AppendLine();
                    errorsThrown = true;
                }
            }

            if (errorsThrown)
            {
                throw new Exception(errorStringBuilder.ToString());
            }

            return returnEducationInsitutionList;
        }

        //private async Task<IEnumerable<Dtos.DtoProperties.PersonSocialMediaDtoProperty>> GetPersonSocialMediaAsync(List<Ellucian.Colleague.Domain.Base.Entities.SocialMedia> mediaTypes)
        //{
        //    List<Dtos.DtoProperties.PersonSocialMediaDtoProperty> socialMediaEntries = new List<Dtos.DtoProperties.PersonSocialMediaDtoProperty>();

        //    foreach (var mediaType in mediaTypes)
        //    {
        //        try
        //        {
        //            var socialMedia = new Dtos.DtoProperties.PersonSocialMediaDtoProperty();
        //            if (mediaType.TypeCode.ToLowerInvariant() == "website")
        //            {
        //                string guid = "";
        //                var socialMediaEntity = (await _referenceDataRepository.GetSocialMediaTypesAsync(false)).FirstOrDefault(ic => ic.Type.ToString() == mediaType.TypeCode);
        //                if (socialMediaEntity != null)
        //                {
        //                    guid = socialMediaEntity.Guid;
        //                    socialMedia = new Dtos.DtoProperties.PersonSocialMediaDtoProperty()
        //                    {
        //                        Type = new Dtos.DtoProperties.PersonSocialMediaType()
        //                        {
        //                            Category = (Dtos.SocialMediaTypeCategory)Enum.Parse(typeof(Dtos.SocialMediaTypeCategory), mediaType.TypeCode.ToString()),
        //                            Detail = new Dtos.GuidObject2(guid)
        //                        },
        //                        Address = mediaType.Handle
        //                    };
        //                }
        //                else
        //                {
        //                    socialMedia = new Dtos.DtoProperties.PersonSocialMediaDtoProperty()
        //                    {
        //                        Type = new Dtos.DtoProperties.PersonSocialMediaType()
        //                        {
        //                            Category = (Dtos.SocialMediaTypeCategory)Enum.Parse(typeof(Dtos.SocialMediaTypeCategory), mediaType.TypeCode.ToString())
        //                        },
        //                        Address = mediaType.Handle
        //                    };
        //                }
        //            }
        //            else
        //            {
        //                var socialMediaEntity = (await _referenceDataRepository.GetSocialMediaTypesAsync(false)).FirstOrDefault(ic => ic.Code == mediaType.TypeCode);
        //                socialMedia = new Dtos.DtoProperties.PersonSocialMediaDtoProperty()
        //                {
        //                    Type = new Dtos.DtoProperties.PersonSocialMediaType()
        //                    {
        //                        Category = (Dtos.SocialMediaTypeCategory)Enum.Parse(typeof(Dtos.SocialMediaTypeCategory), socialMediaEntity.Type.ToString()),
        //                        Detail = new Dtos.GuidObject2(socialMediaEntity.Guid)
        //                    },
        //                    Address = mediaType.Handle
        //                };
        //            }
        //            if (mediaType.IsPreferred) socialMedia.Preference = Dtos.EnumProperties.PersonPreference.Primary;
        //            if (!string.IsNullOrEmpty(mediaType.Status) && mediaType.Status.ToLowerInvariant() == "inactive")
        //            {
        //                socialMedia.Status = Dtos.EnumProperties.PersonActiveInactiveStatus.Inactive;
        //            }
        //            else
        //            {
        //                socialMedia.Status = Dtos.EnumProperties.PersonActiveInactiveStatus.Active;
        //            }

        //            socialMediaEntries.Add(socialMedia);
        //        }
        //        catch
        //        {
        //            // Do not include code since we couldn't find a category
        //        }
        //    }

        //    return socialMediaEntries;
        //}

        private async Task<IEnumerable<Dtos.DtoProperties.PersonPhoneDtoProperty>> GetPhonesAsync(IEnumerable<Domain.Base.Entities.Phone> phoneEntities)
        {
            var phoneDtos = new List<Dtos.DtoProperties.PersonPhoneDtoProperty>();
            if (phoneEntities != null && phoneEntities.Count() > 0)
            {
                foreach (var phoneEntity in phoneEntities)
                {
                    string guid = "";
                    string category = "";
                    try
                    {
                        var phoneTypeEntity = (await _referenceDataRepository.GetPhoneTypesAsync(false)).FirstOrDefault(pt => pt.Code == phoneEntity.TypeCode);
                        guid = phoneTypeEntity.Guid;
                        category = phoneTypeEntity.PhoneTypeCategory.ToString();

                        var phoneDto = new Dtos.DtoProperties.PersonPhoneDtoProperty()
                        {
                            Number = phoneEntity.Number,
                            Extension = string.IsNullOrEmpty(phoneEntity.Extension) ? null : phoneEntity.Extension,
                            Type = new Dtos.DtoProperties.PersonPhoneTypeDtoProperty()
                            {
                                PhoneType = (Dtos.EnumProperties.PersonPhoneTypeCategory)Enum.Parse(typeof(Dtos.EnumProperties.PersonPhoneTypeCategory), category),
                                Detail = string.IsNullOrEmpty(guid) ? null : new Dtos.GuidObject2(guid)
                            },
                            CountryCallingCode = phoneEntity.CountryCallingCode
                        };
                        if (phoneEntity.IsPreferred) phoneDto.Preference = Dtos.EnumProperties.PersonPreference.Primary;
                        
                        phoneDtos.Add(phoneDto);
                    }
                    catch
                    {
                        // do not fail if we can't find a guid from the code table or category
                        // Just exclude the phone number from the output.
                    }
                }
            }
            return phoneDtos;
        }

        //private async Task<IEnumerable<Dtos.DtoProperties.PersonEmailDtoProperty>> GetEmailAddresses(IEnumerable<Domain.Base.Entities.EmailAddress> emailAddressEntities)
        //{
        //    var emailAddressDtos = new List<Dtos.DtoProperties.PersonEmailDtoProperty>();
        //    if (emailAddressEntities != null && emailAddressEntities.Count() > 0)
        //    {
        //        foreach (var emailAddressEntity in emailAddressEntities)
        //        {
        //            string guid = "";
        //            string category = "";
        //            try
        //            {
        //                var codeItem = (await _referenceDataRepository.GetEmailTypesAsync(false)).FirstOrDefault(pt => pt.Code == emailAddressEntity.TypeCode);
        //                guid = codeItem.Guid;
        //                category = codeItem.EmailTypeCategory.ToString();

        //                var addressDto = new Dtos.DtoProperties.PersonEmailDtoProperty()
        //                {
        //                    Type = new Dtos.DtoProperties.PersonEmailTypeDtoProperty()
        //                    {
        //                        EmailType = (Dtos.EmailTypeList)Enum.Parse(typeof(Dtos.EmailTypeList), category),
        //                        Detail = string.IsNullOrEmpty(guid) ? null : new Dtos.GuidObject2(guid)
        //                    },
        //                    Address = emailAddressEntity.Value
        //                };
        //                if (emailAddressEntity.IsPreferred) addressDto.Preference = Dtos.EnumProperties.PersonEmailPreference.PrimaryOverall;
        //                if (!string.IsNullOrEmpty(emailAddressEntity.Status) && emailAddressEntity.Status.ToLowerInvariant() == "active")
        //                {
        //                    addressDto.Status = Dtos.EnumProperties.PersonActiveInactiveStatus.Active;
        //                }
        //                else
        //                {
        //                    addressDto.Status = Dtos.EnumProperties.PersonActiveInactiveStatus.Inactive;
        //                }

        //                emailAddressDtos.Add(addressDto);
        //            }
        //            catch
        //            {
        //                // do not fail if we can't find a guid from the code table or translate the cateory
        //                // Just exclude this email address.
        //            }
        //        }
        //    }
        //    return emailAddressDtos;
        //}

        private async Task<IEnumerable<Dtos.DtoProperties.PersonAddressDtoProperty>> GetAddressesAsync(IEnumerable<Domain.Base.Entities.Address> addressEntities)
        {
            var addressDtos = new List<Dtos.DtoProperties.PersonAddressDtoProperty>();
            if (addressEntities != null && addressEntities.Count() > 0)
            {
                foreach (var addressEntity in addressEntities)
                {
                    if (addressEntity != null && addressEntity.TypeCode != null && !string.IsNullOrEmpty(addressEntity.TypeCode))
                    {
                        // Repeate the address when we have multiple types.
                        // Multiple types are separated by sub-value marks.
                        var addressTypes = await _referenceDataRepository.GetAddressTypes2Async(false);
                        string[] addrTypes = addressEntity.TypeCode.Split(_SM);
                        for (int i = 0; i < addrTypes.Length; i++)
                        {
                            var addrType = addrTypes[i];
                            var addressDto = new Dtos.DtoProperties.PersonAddressDtoProperty();
                            addressDto.address = new Dtos.PersonAddress() { Id = addressEntity.Guid };
                            var type = addressTypes.FirstOrDefault(at => at.Code == addrType);
                            if (type != null)
                            {
                                addressDto.Type = new Dtos.DtoProperties.PersonAddressTypeDtoProperty();
                                addressDto.Type.AddressType =
                                    (Dtos.EnumProperties.AddressType)
                                        Enum.Parse(typeof(Dtos.EnumProperties.AddressType),
                                            type.AddressTypeCategory.ToString());
                                addressDto.Type.Detail = new Dtos.GuidObject2(type.Guid);
                            }
                            if (addressEntity.IsPreferredResidence && i == 0)
                                addressDto.Preference = Dtos.EnumProperties.PersonPreference.Primary;
                            addressDto.AddressEffectiveStart = addressEntity.EffectiveStartDate;
                            addressDto.AddressEffectiveEnd = addressEntity.EffectiveEndDate;

                            addressDtos.Add(addressDto);
                        }
                    }
                }
            }
            return addressDtos;
        }
       
    }
}
