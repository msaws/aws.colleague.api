﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Base.Services
{
    /// <summary>
    /// Interface for EducationalInstitutionUnits services
    /// </summary>
    public interface IEducationalInstitutionsService : IBaseService
    {
        Task<Tuple<IEnumerable<Ellucian.Colleague.Dtos.EducationalInstitution>, int>> GetEducationalInstitutionsAsync(int offset, int limit, bool ignoreCache = false);
        Task<Tuple<IEnumerable<Ellucian.Colleague.Dtos.EducationalInstitution>, int>> GetEducationalInstitutionsByTypeAsync(int offset, int limit, Dtos.EnumProperties.EducationalInstitutionType type, bool ignoreCache = false);
        Task<Ellucian.Colleague.Dtos.EducationalInstitution> GetEducationalInstitutionByGuidAsync(string id);
    }
}
