﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Base.Services
{
    /// <summary>
    /// Interface for Buyers services
    /// </summary>
    public interface IBuyersService
    {
        Task<Tuple<IEnumerable<Ellucian.Colleague.Dtos.Buyers>, int>> GetBuyersAsync(int offset, int limit, bool bypassCache);

        Task<Ellucian.Colleague.Dtos.Buyers> GetBuyersByGuidAsync(string id);
    }
}
