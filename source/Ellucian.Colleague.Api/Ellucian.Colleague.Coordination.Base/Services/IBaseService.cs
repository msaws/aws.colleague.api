﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Base.Services
{
    public interface IBaseService
    {
        /// <summary>
        /// Check if the json content for the update contains any properties protected by data privacy settings and throw an exception if so.
        /// </summary>
        /// <param name="apiName">name of the api to check (eedm schema name)</param>
        /// <param name="jsonUpdateBody">object being updated </param>
        /// <param name="bypassCache">bool on if to bypass the cache or not, true by default</param>
        /// <returns>false if dataprivacy is not violated, throws a permission exception if it is</returns>
        Task<bool> DoesUpdateViolateDataPrivacySettings(string apiName, object jsonUpdateBody, bool bypassCache = true);

        /// <summary>
        /// Gets the list of EEDM data privacy settings by user based on user, roles and permissions
        /// </summary>
        /// <param name="apiName">name of the api (eedm schema name)</param>
        /// <param name="bypassCache"></param>
        /// <returns>list of data privacy strings to apply</returns>
        Task<IEnumerable<string>> GetDataPrivacyListByApi(string apiName, bool bypassCache = false);
    }
}
