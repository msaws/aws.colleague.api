﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;

namespace Ellucian.Colleague.Domain.ResidenceLife.Tests.Entities
{
    [TestClass]
    public class MealPlanAssignmentTests
    {
        [TestClass]
        public class BasicSettingAndRetrievalOfProperties
        {
            private string id;
            private List<ResidenceLifeExternalId> externalIds = new List<ResidenceLifeExternalId>();
            private string personId;
            // Only the meal plan ID is persisted to the data store. But the MealPlan entity is populated in the object because
            // its attributes are used for business logic.
            private MealPlan mealPlan;
            private int numberOfRatePeriods;
            private DateTime startDate;
            private DateTime? endDate;
            // Only the Current Status code is persisted to the data store. But the MealPlanAssignmentStatus entity is populated in the 
            // object because its attributes are used for business logic.
            private MealPlanAssignmentStatus currentStatus;
            private DateTime currentStatusDate;
            private string term;
            private int? usedRatePeriods;
            private int? usedPercent;
            private string cardNumber;
            private decimal? overrideRate;
            private string overrideRateReason;
            private string overrideChargeCode;
            private string overrideReceivableType;
            private string overrideRefundFormula;
            private string comments;

            private MealPlanAssignment mpa;

            [TestInitialize]
            public void Initialize()
            {
                id = "id12344";
                externalIds = new List<ResidenceLifeExternalId>() { new ResidenceLifeExternalId("EXTL1", "HD"), new ResidenceLifeExternalId("EXTL2", "RMS")};
                personId = "personid222";
                mealPlan = new MealPlan( new DateTime(2010,1,4), "a meal plan");
                mealPlan.Id = "anId";
                numberOfRatePeriods = 2;
                startDate = new DateTime(2014, 1, 1);
                endDate = new DateTime(2014,5,31);
                currentStatus = new MealPlanAssignmentStatus("A", "Active", MealPlanAssignmentStatusType.None);
                currentStatusDate = new DateTime(2014, 8, 10);
                term = "aterm";
                usedRatePeriods = 2;
                usedPercent = 30;
                cardNumber = "cardnum333";
                overrideRate = 45.55M;
                overrideRateReason = "OvrReason";
                overrideChargeCode = "CHG1";
                overrideReceivableType = "RCV";
                overrideRefundFormula = "Formula";
                comments = "Comments and more comments.";

                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                             currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
                mpa.Id = id;
                mpa.CardNumber = cardNumber;
                mpa.SetOverrideRate(overrideRate, overrideRateReason);
                mpa.OverrideChargeCode = overrideChargeCode;
                mpa.OverrideReceivableType = overrideReceivableType;
                mpa.OverrideRefundFormula = overrideRefundFormula;
                mpa.Comments = comments;
                foreach (ResidenceLifeExternalId eid in externalIds)
                {
                    mpa.AddExternalId(eid.ExternalId, eid.ExternalIdSource);
                }
            }            

            [TestMethod]
            public void Id()
            {
                Assert.AreEqual(id, mpa.Id);
            }

            [TestMethod]
            public void PersonId()
            {
                Assert.AreEqual(personId, mpa.PersonId);
            }

            [TestMethod]
            public void MealPlanId()
            {
                Assert.AreEqual(mealPlan.Id, mpa.MealPlanId);
            }

            [TestMethod]
            public void NumberOfRatePeriods()
            {
                Assert.AreEqual(numberOfRatePeriods, mpa.NumberOfRatePeriods);
            }

            [TestMethod]
            public void StartDate()
            {
                Assert.AreEqual(startDate, mpa.StartDate);
            }

            [TestMethod]
            public void EndDate()
            {
                Assert.AreEqual(endDate, mpa.EndDate);
            }

            [TestMethod]
            public void CurrentStatus()
            {
                Assert.AreEqual(currentStatus.Code, mpa.CurrentStatusCode);
            }

            [TestMethod]
            public void CurrentStatusDate()
            {
                Assert.AreEqual(currentStatusDate, mpa.CurrentStatusDate);
            }

            [TestMethod]
            public void Term()
            {
                Assert.AreEqual(term, mpa.TermId);
            }

            [TestMethod]
            public void UsedRatePeriods()
            {
                Assert.AreEqual(usedRatePeriods, mpa.UsedRatePeriods);
            }

            [TestMethod]
            public void UsedPercent()
            {
                Assert.AreEqual(usedPercent, mpa.UsedPercent);
            }

            [TestMethod]
            public void Card()
            {
                Assert.AreEqual(cardNumber, mpa.CardNumber);
            }

            [TestMethod]
            public void OverrideRate()
            {
                Assert.AreEqual(overrideRate, mpa.OverrideRate);
            }

            [TestMethod]
            public void OverrideRateReason()
            {
                Assert.AreEqual(overrideRateReason, mpa.OverrideRateReason);
            }

            [TestMethod]
            public void OverrideChargeCode()
            {
                Assert.AreEqual(overrideChargeCode, mpa.OverrideChargeCode);
            }

            [TestMethod]
            public void OverrideReceivableType()
            {
                Assert.AreEqual(overrideReceivableType, mpa.OverrideReceivableType);
            }

            [TestMethod]
            public void OverrideRefundFormula()
            {
                Assert.AreEqual(overrideRefundFormula, mpa.OverrideRefundFormula);
            }

            [TestMethod]
            public void Comments()
            {
                Assert.AreEqual(comments, mpa.Comments);
            }

            [TestMethod]
            public void ExternalIds()
            {
                Assert.AreEqual(externalIds.Count, mpa.ExternalIds.Count);
                for (int i = 0; i < externalIds.Count; i++)
                {
                    Assert.AreEqual(externalIds[i].ExternalId, mpa.ExternalIds[i].ExternalId);
                    Assert.AreEqual(externalIds[i].ExternalIdSource, mpa.ExternalIds[i].ExternalIdSource);
                }
            }
        }

        [TestClass]
        public class ValidationTests
        {
            private string id;
            private List<ResidenceLifeExternalId> externalIds = new List<ResidenceLifeExternalId>();
            private string personId;
            // Only the meal plan ID is persisted to the data store. But the MealPlan entity is populated in the object because
            // its attributes are used for business logic.
            private MealPlan mealPlan;
            private DateTime startDate;
            private DateTime? endDate;
            private string cardNumber;
            private string term;
            private int numberOfRatePeriods;
            private int? usedRatePeriods;
            private int? usedPercent;
            private decimal? overrideRate;
            private string overrideRateReason;
            private string overrideChargeCode;
            private string overrideReceivableType;
            private string overrideRefundFormula;
            private string comments;
            // Only the Current Status code is persisted to the data store. But the MealPlanAssignmentStatus entity is populated in the 
            // object because its attributes are used for business logic.
            private MealPlanAssignmentStatus currentStatus;
            private DateTime currentStatusDate;

            private MealPlanAssignment mpa;

            [TestInitialize]
            public void Initialize()
            {
                id = "id12344";
                externalIds = new List<ResidenceLifeExternalId>() { new ResidenceLifeExternalId("EXTL1", "HD"), new ResidenceLifeExternalId("EXTL2", "RMS") };
                personId = "personid222";
                mealPlan = new MealPlan(new DateTime(2010, 1, 4), "a meal plan");
                mealPlan.Id = "anId";
                startDate = new DateTime(2014, 1, 1);
                endDate = new DateTime(2014, 5, 31);
                cardNumber = "cardnum333";
                term = "aterm";
                numberOfRatePeriods = 2;
                usedRatePeriods = 2;
                usedPercent = 30;
                overrideRate = 45.55M;
                overrideRateReason = "OvrReason";
                overrideChargeCode = "CHG1";
                overrideReceivableType = "RCV";
                overrideRefundFormula = "Formula";
                comments = "Comments and more comments.";
                currentStatus = new MealPlanAssignmentStatus("A", "Active", MealPlanAssignmentStatusType.None);
                currentStatusDate = new DateTime(2014, 8, 10);

            }            

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void PersonIdRequired()
            {
                personId = null;
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void MealPlanIdRequired()
            {
                mealPlan = new MealPlan(new DateTime(2010, 1, 4), "a meal plan");
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void NumberOfRatePeriodsOneIfTermBilling()
            {
                mealPlan.RatePeriod = MealPlanRatePeriods.Term;
                numberOfRatePeriods = 2;
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void NumberOfRatePeriodsOneTo999()
            {
                numberOfRatePeriods = 1000;
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void StartDateIsRequired()
            {
                startDate = default(DateTime);
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void StartDateEarlierThanMealPlanStartDate()
            {
                mealPlan = new MealPlan(new DateTime(2010, 1, 4), "a meal plan");
                startDate = new DateTime(2010, 1, 3);
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            public void StartDateEqualsMealPlanStartDate()
            {
                startDate = mealPlan.StartDate;
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void EndDateRequiredWhenMealPlanEndDate()
            {
                mealPlan.EndDate = mealPlan.StartDate.AddDays(30);
                endDate = null;
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            public void EndDateEqualMealPlanEndDate()
            {
                mealPlan.EndDate = startDate.AddDays(30);
                endDate = mealPlan.EndDate;
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void EndDateLaterThanMealPlanEndDate()
            {
                mealPlan.EndDate = mealPlan.StartDate.AddDays(30);
                endDate = ((DateTime)mealPlan.EndDate).AddDays(1);
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void EndDateEarlierThanStartDate()
            {
                endDate = startDate.AddDays(-1);
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CurrentStatusDateRequired()
            {
                currentStatusDate = default(DateTime);
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void TermRequiredIfMealPlanRateByTerm()
            {
                mealPlan.RatePeriod = MealPlanRatePeriods.Term;
                term = null;
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void UsedRatePeriodsLessThanZero()
            {
                usedRatePeriods = -1;
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void UsedRatePeriodsGreaterThanNumberOfRatePeriods()
            {
                usedRatePeriods = numberOfRatePeriods + 1;
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void UsedPercentageGreaterThan100()
            {
                usedPercent = 101;
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void UsedPercentageLessThanZero()
            {
                usedPercent = -1;
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            // 4/16/15 BFE CR 456.19. A Terminated status is now allowed with no Used Rate Periods or Used Percent values.
            // Remove the test for this validation, since the validation has been removed.

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void CannotHaveBothUsedPercentageOrUsedRatePeriodsRequiredWithEarlyTermination()
            {
                usedPercent = 1;
                usedRatePeriods = 1;
                currentStatus = new MealPlanAssignmentStatus("T", "Early Termination", MealPlanAssignmentStatusType.EarlyTermination);
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void CannotChangeIdOnceSet()
            {
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
                mpa.Id = "ANID";
                mpa.Id = "ANOTHERID";
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void CannotHaveDuplicateExternalIdSource()
            {
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
                mpa.AddExternalId("EXTL1", "HD");
                mpa.AddExternalId("EXTL2", "HD");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void OverrideRateReasonRequiredWithOverrideRate()
            {
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
                mpa.SetOverrideRate(34.33M, string.Empty);
            }

            [TestMethod]
            public void NullOverrideRateReasonOkWithNullOverrideRate()
            {
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
                mpa.SetOverrideRate(null, string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void OverrideRateCannotBeNegative()
            {
                mpa = new MealPlanAssignment(personId, mealPlan, numberOfRatePeriods, startDate, endDate,
                                         currentStatus, currentStatusDate, term, usedRatePeriods, usedPercent);
                mpa.SetOverrideRate(-34.33M, "Reason");
            }
        }
    }


}
