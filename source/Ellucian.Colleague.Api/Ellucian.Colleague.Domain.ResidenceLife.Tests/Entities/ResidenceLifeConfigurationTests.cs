﻿using System;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

//TODO add test methods for remaining properties

namespace Ellucian.Colleague.Domain.ResidenceLife.Tests.Entities
{
    [TestClass]
    public class ResidenceLifeConfigurationTests
    {
        string invArTypeCd = "01";
        string invTypeCd = "RL";
        string extSysId = "ACME";
        string cashier = "0004225";
        string depositDistro = "ROOM";

        ResidenceLifeConfiguration result;

        [TestInitialize]
        public void Initialize()
        {
            result = new ResidenceLifeConfiguration(invArTypeCd, invTypeCd, extSysId, cashier, depositDistro);
        }

        #region InvoiceReceivableTypeCode
        [TestMethod]
        // Constructor correctly preserves the value
        public void ResidenceLifeConfiguration_Constructor_InvoiceReceivableTypeCode()
        {
            Assert.AreEqual(invArTypeCd, result.InvoiceReceivableTypeCode);
        }

        [TestMethod]
        // Null is not allowed
        [ExpectedException(typeof(ArgumentNullException))]
        public void ResidenceLifeConfiguration_Constructor_InvoiceReceivableTypeCode_Null()
        {
            result = new ResidenceLifeConfiguration(null, invTypeCd, extSysId, cashier, depositDistro);
        }

        [TestMethod]
        // Empty string is not allowed
        [ExpectedException(typeof(ArgumentNullException))]
        public void ResidenceLifeConfiguration_Constructor_InvoiceReceivableTypeCode_Empty()
        {
            result = new ResidenceLifeConfiguration(string.Empty, invTypeCd, extSysId, cashier, depositDistro);
        }
        #endregion

        #region InvoiceTypeCode
        [TestMethod]
        // Constructor correctly preserves the value
        public void ResidenceLifeConfiguration_Constructor_InvoiceTypeCode()
        {
            Assert.AreEqual(invTypeCd, result.InvoiceTypeCode);
        }

        [TestMethod]
        // Null is not allowed
        [ExpectedException(typeof(ArgumentNullException))]
        public void ResidenceLifeConfiguration_Constructor_InvoiceTypeCode_Null()
        {
            result = new ResidenceLifeConfiguration(invArTypeCd, null, extSysId, cashier, depositDistro);
        }

        [TestMethod]
        // Empty string is not allowed
        [ExpectedException(typeof(ArgumentNullException))]
        public void ResidenceLifeConfiguration_Constructor_InvoiceTypeCode_Empty()
        {
            result = new ResidenceLifeConfiguration(invArTypeCd, string.Empty, extSysId, cashier, depositDistro);
        }
        #endregion

        #region ExternalSystemId

        [TestMethod]
        // Constructor correctly preserves the value
        public void ResidenceLifeConfiguration_Constructor_ExternalSystemId()
        {
            Assert.AreEqual(extSysId, result.ExternalSystemId);
        }

        [TestMethod]
        // Null is not allowed
        [ExpectedException(typeof(ArgumentNullException))]
        public void ResidenceLifeConfiguration_Constructor_ExternalSystemId_Null()
        {
            result = new ResidenceLifeConfiguration(invArTypeCd, invTypeCd, null, cashier, depositDistro);
        }

        [TestMethod]
        // Empty string is not allowed
        [ExpectedException(typeof(ArgumentNullException))]
        public void ResidenceLifeConfiguration_Constructor_ExternalSystemId_Empty()
        {
            result = new ResidenceLifeConfiguration(invArTypeCd, invTypeCd, string.Empty, cashier, depositDistro);
        }
        #endregion

        #region Casher and DepositDistribution

        [TestMethod]
        // Constructor correctly preserves the value
        public void ResidenceLifeConfiguration_Constructor_Cashier()
        {
            Assert.AreEqual(cashier, result.Cashier);
        }

        [TestMethod]
        // Constructor correctly preserves the value
        public void ResidenceLifeConfiguration_Constructor_DepositDistribution()
        {
            Assert.AreEqual(depositDistro, result.DepositDistribution);
        }

        [TestMethod]
        // Constructor allows null for both Cashier and DepositDistribution
        public void ResidenceLifeConfiguration_Constructor_NullCashierAndDepositDistribution()
        {
            result = new ResidenceLifeConfiguration(invArTypeCd, invTypeCd, extSysId, null, null);
            Assert.IsNull(result.Cashier);
            Assert.IsNull(result.DepositDistribution);
        }

        //
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        // if distribution supplied, cashier must be supplied
        public void ResidenceLifeConfiguration_Constructor_DistributionButNoCashier()
        {
            result = new ResidenceLifeConfiguration(invArTypeCd, invTypeCd, extSysId, null, depositDistro);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        // if cashier supplied, distribution must be supplied
        public void ResidenceLifeConfiguration_Constructor_CashierButNoDistribution()
        {
            result = new ResidenceLifeConfiguration(invArTypeCd, invTypeCd, extSysId, cashier, null);
        }

        #endregion

    }
}
