﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;

namespace Ellucian.Colleague.Domain.ResidenceLife.Tests.Entities
{
    [TestClass]
    public class HousingAssignmentTests
    {
        [TestClass]
        public class BasicSettingAndRetrievalOfProperties
        {
            private HousingAssignment ha;
            private string id;
            private string externalId;
            private string externalIdSource;
            private string personId;
            private DateTime startDate;
            private DateTime endDate;
            private string currentStatus;
            private DateTime currentStatusDate;
            private string building;
            private string room;
            private string term;
            private string roomRateTable;
            private RoomRatePeriods? roomRatePeriod;
            private decimal? overrideRate;
            private string overrideRateReason;
            private string overrideChargeCode;
            private string overrideReceivableType;
            private string overrideRefundFormula;
            private string[] additionalChargeCodes = new string[2];
            private string[] additionalChargeDescriptions = new string[2];
            private decimal[] additionalChargeAmounts = new decimal[2];

            [TestInitialize]
            public void Initialize()
            {
                id = "12345666";
                externalId = "ABCDEFGH";
                externalIdSource = "HD";
                personId = "12345677";
                startDate = new DateTime(2011, 02,27);
                endDate = new DateTime(2011, 02,28);
                currentStatus = "A";
                currentStatusDate = new DateTime(2014, 05,14);

                ha = new HousingAssignment(personId, startDate, endDate, currentStatus, currentStatusDate);

                externalId = "ABCDEFGH";
                externalIdSource = "HD";
                ha.AddExternalId(externalId, externalIdSource);

                building = "BLDG-A";
                room = "ROOM-1";
                ha.SetBuildingAndRoom(building, room);

                term = "14/FA";
                roomRateTable = "MyTab";
                roomRatePeriod = RoomRatePeriods.Term;
                overrideRate = 23.44M;
                overrideRateReason = "REQ";
                overrideChargeCode = "op";
                overrideReceivableType = "Art";
                overrideRefundFormula = "DBL";

                ha.SetRateInformation(term, roomRateTable, roomRatePeriod, overrideRate, overrideRateReason, overrideChargeCode, overrideReceivableType, overrideRefundFormula);

                additionalChargeCodes[0] = "CC1";
                additionalChargeDescriptions[0] = "My additional charge";
                additionalChargeAmounts[0] = -23.22M;

                additionalChargeCodes[1] = "CC2";
                additionalChargeDescriptions[1] = "Other additional charge";
                additionalChargeAmounts[1] = 24.24M;

                ha.AddAdditionalChargeOrCredit(additionalChargeCodes[0], additionalChargeDescriptions[0], additionalChargeAmounts[0]);
                ha.AddAdditionalChargeOrCredit(additionalChargeCodes[1], additionalChargeDescriptions[1], additionalChargeAmounts[1]);

                ha.Id = id;
            }

            [TestCleanup]
            public void CleanUp()
            {
            }

            [TestMethod]
            public void Id()
            {
                Assert.AreEqual(id, ha.Id);
            }

            [TestMethod]
            public void ExternalId()
            {
                Assert.AreEqual(externalId, ha.ExternalIds[0].ExternalId);
            }

            [TestMethod]
            public void ExternalIdSource()
            {
                Assert.AreEqual(externalIdSource, ha.ExternalIds[0].ExternalIdSource);
            }

            [TestMethod]
            public void PersonId()
            {
                Assert.AreEqual(personId, ha.PersonId);
            }

            [TestMethod]
            public void StartDate()
            {
                Assert.AreEqual(startDate, ha.StartDate);
            }
                        
            [TestMethod]
            public void EndDate()
            {
                Assert.AreEqual(endDate, ha.EndDate);
            }

            [TestMethod]
            public void CurrentStatus()
            {
                Assert.AreEqual(currentStatus, ha.CurrentStatus);
            }

            [TestMethod]
            public void CurrentStatusDate()
            {
                Assert.AreEqual(currentStatusDate, ha.CurrentStatusDate);
            }

            [TestMethod]
            public void Building()
            {
                Assert.AreEqual(building, ha.Building);
            }

            [TestMethod]
            public void Room()
            {
                Assert.AreEqual(room, ha.Room);
            }

            [TestMethod]
            public void Term()
            {
                Assert.AreEqual(term, ha.TermId);
            }

            [TestMethod]
            public void RoomRateTable()
            {
                Assert.AreEqual(roomRateTable, ha.RoomRateTable);
            }

            [TestMethod]
            public void RoomRatePeriod()
            {
                Assert.AreEqual(roomRatePeriod, ha.RoomRatePeriod);
            }


            [TestMethod]
            public void OverrideRate()
            {
                Assert.AreEqual(overrideRate, ha.OverrideRate);
            }

            [TestMethod]
            public void OverrideRateReason()
            {
                Assert.AreEqual(overrideRateReason, ha.OverrideRateReason);
            }

            [TestMethod]
            public void OverrideChargeCode()
            {
                Assert.AreEqual(overrideChargeCode, ha.OverrideChargeCode);
            }

            [TestMethod]
            public void OverrideReceivableType()
            {
                Assert.AreEqual(overrideReceivableType, ha.OverrideReceivableType);
            }

            [TestMethod]
            public void OverrideRefundFormula()
            {
                Assert.AreEqual(overrideRefundFormula, ha.OverrideRefundFormula);
            }

            [TestMethod]
            public void HousingAdditionalChargeOrCredit()
            {                
                Assert.AreEqual(2, ha.AdditionalChargesOrCredits.Count);

                Assert.AreEqual(additionalChargeCodes[0], ha.AdditionalChargesOrCredits[0].ArCode);
                Assert.AreEqual(additionalChargeDescriptions[0], ha.AdditionalChargesOrCredits[0].Description);
                Assert.AreEqual(additionalChargeAmounts[0], ha.AdditionalChargesOrCredits[0].Amount);

                Assert.AreEqual(additionalChargeCodes[1], ha.AdditionalChargesOrCredits[1].ArCode);
                Assert.AreEqual(additionalChargeDescriptions[1], ha.AdditionalChargesOrCredits[1].Description);
                Assert.AreEqual(additionalChargeAmounts[1], ha.AdditionalChargesOrCredits[1].Amount);
            }
        }

        [TestClass]
        public class ExerciseOfSetValidations
        {

            [TestMethod]
            [ExpectedException(typeof(System.ArgumentNullException))]
            public void ExternalIdSourceWithoutId()
            {
                HousingAssignment ha;
                string externalId;
                string externalIdSource;
                string personId;
                DateTime startDate;
                DateTime endDate;
                string currentStatus;
                DateTime currentStatusDate;

                externalId = null;
                externalIdSource = "HD";
                personId = "12345677";
                startDate = new DateTime(2011, 02, 27);
                endDate = new DateTime(2011, 02, 28);
                currentStatus = "A";
                currentStatusDate = new DateTime(2014, 05, 14);

                ha = new HousingAssignment(personId, startDate, endDate, currentStatus, currentStatusDate);
                ha.AddExternalId(externalId, externalIdSource);
            }

            [TestMethod]
            [ExpectedException(typeof(System.ArgumentNullException))]
            public void ExternalIdWithoutSource()
            {
                HousingAssignment ha;
                string externalId;
                string externalIdSource;
                string personId;
                DateTime startDate;
                DateTime endDate;
                string currentStatus;
                DateTime currentStatusDate;

                externalId = "ABCDEFG";
                externalIdSource = null;
                personId = "12345677";
                startDate = new DateTime(2011, 02, 27);
                endDate = new DateTime(2011, 02, 28);
                currentStatus = "A";
                currentStatusDate = new DateTime(2014, 05, 14);

                ha = new HousingAssignment(personId, startDate, endDate, currentStatus, currentStatusDate);
                ha.AddExternalId(externalId, externalIdSource);
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public void MultipleExternalIds()
            {
                HousingAssignment ha;
                string personId;
                DateTime startDate;
                DateTime endDate;
                string currentStatus;
                DateTime currentStatusDate;

                personId = "12345677";
                startDate = new DateTime(2011, 02, 27);
                endDate = new DateTime(2011, 02, 28);
                currentStatus = "A";
                currentStatusDate = new DateTime(2014, 05, 14);

                ha = new HousingAssignment(personId, startDate, endDate, currentStatus, currentStatusDate);
                ha.AddExternalId("1", "A");
                ha.AddExternalId("2", "B");
                Assert.AreEqual(2, ha.ExternalIds.Count);
                Assert.AreEqual("2", ha.ExternalIds[1].ExternalId);
                Assert.AreEqual("B", ha.ExternalIds[1].ExternalIdSource);
                Assert.AreEqual("1", ha.ExternalIds[0].ExternalId);
                Assert.AreEqual("A", ha.ExternalIds[0].ExternalIdSource);

                ha.AddExternalId("3", "B");
            }


            [TestMethod]
            [ExpectedException(typeof(System.ArgumentNullException))]
            public void NullPersonId()
            {
                HousingAssignment ha;
                string personId;
                DateTime startDate;
                DateTime endDate;
                string currentStatus;
                DateTime currentStatusDate;

                personId = null;
                startDate = new DateTime(2011, 02, 27);
                endDate = new DateTime(2011, 02, 28);
                currentStatus = "A";
                currentStatusDate = new DateTime(2014, 05, 14);

                ha = new HousingAssignment(personId, startDate, endDate, currentStatus, currentStatusDate);
            }

            [TestMethod]
            [ExpectedException(typeof(System.ArgumentNullException))]
            public void NullCurrentStatus()
            {
                HousingAssignment ha;
                string personId;
                DateTime startDate;
                DateTime endDate;
                string currentStatus;
                DateTime currentStatusDate;

                personId = "1234545";
                startDate = new DateTime(2011, 02, 27);
                endDate = new DateTime(2011, 02, 28);
                currentStatus = null;
                currentStatusDate = new DateTime(2014, 05, 14);

                ha = new HousingAssignment(personId, startDate, endDate, currentStatus, currentStatusDate);
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public void StartDateLaterThanEndDate()
            {
                HousingAssignment ha;
                string personId;
                DateTime startDate;
                DateTime endDate;
                string currentStatus;
                DateTime currentStatusDate;

                personId = "1234545";
                startDate = new DateTime(2011, 02, 28);
                endDate = new DateTime(2011, 02, 27);
                currentStatus = "Stat";
                currentStatusDate = new DateTime(2014, 05, 14);

                ha = new HousingAssignment(personId, startDate, endDate, currentStatus, currentStatusDate);
            }

            private void ConstructHousingAssignment(ref HousingAssignment ha)
            {
                string personId;
                DateTime startDate;
                DateTime endDate;
                string currentStatus;
                DateTime currentStatusDate;

                personId = "1234545";
                startDate = new DateTime(2011, 02, 27);
                endDate = new DateTime(2011, 02, 28);
                currentStatus = "Stat";
                currentStatusDate = new DateTime(2014, 05, 14);

                ha = new HousingAssignment(personId, startDate, endDate, currentStatus, currentStatusDate);
            }

            [TestMethod]
            [ExpectedException(typeof(System.ArgumentException))]
            public void RoomWithoutBuilding()
            {
                HousingAssignment ha = null;
                ConstructHousingAssignment(ref ha);

                string building = null;
                string room = "123";
                ha.SetBuildingAndRoom(building, room);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void RatePeriodWithoutRatetable()
            {
                HousingAssignment ha = null;
                ConstructHousingAssignment(ref ha);
                                   
                string term = "2013/FA";
                string roomRateTable = null;
                RoomRatePeriods? roomRatePeriod = RoomRatePeriods.Term;
                decimal? overrideRate = 23.22M;
                string overrideRateReason = "REASON";
                string overrideChargeCode = "CODEA";
                string overrideReceivableType = "TYPEA";
                string overrideRefundFormula = "FORMULA";

                ha.SetRateInformation(term, roomRateTable, roomRatePeriod, overrideRate, overrideRateReason, overrideChargeCode,
                                        overrideReceivableType, overrideRefundFormula);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void RateTableWithoutRatePeriod()
            {
                HousingAssignment ha = null;
                ConstructHousingAssignment(ref ha);

                string term = "2013/FA";
                string roomRateTable = "TBLA";
                RoomRatePeriods? roomRatePeriod = null;
                decimal? overrideRate = 23.22M;
                string overrideRateReason = "REASON";
                string overrideChargeCode = "CODEA";
                string overrideReceivableType = "TYPEA";
                string overrideRefundFormula = "FORMULA";

                ha.SetRateInformation(term, roomRateTable, roomRatePeriod, overrideRate, overrideRateReason, overrideChargeCode,
                                        overrideReceivableType, overrideRefundFormula);
            }

            [TestMethod]
            [ExpectedException(typeof(ApplicationException))]
            public void TermRatePeriodWithNoTerm()
            {
                HousingAssignment ha = null;
                ConstructHousingAssignment(ref ha);

                string term = "";
                string roomRateTable = "TBLA";
                RoomRatePeriods? roomRatePeriod = RoomRatePeriods.Term;
                decimal? overrideRate = 23.22M;
                string overrideRateReason = "REASON";
                string overrideChargeCode = "CODEA";
                string overrideReceivableType = "TYPEA";
                string overrideRefundFormula = "FORMULA";

                ha.SetRateInformation(term, roomRateTable, roomRatePeriod, overrideRate, overrideRateReason, overrideChargeCode,
                                        overrideReceivableType, overrideRefundFormula);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void OverrideRateWithNoReason()
            {
                HousingAssignment ha = null;
                ConstructHousingAssignment(ref ha);

                string term = "2014/FA";
                string roomRateTable = "TBLA";
                RoomRatePeriods? roomRatePeriod = RoomRatePeriods.Term;
                decimal? overrideRate = 23.22M;
                string overrideRateReason = "";
                string overrideChargeCode = "CODEA";
                string overrideReceivableType = "TYPEA";
                string overrideRefundFormula = "FORMULA";

                ha.SetRateInformation(term, roomRateTable, roomRatePeriod, overrideRate, overrideRateReason, overrideChargeCode,
                                        overrideReceivableType, overrideRefundFormula);
            }

            [TestMethod]
            [ExpectedException(typeof(System.ArgumentException))]
            public void CannotChangeID()
            {
                HousingAssignment ha2;
                ha2 = new HousingAssignment("1", new DateTime(2014,1,1), new DateTime(2014,2,1), "A", DateTime.Now);
                ha2.Id = "343444";
                ha2.Id = "22";
            }

            [TestMethod]
            public void CanSetIdToNullTwice()
            {
                HousingAssignment ha2;
                ha2 = new HousingAssignment("1", new DateTime(2014, 1, 1), new DateTime(2014, 2, 1), "A", DateTime.Now);
                ha2.Id = string.Empty;
                ha2.Id = null;
            }

            [TestMethod]
            [ExpectedException(typeof(System.ArgumentNullException))]
            public void DefaultStartDateNotAllowed()
            {
                HousingAssignment ha2;
                ha2 = new HousingAssignment("1", default(DateTime), new DateTime(2014, 2, 1), "A", DateTime.Now);
            }

            [TestMethod]
            [ExpectedException(typeof(System.ArgumentNullException))]
            public void DefaultEndDateNotAllowed()
            {
                HousingAssignment ha2;
                ha2 = new HousingAssignment("1", new DateTime(2014, 2, 1), default(DateTime), "A", DateTime.Now);
            }

            [TestMethod]
            [ExpectedException(typeof(System.ArgumentNullException))]
            public void DefaultCurrentStatusDateNotAllowed()
            {
                HousingAssignment ha2;
                ha2 = new HousingAssignment("1", new DateTime(2014, 2, 1), default(DateTime), "A", default(DateTime));
            }
        }

        [TestClass]
        public class AdditionalChargeTests
        {
            private HousingAssignment ha;
            
            [TestInitialize]
            public void Initialize()
            {
                string personId;
                DateTime startDate;
                DateTime endDate;
                string currentStatus;
                DateTime currentStatusDate;

                personId = "12345677";
                startDate = new DateTime(2011, 02, 27);
                endDate = new DateTime(2011, 02, 28);
                currentStatus = "A";
                currentStatusDate = new DateTime(2014, 05, 14);
                ha = new HousingAssignment(personId, startDate, endDate, currentStatus, currentStatusDate);
                ha.Id = "555"; // To get error on attempt to change.
            }

            [TestMethod]
            [ExpectedException(typeof(System.ArgumentNullException))]
            public void NullAdditionalChargeCode()
            {
                string additionalChargeCode = null;
                string additionalChargeDescription = "My additional charge";
                decimal additionalChargeAmount = -23.22M;

                ha.AddAdditionalChargeOrCredit(additionalChargeCode, additionalChargeDescription, additionalChargeAmount);
            }

            [TestMethod]
            [ExpectedException(typeof(System.ArgumentNullException))]
            public void NullAdditionalChargeDescription()
            {
                string additionalChargeCode = "CHG1";
                string additionalChargeDescription = null;
                decimal additionalChargeAmount = -23.22M;

                ha.AddAdditionalChargeOrCredit(additionalChargeCode, additionalChargeDescription, additionalChargeAmount);
            }

            [TestMethod]
            [ExpectedException(typeof(System.ArgumentException))]
            public void ZeroAdditionalChargeAmount()
            {
                string additionalChargeCode = "CHG1";
                string additionalChargeDescription = "A description!";
                decimal additionalChargeAmount = 0M;

                ha.AddAdditionalChargeOrCredit(additionalChargeCode, additionalChargeDescription, additionalChargeAmount);
            }
        
            [TestMethod]
            [ExpectedException(typeof(System.ArgumentException))]
            public void TooLargeAdditionalAmount()
            {
                string additionalChargeCode = "CHG1";
                string additionalChargeDescription = "A description!";
                decimal additionalChargeAmount = 1000000M;

                ha.AddAdditionalChargeOrCredit(additionalChargeCode, additionalChargeDescription, additionalChargeAmount);
            }
        
            [TestMethod]
            [ExpectedException(typeof(System.ArgumentException))]
            public void TooSmallAdditionalAmount()
            {
                string additionalChargeCode = "CHG1";
                string additionalChargeDescription = "A description!";
                decimal additionalChargeAmount = -1000000M;

                ha.AddAdditionalChargeOrCredit(additionalChargeCode, additionalChargeDescription, additionalChargeAmount);
            }
        
            [TestMethod]
            public void MaxAdditionalAmount()
            {
                string additionalChargeCode = "CHG1";
                string additionalChargeDescription = "A description!";
                decimal additionalChargeAmount = 999999.99M;

                ha.AddAdditionalChargeOrCredit(additionalChargeCode, additionalChargeDescription, additionalChargeAmount);
                Assert.AreEqual(additionalChargeAmount, ha.AdditionalChargesOrCredits[0].Amount);
            }

            [TestMethod]
            public void MinAdditionalAmount()
            {
                string additionalChargeCode = "CHG1";
                string additionalChargeDescription = "A description!";
                decimal additionalChargeAmount = -999999.99M;

                ha.AddAdditionalChargeOrCredit(additionalChargeCode, additionalChargeDescription, additionalChargeAmount);
                Assert.AreEqual(additionalChargeAmount, ha.AdditionalChargesOrCredits[0].Amount);
            }

        }
    
    }
}
