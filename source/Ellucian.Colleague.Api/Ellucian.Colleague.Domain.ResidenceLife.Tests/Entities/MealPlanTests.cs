﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;

namespace Ellucian.Colleague.Domain.ResidenceLife.Tests.Entities
{
    [TestClass]
    public class MealPlanTests
    {
        [TestClass]
        public class BasicSettingAndRetrievalOfProperties
        {
            private string id;
            private DateTime startDate;
            private DateTime? endDate;
            private List<string> locations = new List<string>();
            private List<MealPlanRate> rates = new List<MealPlanRate>();
            private string description;
            private List<string> mealTypes;
            private List<MealPlanBuildingAndRoom> buildingsAndRooms = new List<MealPlanBuildingAndRoom>();

            private MealPlanRatePeriods? ratePeriod;
            private string chargeCode;
            private string cancelationChargeCode;
            private string mealClass;
            private int? numberOfMealsPer;
            private string mealsPerFrequency;
            private string comments;

            private MealPlan mp;

            [TestInitialize]
            public void Initialize()
            {
                id = "123344";
                startDate = new DateTime(2014, 1, 2);
                endDate = new DateTime(2014, 5, 31);
                locations = new List<String> { "LOC1", "loc2", "loc3" };
                rates = new List<MealPlanRate> { new MealPlanRate(12.33M, new DateTime(2013, 1, 1)),
                    new MealPlanRate(19.99M, new DateTime(2014, 1, 1))};
                description = "The description";
                mealTypes = new List<String> { "MT1", "MT2" };
                buildingsAndRooms = new List<MealPlanBuildingAndRoom> {
                    new MealPlanBuildingAndRoom("BLDG1", null),
                    new MealPlanBuildingAndRoom("BLDG2", "rm1"),
                    new MealPlanBuildingAndRoom("BLDG3", "rm1")
                };
                ratePeriod = MealPlanRatePeriods.Daily;
                chargeCode = "AR1";
                cancelationChargeCode = "AR2";
                mealClass = "class1";
                numberOfMealsPer = 2;
                mealsPerFrequency = "W";
                comments = "The comments";

                mp = new MealPlan(startDate, description);
                mp.SetBuildingsAndRooms(buildingsAndRooms);
                mp.CancelationChargeCode = cancelationChargeCode;
                mp.SetRatesAndChargeCode(rates, chargeCode);
                mp.Comments = comments;
                mp.EndDate = endDate;
                mp.Id = id;
                mp.SetLocations(locations);
                mp.MealClass = mealClass;
                mp.SetMealTypes(mealTypes);
                mp.NumberOfMealsPer = numberOfMealsPer;
                mp.MealsPerFrequency = mealsPerFrequency;
                mp.RatePeriod = ratePeriod;
            }

            [TestMethod]
            public void StartDate()
            {
                Assert.AreEqual(startDate, mp.StartDate);
            }

            [TestMethod]
            public void Description()
            {
                Assert.AreEqual(description, mp.Description);
            }

            [TestMethod]
            public void BuildingsAndRooms()
            {
                Assert.AreEqual(buildingsAndRooms.Count, mp.BuildingsAndRooms.Count);
                for (int i = 0; i < buildingsAndRooms.Count; i++)
                {
                    Assert.AreEqual(buildingsAndRooms[i].Building, mp.BuildingsAndRooms[i].Building);
                    Assert.AreEqual(buildingsAndRooms[i].Room, mp.BuildingsAndRooms[i].Room);
                }
            }

            [TestMethod]
            public void CancelationChargeCode()
            {
                Assert.AreEqual(cancelationChargeCode, mp.CancelationChargeCode);
            }

            [TestMethod]
            public void ChargeCode()
            {
                Assert.AreEqual(chargeCode, mp.ChargeCode);
            }

            [TestMethod]
            public void Rates()
            {
                Assert.AreEqual(rates.Count, mp.Rates.Count);
                for (int i = 0; i < rates.Count; i++)
                {
                    Assert.AreEqual(rates[i].EffectiveDate, mp.Rates[i].EffectiveDate);
                    Assert.AreEqual(rates[i].Rate, mp.Rates[i].Rate);
                }
            }

            [TestMethod]
            public void Comments()
            {
                Assert.AreEqual(comments, mp.Comments);
            }

            [TestMethod]
            public void EndDate()
            {
                Assert.AreEqual(endDate, mp.EndDate);
            }

            [TestMethod]
            public void Id()
            {
                Assert.AreEqual(id, mp.Id);
            }

            [TestMethod]
            public void Locations()
            {
                CollectionAssert.AreEqual(locations, mp.Locations);
            }

            [TestMethod]
            public void MealClass()
            {
                Assert.AreEqual(mealClass, mp.MealClass);
            }

            [TestMethod]
            public void MealTypes()
            {
                CollectionAssert.AreEqual(mealTypes, mp.MealTypes);
            }

            [TestMethod]
            public void NumberOfMealsPer()
            {
                Assert.AreEqual(numberOfMealsPer, mp.NumberOfMealsPer);
            }

            [TestMethod]
            public void MealsPerFrequency()
            {
                Assert.AreEqual(mealsPerFrequency, mp.MealsPerFrequency);
            }

            [TestMethod]
            public void RatePeriod()
            {
                Assert.AreEqual(ratePeriod, mp.RatePeriod);
            }
        }

        [TestClass]
        public class ExerciseOfSetValidations
        {
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void DescriptionRequiredByConstructor()
            {
                MealPlan mp = new MealPlan(new DateTime(2014, 1, 1), string.Empty);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void StartDateRequiredByConstructor()
            {
                MealPlan mp = new MealPlan(new DateTime(), "Description");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void CannotChangeIDOnceSet()
            {
                MealPlan mp = new MealPlan(new DateTime(2014,1,1), "Description");
                mp.Id = "123444";
                mp.Id = "1235555";
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CannotSetStartDateToNull()
            {
                MealPlan mp = new MealPlan(new DateTime(2014, 1, 1), "Description");
                mp.StartDate = new DateTime();
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void CannotSetStartDateLaterThanEndDate()
            {
                MealPlan mp = new MealPlan(new DateTime(2014, 1, 1), "Description");
                mp.EndDate = new DateTime(2014, 1, 2);
                mp.StartDate = new DateTime(2014, 3, 1);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void CannotSetEndDateEarlierThanStartDate()
            {
                MealPlan mp = new MealPlan(new DateTime(2014, 1, 1), "Description");
                mp.EndDate = new DateTime(2013, 12, 2);
            }

            [TestMethod]
            public void CanSetEndDateEqualToStartDate()
            {
                MealPlan mp = new MealPlan(new DateTime(2014, 1, 1), "Description");
                mp.EndDate = new DateTime(2014, 1, 1);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void CannotSetDescriptionToBlank()
            {
                MealPlan mp = new MealPlan(new DateTime(2014, 1, 1), "Description");
                mp.Description = null;
            }

            [TestMethod]
            public void CanClearTheLocationsList()
            {
                MealPlan mp = new MealPlan(new DateTime(2014, 1, 1), "Description");
                mp.SetLocations(new List<string> { "LOC1", "LOC2" });
                mp.SetLocations(null);
                Assert.AreEqual(0, mp.Locations.Count);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void CannotSetDuplicateLocations()
            {
                MealPlan mp = new MealPlan(new DateTime(2014, 1, 1), "Description");
                mp.SetLocations(new List<string> { "LOC1", "LOC2", "LOC1" });
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void CannotSetAnEmptyLocation()
            {
                MealPlan mp = new MealPlan(new DateTime(2014, 1, 1), "Description");
                mp.SetLocations(new List<string> { "LOC1", null, "LOC1" });
            }


            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void CannotSetRatesWithoutChargeCode()
            {
                MealPlan mp = new MealPlan(new DateTime(2014, 1, 1), "Description");
                mp.SetRatesAndChargeCode(new List<MealPlanRate>() { new MealPlanRate(1M, new DateTime(2014, 1, 1)) }, null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void CannotSetChargeCodeWithoutRates()
            {
                MealPlan mp = new MealPlan(new DateTime(2014, 1, 1), "Description");
                mp.SetRatesAndChargeCode(new List<MealPlanRate>() , "CHCODE");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void CannotSetDuplicateRateEffectiveDates()
            {
                MealPlan mp = new MealPlan(new DateTime(2014, 1, 1), "Description");
                mp.SetRatesAndChargeCode(new List<MealPlanRate>() { new MealPlanRate(1M, new DateTime(2014, 1, 1)),
                    new MealPlanRate(3M, new DateTime(2014, 2, 1)), new MealPlanRate(4M, new DateTime(2014, 1, 1))
                }, "CHG");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void CannotSetDuplicateMealType()
            {
                MealPlan mp = new MealPlan(new DateTime(2014, 1, 1), "Description");
                mp.SetMealTypes(new List<string> { "LOC1", "LOC2", "LOC1" });
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void CannotSetAnEmptyMealType()
            {
                MealPlan mp = new MealPlan(new DateTime(2014, 1, 1), "Description");
                mp.SetMealTypes(new List<string> { "LOC1", null, "LOC1" });
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public void CannotSetDuplicateBuildingsAndRooms()
            {
                MealPlan mp = new MealPlan(new DateTime(2014, 1, 1), "Description");
                mp.SetBuildingsAndRooms( new List<MealPlanBuildingAndRoom> ()
                    { new MealPlanBuildingAndRoom("BLDG1", "101"), new MealPlanBuildingAndRoom("BLDG1", "102"),
                      new MealPlanBuildingAndRoom("BLDG1", "101")});
            }

            [TestMethod]
            public void MealPlanBuildingsAndRoomsEquality()
            {
                MealPlanBuildingAndRoom br1;
                MealPlanBuildingAndRoom br2;

                br1 = new MealPlanBuildingAndRoom("A", null);
                br2 = br1;
                Assert.AreEqual(true, br1.Equals(br2));
                Assert.AreEqual(br1.GetHashCode(), br2.GetHashCode());
                Assert.AreEqual(false, br1.Equals("c"));
                Assert.AreEqual(false, br1.Equals(null));
                br2 = new MealPlanBuildingAndRoom("A", "B");
                Assert.AreEqual(false, br1.Equals(br2));
                br1 = new MealPlanBuildingAndRoom("A", "B");
                br2 = new MealPlanBuildingAndRoom("B", "B");
                Assert.AreEqual(false, br1.Equals(br2));
            }        

        }
    
    }
}
