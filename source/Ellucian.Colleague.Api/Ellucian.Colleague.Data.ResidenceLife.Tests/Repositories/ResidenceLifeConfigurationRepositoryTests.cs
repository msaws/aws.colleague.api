﻿using Ellucian.Colleague.Data.ResidenceLife.DataContracts;
using Ellucian.Colleague.Data.ResidenceLife.Repositories;
using Ellucian.Colleague.Data.ResidenceLife.Transactions;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;
using Ellucian.Data.Colleague;
using Ellucian.Web.Cache;
using Ellucian.Colleague.Data.Base.Tests.Repositories;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml.Serialization;
using System.Runtime.Caching;

namespace Ellucian.Colleague.Data.ResidenceLife.Tests.Repositories
{
    [TestClass]
    public class ResidenceLifeConfigurationRepositoryTests : BaseRepositorySetup
    {

        // Repository instance that will be constructed with mock objects
        ResidenceLifeConfigurationRepository resLifeConfigRepo;

        RlExtSysDefaults rlesDefaults;

        // Sample data for a configuration record
        string invoiceArType = "01";
        string invoiceType = "RL";
        string externalSystem = "ACME";
        string cashier = "0004225";
        string distribution = "myDistribution";

        [TestInitialize()]
        public void Initialize()
        {
            MockInitialize();

            rlesDefaults = new RlExtSysDefaults()
            {
                RlesInvoiceArType = invoiceArType,
                RlesInvoiceType = invoiceType,
                RlesExternalId = externalSystem,
                RlesCashier = cashier,
                RlesDistribution = distribution
            };
            dataReaderMock.Setup<RlExtSysDefaults>(reader => reader.ReadRecord<RlExtSysDefaults>("ST.PARMS", "RL.EXT.SYS.DEFAULTS", true)).Returns(rlesDefaults);
            this.resLifeConfigRepo = new ResidenceLifeConfigurationRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);

        }

        [TestClass]
        public class GetResidenceLifeConfiguration : ResidenceLifeConfigurationRepositoryTests
        {
            [TestMethod]
            // Valid configuration object is returned
            public void ResidenceLifeConfigurationRepository_GetFinanceConfiguration_CashierAndDistribution()
            {
                RlExtSysDefaults rlesDefaults = new RlExtSysDefaults();
                rlesDefaults.RlesInvoiceArType = invoiceArType;
                rlesDefaults.RlesInvoiceType = invoiceType;
                rlesDefaults.RlesExternalId = externalSystem;
                rlesDefaults.RlesCashier = cashier;
                rlesDefaults.RlesDistribution = distribution;

                dataReaderMock.Setup<RlExtSysDefaults>(
                    reader => reader.ReadRecord<RlExtSysDefaults>("ST.PARMS", "RL.EXT.SYS.DEFAULTS", true)).Returns(rlesDefaults);
                var result = this.resLifeConfigRepo.GetResidenceLifeConfiguration();
                Assert.AreEqual(invoiceArType, result.InvoiceReceivableTypeCode);
            }

            [TestMethod]
            // Configuration information must have been defined in database
            [ExpectedException(typeof(KeyNotFoundException))]
            public void ResidenceLifeConfigurationRepository_GetFinanceConfiguration_NoRlExtSysDefaults()
            {
                RlExtSysDefaults rlesDefaults = null;
                dataReaderMock.Setup<RlExtSysDefaults>(
                    reader => reader.ReadRecord<RlExtSysDefaults>("ST.PARMS", "RL.EXT.SYS.DEFAULTS", true)).Returns(rlesDefaults);
                var result = this.resLifeConfigRepo.GetResidenceLifeConfiguration();
            }

            [TestMethod]
            // Verify that after we get the configuration, it's stored in the cache
            public void ResidenceLifeConfigurationRepository_GetResidenceLifeConfiguration_VerifyCache()
            {
                // Set up local cache mock to respond to cache request:
                //  -to "Contains" request, return "false" to indicate item is not in cache
                //  -to cache "Get" request, return null so we know it's getting data from "repository"
                string cacheKey = this.resLifeConfigRepo.BuildFullCacheKey("ResidenceLifeConfiguration");
                cacheProviderMock.Setup(x => x.Contains(cacheKey, null)).Returns(false);
                cacheProviderMock.Setup(x => x.Get(cacheKey, null)).Returns(null);
                // Make sure we can verify that it's in the cache
                cacheProviderMock.Setup(x => x.Add(cacheKey, It.IsAny<ResidenceLifeConfiguration>(), It.IsAny<CacheItemPolicy>(), null)).Verifiable();

                // Get the configuration
                var result = this.resLifeConfigRepo.GetResidenceLifeConfiguration();
                Assert.AreEqual(invoiceArType , result.InvoiceReceivableTypeCode);

                // Verify that the config is now in the cache
                cacheProviderMock.Verify(x => x.Add(cacheKey, It.IsAny<ResidenceLifeConfiguration>(), It.IsAny<CacheItemPolicy>(), null));
            }
        }

    }
}
