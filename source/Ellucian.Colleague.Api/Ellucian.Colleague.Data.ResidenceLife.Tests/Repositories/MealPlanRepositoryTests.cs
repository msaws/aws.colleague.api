﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Data.ResidenceLife.DataContracts;
using Ellucian.Colleague.Data.ResidenceLife.Repositories;
using Ellucian.Colleague.Data.ResidenceLife.Transactions;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;
using Ellucian.Data.Colleague;
using Ellucian.Web.Cache;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace Ellucian.Colleague.Data.ResidenceLife.Tests.Repositories
{
    [TestClass]
    public class MealPlanRepositoryTests
    {
        // Mock objects for the repository
        Mock<IColleagueDataReader> dataAccessorMock;
        Mock<IColleagueTransactionFactory> transFactoryMock;
        Mock<ICacheProvider> cacheProviderMock;
        Mock<ILogger> loggerMock;
        Mock<IColleagueTransactionInvoker> txInvokerMock;

        // Repository instance that will be constructed with mock objects
        Mock<MealPlanRepository> mealPlanRepoMock;
        MealPlanRepository mealPlanRepo;

        // Sample data for a meal plan
		string mealPlanRecordKey = "2";
        string mealPlanNoStartDateKey = "3";
        string mealPlanNoDescKey = "4";
        
        string mealDesc = "A description";
		string mealClass = "the class";
		int? MealNoTimes = 2;
		string MealFrequency = "D";
		List<string> mealTypes = new List<string> {"t1", "t2"};
		List<string> mealLocations = new List<string> {"LOC1", "LOC2"};
		List<string> mealBldgs = new List<string> {"BLDG1"};
		List<string> mealRooms  = new List<string> {"RM2"};
		string mealComments = "comments and more comments and more";
		string mealRatePeriod = "B";
		string mealArCode = "AR1";
		List<DateTime?> mealRateEffectiveDates = new List<DateTime?> {new DateTime(2014,1,1), new DateTime(2013,1,1)};
        List<decimal?> mealRates = new List<decimal?> { 34.1M, 22M };
		DateTime? mealStartDate = new DateTime(2013,1,1);
		DateTime? mealEndDate = new DateTime(2020,12,31);
		string mealCancelArCode = "ARCA";

        // Sample data for a meal plan assignment
        string mpasRecordKey = "2";
        string mpasNoNumberOfRatePeriodsRecordKey = "3";
        string mpasNoStartDateKey = "4";
        string mpasNoStatusDateKey = "5";

        string mpasPersonId = "23343903AA";
        string mpasMealPlan = "133";
        DateTime? mpasStartDate = new DateTime(2010, 1, 1);
        DateTime? mpasEndDate = new DateTime(2020, 12, 31);
        string mpasMealCard = "CARD1222";
        string mpasTerm = "2013/FA";
        int? mpasNoRatePeriods = 2;
        Decimal? mpasOverrideRate = 344.33M;
        string mpasRateOverrideReason = "REAS1";
        string mpasOverrideArCode = "ARC1";
        string mpasOverrideArType = "ART1";
        string mpasOverrideRefundFormula = "REF1";
        string mpasComments = "These here are comments, I mean comments";
        List<string> mpasStatus = new List<string> { "L", "A" };
        List<DateTime?> mpasStatusDate = new List<DateTime?> { new DateTime(2014, 9, 1), new DateTime(2014, 8, 1) };
        int? mpasUsedRatePeriods = 1;
        int? mpasUsedPct = 50;
        List<string> mpasExternalIdSource = new List<string> { "3333", "3433" };
        List<string> mpasExternalId = new List<string> { "HD", "RMS" };

        [TestInitialize]
        public void Initialize()
        {
            // Initialize mock objects used to construct a repository
            loggerMock = new Mock<ILogger>();
            cacheProviderMock = new Mock<ICacheProvider>();
            dataAccessorMock = new Mock<IColleagueDataReader>();
            transFactoryMock = new Mock<IColleagueTransactionFactory>();
            txInvokerMock = new Mock<IColleagueTransactionInvoker>();
            
            // Build mock records within dataAccessMock
            BuildMockMealPlanRecords();
            BuildMockMealPlanAssignmentRecords();

            // Set the transaction factory to return our dataAccessMock as the data reader.
            transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(dataAccessorMock.Object);

            // Instantiate the repository
            mealPlanRepoMock = new Mock<MealPlanRepository>(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);
            mealPlanRepoMock.CallBase = true;

            // Mock return of MealPlanAssignmentStatus valcodes
            List<MealPlanAssignmentStatus> mockMealPlanAssignmentStatuses = new List<MealPlanAssignmentStatus>();
            mockMealPlanAssignmentStatuses.Add(new MealPlanAssignmentStatus("L","Late Start", MealPlanAssignmentStatusType.LateStart));
            mockMealPlanAssignmentStatuses.Add(new MealPlanAssignmentStatus("C","Canceled", MealPlanAssignmentStatusType.Canceled));
            mealPlanRepoMock.Setup(m => m.MealPlanAssignmentStatuses).Returns(mockMealPlanAssignmentStatuses);
            mealPlanRepo = mealPlanRepoMock.Object;
            //mealPlanRepo= new MealPlanRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);

        }

        // A successfull Get, making sure the returned entity is properly populated with the data from the 
        // data reader. This exercises the mapping of the datareader object to the domain entity object.
        [TestMethod]
        public void GetMealPlan()
        {
            MealPlan mealPlan = mealPlanRepo.Get(mealPlanRecordKey);
            Assert.AreEqual(mealPlanRecordKey, mealPlan.Id);
            Assert.AreEqual(mealDesc, mealPlan.Description);
            Assert.AreEqual(mealClass, mealPlan.MealClass);
            Assert.AreEqual(MealNoTimes, mealPlan.NumberOfMealsPer);
            Assert.AreEqual(MealFrequency, mealPlan.MealsPerFrequency);
            Assert.AreEqual(mealComments, mealPlan.Comments);
            string ratePeriodCode;
            switch (mealPlan.RatePeriod)
            {
                case MealPlanRatePeriods.PerMeal:
                    ratePeriodCode = "B";
                    break;
                case MealPlanRatePeriods.Daily:
                    ratePeriodCode = "D";
                    break;
                case MealPlanRatePeriods.Weekly:
                    ratePeriodCode = "W";
                    break;
                case MealPlanRatePeriods.Monthly:
                    ratePeriodCode = "M";
                    break;
                case MealPlanRatePeriods.Yearly:
                    ratePeriodCode = "Y";
                    break;
                case MealPlanRatePeriods.Term:
                    ratePeriodCode = "T";
                    break;
                default:
                    ratePeriodCode = "";
                    break;
            }
            Assert.AreEqual(mealRatePeriod, ratePeriodCode);
            Assert.AreEqual(mealArCode, mealPlan.ChargeCode);
            Assert.AreEqual(mealStartDate, mealPlan.StartDate);
            Assert.AreEqual(mealEndDate, mealPlan.EndDate);
            Assert.AreEqual(mealCancelArCode, mealPlan.CancelationChargeCode);
            CollectionAssert.AreEqual(mealTypes, mealPlan.MealTypes);
            CollectionAssert.AreEqual(mealLocations, mealPlan.Locations);
            Assert.AreEqual(mealBldgs.Count, mealPlan.BuildingsAndRooms.Count);
            int i = 0;
            foreach (MealPlanBuildingAndRoom mbr in mealPlan.BuildingsAndRooms)
            {
                Assert.AreEqual(mealBldgs[i], mbr.Building);
                Assert.AreEqual(mealRooms[i], mbr.Room);
                i++;
            }

            Assert.AreEqual(mealRates.Count, mealPlan.Rates.Count);
            i = 0;
            foreach (MealPlanRate mr in mealPlan.Rates)
            {
                Assert.AreEqual(mealRates[i], mr.Rate);
                Assert.AreEqual(mealRateEffectiveDates[i], mr.EffectiveDate);
                i++;
            }
        }

        // Test the detection a failed read in the Get operation
        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void GetMealPlanByBadId()
        {
            MealPlan mealPlan = mealPlanRepo.Get("X");
        }

        // Test the detection a null start date
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GetMealPlanNoStartDate()
        {
            MealPlan mealPlan = mealPlanRepo.Get(mealPlanNoStartDateKey);
        }

        // Test the detection a null desc
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GetMealPlanNoDesc()
        {
            MealPlan mealPlan = mealPlanRepo.Get(mealPlanNoDescKey);
        }

        // Test that a buildings and room domain exception is ignored
        [TestMethod]
        public void GetMealPlanInvalidBuildingAndRoom()
        {
            string mpRecordKey = "77";
            DataContracts.MealPlans mockMealPlans = new MealPlans();
            mockMealPlans.Recordkey = mpRecordKey;
            mockMealPlans.MealStartDate = new DateTime(2010, 1, 1);
            mockMealPlans.MealDesc = "a meal plan";
            // Duplicate building and room
            mockMealPlans.MealBldgs = new List<String>() {"B1", "B1"};
            mockMealPlans.MealRooms = new List<String>() {"R1", "R1"};
            mockMealPlans.buildAssociations();
            dataAccessorMock.Setup<DataContracts.MealPlans>(acc => acc.ReadRecord<DataContracts.MealPlans>(mpRecordKey, true)).Returns(mockMealPlans);
            MealPlan mealPlan = mealPlanRepo.Get(mpRecordKey);
        }

        // Test that a locations domain exception is ignored
        [TestMethod]
        public void GetMealPlanInvalidLocations()
        {
            string mpRecordKey = "77";
            DataContracts.MealPlans mockMealPlans = new MealPlans();
            mockMealPlans.Recordkey = mpRecordKey;
            mockMealPlans.MealStartDate = new DateTime(2010, 1, 1);
            mockMealPlans.MealDesc = "a meal plan";
            // Blank locations
            mockMealPlans.MealLocations = new List<string>() { "", null };
            dataAccessorMock.Setup<DataContracts.MealPlans>(acc => acc.ReadRecord<DataContracts.MealPlans>(mpRecordKey, true)).Returns(mockMealPlans);
            MealPlan mealPlan = mealPlanRepo.Get(mpRecordKey);
        }

        // Test that a meal types domain exception is ignored
        [TestMethod]
        public void GetMealPlanInvalidMealTypes()
        {
            string mpRecordKey = "77";
            DataContracts.MealPlans mockMealPlans = new MealPlans();
            mockMealPlans.Recordkey = mpRecordKey;
            mockMealPlans.MealStartDate = new DateTime(2010, 1, 1);
            mockMealPlans.MealDesc = "a meal plan";
            // Blank meal types
            mockMealPlans.MealTypes = new List<string>() { "", null };
            dataAccessorMock.Setup<DataContracts.MealPlans>(acc => acc.ReadRecord<DataContracts.MealPlans>(mpRecordKey, true)).Returns(mockMealPlans);
            MealPlan mealPlan = mealPlanRepo.Get(mpRecordKey);
        }

        // Test that a meal plan rates and charge code exception is ignored
        [TestMethod]
        public void GetMealPlanInvalidRatesAndChargeCode()
        {
            string mpRecordKey = "77";
            DataContracts.MealPlans mockMealPlans = new MealPlans();
            mockMealPlans.Recordkey = mpRecordKey;
            mockMealPlans.MealStartDate = new DateTime(2010, 1, 1);
            mockMealPlans.MealDesc = "a meal plan";
            // Charge code but no meal plan rates
            mockMealPlans.MealArCode = "AR1";
            dataAccessorMock.Setup<DataContracts.MealPlans>(acc => acc.ReadRecord<DataContracts.MealPlans>(mpRecordKey, true)).Returns(mockMealPlans);
            MealPlan mealPlan = mealPlanRepo.Get(mpRecordKey);
        }



        // A successfull Get, making sure the returned entity is properly populated with the data from the 
        // data reader. This exercises the mapping of the datareader object to the domain entity object.
        [TestMethod]
        public void GetMealPlanAssignment()
        {
            Ellucian.Colleague.Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignment = mealPlanRepo.GetMealPlanAssignment(mealPlanRecordKey);
            Assert.AreEqual(mealPlanRecordKey, mealPlanAssignment.Id);
            Assert.AreEqual(mpasExternalId.Count, mealPlanAssignment.ExternalIds.Count);
            for (int i = 0; i < mpasExternalId.Count; i++)
            {
                Assert.AreEqual(mpasExternalId[i], mealPlanAssignment.ExternalIds[i].ExternalId);
                Assert.AreEqual(mpasExternalIdSource[i], mealPlanAssignment.ExternalIds[i].ExternalIdSource);
            }
            Assert.AreEqual(mpasPersonId, mealPlanAssignment.PersonId);
            Assert.AreEqual(mpasMealPlan, mealPlanAssignment.MealPlanId);
            Assert.AreEqual(mpasStartDate, mealPlanAssignment.StartDate);
            Assert.AreEqual(mpasEndDate, mealPlanAssignment.EndDate);
            Assert.AreEqual(mpasMealCard, mealPlanAssignment.CardNumber);
            Assert.AreEqual(mpasTerm, mealPlanAssignment.TermId);
            Assert.AreEqual(mpasNoRatePeriods, mealPlanAssignment.NumberOfRatePeriods);
            Assert.AreEqual(mpasUsedRatePeriods, mealPlanAssignment.UsedRatePeriods);
            Assert.AreEqual(mpasUsedPct, mealPlanAssignment.UsedPercent);
            Assert.AreEqual(mpasOverrideRate, mealPlanAssignment.OverrideRate);
            Assert.AreEqual(mpasRateOverrideReason, mealPlanAssignment.OverrideRateReason);
            Assert.AreEqual(mpasOverrideArCode, mealPlanAssignment.OverrideChargeCode);
            Assert.AreEqual(mpasOverrideArType, mealPlanAssignment.OverrideReceivableType);
            Assert.AreEqual(mpasOverrideRefundFormula, mealPlanAssignment.OverrideRefundFormula);
            Assert.AreEqual(mpasComments, mealPlanAssignment.Comments);
            Assert.AreEqual(mpasStatus[0], mealPlanAssignment.CurrentStatusCode);
            Assert.AreEqual(mpasStatusDate[0], mealPlanAssignment.CurrentStatusDate);

        }

        // Test the detection of a failed read in the Get operation
        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void GetMealPlanAssignmentByBadId()
        {
            Ellucian.Colleague.Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignment = mealPlanRepo.GetMealPlanAssignment("X");
        }

        // Test the detection of a null start date
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GetMealPlanAssignmentNoStartDate()
        {
            Ellucian.Colleague.Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignment = mealPlanRepo.GetMealPlanAssignment(mpasNoStartDateKey);
        }

        // Test the detection of a null number of rate periods
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GetMealPlanAssignmentNoNumberOfRatePeriods()
        {
            Ellucian.Colleague.Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignment = mealPlanRepo.GetMealPlanAssignment(mpasNoNumberOfRatePeriodsRecordKey);
        }

        // Test the detection of no status date
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void GetMealPlanAssignmentNoStatusDate()
        {
            Ellucian.Colleague.Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignment = mealPlanRepo.GetMealPlanAssignment(mpasNoStatusDateKey);
        }


        // Build mock records to be returned by the mock data reader.
        private void BuildMockMealPlanRecords()
        {
            // Setup the data accessor mock to return a sample MEAL.PLANS record
            MealPlans mealPlan = new DataContracts.MealPlans();
            mealPlan.MealArCode = mealArCode;
            mealPlan.MealBldgs = new List<string>();
            mealPlan.MealBldgs.AddRange(mealBldgs);
            mealPlan.MealCancelArCode = mealCancelArCode;
            mealPlan.MealClass = mealClass;
            mealPlan.MealComments = mealComments;
            mealPlan.MealDesc = mealDesc;
            mealPlan.MealEndDate = mealEndDate;
            mealPlan.MealFrequency = MealFrequency;
            mealPlan.MealLocations  = new List<string>(mealLocations);
            mealPlan.MealNoTimes = MealNoTimes;
            mealPlan.MealRateEffectiveDates = new List<DateTime?>(mealRateEffectiveDates);
            mealPlan.MealRatePeriod = mealRatePeriod;
            mealPlan.MealRates = new List<decimal?>(mealRates);
            mealPlan.MealRooms = new List<string>(mealRooms);
            mealPlan.MealStartDate = mealStartDate;
            mealPlan.MealTypes = new List<string>(mealTypes);
            mealPlan.Recordkey = mealPlanRecordKey;
            mealPlan.buildAssociations();
            
            dataAccessorMock.Setup<DataContracts.MealPlans>(acc => acc.ReadRecord<DataContracts.MealPlans>(mealPlanRecordKey, true)).Returns(mealPlan);

            // Mock the return of a meal plan with no start date
            MealPlans mealPlanNoStartdate = new DataContracts.MealPlans();
            // Copy mealPlan
            foreach (System.Reflection.PropertyInfo pi in mealPlan.GetType().GetProperties())
            {
                pi.SetValue(mealPlanNoStartdate,pi.GetValue(mealPlan,null),null);
            }
            mealPlanNoStartdate.MealStartDate = null;
            dataAccessorMock.Setup<DataContracts.MealPlans>(acc => acc.ReadRecord<DataContracts.MealPlans>(mealPlanNoStartDateKey, true)).Returns(mealPlanNoStartdate);

            // Mock the return of a meal plan with no description
            MealPlans mealPlanNoDesc = new DataContracts.MealPlans();
            // Copy mealPlan
            foreach (System.Reflection.PropertyInfo pi in mealPlan.GetType().GetProperties())
            {
                pi.SetValue(mealPlanNoDesc, pi.GetValue(mealPlan, null), null);
            }
            mealPlanNoDesc.MealDesc = null;
            dataAccessorMock.Setup<DataContracts.MealPlans>(acc => acc.ReadRecord<DataContracts.MealPlans>(mealPlanNoDescKey, true)).Returns(mealPlanNoDesc);

            return;
        }

        // Build mock records to be returned by the mock data reader.
        private void BuildMockMealPlanAssignmentRecords()
        {
            // Setup the data accessor mock to return a sample MEAL.PLAN.ASSIGNMENT record
            DataContracts.MealPlanAssignment mealPlanAssignment = new DataContracts.MealPlanAssignment();
            mealPlanAssignment.MpasPersonId = mpasPersonId;
            mealPlanAssignment.MpasMealPlan = mpasMealPlan;
            mealPlanAssignment.MpasStartDate = mpasStartDate;
            mealPlanAssignment.MpasEndDate = mpasEndDate;
            mealPlanAssignment.MpasMealCard = mpasMealCard;
            mealPlanAssignment.MpasTerm = mpasTerm;
            mealPlanAssignment.MpasNoRatePeriods = mpasNoRatePeriods;
            mealPlanAssignment.MpasOverrideRate = mpasOverrideRate;
            mealPlanAssignment.MpasRateOverrideReason = mpasRateOverrideReason;
            mealPlanAssignment.MpasOverrideArCode = mpasOverrideArCode;
            mealPlanAssignment.MpasOverrideArType = mpasOverrideArType;
            mealPlanAssignment.MpasOverrideRefundFormula = mpasOverrideRefundFormula;
            mealPlanAssignment.MpasComments = mpasComments;
            mealPlanAssignment.MpasStatus = mpasStatus;
            mealPlanAssignment.MpasStatusDate = mpasStatusDate;
            mealPlanAssignment.MpasUsedRatePeriods = mpasUsedRatePeriods;
            mealPlanAssignment.MpasUsedPct = mpasUsedPct;
            mealPlanAssignment.MpasExternalIdSource = mpasExternalIdSource;
            mealPlanAssignment.MpasExternalId = mpasExternalId;
            mealPlanAssignment.Recordkey = mealPlanRecordKey;
            mealPlanAssignment.buildAssociations();

            dataAccessorMock.Setup<DataContracts.MealPlanAssignment>(
                acc => acc.ReadRecord<DataContracts.MealPlanAssignment>(mpasRecordKey, true)).Returns(mealPlanAssignment);

            // Mock the return of a meal plan assignment with no number of rate periods
            DataContracts.MealPlanAssignment mealPlanAssignmentNoRatePeriod = new DataContracts.MealPlanAssignment();
            // Copy mealPlanAssignment
            foreach (System.Reflection.PropertyInfo pi in mealPlanAssignment.GetType().GetProperties())
            {
                pi.SetValue(mealPlanAssignmentNoRatePeriod, pi.GetValue(mealPlanAssignment, null), null);
            }
            mealPlanAssignmentNoRatePeriod.MpasNoRatePeriods = null;
            dataAccessorMock.Setup<DataContracts.MealPlanAssignment>(acc => acc.ReadRecord<DataContracts.MealPlanAssignment>(mpasNoNumberOfRatePeriodsRecordKey, true)).Returns(mealPlanAssignmentNoRatePeriod);

            // Mock the return of a meal plan assignment with no start date
            DataContracts.MealPlanAssignment mealPlanAssignmentNoStartDate = new DataContracts.MealPlanAssignment();
            // Copy mealPlanAssignment
            foreach (System.Reflection.PropertyInfo pi in mealPlanAssignment.GetType().GetProperties())
            {
                pi.SetValue(mealPlanAssignmentNoStartDate, pi.GetValue(mealPlanAssignment, null), null);
            }
            mealPlanAssignmentNoStartDate.MpasStartDate = null;
            dataAccessorMock.Setup<DataContracts.MealPlanAssignment>(acc => acc.ReadRecord<DataContracts.MealPlanAssignment>(mpasNoStartDateKey, true)).Returns(mealPlanAssignmentNoStartDate);

            // Mock the return of a meal plan assignment with no status date
            DataContracts.MealPlanAssignment mealPlanAssignmentNoStatusDate = new DataContracts.MealPlanAssignment();
            // Copy mealPlanAssignment
            foreach (System.Reflection.PropertyInfo pi in mealPlanAssignment.GetType().GetProperties())
            {
                pi.SetValue(mealPlanAssignmentNoStatusDate, pi.GetValue(mealPlanAssignment, null), null);
            }
            mealPlanAssignmentNoStatusDate.MpasStatusDate = new List<DateTime?>();
            dataAccessorMock.Setup<DataContracts.MealPlanAssignment>(acc => acc.ReadRecord<DataContracts.MealPlanAssignment>(mpasNoStatusDateKey, true)).Returns(mealPlanAssignmentNoStatusDate);

            // Mock the return of the meal plan referened by mpasMealPlan.
            // Only needs ID and required properties.
            DataContracts.MealPlans mockMealPlans = new MealPlans();
            mockMealPlans.Recordkey = mpasMealPlan;
            mockMealPlans.MealStartDate = new DateTime(2010, 1, 1);
            mockMealPlans.MealDesc = "a meal plan";
            dataAccessorMock.Setup<DataContracts.MealPlans>(acc => acc.ReadRecord<DataContracts.MealPlans>(mpasMealPlan, true)).Returns(mockMealPlans);

            return;
        }

    }
}
