﻿using Ellucian.Colleague.Data.ResidenceLife.DataContracts;
using Ellucian.Colleague.Data.ResidenceLife.Repositories;
using Ellucian.Colleague.Data.ResidenceLife.Transactions;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;
using Ellucian.Data.Colleague;
using Ellucian.Web.Cache;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using slf4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace Ellucian.Colleague.Data.ResidenceLife.Tests.Repositories
{
    [TestClass]
    public class HousingAssignmentRepositoryTests
    {
        // Mock objects for the repository
        Mock<IColleagueDataReader> dataAccessorMock;
        Mock<IColleagueTransactionFactory> transFactoryMock;
        Mock<ICacheProvider> cacheProviderMock;
        Mock<ILogger> loggerMock;
        Mock<IColleagueTransactionInvoker> txInvokerMock;

        // Repository instance that will be constructed with mock objects
        HousingAssignmentRepository housingAssignmentRepo;

        // Sample data for a housing assignment
        string recordkey = "2";
        string rmasBldg = "EAR";
        string rmasComments = "A really long comment.";
        string rmasContract = "12345";
        DateTime? rmasEndDate = new DateTime(2014, 12, 31);
        string[] rmasExternalId = new string[]  {"EXT1", "EXT2"};
        string[] rmasExternalIdSource = { "RMS", "HD" };
        string rmasOverrideArCode = "AB";
        string rmasOverrideArType = "AT";
        decimal? rmasOverrideRate = 23.44M;
        string rmasOverrideRefundFormula = "REFF";
        string rmasPersonId = "3493939";
        string rmasRateOverrideReason = "REA1";
        string rRmasRatePeriod = "Y";
        string rmasResidentStaffIndic = "S";
        string rmasRoom = "101";
        string rmasRoomRateTable = "EARA1";
        DateTime? rmasStartDate = new DateTime(2014, 7, 1);
        string[] rmasStatus =  { "A", "R" };
        DateTime?[] rmasStatusDate =  { new DateTime(2014, 6, 1), new DateTime(2013, 11, 1) };
        string rmasTerm = "2014/FA";

        string[] arAddnlAmtRecordKeys = { "1", "2" };
        string[] araaArCode = {"AA1", "AA2"};
        string[] araaDesc = {"ARDESC1", "ARDESC2"};
        decimal?[] araaChargeAmt = {11M, null};
        decimal?[] araaCrAmt = {null, 44M};


        [TestInitialize]
        public void Initialize()
        {
            // Initialize mock objects used to construct a repository
            loggerMock = new Mock<ILogger>();
            cacheProviderMock = new Mock<ICacheProvider>();
            dataAccessorMock = new Mock<IColleagueDataReader>();
            transFactoryMock = new Mock<IColleagueTransactionFactory>();
            txInvokerMock = new Mock<IColleagueTransactionInvoker>();
            
            // Build mock records within dataAccessMock
            BuildMockRecords();

            // CREATE.UPDATE.HOUSING.ASSIGNMENT CTX mock for success
            CreateUpdateRoomAssignmentResponse createUpdateCTXResponse1 = new CreateUpdateRoomAssignmentResponse();
            createUpdateCTXResponse1.AError = "0";
            createUpdateCTXResponse1.AId = recordkey;
            createUpdateCTXResponse1.AlErrorMsg = null;
            txInvokerMock.Setup(inv => inv.Execute<CreateUpdateRoomAssignmentRequest, CreateUpdateRoomAssignmentResponse>
                (It.Is<CreateUpdateRoomAssignmentRequest>(r => r.AlExternalId[0] == "EXTL1"))).Returns(createUpdateCTXResponse1);

            // CREATE.UPDATE.HOUSING.ASSIGNMENT CTX mock for failure
            CreateUpdateRoomAssignmentResponse createUpdateCTXResponse2 = new CreateUpdateRoomAssignmentResponse();
            createUpdateCTXResponse2.AError = "1";
            createUpdateCTXResponse2.AId = null;
            createUpdateCTXResponse2.AlErrorMsg.Add("You broke a rule.");
            txInvokerMock.Setup(inv => inv.Execute<CreateUpdateRoomAssignmentRequest, CreateUpdateRoomAssignmentResponse>
                (It.Is<CreateUpdateRoomAssignmentRequest>(r => r.AlExternalId[0] == "EXTL2"))).Returns(createUpdateCTXResponse2);

            // Set the transaction factory to return our dataAccessMock as the data reader.
            transFactoryMock.Setup(transFac => transFac.GetDataReader()).Returns(dataAccessorMock.Object);
            // Set the transaction factory to return our txInvokerMock as the transaction invoker.
            transFactoryMock.Setup(transFac => transFac.GetTransactionInvoker()).Returns(txInvokerMock.Object);
            
            // Instantiate the repository
            housingAssignmentRepo = new HousingAssignmentRepository(cacheProviderMock.Object, transFactoryMock.Object, loggerMock.Object);
        }

        // A successfull Get, making sure the returned entity is properly populated with the data from the 
        // data reader. This exercises the mapping of the datareader object to the domain entity object.
        [TestMethod]
        public void GetHousingAssignmentById()
        {
            HousingAssignment housingAssignment = housingAssignmentRepo.Get(recordkey);
            Assert.AreEqual(rmasBldg,housingAssignment.Building);
            Assert.AreEqual(rmasComments, housingAssignment.Comments);
            Assert.AreEqual(rmasContract, housingAssignment.Contract);
            Assert.AreEqual(rmasEndDate, housingAssignment.EndDate);
            Assert.AreEqual(rmasExternalId[0], housingAssignment.ExternalIds[0].ExternalId);
            Assert.AreEqual(rmasExternalIdSource[0], housingAssignment.ExternalIds[0].ExternalIdSource);
            Assert.AreEqual(rmasExternalId[1], housingAssignment.ExternalIds[1].ExternalId);
            Assert.AreEqual(rmasExternalIdSource[1], housingAssignment.ExternalIds[1].ExternalIdSource);
            Assert.AreEqual(rmasOverrideArCode,housingAssignment.OverrideChargeCode);
            Assert.AreEqual(rmasOverrideArType,housingAssignment.OverrideReceivableType);
            Assert.AreEqual(rmasOverrideRate, housingAssignment.OverrideRate);
            Assert.AreEqual(rmasOverrideRefundFormula, housingAssignment.OverrideRefundFormula);
            Assert.AreEqual(rmasPersonId, housingAssignment.PersonId);
            Assert.AreEqual(rmasRateOverrideReason, housingAssignment.OverrideRateReason);
            Assert.AreEqual(RoomRatePeriods.Yearly, housingAssignment.RoomRatePeriod);
            Assert.AreEqual(rmasResidentStaffIndic, housingAssignment.ResidentStaffIndicator);
            Assert.AreEqual(rmasRoom, housingAssignment.Room);
            Assert.AreEqual(rmasRoomRateTable, housingAssignment.RoomRateTable);
            Assert.AreEqual(rmasStartDate, housingAssignment.StartDate);
            Assert.AreEqual(rmasStatus[0], housingAssignment.CurrentStatus);
            Assert.AreEqual(rmasStatusDate[0], housingAssignment.CurrentStatusDate);
            Assert.AreEqual(rmasTerm, housingAssignment.TermId);
            Assert.AreEqual(araaArCode[0], housingAssignment.AdditionalChargesOrCredits[0].ArCode);
            Assert.AreEqual(araaDesc[0], housingAssignment.AdditionalChargesOrCredits[0].Description);
            Assert.AreEqual(araaChargeAmt[0], housingAssignment.AdditionalChargesOrCredits[0].Amount);
            Assert.AreEqual(araaArCode[1], housingAssignment.AdditionalChargesOrCredits[1].ArCode);
            Assert.AreEqual(araaDesc[1], housingAssignment.AdditionalChargesOrCredits[1].Description);
            Assert.AreEqual(-(araaCrAmt[1]), housingAssignment.AdditionalChargesOrCredits[1].Amount);
        }

        // Test the detection a failed read in the Get operation
        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void GetHousingAssignmentByBadId()
        {
            HousingAssignment housingAssignment = housingAssignmentRepo.Get("X");
        }

        // Test the detection a failed read of AR.ADDNL.AMTS in the Get operation
        [TestMethod]
        [ExpectedException(typeof(KeyNotFoundException))]
        public void GetHousingAssignmentByIdBadAddnlAmtId()
        {
            HousingAssignment housingAssignment = housingAssignmentRepo.Get("BAD.ADDNL.ID");
        }

        // Test the detection of a successful Add operation, and return of the added entity.
        // (The add and get are mocked, so this doesn't test mapping of the get attributes to the domain attribute,
        // nor the mapping of the domain attributes to the request attributes.)
        [TestMethod]
        public void AddHousingAssignmentSuccess()
        {
            HousingAssignment housingAssignment = new HousingAssignment("personid", new DateTime(2014,1,1), new DateTime(2014,5,31),
                "A", DateTime.Now);
            housingAssignment.AddExternalId("EXTL1", "HD");
            HousingAssignment housingAssignmentResult = housingAssignmentRepo.Add(housingAssignment);
            Assert.AreEqual(rmasPersonId, housingAssignmentResult.PersonId);
        }

        // Test the detection of a successful Update operation, and return of the updated entity.
        // (The add and get are mocked, so this doesn't test mapping of the get attributes to the domain attribute,
        // nor the mapping of the domain attributes to the request attributes.)
        [TestMethod]
        public void UpdateHousingAssignmentSuccess()
        {
            HousingAssignment housingAssignment = new HousingAssignment("personid", new DateTime(2014, 1, 1), new DateTime(2014, 5, 31),
                "A", DateTime.Now);
            housingAssignment.AddExternalId("EXTL1", "HD");
            HousingAssignment housingAssignmentResult = housingAssignmentRepo.Add(housingAssignment);
            Assert.AreEqual(rmasPersonId, housingAssignmentResult.PersonId);
        }

        // Test the detection of a failed CTX in the Add operation.
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void CreateHousingAssignmentFailure()
        {
            HousingAssignment housingAssignment = new HousingAssignment("personid", new DateTime(2014, 1, 1), new DateTime(2014, 5, 31),
                "A", DateTime.Now);
            housingAssignment.AddExternalId("EXTL2", "HD");
            HousingAssignment housingAssignmentResult = housingAssignmentRepo.Add(housingAssignment);
            Assert.AreEqual(null, housingAssignmentResult);
        }

        // Test the detection of a failed CTX in the Update operation.
        [TestMethod]
        [ExpectedException(typeof(ApplicationException))]
        public void UpdateHousingAssignmentFailure()
        {
            HousingAssignment housingAssignment = new HousingAssignment("personid", new DateTime(2014, 1, 1), new DateTime(2014, 5, 31),
                "A", DateTime.Now);
            housingAssignment.AddExternalId("EXTL2", "HD");
            HousingAssignment housingAssignmentResult = housingAssignmentRepo.Add(housingAssignment);
            Assert.AreEqual(null, housingAssignmentResult);
        }

        // Test the error of a record ID passed to the Add operation
        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void CreateHousingAssignmentWithId()
        {
            HousingAssignment housingAssignment = new HousingAssignment("personid", new DateTime(2014, 1, 1), new DateTime(2014, 5, 31),
                "A", DateTime.Now);
            housingAssignment.AddExternalId("EXTL2", "HD");
            housingAssignment.Id = "AB";
            HousingAssignment housingAssignmentResult = housingAssignmentRepo.Add(housingAssignment);
            Assert.AreEqual(null, housingAssignmentResult);
        }

        // Build mock records to be returned by the mock data reader.
        private void BuildMockRecords()
        {

            // Setup the data accessor mock to return a sample ROOM.ASSIGNMENT record
            RoomAssignment roomAssignment = new DataContracts.RoomAssignment();
            roomAssignment.Recordkey = recordkey;
            roomAssignment.RmasBldg = rmasBldg;
            roomAssignment.RmasComments = rmasComments;
            roomAssignment.RmasContract = rmasContract;
            roomAssignment.RmasEndDate = rmasEndDate;
            roomAssignment.RmasExternalId = new System.Collections.Generic.List<string>(rmasExternalId);
            roomAssignment.RmasExternalIdSource = new System.Collections.Generic.List<string>(rmasExternalIdSource);
            roomAssignment.RmasOverrideArCode = rmasOverrideArCode;
            roomAssignment.RmasOverrideArType = rmasOverrideArType;
            roomAssignment.RmasOverrideRate = rmasOverrideRate;
            roomAssignment.RmasOverrideRefundFormula = rmasOverrideRefundFormula;
            roomAssignment.RmasPersonId = rmasPersonId;
            roomAssignment.RmasRateOverrideReason = rmasRateOverrideReason;
            roomAssignment.RmasRatePeriod = rRmasRatePeriod;
            roomAssignment.RmasResidentStaffIndic = rmasResidentStaffIndic;
            roomAssignment.RmasRoom = rmasRoom;
            roomAssignment.RmasRoomRateTable = rmasRoomRateTable;
            roomAssignment.RmasStartDate = rmasStartDate;
            roomAssignment.RmasStatus = new System.Collections.Generic.List<string>(rmasStatus);
            roomAssignment.RmasStatusDate = new System.Collections.Generic.List<DateTime?>(rmasStatusDate);
            roomAssignment.RmasTerm = rmasTerm;
            roomAssignment.RmasAddnlAmts = new System.Collections.Generic.List<string>(arAddnlAmtRecordKeys);
            roomAssignment.buildAssociations();

            dataAccessorMock.Setup<DataContracts.RoomAssignment>(acc => acc.ReadRecord<DataContracts.RoomAssignment>(recordkey, true)).Returns(roomAssignment);

            // Setup the data accessor mock to return the two AR.ADDNL.AMTS records pointed to from the room assignment
            Collection<ArAddnlAmts> arAddnlAmtsCol = new Collection<ArAddnlAmts>();           
            ArAddnlAmts arAddnlAmts = new ArAddnlAmts();
            arAddnlAmts.Recordkey = arAddnlAmtRecordKeys[0];
            arAddnlAmts.AraaArCode = araaArCode[0];
            arAddnlAmts.AraaDesc = araaDesc[0];
            arAddnlAmts.AraaChargeAmt = araaChargeAmt[0];
            arAddnlAmts.AraaCrAmt = araaCrAmt[0];
            arAddnlAmtsCol.Add(arAddnlAmts);

            arAddnlAmts = new ArAddnlAmts();
            arAddnlAmts.Recordkey = arAddnlAmtRecordKeys[1];
            arAddnlAmts.AraaArCode = araaArCode[1];
            arAddnlAmts.AraaDesc = araaDesc[1];
            arAddnlAmts.AraaChargeAmt = araaChargeAmt[1];
            arAddnlAmts.AraaCrAmt = araaCrAmt[1];
            arAddnlAmtsCol.Add(arAddnlAmts);

            dataAccessorMock.Setup<ICollection<DataContracts.ArAddnlAmts>>(acc => acc.BulkReadRecord<DataContracts.ArAddnlAmts>(arAddnlAmtRecordKeys, true)).Returns(arAddnlAmtsCol);

            // Create a room assignment record with a bad addnl amt ID, using room assignment ID "BAD.ADDNL.ID"
            RoomAssignment roomAssignmentBAI = new RoomAssignment();
            roomAssignmentBAI.Recordkey = "BAD.ADDNL.ID";
            roomAssignmentBAI.RmasPersonId = "1234";
            roomAssignmentBAI.RmasEndDate = rmasEndDate;
            roomAssignmentBAI.RmasStartDate = rmasStartDate;
            roomAssignmentBAI.RmasStatus = new System.Collections.Generic.List<string>(rmasStatus);
            roomAssignmentBAI.RmasStatusDate = new System.Collections.Generic.List<DateTime?>(rmasStatusDate);
            roomAssignmentBAI.RmasAddnlAmts = new System.Collections.Generic.List<string>();
            roomAssignmentBAI.RmasAddnlAmts.Add("NO.EXIST");
            roomAssignmentBAI.buildAssociations();

            dataAccessorMock.Setup<DataContracts.RoomAssignment>(acc => acc.ReadRecord<DataContracts.RoomAssignment>("BAD.ADDNL.ID", true)).Returns(roomAssignmentBAI);

            return;
        }
    }
}
