﻿// Copyright 2014-2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.Base
{
    [Serializable]
    public static class BasePermissionCodes
    {
        // Access to create person
        public const string CreatePerson = "CREATE.PERSON";

        // Access to update a person
        public const string UpdatePerson = "UPDATE.PERSON";

        //Acces to create a organization
        public const string CreateOrganization = "CREATE.ORGANIZATION";

        //Acces to update a organization
        public const string UpdateOrganization = "UPDATE.ORGANIZATION";

        //Acces to view a organization
        public const string ViewOrganization = "VIEW.ANY.ORGANIZATION";

        // Access to view any person's data
        public const string ViewAnyPerson = "VIEW.ANY.PERSON";

        // Access to view integration configuration information
        public const string ViewIntegrationConfig = "VIEW.INTEGRATION.CONFIG";

        // Enables a person to update their own email addresses.
        // Used in endpoint UpdatePersonProfile: PUT persons/{personId} application/vnd.ellucian-person-profile.v{}+json
        public const string UpdateOwnEmail = "UPDATE.OWN.EMAIL";

        // Enables a person to update their own phone numbers
        // Used in endpoint UpdatePersonProfile: PUT persons/{personId} application/vnd.ellucian-person-profile.v{}+json
        public const string UpdateOwnPhone = "UPDATE.OWN.PHONE";

        // Enables a person to update their own phone numbers
        // Used in endpoint UpdatePersonProfile: PUT persons/{personId} application/vnd.ellucian-person-profile.v{}+json
        public const string UpdateOwnAddress = "UPDATE.OWN.ADDRESS";

        // Access to view any GL Accounts
        public const string ViewAnyGlAccts = "VIEW.ANY.GL.ACCTS";

        // Access to view any Projects
        public const string ViewAnyProjects = "VIEW.ANY.PROJECTS";

        // Access to view external employments
        public const string ViewAnyExternalEmployments = "VIEW.EXTERNAL.EMPLOYMENTS";

        // Access to view any Projects Line Items
        public const string ViewAnyProjectsLineItems = "VIEW.ANY.PROJECTS.LINE.ITEMS";

        // Enables a vender to create, update, or delete banking information
        public const string EditVendorBankingInformation = "EDIT.VENDOR.BANKING.INFORMATION";

        // Enables an employee to create, update, or delete banking information
        public const string EditEChecksBankingInformation = "EDIT.ECHECKS.BANKING.INFORMATION";

        // Enables access to view others' emergency and missing person contacts
        // Used in endpoint GetEmergencyInformation: GET /persons/{personId}/emergency-information
        public const string ViewPersonEmergencyContacts = "VIEW.PERSON.EMERGENCY.CONTACTS";

        // Enables access to view others' health conditions
        // Used in endpoint GetEmergencyInformation: GET /persons/{personId}/emergency-information
        public const string ViewPersonHealthConditions = "VIEW.PERSON.HEALTH.CONDITIONS";

        // Enables access to view others' additional emergency information (e.g. insurance, hospital)
        // Used in endpoint GetEmergencyInformation: GET /persons/{personId}/emergency-information
        public const string ViewPersonOtherEmergencyInformation = "VIEW.PERSON.OTHER.EMERGENCY.INFORMATION";

        // Enables access to view others' Person Restrictions
        // Used in endpoint GetStudentRestrictions: GET students/{studentId}/restrictions
        public const string ViewPersonRestrictions = "VIEW.PERSON.RESTRICTIONS";

        // Enables a person to update the organizational relationships (assign managers/subordinates)
        // Used in endpoints for modifying organizational relationships
        // CreateOrganizationalRelationship: POST /organizational-relationships
        // UpdateOrganizationalRelationship: POST /organizational-relationships/{id}
        // DeleteOrganizationalRelationship: DELETE /organizational-relationships/{id}
        public const string UpdateOrganizationalRelationships = "UPDATE.ORGANIZATIONAL.RELATIONSHIPS";
    }
}