﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Domain.Entities;

namespace Ellucian.Colleague.Domain.Base.Entities
{
    /// <summary>
    /// Building type codes
    /// </summary>
    [Serializable]
    public class Buyer
    {
        /// <summary>
        /// ID of Staff record
        /// </summary>
        public string RecordKey { get; set; }

        /// <summary>
        /// GUID for Staff record
        /// </summary>
        public string Guid { get; set; }

        /// <summary>
        /// First and Last name of Staff
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// PersonId of Staff record
        /// </summary>
        public string PersonGuid { get; set; }

        /// <summary>
        /// Status of Staff record
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// The Add date of the staff record
        /// </summary>
        public DateTime? StartOn { get; set; }

        /// <summary>
        /// The Change Date of the Staff record
        /// </summary>
        public DateTime? EndOn { get; set; }

    }
}
