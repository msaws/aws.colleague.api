﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;

namespace Ellucian.Colleague.Domain.Base.Entities
{

    [Serializable]
    public class AcademicCredential
    {

        private string _guid;
        private string _id;

        /// <summary>
        /// CDD Name: ACAD.PERSON.ID
        /// </summary>       
        public string AcadPersonId { get; set; }

        /// <summary>
        /// CDD Name: ACAD.INSTITUTIONS.ID
        /// </summary>        
        public string AcadInstitutionsId { get; set; }

        /// <summary>
        /// CDD Name: ACAD.START.DATE
        /// </summary>        
        public DateTime? AcadStartDate { get; set; }

        /// <summary>
        /// CDD Name: ACAD.END.DATE
        /// </summary>      
        public DateTime? AcadEndDate { get; set; }

        /// <summary>
        /// CDD Name: ACAD.DEGREE
        /// </summary>
         public string AcadDegree { get; set; }

        /// <summary>
        /// CDD Name: ACAD.DEGREE.DATE
        /// </summary>
         public DateTime? AcadDegreeDate { get; set; }

        /// <summary>
        /// CDD Name: ACAD.CCD
        /// </summary>
       public List<string> AcadCcd { get; set; }

        /// <summary>
        /// CDD Name: ACAD.MAJORS
        /// </summary>
        public List<string> AcadMajors { get; set; }

        /// <summary>
        /// CDD Name: ACAD.MINORS
        /// </summary>
        public List<string> AcadMinors { get; set; }

        /// <summary>
        /// CDD Name: ACAD.SPECIALIZATION
        /// </summary>
         public List<string> AcadSpecialization { get; set; }

        /// <summary>
        /// CDD Name: ACAD.HONORS
        /// </summary>
        public List<string> AcadHonors { get; set; }

        /// <summary>
        /// CDD Name: ACAD.AWARDS
        /// </summary>
         public List<string> AcadAwards { get; set; }

        /// <summary>
        /// CDD Name: ACAD.NO.YEARS
        /// </summary>
       public int? AcadNoYears { get; set; }

        /// <summary>
        /// CDD Name: ACAD.COMMENCEMENT.DATE
        /// </summary>
         public DateTime? AcadCommencementDate { get; set; }

        /// <summary>
        /// CDD Name: ACAD.GPA
        /// </summary>
         public Decimal? AcadGpa { get; set; }

        /// <summary>
        /// CDD Name: ACAD.THESIS
        /// </summary>
         public string AcadThesis { get; set; }

        /// <summary>
        /// CDD Name: ACAD.COMMENTS
        /// </summary>
        public string AcadComments { get; set; }

        /// <summary>
        /// CDD Name: ACAD.ACAD.PROGRAM
        /// </summary>
        public string AcadAcadProgram { get; set; }
		

        public string Id
        {
            get { return _id; }
            set
            {
                if (string.IsNullOrEmpty(_id))
                {
                    _id = value;
                }
                else
                {
                    throw new InvalidOperationException("AcademicCredential Id cannot be changed");
                }
            }
        }

        /// <summary>
        /// GUID for the remark; not required, but cannot be changed once assigned.
        /// </summary>
        public string Guid
        {
            get { return _guid; }
            set
            {
                if (string.IsNullOrEmpty(_guid))
                {
                    if (!string.IsNullOrEmpty(value))
                    {
                        _guid = value.ToLowerInvariant();
                    }
                }
                else
                {
                    throw new InvalidOperationException("Cannot change value of Guid.");
                }
            }
        }

        public AcademicCredential(string guid)
        {
            if (string.IsNullOrEmpty(guid))
            {
                throw new ArgumentNullException("AcademicCredential guid can not be null or empty");
            }
            _guid = guid;
        }
    }
}