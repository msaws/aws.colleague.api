﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Domain.Base.Entities
{
    /// <summary>
    /// Campus Calendar used for scheduling purposes
    /// </summary>
    [Serializable]
    public class CampusCalendar
    {
        private readonly string _id;
        private readonly string _description;
        private readonly DateTimeOffset _defaultStartOfDay;
        private readonly DateTimeOffset _defaultEndOfDay;
        private List<DateTime> _specialDays = new List<DateTime>();

        public string Id { get { return _id; } }
        public string Description { get { return _description; } }
        public DateTimeOffset DefaultStartOfDay { get { return _defaultStartOfDay; } }
        public DateTimeOffset DefaultEndOfDay { get { return _defaultEndOfDay; } }
        public ReadOnlyCollection<DateTime> SpecialDays { get; private set; }

        public int BookPastNumberOfDays { get; set; }

        /// <summary>
        /// Constructor for campus calendar
        /// </summary>
        /// <param name="id">Calendar ID</param>
        /// <param name="description">Description of calendar</param>
        /// <param name="defaultStartOfDay">Time of day at which conflict checking begins during scheduling</param>
        /// <param name="defaultEndOfDay">Time of day at which conflict checking ends during scheduling</param>
        public CampusCalendar(string id, string description, DateTimeOffset defaultStartOfDay, DateTimeOffset defaultEndOfDay)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }
            if (string.IsNullOrEmpty(description))
            {
                throw new ArgumentNullException("description");
            }
            if (defaultEndOfDay < defaultStartOfDay)
            {
                throw new ArgumentOutOfRangeException("defaultEndOfDay", "End of day cannot be earlier than start of day.");
            }

            _id = id;
            _description = description;
            _defaultStartOfDay = defaultStartOfDay;
            _defaultEndOfDay = defaultEndOfDay;

            SpecialDays = _specialDays.AsReadOnly();
        }

        /// <summary>
        /// Add a special day to the calendar
        /// </summary>
        /// <param name="day">Special Day</param>
        public void AddSpecialDay(DateTime day)
        {
            // Prevent duplicates
            if (!SpecialDays.Contains(day))
            {
                _specialDays.Add(day);
            }
        }
    }
}
