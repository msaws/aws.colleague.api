﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Ellucian.Colleague.Domain.Base.Entities
{
    /// <summary>
    /// There may be any number of instances of a specific position in an organization since several different
    /// persons may be assigned to the same position title. This entity represents one such instance
    /// of a position, including its relationships to other organizational position. This entity includes a 
    /// person ID when currently assigned to a person.
    /// </summary>
    [Serializable]
    public class OrganizationalPersonPosition
    {
        /// <summary>
        /// The unique Id of the person position. 
        /// </summary>
        public string Id { get { return id; } }
        private string id;

        /// <summary>
        /// The Id for the position
        /// </summary>
        public string PositionId { get { return positionId; } }
        private string positionId;

        /// <summary>
        /// The Title for this position
        /// </summary>
        public string PositionTitle { get { return positionTitle; } }
        private string positionTitle;

        /// <summary>
        /// Id of the person assigned to the position
        /// </summary>
        public string PersonId { get { return personId; } }
        private string personId { get; set; }

        /// <summary>
        /// List of relationships for this organizational position
        /// </summary>
        public ReadOnlyCollection<OrganizationalRelationship> Relationships;
        private readonly List<OrganizationalRelationship> relationships = new List<OrganizationalRelationship>();

        /// <summary>
        /// Initializes an existing base organizational position.
        /// </summary>
        /// <param name="positionId">The position role id</param>
        /// <param name="positionTitle">The position role title</param>
        public OrganizationalPersonPosition(string id, string personId, string positionId, string positionTitle)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }
            if (string.IsNullOrEmpty(personId))
            {
                throw new ArgumentNullException("personId");
            }
            if (string.IsNullOrEmpty(positionId))
            {
                throw new ArgumentNullException("positionId");
            }
            if (string.IsNullOrEmpty(positionTitle))
            {
                throw new ArgumentNullException("positionTitle");
            }
            this.id = id;
            this.personId = personId;
            this.positionId = positionId;
            this.positionTitle = positionTitle;
            Relationships = relationships.AsReadOnly();
        }

        /// <summary>
        /// Adds a known relationship to this organizational person position. 
        /// </summary>
        /// <param name="relationship">Relationship to add</param>
        public void AddRelationship(OrganizationalRelationship relationship)
        {
            if (relationship == null)
            {
                throw new ArgumentNullException("relationship", "Relationship cannot be null");
            }
            this.ValidateRelationship(relationship);
            this.relationships.Add(relationship);
        }

        /// <summary>
        /// Make sure that this relationship category does not already exist for the person
        /// and that this relationship involves this person.
        /// </summary>
        /// <param name="relationship"></param>
        private void ValidateRelationship(OrganizationalRelationship relationship)
        {
            if (relationship.OrganizationalPersonPositionId != this.Id && relationship.RelatedOrganizationalPersonPositionId != this.Id)
            {
                throw new ArgumentException("Cannot add a relationship that does not represent a relationship with this person position.", "relationship");
            }

            var existingRelationship = Relationships.Where(r => r.Category == relationship.Category && r.OrganizationalPersonPositionId == this.Id && relationship.OrganizationalPersonPositionId == this.Id).FirstOrDefault();
            if (existingRelationship != null)
            {
                throw new ArgumentException("Found duplicate relationship: category " + relationship.Category + " for position " + this.Id);
            }
        }
    }
}
