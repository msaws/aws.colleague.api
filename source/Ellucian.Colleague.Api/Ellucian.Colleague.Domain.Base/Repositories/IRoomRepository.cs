﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Domain.Base.Entities;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.Base.Repositories
{
    /// <summary>
    /// Interface for rooms
    /// </summary>
    public interface IRoomRepository
    {
        /// <summary>
        /// Rooms
        /// </summary>
        Task<IEnumerable<Room>> RoomsAsync();

        /// <summary>
        /// Get a collection of rooms
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of rooms</returns>
        Task<IEnumerable<Room>> GetRoomsAsync(bool ignoreCache = false);

        /// <summary>
        /// Get a collection of rooms
        /// </summary>
        /// <param name="ignoreCache">Bypass cache flag</param>
        /// <returns>Collection of rooms</returns>
        Task<Tuple<IEnumerable<Room>, int>> GetRoomsWithPagingAsync(int offset, int limit, bool ignoreCache = false);
    }
}
