﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Base.Entities;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Domain.Base.Repositories
{
    /// <summary>
    /// Interface for PhoneNumber Repository
    /// </summary>
    public interface IInstitutionRepository 
    {
        /// <summary>
        /// Get all Institution data
        /// </summary>
        /// <returns>List of Institution Objects</returns>
        IEnumerable<Institution> Get();
        
        /// <summary>
        /// Get all Institution data
        /// </summary>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        Task<IEnumerable<Institution>> GetAllInstitutionsAsync(bool bypassCache);
    }
}
