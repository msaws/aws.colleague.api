﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Dtos.Student;
using slf4net;
using Ellucian.Web.Adapters;

namespace Ellucian.Colleague.Coordination.Student.Adapters
{
    public class AcademicCreditEntityToAcademicCredit2DtoAdapter : AutoMapperAdapter<Ellucian.Colleague.Domain.Student.Entities.AcademicCredit, Ellucian.Colleague.Dtos.Student.AcademicCredit2>
    {
        public AcademicCreditEntityToAcademicCredit2DtoAdapter(IAdapterRegistry adapterRegistry, ILogger logger)
            : base(adapterRegistry, logger)
        {
            AddMappingDependency<Ellucian.Colleague.Domain.Student.Entities.MidTermGrade, Ellucian.Colleague.Dtos.Student.MidTermGrade2>();
            AddMappingDependency<Ellucian.Colleague.Domain.Student.Entities.GradeRestriction, Ellucian.Colleague.Dtos.Student.GradeRestriction>();
        }

        /// <summary>
        /// Custom mapping of AcademicCredit entity to AcademicCredit2 DTO.
        /// </summary>
        /// <param name="Source">The source.</param>
        /// <returns><see cref="AcademicCredit2">AcademicCredit2</see> data transfer object</returns>
        public override Dtos.Student.AcademicCredit2 MapToType(Domain.Student.Entities.AcademicCredit Source)
        {
            var academicCredit2Dto = base.MapToType(Source);

            // Custom: Need only the ID of the course
            if (Source.Course != null)
            {
                academicCredit2Dto.CourseId = Source.Course.Id;
            }
            // Custom: Return only the grade ID
            if (Source.VerifiedGrade != null)
            {
                academicCredit2Dto.VerifiedGradeId = Source.VerifiedGrade.Id;
            }

            return academicCredit2Dto;
        }
    }
}
