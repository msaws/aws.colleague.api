﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using System.Diagnostics;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;
using slf4net;
using Ellucian.Colleague.Domain.Student;
using Ellucian.Web.Dependency;
using Ellucian.Web.Adapters;
using Ellucian.Web.Security;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    [RegisterType]
    public class AcademicPeriodService : IAcademicPeriodService
    {
        private readonly ITermRepository _termRepository;
        private ILogger _logger;

        public AcademicPeriodService(ITermRepository termRepository, ILogger logger)
        {
            _termRepository = termRepository;
            _logger = logger;
        }

        /// <remarks>OBSOLETE</remarks>
        /// <summary>
        /// Gets all Academic Periods
        /// </summary>
        /// <returns>Collection of AcademicPeriod DTO objects</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.AcademicPeriod>> GetAcademicPeriodsAsync()
        {
            var academicPeriodCollection = new List<Ellucian.Colleague.Dtos.AcademicPeriod>();

            var termEntities = await _termRepository.GetAsync();

            var academicPeriodEntities = _termRepository.GetAcademicPeriods(termEntities);

            if (academicPeriodEntities != null && academicPeriodEntities.Count() > 0)
            {
                foreach (var academicPeriod in academicPeriodEntities)
                {
                    academicPeriodCollection.Add(ConvertAcademicPeriodEntityToAcademicPeriodDto(academicPeriod));
                }
            }

            return academicPeriodCollection;
        }

        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Gets all Academic Periods
        /// including census dates and registration status
        /// </summary>
        /// <returns>Collection of AcademicPeriod DTO objects</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.AcademicPeriod3>> GetAcademicPeriods3Async(bool bypassCache)
        {
            var academicPeriodCollection = new List<Ellucian.Colleague.Dtos.AcademicPeriod3>();

            var termEntities = await _termRepository.GetAsync(bypassCache);

            var academicPeriodEntities = _termRepository.GetAcademicPeriods(termEntities);

            if (academicPeriodEntities != null && academicPeriodEntities.Any())
            {
                foreach (var academicPeriod in academicPeriodEntities)
                {
                    academicPeriodCollection.Add(ConvertAcademicPeriodEntityToAcademicPeriodDto3(academicPeriod, GetTermRegistrationStatus(academicPeriod)));
                }
            }

            return academicPeriodCollection;
        }

        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Gets all Academic Periods
        /// </summary>
        /// <returns>Collection of AcademicPeriod DTO objects</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.AcademicPeriod2>> GetAcademicPeriods2Async(bool bypassCache)
        {
            var academicPeriodCollection = new List<Ellucian.Colleague.Dtos.AcademicPeriod2>();

            var termEntities = await _termRepository.GetAsync(bypassCache);

            var academicPeriodEntities = _termRepository.GetAcademicPeriods(termEntities);

            if (academicPeriodEntities != null && academicPeriodEntities.Any())
            {
                foreach (var academicPeriod in academicPeriodEntities)
                {
                    academicPeriodCollection.Add(ConvertAcademicPeriodEntityToAcademicPeriodDto2(academicPeriod));
                }
            }

            return academicPeriodCollection;
        }

        /// <remarks>OBSOLETE</remarks>
        /// <summary>
        /// Get an Academic Period from its GUID
        /// </summary>
        /// <returns>AcademicPeriod DTO object</returns>
        public async Task<Ellucian.Colleague.Dtos.AcademicPeriod> GetAcademicPeriodByGuidAsync(string guid)
        {
            try
            {
                var termEntities = await _termRepository.GetAsync();
                return ConvertAcademicPeriodEntityToAcademicPeriodDto(_termRepository.GetAcademicPeriods(termEntities).Where(rt => rt.Guid == guid).First());
            }
            catch (InvalidOperationException ex)
            {
                throw new KeyNotFoundException("Academic Period not found for GUID " + guid, ex);
            }
            catch (Exception ex)
            {
                throw new KeyNotFoundException("Academic Period not found for GUID " + guid, ex);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Get an Academic Period from its GUID
        /// including census dates and registration status
        /// </summary>
        /// <returns>AcademicPeriod DTO object</returns>
        public async Task<Ellucian.Colleague.Dtos.AcademicPeriod3> GetAcademicPeriodByGuid3Async(string guid)
        {
            try
            {
                var termEntities = await _termRepository.GetAsync();
                var academicPeriods = _termRepository.GetAcademicPeriods(termEntities);

                var academicPeriod = academicPeriods.Where(rt => rt.Guid == guid).FirstOrDefault();

                return ConvertAcademicPeriodEntityToAcademicPeriodDto3(academicPeriod, GetTermRegistrationStatus(academicPeriod));
            }
            catch (InvalidOperationException ex)
            {
                throw new KeyNotFoundException("Academic Period not found for GUID " + guid, ex);
            }
            catch (Exception ex)
            {
                throw new KeyNotFoundException("Academic Period not found for GUID " + guid, ex);
            }
        }

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Determine if a term is "open" or "closed" based on its start and end date for pre-registration, registration, add, and drop.
        /// Open if today is greater than/equal to earliest start date and less than/equal to latest start date.
        /// Closed if today is less than earliest start date or greater than latest end date.
        /// </summary>
        /// <returns>Academic registration status</returns>
        private Dtos.EnumProperties.TermRegistrationStatus? GetTermRegistrationStatus(Ellucian.Colleague.Domain.Student.Entities.AcademicPeriod academicPeriod)
        {
            Dtos.EnumProperties.TermRegistrationStatus termRegistrationStatus = new Dtos.EnumProperties.TermRegistrationStatus();
            if (academicPeriod.RegistrationDates != null)
            {
                var preRegistrationStartDate = academicPeriod.RegistrationDates.FirstOrDefault().PreRegistrationStartDate;
                var preRegistrationEndDate = academicPeriod.RegistrationDates.FirstOrDefault().PreRegistrationEndDate;
                var registrationStartDate = academicPeriod.RegistrationDates.FirstOrDefault().RegistrationStartDate;
                var registrationEndDate = academicPeriod.RegistrationDates.FirstOrDefault().RegistrationEndDate;
                var addStartDate = academicPeriod.RegistrationDates.FirstOrDefault().AddStartDate;
                var addEndDate = academicPeriod.RegistrationDates.FirstOrDefault().AddEndDate;
                var dropStartDate = academicPeriod.RegistrationDates.FirstOrDefault().DropStartDate;
                var dropEndDate = academicPeriod.RegistrationDates.FirstOrDefault().DropEndDate;

                /// Buid list of all term pre-registration, registration ,add, and drop dates
                List<DateTime?> termDates = new List<DateTime?>();
                if (preRegistrationStartDate != null)
                {
                    termDates.Add(preRegistrationStartDate);
                }
                if (registrationStartDate != null)
                {
                    termDates.Add(registrationStartDate);
                }
                if (addStartDate != null)
                {
                    termDates.Add(addStartDate);
                }
                if (dropStartDate != null)
                {
                    termDates.Add(dropStartDate);
                }
                if (preRegistrationEndDate != null)
                {
                    termDates.Add(preRegistrationEndDate);
                }
                if (registrationEndDate != null)
                {
                    termDates.Add(registrationEndDate);
                }
                if (addEndDate != null)
                {
                    termDates.Add(addEndDate);
                }
                if (dropEndDate != null)
                {
                    termDates.Add(dropEndDate);
                }
                // Find earliest and latest dates available.
                DateTime? minTermDate = null;
                DateTime? maxTermDate = null;
                foreach (DateTime date in termDates)
                {
                    if (minTermDate == null)
                    {
                        minTermDate = date;
                    }
                    if (maxTermDate == null)
                    {
                        maxTermDate = date;
                    }
                    if (date < minTermDate)
                        minTermDate = date;
                    if (date > maxTermDate)
                        maxTermDate = date;
                }

                // Determine if registration is open or closed.       
                // Open if today is greater than/equal to earliest start date and less than/equal to latest start date.
                // Closed if today is less than earliest start date or greater than latest end date.
                //Dtos.EnumProperties.TermRegistrationStatus termRegistrationStatus = new Dtos.EnumProperties.TermRegistrationStatus();
                if (DateTime.Now >= minTermDate && DateTime.Now <= maxTermDate)
                {
                    termRegistrationStatus = Dtos.EnumProperties.TermRegistrationStatus.Open;
                }
                if (DateTime.Now < minTermDate || DateTime.Now > maxTermDate)
                {
                    termRegistrationStatus = Dtos.EnumProperties.TermRegistrationStatus.Closed;
                }
            }
            return termRegistrationStatus;
        }

        /// <remarks>FOR USE WITH ELLUCIAN HEDM</remarks>
        /// <summary>
        /// Get an Academic Period from its GUID
        /// </summary>
        /// <returns>AcademicPeriod DTO object</returns>
        public async Task<Ellucian.Colleague.Dtos.AcademicPeriod2> GetAcademicPeriodByGuid2Async(string guid)
        {
            try
            {
                var termEntities = await _termRepository.GetAsync();
                var academicPeriods = _termRepository.GetAcademicPeriods(termEntities);
                return ConvertAcademicPeriodEntityToAcademicPeriodDto2(academicPeriods.Where(rt => rt.Guid == guid).First());
            }
            catch (InvalidOperationException ex)
            {
                throw new KeyNotFoundException("Academic Period not found for GUID " + guid, ex);
            }
            catch (Exception ex)
            {
                throw new KeyNotFoundException("Academic Period not found for GUID " + guid, ex);
            }
        }

        /// <remarks>OBSOLETE</remarks>
        /// <summary>
        /// Converts a Academic Period domain entity to its corresponding Academic Periods DTO
        /// </summary>
        /// <param name="source">Academic Periods domain entity</param>
        /// <returns>EmailType DTO</returns>
        private Dtos.AcademicPeriod ConvertAcademicPeriodEntityToAcademicPeriodDto(Ellucian.Colleague.Domain.Student.Entities.AcademicPeriod source)
        {
            var acadedmicPeriod = new Dtos.AcademicPeriod();
            acadedmicPeriod.Guid = source.Guid;
            acadedmicPeriod.Abbreviation = source.Code;
            acadedmicPeriod.Title = source.Description;
            acadedmicPeriod.Description = null;

            acadedmicPeriod.Start = source.StartDate;
            acadedmicPeriod.End = source.EndDate;

            var category = new Dtos.AcademicPeriodCategory();
            category.Type = IsReportingTermEqualCode(source) ? Dtos.AcademicTimePeriod.Term : Dtos.AcademicTimePeriod.Subterm;
            category.ParentGuid = source.ParentId;
            category.PrecedingGuid = source.PreceedingId;
            acadedmicPeriod.category = category;
            return acadedmicPeriod;
        }

        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Converts a Academic Period domain entity to its corresponding Academic Periods DTO
        /// </summary>
        /// <param name="source">Academic Periods domain entity</param>
        /// <returns>EmailType DTO</returns>
        private Dtos.AcademicPeriod3 ConvertAcademicPeriodEntityToAcademicPeriodDto3(Ellucian.Colleague.Domain.Student.Entities.AcademicPeriod source, Dtos.EnumProperties.TermRegistrationStatus? termRegistrationStatus)
        {
            var academicPeriod = new Dtos.AcademicPeriod3();
            if (source == null)
            {
                // throw error
                throw new KeyNotFoundException("No academc period found to convert");
            }
            academicPeriod.Id = source.Guid;
            academicPeriod.Code = source.Code;
            academicPeriod.Title = source.Description;
            academicPeriod.Description = null;

            academicPeriod.Start = source.StartDate;
            academicPeriod.End = source.EndDate;

            if (source.RegistrationDates != null)
            {
                if (source.RegistrationDates.FirstOrDefault().CensusDates.Any())
                {
                    academicPeriod.CensusDates = source.RegistrationDates.FirstOrDefault().CensusDates;
                }                
                if (termRegistrationStatus != null && termRegistrationStatus != Dtos.EnumProperties.TermRegistrationStatus.NotSet)
                {
                    academicPeriod.RegistrationStatus = termRegistrationStatus;
                }
            }
            
            var category = new Dtos.AcademicPeriodCategory2();
            category.Type = IsReportingTermEqualCode(source) ? Dtos.AcademicTimePeriod2.Term : Dtos.AcademicTimePeriod2.Subterm;
            category.ParentGuid = (source.ParentId != null) ? new GuidObject2(source.ParentId) : null;
            category.PrecedingGuid = (source.PreceedingId != null) ? new GuidObject2(source.PreceedingId) : null;
            academicPeriod.Category = category;

            return academicPeriod;
        }

        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Converts a Academic Period domain entity to its corresponding Academic Periods DTO
        /// </summary>
        /// <param name="source">Academic Periods domain entity</param>
        /// <returns>EmailType DTO</returns>
        private Dtos.AcademicPeriod2 ConvertAcademicPeriodEntityToAcademicPeriodDto2(Ellucian.Colleague.Domain.Student.Entities.AcademicPeriod source)
        {
            var acadedmicPeriod = new Dtos.AcademicPeriod2();
            acadedmicPeriod.Id = source.Guid;
            acadedmicPeriod.Code = source.Code;
            acadedmicPeriod.Title = source.Description;
            acadedmicPeriod.Description = null;

            acadedmicPeriod.Start = source.StartDate;
            acadedmicPeriod.End = source.EndDate;

            var category = new Dtos.AcademicPeriodCategory2();
            category.Type = IsReportingTermEqualCode(source) ? Dtos.AcademicTimePeriod2.Term : Dtos.AcademicTimePeriod2.Subterm;
            category.ParentGuid = (source.ParentId != null) ? new GuidObject2(source.ParentId) : null;
            category.PrecedingGuid = (source.PreceedingId != null) ? new GuidObject2(source.PreceedingId) : null;
            acadedmicPeriod.Category = category;
            return acadedmicPeriod;
        }


        /// <summary>
        /// Determine if a AcademicPeriods reporting term is the same as its code
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        private bool IsReportingTermEqualCode(Ellucian.Colleague.Domain.Student.Entities.AcademicPeriod academicPeriod)
        {
            if (academicPeriod == null)
            {
                return false;
            }
            return (academicPeriod.ReportingTerm == academicPeriod.Code);
        }
    }
}