﻿// Copyright 2016 Ellucian Company L.P. and its affiliates

using System;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System.Collections.Generic;
using Ellucian.Colleague.Domain.Base.Repositories;
using System.Linq;
using System.Runtime.CompilerServices;
using Ellucian.Colleague.Domain.Student;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    /// <summary>
    /// Implements the IStudentChargeService
    /// </summary>
    [RegisterType]
    public class StudentChargeService : BaseCoordinationService, IStudentChargeService
    {
        private IStudentChargeRepository studentChargeRepository;
        private IPersonRepository personRepository;
        private IReferenceDataRepository referenceDataRepository;
        private IStudentReferenceDataRepository studentReferenceDataRepository;
        private readonly ITermRepository termRepository;
        private readonly IConfigurationRepository configurationRepository;

        // Constructor to initialize the private attributes
        public StudentChargeService(IStudentChargeRepository studentChargeRepository,
            IPersonRepository personRepository,
            IReferenceDataRepository referenceDataRepository,
            IStudentReferenceDataRepository studentReferenceDataRepository,
            ITermRepository termRepository,
            IConfigurationRepository configurationRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger, configurationRepository:configurationRepository)
        {
            this.studentChargeRepository = studentChargeRepository;
            this.personRepository = personRepository;
            this.referenceDataRepository = referenceDataRepository;
            this.studentReferenceDataRepository = studentReferenceDataRepository;
            this.termRepository = termRepository;
            this.configurationRepository = configurationRepository;
        }

        /// <summary>
        /// Returns the DTO for the specified student charges
        /// </summary>
        /// <param name="id">Guid to General Ledger Transaction</param>
        /// <returns>General Ledger Transaction DTO</returns>
        public async Task<Dtos.StudentCharge> GetByIdAsync(string id)
        {
            CheckViewStudentChargesPermission();
            // Get the student charges domain entity from the repository
            var studentChargeDomainEntity = await studentChargeRepository.GetByIdAsync(id);

            if (studentChargeDomainEntity == null)
            {
                throw new ArgumentNullException("StudentChargeDomainEntity", "StudentChargeDomainEntity cannot be null. ");
            }

            // Convert the student charge object into DTO.
            return await BuildStudentChargeDtoAsync(studentChargeDomainEntity);
        }
        /// <summary>
        /// Returns all student charges for the data model version 6
        /// </summary>
        /// <returns>Collection of StudentCharges</returns>
        public async Task<Tuple<IEnumerable<Dtos.StudentCharge>, int>> GetAsync(int offset, int limit, bool bypassCache, string student = "", string academicPeriod = "", string accountingCode = "", string chargeType = "")
        {
            CheckViewStudentChargesPermission();

            var studentChargeDtos = new List<Dtos.StudentCharge>();
            string personId = "";
            string term = "";
            string arCode = "";
            if (!string.IsNullOrEmpty(student))
            {
                personId = await personRepository.GetPersonIdFromGuidAsync(student);
                if (string.IsNullOrEmpty(personId))
                {
                    // return new Tuple<IEnumerable<Dtos.StudentCharge>, int>(studentChargeDtos, 0);
                    throw new ArgumentException(string.Format("Invalid id '{0}' used in filter parameter 'student'. ", student), "student");
                }
            }
            if (!string.IsNullOrEmpty(academicPeriod))
            {
                var termEntity = (await termRepository.GetAsync(bypassCache)).FirstOrDefault(t => t.RecordGuid == academicPeriod);
                if (termEntity == null || string.IsNullOrEmpty(termEntity.Code))
                {
                    throw new ArgumentException(string.Format("Invalid id '{0}' used in filter parameter 'academicPeriod'. ", academicPeriod), "academicPeriod");
                }
                term = termEntity.Code;
            }
            if (!string.IsNullOrEmpty(accountingCode))
            {
                var arCodeEntity = (await studentReferenceDataRepository.GetAccountingCodesAsync(bypassCache)).FirstOrDefault(ac => ac.Guid == accountingCode);
                if (arCodeEntity == null || string.IsNullOrEmpty(arCodeEntity.Code))
                {
                    throw new ArgumentException(string.Format("Invalid id '{0}' used in filter parameter 'accountingCode'. ", accountingCode), "accountingCode");
                }
                arCode = arCodeEntity.Code;
            }
            if (!string.IsNullOrEmpty(chargeType))
            {
                try
                {
                    var enumChargType = (Dtos.EnumProperties.StudentChargeTypes)Enum.Parse(typeof(Dtos.EnumProperties.StudentChargeTypes), chargeType);
                }
                catch
                {
                    throw new ArgumentException(string.Format("Invalid enumeration '{0}' used in filter parameter 'chargeType'. ", chargeType), "chargeType");
                }
            }

            // Get the student charges domain entity from the repository
            var studentChargeDomainTuple = await studentChargeRepository.GetAsync(offset, limit, bypassCache, personId, term, arCode, chargeType);
            var studentChargeDomainEntities = studentChargeDomainTuple.Item1;
            var totalRecords = studentChargeDomainTuple.Item2;

            if (studentChargeDomainEntities == null)
            {
                throw new ArgumentNullException("StudentChargeDomainEntity", "StudentChargeDomainEntity cannot be null. ");
            }

            // Convert the student charges and all its child objects into DTOs.
            foreach (var entity in studentChargeDomainEntities)
            {
                if (entity != null)
                {
                    var chargeDto = await BuildStudentChargeDtoAsync(entity, bypassCache);
                    studentChargeDtos.Add(chargeDto);
                }
            }
            return new Tuple<IEnumerable<Dtos.StudentCharge>,int>(studentChargeDtos, totalRecords);
        }

        /// <summary>
        /// Update a single student charges for the data model version 6
        /// </summary>
        /// <returns>A single StudentCharge</returns>
        public async Task<Dtos.StudentCharge> UpdateAsync(string id, Dtos.StudentCharge studentCharge)
        {
            CheckCreateStudentChargesPermission();

            ValidateStudentCharges(studentCharge);

            var studentChargeDto = new Dtos.StudentCharge();

            var studentChargeEntity = await BuildStudentChargeEntityAsync(studentCharge);
            var entity = await studentChargeRepository.UpdateAsync(id, studentChargeEntity);
            
            studentChargeDto = await BuildStudentChargeDtoAsync(entity);

            return studentChargeDto;
        }

        /// <summary>
        /// Create a single student charges for the data model version 6
        /// </summary>
        /// <returns>A single StudentCharge</returns>
        public async Task<Dtos.StudentCharge> CreateAsync(Dtos.StudentCharge studentCharge)
        {
            CheckCreateStudentChargesPermission();

            ValidateStudentCharges(studentCharge);

            var studentChargeDto = new Dtos.StudentCharge();

            var studentChargeEntity = await BuildStudentChargeEntityAsync(studentCharge);
            var entity = await studentChargeRepository.CreateAsync(studentChargeEntity);

            studentChargeDto = await BuildStudentChargeDtoAsync(entity);

            return studentChargeDto;
        }

        /// <summary>
        /// Delete a single student charges for the data model version 6
        /// </summary>
        /// <param name="id">The requested student charges GUID</param>
        /// <returns></returns>
        public async Task DeleteAsync(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id", "Must provide a student charges guid for deletion. ");
            }

            await studentChargeRepository.DeleteAsync(id);
        }

        private async Task<Dtos.StudentCharge> BuildStudentChargeDtoAsync(Ellucian.Colleague.Domain.Student.Entities.StudentCharge studentChargeEntity, bool bypassCache = true)
        {
            var studentChargeDto = new Dtos.StudentCharge();

            studentChargeDto.Person = new GuidObject2((!string.IsNullOrEmpty(studentChargeEntity.PersonId)) ?
                await personRepository.GetPersonGuidFromIdAsync(studentChargeEntity.PersonId) :
                string.Empty);
            studentChargeDto.Id = studentChargeEntity.Guid;
            if (string.IsNullOrEmpty(studentChargeDto.Id)) studentChargeDto.Id = "00000000-0000-0000-0000-000000000000";
            if (!string.IsNullOrEmpty(studentChargeEntity.Term))
            {
                var termEntity = (await termRepository.GetAsync()).FirstOrDefault(t => t.Code == studentChargeEntity.Term);
                if (termEntity != null && !string.IsNullOrEmpty(termEntity.RecordGuid))
                {
                    studentChargeDto.AcademicPeriod = new GuidObject2(termEntity.RecordGuid);
                }
            }
            if (!string.IsNullOrEmpty(studentChargeEntity.AccountsReceivableCode))
            {
                var accountingCodeEntity = (await studentReferenceDataRepository.GetAccountingCodesAsync(bypassCache)).FirstOrDefault(acc => acc.Code == studentChargeEntity.AccountsReceivableCode);
                if (accountingCodeEntity != null)
                {
                    studentChargeDto.AccountingCode = new GuidObject2(accountingCodeEntity.Guid);
                }
            }
            if (!string.IsNullOrEmpty(studentChargeEntity.AccountsReceivableTypeCode))
            {
                var accountReceivalbeTypesEntity = (await studentReferenceDataRepository.GetAccountReceivableTypesAsync(bypassCache)).FirstOrDefault(acc => acc.Code == studentChargeEntity.AccountsReceivableTypeCode);
                if (accountReceivalbeTypesEntity != null)
                {
                    studentChargeDto.AccountReceivableType = new GuidObject2(accountReceivalbeTypesEntity.Guid);
                }
            }
            studentChargeDto.ChargeableOn = studentChargeEntity.ChargeDate.Date;
            studentChargeDto.ChargeType = !string.IsNullOrEmpty(studentChargeEntity.ChargeType) ?
                ConvertChargeTypes(studentChargeEntity.ChargeType) :
                Dtos.EnumProperties.StudentChargeTypes.notset;
            studentChargeDto.Comments = studentChargeEntity.Comments != null && studentChargeEntity.Comments.Any() ?
                studentChargeEntity.Comments :
                null;

            if (studentChargeEntity.UnitCost != 0 && !string.IsNullOrEmpty(studentChargeEntity.UnitCurrency))
            {
                studentChargeDto.ChargedAmount = new Dtos.DtoProperties.ChargedAmountDtoProperty()
                {
                    UnitCost = new Dtos.DtoProperties.ChargedAmountUnitCostDtoProperty()
                    {
                        Quantity = studentChargeEntity.UnitQuantity,
                        Cost = new Dtos.DtoProperties.AmountDtoProperty()
                        {
                            Currency = (Dtos.EnumProperties.CurrencyCodes)Enum.Parse(typeof(Dtos.EnumProperties.CurrencyCodes), studentChargeEntity.UnitCurrency),
                            Value = studentChargeEntity.UnitCost
                        }
                    }
                };
            }
            else
            {
                studentChargeDto.ChargedAmount = new Dtos.DtoProperties.ChargedAmountDtoProperty()
                {
                    Amount = new Dtos.DtoProperties.AmountDtoProperty()
                    {
                        Currency = (Dtos.EnumProperties.CurrencyCodes)Enum.Parse(typeof(Dtos.EnumProperties.CurrencyCodes), studentChargeEntity.ChargeCurrency),
                        Value = studentChargeEntity.ChargeAmount
                    }
                };
            }
            return studentChargeDto;
        }

        private Dtos.EnumProperties.StudentChargeTypes ConvertChargeTypes(string chargeType)
        {
            switch (chargeType.ToLowerInvariant())
            {
                case "tuition":
                    {
                        return Dtos.EnumProperties.StudentChargeTypes.tuition;
                    }
                case "fee":
                    {
                        return Dtos.EnumProperties.StudentChargeTypes.fee;
                    }
                case "housing":
                    {
                        return Dtos.EnumProperties.StudentChargeTypes.housing;
                    }
                case "meal":
                    {
                        return Dtos.EnumProperties.StudentChargeTypes.meal;
                    }
                default:
                    {
                        return Dtos.EnumProperties.StudentChargeTypes.notset;
                    }
            }
        }

        private async Task<Ellucian.Colleague.Domain.Student.Entities.StudentCharge> BuildStudentChargeEntityAsync(Dtos.StudentCharge studentChargeDto, bool bypassCache = true)
        {
            if (studentChargeDto.Person == null || string.IsNullOrEmpty(studentChargeDto.Person.Id))
            {
                throw new ArgumentNullException("studentCharge.student.id", "The Student id cannot be null. ");
            }
            if (studentChargeDto.ChargeType == Dtos.EnumProperties.StudentChargeTypes.notset)
            {
                throw new ArgumentNullException("studentCharge.chargeType", "The chargeType must be set and cannot be null. ");
            }

            var personId = await personRepository.GetPersonIdFromGuidAsync(studentChargeDto.Person.Id);
            var chargeType = studentChargeDto.ChargeType.ToString();
            var chargeDate = studentChargeDto.ChargeableOn.HasValue ? new DateTime(studentChargeDto.ChargeableOn.Value.Date.Year, studentChargeDto.ChargeableOn.Value.Date.Month, studentChargeDto.ChargeableOn.Value.Date.Day) : DateTime.Today.Date;
            string arCode = "";
            string arType = "";
            if (studentChargeDto.AccountingCode != null && !string.IsNullOrEmpty(studentChargeDto.AccountingCode.Id))
            {
                var arCodeEntity = (await studentReferenceDataRepository.GetAccountingCodesAsync(bypassCache)).FirstOrDefault(acc => acc.Guid == studentChargeDto.AccountingCode.Id);
                if (arCodeEntity != null)
                {
                    arCode = arCodeEntity.Code;
                }
                else
                {
                    throw new ArgumentException(string.Format("The accountingCode id '{0}' is not valid. ", studentChargeDto.AccountingCode.Id), "studentCharges.accountingCode.id");
                }
            }
            if (studentChargeDto.AccountReceivableType != null && !string.IsNullOrEmpty(studentChargeDto.AccountReceivableType.Id))
            { 
                var arTypeEntity = (await studentReferenceDataRepository.GetAccountReceivableTypesAsync(bypassCache)).FirstOrDefault(acc => acc.Guid == studentChargeDto.AccountReceivableType.Id);
                if (arTypeEntity != null)
                {
                    arType = arTypeEntity.Code;
                }
                else
                {
                    throw new ArgumentException(string.Format("The accountReceivableType id '{0}' is not valid. ", studentChargeDto.AccountReceivableType.Id), "studentCharges.accountReceivableType.id");
                }
            }
            var termEntity = (studentChargeDto.AcademicPeriod != null && !string.IsNullOrEmpty(studentChargeDto.AcademicPeriod.Id)) ?
                (await termRepository.GetAsync(bypassCache)).FirstOrDefault(acc => acc.RecordGuid == studentChargeDto.AcademicPeriod.Id) :
                null;
            if (termEntity == null)
            {
                if (studentChargeDto.AcademicPeriod != null && !string.IsNullOrEmpty(studentChargeDto.AcademicPeriod.Id))
                {
                    throw new ArgumentException(string.Format("The Academic Period id {0} is invalid. ", studentChargeDto.AcademicPeriod.Id), "studentCharge.academicPeriod.id");
                }
                throw new ArgumentException("The Academic Period is required for Colleague. ", "studentCharge.academicPeriod");
            }
            var term = termEntity.Code;

            var studentChargeEntity = new Ellucian.Colleague.Domain.Student.Entities.StudentCharge(personId, chargeType, chargeDate)
                {
                    Guid = (studentChargeDto.Id != null && !string.IsNullOrEmpty(studentChargeDto.Id)) ? studentChargeDto.Id : string.Empty,
                    AccountsReceivableCode = arCode,
                    AccountsReceivableTypeCode = arType,
                    Comments = studentChargeDto.Comments,
                    Term = term,
                    ChargeAmount = (studentChargeDto.ChargedAmount != null && studentChargeDto.ChargedAmount.Amount != null) ? studentChargeDto.ChargedAmount.Amount.Value : 0,
                    ChargeCurrency = (studentChargeDto.ChargedAmount != null && studentChargeDto.ChargedAmount.Amount != null) ? studentChargeDto.ChargedAmount.Amount.Currency.ToString() : string.Empty,
                    UnitQuantity = (studentChargeDto.ChargedAmount != null && studentChargeDto.ChargedAmount.UnitCost != null) ? studentChargeDto.ChargedAmount.UnitCost.Quantity : 0,
                    UnitCost = (studentChargeDto.ChargedAmount != null && studentChargeDto.ChargedAmount.UnitCost != null && studentChargeDto.ChargedAmount.UnitCost.Cost != null) ? studentChargeDto.ChargedAmount.UnitCost.Cost.Value : 0,
                    UnitCurrency = (studentChargeDto.ChargedAmount != null && studentChargeDto.ChargedAmount.UnitCost != null && studentChargeDto.ChargedAmount.UnitCost.Cost != null) ? studentChargeDto.ChargedAmount.UnitCost.Cost.Currency.ToString() : string.Empty
                };

            try
            {
                studentChargeEntity.InvoiceItemID = (studentChargeDto.Id != null && !string.IsNullOrEmpty(studentChargeDto.Id)) ? (await referenceDataRepository.GetGuidLookupResultFromGuidAsync(studentChargeDto.Id)).PrimaryKey : string.Empty;
            }
            catch
            {
                // Do nothing if the GUID doesn't already exist, just leave the invoice item id blank.
            }
            return studentChargeEntity;
        }

        /// <summary>
        /// Helper method to validate Student Charges.
        /// </summary>
        private void ValidateStudentCharges(Dtos.StudentCharge studentCharge)
        {
            if (studentCharge.AcademicPeriod == null)
            {
                throw new ArgumentNullException("studentCharges.academicPeriod", "The academic period is required when submitting a student charge. ");
            }
            if (studentCharge.ChargedAmount == null)
            {
                throw new ArgumentNullException("studentCharges.chargedAmount", "The charged amount cannot be null when submitting a student charge. ");
            }
            if (studentCharge.ChargedAmount.Amount == null && studentCharge.ChargedAmount.UnitCost == null)
            {
                throw new ArgumentNullException("studentCharges.chargeAmount", "The charged amount must contain either amount or unitCost when submitting a student charge. ");
            }
            if (studentCharge.ChargedAmount.Amount != null && studentCharge.ChargedAmount.UnitCost != null)
            {
                throw new ArgumentException("Both amount and unitCost can not be used together. ", "studentCharges.ChargedAmount");
            }
            if (studentCharge.ChargedAmount.Amount != null && studentCharge.ChargedAmount.Amount.Currency != Dtos.EnumProperties.CurrencyCodes.USD && studentCharge.ChargedAmount.Amount.Currency != Dtos.EnumProperties.CurrencyCodes.CAD)
            {
                throw new ArgumentException("The currency code must be set to either 'USD' or 'CAD'. ", "studentCharges.chargedAmount.amount.currency");
            }
            if (studentCharge.ChargedAmount.Amount != null && (studentCharge.ChargedAmount.Amount.Value == null || studentCharge.ChargedAmount.Amount.Value == 0))
            {
                throw new ArgumentException("A charge amount of zero is not allowed.", "studentCharges.chargedAmount.amount.value");
            }
            if (studentCharge.ChargedAmount.UnitCost != null && studentCharge.ChargedAmount.UnitCost.Cost != null && studentCharge.ChargedAmount.UnitCost.Cost.Currency != Dtos.EnumProperties.CurrencyCodes.USD && studentCharge.ChargedAmount.UnitCost.Cost.Currency != Dtos.EnumProperties.CurrencyCodes.CAD)
            {
                throw new ArgumentException("The currency code must be set to either 'USD' or 'CAD'. ", "studentCharges.chargedAmount.unitCost.currency");
            }
            if (studentCharge.ChargedAmount.UnitCost != null && (studentCharge.ChargedAmount.UnitCost.Quantity == null || studentCharge.ChargedAmount.UnitCost.Quantity <= 0))
            {
                throw new ArgumentException("The charged amount unit cost quantity must be greater than 0 when using unit costs. ", "studentCharge.chargedAmount.unitCost.quantity");
            }
            if (studentCharge.ChargedAmount.UnitCost != null && (studentCharge.ChargedAmount.UnitCost.Cost == null || studentCharge.ChargedAmount.UnitCost.Cost.Value == null || studentCharge.ChargedAmount.UnitCost.Cost.Value == 0))
            {
                throw new ArgumentException("A charge amount of zero is not allowed.", "studentCharges.chargedAmount.unitCost.cost.value");
            }
            if (studentCharge.ChargeType == Dtos.EnumProperties.StudentChargeTypes.notset )
            {
                throw new ArgumentException("The chargeType is either invalid or empty and is required when submitting a student charge. ", "studentCharges.chargeType");
            }
            if (studentCharge.Person == null || string.IsNullOrEmpty(studentCharge.Person.Id))
            {
                throw new ArgumentNullException("studentCharges.student.id", "The student id is required when submitting a student charge. ");
            }
        }

        /// <summary>
        /// Helper method to determine if the user has permission to view Student Charges.
        /// </summary>
        /// <exception><see cref="PermissionsException">PermissionsException</see></exception>
        private void CheckViewStudentChargesPermission()
        {
            bool hasPermission = HasPermission(StudentPermissionCodes.ViewStudentCharges);

            // User is not allowed to create or update Student charges without the appropriate permissions
            if (!hasPermission)
            {
                throw new PermissionsException(string.Format("User {0} does not have permission to view Student Charges.", CurrentUser.UserId));
            }
        }

        /// <summary>
        /// Helper method to determine if the user has permission to view Student Charges.
        /// </summary>
        /// <exception><see cref="PermissionsException">PermissionsException</see></exception>
        private void CheckCreateStudentChargesPermission()
        {
            bool hasPermission = HasPermission(StudentPermissionCodes.CreateStudentCharges);

            // User is not allowed to create or update Student charges without the appropriate permissions
            if (!hasPermission)
            {
                throw new PermissionsException(string.Format("User {0} does not have permission to create Student Charges.", CurrentUser.UserId));
            }
        }
    }
}
