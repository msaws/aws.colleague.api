﻿// Copyright 2012-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Dtos.EnumProperties;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    /// <summary>
    /// Coordination service interface for all things related to sections
    /// </summary>
    public interface ISectionCoordinationService : IBaseService
    {
        /// <summary>
        /// Get the roster for a section
        /// </summary>
        /// <param name="sectionId">Section ID</param>
        /// <returns>Collection of RosterStudent DTOs</returns>
        Task<IEnumerable<Dtos.Student.RosterStudent>> GetSectionRosterAsync(string sectionId);

        /// <summary>
        /// Get a section using its GUID
        /// </summary>
        /// <param name="guid">The section's GUID</param>
        /// <returns>A CDM-format Section DTO</returns>
        Task<Section> GetSectionByGuidAsync(string guid);

        /// <summary>
        /// Create a section
        /// </summary>
        /// <param name="section">The DTO of the Section to create</param>
        /// <returns>A CDM-format Section DTO</returns>
        Task<Section> PostSectionAsync(Section section);

        /// <summary>
        /// Update a section
        /// </summary>
        /// <param name="section">The DTO of the Section to update</param>
        /// <returns>A CDM-format Section DTO</returns>
        Task<Section> PutSectionAsync(Section section);

        /// <summary>
        /// Get a SectionMaximum
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="title"></param>
        /// <param name="startOn"></param>
        /// <param name="endOn"></param>
        /// <param name="code"></param>
        /// <param name="number"></param>
        /// <param name="instructionalPlatform"></param>
        /// <param name="academicPeriod"></param>
        /// <param name="academicLevel"></param>
        /// <param name="course"></param>
        /// <param name="site"></param>
        /// <param name="status"></param>
        /// <param name="owningOrganization"></param>
        /// <returns></returns>
        Task<Tuple<IEnumerable<Dtos.SectionMaximum>, int>> GetSectionsMaximumAsync(int offset, int limit, string title = "", string startOn = "", string endOn = "",
            string code = "", string number = "", string instructionalPlatform = "", string academicPeriod = "",
            string academicLevel = "", string course = "", string site = "", string status = "", string owningOrganization = "");

        /// <summary>
        /// Get a SectionMaximum2
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="title"></param>
        /// <param name="startOn"></param>
        /// <param name="endOn"></param>
        /// <param name="code"></param>
        /// <param name="number"></param>
        /// <param name="instructionalPlatform"></param>
        /// <param name="academicPeriod"></param>
        /// <param name="academicLevel"></param>
        /// <param name="course"></param>
        /// <param name="site"></param>
        /// <param name="status"></param>
        /// <param name="owningOrganization"></param>
        /// <returns></returns>
        Task<Tuple<IEnumerable<Dtos.SectionMaximum2>, int>> GetSectionsMaximum2Async(int offset, int limit, string title = "", string startOn = "", string endOn = "",
            string code = "", string number = "", string instructionalPlatform = "", string academicPeriod = "",
            string academicLevel = "", string course = "", string site = "", string status = "", string owningOrganization = "");

        /// <summary>
        /// Get a SectionMaximum2
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="title"></param>
        /// <param name="startOn"></param>
        /// <param name="endOn"></param>
        /// <param name="code"></param>
        /// <param name="number"></param>
        /// <param name="instructionalPlatform"></param>
        /// <param name="academicPeriod"></param>
        /// <param name="academicLevel"></param>
        /// <param name="course"></param>
        /// <param name="site"></param>
        /// <param name="status"></param>
        /// <param name="owningOrganization"></param>
        /// <returns></returns>
        Task<Tuple<IEnumerable<Dtos.SectionMaximum3>, int>> GetSectionsMaximum3Async(int offset, int limit, string title = "", string startOn = "", string endOn = "",
            string code = "", string number = "", string instructionalPlatform = "", string academicPeriod = "",
            string academicLevel = "", string course = "", string site = "", string status = "", string owningOrganization = "");


        /// <summary>
        /// Get a SectionMaximum using its GUID
        /// </summary>
        /// <param name="guid">The section's GUID</param>
        /// <returns>A HEDM-format SectionMaximum DTO</returns>
        Task<SectionMaximum> GetSectionMaximumByGuidAsync(string guid);

        /// <summary>
        /// Get a SectionMaximum using its GUID
        /// </summary>
        /// <param name="guid">The section's GUID</param>
        /// <returns>A HEDM-format SectionMaximum DTO</returns>
        Task<SectionMaximum2> GetSectionMaximumByGuid2Async(string guid);

        /// <summary>
        /// Get a SectionMaximum using its GUID
        /// </summary>
        /// <param name="guid">The section's GUID</param>
        /// <returns>A HEDM-format SectionMaximum DTO</returns>
        Task<SectionMaximum3> GetSectionMaximumByGuid3Async(string guid);


        /// <summary>
        /// Get all with filter parameters
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="title"></param>
        /// <param name="startOn"></param>
        /// <param name="endOn"></param>
        /// <param name="code"></param>
        /// <param name="number"></param>
        /// <param name="instructionalPlatform"></param>
        /// <param name="academicPeriod"></param>
        /// <param name="academicLevel"></param>
        /// <param name="course"></param>
        /// <param name="site"></param>
        /// <param name="status"></param>
        /// <param name="owningOrganization"></param>
        /// <returns>page results of sections by filter criteria</returns>
        Task<Tuple<IEnumerable<Dtos.Section2>, int>> GetSections2Async(int offset, int limit, string title = "", string startOn = "", string endOn = "",
            string code = "", string number = "", string instructionalPlatform = "", string academicPeriod = "",
            string academicLevel = "", string course = "", string site = "", string status = "", string owningOrganization = "");

        /// <summary>
        /// Get all with filter parameters (V6)
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="title"></param>
        /// <param name="startOn"></param>
        /// <param name="endOn"></param>
        /// <param name="code"></param>
        /// <param name="number"></param>
        /// <param name="instructionalPlatform"></param>
        /// <param name="academicPeriod"></param>
        /// <param name="academicLevel"></param>
        /// <param name="course"></param>
        /// <param name="site"></param>
        /// <param name="status"></param>
        /// <param name="owningOrganization"></param>
        /// <returns>page results of sections by filter criteria</returns>
        Task<Tuple<IEnumerable<Dtos.Section3>, int>> GetSections3Async(int offset, int limit, string title = "", string startOn = "", string endOn = "",
            string code = "", string number = "", string instructionalPlatform = "", string academicPeriod = "",
            string academicLevel = "", string course = "", string site = "", string status = "", string owningOrganization = "");

        /// <summary>
        /// Get all with filter parameters (V8)
        /// </summary>
        /// <param name="offset"></param>
        /// <param name="limit"></param>
        /// <param name="title"></param>
        /// <param name="startOn"></param>
        /// <param name="endOn"></param>
        /// <param name="code"></param>
        /// <param name="number"></param>
        /// <param name="instructionalPlatform"></param>
        /// <param name="academicPeriod"></param>
        /// <param name="academicLevel"></param>
        /// <param name="course"></param>
        /// <param name="site"></param>
        /// <param name="status"></param>
        /// <param name="owningOrganization"></param>
        /// <param name="subject"></param>
        /// <param name="instructor"></param>
        /// <param name="search"></param>
        /// <param name="keyword"></param>
        /// <returns>page results of sections by filter criteria</returns>
        Task<Tuple<IEnumerable<Dtos.Section4>, int>> GetSections4Async(int offset, int limit, string title = "", string startOn = "", string endOn = "",
            string code = "", string number = "", string instructionalPlatform = "", string academicPeriod = "",
            string academicLevel = "", string course = "", string site = "", string status = "", string owningOrganization = "",
            string subject = "", string instructor = "", SectionsSearchable search = SectionsSearchable.NotSet, string keyword = "");

        /// <summary>
        /// Get a section using its GUID
        /// </summary>
        /// <param name="guid">The section's GUID</param>
        /// <returns>A HEDM-format Section DTO</returns>
        Task<Section2> GetSection2ByGuidAsync(string guid);

        /// <summary>
        /// Get a section using its GUID
        /// </summary>
        /// <param name="guid">The section's GUID</param>
        /// <returns>A HEDM-format Section DTO</returns>
        Task<Section3> GetSection3ByGuidAsync(string guid);
        
        /// <summary>
        /// Get a section using its GUID
        /// </summary>
        /// <param name="guid">The section's GUID</param>
        /// <returns>A HEDM-format Section DTO</returns>
        Task<Section4> GetSection4ByGuidAsync(string guid);

        /// <summary>
        /// Create a section
        /// </summary>
        /// <param name="section">The DTO of the Section to create</param>
        /// <returns>A HEDM-format Section DTO</returns>
        Task<Section4> PostSection4Async(Section4 section);

        /// <summary>
        /// Update a section
        /// </summary>
        /// <param name="section">The DTO of the Section to update</param>
        /// <returns>A HEDM-format Section DTO</returns>
        Task<Section4> PutSection4Async(Section4 section);

        /// <summary>
        /// Create a section
        /// </summary>
        /// <param name="section">The DTO of the Section to create</param>
        /// <returns>A HEDM-format Section DTO</returns>
        Task<Section2> PostSection2Async(Section2 section);

        /// <summary>
        /// Create a section
        /// </summary>
        /// <param name="section">The DTO of the Section to create</param>
        /// <returns>A HEDM-format Section DTO</returns>
        Task<Section3> PostSection3Async(Section3 section);

        /// <summary>
        /// Update a section
        /// </summary>
        /// <param name="section">The DTO of the Section to update</param>
        /// <returns>A HEDM-format Section DTO</returns>
        Task<Section2> PutSection2Async(Section2 section);

        /// <summary>
        /// Update a section
        /// </summary>
        /// <param name="section">The DTO of the Section to update</param>
        /// <returns>A HEDM-format Section DTO</returns>
        Task<Section3> PutSection3Async(Section3 section);

        /// <summary>
        /// Get an instructional event
        /// </summary>
        /// <param name="guid">GUID of the desired instructional event</param>
        /// <returns>A HEDM-format InstructionalEvent DTO</returns>
        Task<InstructionalEvent> GetInstructionalEventAsync(string guid);
        Task<InstructionalEvent2> GetInstructionalEvent2Async(string id);
        Task<InstructionalEvent3> GetInstructionalEvent3Async(string id);
        Task<Tuple<IEnumerable<Dtos.InstructionalEvent2>, int>> GetInstructionalEvent2Async(int offset, int limit, string section = "", string startOn = "", string endOn = "", string room = "", string instructor = "");
        Task<Tuple<IEnumerable<Dtos.InstructionalEvent3>, int>> GetInstructionalEvent3Async(int offset, int limit, string section = "", string startOn = "", string endOn = "", string room = "", string instructor = "");
        
        /// <summary>
        /// Create an instructional event
        /// </summary>
        /// <param name="meeting">The DTO of the InstructionalEvent to create</param>
        /// <returns>A HEDM-format InstructionalEvent DTO</returns>
        Task<InstructionalEvent> CreateInstructionalEventAsync(InstructionalEvent meeting);
        Task<InstructionalEvent2> CreateInstructionalEvent2Async(InstructionalEvent2 meeting);
        Task<InstructionalEvent3> CreateInstructionalEvent3Async(InstructionalEvent3 meeting);
        
        /// <summary>
        /// Update an instructional event
        /// </summary>
        /// <param name="meeting">The DTO of the InstructionalEvent to update</param>
        /// <returns>A HEDM-format InstructionalEvent DTO</returns>
        Task<InstructionalEvent> UpdateInstructionalEventAsync(InstructionalEvent meeting);
        Task<InstructionalEvent2> UpdateInstructionalEvent2Async(InstructionalEvent2 meeting);
        Task<InstructionalEvent3> UpdateInstructionalEvent3Async(InstructionalEvent3 meeting);
        
        /// <summary>
        /// Delete an instructional event
        /// </summary>
        /// <param name="guid">GUID of the event to delete</param>
        Task DeleteInstructionalEventAsync(string guid);

        Task<IEnumerable<Dtos.Student.SectionGradeResponse>> ImportGradesAsync(Dtos.Student.SectionGrades sectionGrades);

        Task<IEnumerable<Dtos.Student.SectionGradeResponse>> ImportGrades2Async(Dtos.Student.SectionGrades2 sectionGrades);

        Task<IEnumerable<Dtos.Student.SectionGradeResponse>> ImportGrades3Async(Dtos.Student.SectionGrades3 sectionGrades);

        Task<IEnumerable<Dtos.Student.SectionGradeResponse>> ImportGrades4Async(Dtos.Student.SectionGrades3 sectionGrades);

        Task<IEnumerable<Dtos.Student.SectionGradeResponse>> ImportIlpGrades1Async(Dtos.Student.SectionGrades3 sectionGrades);

    }
}