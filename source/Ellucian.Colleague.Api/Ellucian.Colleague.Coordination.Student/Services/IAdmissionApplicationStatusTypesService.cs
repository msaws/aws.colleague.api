//Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    /// <summary>
    /// Interface for AdmissionApplicationStatusTypes services
    /// </summary>
    public interface IAdmissionApplicationStatusTypesService
    {
        Task<IEnumerable<Ellucian.Colleague.Dtos.AdmissionApplicationStatusType>> GetAdmissionApplicationStatusTypesAsync(bool bypassCache = false);
        Task<Ellucian.Colleague.Dtos.AdmissionApplicationStatusType> GetAdmissionApplicationStatusTypesByGuidAsync(string id);
    }
}
