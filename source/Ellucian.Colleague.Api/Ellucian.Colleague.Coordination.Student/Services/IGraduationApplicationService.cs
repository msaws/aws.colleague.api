﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Student;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    public interface IGraduationApplicationService
    {
        /// <summary>
        /// Retrieves a graduation application for the given student Id and program code asynchronously.
        /// </summary>
        /// <param name="studentId">Id of the student</param>
        /// <param name="programCode">program code that student belongs to</param>
        /// <returns><see cref="Ellucian.Colleague.Dtos.Student.GraduationApplication">Graduation Application</see> object that was created</returns>
        Task<Ellucian.Colleague.Dtos.Student.GraduationApplication> GetGraduationApplicationAsync(string studentId, string programCode);

        /// <summary>
        /// Validates a new graduation application and creates a new application in the database, returning the created application asynchronously.
        /// </summary>
        /// <param name="graduateApplication">New graduate application object to add</param>
        /// <returns><see cref="Ellucian.Colleague.Dtos.Student.GraduationApplication">Graduation Application</see> object that was created</returns>
        Task<Ellucian.Colleague.Dtos.Student.GraduationApplication> CreateGraduationApplicationAsync(Dtos.Student.GraduationApplication graduationApplication);

        /// <summary>
        /// Retrieve list of all  the graduation applications for the given student Id asynchronously.
        /// </summary>
        /// <param name="studentId">Id of the student</param>
        /// <returns><see cref="Ellucian.Colleague.Dtos.Student.GraduationApplication">List of Graduation Application</see></returns>
        Task<IEnumerable<Ellucian.Colleague.Dtos.Student.GraduationApplication>> GetGraduationApplicationsAsync(string studentId);

        /// <summary>
        /// Validates an existing graduation application and updates it in the database, returning the updated application asynchronously.
        /// </summary>
        /// <param name="graduateApplication">Updated graduate application object</param>
        /// <returns><see cref="Ellucian.Colleague.Dtos.Student.GraduationApplication">Graduation Application</see> object that was updated</returns>
        Task<Ellucian.Colleague.Dtos.Student.GraduationApplication> UpdateGraduationApplicationAsync(Dtos.Student.GraduationApplication graduationApplication);

        /// <summary>
        /// Retrieves any applicable graduation application fee for the given student Id and program code asynchronously.
        /// </summary>
        /// <param name="studentId">Id of the student</param>
        /// <param name="programCode">program code that student is applying for graduation</param>
        /// <returns><see cref="Ellucian.Colleague.Dtos.Student.GraduationApplicationFee">Graduation ApplicationFee</see> object that was created</returns>
        Task<Ellucian.Colleague.Dtos.Student.GraduationApplicationFee> GetGraduationApplicationFeeAsync(string studentId, string programCode);
    }
}
