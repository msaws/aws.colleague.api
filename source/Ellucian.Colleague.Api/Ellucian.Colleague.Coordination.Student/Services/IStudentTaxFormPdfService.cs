﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System.Threading.Tasks;
using Ellucian.Colleague.Domain.Student.Entities;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    public interface IStudentTaxFormPdfService
    {
        /// <summary>
        /// Returns the pdf data to print a 1098 tax form.
        /// </summary>
        /// <param name="personId">ID of the person assigned to and requesting the 1098.</param>
        /// <param name="recordId">The record ID where the 1098 pdf data is stored</param>
        /// <returns>TaxForm1098PdfData domain entity</returns>
        Task<Form1098PdfData> Get1098TaxFormData(string personId, string recordId);

        /// <summary>
        /// Populates the 1098 PDF with the supplied data.
        /// </summary>
        /// <param name="pdfData">1098 PDF data</param>
        /// <param name="documentPath">Path to the PDF template</param>
        /// <returns>Byte array containing PDF data for the 1098 tax form</returns>
        byte[] Populate1098Pdf(Form1098PdfData pdfData, string documentPath);

        /// <summary>
        /// Populates the 1098 PDF with the supplied data using an RDLC template.
        /// </summary>
        /// <param name="pdfData">1098 PDF data</param>
        /// <param name="documentPath">Path to the RDLC template</param>
        /// <returns>Byte array containing PDF data for the 1098 tax form</returns>
        byte[] Populate1098tReport(Form1098PdfData pdfData, string documentPath);

    }
}
