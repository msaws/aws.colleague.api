﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    public interface IAcademicPeriodService
    {
        /// <remarks>OBSOLETE</remarks>
        /// <summary>
        /// Gets all academic periods
        /// </summary>
        /// <returns>Collection of Academic Period DTO objects</returns>
        Task<IEnumerable<Ellucian.Colleague.Dtos.AcademicPeriod>> GetAcademicPeriodsAsync();

        /// <remarks>OBSOLETE</remarks>
        /// <summary>
        /// Get an academic period from its GUID
        /// </summary>
        /// <returns>Academic Period DTO object</returns>
        Task<Ellucian.Colleague.Dtos.AcademicPeriod> GetAcademicPeriodByGuidAsync(string guid);
        
        /// <remarks>FOR USE WITH ELLUCIAN HEDM 4 </remarks>
        /// <summary>
        /// Gets all academic periods
        /// </summary>
        /// <returns>Collection of Academic Period DTO objects</returns>
        Task<IEnumerable<Ellucian.Colleague.Dtos.AcademicPeriod2>> GetAcademicPeriods2Async(bool bypassCache);

        /// <remarks>FOR USE WITH ELLUCIAN HEDM 8 </remarks>
        /// <summary>
        /// Gets all academic periods
        /// </summary>
        /// <returns>Collection of Academic Period DTO objects</returns>
        Task<IEnumerable<Ellucian.Colleague.Dtos.AcademicPeriod3>> GetAcademicPeriods3Async(bool bypassCache);

        /// <remarks>FOR USE WITH ELLUCIAN HEDM 4</remarks>
        /// <summary>
        /// Get an academic period from its GUID
        /// </summary>
        /// <returns>Academic Period DTO object</returns>
        Task<Ellucian.Colleague.Dtos.AcademicPeriod2> GetAcademicPeriodByGuid2Async(string guid);

        /// <remarks>FOR USE WITH ELLUCIAN HEDM 8</remarks>
        /// <summary>
        /// Get an academic period from its GUID
        /// including census dates and registration status.
        /// </summary>
        /// <returns>Academic Period DTO object</returns>
        Task<Ellucian.Colleague.Dtos.AcademicPeriod3> GetAcademicPeriodByGuid3Async(string guid);

    }
}