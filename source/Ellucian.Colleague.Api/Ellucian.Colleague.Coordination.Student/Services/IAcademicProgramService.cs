﻿using Ellucian.Colleague.Dtos.Student;
// Copyright 2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    public interface IAcademicProgramService
    {
        /// <summary>
        /// Gets all academic programs
        /// </summary>
        /// <returns>Collection of Academic Programs DTO objects</returns>
        Task<IEnumerable<AcademicProgram>> GetAsync();

        /// <remarks>FOR USE WITH ELLUCIAN HeDM 4 </remarks>
        /// <summary>
        /// Gets all academic programs
        /// </summary>
        /// <returns>Collection of Academic Programs DTO objects</returns>
        Task<IEnumerable<Ellucian.Colleague.Dtos.AcademicProgram>> GetAcademicProgramsAsync(bool bypassCache);

        /// <remarks>FOR USE WITH ELLUCIAN HeDM 4</remarks>
        /// <summary>
        /// Get an academic period from its GUID
        /// </summary>
        /// <returns>Academic Period DTO object</returns>
        Task<Ellucian.Colleague.Dtos.AcademicProgram> GetAcademicProgramByGuidAsync(string guid);

        /// <remarks>FOR USE WITH ELLUCIAN HeDM 6 </remarks>
        /// <summary>
        /// Gets all academic programs
        /// </summary>
        /// <returns>Collection of Academic Programs DTO objects</returns>
        Task<IEnumerable<Ellucian.Colleague.Dtos.AcademicProgram2>> GetAcademicProgramsV6Async(bool bypassCache);

        /// <remarks>FOR USE WITH ELLUCIAN HeDM 6</remarks>
        /// <summary>
        /// Get an academic period from its GUID
        /// </summary>
        /// <returns>Academic Period DTO object</returns>
        Task<Ellucian.Colleague.Dtos.AcademicProgram2> GetAcademicProgramByGuidV6Async(string guid);

        /// <remarks>FOR USE WITH ELLUCIAN HeDM 10 </remarks>
        /// <summary>
        /// Gets all academic programs
        /// </summary>
        /// <returns>Collection of Academic Programs DTO objects</returns>
        Task<IEnumerable<Ellucian.Colleague.Dtos.AcademicProgram3>> GetAcademicPrograms3Async(bool bypassCache);

        /// <remarks>FOR USE WITH ELLUCIAN HeDM 10</remarks>
        /// <summary>
        /// Get an academic period from its GUID
        /// </summary>
        /// <returns>Academic Period DTO object</returns>
        Task<Ellucian.Colleague.Dtos.AcademicProgram3> GetAcademicProgramByGuid3Async(string guid);
    }
}
