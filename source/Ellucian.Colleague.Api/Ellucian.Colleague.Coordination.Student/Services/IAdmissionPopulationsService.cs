﻿//Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    /// <summary>
    /// Interface for Admission Populations services
    /// </summary>
    public interface IAdmissionPopulationsService
    {
        Task<IEnumerable<AdmissionPopulations>> GetAdmissionPopulationsAsync(bool bypassCache = false);
        Task<AdmissionPopulations> GetAdmissionPopulationsByGuidAsync(string id);
    }
}
