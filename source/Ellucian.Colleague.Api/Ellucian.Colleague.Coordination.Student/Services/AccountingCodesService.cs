﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Web.Dependency;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.Student.Entities;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    [RegisterType]
    public class AccountingCodesService :IAccountingCodesService
    {
        private readonly IStudentReferenceDataRepository _studentReferenceDataRepository;
        private readonly ILogger _logger;

        public AccountingCodesService(IStudentReferenceDataRepository studentReferenceDataRepository, ILogger logger)
        {
            _studentReferenceDataRepository = studentReferenceDataRepository;
            _logger = logger;
        }
        #region IAccountingCodesService Members

        /// <summary>
        /// Returns all accounting codes
        /// </summary>
        /// <param name="bypassCache"></param>
        /// <returns></returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.AccountingCode>> GetAccountingCodesAsync(bool bypassCache)
        {
            var accountingCodeCollection = new List<Ellucian.Colleague.Dtos.AccountingCode>();

            var accountingCodes = await _studentReferenceDataRepository.GetAccountingCodesAsync(bypassCache);
            if (accountingCodes != null && accountingCodes.Any())
            {
                foreach (var accountingCode in accountingCodes)
                {
                    accountingCodeCollection.Add(ConvertAccountingCodeEntityToDto(accountingCode));
                }
            }
            return accountingCodeCollection;
        }      

        /// <summary>
        /// Returns an accounting code
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<Dtos.AccountingCode> GetAccountingCodeByIdAsync(string id)
        {
            var accountingCodeEntity = (await _studentReferenceDataRepository.GetAccountingCodesAsync(true)).FirstOrDefault(ac => ac.Guid == id);
            if (accountingCodeEntity == null)
            {
                throw new KeyNotFoundException("AR Code is not found.");
            }

            var accountingCode = ConvertAccountingCodeEntityToDto(accountingCodeEntity);
            return accountingCode;
        }
        #endregion

        #region Convert method(s)

        /// <summary>
        /// Converts from AccountingCode entity to AccountingCode dto
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        private Dtos.AccountingCode ConvertAccountingCodeEntityToDto(AccountingCode source)
        {
            Dtos.AccountingCode accountingCode = new Dtos.AccountingCode();
            accountingCode.Id = source.Guid;
            accountingCode.Code = source.Code;
            accountingCode.Title = source.Description;
            accountingCode.Description = string.Empty;
            return accountingCode;
        }

        #endregion
    }
}
