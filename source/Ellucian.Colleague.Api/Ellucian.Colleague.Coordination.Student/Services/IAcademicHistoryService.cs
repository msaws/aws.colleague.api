﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Student.Entities;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    public interface IAcademicHistoryService
    {
        Task<Ellucian.Colleague.Dtos.Student.AcademicHistory> GetAcademicHistoryAsync(string studentId, bool bestFit, bool filter = true, string term = null);
        Task<Ellucian.Colleague.Dtos.Student.AcademicHistory2> GetAcademicHistory2Async(string studentId, bool bestFit, bool filter = true, string term = null);
        Task<Ellucian.Colleague.Dtos.Student.AcademicHistory3> GetAcademicHistory3Async(string studentId, bool bestFit = false, bool filter = true, string term = null);

        Task<IEnumerable<Dtos.Student.AcademicHistoryBatch>> QueryAcademicHistoryAsync(Dtos.Student.AcademicHistoryQueryCriteria criteria);
        Task<IEnumerable<Dtos.Student.AcademicHistoryLevel>> QueryAcademicHistoryLevelAsync(Dtos.Student.AcademicHistoryQueryCriteria criteria);
        Task<IEnumerable<Dtos.Student.AcademicHistoryLevel2>> QueryAcademicHistoryLevel2Async(Dtos.Student.AcademicHistoryQueryCriteria criteria);
        Task<IEnumerable<Dtos.Student.PilotAcademicHistoryLevel>> QueryPilotAcademicHistoryLevelAsync(Dtos.Student.AcademicHistoryQueryCriteria criteria);
        Task<IEnumerable<Dtos.Student.AcademicHistoryBatch>> GetAcademicHistoryByIdsAsync(IEnumerable<string> studentIds, bool bestFit, bool filter = true, string term = null);

        Task<Ellucian.Colleague.Dtos.Student.AcademicHistory2> ConvertAcademicCreditsToAcademicHistoryDtoAsync(string studentId, IEnumerable<AcademicCredit> academicCredits, Domain.Student.Entities.Student student = null);
        Task<Ellucian.Colleague.Dtos.Student.AcademicHistory3> ConvertAcademicCreditsToAcademicHistoryDto2Async(string studentId, IEnumerable<AcademicCredit> academicCredits, Domain.Student.Entities.Student student = null);
        Task<IEnumerable<Dtos.Student.AcademicHistoryLevel>> GetAcademicHistoryLevelByIdsAsync(IEnumerable<string> studentIds, bool bestFit, bool filter = true, string term = null);
        Task<IEnumerable<Dtos.Student.AcademicHistoryLevel2>> GetAcademicHistoryLevel2ByIdsAsync(IEnumerable<string> studentIds, bool bestFit, bool filter = true, string term = null);
        Task<IEnumerable<Dtos.Student.PilotAcademicHistoryLevel>> GetPilotAcademicHistoryLevelByIdsAsync(IEnumerable<string> studentIds, bool bestFit, bool filter = true, string term = null);
        Task<IEnumerable<Dtos.Student.StudentEnrollment>> GetInvalidStudentEnrollmentAsync(IEnumerable<Dtos.Student.StudentEnrollment> enrollmentKeys);
        /// <summary>
        /// Returns a list of academic credits for the specified section Ids in the criteria
        /// </summary>
        /// <param name="criteria">Criteria that contains a list of sections and some other options</param>
        /// <returns>List of AcademicCredit2 Dtos</returns>
        Task<IEnumerable<Dtos.Student.AcademicCredit2>> QueryAcademicCreditsAsync(Dtos.Student.AcademicCreditQueryCriteria criteria);
    }
}
