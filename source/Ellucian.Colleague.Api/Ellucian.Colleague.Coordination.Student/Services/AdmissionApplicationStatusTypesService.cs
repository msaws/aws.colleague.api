//Copyright 2017 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Coordination.Student.Adapters;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos;
using Ellucian.Colleague.Dtos.EnumProperties;
using Ellucian.Colleague.Domain.Base.Repositories;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    [RegisterType]
    public class AdmissionApplicationStatusTypesService : StudentCoordinationService, IAdmissionApplicationStatusTypesService
    {

        private readonly IStudentReferenceDataRepository _referenceDataRepository;
        private readonly IConfigurationRepository _configurationRepository;

        public AdmissionApplicationStatusTypesService(

            IStudentReferenceDataRepository referenceDataRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger,
            IConfigurationRepository configurationRepository)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger, null, configurationRepository)
        {
            _configurationRepository = configurationRepository;
            _referenceDataRepository = referenceDataRepository;
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM VERSION 7</remarks>
        /// <summary>
        /// Gets all admission-application-status-types
        /// </summary>
        /// <returns>Collection of AdmissionApplicationStatusTypes DTO objects</returns>
        public async Task<IEnumerable<Ellucian.Colleague.Dtos.AdmissionApplicationStatusType>> GetAdmissionApplicationStatusTypesAsync(bool bypassCache = false)
        {
            var admissionApplicationStatusTypesCollection = new List<Ellucian.Colleague.Dtos.AdmissionApplicationStatusType>();

            var admissionApplicationStatusTypesEntities = await _referenceDataRepository.GetAdmissionApplicationStatusTypesAsync(bypassCache);
            if (admissionApplicationStatusTypesEntities != null && admissionApplicationStatusTypesEntities.Any())
            {
                foreach (var admissionApplicationStatusTypes in admissionApplicationStatusTypesEntities)
                {
                    admissionApplicationStatusTypesCollection.Add(ConvertAdmissionApplicationStatusTypesEntityToDto(admissionApplicationStatusTypes));
                }
            }
            return admissionApplicationStatusTypesCollection;
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM VERSION 7</remarks>
        /// <summary>
        /// Get a AdmissionApplicationStatusTypes from its GUID
        /// </summary>
        /// <returns>AdmissionApplicationStatusTypes DTO object</returns>
        public async Task<Ellucian.Colleague.Dtos.AdmissionApplicationStatusType> GetAdmissionApplicationStatusTypesByGuidAsync(string guid)
        {
            try
            {
                return ConvertAdmissionApplicationStatusTypesEntityToDto((await _referenceDataRepository.GetAdmissionApplicationStatusTypesAsync(true)).Where(r => r.Guid == guid).First());
            }
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException("admission-application-status-types not found for GUID " + guid, ex);
            }
            catch (InvalidOperationException ex)
            {
                throw new KeyNotFoundException("admission-application-status-types not found for GUID " + guid, ex);
            }
        }


        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Converts a AdmissionApplicationStatusTypes domain entity to its corresponding AdmissionApplicationStatusTypes DTO
        /// </summary>
        /// <param name="source">AdmissionApplicationStatusTypes domain entity</param>
        /// <returns>AdmissionApplicationStatusTypes DTO</returns>
        private Ellucian.Colleague.Dtos.AdmissionApplicationStatusType ConvertAdmissionApplicationStatusTypesEntityToDto(Ellucian.Colleague.Domain.Student.Entities.AdmissionApplicationStatusType source)
        {
            var admissionApplicationStatusTypes = new Ellucian.Colleague.Dtos.AdmissionApplicationStatusType();

            admissionApplicationStatusTypes.Id = source.Guid;
            admissionApplicationStatusTypes.Code = source.Code;
            admissionApplicationStatusTypes.Title = source.Description;
            admissionApplicationStatusTypes.Description = null;

            admissionApplicationStatusTypes.Category = ConvertDomainEnumToCategoryDtoEnum(source.AdmissionApplicationStatusTypesCategory);

            return admissionApplicationStatusTypes;
        }



        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Converts a AdmissionApplicationStatusTypesCategory domain enumeration value to its corresponding AdmissionApplicationStatusTypesCategory DTO enumeration value
        /// </summary>
        /// <param name="source">AdmissionApplicationStatusTypesCategory domain enumeration value</param>
        /// <returns>AdmissionApplicationStatusTypesCategory DTO enumeration value</returns>
        private Ellucian.Colleague.Dtos.EnumProperties.AdmissionApplicationStatusTypesCategory ConvertDomainEnumToCategoryDtoEnum(Ellucian.Colleague.Domain.Student.Entities.AdmissionApplicationStatusTypesCategory source)
        {
            switch (source)
            {
                case Ellucian.Colleague.Domain.Student.Entities.AdmissionApplicationStatusTypesCategory.Started:
                    return Dtos.EnumProperties.AdmissionApplicationStatusTypesCategory.Started;
                case Ellucian.Colleague.Domain.Student.Entities.AdmissionApplicationStatusTypesCategory.Submitted:
                    return Dtos.EnumProperties.AdmissionApplicationStatusTypesCategory.Submitted;
                case Ellucian.Colleague.Domain.Student.Entities.AdmissionApplicationStatusTypesCategory.Readyforreview:
                    return Dtos.EnumProperties.AdmissionApplicationStatusTypesCategory.Readyforreview;
                case Ellucian.Colleague.Domain.Student.Entities.AdmissionApplicationStatusTypesCategory.Decisionmade:
                    return Dtos.EnumProperties.AdmissionApplicationStatusTypesCategory.Decisionmade;
                case Ellucian.Colleague.Domain.Student.Entities.AdmissionApplicationStatusTypesCategory.Enrollmentcomplete:
                    return Dtos.EnumProperties.AdmissionApplicationStatusTypesCategory.Enrollmentcomplete;
                default:
                    return Dtos.EnumProperties.AdmissionApplicationStatusTypesCategory.Started;
            }
        }
    }
}