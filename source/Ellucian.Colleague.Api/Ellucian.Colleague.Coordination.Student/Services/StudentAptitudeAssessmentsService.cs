﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Coordination.Student.Adapters;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System.Threading.Tasks;
using Ellucian.Colleague.Dtos;
using Ellucian.Colleague.Dtos.EnumProperties;
using Ellucian.Colleague.Domain.Student;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Exceptions;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    [RegisterType]
    public class StudentAptitudeAssessmentsService : StudentCoordinationService, IStudentAptitudeAssessmentsService
    {
        private readonly IStudentAptitudeAssessmentsRepository _studentAptitudeAssessmentsRepository;
        private readonly IPersonRepository _personRepository;
        private readonly IStudentReferenceDataRepository _studentReferenceDataRepository;
        private readonly IAptitudeAssessmentsRepository _aptitudeAssessmentsRepository;
        private readonly IConfigurationRepository _configurationRepository;

        public StudentAptitudeAssessmentsService(
            IStudentAptitudeAssessmentsRepository studentAptitudeAssessmentsRepository,
            IPersonRepository personRepository,
            IStudentReferenceDataRepository studentReferenceDataRepository,
            IAptitudeAssessmentsRepository aptitudeAssessmentsRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            IStudentRepository studentRepository,
            ILogger logger,
            IConfigurationRepository configurationRepository)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger, studentRepository, configurationRepository)
        {
            _configurationRepository = configurationRepository;
            _personRepository = personRepository;
            _studentAptitudeAssessmentsRepository = studentAptitudeAssessmentsRepository;
            _studentReferenceDataRepository = studentReferenceDataRepository;
            _aptitudeAssessmentsRepository = aptitudeAssessmentsRepository;
        }


        private IEnumerable<Domain.Student.Entities.NonCourse> _appitudeAssessments = null;
        private async Task<IEnumerable<Domain.Student.Entities.NonCourse>> GetAptitudeAssessmentAsync()
        {
            if (_appitudeAssessments == null)
            {
                _appitudeAssessments = await _aptitudeAssessmentsRepository.GetAptitudeAssessmentsAsync(false);
            }
            return _appitudeAssessments;
        }

        private IEnumerable<Domain.Student.Entities.AssessmentSpecialCircumstance> _assessmentSpecialCircumstances = null;
        private async Task<IEnumerable<Domain.Student.Entities.AssessmentSpecialCircumstance>> GetAssessmentSpecialCircumstancesAsync()
        {
            if (_assessmentSpecialCircumstances == null)
            {
                _assessmentSpecialCircumstances = await _studentReferenceDataRepository.GetAssessmentSpecialCircumstancesAsync(false);
            }
            return _assessmentSpecialCircumstances;
        }
        private IEnumerable<Domain.Student.Entities.IntgTestPercentileType> _assesmentPercentileTypes = null;
        private async Task<IEnumerable<Domain.Student.Entities.IntgTestPercentileType>> GetAssesmentPercentileTypesAsync()
        {
            if (_assesmentPercentileTypes == null)
            {
                _assesmentPercentileTypes = await _studentReferenceDataRepository.GetIntgTestPercentileTypesAsync(false);
            }
            return _assesmentPercentileTypes;
        }
        private IEnumerable<Domain.Student.Entities.TestSource> _testSource = null;
        private async Task<IEnumerable<Domain.Student.Entities.TestSource>> GetTestSourcesAsync()
        {
            if (_testSource == null)
            {
                _testSource = await _studentReferenceDataRepository.GetTestSourcesAsync(false);
            }
            return _testSource;
        }

        /// <remarks>FOR USE WITH ELLUCIAN EEDM VERSION 9</remarks>
        /// <summary>
        /// Gets all student-aptitude-assessments
        /// </summary>
        /// <returns>Collection of StudentAptitudeAssessments DTO objects</returns>
        public async Task<Tuple<IEnumerable<Ellucian.Colleague.Dtos.StudentAptitudeAssessments>,int>> GetStudentAptitudeAssessmentsAsync(int offset, int limit, bool bypassCache = false)
        {
            var studentAptitudeAssessmentsCollection = new List<Ellucian.Colleague.Dtos.StudentAptitudeAssessments>();
            CheckGetStudentAptitudeAssessmentsPermission();
            var studentAptitudeAssessmentsEntitiesTuple = await  _studentAptitudeAssessmentsRepository.GetStudentAptitudeAssessmentsAsync(offset, limit, bypassCache);
            if (studentAptitudeAssessmentsEntitiesTuple != null )
            {
                var studentAptitudeAssessmentsEntities = studentAptitudeAssessmentsEntitiesTuple.Item1;
                var totalCount = studentAptitudeAssessmentsEntitiesTuple.Item2;
                if (studentAptitudeAssessmentsEntities != null && studentAptitudeAssessmentsEntities.Any())
                {
                    //var check = await ConvertStudentAptitudeAssessmentsEntityToDto(studentAptitudeAssessmentsEntities.ToList(), bypassCache);
                    return new Tuple<IEnumerable<Dtos.StudentAptitudeAssessments>,int>(await ConvertStudentAptitudeAssessmentsEntityToDto(studentAptitudeAssessmentsEntities.ToList(), bypassCache), totalCount);
                }
                else
                {
                    return new Tuple<IEnumerable<Dtos.StudentAptitudeAssessments>,int>(new List<Dtos.StudentAptitudeAssessments>(),0);
                }

            }
            else
            {
                return new Tuple<IEnumerable<Dtos.StudentAptitudeAssessments>, int>(new List<Dtos.StudentAptitudeAssessments>(), 0);
            }
           
        }


        /// <remarks>FOR USE WITH ELLUCIAN EEDM VERSION 7</remarks>
        /// <summary>
        /// Get a StudentAptitudeAssessments from its GUID
        /// </summary>
        /// <returns>StudentAptitudeAssessments DTO object</returns>
        public async Task<Ellucian.Colleague.Dtos.StudentAptitudeAssessments> GetStudentAptitudeAssessmentsByGuidAsync(string guid)
        {
            try
            {
                 if (string.IsNullOrEmpty(guid))
                {
                    throw new ArgumentNullException("guid", "GUID is required to get an Student Aptitude Assessments.");
                }
                CheckGetStudentAptitudeAssessmentsPermission();
                var stuAptAssessmentsEntities = new List<StudentTestScores>();
                var stuAptAss = await _studentAptitudeAssessmentsRepository.GetStudentAptitudeAssessmentsByGuidAsync(guid);
                if (stuAptAss != null)
                {
                    stuAptAssessmentsEntities.Add(stuAptAss);
                    return (await ConvertStudentAptitudeAssessmentsEntityToDto(stuAptAssessmentsEntities, true)).FirstOrDefault();
                }
                else
                {
                    throw new KeyNotFoundException("student-aptitude-assessments not found for GUID " + guid);
                }
            }
            
           
            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException("student-aptitude-assessments not found for GUID " + guid, ex);
            }
            catch (InvalidOperationException ex)
            {
                throw new KeyNotFoundException("student-aptitude-assessments not found for GUID " + guid, ex);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// Convert Aptitude Assessment code to GUID
        /// </summary>
        /// <param name="aptitudeAssessments">List of Aptitude Assesments Entities</param>
        /// <param name="aptitudeAssesmentCode">Aptitude Assesment Code</param>
        /// <returns>Aptitude Assesment GUID</returns>
        private async Task<Ellucian.Colleague.Dtos.GuidObject2> ConvertAptitudeAssesmentCodeToGuid(IEnumerable<NonCourse> aptitudeAssessments, string aptitudeAssesmentCode)
        {
            Ellucian.Colleague.Dtos.GuidObject2 guidObject = null;

            if (!string.IsNullOrEmpty(aptitudeAssesmentCode))
            {
                if (aptitudeAssessments != null && aptitudeAssessments.Any())
                {
                    var acadCatalog = aptitudeAssessments.FirstOrDefault(a => a.Code == aptitudeAssesmentCode);
                    if (acadCatalog != null)
                    {
                        var acadGuid = acadCatalog.Guid;
                        if (!string.IsNullOrEmpty(acadGuid))
                        {
                            guidObject = new Ellucian.Colleague.Dtos.GuidObject2(acadGuid);
                        }
                    }
                }
            }
            return guidObject;
        }
        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Converts a StudentAptitudeAssessments domain entity to its corresponding StudentAptitudeAssessments DTO
        /// </summary>
        /// <param name="stuAptAssEntities">StudentAptitudeAssessments domain entity</param>
        /// <returns>StudentAptitudeAssessments DTO</returns>
        private async Task<IEnumerable<Ellucian.Colleague.Dtos.StudentAptitudeAssessments>> ConvertStudentAptitudeAssessmentsEntityToDto(List<StudentTestScores> stuAptAssEntities, bool bypassCache)
        {
                var studentAptitudeAssessmentsDtos = new List<Ellucian.Colleague.Dtos.StudentAptitudeAssessments>();
                foreach (var stuAptAssEnt in stuAptAssEntities)
                {
                    try
                    {
                        var stuAptAssDto = new StudentAptitudeAssessments();
                        stuAptAssDto.Id = stuAptAssEnt.Guid;
                        if (!string.IsNullOrEmpty(stuAptAssEnt.StudentId))
                        {
                            var studentGuid = await _personRepository.GetPersonGuidFromIdAsync(stuAptAssEnt.StudentId);
                            if (!string.IsNullOrEmpty(studentGuid))
                            {
                                stuAptAssDto.Student = new Dtos.GuidObject2(studentGuid);
                            }
                        }
                        if (!string.IsNullOrEmpty(stuAptAssEnt.Code))
                        {
                            var aptitudeGuid = await ConvertAptitudeAssesmentCodeToGuid(await this.GetAptitudeAssessmentAsync(), stuAptAssEnt.Code);
                            if (aptitudeGuid != null)
                            {
                                stuAptAssDto.Assessment = aptitudeGuid;
                            }
                        }
                        stuAptAssDto.AssessedOn = stuAptAssEnt.DateTaken;
                        if (stuAptAssEnt.Score.HasValue)
                        {
                            var score = new StudentAptitudeAssessmentsScore();
                            score.Type = Dtos.EnumProperties.StudentAptitudeAssessmentsScoreType.Numeric;
                            score.Value = stuAptAssEnt.Score;
                            stuAptAssDto.Score = score;
                        }
                        else
                        {
                            throw new KeyNotFoundException("The student-aptitude-assessments with ID '" + stuAptAssDto.Id + "' does not have a score. Score is required.");
                        }
                        var percentiles = new List<StudentAptitudeAssessmentsPercentile>();
                        if (stuAptAssEnt.Percentile1.HasValue)
                        {
                            var percentileType = ConvertCodeToGuid(await this.GetAssesmentPercentileTypesAsync(), "1");
                            if (!string.IsNullOrEmpty(percentileType))
                            {
                                var percentile = new StudentAptitudeAssessmentsPercentile();
                                percentile.Value = stuAptAssEnt.Percentile1;
                                percentile.Type = new Dtos.GuidObject2(percentileType);
                                percentiles.Add(percentile);
                            }
                        }
                        if (stuAptAssEnt.Percentile2.HasValue)
                        {
                            var percentileType = ConvertCodeToGuid(await this.GetAssesmentPercentileTypesAsync(), "2");
                            if (!string.IsNullOrEmpty(percentileType))
                            {
                                var percentile = new StudentAptitudeAssessmentsPercentile();
                                percentile.Value = stuAptAssEnt.Percentile2;
                                percentile.Type = new Dtos.GuidObject2(percentileType);
                                percentiles.Add(percentile);
                            }
                        }
                        if (percentiles.Any())
                        {
                            stuAptAssDto.Percentile = percentiles;
                        }
                        if (!(string.IsNullOrEmpty(stuAptAssEnt.FormNo)) || !(string.IsNullOrEmpty(stuAptAssEnt.FormName)))
                        {
                            var form = new StudentAptitudeAssessmentsForm();
                            form.Name = stuAptAssEnt.FormName;
                            form.Number = stuAptAssEnt.FormNo;
                            stuAptAssDto.Form = form;
                        }
                        if (stuAptAssEnt.SpecialFactors.Any())
                        {
                            var factors = new List<Dtos.GuidObject2>();
                            foreach (var factor in stuAptAssEnt.SpecialFactors)
                            {
                                var factorGuid = ConvertCodeToGuid(await this.GetAssessmentSpecialCircumstancesAsync(), factor);
                                if (factorGuid != null)
                                {
                                    factors.Add(new Dtos.GuidObject2(factorGuid));
                                }
                            }
                            stuAptAssDto.SpecialCircumstances = factors;
                        }
                        
                        if (!string.IsNullOrEmpty(stuAptAssEnt.Source))
                        {
                            var sourceGuid = ConvertCodeToGuid(await this.GetTestSourcesAsync(), stuAptAssEnt.Source);
                            if (sourceGuid != null)
                            {
                                stuAptAssDto.Source = new Dtos.GuidObject2(sourceGuid);
                            }
                        }
                        if (!string.IsNullOrEmpty(stuAptAssEnt.StatusCode))
                        {
                             switch (stuAptAssEnt.StatusCodeSpProcessing)
                            {
                                case "1": stuAptAssDto.Status = StudentAptitudeAssessmentsStatus.Inactive;
                                    stuAptAssDto.Reported = StudentAptitudeAssessmentsReported.Unofficial;
                                    break;
                                case "2": stuAptAssDto.Status = StudentAptitudeAssessmentsStatus.Active;
                                    stuAptAssDto.Reported = StudentAptitudeAssessmentsReported.Official;
                                    stuAptAssDto.Preference = StudentAptitudeAssessmentsPreference.Primary;
                                    break;
                                case "3": stuAptAssDto.Status = StudentAptitudeAssessmentsStatus.Active;
                                    stuAptAssDto.Reported = StudentAptitudeAssessmentsReported.Unofficial;
                                    break;
                                default: stuAptAssDto.Status = StudentAptitudeAssessmentsStatus.Active;
                                    stuAptAssDto.Reported = StudentAptitudeAssessmentsReported.Unofficial;
                                    break;
                            }


                        }
                        studentAptitudeAssessmentsDtos.Add(stuAptAssDto);
                    }
                    catch (Exception ex)
                    {
                        throw new KeyNotFoundException("Student Aptitude Assessment not found for ID." + stuAptAssEnt.Guid);
                    }

                }

                return studentAptitudeAssessmentsDtos;
        }

        /// <summary>
        /// Helper method to determine if the user has permission to view Student Aptitude Assessments.
        /// </summary>
        /// <exception><see cref="PermissionsException">PermissionsException</see></exception>
        private void CheckGetStudentAptitudeAssessmentsPermission()
        {
            bool hasPermission = HasPermission(StudentPermissionCodes.ViewStudentAptitudeAssessmentsConsent);

            // User is not allowed to create or update Student Aptitude Assessments without the appropriate permissions
            if (!hasPermission)
            {
                throw new PermissionsException("User " + CurrentUser.UserId + " does not have permission to view Student Aptitude Assessments (Test Scores).");
            }
        }

      
    }
   
}