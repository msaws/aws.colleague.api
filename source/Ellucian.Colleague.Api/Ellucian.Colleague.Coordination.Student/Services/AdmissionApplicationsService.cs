﻿
//Copyright 2017 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Coordination.Student.Adapters;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Exceptions;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Student;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Colleague.Dtos;
using Ellucian.Colleague.Dtos.DtoProperties;
using Ellucian.Colleague.Dtos.EnumProperties;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    [RegisterType]
    public class AdmissionApplicationsService : BaseCoordinationService, IAdmissionApplicationsService
    {
        private readonly IAdmissionApplicationsRepository _admissionApplicationsRepository;
        private readonly ITermRepository _termRepository;
        private readonly IInstitutionRepository _institutionRepository;
        private readonly IStudentReferenceDataRepository _studentReferenceDataRepository;
        private readonly IReferenceDataRepository _referenceDataRepository;
        private readonly IConfigurationRepository _configurationRepository;

        /// <summary>
        /// ...ctor
        /// </summary>
        /// <param name="admissionApplicationsRepository"></param>
        /// <param name="termRepository"></param>
        /// <param name="iInstitutionRepository"></param>
        /// <param name="studentReferenceDataRepository"></param>
        /// <param name="referenceDataRepository"></param>
        /// <param name="configurationRepository">The configuration repository</param>
        /// <param name="adapterRegistry"></param>
        /// <param name="currentUserFactory"></param>
        /// <param name="roleRepository"></param>
        /// <param name="logger"></param>
        public AdmissionApplicationsService(
            IAdmissionApplicationsRepository admissionApplicationsRepository,
            ITermRepository termRepository,
            IInstitutionRepository iInstitutionRepository,
            IStudentReferenceDataRepository studentReferenceDataRepository,
            IReferenceDataRepository referenceDataRepository,
            IConfigurationRepository configurationRepository,
            IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory,
            IRoleRepository roleRepository,
            ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger, configurationRepository: configurationRepository)
        {
            _admissionApplicationsRepository = admissionApplicationsRepository;
            _termRepository = termRepository;
            _institutionRepository = iInstitutionRepository;
            _studentReferenceDataRepository = studentReferenceDataRepository;
            _referenceDataRepository = referenceDataRepository;
            _configurationRepository = configurationRepository;
        }

        #region GetAll, GetById

        /// <remarks>FOR USE WITH ELLUCIAN EEDM VERSION 6</remarks>
        /// <summary>
        /// Gets all admission-applications
        /// </summary>
        /// <returns>Collection of AdmissionApplications DTO objects</returns>
        public async Task<Tuple<IEnumerable<Ellucian.Colleague.Dtos.AdmissionApplication>, int>> GetAdmissionApplicationsAsync(int offset, int limit, bool bypassCache = false)
        {
            try
            {
                CheckUserAdmissionApplicationsViewPermissions();

                var admissionApplicationsCollection = new List<Ellucian.Colleague.Dtos.AdmissionApplication>();

                Tuple<IEnumerable<Ellucian.Colleague.Domain.Student.Entities.AdmissionApplication>, int> admissionApplicationsEntities = await _admissionApplicationsRepository.GetAdmissionApplicationsAsync(offset, limit, bypassCache);
                if (admissionApplicationsEntities != null && admissionApplicationsEntities.Item1.Any())
                {
                    _personIds = await BuildLocalPersonGuids(admissionApplicationsEntities.Item1);

                    foreach (var admissionApplications in admissionApplicationsEntities.Item1)
                    {
                        admissionApplicationsCollection.Add(await ConvertAdmissionApplicationsEntityToDto(admissionApplications));
                    }
                }
                return admissionApplicationsCollection.Any() ? new Tuple<IEnumerable<Ellucian.Colleague.Dtos.AdmissionApplication>, int>(admissionApplicationsCollection, admissionApplicationsEntities.Item2) :
                    new Tuple<IEnumerable<Ellucian.Colleague.Dtos.AdmissionApplication>, int>(new List<Ellucian.Colleague.Dtos.AdmissionApplication>(), 0);
            }
            catch (ArgumentNullException ex) 
            {
                logger.Error(ex.Message);
                throw new ArgumentNullException(ex.Message, ex);
            }
            catch (KeyNotFoundException ex)
            {
                logger.Error(ex.Message);
                throw new KeyNotFoundException(ex.Message, ex);
            }
            catch (PermissionsException ex)
            {
                logger.Error(ex.Message);
                throw new KeyNotFoundException(ex.Message, ex);
            }
            catch (InvalidOperationException ex)
            {
                logger.Error(ex.Message);
                throw new InvalidOperationException(ex.Message, ex);
            }
            catch(Exception ex)
            {
                logger.Error(ex.Message);
                throw new Exception(ex.Message, ex);
            }
        }    

        /// <remarks>FOR USE WITH ELLUCIAN EEDM VERSION 6</remarks>
        /// <summary>
        /// Get a AdmissionApplications from its GUID
        /// </summary>
        /// <returns>AdmissionApplications DTO object</returns>
        public async Task<Ellucian.Colleague.Dtos.AdmissionApplication> GetAdmissionApplicationsByGuidAsync(string guid)
        {
            try
            {
                CheckUserAdmissionApplicationsViewPermissions();

                Ellucian.Colleague.Domain.Student.Entities.AdmissionApplication admissionApplication = await _admissionApplicationsRepository.GetAdmissionApplicationByIdAsync(guid);
                _personIds = await BuildLocalPersonGuids(new List<Ellucian.Colleague.Domain.Student.Entities.AdmissionApplication>() { admissionApplication });
                return await ConvertAdmissionApplicationsEntityToDto(admissionApplication);
            }
            catch (ArgumentNullException ex)
            {
                logger.Error(ex.Message);
                throw new ArgumentNullException(ex.Message, ex);
            }
            catch (KeyNotFoundException ex)
            {
                logger.Error(ex.Message);
                throw new KeyNotFoundException(ex.Message, ex);
            }
            catch (PermissionsException ex)
            {
                logger.Error(ex.Message);
                throw new KeyNotFoundException(ex.Message, ex);
            }
            catch (InvalidOperationException ex)
            {
                logger.Error(ex.Message);
                throw new InvalidOperationException(ex.Message, ex);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                throw new Exception(ex.Message, ex);
            }
        }

        /// <summary>
        /// Verifies if the user has the correct permissions to view applications.
        /// </summary>
        private void CheckUserAdmissionApplicationsViewPermissions()
        {
            // access is ok if the current user has the view registrations permission
            if (!HasPermission(StudentPermissionCodes.ViewApplications))
            {
                logger.Error("User '" + CurrentUser.UserId + "' is not authorized to view admission-applications.");
                throw new PermissionsException("User is not authorized to view admission-applications.");
            }
        }
        #endregion             

        #region Convert Methods

        /// <remarks>FOR USE WITH ELLUCIAN EEDM</remarks>
        /// <summary>
        /// Converts a AdmissionApplications domain entity to its corresponding AdmissionApplications DTO
        /// </summary>
        /// <param name="source">AdmissionApplications domain entity</param>
        /// <returns>AdmissionApplications DTO</returns>
        private async Task<Ellucian.Colleague.Dtos.AdmissionApplication> ConvertAdmissionApplicationsEntityToDto(Ellucian.Colleague.Domain.Student.Entities.AdmissionApplication source)
        {
            try
            {
                var admissionApplication = new Ellucian.Colleague.Dtos.AdmissionApplication();
                //Required fields
                admissionApplication.Id = source.Guid;

                if (string.IsNullOrEmpty(source.ApplicantPersonId))
                {
                    var error = string.Concat("Admission application source is required for guid ", source.Guid);
                    logger.Error(error);
                    throw new InvalidOperationException(error);
                }
                admissionApplication.Applicant = await ConvertEntityKeyToPersonGuidObjectAsync(source.ApplicantPersonId);

                if (source.AdmissionApplicationStatuses == null)
                {
                    var error = string.Concat("Admission application statuses is required for guid ", source.Guid);
                    logger.Error(error);
                    throw new InvalidOperationException(error);
                }

                if (source.AdmissionApplicationStatuses != null && source.AdmissionApplicationStatuses.Any())
                {
                    var statuses = source.AdmissionApplicationStatuses.Select(i => i.ApplicationStatus);
                    if (statuses != null && !statuses.Any())
                    {
                        var error = string.Concat("Admission application statuses are required for guid ", source.Guid); ;
                        logger.Error(error);
                        throw new KeyNotFoundException(error);
                    }
                    admissionApplication.Statuses = await ConvertEntityToStatusesDtoAsync(source.AdmissionApplicationStatuses);
                                        
                    var admissionApplicationStatus = source.AdmissionApplicationStatuses
                        .Where(i => i.ApplicationStatus != null)
                        .OrderByDescending(dt => dt.ApplicationStatusDate)
                        .FirstOrDefault();

                    try
                    {
                        var id = string.Empty;
                        if (_staffOperIdsDict != null && (!string.IsNullOrEmpty(source.ApplicationAdmissionsRep) || !string.IsNullOrEmpty(admissionApplicationStatus.ApplicationDecisionBy)))
                        {
                            //Check first if admin rep has value
                            if (!string.IsNullOrEmpty(source.ApplicationAdmissionsRep))
                            {
                                admissionApplication.Owner = await ConvertEntityKeyToPersonGuidObjectAsync(source.ApplicationAdmissionsRep);
                            }
                            else if (_staffOperIdsDict.TryGetValue(source.ApplicationAdmissionsRep, out id))
                            {
                                if (!string.IsNullOrEmpty(id))
                                {
                                    admissionApplication.Owner = await ConvertEntityKeyToPersonGuidObjectAsync(id);
                                }
                            }
                            // else check if _staffOperIdsDict has the value
                            else if (_staffOperIdsDict.TryGetValue(admissionApplicationStatus.ApplicationDecisionBy, out id))
                            {
                                if (!string.IsNullOrEmpty(id))
                                {
                                    admissionApplication.Owner = await ConvertEntityKeyToPersonGuidObjectAsync(id);
                                }
                            }
                            else
                            {
                                admissionApplication.Owner = await ConvertEntityKeyToPersonGuidObjectAsync(admissionApplicationStatus.ApplicationDecisionBy);
                            }
                        }                        
                    }
                    catch
                    {
                        //Owner id is not required so no need to throw error
                    }
                }                

                admissionApplication.School = await ConvertEntityToSchoolGuidObjectAsync(source.ApplicationSchool);
                admissionApplication.ReferenceID = string.IsNullOrEmpty(source.ApplicationNo) ? null : source.ApplicationNo;
                admissionApplication.Type = await ConvertEntityToTypeGuidObjectDtoAsync();
                admissionApplication.AcademicPeriod = await ConvertEntityToAcademicPeriodGuidObjectAsync(source.ApplicationStartTerm);
                admissionApplication.Source = await ConvertEntityToApplicationSourceGuidObjectDtoAsync(source.ApplicationSource);
                admissionApplication.AdmissionPopulation = await ConvertEntityToAdmitStatusGuidObjectDtoAsync(source.ApplicationAdmitStatus);
                admissionApplication.Site = await ConvertEntityToApplLocationGuidObjectDtoAsync(source.ApplicationLocations);
                admissionApplication.ResidencyType = await ConvertEntityToApplResidencyStatusGuidObjectDtoAsync(source.ApplicationResidencyStatus);
                admissionApplication.Program = await ConvertEntityToAcadProgGuidObjectDtoAsync(source.ApplicationAcadProgram);
                admissionApplication.Level = await ConvertEntityToAcadProgLevelGuidObjectDtoAsync(source.ApplicationAcadProgram);
                admissionApplication.Disciplines = await ConvertEntityToDisciplinesDtoAsync(source.ApplicationAcadProgram, source.ApplicationStprAcadPrograms);
                admissionApplication.AcademicLoad = ConvertEntityToAcaDemicLoadTypeDto(source.ApplicationStudentLoadIntent);
                admissionApplication.Withdrawal = await ConvertEntityToWithdrawlDtoAsync(source.ApplicationAttendedInstead, source.ApplicationWithdrawReason);
                admissionApplication.Comment = string.IsNullOrEmpty(source.ApplicationComments) ? null : source.ApplicationComments;

                return admissionApplication;
            }
            catch (ArgumentNullException ex)
            {
                throw new KeyNotFoundException(string.Concat(ex.Message, "admission application guid: ", source.Guid), ex);
            }

            catch (KeyNotFoundException ex)
            {
                throw new KeyNotFoundException(string.Concat(ex.Message, "admission application guid: ", source.Guid), ex);
            }
            catch (Exception ex)
            {
                var error = string.Concat("Something unexpected happened for guid ", source.Guid);
                throw new KeyNotFoundException(error, ex);
            }            
        }

        /// <summary>
        /// Gets person guid object
        /// </summary>
        /// <param name="personRecordKey"></param>
        /// <returns></returns>
        private async Task<GuidObject2> ConvertEntityKeyToPersonGuidObjectAsync(string personRecordKey)
        {
            if (string.IsNullOrEmpty(personRecordKey)) 
            {
                throw new ArgumentNullException("Person key is required. ");
            }

            var source = (await PersonGuidsAsync()).FirstOrDefault(i => i.Key.Equals(personRecordKey, StringComparison.OrdinalIgnoreCase));
            if (source.Equals(default(KeyValuePair<string, string>)))
            {
                var error = string.Format("Person not found for key {0}. ", personRecordKey);
                throw new KeyNotFoundException(error);
            }
            return string.IsNullOrEmpty(source.Value) ? null : new GuidObject2(source.Value);
        }

        /// <summary>
        /// Gets status object
        /// </summary>
        /// <param name="admissionApplicationStatuses"></param>
        /// <returns></returns>
        private async Task<List<Dtos.DtoProperties.AdmissionApplicationsStatus>> ConvertEntityToStatusesDtoAsync(List<AdmissionApplicationStatus> admissionApplicationStatuses)
        {
            if (admissionApplicationStatuses == null || (admissionApplicationStatuses != null && !admissionApplicationStatuses.Any()))
            {
                throw new ArgumentNullException("Statuses are required. ");
            }

            if (admissionApplicationStatuses.FirstOrDefault().ApplicationStatus.Equals("PR", StringComparison.OrdinalIgnoreCase))
            {
                // Guid found, but for a "PR" application (for a program of interest).
                // Start message that is concatenated with GUID.
                var message = "No admissions application was found for ";
                logger.Error(message);
                throw new KeyNotFoundException(message);
            }           

            List<Dtos.DtoProperties.AdmissionApplicationsStatus> statusesList = new List<Dtos.DtoProperties.AdmissionApplicationsStatus>();

            foreach (var admissionApplicationStatus in admissionApplicationStatuses.Where(i => !i.ApplicationStatus.Equals("PR", StringComparison.OrdinalIgnoreCase)))
            {
                Dtos.DtoProperties.AdmissionApplicationsStatus status = new Dtos.DtoProperties.AdmissionApplicationsStatus() 
                {
                    AdmissionApplicationsStatusDetail = await ConvertEntityToStatusDetailGuidObjectAsync(admissionApplicationStatus.ApplicationStatus),
                    AdmissionApplicationsStatusType = ConvertEntityToStatusType(admissionApplicationStatus.ApplicationStatus),
                    AdmissionApplicationsStatusStartOn = ConvertEntityDateTimeSpanToDate(admissionApplicationStatus.ApplicationStatusDate, admissionApplicationStatus.ApplicationStatusTime)
                };
                statusesList.Add(status);
            }
            return statusesList.Any() ? statusesList : null;
        }

        /// <summary>
        /// Gets date with time
        /// </summary>
        /// <param name="statusDate"></param>
        /// <param name="statusTime"></param>
        /// <returns></returns>
        private DateTime ConvertEntityDateTimeSpanToDate(DateTime? statusDate, DateTime? statusTime)
        {
            DateTime dateOnly;
            DateTime timeOnly;
            DateTime combined;
            
            if (!statusDate.HasValue)
            {
                throw new ArgumentNullException("Application status date cannot be null. ");
            }

            if(DateTime.TryParse(statusDate.Value.ToShortDateString(), out dateOnly))
            {
                if (statusTime.HasValue)
                { 
                    if(DateTime.TryParse(statusTime.Value.ToLongTimeString(), out timeOnly))
                    {
                        return combined = dateOnly.Date.Add(timeOnly.TimeOfDay);
                    }
                    else
                    {
                        return combined = dateOnly;
                    }
                }
                else
                {
                    return combined = dateOnly;
                }
            }
            return statusDate.Value;
        }

        /// <summary>
        /// Gets admission applications status type
        /// </summary>
        /// <param name="sourceCode"></param>
        /// <returns></returns>
        private AdmissionApplicationsStatusType ConvertEntityToStatusType(string sourceCode)
        {
            switch (sourceCode)
            {
                case "AP":
                    return  AdmissionApplicationsStatusType.Submitted;
                case "CO":
                    return AdmissionApplicationsStatusType.ReadyForReview;
                case "AC":
                case "WL":
                case "RE":
                case "WI":
                    return AdmissionApplicationsStatusType.DecisionMade;
                case "MS":
                    return AdmissionApplicationsStatusType.EnrollmentCompleted;
                default:
                    return Dtos.EnumProperties.AdmissionApplicationsStatusType.Started;
            }
        }

        /// <summary>
        /// Gets status guid object
        /// </summary>
        /// <param name="sourceCode"></param>
        /// <returns></returns>
        private async Task<GuidObject2> ConvertEntityToStatusDetailGuidObjectAsync(string sourceCode)
        {
            if (string.IsNullOrEmpty(sourceCode))
            {
                return null;
            }
            var source = (await AdmissionApplicationStatusTypesAsync()).FirstOrDefault(i => i.Code.Equals(sourceCode, StringComparison.OrdinalIgnoreCase));
            if (source == null)
            {
                return null;
            }
            return new GuidObject2(source.Guid);
        }

        /// <summary>
        /// Convert code to guid.
        /// </summary>
        /// <param name="sourceCode"></param>
        /// <returns></returns>
        private async Task<GuidObject2> ConvertEntityToAcademicPeriodGuidObjectAsync(string sourceCode)
        {
            if (string.IsNullOrEmpty(sourceCode))
            {
                return null;
            }
            var source = (await Terms()).FirstOrDefault(i => i.Code.Equals(sourceCode, StringComparison.OrdinalIgnoreCase));
            if (source == null)
            {
                var error = string.Format("Term not found for code {0}. ", sourceCode);
                throw new KeyNotFoundException(error);
            }
            return string.IsNullOrEmpty(source.RecordGuid) ? null : new GuidObject2(source.RecordGuid);
        }

        /// <summary>
        /// Convert code to guid.
        /// </summary>
        /// <param name="sourceCode"></param>
        /// <returns></returns>
        private async Task<GuidObject2> ConvertEntityToApplicationSourceGuidObjectDtoAsync(string sourceCode)
        {
            if (string.IsNullOrEmpty(sourceCode))
            {
                return null;
            }
            var source = (await ApplicationSourcesAsync()).FirstOrDefault(i => i.Code.Equals(sourceCode, StringComparison.OrdinalIgnoreCase));
            if (source == null)
            {
                var error = string.Format("Application sources not found for code {0}. ", sourceCode);
                throw new KeyNotFoundException(error);
            }
            return string.IsNullOrEmpty(source.Guid) ? null : new GuidObject2(source.Guid);
        }

        /// <summary>
        /// Convert code to guid.
        /// </summary>
        /// <param name="sourceCode"></param>
        /// <returns></returns>
        private async Task<GuidObject2> ConvertEntityToAdmitStatusGuidObjectDtoAsync(string sourceCode)
        {
            if (string.IsNullOrEmpty(sourceCode))
            {
                return null;
            }

            var source = (await AdmissionPopulationsAsync()).FirstOrDefault(i => i.Code.Equals(sourceCode, StringComparison.OrdinalIgnoreCase));
            if (source == null)
            {
                var error = string.Format("Application admit status not found for code {0}. ", sourceCode);
                throw new KeyNotFoundException(error);
            }
            return string.IsNullOrEmpty(source.Guid) ? null : new GuidObject2(source.Guid);
        }

        /// <summary>
        /// Convert code to guid.
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private async Task<GuidObject2> ConvertEntityToApplLocationGuidObjectDtoAsync(List<string> list)
        {
            if (list == null || (list != null && !list.Any()))
            {
                return null;
            }
            var guid = string.Empty;
            if (list != null && list.Any())
            {
                var siteList = list.Distinct();
                var source = (await SitesAsync()).FirstOrDefault(i => siteList.Contains(i.Code));
                if (source == null)
                {
                    throw new KeyNotFoundException("Site not found for code. ");
                }
                guid = source.Guid;
            }
            return string.IsNullOrEmpty(guid) ? null : new GuidObject2(guid);
        }

        /// <summary>
        /// Convert code to guid.
        /// </summary>
        /// <param name="sourceCode"></param>
        /// <returns></returns>
        private async Task<GuidObject2> ConvertEntityToApplResidencyStatusGuidObjectDtoAsync(string sourceCode)
        {
            if (string.IsNullOrEmpty(sourceCode))
            {
                return null;
            }

            var source = (await AdmissionResidencyTypesAsync()).FirstOrDefault(i => i.Code.Equals(sourceCode, StringComparison.OrdinalIgnoreCase));
            if (source == null)
            {
                var error = string.Format("Residency type not found for code {0}. ", sourceCode);
                throw new KeyNotFoundException(error);
            }
            return string.IsNullOrEmpty(source.Guid) ? null : new GuidObject2(source.Guid);
        }

        /// <summary>
        /// Convert code to guid.
        /// </summary>
        /// <param name="sourceCode"></param>
        /// <returns></returns>
        private async Task<GuidObject2> ConvertEntityToAcadProgGuidObjectDtoAsync(string sourceCode)
        {
            if (string.IsNullOrEmpty(sourceCode))
            {
                return null;
            }

            var source = (await AcademicProgramsAsync()).FirstOrDefault(i => i.Code.Equals(sourceCode, StringComparison.OrdinalIgnoreCase));
            if (source == null)
            {
                var error = string.Format("Academic program not found for code {0}. ", sourceCode);
                throw new KeyNotFoundException(error);
            }
            return string.IsNullOrEmpty(source.Guid) ? null : new GuidObject2(source.Guid);
        }

        /// <summary>
        /// Convert code to guid.
        /// </summary>
        /// <param name="sourceCode"></param>
        /// <returns></returns>
        private async Task<GuidObject2> ConvertEntityToAcadProgLevelGuidObjectDtoAsync(string sourceCode)
        {
            if (string.IsNullOrEmpty(sourceCode))
            {
                return null;
            }

            var source = (await AcademicProgramsAsync()).FirstOrDefault(i => i.Code.Equals(sourceCode, StringComparison.OrdinalIgnoreCase));
            if (source == null)
            {
                var error = string.Format("Academic program not found for code {0}. ", sourceCode);
                throw new KeyNotFoundException(error);
            }

            var level = source.AcadLevelCode;
            var levelSourceGuid = string.Empty;
            if (!string.IsNullOrEmpty(level))
            {
                var acadLevel = (await AcademicLevelsAsync()).FirstOrDefault(i => i.Code.Equals(level, StringComparison.OrdinalIgnoreCase));
                if (acadLevel == null)
                {
                    var error = string.Format("Academic level not found for code {0}. ", level);
                    throw new KeyNotFoundException(error);
                }
                levelSourceGuid = acadLevel.Guid;
            }
            return string.IsNullOrEmpty(levelSourceGuid) ? null : new GuidObject2(levelSourceGuid);
        }

        /// <summary>
        /// Gets discipline guid object
        /// </summary>
        /// <param name="sourceCodes"></param>
        /// <returns></returns>
        private async Task<IEnumerable<AdmissionApplicationDiscipline>> ConvertEntityToDisciplinesDtoAsync(string program, List<string> sourceCodes)
        {
            if (sourceCodes == null || (sourceCodes != null && !sourceCodes.Any()))
            {
                return null;
            }

            List<AdmissionApplicationDiscipline> academicDisciplineList = new List<AdmissionApplicationDiscipline>();
            List<string> disciplineCodeList = new List<string>();

            disciplineCodeList.AddRange(sourceCodes);

            var source = (await AcademicProgramsAsync()).FirstOrDefault(i => i.Code.Equals(program, StringComparison.OrdinalIgnoreCase));
            if (source == null)
            {
                var error = string.Format("Academic program not found for code {0}. ", sourceCodes);
                throw new KeyNotFoundException(error);
            }

            //Major
            source.MajorCodes.ForEach(i =>
            {
                if (!string.IsNullOrEmpty(i))
                {
                    if (!disciplineCodeList.Contains(i))
                    {
                        disciplineCodeList.Add(i);
                    }
                }
            });

            //Minor
            source.MinorCodes.ForEach(i =>
            {
                if (!string.IsNullOrEmpty(i))
                {
                    if (!disciplineCodeList.Contains(i))
                    {
                        disciplineCodeList.Add(i);
                    }
                }
            });

            //Specializations
            source.SpecializationCodes.ForEach(i =>
            {
                if (!string.IsNullOrEmpty(i))
                {
                    if (!disciplineCodeList.Contains(i))
                    {
                        disciplineCodeList.Add(i);
                    }
                }
            });


            //Now create guid objects
            if (disciplineCodeList.Any())
            {
                foreach (var item in disciplineCodeList)
                {
                    if (!string.IsNullOrEmpty(item))
                    {
                        var acadDiscSource = (await AcademicDisciplinesAsync()).FirstOrDefault(disc => disc.Code.Equals(item, StringComparison.OrdinalIgnoreCase));
                        if (acadDiscSource == null)
                        {
                            var error = string.Format("Academic disciplines not found for code {0}. ", item);
                            throw new KeyNotFoundException(error);
                        }
                        academicDisciplineList.Add(new AdmissionApplicationDiscipline() { Discipline = new GuidObject2(acadDiscSource.Guid) });
                    }
                }
            }

            return academicDisciplineList.Any() ? academicDisciplineList : null;
        }

        /// <summary>
        /// Gets admission applicaion withdrawl
        /// </summary>
        /// <param name="applicationAttendedInstead"></param>
        /// <param name="applicationWithdrawReason"></param>
        /// <returns></returns>
        private async Task<Dtos.DtoProperties.AdmissionApplicationsWithdrawal> ConvertEntityToWithdrawlDtoAsync(string applicationAttendedInstead, string applicationWithdrawReason)
        {
            if (string.IsNullOrEmpty(applicationWithdrawReason))
            {
                return null;
            }

            Dtos.DtoProperties.AdmissionApplicationsWithdrawal admApplWithdrawReason = new Dtos.DtoProperties.AdmissionApplicationsWithdrawal();

            var source = (await WithdrawReasonsAsync()).FirstOrDefault(i => i.Code.Equals(applicationWithdrawReason, StringComparison.OrdinalIgnoreCase));
            if (source == null)
            {
                var error = string.Format("Withdraw reason not found for reason {0}. ", applicationWithdrawReason);
                throw new KeyNotFoundException(error);
            }
            admApplWithdrawReason.WithdrawalReason = new GuidObject2(source.Guid);
            admApplWithdrawReason.InstitutionAttended = applicationAttendedInstead;
            return admApplWithdrawReason;
        }

        /// <summary>
        /// Gets Contract Type
        /// </summary>
        /// <param name="applicationStudentLoadIntent"></param>
        /// <returns></returns>
        private ContractType ConvertEntityToAcaDemicLoadTypeDto(string applicationStudentLoadIntent)
        {
            if (!string.IsNullOrEmpty(applicationStudentLoadIntent))
            {
                if (applicationStudentLoadIntent.ToUpperInvariant().Equals("F", StringComparison.OrdinalIgnoreCase) ||
                    applicationStudentLoadIntent.ToUpperInvariant().Equals("O", StringComparison.OrdinalIgnoreCase))
                {
                    return ContractType.FullTime;
                }
                else if (applicationStudentLoadIntent.ToUpperInvariant().Equals("P", StringComparison.OrdinalIgnoreCase))
                {
                    return ContractType.PartTime;
                }
                else
                {
                    return ContractType.NotSet;
                }
            }
            else
            {
                return ContractType.NotSet;
            }
        }

        /// <summary>
        /// Gets guid objects
        /// </summary>
        /// <returns></returns>
        private async Task<GuidObject2> ConvertEntityToTypeGuidObjectDtoAsync()
        {
            var source = (await AdmissionApplicationTypesAsync()).FirstOrDefault();
            return string.IsNullOrEmpty(source.Guid) ? null : new GuidObject2(source.Guid);
        }

        /// <summary>
        /// Gets guid objects
        /// </summary>
        /// <param name="sourceCode"></param>
        /// <returns></returns>
        private async Task<GuidObject2> ConvertEntityToSchoolGuidObjectAsync(string sourceCode)
        {
            if (string.IsNullOrEmpty(sourceCode))
            {
                return null;
            }

            var source = (await SchoolsAsync()).FirstOrDefault(i => i.Code.Equals(sourceCode, StringComparison.OrdinalIgnoreCase));
            if (source == null)
            {
                var error = string.Format("School not found for code {0}. ", sourceCode);
                throw new KeyNotFoundException(error);
            }
            return string.IsNullOrEmpty(source.Guid) ? null : new GuidObject2(source.Guid);
        }
        #endregion

        #region Reference Methods

        /// <summary>
        /// Builds person record keys local cache
        /// </summary>
        private IEnumerable<string> _personIds;
        private IDictionary<string, string> _staffOperIdsDict;
        private async Task<IEnumerable<string>> BuildLocalPersonGuids(IEnumerable<Ellucian.Colleague.Domain.Student.Entities.AdmissionApplication> admissionApplicationsEntities)
        {
            var personIds = new List<string>();
            var ownerIds = new List<string>();

            personIds.AddRange(admissionApplicationsEntities
                     .Where(i => !string.IsNullOrEmpty(i.ApplicantPersonId) && !personIds.Contains(i.ApplicantPersonId))
                     .Select(i => i.ApplicantPersonId));
            personIds.AddRange(admissionApplicationsEntities
                     .Where(i => !string.IsNullOrEmpty(i.ApplicationAdmissionsRep) && !personIds.Contains(i.ApplicationAdmissionsRep))
                     .Select(i => i.ApplicationAdmissionsRep));

            admissionApplicationsEntities.ToList().ForEach(admissionApplicationsEntity =>
            {
                if (!string.IsNullOrEmpty(admissionApplicationsEntity.ApplicationAdmissionsRep)) 
                {
                    if (!ownerIds.Contains(admissionApplicationsEntity.ApplicationAdmissionsRep))
                    {
                        ownerIds.Add(admissionApplicationsEntity.ApplicationAdmissionsRep);
                    }
                }
                
                if (admissionApplicationsEntity.AdmissionApplicationStatuses != null && admissionApplicationsEntity.AdmissionApplicationStatuses.Any())
                {
                    var admissionApplicationStatus = admissionApplicationsEntity.AdmissionApplicationStatuses
                        .Where(i => i.ApplicationStatus != null)
                        .OrderByDescending(dt => dt.ApplicationStatusDate)
                        .FirstOrDefault();
                    if (admissionApplicationStatus != null && !ownerIds.Contains(admissionApplicationStatus.ApplicationDecisionBy))
                    {
                        ownerIds.Add(admissionApplicationStatus.ApplicationDecisionBy);
                    }
                }                
            });

            var staffOperIds = ownerIds.Any()? await _admissionApplicationsRepository.GetStaffOperIdsAsync(ownerIds) : null;
            if (staffOperIds != null && staffOperIds.Any())
            {
                _staffOperIdsDict = staffOperIds as Dictionary<string, string>;
                foreach (var item in _staffOperIdsDict)
                {
                    if (!personIds.Contains(item.Value))
                    {
                        personIds.Add(item.Value);
                    }
                }
            }

            return personIds.Distinct().ToList();
        }

        /// <summary>
        /// Person ids, guid key value pairs
        /// </summary>
        private IDictionary<string, string> _personGuidsDict;
        private async Task<IDictionary<string, string>> PersonGuidsAsync()
        {
            if (_personIds != null && _personIds.Any())
            {
                if (_personGuidsDict == null)
                {
                    IDictionary<string, string> dict = await _admissionApplicationsRepository.GetPersonGuidsAsync(_personIds);
                    if (dict != null && dict.Any())
                    {
                        _personGuidsDict = new Dictionary<string, string>();
                        dict.ToList().ForEach(i =>
                        {
                            if (!_personGuidsDict.ContainsKey(i.Key))
                            {
                                _personGuidsDict.Add(i.Key, i.Value);
                            }
                        });
                    }
                }
            }
            return _personGuidsDict;
        }

        /// <summary>
        /// AdmissionApplicationType
        /// </summary>
        private IEnumerable<AdmissionApplicationType> _admissionApplicationTypes;
        private async Task<IEnumerable<AdmissionApplicationType>> AdmissionApplicationTypesAsync()
        {
            if (_admissionApplicationTypes == null)
            {
                _admissionApplicationTypes = await _studentReferenceDataRepository.GetAdmissionApplicationTypesAsync(false);
            }
            return _admissionApplicationTypes;
        }

        /// <summary>
        /// Terms
        /// </summary>
        private IEnumerable<Term> _terms;
        private async Task<IEnumerable<Term>> Terms()
        {
            if (_terms == null)
            {
                _terms = await _termRepository.GetAsync(false);
            }
            return _terms;
        }

        /// <summary>
        /// AdmissionApplicationStatusType
        /// </summary>
        private IEnumerable<Domain.Student.Entities.AdmissionApplicationStatusType> _admissionApplicationStatusTypes;
        private async Task<IEnumerable<Domain.Student.Entities.AdmissionApplicationStatusType>> AdmissionApplicationStatusTypesAsync()
        {
            if (_admissionApplicationStatusTypes == null)
            {
                _admissionApplicationStatusTypes = await _studentReferenceDataRepository.GetAdmissionApplicationStatusTypesAsync(false);
            }
            return _admissionApplicationStatusTypes;
        }

        /// <summary>
        /// 
        /// </summary>
        private IEnumerable<Domain.Student.Entities.ApplicationSource> _sources;
        private async Task<IEnumerable<Domain.Student.Entities.ApplicationSource>> ApplicationSourcesAsync()
        {
            if (_sources == null)
            {
                _sources = await _studentReferenceDataRepository.GetApplicationSourcesAsync(false);
            }
            return _sources;
        }

        /// <summary>
        /// AdmissionPopulation
        /// </summary>
        private IEnumerable<Domain.Student.Entities.AdmissionPopulation> _admissionPopulations;
        private async Task<IEnumerable<Domain.Student.Entities.AdmissionPopulation>> AdmissionPopulationsAsync()
        {
            if (_admissionPopulations == null)
            {
                _admissionPopulations = await _studentReferenceDataRepository.GetAdmissionPopulationsAsync(false);
            }
            return _admissionPopulations;
        }

        /// <summary>
        /// Sites
        /// </summary>
        private IEnumerable<Domain.Base.Entities.Location> _locations;
        private async Task<IEnumerable<Domain.Base.Entities.Location>> SitesAsync()
        {
            if (_locations == null)
            {
                _locations = await _referenceDataRepository.GetLocationsAsync(false);
            }
            return _locations;
        }

        /// <summary>
        /// AdmissionResidencyType
        /// </summary>
        private IEnumerable<Domain.Student.Entities.AdmissionResidencyType> _admissionResidencyTypes;
        private async Task<IEnumerable<Domain.Student.Entities.AdmissionResidencyType>> AdmissionResidencyTypesAsync()
        {
            if (_admissionResidencyTypes == null)
            {
                _admissionResidencyTypes = await _studentReferenceDataRepository.GetAdmissionResidencyTypesAsync(false);
            }
            return _admissionResidencyTypes;
        }

        /// <summary>
        /// AcademicLevels
        /// </summary>
        private IEnumerable<Domain.Student.Entities.AcademicLevel> _academicLevels;
        private async Task<IEnumerable<Domain.Student.Entities.AcademicLevel>> AcademicLevelsAsync()
        {
            if (_academicLevels == null)
            {
                _academicLevels = await _studentReferenceDataRepository.GetAcademicLevelsAsync(false);
            }
            return _academicLevels;
        }

        /// <summary>
        /// AcademicDisciplines
        /// </summary>
        private IEnumerable<Domain.Base.Entities.AcademicDiscipline> _academicDisciplines;
        private async Task<IEnumerable<Domain.Base.Entities.AcademicDiscipline>> AcademicDisciplinesAsync()
        {
            if (_academicDisciplines == null)
            {
                _academicDisciplines = await _referenceDataRepository.GetAcademicDisciplinesAsync(false);
            }
            return _academicDisciplines;
        }

        /// <summary>
        /// AcademicProgram
        /// </summary>
        private IEnumerable<Domain.Student.Entities.AcademicProgram> _academicPrograms;
        private async Task<IEnumerable<Domain.Student.Entities.AcademicProgram>> AcademicProgramsAsync()
        {
            if (_academicPrograms == null)
            {
                _academicPrograms = await _studentReferenceDataRepository.GetAcademicProgramsAsync(false);
            }
            return _academicPrograms;
        }

        /// <summary>
        /// WithdrawReason
        /// </summary>
        private IEnumerable<Domain.Student.Entities.WithdrawReason> _withdrawReasons;
        private async Task<IEnumerable<Domain.Student.Entities.WithdrawReason>> WithdrawReasonsAsync()
        {
            if (_withdrawReasons == null)
            {
                _withdrawReasons = await _studentReferenceDataRepository.GetWithdrawReasonsAsync(false);
            }
            return _withdrawReasons;
        }

        /// <summary>
        /// Schools
        /// </summary>
        private IEnumerable<Domain.Base.Entities.School> _schools;
        private async Task<IEnumerable<Domain.Base.Entities.School>> SchoolsAsync()
        {
            if (_schools == null)
            {
                _schools = await _referenceDataRepository.GetSchoolsAsync(false);
            }
            return _schools;
        }
        #endregion
    }
}