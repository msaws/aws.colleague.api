﻿// Copyright 2015-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    public interface IAcademicCatalogService
    {
        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Gets all academic catalogs
        /// </summary>
        /// <returns>Collection of Academic Catalog DTO objects</returns>
        Task<IEnumerable<Ellucian.Colleague.Dtos.AcademicCatalog>> GetAcademicCatalogsAsync(bool bypassCache = false);

        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Get an academic catalog from its GUID
        /// </summary>
        /// <returns>Academic Catalog DTO object</returns>
        Task<Ellucian.Colleague.Dtos.AcademicCatalog> GetAcademicCatalogByGuidAsync(string guid);

        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Gets all academic catalogs
        /// </summary>
        /// <returns>Collection of AcademicCatalog2 DTO objects</returns>
        Task<IEnumerable<Ellucian.Colleague.Dtos.AcademicCatalog2>> GetAcademicCatalogs2Async(bool bypassCache = false);

        /// <remarks>FOR USE WITH ELLUCIAN HeDM</remarks>
        /// <summary>
        /// Get an academic catalog from its GUID
        /// </summary>
        /// <returns>AcademicCatalog2 DTO object</returns>
        Task<Ellucian.Colleague.Dtos.AcademicCatalog2> GetAcademicCatalogByGuid2Async(string guid);
    }
}
