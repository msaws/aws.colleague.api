﻿// Copyright 2016-2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using Microsoft.Reporting.WebForms;
using Ellucian.Colleague.Coordination.Base.Utility;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    /// <summary>
    /// Service for tax form pdfs.
    /// </summary>
    [RegisterType]
    public class StudentTaxFormPdfService: BaseCoordinationService, IStudentTaxFormPdfService
    {
        public const string ReportType = "PDF";
        public const string DeviceInfo = "<DeviceInfo>" +
                                         " <OutputFormat>PDF</OutputFormat>" +
                                         "</DeviceInfo>";

        IStudentTaxFormPdfDataRepository taxFormPdfDataRepository;
        IPdfSharpRepository pdfSharpRepository;
        IPersonRepository personRepository;

        /// <summary>
        /// Constructor TaxFormPdfService
        /// </summary>
        /// <param name="adapterRegistry">AdapterRegistry</param>
        /// <param name="currentUserFactory">CurrentUserFactory</param>
        /// <param name="roleRepository">RoleRepository</param>
        /// <param name="logger">Logger</param>
        public StudentTaxFormPdfService(IStudentTaxFormPdfDataRepository taxFormPdfDataRepository,
            IPdfSharpRepository pdfSharpRepository, IPersonRepository personRepository, IAdapterRegistry adapterRegistry,
            ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.taxFormPdfDataRepository = taxFormPdfDataRepository;
            this.pdfSharpRepository = pdfSharpRepository;
            this.personRepository = personRepository;
        }

        /// <summary>
        /// Returns the pdf data to print a 1098 tax form.
        /// </summary>
        /// <param name="personId">ID of the person assigned to and requesting the 1098.</param>
        /// <param name="recordId">The record ID where the 1098 pdf data is stored</param>
        /// <returns>TaxForm1098PdfData domain entity</returns>
        public async Task<Form1098PdfData> Get1098TaxFormData(string personId, string recordId)
        {
            if (string.IsNullOrEmpty(personId))
                throw new ArgumentNullException("personId", "Person ID must be specified.");

            if (string.IsNullOrEmpty(recordId))
                throw new ArgumentNullException("recordId", "Record ID must be specified.");

            if (personId != CurrentUser.PersonId)
                throw new ApplicationException("Person ID from request is not the same as the Person ID of the current user.");

            try
            {
                // Call the repository to get all the data to print in the pdf.
                var taxFormPdfData = await this.taxFormPdfDataRepository.Get1098TPdfAsync(personId, recordId);

                var institutionAddressLines = await personRepository.Get1098HierarchyAddressAsync(taxFormPdfData.InstitutionId);

                // Assign the institution address lines.
                if (institutionAddressLines != null)
                {
                    // Combine address lines 1 and 2
                    taxFormPdfData.InstitutionAddressLine1 = institutionAddressLines.ElementAtOrDefault(0) ?? "";
                    if (!string.IsNullOrEmpty(institutionAddressLines.ElementAtOrDefault(1)))
                    {
                        taxFormPdfData.InstitutionAddressLine1 += " " + institutionAddressLines.ElementAtOrDefault(1);
                    }

                    // Combine address lines 3 and 4
                    taxFormPdfData.InstitutionAddressLine2 = institutionAddressLines.ElementAtOrDefault(2) ?? "";
                    if (!string.IsNullOrEmpty(institutionAddressLines.ElementAtOrDefault(3)))
                    {
                        taxFormPdfData.InstitutionAddressLine2 += " " + institutionAddressLines.ElementAtOrDefault(3);
                    }
                }

                // Put the phone number in address line 4 if it's empty.
                if (string.IsNullOrEmpty(taxFormPdfData.InstitutionAddressLine4))
                {
                    taxFormPdfData.InstitutionAddressLine4 = taxFormPdfData.InstitutionPhoneNumber;
                }

                return taxFormPdfData;
            }
            catch (Exception e)
            {
                // Log the error and throw the exception that was given
                logger.Error(e.Message);
                throw;
            }
        }

        /// <summary>
        /// Populates the 1098 PDF with the supplied data.
        /// </summary>
        /// <param name="pdfData">1098 PDF data</param>
        /// <param name="documentPath">Path to the PDF template</param>
        /// <returns>Byte array containing PDF data for the 1098 tax form</returns>
        public byte[] Populate1098Pdf(Form1098PdfData pdfData, string documentPath)
        {
            byte[] reportBytes;
            var memoryStream = new MemoryStream();
            try
            {
                if (pdfData != null && !string.IsNullOrEmpty(documentPath))
                {
                    var document = pdfSharpRepository.OpenDocument(documentPath);

                    var taxFormData = new Dictionary<string, string>()
                        {
                            { "Year2", pdfData.TaxYear.Substring(2) },
                            { "CorrectedForm", pdfData.Correction ? "X" : "" },
                            { "InstNameAddr1", pdfData.InstitutionName },
                            { "InstNameAddr2", pdfData.InstitutionAddressLine1 },
                            { "InstNameAddr3", pdfData.InstitutionAddressLine2 },
                            { "InstNameAddr4", pdfData.InstitutionAddressLine3 },
                            { "InstNameAddr5", pdfData.InstitutionAddressLine4 },
                            { "InstEIN", pdfData.InstitutionEin },
                            { "StudentSSN", pdfData.SSN },
                            { "StudentName1", pdfData.StudentName },
                            { "StudentName2", pdfData.StudentName2 },
                            { "StudentAddress", pdfData.StudentAddressLine1 },
                            { "StudentCSZ", pdfData.StudentAddressLine2 },
                            { "StudentID", pdfData.StudentId },
                            { "AtLeastHalfTime", pdfData.AtLeastHalfTime ? "X" : "" },
                            { "Box2Amt", pdfData.AmountsBilledForTuitionAndExpenses ?? "" },
                            { "Box4Amt", pdfData.AdjustmentsForPriorYear ?? "" },
                            { "Box5Amt", pdfData.ScholarshipsOrGrants ?? "" },
                            { "Box6Amt", pdfData.AdjustmentsToScholarshipsOrGrantsForPriorYear ?? "" },
                            { "ChgRptgMethod", pdfData.ReportingMethodHasBeenChanged ? "X" : "" },
                            { "GraduateStudent", pdfData.IsGradStudent ? "X" : "" },
                            { "First3Months", pdfData.AmountsBilledAndReceivedForQ1Period ? "X" : "" },        // Box 7
                        };

                    pdfSharpRepository.PopulatePdfDocument(ref document, taxFormData);
                    memoryStream = pdfSharpRepository.FinalizePdfDocument(document);
                }

                reportBytes = memoryStream.ToArray();
                return reportBytes;
            }
            catch (Exception e)
            {
                // Log the error and throw the exception that was given
                logger.Error(e.Message);
                throw;
            }

        }

        /// <summary>
        /// Populates the 1098 PDF with the supplied data using an RDLC report.
        /// </summary>
        /// <param name="pdfData">1098 PDF data</param>
        /// <param name="documentPath">Path to the RDLC template</param>
        /// <returns>Byte array containing PDF data for the 1098 tax form</returns>
        public byte[] Populate1098tReport(Form1098PdfData pdfData, string pathToReport)
        {
            if (pdfData == null)
            {
                throw new ArgumentNullException("pdfData");
            }
            if (string.IsNullOrEmpty(pathToReport))
            {
                throw new ArgumentNullException("pathToReport");
            }

            var report = new LocalReport();
            report.ReportPath = pathToReport;
            report.SetBasePermissionsForSandboxAppDomain(new System.Security.PermissionSet(System.Security.Permissions.PermissionState.Unrestricted));
            report.EnableExternalImages = true;

            // Specify the report parameters
            var utility = new ReportUtility();
            var parameters = new List<ReportParameter>();
            parameters.Add(utility.BuildReportParameter("Year2", pdfData.TaxYear.Substring(2)));
            parameters.Add(utility.BuildReportParameter("CorrectedForm", pdfData.Correction ? "X" : ""));
            parameters.Add(utility.BuildReportParameter("InstNameAddr1", pdfData.InstitutionName));
            parameters.Add(utility.BuildReportParameter("InstNameAddr2", pdfData.InstitutionAddressLine1));
            parameters.Add(utility.BuildReportParameter("InstNameAddr3", pdfData.InstitutionAddressLine2));
            parameters.Add(utility.BuildReportParameter("InstNameAddr4", pdfData.InstitutionAddressLine3));
            parameters.Add(utility.BuildReportParameter("InstNameAddr5", pdfData.InstitutionAddressLine4));
            parameters.Add(utility.BuildReportParameter("InstEIN", pdfData.InstitutionEin));
            parameters.Add(utility.BuildReportParameter("StudentSSN", pdfData.SSN));
            parameters.Add(utility.BuildReportParameter("StudentName1", pdfData.StudentName));
            parameters.Add(utility.BuildReportParameter("StudentName2", pdfData.StudentName2));
            parameters.Add(utility.BuildReportParameter("StudentAddress", pdfData.StudentAddressLine1));
            parameters.Add(utility.BuildReportParameter("StudentCSZ", pdfData.StudentAddressLine2));
            parameters.Add(utility.BuildReportParameter("StudentID", pdfData.StudentId));
            parameters.Add(utility.BuildReportParameter("AtLeastHalfTime", pdfData.AtLeastHalfTime ? "X" : ""));                            // Box 8
            parameters.Add(utility.BuildReportParameter("Box2Amt", pdfData.AmountsBilledForTuitionAndExpenses ?? ""));
            parameters.Add(utility.BuildReportParameter("Box4Amt", pdfData.AdjustmentsForPriorYear ?? ""));
            parameters.Add(utility.BuildReportParameter("Box5Amt", pdfData.ScholarshipsOrGrants ?? ""));
            parameters.Add(utility.BuildReportParameter("Box6Amt", pdfData.AdjustmentsToScholarshipsOrGrantsForPriorYear ?? ""));
            parameters.Add(utility.BuildReportParameter("ChgRptgMethod", pdfData.ReportingMethodHasBeenChanged ? "X" : ""));                // Box 3
            parameters.Add(utility.BuildReportParameter("GraduateStudent", pdfData.IsGradStudent ? "X" : ""));                              // Box 9
            parameters.Add(utility.BuildReportParameter("First3Months", pdfData.AmountsBilledAndReceivedForQ1Period ? "X" : ""));           // Box 7

            // Set the report parameters
            report.SetParameters(parameters);

            // Set up some options for the report
            string mimeType = string.Empty;
            string encoding;
            string fileNameExtension;
            Warning[] warnings;
            string[] streams;

            // Render the report as a byte array
            var renderedBytes = report.Render(
                ReportType,
                DeviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            return renderedBytes;
        }
    }
}
