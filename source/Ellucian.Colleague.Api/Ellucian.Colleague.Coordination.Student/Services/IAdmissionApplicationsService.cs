﻿//Copyright 2017 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    /// <summary>
    /// Interface for AdmissionApplications services
    /// </summary>
    public interface IAdmissionApplicationsService : IBaseService
    {
        Task<Tuple<IEnumerable<Ellucian.Colleague.Dtos.AdmissionApplication>, int>> GetAdmissionApplicationsAsync(int offset, int limit, bool bypassCache = false);
        Task<Ellucian.Colleague.Dtos.AdmissionApplication> GetAdmissionApplicationsByGuidAsync(string id);
    }
}