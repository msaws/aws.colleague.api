﻿// Copyright 2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Web.Dependency;
using slf4net;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    [RegisterType]
    public class CourseStatusService : ICourseStatusService
    {
        private readonly IStudentReferenceDataRepository _studentReferenceDataRepository;
        private readonly ILogger logger;

        public CourseStatusService(IStudentReferenceDataRepository studentReferenceDataRepository, ILogger logger)
        {
            _studentReferenceDataRepository = studentReferenceDataRepository;
            if (logger == null)
            {
                throw new ArgumentNullException("logger");
            }
            this.logger = logger;
        }

        /// <remarks>FOR USE WITH ELLUCIAN LDM</remarks>
        /// <summary>
        /// Gets all course statuses
        /// </summary>
        /// <returns>Collection of CourseStatus DTO objects</returns>
        public IEnumerable<Ellucian.Colleague.Dtos.CourseStatus> GetCourseStatuses()
        {
            var courseStatusCollection = new List<Ellucian.Colleague.Dtos.CourseStatus>();

            var courseStatusEntities = _studentReferenceDataRepository.CourseStatuses;
            if (courseStatusEntities != null && courseStatusEntities.Count() > 0)
            {
                foreach (var courseStatus in courseStatusEntities)
                {
                    courseStatusCollection.Add(ConvertCourseStatusEntityToDto(courseStatus));
                }
            }
            return courseStatusCollection;
        }

        /// <remarks>FOR USE WITH ELLUCIAN LDM</remarks>
        /// <summary>
        /// Converts a CourseStatusItem domain entity to its corresponding CourseStatus DTO
        /// </summary>
        /// <param name="source">CourseStatusItem domain entity</param>
        /// <returns>CourseStatus DTO</returns>
        private Ellucian.Colleague.Dtos.CourseStatus ConvertCourseStatusEntityToDto(Ellucian.Colleague.Domain.Student.Entities.CourseStatusItem source)
        {
            var courseStatus = new Ellucian.Colleague.Dtos.CourseStatus();

            courseStatus.Guid = source.Guid;
            courseStatus.Abbreviation = source.Code;
            courseStatus.Title = source.Code;
            courseStatus.Description = source.Description;
            courseStatus.StatusType = ConvertCourseStatusEnumEntityToCourseStatusTypeEnumDto(source.Status);

            return courseStatus;
        }

        /// <remarks>FOR USE WITH ELLUCIAN LDM</remarks>
        /// <summary>
        /// Converts a CourseStatus enum domain entity to its corresponding CourseStatusType enum DTO
        /// </summary>
        /// <param name="source">CourseStatus enum domain entity</param>
        /// <returns>CourseStatusType enum DTO</returns>
        private Ellucian.Colleague.Dtos.CourseStatusType ConvertCourseStatusEnumEntityToCourseStatusTypeEnumDto(Ellucian.Colleague.Domain.Student.Entities.CourseStatus source)
        {
            switch (source)
            {
                case Domain.Student.Entities.CourseStatus.Active:
                    return Dtos.CourseStatusType.Active;
                case Domain.Student.Entities.CourseStatus.Terminated:
                    return Dtos.CourseStatusType.Inactive;
                default:
                    return Dtos.CourseStatusType.Unknown;
            }
        }
    }
}
