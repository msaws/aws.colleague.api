﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Dtos.Student;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Student.Services 
{
    /// <summary>
    /// Interface for Faculty services
    /// </summary>
    public interface IFacultyService 
    {
        Task<Faculty> GetAsync(string facultyId);
        [Obsolete("Obsolete as of API 1.3. Use latest version of this method.")]
        Task<IEnumerable<Section>> GetFacultySectionsAsync(string facultyId, DateTime? startDate, DateTime? endDate, bool bestFit);
        [Obsolete("Obsolete as of API 1.5. Use latest version of this method.")]
        Task<IEnumerable<Section2>> GetFacultySections2Async(string facultyId, DateTime? startDate, DateTime? endDate, bool bestFit);
        [Obsolete("Obsolete as of API 1.13.1. Use latest version of this method.")]
        Task<IEnumerable<Section3>> GetFacultySections3Async(string facultyId, DateTime? startDate, DateTime? endDate, bool bestFit);
        Task<IEnumerable<Section3>> GetFacultySections4Async(string facultyId, DateTime? startDate, DateTime? endDate, bool bestFit);
        Task<IEnumerable<Faculty>> QueryFacultyAsync(FacultyQueryCriteria criteria);
        Task<IEnumerable<Faculty>> GetFacultyByIdsAsync(IEnumerable<string> facultyIds);
        Task<IEnumerable<string>> SearchFacultyIdsAsync(bool facultyOnlyFlag, bool advisorOnlyFlag);
        Task<IEnumerable<string>> GetFacultyPermissionsAsync();
    }
}
