﻿// Copyright 2012-2014 Ellucian Company L.P. and its affiliates.
using System;
using System.Linq;
using System.Collections.Generic;
using Ellucian.Colleague.Dtos.Student;
using System.Threading.Tasks;
using Ellucian.Colleague.Coordination.Base.Services;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    /// <summary>
    /// Interface for Course services
    /// </summary>
    public interface ICourseService : IBaseService
    {
        Task<IEnumerable<Course2>> GetCourses2Async(CourseQueryCriteria criteria);
        [Obsolete("Obsolete as of HeDM Version 4, use Accept Header Version 4 instead.")]
        Task<IEnumerable<Ellucian.Colleague.Dtos.Course>> GetCoursesAsync();
        Task<IEnumerable<Ellucian.Colleague.Dtos.Course2>> GetCourses2Async(bool bypassCache);

        [Obsolete("Obsolete as of API version 1.3. Use the latest version of this method.")]
        Task<IEnumerable<Section>> GetSectionsAsync(IEnumerable<string> courseIds, bool useCache);
        [Obsolete("Obsolete as of API version 1.5. Use the latest version of this method.")]
        Task<IEnumerable<Section2>> GetSections2Async(IEnumerable<string> courseIds, bool useCache);
        Task<IEnumerable<Section3>> GetSections3Async(IEnumerable<string> courseIds, bool useCache);

        [Obsolete("Obsolete as of API version 1.3. Use the latest version of this method.")]
        Task<Ellucian.Colleague.Dtos.Student.Course> GetCourseByIdAsync(string id);
        Task<Ellucian.Colleague.Dtos.Student.Course2> GetCourseById2Async(string id);
        [Obsolete("Obsolete as of HeDM Version 4, use Accept Header Version 4 instead.")]
        Task<Ellucian.Colleague.Dtos.Course> GetCourseByGuidAsync(string guid);
        Task<Ellucian.Colleague.Dtos.Course2> GetCourseByGuid2Async(string guid);

        [Obsolete("Obsolete as of HeDM Version 4, use Accept Header Version 4 instead.")]
        Task<Ellucian.Colleague.Dtos.Course> CreateCourseAsync(Ellucian.Colleague.Dtos.Course course);
        Task<Ellucian.Colleague.Dtos.Course2> CreateCourse2Async(Ellucian.Colleague.Dtos.Course2 course);

        [Obsolete("Obsolete as of HeDM Version 4, use Accept Header Version 4 instead.")]
        Task<Ellucian.Colleague.Dtos.Course> UpdateCourseAsync(Ellucian.Colleague.Dtos.Course course);
        Task<Ellucian.Colleague.Dtos.Course2> UpdateCourse2Async(Ellucian.Colleague.Dtos.Course2 course);

        [Obsolete("Obsolete as of API version 1.3. Use the latest version of this method.")]
        Task<CoursePage> SearchAsync(CourseSearchCriteria criteria, int pageSize, int pageIndex);
        Task<CoursePage2> Search2Async(CourseSearchCriteria criteria, int pageSize, int pageIndex);
        Task<IEnumerable<Dtos.Student.Course>> GetCoursesByIdAsync(IEnumerable<string> courseIds);

        //V6 Changes
        Task<Dtos.Course3> GetCourseByGuid3Async(string id);
        Task<Dtos.Course3> UpdateCourse3Async(Dtos.Course3 course);
        Task<Dtos.Course3> CreateCourse3Async(Dtos.Course3 course);
        Task<IEnumerable<Dtos.Course3>> GetCourses3Async(bool bypassCache, string subject, string number, string academicLevel, string owningInstitutionUnits, string title, string instructionalMethods, string schedulingStartOn, string schedulingEndOn);
        Task<Tuple<IEnumerable<Dtos.Course3>, int>> GetCourses3Async(int offset, int limit, bool bypassCache, string subject, string number, string academicLevel, string owningInstitutionUnits, string title, string instructionalMethods, string schedulingStartOn, string schedulingEndOn);

        //V8 Changes
        Task<Dtos.Course4> GetCourseByGuid4Async(string id);
        Task<Dtos.Course4> UpdateCourse4Async(Dtos.Course4 course);
        Task<Dtos.Course4> CreateCourse4Async(Dtos.Course4 course);
        Task<IEnumerable<Dtos.Course4>> GetCourses4Async(bool bypassCache, string subject, string number, string academicLevel, string owningInstitutionUnits, string title, string instructionalMethods, string schedulingStartOn, string schedulingEndOn);
        Task<Tuple<IEnumerable<Dtos.Course4>, int>> GetCourses4Async(int offset, int limit, bool bypassCache, string subject, string number, string academicLevel, string owningInstitutionUnits, string title, string instructionalMethods, string schedulingStartOn, string schedulingEndOn);


    }
}
