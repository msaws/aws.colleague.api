﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Coordination.Student.Services
{
    /// <summary>
    /// Interface for AccountingCodes service
    /// </summary>
    public interface IAccountingCodesService
    {
        Task<IEnumerable<Ellucian.Colleague.Dtos.AccountingCode>> GetAccountingCodesAsync(bool bypassCache);
        Task<Ellucian.Colleague.Dtos.AccountingCode> GetAccountingCodeByIdAsync(string id);
    }
}
