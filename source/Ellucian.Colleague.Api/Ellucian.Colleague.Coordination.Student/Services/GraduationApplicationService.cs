﻿// Copyright 2015-2017 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Domain.Student.Exceptions;
using Ellucian.Colleague.Domain.Student.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Ellucian.Colleague.Domain.Base.Repositories;
using Ellucian.Colleague.Domain.Student.Entities;
using Ellucian.Colleague.Domain.Base.Entities;
using System.Linq;



namespace Ellucian.Colleague.Coordination.Student.Services
{
    /// <summary>
    /// Coordinates information to enable retrieval and update of Waivers
    /// </summary>
    [RegisterType]
    public class GraduationApplicationService : BaseCoordinationService, IGraduationApplicationService
    {
        private readonly IGraduationApplicationRepository graduationApplicationRepository;
        private readonly ITermRepository termRepository;
        private readonly IProgramRepository programRepository;
        private readonly IStudentRepository studentRepository;
        private IStudentConfigurationRepository studentConfigurationRepository;
        private  IAddressRepository addressRepository;


        /// <summary>
        /// Initialize the service for accessing graduation application functions
        /// </summary>
        /// <param name="adapterRegistry">Dto adapter registry</param>
        /// <param name="graduationApplicationRepository">Graduation Application repository</param>
        /// <param name="termRepository">Term repository</param>
        /// <param name="currentUserFactory">Current User Factory</param>
        /// <param name="roleRepository">Role Repository</param>
        /// <param name="logger">error logging</param>
        public GraduationApplicationService(IAdapterRegistry adapterRegistry, IGraduationApplicationRepository graduationApplicationRepository, ITermRepository termRepository, IProgramRepository programRepository, IStudentRepository studentRepository, IStudentConfigurationRepository studentConfigurationRepository,
            IAddressRepository addressRepository,  ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            this.graduationApplicationRepository = graduationApplicationRepository;
            this.termRepository = termRepository;
            this.programRepository = programRepository;
            this.studentRepository = studentRepository;
            this.studentConfigurationRepository = studentConfigurationRepository;
            this.addressRepository = addressRepository;
        }

        /// <summary>
        /// Retrieves a graduation application for the given student Id and program code asynchronously.
        /// </summary>
        /// <param name="studentId">Id of the student</param>
        /// <param name="programCode">program code that student belongs to</param>
        /// <returns><see cref="Ellucian.Colleague.Dtos.Student.GraduationApplication">Graduation Application</see> object that was created</returns>
        public async Task<Ellucian.Colleague.Dtos.Student.GraduationApplication> GetGraduationApplicationAsync(string studentId, string programCode)
        {
            if (string.IsNullOrEmpty(studentId) || string.IsNullOrEmpty(programCode))
            {
                var message = "Student Id and program Code must be provided";
                logger.Info(message);
                throw new ArgumentNullException(message);
            }
            // Make sure the person requesting the application is the student.
            if (CurrentUser.PersonId != studentId)
            {
                var message = "Current user is not the student of requested graduation application and therefore cannot access it.";
                logger.Info(message);
                throw new PermissionsException(message);
            }
            Domain.Student.Entities.GraduationApplication graduationApplicationEntity = null;
            try
            {
                graduationApplicationEntity = await graduationApplicationRepository.GetGraduationApplicationAsync(studentId, programCode);

                
            }
            catch (KeyNotFoundException)
            {
                logger.Info(string.Format("Graduation Application not found in repository for given student Id {0} and program Id {1}", studentId, programCode));
                throw;
            }
            catch (Exception ex)
            {
                var message = string.Format("Exception occurred while trying to read graduation application from repository using  student Id {0} and program Id {1}  Exception message: ", studentId, programCode, ex.Message);
                logger.Info(ex, message);
                throw;
            }
            // Make sure the person requesting the application is the student.
            if (graduationApplicationEntity.StudentId != studentId || graduationApplicationEntity.ProgramCode != programCode)
            {
                var message = "Current user is not the student of requested graduation application and therefore cannot access it.";
                logger.Info(message);
                throw new PermissionsException(message);
            }
            try
            {
                var applicationDtoAdapter = _adapterRegistry.GetAdapter<Domain.Student.Entities.GraduationApplication, Dtos.Student.GraduationApplication>();
                var graduationDto = applicationDtoAdapter.MapToType(graduationApplicationEntity);

                //check for diploma and preferred addreses are same
                IEnumerable<Address> addresses = addressRepository.GetPersonAddresses(studentId);
                var preferredAddress = addresses != null ? addresses.ToList().Where(a => a.IsPreferredAddress).FirstOrDefault() : null;
                if ((preferredAddress != null && graduationApplicationEntity.MailDiplomaToAddressLines != null && graduationApplicationEntity.MailDiplomaToAddressLines.Count > 0))
                {
                    string studentPreferredAddress = ConvertToAddressLabel(preferredAddress.AddressLines, preferredAddress.City, preferredAddress.State, preferredAddress.PostalCode, preferredAddress.Country);
                    string studentGraduationAddress = ConvertToAddressLabel(graduationApplicationEntity.MailDiplomaToAddressLines, graduationApplicationEntity.MailDiplomaToCity, graduationApplicationEntity.MailDiplomaToState, graduationApplicationEntity.MailDiplomaToPostalCode, graduationApplicationEntity.MailDiplomaToCountry);
                    if (studentPreferredAddress.Equals(studentGraduationAddress, StringComparison.OrdinalIgnoreCase))
                    {
                        graduationDto.IsDiplomaAddressSameAsPreferred = true;
                    }
                    else
                    {
                        graduationDto.IsDiplomaAddressSameAsPreferred = false;
                    }

                }
                return graduationDto;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error converting graduation application Entity to Dto: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Validates a new graduation application and creates a new application in the database, returning the created application asynchronously.
        /// </summary>
        /// <param name="graduateApplication">New graduate application object to add</param>
        /// <returns><see cref="Ellucian.Colleague.Dtos.Student.GraduationApplication">Graduation Application</see> object that was created</returns>
        public async Task<Dtos.Student.GraduationApplication> CreateGraduationApplicationAsync(Dtos.Student.GraduationApplication graduationApplicationDto)
        {
            // Throw exception if incoming graduation application is null
            if (graduationApplicationDto == null)
            {
                var message = "Graduation Application object must be provided.";
                logger.Error(message);
                throw new ArgumentNullException("graduationApplication", message);
            }
            // Throw Exception if the incoming dto is missing any required paramters.
            if (string.IsNullOrEmpty(graduationApplicationDto.StudentId) || string.IsNullOrEmpty(graduationApplicationDto.ProgramCode))
            {
                var message = "Graduation Application is missing a required property.";
                logger.Error(message);
                throw new ArgumentException("graduationApplication", message);
            }
            // Throw exception if application is being submitted by someone other than the student.
            if (CurrentUser.PersonId != graduationApplicationDto.StudentId)
            {
                var message = "User does not have permissions to create a Graduation Application for this student.";
                logger.Error(message);
                throw new PermissionsException(message);
            }
            // VALIDATE THE ENTITY
            if (!string.IsNullOrWhiteSpace(graduationApplicationDto.GraduationTerm))
            {
                var term = await termRepository.GetAsync(graduationApplicationDto.GraduationTerm);
                if (term == null || (term != null && term.Code != graduationApplicationDto.GraduationTerm))
                {
                    var message = string.Format("Provided Term {0} does not exists in repository", graduationApplicationDto.GraduationTerm);
                    logger.Error(message);
                    throw new ArgumentException("gradualtionApplication", message);
                }
            }
            var program = await programRepository.GetAsync(graduationApplicationDto.ProgramCode);
            if (program == null || (program != null && program.Code != graduationApplicationDto.ProgramCode))
            {
                var message = string.Format("Provided Program Code {0} does not exists in repository", graduationApplicationDto.ProgramCode);
                logger.Error(message);
                throw new ArgumentException("gradualtionApplication", message);
            }
            var student = await studentRepository.GetAsync(graduationApplicationDto.StudentId);
            if (student == null || (student != null && student.Id != graduationApplicationDto.StudentId))
            {
                var message = string.Format("Provided Student Id {0} does not exists in repository", graduationApplicationDto.StudentId);
                logger.Error(message);
                throw new ArgumentException("gradualtionApplication", message);
            }
            if (!student.ProgramIds.Contains(graduationApplicationDto.ProgramCode))
            {
                var message = string.Format("Student Id {0} does not have active Program Code {1}", graduationApplicationDto.StudentId, graduationApplicationDto.ProgramCode);
                logger.Error(message);
                throw new ArgumentException("gradualtionApplication", message);
            }
            Domain.Student.Entities.GraduationApplication graduationApplicationToAddEntity = null;
            try
            {
                //if there is no diploma address provided, save preferred address in graduation file
                //this is done by updating diploma mail address with preferred address
                if (graduationApplicationDto.MailDiplomaToAddressLines == null || graduationApplicationDto.MailDiplomaToAddressLines.Count == 0)

                {
                    UpdateGraduationDiplomaMailAddress(graduationApplicationDto);
                }
                var graduationApplicationDtoToEntityAdapter = _adapterRegistry.GetAdapter<Dtos.Student.GraduationApplication, Domain.Student.Entities.GraduationApplication>();
                graduationApplicationToAddEntity = graduationApplicationDtoToEntityAdapter.MapToType(graduationApplicationDto);
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error converting incoming graduation application Dto to Entity: " + ex.Message);
                throw;
            }
            try
            {
                var graduationApplicationAddedEntity = await graduationApplicationRepository.CreateGraduationApplicationAsync(graduationApplicationToAddEntity);
                var applicationDtoAdapter = _adapterRegistry.GetAdapter<Domain.Student.Entities.GraduationApplication, Dtos.Student.GraduationApplication>();
                return applicationDtoAdapter.MapToType(graduationApplicationAddedEntity);
            }
            catch (ExistingResourceException egx)
            {
                logger.Error(string.Format("Graduation Application already exists for student Id {0} in program Code {1}", graduationApplicationDto.StudentId, graduationApplicationDto.ProgramCode)); logger.Info(egx.ToString());
                throw;
            }
            catch (Exception ex)
            {
                logger.Info(ex, ex.ToString());
                throw;
            }
        }

        /// <summary>
        /// Retrieve list of all  the graduation applications for the given student Id asynchronously.
        /// </summary>
        /// <param name="studentId">Id of the student</param>
        /// <returns><see cref="Ellucian.Colleague.Dtos.Student.GraduationApplication">List of Graduation Application</see></returns>
        public async Task<IEnumerable<Dtos.Student.GraduationApplication>> GetGraduationApplicationsAsync(string studentId)
        {
            List<Dtos.Student.GraduationApplication> graduationApplicationsDtoLst = new List<Dtos.Student.GraduationApplication>();
            List<Domain.Student.Entities.GraduationApplication> GraduationApplicationsEntityLst = null;
            if (string.IsNullOrEmpty(studentId))
            {
                var message = "Student Id must be provided";
                logger.Info(message);
                throw new ArgumentNullException(message);
            }
            if (CurrentUser.PersonId != studentId)
            {
                var message = "Current user is not the student of requested graduation applications and therefore cannot access it.";
                logger.Info(message);
                throw new PermissionsException(message);
            }
            try
            {
                GraduationApplicationsEntityLst = await graduationApplicationRepository.GetGraduationApplicationsAsync(studentId);
                if (GraduationApplicationsEntityLst == null)
                {
                    var errorMessage = string.Format("Unable to access Graduates record with student Id {0} ", studentId);
                    throw new KeyNotFoundException(errorMessage);
                }
            }
            catch (KeyNotFoundException)
            {
                logger.Info(string.Format("Graduation Applications not found in repository for given student Id {0}", studentId));
                throw;
            }
            catch (Exception ex)
            {
                var message = string.Format("Exception occurred while trying to retrieve graduation applications from repository for  student Id {0} - Exception message: ", studentId, ex.Message);
                logger.Info(ex, message);
                throw;
            }
            try
            {
                var applicationDtoAdapter = _adapterRegistry.GetAdapter<Domain.Student.Entities.GraduationApplication, Dtos.Student.GraduationApplication>();
                foreach (var graduationApplicationEntity in GraduationApplicationsEntityLst)
                {
                    var graduationDto = applicationDtoAdapter.MapToType(graduationApplicationEntity);
                    graduationApplicationsDtoLst.Add(graduationDto);
                }
                return graduationApplicationsDtoLst;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error converting incoming graduation application Entity to Dto: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// Validates an existing graduation application and updates it in the database, returning the updated application asynchronously.
        /// </summary>
        /// <param name="graduateApplication">Updated graduate application object</param>
        /// <returns><see cref="Ellucian.Colleague.Dtos.Student.GraduationApplication">Graduation Application</see> object that was updated</returns>
        public async Task<Dtos.Student.GraduationApplication> UpdateGraduationApplicationAsync(Dtos.Student.GraduationApplication graduationApplicationDto)
        {
            try
            {
                // Throw exception if incoming graduation application is null
                if (graduationApplicationDto == null)
                {
                    var message = "Graduation Application object must be provided to update.";
                    logger.Error(message);
                    throw new ArgumentNullException("graduationApplication", message);
                }
                // Throw Exception if the incoming dto is missing any required paramters.
                if (string.IsNullOrWhiteSpace(graduationApplicationDto.StudentId) || string.IsNullOrWhiteSpace(graduationApplicationDto.ProgramCode))
                {
                    var message = "Graduation Application is missing a required property to update.";
                    logger.Error(message);
                    throw new ArgumentException("graduationApplication", message);
                }
                // Throw exception if application is being submitted by someone other than the student.
                if (CurrentUser.PersonId != graduationApplicationDto.StudentId)
                {
                    var message = "User does not have permissions to update a Graduation Application for this student.";
                    logger.Error(message);
                    throw new PermissionsException(message);
                }
                // VALIDATE THE ENTITY
                var program = await programRepository.GetAsync(graduationApplicationDto.ProgramCode);
                if (program == null || (program != null && program.Code != graduationApplicationDto.ProgramCode))
                {
                    var message = string.Format("Provided Program Code {0} does not exists in repository", graduationApplicationDto.ProgramCode);
                    logger.Error(message);
                    throw new ArgumentException("gradualtionApplication", message);
                }
                var student = await studentRepository.GetAsync(graduationApplicationDto.StudentId);
                if (student == null || (student != null && student.Id != graduationApplicationDto.StudentId))
                {
                    var message = string.Format("Provided Student Id {0} does not exists in repository", graduationApplicationDto.StudentId);
                    logger.Error(message);
                    throw new ArgumentException("gradualtionApplication", message);
                }
                if (!student.ProgramIds.Contains(graduationApplicationDto.ProgramCode))
                {
                    var message = string.Format("Student Id {0} does not have active Program Code {1}", graduationApplicationDto.StudentId, graduationApplicationDto.ProgramCode);
                    logger.Error(message);
                    throw new ArgumentException("gradualtionApplication", message);
                }
                GraduationApplication application = await graduationApplicationRepository.GetGraduationApplicationAsync(graduationApplicationDto.StudentId, graduationApplicationDto.ProgramCode);
                if (application == null)
                {
                    var errorMessage = string.Format("Unable to retrieve graduate record for student Id {0} and program Code {1} ", graduationApplicationDto.StudentId, graduationApplicationDto.ProgramCode);
                    logger.Info(errorMessage);
                    throw new KeyNotFoundException(errorMessage);
                }
                Domain.Student.Entities.GraduationConfiguration studentConfiguration = await studentConfigurationRepository.GetGraduationConfigurationAsync();
                if (studentConfiguration.GraduationTerms == null || (studentConfiguration.GraduationTerms != null && !studentConfiguration.GraduationTerms.Contains(application.GraduationTerm)))
                {
                    var errorMessage = string.Format("Unable to update Graduates record with student Id {0} and program Code {1}, Term {2} is closed", graduationApplicationDto.StudentId, graduationApplicationDto.ProgramCode, application.GraduationTerm);
                    logger.Info(errorMessage);
                    throw new Exception(errorMessage);
                }
                //if there are no diploma address provided, save preferred address in graduation file
                //this is done by updating diploma mail address with preferred address
                if (graduationApplicationDto.MailDiplomaToAddressLines == null || graduationApplicationDto.MailDiplomaToAddressLines.Count == 0)
                {
                    UpdateGraduationDiplomaMailAddress(graduationApplicationDto);
                   
                }

                Domain.Student.Entities.GraduationApplication graduationApplicationToUpdateEntity = null;
                try
                {
                    var graduationApplicationDtoToEntityAdapter = _adapterRegistry.GetAdapter<Dtos.Student.GraduationApplication, Domain.Student.Entities.GraduationApplication>();
                    graduationApplicationToUpdateEntity = graduationApplicationDtoToEntityAdapter.MapToType(graduationApplicationDto);
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "Error converting incoming graduation application Dto to Entity: " + ex.Message);
                    throw;
                }

                var graduationApplicationUpdatedEntity = await graduationApplicationRepository.UpdateGraduationApplicationAsync(graduationApplicationToUpdateEntity);
                var applicationDtoAdapter = _adapterRegistry.GetAdapter<Domain.Student.Entities.GraduationApplication, Dtos.Student.GraduationApplication>();
                return applicationDtoAdapter.MapToType(graduationApplicationUpdatedEntity);
            }
            catch (KeyNotFoundException)
            {
                logger.Info(string.Format("Graduation Application not found in repository for given student Id {0} and program Id {1}", graduationApplicationDto.StudentId, graduationApplicationDto.ProgramCode));
                throw;
            }
            catch (Exception ex)
            {
                logger.Info(ex, ex.ToString());
                throw;
            }
        }

        

        /// <summary>
        /// Retrieves graduation application fee information for the given student Id and program code asynchronously.
        /// </summary>
        /// <param name="studentId">Id of the student</param>
        /// <param name="programCode">program code that student for which student is applying for graduation</param>
        /// <returns><see cref="Ellucian.Colleague.Dtos.Student.GraduationApplicationFee">Graduation Application</see> object that was created</returns>
        public async Task<Ellucian.Colleague.Dtos.Student.GraduationApplicationFee> GetGraduationApplicationFeeAsync(string studentId, string programCode)
        {
            if (string.IsNullOrEmpty(studentId) || string.IsNullOrEmpty(programCode))
            {
                var message = "Student Id and program Code must be provided";
                logger.Info(message);
                throw new ArgumentNullException(message);
            }
            Domain.Student.Entities.GraduationApplicationFee graduationApplicationFeeEntity = null;
            try
            {
                graduationApplicationFeeEntity = await graduationApplicationRepository.GetGraduationApplicationFeeAsync(studentId, programCode);
            }
            catch (Exception ex)
            {
                var message = string.Format("Exception occurred while trying to get graduation application fee from repository using  student Id {0} and program Id {1}  Exception message: ", studentId, programCode, ex.Message);
                logger.Info(ex, message);
                throw;
            }
            try
            {
                var applicationFeeDtoAdapter = _adapterRegistry.GetAdapter<Domain.Student.Entities.GraduationApplicationFee, Dtos.Student.GraduationApplicationFee>();
                var graduationFeeDto = applicationFeeDtoAdapter.MapToType(graduationApplicationFeeEntity);
                return graduationFeeDto;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Error converting graduation application fee Entity to Dto: " + ex.Message);
                throw;
            }
        }

        /// <summary>
        /// convert address properties to string
        /// </summary>
        /// <param name="addressLines"></param>
        /// <param name="city"></param>
        /// <param name="state"></param>
        /// <param name="postalCode"></param>
        /// <param name="country"></param>
        /// <returns></returns>
        private string ConvertToAddressLabel(List<string> addressLines, string city, string state, string postalCode, string country)
        {
            StringBuilder addressLabel = new StringBuilder();
            if (addressLines != null && addressLines.Count > 0)
            {
                addressLines.ForEach(item => addressLabel.Append(item));
            }
            if (!string.IsNullOrEmpty(city))
            {
                addressLabel.Append(city);
            }
            if (!string.IsNullOrEmpty(state))
            {
                addressLabel.Append(state);
            }
            if (!string.IsNullOrEmpty(postalCode))
            {
                addressLabel.Append(postalCode);
            }
            if (!string.IsNullOrEmpty(country))
            {
                addressLabel.Append(country);
            }
            return addressLabel.ToString();
        }

        /// <summary>
        /// this is to update diploma address with preferred address
        /// </summary>
        /// <param name="graduationApplicationDto"></param>
        private void UpdateGraduationDiplomaMailAddress(Dtos.Student.GraduationApplication graduationApplicationDto)
        {
            var addresses = addressRepository.GetPersonAddresses(graduationApplicationDto.StudentId);
            var preferredAddress = addresses != null ? addresses.ToList().Where(a => a.IsPreferredAddress).FirstOrDefault() : null;
            if (preferredAddress != null)
            {
                graduationApplicationDto.MailDiplomaToAddressLines.AddRange(preferredAddress.AddressLines);
                graduationApplicationDto.MailDiplomaToCity = preferredAddress.City;
                graduationApplicationDto.MailDiplomaToCountry = preferredAddress.Country;
                graduationApplicationDto.MailDiplomaToPostalCode = preferredAddress.PostalCode;
                graduationApplicationDto.MailDiplomaToState = preferredAddress.State;
            }
        }
    }
}
