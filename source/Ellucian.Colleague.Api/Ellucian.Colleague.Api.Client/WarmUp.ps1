# Copyright 2014-2017 Ellucian Company L.P. and its affiliates.
# Version 1.16 (Delivered with Colleague Web API 1.16)

# PURPOSE:
# Warm up the Colleague Web API by pre-loading the most frequently-used and shared API data
# repository caches to reduce initial load time.

# PARAMETERS:
# -webApiBaseUrl: Full URL of the Colleague Web API instance to be warmed-up.
# -userId: Colleague Web username to use when running the warm-up.
# -password: Password for the above Colleague Web username.
# -recycleAppPool: The name of the IIS application pool to be recycled prior to running
#    the warm-up script.

# EXAMPLE POWERSHELL COMMANDS:
# PS C:\scripts> .\WarmUp.ps1 "http://serverAddress:1234/ColleagueApi" "loginID" "password"
# PS C:\scripts> .\WarmUp.ps1 "http://serverAddress:1234/ColleagueApi" "loginID" "password" "applicationpoolname"

# RECOMMENDED USAGE:
# The data caches maintained by Colleague Web API are not retained when its application pool
# is recycled. Accordingly, there can be a noticeable delay in the responsiveness of applications
# that make use of the Colleague Web API. This may result in a poor user experience for whoever 
# happens to access the application first. To alleviate this, IT administrators should consider 
# running this script at least once every 24 hours, during off-peak time or just after daily
# Colleague backup activities. The script can be run more frequently as well.
#
# The script can be used with or without an option that preforms a recycle of the Colleague Web
# API's IIS application pool. When using the -recycleAppPool option the traditional recycling
# settings within IIS can be set to not recycle on periodic bases (no regular time interval or 
# specific times) and to never time out (idle time-out) and a scheduled run of this script with 
# the -recycleAppPool can be used instead to ensure that the application pool recycle and 
# warm-up happen at the same time rather than trying to coordinate a scheduled task just after
# a periodic application pool recycle or worse not warming-up due to an idle time-out shutting-
# down the application pool. The suggested usage of the -recycleAppPool option is to create a scheduled task that runs 
# once a day, during off-peak time or just after Colleague backup activities are finished that
# uses the -recycleAppPool option and then, optionally if you wish to run the warm-up periodically
# throughout the day, create another scheduled task that does not use the -recycleAppPool so that
# the application pool is not being recycled during the normal hours of the day.
#
# You can find when and how IIS automatically recycles the Colleague Web API application pool
# by right-clicking on the application pool in IIS Manager and choosing 'Recycling.'
#
# This script could be scheduled using the Windows Task Scheduler as described in this blog post:
# http://blogs.technet.com/b/heyscriptingguy/archive/2012/08/11/weekend-scripter-use-the-windows-task-scheduler-to-run-a-windows-powershell-script.aspx

# NOTES: 
# 1. This script, as delivered by Ellucian, pre-loads some caches that are most frequently used
#    and shared. It does not pre-load all caches.
# 2. The endpoints used in the delivered script do not require special user permission/roles or
#    valid input parameters. Therefore, no modification is necessary prior to running this script.
# 3. You may add to this script more warm up requests for your own endpoints that use caching,
#    or for any other endpoints that you deem necessary to improve performance.
# 4. In order to use the -recycleAppPool option this script MUST be run from the Colleague Web API
#    host web server.

# CAUTIONS:
# 1. You should take special care to select a user ID that does not have broad access to the
#    system (i.e. a user that is assigned many self-service roles), in case the credentials are
#    somehow compromised.
# 2. When adding additional warm up requests to this script, you should ensure the endpoints used
#    and any associated parameters are protected against unauthorized access.
# 3. This file will be overwritten when you perform an upgrade installation. For that reason,
#    it is recommended that you use a separate copy of the script for your customization needs.
# 4. Use of the -recycleAppPool option should only be done during off-peak times, such as in the
#    middle of the night or just after Colleague backup activities are complete. If you run this
#    script periodically throughout the day you should not use this option during those runs.
###


Param(
	[string]$webApiBaseUrl,
	[string]$userId,
	[string]$password,
	[string]$recycleAppPool
)
#various Accept headers
$vndV1 = "application/vnd.ellucian.v1+json"
$vndV2 = "application/vnd.ellucian.v2+json"
$vndV3 = "application/vnd.ellucian.v3+json"
$vndV4 = "application/vnd.ellucian.v4+json"
$vndV5 = "application/vnd.ellucian.v5+json"
$vndStuPlanV1 = "application/vnd.ellucian-planning-student.v1+json"
$vndStuProfV1 = "application/vnd.ellucian-person-profile.v1+json"
$vndHedtechV4 = "application/vnd.hedtech.integration.v4+json"
$vndPilotV1 = "application/vnd.ellucian-pilot.v1+json"
$vndInvoicePayV1 = "application/vnd.ellucian-invoice-payment.v1+json"
$vndIlpV1 = "application/vnd.ellucian-ilp.v1+json"
$vndHrDemoV1 = "application/vnd.ellucian-human-resource-demographics.v1+json"


# Sends a GET request to the specified endpoint URL using the provided token
function get([string]$url, [string]$token, [string]$accept) 
{
	$req = [System.Net.WebRequest]::Create($url)
	$req.Method ="GET"
	if ($token) 
	{
		$req.Headers.Add("X-CustomCredentials", $token)
	}
	if ($accept) 
	{
		$req.Accept = $accept
	}
	$req.ContentType = "application/json"
	$req.Timeout = 300000 # max 5 minutes
	$resp = $req.GetResponse()
	if ($resp)
	{	
		$reader = new-object System.IO.StreamReader($resp.GetResponseStream())
		$response = $reader.ReadToEnd().Trim()
		return $response
	}
	else
	{
		return ""
	}
}

# Sends a POST request to the specified endpoint URL using the provided token
function post([string]$url, [string]$body, [string]$token, [string]$accept) 
{
	$req = [System.Net.WebRequest]::Create($url)
	$req.Method ="POST"
	if ($token) 
	{
		$req.Headers.Add("X-CustomCredentials", $token)
	}
	if ($accept) 
	{
		$req.Accept = $accept
	}
	$req.ContentType = "application/json"
	$req.Timeout = 300000 # max 5 minutes
	$Post = $body
	$PostStr = [System.Text.Encoding]::UTF8.GetBytes($Post)
	$req.ContentLength = $PostStr.Length
	$requestStream = $req.GetRequestStream()
	$requestStream.Write($PostStr, 0,$PostStr.length)
	$requestStream.Close()	
	$resp = $req.GetResponse()
	if ($resp)
	{	
		$reader = new-object System.IO.StreamReader($resp.GetResponseStream())
		$response = $reader.ReadToEnd().Trim()
		return $response
	}
	else
	{
		return ""
	}
}

# Recycle application pool if supplied
if (![System.String]::IsNullOrEmpty($recycleAppPool))
{
	Write-Host "Recycling application pool" $recycleAppPool
	$commandPath = [Environment]::GetEnvironmentVariable("systemroot") + "\system32\inetsrv\appcmd.exe"
	if (Test-Path $commandPath)
	{
		$command = "'" + $commandPath + "' recycle apppool " + "'" + $recycleAppPool + "'"
		iex "& $command"
	}
	else
	{
		Write-Host "Cannot recycle because appcmd.exe cannot be found at" $commandPath
	}
}

# Login
Try
{
	Write-Host "Logging in..."
	$token = post ($webApiBaseUrl + "/session/login") ("{'UserId':'" + $userId + "','Password':'" + $password + "'}")
}
Catch [Exception] 
{
	Write-Error "Login failed. Check API URL and user credentials then try again."
	Exit
}

# Send HTTP requests to "warm up" or pre-load the API caches

Write-Host "Getting courses, course sections, terms and associated valcodes..."
$results = post ($webApiBaseUrl + "/Courses/Search?pageSize=10&pageIndex=1") "{'Keywords':'math'}" $token $vndV2

Write-Host "Getting faculty data..."
Try
{
	# An invalid faculty ID will cause an exception. However, that exception does not
	# mean the faculty cache did not load successfully, so it can be ignored.
	$results = get ($webApiBaseUrl + "/faculty/0000001") $token
}
Catch{}

Write-Host "Getting programs, transcript groupings, credit types..."
$results = get ($webApiBaseUrl + "/programs") $token

Write-Host "Getting academic levels..."
$results = get ($webApiBaseUrl + "/academic-levels") $token $vndV1

Write-Host "Getting CCDs..."
$results = get ($webApiBaseUrl + "/ccds") $token

Write-Host "Getting degrees..."
$results = get ($webApiBaseUrl + "/degrees") $token

Write-Host "Getting majors..."
$results = get ($webApiBaseUrl + "/majors") $token

Write-Host "Getting minors..."
$results = get ($webApiBaseUrl + "/minors") $token

Write-Host "Getting specializations..."
$results = get ($webApiBaseUrl + "/specializations") $token

Write-Host "Getting subjects..."
$results = get ($webApiBaseUrl + "/subjects") $token $vndV1

Write-Host "Getting grades..."
$results = get ($webApiBaseUrl + "/grades") $token

Write-Host "Getting course levels..."
$results = get ($webApiBaseUrl + "/course-levels") $token $vndV1

Write-Host "Getting course types..."
$results = get ($webApiBaseUrl + "/course-types") $token

Write-Host "Getting ImportantNumber records..."
$results = get ($webApiBaseUrl + "/important-numbers") $token

Write-Host "Getting rooms..."
$results = get ($webApiBaseUrl + "/rooms") $token $vndV1

Write-Host "Getting buildings..."
$results = get ($webApiBaseUrl + "/buildings") $token $vndV1

Write-Host "Getting departments..."
$results = get ($webApiBaseUrl + "/departments") $token

Write-Host "Getting locations..."
$results = get ($webApiBaseUrl + "/locations") $token

Write-Host "Getting instructional methods..."
$results = get ($webApiBaseUrl + "/instructional-methods") $token $vndV1

Write-Host "Getting races..."
$results = get ($webApiBaseUrl + "/races") $token $vndV1

Write-Host "Getting terms..."
$results = get ($webApiBaseUrl + "/terms") $token

Write-Host "Getting finance configuration..."
$results = get ($webApiBaseUrl + "/configuration") $token

# The endpoints used below are available only with Colleague Web API 1.4 and later.

Write-Host "Getting communication codes..."
$results = get ($webApiBaseUrl + "/communication-codes") $token

Write-Host "Getting convenience fees..."
$results = get ($webApiBaseUrl + "/ecommerce/convenience-fees") $token

Write-Host "Getting denominations..."
$results = get ($webApiBaseUrl + "/denominations") $token

Write-Host "Getting disability types..."
$results = get ($webApiBaseUrl + "/disability-types") $token

Write-Host "Getting interests..."
$results = get ($webApiBaseUrl + "/interests") $token $vndV1

Write-Host "Getting receivable types..."
$results = get ($webApiBaseUrl + "/receivables/receivable-types") $token

Write-Host "Getting deposit types..."
$results = get ($webApiBaseUrl + "/deposits/deposit-types") $token

Write-Host "Getting admitted statuses..."
$results = get ($webApiBaseUrl + "/admitted-statuses") $token

Write-Host "Getting application statuses..."
$results = get ($webApiBaseUrl + "/application-statuses") $token

Write-Host "Getting topic codes..."
$results = get ($webApiBaseUrl + "/topic-codes") $token

Write-Host "Getting prefixes..."
$results = get ($webApiBaseUrl + "/prefixes") $token

Write-Host "Getting suffixes..."
$results = get ($webApiBaseUrl + "/suffixes") $token

Write-Host "Getting ImportantNumber categories..."
$results = get ($webApiBaseUrl + "/important-number-categories") $token

Write-Host "Getting building types..."
$results = get ($webApiBaseUrl + "/building-types") $token

Write-Host "Getting degree types..."
$results = get ($webApiBaseUrl + "/degree-types") $token

Write-Host "Getting citizen types..."
$results = get ($webApiBaseUrl + "/citizen-types") $token

Write-Host "Getting academic-programs..."
$results = get ($webApiBaseUrl + "/academic-programs") $token $vndV1

Write-Host "Getting institution types..."
$results = get ($webApiBaseUrl + "/institution-types") $token

Write-Host "Getting languages..."
$results = get ($webApiBaseUrl + "/languages") $token

Write-Host "Getting marital-statuses..."
$results = get ($webApiBaseUrl + "/marital-statuses") $token $vndV1

Write-Host "Getting prospect sources..."
$results = get ($webApiBaseUrl + "/prospect-sources") $token

Write-Host "Getting visa-types..."
$results = get ($webApiBaseUrl + "/visa-types") $token

Write-Host "Getting application influences..."
$results = get ($webApiBaseUrl + "/application-influences") $token

Write-Host "Getting application status categories..."
$results = get ($webApiBaseUrl + "/application-status-categories") $token

Write-Host "Getting career goals..."
$results = get ($webApiBaseUrl + "/career-goals") $token

Write-Host "Getting external transcript statuses..."
$results = get ($webApiBaseUrl + "/external-transcript-statuses") $token

Write-Host "Getting student loads..."
$results = get ($webApiBaseUrl + "/student-loads") $token

Write-Host "Getting transcript categories..."
$results = get ($webApiBaseUrl + "/transcript-categories") $token

Write-Host "Getting noncourse statuses..."
$results = get ($webApiBaseUrl + "/noncourse-statuses") $token

# Added Ethos Endpoints to the script

Write-Host "Getting Ethos academic-catalogs..."
$results = get ($webApiBaseUrl + "/academic-catalogs") $token 

Write-Host "Getting Ethos academic-credentials..."
$results = get ($webApiBaseUrl + "/academic-credentials") $token

Write-Host "Getting Ethos academic-disciplines..."
$results = get ($webApiBaseUrl + "/academic-disciplines") $token 

Write-Host "Getting Ethos academic-honors..."
$results = get ($webApiBaseUrl + "/academic-honors") $token

Write-Host "Getting Ethos academic levels..."
$results = get ($webApiBaseUrl + "/academic-levels") $token

Write-Host "Getting Ethos academic-period-enrollment-statuses..."
$results = get ($webApiBaseUrl + "/academic-period-enrollment-statuses") $token 

Write-Host "Getting Ethos academic-periods..."
$results = get ($webApiBaseUrl + "/academic-periods") $token 

Write-Host "Getting Ethos academic-programs..."
$results = get ($webApiBaseUrl + "/academic-programs") $token 

Write-Host "Getting Ethos academic-standings..."
$results = get ($webApiBaseUrl + "/academic-standings") $token

Write-Host "Getting Ethos accounting-string-components..."
$results = get ($webApiBaseUrl + "/accounting-string-components") $token

Write-Host "Getting Ethos accounting-string-formats..."
$results = get ($webApiBaseUrl + "/accounting-string-formats") $token

Write-Host "Getting Ethos accounts-payable-sources..."
$results = get ($webApiBaseUrl + "/accounts-payable-sources") $token 

Write-Host "Getting Ethos accounting-codes..."
$results = get ($webApiBaseUrl + "/accounting-codes") $token 

Write-Host "Getting Ethos account-receivable-types..."
$results = get ($webApiBaseUrl + "/account-receivable-types") $token 

Write-Host "Getting Ethos address-types..."
$results = get ($webApiBaseUrl + "/address-types") $token 

Write-Host "Getting Ethos admission-application-status-types..."
$results = get ($webApiBaseUrl + "/admission-application-status-types") $token

Write-Host "Getting Ethos admission-application-types..."
$results = get ($webApiBaseUrl + "/admission-application-types") $token 

Write-Host "Getting Ethos admission-application-withdrawal-reasons..."
$results = get ($webApiBaseUrl + "/admission-application-withdrawal-reasons") $token 

Write-Host "Getting Ethos admission-populations..."
$results = get ($webApiBaseUrl + "/admission-populations") $token 

Write-Host "Getting Ethos admission-residency-types..."
$results = get ($webApiBaseUrl + "/admission-residency-types") $token

Write-Host "Getting Ethos advisor-types..."
$results = get ($webApiBaseUrl + "/advisor-types") $token

Write-Host "Getting Ethos aptitude-assessment-types..."
$results = get ($webApiBaseUrl + "/aptitude-assessment-types") $token 

Write-Host "Getting Ethos aptitude-assessments..."
$results = get ($webApiBaseUrl + "/aptitude-assessments") $token

Write-Host "Getting Ethos assessment-calculation-methods..."
$results = get ($webApiBaseUrl + "/assessment-calculation-methods") $token 

Write-Host "Getting Ethos assessment-percentile-types..."
$results = get ($webApiBaseUrl + "/assessment-percentile-types") $token 

Write-Host "Getting Ethos assessment-special-circumstances..."
$results = get ($webApiBaseUrl + "/assessment-special-circumstances") $token 

Write-Host "Getting Ethos bargaining-units..."
$results = get ($webApiBaseUrl + "/bargaining-units") $token 

Write-Host "Getting Ethos billing-override-reasons..."
$results = get ($webApiBaseUrl + "/billing-override-reasons") $token

Write-Host "Getting Ethos buildings..."
$results = get ($webApiBaseUrl + "/buildings") $token

Write-Host "Getting Ethos building-wings..."
$results = get ($webApiBaseUrl + "/building-wings") $token

Write-Host "Getting Ethos campus-involvement-roles..."
$results = get ($webApiBaseUrl + "/campus-involvement-roles") $token

Write-Host "Getting Ethos campus-organizations..."
$results = get ($webApiBaseUrl + "/campus-organizations") $token

Write-Host "Getting Ethos campus-organization-types..."
$results = get ($webApiBaseUrl + "/campus-organization-types") $token 

Write-Host "Getting Ethos citizenship-statuses..."
$results = get ($webApiBaseUrl + "/citizenship-statuses") $token

Write-Host "Getting Ethos comment-subject-area..."
$results = get ($webApiBaseUrl + "/comment-subject-area") $token

Write-Host "Getting Ethos commerce-tax-codes..."
$results = get ($webApiBaseUrl + "/commerce-tax-codes") $token 

Write-Host "Getting Ethos commodity-codes..."
$results = get ($webApiBaseUrl + "/commodity-codes") $token 

Write-Host "Getting Ethos commodity-unit-types..."
$results = get ($webApiBaseUrl + "/commodity-unit-types") $token 

Write-Host "Getting Ethos course levels..."
$results = get ($webApiBaseUrl + "/course-levels") $token

Write-Host "Getting Ethos courses..."
$results = get ($webApiBaseUrl + "/courses") $token

Write-Host "Getting Ethos credit categories..."
$results = get ($webApiBaseUrl + "/credit-categories") $token

Write-Host "Getting Ethos deduction-types..."
$results = get ($webApiBaseUrl + "/deduction-types") $token

Write-Host "Getting Ethos educational-institution-units..."
$results = get ($webApiBaseUrl + "/educational-institution-units") $token

Write-Host "Getting Ethos email-types..."
$results = get ($webApiBaseUrl + "/email-types") $token 

Write-Host "Getting Ethos employment-classifications..."
$results = get ($webApiBaseUrl + "/employment-classifications") $token 

Write-Host "Getting Ethos employment-leave-of-absence-reasons..."
$results = get ($webApiBaseUrl + "/employment-leave-of-absence-reasons") $token

Write-Host "Getting Ethos employment-proficiencies..."
$results = get ($webApiBaseUrl + "/employment-proficiencies") $token 

Write-Host "Getting Ethos employment-termination-reasons..."
$results = get ($webApiBaseUrl + "/employment-termination-reasons") $token

Write-Host "Getting Ethos enrollment-statuses..."
$results = get ($webApiBaseUrl + "/enrollment-statuses") $token 

Write-Host "Getting Ethos employment-vocations..."
$results = get ($webApiBaseUrl + "/employment-vocations") $token

Write-Host "Getting Ethos ethnicities..."
$results = get ($webApiBaseUrl + "/ethnicities") $token

Write-Host "Getting Ethos external-employment-positions..."
$results = get ($webApiBaseUrl + "/external-employment-positions") $token

Write-Host "Getting Ethos external-employment-statuses..."
$results = get ($webApiBaseUrl + "/external-employment-statuses") $token

Write-Host "Getting Ethos financial-aid-award-periods..."
$results = get ($webApiBaseUrl + "/financial-aid-award-periods") $token

Write-Host "Getting Ethos financial-aid-fund-categories..."
$results = get ($webApiBaseUrl + "/financial-aid-fund-categories") $token

Write-Host "Getting Ethos financial-aid-fund-classifications..."
$results = get ($webApiBaseUrl + "/financial-aid-fund-classifications") $token

Write-Host "Getting Ethos financial-aid-offices..."
$results = get ($webApiBaseUrl + "/financial-aid-offices") $token

Write-Host "Getting Ethos financial-aid-years..."
$results = get ($webApiBaseUrl + "/financial-aid-years") $token

Write-Host "Getting Ethos floor-characteristics..."
$results = get ($webApiBaseUrl + "/floor-characteristics") $token

Write-Host "Getting Ethos free-on-board-types..."
$results = get ($webApiBaseUrl + "/free-on-board-types") $token

Write-Host "Getting Ethos geographic-areas..."
$results = get ($webApiBaseUrl + "/geographic-areas") $token 

Write-Host "Getting Ethos geographic-area-types..."
$results = get ($webApiBaseUrl + "/geographic-area-types") $token 

Write-Host "Getting Ethos grade-change-reasons..."
$results = get ($webApiBaseUrl + "/grade-change-reasons") $token 

Write-Host "Getting Ethos grade-definitions..."
$results = get ($webApiBaseUrl + "/grade-definitions") $token

Write-Host "Getting Ethos grade schemes..."
$results = get ($webApiBaseUrl + "/grade-schemes") $token

Write-Host "Getting Ethos housing-resident-types..."
$results = get ($webApiBaseUrl + "/housing-resident-types") $token

Write-Host "Getting Ethos identity-document-types..."
$results = get ($webApiBaseUrl + "/identity-document-types") $token 

Write-Host "Getting Ethos instructional methods..."
$results = get ($webApiBaseUrl + "/instructional-methods") $token

Write-Host "Getting Ethos instructional-platforms..."
$results = get ($webApiBaseUrl + "/instructional-platforms") $token 

Write-Host "Getting Ethos instructor-categories..."
$results = get ($webApiBaseUrl + "/instructor-categories") $token

Write-Host "Getting Ethos instructor-staff-typess..."
$results = get ($webApiBaseUrl + "/instructor-staff-types") $token

Write-Host "Getting Ethos interest-areas..."
$results = get ($webApiBaseUrl + "/interest-areas") $token 

Write-Host "Getting Ethos interests..."
$results = get ($webApiBaseUrl + "/interests") $token

Write-Host "Getting Ethos job-change-reasons..."
$results = get ($webApiBaseUrl + "/job-change-reasons") $token

Write-Host "Getting Ethos marital-statuses..."
$results = get ($webApiBaseUrl + "/marital-statuses") $token

Write-Host "Getting Ethos meal-plans..."
$results = get ($webApiBaseUrl + "/meal-plans") $token

Write-Host "Getting Ethos meal-types..."
$results = get ($webApiBaseUrl + "/meal-types") $token

Write-Host "Getting Ethos payroll-deduction-arrangement-change-reasons..."
$results = get ($webApiBaseUrl + "/payroll-deduction-arrangement-change-reasons") $token

Write-Host "Getting Ethos personal-relationship-statuses..."
$results = get ($webApiBaseUrl + "/personal-relationship-statuses") $token

Write-Host "Getting Ethos personal-relationship-types..."
$results = get ($webApiBaseUrl + "/personal-relationship-types") $token 

Write-Host "Getting Ethos person-filters..."
$results = get ($webApiBaseUrl + "/person-filters") $token 

Write-Host "Getting Ethos person-hold-types..."
$results = get ($webApiBaseUrl + "/person-hold-types") $token 

Write-Host "Getting Ethos person-name-types..."
$results = get ($webApiBaseUrl + "/person-name-types") $token 

Write-Host "Getting Ethos phone-types..."
$results = get ($webApiBaseUrl + "/phone-types") $token 

Write-Host "Getting Ethos privacy-statuses..."
$results = get ($webApiBaseUrl + "/privacy-statuses") $token

Write-Host "Getting Ethos races..."
$results = get ($webApiBaseUrl + "/races") $token

Write-Host "Getting Ethos rehire-types..."
$results = get ($webApiBaseUrl + "/rehire-types") $token 

Write-Host "Getting Ethos religions..."
$results = get ($webApiBaseUrl + "/religions") $token

Write-Host "Getting Ethos residency-types..."
$results = get ($webApiBaseUrl + "/residency-types") $token 

Write-Host "Getting Ethos room-characteristics..."
$results = get ($webApiBaseUrl + "/room-characteristics") $token

Write-Host "Getting Ethos room-rates..."
$results = get ($webApiBaseUrl + "/room-rates") $token

Write-Host "Getting Ethos room-types..."
$results = get ($webApiBaseUrl + "/room-types") $token 

Write-Host "Getting Ethos roommate-characteristics..."
$results = get ($webApiBaseUrl + "/roommate-characteristics") $token

Write-Host "Getting Ethos rooms..."
$results = get ($webApiBaseUrl + "/rooms") $token

Write-Host "Getting Ethos section-grade-types..."
$results = get ($webApiBaseUrl + "/section-grade-types") $token 

Write-Host "Getting Ethos section-registration-statuses..."
$results = get ($webApiBaseUrl + "/section-registration-statuses") $token 

Write-Host "Getting Ethos sections..."
$results = get ($webApiBaseUrl + "/sections") $token

Write-Host "Getting Ethos ship-to-destinations..."
$results = get ($webApiBaseUrl + "/ship-to-destinations") $token

Write-Host "Getting Ethos shipping-methods..."
$results = get ($webApiBaseUrl + "/shipping-methods") $token

Write-Host "Getting Ethos sites..."
$results = get ($webApiBaseUrl + "/sites") $token

Write-Host "Getting Ethos social-media-types..."
$results = get ($webApiBaseUrl + "/social-media-types") $token 

Write-Host "Getting Ethos sources..."
$results = get ($webApiBaseUrl + "/sources") $token 

Write-Host "Getting Ethos student-classifications..."
$results = get ($webApiBaseUrl + "/student-classifications") $token 

Write-Host "Getting Ethos student-cohorts..."
$results = get ($webApiBaseUrl + "/student-cohorts") $token 

Write-Host "Getting Ethos student-residential-categories..."
$results = get ($webApiBaseUrl + "/student-residential-categories") $token

Write-Host "Getting Ethos student-statuses..."
$results = get ($webApiBaseUrl + "/student-statuses") $token 

Write-Host "Getting Ethos student-types..."
$results = get ($webApiBaseUrl + "/student-types") $token 

Write-Host "Getting Ethos subjects..."
$results = get ($webApiBaseUrl + "/subjects") $token

Write-Host "Getting Ethos visa-types..."
$results = get ($webApiBaseUrl + "/visa-types") $token 

Write-Host "Getting Ethos vendor-classifications..."
$results = get ($webApiBaseUrl + "/vendor-classifications") $token 

Write-Host "Getting Ethos vendor-hold-reasons..."
$results = get ($webApiBaseUrl + "/vendor-hold-reasons") $token 

Write-Host "Getting Ethos vendor-payment-terms..."
$results = get ($webApiBaseUrl + "/vendor-payment-terms") $token

# The endpoint used below is available only with Colleague Web API 1.5 and later.

Write-Host "Getting payment plan templates..."
$results = get ($webApiBaseUrl + "/payment-plans/templates") $token


# The endpoint used below is available only with Colleague Web API 1.6 and later.

Write-Host "Getting project data..."
Try
{
	# An invalid project ID will cause an exception. However, that exception does not
	# mean the GL configuration cache did not load successfully, so it can be ignored.
	$results = get ($webApiBaseUrl + "/projects/0") $token
}
Catch{}

Write-Host "Getting financial aid external hyperlinks..."
$results = get ($webApiBaseUrl + "/financial-aid-links") $token

Write-Host "Getting financial aid offices..."
$results = get ($webApiBaseUrl + "/financial-aid-offices") $token $vndV1

Write-Host "Getting award year definitions..."
$results = get ($webApiBaseUrl + "/award-years") $token

Write-Host "Getting awards..."
$results = get ($webApiBaseUrl + "/awards") $token

Write-Host "Getting award statuses..."
$results = get ($webApiBaseUrl + "/award-statuses") $token

Write-Host "Getting award periods..."
$results = get ($webApiBaseUrl + "/award-periods") $token

Write-Host "Getting ipeds institutions..."
try
{
	$results = post ($webApiBaseUrl + "/ipeds-institutions") "['00000001']" $token
}
Catch{}

Write-Host "Getting restriction-types..."
$results = get ($webApiBaseUrl + "/restriction-types") $token

# The endpoints used below are available only with Colleague Web API 1.7 and later.

Write-Host "Getting institutions..."
$results = get ($webApiBaseUrl + "/institutions") $token

Write-Host "Getting financial aid budget components..."
$results = get ($webApiBaseUrl + "/financial-aid-budget-components") $token

Write-Host "Getting financial aid checklist items..."
$results = get ($webApiBaseUrl + "/financial-aid-checklist-items") $token

# The endpoints used below are available only with Colleague Web API 1.8 and later.

Write-Host "Getting academic progress status items..."
$results = get ($webApiBaseUrl + "/academic-progress-statuses") $token

Write-Host "Getting petition-statuses..."
$results = get ($webApiBaseUrl + "/petition-statuses") $token

Write-Host "Getting countries..."
$results = get ($webApiBaseUrl + "/countries") $token

Write-Host "Getting states..."
$results = get ($webApiBaseUrl + "/states") $token

# The endpoints used below are available only with Colleague Web API 1.10 and later.

Write-Host "Getting relationship types..."
$results = get ($webApiBaseUrl + "/relationship-types") $token

# The endpoints used below are available only with Colleague Web API 1.11 and later.

try
{
	Write-Host "Getting banks..."
	$results = get ($webApiBaseUrl + "/banks/211691185") $token
}
catch {}

Write-Host "Getting banking information configuration..."
$results = get ($webApiBaseUrl + "/banking-information-configuration") $token

# The endpoints used below are available only with Colleague Web API 1.14 and later.

Write-Host "Getting General Ledger Major Components..."
$results = get ($webApiBaseUrl + "/configuration/general-ledger") $token


# The endpoints used below are available only with Colleague Web API 1.15 and later.

# Add additional warm up requests for your custom APIs below.

# Logout
Write-Host "Logging out..."
post ($webApiBaseUrl + "/session/logout") "" $token
Write-Host "Done."
