﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Dtos.ProjectsAccounting;
using Ellucian.Rest.Client.Exceptions;
using Ellucian.Web.Utility;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Collections.Specialized;
using System;
using Ellucian.Colleague.Dtos.Base;
using System.Web;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Api.Client
{
    public partial class ColleagueApiClient
    {
        /// <summary>
        /// Gets a filtered list of projects for a user.
        /// </summary>
        /// <param name="summaryOnly">summaryOnly</param>
        /// <param name="filter">filter</param>
        /// <returns>A list of projects filtered according to the specified criteria.</returns>
        [Obsolete("Obsolete as of API 1.12. Use GetProjectsAsync.")]
        public IEnumerable<Project> GetProjects(bool summaryOnly = true, IEnumerable<UrlFilter> filter = null)
        {
            try
            {
                // Create and execute a request to get all projects
                string query = "summaryOnly=" + summaryOnly;
                string urlField = "";
                string urlOperator = "";
                string urlValue = "";

                if (filter != null)
                {
                    List<UrlFilter> filtersList = (List<UrlFilter>)filter;
                    for (int i = 0; i < filtersList.Count; i++)
                    {
                        // Get/encode the values in the filter object
                        urlField = HttpUtility.UrlEncode(filtersList[i].Field);
                        urlOperator = filtersList[i].Operator;
                        urlValue = HttpUtility.UrlEncode(filtersList[i].Value);

                        // Create the URL sections for the field, operator, and value
                        query += String.Format("&filter[{0}][field]={1}", i, urlField);
                        query += String.Format("&filter[{0}][operator]={1}", i, urlOperator);
                        query += String.Format("&filter[{0}][value]={1}", i, urlValue);
                    }
                }

                string urlPath = UrlUtility.CombineUrlPath(_projectsPath);
                urlPath = UrlUtility.CombineUrlPathAndArguments(urlPath, query);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = ExecuteGetRequestWithResponse(urlPath, headers: headers);

                var resource = JsonConvert.DeserializeObject<List<Project>>(response.Content.ReadAsStringAsync().Result);
                return resource;
            }
            // Log any exception, then rethrow it and let calling code determine how to handle it.
            catch (ResourceNotFoundException ex)
            {
                logger.Error(ex, "Unable to get projects.");
                throw;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to get projects.");
                throw;
            }
        }

        /// <summary>
        /// Gets a filtered list of projects for a user.
        /// </summary>
        /// <param name="summaryOnly">summaryOnly</param>
        /// <param name="filter">filter</param>
        /// <returns>A list of projects filtered according to the specified criteria.</returns>
        /// <exception cref="ResourceNotFoundException">The requested resource cannot be found.</exception>
        /// <exception cref="Exception">The requested resource cannot be found.</exception> 
        public async Task<IEnumerable<Project>> GetProjectsAsync(bool summaryOnly = true, IEnumerable<UrlFilter> filter = null)
        {
            try
            {
                // Create and execute a request to get all projects
                string query = "summaryOnly=" + summaryOnly;
                string urlField = "";
                string urlOperator = "";
                string urlValue = "";

                if (filter != null)
                {
                    List<UrlFilter> filtersList = (List<UrlFilter>)filter;
                    for (int i = 0; i < filtersList.Count; i++)
                    {
                        // Get/encode the values in the filter object
                        urlField = HttpUtility.UrlEncode(filtersList[i].Field);
                        urlOperator = filtersList[i].Operator;
                        urlValue = HttpUtility.UrlEncode(filtersList[i].Value);

                        // Create the URL sections for the field, operator, and value
                        query += String.Format("&filter[{0}][field]={1}", i, urlField);
                        query += String.Format("&filter[{0}][operator]={1}", i, urlOperator);
                        query += String.Format("&filter[{0}][value]={1}", i, urlValue);
                    }
                }

                string urlPath = UrlUtility.CombineUrlPath(_projectsPath);
                urlPath = UrlUtility.CombineUrlPathAndArguments(urlPath, query);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = ExecuteGetRequestWithResponse(urlPath, headers: headers);

                var resource = JsonConvert.DeserializeObject<List<Project>>(await response.Content.ReadAsStringAsync());
                return resource;
            }
            // Log any exception, then rethrow it and let calling code determine how to handle it.
            catch (ResourceNotFoundException ex)
            {
                logger.Error(ex, "Unable to get projects.");
                throw;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to get projects.");
                throw;
            }
        }

        /// <summary>
        /// Get all projects
        /// </summary>
        /// <returns>Returns all projects for the user</returns>
        /// <exception cref="ResourceNotFoundException">The requested resource cannot be found.</exception>
        [Obsolete("Obsolete as of API 1.6. Use GetProjects.")]
        public IEnumerable<Project> GetAllProjects()
        {
            try
            {
                // Create and execute a request to get all projects
                string urlPath = UrlUtility.CombineUrlPath(_projectsPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = ExecuteGetRequestWithResponse(urlPath, headers: headers);

                var resource = JsonConvert.DeserializeObject<List<Project>>(response.Content.ReadAsStringAsync().Result);
                return resource;
            }
            // Log any exception, then rethrow it and let calling code determine how to handle it.
            catch (ResourceNotFoundException ex)
            {
                logger.Error(ex, "Unable to get projects.");
                throw;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to get projects.");
                throw;
            }
        }

        /// <summary>
        /// Get all project types
        /// </summary>
        /// <returns>Returns all project types</returns>
        /// <exception cref="ResourceNotFoundException">The requested resource cannot be found.</exception>
        [Obsolete("Obsolete as of API 1.12. Use GetProjectTypesAsync.")]
        public IEnumerable<ProjectType> GetProjectTypes()
        {
            try
            {
                // Create and execute a request to get all project types
                string urlPath = UrlUtility.CombineUrlPath(_projectTypesPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = ExecuteGetRequestWithResponse(urlPath, headers: headers);

                var resource = JsonConvert.DeserializeObject<List<ProjectType>>(response.Content.ReadAsStringAsync().Result);
                return resource;
            }
            // Log any exception, then rethrow it and let calling code determine how to handle it.
            catch (ResourceNotFoundException ex)
            {
                logger.Error(ex, "Unable to get project Types.");
                throw;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to get project Types.");
                throw;
            }
        }

        /// <summary>
        /// Get all project types
        /// </summary>
        /// <returns>Returns all project types</returns>
        /// <exception cref="ResourceNotFoundException">The requested resource cannot be found.</exception>
        /// <exception cref="Exception">The requested resource cannot be found.</exception>
        public async Task<IEnumerable<ProjectType>> GetProjectTypesAsync()
        {
            try
            {
                // Create and execute a request to get all project types
                string urlPath = UrlUtility.CombineUrlPath(_projectTypesPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = ExecuteGetRequestWithResponse(urlPath, headers: headers);

                var resource = JsonConvert.DeserializeObject<List<ProjectType>>(await response.Content.ReadAsStringAsync());
                return resource;
            }
            // Log any exception, then rethrow it and let calling code determine how to handle it.
            catch (ResourceNotFoundException ex)
            {
                logger.Error(ex, "Unable to get project Types.");
                throw;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to get project Types.");
                throw;
            }
        }

        /// <summary>
        /// Get all project item codes
        /// </summary>
        /// <returns>Returns all project types</returns>
        /// <exception cref="ResourceNotFoundException">The requested resource cannot be found.</exception>
        [Obsolete("Obsolete as of API 1.12. Use GetProjectItemCodesAsync.")]
        public IEnumerable<ProjectItemCode> GetProjectItemCodes()
        {
            try
            {
                // Create and execute a request to get all project item codes.
                string urlPath = UrlUtility.CombineUrlPath(_projectItemCodesPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = ExecuteGetRequestWithResponse(urlPath, headers: headers);

                var resource = JsonConvert.DeserializeObject<List<ProjectItemCode>>(response.Content.ReadAsStringAsync().Result);
                return resource;
            }
            // Log any exception, then rethrow it and let calling code determine how to handle it.
            catch (ResourceNotFoundException ex)
            {
                logger.Error(ex, "Unable to get project item codes.");
                throw;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to get project item codes.");
                throw;
            }
        }

        /// <summary>
        /// Get all project item codes
        /// </summary>
        /// <returns>Returns all project types</returns>
        /// <exception cref="ResourceNotFoundException">The requested resource cannot be found.</exception>
        /// <exception cref="Exception">The requested resource cannot be found.</exception>
        public async Task<IEnumerable<ProjectItemCode>> GetProjectItemCodesAsync()
        {
            try
            {
                // Create and execute a request to get all project item codes.
                string urlPath = UrlUtility.CombineUrlPath(_projectItemCodesPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = ExecuteGetRequestWithResponse(urlPath, headers: headers);

                var resource = JsonConvert.DeserializeObject<List<ProjectItemCode>>(await response.Content.ReadAsStringAsync());
                return resource;
            }
            // Log any exception, then rethrow it and let calling code determine how to handle it.
            catch (ResourceNotFoundException ex)
            {
                logger.Error(ex, "Unable to get project item codes.");
                throw;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to get project item codes.");
                throw;
            }
        }

        /// <summary>
        /// Get a single project
        /// </summary>
        /// <returns>Returns a project.</returns>
        /// <exception cref="ResourceNotFoundException">The requested resource cannot be found.</exception>
        [Obsolete("Obsolete as of API 1.12. Use GetProjectAsync.")]
        public Project GetProject(string id, string sequenceNumber = null)
        {
            try
            {
                // Create and execute a request to get a specified project
                string query = "sequenceNumber=" + sequenceNumber;
                string[] pathStrings = new string[] { _projectsPath, id };
                string urlPath = UrlUtility.CombineUrlPath(pathStrings);
                urlPath = UrlUtility.CombineUrlPathAndArguments(urlPath, query);

                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = ExecuteGetRequestWithResponse(urlPath, headers: headers);

                var resource = JsonConvert.DeserializeObject<Project>(response.Content.ReadAsStringAsync().Result);
                return resource;
            }
            // Log any exception, then rethrow it and let calling code determine how to handle it.
            catch (ResourceNotFoundException ex)
            {
                logger.Error(ex, "Unable to get project {0}.", id);
                throw;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to get project.");
                throw;
            }
        }

        /// <summary>
        /// Get a single project
        /// </summary>
        /// <param name="id">Project ID</param>
        /// <param name="sequenceNumber">Project budget period sequence number</param>
        /// <returns>Returns a project</returns>
        /// <exception cref="ResourceNotFoundException">The requested resource cannot be found.</exception>
        /// <exception cref="Exception">The requested resource cannot be found.</exception>
        public async Task<Project> GetProjectAsync(string id, string sequenceNumber = null)
        {
            try
            {
                // Create and execute a request to get a specified project
                string query = "sequenceNumber=" + sequenceNumber;
                string[] pathStrings = new string[] { _projectsPath, id };
                string urlPath = UrlUtility.CombineUrlPath(pathStrings);
                urlPath = UrlUtility.CombineUrlPathAndArguments(urlPath, query);

                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = ExecuteGetRequestWithResponse(urlPath, headers: headers);

                var resource = JsonConvert.DeserializeObject<Project>(await response.Content.ReadAsStringAsync());
                return resource;
            }
            // Log any exception, then rethrow it and let calling code determine how to handle it.
            catch (ResourceNotFoundException ex)
            {
                logger.Error(ex, "Unable to get project {0}.", id);
                throw;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to get project.");
                throw;
            }
        }
    }
}
