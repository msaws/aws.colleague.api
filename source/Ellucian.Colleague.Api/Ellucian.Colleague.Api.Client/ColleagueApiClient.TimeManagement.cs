﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using Ellucian.Colleague.Api.Client.Exceptions;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Rest.Client.Exceptions;
using Ellucian.Web.Utility;
using Newtonsoft.Json;
using System.Threading.Tasks;
namespace Ellucian.Colleague.Api.Client
{
    public partial class ColleagueApiClient
    {
        [Obsolete("This method is obsolete as of API release 1.15; Please use GetTimecards2Async instead")]
        public async Task<IEnumerable<Timecard>> GetTimecardsAsync()
        {
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_timecardsPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                return JsonConvert.DeserializeObject<List<Timecard>>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to get time cards");
                logger.Error(e, message);
                throw;
            }
        }

        [Obsolete("This method is obsolete as of API release 1.15; Please use GetTimecardHistories2Async instead")]
        public async Task<IEnumerable<TimecardHistory>> GetTimecardHistoriesAsync(DateTime startDate, DateTime endDate)
        {
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_timecardHistoriesPath);
                var urlPathWithArguments = UrlUtility.CombineEncodedUrlPathAndArguments(
                    urlPath, 
                    new Dictionary<string, string>() 
                    { 
                        {
                            "startDate", startDate.ToShortDateString()
                        },
                        { 
                            "endDate", endDate.ToShortDateString() 
                        }
                    }
                );
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecuteGetRequestWithResponseAsync(urlPathWithArguments, headers: headers);
                return JsonConvert.DeserializeObject<List<TimecardHistory>>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to get time cards");
                logger.Error(e, message);
                throw;
            }
        }

        [Obsolete("This method is obsolete as of API release 1.15; Please use GetTimecard2Async instead")]
        public async Task<Timecard> GetTimecardAsync(string timecardId)
        {
            if (string.IsNullOrWhiteSpace(timecardId))
            {
                throw new ArgumentNullException("timecardId");
            }
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_timecardsPath, timecardId);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                return JsonConvert.DeserializeObject<Timecard>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to get time card");
                logger.Error(e, message);
                throw;
            }
        }

        [Obsolete("This method is obsolete as of API release 1.15; Please use CreateTimecards2Async instead")]
        public async Task<Timecard> CreateTimecardsAsync(Timecard timecard)
        {
            if (timecard == null)
            {
                throw new ArgumentNullException("timecard");
            }
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_timecardsPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecutePostRequestWithResponseAsync(timecard, urlPath, headers: headers);
                return JsonConvert.DeserializeObject<Timecard>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to create time card");
                logger.Error(e, message);
                throw;
            }
        }

        [Obsolete("This method is obsolete as of API release 1.15; Please use UpdateTimecard2Async instead")]
        public async Task<Timecard> UpdateTimecardsAsync(Timecard timecard)
        {
            if (timecard == null)
            {
                throw new ArgumentNullException("timecard");
            }
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_timecardsPath, timecard.Id);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecutePutRequestWithResponseAsync(timecard, urlPath, headers: headers);
                return JsonConvert.DeserializeObject<Timecard>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to update time cards");
                logger.Error(e, message);
                throw;
            }
        }

        public async Task<IEnumerable<Timecard2>> GetTimecards2Async()
        {
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_timecardsPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion2);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                return JsonConvert.DeserializeObject<List<Timecard2>>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to get time cards");
                logger.Error(e, message);
                throw;
            }
        }

        public async Task<IEnumerable<TimecardHistory2>> GetTimecardHistories2Async(DateTime startDate, DateTime endDate)
        {
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_timecardHistoriesPath);
                var urlPathWithArguments = UrlUtility.CombineEncodedUrlPathAndArguments(
                    urlPath,
                    new Dictionary<string, string>() 
                    { 
                        {
                            "startDate", startDate.ToShortDateString()
                        },
                        { 
                            "endDate", endDate.ToShortDateString() 
                        }
                    }
                );
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion2);
                var response = await ExecuteGetRequestWithResponseAsync(urlPathWithArguments, headers: headers);
                return JsonConvert.DeserializeObject<List<TimecardHistory2>>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to get time cards");
                logger.Error(e, message);
                throw;
            }
        }

        public async Task<Timecard2> GetTimecard2Async(string timecardId)
        {
            if (string.IsNullOrWhiteSpace(timecardId))
            {
                throw new ArgumentNullException("timecardId");
            }
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_timecardsPath, timecardId);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion2);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                return JsonConvert.DeserializeObject<Timecard2>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to get time card");
                logger.Error(e, message);
                throw;
            }
        }

        public async Task<Timecard2> CreateTimecards2Async(Timecard2 timecard2)
        {
            if (timecard2 == null)
            {
                throw new ArgumentNullException("timecard");
            }
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_timecardsPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion2);
                var response = await ExecutePostRequestWithResponseAsync(timecard2, urlPath, headers: headers);
                return JsonConvert.DeserializeObject<Timecard2>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to create time card");
                logger.Error(e, message);
                throw;
            }
        }

        public async Task<Timecard2> UpdateTimecard2Async(Timecard2 timecard2)
        {
            if (timecard2 == null)
            {
                throw new ArgumentNullException("timecard");
            }
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_timecardsPath, timecard2.Id);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion2);
                var response = await ExecutePutRequestWithResponseAsync(timecard2, urlPath, headers: headers);
                return JsonConvert.DeserializeObject<Timecard2>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to update time cards");
                logger.Error(e, message);
                throw;
            }
        }

        public async Task<TimecardStatus> CreateTimecardStatusAsync(TimecardStatus status)
        {
            if (status == null)
            {
                throw new ArgumentNullException("status");
            }
            try
            {
                string timecardId = status.TimecardId;
                string urlPath = UrlUtility.CombineUrlPath(_timecardsPath, timecardId, _timecardStatusesPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecutePostRequestWithResponseAsync(status, urlPath, headers: headers);
                return JsonConvert.DeserializeObject<TimecardStatus>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to update timecard status");
                logger.Error(e, message);
                throw;
            }
        }

        /// <summary>
        /// Create multiple Timecard Status records.
        /// </summary>
        /// <param name="statuses">List of TimecardStatus items to create</param>
        /// <returns>List of created TimecardStatus items</returns>
        public async Task<IEnumerable<TimecardStatus>> CreateTimecardStatusesAsync(List<TimecardStatus> statuses)
        {
            if (statuses == null || statuses.Count() == 0)
            {
                throw new ArgumentNullException("statuses");
            }
            try
            {
                string urlPath = _timecardStatusesPath;
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecutePostRequestWithResponseAsync(statuses, urlPath, headers: headers);
                return JsonConvert.DeserializeObject<IEnumerable<TimecardStatus>>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to update timecard statuses");
                logger.Error(e, message);
                throw;
            }
        }

        public async Task<IEnumerable<TimecardStatus>> GetTimecardStatusesByTimecardIdAsync(string timecardId)
        {
            if (string.IsNullOrWhiteSpace(timecardId))
            {
                throw new ArgumentNullException("timecardId");
            }
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_timecardsPath, timecardId, _timecardStatusesPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                return JsonConvert.DeserializeObject<IEnumerable<TimecardStatus>>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to get timecard statuses");
                logger.Error(e, message);
                throw;
            }
        }

        public async Task<IEnumerable<TimecardStatus>> GetLatestTimecardStatuses()
        {
            try
            {
                string urlPath = _timecardStatusesPath;
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                return JsonConvert.DeserializeObject<IEnumerable<TimecardStatus>>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to get timecard statuses");
                logger.Error(e, message);
                throw;
            }
        }

        public async Task<OvertimeCalculationResult> CalculateOvertime(OvertimeQueryCriteria criteria)
        {
            if (criteria == null)
            {
                throw new ArgumentNullException("criteria");
            }
            if (string.IsNullOrWhiteSpace(criteria.PersonId))
            {
                throw new ArgumentException("PersonId required attr", "criteria");
            }
            if (string.IsNullOrWhiteSpace(criteria.PayCycleId))
            {
                throw new ArgumentException("PayCycleId required attr", "criteria");
            }
            if (criteria.StartDate > criteria.EndDate)
            {
                throw new ArgumentException("start date must be before end date");
            }
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_qapiPath, _overtimePath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);

                var response = await ExecutePostRequestWithResponseAsync(criteria, urlPath, headers: headers);
                return JsonConvert.DeserializeObject<OvertimeCalculationResult>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to calculate overtime");
                logger.Error(e, message);
                throw;
            }

        }

        public async Task<List<OvertimeCalculationDefinition>> GetOvertimeCalculationDefinitionsAsync()
        {
            try
            {
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecuteGetRequestWithResponseAsync(_overtimeCalculationDefinitionsPath, headers: headers);
                return JsonConvert.DeserializeObject<List<OvertimeCalculationDefinition>>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to get overtime calculation definitions");
                logger.Error(e, message);
                throw;
            }
        }

        public async Task<IEnumerable<TimeEntryComments>> GetTimeEntryCommentsAsync()
        {
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_timeEntryCommentsPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                return JsonConvert.DeserializeObject<List<TimeEntryComments>>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to get time entry comments");
                logger.Error(e, message);
                throw;
            }
        }

        public async Task<TimeEntryComments> CreateTimeEntryCommentsAsync(TimeEntryComments comments)
        {
            if (comments == null)
                throw new ArgumentNullException("comments");

            if (string.IsNullOrWhiteSpace(comments.EmployeeId))
                throw new ArgumentNullException("employeeId");

            if (string.IsNullOrWhiteSpace(comments.TimecardId) &&
                string.IsNullOrWhiteSpace(comments.TimecardStatusId) &&
                string.IsNullOrWhiteSpace(comments.PayCycleId))
                throw new ArgumentException("Comments must be applied to an associable entity");

            if (!string.IsNullOrWhiteSpace(comments.PayCycleId) && comments.PayPeriodEndDate == null)
                throw new ArgumentException("payPeriodEndDate must have value if there is a payCycleId");

            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_timeEntryCommentsPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecutePostRequestWithResponseAsync(comments, urlPath, headers: headers);
                return JsonConvert.DeserializeObject<TimeEntryComments>(await response.Content.ReadAsStringAsync());
            }
            catch (Exception e)
            {
                var message = string.Format("{0}", "unable to create time entry comments");
                logger.Error(e, message);
                throw;
            }
        }
    }
}
