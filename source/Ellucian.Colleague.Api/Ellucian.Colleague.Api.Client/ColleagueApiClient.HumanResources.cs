﻿/*Copyright 2015-2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.HumanResources;
using Ellucian.Web.Utility;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Api.Client
{
    public partial class ColleagueApiClient
    {

        /// <summary>
        /// Obsolete as of API 1.16 for security reasons. Use GetPayrollDepositDirectives instead
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        [Obsolete("Obsolete as of API 1.16.")]
        public Task<DirectDeposits> GetDirectDepositsAsync(string employeeId)
        {
            throw new InvalidOperationException("GetDirectDepositsAsync method is obsolete as of API 1.16. Use GetPayrollDepositDirectives instead.");
        }

        /// <summary>
        /// Obsolete as of API 1.16 for security reasons. Use UpdatePayrollDepositDirectives instead
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns></returns>
        [Obsolete("Obsolete as of API 1.16.")]
        public Task<DirectDeposits> UpdateDirectDepositsAsync(string employeeId, DirectDeposits directDeposits)
        {
            throw new InvalidOperationException("UpdateDirectDepositsAsync method is obsolete as of API 1.16. Use UpdatePayrollDepositDirectives instead.");
        }


        /// <summary>
        /// Get PersonStatus data based on the permissions of the current user.      
        /// </summary>
        /// <example>SelfService getting person-Statuses on behalf of an employee will return that employee's PersonStatuses</example>
        /// <example>SelfService getting person-Statuses on behalf of a supervisor will return that supervisor's PersonStatuses and all the PersonStatuses of the supervisors reports</example>
        /// <returns></returns>
        public async Task<IEnumerable<PersonEmploymentStatus>> GetPersonEmploymentStatusesAsync()
        {
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_personEmploymentStatusesPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                var resource = JsonConvert.DeserializeObject<IEnumerable<PersonEmploymentStatus>>(await response.Content.ReadAsStringAsync());
                return resource;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Unable to get PersonEmploymentStatus data");
                throw;
            }
        }

        /// <summary>
        /// Get PersonPosition data based on the permissions of the current user.      
        /// </summary>
        /// <example>SelfService getting person-positions on behalf of an employee will return that employee's PersonPositions</example>
        /// <example>SelfService getting person-positions on behalf of a supervisor will return that supervisor's PersonPositions and all the PersonPositions of the supervisors reports</example>
        /// <returns></returns>
        public async Task<IEnumerable<PersonPosition>> GetPersonPositionsAsync()
        {
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_personPositionsPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                var resource = JsonConvert.DeserializeObject<IEnumerable<PersonPosition>>(await response.Content.ReadAsStringAsync());
                return resource;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Unable to get PersonPosition data");
                throw;
            }
        }

        /// <summary>
        /// Get PersonPosition data based on the permissions of the current user.
        /// </summary>
        /// <example>SelfService getting person-position-wages on behalf of an employee will return that employee's PersonPositionWages</example>
        /// <example>SelfService getting person-position-wages on behalf of a supervisor will return that supervisor's PersonPositionWages and all the PersonPositionWages of the supervisors reports</example>      
        /// <returns></returns>
        public async Task<IEnumerable<PersonPositionWage>> GetPersonPositionWagesAsync()
        {
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_personPositionWagesPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                var resource = JsonConvert.DeserializeObject<IEnumerable<PersonPositionWage>>(await response.Content.ReadAsStringAsync());
                return resource;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Unable to get PersonPositionWage data");
                throw;
            }
        }

        /// <summary>
        /// Get all the employment Positions
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<Position>> GetPositionsAsync()
        {
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_positionsPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                var resource = JsonConvert.DeserializeObject<IEnumerable<Position>>(await response.Content.ReadAsStringAsync());
                return resource;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Unable to get Position data");
                throw;
            }
        }

        /// <summary>
        /// Get all EarningsType data
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<EarningsType>> GetEarningsTypesAsync()
        {
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_earningsTypesPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                var resource = JsonConvert.DeserializeObject<IEnumerable<EarningsType>>(await response.Content.ReadAsStringAsync());
                return resource;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Unable to get EarningsType data");
                throw;
            }
        }


        /// <summary>
        /// Get all HumanResourceDemographics data
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<HumanResourceDemographics>> GetHumanResourceDemographicsAsync()
        {
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_humanResourceDemographicsPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderHumanResourceDemographics);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                var resource = JsonConvert.DeserializeObject<IEnumerable<HumanResourceDemographics>>(await response.Content.ReadAsStringAsync());
                return resource;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Unable to get HumanResourceDemographics data");
                throw;
            }
        }

        /// <summary>
        /// Get all HumanResourceDemographics data
        /// </summary>
        /// <returns></returns>
        public async Task<HumanResourceDemographics> GetSpecificHumanResourceDemographicsAsync(string id)
        {
            try
            {
                string urlPath = UrlUtility.CombineUrlPath(_humanResourceDemographicsPath, id);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderHumanResourceDemographics);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                var resource = JsonConvert.DeserializeObject<HumanResourceDemographics>(await response.Content.ReadAsStringAsync());
                return resource;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Unable to get HumanResourceDemographics data");
                throw;
            }
        }

        /// <summary>
        /// Get PayCycle data
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<PayCycle>> GetPayCyclesAsync()
        {
            try
            {
                // Build url path and create and execute a request to get all pay cycles
                string urlPath = UrlUtility.CombineUrlPath(_payCyclesPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                var resource = JsonConvert.DeserializeObject<IEnumerable<PayCycle>>(await response.Content.ReadAsStringAsync());
                return resource;
            }
            catch (Exception ex)
            {
                logger.Error(ex, "Unable to get PayCycle data");
                throw;
            }
        }

        /// <summary>
        /// Retrieve a set of statement DTOs from the human resources area.
        /// </summary>
        /// <param name="personId">Person ID</param>
        /// <param name="taxForm">Type of tax form</param>
        /// <returns>Set of tax form statements</returns>
        [Obsolete("Obsolete as of API version 1.14, use GetTaxFormStatements2 instead")]
        public async Task<IEnumerable<TaxFormStatement>> GetTaxFormStatements(string personId, TaxForms taxForm)
        {
            if (string.IsNullOrEmpty(personId))
                throw new ArgumentNullException("personId", "personId is required.");

            try
            {
                // Create and execute a request to get all projects
                string urlPath = UrlUtility.CombineUrlPath(_taxFormStatementsPath, personId, taxForm.ToString());
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);

                var resource = JsonConvert.DeserializeObject<List<TaxFormStatement>>(await response.Content.ReadAsStringAsync());
                return resource;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to get tax form statements.");
                throw;
            }
        }

        /// <summary>
        /// Get a W-2 tax form PDF
        /// </summary>
        /// <param name="personId">ID of the person assigned to and requesting the W-2.</param>
        /// <param name="recordId">The record ID where the W-2 pdf data is stored</param>
        /// <returns>Byte array containing PDF data</returns>
        public async Task<byte[]> GetW2TaxFormPdf(string personId, string recordId)
        {
            if (string.IsNullOrEmpty(personId))
                throw new ArgumentNullException("personId", "personId cannot be null or empty.");

            if (string.IsNullOrEmpty(recordId))
                throw new ArgumentNullException("id", "Record ID cannot be null or empty.");

            try
            {
                // Build url path and create and execute a request to get the tax form pdf
                var urlPath = UrlUtility.CombineUrlPath(_personsPath, personId, _taxFormW2PdfPath, recordId);

                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                headers.Add(AcceptHeaderKey, "application/pdf");
                headers.Add(AcceptHeaderKey, "application/vnd.ellucian.v1+pdf");
                headers.Add("X-Ellucian-Media-Type", "application/vnd.ellucian.v1+pdf");
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);

                var resource = response.Content.ReadAsByteArrayAsync().Result;
                return resource;
            }
            catch (Exception ex)
            {
                logger.Error(ex.GetBaseException(), "Unable to retrieve W-2 tax form pdf.");
                throw;
            }
        }

        /// <summary>
        /// Get a 1095-C tax form PDF
        /// </summary>
        /// <param name="personId">ID of the person assigned to and requesting the 1095-C.</param>
        /// <param name="recordId">The record ID where the 1095-C pdf data is stored</param>
        /// <returns>Byte array containing PDF data</returns>
        public async Task<byte[]> Get1095cTaxFormPdf(string personId, string recordId)
        {
            if (string.IsNullOrEmpty(personId))
                throw new ArgumentNullException("personId", "PersonId ID cannot be null or empty.");

            if (string.IsNullOrEmpty(recordId))
                throw new ArgumentNullException("recordId", "Record ID cannot be null or empty.");

            try
            {
                // Build url path and create and execute a request to get the tax form pdf
                var urlPath = UrlUtility.CombineUrlPath(_personsPath, personId, _taxForm1095cPdfPath, recordId);

                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                headers.Add(AcceptHeaderKey, "application/pdf");
                headers.Add(AcceptHeaderKey, "application/vnd.ellucian.v1+pdf");
                headers.Add("X-Ellucian-Media-Type", "application/vnd.ellucian.v1+pdf");
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);

                var resource = response.Content.ReadAsByteArrayAsync().Result;
                return resource;
            }
            catch (Exception ex)
            {
                logger.Error(ex.GetBaseException(), "Unable to retrieve 1095-C tax form pdf.");
                throw;
            }
        }


        /// <summary>
        /// Get all PayrollDepositDirectives owned by the Current User
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<PayrollDepositDirective>> GetPayrollDepositDirectivesAsync()
        {
            try
            {
                var urlPath = UrlUtility.CombineUrlPath(_payrollDepositDirectivesPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                var resource = JsonConvert.DeserializeObject<IEnumerable<PayrollDepositDirective>>(await response.Content.ReadAsStringAsync());

                return resource;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to get PayrollDepositDirectives");
                throw;
            }
        }

        /// <summary>
        /// Get a single PayrollDepositDirective
        /// </summary>
        /// <param name="payrollDepositDirectiveId"></param>
        /// <returns></returns>
        public async Task<PayrollDepositDirective> GetPayrollDepositDirectiveAsync(string payrollDepositDirectiveId)
        {
            if (string.IsNullOrEmpty(payrollDepositDirectiveId))
            {
                throw new ArgumentNullException("payrollDepositDirectiveId");
            }
            try
            {
                var urlPath = UrlUtility.CombineUrlPath(_payrollDepositDirectivesPath, payrollDepositDirectiveId);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                var response = await ExecuteGetRequestWithResponseAsync(urlPath, headers: headers);
                var resource = JsonConvert.DeserializeObject<PayrollDepositDirective>(await response.Content.ReadAsStringAsync());

                return resource;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to get PayrollDepositDirective");
                throw;
            }
        }


        /// <summary>
        /// Update a single PayrollDepositDirective. BankingAuthenticationToken is required
        /// </summary>
        /// <param name="payrollDepositDirective"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<PayrollDepositDirective> UpdatePayrollDepositDirectiveAsync(PayrollDepositDirective payrollDepositDirective, BankingAuthenticationToken token)
        {
            if (payrollDepositDirective == null)
            {
                throw new ArgumentNullException("payrollDepositDirective");
            }
            if (string.IsNullOrEmpty(payrollDepositDirective.Id))
            {
                throw new ArgumentException("Id of directive must be specified", "payrollDepositDirective");
            }
            if (token == null)
            {
                throw new ArgumentNullException("token");
            }
            if (token.ExpirationDateTimeOffset < DateTimeOffset.Now)
            {
                throw new ArgumentException("token is expired", "token");
            }
            try
            {
                var urlPath = UrlUtility.CombineUrlPath(_payrollDepositDirectivesPath, payrollDepositDirective.Id);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                headers.Add(StepUpAuthenticationHeaderKey, token.Token.ToString());
                var response = await ExecutePutRequestWithResponseAsync<PayrollDepositDirective>(payrollDepositDirective, urlPath, headers: headers);
                var resource = JsonConvert.DeserializeObject<PayrollDepositDirective>(await response.Content.ReadAsStringAsync());

                return resource;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to update PayrollDepositDirectives");
                throw;
            }
        }

        /// <summary>
        /// Batch update a list of PayrollDepositDirectives. BankingAuthenticationToken is required
        /// </summary>
        /// <param name="payrollDepositDirectives"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<IEnumerable<PayrollDepositDirective>> UpdatePayrollDepositDirectivesAsync(IEnumerable<PayrollDepositDirective> payrollDepositDirectives, BankingAuthenticationToken token)
        {
            if (payrollDepositDirectives == null || !payrollDepositDirectives.Any())
            {
                throw new ArgumentNullException("payrollDepositDirectives");
            }
            if (token == null)
            {
                throw new ArgumentNullException("token");
            }
            if (token.ExpirationDateTimeOffset < DateTimeOffset.Now)
            {
                throw new ArgumentException("token is expired", "token");
            }
            try
            {
                var urlPath = UrlUtility.CombineUrlPath(_payrollDepositDirectivesPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                headers.Add(StepUpAuthenticationHeaderKey, token.Token.ToString());
                var response = await ExecutePutRequestWithResponseAsync<IEnumerable<PayrollDepositDirective>>(payrollDepositDirectives, urlPath, headers: headers);
                var resource = JsonConvert.DeserializeObject<IEnumerable<PayrollDepositDirective>>(await response.Content.ReadAsStringAsync());

                return resource;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to update PayrollDepositDirectives");
                throw;
            }
        }

        /// <summary>
        /// Create a PayrollDepositDirective. BankingAuthenticationToken is required
        /// </summary>
        /// <param name="payrollDepositDirective"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task<PayrollDepositDirective> CreatePayrollDepositDirectiveAsync(PayrollDepositDirective payrollDepositDirective, BankingAuthenticationToken token)
        {
            if (payrollDepositDirective == null)
            {
                throw new ArgumentNullException("payrollDepositDirective");
            }
            if (token == null)
            {
                throw new ArgumentNullException("token");
            }
            if (token.ExpirationDateTimeOffset < DateTimeOffset.Now)
            {
                throw new ArgumentException("token is expired", "token");
            }
            try
            {
                var urlPath = UrlUtility.CombineUrlPath(_payrollDepositDirectivesPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                headers.Add(StepUpAuthenticationHeaderKey, token.Token.ToString());
                var response = await ExecutePostRequestWithResponseAsync<PayrollDepositDirective>(payrollDepositDirective, urlPath, headers: headers);
                var resource = JsonConvert.DeserializeObject<PayrollDepositDirective>(await response.Content.ReadAsStringAsync());

                return resource;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to create PayrollDepositDirective");
                throw;
            }
        }

        /// <summary>
        /// Delete a PayrollDepositDirect. Requires a BankingAuthenticationToken
        /// </summary>
        /// <param name="id"></param>
        /// <param name="token"></param>
        /// <returns></returns>
        public async Task DeletePayrollDepositDirectiveAsync(string id, BankingAuthenticationToken token)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }
            if (token == null)
            {
                throw new ArgumentNullException("token");
            }
            if (token.ExpirationDateTimeOffset < DateTimeOffset.Now)
            {
                throw new ArgumentException("token is expired", "token");
            }
            try
            {
                var urlPath = UrlUtility.CombineUrlPath(_payrollDepositDirectivesPath, id);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeHeaderVersion1);
                headers.Add(StepUpAuthenticationHeaderKey, token.Token.ToString());
                await ExecuteDeleteRequestWithResponseAsync(urlPath, headers: headers);

                return;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to delete PayrollDepositDirectives");
                throw;
            }
        }

        /// <summary>
        /// Get a BankingAuthenticationToken to update a specific PayrollDepositDirective
        /// </summary>
        /// <param name="id"></param>
        /// <param name="authenticationValue"></param>
        /// <returns></returns>
        public async Task<BankingAuthenticationToken> AuthenticatePayrollDepositDirectiveAsync(string id, string authenticationValue)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ArgumentNullException("id");
            }
            if (string.IsNullOrEmpty(authenticationValue))
            {
                throw new ArgumentNullException("authenticationValue");
            }

            try
            {
                var urlPath = UrlUtility.CombineUrlPath(_payrollDepositDirectivesPath, id);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeStepUpAuthenticationVersion1);

                var response = await ExecutePostRequestWithResponseAsync<string>(authenticationValue, urlPath, headers: headers);
                var resource = JsonConvert.DeserializeObject<BankingAuthenticationToken>(await response.Content.ReadAsStringAsync());

                return resource;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to get BankingAuthenticationToken for PayrollDepositDirective");
                throw;
            }
        }

        /// <summary>
        /// Get a BankingAuthenticationToken for an employee with no payroll depositDirectives
        /// </summary>
        /// <returns></returns>
        public async Task<BankingAuthenticationToken> AuthenticatePayrollDepositDirectiveAsync()
        {
            try
            {
                var urlPath = UrlUtility.CombineUrlPath(_payrollDepositDirectivesPath);
                var headers = new NameValueCollection();
                headers.Add(AcceptHeaderKey, _mediaTypeStepUpAuthenticationVersion1);

                var response = await ExecutePostRequestWithResponseAsync<string>(null, urlPath, headers: headers);
                var resource = JsonConvert.DeserializeObject<BankingAuthenticationToken>(await response.Content.ReadAsStringAsync());

                return resource;
            }
            catch (Exception e)
            {
                logger.Error(e, "Unable to get BankingAuthenticationToken for PayrollDepositDirective");
                throw;
            }
        }


    }
}
