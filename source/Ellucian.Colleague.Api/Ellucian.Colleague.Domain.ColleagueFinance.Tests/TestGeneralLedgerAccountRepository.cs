﻿// Copyright 2017 Ellucian Company L.P. and its affiliates.

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ColleagueFinance.Repositories;

namespace Ellucian.Colleague.Domain.ColleagueFinance.Tests
{
    public class TestGeneralLedgerAccountRepository : IGeneralLedgerAccountRepository
    {
        public TestGeneralLedgerAccountRepository()
        {

        }

        /// <summary>
        /// Retrieves a set of general ledger accounts.
        /// </summary>
        /// <param name="generalLedgerAccountIds">Set of GL account ID.</param>
        /// <param name="majorComponentStartPositions">List of positions used to format the GL account ID.</param>
        /// <returns>Set of general ledger account domain entities.</returns>
        public async Task<Dictionary<string, string>> GetGlAccountDescriptionsAsync(IEnumerable<string> generalLedgerAccountIds, GeneralLedgerAccountStructure glAccountStructure)
        {
            var glAccountsList = new Dictionary<string, string>();

            for (int i = 0; i < generalLedgerAccountIds.Count(); i++)
            {
                glAccountsList.Add(generalLedgerAccountIds.ElementAt(i), "Description " + i.ToString());
            }

            return await Task.Run(() => { return glAccountsList; });
        }

        /// <summary>
        /// Retrieves a single general ledger account.
        /// </summary>
        /// <param name="generalLedgerAccountId">GL account ID.</param>
        /// <returns>General ledger account domain entity.</returns>
        public async Task<GeneralLedgerAccount> GetAsync(string generalLedgerAccountId, IEnumerable<string> majorComponentStartPositions)
        {
            return await Task.Run(() => { return new GeneralLedgerAccount(generalLedgerAccountId, majorComponentStartPositions) { Description = "Operating fund : General" }; });
        }
    }
}