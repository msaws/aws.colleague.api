﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Ellucian.Colleague.Domain.ColleagueFinance.Tests.Builders;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Colleague.Domain.ColleagueFinance.Tests.Entities
{
    [TestClass]
    public class GeneralLedgerAccountStructureTests
    {
        #region Initialize and Cleanup
        GeneralLedgerAccountStructureBuilder AccountStructureBuilder;
        GeneralLedgerComponentBuilder ComponentBuilder;

        [TestInitialize]
        public void Initialize()
        {
            AccountStructureBuilder = new GeneralLedgerAccountStructureBuilder();
            ComponentBuilder = new GeneralLedgerComponentBuilder();
        }

        [TestCleanup]
        public void Cleanup()
        {
            AccountStructureBuilder = null;
            ComponentBuilder = null;
        }
        #endregion

        #region Constructor tests

        [TestMethod]
        public void Constructor_Success()
        {
            var actualEntity = AccountStructureBuilder.Build();
            Assert.IsTrue(actualEntity is GeneralLedgerAccountStructure);
            Assert.IsTrue(actualEntity.MajorComponents is ReadOnlyCollection<GeneralLedgerComponent>);
            Assert.IsTrue(actualEntity.MajorComponentStartPositions is ReadOnlyCollection<string>);
            Assert.IsTrue(actualEntity.MajorComponents.Count == 0);
            Assert.IsTrue(actualEntity.MajorComponentStartPositions.Count() == 0, "MajorConponentStartPosition count should be zero.");
        }

        #endregion

        #region AddMajorComponent tests

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void AddMajorComponent_NullArgument()
        {
            var glAccountStructure = AccountStructureBuilder.Build();
            glAccountStructure.AddMajorComponent(null);
        }

        [TestMethod]
        public void AddMajorComponent_AddOne()
        {
            var glAccountStructure = AccountStructureBuilder.Build();
            var glComponent = ComponentBuilder.Build();
            glAccountStructure.AddMajorComponent(glComponent);
            Assert.AreEqual(1, glAccountStructure.MajorComponents.Count);
        }

        [TestMethod]
        public void AddMajorComponent_AddTwoDifferentGlComponents()
        {
            var glAccountStructure = AccountStructureBuilder.Build();
            var glComponent1 = ComponentBuilder.WithComponent("PROGRAM").Build();
            var glComponent2 = ComponentBuilder.WithComponent("OBJECT").Build();

            glAccountStructure.AddMajorComponent(glComponent1);
            Assert.AreEqual(1, glAccountStructure.MajorComponents.Count);

            glAccountStructure.AddMajorComponent(glComponent2);
            Assert.AreEqual(2, glAccountStructure.MajorComponents.Count);
        }

        [TestMethod]
        public void AddMajorComponent_AddDuplicateGlComponent()
        {
            var glAccountStructure = AccountStructureBuilder.Build();
            var glComponent = ComponentBuilder.Build();

            glAccountStructure.AddMajorComponent(glComponent);
            Assert.AreEqual(1, glAccountStructure.MajorComponents.Count);

            glAccountStructure.AddMajorComponent(glComponent);
            Assert.AreEqual(1, glAccountStructure.MajorComponents.Count);
        }

        #endregion

        #region Tests for SetMajorComponentStartPositions

        [TestMethod]
        public void SetMajorComponentStartPositions_Success()
        {
            var startPositions = new List<string>() { "3", "5", "7", "12", "16" };
            var actualEntity = AccountStructureBuilder.Build();
            actualEntity.SetMajorComponentStartPositions(startPositions);
            Assert.AreEqual(startPositions.Count(), actualEntity.MajorComponentStartPositions.Count(), "List counts should match.");

            for (var i = 0; i < startPositions.Count(); i++)
            {
                var seedPosition = startPositions[i];
                Assert.AreEqual(startPositions[i], actualEntity.MajorComponentStartPositions[i], "Start positions should match.");
            }
        }

        [TestMethod]
        public void SetMajorComponentStartPositions_CallTwice()
        {
            // Set the start positions and confirm that everything was added correctly.
            var startPositions1 = new List<string>() { "3", "5", "7", "12", "16" };
            var actualEntity = AccountStructureBuilder.Build();
            actualEntity.SetMajorComponentStartPositions(startPositions1);
            Assert.AreEqual(startPositions1.Count(), actualEntity.MajorComponentStartPositions.Count(), "List counts should match.");

            for (var i = 0; i < startPositions1.Count(); i++)
            {
                var seedPosition = startPositions1[i];
                Assert.AreEqual(startPositions1[i], actualEntity.MajorComponentStartPositions[i], "Start positions should match.");
            }

            // Set the start positions with a new list and confirm that the old information was discarded.
            var startPositions2 = new List<string>() { "2", "6", "9", "15" };
            actualEntity.SetMajorComponentStartPositions(startPositions2);
            Assert.AreEqual(startPositions2.Count(), actualEntity.MajorComponentStartPositions.Count(), "List counts should match.");

            for (var i = 0; i < startPositions2.Count(); i++)
            {
                var seedPosition = startPositions1[i];
                Assert.AreEqual(startPositions2[i], actualEntity.MajorComponentStartPositions[i], "Start positions should match.");
            }
        }

        #endregion
    }
}