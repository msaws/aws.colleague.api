﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using System;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Domain.ColleagueFinance.Tests.Builders;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Colleague.Domain.ColleagueFinance.Tests.Entities
{
    [TestClass]
    public class GeneralLedgerFiscalYearConfigurationTests
    {
        #region Initialize and Cleanup
        private GeneralLedgerFiscalYearConfigurationBuilder Builder;

        [TestInitialize]
        public void Initialize()
        {
            Builder = new GeneralLedgerFiscalYearConfigurationBuilder();
        }

        [TestCleanup]
        public void Cleanup()
        {
            Builder = null;
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        [ExpectedException(typeof(ConfigurationException))]
        public void Constructor_StartMonthOutOfRange_0()
        {
            Builder.WithStartMonth(0).Build();
        }

        [TestMethod]
        [ExpectedException(typeof(ConfigurationException))]
        public void Constructor_StartMonthOutOfRange_13()
        {
            Builder.WithStartMonth(13).Build();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullCurrentYear()
        {
            Builder.WithCurrentYear(null).Build();
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_EmptyCurrentYear()
        {
            Builder.WithCurrentYear("").Build();
        }

        [TestMethod]
        public void Constructor_Success()
        {
            var actualEntity = Builder.Build();
            Assert.AreEqual(Builder.StartMonth, actualEntity.StartMonth);
            Assert.AreEqual(Builder.CurrentYear, actualEntity.CurrentFiscalYear);
        }
        #endregion

        #region FiscalYearForToday tests
        [TestMethod]
        public void FiscalYearForToday_CurrentMonthIsBeforeStartMonth()
        {
            // Set the FY start month in a future month
            var startMonth = DateTime.Now.Add(new TimeSpan(32, 0, 0, 0)).Month;
            var expectedYear = DateTime.Now.Year;

            if (DateTime.Now.Month >= startMonth)
                expectedYear += 1;

            var configuration = Builder.WithStartMonth(startMonth).Build();
            var actualYear = configuration.FiscalYearForToday;
            Assert.AreEqual(expectedYear, actualYear);
        }

        [TestMethod]
        public void FiscalYearForToday_CurrentMonthSameAsStartMonth()
        {
            // Set the FY start month as today
            var startMonth = DateTime.Now.Month;
            var expectedYear = DateTime.Now.Year;

            if (DateTime.Now.Month >= startMonth)
                expectedYear += 1;

            var configuration = Builder.WithStartMonth(startMonth).Build();
            var actualYear = configuration.FiscalYearForToday;
            Assert.AreEqual(expectedYear, actualYear);
        }

        [TestMethod]
        public void FiscalYearForToday_CurrentMonthIsAfterStartMonth()
        {
            // Set the FY start month as today
            var startMonth = DateTime.Now.Subtract(new TimeSpan(32, 0, 0, 0)).Month;
            var expectedYear = DateTime.Now.Year;

            if (DateTime.Now.Month >= startMonth)
                expectedYear += 1;

            var configuration = Builder.WithStartMonth(startMonth).Build();
            var actualYear = configuration.FiscalYearForToday;
            Assert.AreEqual(expectedYear, actualYear);
        }
        #endregion
    }
}