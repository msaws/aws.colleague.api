﻿// Copyright 2015 Ellucian Company L.P. and its affiliates.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ellucian.Colleague.Domain.ColleagueFinance.Entities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Ellucian.Colleague.Domain.ColleagueFinance.Tests.Entities
{
    /// <summary>
    /// This class tests the valid and invalid conditions of an Approver domain entity. The
    /// Approver domain entity requires an ID.
    /// </summary>
    [TestClass]
    public class ApproverTests
    {
        #region Initialize and Cleanup
        [TestInitialize]
        public void Initialize()
        {
            
        }

        [TestCleanup]
        public void Cleanup()
        {
            
        }
        #endregion

        #region Constructor tests
        [TestMethod]
        public void Approver_Base()
        {
            string approverId = "GTT";
            var approver = new Approver(approverId);

            Assert.AreEqual(approverId, approver.ApproverId);
            Assert.IsNull(approver.ApprovalDate);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Approver_NullApproverId()
        {
            var approver = new Approver(null);
        }
        #endregion

        #region SetApproverName method tests
        [TestMethod]
        public void SetApproverName_NullName()
        {
            string approverId = "TGL";
            var approver = new Approver(approverId);
            approver.SetApprovalName(null);
            Assert.AreEqual(approverId, approver.ApprovalName);
        }
        
        [TestMethod]
        public void SetApproverName_NotNullName()
        {
            string approverId = "AJK",
                approvalName = "Andy Kleehammer";
            var approver = new Approver(approverId);
            approver.SetApprovalName(approvalName);
            Assert.AreEqual(approvalName, approver.ApprovalName);
        }
        #endregion
    }
}
