﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.

using Ellucian.Colleague.Domain.ColleagueFinance.Entities;

namespace Ellucian.Colleague.Domain.ColleagueFinance.Tests.Builders
{
    public class GeneralLedgerFiscalYearConfigurationBuilder
    {
        public GeneralLedgerFiscalYearConfiguration FiscalYearConfiguration;
        public int StartMonth;
        public string CurrentYear;

        public GeneralLedgerFiscalYearConfigurationBuilder()
        {
            this.StartMonth = 6;
            this.CurrentYear = "2016";
        }

        public GeneralLedgerFiscalYearConfigurationBuilder WithStartMonth(int startMonth)
        {
            this.StartMonth = startMonth;
            return this;
        }

        public GeneralLedgerFiscalYearConfigurationBuilder WithCurrentYear(string currentYear)
        {
            this.CurrentYear = currentYear;
            return this;
        }

        public GeneralLedgerFiscalYearConfiguration Build()
        {
            this.FiscalYearConfiguration = new GeneralLedgerFiscalYearConfiguration(this.StartMonth, this.CurrentYear);
            return this.FiscalYearConfiguration;
        }
    }
}
