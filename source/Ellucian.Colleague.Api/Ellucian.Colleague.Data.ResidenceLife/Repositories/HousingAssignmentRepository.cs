﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Data.ResidenceLife.DataContracts;
using Ellucian.Colleague.Data.ResidenceLife.Transactions;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;
using Ellucian.Colleague.Domain.ResidenceLife.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Data.ResidenceLife.Repositories
{
    /// <summary>
    /// An implementation of the repository for the housing assignment domain entity
    /// </summary>
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class HousingAssignmentRepository : BaseColleagueRepository, IHousingAssignmentRepository
    {
        /// <summary>
        /// The constructor of the repository
        /// </summary>
        /// <param name="cacheProvider">A cache provider</param>
        /// <param name="transactionFactory">A Colleague transaction factory</param>
        /// <param name="logger">A logger</param>
        public HousingAssignmentRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
        }

        /// <summary>
        /// Get a housing assignment entity by a unique external ID
        /// </summary>
        /// <param name="externalID">The external ID</param>
        /// <param name="externalIdSource">The corresponding external ID source</param>
        /// <returns>Returns a housing assignment domain entity</returns>
        public HousingAssignment GetByExternalId(string externalID, string externalIdSource)
        {
            // Returned as null unless a matching record is found.
            HousingAssignment housingAssignment = null;

            // Query the database for the record with this unique external ID. First query by external Id only because
            // it is indexed and the query will be fast.
            string criteria = "WITH RMAS.EXTERNAL.ID EQ '" + externalID + "'";
            Collection<RoomAssignment> roomAssignmentResults = DataReader.BulkReadRecord<RoomAssignment>(criteria);

            if (roomAssignmentResults == null || roomAssignmentResults.Count == 0)
            {
                throw new KeyNotFoundException("Record not found.");
            }
            else
            {
                // Filter to just records with our *pair* of external ID/source                
                List<RoomAssignment> roomAssignments = roomAssignmentResults.Where(r => r.RmasExternalIdsEntityAssociation.Any(
                            e => e.RmasExternalIdAssocMember == externalID && e.RmasExternalIdSourceAssocMember == externalIdSource)).ToList();

                // Account for data corruption, but the pair to comprise a *unique* alternate ID.                
                if (roomAssignments.Count > 1)
                {
                    // This is data corruption. The software is designed to prevent this, but throw exception is found.
                    throw new ApplicationException("Could not obtain a unique housing assignment for the specified external Id");
                }

                if (roomAssignments.Count == 1)
                {
                    // Return a domain entity from the resulting record (and child records if applicable)
                    RoomAssignment roomAssignment = roomAssignments[0];
                    housingAssignment = MapCTXResultToDomain(roomAssignment);
                }
            }

            return housingAssignment;
        }

        /// <summary>
        /// Get a housing assignment entity by the Id in the native system
        /// </summary>
        /// <param name="id">The Id</param>
        /// <returns>Returns a housing assignment domain entity</returns>        
        public HousingAssignment Get(string id)
        {
            // Returned as null unless a matching record is found.
            HousingAssignment housingAssignment = null;

            // Read the record directly by ID
            RoomAssignment roomAssignment = DataReader.ReadRecord<RoomAssignment>(id);
            if (roomAssignment == null)
            {
                throw new KeyNotFoundException("Record not found.");
            } 
            else
            {
                // Return a domain entity from the resulting record (and child records if applicable)
                housingAssignment = MapCTXResultToDomain(roomAssignment);
            } 
            
            return housingAssignment;
        }

        // *****************************************************************************************
        // Populate a HousingAssignment domain entity from a RoomAssigment object returned by the 
        // Colleague Transaction.
        // Reads in any pointed-to additional amount records from Colleague as needed.
        //
        // Arguments:
        // roomAssignment   In  A room assignment object returned by the Colleague data read4er
        // Returns a HousingAssignment domain entity.
        // *****************************************************************************************
        private HousingAssignment MapCTXResultToDomain(RoomAssignment roomAssignment)
        {
            // The Colleague database can store blanks in these required date fields, even though this is
            // against business rules. But the corresponding entity data type cannot accept them.
            if (roomAssignment.RmasStartDate == null)
            {
                throw new ApplicationException("Start date is required.");
            }

            if (roomAssignment.RmasEndDate == null)
            {
                throw new ApplicationException("End date is required.");
            }

            if (roomAssignment.RmasStatusDate == null || roomAssignment.RmasStatusDate.First() == null)
            {
                throw new ApplicationException("Current status date is required.");
            }

            // Most entity properties are set via the constructor or methods. Do so below.

            HousingAssignment housingAssignment = new HousingAssignment(roomAssignment.RmasPersonId, (DateTime)roomAssignment.RmasStartDate,
                        (DateTime)roomAssignment.RmasEndDate, roomAssignment.RmasStatus.First(), (DateTime)roomAssignment.RmasStatusDate.First());

            // Map the rate period valcode to the domain enumeration
            RoomRatePeriods? ratePeriod = null;
            switch (roomAssignment.RmasRatePeriod)
            {
                case "D":
                    ratePeriod = RoomRatePeriods.Daily;
                    break;
                case "M":
                    ratePeriod = RoomRatePeriods.Monthly;
                    break;
                case "T":
                    ratePeriod = RoomRatePeriods.Term;
                    break;
                case "W":
                    ratePeriod = RoomRatePeriods.Weekly;
                    break;
                case "Y":
                    ratePeriod = RoomRatePeriods.Yearly;
                    break;
                default:
                    break;
            }

            housingAssignment.SetRateInformation(
                roomAssignment.RmasTerm == "" ? null : roomAssignment.RmasTerm,
                roomAssignment.RmasRoomRateTable == "" ? null : roomAssignment.RmasRoomRateTable,
                ratePeriod,
                roomAssignment.RmasOverrideRate,
                roomAssignment.RmasRateOverrideReason == "" ? null : roomAssignment.RmasRateOverrideReason,
                roomAssignment.RmasOverrideArCode == "" ? null : roomAssignment.RmasOverrideArCode,
                roomAssignment.RmasOverrideArType == "" ? null : roomAssignment.RmasOverrideArType,
                roomAssignment.RmasOverrideRefundFormula == "" ? null : roomAssignment.RmasOverrideRefundFormula);

            if (roomAssignment.RmasExternalIdSource != null)
            {
                for (int i = 0; i < roomAssignment.RmasExternalIdSource.Count; i++)
                {
                    housingAssignment.AddExternalId(roomAssignment.RmasExternalId[i], roomAssignment.RmasExternalIdSource[i]);
                }
            }

            housingAssignment.SetBuildingAndRoom(roomAssignment.RmasBldg == "" ? null : roomAssignment.RmasBldg,
                roomAssignment.RmasRoom == "" ? null : roomAssignment.RmasRoom);
            housingAssignment.Comments = roomAssignment.RmasComments == "" ? null : roomAssignment.RmasComments;
            housingAssignment.Contract = roomAssignment.RmasContract == "" ? null : roomAssignment.RmasContract;
            housingAssignment.ResidentStaffIndicator = roomAssignment.RmasResidentStaffIndic == "" ? null : roomAssignment.RmasResidentStaffIndic;
            housingAssignment.Id = roomAssignment.Recordkey;

            if (roomAssignment.RmasAddnlAmts != null && roomAssignment.RmasAddnlAmts.Count > 0)
            {
                // Read all additional amounts at once from Colleague
                Collection<ArAddnlAmts> arAddnlAmts = DataReader.BulkReadRecord<ArAddnlAmts>(roomAssignment.RmasAddnlAmts.ToArray());

                if (arAddnlAmts == null || arAddnlAmts.Count != roomAssignment.RmasAddnlAmts.Count)
                {
                    throw new KeyNotFoundException("Additional amount record not found.");
                }
                else
                {
                    // Map each additional amount record to a domain entity
                    foreach (ArAddnlAmts arAddnlAmt in arAddnlAmts)
                    {
                        // Colleague stores charge and credit in separate fields, even though only one is allowed.
                        // The entity has only one generic amount property.
                        decimal amt;
                        if (arAddnlAmt.AraaChargeAmt != null && arAddnlAmt.AraaChargeAmt != 0 &&
                            arAddnlAmt.AraaCrAmt != null && arAddnlAmt.AraaCrAmt != 0)
                        {
                            throw new ApplicationException("Colleague additional amount contains both a charge and a credit. Only one is allowed.");
                        }

                        // Colleague could contain a null charge and credit, even though one is required.
                        if ((arAddnlAmt.AraaChargeAmt == null || arAddnlAmt.AraaChargeAmt == 0) &&
                            (arAddnlAmt.AraaCrAmt == null || arAddnlAmt.AraaCrAmt == 0))
                        {
                            throw new ApplicationException("An additional amount must have a value.");
                        }

                        if (arAddnlAmt.AraaChargeAmt != null && arAddnlAmt.AraaChargeAmt != 0)
                        {
                            amt = (decimal)arAddnlAmt.AraaChargeAmt;
                        }
                        else
                        {
                            amt = -(decimal)(arAddnlAmt.AraaCrAmt);
                        }

                        housingAssignment.AddAdditionalChargeOrCredit(arAddnlAmt.AraaArCode, arAddnlAmt.AraaDesc, amt);
                    }
                }
            }

            return housingAssignment;
        }

        /// <summary>
        /// Persist a new housing assignment
        /// </summary>
        /// <param name="housingAssignment">The new housing assignment domain entity</param>
        /// <returns>Returns a get of housing assignment after persisting</returns>
        public HousingAssignment Add(HousingAssignment housingAssignment)
        {
            if (housingAssignment.Id != null)
            {
                throw new ArgumentException("Id cannot be supplied to the Add operation.");
            }

            // Map the housing assignment domain entity to the request object for the CTX
            CreateUpdateRoomAssignmentRequest request = new CreateUpdateRoomAssignmentRequest();
            request.AAddOrUpdate = "A";
            request.AId = "";
            MapEntityToCreateUpdateRoomAssignmentCTXRequest(housingAssignment, request);

            // Execute the Colleague transaction to create a new room assignment
            CreateUpdateRoomAssignmentResponse updateResponse = transactionInvoker.Execute<CreateUpdateRoomAssignmentRequest, CreateUpdateRoomAssignmentResponse>(request);

            if (updateResponse.AError == "1")
            {
                throw new ApplicationException(string.Join("; ", updateResponse.AlErrorMsg));
            }
            else
            {
                // Return a "get" of the room assignment just created
                return Get(updateResponse.AId);
            }
        }

        /// <summary>
        /// Persist an updated housing assignment
        /// </summary>
        /// <param name="housingAssignment">The updated housing assignment domain entity</param>
        /// <returns>Returns a get of housing assignment after persisting</returns>
        public HousingAssignment Update(HousingAssignment housingAssignment)
        {

            if ((housingAssignment.Id == null) && (housingAssignment.ExternalIds.Count == 0))
            {
                throw new ArgumentException("An Id or External Id must be supplied to the update operation.");
            }

            // Map the domain entity to the request object for the CTX
            CreateUpdateRoomAssignmentRequest request = new CreateUpdateRoomAssignmentRequest();
            request.AAddOrUpdate = "U";
            request.AId = housingAssignment.Id;
            MapEntityToCreateUpdateRoomAssignmentCTXRequest(housingAssignment, request);

            // Execute the Colleague transaction to update an existing room assignment
            CreateUpdateRoomAssignmentResponse updateResponse = transactionInvoker.Execute<CreateUpdateRoomAssignmentRequest, CreateUpdateRoomAssignmentResponse>(request);

            if (updateResponse.AError == "1")
            {
                throw new ApplicationException(string.Join(" ", updateResponse.AlErrorMsg));
            }
            else
            {
                // Return a "get" of the room assignment just created
                return Get(updateResponse.AId);
            }
        }

        //*************************************************************************************************************
        // Populates the request object for the CreateUpdateRoomAssignment CTX from a housing assignment domain entity.
        // Shared by both the Add and Update methods.
        // The AId and AAddOrUpdate properties of the request object are left to be set by the caller.
        //
        // Arguments:
        // housingAssignment      In    A HousingAssignment domain entity
        // request                Out    A request object for the CTX
        //*************************************************************************************************************
        private static void MapEntityToCreateUpdateRoomAssignmentCTXRequest(HousingAssignment housingAssignment, CreateUpdateRoomAssignmentRequest request)
        {
            foreach (ResidenceLifeExternalId externalId in housingAssignment.ExternalIds)
            {
                request.AlExternalId.Add(externalId.ExternalId);
                request.AlExternalIdSource.Add(externalId.ExternalIdSource);
            }
            request.APersonId = housingAssignment.PersonId;
            request.AStartDate = housingAssignment.StartDate;
            request.AEndDate = housingAssignment.EndDate;
            request.ACurrentStatus = housingAssignment.CurrentStatus;
            request.ACurrentStatusDate = housingAssignment.CurrentStatusDate;
            request.ABuildingId = housingAssignment.Building;
            request.ARoomId = housingAssignment.Room;
            request.ATerm = housingAssignment.TermId;
            request.ARoomRateTable = housingAssignment.RoomRateTable;
            if (housingAssignment.RoomRatePeriod == null)
            {
                request.ARoomRatePeriod = null;
            }
            else
            {
                // Map the domain entity enumerator to the Colleague valcode
                switch (housingAssignment.RoomRatePeriod)
                {
                    case RoomRatePeriods.Daily:
                        request.ARoomRatePeriod = "D";
                        break;
                    case RoomRatePeriods.Monthly:
                        request.ARoomRatePeriod = "M";
                        break;
                    case RoomRatePeriods.Term:
                        request.ARoomRatePeriod = "T";
                        break;
                    case RoomRatePeriods.Weekly:
                        request.ARoomRatePeriod = "W";
                        break;
                    case RoomRatePeriods.Yearly:
                        request.ARoomRatePeriod = "Y";
                        break;
                }
            }
            request.AOverrideRate = housingAssignment.OverrideRate;
            request.AOverrideRateReason = housingAssignment.OverrideRateReason;
            request.AOverrideArCode = housingAssignment.OverrideChargeCode;
            request.AOverrideArType = housingAssignment.OverrideReceivableType;
            request.AOverrideRefundFormula = housingAssignment.OverrideRefundFormula;
            request.AContractNo = housingAssignment.Contract;
            request.AComments = housingAssignment.Comments;
            request.AResidentStaffIndicator = housingAssignment.ResidentStaffIndicator;
            foreach (HousingAdditionalChargeOrCredit acc in housingAssignment.AdditionalChargesOrCredits)
            {
                request.AlAddnlChargeCodes.Add(acc.ArCode);
                request.AlAddnlChargeDescriptions.Add(acc.Description);
                request.AlAddnlChargeAmounts.Add(acc.Amount);
            }
        }

    }
}
