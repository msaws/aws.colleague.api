﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using Ellucian.Colleague.Data.ResidenceLife.DataContracts;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;
using Ellucian.Colleague.Domain.ResidenceLife.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Web.Utility;
using slf4net;

namespace Ellucian.Colleague.Data.ResidenceLife.Repositories
{
    [RegisterType]
    public class ResidenceLifeConfigurationRepository : BaseColleagueRepository, IResidenceLifeConfigurationRepository
    {
        public ResidenceLifeConfigurationRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            CacheTimeout = Level1CacheTimeoutValue;
        }

        /// <summary>
        /// Get the master configuration for Residence Life.
        /// </summary>
        /// <returns>Residence Life configuration</returns>
        public ResidenceLifeConfiguration GetResidenceLifeConfiguration()
        {
            return GetOrAddToCache<ResidenceLifeConfiguration>("ResidenceLifeConfiguration",
                () => { return BuildConfiguration(); });
        }

        #region Private methods

        private ResidenceLifeConfiguration BuildConfiguration()
        {
            // Get parameters from RL.EXT.SYS.DEFAULTS
            var rlesDefaults = GetRlExtSysDefaults();

            var configuration = new
            ResidenceLifeConfiguration(
                rlesDefaults.RlesInvoiceArType,
                rlesDefaults.RlesInvoiceType,
                rlesDefaults.RlesExternalId,
                rlesDefaults.RlesCashier,
                rlesDefaults.RlesDistribution);

            return configuration;

        }

        private RlExtSysDefaults GetRlExtSysDefaults()
        {
            RlExtSysDefaults rlesDefaults = DataReader.ReadRecord<RlExtSysDefaults>("ST.PARMS", "RL.EXT.SYS.DEFAULTS");
            // Default values must be specified
            if (rlesDefaults == null)
            {
                throw new KeyNotFoundException("Residence Life configuration record not found.");
            }

            return rlesDefaults;
        }
        #endregion

    }
}
