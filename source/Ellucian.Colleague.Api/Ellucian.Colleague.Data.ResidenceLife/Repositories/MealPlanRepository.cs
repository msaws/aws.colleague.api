﻿// Copyright 2014-2015 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Data.ResidenceLife.DataContracts;
using Ellucian.Colleague.Data.ResidenceLife.Transactions;
using Ellucian.Colleague.Domain.ResidenceLife.Entities;
using Ellucian.Colleague.Domain.ResidenceLife.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Web.Utility;
using slf4net;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace Ellucian.Colleague.Data.ResidenceLife.Repositories
{
    /// <summary>
    /// An implementation of the repository for the MealPlan domain entity.
    /// This also serves as the repository for related entities MealPlanAssignment and MealPlanAssignmentStatus
    /// </summary>
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class MealPlanRepository : BaseColleagueRepository, IMealPlanRepository
    {
        /// <summary>
        /// The constructor of the repository
        /// </summary>
        /// <param name="cacheProvider">A cache provider</param>
        /// <param name="transactionFactory">A Colleague transaction factory</param>
        /// <param name="logger">A logger</param>
        public MealPlanRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
            // Using level 1 cache time out value for data that rarely changes.
            CacheTimeout = Level1CacheTimeoutValue;
        }

        /// <summary>
        /// Get a MealPlan entity by the Id
        /// </summary>
        /// <param name="id">The Id</param>
        /// <returns>Returns a housing assignment domain entity</returns>
        public MealPlan Get(string id)
        {
            // Returned as null unless a matching record is found.
            MealPlan mealPlan = null;

            MealPlans colleagueMealPlans = DataReader.ReadRecord<MealPlans>(id);
            if (colleagueMealPlans == null)
            {
                throw new KeyNotFoundException("Record not found.");
            } 
            else
            {
                // The Colleague database can store blanks in these required date fields, even though this is
                // against business rules. But the corresponding entity data type cannot accept them.
                string appExceptionMsg = null;
                if (colleagueMealPlans.MealStartDate == null) appExceptionMsg = "Meal plan Start Date is required.";
                if (colleagueMealPlans.MealDesc == null) appExceptionMsg = "Meal plan description is required.";

                if (appExceptionMsg != null)
                {
                    string errMessage = "MealPlan Repository Get Record ID: " + id + " " + appExceptionMsg;
                    LogDataError("MealPlan", colleagueMealPlans.Recordkey, colleagueMealPlans, null, errMessage);
                    throw new ApplicationException(appExceptionMsg);
                }

                // Populate a domain entity from the Colleague MealPlans record
                mealPlan = new MealPlan((DateTime)colleagueMealPlans.MealStartDate, colleagueMealPlans.MealDesc);
                mealPlan.CancelationChargeCode = colleagueMealPlans.MealCancelArCode;
                mealPlan.Comments = colleagueMealPlans.MealComments;
                mealPlan.EndDate = colleagueMealPlans.MealEndDate;
                mealPlan.Id = colleagueMealPlans.Recordkey;
                mealPlan.MealClass = colleagueMealPlans.MealClass;
                mealPlan.MealsPerFrequency = colleagueMealPlans.MealFrequency;
                mealPlan.NumberOfMealsPer = colleagueMealPlans.MealNoTimes;
                switch (colleagueMealPlans.MealRatePeriod)
                {
                    case "B":
                        mealPlan.RatePeriod = MealPlanRatePeriods.PerMeal;
                        break;
                    case "D":
                        mealPlan.RatePeriod = MealPlanRatePeriods.Daily;
                        break;
                    case "W":
                        mealPlan.RatePeriod = MealPlanRatePeriods.Weekly;
                        break;
                    case "M":
                        mealPlan.RatePeriod = MealPlanRatePeriods.Monthly;
                        break;
                    case "Y":
                        mealPlan.RatePeriod = MealPlanRatePeriods.Yearly;
                        break;
                    case "T":
                        mealPlan.RatePeriod = MealPlanRatePeriods.Term;
                        break;
                    default:
                        break;
                }

                if ((colleagueMealPlans.MealBldgs != null) && (colleagueMealPlans.MealBldgs.Count > 0))
                {
                    List<MealPlanBuildingAndRoom> buildingsAndRooms = new List<MealPlanBuildingAndRoom>();
                    for (int i = 0; i < colleagueMealPlans.MealBldgs.Count; i++)
                    {
                        if ((colleagueMealPlans.MealRooms != null) && (colleagueMealPlans.MealRooms.Count > i))
                        {
                            buildingsAndRooms.Add(new MealPlanBuildingAndRoom(colleagueMealPlans.MealBldgs[i], colleagueMealPlans.MealRooms[i]));
                        } else
                        {
                            buildingsAndRooms.Add(new MealPlanBuildingAndRoom(colleagueMealPlans.MealBldgs[i], null));
                        }
                    }
                    // Corrupt Colleague in these attributes should not halt the API. The API business logic does
                    // not presently use these attributes. Catch and log this case.
                    try
                    {
                        mealPlan.SetBuildingsAndRooms(buildingsAndRooms);
                    }
                    catch (Exception ex)
                    {
                        string errMessage = "Non fatal exception with building and room data in the MealPlan Get method for Record ID: " + id + " " + ex.Message;
                        LogDataError("Meal Plan Building", colleagueMealPlans.Recordkey, colleagueMealPlans, ex, errMessage);
                    }
                }

                // Corrupt Colleague in these attributes should not halt the API. The API business logic does
                // not presently use these attributes. Catch and log this case.
                try
                {
                    mealPlan.SetLocations(colleagueMealPlans.MealLocations);
                }
                catch (Exception ex)
                {
                    string errMessage = "Non fatal exception with locations data in the MealPlan Get method for Record ID: " + id + " " + ex.Message;
                    LogDataError("Meal Plan Locations", colleagueMealPlans.Recordkey, colleagueMealPlans, ex, errMessage);
                }

                // Corrupt Colleague in these attributes should not halt the API. The API business logic does
                // not presently use these attributes. Catch and log this case.
                try
                {
                    mealPlan.SetMealTypes(colleagueMealPlans.MealTypes);
                }
                catch (Exception ex)
                {
                    string errMessage = "Non fatal exception with meal types data in the MealPlan Get method for Record ID: " + id + " " + ex.Message;
                    LogDataError("Meal Plan Type", colleagueMealPlans.Recordkey, colleagueMealPlans, ex, errMessage);
                }

                List<MealPlanRate> mealPlanRates = null;
                if (colleagueMealPlans.MealRates != null)
                {
                    for (int i = 0; i < colleagueMealPlans.MealRates.Count; i++)
                    {
                        if (i == 0) { mealPlanRates = new List<MealPlanRate>(); }
                        mealPlanRates.Add(new MealPlanRate((decimal)colleagueMealPlans.MealRates[i], (DateTime)colleagueMealPlans.MealRateEffectiveDates[i]));
                    }
                }

                // Corrupt Colleague in these attributes should not halt the API. The API business logic does
                // not presently use these attributes. Catch and log this case.
                try
                {
                    mealPlan.SetRatesAndChargeCode(mealPlanRates, colleagueMealPlans.MealArCode);
                }
                catch (Exception ex)
                {
                    string errMessage = "Non fatal exception with meal plan rates and charge code data in the MealPlan Get method for Record ID: " + id + " " + ex.Message;
                    LogDataError("Meal Plan Rates", colleagueMealPlans.Recordkey, colleagueMealPlans, ex, errMessage);
                }
            } 
            
            return mealPlan;
        }

        /// <summary>
        /// Get a MealPlanAssignment entity by the Id
        /// </summary>
        /// <param name="id">The Id</param>
        /// <returns>Returns a meal plan assignment domain entity</returns>
        // This method was grouped into the MealPlan repository to handle the fact that the MealPlanAssignment entity includes 
        // MealPlan entity and this method needs to call the MealPlan repository Get method. 
        public Ellucian.Colleague.Domain.ResidenceLife.Entities.MealPlanAssignment GetMealPlanAssignment(string id)
        {
            // Returned as null unless a matching record is found.
            Ellucian.Colleague.Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignment = null;

            Ellucian.Colleague.Data.ResidenceLife.DataContracts.MealPlanAssignment colleagueMealPlanAssignment
                = DataReader.ReadRecord<Ellucian.Colleague.Data.ResidenceLife.DataContracts.MealPlanAssignment>(id);
            if (colleagueMealPlanAssignment == null)
            {
                throw new KeyNotFoundException("Record not found.");
            }
            else
            {
                // Populate a domain entity from the Colleague MealPlanAssignment record

                // The Colleague database can store blanks in these required date fields, even though this is
                // against business rules. But the corresponding entity data type cannot accept them.

                // The Colleague database can store blanks in these required date fields, even though this is
                // against business rules. But the corresponding entity data type cannot accept them.
                string appExceptionMsg = null;
                if (!colleagueMealPlanAssignment.MpasStartDate.HasValue) appExceptionMsg = "Start Date is required.";
                if (!colleagueMealPlanAssignment.MpasNoRatePeriods.HasValue) appExceptionMsg = "Number of Rate Periods is required.";
                if (colleagueMealPlanAssignment.MpasStatusDate == null || colleagueMealPlanAssignment.MpasStatusDate.Count < 1 ||
                    !colleagueMealPlanAssignment.MpasStatusDate.First().HasValue) appExceptionMsg = "Status Date is required.";

                if (appExceptionMsg != null)
                {
                    string errMessage = "Repository GetMealPlanAssignment Record ID: " + id + " " + appExceptionMsg;
                    LogDataError("GetMealPlanAssignment", colleagueMealPlanAssignment.Recordkey, colleagueMealPlanAssignment, null, errMessage);
                    throw new ApplicationException(appExceptionMsg);
                }

                // Get a MealPlan entity for the MealPlan ID in the assignment
                MealPlan mealPlan = Get(colleagueMealPlanAssignment.MpasMealPlan);

                // Get the MealPlanAssignmentStatus entity for the current status code of the assignment
                MealPlanAssignmentStatus mealPlanAssignmentStatus =
                    MealPlanAssignmentStatuses.Where(mp => mp.Code == colleagueMealPlanAssignment.MpasStatus[0]).Single();

                mealPlanAssignment = new Ellucian.Colleague.Domain.ResidenceLife.Entities.MealPlanAssignment(
                    colleagueMealPlanAssignment.MpasPersonId, mealPlan, (int)colleagueMealPlanAssignment.MpasNoRatePeriods,
                    (DateTime)colleagueMealPlanAssignment.MpasStartDate, colleagueMealPlanAssignment.MpasEndDate,
                    mealPlanAssignmentStatus, (DateTime)colleagueMealPlanAssignment.MpasStatusDate.First(),
                    colleagueMealPlanAssignment.MpasTerm, colleagueMealPlanAssignment.MpasUsedRatePeriods,
                    colleagueMealPlanAssignment.MpasUsedPct);

                mealPlanAssignment.Id = colleagueMealPlanAssignment.Recordkey;
                mealPlanAssignment.CardNumber = colleagueMealPlanAssignment.MpasMealCard;
                mealPlanAssignment.OverrideChargeCode = colleagueMealPlanAssignment.MpasOverrideArCode;
                mealPlanAssignment.OverrideReceivableType = colleagueMealPlanAssignment.MpasOverrideArType;
                mealPlanAssignment.OverrideRefundFormula = colleagueMealPlanAssignment.MpasOverrideRefundFormula;
                mealPlanAssignment.Comments = colleagueMealPlanAssignment.MpasComments;

                if (colleagueMealPlanAssignment.MpasExternalIdSource != null)
                {
                    for (int i = 0; i < colleagueMealPlanAssignment.MpasExternalIdSource.Count; i++)
                    {
                        mealPlanAssignment.AddExternalId(colleagueMealPlanAssignment.MpasExternalId[i], colleagueMealPlanAssignment.MpasExternalIdSource[i]);
                    }
                }

                mealPlanAssignment.SetOverrideRate(colleagueMealPlanAssignment.MpasOverrideRate, colleagueMealPlanAssignment.MpasRateOverrideReason);

                return mealPlanAssignment;
            }
        }

        /// <summary>
        /// Persist a new meal plan assignment
        /// </summary>
        /// <param name="housingAssignment">The new meal plan assignment domain entity</param>
        /// <returns>Returns a get of meal plan assignment after persisting</returns>
        public Domain.ResidenceLife.Entities.MealPlanAssignment AddMealPlanAssignment(Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignment)
        {
            if (mealPlanAssignment.Id != null)
            {
                throw new ArgumentException("Id cannot be supplied to the Add operation.");
            }

            // Map the housing assignment domain entity to the request object for the CTX
            CreateUpdateMealPlanAssignmentRequest request = new CreateUpdateMealPlanAssignmentRequest();
            request.AAddOrUpdate = "A";
            request.AId = "";
            MapEntityToCreateUpdateMealPlanAssignmentCTXRequest(mealPlanAssignment, request);

            // Execute the Colleague transaction to create a new room assignment
            CreateUpdateMealPlanAssignmentResponse updateResponse = transactionInvoker.Execute<CreateUpdateMealPlanAssignmentRequest, CreateUpdateMealPlanAssignmentResponse>(request);

            if (updateResponse.AError == "1")
            {
                throw new ApplicationException(string.Join("; ", updateResponse.AlErrorMsg));
            }
            else
            {
                // Return a "get" of the room assignment just created
                return GetMealPlanAssignment(updateResponse.AId);
            }
        }

        /// <summary>
        /// Persist an updated meal plan assignment
        /// </summary>
        /// <param name="housingAssignment">The updated meal plan assignment domain entity</param>
        /// <returns>Returns a get of meal plan assignment after persisting</returns>
        public Domain.ResidenceLife.Entities.MealPlanAssignment UpdateMealPlanAssignment(Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignment)
        {
            if ((mealPlanAssignment.Id == null) && (mealPlanAssignment.ExternalIds.Count == 0))
            {
                throw new ArgumentException("An Id or External Id must be supplied to the update operation.");
            }

            // Map the meal plan assignment domain entity to the request object for the CTX
            CreateUpdateMealPlanAssignmentRequest request = new CreateUpdateMealPlanAssignmentRequest();
            request.AAddOrUpdate = "U";
            request.AId = mealPlanAssignment.Id;
            MapEntityToCreateUpdateMealPlanAssignmentCTXRequest(mealPlanAssignment, request);

            // Execute the Colleague transaction to create a new room assignment
            CreateUpdateMealPlanAssignmentResponse updateResponse = transactionInvoker.Execute<CreateUpdateMealPlanAssignmentRequest, CreateUpdateMealPlanAssignmentResponse>(request);

            if (updateResponse.AError == "1")
            {
                throw new ApplicationException(string.Join("; ", updateResponse.AlErrorMsg));
            }
            else
            {
                // Return a "get" of the updated meal plan assignment
                return GetMealPlanAssignment(updateResponse.AId);
            }
        }

        /// <summary>
        /// Return a list of MealPlanAssignmentStatus entities, based on the Colleague MEAL.ASSIGN.STATUSES code table
        /// </summary>
        // This is included in the MealPlan repository to address the fact that the GetMealPlanAssignment repository method
        // needs to Get this property, but we want to avoid one repository calling another repository.
        // "virtual" is so that this can be mocked when testing other methods of this repository. Questionable,
        // but the best compromise I could devise to write the tests.
        public virtual IEnumerable<MealPlanAssignmentStatus> MealPlanAssignmentStatuses
        {
            get
            {
                return GetValcode<MealPlanAssignmentStatus>("ST", "MEAL.ASSIGN.STATUSES",
                    valCode =>
                    {
                        // Map colleague special processing codes to the domain MealPlanAssignmentStatusType enum
                        MealPlanAssignmentStatusType statusType;
                        switch (valCode.ValActionCode1AssocMember)
                        {
                            case "1":
                                statusType = MealPlanAssignmentStatusType.Canceled;
                                break;
                            case "2":
                                statusType = MealPlanAssignmentStatusType.EarlyTermination;
                                break;
                            case "3":
                                statusType = MealPlanAssignmentStatusType.LateStart;
                                break;
                            default:
                                statusType = MealPlanAssignmentStatusType.None;
                                break;
                        };
                        return new MealPlanAssignmentStatus(valCode.ValInternalCodeAssocMember, valCode.ValExternalRepresentationAssocMember, statusType);
                    });
            }
        }

        //*************************************************************************************************************
        // Populates the request object for the CreateUpdateRoomAssignment CTX from a housing assignment domain entity.
        // Shared by both the Add and Update methods.
        // The AId and AAddOrUpdate properties of the request object are left to be set by the caller.
        //
        // Arguments:
        // housingAssignment      In    A HousingAssignment domain entity
        // request                Out    A request object for the CTX
        //*************************************************************************************************************
        private static void MapEntityToCreateUpdateMealPlanAssignmentCTXRequest(Domain.ResidenceLife.Entities.MealPlanAssignment mealPlanAssignment, 
                CreateUpdateMealPlanAssignmentRequest request)
        {
            foreach (ResidenceLifeExternalId externalId in mealPlanAssignment.ExternalIds)
            {
                request.AlExternalId.Add(externalId.ExternalId);
                request.AlExternalIdSource.Add(externalId.ExternalIdSource);
            }
            request.AComments = mealPlanAssignment.Comments;
            request.ACurrentStatus = mealPlanAssignment.CurrentStatusCode;
            request.ACurrentStatusDate = mealPlanAssignment.CurrentStatusDate;
            request.AEndDate = mealPlanAssignment.EndDate;
            request.AId = mealPlanAssignment.Id;
            request.AMealCard = mealPlanAssignment.CardNumber;
            request.AMealPlanId = mealPlanAssignment.MealPlanId;
            request.ANoRatePeriods = mealPlanAssignment.NumberOfRatePeriods;
            request.AOverrideArCode = mealPlanAssignment.OverrideChargeCode;
            request.AOverrideArType = mealPlanAssignment.OverrideReceivableType;
            request.AOverrideRate = mealPlanAssignment.OverrideRate;
            request.AOverrideRateReason = mealPlanAssignment.OverrideRateReason;
            request.AOverrideRefundFormula = mealPlanAssignment.OverrideRefundFormula;
            request.APersonId = mealPlanAssignment.PersonId;
            request.AStartDate = mealPlanAssignment.StartDate;
            request.ATerm = mealPlanAssignment.TermId;
            request.AUsedPct = mealPlanAssignment.UsedPercent;
            request.AUsedRatePeriods = mealPlanAssignment.UsedRatePeriods;
        }

        
    }
}
