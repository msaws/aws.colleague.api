﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Finance;
using Ellucian.Colleague.Domain.Finance.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Colleague.Dtos.Finance;
using Ellucian.Colleague.Dtos.Finance.AccountActivity;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;
using System.Linq;

namespace Ellucian.Colleague.Coordination.Finance.Services
{
    [RegisterType]
    public class AccountActivityService : BaseCoordinationService, IAccountActivityService
    {
        private IAccountActivityRepository _activityRepository;
        private IAccountsReceivableRepository _arRepository;

        public AccountActivityService(IAdapterRegistry adapterRegistry, IAccountActivityRepository activityRepository, IAccountsReceivableRepository arRepository,
            ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            _activityRepository = activityRepository;
            _arRepository = arRepository;
        }

        private void CheckPermission(string studentId)
        {
            bool hasAdminPermission = HasPermission(FinancePermissionCodes.ViewStudentAccountActivity);

            var proxySubject = CurrentUser.ProxySubjects.FirstOrDefault();

            // They're allowed to see another's data if they are a proxy for that user or have the admin permission
            if (!CurrentUser.IsPerson(studentId) && !HasProxyAccessForPerson(studentId) && !hasAdminPermission)
            {
                logger.Info(CurrentUser + " does not have permission code " + FinancePermissionCodes.ViewStudentAccountActivity);
                throw new PermissionsException();
            }
        }

        public AccountActivityPeriods GetAccountActivityPeriodsForStudent(string studentId)
        {
            CheckPermission(studentId);

            var termPeriods = _activityRepository.GetAccountPeriods(studentId);
            var nonTermPeriod = _activityRepository.GetNonTermAccountPeriod(studentId);

            var activityDtoAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Finance.Entities.AccountActivity.AccountPeriod, AccountPeriod>();
            var activityDto = new AccountActivityPeriods();
            var activityDtoCollection = new List<AccountPeriod>();

            foreach (var period in termPeriods)
            {
                activityDtoCollection.Add(activityDtoAdapter.MapToType(period));
            }

            activityDto.Periods = activityDtoCollection;
            activityDto.NonTermActivity = activityDtoAdapter.MapToType(nonTermPeriod);

            return activityDto;
        }

        [Obsolete("Obsolete as of API version 1.8, use GetAccountActivityByTermForStudent2 instead")]
        public DetailedAccountPeriod GetAccountActivityByTermForStudent(string termId, string personId)
        {
            CheckPermission(personId);

            var accountPeriodEntity = _activityRepository.GetTermActivityForStudent(termId, personId);

            var adapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Finance.Entities.AccountActivity.DetailedAccountPeriod, DetailedAccountPeriod>();

            var accountPeriodDto = adapter.MapToType(accountPeriodEntity);

            return accountPeriodDto;
        }

        /// <summary>
        /// Retrieves account activity details for a student for a term
        /// </summary>
        /// <param name="termId">Term for which to retrieve account activity</param>
        /// <param name="personId">Person for whom to retrieve account activity</param>
        /// <returns>Account activity details for a student for a term</returns>
        public DetailedAccountPeriod GetAccountActivityByTermForStudent2(string termId, string personId)
        {
            CheckPermission(personId);

            var accountPeriodEntity = _activityRepository.GetTermActivityForStudent2(termId, personId);

            var adapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Finance.Entities.AccountActivity.DetailedAccountPeriod, DetailedAccountPeriod>();

            var accountPeriodDto = adapter.MapToType(accountPeriodEntity);

            return accountPeriodDto;
        }

        [Obsolete("Obsolete as of API version 1.8, use PostAccountActivityByPeriodForStudent2 instead")]
        public DetailedAccountPeriod PostAccountActivityByPeriodForStudent(IEnumerable<string> periods, DateTime? startDate, DateTime? endDate, string personId)
        {
            CheckPermission(personId);

            var accountPeriodEntity = _activityRepository.GetPeriodActivityForStudent(periods, startDate, endDate, personId);

            var adapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Finance.Entities.AccountActivity.DetailedAccountPeriod, DetailedAccountPeriod>();

            var accountPeriodDto = adapter.MapToType(accountPeriodEntity);

            return accountPeriodDto;
        }

        /// <summary>
        /// Retrieves account activity details for a student for a collection of periods
        /// </summary>
        /// <param name="periods">Periods for which to retrieve account activity</param>
        /// <param name="startDate">Date on which retrieved account activity details start</param>
        /// <param name="endDate">Date on which retrieved account activity details end</param>
        /// <param name="personId">Person for whom to retrieve account activity</param>
        /// <returns>Account activity details for a student for a term</returns>
        public DetailedAccountPeriod PostAccountActivityByPeriodForStudent2(IEnumerable<string> periods, DateTime? startDate, DateTime? endDate, string personId)
        {
            CheckPermission(personId);

            var accountPeriodEntity = _activityRepository.GetPeriodActivityForStudent2(periods, startDate, endDate, personId);

            var adapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Finance.Entities.AccountActivity.DetailedAccountPeriod, DetailedAccountPeriod>();

            var accountPeriodDto = adapter.MapToType(accountPeriodEntity);

            return accountPeriodDto;
        }

        public IEnumerable<DepositDue> GetDepositsDue(string id)
        {
            CheckPermission(id);

            var depositsDueEntity = _arRepository.GetDepositsDue(id);
            var adapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Finance.Entities.DepositDue, DepositDue>();
            List<DepositDue> depositsDueDto = new List<DepositDue>();
            foreach (var item in depositsDueEntity)
            {
                depositsDueDto.Add(adapter.MapToType(item));
            }
            return depositsDueDto;
        }

        public AccountHolder GetAccountHolder(string id)
        {
            CheckPermission(id);

            var accountHolderEntity = _arRepository.GetAccountHolder(id);
            var adapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Finance.Entities.AccountHolder, AccountHolder>();
            var accountHolderDto = adapter.MapToType(accountHolderEntity);
            
            return accountHolderDto;
        }
    }
}
