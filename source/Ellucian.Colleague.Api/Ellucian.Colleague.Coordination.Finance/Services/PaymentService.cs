﻿// Copyright 2012-2015 Ellucian Company L.P. and its affiliates.
using System.Diagnostics;
using System.Linq;
using Ellucian.Colleague.Coordination.Base.Services;
using Ellucian.Colleague.Domain.Finance.Repositories;
using Ellucian.Colleague.Domain.Repositories;
using Ellucian.Web.Adapters;
using Ellucian.Web.Dependency;
using Ellucian.Web.Security;
using slf4net;

namespace Ellucian.Colleague.Coordination.Finance.Services
{
    [RegisterType]
    public class PaymentService : BaseCoordinationService, IPaymentService
    {
        private readonly IPaymentRepository _paymentRepository;
        private readonly IAccountDueRepository _accountRepository;

        public PaymentService(IAdapterRegistry adapterRegistry, IPaymentRepository paymentRepository, IAccountDueRepository accountRepository,
            ICurrentUserFactory currentUserFactory, IRoleRepository roleRepository, ILogger logger)
            : base(adapterRegistry, currentUserFactory, roleRepository, logger)
        {
            _paymentRepository = paymentRepository;
            _accountRepository = accountRepository;
        }

        public Ellucian.Colleague.Dtos.Finance.Payments.PaymentConfirmation GetPaymentConfirmation(string distribution, string paymentMethod, string amountToPay)
        {
            var paymentConfirmationEntity = _paymentRepository.GetConfirmation(distribution, paymentMethod, amountToPay);

            var adapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Finance.Entities.Payments.PaymentConfirmation, Ellucian.Colleague.Dtos.Finance.Payments.PaymentConfirmation>();

            var paymentConfirmationDto = adapter.MapToType(paymentConfirmationEntity);

            return paymentConfirmationDto;
        }

        private void CheckPayOwnAccountPermission(string personId)
        {
            var proxySubject = CurrentUser.ProxySubjects.FirstOrDefault();
            if (!CurrentUser.IsPerson(personId) && !HasProxyAccessForPerson(personId))
            {
                logger.Info(CurrentUser + " attempted to pay on account " + personId);
                throw new PermissionsException();
            }
        }

        public Ellucian.Colleague.Dtos.Finance.Payments.PaymentProvider PostPaymentProvider(Ellucian.Colleague.Dtos.Finance.Payments.Payment paymentDetails)
        {
            CheckPayOwnAccountPermission(paymentDetails.PersonId);
            var paymentAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Dtos.Finance.Payments.Payment, Ellucian.Colleague.Domain.Finance.Entities.Payments.Payment>();

            var paymentEntity = paymentAdapter.MapToType(paymentDetails);

            var paymentConfirmationEntity = _paymentRepository.StartPaymentProvider(paymentEntity);

            var paymentProviderAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Finance.Entities.Payments.PaymentProvider, Ellucian.Colleague.Dtos.Finance.Payments.PaymentProvider>();

            var paymentProviderDto = paymentProviderAdapter.MapToType(paymentConfirmationEntity);

            return paymentProviderDto;
        }

        public Ellucian.Colleague.Dtos.Finance.Payments.PaymentReceipt GetPaymentReceipt(string transactionId, string cashReceiptId)
        {
            var paymentReceiptEntity = _paymentRepository.GetCashReceipt(transactionId, cashReceiptId);

            var adapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Finance.Entities.Payments.PaymentReceipt, Ellucian.Colleague.Dtos.Finance.Payments.PaymentReceipt>();

            var paymentReceiptDto = adapter.MapToType(paymentReceiptEntity);

            return paymentReceiptDto;
        }

        public Ellucian.Colleague.Dtos.Finance.Payments.ElectronicCheckProcessingResult PostProcessElectronicCheck(Ellucian.Colleague.Dtos.Finance.Payments.Payment paymentDetails)
        {
            CheckPayOwnAccountPermission(paymentDetails.PersonId);
            var paymentAdapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Dtos.Finance.Payments.Payment, Ellucian.Colleague.Domain.Finance.Entities.Payments.Payment>();

            var paymentEntity = paymentAdapter.MapToType(paymentDetails);

            var checkProcessingResultEntity = _accountRepository.ProcessCheck(paymentEntity);

            var adapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Finance.Entities.Payments.ElectronicCheckProcessingResult, Ellucian.Colleague.Dtos.Finance.Payments.ElectronicCheckProcessingResult>();

            var checkProcessingResultDto = adapter.MapToType(checkProcessingResultEntity);

            return checkProcessingResultDto;
        }

        public Ellucian.Colleague.Dtos.Finance.Payments.ElectronicCheckPayer GetCheckPayerInformation(string personId)
        {
            CheckPayOwnAccountPermission(personId);
            var checkPayerInformationEntity = _accountRepository.GetCheckPayerInformation(personId);

            var adapter = _adapterRegistry.GetAdapter<Ellucian.Colleague.Domain.Finance.Entities.Payments.ElectronicCheckPayer, Ellucian.Colleague.Dtos.Finance.Payments.ElectronicCheckPayer>();

            var checkPayerInformationDto = adapter.MapToType(checkPayerInformationEntity);

            return checkPayerInformationDto;
        }
    }
}
