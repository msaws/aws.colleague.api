﻿// Copyright 2012-2016 Ellucian Company L.P. and its affiliates.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.Student;
using Ellucian.Colleague.Dtos.Student.Requirements;
using Ellucian.Web.Http.TestUtil;
using Ellucian.Web.Infrastructure.TestUtil;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using slf4net;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Api.Client.Tests
{
    [TestClass]
    public class ColleagueApiClientStudentTests
    {
        [TestClass]
        public class CoursesAndSections
        {

            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _studentId = "123456";
            private const string _studentId2 = "678";
            private const string _token = "1234567890";
            private const string _courseId = "MATH-100";
            private const string _courseId2 = "ENGL-101";
            private const string _facultyId = "2222222";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public void GetCourse()
            {
                // Arrange
                var courseResponse = new Course()
                {
                    Id = _courseId,
                    SubjectCode = "MATH",
                    Title = "Mathematics"
                };

                var serializedResponse = JsonConvert.SerializeObject(courseResponse);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var result = client.GetCourse(_courseId);

                // Assert
                Assert.AreEqual(courseResponse.Id, result.Id);
                Assert.AreEqual(courseResponse.SubjectCode, result.SubjectCode);
                Assert.AreEqual(courseResponse.Title, result.Title);
            }


            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void GetCourse_NullCourseId()
            {
                // Arrange
                var response = new HttpResponseMessage(HttpStatusCode.BadRequest);
                response.Content = new StringContent(string.Empty, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var result = client.GetCourse(null);
            }

            [TestMethod]
            public void GetCourse_EmptyResponse()
            {
                // Arrange
                var serializedResponse = JsonConvert.SerializeObject(string.Empty);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var result = client.GetCourse(_courseId);

                // Assert
                Assert.IsNull(result);
            }

            [TestMethod]
            public void SearchCourses()
            {
                // Arrange
                var simpleStringValue = "12345678";
                var simpleStringArgument = new List<string>() { simpleStringValue };
                var pageSize = 10;
                var pageIndex = 1;

                var courseResponse = new CourseSearch()
                {
                    Id = _courseId,
                    SubjectCode = "MATH",
                    Title = "Mathematics"
                };

                var courseListResponse = new List<CourseSearch>();
                courseListResponse.Add(courseResponse);

                var coursePageResponse = new CoursePage(courseListResponse, pageSize, pageIndex);
                coursePageResponse.AcademicLevels = new List<Filter>() {
                new Filter() {
                    Count = 0,
                    Selected = false,
                    Value = simpleStringValue
                }
            };

                var serializedResponse = JsonConvert.SerializeObject(coursePageResponse);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act

                var result = client.SearchCourses(simpleStringArgument, simpleStringArgument, simpleStringArgument, simpleStringArgument, simpleStringArgument, simpleStringArgument, simpleStringArgument, simpleStringArgument, simpleStringArgument, simpleStringArgument, null, null, string.Empty, null, string.Empty, simpleStringArgument, simpleStringArgument, pageSize, pageIndex);

                var firstCourse = result.CurrentPageItems.ElementAt(0);
                var academicLevelFilterValues = result.AcademicLevels.Select(x => x.Value).ToList();

                // Assert
                Assert.AreEqual(pageIndex, result.CurrentPageIndex);
                Assert.AreEqual(pageSize, result.PageSize);
                Assert.AreEqual(courseResponse.Id, firstCourse.Id);
                CollectionAssert.AreEquivalent(simpleStringArgument, academicLevelFilterValues);
            }

            [TestMethod]
            public void GetSectionsByCourse()
            {
                // Arrange
                var mathSection = new Section()
                {
                    Id = "01",
                    CourseId = _courseId,
                    Title = "Mathematics"
                };

                var courseListResponse = new List<Section>();
                courseListResponse.Add(mathSection);

                var courseId = mathSection.Id;

                var serializedResponse = JsonConvert.SerializeObject(courseListResponse);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var result = client.GetSectionsByCourse(new List<string>() { courseId });

                var retrievedMathSection = result.ElementAt(0);

                // Assert
                Assert.IsNotNull(result);
                Assert.AreEqual(mathSection.Id, retrievedMathSection.Id);
                Assert.AreEqual(mathSection.CourseId, retrievedMathSection.CourseId);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void GetSectionsByCourse_NullCourseIdList()
            {
                // Arrange
                var response = new HttpResponseMessage(HttpStatusCode.BadRequest);
                response.Content = new StringContent(string.Empty, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var result = client.GetSectionsByCourse(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void GetSectionsByCourse_EmptyCourseId()
            {
                // Arrange
                var emptyId = string.Empty;
                var response = new HttpResponseMessage(HttpStatusCode.BadRequest);
                response.Content = new StringContent(string.Empty, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var result = client.GetSectionsByCourse(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void GetSections_NullSectionIdList()
            {
                // Arrange
                var response = new HttpResponseMessage(HttpStatusCode.BadRequest);
                response.Content = new StringContent(string.Empty, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var result = client.GetSections(null, false);
            }

            [TestMethod]
            public void GetCourses()
            {
                // Arrange
                var courseIds = new List<string>() { "1", "2", "3" };
                var coursesResponse = new List<Course2>()
                {
                    new Course2()
                    {
                        Id = "1",
                        SubjectCode = "MATH",
                        Title = "Mathematics"
                    },
                    new Course2()
                    {
                        Id = "2",
                        SubjectCode = "ENGL",
                        Title = "English 1"
                    }
                };

                var serializedResponse = JsonConvert.SerializeObject(coursesResponse);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var criteria = new CourseQueryCriteria() { CourseIds = courseIds };
                var result = client.QueryCourses2(criteria);

                // Assert
                Assert.AreEqual(2, result.Count());
                Assert.IsTrue(result is IEnumerable<Course2>);

            }
            [TestMethod]
            public void GetFacultySections()
            {
                // Arrange
                var mathSection = new Section3()
                {
                    Id = "01",
                    CourseId = _courseId,
                    Title = "Mathematics",
                    FacultyIds = new List<string>() { _facultyId }
                };

                var courseListResponse = new List<Section3>();
                courseListResponse.Add(mathSection);

                var courseId = mathSection.Id;
                var facultyId = mathSection.FacultyIds.First();

                var serializedResponse = JsonConvert.SerializeObject(courseListResponse);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var result = client.GetFacultySections3(facultyId);

                var retrievedMathSection = result.ElementAt(0);

                // Assert
                Assert.IsNotNull(result);
                Assert.AreEqual(mathSection.Id, retrievedMathSection.Id);
                Assert.AreEqual(mathSection.CourseId, retrievedMathSection.CourseId);
            }
        }

        [TestClass]
        public class CheckRegistrationEligibility
        {

            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _studentId = "123456";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public void CheckRegistrationEligibilityResponse()
            {
                // Arrange
                var regElig = new RegistrationEligibility()
                {
                    Messages = new List<RegistrationMessage>()
                        {new RegistrationMessage() {Message = "eligibility message"}
                        },
                    IsEligible = true,
                    HasOverride = true
                };

                var serializedResponse = JsonConvert.SerializeObject(regElig);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.CheckRegistrationEligibility(_studentId);

                // Assert
                Assert.AreEqual(regElig.Messages.ElementAt(0).Message, clientResponse.Messages.ElementAt(0).Message);
                Assert.AreEqual(regElig.IsEligible, clientResponse.IsEligible);
                Assert.AreEqual(regElig.HasOverride, clientResponse.HasOverride);
            }
        }

        [TestClass]
        public class QueryFaculty
        {

            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _studentId = "123456";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public void QueryFaculty_ReturnsSerializedFaculty()
            {
                // Arrange
                var facultyDtos = new List<Faculty>()
                {
                    new Faculty() {Id = "0000001", LastName = "Smith"},
                    new Faculty() {Id = "0000011", LastName = "Jones"}
                };
                IEnumerable<string> facultyIds = facultyDtos.Select(f => f.Id);

                var serializedResponse = JsonConvert.SerializeObject(facultyDtos.AsEnumerable());

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var criteria = new FacultyQueryCriteria() { FacultyIds = facultyIds };
                var clientResponse = client.QueryFaculty(criteria);

                // Assert
                Assert.AreEqual(facultyDtos.Count(), clientResponse.Count());
                foreach (var id in facultyIds)
                {
                    var facultyDto = facultyDtos.Where(f => f.Id == id).First();
                    var facultyResponse = clientResponse.Where(f => f.Id == id).First();

                    Assert.AreEqual(facultyDto.Id, facultyResponse.Id);
                    Assert.AreEqual(facultyDto.LastName, facultyResponse.LastName);
                }
            }
        }

        [TestClass]
        public class QueryRequirements
        {

            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _studentId = "123456";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public void QueryRequirements_ReturnsSerializedRequirements()
            {
                // Arrange
                var requirementDtos = new List<Requirement>()
                {
                    new Requirement() {Id = "1", Code = "REQ1"},
                    new Requirement() {Id = "2", Code = "REQ2"}
                };
                List<string> requirementIds = requirementDtos.Select(r => r.Id).ToList();

                var serializedResponse = JsonConvert.SerializeObject(requirementDtos.AsEnumerable());

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.QueryRequirements(requirementIds);

                // Assert
                Assert.AreEqual(requirementDtos.Count(), clientResponse.Count());
                foreach (var id in requirementIds)
                {
                    var requirementDto = requirementDtos.Where(r => r.Id == id).First();
                    var requirementResponse = clientResponse.Where(r => r.Id == id).First();

                    Assert.AreEqual(requirementDto.Id, requirementResponse.Id);
                    Assert.AreEqual(requirementDto.Code, requirementResponse.Code);
                }
            }
        }

        [TestClass]
        public class AcademicHistory
        {

            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task AcademicHistory_GetAcademicHistory3Async()
            {
                // Arrange
                var id = "0001234";
                var academicHistory3 = new AcademicHistory3()
                {
                    AcademicTerms = new List<AcademicTerm3>()
                        {
                             new AcademicTerm3(),
                             new AcademicTerm3()
                        },
                    NonTermAcademicCredits = new List<AcademicCredit2>()
                        {
                             new AcademicCredit2(),
                             new AcademicCredit2(),
                             new AcademicCredit2()
                        },
                    GradeRestriction = new GradeRestriction(),
                    TotalCreditsCompleted = 3,
                    OverallGradePointAverage = (decimal)3.4,
                    StudentId = id
                };

                var serializedResponse = JsonConvert.SerializeObject(academicHistory3);
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);
                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetAcademicHistory3Async(id);

                //Assert
                Assert.IsInstanceOfType(clientResponse, typeof(AcademicHistory3));
                Assert.AreEqual(id, clientResponse.StudentId);
                Assert.AreEqual(academicHistory3.AcademicTerms.Count(), clientResponse.AcademicTerms.Count());
                Assert.AreEqual(academicHistory3.NonTermAcademicCredits.Count(), clientResponse.NonTermAcademicCredits.Count());
                Assert.AreEqual(academicHistory3.TotalCreditsCompleted, clientResponse.TotalCreditsCompleted);
                Assert.AreEqual(academicHistory3.OverallGradePointAverage, clientResponse.OverallGradePointAverage);
                Assert.AreEqual((decimal)3.4, clientResponse.OverallGradePointAverage);
            }
        }

        [TestClass]
        public class GetFacultyPermissions
        {

            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public void GetFacultyPermissions_ReturnsSerializedPermissions()
            {
                // Arrange
                var permissions = new List<string>()
                {
                    "Permission1",
                    "Permission2"
                };

                var serializedResponse = JsonConvert.SerializeObject(permissions.AsEnumerable());

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.GetFacultyPermissions();

                // Assert
                Assert.AreEqual(permissions.Count(), clientResponse.Count());
                foreach (var item in permissions)
                {
                    Assert.IsTrue(clientResponse.Contains(item));
                }
            }
        }

        [TestClass]
        public class GetSectionWaivers
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public void Client_GetSectionStudentWaivers_ReturnsSerializedWaivers()
            {
                // Arrange
                var waivers = new List<Dtos.Student.StudentWaiver>()
                {
                    new StudentWaiver() {Id = "1", StudentId = "STU1", SectionId = "SEC+1", ReasonCode = "OTH", RequisiteWaivers = new List<RequisiteWaiver>() {new RequisiteWaiver() {RequisiteId = "R1", Status = WaiverStatus.Waived}}},
                    new StudentWaiver() {Id = "2", StudentId = "STU2", SectionId = "SEC+1", ReasonCode = "OTH", RequisiteWaivers = new List<RequisiteWaiver>()}
                };

                var serializedResponse = JsonConvert.SerializeObject(waivers.AsEnumerable());

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.GetSectionStudentWaivers("SEC+1");

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.AreEqual(waivers.Count(), clientResponse.Count());
                foreach (var item in waivers)
                {
                    var waiver = clientResponse.Where(w => w.Id == item.Id).FirstOrDefault();
                    Assert.IsNotNull(waiver);
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void Client_GetSectionStudentWaivers_ThrowsExceptionForNullSectionId()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.GetSectionStudentWaivers("");
            }
        }

        [TestClass]
        public class AddWaiver
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public void Client_AddWaiver_ReturnsNewWaiver()
            {
                // Arrange
                var waiver = new Dtos.Student.StudentWaiver()
                {
                    StudentId = "STU1",
                    SectionId = "SEC1",
                    ReasonCode = "OTH",
                    RequisiteWaivers = new List<RequisiteWaiver>()
                        {
                            new RequisiteWaiver() {RequisiteId = "R1", Status = WaiverStatus.Waived}
                        }
                };

                var serializedResponse = JsonConvert.SerializeObject(waiver);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.AddStudentWaiver(waiver);

                // Assert that the expected item is found in the response
                Assert.AreEqual(waiver.ReasonCode, clientResponse.ReasonCode);
                Assert.AreEqual(waiver.StudentId, clientResponse.StudentId);
                Assert.AreEqual(waiver.SectionId, clientResponse.SectionId);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void Client_AddWaiver_ThrowsExceptionForNullWaiver()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.AddStudentWaiver(null);
            }
        }

        [TestClass]
        public class GetStudentWaiverReasons
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public void Client_GetStudentWaiverReasons_ReturnsSerializedWaiverReasons()
            {
                // Arrange
                var waiverReasons = new List<Dtos.Student.StudentWaiverReason>()
                {
                    new StudentWaiverReason() {Code = "LIFE", Description = "Life Experience"},
                    new StudentWaiverReason() {Code = "OTHER", Description = "Other reason"}
                };

                var serializedResponse = JsonConvert.SerializeObject(waiverReasons.AsEnumerable());

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.GetStudentWaiverReasons();

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.AreEqual(waiverReasons.Count(), clientResponse.Count());
                foreach (var item in waiverReasons)
                {
                    var waiverReason = clientResponse.Where(w => w.Code == item.Code).FirstOrDefault();
                    Assert.IsNotNull(waiverReason);
                    Assert.AreEqual(item.Code, waiverReason.Code);
                    Assert.AreEqual(item.Description, waiverReason.Description);
                }
            }
        }

        [TestClass]
        public class GetStudentWaiver
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public void Client_GetStudentWaiver_ReturnsSerializedWaiver()
            {
                // Arrange
                var waiver = new Dtos.Student.StudentWaiver()
                {
                    Id = "2",
                    StudentId = "STU1",
                    SectionId = "SEC+1",
                    ReasonCode = "OTH",
                    RequisiteWaivers = new List<RequisiteWaiver>() { new RequisiteWaiver() { RequisiteId = "R1", Status = WaiverStatus.Waived } }
                };

                var serializedResponse = JsonConvert.SerializeObject(waiver);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.GetStudentWaiver("STU1", "2");

                // Assert that the expected item is returned
                Assert.IsNotNull(clientResponse);
                Assert.AreEqual(waiver.Id, clientResponse.Id);
                Assert.AreEqual(waiver.StudentId, clientResponse.StudentId);
                Assert.AreEqual(waiver.SectionId, clientResponse.SectionId);
                Assert.AreEqual(waiver.RequisiteWaivers.ElementAt(0).RequisiteId, clientResponse.RequisiteWaivers.ElementAt(0).RequisiteId);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void Client_GetWaiver_ThrowsExceptionForNullWaiverId()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.GetStudentWaiver(null, null);
            }
        }

        [TestClass]
        public class QuerySectionRegistrationDates
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public void Client_GetSectionRegistrationDates_ReturnsSerializedDateObject()
            {
                // Arrange
                var dates = new List<Dtos.Student.SectionRegistrationDate>();
                var sectionRegistrationDate = new Dtos.Student.SectionRegistrationDate()
                {
                    SectionId = "section1",
                    RegistrationStartDate = DateTime.Today,
                    RegistrationEndDate = DateTime.Today.AddDays(1),
                    PreRegistrationStartDate = DateTime.Today.AddDays(2)
                };
                dates.Add(sectionRegistrationDate);
                var sectionIds = dates.Select(d => d.SectionId);
                var serializedResponse = JsonConvert.SerializeObject(dates);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.GetSectionRegistrationDates(sectionIds);

                // Assert
                Assert.AreEqual(sectionIds.Count(), clientResponse.Count());
                foreach (var date in dates)
                {
                    var sectionRegDate = clientResponse.Where(s => s.SectionId == date.SectionId).First();
                    Assert.AreEqual(date.RegistrationStartDate, sectionRegDate.RegistrationStartDate);
                    Assert.AreEqual(date.RegistrationEndDate, sectionRegDate.RegistrationEndDate);
                    Assert.AreEqual(date.PreRegistrationStartDate, sectionRegDate.PreRegistrationStartDate);
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void Client_QuerySectionRegistrationDates_ThrowsExceptionForNullSectionIds()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.GetSectionRegistrationDates(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void Client_QuerySectionRegistrationDates_ThrowsExceptionForEmptySectionIds()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.GetSectionRegistrationDates(new List<string>());
            }
        }

        [TestClass]
        public class GetSectionPermissions
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task ClientGetSectionPermissions_ReturnsSerializedSectionPermissions()
            {
                // Arrange
                var sectionPermission = new SectionPermission();
                sectionPermission.FacultyConsents = new List<StudentPetition>()
                {
                    new StudentPetition(){Id="1",SectionId="19143"},
                     new StudentPetition(){Id="2",SectionId="19143"},
                };
                sectionPermission.StudentPetitions = new List<StudentPetition>()
                {
                    new StudentPetition(){Id="3",SectionId="19143"},
                     new StudentPetition(){Id="1",SectionId="19143"},
                };
                var serializedResponse = JsonConvert.SerializeObject(sectionPermission);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetSectionPermissionsAsync("19143");

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                //Assert.IsNotNull(clientResponse.FacultyConsents);
                //Assert.IsNotNull(clientResponse.StudentPetitions);
                //Assert.AreEqual(sectionPermission.FacultyConsents.Count(), clientResponse.FacultyConsents.Count());
                //Assert.AreEqual(sectionPermission.StudentPetitions.Count(), clientResponse.StudentPetitions.Count());
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void Client_GetSectionStudentWaivers_ThrowsExceptionForNullSectionId()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.GetSectionStudentWaivers("");
            }
        }

        [TestClass]
        public class GetPetitionStatuses
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public void Client_GetPetitionStatuses_ReturnsSerializedPetitionStatuses()
            {
                // Arrange
                var petitionStatuses = new List<Dtos.Student.PetitionStatus>()
                {
                    new PetitionStatus() {Code = "ACC", Description = "Accepted", IsGranted = true},
                    new PetitionStatus() {Code = "OTHER", Description = "Other reason"}
                };

                var serializedResponse = JsonConvert.SerializeObject(petitionStatuses.AsEnumerable());

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.GetPetitionStatuses();

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.AreEqual(petitionStatuses.Count(), clientResponse.Count());
                foreach (var item in petitionStatuses)
                {
                    var status = clientResponse.Where(w => w.Code == item.Code).FirstOrDefault();
                    Assert.IsNotNull(status);
                    Assert.AreEqual(item.Code, status.Code);
                    Assert.AreEqual(item.Description, status.Description);
                }
            }
        }

        [TestClass]
        public class GetStudentPetitionReasons
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public void Client_GetStudentPetitionReasons_ReturnsSerializedStudentPetitionReasons()
            {
                // Arrange
                var studentPetitionReasons = new List<Dtos.Student.StudentPetitionReason>()
                {
                    new StudentPetitionReason() {Code = "ICJI", Description = "I can handle it"},
                    new StudentPetitionReason() {Code = "OVMH", Description = "Over my head"}
                };

                var serializedResponse = JsonConvert.SerializeObject(studentPetitionReasons.AsEnumerable());

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.GetStudentPetitionReasons();

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.AreEqual(studentPetitionReasons.Count(), clientResponse.Count());
                foreach (var item in studentPetitionReasons)
                {
                    var studentPetitionReason = clientResponse.Where(w => w.Code == item.Code).FirstOrDefault();
                    Assert.IsNotNull(studentPetitionReason);
                    Assert.AreEqual(item.Code, studentPetitionReason.Code);
                    Assert.AreEqual(item.Description, studentPetitionReason.Description);
                }
            }
        }

        [TestClass]
        public class AddStudentPetition
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public void Client_AddStudentPetition_ReturnsNewPetition()
            {
                // Arrange
                var studentPetition = new Dtos.Student.StudentPetition()
                {
                    StudentId = "STU1",
                    SectionId = "SEC1",
                    ReasonCode = "OTH",
                    StatusCode = "A",
                    Type = Dtos.Student.StudentPetitionType.FacultyConsent,
                    DateTimeChanged = DateTime.Now,
                    UpdatedBy = "1111111",
                    Id = "9999"

                };

                var serializedResponse = JsonConvert.SerializeObject(studentPetition);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.AddStudentPetition(studentPetition);

                // Assert that the expected item is found in the response
                Assert.AreEqual(studentPetition.ReasonCode, clientResponse.ReasonCode);
                Assert.AreEqual(studentPetition.StudentId, clientResponse.StudentId);
                Assert.AreEqual(studentPetition.SectionId, clientResponse.SectionId);
                Assert.AreEqual(studentPetition.StatusCode, clientResponse.StatusCode);
                Assert.AreEqual(studentPetition.UpdatedBy, clientResponse.UpdatedBy);
                Assert.AreEqual(studentPetition.DateTimeChanged, clientResponse.DateTimeChanged);
                Assert.AreEqual(studentPetition.Type, clientResponse.Type);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void Client_AddStudentPetition_ThrowsExceptionForNullPetition()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.AddStudentPetition(null);
            }
        }

        [TestClass]
        public class GetStudentPetition
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public void Client_GetStudentPetition_ReturnsSerializedStudentPetition()
            {
                // Arrange
                var petitionId = "9999";
                var sectionId = "SectionId";
                StudentPetitionType type = StudentPetitionType.FacultyConsent;
                var studentPetition = new Dtos.Student.StudentPetition()
                {
                    StudentId = "STU1",
                    SectionId = sectionId,
                    ReasonCode = "OTH",
                    StatusCode = "A",
                    Type = type,
                    DateTimeChanged = DateTime.Now,
                    UpdatedBy = "1111111",
                    Id = petitionId
                };

                var serializedResponse = JsonConvert.SerializeObject(studentPetition);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.GetStudentPetition(petitionId, sectionId, type);

                // Assert that the expected item is found in the response
                Assert.AreEqual(studentPetition.ReasonCode, clientResponse.ReasonCode);
                Assert.AreEqual(studentPetition.StudentId, clientResponse.StudentId);
                Assert.AreEqual(studentPetition.SectionId, clientResponse.SectionId);
                Assert.AreEqual(studentPetition.StatusCode, clientResponse.StatusCode);
                Assert.AreEqual(studentPetition.UpdatedBy, clientResponse.UpdatedBy);
                Assert.AreEqual(studentPetition.DateTimeChanged, clientResponse.DateTimeChanged);
                Assert.AreEqual(studentPetition.Type, clientResponse.Type);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void Client_GetStudentPetition_ThrowsExceptionForNullPetitionId()
            {
                // Arrange
                var sectionId = "SectionId";
                StudentPetitionType type = StudentPetitionType.FacultyConsent;
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.GetStudentPetition(null, sectionId, type);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public void Client_GetStudentPetition_ThrowsExceptionForNullSectionId()
            {
                // Arrange
                var petitionId = "9999";
                StudentPetitionType type = StudentPetitionType.FacultyConsent;
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = client.GetStudentPetition(petitionId, null, type);
            }

            [TestMethod]
            public async Task Client_GetStudentPetition_ReturnsSerializedStudentPetitions()
            {
                // Arrange
                List<StudentPetition> petitions = new List<StudentPetition>();
                StudentPetitionType type = StudentPetitionType.FacultyConsent;

                var studentPetition = new Dtos.Student.StudentPetition()
                {
                    StudentId = "STU1",
                    SectionId = "SectionId",
                    ReasonCode = "OTH",
                    StatusCode = "A",
                    Type = type,
                    DateTimeChanged = DateTime.Now,
                    UpdatedBy = "1111111",
                    Id = "9999"
                };
                petitions.Add(studentPetition);
                studentPetition = new Dtos.Student.StudentPetition()
                {
                    StudentId = "STU1",
                    SectionId = "SectionId",
                    ReasonCode = "OTH",
                    StatusCode = "A",
                    Type = StudentPetitionType.StudentPetition,
                    DateTimeChanged = DateTime.Now,
                    UpdatedBy = "1111111",
                    Id = "999999"
                };
                petitions.Add(studentPetition);
                var serializedResponse = JsonConvert.SerializeObject(petitions);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = (await client.GetStudentPetitionsAsync("STU1")).ToList();

                // Assert that the expected item is found in the response
                Assert.AreEqual(petitions.Count(), clientResponse.Count());
                Assert.AreEqual(petitions[0].ReasonCode, clientResponse[0].ReasonCode);
                Assert.AreEqual(petitions[0].StudentId, clientResponse[0].StudentId);
                Assert.AreEqual(petitions[0].SectionId, clientResponse[0].SectionId);
                Assert.AreEqual(petitions[0].StatusCode, clientResponse[0].StatusCode);
                Assert.AreEqual(petitions[0].UpdatedBy, clientResponse[0].UpdatedBy);
                Assert.AreEqual(petitions[0].DateTimeChanged, clientResponse[0].DateTimeChanged);
                Assert.AreEqual(petitions[0].Type, clientResponse[0].Type);
            }
        }

        [TestClass]
        public class GetGraduationConfigurationAsync
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task ClientGetGraduationConfigurationAsync_ReturnsSerializedGraduationConfiguration()
            {
                // Arrange
                var graduationConfiguration = new GraduationConfiguration();
                graduationConfiguration.ApplyForDifferentProgramLink = "www.differentprogram.com/abc";
                graduationConfiguration.CapAndGownLink = "http:\\capandgown.com";
                graduationConfiguration.CommencementInformationLink = "someCommencementLinkText.com";
                graduationConfiguration.MaximumCommencementGuests = 5;
                graduationConfiguration.PhoneticSpellingLink = "phoneticspelling.com";
                graduationConfiguration.ApplicationQuestions = new List<GraduationQuestion>()
                {
                    new GraduationQuestion() {Type = GraduationQuestionType.DiplomaName, IsRequired = false},
                     new GraduationQuestion(){Type = GraduationQuestionType.AttendCommencement, IsRequired = true},
                };
                graduationConfiguration.GraduationTerms = new List<string>()
                {
                    "2016/SP",
                     "2016/FA"
                };
                var serializedResponse = JsonConvert.SerializeObject(graduationConfiguration);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetGraduationConfigurationAsync();

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.IsNotNull(clientResponse.CommencementInformationLink);
                Assert.IsNotNull(clientResponse.ApplyForDifferentProgramLink);
                Assert.IsNotNull(clientResponse.CapAndGownLink);
                Assert.IsNotNull(clientResponse.PhoneticSpellingLink);
                Assert.IsNotNull(clientResponse.MaximumCommencementGuests);
                Assert.AreEqual(graduationConfiguration.ApplicationQuestions.Count(), clientResponse.ApplicationQuestions.Count());
                Assert.AreEqual(graduationConfiguration.GraduationTerms.Count(), clientResponse.GraduationTerms.Count());
            }

        }

        [TestClass]
        public class GetCapSizesAsync
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task ClientGetCapSizesAsync_ReturnsSerializedCapSizes()
            {
                // Arrange
                var capSizes = new List<CapSize>()
                    {
                        new CapSize(){Code="SMALL",Description="Small"},
                        new CapSize(){Code="MEDIUM",Description="Meduium"},
                        new CapSize(){Code="LARGE",Description="Large"}
                    };
                var serializedResponse = JsonConvert.SerializeObject(capSizes);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetCapSizesAsync();

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.AreEqual(capSizes.Count(), clientResponse.Count());
                foreach (var cap in clientResponse)
                {
                    Assert.IsNotNull(cap.Code);
                    Assert.IsNotNull(cap.Description);
                    var capSize = capSizes.Where(c => c.Code == cap.Code).FirstOrDefault();
                    Assert.AreEqual(capSize.Code, cap.Code);
                    Assert.AreEqual(capSize.Description, cap.Description);
                }
            }

        }

        [TestClass]
        public class GetGownSizesAsync
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task ClientGetGownSizesAsync_ReturnsSerializedGownSizes()
            {
                // Arrange
                var gownSizes = new List<GownSize>()
                    {
                        new GownSize(){Code="SMALL",Description="Small"},
                        new GownSize(){Code="MEDIUM",Description="Meduium"},
                        new GownSize(){Code="LARGE",Description="Large"}
                    };
                var serializedResponse = JsonConvert.SerializeObject(gownSizes);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetGownSizesAsync();

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.AreEqual(gownSizes.Count(), clientResponse.Count());
                foreach (var gown in clientResponse)
                {
                    Assert.IsNotNull(gown.Code);
                    Assert.IsNotNull(gown.Description);
                    var gownSize = gownSizes.Where(c => c.Code == gown.Code).FirstOrDefault();
                    Assert.AreEqual(gownSize.Code, gown.Code);
                    Assert.AreEqual(gownSize.Description, gown.Description);
                }
            }

        }


        [TestClass]
        public class GetGraduationApplicationAsync
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task GetGraduationApplicationAsync_ReturnsSerializedGraduationApplication()
            {
                // Arrange
                var graduationApplication = new GraduationApplication();
                graduationApplication.StudentId = "000011";
                graduationApplication.ProgramCode = "MATH.BA";
                graduationApplication.GraduationTerm = "2015/FA";

                var serializedGraduationApplication = JsonConvert.SerializeObject(graduationApplication);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedGraduationApplication, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.RetrieveGraduationApplicationAsync("000011", "MATH.BA");

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.AreEqual(graduationApplication.StudentId, clientResponse.StudentId);
                Assert.AreEqual(graduationApplication.ProgramCode, clientResponse.ProgramCode);
            }

            [TestMethod]
            public async Task GetGraduationApplicationAsync_ReturnsSerializedGraduationApplications()
            {
                // Arrange
                var graduationApplications = new List<GraduationApplication>()
                    {
                        new GraduationApplication(){StudentId="000011",ProgramCode="MATH.BA", GraduationTerm="2015/FA"},
                        new GraduationApplication(){StudentId="000011",ProgramCode="ENGL.BA", GraduationTerm="2016/FA"},
                        new GraduationApplication(){StudentId="000011",ProgramCode="ACCT.BA", GraduationTerm="2017/FA"}
                    };
                var serializedResponse = JsonConvert.SerializeObject(graduationApplications);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.RetrieveGraduationApplicationsAsync("000011");

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.AreEqual(graduationApplications.Count(), clientResponse.Count());
                foreach (var application in clientResponse)
                {
                    Assert.IsNotNull(application.StudentId);
                    Assert.IsNotNull(application.ProgramCode);
                    var gradApp = graduationApplications.Where(c => c.StudentId == application.StudentId && c.ProgramCode == application.ProgramCode).FirstOrDefault();
                    Assert.AreEqual(gradApp.StudentId, application.StudentId);
                    Assert.AreEqual(gradApp.ProgramCode, application.ProgramCode);
                }
            }

        }

        [TestClass]
        public class UpdateGraduationApplicationAsync
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task UpdateGraduationApplicationAsync_ReturnsSerializedGraduationApplication()
            {
                // Arrange
                var graduationApplication = new GraduationApplication();
                graduationApplication.StudentId = "StudentId";
                graduationApplication.ProgramCode = "ProgramCode";
                graduationApplication.GraduationTerm = "TermCode";

                var serializedGraduationApplication = JsonConvert.SerializeObject(graduationApplication);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedGraduationApplication, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.UpdateGraduationApplicationAsync(graduationApplication);

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.AreEqual(graduationApplication.StudentId, clientResponse.StudentId);
                Assert.AreEqual(graduationApplication.ProgramCode, clientResponse.ProgramCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task UpdateGraduationApplicationAsync_ApplicationNull()
            {
                // Arrange
                var mockHandler = new MockHandler();

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.UpdateGraduationApplicationAsync(null);

            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task UpdateGraduationApplicationAsync_ApplicationHasEmptyStudentId()
            {
                // Arrange
                var graduationApplication = new GraduationApplication();
                graduationApplication.StudentId = string.Empty;
                graduationApplication.ProgramCode = "ProgramCode";
                graduationApplication.GraduationTerm = "TermCode";
                var mockHandler = new MockHandler();

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.UpdateGraduationApplicationAsync(graduationApplication);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task UpdateGraduationApplicationAsync_ApplicationHasNullStudentId()
            {
                // Arrange
                var graduationApplication = new GraduationApplication();
                graduationApplication.StudentId = null;
                graduationApplication.ProgramCode = "ProgramCode";
                graduationApplication.GraduationTerm = "TermCode";
                var mockHandler = new MockHandler();

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.UpdateGraduationApplicationAsync(graduationApplication);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task UpdateGraduationApplicationAsync_ApplicationHasEmptyProgramCode()
            {
                // Arrange
                var graduationApplication = new GraduationApplication();
                graduationApplication.StudentId = "StudentId";
                graduationApplication.ProgramCode = string.Empty;
                graduationApplication.GraduationTerm = "TermCode";
                var mockHandler = new MockHandler();

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.UpdateGraduationApplicationAsync(graduationApplication);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentException))]
            public async Task UpdateGraduationApplicationAsync_ApplicationHasNullProgramCode()
            {
                // Arrange
                var graduationApplication = new GraduationApplication();
                graduationApplication.StudentId = "StudentId";
                graduationApplication.ProgramCode = null;
                graduationApplication.GraduationTerm = "TermCode";
                var mockHandler = new MockHandler();

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.UpdateGraduationApplicationAsync(graduationApplication);
            }
        }

        [TestClass]
        public class GetGraduationApplicationFeeAsync
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task GetGraduationApplicationFeeAsync_ReturnsSerializedGraduationApplicationFee()
            {
                // Arrange
                var graduationApplicationFee = new GraduationApplicationFee();
                graduationApplicationFee.StudentId = "000011";
                graduationApplicationFee.ProgramCode = "MATH.BA";
                graduationApplicationFee.PaymentDistributionCode = "DIST";
                graduationApplicationFee.Amount = 30m;

                var serializedGraduationApplication = JsonConvert.SerializeObject(graduationApplicationFee);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedGraduationApplication, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetGraduationApplicationFeeAsync("000011", "MATH.BA");

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.AreEqual(graduationApplicationFee.StudentId, clientResponse.StudentId);
                Assert.AreEqual(graduationApplicationFee.ProgramCode, clientResponse.ProgramCode);
                Assert.AreEqual(graduationApplicationFee.Amount, clientResponse.Amount);
                Assert.AreEqual(graduationApplicationFee.PaymentDistributionCode, clientResponse.PaymentDistributionCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task GetGraduationApplicationFeeAsync_NullStudentId()
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetGraduationApplicationFeeAsync(null, "MATH.BA");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task GetGraduationApplicationFeeAsync_EmptyStudentId()
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetGraduationApplicationFeeAsync(string.Empty, "MATH.BA");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task GetGraduationApplicationFeeAsync_NullProgramCode()
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetGraduationApplicationFeeAsync("studentId", null);
            }
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task GetGraduationApplicationFeeAsync_EmptyProgramCode()
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetGraduationApplicationFeeAsync("studentId", string.Empty);
            }
        }

        [TestClass]
        public class GetStudentPrograms2Async
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }
            [TestMethod]
            public async Task GetStudentPrograms2Async_ReturnsSerializedStudentProgram2()
            {
                // Arrange
                List<StudentProgram2> lstOfStudentPrograms = new List<StudentProgram2>()
                {
                new StudentProgram2(){StudentId = "000011",ProgramCode = "MATH.BA"},
                new StudentProgram2(){StudentId = "000011",ProgramCode = "ENG.BA"}
            };


                var serializedStudentPrograms = JsonConvert.SerializeObject(lstOfStudentPrograms);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedStudentPrograms, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentPrograms2Async("000011");

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.AreEqual(lstOfStudentPrograms.Count(), clientResponse.Count());
                foreach (var program in clientResponse)
                {
                    Assert.IsNotNull(program.StudentId);
                    Assert.IsNotNull(program.ProgramCode);
                    var programRetrieved = lstOfStudentPrograms.Where(c => c.StudentId == program.StudentId && c.ProgramCode == program.ProgramCode).FirstOrDefault();
                    Assert.AreEqual(program.StudentId, programRetrieved.StudentId);
                    Assert.AreEqual(program.ProgramCode, programRetrieved.ProgramCode);
                }
            }
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task GetStudentPrograms2Async_NullStudentId()
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetGraduationApplicationFeeAsync(null, "MATH.BA");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task GetStudentPrograms2Async_EmptyStudentId()
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetGraduationApplicationFeeAsync(string.Empty, "MATH.BA");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task GetStudentPrograms2Async_NullProgramCode()
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetGraduationApplicationFeeAsync("studentId", null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task GetStudentPrograms2Async_EmptyProgramCode()
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetGraduationApplicationFeeAsync("studentId", string.Empty);
            }

        }

        [TestClass]
        public class GetStudentWaivers
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task Client_GetStudentWaivers_ReturnsSerializedWaivers()
            {
                // Arrange
                var waivers = new List<Dtos.Student.StudentWaiver>()
                {
                    new StudentWaiver() {Id = "1", StudentId = "STU1", SectionId = "SEC+1", ReasonCode = "OTH", RequisiteWaivers = new List<RequisiteWaiver>() {new RequisiteWaiver() {RequisiteId = "R1", Status = WaiverStatus.Waived}}},
                    new StudentWaiver() {Id = "2", StudentId = "STU1", SectionId = "SEC+1", ReasonCode = "OTH", RequisiteWaivers = new List<RequisiteWaiver>()}
                };

                var serializedResponse = JsonConvert.SerializeObject(waivers.AsEnumerable());

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentWaiversAsync("STU1");

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.AreEqual(waivers.Count(), clientResponse.Count());
                foreach (var item in waivers)
                {
                    var waiver = clientResponse.Where(w => w.Id == item.Id).FirstOrDefault();
                    Assert.IsNotNull(waiver);
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task Client_GetStudentWaivers_ThrowsExceptionForNullSectionId()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentWaiversAsync("");
            }
        }

        [TestClass]
        public class GetSessionCyclesAsync
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task ClientGetSessionCyclesAsync_ReturnsSerializedSessionCycles()
            {
                // Arrange
                var sessionCycles = new List<SessionCycle>()
                    {
                        new SessionCycle(){Code="FO",Description="Fall Only"},
                        new SessionCycle(){Code="SO",Description="Spring Only"},
                        new SessionCycle(){Code="WO",Description="Winter Only"}
                    };
                var serializedResponse = JsonConvert.SerializeObject(sessionCycles);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetSessionCyclesAsync();

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.AreEqual(sessionCycles.Count(), clientResponse.Count());
                foreach (var sc in clientResponse)
                {
                    Assert.IsNotNull(sc.Code);
                    Assert.IsNotNull(sc.Description);
                    var sessionCycle = sessionCycles.Where(c => c.Code == sc.Code).FirstOrDefault();
                    Assert.AreEqual(sessionCycle.Code, sc.Code);
                    Assert.AreEqual(sessionCycle.Description, sc.Description);
                }
            }

        }

        [TestClass]
        public class GetYearlyCyclesAsync
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task ClientGetYearlyCyclesAsync_ReturnsSerializedYearlyCycles()
            {
                // Arrange
                var yearlyCycles = new List<YearlyCycle>()
                    {
                        new YearlyCycle(){Code="EY",Description="Even Years Only"},
                        new YearlyCycle(){Code="OY",Description="Odd Years Only"},
                        new YearlyCycle(){Code="LY",Description="Election Years Only"}
                    };
                var serializedResponse = JsonConvert.SerializeObject(yearlyCycles);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetYearlyCyclesAsync();

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.AreEqual(yearlyCycles.Count(), clientResponse.Count());
                foreach (var sc in clientResponse)
                {
                    Assert.IsNotNull(sc.Code);
                    Assert.IsNotNull(sc.Description);
                    var yearlyCycle = yearlyCycles.Where(c => c.Code == sc.Code).FirstOrDefault();
                    Assert.AreEqual(yearlyCycle.Code, sc.Code);
                    Assert.AreEqual(yearlyCycle.Description, sc.Description);
                }
            }

        }

        [TestClass]
        public class GetStudentRequestConfigurationAsync
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task ClientGetStudentRequestConfigurationAsync_ReturnsSerializedTranscriptRequestConfiguration()
            {
                // Arrange
                var studentRequestConfiguration = new StudentRequestConfiguration();
                studentRequestConfiguration.DefaultWebEmailType = "XXX";
                studentRequestConfiguration.SendTranscriptRequestConfirmation = true;
                studentRequestConfiguration.SendEnrollmentRequestConfirmation = true;
                var serializedResponse = JsonConvert.SerializeObject(studentRequestConfiguration);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentRequestConfigurationAsync();

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.IsTrue(clientResponse.SendTranscriptRequestConfirmation);
                Assert.IsTrue(clientResponse.SendEnrollmentRequestConfirmation);
                Assert.AreEqual("XXX", clientResponse.DefaultWebEmailType);

            }

        }

        [TestClass]
        public class GetHoldRequestTypesAsync
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task ClientGetHoldRequestTypesAsync_ReturnsSerializedHoldRequestTypes()
            {
                // Arrange
                var holdRequestTypes = new List<HoldRequestType>()
                    {
                        new HoldRequestType(){Code="GRADE",Description="Hold For Grades"},
                        new HoldRequestType(){Code="OTHER",Description="Other"},
                    };
                var serializedResponse = JsonConvert.SerializeObject(holdRequestTypes);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetHoldRequestTypesAsync();

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.AreEqual(holdRequestTypes.Count(), clientResponse.Count());
                foreach (var hrt in clientResponse)
                {
                    Assert.IsNotNull(hrt.Code);
                    Assert.IsNotNull(hrt.Description);
                    var holdRequestType = holdRequestTypes.Where(c => c.Code == hrt.Code).FirstOrDefault();
                    Assert.AreEqual(holdRequestType.Code, hrt.Code);
                    Assert.AreEqual(holdRequestType.Description, hrt.Description);
                }
            }

        }

        [TestClass]
        public class AddStudentTranscriptRequest
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task Client_AddTranscriptRequest_ReturnsNewTranscriptRequest()
            {
                // Arrange
                var request = new Dtos.Student.StudentTranscriptRequest("STU1", "my name", new List<string> { "line 1" })
                {
                    HoldRequest = "GRADE",
                    NumberOfCopies = 3,
                    TranscriptGrouping = "UG"
                };

                var serializedResponse = JsonConvert.SerializeObject(request);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.AddStudentTranscriptRequestAsync(request);

                // Assert that the expected item is found in the response
                Assert.AreEqual(request.TranscriptGrouping, clientResponse.TranscriptGrouping);
                Assert.AreEqual(request.HoldRequest, clientResponse.HoldRequest);
                Assert.AreEqual(request.StudentId, clientResponse.StudentId);
                Assert.AreEqual(request.RecipientName, clientResponse.RecipientName);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task Client_AddTranscriptRequest_ThrowsExceptionForNullRequest()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.AddStudentTranscriptRequestAsync(null);
            }
        }


        [TestClass]
        public class AddStudentEnrollmentRequest
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task Client_AddEnrollmentRequest_ReturnsNewEnrollmentRequest()
            {
                // Arrange
                var request = new Dtos.Student.StudentEnrollmentRequest("STU1", "my name", new List<string>() { "address line 1" })
                {
                    HoldRequest = "GRADE",
                    NumberOfCopies = 3
                };

                var serializedResponse = JsonConvert.SerializeObject(request);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.AddStudentEnrollmentRequestAsync(request);

                // Assert that the expected item is found in the response
                Assert.AreEqual(request.HoldRequest, clientResponse.HoldRequest);
                Assert.AreEqual(request.StudentId, clientResponse.StudentId);
                Assert.AreEqual(request.RecipientName, clientResponse.RecipientName);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task Client_AddEnrollmentRequest_ThrowsExceptionForNullRequest()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.AddStudentEnrollmentRequestAsync(null);
            }
        }

        [TestClass]
        public class GetStudentTranscriptRequest
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task Client_GetStudentTranscriptRequest_ReturnsSerializedRequest()
            {
                // Arrange
                var request = new Dtos.Student.StudentTranscriptRequest("STU1", "my name", new List<string>() { "address line 1" })
                {
                    Id = "2",
                    HoldRequest = "GRADE",
                    NumberOfCopies = 3,
                    TranscriptGrouping = "UG"
                };

                var serializedResponse = JsonConvert.SerializeObject(request);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentTranscriptRequestAsync("2");

                // Assert that the expected item is returned
                Assert.IsNotNull(clientResponse);
                Assert.AreEqual(request.Id, clientResponse.Id);
                Assert.AreEqual(request.StudentId, clientResponse.StudentId);
                Assert.AreEqual(request.RecipientName, clientResponse.RecipientName);
                Assert.AreEqual(request.TranscriptGrouping, clientResponse.TranscriptGrouping);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task Client_GetStudentTranscriptRequest_ThrowsExceptionForNullRequestId()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentTranscriptRequestAsync(null);
            }
        }

        [TestClass]
        public class GetStudentEnrollmentRequest
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task Client_GetStudentEnrollmentRequest_ReturnsSerializedRequest()
            {
                // Arrange
                var request = new Dtos.Student.StudentEnrollmentRequest("STU1", "my name", new List<string>() { "address line 1" })
                {
                    Id = "2",
                    HoldRequest = "GRADE",
                    NumberOfCopies = 3
                };

                var serializedResponse = JsonConvert.SerializeObject(request);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentEnrollmentRequestAsync("2");

                // Assert that the expected item is returned
                Assert.IsNotNull(clientResponse);
                Assert.AreEqual(request.Id, clientResponse.Id);
                Assert.AreEqual(request.StudentId, clientResponse.StudentId);
                Assert.AreEqual(request.RecipientName, clientResponse.RecipientName);
                Assert.AreEqual(request.HoldRequest, clientResponse.HoldRequest);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task Client_GetStudentEnrollmentRequest_ThrowsExceptionForNullRequestId()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentEnrollmentRequestAsync(null);
            }
        }

        [TestClass]
        public class GetStudentEnrollmentRequests
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task Client_GetStudentEnrollmentRequests_ReturnsSerializedRequest()
            {
                // Arrange
                List<Dtos.Student.StudentEnrollmentRequest> enrollRequests = new List<Dtos.Student.StudentEnrollmentRequest>();
                var request1 = new Dtos.Student.StudentEnrollmentRequest("STU1", "my name", new List<string>() { "address line 1" })
                {
                    Id = "2",
                    Comments = "Something",
                    NumberOfCopies = 1
                };
                enrollRequests.Add(request1);
                var request2 = new Dtos.Student.StudentEnrollmentRequest("STU1", "my name", new List<string>() { "address line 2" })
                {
                    Id = "4",
                    Comments = "Anything",
                    NumberOfCopies = 3
                };
                enrollRequests.Add(request2);

                var serializedResponse = JsonConvert.SerializeObject(enrollRequests);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentEnrollmentRequestsAsync("STU1");

                // Assert that the expected items are returned
                Assert.IsNotNull(clientResponse);
                foreach (var request in enrollRequests)
                {
                    var responseItem = clientResponse.Where(cr => cr.Id == request.Id).FirstOrDefault();
                    Assert.IsNotNull(responseItem);
                    Assert.AreEqual(request.StudentId, responseItem.StudentId);
                    Assert.AreEqual(request.RecipientName, responseItem.RecipientName);
                    Assert.AreEqual(request.MailToAddressLines.ElementAt(0), responseItem.MailToAddressLines.ElementAt(0));
                    Assert.AreEqual(request.Comments, responseItem.Comments);
                }

            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task Client_GetStudentEnrollmentRequests_ThrowsExceptionForNullRequestId()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentEnrollmentRequestsAsync(null);
            }
        }

        [TestClass]
        public class GetStudentTranscriptRequests
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task Client_GetStudentTransriptRequests_ReturnsSerializedRequest()
            {
                // Arrange
                List<Dtos.Student.StudentTranscriptRequest> transcriptRequests = new List<Dtos.Student.StudentTranscriptRequest>();
                var request1 = new Dtos.Student.StudentTranscriptRequest("STU1", "my name", new List<string>() { "address line 1" })
                {
                    Id = "2",
                    Comments = "Something",
                    NumberOfCopies = 1,
                    TranscriptGrouping = "GR"
                };
                transcriptRequests.Add(request1);
                var request2 = new Dtos.Student.StudentTranscriptRequest("STU1", "my name", new List<string>() { "address line 2" })
                {
                    Id = "4",
                    Comments = "Anything",
                    NumberOfCopies = 3,
                    TranscriptGrouping = "UG"
                };
                transcriptRequests.Add(request2);

                var serializedResponse = JsonConvert.SerializeObject(transcriptRequests);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentTranscriptRequestsAsync("STU1");

                // Assert that the expected items are returned
                Assert.IsNotNull(clientResponse);
                foreach (var request in transcriptRequests)
                {
                    var responseItem = clientResponse.Where(cr => cr.Id == request.Id).FirstOrDefault();
                    Assert.IsNotNull(responseItem);
                    Assert.AreEqual(request.StudentId, responseItem.StudentId);
                    Assert.AreEqual(request.RecipientName, responseItem.RecipientName);
                    Assert.AreEqual(request.MailToAddressLines.ElementAt(0), responseItem.MailToAddressLines.ElementAt(0));
                    Assert.AreEqual(request.Comments, responseItem.Comments);
                }

            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task Client_GetStudentTranscriptRequests_ThrowsExceptionForNullRequestId()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentTranscriptRequestsAsync(null);
            }
        }

        [TestClass]
        public class GetStudentRequestFeeAsync
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task GetStudentRequestFeeAsync_ReturnsSerializedStudentRequestFee()
            {
                // Arrange
                var studentRequestFee = new StudentRequestFee();
                studentRequestFee.StudentId = "000011";
                studentRequestFee.RequestId = "12345";
                studentRequestFee.PaymentDistributionCode = "DIST";
                studentRequestFee.Amount = 30m;

                var serializedStudentRequest = JsonConvert.SerializeObject(studentRequestFee);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedStudentRequest, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentRequestFeeAsync("000011", "12345");

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.AreEqual(studentRequestFee.StudentId, clientResponse.StudentId);
                Assert.AreEqual(studentRequestFee.RequestId, clientResponse.RequestId);
                Assert.AreEqual(studentRequestFee.Amount, clientResponse.Amount);
                Assert.AreEqual(studentRequestFee.PaymentDistributionCode, clientResponse.PaymentDistributionCode);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task GetStudentRequestFeeAsync_NullStudentId()
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentRequestFeeAsync(null, "12345");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task GetStudentRequestFeeAsync_EmptyStudentId()
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentRequestFeeAsync(string.Empty, "12345");
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task GetStudentRequestFeeAsync_NullProgramCode()
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentRequestFeeAsync("studentId", null);
            }
            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task GetStudentRequestFeeAsync_EmptyProgramCode()
            {
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentRequestFeeAsync("studentId", string.Empty);
            }
        }


        [TestClass]
        public class QueryAcademicCreditsAsync
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task Client_QueryAcademicCreditsAsync_ReturnsSerializedRequest()
            {
                // Arrange
                List<Dtos.Student.AcademicCredit2> academicCredits = new List<Dtos.Student.AcademicCredit2>();
                var credit1 = new Dtos.Student.AcademicCredit2()
                {
                    Id = "1",
                    CourseId = "100",
                    Credit = 3.0m,
                    Status = CreditStatus.Add.ToString(),
                    SectionId = "3000",
                    StudentId = "1111111"

                };
                academicCredits.Add(credit1);
                var credit2 = new Dtos.Student.AcademicCredit2()
                {
                    Id = "2",
                    CourseId = "100",
                    Credit = 3.0m,
                    Status = CreditStatus.Dropped.ToString(),
                    SectionId = "3000",
                    StudentId = "1111112"

                };
                academicCredits.Add(credit2);

                AcademicCreditQueryCriteria criteria = new AcademicCreditQueryCriteria() { SectionIds = new List<string>() { "3000" } };

                var serializedResponse = JsonConvert.SerializeObject(academicCredits);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.QueryAcademicCreditsAsync(criteria);

                // Assert that the expected items are returned
                Assert.IsNotNull(clientResponse);
                foreach (var credit in academicCredits)
                {
                    var responseItem = clientResponse.Where(cr => cr.Id == credit.Id).FirstOrDefault();
                    Assert.IsNotNull(responseItem);
                    Assert.AreEqual(credit.StudentId, responseItem.StudentId);
                    Assert.AreEqual(credit.CourseId, responseItem.CourseId);
                    Assert.AreEqual(credit.Credit, responseItem.Credit);
                    Assert.AreEqual(credit.SectionId, responseItem.SectionId);
                    Assert.AreEqual(credit.Status, responseItem.Status);
                }

            }

        }

        [TestClass]
        public class GetFacultyGradingConfigurationAsync
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task ClientGetFacultyGradingConfigurationAsync_ReturnsSerializedFacultyGradingConfiguration()
            {
                // Arrange
                var facultyGradingConfiguration = new FacultyGradingConfiguration();
                facultyGradingConfiguration.IncludeCrosslistedStudents = true;
                facultyGradingConfiguration.IncludeDroppedWithdrawnStudents = true;
                facultyGradingConfiguration.AllowedGradingTerms = new List<string>()
                {
                    "2016/SP",
                     "2016/FA"
                };
                var serializedResponse = JsonConvert.SerializeObject(facultyGradingConfiguration);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetFacultyGradingConfigurationAsync();

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.IsTrue(clientResponse.IncludeCrosslistedStudents);
                Assert.IsTrue(clientResponse.IncludeDroppedWithdrawnStudents);
                Assert.AreEqual(facultyGradingConfiguration.AllowedGradingTerms.Count(), clientResponse.AllowedGradingTerms.Count());
            }

        }

        [TestClass]
        public class GetTranscriptRestrictions2Async
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task ClientGetTranscriptRestrictions2Async_ReturnsSerializedTranscriptAccess()
            {
                // Arrange
                var studentId = "studentId";
                var transcriptAccess = new TranscriptAccess();
                transcriptAccess.EnforceTranscriptRestriction = true;
                var ta1 = new TranscriptRestriction() { Code = "BH", Description = "Business Office Hold" };
                var ta2 = new TranscriptRestriction() { Code = "PF", Description = "Parking Fine" };
                transcriptAccess.TranscriptRestrictions = new List<TranscriptRestriction>() { ta1, ta2 };

                var serializedResponse = JsonConvert.SerializeObject(transcriptAccess);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetTranscriptRestrictions2Async(studentId);

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.IsTrue(clientResponse.EnforceTranscriptRestriction);
                Assert.AreEqual(transcriptAccess.TranscriptRestrictions.Count(), clientResponse.TranscriptRestrictions.Count());
            }

        }

        [TestClass]
        public class GetTestResult2Async
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task GetTestResult2Async_ReturnsSerializedTestResults()
            {
                // Arrange
                var studentId = "studentId";
                var tr1 = new TestResult2() { Code = "SAT", Description = "SAT Test", Score = 10.25m, StudentId = "studentId", Category = TestType.Admissions, DateTaken = DateTime.Now.AddDays(-30) };
                var tr2 = new TestResult2() { Code = "MATH", Description = "Math Placement Test", Score = 1.333m, StudentId = "studentId", Category = TestType.Placement, DateTaken = DateTime.Now.AddDays(-30) };
                var testResults = new List<TestResult2>() { tr1, tr2 };

                var serializedResponse = JsonConvert.SerializeObject(testResults);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetTestResults2Async(studentId, null);

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.AreEqual(testResults.Count(), clientResponse.Count());
                Assert.IsInstanceOfType(clientResponse.ElementAt(0), typeof(TestResult2));
            }

        }

        [TestClass]
        public class GetCourseCatalogConfigurationAsync
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task ClientGetCourseCatalogConfigurationAsync_ReturnsSerializedCourseCatalogConfiguration()
            {
                // Arrange
                var searchStartDate = DateTime.Now.AddDays(-180);
                var searchEndDate = DateTime.Now.AddDays(180);


                var courseCatalogConfiguration = new CourseCatalogConfiguration();
                courseCatalogConfiguration.EarliestSearchDate = searchStartDate;
                courseCatalogConfiguration.LatestSearchDate = searchEndDate;
                var serializedResponse = JsonConvert.SerializeObject(courseCatalogConfiguration);
                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetCourseCatalogConfigurationAsync();

                // Assert that the expected number of items is returned and each of the expected items is found in the response
                Assert.IsNotNull(clientResponse);
                Assert.AreEqual(searchStartDate, clientResponse.EarliestSearchDate);
                Assert.AreEqual(searchEndDate, clientResponse.LatestSearchDate);

            }

        }
        [TestClass]
        public class QueryStudentsById4AsyncTests
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task Client_QueryStudentsById4Async_ReturnsSerializedObject()
            {
                // Arrange
                var students = new List<Dtos.Student.StudentBatch3>();
                var student = new Dtos.Student.StudentBatch3()
                {
                    Id = "10000",
                    FirstName = "FirstName",
                    LastName = "LastName",
                    MiddleName = "M",
                    PreferredEmailAddress = "firstname@yahoo.com",
                    PersonDisplayName = new PersonHierarchyName() { FullName = "DisplayName", LastName = "Last", HierarchyCode = "PREFERRED" },
                    StudentRestrictionIds = new List<string>() { "111", "222" },
                    IsConfidential = false,
                    IsLegacyStudent = true,
                    IsTransfer = false,
                    BirthDate = DateTime.Now.AddYears(-20),


                };
                students.Add(student);
                var studentIds = students.Select(d => d.Id);
                var serializedResponse = JsonConvert.SerializeObject(students);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.QueryStudentsById4Async(studentIds);

                // Assert
                Assert.AreEqual(students.Count(), clientResponse.Count());
                foreach (var stuBatch in students)
                {
                    var batchResponse = clientResponse.Where(s => s.Id == student.Id).First();
                    Assert.AreEqual(student.FirstName, batchResponse.FirstName);
                    Assert.AreEqual(student.LastName, batchResponse.LastName);
                    Assert.AreEqual(student.MiddleName, batchResponse.MiddleName);
                    Assert.AreEqual(student.PreferredEmailAddress, batchResponse.PreferredEmailAddress);
                    Assert.IsNotNull(batchResponse.PersonDisplayName);
                    Assert.AreEqual(student.IsLegacyStudent, batchResponse.IsLegacyStudent);
                    Assert.AreEqual(student.StudentRestrictionIds.Count(), batchResponse.StudentRestrictionIds.Count());

                }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task Client_QueryStudentsById4Async__ThrowsExceptionForNullStudentIds()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.QueryStudentsById4Async(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task Client_QueryStudentsById4Async_ThrowsExceptionForEmptyStudentIds()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.QueryStudentsById4Async(new List<string>());
            }
        }

        [TestClass]
        public class GetStudentRestrictions2Async
        {
            private const string _serviceUrl = "http://service.url";
            private const string _contentType = "application/json";
            private const string _token = "1234567890";

            private Mock<ILogger> _loggerMock;
            private ILogger _logger;

            [TestInitialize]
            public void Initialize()
            {
                _loggerMock = MockLogger.Instance;

                _logger = _loggerMock.Object;
            }

            [TestMethod]
            public async Task Client_GetStudentRestrictions2Async_ReturnsSerializedObject()
            {
                // Arrange
                var restrictions = new List<PersonRestriction>
                {
                    new PersonRestriction()
                    {
                        Id = "PR1",
                        Title = "Person Restriction 1"
                    }
                };
                var serializedResponse = JsonConvert.SerializeObject(restrictions);

                var response = new HttpResponseMessage(HttpStatusCode.OK);
                response.Content = new StringContent(serializedResponse, Encoding.UTF8, _contentType);
                var mockHandler = new MockHandler();
                mockHandler.Responses.Enqueue(response);

                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentRestrictions2Async("0000001");

                // Assert
                Assert.AreEqual(restrictions.Count(), clientResponse.Count());
                foreach (var restriction in restrictions)
                {
                    var batchResponse = clientResponse.Where(r => r.Id == restriction.Id).First();
                    Assert.AreEqual(restriction.Id, batchResponse.Id);
                    Assert.AreEqual(restriction.Title, batchResponse.Title);
                }
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task Client_GetStudentRestrictions2Async__ThrowsExceptionForNullStudentIds()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentRestrictions2Async(null);
            }

            [TestMethod]
            [ExpectedException(typeof(ArgumentNullException))]
            public async Task Client_GetStudentRestrictions2Async_ThrowsExceptionForEmptyStudentId()
            {
                // Arrange
                var mockHandler = new MockHandler();
                var testHttpClient = new HttpClient(mockHandler);
                testHttpClient.BaseAddress = new Uri(_serviceUrl);

                var client = new ColleagueApiClient(testHttpClient, _logger);

                // Act
                var clientResponse = await client.GetStudentRestrictions2Async(string.Empty);
            }

        }
    }

}

