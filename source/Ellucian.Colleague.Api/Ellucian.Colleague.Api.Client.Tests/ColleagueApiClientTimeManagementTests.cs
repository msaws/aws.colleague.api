﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Dtos.Base;
using Ellucian.Colleague.Dtos.TimeManagement;
using Ellucian.Web.Http.TestUtil;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Api.Client.Tests
{
     [TestClass]
     public class ColleagueApiClientTimeManagementTests
     {
          public const string contentType = "application/json";
          public const string serviceUrl = "http://service.url";
          public const string employeeId = "24601";

          private Mock<ILogger> loggerMock;

          [TestInitialize]
          public void Initialize()
          {
               loggerMock = new Mock<ILogger>();
          }

          #region Timecards

          [TestMethod]
          public async Task Timecards_GetTimecardsAsynctTest()
          {
               var responseObj = new List<Timecard>()
            {
                new Timecard()
                {
                    Id = "001",
                    EmployeeId = "24601",
                    PayCycleId = "001",
                    PeriodEndDate = new DateTime(2016, 03, 31),
                    PeriodStartDate = new DateTime(2016, 02, 29),
                    PositionId = "001",
                    Timestamp = new Timestamp(),
                    TimeEntries = new List<TimeEntry>() 
                    { 
                        new TimeEntry()
                        {
                            Id = "A",
                            EarningsTypeId = "B",
                            InDateTime = new DateTime(2016, 03, 21, 09,00,00),
                            OutDateTime = new DateTime(2016, 03, 21, 17,00,00),
                            PersonLeaveId = "C",
                            ProjectId = "D",
                            TimecardId = "001",
                            Timestamp = new Timestamp(),
                            WorkedTime = new TimeSpan(08,00,00),                      
                            WorkedDate = new DateTime(2016, 03, 21, 00,00,00)                        
                        }
                    }
                }
            };

               var serializedResponse = JsonConvert.SerializeObject(responseObj);

               var response = new HttpResponseMessage(HttpStatusCode.OK);
               response.Content = new StringContent(serializedResponse, Encoding.UTF8, contentType);
               var mockHandler = new MockHandler();
               mockHandler.Responses.Enqueue(response);

               var testHttpClient = new HttpClient(mockHandler);
               testHttpClient.BaseAddress = new Uri(serviceUrl);

               var client = new ColleagueApiClient(testHttpClient, loggerMock.Object);
                 var actualResult = (await client.GetTimecardsAsync()).ToList();

               Assert.IsInstanceOfType(actualResult, typeof(List<Timecard>));

               for (int i = 0; i < responseObj.Count; i++)
               {
                    Assert.AreEqual(responseObj[i].EmployeeId, actualResult[i].EmployeeId);
               }
          }

          #endregion

          #region timecard histories
          [TestMethod]
          public async Task TimecardHistories_GetTimecardHistoriesAsyncTest()
          {
              #region data
              var startDate = new DateTime(2016, 08, 15);
              var endDate = new DateTime(2016, 09, 15);
              var responseObj = new List<TimecardHistory>()
                {
                    new TimecardHistory()
                    {
                        Id = "001",
                        EmployeeId = "24601",
                        StartDate = new DateTime(2016,09,01),
                        EndDate = new DateTime(2016, 09, 07),
                        PayCycleId = "77",
                        PeriodStartDate = new DateTime(2016, 09, 01),
                        PeriodEndDate = new DateTime(2016, 09, 14),
                        PositionId = "JAJA",
                        Timestamp = new Timestamp()
                        {
                            AddDateTime = new DateTime(2016, 09, 15),
                            ChangeDateTime = new DateTime(2016, 09, 15),
                            AddOperator = "24601",
                            ChangeOperator = "24601"
                        },
                        TimeEntryHistories = new List<TimeEntryHistory>()
                        {
                            new TimeEntryHistory()
                            {
                                Id = "11",
                                EarningsTypeId = ":)LOL",
                                InDateTime = new DateTime(2016, 09, 04),
                                OutDateTime = new DateTime(2016, 09, 04),
                                PersonLeaveId = ":(toobad",
                                ProjectId = "HISTRIONICPERSONALITYDISORDER",
                                TimecardHistoryId = "001",
                                Timestamp = new Timestamp()
                                {
                                    AddDateTime = new DateTime(2016, 09, 15),
                                    ChangeDateTime = new DateTime(2016, 09, 15),
                                    AddOperator = "24601",
                                    ChangeOperator = "24601"
                                },
                                WorkedDate = new DateTime(2016,09,04),
                                WorkedTime = new TimeSpan(42)                            
                            }
                        }
                    }
                };
              #endregion

              var serializedResponse = JsonConvert.SerializeObject(responseObj);

              var response = new HttpResponseMessage(HttpStatusCode.OK);
              response.Content = new StringContent(serializedResponse, Encoding.UTF8, contentType);
              var mockHandler = new MockHandler();
              mockHandler.Responses.Enqueue(response);

              var testHttpClient = new HttpClient(mockHandler);
              testHttpClient.BaseAddress = new Uri(serviceUrl);

              var client = new ColleagueApiClient(testHttpClient, loggerMock.Object);
              var actualResult = (await client.GetTimecardHistoriesAsync(startDate, endDate)).ToList();
              Assert.IsInstanceOfType(actualResult, typeof(List<TimecardHistory>));

              for (int i = 0; i < responseObj.Count; i++)
              {
                  Assert.AreEqual(responseObj[i].Id, actualResult[i].Id);
                  Assert.AreEqual(responseObj[i].EmployeeId, actualResult[i].EmployeeId);
                  Assert.AreEqual(responseObj[i].EndDate, actualResult[i].EndDate);
                  Assert.AreEqual(responseObj[i].PayCycleId, actualResult[i].PayCycleId);
                  Assert.AreEqual(responseObj[i].PeriodEndDate, actualResult[i].PeriodEndDate);
                  Assert.AreEqual(responseObj[i].PeriodStartDate, actualResult[i].PeriodStartDate);
                  Assert.AreEqual(responseObj[i].PositionId, actualResult[i].PositionId);
                  Assert.AreEqual(responseObj[i].StartDate, actualResult[i].StartDate);
                  Assert.AreEqual(responseObj[i].Timestamp.AddDateTime, actualResult[i].Timestamp.AddDateTime);
                  Assert.AreEqual(responseObj[i].Timestamp.ChangeDateTime, actualResult[i].Timestamp.ChangeDateTime);
                  Assert.AreEqual(responseObj[i].Timestamp.AddOperator, actualResult[i].Timestamp.AddOperator);
                  Assert.AreEqual(responseObj[i].Timestamp.ChangeOperator, actualResult[i].Timestamp.ChangeOperator);
                  Assert.AreEqual(responseObj[i].TimeEntryHistories.Count, actualResult[i].TimeEntryHistories.Count);
              }
          }
          #endregion

          #region time entry comments
          [TestMethod]
          public async Task TimeEntryComments_GetTimeEntryCommentsAsyncTest()
          {
              var responseObj = new List<TimeEntryComments>()
              {
                  new TimeEntryComments()
                  {
                      Comments = "commmmmments",
                      CommentsTimestamp = new Timestamp(),
                      EmployeeId = "24601",
                      Id = "1234",
                      PayCycleId = "A01",
                      PayPeriodEndDate = DateTime.UtcNow,
                      TimecardId = "999",
                      TimecardStatusId = ""
                  }
              };
              var response = new HttpResponseMessage(HttpStatusCode.OK);
              var serializedResponse = JsonConvert.SerializeObject(responseObj);
              response.Content = new StringContent(serializedResponse, Encoding.UTF8, contentType);
              var mockHandler = new MockHandler();
              mockHandler.Responses.Enqueue(response);
              var testHttpClient = new HttpClient(mockHandler);
              testHttpClient.BaseAddress = new Uri(serviceUrl);

              var client = new ColleagueApiClient(testHttpClient, loggerMock.Object);
              var actualResult = (await client.GetTimeEntryCommentsAsync()).ToList();

              Assert.IsInstanceOfType(actualResult, typeof(List<TimeEntryComments>));
              for(var i = 0; i < responseObj.Count; i++)
              {
                  Assert.AreEqual(responseObj[i].Comments, actualResult[i].Comments);
                  Assert.AreEqual(responseObj[i].EmployeeId, actualResult[i].EmployeeId);
                  Assert.AreEqual(responseObj[i].TimecardId, actualResult[i].TimecardId);
                  Assert.AreEqual(responseObj[i].TimecardStatusId, actualResult[i].TimecardStatusId);
                  Assert.AreEqual(responseObj[i].PayCycleId, actualResult[i].PayCycleId);
                  Assert.AreEqual(responseObj[i].PayPeriodEndDate, actualResult[i].PayPeriodEndDate);
              }
          }
          [TestMethod]
          public async Task TimeEntryComments_CreateTimeEntryCommentsAsyncTest()
          {
              var responseObj = new TimeEntryComments()
              {
                  Comments = "commmmmments",
                  CommentsTimestamp = new Timestamp(),
                  EmployeeId = "24601",
                  Id = "1234",
                  PayCycleId = "A01",
                  PayPeriodEndDate = DateTime.UtcNow,
                  TimecardId = "999",
                  TimecardStatusId = ""
              };
              var response = new HttpResponseMessage(HttpStatusCode.OK);
              var serializedResponse = JsonConvert.SerializeObject(responseObj);
              response.Content = new StringContent(serializedResponse, Encoding.UTF8, contentType);
              var mockHandler = new MockHandler();
              mockHandler.Responses.Enqueue(response);
              var testHttpClient = new HttpClient(mockHandler);
              testHttpClient.BaseAddress = new Uri(serviceUrl);

              var client = new ColleagueApiClient(testHttpClient, loggerMock.Object);
              var actualResult = await client.CreateTimeEntryCommentsAsync(responseObj);

              Assert.IsInstanceOfType(actualResult, typeof(TimeEntryComments));
              Assert.AreEqual(responseObj.Comments, actualResult.Comments);
              Assert.AreEqual(responseObj.EmployeeId, actualResult.EmployeeId);
              Assert.AreEqual(responseObj.TimecardId, actualResult.TimecardId);
              Assert.AreEqual(responseObj.TimecardStatusId, actualResult.TimecardStatusId);
              Assert.AreEqual(responseObj.PayCycleId, actualResult.PayCycleId);
              Assert.AreEqual(responseObj.PayPeriodEndDate, actualResult.PayPeriodEndDate);
          }

          #endregion

          #region timecard statuses
          [TestMethod]
          public async Task Timecard_Statuses_CreateTimecardStatusAsyncTest()
          {
               var responseObj = new TimecardStatus()
               {
                    ActionType = StatusAction.Submitted,
                    ActionerId = "24601",
                    HistoryId = "pg1",
                    Id = "",
                    TimecardId = "001",
                    Timestamp = new Timestamp(),
               };
               var response = new HttpResponseMessage(HttpStatusCode.OK);
               var serializedResponse = JsonConvert.SerializeObject(responseObj);
               response.Content = new StringContent(serializedResponse, Encoding.UTF8, contentType);
               var mockHandler = new MockHandler();
               mockHandler.Responses.Enqueue(response);
               var testHttpClient = new HttpClient(mockHandler);
               testHttpClient.BaseAddress = new Uri(serviceUrl);

               var client = new ColleagueApiClient(testHttpClient, loggerMock.Object);
               var actualResult = await client.CreateTimecardStatusAsync(responseObj);

               Assert.IsInstanceOfType(actualResult, typeof(TimecardStatus));
               Assert.AreEqual(responseObj.TimecardId, actualResult.TimecardId);
          }

          public async Task Timecard_Statuses_CreateTimecardStatusesAsyncTest()
          {
              var responseObj = new List<TimecardStatus>() {
                  new TimecardStatus()
                  {
                      ActionType = StatusAction.Submitted,
                      ActionerId = "24601",
                      HistoryId = "pg1",
                      Id = "",
                      TimecardId = "001",
                      Timestamp = new Timestamp(),
                  }
              };
              var response = new HttpResponseMessage(HttpStatusCode.OK);
              var serializedResponse = JsonConvert.SerializeObject(responseObj);
              response.Content = new StringContent(serializedResponse, Encoding.UTF8, contentType);
              var mockHandler = new MockHandler();
              mockHandler.Responses.Enqueue(response);
              var testHttpClient = new HttpClient(mockHandler);
              testHttpClient.BaseAddress = new Uri(serviceUrl);

              var client = new ColleagueApiClient(testHttpClient, loggerMock.Object);
              var actualResult = await client.CreateTimecardStatusesAsync(responseObj);

              Assert.IsInstanceOfType(actualResult, typeof(TimecardStatus));
              Assert.AreEqual(responseObj.ElementAt(0).TimecardId, actualResult.ElementAt(0).TimecardId);
          }
          #endregion

          #region overtime calculation definition tests

          [TestMethod]
          public async Task OvertimeCalculationDefinition_GetOvertimeCalculationDefinitionsAsyncTest()
          {
               var responseObj = new List<OvertimeCalculationDefinition>();
               responseObj.Add(createOvertimeCalculationDefinition());

               var serializedResponse = JsonConvert.SerializeObject(responseObj);

               var response = new HttpResponseMessage(HttpStatusCode.OK);
               response.Content = new StringContent(serializedResponse, Encoding.UTF8, contentType);
               var mockHandler = new MockHandler();
               mockHandler.Responses.Enqueue(response);

               var testHttpClient = new HttpClient(mockHandler);
               testHttpClient.BaseAddress = new Uri(serviceUrl);

               var client = new ColleagueApiClient(testHttpClient, loggerMock.Object);
               var actualResult = await client.GetOvertimeCalculationDefinitionsAsync();

               Assert.IsInstanceOfType(actualResult, typeof(List<OvertimeCalculationDefinition>));

               for (int i = 0; i < responseObj.Count; i++)
               {
                    Assert.AreEqual(responseObj[i].Id, actualResult[i].Id);
                    Assert.AreEqual(responseObj[i].Description, actualResult[i].Description);
                    Assert.AreEqual(responseObj[i].OvertimePayCalculationMethod, actualResult[i].OvertimePayCalculationMethod);
                    Assert.AreEqual(responseObj[i].WeeklyHoursThreshold.Count, actualResult[i].WeeklyHoursThreshold.Count);
                    Assert.AreEqual(responseObj[i].DailyHoursThreshold.Count, actualResult[i].DailyHoursThreshold.Count);
                    Assert.AreEqual(responseObj[i].IncludedEarningsTypes.Count, actualResult[i].IncludedEarningsTypes.Count);
               }
          }
          private OvertimeCalculationDefinition createOvertimeCalculationDefinition()
          {
               var ef1 = new EarningsFactor()
               {
                    EarningsTypeId = "OT1",
                    Factor = 1.5m
               };
               var ef2 = new EarningsFactor()
               {
                    EarningsTypeId = "OT2",
                    Factor = 1.75m
               };
               var ef3 = new EarningsFactor()
               {
                    EarningsTypeId = "OT3",
                    Factor = 2.0m
               };
               var ef4 = new EarningsFactor()
               {
                    EarningsTypeId = "OT4",
                    Factor = 2.25m
               };

               var otcd = new OvertimeCalculationDefinition();
               otcd.Id = "OTCD1";
               otcd.Description = "First OTCD";
               otcd.OvertimePayCalculationMethod = OvertimePayCalculationMethod.WeightedAverage;

               // create the WeeklyHoursThreshold
               var wh1 = new HoursThreshold();
               wh1.Threshold = 35;
               wh1.DefaultEarningsType = ef1;
               wh1.AlternateEarningsType = ef2;

               var wh2 = new HoursThreshold();
               wh2.Threshold = 40;
               wh2.DefaultEarningsType = ef3;
               wh2.AlternateEarningsType = ef4;

               // add the HoursThresholds to the list
               otcd.WeeklyHoursThreshold = new List<HoursThreshold>();
               otcd.WeeklyHoursThreshold.Add(wh1);
               otcd.WeeklyHoursThreshold.Add(wh2);

               // create the DailyHoursThreshold
               var dh1 = new HoursThreshold();
               wh1.Threshold = 6;
               wh1.DefaultEarningsType = ef1;
               wh1.AlternateEarningsType = ef2;

               var dh2 = new HoursThreshold();
               wh2.Threshold = 10;
               wh2.DefaultEarningsType = ef3;
               wh2.AlternateEarningsType = ef4;

               // add the HoursThresholds to the list
               otcd.DailyHoursThreshold = new List<HoursThreshold>();
               otcd.DailyHoursThreshold.Add(dh1);
               otcd.DailyHoursThreshold.Add(dh2);

               // create the InclEarntype list
               otcd.IncludedEarningsTypes = new List<IncludedEarningsType>(){
                    new IncludedEarningsType(){
                         EarningsTypeId = "OT2",
                         UseAlternate = true
                    },
                    new IncludedEarningsType(){
                         EarningsTypeId = "OT4",
                         UseAlternate = true
                    },
                    new IncludedEarningsType(){
                         EarningsTypeId = "OT5",
                         UseAlternate = false
                    }
               };

               return otcd;
          }

          #endregion
     }
}
