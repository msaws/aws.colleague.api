﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Http.Configuration;
using Ellucian.Colleague.Domain.Base.Exceptions;
using Ellucian.Colleague.Data.TimeManagement.Transactions;



namespace Ellucian.Colleague.Data.TimeManagement.Repositories
{
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class TimeEntryCommentsRepository : BaseColleagueRepository, ITimeEntryCommentsRepository
    {
        private readonly string colleagueTimeZone;
        private readonly int bulkReadSize;

        #region CONSTRUCTOR(S)

        /// <summary>
        /// Repository constrcutor
        /// </summary>
        /// <param name="cacheProvider"></param>
        /// <param name="transactionFactory"></param>
        /// <param name="logger"></param>
        /// <param name="settings"></param>
        public TimeEntryCommentsRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings settings)
            : base(cacheProvider, transactionFactory, logger)
        {
            colleagueTimeZone = settings.ColleagueTimeZone;
            bulkReadSize = settings != null && settings.BulkReadSize > 0 ? settings.BulkReadSize : 5000;
        }
        #endregion

        #region GET COMMENTS

        public async Task<IEnumerable<TimeEntryComment>> GetCommentsAsync(IEnumerable<string> employeeIds)
        {
            if (employeeIds == null)
            {
                throw new ArgumentNullException("employeeIds");
            }

            var criteria = "WITH TCC.EMPLOYEE.ID EQ ?";
            var commentKeys = await DataReader.SelectAsync("TIMECARD.COMMENTS", criteria, employeeIds.Select(id => string.Format("\"{0}\"", id)).ToArray());

            if (commentKeys == null)
            {
                var message = "Unexpected null returned from TIMECARD.COMMENTS SelectAsyc";
                logger.Error(message);
                throw new ApplicationException(message);
            }
            if (!commentKeys.Any())
            {
                logger.Info("No TIMECARD.COMMENTS keys exist for the given Timecard.COMMENT Ids: " + string.Join(",", commentKeys));
            }

            var dbComments = new List<DataContracts.TimecardComments>();

            for (int i = 0; i < commentKeys.Count(); i += bulkReadSize)
            {
                var subList = commentKeys.Skip(i).Take(bulkReadSize);
                var records = await DataReader.BulkReadRecordAsync<DataContracts.TimecardComments>(subList.ToArray());
                if (records == null)
                {
                    logger.Error("Unexpected null from bulk read of Timecard records");
                }
                else
                {
                    dbComments.AddRange(records);
                }
            }

            var entityComments = new List<TimeEntryComment>();
            foreach (var comment in dbComments)
            {
                try
                {
                    entityComments.Add(BuildCommentsEntity(comment));
                }
                catch (Exception e)
                {
                    LogDataError("TIMECARD.COMMENTS", comment.Recordkey, comment, e);
                }
            }
            return entityComments;          
        }




        #endregion

        #region CREATE COMMENTS
        public async Task<TimeEntryComment> CreateCommentsAsync(TimeEntryComment comments)
        {
            if (comments == null)
                throw new ArgumentNullException("comments");

            var request = new CreateTimecardCommentsRequest()
            {
                Comments = new List<string>() { comments.Comments },
                EmployeeId = comments.EmployeeId,
                PositionId = comments.PositionId,
                PaycycleId = comments.PayCycleId,
                PayPeriodEndDate = comments.PayPeriodEndDate,
                TimecardId = comments.TimecardId,
            };

            // execute the request
            var response = await transactionInvoker.ExecuteAsync<CreateTimecardCommentsRequest, CreateTimecardCommentsResponse>(request);
            if (string.IsNullOrWhiteSpace(response.NewKey))
            {
                var message = "Record key not returned";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            var newComments = await DataReader.ReadRecordAsync<DataContracts.TimecardComments>(response.NewKey);

            if (newComments == null)
            {
                var message = "New comments not found";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            return BuildCommentsEntity(newComments);
        }
        #endregion

        #region OBJECT BUILDERS

        private TimeEntryComment BuildCommentsEntity(DataContracts.TimecardComments dbCommments)
        {
            if (dbCommments == null)
            {
                throw new ArgumentNullException("dbComments");
            }
            if (!dbCommments.TccPayPeriodEndDate.HasValue)
            {
                throw new ArgumentException("TccPayPeriodEndDate must have a value");
            }

            return new TimeEntryComment(
                dbCommments.Recordkey, 
                dbCommments.TccEmployeeId,
                dbCommments.TccTimecardId, 
                dbCommments.TccPositionId,
                dbCommments.TccPaycycleId,
                dbCommments.TccPayPeriodEndDate.Value,
                dbCommments.TccComments,
                new Timestamp(dbCommments.TimecardCommentsAddopr,
                    dbCommments.TimecardCommentsAddtime.ToPointInTimeDateTimeOffset(dbCommments.TimecardCommentsAdddate, colleagueTimeZone).Value,
                     dbCommments.TimecardCommentsChgopr,
                     dbCommments.TimecardCommentsChgtime.ToPointInTimeDateTimeOffset(dbCommments.TimecardCommentsChgdate, colleagueTimeZone).Value)
         
            );
        }

        #endregion
    }
}
