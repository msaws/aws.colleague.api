﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Data.Base.DataContracts;
using Ellucian.Colleague.Data.TimeManagement.Transactions;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.TimeManagement;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Web.Http.Configuration;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.TimeManagement.Repositories
{
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class TimecardHistoriesRepository : BaseColleagueRepository, ITimecardHistoriesRepository
    {
        private readonly string colleagueTimeZone;
        private readonly int bulkReadSize;
        private int _version = 2;

        #region CONSTRUCTOR(S)

        /// <summary>
        /// Repository constrcutor
        /// </summary>
        /// <param name="cacheProvider"></param>
        /// <param name="transactionFactory"></param>
        /// <param name="logger"></param>
        /// <param name="settings"></param>
        public TimecardHistoriesRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings settings)
            : base(cacheProvider, transactionFactory, logger)
        {
            colleagueTimeZone = settings.ColleagueTimeZone;
            bulkReadSize = settings != null && settings.BulkReadSize > 0 ? settings.BulkReadSize : 5000;
        }
        #endregion

        /// <summary>
        /// Creates a timecardhistory and timeentryhistoryrecords
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        public async Task<string> CreateTimecardHistoryAsync(Timecard timecard, TimecardStatus timecardStatus, IEnumerable<TimeEntryComment> timeEntryComments)
        {
            _version = 1;
            if (timecard == null)
            {
                throw new ArgumentNullException("timecard");
            }
            if (timecardStatus == null)
            {
                throw new ArgumentNullException("timecardStatus");
            }
            if (timeEntryComments == null)
            {
                logger.Info("timeEntryComments is null in history repository. no comments will be saved for this history record");
                timeEntryComments = new List<TimeEntryComment>();
            }
            if (timeEntryComments.Any() && !timeEntryComments.All(tc => tc.IsLinkedToTimecard(timecard)))
            {
                var message = "cannot create timecard history because all timeEntryComments must be associated to the given timecard";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            var request = new CreateTimecardHistoryRequest()
            {

                TcdhEmployeeId = timecard.EmployeeId,
                TcdhEndDate = timecard.EndDate,
                TcdhPeriodEndDate = timecard.PeriodEndDate,
                TcdhPeriodStartDate = timecard.PeriodStartDate,
                TcdhPaycycleId = timecard.PayCycleId,
                TcdhPositionId = timecard.PositionId,
                TcdhStartDate = timecard.StartDate,
                TcdhStatusActionerId = timecardStatus.ActionerId,
                TcdhStatusActionType = timecardStatus.ActionType.ToString(),
                CreateTimeEntryHistories = timecard.TimeEntries.Select(te => new CreateTimeEntryHistories()
                {
                    TehEarntypeId = te.EarningsTypeId,
                    TehInDate = te.InDateTime.HasValue ? (DateTime?)te.InDateTime.Value.ToLocalDateTime(colleagueTimeZone).Date : null,
                    TehOutDate = te.OutDateTime.HasValue ? (DateTime?)te.OutDateTime.Value.ToLocalDateTime(colleagueTimeZone).Date : null,
                    TehInTime = te.InDateTime.HasValue ? te.InDateTime.ToLocalDateTime(colleagueTimeZone) : null,
                    TehOutTime = te.OutDateTime.HasValue ? te.OutDateTime.ToLocalDateTime(colleagueTimeZone) : null,
                    TehWorkedDate = te.WorkedDate.Date,
                    TehWorkedMinutes = te.WorkedTime.HasValue ? (int?)te.WorkedTime.Value.TotalMinutes : null,
                    TehPerleaveId = te.PersonLeaveId,
                    TehProjectId = te.ProjectId
                }).ToList(),
                CreateTimecardComments = timeEntryComments.Select(tc => new CreateTimecardComments()
                {
                    TcchOrigCommentAdddate = tc.CommentsTimestamp.AddDateTime.ToLocalDateTime(colleagueTimeZone).Date,
                    TcchOrigCommentAddopr = tc.CommentsTimestamp.AddOperator,
                    TcchOrigCommentAddtime = tc.CommentsTimestamp.AddDateTime.ToLocalDateTime(colleagueTimeZone),
                    TcchOrigComments = tc.Comments,
                    TcchPaycycleId = tc.PayCycleId,
                    TcchPayPeriodEndDate = tc.PayPeriodEndDate,
                    TcchPositionId = tc.PositionId

                }).ToList()
            };


            var response = await transactionInvoker.ExecuteAsync<CreateTimecardHistoryRequest, CreateTimecardHistoryResponse>(request);


            if (response == null)
            {
                var message = "Could not create timecard history. Unexpected null response from CTX.";
                logger.Error(message);
                throw new ApplicationException(message);
            }
            if (!string.IsNullOrWhiteSpace(response.ErrorMessage))
            {
                logger.Error(response.ErrorMessage);
                throw new ApplicationException(response.ErrorMessage);
            }
            if (string.IsNullOrWhiteSpace(response.NewTimecardHistoryId))
            {
                var message = "New history id not returned";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            return response.NewTimecardHistoryId;
        }

        /// <summary>
        /// Gets timecard history and time entry history records
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="employeeIds"></param>
        /// <returns></returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        public async Task<IEnumerable<TimecardHistory>> GetTimecardHistoriesAsync(DateTime startDate, DateTime endDate, IEnumerable<string> employeeIds)
        {
            _version = 1;
            if (employeeIds == null)
            {
                var message = "Employee Ids are required to get time cards";
                logger.Error(message);
                throw new ArgumentNullException("employeeIds");
            }

            if (!employeeIds.Any())
            {
                var message = "No employee ids found in input list";
                logger.Error(message);
                throw new ArgumentException(message);
            }

            employeeIds = employeeIds.Distinct().ToList();

            #region DATA SELECT
            var internationalParameters = await GetInternationalParameters();
            if (internationalParameters == null)
            {
                var message = "InternationalParameters cannot be null";
                logger.Error(message);
                throw new ApplicationException(message);
            }
            if (string.IsNullOrWhiteSpace(internationalParameters.HostShortDateFormat) || string.IsNullOrWhiteSpace(internationalParameters.HostDateDelimiter))
            {
                var message = "InternationalParameters Host Short Date Format and Host Date Delimiter are required for use";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            var timecardHistoryKeyCriteria = string.Format(
                "WITH TCDH.EMPLOYEE.ID EQ ? AND TCDH.END.DATE GE '{0}' AND TCDH.START.DATE LE '{1}'",
                UniDataFormatter.UnidataFormatDate(startDate, internationalParameters.HostShortDateFormat, internationalParameters.HostDateDelimiter),
                UniDataFormatter.UnidataFormatDate(endDate, internationalParameters.HostShortDateFormat, internationalParameters.HostDateDelimiter)
            );
            // todo bhr: find out if underlying code 'chunks' this up...
            // update:   looking at the code, it looks like it might, but I don't know what the specific 'chunk' size is
            var timecardHistoryKeys = await DataReader.SelectAsync("TIMECARD.HISTORY", timecardHistoryKeyCriteria, employeeIds.Select(id => string.Format("\"{0}\"", id)).ToArray());
            if (timecardHistoryKeys == null)
            {
                var message = "Unexpected null returned from TIMECARD.HISTORY SelectAsyc";
                logger.Error(message);
                timecardHistoryKeys = new string[] { };
            }
            if (!timecardHistoryKeys.Any())
            {
                logger.Info("No TIMECARD.HISTORY keys exist for the given employee Ids: " + string.Join(",", employeeIds));
            }

            var timeEntryHistoryKeyCriteria = "WITH TEH.TIMECARD.HISTORY.ID EQ ?";
            var timeEntryHistoryKeys =
                await DataReader.SelectAsync("TIME.ENTRY.HISTORY", timeEntryHistoryKeyCriteria, timecardHistoryKeys.Select(key => string.Format("\"{0}\"", key)).ToArray());
            if (timeEntryHistoryKeys == null)
            {
                var message = "Unexpected null returned from TIME.ENTRY.HISTORY SelectAsync";
                logger.Error(message);
                timeEntryHistoryKeys = new string[] { };
            }
            if (!timeEntryHistoryKeys.Any())
            {
                logger.Info("No TIME.ENTRY.HISTORY keys exist for the given Timecard History Ids: " + string.Join(",", timecardHistoryKeys));
            }

            #endregion

            #region DATA READ

            var dbTimecardHistoryRecords = new List<DataContracts.TimecardHistory>();
            var dbTimeEntryHistoryRecords = new List<DataContracts.TimeEntryHistory>();

            for (int i = 0; i < timecardHistoryKeys.Count(); i += bulkReadSize)
            {
                var subList = timecardHistoryKeys.Skip(i).Take(bulkReadSize);
                var records = await DataReader.BulkReadRecordAsync<DataContracts.TimecardHistory>(subList.ToArray());
                if (records == null)
                {
                    logger.Error("Unexpected null from bulk read of Timecard History records");
                }
                else
                {
                    dbTimecardHistoryRecords.AddRange(records);
                }
            }

            for (int i = 0; i < timeEntryHistoryKeys.Count(); i += bulkReadSize)
            {
                var subList = timeEntryHistoryKeys.Skip(i).Take(bulkReadSize);
                var records = await DataReader.BulkReadRecordAsync<DataContracts.TimeEntryHistory>(subList.ToArray());
                if (records == null)
                {
                    logger.Error("Unexpected null from bulk read of Time Entry records");
                }
                else
                {
                    dbTimeEntryHistoryRecords.AddRange(records);
                }
            }

            #endregion

            #region DOMAIN MAPPING

            var domainTimeEntryHistories = new List<TimeEntryHistory>();
            var domainTimecardHistories = new List<TimecardHistory>();

            var timeEntryHistoryLookup = dbTimeEntryHistoryRecords.ToLookup(te => te.TehTimecardHistoryId);
            foreach (var timecardHistoryRecord in dbTimecardHistoryRecords)
            {
                try
                {
                    var timecardHistory = this.MapToDomainTimecardHistory(timecardHistoryRecord);

                    foreach (var timeEntryHistoryRecord in timeEntryHistoryLookup[timecardHistoryRecord.Recordkey])
                    {
                        try
                        {
                            timecardHistory.TimeEntryHistories.Add(this.MapToDomainTimeEntryHistory(timeEntryHistoryRecord));
                        }
                        catch (Exception e)
                        {
                            LogDataError("TIME.ENTRY.HISTORY", timeEntryHistoryRecord.Recordkey, timeEntryHistoryRecord, e, e.Message);
                        }
                    }

                    domainTimecardHistories.Add(timecardHistory);
                }
                catch (Exception e)
                {
                    LogDataError("TIMECARD.HISTORY", timecardHistoryRecord.Recordkey, timecardHistoryRecord, e, e.Message);
                }
            }
            #endregion

            return domainTimecardHistories;
        }

        /// <summary>
        /// Creates a timecardhistory and timeentryhistoryrecords
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task<string> CreateTimecardHistory2Async(Timecard2 timecard2, TimecardStatus timecardStatus, IEnumerable<TimeEntryComment> timeEntryComments)
        {
            _version = 2;
            if (timecard2 == null)
            {
                throw new ArgumentNullException("timecard");
            }
            if (timecardStatus == null)
            {
                throw new ArgumentNullException("timecardStatus");
            }
            if (timeEntryComments == null)
            {
                logger.Info("timeEntryComments is null in history repository. no comments will be saved for this history record");
                timeEntryComments = new List<TimeEntryComment>();
            }
            if (timeEntryComments.Any() && !timeEntryComments.All(tc => tc.IsLinkedToTimecard(timecard2)))
            {
                var message = "cannot create timecard history because all timeEntryComments must be associated to the given timecard";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            var request = new CreateTimecardHistoryRequest()
            {

                TcdhEmployeeId = timecard2.EmployeeId,
                TcdhEndDate = timecard2.EndDate,
                TcdhPeriodEndDate = timecard2.PeriodEndDate,
                TcdhPeriodStartDate = timecard2.PeriodStartDate,
                TcdhPaycycleId = timecard2.PayCycleId,
                TcdhPositionId = timecard2.PositionId,
                TcdhStartDate = timecard2.StartDate,
                TcdhStatusActionerId = timecardStatus.ActionerId,
                TcdhStatusActionType = timecardStatus.ActionType.ToString(),
                CreateTimeEntryHistories = timecard2.TimeEntries.Select(te => new CreateTimeEntryHistories()
                {
                    TehEarntypeId = te.EarningsTypeId,
                    TehInDate = te.InDateTime.HasValue ? DateTime.SpecifyKind(te.InDateTime.Value, DateTimeKind.Unspecified) as DateTime? : null,
                    TehOutDate = te.OutDateTime.HasValue ? DateTime.SpecifyKind(te.OutDateTime.Value, DateTimeKind.Unspecified) as DateTime? : null,
                    TehInTime = te.InDateTime.HasValue ? DateTime.SpecifyKind(te.InDateTime.Value, DateTimeKind.Unspecified) as DateTime? : null,
                    TehOutTime = te.OutDateTime.HasValue ? DateTime.SpecifyKind(te.OutDateTime.Value, DateTimeKind.Unspecified) as DateTime? : null,
                    TehWorkedDate = te.WorkedDate.Date,
                    TehWorkedMinutes = te.WorkedTime.HasValue ? (int?)te.WorkedTime.Value.TotalMinutes : null,
                    TehPerleaveId = te.PersonLeaveId,
                    TehProjectId = te.ProjectId
                }).ToList(),
                CreateTimecardComments = timeEntryComments.Select(tc => new CreateTimecardComments()
                {
                    TcchOrigCommentAdddate = tc.CommentsTimestamp.AddDateTime.ToLocalDateTime(colleagueTimeZone).Date,
                    TcchOrigCommentAddopr = tc.CommentsTimestamp.AddOperator,
                    TcchOrigCommentAddtime = tc.CommentsTimestamp.AddDateTime.ToLocalDateTime(colleagueTimeZone),
                    TcchOrigComments = tc.Comments,
                    TcchPaycycleId = tc.PayCycleId,
                    TcchPayPeriodEndDate = tc.PayPeriodEndDate,
                    TcchPositionId = tc.PositionId

                }).ToList()
            };


            var response = await transactionInvoker.ExecuteAsync<CreateTimecardHistoryRequest, CreateTimecardHistoryResponse>(request);


            if (response == null)
            {
                var message = "Could not create timecard history. Unexpected null response from CTX.";
                logger.Error(message);
                throw new ApplicationException(message);
            }
            if (!string.IsNullOrWhiteSpace(response.ErrorMessage))
            {
                logger.Error(response.ErrorMessage);
                throw new ApplicationException(response.ErrorMessage);
            }
            if (string.IsNullOrWhiteSpace(response.NewTimecardHistoryId))
            {
                var message = "New history id not returned";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            return response.NewTimecardHistoryId;
        }

        /// <summary>
        /// Gets timecard history and time entry history records
        /// </summary>
        /// <param name="startDate"></param>
        /// <param name="endDate"></param>
        /// <param name="employeeIds"></param>
        /// <returns></returns>
        public async Task<IEnumerable<TimecardHistory2>> GetTimecardHistories2Async(DateTime startDate, DateTime endDate, IEnumerable<string> employeeIds)
        {
            _version = 2;
            if (employeeIds == null)
            {
                var message = "Employee Ids are required to get time cards";
                logger.Error(message);
                throw new ArgumentNullException("employeeIds");
            }

            if (!employeeIds.Any())
            {
                var message = "No employee ids found in input list";
                logger.Error(message);
                throw new ArgumentException(message);
            }

            employeeIds = employeeIds.Distinct().ToList();

            #region DATA SELECT
            var internationalParameters = await GetInternationalParameters();
            if (internationalParameters == null)
            {
                var message = "InternationalParameters cannot be null";
                logger.Error(message);
                throw new ApplicationException(message);
            }
            if (string.IsNullOrWhiteSpace(internationalParameters.HostShortDateFormat) || string.IsNullOrWhiteSpace(internationalParameters.HostDateDelimiter))
            {
                var message = "InternationalParameters Host Short Date Format and Host Date Delimiter are required for use";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            var timecardHistoryKeyCriteria = string.Format(
                "WITH TCDH.EMPLOYEE.ID EQ ? AND TCDH.END.DATE GE '{0}' AND TCDH.START.DATE LE '{1}'",
                UniDataFormatter.UnidataFormatDate(startDate, internationalParameters.HostShortDateFormat, internationalParameters.HostDateDelimiter),
                UniDataFormatter.UnidataFormatDate(endDate, internationalParameters.HostShortDateFormat, internationalParameters.HostDateDelimiter)
            );
            // todo bhr: find out if underlying code 'chunks' this up...
            // update:   looking at the code, it looks like it might, but I don't know what the specific 'chunk' size is
            var timecardHistoryKeys = await DataReader.SelectAsync("TIMECARD.HISTORY", timecardHistoryKeyCriteria, employeeIds.Select(id => string.Format("\"{0}\"", id)).ToArray());
            if (timecardHistoryKeys == null)
            {
                var message = "Unexpected null returned from TIMECARD.HISTORY SelectAsyc";
                logger.Error(message);
                timecardHistoryKeys = new string[] { };
            }
            if (!timecardHistoryKeys.Any())
            {
                logger.Info("No TIMECARD.HISTORY keys exist for the given employee Ids: " + string.Join(",", employeeIds));
            }

            var timeEntryHistoryKeyCriteria = "WITH TEH.TIMECARD.HISTORY.ID EQ ?";
            var timeEntryHistoryKeys =
                await DataReader.SelectAsync("TIME.ENTRY.HISTORY", timeEntryHistoryKeyCriteria, timecardHistoryKeys.Select(key => string.Format("\"{0}\"", key)).ToArray());
            if (timeEntryHistoryKeys == null)
            {
                var message = "Unexpected null returned from TIME.ENTRY.HISTORY SelectAsync";
                logger.Error(message);
                timeEntryHistoryKeys = new string[] { };
            }
            if (!timeEntryHistoryKeys.Any())
            {
                logger.Info("No TIME.ENTRY.HISTORY keys exist for the given Timecard History Ids: " + string.Join(",", timecardHistoryKeys));
            }

            #endregion

            #region DATA READ

            var dbTimecardHistoryRecords = new List<DataContracts.TimecardHistory>();
            var dbTimeEntryHistoryRecords = new List<DataContracts.TimeEntryHistory>();

            for (int i = 0; i < timecardHistoryKeys.Count(); i += bulkReadSize)
            {
                var subList = timecardHistoryKeys.Skip(i).Take(bulkReadSize);
                var records = await DataReader.BulkReadRecordAsync<DataContracts.TimecardHistory>(subList.ToArray());
                if (records == null)
                {
                    logger.Error("Unexpected null from bulk read of Timecard History records");
                }
                else
                {
                    dbTimecardHistoryRecords.AddRange(records);
                }
            }

            for (int i = 0; i < timeEntryHistoryKeys.Count(); i += bulkReadSize)
            {
                var subList = timeEntryHistoryKeys.Skip(i).Take(bulkReadSize);
                var records = await DataReader.BulkReadRecordAsync<DataContracts.TimeEntryHistory>(subList.ToArray());
                if (records == null)
                {
                    logger.Error("Unexpected null from bulk read of Time Entry records");
                }
                else
                {
                    dbTimeEntryHistoryRecords.AddRange(records);
                }
            }

            #endregion

            #region DOMAIN MAPPING

            var domainTimeEntryHistories2 = new List<TimeEntryHistory2>();
            var domainTimecardHistories2 = new List<TimecardHistory2>();

            var timeEntryHistoryLookup = dbTimeEntryHistoryRecords.ToLookup(te => te.TehTimecardHistoryId);
            foreach (var timecardHistoryRecord in dbTimecardHistoryRecords)
            {
                try
                {
                    var timecardHistory = this.MapToDomainTimecardHistory(timecardHistoryRecord);

                    foreach (var timeEntryHistoryRecord in timeEntryHistoryLookup[timecardHistoryRecord.Recordkey])
                    {
                        try
                        {
                            timecardHistory.TimeEntryHistories2.Add(this.MapToDomainTimeEntryHistory(timeEntryHistoryRecord));
                        }
                        catch (Exception e)
                        {
                            LogDataError("TIME.ENTRY.HISTORY", timeEntryHistoryRecord.Recordkey, timeEntryHistoryRecord, e, e.Message);
                        }
                    }

                    domainTimecardHistories2.Add(timecardHistory);
                }
                catch (Exception e)
                {
                    LogDataError("TIMECARD.HISTORY", timecardHistoryRecord.Recordkey, timecardHistoryRecord, e, e.Message);
                }
            }
            #endregion

            return domainTimecardHistories2;
        }


        #region HELPERS

        private async Task<IntlParams> GetInternationalParameters()
        {
            return await GetOrAddToCacheAsync<IntlParams>("InternationalParameters",
                async () =>
                {
                    IntlParams intlParams = await DataReader.ReadRecordAsync<IntlParams>("INTL.PARAMS", "INTERNATIONAL");
                    if (intlParams == null)
                    {
                        var errorMessage = "Unable to access international parameters INTL.PARAMS INTERNATIONAL.";
                        logger.Info(errorMessage);
                        IntlParams newIntlParams = new IntlParams();
                        newIntlParams.HostShortDateFormat = "MDY";
                        newIntlParams.HostDateDelimiter = "/";
                        intlParams = newIntlParams;
                    }
                    return intlParams;
                }, Level1CacheTimeoutValue);
        }

        private dynamic MapToDomainTimecardHistory(DataContracts.TimecardHistory timecardHistory)
        {
            // note: if a date has no value, an exception will occur while building this timecardHistory
            //       it will be caught and the error logged in the record loop calling this method
            //       and this record will not be added

            StatusAction statusAction;
            if (!Enum.TryParse<StatusAction>(timecardHistory.TcdhStatusActionType, true, out statusAction))
            {
                throw new ApplicationException("Unable to parse the status action " + timecardHistory.TcdhStatusActionType);
            }

            if (_version == 2) // check the version
            {
                return new TimecardHistory2(
                    timecardHistory.Recordkey,
                    timecardHistory.TcdhEmployeeId,
                    timecardHistory.TcdhPaycycleId,
                    timecardHistory.TcdhPositionId,
                    timecardHistory.TcdhPeriodStartDate.Value,
                    timecardHistory.TcdhPeriodEndDate.Value,
                    timecardHistory.TcdhStartDate.Value,
                    timecardHistory.TcdhEndDate.Value,
                    statusAction,
                    new Timestamp(timecardHistory.TimecardHistoryAddopr,
                        timecardHistory.TimecardHistoryAddtime.ToPointInTimeDateTimeOffset(timecardHistory.TimecardHistoryAdddate, colleagueTimeZone).Value,
                        timecardHistory.TimecardHistoryChgopr,
                        timecardHistory.TimecardHistoryChgtime.ToPointInTimeDateTimeOffset(timecardHistory.TimecardHistoryChgdate, colleagueTimeZone).Value)
                ) { };
            }
            else
            {
                return new TimecardHistory(
                    timecardHistory.Recordkey,
                    timecardHistory.TcdhEmployeeId,
                    timecardHistory.TcdhPaycycleId,
                    timecardHistory.TcdhPositionId,
                    timecardHistory.TcdhPeriodStartDate.Value,
                    timecardHistory.TcdhPeriodEndDate.Value,
                    timecardHistory.TcdhStartDate.Value,
                    timecardHistory.TcdhEndDate.Value,
                    statusAction,
                    new Timestamp(timecardHistory.TimecardHistoryAddopr,
                        timecardHistory.TimecardHistoryAddtime.ToPointInTimeDateTimeOffset(timecardHistory.TimecardHistoryAdddate, colleagueTimeZone).Value,
                        timecardHistory.TimecardHistoryChgopr,
                        timecardHistory.TimecardHistoryChgtime.ToPointInTimeDateTimeOffset(timecardHistory.TimecardHistoryChgdate, colleagueTimeZone).Value)
                ) { };
            }
        }

        private dynamic MapToDomainTimeEntryHistory(DataContracts.TimeEntryHistory timeEntryHistory)
        {
            // note: if a time element has no value, an exception will occur while building this timecardHistory
            //       it will be caught and the error logged in the record loop calling this method
            //       and this record will not be added

            if (_version == 2) // check the version
            {
                var inDateTime = TimeMangementUtility.SetTimeOfDate(timeEntryHistory.TehInDate, timeEntryHistory.TehInTime);
                var outDateTime = TimeMangementUtility.SetTimeOfDate(timeEntryHistory.TehOutDate, timeEntryHistory.TehOutTime);

                return new TimeEntryHistory2(
                    timeEntryHistory.Recordkey,
                    timeEntryHistory.TehTimecardHistoryId,
                    timeEntryHistory.TehEarntypeId,
                    inDateTime,
                    outDateTime,
                    new TimeSpan(0, timeEntryHistory.TehWorkedMinutes.Value, 0),
                    timeEntryHistory.TehWorkedDate.Value,
                    timeEntryHistory.TehPerleaveId,
                    timeEntryHistory.TehProjectId,
                    new Timestamp(timeEntryHistory.TimeEntryHistoryAddopr,
                        timeEntryHistory.TimeEntryHistoryAddtime.ToPointInTimeDateTimeOffset(timeEntryHistory.TimeEntryHistoryAdddate, colleagueTimeZone).Value,
                        timeEntryHistory.TimeEntryHistoryChgopr,
                        timeEntryHistory.TimeEntryHistoryChgtime.ToPointInTimeDateTimeOffset(timeEntryHistory.TimeEntryHistoryChgdate, colleagueTimeZone).Value)
                ) { };
            }
            else
            {
                return new TimeEntryHistory(
                timeEntryHistory.Recordkey,
                timeEntryHistory.TehTimecardHistoryId,
                timeEntryHistory.TehEarntypeId,
                new TimeSpan(0, timeEntryHistory.TehWorkedMinutes.Value, 0),
                timeEntryHistory.TehWorkedDate.Value,
                timeEntryHistory.TehPerleaveId,
                timeEntryHistory.TehProjectId,
                new Timestamp(timeEntryHistory.TimeEntryHistoryAddopr,
                    timeEntryHistory.TimeEntryHistoryAddtime.ToPointInTimeDateTimeOffset(timeEntryHistory.TimeEntryHistoryAdddate, colleagueTimeZone).Value,
                    timeEntryHistory.TimeEntryHistoryChgopr,
                    timeEntryHistory.TimeEntryHistoryChgtime.ToPointInTimeDateTimeOffset(timeEntryHistory.TimeEntryHistoryChgdate, colleagueTimeZone).Value)
            ) { };
            }
        }

        #endregion

    }
}
