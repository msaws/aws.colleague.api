﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Web.Http.Configuration;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;

namespace Ellucian.Colleague.Data.TimeManagement.Repositories
{
     [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
     public class OvertimeCalculationDefinitionsRepository : BaseColleagueRepository, IOvertimeCalculationDefinitionsRepository
     {

          private const string OvertimeCalculationDefinitionsCacheKey = "OvertimeCalculationDefinitions";
          private readonly string colleagueTimeZone;
          private readonly int bulkReadSize;

          #region CONSTRUCTOR(S)

          /// <summary>
          /// Repository constructor
          /// </summary>
          /// <param name="cacheProvider"></param>
          /// <param name="transactionFactory"></param>
          /// <param name="logger"></param>
          /// <param name="settings"></param>
          public OvertimeCalculationDefinitionsRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings settings)
               : base(cacheProvider, transactionFactory, logger)
          {
               colleagueTimeZone = settings.ColleagueTimeZone;
               bulkReadSize = settings != null && settings.BulkReadSize > 0 ? settings.BulkReadSize : 5000;
          }
          #endregion

          public async Task<List<OvertimeCalculationDefinition>> GetOvertimeCalculationDefinitionsAsync()
          {
               return await GetOrAddToCacheAsync<List<OvertimeCalculationDefinition>>(OvertimeCalculationDefinitionsCacheKey, async () => await this.BuildAllOvertimeCalculationDefinitions(), Level1CacheTimeoutValue);
          }

          private async Task<List<OvertimeCalculationDefinition>> BuildAllOvertimeCalculationDefinitions()
          {
               //Obtain data for entities
               ICollection<DataContracts.OtCalcDefinition> overtimeCalculationDefinitions;
               ICollection<DataContracts.OtcdWeeklyParms> weeklyParameters;
               ICollection<DataContracts.OtcdDailyParms> dailyParameters;
               ICollection<DataContracts.OtcdInclEarntypes> includedEarntypes;
               ICollection<DataContracts.Earntype> earntypes;

               var overtimeCalculationDefinitionEntities = new List<OvertimeCalculationDefinition>();

               overtimeCalculationDefinitions = await DataReader.BulkReadRecordAsync<DataContracts.OtCalcDefinition>("OT.CALC.DEFINITION", "", true);
               weeklyParameters = await DataReader.BulkReadRecordAsync<DataContracts.OtcdWeeklyParms>("OTCD.WEEKLY.PARMS", "", true);
               dailyParameters = await DataReader.BulkReadRecordAsync<DataContracts.OtcdDailyParms>("OTCD.DAILY.PARMS", "", true);
               includedEarntypes = await DataReader.BulkReadRecordAsync<DataContracts.OtcdInclEarntypes>("OTCD.INCL.EARNTYPES", "", true);
               earntypes = await DataReader.BulkReadRecordAsync<DataContracts.Earntype>("EARNTYPE", "", true);

               // Sort data. Each Anonymous object will equate to one entity
               var query =
                    from OTCD in overtimeCalculationDefinitions
                    join weeklyParameter in weeklyParameters on OTCD.Recordkey equals weeklyParameter.OtcdWeeklyOtCalcDefId into weeklyParameterGroup
                    join dailyParameter in dailyParameters on OTCD.Recordkey equals dailyParameter.OtcdDailyOtCalcDefId into dailyParameterGroup
                    join includedEarntype in includedEarntypes on OTCD.Recordkey equals includedEarntype.OtcdOvertimeCalcDefId into includedEarntypeGroup
                    select new { OvertimeCalculationDefinition = OTCD, WeeklyParameters = weeklyParameterGroup, DailyParameters = dailyParameterGroup, includedEarntypes = includedEarntypeGroup };

               var queryList = query.ToList();
               var entityList = new List<OvertimeCalculationDefinition>();
               foreach (var obj in queryList)
               {
                    try
                    {
                         //create the entity with the required DataContracts
                         var ocd = BuildOvertimeCalculationDefinitionEntity(obj.OvertimeCalculationDefinition, obj.WeeklyParameters, obj.DailyParameters, obj.includedEarntypes, earntypes);

                         //Add the entity to the entityList
                         entityList.Add(ocd);
                    }
                    catch
                    {
                         var message = "Something went wrong when trying to build an entity from the data contracts contained in the obj object";
                         LogDataError("ocd", null, obj, new ApplicationException(), message);
                    }

               }

               //Return the list of entities
               return entityList;
          }

          private OvertimeCalculationDefinition BuildOvertimeCalculationDefinitionEntity(DataContracts.OtCalcDefinition otcd, IEnumerable<DataContracts.OtcdWeeklyParms> wp, IEnumerable<DataContracts.OtcdDailyParms> dp, IEnumerable<DataContracts.OtcdInclEarntypes> ie, IEnumerable<DataContracts.Earntype> earntypes)
          {
               // build the OvertimeCalculationDefinition
               OvertimePayCalculationMethod overtimePayCalculationMethod;
               switch (otcd.OtCalculationMethod)
               {
                    case "WTDAVG":
                         overtimePayCalculationMethod = OvertimePayCalculationMethod.WeightedAverage;
                         break;
                    case "PRATE":
                         overtimePayCalculationMethod = OvertimePayCalculationMethod.PositionRate;
                         break;
                    default:
                         var message = "Unexpected: supplied OtCalculationMethod did not match any cases";
                         logger.Error(message);
                         throw new ApplicationException(message);
               }

               var overtimeCalculationDefinition = new OvertimeCalculationDefinition(otcd.Recordkey, otcd.OtDescription, overtimePayCalculationMethod);

               //build the weekly HoursThresholds
               foreach (var wparm in wp)
               {
                    // Create the new HoursThreshold
                    EarningsFactor defaultFactor = null;
                    EarningsFactor alternateFactor = null;
                    if (!string.IsNullOrWhiteSpace(wparm.OtcdWeeklyEarntype))
                    {
                         var earntype = earntypes.Where(et => et.Recordkey == wparm.OtcdWeeklyEarntype).FirstOrDefault();
                         var factor = (earntype != null) ? earntype.EtpOtFactor : null;
                         defaultFactor = new EarningsFactor(wparm.OtcdWeeklyEarntype, (decimal?)factor);
                    }
                    if (!string.IsNullOrWhiteSpace(wparm.OtcdWeeklyAltEarntype))
                    {
                         var earntype = earntypes.Where(et => et.Recordkey == wparm.OtcdWeeklyAltEarntype).FirstOrDefault();
                         var factor = (earntype != null) ? earntype.EtpOtFactor : null;
                         alternateFactor = new EarningsFactor(wparm.OtcdWeeklyAltEarntype, (decimal?)factor);
                    }
                    //wparm.OtcdWeeklyHours will never be null. A wparm can't be saved in Colleague if it doesn't have an hours threshold
                    var threshold = new HoursThreshold((decimal)wparm.OtcdWeeklyHours, defaultFactor, alternateFactor);
                    // Add the HoursThreshold to the list WeeklyHoursThreshold in the OvertimeCalculationDefinition
                    overtimeCalculationDefinition.WeeklyHoursThreshold.Add(threshold);
               }
               //build the daily HoursThresholds
               foreach (var dparm in dp)
               {
                    // Create the new HoursThreshold
                    EarningsFactor defaultFactor = null;
                    EarningsFactor alternateFactor = null;
                    if (!string.IsNullOrWhiteSpace(dparm.OtcdDailyEarntype))
                    {
                         var earntype = earntypes.Where(et => et.Recordkey == dparm.OtcdDailyEarntype).FirstOrDefault();
                         var factor = (earntype != null) ? earntype.EtpOtFactor : null;
                         defaultFactor = new EarningsFactor(dparm.OtcdDailyEarntype, (decimal?)factor);
                    }
                    if (!string.IsNullOrWhiteSpace(dparm.OtcdDailyAltEarntype))
                    {
                         var earntype = earntypes.Where(et => et.Recordkey == dparm.OtcdDailyAltEarntype).FirstOrDefault();
                         var factor = (earntype != null) ? earntype.EtpOtFactor : null;
                         alternateFactor = new EarningsFactor(dparm.OtcdDailyAltEarntype, (decimal?)factor);
                    }
                    //dparm.OtcdWeeklyHours will never be null. A dparm can't be saved in Colleague if it doesn't have an hours threshold
                    var threshold = new HoursThreshold((decimal)dparm.OtcdDailyHours, defaultFactor, alternateFactor);
                    // Add the HoursThreshold to the list WeeklyHoursThreshold in the OvertimeCalculationDefinition
                    overtimeCalculationDefinition.DailyHoursThreshold.Add(threshold);
               }
               //build the IncludedEarningsType
               foreach (var iet in ie)
               {
                    var condition = false;
                    if (iet.OtcdUseAltOtEarntype.Equals("Y", StringComparison.InvariantCultureIgnoreCase)) condition = true;
                    var includedEarningsType = new IncludedEarningsType(iet.OtcdInclEarntype, condition);
                    overtimeCalculationDefinition.IncludedEarningsTypes.Add(includedEarningsType);
               }

               return overtimeCalculationDefinition;
          }
     }
}

