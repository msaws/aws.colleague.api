﻿/*Copyright 2016-2017 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ellucian.Web.Http.Configuration;
using Ellucian.Colleague.Domain.Base.Exceptions;
using System.Collections.ObjectModel;
using Ellucian.Colleague.Domain.TimeManagement;



namespace Ellucian.Colleague.Data.TimeManagement.Repositories
{
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class TimecardsRepository : BaseColleagueRepository, ITimecardsRepository
    {
        private readonly string colleagueTimeZone;
        private readonly int bulkReadSize;
        private int _version = 2;

        #region CONSTRUCTOR(S)

        /// <summary>
        /// Repository constrcutor
        /// </summary>
        /// <param name="cacheProvider"></param>
        /// <param name="transactionFactory"></param>
        /// <param name="logger"></param>
        /// <param name="settings"></param>
        public TimecardsRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings settings)
            : base(cacheProvider, transactionFactory, logger)
        {
            colleagueTimeZone = settings.ColleagueTimeZone;
            bulkReadSize = settings != null && settings.BulkReadSize > 0 ? settings.BulkReadSize : 5000;
        }
        #endregion

        #region GET TIMECARDS
        /// <summary>
        /// Gets a set of Timecards
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns>A timecard list task</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        public async Task<IEnumerable<Timecard>> GetTimecardsAsync(List<string> inputEmployeeIds)
        {
            _version = 1;
            if (inputEmployeeIds == null)
            {
                var message = "Employee Ids are required to get time cards";
                logger.Error(message);
                throw new ArgumentNullException(message);
            }

            var employeeIds = inputEmployeeIds.Where(e => !string.IsNullOrWhiteSpace(e)).Distinct();

            if (!employeeIds.Any())
            {
                var message = "No employee ids found in input list";
                logger.Error(message);
                throw new ArgumentException(message);
            }

            #region DATA ACCESS

            var timecardCriteria = "WITH TCD.EMPLOYEE.ID EQ ?";
            var timecardKeys = await DataReader.SelectAsync("TIMECARD", timecardCriteria, employeeIds.Select(id => string.Format("\"{0}\"", id)).ToArray());
            if (timecardKeys == null)
            {
                var message = "Unexpected null returned from TIMECARD SelectAsync";
                logger.Error(message);
                throw new ApplicationException(message);
            }
            if (!timecardKeys.Any())
            {
                logger.Info("No TIMECARD keys exist for the given employee Ids: " + string.Join(",", employeeIds));
            }
            var timeEntryCriteria = "WITH TE.TIMECARD.ID EQ ?";
            var timeEntryKeys = await DataReader.SelectAsync("TIME.ENTRY", timeEntryCriteria, timecardKeys.Select(key => string.Format("\"{0}\"", key)).ToArray());
            if (timeEntryKeys == null)
            {
                var message = "Unexpected null returned from TIME.ENTRY SelectAsyc";
                logger.Error(message);
                throw new ApplicationException(message);
            }
            if (!timeEntryKeys.Any())
            {
                logger.Info("No TIME.ENTRY keys exist for the given Timecard Ids: " + string.Join(",", timecardKeys));
            }

            var dbTimecards = new List<DataContracts.Timecard>();
            var dbTimeEntries = new List<DataContracts.TimeEntry>();

            for (int i = 0; i < timecardKeys.Count(); i += bulkReadSize)
            {
                var subList = timecardKeys.Skip(i).Take(bulkReadSize);
                var records = await DataReader.BulkReadRecordAsync<DataContracts.Timecard>(subList.ToArray());
                if (records == null)
                {
                    logger.Error("Unexpected null from bulk read of Timecard records");
                }
                else
                {
                    dbTimecards.AddRange(records);
                }
            }

            for (int i = 0; i < timeEntryKeys.Count(); i += bulkReadSize)
            {
                var subList = timeEntryKeys.Skip(i).Take(bulkReadSize);
                var records = await DataReader.BulkReadRecordAsync<DataContracts.TimeEntry>(subList.ToArray());
                if (records == null)
                {
                    logger.Error("Unexpected null from bulk read of Time Entry records");
                }
                else
                {
                    dbTimeEntries.AddRange(records);
                }
            }

            #endregion

            #region DOMAIN MAPPING

            var domainTimeEntries = new List<TimeEntry>();
            var domainTimecards = new List<Timecard>();

            //group time entry records by timecard id and create a dictionary lookup based on the timecard id for quick access.
            var timeEntryRecordDict = dbTimeEntries.GroupBy(te => te.TeTimecardId).ToDictionary(te => te.Key);

            //loop through each timecard record
            foreach (var timecardRecord in dbTimecards)
            {
                try
                {
                    var timecardEntity = mapToDomainTimecard(timecardRecord);

                    //make sure the dictionary contains the timecard id as a key before continuing. 
                    //if the key doesn't exist in the dictionary, there are probably no time entries for this timecard
                    if (timeEntryRecordDict.ContainsKey(timecardEntity.Id))
                    {
                        //loop through each time entry record for the current timecard id
                        foreach (var timeEntryRecord in timeEntryRecordDict[timecardEntity.Id])
                        {
                            try
                            {
                                timecardEntity.TimeEntries.Add(mapToDomainTimeEntry(timeEntryRecord));
                            }
                            catch (Exception e)
                            {
                                LogDataError("TIME.ENTRY", timeEntryRecord.Recordkey, timeEntryRecord, e, "Unable to build TimeEntry from db record");
                            }
                        }
                    }

                    domainTimecards.Add(timecardEntity);
                }
                catch (Exception e)
                {
                    LogDataError("TIMECARD", timecardRecord.Recordkey, timecardRecord, e, "Unable to build Timecard from db record");
                }
            }
            #endregion

            return domainTimecards;
        }
        #endregion

        #region GET A SINGLE TIMECARD
        
        /// <summary>
        /// Gets a single Timecard
        /// </summary>
        /// <param name="timecardId"></param>
        /// <returns>A timecard task</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        public async Task<Timecard> GetTimecardAsync(string timecardId)
        {
            _version = 1;
            if (string.IsNullOrWhiteSpace(timecardId))
            {
                var message = "Timecard Id required to get time card";
                logger.Error(message);
                throw new ArgumentNullException(message);
            }

            var dbTimecard = await DataReader.ReadRecordAsync<DataContracts.Timecard>(timecardId);

            if (dbTimecard == null)
            {
                var message = "TIMECARD " + timecardId + " does not exist.";
                logger.Error(message);
                throw new KeyNotFoundException(message);
            }

            var timeEntryCriteria = "WITH TE.TIMECARD.ID EQ " + timecardId;
            var dbTimeEntries = await DataReader.BulkReadRecordAsync<DataContracts.TimeEntry>(timeEntryCriteria);

            if (dbTimeEntries == null)
            {
                var message = "Unexpected null returned from TIME.ENTRY BulkReadRecordAsync";
                logger.Error(message);
                dbTimeEntries = new Collection<DataContracts.TimeEntry>();
            }

            try
            {
                var domainTimecard = mapToDomainTimecard(dbTimecard);

                foreach (var te in dbTimeEntries)
                {
                    try
                    {
                        domainTimecard.TimeEntries.Add(this.mapToDomainTimeEntry(te));
                    }
                    catch (Exception e)
                    {
                        LogDataError("TIME.ENTRY", te.Recordkey, te, e, "Unable to build Time Entry from db record");
                    }
                }

                return domainTimecard;
            }
            catch (Exception e)
            {
                LogDataError("TIMECARD", dbTimecard.Recordkey, dbTimecard, e, "Unable to build Timecard from db record");
                throw;
            }
        }
        #endregion

        #region ADD A SINGLE TIMECARD
    
        /// <summary>
        /// Adds a single Timecard
        /// </summary>
        /// <param name="timecardToAdd"></param>
        /// <returns>A timecard task</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        public async Task<Timecard> CreateTimecardAsync(Timecard timecardToAdd)
        {
            _version = 1;
            if (timecardToAdd == null)
            {
                var message = "Timecard object is required to add a timecard";
                logger.Error(message);
                throw new ArgumentNullException(message);
            }

            var request = new Ellucian.Colleague.Data.TimeManagement.Transactions.CreateTimecardRequest()
            {
                TcdEmployeeId = timecardToAdd.EmployeeId,
                TcdPayCycleId = timecardToAdd.PayCycleId,
                TcdPeriodStartDate = DateTime.SpecifyKind(timecardToAdd.PeriodStartDate.Date, DateTimeKind.Unspecified),
                TcdPeriodEndDate = DateTime.SpecifyKind(timecardToAdd.PeriodEndDate.Date, DateTimeKind.Unspecified),
                TcdStartDate = DateTime.SpecifyKind(timecardToAdd.StartDate.Date, DateTimeKind.Unspecified),
                TcdEndDate = DateTime.SpecifyKind(timecardToAdd.EndDate.Date, DateTimeKind.Unspecified),
                TcdPositionId = timecardToAdd.PositionId,
                CreateTimeEntries = timecardToAdd.TimeEntries.Select(te => new Ellucian.Colleague.Data.TimeManagement.Transactions.CreateTimeEntries()
                {
                    TeEarntypeId = te.EarningsTypeId,
                    TeInDate = te.InDateTime.HasValue ? (DateTime?)te.InDateTime.Value.ToLocalDateTime(colleagueTimeZone).Date : null,
                    TeOutDate = te.OutDateTime.HasValue ? (DateTime?)te.OutDateTime.Value.ToLocalDateTime(colleagueTimeZone).Date : null,
                    TeInTime = te.InDateTime.HasValue ? te.InDateTime.ToLocalDateTime(colleagueTimeZone) : null,
                    TeOutTime = te.OutDateTime.HasValue ? te.OutDateTime.ToLocalDateTime(colleagueTimeZone) : null,
                    TeWorkedDate = DateTime.SpecifyKind(te.WorkedDate.Date, DateTimeKind.Unspecified),
                    TeWorkedMinutes = te.WorkedTime.HasValue ? (int?)te.WorkedTime.Value.TotalMinutes : null,
                    TePerleaveId = te.PersonLeaveId,
                    TeProjectId = te.ProjectId
                }
                ).ToList()
            };

            var response = await transactionInvoker.ExecuteAsync<Ellucian.Colleague.Data.TimeManagement.Transactions.CreateTimecardRequest,
                Ellucian.Colleague.Data.TimeManagement.Transactions.CreateTimecardResponse>(request);

            if (response == null)
            {
                var message = "Could not create timecard. Unexpected null response from CTX.";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            if (!string.IsNullOrEmpty(response.ErrorMessage))
            {
                logger.Error(response.ErrorMessage);

                throw new ApplicationException(response.ErrorMessage);
            }

            return await GetTimecardAsync(response.OutTimecardId);
        }
        #endregion

        #region UPDATE TIMECARDS
        /// <summary>
        /// Update a Timecard
        /// </summary>
        /// <param name="timecard"></param>
        /// <returns>A timecard task</returns>
        [Obsolete("This method is obsolete as of API release 1.15; Please use version 2 instead")]
        public async Task<Timecard> UpdateTimecardAsync(TimecardHelper timecardHelper)
        {
            _version = 1;
            if (timecardHelper == null)
            {
                var message = "timecardHelper is null";
                logger.Error(message);
                throw new ArgumentNullException(message);
            }

            var allTimeEntries = new List<Ellucian.Colleague.Data.TimeManagement.Transactions.TimeEntries>();

            var timeEntriesToCreate = timecardHelper.TimeEntriesToCreate.Select(te => new Ellucian.Colleague.Data.TimeManagement.Transactions.TimeEntries()
            {
                TeType = "ADD",
                TimeEntryId = "",
                TeEarntypeId = te.EarningsTypeId,
                TeInDate = te.InDateTime.HasValue ? (DateTime?)te.InDateTime.Value.ToLocalDateTime(colleagueTimeZone).Date : null,
                TeOutDate = te.OutDateTime.HasValue ? (DateTime?)te.OutDateTime.Value.ToLocalDateTime(colleagueTimeZone).Date : null,
                TeInTime = te.InDateTime.ToLocalDateTime(colleagueTimeZone),
                TeOutTime = te.OutDateTime.ToLocalDateTime(colleagueTimeZone),
                TeWorkedDate = DateTime.SpecifyKind(te.WorkedDate.Date, DateTimeKind.Unspecified),
                TeWorkedMinutes = te.WorkedTime.HasValue ? (int?)te.WorkedTime.Value.TotalMinutes : null,
                TePerleaveId = te.PersonLeaveId,
                TeProjectId = te.ProjectId
            }).ToList();

            allTimeEntries.AddRange(timeEntriesToCreate);

            var timeEntriesToUpdate = timecardHelper.TimeEntriesToUpdate.Select(te => new Ellucian.Colleague.Data.TimeManagement.Transactions.TimeEntries()
            {
                TeType = "UPDATE",
                TimeEntryId = te.Id,
                TeEarntypeId = te.EarningsTypeId,
                TeInDate = te.InDateTime.HasValue ? (DateTime?)te.InDateTime.Value.ToLocalDateTime(colleagueTimeZone).Date : null,
                TeOutDate = te.OutDateTime.HasValue ? (DateTime?)te.OutDateTime.Value.ToLocalDateTime(colleagueTimeZone).Date : null,
                TeInTime = te.InDateTime.ToLocalDateTime(colleagueTimeZone),
                TeOutTime = te.OutDateTime.ToLocalDateTime(colleagueTimeZone),
                TeWorkedDate = DateTime.SpecifyKind(te.WorkedDate.Date, DateTimeKind.Unspecified),
                TeWorkedMinutes = te.WorkedTime.HasValue ? (int?)te.WorkedTime.Value.TotalMinutes : null,
                TePerleaveId = te.PersonLeaveId,
                TeProjectId = te.ProjectId
            }).ToList();

            allTimeEntries.AddRange(timeEntriesToUpdate);

            var timeEntriesToDelete = timecardHelper.TimeEntriesToDelete.Select(te => new Ellucian.Colleague.Data.TimeManagement.Transactions.TimeEntries()
            {
                TeType = "DELETE",
                TimeEntryId = te.Id,
            }).ToList();

            allTimeEntries.AddRange(timeEntriesToDelete);

            var request = new Ellucian.Colleague.Data.TimeManagement.Transactions.UpdateTimecardRequest()

            {
                TimecardId = timecardHelper.Timecard.Id,
                TcdEmployeeId = timecardHelper.Timecard.EmployeeId,
                TcdPayCycleId = timecardHelper.Timecard.PayCycleId,
                TcdPeriodStartDate = DateTime.SpecifyKind(timecardHelper.Timecard.PeriodStartDate.Date, DateTimeKind.Unspecified),
                TcdPeriodEndDate = DateTime.SpecifyKind(timecardHelper.Timecard.PeriodEndDate.Date, DateTimeKind.Unspecified),
                TcdPositionId = timecardHelper.Timecard.PositionId,
                TimeEntries = allTimeEntries
            };

            var response = await transactionInvoker.ExecuteAsync<Ellucian.Colleague.Data.TimeManagement.Transactions.UpdateTimecardRequest,
                Ellucian.Colleague.Data.TimeManagement.Transactions.UpdateTimecardResponse>(request);

            if (response == null)
            {
                var message = "Could not update timecard. Unexpected null response from CTX.";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            if (!string.IsNullOrEmpty(response.ErrorMessage))
            {
                logger.Error(response.ErrorMessage);
                if (response.ErrorMessage.StartsWith("CONFLICT", StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new RecordLockException(response.ErrorMessage, "TIMECARD", timecardHelper.Timecard.EmployeeId);
                }
                else
                {
                    throw new ApplicationException(response.ErrorMessage);
                }
            }

            return await GetTimecardAsync(timecardHelper.Timecard.Id);
        }
        #endregion

        #region GET Timecards2
        /// <summary>
        /// Gets a set of Timecards
        /// </summary>
        /// <param name="employeeId"></param>
        /// <returns>A timecard list task</returns>
        public async Task<IEnumerable<Timecard2>> GetTimecards2Async(List<string> inputEmployeeIds)
        {
            _version = 2;
            if (inputEmployeeIds == null)
            {
                var message = "Employee Ids are required to get time cards";
                logger.Error(message);
                throw new ArgumentNullException(message);
            }

            var employeeIds = inputEmployeeIds.Where(e => !string.IsNullOrWhiteSpace(e)).Distinct();

            if (!employeeIds.Any())
            {
                var message = "No employee ids found in input list";
                logger.Error(message);
                throw new ArgumentException(message);
            }

            #region DATA ACCESS

            var timecardCriteria = "WITH TCD.EMPLOYEE.ID EQ ?";
            var timecardKeys = await DataReader.SelectAsync("TIMECARD", timecardCriteria, employeeIds.Select(id => string.Format("\"{0}\"", id)).ToArray());
            if (timecardKeys == null)
            {
                var message = "Unexpected null returned from TIMECARD SelectAsync";
                logger.Error(message);
                throw new ApplicationException(message);
            }
            if (!timecardKeys.Any())
            {
                logger.Info("No TIMECARD keys exist for the given employee Ids: " + string.Join(",", employeeIds));
            }
            var timeEntryCriteria = "WITH TE.TIMECARD.ID EQ ?";
            var timeEntryKeys = await DataReader.SelectAsync("TIME.ENTRY", timeEntryCriteria, timecardKeys.Select(key => string.Format("\"{0}\"", key)).ToArray());
            if (timeEntryKeys == null)
            {
                var message = "Unexpected null returned from TIME.ENTRY SelectAsyc";
                logger.Error(message);
                throw new ApplicationException(message);
            }
            if (!timeEntryKeys.Any())
            {
                logger.Info("No TIME.ENTRY keys exist for the given Timecard Ids: " + string.Join(",", timecardKeys));
            }

            var dbTimecards = new List<DataContracts.Timecard>();
            var dbTimeEntries = new List<DataContracts.TimeEntry>();

            for (int i = 0; i < timecardKeys.Count(); i += bulkReadSize)
            {
                var subList = timecardKeys.Skip(i).Take(bulkReadSize);
                var records = await DataReader.BulkReadRecordAsync<DataContracts.Timecard>(subList.ToArray());
                if (records == null)
                {
                    logger.Error("Unexpected null from bulk read of Timecard records");
                }
                else
                {
                    dbTimecards.AddRange(records);
                }
            }

            for (int i = 0; i < timeEntryKeys.Count(); i += bulkReadSize)
            {
                var subList = timeEntryKeys.Skip(i).Take(bulkReadSize);
                var records = await DataReader.BulkReadRecordAsync<DataContracts.TimeEntry>(subList.ToArray());
                if (records == null)
                {
                    logger.Error("Unexpected null from bulk read of Time Entry records");
                }
                else
                {
                    dbTimeEntries.AddRange(records);
                }
            }

            #endregion

            #region DOMAIN MAPPING

            var domainTimeEntries2 = new List<TimeEntry2>();
            var domainTimecards2 = new List<Timecard2>();

            //group time entry records by timecard id and create a dictionary lookup based on the timecard id for quick access.
            var timeEntryRecordDict = dbTimeEntries.GroupBy(te => te.TeTimecardId).ToDictionary(te => te.Key);

            //loop through each timecard record
            foreach (var timecardRecord in dbTimecards)
            {
                try
                {
                    Timecard2 timecard2Entity = mapToDomainTimecard(timecardRecord);

                    //make sure the dictionary contains the timecard id as a key before continuing. 
                    //if the key doesn't exist in the dictionary, there are probably no time entries for this timecard
                    if (timeEntryRecordDict.ContainsKey(timecard2Entity.Id))
                    {
                        //loop through each time entry record for the current timecard id
                        foreach (var timeEntryRecord in timeEntryRecordDict[timecard2Entity.Id])
                        {
                            try
                            {
                                timecard2Entity.TimeEntries.Add(mapToDomainTimeEntry(timeEntryRecord));
                            }
                            catch (Exception e)
                            {
                                LogDataError("TIME.ENTRY", timeEntryRecord.Recordkey, timeEntryRecord, e, "Unable to build TimeEntry from db record");
                            }
                        }
                    }

                    domainTimecards2.Add(timecard2Entity);
                }
                catch (Exception e)
                {
                    LogDataError("TIMECARD", timecardRecord.Recordkey, timecardRecord, e, "Unable to build Timecard from db record");
                }
            }
            #endregion

            return domainTimecards2;
        }
        #endregion

        #region GET A SINGLE TIMECARD2

        /// <summary>
        /// Gets a single Timecard
        /// </summary>
        /// <param name="timecardId"></param>
        /// <returns>A timecard task</returns>
        public async Task<Timecard2> GetTimecard2Async(string timecardId)
        {
            _version = 2;
            if (string.IsNullOrWhiteSpace(timecardId))
            {
                var message = "Timecard Id required to get time card";
                logger.Error(message);
                throw new ArgumentNullException(message);
            }

            var dbTimecard = await DataReader.ReadRecordAsync<DataContracts.Timecard>(timecardId);

            if (dbTimecard == null)
            {
                var message = "TIMECARD " + timecardId + " does not exist.";
                logger.Error(message);
                throw new KeyNotFoundException(message);
            }

            var timeEntryCriteria = "WITH TE.TIMECARD.ID EQ " + timecardId;
            var dbTimeEntries = await DataReader.BulkReadRecordAsync<DataContracts.TimeEntry>(timeEntryCriteria);

            if (dbTimeEntries == null)
            {
                var message = "Unexpected null returned from TIME.ENTRY BulkReadRecordAsync";
                logger.Error(message);
                dbTimeEntries = new Collection<DataContracts.TimeEntry>();
            }

            try
            {
                var domainTimecard = mapToDomainTimecard(dbTimecard);

                foreach (var te in dbTimeEntries)
                {
                    try
                    {
                        domainTimecard.TimeEntries.Add(this.mapToDomainTimeEntry(te));
                    }
                    catch (Exception e)
                    {
                        LogDataError("TIME.ENTRY", te.Recordkey, te, e, "Unable to build Time Entry from db record");
                    }
                }

                return domainTimecard;
            }
            catch (Exception e)
            {
                LogDataError("TIMECARD", dbTimecard.Recordkey, dbTimecard, e, "Unable to build Timecard from db record");
                throw;
            }
        }
        #endregion

        #region ADD A SINGLE TIMECARD2

        /// <summary>
        /// Adds a single Timecard
        /// </summary>
        /// <param name="timecardToAdd"></param>
        /// <returns>A timecard task</returns>
        public async Task<Timecard2> CreateTimecard2Async(Timecard2 timecardToAdd)
        {
            _version = 2;
            if (timecardToAdd == null)
            {
                var message = "Timecard object is required to add a timecard";
                logger.Error(message);
                throw new ArgumentNullException(message);
            }

            var request = new Ellucian.Colleague.Data.TimeManagement.Transactions.CreateTimecardRequest()
            {
                TcdEmployeeId = timecardToAdd.EmployeeId,
                TcdPayCycleId = timecardToAdd.PayCycleId,
                TcdPeriodStartDate = DateTime.SpecifyKind(timecardToAdd.PeriodStartDate.Date, DateTimeKind.Unspecified),
                TcdPeriodEndDate = DateTime.SpecifyKind(timecardToAdd.PeriodEndDate.Date, DateTimeKind.Unspecified),
                TcdStartDate = DateTime.SpecifyKind(timecardToAdd.StartDate.Date, DateTimeKind.Unspecified),
                TcdEndDate = DateTime.SpecifyKind(timecardToAdd.EndDate.Date, DateTimeKind.Unspecified),
                TcdPositionId = timecardToAdd.PositionId,
                CreateTimeEntries = timecardToAdd.TimeEntries.Select(te => new Ellucian.Colleague.Data.TimeManagement.Transactions.CreateTimeEntries()
                {
                    TeEarntypeId = te.EarningsTypeId,
                    TeInDate = te.InDateTime.HasValue ? DateTime.SpecifyKind(te.InDateTime.Value, DateTimeKind.Unspecified) as DateTime? : null,
                    TeOutDate = te.OutDateTime.HasValue ? DateTime.SpecifyKind(te.OutDateTime.Value, DateTimeKind.Unspecified) as DateTime? : null,
                    TeInTime = te.InDateTime.HasValue ? DateTime.SpecifyKind(te.InDateTime.Value, DateTimeKind.Unspecified) as DateTime? : null,
                    TeOutTime = te.OutDateTime.HasValue ? DateTime.SpecifyKind(te.OutDateTime.Value, DateTimeKind.Unspecified) as DateTime? : null,
                    TeWorkedDate = DateTime.SpecifyKind(te.WorkedDate.Date, DateTimeKind.Unspecified),
                    TeWorkedMinutes = te.WorkedTime.HasValue ? (int?)te.WorkedTime.Value.TotalMinutes : null,
                    TePerleaveId = te.PersonLeaveId,
                    TeProjectId = te.ProjectId
                }
                ).ToList()
            };

            var response = await transactionInvoker.ExecuteAsync<Ellucian.Colleague.Data.TimeManagement.Transactions.CreateTimecardRequest,
                Ellucian.Colleague.Data.TimeManagement.Transactions.CreateTimecardResponse>(request);

            if (response == null)
            {
                var message = "Could not create timecard. Unexpected null response from CTX.";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            if (!string.IsNullOrEmpty(response.ErrorMessage))
            {
                logger.Error(response.ErrorMessage);

                throw new ApplicationException(response.ErrorMessage);
            }

            return await GetTimecard2Async(response.OutTimecardId);
        }
        #endregion

        #region UPDATE Timecards2
        /// <summary>
        /// Update a Timecard
        /// </summary>
        /// <param name="timecard"></param>
        /// <returns>A timecard task</returns>
        public async Task<Timecard2> UpdateTimecard2Async(Timecard2Helper timecard2Helper)
        {
            _version = 2;
            if (timecard2Helper == null)
            {
                var message = "timecardHelper is null";
                logger.Error(message);
                throw new ArgumentNullException(message);
            }

            var allTimeEntries = new List<Ellucian.Colleague.Data.TimeManagement.Transactions.TimeEntries>();

            var timeEntriesToCreate = timecard2Helper.TimeEntrysToCreate.Select(te => new Ellucian.Colleague.Data.TimeManagement.Transactions.TimeEntries()
            {
                TeType = "ADD",
                TimeEntryId = "",
                TeEarntypeId = te.EarningsTypeId,
                TeInDate = te.InDateTime.HasValue ? DateTime.SpecifyKind(te.InDateTime.Value, DateTimeKind.Unspecified) as DateTime? : null,
                TeOutDate = te.OutDateTime.HasValue ? DateTime.SpecifyKind(te.OutDateTime.Value, DateTimeKind.Unspecified) as DateTime? : null,
                TeInTime = te.InDateTime.HasValue ? DateTime.SpecifyKind(te.InDateTime.Value, DateTimeKind.Unspecified) as DateTime? : null,
                TeOutTime = te.OutDateTime.HasValue ? DateTime.SpecifyKind(te.OutDateTime.Value, DateTimeKind.Unspecified) as DateTime? : null,
                TeWorkedDate = DateTime.SpecifyKind(te.WorkedDate.Date, DateTimeKind.Unspecified),
                TeWorkedMinutes = te.WorkedTime.HasValue ? (int?)te.WorkedTime.Value.TotalMinutes : null,
                TePerleaveId = te.PersonLeaveId,
                TeProjectId = te.ProjectId
            }).ToList();

            allTimeEntries.AddRange(timeEntriesToCreate);

            var timeEntriesToUpdate = timecard2Helper.TimeEntrysToUpdate.Select(te => new Ellucian.Colleague.Data.TimeManagement.Transactions.TimeEntries()
            {
                TeType = "UPDATE",
                TimeEntryId = te.Id,
                TeEarntypeId = te.EarningsTypeId,
                TeInDate = te.InDateTime.HasValue ? DateTime.SpecifyKind(te.InDateTime.Value, DateTimeKind.Unspecified) as DateTime? : null,
                TeOutDate = te.OutDateTime.HasValue ? DateTime.SpecifyKind(te.OutDateTime.Value, DateTimeKind.Unspecified) as DateTime? : null,
                TeInTime = te.InDateTime.HasValue ? DateTime.SpecifyKind(te.InDateTime.Value, DateTimeKind.Unspecified) as DateTime? : null,
                TeOutTime = te.OutDateTime.HasValue ? DateTime.SpecifyKind(te.OutDateTime.Value, DateTimeKind.Unspecified) as DateTime? : null,
                TeWorkedDate = DateTime.SpecifyKind(te.WorkedDate.Date, DateTimeKind.Unspecified),
                TeWorkedMinutes = te.WorkedTime.HasValue ? (int?)te.WorkedTime.Value.TotalMinutes : null,
                TePerleaveId = te.PersonLeaveId,
                TeProjectId = te.ProjectId
            }).ToList();

            allTimeEntries.AddRange(timeEntriesToUpdate);

            var timeEntriesToDelete = timecard2Helper.TimeEntrysToDelete.Select(te => new Ellucian.Colleague.Data.TimeManagement.Transactions.TimeEntries()
            {
                TeType = "DELETE",
                TimeEntryId = te.Id,
            }).ToList();

            allTimeEntries.AddRange(timeEntriesToDelete);

            var request = new Ellucian.Colleague.Data.TimeManagement.Transactions.UpdateTimecardRequest()

            {
                TimecardId = timecard2Helper.Timecard.Id,
                TcdEmployeeId = timecard2Helper.Timecard.EmployeeId,
                TcdPayCycleId = timecard2Helper.Timecard.PayCycleId,
                TcdPeriodStartDate = DateTime.SpecifyKind(timecard2Helper.Timecard.PeriodStartDate.Date, DateTimeKind.Unspecified),
                TcdPeriodEndDate = DateTime.SpecifyKind(timecard2Helper.Timecard.PeriodEndDate.Date, DateTimeKind.Unspecified),
                TcdPositionId = timecard2Helper.Timecard.PositionId,
                TimeEntries = allTimeEntries
            };

            var response = await transactionInvoker.ExecuteAsync<Ellucian.Colleague.Data.TimeManagement.Transactions.UpdateTimecardRequest,
                Ellucian.Colleague.Data.TimeManagement.Transactions.UpdateTimecardResponse>(request);

            if (response == null)
            {
                var message = "Could not update timecard. Unexpected null response from CTX.";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            if (!string.IsNullOrEmpty(response.ErrorMessage))
            {
                logger.Error(response.ErrorMessage);
                if (response.ErrorMessage.StartsWith("CONFLICT", StringComparison.InvariantCultureIgnoreCase))
                {
                    throw new RecordLockException(response.ErrorMessage, "TIMECARD", timecard2Helper.Timecard.EmployeeId);
                }
                else
                {
                    throw new ApplicationException(response.ErrorMessage);
                }
            }

            return await GetTimecard2Async(timecard2Helper.Timecard.Id);
        }
        #endregion

        #region HELPERS

        private dynamic mapToDomainTimecard(DataContracts.Timecard tc)
        {
            if (!tc.TcdPeriodStartDate.HasValue || !tc.TcdPeriodEndDate.HasValue)
            {
                var message = "Both timecard period start date and timecard period end date are required for timecard when reading from database";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            if (!tc.TimecardAdddate.HasValue || !tc.TimecardChgdate.HasValue || !tc.TimecardAddtime.HasValue || !tc.TimecardAddtime.HasValue)
            {
                var message = "Timestamp is required for timecard when reading from database";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            if (!tc.TcdStartDate.HasValue || !tc.TcdEndDate.HasValue)
            {
                var message = "Both timecard start date and timecard end date are required for timecard when reading from database";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            if (_version == 2)
            {
                return new Timecard2(tc.TcdEmployeeId, tc.TcdPaycycleId, tc.TcdPositionId, tc.TcdPeriodStartDate.Value, tc.TcdPeriodEndDate.Value, tc.TcdStartDate.Value, tc.TcdEndDate.Value, tc.Recordkey)
                {
                    Timestamp = new Timestamp(tc.TimecardAddopr,
                        tc.TimecardAddtime.ToPointInTimeDateTimeOffset(tc.TimecardAdddate, colleagueTimeZone).Value,
                        tc.TimecardChgopr,
                        tc.TimecardChgtime.ToPointInTimeDateTimeOffset(tc.TimecardChgdate, colleagueTimeZone).Value),
                    TimeEntries = new List<TimeEntry2>(),
                };
            }
            else
            {
                return new Timecard(tc.TcdEmployeeId, tc.TcdPaycycleId, tc.TcdPositionId, tc.TcdPeriodStartDate.Value, tc.TcdPeriodEndDate.Value, tc.TcdStartDate.Value, tc.TcdEndDate.Value, tc.Recordkey)
                {
                    Timestamp = new Timestamp(tc.TimecardAddopr,
                        tc.TimecardAddtime.ToPointInTimeDateTimeOffset(tc.TimecardAdddate, colleagueTimeZone).Value,
                        tc.TimecardChgopr,
                        tc.TimecardChgtime.ToPointInTimeDateTimeOffset(tc.TimecardChgdate, colleagueTimeZone).Value),
                    TimeEntries = new List<TimeEntry>(),
                };
            }
        }

        private dynamic mapToDomainTimeEntry(DataContracts.TimeEntry te)
        {

            if (!te.TeWorkedDate.HasValue)
            {
                var message = "Worked Date is required for time entry when reading from database";
                throw new ApplicationException(message);
            }

            if (!te.TimeEntryAdddate.HasValue || !te.TimeEntryChgdate.HasValue || !te.TimeEntryAddtime.HasValue || !te.TimeEntryChgdate.HasValue)
            {
                var message = "Timestamp is required for time entry when reading from database";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            if (_version == 2)
            {
                var inDateTime = TimeMangementUtility.SetTimeOfDate(te.TeInDate, te.TeInTime);
                var outDateTime = TimeMangementUtility.SetTimeOfDate(te.TeOutDate, te.TeOutTime);

                TimeEntry2 entity = null;
                if (inDateTime.HasValue || outDateTime.HasValue)
                {
                    entity = new TimeEntry2(te.TeTimecardId, te.TeEarntypeId, inDateTime, outDateTime, te.TeWorkedDate.Value, te.Recordkey);
                }
                else
                {
                    TimeSpan? workedTime = null;
                    if (te.TeWorkedMinutes.HasValue)
                    {
                        workedTime = TimeSpan.FromMinutes((double)te.TeWorkedMinutes.Value);
                    }
                    entity = new TimeEntry2(te.TeTimecardId, te.TeEarntypeId, workedTime, te.TeWorkedDate.Value, te.Recordkey);
                }

                entity.Timestamp = new Timestamp(te.TimeEntryAddopr,
                            te.TimeEntryAddtime.ToPointInTimeDateTimeOffset(te.TimeEntryAdddate, colleagueTimeZone).Value,
                            te.TimeEntryChgopr,
                            te.TimeEntryChgtime.ToPointInTimeDateTimeOffset(te.TimeEntryChgdate, colleagueTimeZone).Value);

                return entity;
            }
            else
            {
                var inDateTime = te.TeInTime.ToPointInTimeDateTimeOffset(te.TeInDate, colleagueTimeZone);
                var outDateTime = te.TeOutTime.ToPointInTimeDateTimeOffset(te.TeOutDate, colleagueTimeZone);

                TimeEntry entity = null;
                if (inDateTime.HasValue || outDateTime.HasValue)
                {
                    entity = new TimeEntry(te.TeTimecardId, te.TeEarntypeId, inDateTime, outDateTime, te.TeWorkedDate.Value, te.Recordkey);
                }
                else
                {
                    TimeSpan? workedTime = null;
                    if (te.TeWorkedMinutes.HasValue)
                    {
                        workedTime = TimeSpan.FromMinutes((double)te.TeWorkedMinutes.Value);
                    }
                    entity = new TimeEntry(te.TeTimecardId, te.TeEarntypeId, workedTime, te.TeWorkedDate.Value, te.Recordkey);
                }

                entity.Timestamp = new Timestamp(te.TimeEntryAddopr,
                            te.TimeEntryAddtime.ToPointInTimeDateTimeOffset(te.TimeEntryAdddate, colleagueTimeZone).Value,
                            te.TimeEntryChgopr,
                            te.TimeEntryChgtime.ToPointInTimeDateTimeOffset(te.TimeEntryChgdate, colleagueTimeZone).Value);

                return entity;
            }
        }

        #endregion
    }
}
