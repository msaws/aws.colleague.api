﻿/*Copyright 2016 Ellucian Company L.P. and its affiliates.*/
using Ellucian.Colleague.Data.TimeManagement.Transactions;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.TimeManagement.Repositories
{
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class OvertimeCalculationRepository : BaseColleagueRepository, IOvertimeCalculationRepository
    {
        public OvertimeCalculationRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger)
            : base(cacheProvider, transactionFactory, logger)
        {
        }



        public async Task<OvertimeCalculationResult> CalculateOvertime(string personId, DateTime startDate, DateTime endDate, string payCycleId)
        {
            //argument checking
            if (string.IsNullOrWhiteSpace(personId))
            {
                throw new ArgumentNullException("personId");
            }

            if (string.IsNullOrWhiteSpace(payCycleId))
            {
                throw new ArgumentNullException("payCycleId");
            }

            if (startDate > endDate)
            {
                throw new ArgumentException("start date must be before end date");
            }

            //build CTX
            var ctxRequest = new CalculateOvertimeRequest()
            {
                PersonId = personId,
                StartDate = startDate,
                EndDate = endDate,
                PaycycleId = payCycleId
            };

            //invoke CTX
            var ctxResponse = await transactionInvoker.ExecuteAsync<CalculateOvertimeRequest, CalculateOvertimeResponse>(ctxRequest);
                      

            //error checking
            if (ctxResponse == null)
            {
                var message = "Unknown error resulted in a null CTX response";
                logger.Error(message);
                throw new ApplicationException(message);
            }
            if (!string.IsNullOrEmpty(ctxResponse.ErrorMessage))
            {
                logger.Error(ctxResponse.ErrorMessage);
                throw new ApplicationException(ctxResponse.ErrorMessage);
            }

            if (!ctxResponse.StartDate.HasValue || !ctxResponse.EndDate.HasValue)
            {
                var message = "Start and End Dates from CTX must have value";
                LogDataError("CalculateOvertime", string.Format("{0}:{1}-{2}", personId, startDate, endDate), ctxResponse, null, message);  
                throw new ApplicationException(message);
            }
            if (ctxResponse.Thresholds.Any(t => !t.ThresholdTotalOvertimeMinutes.HasValue || string.IsNullOrEmpty(t.ThresholdEarningsTypeId) || !t.ThresholdRateFactor.HasValue)) 
            {
                var message = "All thresholds must contain Total Overtime Minutes, a factor, and an earnings type id";
                LogDataError("CalculateOvertime", string.Format("{0}:{1}-{2}", personId, startDate, endDate), ctxResponse, null, message);                
                throw new ApplicationException(message);
            }
            if (ctxResponse.DayAllocations.Any(d => !d.DayAllocationsDate.HasValue || !d.DayAllocationsOvertimeMinutes.HasValue || !d.DayAllocationsThresholdPosition.HasValue))
            {
                var message = "All day allocations in must contain a date, overtime minutes, and a position";
                LogDataError("CalculateOvertime", string.Format("{0}:{1}-{2}", personId, startDate, endDate), ctxResponse, null, message);
                throw new ApplicationException(message);

            }

            //build result
            var result = new OvertimeCalculationResult(ctxResponse.PersonId, ctxResponse.PaycycleId, ctxResponse.StartDate.Value, ctxResponse.EndDate.Value, DateTime.Now);

            for (int i = 0; i < ctxResponse.Thresholds.Count; i++)
            {
                var earningsTypeId = ctxResponse.Thresholds[i].ThresholdEarningsTypeId;
                var factor = ctxResponse.Thresholds[i].ThresholdRateFactor.Value;
                var totalOvertimeMinutes = ctxResponse.Thresholds[i].ThresholdTotalOvertimeMinutes.Value;
               

                var thresholdEntity = new OvertimeThresholdResult(earningsTypeId, factor, TimeSpan.FromMinutes(totalOvertimeMinutes));
                
                //find the day allocations that match the position of the threshold
                var thresholdDayAllocations = ctxResponse.DayAllocations.Where(d => d.DayAllocationsThresholdPosition.Value == i).ToList();
                
                //build DailyOvertimeAllocation objects for this threshold
                if (thresholdDayAllocations != null && thresholdDayAllocations.Any())
                {
                    var dayAllocationEntities = thresholdDayAllocations.Select(td =>
                        new DailyOvertimeAllocation(td.DayAllocationsDate.Value, TimeSpan.FromMinutes(td.DayAllocationsOvertimeMinutes.Value)));

                    thresholdEntity.DailyAllocations.AddRange(dayAllocationEntities);
                }

                result.Thresholds.Add(thresholdEntity);
            }

            return result;
        }
    }
}
