﻿// Copyright 2016 Ellucian Company L.P. and its affiliates.
using Ellucian.Colleague.Data.TimeManagement.Transactions;
using Ellucian.Colleague.Domain.Base.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Entities;
using Ellucian.Colleague.Domain.TimeManagement.Repositories;
using Ellucian.Data.Colleague;
using Ellucian.Data.Colleague.Repositories;
using Ellucian.Web.Cache;
using Ellucian.Web.Dependency;
using Ellucian.Web.Http.Configuration;
using slf4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ellucian.Colleague.Data.TimeManagement.Repositories
{
    [RegisterType(Lifetime = RegistrationLifetime.Hierarchy)]
    public class TimecardStatusesRepository : BaseColleagueRepository, ITimecardStatusesRepository
    {

        private readonly string colleagueTimeZone;
        private readonly int bulkReadSize;

        #region CONSTRUCTOR(S)

        /// <summary>
        /// Repository constrcutor
        /// </summary>
        /// <param name="cacheProvider"></param>
        /// <param name="transactionFactory"></param>
        /// <param name="logger"></param>
        /// <param name="settings"></param>
        public TimecardStatusesRepository(ICacheProvider cacheProvider, IColleagueTransactionFactory transactionFactory, ILogger logger, ApiSettings settings)
            : base(cacheProvider, transactionFactory, logger)
        {
            colleagueTimeZone = settings.ColleagueTimeZone;
            bulkReadSize = settings != null && settings.BulkReadSize > 0 ? settings.BulkReadSize : 5000;
        }
        #endregion

        /// <summary>
        /// Creates a timecard status record
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        public async Task<IEnumerable<TimecardStatus>> CreateTimecardStatusesAsync(List<TimecardStatus> statuses)
        {
            // check the argument for errors
            if (statuses == null || statuses.Count() == 0)
            {
                var message = "timecard status is required to create a timecard status record";
                logger.Error(message);
                throw new ArgumentNullException("status", message);
            }

            var request = new Ellucian.Colleague.Data.TimeManagement.Transactions.CreateTimecardStatusesRequest();
           
            foreach (var status in statuses)
            {
                request.TimecardStatusGroup.Add(new Data.TimeManagement.Transactions.TimecardStatusGroup() {
                    TsActionerId = status.ActionerId, 
                    TsActionType = status.ActionType.ToString(),
                    TsTimecardId = status.TimecardId
                });
            }

            CreateTimecardStatusesResponse response = null;
            try
            {
                // execute the request
                response = await transactionInvoker.ExecuteAsync<CreateTimecardStatusesRequest, CreateTimecardStatusesResponse>(request);
            }
            catch (Exception ex)
            {
                logger.Error(ex.Message);
                throw new ApplicationException("Error occurred in timecard transaction. See API log for details.");
            }

            // check the response for errors and throw an exception for any irregularities
            if (response == null)
            {
                var message = "Could not create timecard status. Unexpected null response from CTX.";
                logger.Error(message);
                throw new ApplicationException(message);
            }
            if (response.ErrorMessage != null && response.ErrorMessage.Count() > 0)
            {
                foreach (var errorMessage in response.ErrorMessage)
                {
                    logger.Error(errorMessage);
                }
            }
            if (response.NewKey.Count() < request.TimecardStatusGroup.Count())
            {
                logger.Error("Error: Timecard Status keys not returned for all requested timecards");
            }

            var newTimecardStatuses = new List<TimecardStatus>();
            if (response.NewKey.Count() > 0)
            {
                // return the newly created statuses
                var createdStatuses = await DataReader.BulkReadRecordAsync<Ellucian.Colleague.Data.TimeManagement.DataContracts.TimecardStatus>(response.NewKey.ToArray());
                if (createdStatuses == null || createdStatuses.Count() == 0)
                {
                    var message = "There was an error creating the timecard statuses; no records retrieved by bulk read.";
                    logger.Error(message);
                    throw new ApplicationException(message);
                }
                foreach (var newKey in response.NewKey)
                {
                    var createdStatus = createdStatuses.Where(c => c.Recordkey == newKey).FirstOrDefault();
                    if (createdStatus == null)
                    {
                        logger.Error("There was an error creating the timecard status. The record does not exist for the created id " + newKey);
                    }
                    else
                    {
                        newTimecardStatuses.Add(BuildTimecardStatusEntity(createdStatus));
                    }
                }
            }
            return newTimecardStatuses;
        }

        public async Task<IEnumerable<TimecardStatus>> GetTimecardStatusesByTimecardIdAsync(string timecardId)
        {
            // the id of the timecard must be present to obtain the statuses...
            if (string.IsNullOrEmpty(timecardId))
            {
                var message = "timecard id is required to get timecard statuses";
                logger.Error(message);
                throw new ArgumentNullException(message);
            }

            // attempt to obtain the timecard status  for this timecard...
            var timecardStatusCriteria = "WITH TS.TIMECARD.ID EQ " + timecardId;
            var dbTimecardStatues = await DataReader.BulkReadRecordAsync<DataContracts.TimecardStatus>(timecardStatusCriteria);
            if (dbTimecardStatues == null)
            {
                var message = "Unexpected null returned from TIMECARD.STATUS BulkReadRecordAsync";
                logger.Error(message);
                throw new ApplicationException(message);
            }

            var domainTimecardStatuses = new List<TimecardStatus>();

            // new up a timecard domain object and add to return list...
            foreach (var thisTimecardStatus in dbTimecardStatues)
            {
                try
                {
                    var status = BuildTimecardStatusEntity(thisTimecardStatus);
                    domainTimecardStatuses.Add(status);
                }
                catch (Exception e)
                {
                    LogDataError("TIMECARD.STATUS", thisTimecardStatus.Recordkey, thisTimecardStatus, e, e.Message);
                }
            }

            return domainTimecardStatuses;
        }

        public async Task<IEnumerable<TimecardStatus>> GetTimecardStatusesByPersonIdsAsync(IEnumerable<string> personIds)
        {
            if (personIds == null)
            {
                throw new ArgumentNullException("personIds");
            }
            if (!personIds.Any())
            {
                logger.Info("No person ids passed to TimecardStatus repository");
                return new List<TimecardStatus>();
            }

            var timecardIdsCriteria = "WITH TCD.EMPLOYEE.ID EQ ?";
            var timecardIds = await DataReader.SelectAsync("TIMECARD", timecardIdsCriteria, personIds.Select(id => string.Format("\"{0}\"", id)).ToArray());
            if (timecardIds == null || !timecardIds.Any())
            {
                logger.Info("No timecard ids selected from given list of person ids");
                return new List<TimecardStatus>();
            }

            var timecardStatusIdsCriteria = "WITH TS.TIMECARD.ID EQ ?";
            var timecardStatusIds = await DataReader.SelectAsync("TIMECARD.STATUS", timecardStatusIdsCriteria, timecardIds.Select(id => string.Format("\"{0}\"", id)).ToArray());

            if (timecardStatusIds == null || !timecardStatusIds.Any())
            {
                logger.Info("No timecard status ids selected from of timecard ids");
                return new List<TimecardStatus>();
            }

            var timecardStatusRecords = new List<DataContracts.TimecardStatus>();
            for (int i = 0; i < timecardStatusIds.Length; i += bulkReadSize)
            {
                var subList = timecardStatusIds.Skip(i).Take(bulkReadSize);
                var records = await DataReader.BulkReadRecordAsync<DataContracts.TimecardStatus>(subList.ToArray());
                if (records == null)
                {
                    logger.Info("Null TimecardStatus records returned from BulkRead");
                }
                else
                {
                    timecardStatusRecords.AddRange(records);
                }
            }

            var timecardStatusEntities = new List<TimecardStatus>();
            foreach (var record in timecardStatusRecords)
            {
                try
                {
                    var entity = BuildTimecardStatusEntity(record);
                    timecardStatusEntities.Add(entity);
                }
                catch (Exception e)
                {
                    LogDataError("TIMECARD.STATUS", record.Recordkey, record, e, "Unable to build TimecardStatus from record");
                }
            }

            return timecardStatusEntities;

        }

        private TimecardStatus BuildTimecardStatusEntity(DataContracts.TimecardStatus timecardStatusRecord)
        {
            if (timecardStatusRecord == null)
            {
                throw new ArgumentNullException("timecardStatusRecord");
            }

            StatusAction action;
            if (!Enum.TryParse(timecardStatusRecord.TsActionType, true, out action))
            {
                var message = string.Format("Unable to resolve action type for timecardStatus {0}", timecardStatusRecord.Recordkey);
                throw new ApplicationException(message);
            }

            var newTimecardStatus = new TimecardStatus(
                timecardStatusRecord.TsTimecardId,
                timecardStatusRecord.TsActionerId,
                action,
                timecardStatusRecord.Recordkey)
            {
                Timestamp = new Timestamp(timecardStatusRecord.TimecardStatusAddopr,
                    timecardStatusRecord.TimecardStatusAddtime.ToPointInTimeDateTimeOffset(timecardStatusRecord.TimecardStatusAdddate, colleagueTimeZone).Value,
                    timecardStatusRecord.TimecardStatusChgopr,
                    timecardStatusRecord.TimecardStatusChgtime.ToPointInTimeDateTimeOffset(timecardStatusRecord.TimecardStatusChgdate, colleagueTimeZone).Value)

            };

            return newTimecardStatus;

        }

    }
}
